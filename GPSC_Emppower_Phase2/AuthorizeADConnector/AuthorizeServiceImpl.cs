using System;
using System.Configuration;
using System.DirectoryServices;
using System.Reflection;
using ESS.SECURITY;
using ESS.SHAREDATASERVICE;
using ESS.AUTHORIZE.AD.BSA_Authorize;
using System.Net;
namespace ESS.AUTHORIZE.AD
{
    public class AuthorizeServiceImpl : AbstractAuthorizeService
    {
        #region Constructor
        public AuthorizeServiceImpl(string oCompanyCode)
        {
            //Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID);
            CompanyCode = oCompanyCode;
        }

        #endregion Constructor

        #region Member
        //private Dictionary<string, string> Config { get; set; }
        internal string BSA_CheckAuthorizeURL
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BSA_CheckAuthorizeURL", "");
            }
        }
        internal string BSA_CheckAuthorizeUserName
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BSA_CheckAuthorizeUserName", "");
            }
        }
        internal string BSA_CheckAuthorizePassword
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BSA_CheckAuthorizePassword", "");
            }
        }
        public bool IsAuthorizeWithESS
        {
            get
            {
                return Convert.ToBoolean(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "IsAuthorizeWithESS", "False").ToString());
            }
        }

        public string CompanyCode { get; set; }

        private static string ModuleID = "ESS.AUTHORIZE.AD";

        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }

        #endregion Member

        #region " private data "



        private string BYPASS
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BYPASS").ToUpper();
            }
        }

        private string PATH
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "PATH");
            }
        }

        private string DOMAIN
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "DOMAIN");
            }
        }
        #endregion " private data "

        #region IAuthorizeService Members

        #region " CheckAuthorize "
        public bool CheckAutorize(string oUserName, string oPassword)
        {
            bool flg = false;
            SecurityService oSecurityService = new SecurityService();
            oSecurityService.Url = BSA_CheckAuthorizeURL;
            if (!BSA_CheckAuthorizeUserName.Equals(string.Empty))
            {
                oSecurityService.Credentials = new NetworkCredential(BSA_CheckAuthorizeUserName, BSA_CheckAuthorizePassword);
            }
            try
            {
                flg = oSecurityService.CheckAuthorize(oUserName, oPassword);
            }
            catch (Exception ex)
            {
                flg = false;
            }
            return flg;
        }
        public override bool CheckAuthroize(string UserName, string Password)
        {
            bool flg = false;
            if (BYPASS == "TRUE")
            {
                flg = true;
            }
            else if(IsAuthorizeWithESS)
            {
                flg = CheckAutorize(UserName, Password);
            }
            else
            {
                DirectoryEntry entry = new DirectoryEntry(@"LDAP://" + PATH, UserName, Password, AuthenticationTypes.Secure);
                try
                {
                    DirectorySearcher search = new DirectorySearcher(entry);

                    search.Filter = "(SAMAccountName=" + UserName + ")";
                    search.PropertiesToLoad.Add("cn");
                    SearchResult result = search.FindOne();

                    if (null == result)
                    {
                        flg = true;
                    }
                }
                catch (Exception ex)
                {
                    if (isPasswordExpired(UserName))
                    {
                        throw new Exception("Password Expired");
                    }
                    throw ex;
                }
            }
            return flg;
        }

        private bool isPasswordExpired(string UserName)
        {
            DirectoryEntry de = new DirectoryEntry("LDAP://" + PATH);
            DirectorySearcher search = new DirectorySearcher(de);
            search.Filter = "(SAMAccountName=" + UserName + ")";
            search.PropertiesToLoad.Add("maxPwdAge");
            search.PropertiesToLoad.Add("pwdLastSet");
            search.PropertiesToLoad.Add("userAccountControl");
            SearchResult result = search.FindOne();
            try
            {
                Int32 val1 = (Int32)result.Properties["userAccountControl"][0];
                if ((val1 & 65536) == 65536)
                {
                    return false;
                }
                else
                {
                    Int64 val = (Int64)result.Properties["pwdLastSet"][0];
                    DateTime d = DateTime.FromFileTime(val);
                    TimeSpan maxPwdAge = GetMaxPasswordAge();
                    DateTime exp = d.Add(maxPwdAge);
                    return exp < DateTime.Now;
                }
            }
            catch
            {
            }
            return false;
        }

        private static TimeSpan GetMaxPasswordAge()
        {
            using (System.DirectoryServices.ActiveDirectory.Domain d = System.DirectoryServices.ActiveDirectory.Domain.GetCurrentDomain())
            using (DirectoryEntry domain = d.GetDirectoryEntry())
            {
                DirectorySearcher ds = new DirectorySearcher(
                  domain,
                  "(objectClass=*)",
                  null,
                  SearchScope.Base
                  );
                SearchResult sr = ds.FindOne();
                TimeSpan maxPwdAge = TimeSpan.MinValue;
                if (sr.Properties.Contains("maxPwdAge"))
                    maxPwdAge = TimeSpan.FromTicks((long)sr.Properties["maxPwdAge"][0]);
                return maxPwdAge.Duration();
            }
        }

        public override bool ValidateDomain(string Domain)
        {
            return this.DOMAIN.Trim().ToUpper() == Domain.Trim().ToUpper();
        }
        #endregion " CheckAuthorize "

        #endregion IAuthorizeService Members
    }
}