﻿using ESS.EMPLOYEE;
using ESS.HR.BE.DATACLASS;
using ESS.HR.BE.INFOTYPE;
using System;
using System.Collections.Generic;
using System.Data;
using ESS.WORKFLOW;
using ESS.UTILITY.DATACLASS;
using ESS.UTILITY.LOG;
using System.Linq;
using ClosedXML.Excel;
using ESS.PORTALENGINE;
using ESS.SHAREDATASERVICE;
using ESS.HR.TM.INFOTYPE;

namespace ESS.HR.BE
{
    public class HRBEManagement
    {
        #region Constructor
        private HRBEManagement()
        {
        }

        #endregion

        #region MultiCompany  Framework
        private static Dictionary<string, HRBEManagement> Cache = new Dictionary<string, HRBEManagement>();

        private static string ModuleID = "ESS.HR.BE";

        public string CompanyCode { get; set; }


        public string PreviouYearEmployeeBenifits
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BE_PREVIOUS_YEAR");
            }
        }

        public Boolean IsActiveFlexibleBenefitDashboard
        {
            get
            {
                return Boolean.Parse(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "IsActiveFlexibleBenefitDashboard"));
            }
        }

        public Boolean IsActiveHealthInsuranceDashboard
        {
            get
            {
                return Boolean.Parse(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "IsActiveHealthInsuranceDashboard"));
            }
        }

        public static HRBEManagement CreateInstance(string oCompanyCode)
        {
            HRBEManagement oHRBEManagement = new HRBEManagement()
            {
                CompanyCode = oCompanyCode
            };
            return oHRBEManagement;
        }
        #endregion MultiCompany  Framework

        public void SaveBETraining(TrainingForDev oTrainingForDev)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveBETraining(oTrainingForDev);
        }

        public List<TrainingForDev> GetBETraining(String EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetBETraining(EmployeeID);
        }

        public List<BERegistorHealthInsuranceConfig> GetRegistorHealthInsuranceConfig(String EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetRegistorHealthInsuranceConfig(EmployeeID);
        }

        public List<BERegistorHealthInsuranceStatusConfigList> GetRegistorHealthInsuranceStatusConfig(String EmployeeID, int year)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetRegistorHealthInsuranceStatusConfig(EmployeeID, year);
        }


        public INFOTYPE0002 GetPersonalData(string EmployeeID, DateTime checkDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ERPData.GetPersonalData(EmployeeID, checkDate);
        }

        public List<INFOTYPE0021> GetFamilyDataB(string EmployeeID)
        {
            List<INFOTYPE0021> oReturn = ServiceManager.CreateInstance(CompanyCode).ERPData.GetFamilyDataB(EmployeeID);

            //Addby:Nuttapon.a 12.03.2013  PTT requirement ONLY
            //Fillter subtype 6 (����Ѻ�Ż���ª��) �͡���������� user maintain ��ҹ�к��ͧ�ع���ͧ
            //oReturn = oReturn.FindAll(
            //    delegate (INFOTYPE0021 temp)
            //    {
            //        return temp.FamilyMember != ServiceManager.CreateInstance(CompanyCode).ProvidentFundHeir
            //        && temp.FamilyMember != ServiceManager.CreateInstance(CompanyCode).LifeInsuranceHeir
            //        && temp.FamilyMember != ServiceManager.CreateInstance(CompanyCode).AccidentInsuranceHeir
            //        && temp.FamilyMember != ServiceManager.CreateInstance(CompanyCode).Surety;
            //    });
            return oReturn;
        }

        public DataSet GetReportData(DateTime UpdateDate_From, DateTime UpdateDate_To, string Types, string Status, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetReportData(UpdateDate_From, UpdateDate_To, Types, Status, LanguageCode);
        }

        public List<BEEducationAllowance> GetEducationAllowance(String EmployeeID, String RequestNo, String KeyYear)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetEducationAllowance(EmployeeID, RequestNo, KeyYear);
        }

        public List<BEEducationAllowance> GetEducationAllowanceQuotaList(String EmployeeID, String KeyYear)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetEducationAllowanceQuotaList(EmployeeID, KeyYear);
        }

        public List<BEEducationAllowance> GetQuotaList(String EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetQuotaList(EmployeeID);
        }

        public bool EducationAllowanceCheckLevel(string EmployeeID, string RequestNo, string ChildNo, string Year, int EducationLevel)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.EducationAllowanceCheckLevel(EmployeeID, RequestNo, ChildNo, Year, EducationLevel);
        }


        internal void SaveBEEducationAllowance(BEEducationAllowance oBEEducationAllowance)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveBEEducationAllowance(oBEEducationAllowance);
        }

        public List<BEConfig> GetBEConfig(String BE_Type)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetBEConfig(BE_Type);
        }

        public List<INFOTYPE0021> GetChildsData(string EmployeeID)
        {
            List<INFOTYPE0021> oReturn = ServiceManager.CreateInstance(CompanyCode).ERPData.GetChildsData(EmployeeID);


            return oReturn;
        }


        public List<INFOTYPE0021> GetFamilyData(string EmployeeID, string SubType)
        {
            List<INFOTYPE0021> oReturn = ServiceManager.CreateInstance(CompanyCode).ERPData.GetFamilyData(EmployeeID, SubType);


            return oReturn;
        }


        public decimal CheckQuota(string EmployeeID, string EducationYear, string ChildNo, int EducationLevel, string RequestNo)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.CheckQuota(EmployeeID, EducationYear, ChildNo, EducationLevel, RequestNo);
        }

        public List<BEEducationAllowance> UpdateDiscountStatus(String EmployeeID, String EducationLevel, String check)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.UpdateDiscountStatus(EmployeeID, EducationLevel, check);
        }

        public List<BEEducationAllowance> CheckRequestNo(String RequestNo)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.CheckRequestNo(RequestNo);
        }



        public bool CheckDupplicate(string EmployeeID, string ReceiptNumber, DateTime PayDate, string RequestNo)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.CheckDupplicate(EmployeeID, ReceiptNumber, PayDate, RequestNo);
        }

        public List<BEHealthInsAllowanceConfig> GetHealthInsAllowanceConfig(string Year)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetHealthInsAllowanceConfig(Year);
        }


        internal void SaveBEHealthInsAllowance(BEHealthInsAllowance oBEHealthInsAllowance)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveBEHealthInsAllowance(oBEHealthInsAllowance);
        }

      

        internal void SaveBEHealthInsAllowanceItem(BEHealthInsAllowanceItem oBEHealthInsAllowanceItem)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveBEHealthInsAllowanceItem(oBEHealthInsAllowanceItem);
        }

        internal void DeleteBEHealthInsAllowanceItem(string RequestNo)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.DeleteBEHealthInsAllowanceItem(RequestNo);
        }

        internal void DeleteBERegisterHealthInsuranceItem(string RequestNo)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.DeleteBERegisterHealthInsuranceItem(RequestNo);
        }

        


        internal void SaveRegisterBEHealthInsurance(BERegisterHealthInsurance oBEHealthIns)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveRegisterBEHealthInsurance(oBEHealthIns);
        }

        internal void SaveRegisterBEHealthInsuranceItem(BERegisterHealthInsuranceItem oBEHealthInsItem)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveRegisterBEHealthInsuranceItem(oBEHealthInsItem);
        }


        public List<BEHealthInsAllowance> GetHealthInsAllowance(String EmployeeID, String RequestNo, String KeyYear)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetHealthInsAllowance(EmployeeID, RequestNo, KeyYear);
        }

        public List<BEHealthInsAllowanceItem> GetHealthInsAllowanceItem(String EmployeeID, String RequestNo)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetHealthInsAllowanceItem(EmployeeID, RequestNo);
        }

        public List<BEHealthInsAllowance> GetHealthInsAllowanceQuota(String EmployeeID, String Year, String FamilyData)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetHealthInsAllowanceQuota(EmployeeID, Year, FamilyData);
        }

        public List<YearsConfig> GetYears()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetYears();
        }

        public decimal BEHealthInsAllowanceCheckQuota(string EmployeeID, string Year, int RelationshipType, string RequestNo)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.BEHealthInsAllowanceCheckQuota(EmployeeID, Year, RelationshipType, RequestNo);
        }

        public List<BEFitnessAllowance> GetBEFitnessAllowance(String EmployeeID, String RequestNo, String KeyYear)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetBEFitnessAllowance(EmployeeID, RequestNo, KeyYear);
        }

        public List<BEFitnessAllowanceItem> GetBEFitnessAllowanceItem(String EmployeeID, String RequestNo)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetBEFitnessAllowanceItem(EmployeeID, RequestNo);
        }

        public List<BEFitnessAllowance> GetBEFitnessAllowanceQuota(String EmployeeID, DateTime PassProbation, String Year)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetBEFitnessAllowanceQuota(EmployeeID, PassProbation, Year);
        }


        public decimal BEFitnessAllowanceCheckQuota(string EmployeeID, string Year, string RequestNo)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.BEFitnessAllowanceCheckQuota(EmployeeID, Year, RequestNo);
        }


        internal void SaveBEFitnessAllowance(BEFitnessAllowance oBEFitnessAllowance)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveBEFitnessAllowance(oBEFitnessAllowance);
        }

        internal void SaveBEFitnessAllowanceItem(BEFitnessAllowanceItem oBEFitnessAllowanceItem)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveBEFitnessAllowanceItem(oBEFitnessAllowanceItem);
        }


        internal void DeleteBEFitnessAllowanceItem(string RequestNo)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.DeleteBEFitnessAllowanceItem(RequestNo);
        }


        public List<BEFitnessAllowanceConfig> GetBEFitnessAllowanceConfig(string EmployeeID, DateTime PassProbation, string Year)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetBEFitnessAllowanceConfig(EmployeeID, PassProbation, Year);
        }

        public bool HealthInsAllowanceCheckDupplicate(string EmployeeID, string ReceiptNumber, DateTime RecieptDate, string RequestNo)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.HealthInsAllowanceCheckDupplicate(EmployeeID, ReceiptNumber, RecieptDate, RequestNo);
        }

        public bool FitnessAllowanceCheckDupplicate(string EmployeeID, string ReceiptNumber, DateTime RecieptDate, string RequestNo)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.FitnessAllowanceCheckDupplicate(EmployeeID, ReceiptNumber, RecieptDate, RequestNo);
        }

        public int BEFitnessAllowanceCheckEnrollment(string EmployeeID, string Year)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.BEFitnessAllowanceCheckEnrollment(EmployeeID, Year);
        }


        public List<BEPostingData> GetBEEducationAllowanceGetToPosting(int AllowanceWagetypeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetBEEducationAllowanceGetToPosting(AllowanceWagetypeID);
        }

        public List<BEPostingData> GetBEFitnessAllowanceGetToPosting(int AllowanceWagetypeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetBEFitnessAllowanceGetToPosting(AllowanceWagetypeID);
        }

        public List<BEPostingData> GetBEHealthInsAllowanceGetToPosting(int AllowanceWagetypeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetBEHealthInsAllowanceGetToPosting(AllowanceWagetypeID);
        }

        public List<BEPostingData> GetBEBereavementInfoGetToPosting(int AllowanceWagetypeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetBEBereavementInfoGetToPosting(AllowanceWagetypeID);
        }

        public List<BEPostingData> GetFBReimburseGetToPosting(int AllowanceWagetypeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetFBReimburseGetToPosting(AllowanceWagetypeID);
        }

        public void UpdateBEEducationAllowancePosting(bool StatusPost, string RequestNo, string PostingMessage, DateTime PostingDate)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.UpdateBEEducationAllowancePosting(StatusPost, RequestNo, PostingMessage, PostingDate);
        }

        public void UpdateBEFitnessAllowancePosting(bool StatusPost, string RequestNo, string PostingMessage, DateTime PostingDate)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.UpdateBEFitnessAllowancePosting(StatusPost, RequestNo, PostingMessage, PostingDate);
        }

        public void UpdateBEHealthInsAllowancePosting(bool StatusPost, string RequestNo, string PostingMessage, DateTime PostingDate)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.UpdateBEHealthInsAllowancePosting(StatusPost, RequestNo, PostingMessage, PostingDate);
        }

        public void UpdateBEBereavementInfoPosting(bool StatusPost, string RequestNo, string PostingMessage, DateTime PostingDate)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.UpdateBEBereavementInfoPosting(StatusPost, RequestNo, PostingMessage, PostingDate);
        }

        public void UpdateFBReimbursePosting(bool StatusPost, string RequestNo, string PostingMessage, DateTime PostingDate)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.UpdateFBReimbursePosting(StatusPost, RequestNo, PostingMessage, PostingDate);
        }

        public void SaveWageAdditionalData(BEPostingData oBEPostingData)
        {
            ServiceManager.CreateInstance(CompanyCode).ERPData.SaveWageAdditionalData(oBEPostingData);
        }

        public DataTable SaveWageAdditionalToSAP(List<BEPostingData> oBEPostingDataList)
        {
            DataTable oException = new DataTable();
            oException.Columns.Add("detail", typeof(string));

            List<string> oListReqSuccess = new List<string>();
            List<BEPostingData> oTransactions1 = oBEPostingDataList.ToList();
            List<BEPostingData> oTransactions2 = oBEPostingDataList.ToList();
            Dictionary<string, string> temp = new Dictionary<string, string>();
            oTransactions1.ForEach(current =>
            {
                List<BEPostingData> oTempTrans = oTransactions2.FindAll(all => all.EmployeeID == current.EmployeeID && all.BeginDate == current.BeginDate);
                if (oTempTrans != null && oTempTrans.Count > 1)
                {
                    if (!temp.ContainsKey(current.EmployeeID))
                    {
                        current.Amount = oTempTrans.Sum(all => all.Amount);
                        temp[current.EmployeeID] = string.Join(",", oTempTrans.Select(all => all.RequestNo));
                    }
                    else
                        current.Amount = 0;
                }
            });


            foreach (BEPostingData oBEPostingData in oTransactions1)
            {
                try
                {
                    string sFirstReqNo = string.Empty;
                    string sDic = temp.FirstOrDefault(all => all.Value.IndexOf(oBEPostingData.RequestNo) >= 0).Value;
                    if (!String.IsNullOrEmpty(sDic))
                    {
                        sFirstReqNo = sDic.Split(',')[0];
                    }
                    if (!oListReqSuccess.Contains(sFirstReqNo))
                    {
                        SaveWageAdditionalData(oBEPostingData);
                    }

                    oListReqSuccess.Add(oBEPostingData.RequestNo);

                    if (oBEPostingData.WageTypeID == 1)
                    {
                        UpdateBEEducationAllowancePosting(true, oBEPostingData.RequestNo, string.Empty, oBEPostingData.BeginDate);
                    }
                    else if (oBEPostingData.WageTypeID == 2)
                    {
                        UpdateBEFitnessAllowancePosting(true, oBEPostingData.RequestNo, string.Empty, oBEPostingData.BeginDate);
                    }
                    else if (oBEPostingData.WageTypeID == 3)
                    {
                        UpdateBEHealthInsAllowancePosting(true, oBEPostingData.RequestNo, string.Empty, oBEPostingData.BeginDate);
                    }
                    else if (oBEPostingData.WageTypeID == 4)
                    {
                        UpdateBEBereavementInfoPosting(true, oBEPostingData.RequestNo, string.Empty, oBEPostingData.BeginDate);
                    }
                    else if (oBEPostingData.WageTypeID == 5)
                    {
                        UpdateFBReimbursePosting(true, oBEPostingData.RequestNo, string.Empty, oBEPostingData.BeginDate);
                    }

                }
                catch (Exception ex)
                {
                    DataRow dr = oException.NewRow();
                    dr["detail"] = string.Format("{0} : {1}", oBEPostingData.EmployeeID, ex.Message);
                    oException.Rows.Add(dr);
                    if (oBEPostingData.WageTypeID == 1)
                    {
                        UpdateBEEducationAllowancePosting(false, oBEPostingData.RequestNo, ex.Message, oBEPostingData.BeginDate);
                    }
                    else if (oBEPostingData.WageTypeID == 2)
                    {
                        UpdateBEFitnessAllowancePosting(false, oBEPostingData.RequestNo, ex.Message, oBEPostingData.BeginDate);
                    }
                    else if (oBEPostingData.WageTypeID == 3)
                    {
                        UpdateBEHealthInsAllowancePosting(false, oBEPostingData.RequestNo, ex.Message, oBEPostingData.BeginDate);
                    }
                    else if (oBEPostingData.WageTypeID == 4)
                    {
                        UpdateBEBereavementInfoPosting(false, oBEPostingData.RequestNo, ex.Message, oBEPostingData.BeginDate);
                    }
                    else if (oBEPostingData.WageTypeID == 5)
                    {
                        UpdateFBReimbursePosting(false, oBEPostingData.RequestNo, ex.Message, oBEPostingData.BeginDate);
                    }
                }
            }
            return oException;
        }

        public bool InsertActionLog(object objOld, object objNew, string oAction, bool oStatus)
        {
            bool flg = false;
            ActionLog oActionLog = new ActionLog();
            oActionLog.LogAction = oAction;
            oActionLog.LogActionBy = string.Format("{0}", "SYSTEM");
            oActionLog.LogData = string.Format("OLD : {0}\r\n,NEW : {1}", (objOld != null) ? LogMgr.GetSerialize(objOld) : "", (objNew != null) ? LogMgr.GetSerialize(objNew) : "");
            oActionLog.LogActionDate = DateTime.Now; //  generate from db
            oActionLog.LogID = Guid.NewGuid().ToString();//  generate from db
            oActionLog.LogStatus = oStatus;

            flg = ServiceManager.CreateInstance(CompanyCode).ESSData.InsertActionLog(oActionLog);
            return flg;
        }


        #region Bereavement
        public List<BEBereavementInfo> GetBereavementInfoHistory(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetBereavementInfoHistory(EmployeeID);
        }

        public List<BEBereavementInfo> GetBereavementItem(string EmployeeID, string RequestNo, string KeyType)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetBereavementItem(EmployeeID, RequestNo, KeyType);
        }

        public List<BEBereavementConfig> GetBereavementConfig()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetBereavementConfig();
        }

        public void SaveBEBereavementInfo(BEBereavementInfo oBereavementInfo)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveBEBereavementInfo(oBereavementInfo);
        }

        public void SaveBEBereavementItem(BEBereavementItem oBereavementItem)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveBEBereavementItem(oBereavementItem);
        }

        public void DeleteBEBereavementItem(string RequestNo)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.DeleteBEBereavementItem(RequestNo);
        }


        public void SaveFamilyData(List<INFOTYPE0021> data)
        {
            ServiceManager.CreateInstance(CompanyCode).ERPData.SaveFamilyData(data);
        }

        public bool BEBereavementCheckDupplicate(string EmployeeID, int BereavementType, string RequestNo)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.BEBereavementCheckDupplicate(EmployeeID, BereavementType, RequestNo);
        }

        #endregion Bereavement


        public List<BenefitReport> GetBenefitReport(DateTime UpdateDate_From, DateTime UpdateDate_To, string Benefit_Type, string Benefit_Status, string language)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetBenefitReport(UpdateDate_From, UpdateDate_To, Benefit_Type, Benefit_Status, language);
        }
        


        public bool EmployeeTrainee(string EmpGroup, DateTime PassProbation)
        {
            var result = false;
            //if (EmpGroup == "B" || ((EmpGroup == "I" || EmpGroup == "J")) && DateTime.Now < PassProbation)

            if (EmpGroup == "B")
            {
                result = true;
            }
            return result;
        }


        public List<DataReligion> GetReligionAll()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetReligion();
        }

        #region รายงานรายละเอียดการเบิกสวัสดิการสำหรับผู้ดูแลระบบ

        // รายงานการเบิกเงินช่วยเหลือการศึกษาบุตร
        public void GetSheetExcelEducationAllowance(XLWorkbook workbook, DateTime begin_date, DateTime end_date, string language)
        {
            var sheet = workbook.Worksheets.Add("ค่าการศึกษาบุตร");

            #region Header Excel

            int header_row = 1;
            int header_column = 1;

            sheet.Row(header_row).Cell(header_column).Value = "รายงานการเบิกเงินช่วยเหลือการศึกษาบุตร";
            var range2 = sheet.Range("A1:S1");
            range2.Style.Font.FontSize = 11;
            range2.Style.Font.FontName = "Tahoma";
            var rowInRange = range2.Row(1);
            rowInRange.Merge();
            rowInRange.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            rowInRange.Style.Fill.BackgroundColor = XLColor.LightBlue;

            header_row++;

            sheet.Row(header_row).Cell(header_column).Value = "ลำดับ";
            sheet.Column(header_column).Width = 10;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "รหัสพนักงาน";
            sheet.Column(header_column).Width = 20;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "ชื่อ-สกุลพนักงาน";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "สถานที่ปฏิบัติงาน";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "จังหวัดที่ปฏิบัติงาน";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "ชื่อบุตร";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "อายุบุตร";
            sheet.Column(header_column).Width = 10;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "ลำดับที่บุตร";
            sheet.Column(header_column).Width = 10;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "ระดับการศึกษา";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "ระดับชั้นปี";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "ปีการศึกษา";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "ชื่อสถานศึกษา";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "ประเภทสถานศึกษา";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "ประเภทการเบิก";
            sheet.Column(header_column).Width = 25;
            header_column++;


            sheet.Row(header_row).Cell(header_column).Value = "จำนวนเงิน";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "ยอดเงินที่เบิกได้จริง";
            sheet.Column(header_column).Width = 15;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "วันที่ขออนุมัติ";
            sheet.Column(header_column).Width = 15;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "วันที่อนุมัติ";
            sheet.Column(header_column).Width = 15;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "วันที่ Post ลง SAP";
            sheet.Column(header_column).Width = 15;
            header_column++;

            var rang_header2 = sheet.Range("A2:S2");
            rang_header2.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            rang_header2.Style.Font.FontSize = 11;
            rang_header2.Style.Font.FontName = "Tahoma";
            rang_header2.Style.Fill.BackgroundColor = XLColor.LightBlue;
            rang_header2.Style.Border.SetOutsideBorder(XLBorderStyleValues.Thick);
            rang_header2.Style.Border.SetInsideBorder(XLBorderStyleValues.Thick);



            #endregion

            #region Data Excel

            List<ReportBEEducationAllowanceByAdmin> welfare_education = ServiceManager.CreateInstance(CompanyCode).ESSData.GetReportEducationAllowanceByAdmin(begin_date, end_date);
            if (welfare_education.Count > 0)
            {
                int data_row = 3;
                int data_column;
                int order = 0;
                foreach (var item in welfare_education)
                {
                    data_column = 1;

                    sheet.Row(data_row).Cell(data_column).Value = ++order; // ลำดับ
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.EmployeeID; // รหัสพนักงาน
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.EmployeeName; // ชื่อสกุล - พนักงาน
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.WorkLocationName; // สถานที่ปฎิบัติงาน
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.County; // จังหวัดปฎิบัติงาน
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.FirstName + " " + item.LastName; // ชื่อบุตร
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    data_column++;

                    string age_child = "";
                    if (item.BirthDate != null && item.PayDate != null)
                    {
                        AgeFamily age = CalculateAge(item.BirthDate.Value.Date, item.PayDate.Value.Date);

                        if (language.ToLower() == "th")
                            age_child = string.Format("{0} ปี {1} เดือน", age.Year, age.Month);

                        if (language.ToLower() == "en")
                            age_child = string.Format("{0} Year {1} Month", age.Year, age.Month);
                    }

                    sheet.Row(data_row).Cell(data_column).Value = age_child; // อายุบุตร
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.ChildNo; // ลำดับที่บุตร
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.BE_Level_TH; // ระดับการศึกษา
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.EducationLevel; // ระดับชั้นปี
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.EducationYear; // ปีการศึกษา
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.Academy; // ชื่อสถานศึกษา
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.AcademyType == 1 ? "รัฐบาล" : "เอกชน"; // ประเภทสถานศึกษา
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.WithdrawType == 1 ? "เหมาจ่าย" : "ตามจริง"; // ประเภทการเบิก
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.Amount;  // จำนวนเงิน
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                    sheet.Row(data_row).Cell(data_column).Style.NumberFormat.Format = "#,##0.00";
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.AmountReimburse;  // ยอดเงินที่เบิกได้จริง
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                    sheet.Row(data_row).Cell(data_column).Style.NumberFormat.Format = "#,##0.00";
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.SubmitDate.HasValue ? item.SubmitDate.Value.Date.ToString("yyyy/MM/dd") : ""; // วันที่ขออนุมัติ
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.ActionDate.HasValue ? item.ActionDate.Value.ToString("yyyy/MM/dd") : ""; // วันที่อนุมัติ
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.PostingDate.HasValue ? item.PostingDate.Value.ToString("yyyy/MM/dd") : ""; // วันที่อนุมัติ
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    data_row++;
                }
            }
            #endregion
        }

        // รายงานการเบิกเงินช่วยเหลือค่าสถานออกกำลังกาย
        public void GetSheetExcelFitnessAllowance(XLWorkbook workbook, DateTime begin_date, DateTime end_date)
        {
            var sheet = workbook.Worksheets.Add("ค่าสถานออกกำลังกาย");

            #region Header Excel

            int header_row = 1;
            int header_column = 1;

            sheet.Row(header_row).Cell(header_column).Value = "รายงานการเบิกเงินช่วยเหลือค่าสถานออกกำลังกาย";
            var range2 = sheet.Range("A1:S1");
            range2.Style.Font.FontSize = 11;
            range2.Style.Font.FontName = "Tahoma";
            var rowInRange = range2.Row(1);
            rowInRange.Merge();
            rowInRange.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            rowInRange.Style.Fill.BackgroundColor = XLColor.LightBlue;

            header_row++;

            sheet.Row(header_row).Cell(header_column).Value = "ลำดับ";
            sheet.Column(header_column).Width = 10;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "รหัสพนักงาน";
            sheet.Column(header_column).Width = 20;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "ชื่อ-สกุลพนักงาน";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "สถานที่ปฏิบัติงาน";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "จังหวัดที่ปฏิบัติงาน";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "ชื่อสถานประกอบการ";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "จำนวนเงินก่อน VAT(บาท)";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "VAT(บาท)";
            sheet.Column(header_column).Width = 10;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "จำนวนเงินรวม(บาท)";
            sheet.Column(header_column).Width = 20;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "ยอดเงินที่เบิกได้จริง(บาท)";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "วันที่ขออนุมัติ";
            sheet.Column(header_column).Width = 15;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "วันที่อนุมัติ";
            sheet.Column(header_column).Width = 15;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "วันที่ Post ลง SAP";
            sheet.Column(header_column).Width = 20;
            header_column++;

            var rang_header2 = sheet.Range("A2:M2");
            rang_header2.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            rang_header2.Style.Font.FontSize = 11;
            rang_header2.Style.Font.FontName = "Tahoma";
            rang_header2.Style.Fill.BackgroundColor = XLColor.LightBlue;
            rang_header2.Style.Border.SetOutsideBorder(XLBorderStyleValues.Thick);
            rang_header2.Style.Border.SetInsideBorder(XLBorderStyleValues.Thick);

            #endregion

            #region Data Excel
            List<ReportFitnessAllowanceByAdmin> list_fitness = ServiceManager.CreateInstance(CompanyCode).ESSData.GetReportFitnessAllowanceByAdmin(begin_date, end_date);
            if (list_fitness.Count > 0)
            {
                int data_row = 3;
                int data_column;
                int order = 0;
                var group_list_fitness = list_fitness.GroupBy(s => s.RequestNo).ToList();
                foreach (var fitness in group_list_fitness)
                {
                    int count_dup_transaction = fitness.Count();
                    decimal net_paid = 0;
                    foreach (var item in fitness)
                    {
                        data_column = 1;

                        sheet.Row(data_row).Cell(data_column).Value = ++order; // ลำดับ
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.EmployeeID; // รหัสพนักงาน
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.EmployeeName; // ชื่อสกุล - พนักงาน
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.WorkLocationName; // สถานที่ปฎิบัติงาน
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.County; // จังหวัดที่ปฎิบัติงาน
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.VendorName; // ชื่อสถานประกอบการ
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.AmountExcludeVAT; // จำนวนเงินก่อน VAT(บาท)
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                        sheet.Row(data_row).Cell(data_column).Style.NumberFormat.Format = "#,##0.00";
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.Vat; // VAT(บาท)
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                        sheet.Row(data_row).Cell(data_column).Style.NumberFormat.Format = "#,##0.00";
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.TotalAmount; // จำนวนเงินรวม(บาท)
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                        sheet.Row(data_row).Cell(data_column).Style.NumberFormat.Format = "#,##0.00";
                        data_column++;

                        decimal net_payment = 0;
                        if (item.TotalAmount <= item.PaymentAmount)
                        {
                            net_payment = item.TotalAmount;
                            net_paid = net_paid + (item.PaymentAmount - item.TotalAmount);
                        }
                        else
                        {
                            //net_payment = item.TotalAmount - net_paid;
                            net_payment = item.PaymentAmount;
                        }

                        sheet.Row(data_row).Cell(data_column).Value = net_payment; // ยอดเงินที่เบิกได้จริง (บาท)
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                        sheet.Row(data_row).Cell(data_column).Style.NumberFormat.Format = "#,##0.00";
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.SubmitDate.HasValue ? item.SubmitDate.Value.Date.ToString("yyyy/MM/dd") : ""; // วันที่ขออนุมัติ
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.ActionDate.HasValue ? item.ActionDate.Value.ToString("yyyy/MM/dd") : ""; // วันที่อนุมัติ
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.PostingDate.HasValue ? item.PostingDate.Value.ToString("yyyy/MM/dd") : ""; // วันที่อนุมัติ
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        data_column++;

                        data_row++;
                    }
                }
            }
            #endregion
        }

        // รายงานการเบิกเงินช่วยเหลือค่ารักษาพยาบาลบุพการี
        public void GetSheetExcelHealthInsAllowance(XLWorkbook workbook, DateTime begin_date, DateTime end_date,string language)
        {
            var sheet = workbook.Worksheets.Add("ค่ารักษาพยาบาลบุพการี");

            #region Header Excel

            int header_row = 1;
            int header_column = 1;

            sheet.Row(header_row).Cell(header_column).Value = "รายงานการเบิกเงินช่วยเหลือค่ารักษาพยาบาลบุพการี";
            var range2 = sheet.Range("A1:P1");
            range2.Style.Font.FontSize = 11;
            range2.Style.Font.FontName = "Tahoma";
            var rowInRange = range2.Row(1);
            rowInRange.Merge();
            rowInRange.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            rowInRange.Style.Fill.BackgroundColor = XLColor.LightBlue;

            header_row++;

            sheet.Row(header_row).Cell(header_column).Value = "ลำดับ";
            sheet.Column(header_column).Width = 10;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "รหัสพนักงาน";
            sheet.Column(header_column).Width = 20;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "ชื่อ-สกุลพนักงาน";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "สถานที่ปฏิบัติงาน";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "จังหวัดที่ปฏิบัติงาน";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "ชื่อบุพการี";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "ความสัมพันธ์";
            sheet.Column(header_column).Width = 15;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "อายุบุพการี";
            sheet.Column(header_column).Width = 10;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "ชื่อสถานประกอบการ";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "จำนวนเงินก่อน VAT(บาท)";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "VAT(ถ้ามี)(บาท)";
            sheet.Column(header_column).Width = 20;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "จำนวนเงินรวม(บาท)";
            sheet.Column(header_column).Width = 20;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "ยอดเงินที่เบิกได้จริง(บาท)";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "วันที่ขออนุมัติ";
            sheet.Column(header_column).Width = 15;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "วันที่อนุมัติ";
            sheet.Column(header_column).Width = 15;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "วันที่ Post ลง SAP";
            sheet.Column(header_column).Width = 20;
            header_column++;

            var rang_header2 = sheet.Range("A2:P2");
            rang_header2.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            rang_header2.Style.Font.FontSize = 11;
            rang_header2.Style.Font.FontName = "Tahoma";
            rang_header2.Style.Fill.BackgroundColor = XLColor.LightBlue;
            rang_header2.Style.Border.SetOutsideBorder(XLBorderStyleValues.Thick);
            rang_header2.Style.Border.SetInsideBorder(XLBorderStyleValues.Thick);

            #endregion

            #region Data Excel

            List<ReportHealthInsAllowanceByAdmin> list_HealthInsAllowance = ServiceManager.CreateInstance(CompanyCode).ESSData.GetReportHealthInsAllowanceByAdmin(begin_date, end_date);
            if (list_HealthInsAllowance.Count > 0)
            {
                int data_row = 3;
                int data_column;
                int order = 0;
                var group_list_HealthInsAllowance = list_HealthInsAllowance.GroupBy(s => s.RequestNo).ToList();
                foreach (var healthAllwance in group_list_HealthInsAllowance)
                {
                    int count_dup_transaction = healthAllwance.Count();
                    decimal net_paid = 0;
                    foreach (var item in healthAllwance)
                    {
                        data_column = 1;

                        sheet.Row(data_row).Cell(data_column).Value = ++order; // ลำดับ
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.EmployeeID; // รหัสพนักงาน
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.EmployeeName; // ชื่อสกุล - พนักงาน
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.WorkLocationName; // สถานที่ปฎิบัติงาน
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.County; // จังหวัดปฎิบัติงาน
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.FirstName + " " + item.LastName; // ชื่อบุพการี
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.Relationship; // ความสัมพันธ์
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        data_column++;

                        // คำนวนอายุบุพการี
                        string age_parent = "";
                        if(item.BirthDate != null && item.RecieptDate != null)
                        {
                            AgeFamily age = CalculateAge(item.BirthDate.Value.Date, item.RecieptDate.Value.Date);

                            if(language.ToLower() == "th")
                                age_parent = string.Format("{0} ปี {1} เดือน", age.Year, age.Month);

                            if(language.ToLower() == "en")
                                age_parent = string.Format("{0} Year {1} Month", age.Year, age.Month);
                        }
                        
                        sheet.Row(data_row).Cell(data_column).Value = age_parent; // อายุบุพการี
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.VendorName; // ชื่อสถานประกอบการ
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.AmountExcludeVAT; // จำนวนเงินก่อน VAT(บาท)
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                        sheet.Row(data_row).Cell(data_column).Style.NumberFormat.Format = "#,##0.00";
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.Vat; // VAT(ถ้ามี)(บาท)
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                        sheet.Row(data_row).Cell(data_column).Style.NumberFormat.Format = "#,##0.00";
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.TotalAmount; // จำนวนเงินรวม(บาท) 
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                        sheet.Row(data_row).Cell(data_column).Style.NumberFormat.Format = "#,##0.00";
                        data_column++;

                        decimal net_payment = 0;
                        if (item.TotalAmount <= item.PaymentAmount)
                        {
                            net_payment = item.TotalAmount;
                            net_paid = net_paid + (item.PaymentAmount - item.TotalAmount);
                        }
                        else
                        {
                            net_payment = item.TotalAmount - net_paid;
                        }

                        sheet.Row(data_row).Cell(data_column).Value = net_payment; // ยอดเงินที่เบิกได้จริง(บาท)
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                        sheet.Row(data_row).Cell(data_column).Style.NumberFormat.Format = "#,##0.00";
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.SubmitDate.HasValue ? item.SubmitDate.Value.Date.ToString("yyyy/MM/dd") : ""; // วันที่ขออนุมัติ
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.ActionDate.HasValue ? item.ActionDate.Value.ToString("yyyy/MM/dd") : ""; // วันที่อนุมัติ
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.PostingDate.HasValue ? item.PostingDate.Value.ToString("yyyy/MM/dd") : ""; // วันที่ Post SAP
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        data_column++;

                        data_row++;
                    }
                }
            }
            #endregion
        }

        // รายงานการเบิกเงินช่วยเหลืองานฌาปนกิจสงเคราะห์
        public void GetSheetExcelBereavment(XLWorkbook workbook, DateTime begin_date, DateTime end_date, string language)
        {
            var sheet = workbook.Worksheets.Add("ค่าช่วยเหลืองานฌาปนกิจฯ");

            #region Header Excel

            int header_row = 1;
            int header_column = 1;

            sheet.Row(header_row).Cell(header_column).Value = "รายงานการเบิกเงินช่วยเหลืองานฌาปนกิจสงเคราะห์";
            var range2 = sheet.Range("A1:R1");
            range2.Style.Font.FontSize = 11;
            range2.Style.Font.FontName = "Tahoma";
            var rowInRange = range2.Row(1);
            rowInRange.Merge();
            rowInRange.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            rowInRange.Style.Fill.BackgroundColor = XLColor.LightBlue;

            header_row++;

            sheet.Row(header_row).Cell(header_column).Value = "ลำดับ";
            sheet.Column(header_column).Width = 10;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "รหัสพนักงาน";
            sheet.Column(header_column).Width = 20;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "ชื่อ-สกุลพนักงาน";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "สถานที่ปฏิบัติงาน";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "จังหวัดที่ปฏิบัติงาน";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "ชื่อผู้เสียชีวิต";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "ความสัมพันธ์";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "วันที่ถึงแก่กรรม";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "รายละเอียดสถานที่";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "ศาสนา";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "ประสงค์ให้ออก PR-HR";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "เสียชีวิตจากการทำงาน";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "จำนวนเงินช่วยเหลือ(บาท)";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "จำนวนเงินชดเชย (บาท)";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "จำนวนเงินรวม(บาท)";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "วันที่ขออนุมัติ";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "วันที่อนุมัติ";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "วันที่ Post ลง SAP";
            sheet.Column(header_column).Width = 25;
            header_column++;


            var rang_header2 = sheet.Range("A2:R2");
            rang_header2.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            rang_header2.Style.Font.FontSize = 11;
            rang_header2.Style.Font.FontName = "Tahoma";
            rang_header2.Style.Fill.BackgroundColor = XLColor.LightBlue;
            rang_header2.Style.Border.SetOutsideBorder(XLBorderStyleValues.Thick);
            rang_header2.Style.Border.SetInsideBorder(XLBorderStyleValues.Thick);

            #endregion

            #region Data Excel

            List<ReportBEBereavementInfoByAdmin> list_Bereavement = ServiceManager.CreateInstance(CompanyCode).ESSData.GetReporttBEBereavementByAdmin(begin_date, end_date);
            if (list_Bereavement.Count > 0)
            {

                int data_row = 3;
                int data_column;
                int order = 0;
                var group_list_Bereavement = list_Bereavement.GroupBy(s => s.RequestNo).ToList();
                foreach (var bereavement in group_list_Bereavement)
                {
                    foreach (var item in bereavement)
                    {
                        data_column = 1;

                        sheet.Row(data_row).Cell(data_column).Value = ++order; // ลำดับ
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.EmployeeID; // รหัสพนักงาน
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.EmployeeName; // ชื่อสกุล - พนักงาน
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.WorkLocationName; // สถานที่ปฎิบัติงาน
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.County; // จังหวัดปฎิบัติงาน
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.FirstName + " " + item.LastName; // ชื่อผู้เสียชีวิต
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = GetCommonText("FAMILYMEMBER", language, item.BereavementType.ToString());// ความสัมพันธ์
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.BereavementDate; // วันที่ถึงแก่กรรม
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.PlaceDetail; // รายละเอียดสถานที่
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.ReligionName; // ศาสนา
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                        data_column++;

                        string txt_pr = null;
                        if (item.IsAllowPR == 1)
                        {
                            txt_pr = GetCommonText("BE_CREMATION", language, "T_WANTTOPR1");
                        }
                        else if (item.IsAllowPR == 2)
                        {
                            txt_pr = GetCommonText("BE_CREMATION", language, "T_WANTTOPR2");
                        }
                        sheet.Row(data_row).Cell(data_column).Value = txt_pr; // ประสงค์ให้ออก PR
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                        data_column++;

                        string txt_DeceasedInDuty = null;
                        if (item.DeceasedInDuty.HasValue)
                        {
                            if (item.DeceasedInDuty.Value == true)
                                txt_DeceasedInDuty = GetCommonText("BE_CREMATION", language, "DeceasedInDuty");
                            else
                                txt_DeceasedInDuty = GetCommonText("BE_CREMATION", language, "NoDeceasedInDuty");
                        }
                        sheet.Row(data_row).Cell(data_column).Value = txt_DeceasedInDuty; // เสียชีวิตจากการทำงาน
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.SubsidyAmount; // จำนวนเงินช่วยเหลือ (บาท)
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                        sheet.Row(data_row).Cell(data_column).Style.NumberFormat.Format = "#,##0.00";
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.PaymentFuneralAmount; // จำนวนเงินช่วยเหลือ (บาท)
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                        sheet.Row(data_row).Cell(data_column).Style.NumberFormat.Format = "#,##0.00";
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.Amount; // จำนวนเงินรวม (บาท)
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                        sheet.Row(data_row).Cell(data_column).Style.NumberFormat.Format = "#,##0.00";
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.SubmitDate.HasValue ? item.SubmitDate.Value.Date.ToString("yyyy/MM/dd") : ""; // วันที่ขออนุมัติ
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.ActionDate.HasValue ? item.ActionDate.Value.ToString("yyyy/MM/dd") : ""; // วันที่อนุมัติ
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.PostingDate.HasValue ? item.PostingDate.Value.ToString("yyyy/MM/dd") : ""; // วันที่ Post SAP
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        data_column++;

                        data_row++;
                    }
                }
            }

            #endregion
        }

        // รายงานการเบิกจ่ายสวัสดิการ
        public void GetSheetExcelDisbursementOfWelfare(XLWorkbook workbook, DateTime begin_date, DateTime end_date, string language)
        {
            var sheet = workbook.Worksheets.Add("การเบิกจ่ายสวัสดิการ");

            #region Header Excel
            int header_row = 1;
            int header_column = 1;

            //sheet.Row(header_row).Cell(header_column).Value = "รายงานการเบิกเงินช่วยเหลืองานฌาปนกิจสงเคราะห์";
            //var range2 = sheet.Range("A1:R1");
            //range2.Style.Font.FontSize = 11;
            //range2.Style.Font.FontName = "Tahoma";
            //var rowInRange = range2.Row(1);
            //rowInRange.Merge();
            //rowInRange.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            //rowInRange.Style.Fill.BackgroundColor = XLColor.LightBlue;

            //header_row++;

            sheet.Row(header_row).Cell(header_column).Value = "ลำดับ";
            sheet.Column(header_column).Width = 10;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "บริษัท";
            sheet.Column(header_column).Width = 15;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "รหัสพนักงาน";
            sheet.Column(header_column).Width = 20;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "ชื่อ-สกุลพนักงาน";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "หน่วยงาน";
            sheet.Column(header_column).Width = 15;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "สังกัด";
            sheet.Column(header_column).Width = 20;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "สถานที่ปฏิบัติงาน";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "จังหวัดที่ปฏิบัติงาน";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "วันที่ส่งเอกสาร";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "วันที่อนุมัติเอกสาร";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "จำนวนเงิน";
            sheet.Column(header_column).Width = 15;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "วันที่ Post ลง SAP";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "ประเภทสวัสดิการ";
            sheet.Column(header_column).Width = 25;
            header_column++;


            var rang_header2 = sheet.Range("A1:M1");
            rang_header2.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            rang_header2.Style.Font.FontSize = 11;
            rang_header2.Style.Font.FontName = "Tahoma";
            rang_header2.Style.Fill.BackgroundColor = XLColor.LightBlue;
            rang_header2.Style.Border.SetOutsideBorder(XLBorderStyleValues.Thick);
            rang_header2.Style.Border.SetInsideBorder(XLBorderStyleValues.Thick);
            #endregion

            #region Data Excel

            List<ReportDisbursementOfWelfareByAdmin> welfare_all = ServiceManager.CreateInstance(CompanyCode).ESSData.getReportDisbursementOfWelfareByAdmin(begin_date, end_date);
            //var list_welfare = welfare_all.OrderBy(s => s.SubmitDate).ToList();
            if (welfare_all.Count > 0)
            {
                int data_row = 2;
                int data_column;
                int order = 0;
                foreach (var item in welfare_all)
                {
                    data_column = 1;

                    sheet.Row(data_row).Cell(data_column).Value = ++order; // ลำดับ
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = CompanyCode; // บริษัท
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.EmployeeId; // รหัสพนักงาน
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.EmployeeName; // ชื่อ-สกุล
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.OrgunitId; // หน่วยงาน (ชื่อย่อ)
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.PositionId; // สังกัด (ระดับฝ่ายขึ้นไป)
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.WorkLocationName; // สถานที่ปฎิบัติงาน
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.County; // จังหวัดที่ปฏิบัติงาน
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    data_column++;

                    // วันที่ส่งเอกสาร
                    sheet.Row(data_row).Cell(data_column).Value = item.SubmitDate.HasValue ? item.SubmitDate.Value.Date.ToString("yyyy/MM/dd") : ""; // วันที่ขออนุมัติ
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    // วันที่อนุมัติเอกสาร
                    sheet.Row(data_row).Cell(data_column).Value = item.ActionDate.HasValue ? item.ActionDate.Value.ToString("yyyy/MM/dd") : ""; // วันที่อนุมัติ
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.TotalAmount; // จำนวนเงินที่ขอเบิก
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                    sheet.Row(data_row).Cell(data_column).Style.NumberFormat.Format = "#,##0.00";
                    data_column++;

                    // วันที่ Post ลง SAP
                    sheet.Row(data_row).Cell(data_column).Value = item.PostingDate.HasValue ? item.PostingDate.Value.ToString("yyyy/MM/dd") : ""; // วันที่ Post SAP
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    // สวัสดิการ
                    sheet.Row(data_row).Cell(data_column).Value = GetCommonText("BENEFITREPORT", language, "CATEGORY" + item.TypeWelfare);// ความสัมพันธ์
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    data_column++;

                    data_row++;

                }
            }
            #endregion
        }

        #endregion

        public string GetCommonText(string Category, string Language, string Code)
        {
            CommonText oCommonText = new CommonText(CompanyCode);
            return oCommonText.LoadText(Category, Language, Code);
        }

        #region สวัสดิการพนักงานและศิทธิผลประโยชน์

        public List<int> LoadYearEmployeeBenifits()
        {
            var list_year = new List<int>();

            int pereviou_year = Convert.ToInt16(PreviouYearEmployeeBenifits);

            int year_now = DateTime.Now.Date.Year;

            list_year.Add(year_now);

            pereviou_year--;

            int temp_year = year_now;
            for (int i = pereviou_year; i > 0; i--)
            {
                temp_year--;
                list_year.Add(temp_year);
            }


            return list_year;
        }

        public List<BenefitsAbsence> getSumQuotaAbsence(EmployeeData oEmp, string year, string employeeId)
        {
            List<BenefitsAbsence> list_absence = new List<BenefitsAbsence>();

            List<INFOTYPE2006> oListINFOTYPE2006 = new List<INFOTYPE2006>();

            INFOTYPE2006 oINFOTYPE2006 = new INFOTYPE2006();
            oINFOTYPE2006.CompanyCode = CompanyCode;
            oListINFOTYPE2006 = oINFOTYPE2006.GetData(oEmp);

            if (oListINFOTYPE2006.Count > 0)
            {
                foreach (var item in oListINFOTYPE2006)
                {
                    BenefitsAbsence absence = new BenefitsAbsence()
                    {
                        TotalAbsence = item.QuotaAmount,
                        TextCodeShort = item.AbsenceType,
                        UseAbsence = item.UsedAmount,
                        TextCode = item.DefaultAbsenceType,
                        ProcessApproval = item.InprocessAmount
                    };

                    if (item.QuotaAmount > 0)
                    {
                        decimal percentage_use = (item.UsedAmount / item.QuotaAmount) * 100;
                        decimal percentage = Math.Round(percentage_use, 2);
                        absence.Percent = percentage;
                    }
                    list_absence.Add(absence);
                }
            }

            //DataSumQuotaAbsence result = ServiceManager.CreateInstance(CompanyCode).ESSData.getSumQuotaAbsence(year, employeeId);
            //if (result.SumQuotaAbsence > 0)
            //{
            //    List<EmployeeAbsenceByType> list_absenceby_type = ServiceManager.CreateInstance(CompanyCode).ESSData.getAbsenceByTypeEmployee(year, employeeId);
            //    if (list_absenceby_type.Count > 0)
            //    {
            //        foreach (var item in list_absenceby_type)
            //        {
            //            BenefitsAbsence data_absence = new BenefitsAbsence()
            //            {
            //                TextCode = item.AbsAttType,
            //                UseAbsence = item.SumLeave,
            //                TotalAbsence = result.SumQuotaAbsence
            //            };

            //            decimal percent = (item.SumLeave / result.SumQuotaAbsence) * 100;
            //            data_absence.Percent = Math.Round(percent, 2);

            //            list_absence.Add(data_absence);
            //        }
            //    }
            //}
            return list_absence;
        }

        public TranasctionUseReimburseByEmployee GetInformationDashboardUseReimburseByEmployee(string year, string empSubGroupId, string employeeId)
        {
            var reimburse = ServiceManager.CreateInstance(CompanyCode).ESSData.getUseReimburseByEmployee(year, empSubGroupId, employeeId);

            // มีวงเงินสำหรับเบิกสวัสดิการทางเลือก
            if (reimburse.IsHaveAmountReimburseByEmployee && reimburse.TotalAmount > 0)
            {
                reimburse.TotalAmountUse = reimburse.ListUseReimburse.Sum(s => s.Amount + s.Vat);

                if (reimburse.TotalAmountUse > reimburse.TotalAmount)
                {
                    // ใช้เงินเกินวงเงินที่บริษัทให้
                    reimburse.PercentageUse = 100;
                    reimburse.PercentageRemain = 0;
                    reimburse.TotalAmountRemain = 0;
                }
                else
                {
                    // ใช้เงินไม่เกินวงเงินที่บริษัทให้
                    decimal percentage = (reimburse.TotalAmountUse / reimburse.TotalAmount) * 100;
                    percentage = Math.Round(percentage, 2);

                    reimburse.PercentageUse = percentage;
                    reimburse.PercentageRemain = 100 - percentage;

                    reimburse.TotalAmountRemain = reimburse.TotalAmount - reimburse.TotalAmountUse;
                }
            }
            return reimburse;
        }

        public DashbaordEducationChildByEmployee GetInformationDashbaordUseEducationChildByEmployee(string year, string employeeId)
        {
            DashbaordEducationChildByEmployee result = new DashbaordEducationChildByEmployee();

            List<EducationChildByEmployee> db_education = ServiceManager.CreateInstance(CompanyCode).ESSData.getUseEducationChildByEmployee(year, employeeId);
            if (db_education.Count > 0)
            {
                var group_name = db_education.OrderBy(s => s.ChildNo).GroupBy(s => s.FirstName).ToList();
                if (group_name.Count > 0)
                {
                    decimal total_amount_quota = 0;
                    decimal total_amount_reimburse = 0;
                    decimal total_amount_remain = 0;
                    foreach (var grop in group_name)
                    {
                        TransactionUseEducationChildByEmployee use_education = new TransactionUseEducationChildByEmployee();

                        string first_name = grop.FirstOrDefault().FirstName;
                        string last_name = grop.FirstOrDefault().LastName;
                        decimal amount_quota = grop.FirstOrDefault().QuotaEducation; // วงเงินที่บริษัทให้เบิก
                        decimal amount_reimburse = grop.Sum(s => s.AmountReimburse); // ยอดเงินเบิก

                        decimal amount_remain = 0;
                        decimal percentage_use = 0;
                        if (amount_reimburse > amount_quota)
                        {
                            percentage_use = 100;
                            amount_reimburse = amount_quota;
                        }
                        else
                        {
                            percentage_use = (amount_reimburse / amount_quota) * 100;
                            amount_remain = amount_quota - amount_reimburse;
                        }

                        total_amount_quota += amount_quota;
                        total_amount_reimburse += amount_reimburse;
                        total_amount_remain += amount_remain;

                        use_education.Fullname = first_name + " " + last_name;
                        use_education.TextLevelTH = grop.FirstOrDefault().TextEducationTH;
                        use_education.TextLevelEN = grop.FirstOrDefault().TextEducationEN;
                        use_education.AmountQuota = amount_quota;
                        use_education.AmountReimburse = amount_reimburse;
                        use_education.AmountRemain = amount_remain;
                        use_education.PercentageUse = percentage_use;

                        result.ListUseEducationChild.Add(use_education);
                    }
                    result.TotalAmountQuota = total_amount_quota;
                    result.TotalAmountReimburse = total_amount_reimburse;
                    result.TotalAmountRemain = total_amount_remain;
                }
                result.IsHaveUseEducationChildEmployee = true;
            }
            return result;
        }

        public DashboardFitnessByEmployee GetInformationDasboardUseFitnessByEmployee(string year, string employeeId)
        {
            DashboardFitnessByEmployee fitness = new DashboardFitnessByEmployee();

            // 1. ดูลทะเบียนสวัสดิการ fitness ก่อน
            var emp_enrollment = ServiceManager.CreateInstance(CompanyCode).ESSData.GetEnrollmentWelfareByEmployee(employeeId, year);

            if (!string.IsNullOrEmpty(emp_enrollment.EmployeeID))
            {
                if (emp_enrollment.isMainBenefit)
                {
                    List<FitnessByEmployee> db_fitness = ServiceManager.CreateInstance(CompanyCode).ESSData.GetUseFitnessByEmployee(year, employeeId);
                    // มีข้อมูลการใช้งานสวัสดิการ
                    if (db_fitness.Count > 0)
                    {
                        var group_fitness = db_fitness.GroupBy(s => s.FitnessConfigID).ToList();

                        decimal total_quota = 0;
                        decimal payment_amount = 0;
                        foreach (var group in group_fitness)
                        {
                            total_quota += group.FirstOrDefault().QuotaFitness;
                            payment_amount += group.Sum(s => s.PaymentAmount);
                        }

                        if (payment_amount > total_quota)
                        {
                            // จำนวนเงินที่จ่ายมากกว่าจำนวนเงิน Quota
                            fitness.TotalAmountQuota = total_quota;
                            fitness.TotalAmountRemain = 0;
                            fitness.PercentageUse = 100;
                            fitness.PercentageRemain = 0;
                        }
                        else
                        {
                            // ใช้เงินไม่เกินวงเงินที่บริษัทให้
                            fitness.TotalAmountQuota = total_quota;
                            fitness.TotalAmountRemain = total_quota - payment_amount;
                            fitness.TotalAmountUse = payment_amount;

                            decimal percentage_use = (payment_amount / total_quota) * 100;
                            percentage_use = Math.Round(percentage_use, 2);

                            fitness.PercentageUse = percentage_use;
                            fitness.PercentageRemain = 100 - percentage_use;
                        }
                        fitness.IsHaveUseFitness = true;
                    }
                    else
                    {
                        fitness.PercentageRemain = 100;
                        fitness.PercentageUse = 0;
                        fitness.TotalAmountQuota = emp_enrollment.Quota;
                        fitness.TotalAmountRemain = emp_enrollment.Quota;
                        fitness.TotalAmountUse = 0;
                    }
                    fitness.IsHaveUseFitness = true;
                }
                else
                {
                    fitness.IsHaveUseFitness = false;
                }
            }
            else
            {
                fitness.IsEnrollment = true;


                List<BEFitnessAllowance> TResult = new List<BEFitnessAllowance>();
                EmployeeData oEmpData = new EmployeeData(employeeId, DateTime.Now) { CompanyCode = CompanyCode };
                TResult = GetBEFitnessAllowanceQuota(employeeId, Convert.ToDateTime(oEmpData.DateSpecific.PassProbation), year);
                var firstElement = TResult.First();
                decimal total_quota = firstElement.Quota_all;
                decimal payment_amount = firstElement.TotalAmount;

                if (total_quota == 0)
                {
                    fitness.PercentageRemain = 100;
                    fitness.PercentageUse = 0;
                    fitness.TotalAmountQuota = emp_enrollment.Quota;
                    fitness.TotalAmountRemain = emp_enrollment.Quota;
                    fitness.TotalAmountUse = 0;
                }
                else
                {
                    if (payment_amount > total_quota)
                    {
                        // จำนวนเงินที่จ่ายมากกว่าจำนวนเงิน Quota
                        fitness.TotalAmountQuota = total_quota;
                        fitness.TotalAmountRemain = 0;
                        fitness.PercentageUse = 100;
                        fitness.PercentageRemain = 0;
                    }
                    else
                    {
                        // ใช้เงินไม่เกินวงเงินที่บริษัทให้
                        fitness.TotalAmountQuota = total_quota;
                        fitness.TotalAmountRemain = total_quota - payment_amount;
                        fitness.TotalAmountUse = payment_amount;

                        decimal percentage_use = (payment_amount / total_quota) * 100;
                        percentage_use = Math.Round(percentage_use, 2);

                        fitness.PercentageUse = percentage_use;
                        fitness.PercentageRemain = 100 - percentage_use;
                    }
                }
                fitness.IsHaveUseFitness = true;

            }

            return fitness;
        }

        public DashboardHealthInsAllowanceParentByEmployee GetInformationDashBoardHealthInsAllowanceParentByEmployee(string year, string employeeId)
        {
            DashboardHealthInsAllowanceParentByEmployee dashboard_health = new DashboardHealthInsAllowanceParentByEmployee();

            List<HealthInsAllowanceParent> list_parent = ServiceManager.CreateInstance(CompanyCode).ESSData
                .GetUseHealthInsAllowanceParent(year, employeeId);

            if (list_parent.Count > 0)
            {
                var group_parent = list_parent.GroupBy(s => s.RelationshipType).ToList();
                if (group_parent.Count > 0)
                {
                    decimal total_amount_use = 0;
                    decimal total_amount_remain = 0;
                    decimal total_amount_quota = 0;
                    foreach (var group in group_parent)
                    {

                        string first_name = group.FirstOrDefault().FirstName;
                        string last_name = group.FirstOrDefault().LastName;
                        decimal amount_quota = group.FirstOrDefault().AmountQuota; // วงเงินที่บริษัทให้เบิก
                        decimal amount_reimburse = group.Sum(s => s.PaymentAmount); // ยอดเงินเบิก

                        decimal amount_remain = 0;
                        decimal percentage_use = 0;
                        decimal percentage_remain = 0;
                        if (amount_reimburse > amount_quota)
                        {
                            percentage_use = 100;
                            amount_reimburse = amount_quota;
                        }
                        else
                        {
                            percentage_use = (amount_reimburse / amount_quota) * 100;
                            percentage_use = Math.Round(percentage_use, 2);
                            percentage_remain = 100 - percentage_use;

                            amount_remain = amount_quota - amount_reimburse;
                        }

                        total_amount_quota += amount_quota;
                        total_amount_use += amount_reimburse;

                        TransactionUseHealthInsAllowanceParent data = new TransactionUseHealthInsAllowanceParent()
                        {
                            AmountQuota = amount_quota,
                            AmountReimburse = amount_reimburse,
                            AmountRemain = amount_remain,
                            Fullname = first_name + " " + last_name + " (" + group.FirstOrDefault().TextRelation + ")",
                            PercentageUse = percentage_use,
                            PercentageRemain = percentage_remain
                        };

                        dashboard_health.ListUseHealthInsAllowanceParent.Add(data);
                    }

                    total_amount_remain = total_amount_quota - total_amount_use;

                    dashboard_health.IsHaveUseHealth = true;
                    dashboard_health.TotalAmountQuota = total_amount_quota;
                    dashboard_health.TotalAmountReimburse = total_amount_use;
                    dashboard_health.TotalAmountRemain = total_amount_quota - total_amount_use;
                }
            }

            return dashboard_health;
        }

        public DashboardProvidentFundByEmployee GetInformationDashbaordProvidentfundByEmployee(string year, string employeeId)
        {
            DashboardProvidentFundByEmployee dashboard_provident = new DashboardProvidentFundByEmployee();

            List<BenefitsProvidentFundByEmployee> list_provident = ServiceManager.CreateInstance(CompanyCode).ESSData
                .GetBenefitsProvidentFundByEmployee(year, employeeId);

            if (list_provident.Count > 0)
            {
                var group_fundid_all = list_provident.GroupBy(s => s.FundID).ToList();

                if (group_fundid_all.Count > 0)
                {
                    bool is_assign = false;
                    foreach (var item in group_fundid_all)
                    {
                        // กองทุนเดือนล่าสุด
                        var data = item.OrderByDescending(s => s.PF_Month).FirstOrDefault();

                        dashboard_provident.TotalContributionCompany += (data.Total_company_amount + data.Total_company_benefit_amount);
                        dashboard_provident.TotalContributionEmployee += (data.Total_employee_amount + data.Total_employee_benefit_amount);

                        if (!is_assign)
                        {
                            dashboard_provident.EmployeeId = data.EmployeeID;
                            dashboard_provident.FullName = data.FirstName + " " + data.LastName;

                            // 1. หาอายุงาน
                            // 2. หาเรทสมทบกองทุยของบริษัท ตามอายุงาน
                            // 3. Payroll Result
                            // 4. หาเรทที่พนักงานเอาเงินสมทบเอง

                            dashboard_provident.AmountEmployee1 = 0;
                            dashboard_provident.TextContributionCompany1 = "";
                            dashboard_provident.TextWorkingAge1 = "";

                            dashboard_provident.AmountEmployee2 = 0;
                            dashboard_provident.TextContributionCompany2 = "";
                            dashboard_provident.TextWorkingAge2 = "";
                            is_assign = true;
                        }
                    }
                }
                dashboard_provident.GrandTotal = dashboard_provident.TotalContributionCompany + dashboard_provident.TotalContributionEmployee;
                dashboard_provident.IsHaveProvidentFund = true;
            }

            return dashboard_provident;
        }

        public List<DashbaordOtherWelfare> GetInformationDashboardAnnouncementText()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetAnnouncementTextAllEmployee();
        }

        public DashboardIncomeYearByEmployee GetInformationDashboardIncomEmployee(int year, string employeeId)
        {
            DashboardIncomeYearByEmployee income_employee = new DashboardIncomeYearByEmployee();
            income_employee.Employee = employeeId;
            income_employee.Year = year;
            // 1.หารายได้พนักงานทั้งปี
            List<EmployeeIncomeForYear> list_income_employee = ServiceManager.CreateInstance(CompanyCode).ESSData.GetIncomdeForYearByEmployee(employeeId, year);

            decimal sum_total_income = 0;
            if (list_income_employee.Count > 0)
            {
                // 2. group ราย WageType ซ้ำกัน
                var group_wage_type = list_income_employee.GroupBy(s => s.WageTypeCode).ToList();
                if (group_wage_type.Count > 0)
                {
                    foreach (var wage_type in group_wage_type)
                    {
                        decimal total_amount = wage_type.Sum(s => s.Amount);
                        DashboardIncomeYearByEmployeeDetail income = new DashboardIncomeYearByEmployeeDetail()
                        {
                            TotalAmount = total_amount,
                            WageTypeCode = wage_type.FirstOrDefault().WageTypeCode,
                            WageTypeName = wage_type.FirstOrDefault().WageTypeName
                        };
                        income_employee.DetailIncome.Add(income);

                        sum_total_income += total_amount;
                    }
                }
            }

            income_employee.TotalIncome = sum_total_income;

            return income_employee;
        }

        public decimal GetPercentageWorkingAge(decimal ageWorking)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetPercentageWorkingAge(ageWorking);
        }

        public decimal GetAmountSalaryByEmployee(string employeeId, string year)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetSalaryEmployee(employeeId, year);
        }

        public decimal GetRateProvidentfundEmployeeSaving(string employeeId, string year)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetRateProvidentEmployeeSaving(employeeId, year);
        }
        #endregion

        // คำนวณอายุ
        public AgeFamily CalculateAge(DateTime Bday, DateTime Cday)
        {
            AgeFamily age_family = new AgeFamily();

            if ((Cday.Year - Bday.Year) > 0 
                || (((Cday.Year - Bday.Year) == 0) && ((Bday.Month < Cday.Month) 
                || ((Bday.Month == Cday.Month) && (Bday.Day <= Cday.Day)))))
            {
                int DaysInBdayMonth = DateTime.DaysInMonth(Bday.Year, Bday.Month);
                int DaysRemain = Cday.Day + (DaysInBdayMonth - Bday.Day);

                if (Cday.Month > Bday.Month)
                {
                    age_family.Year = Cday.Year - Bday.Year;
                    age_family.Month = Cday.Month - (Bday.Month + 1) + Math.Abs(DaysRemain / DaysInBdayMonth);
                    age_family.Day = (DaysRemain % DaysInBdayMonth + DaysInBdayMonth) % DaysInBdayMonth;
                }
                else if (Cday.Month == Bday.Month)
                {
                    if (Cday.Day >= Bday.Day)
                    {
                        age_family.Year = Cday.Year - Bday.Year;
                        age_family.Month = 0;
                        age_family.Day = Cday.Day - Bday.Day;
                    }
                    else
                    {
                        age_family.Year = (Cday.Year - 1) - Bday.Year;
                        age_family.Month = 11;
                        age_family.Day = DateTime.DaysInMonth(Bday.Year, Bday.Month) - (Bday.Day - Cday.Day);
                    }
                }
                else
                {
                    age_family.Year = (Cday.Year - 1) - Bday.Year;
                    age_family.Month = Cday.Month + (11 - Bday.Month) + Math.Abs(DaysRemain / DaysInBdayMonth);
                    age_family.Day = (DaysRemain % DaysInBdayMonth + DaysInBdayMonth) % DaysInBdayMonth;
                }
            }
            else
            {
                throw new ArgumentException("Birthday date must be earlier than current date");
            }
            return age_family;
        }

        public Boolean GetIsActiveFlexibleBenefitDashboard()
        {
            return IsActiveFlexibleBenefitDashboard;
        }

        public Boolean GetIsActiveHealthInsuranceDashboard()
        {
            return IsActiveHealthInsuranceDashboard;
        }

        #region HealthInsuranceConfig
        public List<BEHealthInsConfig> GetHealthInsuranceConfig()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetHealthInsuranceConfig();
        }


        public void SaveHealthInsuranceConfig(BEHealthInsConfig oHealthConfig)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveHealthInsuranceConfig(oHealthConfig);
        }

        #endregion






















        // รายงานการลงทะเบียนประกันสุขภาพ
        public void GetSheetExcelHealthInsAllowanceByAdmin(XLWorkbook workbook, DateTime begin_date, DateTime end_date, string language)
        {
            var sheet = workbook.Worksheets.Add("ลงทะเบียนประกันสุขภาพ");

            #region Header Excel

            int header_row = 1;
            int header_column = 1;

            sheet.Row(header_row).Cell(header_column).Value = "รายงานการลงทะเบียนประกันสุขภาพ";
            var range2 = sheet.Range("A1:P1");
            range2.Style.Font.FontSize = 11;
            range2.Style.Font.FontName = "Tahoma";
            var rowInRange = range2.Row(1);
            rowInRange.Merge();
            rowInRange.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            rowInRange.Style.Fill.BackgroundColor = XLColor.LightBlue;

            header_row++;

            sheet.Row(header_row).Cell(header_column).Value = "ลำดับที่";
            sheet.Column(header_column).Width = 10;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "หน่วยงาน";
            sheet.Column(header_column).Width = 20;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "รหัสพนักงาน";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "ชื่อ-นามสกุล";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "เพศ";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "เลขบัตรประจำตัวประชาชน";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "ว/ด/ป เกิด";
            sheet.Column(header_column).Width = 15;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "เข้า-ออก-เปลี่ยนแผน";
            sheet.Column(header_column).Width = 10;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "สถานภาพ/Status";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "วันที่มีผลบังคับ";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "แผนประกัน";
            sheet.Column(header_column).Width = 20;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "กรณีโอนเครมเข้าชื่อธนาคาร";
            sheet.Column(header_column).Width = 20;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "เลขที่ธนาคาร";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "อีเมล์";
            sheet.Column(header_column).Width = 15;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "เบอร์โทรศัพท์";
            sheet.Column(header_column).Width = 15;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "หมายเหตุ";
            sheet.Column(header_column).Width = 20;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "สถานะเอกสาร";
            sheet.Column(header_column).Width = 20;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "ความสัมพันธ์";
            sheet.Column(header_column).Width = 20;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "ประเภทสิทธิ์";
            sheet.Column(header_column).Width = 20;
            header_column++;

            var rang_header2 = sheet.Range("A2:S2");
            rang_header2.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            rang_header2.Style.Font.FontSize = 11;
            rang_header2.Style.Font.FontName = "Tahoma";
            rang_header2.Style.Fill.BackgroundColor = XLColor.LightBlue;
            rang_header2.Style.Border.SetOutsideBorder(XLBorderStyleValues.Thick);
            rang_header2.Style.Border.SetInsideBorder(XLBorderStyleValues.Thick);

            #endregion

            #region Data Excel

            List<ReportHealthInsAllowanceByAdmin> list_HealthInsAllowance = ServiceManager.CreateInstance(CompanyCode).ESSData.GetReportHealthInsAllowanceByAdmin(begin_date, end_date);
            if (list_HealthInsAllowance.Count > 0)
            {
                int data_row = 3;
                int data_column;
                int order = 0;
                var group_list_HealthInsAllowance = list_HealthInsAllowance.GroupBy(s => s.RequestNo).ToList();
                foreach (var healthAllwance in group_list_HealthInsAllowance)
                {
                    int count_dup_transaction = healthAllwance.Count();
                    decimal net_paid = 0;
                    foreach (var item in healthAllwance)
                    {
                        data_column = 1;

                        sheet.Row(data_row).Cell(data_column).Value = ++order; // ลำดับ
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.EmployeeID; // รหัสพนักงาน
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.EmployeeName; // ชื่อสกุล - พนักงาน
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.WorkLocationName; // สถานที่ปฎิบัติงาน
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.County; // จังหวัดปฎิบัติงาน
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.FirstName + " " + item.LastName; // ชื่อบุพการี
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.Relationship; // ความสัมพันธ์
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        data_column++;

                        // คำนวนอายุบุพการี
                        string age_parent = "";
                        if (item.BirthDate != null && item.RecieptDate != null)
                        {
                            AgeFamily age = CalculateAge(item.BirthDate.Value.Date, item.RecieptDate.Value.Date);

                            if (language.ToLower() == "th")
                                age_parent = string.Format("{0} ปี {1} เดือน", age.Year, age.Month);

                            if (language.ToLower() == "en")
                                age_parent = string.Format("{0} Year {1} Month", age.Year, age.Month);
                        }

                        sheet.Row(data_row).Cell(data_column).Value = age_parent; // อายุบุพการี
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.VendorName; // ชื่อสถานประกอบการ
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.AmountExcludeVAT; // จำนวนเงินก่อน VAT(บาท)
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                        sheet.Row(data_row).Cell(data_column).Style.NumberFormat.Format = "#,##0.00";
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.Vat; // VAT(ถ้ามี)(บาท)
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                        sheet.Row(data_row).Cell(data_column).Style.NumberFormat.Format = "#,##0.00";
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.TotalAmount; // จำนวนเงินรวม(บาท) 
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                        sheet.Row(data_row).Cell(data_column).Style.NumberFormat.Format = "#,##0.00";
                        data_column++;

                        decimal net_payment = 0;
                        if (item.TotalAmount <= item.PaymentAmount)
                        {
                            net_payment = item.TotalAmount;
                            net_paid = net_paid + (item.PaymentAmount - item.TotalAmount);
                        }
                        else
                        {
                            net_payment = item.TotalAmount - net_paid;
                        }

                        sheet.Row(data_row).Cell(data_column).Value = net_payment; // ยอดเงินที่เบิกได้จริง(บาท)
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                        sheet.Row(data_row).Cell(data_column).Style.NumberFormat.Format = "#,##0.00";
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.SubmitDate.HasValue ? item.SubmitDate.Value.Date.ToString("yyyy/MM/dd") : ""; // วันที่ขออนุมัติ
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.ActionDate.HasValue ? item.ActionDate.Value.ToString("yyyy/MM/dd") : ""; // วันที่อนุมัติ
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        data_column++;

                        sheet.Row(data_row).Cell(data_column).Value = item.PostingDate.HasValue ? item.PostingDate.Value.ToString("yyyy/MM/dd") : ""; // วันที่ Post SAP
                        sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        data_column++;

                        data_row++;
                    }
                }
            }
            #endregion
        }




        public List<HealthInsAllowanceReport> GetHealthInsAllowanceReport(DateTime UpdateDate_From, DateTime UpdateDate_To)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetHealthInsAllowanceReport(UpdateDate_From, UpdateDate_To);
        }



        public int YEAR_OTLOG_HOUR
        {
            get
            {
                return Convert.ToInt16(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "YEAR_OTLOG_HOUR"));
            }
        }

        public List<string> GetListYearConfigHealthInsAllowance()
        {
            DateTime date_now = DateTime.Now;
            List<string> list_year = new List<string>();
            for (int i = 0; i < YEAR_OTLOG_HOUR; i++)
            {
                if (i == 0)
                {
                    string year = date_now.Date.Year.ToString();
                    list_year.Add(year);
                }
                else
                {
                    string year = (date_now.Date.Year - i).ToString();
                    list_year.Add(year);
                }

            }
            return list_year;
        }






    }
}
