using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using ESS.HR.BE.INTERFACE;
using ESS.SHAREDATASERVICE;

namespace ESS.HR.BE
{
    public class ServiceManager
    {
        #region Constructor
        private ServiceManager()
        {

        }
        #endregion 

	    #region MultiCompany  Framework
        private static Dictionary<string, ServiceManager> Cache = new Dictionary<string, ServiceManager>();

        private static string ModuleID = "ESS.HR.BE";

        public string CompanyCode { get; set; }

        public static ServiceManager CreateInstance(string oCompanyCode)
        {
            ServiceManager oServiceManager = new ServiceManager()
            {
                CompanyCode = oCompanyCode
            };
            return oServiceManager;
        }
        #endregion MultiCompany  Framework

        #region " privatedata "        
        private Type GetService(string Mode, string ClassName)
        {
            Type oReturn = null;
            Assembly oAssembly;
            string assemblyName = string.Format("{0}.{1}",ModuleID, Mode.ToUpper());
            string typeName = string.Format("{0}.{1}.{2}",ModuleID, Mode.ToUpper(), ClassName);// CultureInfo.CurrentCulture.TextInfo.ToTitleCase(ClassName.ToLower()));
            oAssembly = Assembly.Load(assemblyName);    // Load assembly (dll)
            oReturn = oAssembly.GetType(typeName);      // Load class
            return oReturn;
        }

        #endregion " privatedata "


        internal IHRBEDataService GetMirrorService(string Mode)
        {
            Type oType = GetService(Mode, "HRBEDataServiceImpl");
            if (oType == null)
            {
                return null;
            }
            else
            {
                return (IHRBEDataService)Activator.CreateInstance(oType, CompanyCode);
            }
        }

        private string ESSCONNECTOR
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ESSCONNECTOR");
            }
        }

        private string ERPCONNECTOR
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ERPCONNECTOR");
            }
        }

        public IHRBEDataService ESSData
        {
            get
            {
                Type oType = GetService(ESSCONNECTOR, "HRBEDataServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IHRBEDataService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }

        public IHRBEDataService ERPData
        {
            get
            {
                Type oType = GetService(ERPCONNECTOR, "HRBEDataServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IHRBEDataService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }

        public string ProvidentFundHeir
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ProvidentFundHeir");
            }
        }
        public string LifeInsuranceHeir
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "LifeInsuranceHeir");
            }
        }
        public string AccidentInsuranceHeir
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "AccidentInsuranceHeir");
            }
        }
        public string Surety
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "Surety");
            }
        }


    }
}