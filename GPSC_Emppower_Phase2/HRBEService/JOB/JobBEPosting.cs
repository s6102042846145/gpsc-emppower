﻿using System;
using System.Collections.Generic;
using ESS.JOB.ABSTRACT;
using ESS.JOB.INTERFACE;
using System.Globalization;
using ESS.WORKFLOW;
using ESS.HR.BE.DATACLASS;

namespace ESS.HR.BE.JOB
{
    public class JobBEPosting : AbstractJobWorker
    {
        CultureInfo oCL = new CultureInfo("en-US");
        public JobBEPosting()
        {
        }

        public override List<ITaskWorker> LoadTasks()
        {
            List<ITaskWorker> returnValue = new List<ITaskWorker>();

           
            TaskBEPosting task;
            task = new TaskBEPosting();
            task.InfoType = "INFOTYPE0015";
            task.SourceMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEMODE;
            task.SourceProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEPROFILE;
            task.TargetMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETMODE;
            task.TargetProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETPROFILE;
            task.JobTypeID = 30;
            task.TaskID = 30;
            returnValue.Add(task);
            return returnValue;
        }
    }
}
