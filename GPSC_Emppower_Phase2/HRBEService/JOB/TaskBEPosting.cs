﻿using System;
using System.Data;
using System.Collections.Generic;
using ESS.HR.BE.INTERFACE;
using ESS.JOB.ABSTRACT;
using ESS.HR.BE.DATACLASS;


namespace ESS.HR.BE.JOB
{
    public class TaskBEPosting : AbstractTaskWorker
    {
        private string __infoType = "";
        private string __sourceMode = "";
        private string __sourceProfile = "";
        private string __targetMode = "";
        private string __targetProfile = "";
        private int __jobTypeID;

        public TaskBEPosting()
        {
        }

        public string InfoType { get; set; }
        public string SourceMode { get; set; }
        public string SourceProfile { get; set; }
        public string TargetMode { get; set; }
        public string TargetProfile { get; set; }
        public int JobTypeID { get; set; }

        public override void Run()
        {
            string oWorkAction = string.Format("Try to copy data {0} from {1} to {3}", InfoType, SourceMode, SourceProfile, TargetMode, TargetProfile);
            Console.WriteLine(oWorkAction);

            if (InfoType == "INFOTYPE0015")
            {
                if (JobTypeID == 30)
                {
                    HRBEManagement oHRBEManagement = HRBEManagement.CreateInstance(CompanyCode);
                    List<BEPostingData> lstBEEducation = oHRBEManagement.GetBEEducationAllowanceGetToPosting(1);
                    Console.WriteLine("Load {0} record(s) completed", lstBEEducation.Count);
                    DataTable oResultBEEducation = oHRBEManagement.SaveWageAdditionalToSAP(lstBEEducation);
                    HRBEManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format("Copy {0} records", lstBEEducation.Count), oWorkAction, true);
                    Console.WriteLine("Save {0} record(s) completed.", lstBEEducation.Count);

                    List<BEPostingData> lstBEFitness = oHRBEManagement.GetBEFitnessAllowanceGetToPosting(2);
                    Console.WriteLine("Load {0} record(s) completed", lstBEFitness.Count);
                    DataTable oResultBEFitness = oHRBEManagement.SaveWageAdditionalToSAP(lstBEFitness);
                    HRBEManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format("Copy {0} records", lstBEFitness.Count), oWorkAction, true);
                    Console.WriteLine("Save {0} record(s) completed.", lstBEFitness.Count);

                    List<BEPostingData> lstBEHealthIns = oHRBEManagement.GetBEHealthInsAllowanceGetToPosting(3);
                    Console.WriteLine("Load {0} record(s) completed", lstBEHealthIns.Count);
                    DataTable oResultBEHealthIns = oHRBEManagement.SaveWageAdditionalToSAP(lstBEHealthIns);
                    HRBEManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format("Copy {0} records", lstBEHealthIns.Count), oWorkAction, true);
                    Console.WriteLine("Save {0} record(s) completed.", lstBEHealthIns.Count);

                    List<BEPostingData> lstBEBereavementInfo = oHRBEManagement.GetBEBereavementInfoGetToPosting(4);
                    Console.WriteLine("Load {0} record(s) completed", lstBEBereavementInfo.Count);
                    DataTable oResultBEBereavementInfo = oHRBEManagement.SaveWageAdditionalToSAP(lstBEBereavementInfo);
                    HRBEManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format("Copy {0} records", lstBEBereavementInfo.Count), oWorkAction, true);
                    Console.WriteLine("Save {0} record(s) completed.", lstBEBereavementInfo.Count);

                    List<BEPostingData> lstFBReimburse = oHRBEManagement.GetFBReimburseGetToPosting(5);
                    Console.WriteLine("Load {0} record(s) completed", lstFBReimburse.Count);
                    DataTable oResultFBReimburse = oHRBEManagement.SaveWageAdditionalToSAP(lstFBReimburse);
                    HRBEManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format("Copy {0} records", lstFBReimburse.Count), oWorkAction, true);
                    Console.WriteLine("Save {0} record(s) completed.", lstFBReimburse.Count);
                }
            }
        }
    }
}
