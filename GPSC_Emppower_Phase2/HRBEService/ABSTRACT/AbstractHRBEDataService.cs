﻿using ESS.HR.BE.DATACLASS;
using ESS.HR.BE.INTERFACE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.HR.BE.INFOTYPE;
using ESS.UTILITY.DATACLASS;

namespace ESS.HR.BE.ABSTRACT
{
    public abstract class AbstractHRBEDataService : IHRBEDataService
    {
        public virtual void SaveBETraining(TrainingForDev oTrainingForDev)
        {
            throw new NotImplementedException();
        }

        public virtual List<TrainingForDev> GetBETraining(String EmployeeID)
        {
            throw new NotImplementedException();
        }

        public virtual List<BERegistorHealthInsuranceConfig> GetRegistorHealthInsuranceConfig(String EmployeeID)
        {
            throw new NotImplementedException();
        }

        public virtual List<BERegistorHealthInsuranceStatusConfigList> GetRegistorHealthInsuranceStatusConfig(String EmployeeID, int year)
        {
            throw new NotImplementedException();
        }


        public abstract INFOTYPE0002 GetPersonalData(string EmployeeID, DateTime CheckDate);




        public virtual void SaveBEEducationAllowance(BEEducationAllowance oBEEducationAllowance)
        {
            throw new NotImplementedException();
        }


        public virtual List<BEEducationAllowance> GetEducationAllowance(String EmployeeID, String RequestNo, String KeyYear)
        {
            throw new NotImplementedException();
        }


        public virtual List<BEEducationAllowance> GetEducationAllowanceQuotaList(String EmployeeID,String KeyYear)
        {
            throw new NotImplementedException();
        }

        public virtual List<BEEducationAllowance> GetQuotaList(String EmployeeID)
        {
            throw new NotImplementedException();
        }

        public virtual List<BEConfig> GetBEConfig(String BE_Type)
        {
            throw new NotImplementedException();
        }

        public virtual List<INFOTYPE0021> GetFamilyDataB(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0021> GetChildsData(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveInfotype0002(string EmployeeID1, string EmployeeID2, List<INFOTYPE0002> List, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SavePersonalData(INFOTYPE0002 data)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<INFOTYPE0002> GetInfotype0002List(string EmployeeID1, string EmployeeID2)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual decimal CheckQuota(string EmployeeID, string EducationYear, string ChildNo, int EducationLevel, string RequestNo)
        {
            throw new NotImplementedException();
        }

        public virtual List<BEEducationAllowance> UpdateDiscountStatus(String EmployeeID, String EducationLevel, String check)
        {
            throw new NotImplementedException();
        }

        public virtual List<BEEducationAllowance> CheckRequestNo(String RequestNo)
        {
            throw new NotImplementedException();
        }

        public virtual bool CheckDupplicate(string EmployeeID, string ReceiptNumber, DateTime PayDate, string RequestNo)
        {
            throw new NotImplementedException();
        }

        public virtual List<INFOTYPE0021> GetFamilyData(string EmployeeID, string SubType)
        {
            throw new NotImplementedException();
        }

        public virtual List<BEHealthInsAllowanceConfig> GetHealthInsAllowanceConfig(string Year)
        {
            throw new NotImplementedException();
        }

        public virtual void SaveBEHealthInsAllowance(BEHealthInsAllowance oBEHealthInsAllowance)
        {
            throw new NotImplementedException();
        }



        public virtual void SaveBEHealthInsAllowanceItem(BEHealthInsAllowanceItem oBEHealthInsAllowanceItem)
        {
            throw new NotImplementedException();
        }


        public virtual void SaveRegisterBEHealthInsurance(BERegisterHealthInsurance oBEHealthIns)
        {
            throw new NotImplementedException();
        }



        public virtual void SaveRegisterBEHealthInsuranceItem(BERegisterHealthInsuranceItem oBEHealthInsItem)
        {
            throw new NotImplementedException();
        }


        
        


        public virtual List<BEHealthInsAllowance> GetHealthInsAllowance(String EmployeeID, String RequestNo, String KeyYear)
        {
            throw new NotImplementedException();
        }

        public virtual List<BEHealthInsAllowanceItem> GetHealthInsAllowanceItem(String EmployeeID, String RequestNo)
        {
            throw new NotImplementedException();
        }

        public virtual List<BEHealthInsAllowance> GetHealthInsAllowanceQuota(String EmployeeID, String Year, String FamilyData)
        {
            throw new NotImplementedException();
        }

        public virtual List<YearsConfig> GetYears()
        {
            throw new NotImplementedException();
        }

        public virtual void DeleteBEHealthInsAllowanceItem(string RequestNo)
        {
            throw new NotImplementedException();
        }

        public virtual void DeleteBERegisterHealthInsuranceItem(string RequestNo)
        {
            throw new NotImplementedException();
        }



        

        public virtual decimal BEHealthInsAllowanceCheckQuota(string EmployeeID, string Year, int RelationshipType, string RequestNo)
        {
            throw new NotImplementedException();
        }

        public virtual List<BEFitnessAllowance> GetBEFitnessAllowance(String EmployeeID, String RequestNo, String KeyYear)
        {
            throw new NotImplementedException();
        }

        public virtual List<BEFitnessAllowanceItem> GetBEFitnessAllowanceItem(String EmployeeID, String RequestNo)
        {
            throw new NotImplementedException();
        }

        public virtual List<BEFitnessAllowance> GetBEFitnessAllowanceQuota(String EmployeeID, DateTime PassProbation, String Year)
        {
            throw new NotImplementedException();
        }

        public virtual decimal BEFitnessAllowanceCheckQuota(string EmployeeID, string Year, string RequestNo)
        {
            throw new NotImplementedException();
        }

        public virtual void SaveBEFitnessAllowance(BEFitnessAllowance oBEFitnessAllowance)
        {
            throw new NotImplementedException();
        }

        public virtual void SaveBEFitnessAllowanceItem(BEFitnessAllowanceItem oBEFitnessAllowanceItem)
        {
            throw new NotImplementedException();
        }

        public virtual void DeleteBEFitnessAllowanceItem(string RequestNo)
        {
            throw new NotImplementedException();
        }

        public virtual List<BEFitnessAllowanceConfig> GetBEFitnessAllowanceConfig(string EmployeeID, DateTime PassProbation, string Year)
        {
            throw new NotImplementedException();
        }

        public virtual bool HealthInsAllowanceCheckDupplicate(string EmployeeID, string ReceiptNumber, DateTime RecieptDate, string RequestNo)
        {
            throw new NotImplementedException();
        }

        public virtual bool FitnessAllowanceCheckDupplicate(string EmployeeID, string ReceiptNumber, DateTime RecieptDate, string RequestNo)
        {
            throw new NotImplementedException();
        }

        public virtual void UpdateBEEducationAllowancePosting(bool StatusPost, string RequestNo, string PostingMessage, DateTime PostingDate)
        {
            throw new NotImplementedException();
        }

        public virtual void UpdateBEFitnessAllowancePosting(bool StatusPost, string RequestNo, string PostingMessage, DateTime PostingDate)
        {
            throw new NotImplementedException();
        }

        public virtual void UpdateBEHealthInsAllowancePosting(bool StatusPost, string RequestNo, string PostingMessage, DateTime PostingDate)
        {
            throw new NotImplementedException();
        }

        public virtual void UpdateBEBereavementInfoPosting(bool StatusPost, string RequestNo, string PostingMessage, DateTime PostingDate)
        {
            throw new NotImplementedException();
        }

        public virtual void UpdateFBReimbursePosting(bool StatusPost, string RequestNo, string PostingMessage, DateTime PostingDate)
        {
            throw new NotImplementedException();
        }

        public virtual void SaveWageAdditionalData(BEPostingData oBEEducationAllowance)
        {
            throw new NotImplementedException();
        }

        public virtual List<BEPostingData> GetBEEducationAllowanceGetToPosting(int AllowanceWagetypeID)
        {
            throw new NotImplementedException();
        }

        public virtual List<BEPostingData> GetBEFitnessAllowanceGetToPosting(int AllowanceWagetypeID)
        {
            throw new NotImplementedException();
        }

        public virtual List<BEPostingData> GetBEHealthInsAllowanceGetToPosting(int AllowanceWagetypeID)
        {
            throw new NotImplementedException();
        }

        public virtual List<BEPostingData> GetBEBereavementInfoGetToPosting(int AllowanceWagetypeID)
        {
            throw new NotImplementedException();
        }

        public virtual List<BEPostingData> GetFBReimburseGetToPosting(int AllowanceWagetypeID)
        {
            throw new NotImplementedException();
        }

        public virtual bool InsertActionLog(ActionLog oActionLog)
        {
            throw new NotImplementedException();
        }

        public virtual int BEFitnessAllowanceCheckEnrollment(string EmployeeID, string Year)
        {
            throw new NotImplementedException();
        }
         #region Bereavement
		 
        public virtual List<BEBereavementInfo> GetBereavementInfoHistory(String EmployeeID)
        {
            throw new NotImplementedException();
        }

        public virtual List<BEBereavementInfo> GetBereavementItem(String EmployeeID, String RequestNo, String KeyType)
        {
            throw new NotImplementedException();
        }

        public virtual List<BEBereavementConfig> GetBereavementConfig()
        {
            throw new NotImplementedException();
        }

        public virtual void SaveBEBereavementInfo(BEBereavementInfo oBereavementInfo)
        {
            throw new NotImplementedException();
        }

        public virtual void SaveBEBereavementItem(BEBereavementItem oBereavementItem)
        {
            throw new NotImplementedException();
        }

        public virtual void DeleteBEBereavementItem(string RequestNo)
        {
            throw new NotImplementedException();
        }

        public virtual void SaveFamilyData(List<INFOTYPE0021> data)
        {
            throw new NotImplementedException();
        }

        public virtual bool BEBereavementCheckDupplicate(string EmployeeID, int BereavementType, string RequestNo)
        {
            throw new NotImplementedException();
        }

        public virtual DataSet GetReportData(DateTime UpdateDate_From, DateTime UpdateDate_To, string Types, string Status, string LanguageCode)
        {
            throw new NotImplementedException();
        }

        #endregion Bereavement


        public virtual List<BenefitReport> GetBenefitReport(DateTime UpdateDate_From, DateTime UpdateDate_To, string Benefit_Type, string Benefit_Status, string language)
        {
            throw new NotImplementedException();
        }
        

        public virtual bool EducationAllowanceCheckLevel(string EmployeeID, string RequestNo, string ChildNo, string Year, int EducationLevel)
        {
            throw new NotImplementedException();
        }

        public virtual bool EmployeeTrainee(string EmpGroup, DateTime PassProbation)
        {
            throw new NotImplementedException();
        }

        public virtual List<DataReligion> GetReligion()
        {
            throw new NotImplementedException();
        }

        public virtual List<ReportBEEducationAllowanceByAdmin> GetReportEducationAllowanceByAdmin(DateTime begin_date, DateTime end_date)
        {
            throw new NotImplementedException();
        }

        public virtual List<ReportHealthInsAllowanceByAdmin> GetReportHealthInsAllowanceByAdmin(DateTime begin_date, DateTime end_date)
        {
            throw new NotImplementedException();
        }

        public virtual List<ReportFitnessAllowanceByAdmin> GetReportFitnessAllowanceByAdmin(DateTime begin_date, DateTime end_date)
        {
            throw new NotImplementedException();
        }

        public virtual List<ReportBEBereavementInfoByAdmin> GetReporttBEBereavementByAdmin(DateTime begin_date, DateTime end_date)
        {
            throw new NotImplementedException();
        }

        public virtual List<ReportDisbursementOfWelfareByAdmin> getReportDisbursementOfWelfareByAdmin(DateTime begin_date, DateTime end_date)
        {
            throw new NotImplementedException();
        }

        #region สวัสดิการและสิทธิผลประโยชน์พนักงาน

        public virtual DataSumQuotaAbsence getSumQuotaAbsence(string year, string employeeId)
        {
            throw new NotImplementedException();
        }

        public virtual List<EmployeeAbsenceByType> getAbsenceByTypeEmployee(string year, string employeeId)
        {
            throw new NotImplementedException();
        }

        public virtual TranasctionUseReimburseByEmployee getUseReimburseByEmployee(string year, string empSubGroupId,string employeeId)
        {
            throw new NotImplementedException();
        }

        public virtual List<EducationChildByEmployee> getUseEducationChildByEmployee(string year ,string employeeId)
        {
            throw new NotImplementedException();
        }

        public virtual List<FitnessByEmployee> GetUseFitnessByEmployee(string year, string employeeId)
        {
            throw new NotImplementedException();
        }

        public virtual List<HealthInsAllowanceParent> GetUseHealthInsAllowanceParent(string year, string employeeId)
        {
            throw new NotImplementedException();
        }

        public virtual List<BenefitsProvidentFundByEmployee> GetBenefitsProvidentFundByEmployee(string year,string employeeId)
        {
            throw new NotImplementedException();
        }

        public virtual List<DashbaordOtherWelfare> GetAnnouncementTextAllEmployee()
        {
            throw new NotImplementedException();
        }

        public virtual List<EmployeeIncomeForYear> GetIncomdeForYearByEmployee(string employeeId,int year)
        {
            throw new NotImplementedException();
        }

        public virtual void GetBenifitsRegisterEnrollment(string year, string employeeId)
        {
            throw new NotImplementedException();
        }

        public virtual decimal GetPercentageWorkingAge(decimal ageWorking)
        {
            throw new NotImplementedException();
        }

        public virtual decimal GetSalaryEmployee(string employeeId, string year)
        {
            throw new NotImplementedException();
        }

        public virtual decimal GetRateProvidentEmployeeSaving(string employeeId, string year)
        {
            throw new NotImplementedException();
        }

        public virtual EnrollmentWelfareByEmployee GetEnrollmentWelfareByEmployee(string employeeId,string year)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region HealthInsuranceConfig

        public virtual List<BEHealthInsConfig> GetHealthInsuranceConfig()
        {
            throw new NotImplementedException();
        }


        public virtual void SaveHealthInsuranceConfig(BEHealthInsConfig oHealthConfig)
        {
            throw new NotImplementedException();
        }
        #endregion



        public virtual List<HealthInsAllowanceReport> GetHealthInsAllowanceReport(DateTime UpdateDate_From, DateTime UpdateDate_To)
        {
            throw new NotImplementedException();
        }

    }
}
