﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.BE.DATACLASS
{

    public class DataSumQuotaAbsence : AbstractObject
    {
        public string EmployeeID { get; set; }
        public string Year { get; set; }
        public decimal SumQuotaAbsence { get; set; }
    }

    public class EmployeeAbsenceByType : AbstractObject
    {
        public string EmployeeID { get; set; }
        public string Year { get; set; }
        public string AbsAttType { get; set; }
        public decimal SumLeave { get; set; }
    }

    public class ResponeVerifyPincode
    {
        public ResponeVerifyPincode()
        {
            ListYear = new List<int>();
        }
        public bool IsVerifyPincode { get; set; }
        public List<int> ListYear { get; set; }
    }

    public class BenefitsAbsence
    {
        public string TextCode { get; set; }
        public string TextCodeShort { get; set; }
        public string TextCodeFull { get; set; }
        public decimal UseAbsence { get; set; }
        public decimal ProcessApproval { get; set; }
        public decimal TotalAbsence { get; set; }
        public decimal Percent { get; set; }
    }
    
    public class DbEmployeeUseReimburse : AbstractObject
    {
        public string RequestNo { get; set; }
        public string EmployeeID { get; set; }
        public string TextReimburseMain { get; set; }
        public string TextReimburseSub { get; set; }
        public decimal Amount { get; set; }
        public decimal Vat { get; set; }
        public decimal AmountReimburse { get; set; }
    }

    public class DbRegisterEnrollment: AbstractObject
    {
        public int EnrollmentID { get; set; }
        public int SelectionItemID { get; set; }
        public bool isMainBenefit { get; set; }
        public string TextNameEnrollment { get; set; }
        public decimal Amount { get; set; }
    }

    public class TranasctionUseReimburseByEmployee
    {
        public TranasctionUseReimburseByEmployee()
        {
            ListUseReimburse = new List<DbEmployeeUseReimburse>();
            ListRegisterEnrollment = new List<DbRegisterEnrollment>();
        }
        public decimal TotalAmount { get; set; }
        public decimal TotalAmountUse { get; set; }
        public decimal TotalAmountRemain { get; set; }
        public decimal PercentageUse { get; set; }
        public decimal PercentageRemain { get; set; }
        public List<DbEmployeeUseReimburse> ListUseReimburse { get; set; }
        public List<DbRegisterEnrollment> ListRegisterEnrollment { get; set; }
        public bool IsHaveAmountReimburseByEmployee { get; set; }
    }

    public class EducationChildByEmployee : AbstractObject
    {
        public string RequestNo { get; set; }
        public string EmployeeID { get; set; }
        public string EducationYear { get; set; }
        public string ChildNo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EducationLevel { get; set; }
        public string Academy { get; set; }
        public decimal Amount { get; set; }
        public string EducationClass { get; set; }
        public decimal AmountReimburse { get; set; }
        public decimal QuotaEducation { get; set; }
        public string TextEducationTH { get; set; }
        public string TextEducationEN { get; set; }
    }

    public class DashbaordEducationChildByEmployee
    {
        public DashbaordEducationChildByEmployee()
        {
            ListUseEducationChild = new List<TransactionUseEducationChildByEmployee>();
        }
        public bool IsHaveUseEducationChildEmployee { get; set; }
        public decimal TotalAmountQuota { get; set; }
        public decimal TotalAmountReimburse { get; set; }
        public decimal TotalAmountRemain { get; set; }
        public List<TransactionUseEducationChildByEmployee> ListUseEducationChild { get; set; }
    }

    public class TransactionUseEducationChildByEmployee
    {
        public string Fullname { get; set; }
        public decimal AmountQuota { get; set; }
        public decimal AmountReimburse { get; set; }
        public decimal AmountRemain { get; set; }
        public decimal PercentageUse { get; set; }
        public string TextLevelTH { get; set; }
        public string TextLevelEN { get; set; }
    }

    public class FitnessByEmployee :AbstractObject
    {
        public string EmployeeID { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal PaymentAmount { get; set; }
        public int FitnessConfigID { get; set; }
        public decimal QuotaFitness { get; set; }
    }

    public class DashboardFitnessByEmployee
    {
        public bool IsHaveUseFitness { get; set; }
        public bool IsEnrollment { get; set; }
        public decimal PercentageUse { get; set; }
        public decimal PercentageRemain { get; set; }
        public decimal TotalAmountRemain { get; set; }
        public decimal TotalAmountQuota { get; set; }
        public decimal TotalAmountUse { get; set; }
    }

    public class HealthInsAllowanceParent : AbstractObject
    {
        public string EmployeeID { get; set; }
        public int RelationshipType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal PaymentAmount { get; set; }
        public decimal AmountQuota { get; set; }
        public string TextRelation { get; set; }
    }

    public class DashboardHealthInsAllowanceParentByEmployee
    {
        public DashboardHealthInsAllowanceParentByEmployee()
        {
            ListUseHealthInsAllowanceParent = new List<TransactionUseHealthInsAllowanceParent>();
        }

        public bool IsHaveUseHealth { get; set; }
        public decimal TotalAmountQuota { get; set; }
        public decimal TotalAmountReimburse { get; set; }
        public decimal TotalAmountRemain { get; set; }
        public List<TransactionUseHealthInsAllowanceParent> ListUseHealthInsAllowanceParent { get; set; }
    }

    public class TransactionUseHealthInsAllowanceParent
    {
        public string Fullname { get; set; }
        public decimal AmountQuota { get; set; }
        public decimal AmountReimburse { get; set; }
        public decimal AmountRemain { get; set; }
        public decimal PercentageUse { get; set; }
        public decimal PercentageRemain { get; set; }
    }

    public class BenefitsProvidentFundByEmployee : AbstractObject
    {
        public string FundID { get; set; }
        public string EmployeeID { get; set; }
        public string TitleName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int PF_Year { get; set; }
        public int PF_Month { get; set; }
        public decimal Total_company_amount { get; set; }
        public decimal Total_company_benefit_amount { get; set; }
        public decimal Total_employee_amount { get; set; }
        public decimal Total_employee_benefit_amount { get; set; }
    }
    
    public class DashboardProvidentFundByEmployee
    {
        public bool IsHaveProvidentFund { get; set; }
        public string EmployeeId { get; set; }
        public string FullName { get; set; }
        public decimal TotalContributionCompany { get; set; }
        public decimal TotalContributionEmployee { get; set; }
        public decimal GrandTotal { get; set; }
        public string TextWorkingAge1 { get; set; } // อายุงาน
        public string TextContributionCompany1 { get; set; } // หลักเกณฑ์อัตราเงินสมทบ/อัตราสะสม ปตท.
        public decimal AmountEmployee1 { get; set; } // ยอดเงินของพนักงาน (บาท)
        public string TextWorkingAge2 { get; set; } // อายุงาน
        public string TextContributionCompany2 { get; set; } // หลักเกณฑ์อัตราเงินสมทบ/อัตราสะสม ปตท.
        public decimal AmountEmployee2 { get; set; } // ยอดเงินของพนักงาน (บาท)
    }

    public class DashbaordOtherWelfare : AbstractObject
    {
        public DashbaordOtherWelfare()
        {
            ListOtherWelfareFile_TH = new List<OtherWelfareFile_TH>();
            ListOtherWelfareFile_EN = new List<OtherWelfareFile_EN>();
        }
        public int AnnouncementId { get; set; }
        public string Title_TH { get; set; }
        public string Title_EN { get; set; }
        public string Description_TH { get; set; }
        public string Description_EN { get; set; }
        public string Remark_TH { get; set; }
        public string Remark_EN { get; set; }
        public DateTime UpdateDate { get; set; }
        public List<OtherWelfareFile_TH> ListOtherWelfareFile_TH { get; set; }
        public List<OtherWelfareFile_EN> ListOtherWelfareFile_EN { get; set; }
    }

    public class OtherWelfareFile_TH
    {
        public string TextFile { get; set; }
        public string NameFile { get; set; }
    }

    public class OtherWelfareFile_EN
    {
        public string TextFile { get; set; }
        public string NameFile { get; set; }
    }

    public class EmployeeIncomeForYear : AbstractObject
    {
        public string EmployeeID { get; set; }
		public int  PeriodYear { get; set; }
		public int PeriodMonth { get; set; }
		public char OffCycleFlag { get; set; }
        public string WageTypeCode { get; set; }
        public string WageTypeName { get; set; }
        public string CurrencyAmount { get; set; }
        public decimal Amount { get; set; }
        public string ForPeriod { get; set; }
        public string InPeriod { get; set; }
    }
    
    public class DashboardIncomeYearByEmployee
    {
        public DashboardIncomeYearByEmployee()
        {
            DetailIncome = new List<DashboardIncomeYearByEmployeeDetail>();
        }
        public string Employee { get; set; }
        public int Year { get; set; }
        public decimal TotalIncome { get; set; }
        public List<DashboardIncomeYearByEmployeeDetail> DetailIncome { get; set; }
    }

    public class DashboardIncomeYearByEmployeeDetail
    {
        public string WageTypeCode { get; set; }
        public string WageTypeName { get; set; }
        public decimal TotalAmount { get; set; }
    }

    public class EnrollmentWelfareByEmployee : AbstractObject
    {
        public int EnrollmentID { get; set; }
        public string EmployeeID { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal FlexPoints { get; set; }
        public string RequestNo { get; set; }
        public int SelectionItemID { get; set; }
        public bool isMainBenefit { get; set; }
        public string EmpSubGroup { get; set; }
        public decimal Quota { get; set; }
    }



}
