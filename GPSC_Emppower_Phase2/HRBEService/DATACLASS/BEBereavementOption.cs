﻿using ESS.HR.BE.INFOTYPE;
using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.BE.DATACLASS
{
    public class BEBereavementOption : AbstractObject
    {
        public BEBereavementOption()
        {
            BEBereavementItemData = new List<BEBereavementItem>();
        }

        private BEBereavementInfo __BEBereavementInfo = new BEBereavementInfo();
        public BEBereavementInfo BEBereavementInfo
        {
            get
            {
                return __BEBereavementInfo;
            }
            set
            {
                __BEBereavementInfo = value;
            }
        }


        private List<BEBereavementConfig> __BEBereavementConfigData = new List<BEBereavementConfig>();
        public List<BEBereavementConfig> BEBereavementConfigData
        {
            get
            {
                return __BEBereavementConfigData;
            }
            set
            {
                __BEBereavementConfigData = value;
            }
        }

        public List<BEBereavementItem> BEBereavementItemData { get; set; }

        //private BEBereavementItem[] __BEBereavementItemData = new BEBereavementItem[10];
        //public BEBereavementItem[] BEBereavementItemData
        //{
        //    get
        //    {
        //        return __BEBereavementItemData;
        //    }
        //    set
        //    {
        //        __BEBereavementItemData = value;
        //    }
        //}

        private List<INFOTYPE0021> __FamilyData = new List<INFOTYPE0021>();
        public List<INFOTYPE0021> FamilyData
        {
            get
            {
                return __FamilyData;
            }
            set
            {
                __FamilyData = value;
            }
        }

       


    }
}
