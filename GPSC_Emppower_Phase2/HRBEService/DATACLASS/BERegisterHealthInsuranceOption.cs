﻿using ESS.HR.BE.INFOTYPE;
using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.BE.DATACLASS
{
    public class BERegisterHealthInsuranceOption : AbstractObject
    {
        

        private BERegisterHealthInsurance __BEHealthInsuranceData = new BERegisterHealthInsurance();
        public BERegisterHealthInsurance BEHealthInsuranceData
        {
            get
            {
                return __BEHealthInsuranceData;
            }
            set
            {
                __BEHealthInsuranceData = value;
            }
        }


        private List<BEHealthInsConfig> __BEHealthInsConfigData = new List<BEHealthInsConfig>();
        public List<BEHealthInsConfig> BEHealthInsConfigData
        {
            get
            {
                return __BEHealthInsConfigData;
            }
            set
            {
                __BEHealthInsConfigData = value;
            }
        }

        private List<BERegisterHealthInsuranceItem> __BEHealthInsuranceItemData = new List<BERegisterHealthInsuranceItem>();
        public List<BERegisterHealthInsuranceItem> BEHealthInsuranceItemData
        {
            get
            {
                return __BEHealthInsuranceItemData;
            }
            set
            {
                __BEHealthInsuranceItemData = value;
            }
        }

        private List<INFOTYPE0021> __FamilyData = new List<INFOTYPE0021>();
        public List<INFOTYPE0021> FamilyData
        {
            get
            {
                return __FamilyData;
            }
            set
            {
                __FamilyData = value;
            }
        }

        private List<YearsConfig> __YearData = new List<YearsConfig>();
        public List<YearsConfig> YearData
        {
            get
            {
                return __YearData;
            }
            set
            {
                __YearData = value;
            }
        }


    }
}
