﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.BE.DATACLASS
{
    public class BEPostingData : AbstractObject
    {
        public string RequestNo { get; set; }
        public string EmployeeID { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public int WageTypeID { get; set; }
        public string WageType { get; set; }
        public decimal Amount { get; set; }
        public decimal Number { get; set; } = 0;
        public string Unit { get; set; } = "";
        public string Currency { get; set; }
        public int AssignmentNumber { get; set; } = 0;
    }
}
