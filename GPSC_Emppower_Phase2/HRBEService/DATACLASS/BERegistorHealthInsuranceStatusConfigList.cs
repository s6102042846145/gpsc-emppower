﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.BE.DATACLASS
{
    public  class BERegistorHealthInsuranceStatusConfigList : AbstractObject
    {
        public int Year { get; set; }
        public DateTime BeginDate { get; set; }

        public string EmployeeID { get; set; }

        public DateTime EndDate { get; set; }
        public DateTime CreateDate { get; set; }
        public string Status { get; set; }
        public int HealthConfigID { get; set; }
    }
}
