﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.BE.DATACLASS
{
    public class BEHealthInsAllowanceItem : AbstractObject
    {
        public int HealthInsItemID { get; set; }
        public string RequestNo { get; set; }
        public string VendorName { get; set; }
        public string ReceiptNumber { get; set; }
        public virtual decimal AmountExcludeVAT { get; set; }
        public virtual decimal VAT { get; set; }
        public virtual decimal TotalAmount { get; set; }
        public DateTime RecieptDate { get; set; }

        public string FullName { get; set; }
        public int RelationshipType { get; set; }
        public virtual decimal PaymentAmount { get; set; }
    }
}
