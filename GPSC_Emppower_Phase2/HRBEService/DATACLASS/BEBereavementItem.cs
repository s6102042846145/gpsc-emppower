﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.BE.DATACLASS
{
    public class BEBereavementItem : AbstractObject
    {
        public int ItemID { get; set; }
        public string RequestNo { get; set; }
        public int BereavementType { get; set; }

        public String BereavementTypeText { get; set; }

        public string ChildNo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public virtual decimal Amount { get; set; }

        public virtual decimal SubsidyAmount { get; set; }
        public virtual decimal PaymentFuneralAmount { get; set; }
        public bool DeceasedInDuty { get; set; }
    }
}
