﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.BE.DATACLASS
{
    public class HealthInsAllowanceReport : AbstractObject
    {
        public string DepartmentName { get; set; }
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string RequestNo { get; set; }
        public string Sex { get; set; }
        public string IDCard { get; set; }
        public string BirthDate { get; set; }
        public string Age { get; set; }
        public string InOut { get; set; }
        public string Status { get; set; }
        public string EffectiveDate { get; set; }
        public string InsurancePlan { get; set; }
        public string BankName { get; set; }
        public string BankNo { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Remark { get; set; }
        public string DocumentStatus { get; set; }
        public string Relationship { get; set; }
        public string PermissionType { get; set; }
    }
}
