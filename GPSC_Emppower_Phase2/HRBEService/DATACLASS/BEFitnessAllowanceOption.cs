﻿using ESS.HR.BE.INFOTYPE;
using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.BE.DATACLASS
{
    public class BEFitnessAllowanceOption : AbstractObject
    {

        private BEFitnessAllowance __BEFitnessAllowanceData = new BEFitnessAllowance();
        public BEFitnessAllowance BEFitnessAllowanceData
        {
            get
            {
                return __BEFitnessAllowanceData;
            }
            set
            {
                __BEFitnessAllowanceData = value;
            }
        }

        private BEFitnessAllowanceConfig __BEFitnessAllowanceConfigData = new BEFitnessAllowanceConfig();
        public BEFitnessAllowanceConfig BEFitnessAllowanceConfigData
        {
            get
            {
                return __BEFitnessAllowanceConfigData;
            }
            set
            {
                __BEFitnessAllowanceConfigData = value;
            }
        }



        private BEFitnessAllowanceItem[] __BEFitnessAllowanceItemData = new BEFitnessAllowanceItem[10];
        public BEFitnessAllowanceItem[] BEFitnessAllowanceItemData
        {
            get
            {
                return __BEFitnessAllowanceItemData;
            }
            set
            {
                __BEFitnessAllowanceItemData = value;
            }
        }


        private List<YearsConfig> __YearData = new List<YearsConfig>();
        public List<YearsConfig> YearData
        {
            get
            {
                return __YearData;
            }
            set
            {
                __YearData = value;
            }
        }


    }
}
