﻿using ESS.HR.BE.INFOTYPE;
using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.BE.DATACLASS
{
    public class BEEducationAllowanceOption : AbstractObject
    {
        private List<INFOTYPE0021>  __ChildData = new List<INFOTYPE0021>();
        public List<INFOTYPE0021> ChildData
        {
            get
            {
                return __ChildData;
            }
            set
            {
                __ChildData = value;
            }
        }

        private BEEducationAllowance __EducationAllowanceData = new BEEducationAllowance();
        public BEEducationAllowance EducationAllowanceData
        {
            get
            {
                return __EducationAllowanceData;
            }
            set
            {
                __EducationAllowanceData = value;
            }
        }

        private List<BEConfig> __EducationLevelData = new List<BEConfig>();
        public List<BEConfig> EducationLevelData
        {
            get
            {
                return __EducationLevelData;
            }
            set
            {
                __EducationLevelData = value;
            }
        }


        private List<BEConfig> __EducationYearData = new List<BEConfig>();
        public List<BEConfig> EducationYearData
        {
            get
            {
                return __EducationYearData;
            }
            set
            {
                __EducationYearData = value;
            }
        }

    }
}
