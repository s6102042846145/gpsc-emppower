﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.BE.DATACLASS
{
    public  class BEBereavementInfo : AbstractObject
    {
        public string RequestNo { get; set; }
        public string EmployeeID { get; set; }
        public DateTime BereavementDate { get; set; }
        public int FuneralType { get; set; }
        public DateTime FuneralDate { get; set; }
        public DateTime FuneralBeginDate { get; set; }
        public DateTime FuneralEndDate { get; set; }
        public string Place { get; set; }
        public string Pavilion { get; set; }
        public string PlaceDetail { get; set; }
        public int CremationType { get; set; }
        public DateTime CremationDate { get; set; }
        public string CremationPlace { get; set; }
        public string PresidentDepartment { get; set; }
        public DateTime PresidentDate { get; set; }
        public string ResponsibleName { get; set; }
        public string ResponsibleDepartment { get; set; }
        public string ResponsiblePhone { get; set; }
        public int ResponsibleFuneral { get; set; }
       
        public  decimal TotalAmount { get; set; }
        public int IsAllowPR { get; set; }

        public int Wreath { get; set; }
        
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public string Status { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime PostingDate { get; set; }
        public string PostingMessage { get; set; }
        public string ErrorText { get; set; }

        public decimal Salary { get; set; }

        public string RoleAdminName { get; set; }

        public string RoleAdmin { get; set; }


        public string RequestorCompanyCode { get; set; }
        public string KeyMaster { get; set; }
        public int BereavementType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual decimal SubsidyAmount { get; set; }
        public virtual decimal PaymentFuneralAmount { get; set; }
        public string ReligionId { get; set; }
        public bool DeceasedInDuty { get; set; }
    }

    public class ReportBEBereavementInfoByAdmin
    {
        public string RequestNo { get; set; }
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string WorkLocationName { get; set; }
        public string County { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int BereavementType { get; set; }
        public DateTime BereavementDate { get; set; }
        public string PlaceDetail { get; set; }
        public int IsAllowPR { get; set; }
        public string ReligionName { get; set; }
        public bool? DeceasedInDuty { get; set; }
        public decimal SubsidyAmount { get; set; }
        public decimal PaymentFuneralAmount { get; set; }
        public decimal Amount { get; set; }
        public DateTime? SubmitDate { get; set; }
        public DateTime? ActionDate { get; set; }
        public DateTime? PostingDate { get; set; }
    }
}
