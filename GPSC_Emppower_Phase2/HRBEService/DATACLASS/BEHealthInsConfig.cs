﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.BE.DATACLASS
{
    public class BEHealthInsConfig
    {
        public int HealthConfigID { get; set; }
        public string BeginDate { get; set; }

        public string EndDate { get; set; }

        public int Year { get; set; }

        public string Status { get; set; }

    }
}
