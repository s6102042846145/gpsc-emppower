﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.BE.DATACLASS
{
    public class BenefitReport : AbstractObject
    {
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string RequestNo { get; set; }
        public string Type { get; set; }
        public string CreateDate { get; set; }
        public string UpdateDate { get; set; }
        public virtual decimal AmountReimburse { get; set; }
        public string status { get; set; }
        public string PostingDate { get; set; }
        public string PostingMessage { get; set; }
    }
}
