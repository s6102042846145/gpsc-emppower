﻿using ESS.HR.BE.INFOTYPE;
using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.BE.DATACLASS
{
    public class BEHealthInsAllowanceOption : AbstractObject
    {
        

        private BEHealthInsAllowance __BEHealthInsAllowanceData = new BEHealthInsAllowance();
        public BEHealthInsAllowance BEHealthInsAllowanceData
        {
            get
            {
                return __BEHealthInsAllowanceData;
            }
            set
            {
                __BEHealthInsAllowanceData = value;
            }
        }


        private List<BEHealthInsAllowanceConfig> __BEHealthInsAllowanceConfigData = new List<BEHealthInsAllowanceConfig>();
        public List<BEHealthInsAllowanceConfig> BEHealthInsAllowanceConfigData
        {
            get
            {
                return __BEHealthInsAllowanceConfigData;
            }
            set
            {
                __BEHealthInsAllowanceConfigData = value;
            }
        }

        private BEHealthInsAllowanceItem[] __BEHealthInsAllowanceItemData = new BEHealthInsAllowanceItem[10];
        public BEHealthInsAllowanceItem[] BEHealthInsAllowanceItemData
        {
            get
            {
                return __BEHealthInsAllowanceItemData;
            }
            set
            {
                __BEHealthInsAllowanceItemData = value;
            }
        }

        private List<INFOTYPE0021> __FamilyData = new List<INFOTYPE0021>();
        public List<INFOTYPE0021> FamilyData
        {
            get
            {
                return __FamilyData;
            }
            set
            {
                __FamilyData = value;
            }
        }

        private List<YearsConfig> __YearData = new List<YearsConfig>();
        public List<YearsConfig> YearData
        {
            get
            {
                return __YearData;
            }
            set
            {
                __YearData = value;
            }
        }


    }
}
