﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.BE.DATACLASS
{
    public class BERegisterHealthInsurance : AbstractObject
    {


        public string RequestNo { get; set; }
        public string EmployeeID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public string Year { get; set; }
        public virtual decimal TotalAmount { get; set; }
        public virtual decimal PaymentAmount { get; set; }
        public string Status { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public string UpdateBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public DateTime PostingDate { get; set; }
        public string PostingMessage { get; set; }
        public int HealthConfigID { get; set; }
       

     

    }

    public class BERegisterHealthInsuranceByAdmin
    {
        public string RequestNo { get; set; }
        public string EmployeeID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public string Year { get; set; }
        public virtual decimal TotalAmount { get; set; }
        public virtual decimal PaymentAmount { get; set; }
        public string Status { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public string UpdateBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public DateTime PostingDate { get; set; }
        public string PostingMessage { get; set; }
        public int HealthConfigID { get; set; }
        //public string RequestNo { get; set; }
        //public string EmployeeID { get; set; }
        //public string EmployeeName { get; set; }
        //public string WorkLocationName { get; set; }
        //public string County { get; set; }
        //public string FirstName { get; set; }
        //public string LastName { get; set; }
        //public string VendorName { get; set; }
        //public decimal AmountExcludeVAT { get; set; }
        //public decimal Vat { get; set; }
        //public decimal TotalAmount { get; set; }
        //public decimal PaymentAmount { get; set; }
        //public DateTime? CreateDate { get; set; }
        //public DateTime? PostingDate { get; set; }
        //public DateTime? SubmitDate { get; set; }
        //public DateTime? ActionDate { get; set; }
        //public string Relationship { get; set; }
        //public DateTime? BirthDate { get; set; }
        //public DateTime? RecieptDate { get; set; }
    }
}
