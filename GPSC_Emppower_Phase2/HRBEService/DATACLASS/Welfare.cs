﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.BE.DATACLASS
{
    public class ReportDisbursementOfWelfareByAdmin
    {
        public string RequestNo { get; set; }
        public string Company { get; set; }
        public string EmployeeId { get; set; }
        public string OrgunitId { get; set; }
        public string PositionId { get; set; }
        public string Name { get; set; }
        public string EmployeeName { get; set; }
        public string WorkLocationName { get; set; }
        public string County { get; set; }
        public string Agency { get; set; } // หน่วยงาน
        public string Affiliate { get; set; } // สังกัด
        public string WorkLocation { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? SubmitDate { get; set; }
        public DateTime? ActionDate { get; set; }
        public decimal TotalAmount { get; set; }
        public DateTime? PostingDate { get; set; }
        public string TypeWelfare { get; set; }
    }

    public class EducationWelfare
    {
        public string RequestNo { get; set; }
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string OrgunitId { get; set; }
        public string PositionId { get; set; }
        public string WorkLocationName { get; set; }
        public string County { get; set; }
        public decimal Amount { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? SubmitDate { get; set; }
        public DateTime? ActionDate { get; set; }
        public DateTime? PostingDate { get; set; }
    }

    public class FitnessWelfare
    {
        public string RequestNo { get; set; }
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string OrgunitId { get; set; }
        public string PositionId { get; set; }
        public string WorkLocationName { get; set; }
        public string County { get; set; }
        public decimal TotalAmount { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? PostingDate { get; set; }
        public DateTime? SubmitDate { get; set; }
        public DateTime? ActionDate { get; set; }
    }

    public class HealthWelfare
    {
        public string RequestNo { get; set; }
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string OrgunitId { get; set; }
        public string PositionId { get; set; }
        public string WorkLocationName { get; set; }
        public string County { get; set; }
        public decimal TotalAmount { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? PostingDate { get; set; }
        public DateTime? SubmitDate { get; set; }
        public DateTime? ActionDate { get; set; }
    }

    public class BereavmentWelfare
    {
        public string RequestNo { get; set; }
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string OrgunitId { get; set; }
        public string PositionId { get; set; }
        public string WorkLocationName { get; set; }
        public string County { get; set; }
        public decimal Amount { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? SubmitDate { get; set; }
        public DateTime? ActionDate { get; set; }
        public DateTime? PostingDate { get; set; }
    }

    public class ReimburseWelfare
    {
        public string RequestNo { get; set; }
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string OrgunitId { get; set; }
        public string PositionId { get; set; }
        public string WorkLocationName { get; set; }
        public string County { get; set; }
        public decimal Amount { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? SubmitDate { get; set; }
        public DateTime? ActionDate { get; set; }
        public DateTime? PostingDate { get; set; }
    }

    public class AgeFamily
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }
    }

}
