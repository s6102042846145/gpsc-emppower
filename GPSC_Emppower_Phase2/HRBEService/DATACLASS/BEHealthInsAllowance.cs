﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.BE.DATACLASS
{
    public class BEHealthInsAllowance : AbstractObject
    {
        public string RequestNo { get; set; }
        public string EmployeeID { get; set; }
        public int RelationshipType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public string Year { get; set; }
        public virtual decimal TotalAmount { get; set; }
        public virtual decimal PaymentAmount { get; set; }
        public string Status { get; set; }
        public string Status_TH { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public string UpdateBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public DateTime PostingDate { get; set; }
        public string PostingMessage { get; set; }
        public string RequestorCompanyCode { get; set; }
        public string KeyMaster { get; set; }

        public string ErrorText { get; set; }
        public virtual decimal Quota_all { get; set; }
        public virtual decimal Balance { get; set; }
        public virtual decimal sum_TotalAmount { get; set; }

        public int HealthInsConfigID { get; set; }
        public int Attachments_num { get; set; }

        public string RoleAdmin { get; set; }
        public string RoleAdminName { get; set; }

    }

    public class ReportHealthInsAllowanceByAdmin
    {
        public string RequestNo { get; set; }
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string WorkLocationName { get; set; }
        public string County { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string VendorName { get; set; }
        public decimal AmountExcludeVAT { get; set; }
        public decimal Vat { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal PaymentAmount { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? PostingDate { get; set; }
        public DateTime? SubmitDate { get; set; }
        public DateTime? ActionDate { get; set; }
        public string Relationship { get; set; }
        public DateTime? BirthDate { get; set; }
        public DateTime? RecieptDate { get; set; }
    }
}
