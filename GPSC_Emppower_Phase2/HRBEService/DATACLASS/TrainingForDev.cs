﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.BE.DATACLASS
{
    public class TrainingForDev : AbstractObject
    {
        public string RequestNo { get; set; }

        public string ContentType { get; set; }
        public string WorkflowType { get; set; }
        public string EmployeeID { get; set; }
        public virtual decimal TotalAmount { get; set; }
        public string Status { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }


    }
}
