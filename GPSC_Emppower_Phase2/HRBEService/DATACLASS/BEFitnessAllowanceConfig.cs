﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.BE.DATACLASS
{
    public class BEFitnessAllowanceConfig : AbstractObject
    {
        public int FitnessConfigID { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public string AssignmentBeginPeriod { get; set; }
        public string AssignmentEndPeriod { get; set; }
        public virtual decimal Quota { get; set; }
    }
}
