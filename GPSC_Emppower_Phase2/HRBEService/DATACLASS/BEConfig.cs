﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.BE.DATACLASS
{
    public class BEConfig : AbstractObject
    {
        public int BE_ConfigID { get; set; }
        public string BE_Type { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string BE_Level_TH { get; set; }
        public string BE_Level_EN { get; set; }
        public virtual decimal Quota { get; set; }
    }
}
