﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.BE.DATACLASS
{
    public class BERegisterHealthInsuranceItem : AbstractObject
    {



        public int HealthInsItemID { get; set; }
        public string RequestNo { get; set; }
        public string RelationshipType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public virtual decimal TotalAmount { get; set; }
        public virtual decimal PaymentAmount { get; set; }
        public string Status { get; set; }
        public DateTime CreateDate { get; set; }

        public string CreateBy { get; set; }
        public string UpdateBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public DateTime PostingDate { get; set; }
        public string PostingMessage { get; set; }

    }
}
