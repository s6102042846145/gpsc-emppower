﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.BE.DATACLASS
{
    public  class BEBereavementConfig : AbstractObject
    {
        public int BereavementConfigID { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public int FamilyType { get; set; }
        public virtual decimal Quota { get; set; }
        public string Type { get; set; }
        public virtual decimal MinValue { get; set; }
        public int BereavementType { get; set; }
    }
}
