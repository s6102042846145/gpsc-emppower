﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.BE.DATACLASS
{
    public class BEEducationAllowance : AbstractObject
    {
        public string RequestNo { get; set; }
        public string EmployeeID { get; set; }
        public string EducationYear { get; set; }
        public string ChildNo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int EducationLevel { get; set; }
        public DateTime BirthDate { get; set; }
        public int AcademyType { get; set; }
        public int WithdrawType { get; set; }
        public string Academy { get; set; }
        public virtual decimal Amount { get; set; }
        public string Attachment { get; set; }
        public string ReceiptNumber { get; set; }
        public string status { get; set; }
        public string Status_TH { get; set; }
        public DateTime PayDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public string EducationClass { get; set; }
        public string RequestorCompanyCode { get; set; }
        public string KeyMaster { get; set; }
        public string ErrorText { get; set; }
        public decimal AmountReimburse { get; set; }
        public DateTime PostingDate { get; set; }
        public string PostingMessage { get; set; }
        public int PayDate_String { get; set; }
        public string Amount_String { get; set; }
        public string EducationLevel_String { get; set; }
        public int IsAdmin { get; set; }
        public int IsEdit { get; set; }
        public int MoveStatus { get; set; }
        public int DiscountStatus { get; set; }
        public string RoleAdmin { get; set; }
        public string RoleAdminName { get; set; }
        public virtual decimal QuotaAll { get; set; }
        public string BE_Level_TH { get; set; }
        public string BE_Level_EN { get; set; }

    }


    public class ReportBEEducationAllowanceByAdmin : AbstractObject
    {
        public string RequestNo { get; set; }
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string WorkLocationName { get; set; }
        public string County { get; set; }
        public string EducationYear { get; set; }
        public string ChildNo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int EducationLevel { get; set; }
        public string BE_Level_TH { get; set; }
        public string BE_Level_EN { get; set; }
        public string Academy { get; set; }
        public int AcademyType { get; set; }
        public int WithdrawType { get; set; }
        public decimal Amount { get; set; }
        public decimal AmountReimburse { get; set; }
        public string status { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? SubmitDate { get; set; }
        public DateTime? ActionDate { get; set; }
        public DateTime? PostingDate { get; set; }
        public DateTime? BirthDate { get; set; }
        public DateTime? PayDate { get; set; }
    }

    
}
