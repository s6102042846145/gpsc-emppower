﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.BE.DATACLASS
{
    public class DataReligion : AbstractObject
    {
        public string ReligionKey { get; set; }
        public string ReligionName { get; set; }
    }
}
