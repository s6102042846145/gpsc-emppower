﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.BE.DATACLASS
{
    public class BEFitnessAllowanceItem : AbstractObject
    {
        public int FitnessItemID { get; set; }
        public string RequestNo { get; set; }
        public string VendorName { get; set; }
        public string ReceiptNumber { get; set; }
        public virtual decimal AmountExcludeVAT { get; set; }
        public virtual decimal VAT { get; set; }
        public virtual decimal TotalAmount { get; set; }
        public DateTime RecieptDate { get; set; }


        public string ErrorText { get; set; }

        public virtual decimal PaymentAmount { get; set; }

    }
}
