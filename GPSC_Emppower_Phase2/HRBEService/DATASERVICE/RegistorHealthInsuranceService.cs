﻿using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE;
using ESS.HR.BE.DATACLASS;
using ESS.HR.BE.INFOTYPE;
using ESS.HR.PA;
using ESS.HR.PA.DATACLASS;
using ESS.SHAREDATASERVICE;
using ESS.HR.BE.INFOTYPE;

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.BE.DATASERVICE
{
    class RegistorHealthInsuranceService : AbstractDataService
    {
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {

            BERegisterHealthInsuranceOption oBEHealthInsurance = new BERegisterHealthInsuranceOption();

            //Get Year
            var Year = "2021";
            if (!string.IsNullOrEmpty(CreateParam))
            {
                //set RecieptDate

                oBEHealthInsurance.BEHealthInsuranceData.CreateDate = DateTime.Now;
                DateTime DateNow = DateTime.Now;
                INFOTYPE0002 tmp = new INFOTYPE0002();
                PersonalInformation oResult = new PersonalInformation();
                tmp = HRBEManagement.CreateInstance(Requestor.CompanyCode).GetPersonalData(Requestor.EmployeeID, DateNow);
                List<INFOTYPE0021> Son = new List<INFOTYPE0021>();
                List<INFOTYPE0021> FamilyData = HRBEManagement.CreateInstance(Requestor.CompanyCode).GetFamilyDataB(Requestor.EmployeeID);



             
                oBEHealthInsurance.BEHealthInsuranceData.EmployeeID = Requestor.EmployeeID;
                oBEHealthInsurance.BEHealthInsuranceData.FirstName = tmp.FirstName;
                oBEHealthInsurance.BEHealthInsuranceData.LastName = tmp.LastName;
                oBEHealthInsurance.BEHealthInsuranceData.BirthDate = tmp.BeginDate;
                oBEHealthInsurance.BEHealthInsuranceData.Year = DateNow.Year.ToString();
                oBEHealthInsurance.BEHealthInsuranceData.Status = null;
                oBEHealthInsurance.BEHealthInsuranceData.CreateBy = Requestor.EmployeeID;
                oBEHealthInsurance.BEHealthInsuranceData.UpdateBy = Requestor.EmployeeID;
                oBEHealthInsurance.BEHealthInsuranceData.UpdateDate = DateNow;
                oBEHealthInsurance.BEHealthInsuranceData.HealthConfigID = Int16.Parse(CreateParam);


                





        DateTime CheckStartWork = new DateTime(2020, 4, 1);
              
                DateTime Datenew = DateTime.Now;
                int Age = Datenew.Year - tmp.DOB.Year;
                INFOTYPE0021 pushtmp = new INFOTYPE0021();
                pushtmp = new INFOTYPE0021
                {
                    Ischeck = true,
                    IsDisable = true,
                    FamilyMember = "13",
                    ChildNo = null,
                    TitleName = tmp.TitleID,
                    TitleRank = null,
                    Name = tmp.FirstName,
                    Surname = tmp.LastName,
                    BirthDate = tmp.DOB,
                    Sex = tmp.Gender,
                    BirthPlace = tmp.BirthPlace,
                    CityOfBirth = tmp.BirthCity,
                    Nationality = tmp.Nationality,
                    FatherID = null,
                    MotherID = null,
                    SpouseID = null,
                    MotherSpouseID = null,
                    FatherSpouseID = null,
                    JobTitle = null,
                    Employer = null,
                    Dead = null,
                    Remark = null,
                    Address = null,
                    Street = null,
                    District = null,
                    City = null,
                    Postcode = null,
                    Country = null,
                    TelephoneNo = null,
                    Age = Age
                };
                FamilyData.Insert(0, pushtmp);
                if (tmp.BeginDate > CheckStartWork)
                {

                    var itemToRemove11 = FamilyData.Single(r => r.FamilyMember == "11");
                    FamilyData.Remove(itemToRemove11);
                    var itemToRemove12 = FamilyData.Single(r => r.FamilyMember == "12");
                    FamilyData.Remove(itemToRemove12);

                }
                
                    
                
                FamilyData.RemoveAll(t => t.FamilyMember == "7");
                FamilyData.RemoveAll(t => t.FamilyMember == "6");

                var itemToRemove2 = FamilyData.Where(r => r.FamilyMember == "2");


                int Soncount = itemToRemove2.Count();
                if (itemToRemove2.Count() > 0)
                {
                    for (int i = 1; i <= Soncount; i++)
                    {
                        var itemToRemove2Set = FamilyData.First(r => r.FamilyMember == "2");
                        if (!string.IsNullOrEmpty(itemToRemove2Set.FamilyMember))
                        {
                            Son.Add(itemToRemove2Set);
                            FamilyData.Remove(itemToRemove2Set);
                        }

                    }
                }

                //var itemToRemoveDead = Son.Where(r => r.Dead == "X");

                //if (itemToRemoveDead.Count() > 0)
                //{
                //    for (int i = 1; i <= itemToRemoveDead.Count(); i++)
                //    {
                //        var itemToRemoveDeadSet = Son.Single(r => r.Dead == "X");
                //        if (!string.IsNullOrEmpty(itemToRemoveDeadSet.FamilyMember))
                //        {
                //            Son.Remove(itemToRemoveDeadSet);
                //        }

                //    }
                //}


                Son.Sort(delegate (INFOTYPE0021 x, INFOTYPE0021 y)
                {
                    if (x.BirthDate == null && y.BirthDate == null) return 0;
                    else if (x.BirthDate == null) return -1;
                    else if (y.BirthDate == null) return 1;
                    else return x.BirthDate.CompareTo(y.BirthDate);
                });

                foreach (INFOTYPE0021 member in Son)
                {
                    if (member.Age > 20 && member.Dead == "X") {
                    }
                    else
                    {
                        FamilyData.Add(member);
                    }
                   
                }
                int HealthInsuranceBeginAge = Convert.ToInt32(ShareDataManagement.LookupCache(Requestor.CompanyCode, "ESS.HR.BE", "RegisterHealthInsuranceBeginAge"));
                int HealthInsuranceEndAge = Convert.ToInt32(ShareDataManagement.LookupCache(Requestor.CompanyCode, "ESS.HR.BE", "RegisterHealthInsuranceEndDate"));
                int CHILDNO = 0;
                int countCHIILDDead = 0;
                bool CHILD1Dead = false;
                DateTime birthDate = DateTime.Now;
                foreach (INFOTYPE0021 member in FamilyData)
                {
                    if (member.FamilyMember == "11") {
                        if(member.Age <= HealthInsuranceBeginAge && member.Age >= HealthInsuranceEndAge)
                        {
                            member.IsDisable = true;
                            member.Remark = "FATHERMOTHERAGELIMIT";
                        }
                    }
                    if (member.FamilyMember == "12") { }
                    {
                        if (member.Age <= HealthInsuranceBeginAge && member.Age >= HealthInsuranceEndAge)
                        {
                            member.IsDisable = true;
                            member.Remark = "FATHERMOTHERAGELIMIT";
                        }
                    }
                    if (member.FamilyMember == "2")
                    {
                       
                        if (member.Age > 23) {
                            member.IsDisable = true;
                            member.Remark = "CHILDAGELIMIT";
                        }
                        else
                        {
                            CHILDNO = CHILDNO + 1;
                            if (member.Age > 20)
                            {
                                if (member.Dead == "X")
                                {
                                    countCHIILDDead = countCHIILDDead + 1;
                                    CHILD1Dead = true;
                                }
                            }
                            else
                            {
                                if (CHILDNO > 3 && CHILD1Dead)
                                {
                                    member.IsDisable = true;
                                }
                                else
                                {
                                    if (member.Dead == "X")
                                    {
                                        CHILD1Dead = true;
                                    }
                                }
                            }
                           
                        }
                    }
                    if (member.Dead == "X") {
                        member.IsDisable = true;
                        member.Remark = "Dead";
                    }

                    oBEHealthInsurance.FamilyData.Add(member);
                }

            }

            return oBEHealthInsurance;
        }

        public override void PrepareData(EmployeeData Requestor, object Data)
        {

        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            BERegisterHealthInsuranceOption oBEHealthInsurance = JsonConvert.DeserializeObject<BERegisterHealthInsuranceOption>(newData.ToString());

            int countIsCheck = 0;
            foreach (var Item in oBEHealthInsurance.FamilyData)
            { 
                if(Item.Ischeck == true)
                {
                    countIsCheck = countIsCheck + 1;
                }
            }
            if(countIsCheck > 5)
            {
                throw new DataServiceException("BE_HEALTHINSURANCEREGISTER", "ISCHECK");
            }

            List<BERegistorHealthInsuranceConfig> oResult = new List<BERegistorHealthInsuranceConfig>();
            oResult = HRBEManagement.CreateInstance(Requestor.CompanyCode).GetRegistorHealthInsuranceConfig(Requestor.EmployeeID);
            if(oResult[0].Status == 1)
            {

            }
            else
            {
                throw new DataServiceException("BE_HEALTHINSURANCEREGISTER", "TIMELIMIT");
            }


        }

        public override void CalculateInfoData(EmployeeData Requestor, object Data, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
           
        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            BERegisterHealthInsuranceOption oBEHealthInsurance = JsonConvert.DeserializeObject<BERegisterHealthInsuranceOption>(Data.ToString());
            HRBEManagement oHRBEManagement = HRBEManagement.CreateInstance(Requestor.CompanyCode);
            int YEARREGISHEALTH = Convert.ToInt32(ShareDataManagement.LookupCache(Requestor.CompanyCode, "ESS.HR.BE", "YEAR_BE_HEALTH_FROM_REGIS_HEALTH"));
            foreach (var Item in oBEHealthInsurance.FamilyData)
            {
                if(Item.Ischeck == true)
                {
                    BERegisterHealthInsuranceItem pushitem = new BERegisterHealthInsuranceItem();
                    pushitem = new BERegisterHealthInsuranceItem
                    {
                        RequestNo = RequestNo,
                        RelationshipType = Item.FamilyMember,
                        FirstName = Item.Name,
                        LastName = Item.Surname,
                        BirthDate = Item.BirthDate,
                        TotalAmount = 0,
                        PaymentAmount  = 0,
                        Status = State,
                        CreateDate = DateTime.Now,
                        CreateBy = Requestor.EmployeeID,
                        UpdateBy = Requestor.EmployeeID,
                    };
                    oBEHealthInsurance.BEHealthInsuranceItemData.Add(pushitem);
                }
            }
            oBEHealthInsurance.BEHealthInsuranceData.RequestNo = RequestNo;
            oBEHealthInsurance.BEHealthInsuranceData.Status = State;
            oHRBEManagement.DeleteBERegisterHealthInsuranceItem(RequestNo);
            oHRBEManagement.SaveRegisterBEHealthInsurance(oBEHealthInsurance.BEHealthInsuranceData);
           
            
            foreach (var Item in oBEHealthInsurance.BEHealthInsuranceItemData)
            {
               oHRBEManagement.SaveRegisterBEHealthInsuranceItem(Item);   
            }

            DataRow dr = Info.NewRow();
            dr["BENEFITCODE"] = "";
            dr["DOCUMENTNO"] = oBEHealthInsurance.BEHealthInsuranceData.RequestNo;
            Info.Rows.Add(dr);
            Data = oBEHealthInsurance;

        }

        public override void ValidateDataInAction(object newData, DataTable Info, EmployeeData Requestor, string RequestNo, string State, string Action, bool HaveComment, bool HaveComment2, DateTime SubmitDate)
        {
            if (Action != "CANCEL")
            {
                if (DateTime.Now.Year != SubmitDate.Year)
                {
                    throw new DataServiceException("HRTM_EXCEPTION", "TIMEOUT_PERIOD_SETTING");
                }
            }
        }
    }
}
