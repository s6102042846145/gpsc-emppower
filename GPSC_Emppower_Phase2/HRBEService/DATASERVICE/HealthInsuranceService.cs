﻿using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE;
using ESS.HR.BE.DATACLASS;
using ESS.HR.BE.INFOTYPE;
using ESS.SHAREDATASERVICE;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.BE.DATASERVICE
{
    class HealthInsuranceService : AbstractDataService
    {
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
           
            BEHealthInsAllowanceOption oBEHealthInsAllowance = new BEHealthInsAllowanceOption();

            //Get Year
            var Year = CreateParam;
            if (!string.IsNullOrEmpty(CreateParam))
            {
                //set RecieptDate
                for (var i = 0; i < 10; i++)
                {
                    oBEHealthInsAllowance.BEHealthInsAllowanceItemData[i] = new BEHealthInsAllowanceItem();

                if (Year == DateTime.Now.Year.ToString())
                {
                    oBEHealthInsAllowance.BEHealthInsAllowanceItemData[i].RecieptDate = DateTime.Now;
                }
                else
                {
                    oBEHealthInsAllowance.BEHealthInsAllowanceItemData[i].RecieptDate = DateTime.ParseExact(Year + "/01/01", "yyyy/MM/dd", CultureInfo.InvariantCulture); ;
                }
            }

            //Get Config
         
            oBEHealthInsAllowance.BEHealthInsAllowanceConfigData = HRBEManagement.CreateInstance(Requestor.CompanyCode).GetHealthInsAllowanceConfig(Year);
          
            var sub_type = "";
            foreach (BEHealthInsAllowanceConfig item in oBEHealthInsAllowance.BEHealthInsAllowanceConfigData)
            {
                sub_type += item.RelationshipType + ",";
            }
            sub_type = sub_type.Remove(sub_type.Length - 1);

            //get ข้อมมูลครอบครัว

            var FamilyList = HRBEManagement.CreateInstance(Requestor.CompanyCode).GetFamilyData(Requestor.EmployeeID, sub_type);
            // อายุของบิดา มารดา จะอยู่ระหว่าง 70 – 80 ปี 
            int HealthInsAllowanceBeginAge = Convert.ToInt32(ShareDataManagement.LookupCache(Requestor.CompanyCode, "ESS.HR.BE.SAP", "HealthInsAllowanceBeginAge"));
            int HealthInsAllowanceEndAge = Convert.ToInt32(ShareDataManagement.LookupCache(Requestor.CompanyCode, "ESS.HR.BE.SAP", "HealthInsAllowanceEndAge"));
            foreach (var item in FamilyList)
            {

                    //var Age = item.Age0101;
                    int YearChk = ((item.BirthDate.Day == 1 && item.BirthDate.Month == 1) ? 0 : 1);     // 19-01-2021 : เช็คเพิ่มเติม เกิดวันที่ 1 ม.ค. ไม่ต้องบวก 1 
                    DateTime DateChk = new DateTime(item.BirthDate.Year + YearChk, 1, 1);
                    var BirthDateAdd70 = DateChk.AddYears(HealthInsAllowanceBeginAge);
                    var BirthDateAdd80 = DateChk.AddYears(HealthInsAllowanceEndAge);

                    if (DateTime.Now >= BirthDateAdd70 && DateTime.Now < BirthDateAdd80)
                    {
                        oBEHealthInsAllowance.FamilyData.Add(item);
                    }

                    //if (Age >= HealthInsAllowanceBeginAge && Age <= HealthInsAllowanceEndAge)
                    //{
                    //    oBEHealthInsAllowance.FamilyData.Add(item);
                    //}
                }


                //get ข้อมูลปี
                oBEHealthInsAllowance.YearData = HRBEManagement.CreateInstance(Requestor.CompanyCode).GetYears();

                //Get Role Admin
                oBEHealthInsAllowance.BEHealthInsAllowanceData.RoleAdminName = ShareDataManagement.LookupCache(Requestor.CompanyCode, "ESS.HR.BE", "ADMIN");

            }

            return oBEHealthInsAllowance;
        }

        public override void PrepareData(EmployeeData Requestor, object Data)
        {

        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            BEHealthInsAllowanceOption oBEHealthInsAllowanceOption = JsonConvert.DeserializeObject<BEHealthInsAllowanceOption>(newData.ToString());

            //if (string.IsNullOrEmpty(oBEHealthInsAllowanceOption.BEHealthInsAllowanceData.Year))
            //{
            //    throw new DataServiceException("BE_HEALTHIANSALLOWANCE", "YEAR_CANTBE_NULL");
            //}

            if (oBEHealthInsAllowanceOption.BEHealthInsAllowanceData.RelationshipType==0)
            {
                throw new DataServiceException("BE_HEALTHIANSALLOWANCE", "RELATIONSHIP_CANTBE_NULL");
            }

            var CountItem = 0;
            foreach (var Item in oBEHealthInsAllowanceOption.BEHealthInsAllowanceItemData)
            {
                if (Item != null)
                {
                    if (!string.IsNullOrEmpty(Item.VendorName))
                    {
                        CountItem++;
                        if (string.IsNullOrEmpty(Item.ReceiptNumber))
                        {
                            throw new DataServiceException("BE_HEALTHIANSALLOWANCE", "RECEIPTNUMBER_CANTBE_NULL");
                        }

                        if (Item.RecieptDate == null)
                        {
                            throw new DataServiceException("BE_HEALTHIANSALLOWANCE", "RECIEPTDATE_CANTBE_NULL");
                        }

                        if (Item.AmountExcludeVAT == 0)
                        {
                            throw new DataServiceException("BE_HEALTHIANSALLOWANCE", "AMOUNTEXCLUDEVAT_CANTBE_NULL");
                        }

                        //if (Item.TotalAmount == 0)
                        //{
                        //    throw new DataServiceException("BE_HEALTHIANSALLOWANCE", "TOTALAMOUNT_CANTBE_NULL");
                        //}
                    }

                    if (!string.IsNullOrEmpty(Item.ReceiptNumber))
                    {
                        CountItem++;
                        if (string.IsNullOrEmpty(Item.VendorName))
                        {
                            throw new DataServiceException("BE_HEALTHIANSALLOWANCE", "VENDORNAME_CANTBE_NULL");
                        }

                        if (Item.RecieptDate == null)
                        {
                            throw new DataServiceException("BE_HEALTHIANSALLOWANCE", "RECIEPTDATE_CANTBE_NULL");
                        }

                        if (Item.AmountExcludeVAT == 0)
                        {
                            throw new DataServiceException("BE_HEALTHIANSALLOWANCE", "AMOUNTEXCLUDEVAT_CANTBE_NULL");
                        }

                        //if (Item.TotalAmount == 0)
                        //{
                        //    throw new DataServiceException("BE_HEALTHIANSALLOWANCE", "TOTALAMOUNT_CANTBE_NULL");
                        //}
                    }


                    if (Item.TotalAmount != 0)
                    {
                        CountItem++;
                        if (string.IsNullOrEmpty(Item.ReceiptNumber))
                        {
                            throw new DataServiceException("BE_HEALTHIANSALLOWANCE", "RECEIPTNUMBER_CANTBE_NULL");
                        }

                        if (string.IsNullOrEmpty(Item.ReceiptNumber))
                        {
                            throw new DataServiceException("BE_HEALTHIANSALLOWANCE", "RECEIPTNUMBER_CANTBE_NULL");
                        }

                        if (Item.RecieptDate == null)
                        {
                            throw new DataServiceException("BE_HEALTHIANSALLOWANCE", "RECIEPTDATE_CANTBE_NULL");
                        }

                        if (string.IsNullOrEmpty(Item.VendorName))
                        {
                            throw new DataServiceException("BE_HEALTHIANSALLOWANCE", "VENDORNAME_CANTBE_NULL");
                        }
                    }

                   



                    //เบิกผ่านระบบภายใน 90 วันนับแต่วันที่ได้จ่ายเงินค่ารักษาพยาบาล ใบเสร็จของปีที่แล้วสามารถเบิกได้ภายในวันที่ 5 มกราคมของปีถัดไปเท่านั้น
                    var datelimit = Convert.ToInt32(ShareDataManagement.LookupCache(Requestor.CompanyCode, "ESS.HR.BE", "HEALTHIANSALLOWANCE_LIMIT"));
                    if (!string.IsNullOrEmpty(Item.ReceiptNumber))
                    {
                        //ไม่ได้เป็น Admin จะต้องเช็ควันที่เบิก
                        if (Convert.ToInt32(oBEHealthInsAllowanceOption.BEHealthInsAllowanceData.RoleAdmin) == 0)
                        {

                            if (Item.RecieptDate.AddDays(datelimit).Date <= DateTime.Now.Date)
                            {
                                throw new DataServiceException("BE_HEALTHIANSALLOWANCE", "VALIDETE_DATE90");
                            }

                            //เบิกล่วงหน้าไม่ได้
                            if (Item.RecieptDate.Date > DateTime.Now.Date)
                            {

                                throw new DataServiceException("BE_HEALTHIANSALLOWANCE", "VALIDDATE");
                            }

                            var YearInt = Convert.ToInt32(Item.RecieptDate.Year);
                            //if (2019 > YearInt)
                            //{
                            if (DateTime.Now.Year > YearInt)
                            {
                                var LastYearLimit = ShareDataManagement.LookupCache(Requestor.CompanyCode, "ESS.HR.BE", "HEALTHIANSALLOWANCE_LASTYEAR_LIMIT");
                                DateTime Limit = DateTime.ParseExact(LastYearLimit + "/" + DateTime.Now.Year + " 23:59:59", "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                                //DateTime Test = DateTime.ParseExact("06/01/" + DateTime.Now.Year+" 23:59:58", "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                                //if (Test > Limit)
                                //{
                                if (DateTime.Now > Limit)
                                {
                                    throw new DataServiceException("BE_HEALTHIANSALLOWANCE", "LIMITDATE");
                                }
                            }
                            else if (DateTime.Now.Year < YearInt)
                            {
                                throw new DataServiceException("BE_HEALTHIANSALLOWANCE", "INVALIDYEAR");
                            }


                            if (YearInt != Item.RecieptDate.Year)
                            {

                                throw new DataServiceException("BE_HEALTHIANSALLOWANCE", "INVALIDYEARITEM");
                            }
                        }
                    }


                    //เช็คการเบิกซ้ำจากเลขที่ใบเสร็จ + วันที่
                    if (!string.IsNullOrEmpty(Item.ReceiptNumber))
                    {
                        var Dupplicate = HRBEManagement.CreateInstance(Requestor.CompanyCode).HealthInsAllowanceCheckDupplicate(Requestor.EmployeeID, Item.ReceiptNumber, Item.RecieptDate, RequestNo);
                        if (Dupplicate)
                        {
                            throw new DataServiceException("BE_HEALTHIANSALLOWANCE", "DUPPLICATE_DATA");
                        }

                        var Count_Item = 0;
                        foreach (var Item_Check in oBEHealthInsAllowanceOption.BEHealthInsAllowanceItemData)
                        {
                            if (Item.ReceiptNumber == Item_Check.ReceiptNumber && Item.RecieptDate == Item_Check.RecieptDate)
                            {
                                Count_Item++;
                            }

                            ////ปีในวันที่ใบเสร็จไม่ตรงกัน
                            if (!string.IsNullOrEmpty(Item_Check.ReceiptNumber))
                            {
                                if (Item.RecieptDate.Year != Item_Check.RecieptDate.Year)
                                {
                                    throw new DataServiceException("BE_HEALTHIANSALLOWANCE", "INVALIDYEARITEM");
                                }
                            }


                        }

                        if (Count_Item > 1)
                        {
                            throw new DataServiceException("BE_HEALTHIANSALLOWANCE", "DUPPLICATE_DATA");
                        }

                        ///วันที่ใบเสร็จน้อยกว่าวันที่บรรจุ
                        EmployeeData oEmpData = new EmployeeData(Requestor.EmployeeID, Requestor.PositionID, Requestor.CompanyCode, DateTime.Now);
                        //DateTime Test = DateTime.ParseExact("06/01/2019" + " 23:59:58", "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                        //oEmpData.DateSpecific.PassProbation = Test;
                        if (oEmpData.DateSpecific.PassProbation > Item.RecieptDate)
                        {
                            throw new DataServiceException("BE_HEALTHIANSALLOWANCE", "OVER_PASSPROBATION");
                        }

                        ///วันที่ใบเสร็จ หลังวันเกษียณ
                        if (Item.RecieptDate > oEmpData.DateSpecific.RetirementDate)
                        {
                            throw new DataServiceException("BE_HEALTHIANSALLOWANCE", "OVER_RETIREMENTDATE");
                        }


                    }


                    }
            }

            if (CountItem == 0)
            {
                throw new DataServiceException("BE_HEALTHIANSALLOWANCE", "ITEM_CANTBE_NULL");
            }

            if (oBEHealthInsAllowanceOption.BEHealthInsAllowanceData.Attachments_num == 0)
            {

                throw new DataServiceException("BE_HEALTHIANSALLOWANCE", "ATTACHMENTS_NULL");
            }

            if (oBEHealthInsAllowanceOption.BEHealthInsAllowanceData.Attachments_num < 2)
            {

                throw new DataServiceException("BE_HEALTHIANSALLOWANCE", "ATTACHMENTS_TWO_NULL");
            }
        }

        public override void CalculateInfoData(EmployeeData Requestor, object Data, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
           
        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            BEHealthInsAllowanceOption oBEHealthInsAllowance = JsonConvert.DeserializeObject<BEHealthInsAllowanceOption>(Data.ToString());
            HRBEManagement oHRBEManagement = HRBEManagement.CreateInstance(Requestor.CompanyCode);

            oBEHealthInsAllowance.BEHealthInsAllowanceData.Status = State;
            oBEHealthInsAllowance.BEHealthInsAllowanceData.RequestNo = RequestNo;
            oBEHealthInsAllowance.BEHealthInsAllowanceData.EmployeeID = Requestor.EmployeeID;
            // Get ข้อมูลครอบครัวที่กดเลือก
            var FamilyNoSelect = oBEHealthInsAllowance.BEHealthInsAllowanceData.RelationshipType.ToString();
            var oFamilyNoSelect = oBEHealthInsAllowance.FamilyData.First(s => s.FamilyMember == FamilyNoSelect);

            DateTime BirthDate = oBEHealthInsAllowance.FamilyData[0].BirthDate;
            oBEHealthInsAllowance.BEHealthInsAllowanceData.FirstName = oFamilyNoSelect.Name;
            oBEHealthInsAllowance.BEHealthInsAllowanceData.LastName = oFamilyNoSelect.Surname;

            oBEHealthInsAllowance.BEHealthInsAllowanceData.BirthDate = BirthDate;

            // Get ข้อมูลจำนวนเงินที่เบิกก่อนหน้า
            var ItemYear = "";
            foreach (var Item_Check in oBEHealthInsAllowance.BEHealthInsAllowanceItemData)
            {
                if (!string.IsNullOrEmpty(Item_Check.ReceiptNumber))
                {
                    ItemYear = Item_Check.RecieptDate.Year.ToString();
                }
            }

            oBEHealthInsAllowance.BEHealthInsAllowanceData.Year = ItemYear;

            var QuotaUsed = HRBEManagement.CreateInstance(Requestor.CompanyCode).BEHealthInsAllowanceCheckQuota(Requestor.EmployeeID, oBEHealthInsAllowance.BEHealthInsAllowanceData.Year, oBEHealthInsAllowance.BEHealthInsAllowanceData.RelationshipType, oBEHealthInsAllowance.BEHealthInsAllowanceData.RequestNo);
            var oRelationshipTypeSelect = oBEHealthInsAllowance.BEHealthInsAllowanceConfigData.First(s => s.RelationshipType == oBEHealthInsAllowance.BEHealthInsAllowanceData.RelationshipType);
            oBEHealthInsAllowance.BEHealthInsAllowanceData.HealthInsConfigID = oRelationshipTypeSelect.HealthInsConfigID;
            var Quota = oRelationshipTypeSelect.Quota;

            // เช็คจำนวนเงินที่เบิกเกินกำหนด
            var QuotaNew = Quota - QuotaUsed;
            if (QuotaNew <= 0)
            {
                object[] objs = new object[1] { Quota };
                throw new DataServiceException("BE_HEALTHIANSALLOWANCE", "OVER_QUOTA", objs);
            }
            else
            {
                //ยอดเงินตามที่สามารถเบิกได้จริงตามโควต้า
                Quota = Quota - QuotaUsed;
                var AmountReimburse = Quota - oBEHealthInsAllowance.BEHealthInsAllowanceData.TotalAmount;
                if (AmountReimburse < 0)
                {
                    oBEHealthInsAllowance.BEHealthInsAllowanceData.PaymentAmount = Quota;
                }
                else
                {
                    oBEHealthInsAllowance.BEHealthInsAllowanceData.PaymentAmount = oBEHealthInsAllowance.BEHealthInsAllowanceData.TotalAmount;
                }
            }


            oHRBEManagement.SaveBEHealthInsAllowance(oBEHealthInsAllowance.BEHealthInsAllowanceData);
            oHRBEManagement.DeleteBEHealthInsAllowanceItem(RequestNo);
            foreach (var Item in oBEHealthInsAllowance.BEHealthInsAllowanceItemData)
            {
                if (Item!=null) {
                    if (!string.IsNullOrEmpty(Item.VendorName))
                    {
                        if (Item.VendorName != "")
                        {
                            Item.RequestNo = RequestNo;
                            oHRBEManagement.SaveBEHealthInsAllowanceItem(Item);
                        }
                    }
                }
            }

            DataRow dr = Info.NewRow();
            dr["BENEFITCODE"] = "";
            dr["DOCUMENTNO"] = oBEHealthInsAllowance.BEHealthInsAllowanceData.RequestNo;
            dr["REIMBURSEAMOUNT"] = oBEHealthInsAllowance.BEHealthInsAllowanceData.PaymentAmount;
            Info.Rows.Add(dr);


            Data = oBEHealthInsAllowance;

        }

        public override void ValidateDataInAction(object newData, DataTable Info, EmployeeData Requestor, string RequestNo, string State, string Action, bool HaveComment, bool HaveComment2, DateTime SubmitDate)
        {
            if (Action != "CANCEL")
            {
                if (DateTime.Now.Year != SubmitDate.Year)
                {
                    throw new DataServiceException("HRTM_EXCEPTION", "TIMEOUT_PERIOD_SETTING");
                }
            }
        }
    }
}
