﻿using ESS.DATA.ABSTRACT;
using ESS.EMPLOYEE;
using ESS.HR.BE.DATACLASS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Newtonsoft.Json;
using ESS.HR.PA.INFOTYPE;
using ESS.HR.PA;
using ESS.SHAREDATASERVICE;
using ESS.DATA.EXCEPTION;
using System.Globalization;

namespace ESS.HR.BE.DATASERVICE
{
    class CremationService : AbstractDataService
    {
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            BEBereavementOption oBEBereavementOption = new BEBereavementOption();

            //Get เวลาปัจจุบัน
            oBEBereavementOption.BEBereavementInfo.BereavementDate = DateTime.Now;
            oBEBereavementOption.BEBereavementInfo.FuneralDate = DateTime.Now;
            oBEBereavementOption.BEBereavementInfo.FuneralBeginDate = DateTime.Now;
            oBEBereavementOption.BEBereavementInfo.FuneralEndDate = DateTime.Now;
            oBEBereavementOption.BEBereavementInfo.CremationDate = DateTime.Now;
            oBEBereavementOption.BEBereavementInfo.PresidentDate = DateTime.Now;

            //Get เงินเดือนพนักงาน
            List<INFOTYPE0008> oSalaryList = HRPAManagement.CreateInstance(Requestor.CompanyCode).GetSalaryList(Requestor.EmployeeID, DateTime.Now);
            if (oSalaryList.Count > 0)
            {
                oBEBereavementOption.BEBereavementInfo.Salary = oSalaryList[0].Salary;
            }

           

            //Get Config
            oBEBereavementOption.BEBereavementConfigData = HRBEManagement.CreateInstance(Requestor.CompanyCode).GetBereavementConfig();
            var sub_type = "";
            foreach (BEBereavementConfig item in oBEBereavementOption.BEBereavementConfigData.Where(item => item.BereavementType == 1&& item.FamilyType!=0))
            {
                
                    sub_type += item.FamilyType + ",";
              
            }
            sub_type = sub_type.Remove(sub_type.Length - 1);

            //Get Family Data
            oBEBereavementOption.FamilyData = HRBEManagement.CreateInstance(Requestor.CompanyCode).GetFamilyData(Requestor.EmployeeID, sub_type);
            INFOTYPE0002 oPersonal = HRPAManagement.CreateInstance(Requestor.CompanyCode).GetPersonalData(Requestor.EmployeeID, DateTime.Now);
            ESS.HR.BE.INFOTYPE.INFOTYPE0021 IF21 = new ESS.HR.BE.INFOTYPE.INFOTYPE0021();
            IF21.Name = oPersonal.FirstName;
            IF21.Surname = oPersonal.LastName;
            IF21.ChildNo = "";
            IF21.FamilyMember ="0";
            oBEBereavementOption.FamilyData.Add(IF21);

            if(oBEBereavementOption.FamilyData.Count > 0)
            {
                int key = 0;
                foreach(var item in oBEBereavementOption.FamilyData)
                {
                    key++;
                    item.UniqueBereavementId = key;
                }
            }
            

            //Get Role Admin
            oBEBereavementOption.BEBereavementInfo.RoleAdminName = ShareDataManagement.LookupCache(Requestor.CompanyCode, "ESS.HR.BE", "ADMIN");

            return oBEBereavementOption;
        }

        public override void PrepareData(EmployeeData Requestor, object Data)
        {
        
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            BEBereavementOption oBEBereavementOption = JsonConvert.DeserializeObject<BEBereavementOption>(newData.ToString());

            //วันที่เสียชีวิต
            if (NoOfFileAttached<1)
            {
                throw new DataServiceException("BE_BEREAVEMENT", "FILE_CANTBE_NULL");
            }

            if (!string.IsNullOrEmpty(oBEBereavementOption.BEBereavementInfo.ReligionId))
            {
                int ReligionId_int = Int32.Parse(oBEBereavementOption.BEBereavementInfo.ReligionId);
                if(ReligionId_int <= 0)
                {
                    // ไม่เลือกศาสนา
                    throw new DataServiceException("BE_BEREAVEMENT", "Religion Not Found");
                }
            }
            else
            {
                // ไม่มีข้อมูลศาสนา
                throw new DataServiceException("BE_BEREAVEMENT", "Religion Not Found");
            }


            //วันที่เสียชีวิต
            if (oBEBereavementOption.BEBereavementInfo.BereavementDate == null)
            {
                throw new DataServiceException("BE_BEREAVEMENT", "BEREVEMENTDATE_CANTBE_NULL");
            }

            //ประสงค์ให้ออก PR
            if (oBEBereavementOption.BEBereavementInfo.IsAllowPR == 0)
            {
                throw new DataServiceException("BE_BEREAVEMENT", "ISALLOWPR_CANTBE_NULL");
            }


            ///วันที่ใบเสร็จน้อยกว่าวันที่เริ่มงาน
            EmployeeData oEmpData = new EmployeeData(Requestor.EmployeeID, Requestor.PositionID, Requestor.CompanyCode, DateTime.Now);
            if (oEmpData.DateSpecific.HiringDate > oBEBereavementOption.BEBereavementInfo.BereavementDate)
            {
                throw new DataServiceException("BE_BEREAVEMENT", "OVER_PASSPROBATION");
            }

            //พนักงานเบิกได้ไม่เกิน 90 วัน (นับจากวันที่เสียชีวิต) 
            var datelimit = Convert.ToInt32(ShareDataManagement.LookupCache(Requestor.CompanyCode, "ESS.HR.BE", "BEREAVEMENT_LIMIT"));
            //วันที่เสียชีวิตของปีที่แล้วสามารถเบิกได้ภายในวันที่ xx/xx ของปีถัดไปเท่านั้น

            if (!string.IsNullOrEmpty(oBEBereavementOption.BEBereavementInfo.BereavementDate.ToString()))
            {
                if (Convert.ToInt32(oBEBereavementOption.BEBereavementInfo.RoleAdmin) == 0)
                {
                    if (oBEBereavementOption.BEBereavementInfo.BereavementDate.AddDays(datelimit).Date <= DateTime.Now.Date)
                    {
                        throw new DataServiceException("BE_BEREAVEMENT", "VALIDETE_BEREAVEMENTDATE");
                    }

                    //เบิกล่วงหน้าไม่ได้
                    if (oBEBereavementOption.BEBereavementInfo.BereavementDate.Date > DateTime.Now.Date)
                    {

                        throw new DataServiceException("BE_BEREAVEMENT", "VALIDDATE");
                    }

                    var YearInt = Convert.ToInt32(oBEBereavementOption.BEBereavementInfo.BereavementDate.ToString("yyyy"));
                    if (DateTime.Now.Year > YearInt)
                    {
                        var LastYearLimit = ShareDataManagement.LookupCache(Requestor.CompanyCode, "ESS.HR.BE", "BEREAVEMENT_LASTYEAR_LIMIT");
                       
                        DateTime Limit = DateTime.ParseExact(LastYearLimit + "/" + DateTime.Now.Year + " 23:59:59", "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                        if (DateTime.Now > Limit)
                        {
                            throw new DataServiceException("BE_BEREAVEMENT", "LIMITDATE");
                        }
                    }
                 }
            }

            //เช็คการเบิกซ้ำ
            var CountItem = 0;
            var CountItemEmployee = 0;
            foreach (var Item in oBEBereavementOption.BEBereavementItemData)
            {
                if (Item != null)
                {
                    ///เช็คว่ามีการเลือกผู้เสียชีวิต
                    if (Item.BereavementType != 999)
                    {
                        CountItem++;
                        var Dupplicate = HRBEManagement.CreateInstance(Requestor.CompanyCode).BEBereavementCheckDupplicate(Requestor.EmployeeID, Item.BereavementType, RequestNo);
                        if (Dupplicate)
                        {
                            throw new DataServiceException("BE_BEREAVEMENT", "DUPPLICATE_DATA");
                        }

                        ///เช็คว่ามีการเลือกผู้เสียชีวิตเป็น พนักงาน
                        if (Item.BereavementType == 0) {
                            CountItemEmployee++;
                        }
                    }
                }
            }

            if (CountItem == 0)
            {
                throw new DataServiceException("BE_BEREAVEMENT", "ITEM_CANTBE_NULL");
            }

            //Admin ต้องเลือก ผู้รับผิดชอบจัดงาน
            if (oBEBereavementOption.BEBereavementInfo.RoleAdmin == "1" && oBEBereavementOption.BEBereavementInfo.ResponsibleFuneral == 0&& CountItemEmployee>0)
            {
                throw new DataServiceException("BE_BEREAVEMENT", "RESPONSIBLEFUNERAL_CANTBE_NULL");
            }


        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            BEBereavementOption oBEBereavementOption = JsonConvert.DeserializeObject<BEBereavementOption>(Data.ToString());
            HRBEManagement oHRBEManagement = HRBEManagement.CreateInstance(Requestor.CompanyCode);

            oBEBereavementOption.BEBereavementInfo.Status = State;
            oBEBereavementOption.BEBereavementInfo.RequestNo = RequestNo;
            oBEBereavementOption.BEBereavementInfo.EmployeeID = Requestor.EmployeeID;

            // SaveBEBereavementInfo 
            oHRBEManagement.SaveBEBereavementInfo(oBEBereavementOption.BEBereavementInfo);

            oHRBEManagement.DeleteBEBereavementItem(RequestNo);

            List<ESS.HR.BE.INFOTYPE.INFOTYPE0021> FamilyList = new List<ESS.HR.BE.INFOTYPE.INFOTYPE0021>();

            foreach (var Item in oBEBereavementOption.BEBereavementItemData)
            {
                if (Item != null)
                {
                    if (Item.BereavementType != 999)
                    {
                        //ถ้า BereavementType คือ บุตร
                        if (Item.BereavementType > 200)
                        {
                            var ChildNo = Item.BereavementType.ToString().Substring(1, 2);
                            Item.BereavementType = 2;
                            var BereavementType = Item.BereavementType.ToString();
                            var ofamily = oBEBereavementOption.FamilyData.First(s => s.FamilyMember == BereavementType && s.ChildNo == ChildNo);
                            Item.FirstName = ofamily.Name;
                            Item.LastName = ofamily.Surname;
                            Item.ChildNo = ofamily.ChildNo;

                            ///set data for post sap
                            ofamily.BeginDate = oBEBereavementOption.BEBereavementInfo.BereavementDate;
                            ofamily.EndDate = new DateTime(9999, 12, 31);
                            ofamily.Dead = "X";
                            ofamily.EmployeeID = Requestor.EmployeeID;
                            FamilyList.Add(ofamily);

                        }
                        else {
                            var BereavementType = Item.BereavementType.ToString();
                            var ofamily = oBEBereavementOption.FamilyData.First(s => s.FamilyMember == BereavementType);
                            Item.FirstName = ofamily.Name;
                            Item.LastName = ofamily.Surname;

                            ///set data for post sap
                            ofamily.BeginDate = oBEBereavementOption.BEBereavementInfo.BereavementDate;
                            ofamily.EndDate = new DateTime(9999, 12, 31);
                            ofamily.Dead = "X";
                            ofamily.EmployeeID = Requestor.EmployeeID;

                            //ถ้าเป็นพนักงานต้องไม่ Post SAP
                            if (BereavementType != "0")
                            {
                                FamilyList.Add(ofamily);
                            }
                        }
                        Item.RequestNo = RequestNo;
                        oHRBEManagement.SaveBEBereavementItem(Item);
                       
                    }
                }
            }

            DataRow dr = Info.NewRow();
            dr["BENEFITCODE"] = "";
            dr["DOCUMENTNO"] = oBEBereavementOption.BEBereavementInfo.RequestNo;
            dr["REIMBURSEAMOUNT"] = oBEBereavementOption.BEBereavementInfo.TotalAmount;
            Info.Rows.Add(dr);

            //Save Data To SAP
            string dataCategory = "Bereavement";
            if (State == "COMPLETED")
            {
                try
                {
                    if (FamilyList.Count > 0)
                    {
                        HRBEManagement.CreateInstance(Requestor.CompanyCode).SaveFamilyData(FamilyList);
                    }
                }
                catch (Exception ex)
                {
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, true);
                    throw new SaveExternalDataException("Post data error", true, ex);
                }
            }





        }
        public override void ValidateDataInAction(object newData, DataTable Info, EmployeeData Requestor, string RequestNo, string State, string Action, bool HaveComment, bool HaveComment2, DateTime SubmitDate)
        {
            if (Action != "CANCEL")
            {
                if (DateTime.Now.Year != SubmitDate.Year)
                {
                    throw new DataServiceException("HRTM_EXCEPTION", "TIMEOUT_PERIOD_SETTING");
                }
            }
        }
    }
}
