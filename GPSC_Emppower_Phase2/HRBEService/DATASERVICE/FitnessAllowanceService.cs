﻿using ESS.DATA.ABSTRACT;
using ESS.EMPLOYEE;
using ESS.HR.BE.DATACLASS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Newtonsoft.Json;
using ESS.DATA.EXCEPTION;
using ESS.SHAREDATASERVICE;
using System.Globalization;

namespace ESS.HR.BE.DATASERVICE
{
    public class FitnessAllowanceService : AbstractDataService
    {
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {

            BEFitnessAllowanceOption oBEFitnessAllowance = new BEFitnessAllowanceOption();

            //Get Year
            var Year = CreateParam;
            if (!string.IsNullOrEmpty(CreateParam))
            {
                //set RecieptDate
                for (var i = 0; i < 10; i++)
                {
                    oBEFitnessAllowance.BEFitnessAllowanceItemData[i] = new BEFitnessAllowanceItem();

                    if (Year == DateTime.Now.Year.ToString())
                    {
                        oBEFitnessAllowance.BEFitnessAllowanceItemData[i].RecieptDate = DateTime.Now;
                    }
                    else
                    {
                        oBEFitnessAllowance.BEFitnessAllowanceItemData[i].RecieptDate = DateTime.ParseExact(Year + "/01/01", "yyyy/MM/dd", CultureInfo.InvariantCulture); ;
                    }
                }

                //get ข้อมูลปี
                oBEFitnessAllowance.YearData = HRBEManagement.CreateInstance(Requestor.CompanyCode).GetYears();
                //oBEFitnessAllowance.BEFitnessAllowanceData.Year = DateTime.Now.Year.ToString();


                //Get Role Admin
                oBEFitnessAllowance.BEFitnessAllowanceData.RoleAdminName = ShareDataManagement.LookupCache(Requestor.CompanyCode, "ESS.HR.BE", "ADMIN");
            }
            return oBEFitnessAllowance;
        }

        public override void PrepareData(EmployeeData Requestor, object Data)
        {

        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            BEFitnessAllowanceOption oBEFitnessAllowanceOption = JsonConvert.DeserializeObject<BEFitnessAllowanceOption>(newData.ToString());

            //if (string.IsNullOrEmpty(oBEFitnessAllowanceOption.BEFitnessAllowanceData.Year))
            //{
            //    throw new DataServiceException("BE_FITNESSALLOWANCE", "YEAR_CANTBE_NULL");
            //}

            //มีการเลือกสวัสดิการอื่นแทน Fitness
            var Enrollment = HRBEManagement.CreateInstance(Requestor.CompanyCode).BEFitnessAllowanceCheckEnrollment(Requestor.EmployeeID, oBEFitnessAllowanceOption.BEFitnessAllowanceData.Year);
            if (Enrollment == 1) {
                throw new DataServiceException("BE_FITNESSALLOWANCE", "CHECKENROLLMENT");
            }

            var CountItem = 0;
            foreach (var Item in oBEFitnessAllowanceOption.BEFitnessAllowanceItemData)
            {
                if (Item != null)
                {
                    if (!string.IsNullOrEmpty(Item.VendorName))
                    {
                        CountItem++;
                        if (string.IsNullOrEmpty(Item.ReceiptNumber))
                        {
                            throw new DataServiceException("BE_FITNESSALLOWANCE", "RECEIPTNUMBER_CANTBE_NULL");
                        }

                        if (Item.RecieptDate == null)
                        {
                            throw new DataServiceException("BE_FITNESSALLOWANCE", "RECIEPTDATE_CANTBE_NULL");
                        }

                        if (Item.AmountExcludeVAT == 0)
                        {
                            throw new DataServiceException("BE_FITNESSALLOWANCE", "AMOUNTEXCLUDEVAT_CANTBE_NULL");
                        }

                        //if (Item.TotalAmount == 0)
                        //{
                        //    throw new DataServiceException("BE_FITNESSALLOWANCE", "TOTALAMOUNT_CANTBE_NULL");
                        //}
                    }

                    if (!string.IsNullOrEmpty(Item.ReceiptNumber))
                    {
                        CountItem++;
                        if (string.IsNullOrEmpty(Item.VendorName))
                        {
                            throw new DataServiceException("BE_FITNESSALLOWANCE", "VENDORNAME_CANTBE_NULL");
                        }

                        if (Item.RecieptDate == null)
                        {
                            throw new DataServiceException("BE_FITNESSALLOWANCE", "RECIEPTDATE_CANTBE_NULL");
                        }

                        if (Item.AmountExcludeVAT == 0)
                        {
                            throw new DataServiceException("BE_FITNESSALLOWANCE", "AMOUNTEXCLUDEVAT_CANTBE_NULL");
                        }

                        //if (Item.TotalAmount == 0)
                        //{
                        //    throw new DataServiceException("BE_FITNESSALLOWANCE", "TOTALAMOUNT_CANTBE_NULL");
                        //}
                    }


                    if (Item.TotalAmount != 0)
                    {
                        CountItem++;
                        if (string.IsNullOrEmpty(Item.ReceiptNumber))
                        {
                            throw new DataServiceException("BE_FITNESSALLOWANCE", "RECEIPTNUMBER_CANTBE_NULL");
                        }

                        if (string.IsNullOrEmpty(Item.ReceiptNumber))
                        {
                            throw new DataServiceException("BE_FITNESSALLOWANCE", "RECEIPTNUMBER_CANTBE_NULL");
                        }

                        if (Item.RecieptDate == null)
                        {
                            throw new DataServiceException("BE_FITNESSALLOWANCE", "RECIEPTDATE_CANTBE_NULL");
                        }

                        if (string.IsNullOrEmpty(Item.VendorName))
                        {
                            throw new DataServiceException("BE_FITNESSALLOWANCE", "VENDORNAME_CANTBE_NULL");
                        }
                    }


                    //เบิกผ่านระบบภายใน 90 วัน
                    var datelimit = Convert.ToInt32(ShareDataManagement.LookupCache(Requestor.CompanyCode, "ESS.HR.BE", "FITNESSALLOWANCE_LIMIT"));
                    if (!string.IsNullOrEmpty(Item.ReceiptNumber))
                    {
                        //ไม่ได้เป็น Admin จะต้องเช็ควันที่เบิก
                        if (Convert.ToInt32(oBEFitnessAllowanceOption.BEFitnessAllowanceData.RoleAdmin) == 0)
                        {

                            if (Item.RecieptDate.AddDays(datelimit).Date <= DateTime.Now.Date)
                            {
                                throw new DataServiceException("BE_FITNESSALLOWANCE", "VALIDETE_DATE90");
                            }

                            //เบิกล่วงหน้าไม่ได้
                            if (Item.RecieptDate.Date > DateTime.Now.Date)
                            {

                                throw new DataServiceException("BE_FITNESSALLOWANCE", "VALIDDATE");
                            }

                            var YearInt = Convert.ToInt32(Item.RecieptDate.Year);

                            ////ปีในใบเสร็จตรงกับปีที่เลือก
                            //if (YearInt != Item.RecieptDate.Year)
                            //{

                            //    throw new DataServiceException("BE_FITNESSALLOWANCE", "INVALIDYEARITEM");
                            //}


                            if (DateTime.Now.Year > YearInt)
                            {
                                var LastYearLimit = ShareDataManagement.LookupCache(Requestor.CompanyCode, "ESS.HR.BE", "FITNESSALLOWANCE_LASTYEAR_LIMIT");
                                DateTime Limit = DateTime.ParseExact(LastYearLimit + "/" + DateTime.Now.Year + " 23:59:59", "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                                if (DateTime.Now > Limit)
                                {
                                    throw new DataServiceException("BE_FITNESSALLOWANCE", "LIMITDATE");
                                }
                            }

                        }
                    }



                    //เช็คการเบิกซ้ำจากเลขที่ใบเสร็จ + วันที่
                    if (!string.IsNullOrEmpty(Item.ReceiptNumber))
                    {
                        var Dupplicate = HRBEManagement.CreateInstance(Requestor.CompanyCode).FitnessAllowanceCheckDupplicate(Requestor.EmployeeID, Item.ReceiptNumber, Item.RecieptDate, RequestNo);
                        if (Dupplicate)
                        {
                            throw new DataServiceException("BE_FITNESSALLOWANCE", "DUPPLICATE_DATA");
                        }

                        var Count_Item = 0;
                        foreach (var Item_Check in oBEFitnessAllowanceOption.BEFitnessAllowanceItemData)
                        {
                            if (Item.ReceiptNumber == Item_Check.ReceiptNumber && Item.RecieptDate == Item_Check.RecieptDate)
                            {
                                Count_Item++;
                            }

                            ////ปีในวันที่ใบเสร็จไม่ตรงกัน
                            if (!string.IsNullOrEmpty(Item_Check.ReceiptNumber))
                            {
                                if (Item.RecieptDate.Year != Item_Check.RecieptDate.Year)
                                {
                                    throw new DataServiceException("BE_FITNESSALLOWANCE", "INVALIDYEARITEM");
                                }
                            }


                        }

                        if (Count_Item > 1)
                        {
                            throw new DataServiceException("BE_FITNESSALLOWANCE", "DUPPLICATE_DATA");
                        }

                        ///วันที่ใบเสร็จน้อยกว่าวันที่บรรจุ | พนักงานทดลองงานสามารถเบิกได้
                        EmployeeData oEmpData = new EmployeeData(Requestor.EmployeeID, Requestor.PositionID, Requestor.CompanyCode, DateTime.Now);
                        var Trainee = HRBEManagement.CreateInstance(Requestor.CompanyCode).EmployeeTrainee(oEmpData.EmpGroup, oEmpData.DateSpecific.PassProbation);
                        if (oEmpData.DateSpecific.PassProbation > Item.RecieptDate|| Trainee)
                        {
                            throw new DataServiceException("BE_FITNESSALLOWANCE", "OVER_PASSPROBATION");
                        }

                        ///วันที่ใบเสร็จ หลังวันเกษียณ
                        if (Item.RecieptDate > oEmpData.DateSpecific.RetirementDate)
                        {
                            throw new DataServiceException("BE_FITNESSALLOWANCE", "OVER_RETIREMENTDATE");
                        }

                    }
                }
            }

            if (CountItem == 0)
            {
                throw new DataServiceException("BE_FITNESSALLOWANCE", "ITEM_CANTBE_NULL");
            }

            if (oBEFitnessAllowanceOption.BEFitnessAllowanceData.Attachments_num == 0)
            {

                throw new DataServiceException("BE_FITNESSALLOWANCE", "ATTACHMENTS_NULL");
            }

        }

        public override void CalculateInfoData(EmployeeData Requestor, object Data, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
           
           
        }


        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            BEFitnessAllowanceOption oFitnessAllowance = JsonConvert.DeserializeObject<BEFitnessAllowanceOption>(Data.ToString());
            HRBEManagement oHRBEManagement = HRBEManagement.CreateInstance(Requestor.CompanyCode);

            oFitnessAllowance.BEFitnessAllowanceData.Status = State;
            oFitnessAllowance.BEFitnessAllowanceData.RequestNo = RequestNo;
            oFitnessAllowance.BEFitnessAllowanceData.EmployeeID = Requestor.EmployeeID;



            //get ข้อมูล Config
            var ItemYear = "";
            foreach (var Item_Check in oFitnessAllowance.BEFitnessAllowanceItemData)
            {
                if (!string.IsNullOrEmpty(Item_Check.ReceiptNumber))
                {
                    ItemYear = Item_Check.RecieptDate.Year.ToString();
                }
            }
            oFitnessAllowance.BEFitnessAllowanceData.Year = ItemYear;


            // Get ข้อมูลจำนวนเงินที่เบิกก่อนหน้า
            var QuotaUsed = HRBEManagement.CreateInstance(Requestor.CompanyCode).BEFitnessAllowanceCheckQuota(Requestor.EmployeeID, oFitnessAllowance.BEFitnessAllowanceData.Year, oFitnessAllowance.BEFitnessAllowanceData.RequestNo);


            EmployeeData oEmpData = new EmployeeData(Requestor.EmployeeID, Requestor.PositionID, Requestor.CompanyCode, DateTime.Now);
            var oConfig = HRBEManagement.CreateInstance(Requestor.CompanyCode).GetBEFitnessAllowanceConfig(Requestor.EmployeeID, oEmpData.DateSpecific.PassProbation, ItemYear);
            oFitnessAllowance.BEFitnessAllowanceConfigData = oConfig[0];

            var Quota = oFitnessAllowance.BEFitnessAllowanceConfigData.Quota;
            oFitnessAllowance.BEFitnessAllowanceData.FitnessConfigID = oFitnessAllowance.BEFitnessAllowanceConfigData.FitnessConfigID;


            // เช็คจำนวนเงินที่เบิกเกินกำหนด
            var QuotaNew = Quota - QuotaUsed;
            if (QuotaNew <= 0)
            {
                object[] objs = new object[1] { Quota };
                throw new DataServiceException("BE_FITNESSALLOWANCE", "OVER_QUOTA", objs);
            }
            else
            {
                //ยอดเงินตามที่สามารถเบิกได้จริงตามโควต้า
                Quota = Quota - QuotaUsed;
                var AmountReimburse = Quota - oFitnessAllowance.BEFitnessAllowanceData.TotalAmount;
                if (AmountReimburse < 0)
                {
                    oFitnessAllowance.BEFitnessAllowanceData.PaymentAmount = Quota;
                }
                else
                {
                    oFitnessAllowance.BEFitnessAllowanceData.PaymentAmount = oFitnessAllowance.BEFitnessAllowanceData.TotalAmount;
                }
            }

            oHRBEManagement.SaveBEFitnessAllowance(oFitnessAllowance.BEFitnessAllowanceData);
            oHRBEManagement.DeleteBEFitnessAllowanceItem(RequestNo);
            foreach (var Item in oFitnessAllowance.BEFitnessAllowanceItemData)
            {
                if (Item != null)
                {
                    if (Item.VendorName != null)
                    {
                        Item.RequestNo = RequestNo;
                        oHRBEManagement.SaveBEFitnessAllowanceItem(Item);
                    }
                }
            }

            DataRow dr = Info.NewRow();
            dr["BENEFITCODE"] = "";
            dr["DOCUMENTNO"] = oFitnessAllowance.BEFitnessAllowanceData.RequestNo;
            dr["REIMBURSEAMOUNT"] = oFitnessAllowance.BEFitnessAllowanceData.PaymentAmount;
            Info.Rows.Add(dr);

        }
        public override void ValidateDataInAction(object newData, DataTable Info, EmployeeData Requestor, string RequestNo, string State, string Action, bool HaveComment, bool HaveComment2, DateTime SubmitDate)
        {
            if (Action != "CANCEL")
            {
                if (DateTime.Now.Year != SubmitDate.Year)
                {
                    throw new DataServiceException("HRTM_EXCEPTION", "TIMEOUT_PERIOD_SETTING");
                }
            }
        }
    }
}
