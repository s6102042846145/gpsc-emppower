﻿using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE;
using ESS.HR.BE.DATASERVICE;
using ESS.HR.BE.DATACLASS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXPORT;
using Microsoft.Reporting.WebForms;
using ESS.UTILITY.FILE;

namespace ESS.HR.BE.DATASERVICE
{
    class PENTService : AbstractDataService
    {
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            TrainingForDev oTrainingForDevpaint = new TrainingForDev();
            oTrainingForDevpaint.EmployeeID = Requestor.EmployeeID;
            return oTrainingForDevpaint;
        }

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info)
        {
            return Info.Rows[0]["WorkflowType"].ToString();
        }

        public override void PrepareData(EmployeeData Requestor, object Data)
        {
      
        }
        public override void CalculateInfoData(EmployeeData Requestor, object Data, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
            TrainingForDev oTrainingForDevpaint = JsonConvert.DeserializeObject<TrainingForDev>(Data.ToString());
            DataRow dr = Info.NewRow();
            dr["BeginDate"] = oTrainingForDevpaint.BeginDate;
            dr["EndDate"] = oTrainingForDevpaint.EndDate;
            dr["TotalAmount"] = oTrainingForDevpaint.TotalAmount;
            dr["WorkflowType"] = oTrainingForDevpaint.WorkflowType;
            Info.Rows.Add(dr);
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
           
            TrainingForDev oTrainingForDevpaint = JsonConvert.DeserializeObject<TrainingForDev>(newData.ToString());
            if (oTrainingForDevpaint.TotalAmount <= 0)
            {
                object[] objs = new object[1] { oTrainingForDevpaint.TotalAmount.ToString() };
                throw new DataServiceException("BE", "Totalamount_cantbe_null", objs);
            }

        }

        public override void ValidateDataInAction(object newData, DataTable Info, EmployeeData Requestor, string RequestNo, string State, string Action, bool HaveComment, bool HaveComment2, DateTime SubmitDate)
        {
            //if(!HaveComment)
            //    throw new RequireCommentException("BE", "REQUIRED_COMMENT");
        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {

            TrainingForDev oTrainingForDev = JsonConvert.DeserializeObject<TrainingForDev>(Data.ToString());
            HRBEManagement oHRBEManagement = HRBEManagement.CreateInstance(Requestor.CompanyCode);
            oTrainingForDev.RequestNo = RequestNo;
            oTrainingForDev.Status = State;
            oHRBEManagement.SaveBETraining(oTrainingForDev);

        }

        //public override void PostProcess(EmployeeData Requestor, DataTable Info, object Data, string PreviousState, string State, string RequestNo, string Comment, string ActionCode)
        //{
        //    ReportObject oReportObject = new ReportObject();
        //    oReportObject.ExportType = "PDF";
        //    oReportObject.ReportName = "Views/Report/HR/BEReport.rdlc";
        //    oReportObject.ExportFileName = string.Format("{0}\\{1}", @"D:\Project\GPSC\SourceCode\ESSClient\iSSWS\Client\files", "BEReport");
        //    DataSet ds = HRBEManagement.CreateInstance(Requestor.CompanyCode).GetReportData(RequestNo);
        //    oReportObject.ReportParameters = new List<ReportParameter>()
        //        {
        //             new ReportParameter("MyParam","HelloWorld Report!!")
        //    };
        //    oReportObject.DataSource = ds;

        //    ReportViewer oReportViewer = new ReportViewer();
        //    oReportViewer.LocalReport.DataSources.Clear();
        //    oReportViewer.ProcessingMode = ProcessingMode.Local;
        //    oReportViewer.LocalReport.ReportPath = oReportObject.ReportName;
        //    oReportViewer.LocalReport.DataSources.Add(new ReportDataSource("ExampleData", ds.Tables[0]));

        //    if (oReportObject.ReportParameters != null && oReportObject.ReportParameters.Count > 0)
        //    {
        //        oReportViewer.LocalReport.SetParameters(oReportObject.ReportParameters);
        //    }

        //    oReportViewer.ShowRefreshButton = false;
        //    ReportPageSettings oReportPageSettings = oReportViewer.LocalReport.GetDefaultPageSettings();
        //    Warning[] warnings;
        //    string[] streamids;
        //    string mimeType;
        //    string encoding;
        //    string filenameExtension;

        //    byte[] bytes = oReportViewer.LocalReport.Render(
        //        oReportObject.ExportType, null, out mimeType, out encoding, out filenameExtension,
        //        out streamids, out warnings);
        //    oReportObject.Warnings = warnings;
        //    oReportObject.Streamids = streamids;
        //    oReportObject.ContentType = mimeType;
        //    oReportObject.Encoding = encoding;
        //    oReportObject.FilenameExtension = filenameExtension;
        //    oReportObject.ExportFileName = string.Format("{0}.{1}", oReportObject.ExportFileName, filenameExtension);
        //    FileManager.SaveFile(oReportObject.ExportFileName, bytes, true);

        //    //ReportObject oReportObject = new ReportObject();
        //    //ReportingServices oReportingServices = new ReportingServices();
        //    //oReportObject.ExportType = "PDF";
        //    //oReportObject.ReportName = "Views/Report/HR/BEReport.rdlc";
        //    //oReportObject.ExportFileName = string.Format("{0}\\{1}", @"D:\Project\GPSC\SourceCode\ESSClient\iSSWS\Client\files", "BEReport");
        //    //DataSet ds = HRBEManagement.CreateInstance(Requestor.CompanyCode).GetReportData(RequestNo);
        //    //oReportObject.ReportParameters = new List<ReportParameter>()
        //    //    {
        //    //         new ReportParameter("MyParam","HelloWorld Report!!")
        //    //};
        //    //oReportObject.DataSource = ds;
        //    //oReportingServices.ExportToFile(oReportObject);
        //}

    }
}
