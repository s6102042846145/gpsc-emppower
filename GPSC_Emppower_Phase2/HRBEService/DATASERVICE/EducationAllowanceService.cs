﻿using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE;
using ESS.HR.BE.DATACLASS;
using ESS.HR.BE.INFOTYPE;
using ESS.SHAREDATASERVICE;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.BE.DATASERVICE
{
    class EducationAllowanceService : AbstractDataService
    {
        public override object GenerateAdditionalData(ESS.EMPLOYEE.EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            var Year = CreateParam;
            BEEducationAllowanceOption oBEEducationAllowance = new BEEducationAllowanceOption();
            //Get ข้อมูลบุตร
            oBEEducationAllowance.ChildData = HRBEManagement.CreateInstance(Requestor.CompanyCode).GetChildsData(Requestor.EmployeeID);
            //Get Config
            oBEEducationAllowance.EducationLevelData = HRBEManagement.CreateInstance(Requestor.CompanyCode).GetBEConfig(Year);
            //Get เวลาปัจจุบัน
            if (!string.IsNullOrEmpty(CreateParam))
            {
                if (Year == DateTime.Now.Year.ToString())
                {
                    oBEEducationAllowance.EducationAllowanceData.PayDate = DateTime.Now;
                }
                else
                {
                    oBEEducationAllowance.EducationAllowanceData.PayDate = DateTime.ParseExact(Year + "/01/01", "yyyy/MM/dd", CultureInfo.InvariantCulture); ;
                }
            }

            //Get Role Admin
            oBEEducationAllowance.EducationAllowanceData.RoleAdminName = ShareDataManagement.LookupCache(Requestor.CompanyCode, "ESS.HR.BE", "ADMIN");

            return oBEEducationAllowance;
        }

        public override void PrepareData(ESS.EMPLOYEE.EmployeeData Requestor, object Data)
        {


        }

        public override void ValidateData(object newData, ESS.EMPLOYEE.EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {

            BEEducationAllowanceOption oBEEducationAllowanceOption = JsonConvert.DeserializeObject<BEEducationAllowanceOption>(newData.ToString());
            BEEducationAllowance oBEEducationAllowance = oBEEducationAllowanceOption.EducationAllowanceData;


            if (oBEEducationAllowance.WithdrawType == 1)
            {
                oBEEducationAllowance.Amount_String = "AUTOQUOTA";
            }

            if (oBEEducationAllowance.ChildNo == null)
            {

                throw new DataServiceException("BE_EDUCATIONALLOWANCE", "CHILODNO_CANTBE_NULL");
            }

            if (string.IsNullOrEmpty(oBEEducationAllowance.EducationLevel_String))
            {
                throw new DataServiceException("BE_EDUCATIONALLOWANCE", "EDUCATIONLEVEL_CANTBE_NULL");
            }

            if (string.IsNullOrEmpty(oBEEducationAllowance.EducationClass))
            {
                throw new DataServiceException("BE_EDUCATIONALLOWANCE", "EDUCATIONCLASS_CANTBE_NULL");
            }

            if (string.IsNullOrEmpty(oBEEducationAllowance.EducationYear))
            {
                throw new DataServiceException("BE_EDUCATIONALLOWANCE", "EDUCATIONYEAR_CANTBE_NULL");
            }

            if (Convert.ToInt32(oBEEducationAllowance.EducationYear) <= 0)
            {
                throw new DataServiceException("BE_EDUCATIONALLOWANCE", "EDUCATIONYEARNUM_CANTBE_NULL");
            }

            if (string.IsNullOrEmpty(oBEEducationAllowance.Academy))
            {
                throw new DataServiceException("BE_EDUCATIONALLOWANCE", "ACADEMY_CANTBE_NULL");
            }

            if (oBEEducationAllowance.AcademyType == 0)
            {
                throw new DataServiceException("BE_EDUCATIONALLOWANCE", "ACADEMYTYPE_CANTBE_NULL");
            }

            if (oBEEducationAllowance.WithdrawType == 0)
            {
                throw new DataServiceException("BE_EDUCATIONALLOWANCE", "WITHDRAWTYPE_CANTBE_NULL");
            }

            if (string.IsNullOrEmpty(oBEEducationAllowance.Amount_String))
            {
                throw new DataServiceException("BE_EDUCATIONALLOWANCE", "AMOUNT_CANTBE_NULL");
            }

            if (oBEEducationAllowance.PayDate == null)
            {
                throw new DataServiceException("BE_EDUCATIONALLOWANCE", "PAYDATE_CANTBE_NULL");
            }

            //เบิกค่่าเล่าเรียนตามจริง ไม่เช็คการแนบไฟล์แนบ
            if (oBEEducationAllowance.WithdrawType == 1) {
                if (NoOfFileAttached < 1)
                {
                    throw new DataServiceException("BE_EDUCATIONALLOWANCE", "FILE_CANTBE_NULL");
                }
            }

            //ถ้าเลือกประเภทการเบิกเป็น ตามจริง จะต้องกรอก เลขที่ใบเสร็จ
            if (oBEEducationAllowance.WithdrawType == 2 && string.IsNullOrEmpty(oBEEducationAllowance.ReceiptNumber))
            {
                throw new DataServiceException("BE_EDUCATIONALLOWANCE", "RECEIPTNUMBER_CANTBE_NULL");
            }

            //พนักงานเบิกได้ไม่เกิน 180 วัน (นับจากวันที่ใบเสร็จ) 
            var datelimit = Convert.ToInt32(ShareDataManagement.LookupCache(Requestor.CompanyCode, "ESS.HR.BE", "EDUCATIONALLOWANCE_LIMIT"));
            //ใบเสร็จของปีที่แล้วสามารถเบิกได้ภายในวันที่ xx/xx ของปีถัดไปเท่านั้น
            var datelimit_lastyear = ShareDataManagement.LookupCache(Requestor.CompanyCode, "ESS.HR.BE", "HEALTHIANSALLOWANCE_LASTYEAR_LIMIT");
            if (!string.IsNullOrEmpty(oBEEducationAllowance.PayDate.ToString()))
            {
                if (Convert.ToInt32(oBEEducationAllowance.RoleAdmin) == 0)
                {
                    if (oBEEducationAllowance.PayDate.AddDays(datelimit).Date <= DateTime.Now.Date)
                    {
                        throw new DataServiceException("BE_EDUCATIONALLOWANCE", "VALIDETE_PAYDATE180");
                    }

                    //เบิกล่วงหน้าไม่ได้
                    if (oBEEducationAllowance.PayDate.Date > DateTime.Now.Date)
                    {

                        throw new DataServiceException("BE_EDUCATIONALLOWANCE", "VALIDDATE");
                    }

                    var YearInt = Convert.ToInt32(oBEEducationAllowance.PayDate.ToString("yyyy"));
                    if (DateTime.Now.Year > YearInt)
                    {
                        var LastYearLimit = ShareDataManagement.LookupCache(Requestor.CompanyCode, "ESS.HR.BE", "EDUCATIONALLOWANCE_LASTYEAR_LIMIT");
                        DateTime Limit = DateTime.ParseExact(LastYearLimit + "/" + DateTime.Now.Year + " 23:59:59", "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                        if (DateTime.Now > Limit)
                        {
                            throw new DataServiceException("BE_EDUCATIONALLOWANCE", "LIMITDATE");
                        }
                    }
                    


                }
            }

            ///วันที่ใบเสร็จน้อยกว่าวันที่เข้างาน
            EmployeeData oEmpData = new EmployeeData(Requestor.EmployeeID, Requestor.PositionID, Requestor.CompanyCode, DateTime.Now);
            if (oEmpData.DateSpecific.HiringDate > oBEEducationAllowance.PayDate)
            {
                throw new DataServiceException("BE_EDUCATIONALLOWANCE", "OVER_PASSPROBATION");
            }

            ///วันที่ใบเสร็จ หลังวันเกษียณ
            if (oBEEducationAllowance.PayDate > oEmpData.DateSpecific.RetirementDate)
            {
                throw new DataServiceException("BE_EDUCATIONALLOWANCE", "OVER_RETIREMENTDATE");
            }


            //เช็คการเบิกซ้ำจากเลขที่ใบเสร็จ + วันที่
            var Dupplicate = HRBEManagement.CreateInstance(Requestor.CompanyCode).CheckDupplicate(Requestor.EmployeeID, oBEEducationAllowance.ReceiptNumber, oBEEducationAllowance.PayDate, RequestNo);
            if (Dupplicate)
            {
                throw new DataServiceException("BE_EDUCATIONALLOWANCE", "DUPPLICATE_DATA");
            }


            //เช็คการเบิกในปีเดียวกันต้องเบิกระดับเดียวกันเท่านั้น
            var CheckLevel = HRBEManagement.CreateInstance(Requestor.CompanyCode).EducationAllowanceCheckLevel(Requestor.EmployeeID, RequestNo, oBEEducationAllowance.ChildNo, oBEEducationAllowance.PayDate.ToString("yyyy"),Convert.ToInt32(oBEEducationAllowance.EducationLevel_String));
            if (!CheckLevel)
            {
                throw new DataServiceException("BE_EDUCATIONALLOWANCE", "DUPPLICATELEVEL_DATA");
            }


        }

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info)
        {
            string ReturnKey = "";

            return ReturnKey;
        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            BEEducationAllowanceOption oBEEducationAllowance = JsonConvert.DeserializeObject<BEEducationAllowanceOption>(Data.ToString());
            HRBEManagement oHRBEManagement = HRBEManagement.CreateInstance(Requestor.CompanyCode);

            oBEEducationAllowance.EducationAllowanceData.EducationLevel = Convert.ToInt32(oBEEducationAllowance.EducationAllowanceData.EducationLevel_String);

            oBEEducationAllowance.EducationAllowanceData.status = State;
            oBEEducationAllowance.EducationAllowanceData.RequestNo = RequestNo;
            oBEEducationAllowance.EducationAllowanceData.EmployeeID = Requestor.EmployeeID;
            // Get ข้อมูลบุตรที่กดเลือก
            var ChildNoSelect = oBEEducationAllowance.EducationAllowanceData.ChildNo;
            var oChildNoSelect = oBEEducationAllowance.ChildData.First(s => s.ChildNo == ChildNoSelect);
            oBEEducationAllowance.EducationAllowanceData.FirstName = oChildNoSelect.Name;
            oBEEducationAllowance.EducationAllowanceData.LastName = oChildNoSelect.Surname;
            oBEEducationAllowance.EducationAllowanceData.BirthDate = oChildNoSelect.BirthDate;


            if (oBEEducationAllowance.EducationAllowanceData.WithdrawType == 1)
            {
                oBEEducationAllowance.EducationAllowanceData.Amount_String = "AUTOQUOTA";
            }



            //gen เลขที่ใบเสร็จ กรณีพนักงานเบิกแบบเหมาจ่าย (รหัสพนักงาน + yyyymmddhhmmss)
            if (oBEEducationAllowance.EducationAllowanceData.WithdrawType == 1)
            {
                oBEEducationAllowance.EducationAllowanceData.ReceiptNumber = oBEEducationAllowance.EducationAllowanceData.EmployeeID + DateTime.Now.ToString("yyyyMMddHHmmss");
            }

            // Get ข้อมูลระดับการศึกษาที่เลือก
            var EducationLevelSelect = oBEEducationAllowance.EducationAllowanceData.EducationLevel;
            var oEducationLevelSelect = oBEEducationAllowance.EducationLevelData.First(s => s.BE_ConfigID == EducationLevelSelect);
            var Quota = oEducationLevelSelect.Quota;

            // Get ข้อมูลจำนวนเงินที่เบิกก่อนหน้า
            var EducationYear = oBEEducationAllowance.EducationAllowanceData.PayDate.ToString("yyyy");

            //Update เมื่อ Admin เลือกย้ายที่ทำงาน
            var CheckRequestNo = HRBEManagement.CreateInstance(Requestor.CompanyCode).CheckRequestNo(RequestNo);

            if (CheckRequestNo.Count == 0)
            {
                if (Convert.ToInt32(oBEEducationAllowance.EducationAllowanceData.RoleAdmin) == 1 && oBEEducationAllowance.EducationAllowanceData.MoveStatus == 1)
                {
                    var Update_UpdateDiscount_check = HRBEManagement.CreateInstance(Requestor.CompanyCode).UpdateDiscountStatus(Requestor.EmployeeID, EducationLevelSelect.ToString(), "seleted");
                }
            }


            var QuotaUsed = HRBEManagement.CreateInstance(Requestor.CompanyCode).CheckQuota(Requestor.EmployeeID, EducationYear, oBEEducationAllowance.EducationAllowanceData.ChildNo, oBEEducationAllowance.EducationAllowanceData.EducationLevel, oBEEducationAllowance.EducationAllowanceData.RequestNo);

            //Check Quota auto
            if (oBEEducationAllowance.EducationAllowanceData.Amount_String == "AUTOQUOTA")
            {
                oBEEducationAllowance.EducationAllowanceData.Amount = Quota - QuotaUsed;
            }
            else
            {
                oBEEducationAllowance.EducationAllowanceData.Amount = Math.Round(Convert.ToDecimal(oBEEducationAllowance.EducationAllowanceData.Amount_String), 2);
            }

            // เช็คจำนวนเงินที่เบิกเกินกำหนด
            var QuotaNew = Quota - QuotaUsed;
            if (QuotaNew <= 0)
            {
                object[] objs = new object[1] { Quota };
                throw new DataServiceException("BE_EDUCATIONALLOWANCE", "OVER_QUOTA", objs);
            }
            else
            {
                //ยอดเงินตามที่สามารถเบิกได้จริงตามโควต้า
                Quota = Quota - QuotaUsed;
                var AmountReimburse = Quota - oBEEducationAllowance.EducationAllowanceData.Amount;
                if (AmountReimburse < 0)
                {
                    oBEEducationAllowance.EducationAllowanceData.AmountReimburse = Quota;
                }
                else
                {
                    oBEEducationAllowance.EducationAllowanceData.AmountReimburse = oBEEducationAllowance.EducationAllowanceData.Amount;
                }
            }

            oHRBEManagement.SaveBEEducationAllowance(oBEEducationAllowance.EducationAllowanceData);
            Data = oBEEducationAllowance;

            DataRow dr = Info.NewRow();
            dr["BENEFITCODE"] = "";
            dr["DOCUMENTNO"] = oBEEducationAllowance.EducationAllowanceData.RequestNo;
            dr["REIMBURSEAMOUNT"] = oBEEducationAllowance.EducationAllowanceData.AmountReimburse;
            Info.Rows.Add(dr);
            Data = oBEEducationAllowance;

        }


        public override void CalculateInfoData(EmployeeData Requestor, object Data, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
            
         
        }

        public override void ValidateDataInAction(object newData, DataTable Info, EmployeeData Requestor, string RequestNo, string State, string Action, bool HaveComment, bool HaveComment2, DateTime SubmitDate)
        {
            if (Action != "CANCEL")
            {
                if (DateTime.Now.Year != SubmitDate.Year)
                {
                    throw new DataServiceException("HRTM_EXCEPTION", "TIMEOUT_PERIOD_SETTING");
                }
            }
        }
    }
}
