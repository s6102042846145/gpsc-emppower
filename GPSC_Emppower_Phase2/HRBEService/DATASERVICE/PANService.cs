﻿using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE;
using ESS.HR.BE.DATASERVICE;
using ESS.HR.BE.DATACLASS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.BE.DATASERVICE
{
    class PANService : AbstractDataService
    {
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            TrainingForDev oTrainingForDev = new TrainingForDev();
            oTrainingForDev.EmployeeID = Requestor.EmployeeID;
            return oTrainingForDev;
        }

        public override void PrepareData(EmployeeData Requestor, object Data)
        {

        }
        public override void CalculateInfoData(EmployeeData Requestor, object Data, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
            TrainingForDev oTrainingForDev = JsonConvert.DeserializeObject<TrainingForDev>(Data.ToString());
            DataRow dr = Info.NewRow();
            dr["TOTALAMOUNT"] = oTrainingForDev.TotalAmount;
            dr["WORKFLOWTYPE"] = oTrainingForDev.WorkflowType;
            dr["BEGINDATE"] = oTrainingForDev.BeginDate;
            dr["ENDDATE"] = oTrainingForDev.EndDate;
            Info.Rows.Add(dr);
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            TrainingForDev oTrainingForDev = JsonConvert.DeserializeObject<TrainingForDev>(newData.ToString());
            if (oTrainingForDev.TotalAmount <= 0)
            {
                object[] objs = new object[1] { oTrainingForDev.TotalAmount.ToString() };
                throw new DataServiceException("BE", "Totalamount_cantbe_null", objs);
            }
        }
        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            TrainingForDev oTrainingForDev = JsonConvert.DeserializeObject<TrainingForDev>(Data.ToString());
            HRBEManagement oHRBEManagement = HRBEManagement.CreateInstance(Requestor.CompanyCode);
            oTrainingForDev.RequestNo = RequestNo;
            oTrainingForDev.Status = State;
            oHRBEManagement.SaveBETraining(oTrainingForDev);

        }
        public override string GenerateFlowKey(EmployeeData Requestor, DataTable Info)
        {
            string ReturnKey = "";
            DataRow drow = Info.Rows[0];
            string value = drow.Field<string>("WORKFLOWTYPE");


            return value;
        }
    }
}
