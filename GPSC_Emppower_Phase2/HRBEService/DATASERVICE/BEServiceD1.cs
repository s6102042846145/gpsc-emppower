﻿using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE;
using ESS.HR.BE.DATASERVICE;
using ESS.HR.BE.DATACLASS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.BE.DATASERVICE
{
    class BEServiceD1 : AbstractDataService
    {
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            TrainingForDev oTrainingForDevD1 = new TrainingForDev();
            //oTrainingForDevD1.ContentType = "D1";
            oTrainingForDevD1.EmployeeID = Requestor.EmployeeID;
            return oTrainingForDevD1;
        }

        public override void PrepareData(EmployeeData Requestor, object Data)
        {

        }

        public override void CalculateInfoData(EmployeeData Requestor, object Data, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
            TrainingForDev oTrainingForDevD1 = JsonConvert.DeserializeObject<TrainingForDev>(Data.ToString());
            DataRow dr = Info.NewRow();
            dr["TotalAmount"] = oTrainingForDevD1.TotalAmount;
            //dr["ContentType"] = oTrainingForDevD1.ContentType;
            Info.Rows.Add(dr);
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            TrainingForDev oTrainingForDevD1 = JsonConvert.DeserializeObject<TrainingForDev>(newData.ToString());
            if (oTrainingForDevD1.TotalAmount <= 0)
            {
                object[] objs = new object[1] { oTrainingForDevD1.TotalAmount.ToString() };
                throw new DataServiceException("BE", "Totalamount_cantbe_null", objs);
            }
        }
        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            TrainingForDev oTrainingForDevD1 = JsonConvert.DeserializeObject<TrainingForDev>(Data.ToString());
            HRBEManagement oHRBEManagement = HRBEManagement.CreateInstance(Requestor.CompanyCode);
            oTrainingForDevD1.RequestNo = RequestNo;
            oTrainingForDevD1.Status = State;
            oHRBEManagement.SaveBETraining(oTrainingForDevD1);

        }
    }
}
