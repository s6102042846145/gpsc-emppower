﻿using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE;
using ESS.HR.BE.DATASERVICE;
using ESS.HR.BE.DATACLASS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.BE.DATASERVICE
{
    class DEService : AbstractDataService
    {
        public override object GenerateAdditionalData(ESS.EMPLOYEE.EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            TrainingForDev oTrainingForDev = new TrainingForDev();
            oTrainingForDev.ContentType = "DE";
            oTrainingForDev.EmployeeID = Requestor.EmployeeID;
            return oTrainingForDev;
        }

        public override void PrepareData(ESS.EMPLOYEE.EmployeeData Requestor, object Data)
        {
            
          

        }

        public override void ValidateData(object newData, ESS.EMPLOYEE.EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            TrainingForDev oTrainingForDev = JsonConvert.DeserializeObject<TrainingForDev>(newData.ToString());
           
            if (oTrainingForDev.TotalAmount<=0)
            {
                object[] objs = new object[1] { oTrainingForDev.TotalAmount.ToString() };
                throw new DataServiceException("BE", "totalamount_cantbe_null", objs);
            }

           

        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            TrainingForDev oTrainingForDev = JsonConvert.DeserializeObject<TrainingForDev>(Data.ToString());
            HRBEManagement oHRBEManagement = HRBEManagement.CreateInstance(Requestor.CompanyCode);
            oTrainingForDev.Status = State;
            oTrainingForDev.RequestNo = RequestNo;

            oHRBEManagement.SaveBETraining(oTrainingForDev);
        }

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info)
        {

            string ReturnKey = "";
            DataRow drow = Info.Rows[0];
            string value = drow.Field<string>("WORKFLOWTYPE");


            return value;
        }

        public override void CalculateInfoData(EmployeeData Requestor, object Data, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
            Info.Rows.Clear();
            TrainingForDev oPersonalInformation = JsonConvert.DeserializeObject<TrainingForDev>(Data.ToString());
            DataRow dr = Info.NewRow();
            dr["BEGINDATE"] = oPersonalInformation.BeginDate;
            dr["ENDDATE"] = oPersonalInformation.EndDate;
            dr["TOTALAMOUNT"] = oPersonalInformation.TotalAmount;
            dr["REMARK"] = "";
            dr["WORKFLOWTYPE"] = oPersonalInformation.WorkflowType;
            Info.Rows.Add(dr);

        }

        //public override void PostProcess(EmployeeData Requestor, DataTable Info, object Data, string PreviousState, string State, string RequestNo, string Comment, string ActionCode)
        //{
        //    base.PostProcess(Requestor, Info, Data, PreviousState, State, RequestNo, Comment, ActionCode);
        //}

        //public override void ValidateDataInAction(object newData, DataTable Info, EmployeeData Requestor, string RequestNo, string State, string Action, bool HaveComment, bool HaveComment2, DateTime SubmitDate)
        //{

        //}


    }
}
