﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.DATA.ABSTRACT;
using ESS.UTILITY.CONVERT;
using ESS.HR.BE.DATACLASS;
using Newtonsoft.Json;
using ESS.EMPLOYEE;
using System.Data;
using ESS.DATA.EXCEPTION;

namespace ESS.HR.BE.DATASERVICE
{
    public class BEServiceD2 : AbstractDataService
    {
        public override object GenerateAdditionalData(ESS.EMPLOYEE.EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            TrainingForDev oTrainingForDev = new TrainingForDev();
            //oTrainingForDev.ContentType = "D2";
            oTrainingForDev.EmployeeID = Requestor.EmployeeID;
            return oTrainingForDev;
        }
        public override void PrepareData(EmployeeData Requestor, object Data)
        {

        }

        public override void CalculateInfoData(EmployeeData Requestor, object Data, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
            Info.Rows.Clear();
            TrainingForDev oPersonalInformation = JsonConvert.DeserializeObject<TrainingForDev>(Data.ToString());
            DataRow dr = Info.NewRow();
            dr["TotalAmount"] = oPersonalInformation.TotalAmount;
            dr["ContentType"] = oPersonalInformation.ContentType;
            Info.Rows.Add(dr);
        }


        public override void ValidateData(object newData, ESS.EMPLOYEE.EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            TrainingForDev oTrainingForDev = JsonConvert.DeserializeObject<TrainingForDev>(newData.ToString());
            if (oTrainingForDev.TotalAmount <= 0)
            {
                object[] obj = new object[1] { oTrainingForDev.TotalAmount.ToString() };
                throw new DataServiceException("BE", "Totalamount_cantbe_null", obj);
            }
        }

       
        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            TrainingForDev oTrainingForDev = JsonConvert.DeserializeObject<TrainingForDev>(Data.ToString());
            HRBEManagement oHRBEManagement = HRBEManagement.CreateInstance(Requestor.CompanyCode);
            oTrainingForDev.RequestNo = RequestNo;
            oTrainingForDev.Status = State;
            oHRBEManagement.SaveBETraining(oTrainingForDev);
        }
    }
}
