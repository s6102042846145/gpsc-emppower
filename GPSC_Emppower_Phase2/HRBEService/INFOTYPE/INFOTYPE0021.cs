using System;
using System.Collections.Generic;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.ABSTRACT;

namespace ESS.HR.BE.INFOTYPE
{
    public class INFOTYPE0021 : AbstractInfoType
    {
        

        public INFOTYPE0021()
        {
        }

        public override string InfoType
        {
            get { return "0021"; }
        }
        public bool  Ischeck { get; set; }
        public bool IsDisable { get; set; }
        public string FamilyMember { get; set; }

        public string ChildNo { get; set; }

        public string TitleName { get; set; }

        public string TitleRank { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public DateTime BirthDate { get; set; }

        public string Sex { get; set; }

        public string BirthPlace { get; set; }

        public string CityOfBirth { get; set; }

        public string Nationality { get; set; }

        public string FatherID { get; set; }

        public string MotherID { get; set; }

        public string SpouseID { get; set; }

        public string MotherSpouseID { get; set; }

        public string FatherSpouseID { get; set; }

        public string JobTitle { get; set; }

        public string Employer { get; set; }
   
        public string Dead { get; set; }

        public string Remark { get; set; }

        public string Address { get; set; }

        public string Street { get; set; }

        public string District { get; set; }

        public string City { get; set; }

        public string Postcode { get; set; }

        public string Country { get; set; }

        public string TelephoneNo { get; set; }

       

        public int Age { get; set; }

        public int Age0101 { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public String EmployeeID { get; set; }

        public String SubType { get; set; }

        public int? UniqueBereavementId { get; set; }
    }
}