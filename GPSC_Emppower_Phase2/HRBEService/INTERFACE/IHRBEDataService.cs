﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.HR.BE.DATACLASS;
using ESS.HR.BE.INFOTYPE;

namespace ESS.HR.BE.INTERFACE
{
    public interface IHRBEDataService
    {
        void SaveBETraining(TrainingForDev oTrainingForDev);

        List<TrainingForDev> GetBETraining(String EmployeeID);

        List<BERegistorHealthInsuranceConfig> GetRegistorHealthInsuranceConfig(String EmployeeID);

        List<BERegistorHealthInsuranceStatusConfigList> GetRegistorHealthInsuranceStatusConfig(String EmployeeID, int year);

        INFOTYPE0002 GetPersonalData(string EmployeeID, DateTime CheckDate);

        DataSet GetReportData(DateTime UpdateDate_From, DateTime UpdateDate_To, string Types, string Status, string LanguageCode);

        void SaveBEEducationAllowance(BEEducationAllowance oBEEducationAllowance);
        List<BEEducationAllowance> GetEducationAllowance(String EmployeeID, String RequestNo, String KeyYear);

        bool EducationAllowanceCheckLevel(String EmployeeID, String RequestNo, String ChildNo, String Year, int EducationLevel);

        List<BEEducationAllowance> GetEducationAllowanceQuotaList(String EmployeeID, String KeyYear);
        List<BEEducationAllowance> GetQuotaList(String EmployeeID);
        List<BEConfig> GetBEConfig(String BE_Type);

        List<INFOTYPE0002> GetInfotype0002List(string EmployeeID1, string EmployeeID2);

        void SavePersonalData(INFOTYPE0002 data);

        List<INFOTYPE0021> GetChildsData(string EmployeeID);

        void SaveInfotype0002(string EmployeeID1, string EmployeeID2, List<INFOTYPE0002> List, string Profile);
        List<INFOTYPE0021> GetFamilyDataB(string EmployeeID);

        List<INFOTYPE0021> GetFamilyData(string EmployeeID, string SubType);

        decimal CheckQuota(string EmployeeID, string EducationYear, string ChildNo, int EducationLevel,string RequestNo);

        List<BEEducationAllowance> UpdateDiscountStatus(String EmployeeID, String EducationLevel, String check);
        List<BEEducationAllowance> CheckRequestNo(String RequestNo);
        bool CheckDupplicate(string EmployeeID, string ReceiptNumber, DateTime PayDate,string RequestNo);

        List<BEHealthInsAllowanceConfig> GetHealthInsAllowanceConfig(string Year);
        void SaveBEHealthInsAllowance(BEHealthInsAllowance oBEHealthInsAllowance);


        void SaveBEHealthInsAllowanceItem(BEHealthInsAllowanceItem oBEHealthInsAllowanceItem);

        void DeleteBEHealthInsAllowanceItem(string RequestNo);

        void DeleteBERegisterHealthInsuranceItem(string RequestNo);
       

        void SaveRegisterBEHealthInsurance(BERegisterHealthInsurance oBEHealthIns);
        void SaveRegisterBEHealthInsuranceItem(BERegisterHealthInsuranceItem oBEHealthInsItem);


       



        List<BEHealthInsAllowance> GetHealthInsAllowance(String EmployeeID, String RequestNo, String KeyYear);
        List<BEHealthInsAllowanceItem> GetHealthInsAllowanceItem(String EmployeeID, String RequestNo);
        List<BEHealthInsAllowance> GetHealthInsAllowanceQuota(String EmployeeID, String Year, String FamilyData);
        List<YearsConfig> GetYears();

        List<BEFitnessAllowance> GetBEFitnessAllowance(String EmployeeID, String RequestNo, String KeyYear);
        List<BEFitnessAllowanceItem> GetBEFitnessAllowanceItem(String EmployeeID, String RequestNo);
        List<BEFitnessAllowance> GetBEFitnessAllowanceQuota(String EmployeeID, DateTime PassProbation, String Year);
        decimal BEHealthInsAllowanceCheckQuota(string EmployeeID, string Year, int RelationshipType,string RequestNo);

        decimal BEFitnessAllowanceCheckQuota(string EmployeeID, string Year,string RequestNo);

        void SaveBEFitnessAllowance(BEFitnessAllowance oBEFitnessAllowance);
        void SaveBEFitnessAllowanceItem(BEFitnessAllowanceItem oBEFitnessAllowanceItem);

        void DeleteBEFitnessAllowanceItem(string RequestNo);

        List<BEFitnessAllowanceConfig> GetBEFitnessAllowanceConfig(String EmployeeID, DateTime PassProbation, String Year);


        bool HealthInsAllowanceCheckDupplicate(string EmployeeID, string ReceiptNumber, DateTime RecieptDate, string RequestNo);

        bool FitnessAllowanceCheckDupplicate(string EmployeeID, string ReceiptNumber, DateTime RecieptDate, string RequestNo);

        void UpdateBEEducationAllowancePosting(bool StatusPost, string RequestNo, string PostingMessage, DateTime PostingDate);

        void UpdateBEFitnessAllowancePosting(bool StatusPost, string RequestNo, string PostingMessage, DateTime PostingDate);

        void UpdateBEHealthInsAllowancePosting(bool StatusPost, string RequestNo, string PostingMessage, DateTime PostingDate);

        void UpdateBEBereavementInfoPosting(bool StatusPost, string RequestNo, string PostingMessage, DateTime PostingDate);

        void UpdateFBReimbursePosting(bool StatusPost, string RequestNo, string PostingMessage, DateTime PostingDate);

        void SaveWageAdditionalData(BEPostingData oBEEducationAllowance);

        int BEFitnessAllowanceCheckEnrollment(string EmployeeID, string Year);

        List<BEPostingData> GetBEEducationAllowanceGetToPosting(int AllowanceWagetypeID);

        List<BEPostingData> GetBEFitnessAllowanceGetToPosting(int AllowanceWagetypeID);

        List<BEPostingData> GetBEHealthInsAllowanceGetToPosting(int AllowanceWagetypeID);

        List<BEPostingData> GetBEBereavementInfoGetToPosting(int AllowanceWagetypeID);

        List<BEPostingData> GetFBReimburseGetToPosting(int AllowanceWagetypeID);

        bool InsertActionLog(UTILITY.DATACLASS.ActionLog oActionLog);



        #region Bereavement

        List<BEBereavementInfo> GetBereavementInfoHistory(String EmployeeID);
        List<BEBereavementInfo> GetBereavementItem(String EmployeeID, String RequestNo, String KeyType);

        List<BEBereavementConfig> GetBereavementConfig();

        void SaveBEBereavementInfo(BEBereavementInfo oBereavementInfo);
        void SaveBEBereavementItem(BEBereavementItem oBereavementItem);

        void DeleteBEBereavementItem(string RequestNo);

        void SaveFamilyData(List<INFOTYPE0021> data);

        bool BEBereavementCheckDupplicate(string EmployeeID, int BereavementType, string RequestNo);


        #endregion Bereavement

        List<BenefitReport> GetBenefitReport(DateTime UpdateDate_From, DateTime UpdateDate_To, string Benefit_Type, string Benefit_Status, string language);
       
        bool EmployeeTrainee(string EmpGroup, DateTime PassProbation);

        List<DataReligion> GetReligion();

        List<ReportBEEducationAllowanceByAdmin> GetReportEducationAllowanceByAdmin(DateTime begin_date, DateTime end_date);

        List<ReportHealthInsAllowanceByAdmin> GetReportHealthInsAllowanceByAdmin(DateTime begin_date, DateTime end_date);

        List<ReportFitnessAllowanceByAdmin> GetReportFitnessAllowanceByAdmin(DateTime begin_date, DateTime end_date);

        List<ReportBEBereavementInfoByAdmin> GetReporttBEBereavementByAdmin(DateTime begin_date, DateTime end_date);

        List<ReportDisbursementOfWelfareByAdmin> getReportDisbursementOfWelfareByAdmin(DateTime begin_date, DateTime end_date);

        #region สวัสดิการและสิทธิผลประโยชน์พนักงาน
        DataSumQuotaAbsence getSumQuotaAbsence(string year, string employeeId);
        List<EmployeeAbsenceByType> getAbsenceByTypeEmployee(string year, string employeeId);
        TranasctionUseReimburseByEmployee getUseReimburseByEmployee(string year, string empSubGroupId, string employeeId);
        List<EducationChildByEmployee> getUseEducationChildByEmployee(string year, string employeeId);
        List<FitnessByEmployee> GetUseFitnessByEmployee(string year, string employeeId);
        List<HealthInsAllowanceParent> GetUseHealthInsAllowanceParent(string year, string employeeId);
        List<BenefitsProvidentFundByEmployee> GetBenefitsProvidentFundByEmployee(string year, string employeeId);
        List<DashbaordOtherWelfare> GetAnnouncementTextAllEmployee();
        List<EmployeeIncomeForYear> GetIncomdeForYearByEmployee(string employeeId, int year);
        void GetBenifitsRegisterEnrollment(string year, string employeeId);
        decimal GetPercentageWorkingAge(decimal ageWorking);
        decimal GetSalaryEmployee(string employeeId, string year);
        decimal GetRateProvidentEmployeeSaving(string employeeId, string year);
        EnrollmentWelfareByEmployee GetEnrollmentWelfareByEmployee(string employeeId, string year);
        #endregion

        #region HealthInsuranceConfig
        List<BEHealthInsConfig> GetHealthInsuranceConfig();

        void SaveHealthInsuranceConfig(BEHealthInsConfig oHealthConfig);
        #endregion

        List<HealthInsAllowanceReport> GetHealthInsAllowanceReport(DateTime UpdateDate_From, DateTime UpdateDate_To);

    }
}
