using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using ESS.EMPLOYEE.ABSTRACT;
using ESS.EMPLOYEE.CONFIG.PA;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.EMPLOYEE.JOB;
using ESS.SHAREDATASERVICE;
using ESS.EMPLOYEE.CONFIG.OM;
using ESS.UTILITY.EXTENSION;
using System.Linq;
using ESS.UTILITY.DATACLASS;
using ESS.UTILITY.LOG;
using System.Xml.Serialization;
using System.IO;
using ESS.EMPLOYEE.DATACLASS;

namespace ESS.EMPLOYEE.DB
{
    public class EmployeeDataServiceImpl : AbstractEmployeeDataService
    {
        #region Constructor
        public EmployeeDataServiceImpl(string oCompanyCode)
        {
            //Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID);
            CompanyCode = oCompanyCode;
            oSqlManage["BaseConnStr"] = BaseConnStr;
        }

        #endregion Constructor

        #region Member
        //private Dictionary<string, string> Config { get; set; }
        public string CompanyCode { get; set; }

        private static string ModuleID = "ESS.EMPLOYEE.DB";

        private Dictionary<string, string> oSqlManage = new Dictionary<string, string>();

        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }

        private string PINConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "PINConnStr");
            }
        }

        private string APPPATH
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "APPPATH");
            }
        }

        private string EXCHANGESERVER
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "ESS.MAIL.EXCHANGE", "EXCHANGESERVER");
            }
        }

        private string SYSTEMEMAIL
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "ESS.MAIL.EXCHANGE", "SYSTEMEMAIL");
            }
        }
        #endregion Member

        #region " private data "
        //private string BaseConnStr
        //{
        //    get
        //    {
        //        return ShareDataManager.LookupCache(CompanyCode, ModuleID, "BASECONNSTR"];
        //    }
        //}

        //private string BaseConnStr
        //{
        //    get
        //    {
        //        return ShareDataManager.LookupCache(CompanyCode, ModuleID, "BASECONNSTR"];
        //    }
        //}


        #endregion " private data "

        #region " ParseToUserSetting "

        private UserSetting ParseToUserSetting(DataRow dr)
        {
            UserSetting Item = new UserSetting((string)dr["UserID"], CompanyCode);
            Item.Language = (string)dr["LanguageCode"];
            if (Item.Language.Equals(""))
            {
                Item.Language = ShareDataManagement.LookupCache(CompanyCode, ModuleID, "DEFAULTLANGUAGE");
            }
            if (dr["ReceiveMail"] == null || dr["ReceiveMail"].Equals(""))
            {
                Item.ReceiveMail = true;
            }
            else
            {
                Item.ReceiveMail = (bool)dr["ReceiveMail"];
            }
            return Item;
        }

        #endregion " ParseToUserSetting "

        #region " ParseToUserRoleSetting "

        private UserRoleSetting ParseToUserRoleSetting(DataRow dr)
        {
            UserRoleSetting Item = new UserRoleSetting((string)dr["UserRole"]);
            Item.Language = (string)dr["LanguageCode"];
            return Item;
        }

        #endregion " ParseToUserRoleSetting "

        #region IEmployeeService Members

        #region " FindManager "

        public override EmployeeData FindManager(string EmployeeID, string PositionID, string ManagerCode, DateTime CheckDate, string LanguageCode)
        {
            oSqlManage["ProcedureName"] = "sp_EmployeeManagerGet";
            DataTable oResult = new DataTable();
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_EmployeeID"] = EmployeeID;
            oParamRequest["@p_PositionID"] = PositionID;
            oParamRequest["@p_ManagerCode"] = ManagerCode;
            oParamRequest["@p_CheckDate"] = CheckDate;
            DataTable oTable = DatabaseHelper.ExecuteQuery(oSqlManage, oParamRequest);

            //EmployeeData oReturn = new EmployeeData() { CompanyCode = this.CompanyCode };
            EmployeeData oReturn = null;
            foreach (DataRow dr in oTable.Rows)
            {
                string cManagerType = (string)dr["RECEIPIENTTYPE"];
                string cManager = (string)dr["RECEIPIENTCODE"];
                switch (cManagerType.Trim().ToUpper())
                {
                    case "S":
                        oReturn = EmployeeManagement.CreateInstance(this.CompanyCode).GetEmployeeDataByPosition(cManager, CheckDate, LanguageCode);
                        break;

                    case "P":
                        oReturn = new EmployeeData(cManager, CheckDate);
                        break;
                }
                break;
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " FindManager "

        #region " FindManagerPos "

        public string FindManagerPos(string EmployeeID, string ManagerCode, DateTime CheckDate)
        {
            string oReturn = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from GetSpecialReceipient(@EmployeeID, @Code, @CheckDate)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Code", SqlDbType.VarChar);
            oParam.Value = ManagerCode;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("RECEIPIENT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                string cManagerType = (string)dr["RECEIPIENTTYPE"];
                string cManager = (string)dr["RECEIPIENTCODE"];
                oReturn = cManager;
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " FindManagerPos "

        #region " GetEmployeeIDFromUsername "

        public override string GetEmployeeIDFromUserID(string UserID)
        {
            string returnValue;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            //SqlCommand oCommand = new SqlCommand("select EmployeeID from INFOTYPE0105 Where CategoryCode = 'USERID' and DataText = @UserID", oConnection);
            SqlCommand oCommand = new SqlCommand("sp_INFOTYPE0105GetByUserID", oConnection);

            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_UserID", UserID);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EMPLOYEE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            if (oTable.Rows.Count > 0)
            {
                DataRow dr = oTable.Rows[0];
                returnValue = (string)dr["EmployeeID"];
            }
            else
            {
                returnValue = UserID.PadLeft(8, '0');
            }
            oTable.Dispose();
            return returnValue;
        }

        #endregion " GetEmployeeIDFromUsername "

        #region " ValidateEmployeeID "

        public override bool ValidateEmployeeID(string EmployeeID, DateTime CheckDate)
        {
            bool returnValue = false;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select EmployeeID from v_current_INFOTYPE0001 Where EmployeeID = @EmpID", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EMPLOYEE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            if (oTable.Rows.Count > 0)
            {
                returnValue = true;
            }
            oTable.Dispose();
            return returnValue;
        }

        #endregion " ValidateEmployeeID "

        public override List<CARDSETTING> GetCardSettingList(string EmployeeID, DateTime CheckDate, string Profile)
        {
            List<CARDSETTING> returnValue = new List<CARDSETTING>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;

            oCommand = new SqlCommand("select * from CardSetting Where EmployeeID = @EmpID1 and @CheckDate Between BeginDate and EndDate", oConnection);

            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate.Date;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("CardSetting");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                CARDSETTING item = new CARDSETTING();
                item.ParseToObject(dr);
                returnValue.Add(item);
            }
            oTable.Dispose();
            return returnValue;
        }

        #region " INFOTYPE0001 "

        #region " GetInfotype0001 "

        public override List<INFOTYPE0001> GetInfotype0001List(string EmployeeID1, string EmployeeID2, DateTime CheckDate, string Profile)
        {
            List<INFOTYPE0001> returnValue = new List<INFOTYPE0001>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            if (CheckDate == DateTime.MinValue)
            {
                oCommand = new SqlCommand("select * from INFOTYPE0001 Where EmployeeID between @EmpID1 and @EmpID2 and getdate() Between BeginDate and EndDate", oConnection);
            }
            else
            {
                oCommand = new SqlCommand("select * from INFOTYPE0001 Where EmployeeID between @EmpID1 and @EmpID2 and @CheckDate Between BeginDate and EndDate", oConnection);
            }
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand.Parameters.Add(oParam);

            if (CheckDate != DateTime.MinValue)
            {
                oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
                oParam.Value = CheckDate.Date;
                oCommand.Parameters.Add(oParam);
            }

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EMPLOYEE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE0001 item = new INFOTYPE0001();
                item.ParseToObject(dr);
                returnValue.Add(item);
            }
            oTable.Dispose();
            return returnValue;
        }

        #endregion " GetInfotype0001 "

        #region " SaveInfotype0001 "

        public override void SaveInfotype0001(string EmployeeID1, string EmployeeID2, List<INFOTYPE0001> data, string profile)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand1 = new SqlCommand("delete from INFOTYPE0001", oConnection, tx);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE0001 where EmployeeID between @EmpID1 and @EmpID2", oConnection, tx);
            oCommand1.CommandTimeout = 6000;
            oCommand.CommandTimeout = 6000;
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oAdapter.UpdateBatchSize = 1000;
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("INFOTYPE0001");
            try
            {
                oCommand1.ExecuteNonQuery();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                foreach (INFOTYPE0001 item in data)
                {
                    item.EndDate = item.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);
                    item.LoadDataToTable(oTable);
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                //tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand1.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }

        #endregion " SaveInfotype0001 "
        #endregion " INFOTYPE0001 "

        #region " INFOTYPE0002 "
        public override void SaveInfotype0002(string EmployeeID1, string EmployeeID2, List<INFOTYPE0002> List, string Profile)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand1 = new SqlCommand("delete from INFOTYPE0002", oConnection, tx);

            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE0002 where EmployeeID between @EmpID1 and @EmpID2", oConnection, tx);
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oAdapter.UpdateBatchSize = 10000;
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("INFOTYPE0002");
            try
            {
                oCommand1.ExecuteNonQuery();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                foreach (INFOTYPE0002 item in List)
                {
                    item.MaritalEffectiveDate = new DateTime(1800, 01, 01, 0, 0, 0, 0);
                    item.LoadDataToTable(oTable);
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand1.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }
        #endregion

        #region " INFOTYPE0182 "

        #region " GetInfoType0182 "

        public override INFOTYPE0182 GetInfotype0182(string EmployeeID, string Language)
        {
            INFOTYPE0182 returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE0182 Where EmployeeID = @EmpID and SubType = @Language and GetDate() between BeginDate and EndDate", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            string langcode = "";
            switch (Language.ToUpper())
            {
                case "EN":
                    langcode = "2";
                    break;
            }
            oParam = new SqlParameter("@Language", SqlDbType.VarChar);
            oParam.Value = langcode;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EMPLOYEE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new INFOTYPE0182();
            returnValue.ParseToObject(oTable);
            oTable.Dispose();
            return returnValue;
        }

        #endregion " GetInfoType0182 "

        #region " SaveInfotype0182 "

        public override void SaveInfotype0182(string EmployeeID1, string EmployeeID2, List<INFOTYPE0182> data, string Profile)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand1 = new SqlCommand("delete from INFOTYPE0182", oConnection, tx);

            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE0182 where EmployeeID between @EmpID1 and @EmpID2", oConnection, tx);
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oAdapter.UpdateBatchSize = 10000;
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("INFOTYPE0182");
            try
            {
                oCommand1.ExecuteNonQuery();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                foreach (INFOTYPE0182 item in data)
                {
                    //AddBy: Ratchatawan W. (2012-02-29)
                    item.EndDate = item.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                    item.LoadDataToTable(oTable);
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand1.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }

        #endregion " SaveInfotype0182 "

        #endregion " INFOTYPE0182 "

        #region " INFOTYPE0105 "

        public override List<INFOTYPE0105> GetInfotype0105List(string EmployeeID1, string EmployeeID2, string Profile)
        {
            List<INFOTYPE0105> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE0105 Where EmployeeID >= @EmpID1 and EmployeeID <= @EmpID2", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EMPLOYEE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<INFOTYPE0105>();
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE0105 item = new INFOTYPE0105();
                item.ParseToObject(dr);
                returnValue.Add(item);
            }
            oTable.Dispose();
            return returnValue;
        }

        public override INFOTYPE0105 GetInfotype0105(string EmployeeID, string oCategoryCode)
        {
            //CELLPHONE
            //FAXNUMBER
            //HOMENUMBER
            //MAIL
            //OFFICENUMBER
            //USERID
            INFOTYPE0105 oReturn = new INFOTYPE0105();
            try
            {
                List<INFOTYPE0105> oResult = GetInfotype0105ByCategoryCode(EmployeeID, oCategoryCode);
                if (oResult.Count > 0)
                {
                    oReturn = oResult.SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
            }
            return oReturn;
        }
        //public override INFOTYPE0105 GetInfotype0105ByCategoryCode(string EmployeeID, string oCategoryCode)
        //{
        //    INFOTYPE0105 oReturn = GetInfotype0105ByCategoryCode(EmployeeID, oCategoryCode).First();
        //    return oReturn;
        //}

        public override List<INFOTYPE0105> GetInfotype0105ByCategoryCode(string EmployeeID, string oCategoryCode)
        {
            List<INFOTYPE0105> oReturn = new List<INFOTYPE0105>();
            oSqlManage["ProcedureName"] = "sp_PA_INFOTYPE0105Get";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_EmployeeID"] = EmployeeID;
            oParamRequest["@p_CategoryCode"] = oCategoryCode;
            oReturn.AddRange(DatabaseHelper.ExecuteQuery<INFOTYPE0105>(oSqlManage, oParamRequest));
            return oReturn;
        }
        private Dictionary<string, string> GetInf105Category()
        {
            Dictionary<string, string> oReturn = new Dictionary<string, string>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE0105_Mapping", oConnection, tx);

            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("INFOTYPE0105_Mapping");
            try
            {
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);
                foreach (DataRow row in oTable.Rows)
                {
                    string subtype = row["SubType"].ToString();
                    string catcode = row["CategoryCode"].ToString();
                    oReturn[subtype] = catcode;
                }

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
            return oReturn;
        }
        public override void SaveInfotype0105(string EmployeeID1, string EmployeeID2, List<INFOTYPE0105> data, string profile)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand1 = new SqlCommand("delete from INFOTYPE0105", oConnection, tx);

            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE0105 where EmployeeID between @EmpID1 and @EmpID2", oConnection, tx);
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand.Parameters.Add(oParam);

            Dictionary<string, string> dictCategoryMapping = GetInf105Category();

            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oAdapter.UpdateBatchSize = 10000;
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("INFOTYPE0105");
            try
            {
                oCommand1.ExecuteNonQuery();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                foreach (INFOTYPE0105 item in data)
                {
                    //AddBy: Ratchatawan W. (2012-02-29)
                    item.EndDate = item.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                    if (dictCategoryMapping.ContainsKey(item.SubType))
                        item.CategoryCode = dictCategoryMapping[item.SubType];
                    else
                        item.CategoryCode = item.SubType;

                    item.LoadDataToTable(oTable);
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand1.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }

        #endregion " INFOTYPE0105 "

        #region " INFOTYPE0007 "

        #region " GetInfotype0007List "

        public override List<INFOTYPE0007> GetInfotype0007List(int Year, int Month, string TimeEvaluationClass)
        {
            List<INFOTYPE0007> returnValue = new List<INFOTYPE0007>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            DateTime Date1, Date2;
            Date1 = new DateTime(Year, Month, 1);
            Date2 = Date1.AddMonths(1).AddDays(-1);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE0007 Where BeginDate <= @Date2 and EndDate >= @Date1 and TimeEvaluateClass = @TimeEvaluationClass", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@Date1", SqlDbType.DateTime);
            oParam.Value = Date1;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Date2", SqlDbType.DateTime);
            oParam.Value = Date2;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@TimeEvaluationClass", SqlDbType.VarChar);
            oParam.Value = TimeEvaluationClass;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE0007");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE0007 item = new INFOTYPE0007();
                item.ParseToObject(dr);
                returnValue.Add(item);
            }
            oTable.Dispose();
            return returnValue;
        }

        public override List<INFOTYPE0007> GetInfotype0007List(string EmployeeID1, string EmployeeID2, int Year, int Month, string Profile)
        {
            List<INFOTYPE0007> returnValue = new List<INFOTYPE0007>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlParameter oParam;

            if (Year == -1)
            {
                oCommand = new SqlCommand("select * from INFOTYPE0007 Where EmployeeID Between @EmpID and @EmpID1 ", oConnection);
                oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
                oParam.Value = EmployeeID1;
                oCommand.Parameters.Add(oParam);

                oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
                oParam.Value = EmployeeID2;
                oCommand.Parameters.Add(oParam);
            }
            else
            {
                DateTime Date1, Date2;
                Date1 = new DateTime(Year, Month, 1);
                Date2 = Date1.AddMonths(1).AddDays(-1);
                oCommand = new SqlCommand("select * from INFOTYPE0007 Where EmployeeID Between @EmpID and @EmpID1 and BeginDate <= @Date2 and EndDate >= @Date1", oConnection);
                oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
                oParam.Value = EmployeeID1;
                oCommand.Parameters.Add(oParam);

                oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
                oParam.Value = EmployeeID2;
                oCommand.Parameters.Add(oParam);

                oParam = new SqlParameter("@Date1", SqlDbType.DateTime);
                oParam.Value = Date1;
                oCommand.Parameters.Add(oParam);

                oParam = new SqlParameter("@Date2", SqlDbType.DateTime);
                oParam.Value = Date2;
                oCommand.Parameters.Add(oParam);
            }

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE0007");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE0007 item = new INFOTYPE0007();
                item.ParseToObject(dr);
                returnValue.Add(item);
            }
            oTable.Dispose();
            return returnValue;
        }

        public override List<INFOTYPE0007> GetInfotype0007List(DateTime BeginDate, DateTime EndDate, string TimeEvaluateClass)
        {
            List<INFOTYPE0007> returnValue = new List<INFOTYPE0007>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            DateTime Date1, Date2;
            Date1 = BeginDate;
            Date2 = EndDate;
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE0007 Where BeginDate <= @Date2 and EndDate >= @Date1 and TimeEvaluateClass = @TimeEvaluationClass", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@Date1", SqlDbType.DateTime);
            oParam.Value = Date1;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Date2", SqlDbType.DateTime);
            oParam.Value = Date2;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@TimeEvaluationClass", SqlDbType.VarChar);
            oParam.Value = TimeEvaluateClass;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE0007");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE0007 item = new INFOTYPE0007();
                item.ParseToObject(dr);
                returnValue.Add(item);
            }
            oTable.Dispose();
            return returnValue;
        }

        #endregion " GetInfotype0007List "

        #region " SaveInfotype0007 "

        public override void SaveInfotype0007(string EmployeeID1, string EmployeeID2, List<INFOTYPE0007> data, string profile)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam, oParam1;

            SqlCommand oCommand1 = new SqlCommand("delete from INFOTYPE0007 where EmployeeID between @EmpID1 and @EmpID2", oConnection, tx);
            oParam1 = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam1.Value = EmployeeID1;
            oCommand1.Parameters.Add(oParam1);
            oParam1 = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam1.Value = EmployeeID2;
            oCommand1.Parameters.Add(oParam1);


            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE0007 where EmployeeID between @EmpID1 and @EmpID2", oConnection, tx);
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oAdapter.UpdateBatchSize = 10000;
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("INFOTYPE0007");
            try
            {
                oCommand1.ExecuteNonQuery();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                foreach (INFOTYPE0007 item in data)
                {
                    //AddBy: Ratchatawan W. (2012-02-29)
                    item.EndDate = item.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                    item.LoadDataToTable(oTable);
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand1.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }

        #endregion " SaveInfotype0007 "

        #endregion " INFOTYPE0007 "

        #region " GetDelegatePersons "

        public override List<EmployeeData> GetDelegatePersons(string EmployeeID)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select DelegateFrom from DelegateData Where DelegateTo = @EmployeeID and GetDate() between BeginDate and EndDate", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EMPLOYEE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            EmployeeData oEmp;
            foreach (DataRow dr in oTable.Rows)
            {
                oEmp = new EmployeeData((string)dr["DelegateFrom"]);
                oReturn.Add(oEmp);
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetDelegatePersons "

        #region " SaveWorkPlaceData "

        public override void SaveWorkPlaceData(string EmployeeID, List<WorkPlaceCommunication> workplaceList, string Profile)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;

            SqlCommand oCommand1 = new SqlCommand("delete from WorkPlaceData where EmployeeID = @EmpID1 or @EmpID1 = ''", oConnection, tx);
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand1.Parameters.Add(oParam);

            SqlCommand oCommand = new SqlCommand("select * from WorkPlaceData where EmployeeID = @EmpID1", oConnection, tx);
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("WORKPLACEDATA");
            try
            {
                oCommand1.ExecuteNonQuery();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                foreach (WorkPlaceCommunication item in workplaceList)
                {
                    item.LoadDataToTable(oTable);
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand1.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }

        #endregion " SaveWorkPlaceData "

        #endregion IEmployeeService Members

        #region " User "

        #region " GetUserInResponse "

        public override List<EmployeeData> GetUserInResponse(UserRoleResponseSetting setting)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select distinct * from GetUserInResponse(@EmpID,@UserRole,@IncludeSub,@CheckDate)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = WORKFLOW.WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@UserRole", SqlDbType.VarChar);
            oParam.Value = setting.UserRole;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@IncludeSub", SqlDbType.Bit);
            oParam.Value = setting.IncludeSub;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = DateTime.Now;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("USER");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(new EmployeeData((string)dr["EmployeeID"]));
            }
            oTable.Dispose();
            return oReturn;
        }

        public override List<EmployeeData> GetUserInResponse(UserRoleResponseSetting setting, DateTime oCheckDate)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select distinct * from GetUserInResponse(@EmpID,@UserRole,@IncludeSub,@CheckDate)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = WORKFLOW.WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@UserRole", SqlDbType.VarChar);
            oParam.Value = setting.UserRole;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@IncludeSub", SqlDbType.Bit);
            oParam.Value = setting.IncludeSub;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = oCheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("USER");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(new EmployeeData((string)dr["EmployeeID"]));
            }
            oTable.Dispose();
            return oReturn;
        }

        public override List<EmployeeData> GetUserInResponse(UserRoleResponseSetting setting, DateTime BeginDate, DateTime EndDate)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select distinct * from GetUserInResponseInRange(@EmpID,@UserRole,@IncludeSub,@BeginDate,@EndDate)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = WORKFLOW.WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@UserRole", SqlDbType.VarChar);
            oParam.Value = setting.UserRole;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@IncludeSub", SqlDbType.Bit);
            oParam.Value = setting.IncludeSub;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("USER");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
                oReturn.Add(new EmployeeData((string)dr["EmployeeID"]));
            oTable.Dispose();
            return oReturn;
        }

        public override List<EmployeeData> GetUserInResponse(UserRoleResponseSetting setting, string EmpID)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select distinct * from GetUserInResponse(@EmpID,@UserRole,@IncludeSub,@CheckDate)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmpID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@UserRole", SqlDbType.VarChar);
            oParam.Value = setting.UserRole;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@IncludeSub", SqlDbType.Bit);
            oParam.Value = setting.IncludeSub;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = DateTime.Now;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("USER");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(new EmployeeData((string)dr["EmployeeID"]));
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetUserInResponse "

        #region " GetUserSetting "

        public override UserSetting GetUserSetting(string EmployeeID)
        {
            UserSetting oReturn = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from GetUserSetting(@UserID)", oConnection);
            SqlParameter oParam = new SqlParameter("@UserID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("USERSETTING");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();

            oAdapter.Dispose();
            oCommand.Dispose();
            oConnection.Dispose();

            if (oTable.Rows.Count > 0)
            {
                oReturn = this.ParseToUserSetting(oTable.Rows[0]);
            }
            else
            {
                oReturn = new UserSetting(EmployeeID, CompanyCode);
            }

            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetUserSetting "

        public override void SaveUserSetting(UserSetting userSetting)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand;
            SqlParameter oParam;
            oCommand = new SqlCommand("select * from UserSetting where UserID = @UserID", oConnection, tx);
            oParam = new SqlParameter("@UserID", SqlDbType.VarChar);
            oParam.Value = userSetting.Employee.EmployeeID;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            SqlCommandBuilder oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("USERSETTING");

            oCommand = new SqlCommand("select * from UserRole where EmployeeID = @EmployeeID", oConnection, tx);
            oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            oParam.Value = userSetting.Employee.EmployeeID;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter1 = new SqlDataAdapter(oCommand);
            SqlCommandBuilder oCB1 = new SqlCommandBuilder(oAdapter1);
            //DataTable oTable1 = new DataTable("USERROLE");

            try
            {
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                DataRow oNewRow;
                oNewRow = oTable.NewRow();
                oNewRow["UserID"] = userSetting.Employee.EmployeeID;
                oNewRow["LanguageCode"] = userSetting.Language;
                oNewRow["ReceiveMail"] = userSetting.ReceiveMail;
                oTable.LoadDataRow(oNewRow.ItemArray, false);

                //List<string> notSaveList = new List<string>();
                //notSaveList.Add("GROUPREQUESTOR");
                //notSaveList.Add("GROUPEMPLOYEE");
                //foreach (string UserRole in userSetting.Roles)
                //{
                //    if (UserRole.StartsWith("#"))
                //        continue;
                //    if (notSaveList.Contains(UserRole))
                //        continue;
                //    oNewRow = oTable1.NewRow();
                //    oNewRow["EmployeeID"] = userSetting.Employee.EmployeeID;
                //    oNewRow["UserRole"] = UserRole;
                //    oTable1.LoadDataRow(oNewRow.ItemArray, false);
                //}
                //DataView oDV = new DataView(oTable1);
                //oDV.RowStateFilter = DataViewRowState.Unchanged;
                //foreach (DataRowView drv in oDV)
                //{
                //    drv.Row.Delete();
                //}

                oAdapter.Update(oTable);
                //oAdapter1.Update(oTable1);

                if (!userSetting.ReceiveMail)
                {
                    oCommand = new SqlCommand("sp_MailToSetDisabled", oConnection, tx);
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
                    oParam.Value = userSetting.Employee.EmployeeID;
                    oCommand.Parameters.Add(oParam);
                    oParam = new SqlParameter("@p_DisabledDate", SqlDbType.DateTime);
                    oParam.Value = DateTime.Now;
                    oCommand.Parameters.Add(oParam);
                    oCommand.ExecuteNonQuery();
                }

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("Save user setting error", ex);
            }
            finally
            {
                oConnection.Close();
                oAdapter.Dispose();
                oCommand.Dispose();
                oConnection.Dispose();
                oTable.Dispose();
            }
        }

        #region " GetUserRole "

        public override List<string> GetUserRole(string EmployeeID)
        {
            List<string> oReturn = new List<string>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select UserRole from GetUserRoles(@ID) ", oConnection);
            SqlParameter oParam = new SqlParameter("@ID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("USERROLE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            foreach (DataRow dr in oTable.Rows)
            {
                string userRole = (string)dr["UserRole"];
                if (!oReturn.Contains(userRole))
                {
                    oReturn.Add(userRole);
                }
            }

            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetUserRole "

        public override List<string> GetUserInRole(string UserRole)
        {
            List<string> oReturn = new List<string>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select UserID from UserRole where UserRole = @UserRole", oConnection);
            SqlParameter oParam = new SqlParameter("@UserRole", SqlDbType.VarChar);
            oParam.Value = UserRole;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("USERROLE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            foreach (DataRow dr in oTable.Rows)
            {
                string UserID = (string)dr["UserID"];
                if (!oReturn.Contains(UserID))
                {
                    oReturn.Add(UserID);
                }
            }

            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            oTable.Dispose();
            return oReturn;
        }

        #region " GetUserRoleSetting "

        public override List<string> GetEmployeeIDByRole(string UserRole)
        {
            List<string> oReturn = new List<string>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_UserResponseGet", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            oParam = new SqlParameter("@p_UserRole", SqlDbType.VarChar);
            oParam.Value = UserRole;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_AdminGroup", SqlDbType.VarChar);
            oParam.Value = DBNull.Value;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_Organization", SqlDbType.VarChar);
            oParam.Value = DBNull.Value;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_BusinessArea", SqlDbType.VarChar);
            oParam.Value = DBNull.Value;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("USERROLE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            foreach (DataRow dr in oTable.Rows)
            {
                string UserID = (string)dr["EmployeeID"];
                if (!oReturn.Contains(UserID))
                {
                    oReturn.Add(UserID);
                }
            }

            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            oTable.Dispose();
            return oReturn;
        }

        public override UserRoleSetting GetUserRoleSetting(string UserRole)
        {
            UserRoleSetting oReturn = new UserRoleSetting(UserRole);
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from GetUserRoleSetting(@UserRole)", oConnection);
            SqlParameter oParam = new SqlParameter("@UserRole", SqlDbType.VarChar);
            oParam.Value = UserRole;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("USERSETTING");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();

            oAdapter.Dispose();
            oCommand.Dispose();
            oConnection.Dispose();

            if (oTable.Rows.Count > 0)
            {
                oReturn = this.ParseToUserRoleSetting(oTable.Rows[0]);
            }

            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetUserRoleSetting "

        #region " GetUserResponse "

        public override List<EmployeeData> GetUserResponse(string Role, string AdminGroup)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from GetUserResponse(@UserRole,@responsecode)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@UserRole", SqlDbType.VarChar);
            oParam.Value = Role;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@responsecode", SqlDbType.VarChar);
            oParam.Value = AdminGroup;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("USER");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(new EmployeeData((string)dr["UserID"]));
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetUserResponse "

        #region " IsUserInResponse "

        public override bool IsUserInResponse(UserRoleResponseSetting role, string EmployeeID)
        {
            bool oReturn = false;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select dbo.IsUserInResponse(@EmpID,@UserRole,@IncludeSub,@CheckDate,@CheckUser) ", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = WORKFLOW.WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@UserRole", SqlDbType.VarChar);
            oParam.Value = role.UserRole;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@IncludeSub", SqlDbType.Bit);
            oParam.Value = role.IncludeSub;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = DateTime.Now;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckUser", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("USER");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn = (bool)dr[0];
                break;
            }
            oTable.Dispose();
            return oReturn;
        }

        public override bool IsUserInResponse(UserRoleResponseSetting role, string EmployeeID, DateTime checkDate)
        {
            bool oReturn = false;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select dbo.IsUserInResponse(@EmpID,@UserRole,@IncludeSub,@CheckDate,@CheckUser) ", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = WORKFLOW.WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@UserRole", SqlDbType.VarChar);
            oParam.Value = role.UserRole;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@IncludeSub", SqlDbType.Bit);
            oParam.Value = role.IncludeSub;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = checkDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckUser", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("USER");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn = (bool)dr[0];
                break;
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " IsUserInResponse "

        #endregion " User "

        #region " GetTasks "

        public override List<TaskCopyEmployeeConfig> GetTasks()
        {
            List<TaskCopyEmployeeConfig> oReturn = new List<TaskCopyEmployeeConfig>();
            TaskCopyEmployeeConfig task;
            task = new TaskCopyEmployeeConfig();
            task.SourceMode = "SAP";
            task.SourceProfile = "DEFAULT";
            task.TargetMode = "DB";
            task.TargetProfile = "WORKFLOW";
            task.TaskID = 0;
            task.ConfigName = "PERSONALSUBAREASETTING";
            oReturn.Add(task);

            task = new TaskCopyEmployeeConfig();
            task.SourceMode = "SAP";
            task.SourceProfile = "DEFAULT";
            task.TargetMode = "DB";
            task.TargetProfile = "WORKFLOW";
            task.TaskID = 1;
            task.ConfigName = "PERSONALSUBGROUPSETTING";
            oReturn.Add(task);

            task = new TaskCopyEmployeeConfig();
            task.SourceMode = "SAP";
            task.SourceProfile = "DEFAULT";
            task.TargetMode = "DB";
            task.TargetProfile = "WORKFLOW";
            task.TaskID = 2;
            task.ConfigName = "MONTHLYWS";
            task.Param1 = DateTime.Now.Year.ToString();
            oReturn.Add(task);

            task = new TaskCopyEmployeeConfig();
            task.SourceMode = "SAP";
            task.SourceProfile = "DEFAULT";
            task.TargetMode = "DB";
            task.TargetProfile = "WORKFLOW";
            task.TaskID = 3;
            task.ConfigName = "DAILYWS";
            oReturn.Add(task);
            return oReturn;
        }

        #endregion " GetTasks "

        #region " Encrypt "

        private string Encrypt(string employeid, string pws)
        {
            PasswordDeriveBytes PDB;
            RC2CryptoServiceProvider RC2CSP;
            Int32 IVSize;
            Byte[] IV;
            Int32 Counter;
            Byte[] Seed = { 0x05 , 0x02 , 0x06 ,0x04,
                              0x01 , 0x07 , 0x03 ,0x08 };
            PDB = new PasswordDeriveBytes("MN" + employeid + pws, Seed, "MD5", 5);
            RC2CSP = new RC2CryptoServiceProvider();
            IVSize = RC2CSP.BlockSize / 8;
            IV = new Byte[IVSize];
            for (Counter = 0; Counter < IV.Length; Counter++)
            {
                IV[Counter] = Convert.ToByte(Counter);
            }
            RC2CSP.Key = PDB.CryptDeriveKey("RC2", "MD5", RC2CSP.KeySize, IV);
            string strRet = "";
            for (Counter = 0; Counter < RC2CSP.Key.Length; Counter++)
            {
                strRet += int.Parse(RC2CSP.Key.GetValue(Counter).ToString()).ToString("000");
            }
            return strRet;
        }

        #endregion " Encrypt "

        #region " PIN "
        public override bool HavePinCode(string EmployeeID)
        {
            bool returnValue = false;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select EmployeeID from EmployeePIN Where EmployeeID = @EmpID ", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EMPLOYEEPIN");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            if (oTable.Rows.Count > 0)
            {
                returnValue = true;
            }
            oTable.Dispose();
            return returnValue;
        }

        public override bool VerifyPinCode(string EmployeeID, string pincode)
        {
            bool returnValue = false;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select EmployeeID from EmployeePIN Where EmployeeID = @EmpID and PINData = @pincode", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@pincode", SqlDbType.VarChar);
            oParam.Value = Encrypt(EmployeeID, pincode);
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EMPLOYEEPIN");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            if (oTable.Rows.Count > 0)
            {
                returnValue = true;
            }
            oTable.Dispose();
            return returnValue;
        }

        public override void ChangePINCode(string EmployeeID, string pincode, string NewPINcode)
        {
            if (!VerifyPinCode(EmployeeID, pincode))
            {
                return;
            }
            SetNewPin(EmployeeID, NewPINcode);
        }

        public override void CreateNewPIN(string EmployeeID, string TicketID, string NewPINcode)
        {
            if (!ValidateTicket(EmployeeID, "NEWPIN", TicketID))
            {
                throw new Exception("TICKET_INCORRECT");
            }
            RemoveTicket(EmployeeID, "NEWPIN");
            SetNewPin(EmployeeID, NewPINcode);
        }

        public override string CreateTicket(string TicketClass, TimeSpan LifeTime, string PINcode)
        {
            string empID = WORKFLOW.WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID;
            if (!this.VerifyPinCode(empID, PINcode))
            {
                throw new Exception("PINCODE Incorrect");
            }
            return CreateTicket(empID, TicketClass, LifeTime);
        }

        private string CreateTicket(string EmployeeID, string TicketClass, TimeSpan LifeTime)
        {
            string returnValue = "";
            SqlConnection oConnection = new SqlConnection(PINConnStr);
            SqlCommand oCommand = new SqlCommand("select * from TicketMaster Where EmployeeID = @EmpID and TicketClass = @TicketClass", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@TicketClass", SqlDbType.VarChar);
            oParam.Value = TicketClass;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            SqlCommandBuilder oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("TICKETMASTER");
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();

            returnValue = Guid.NewGuid().ToString();
            DataRow oNewRow = oTable.NewRow();
            oNewRow["EmployeeID"] = EmployeeID;
            oNewRow["TicketClass"] = TicketClass;
            oNewRow["TicketExpired"] = DateTime.Now.Add(LifeTime);
            oNewRow["TicketID"] = returnValue;
            oTable.LoadDataRow(oNewRow.ItemArray, false);

            oConnection.Open();
            oAdapter.Update(oTable);
            oConnection.Close();

            oTable.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            return returnValue;
        }

        private void RemoveTicket(string EmployeeID, string TicketClass)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("Delete from TicketMaster Where EmployeeID = @EmpID and TicketClass = @TicketClass", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@TicketClass", SqlDbType.VarChar);
            oParam.Value = TicketClass;
            oCommand.Parameters.Add(oParam);
            oConnection.Open();
            oCommand.ExecuteNonQuery();
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
        }

        private void SetNewPin(string EmployeeID, string NewPINcode)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from EmployeePIN Where EmployeeID = @EmpID", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            SqlCommandBuilder oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("EmployeePIN");
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();

            DataRow oNewRow = oTable.NewRow();
            oNewRow["EmployeeID"] = EmployeeID;
            oNewRow["PinData"] = Encrypt(EmployeeID, NewPINcode);
            oTable.LoadDataRow(oNewRow.ItemArray, false);

            oConnection.Open();
            oAdapter.Update(oTable);
            oConnection.Close();

            oTable.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
        }

        public override bool ValidateTicket(string EmployeeID, string TicketClass, string TicketID)
        {
            bool returnValue = false;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select EmployeeID from TicketMaster Where EmployeeID = @EmpID and TicketClass = @TicketClass and TicketID = @TicketID and GetDate() < TicketExpired", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@TicketClass", SqlDbType.VarChar);
            oParam.Value = TicketClass;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@TicketID", SqlDbType.VarChar);
            oParam.Value = TicketID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("TICKETMASTER");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            if (oTable.Rows.Count > 0)
            {
                returnValue = true;
            }
            oTable.Dispose();
            return returnValue;
        }

        public override void RequestNewPIN(EmployeeData oEmp)
        {
            string ticket = this.CreateTicket(oEmp.EmployeeID, "NEWPIN", new TimeSpan(3, 0, 0));
            string cPath = this.APPPATH;

            #region " send mail "
            System.Net.Mail.SmtpClient oClient = new System.Net.Mail.SmtpClient(EXCHANGESERVER);
            System.Net.Mail.MailAddress oSender = new System.Net.Mail.MailAddress(SYSTEMEMAIL, "WORKFLOW SYSTEM");
            System.Net.Mail.MailMessage oMessage = new System.Net.Mail.MailMessage();
            System.Net.Mail.MailAddress oTo;

            //EmployeeData oEmp = new EmployeeData(Employeeid);
            oTo = new System.Net.Mail.MailAddress(oEmp.EmailAddress, oEmp.Name);
            oMessage.To.Add(oTo);

            oMessage.ReplyTo = oTo;
            oMessage.From = oSender;
            oMessage.Sender = oSender;
            oMessage.Subject = "CHANGE NEW PIN CODE";
            oMessage.Body = string.Format("<a href=\"{0}\">Click here to create new PINCODE</a>", string.Format(cPath, ticket));
            oMessage.BodyEncoding = Encoding.UTF8;
            oMessage.IsBodyHtml = true;
            oClient.Send(oMessage);
            oMessage.Dispose();

            #endregion
        }
        #endregion

        public override void AlertIncorrectPIN(EmployeeData oEmp, string subjectAlert, string bodyAlert)
        {
            //string ticket = this.CreateTicket(oEmp.EmployeeID, "NEWPIN", new TimeSpan(3, 0, 0));
            //string cPath = this.APPPATH;

            #region " send mail "
            System.Net.Mail.SmtpClient oClient = new System.Net.Mail.SmtpClient(EXCHANGESERVER);
            System.Net.Mail.MailAddress oSender = new System.Net.Mail.MailAddress(SYSTEMEMAIL, "WORKFLOW SYSTEM");
            System.Net.Mail.MailMessage oMessage = new System.Net.Mail.MailMessage();
            System.Net.Mail.MailAddress oTo;

            //EmployeeData oEmp = new EmployeeData(Employeeid);
            oTo = new System.Net.Mail.MailAddress(oEmp.EmailAddress, oEmp.Name);
            oMessage.To.Add(oTo);

            oMessage.ReplyTo = oTo;
            oMessage.From = oSender;
            oMessage.Sender = oSender;
            oMessage.Subject = subjectAlert;
            oMessage.Body = string.Format("<a>{0}</a>", bodyAlert);
            oMessage.BodyEncoding = Encoding.UTF8;
            oMessage.IsBodyHtml = true;
            oClient.Send(oMessage);
            oMessage.Dispose();

            #endregion
        }

        #region Get InfoType1001_A002

        public DataTable getInfoType1001_A002(string OrgUnit)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select ObjectID from INFOTYPE1001_A002 Where ObjectType ='O' And NextObjectID = @OrgUnit and BeginDate >= @Date1 and EndDate <= @Date1", oConnection);

            SqlParameter oParam;
            oParam = new SqlParameter("@OrgUnit", SqlDbType.VarChar);
            oParam.Value = OrgUnit;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Date1", SqlDbType.DateTime);
            oParam.Value = DateTime.Now;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE1001_A002");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            return oTable;
        }

        #endregion Get InfoType1001_A002

        public override List<Substitution> GetInfotype2003(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            List<Substitution> returnValue = new List<Substitution>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE2003_Log Where @EmpID IN (EmployeeID,Substitute) and BeginDate < @Date2 and EndDate >= @Date1 ORDER BY RequestNo ASC", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Date1", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Date2", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE2003");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                Substitution item = new Substitution();
                item.ParseToObject(dr);
                returnValue.Add(item);
            }
            oTable.Dispose();
            return returnValue;
        }

        public override List<Substitution> GetInfotype2003(string EmployeeID, DateTime BeginDate, DateTime EndDate, string ExcludeReqNo)
        {
            List<Substitution> returnValue = new List<Substitution>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE2003_Log Where (EmployeeID = @EmpID OR Substitute=@EmpID) and BeginDate <= @Date2 and EndDate >= @Date1 AND RequestNo <> @ReqNo ORDER BY RequestNo ASC", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Date1", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Date2", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@ReqNo", SqlDbType.VarChar);
            oParam.Value = ExcludeReqNo;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE2003");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                Substitution item = new Substitution();
                item.ParseToObject(dr);
                returnValue.Add(item);
            }
            oTable.Dispose();
            return returnValue;
        }

        public override List<WorkPlaceCommunication> GetWorkplaceData(string EmployeeID)
        {
            List<WorkPlaceCommunication> oReturn = new List<WorkPlaceCommunication>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlParameter oParam;
            if (EmployeeID.Trim() == "")
            {
                oCommand = new SqlCommand("select * from WorkPlaceData where GetDate() between BeginDate and EndDate + '23:59:59'", oConnection);
            }
            else
            {
                oCommand = new SqlCommand("select * from WorkPlaceData where GetDate() between BeginDate and EndDate + '23:59:59' and EmployeeID = @EmployeeID", oConnection);
                oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
                oParam.Value = EmployeeID;
                oCommand.Parameters.Add(oParam);
            }
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EMPLOYEE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            WorkPlaceCommunication oWorkplace;
            foreach (DataRow dr in oTable.Rows)
            {
                oWorkplace = new WorkPlaceCommunication();
                oWorkplace.ParseToObject(dr);
                oReturn.Add(oWorkplace);
            }
            oTable.Dispose();
            return oReturn;
        }

        //CHAT 2011-10-05 For MassPayslip & MassTaxReport
        //CHAT 2011-10-05 ����Ѻ�֧���������ʾ�ѡ�ҹ��������˹��§ҹ�������

        #region "GetEmployeeIDByOrgUnit"

        public override DataTable GetEmployeeIDByOrgUnit(string OrgUnit)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select employeeid from infotype0001 where orgunit = @OrgUnit and BeginDate < GETDATE() AND EndDate > GETDATE() order by employeeid", oConnection);

            SqlParameter oParam;
            oParam = new SqlParameter("@OrgUnit", SqlDbType.VarChar);
            oParam.Value = OrgUnit;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE1001_A002");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            return oTable;
        }

        #endregion "GetEmployeeIDByOrgUnit"

        //CHAT 2011-10-05 ����Ѻ�֧������ ResponseType �ͧ��������͵�Ǩ�ͺ�Է�������ҹ��� Type

        #region " GetUserResponseType "

        public override List<string> GetUserResponseType(string EmpID, string UserRole)
        {
            List<string> oReturn = new List<string>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select distinct ResponseType from UserRoleResponse Where UserID = @UserID and UserRole in (@UserRole) ", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@UserID", SqlDbType.VarChar);
            oParam.Value = EmpID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@UserRole", SqlDbType.VarChar);
            oParam.Value = UserRole;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("USERROLERESPONSE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add((string)dr["ResponseType"]);
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion " GetUserResponseType "

        //CHAT 2011-10-10 ����Ѻ�֧������ Performance �ͧ��ѡ�ҹ

        #region " GetRequestorPerformance "

        public override DataTable GetRequestorPerformance(string EmpID)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("SELECT EmployeeID,Year,Grade FROM PERFORMANCE_INFOTYPE0147 WHERE EmployeeID = @EmpID ORDER BY Year DESC", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmpID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EMPLOYEEPERFORMANCE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();

            return oTable;
        }

        #endregion " GetRequestorPerformance "

        //CHAT 2011-10-10 ����Ѻ�֧������ Competency �ͧ��ѡ�ҹ

        #region " GetRequestorCompetency "

        public override DataTable GetRequestorCompetency(string EmpID)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("SELECT EmployeeID,Year,CompetencyID,Competency_Name,Percentage FROM COMPETENCY_INFOTYPE0024 WHERE EmployeeID = @EmpID ORDER BY Year DESC", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmpID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EMPLOYEECOMPETENCY");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();

            return oTable;
        }

        #endregion " GetRequestorCompetency "

        //AddBy: Ratchatawan W. (2012-02-22)
        public override INFOTYPE0007 GetInfotype0007(string EmployeeID, DateTime CheckDate, string Profile)
        {
            INFOTYPE0007 returnValue = new INFOTYPE0007();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_INFOTYPE0007GET", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE0007");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE0007 item = new INFOTYPE0007();
                item.ParseToObject(dr);
                returnValue = item;
            }

            oTable.Dispose();
            return returnValue;
        }

        //AddBy: Ratchatawan W. (2012-14-23)
        public override bool ValidateManager(string EmployeeID)
        {
            bool returnValue = false;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_ValidateManager", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter oParam;
            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oConnection.Open();
            returnValue = ((int)oCommand.ExecuteScalar() > 0);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            return returnValue;
        }

        public override List<EmployeeData> GetDelegateEmployeeOMByPosition(string EmployeeID, string PositionID)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_DelegateGetEmployeeOMByPosition", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_Position", SqlDbType.VarChar);
            oParam.Value = PositionID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("RECEIPIENT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();

            EmployeeData emp = new EmployeeData() { CompanyCode = CompanyCode };
            foreach (DataRow dr in oTable.Rows)
            {
                string cManagerType = (string)dr["RECEIPIENTTYPE"];
                string cManager = (string)dr["RECEIPIENTCODE"];
                switch (cManagerType.Trim().ToUpper())
                {
                    case "S":
                        emp = emp.GetEmployeeByPositionID(cManager, DateTime.Now);
                        if (emp != null)
                        {
                            if (!oReturn.Exists(delegate (EmployeeData empDat) { return (empDat.EmployeeID == emp.EmployeeID); }))
                                oReturn.Add(emp);
                        }
                        break;

                    case "P":
                        emp = new EmployeeData(cManager, DateTime.Now);
                        if (emp != null)
                        {
                            if (!oReturn.Exists(delegate (EmployeeData empDat) { return (empDat.EmployeeID == emp.EmployeeID); }))
                                oReturn.Add(emp);
                        }
                        break;
                }
            }
            oTable.Dispose();
            return oReturn;
        }

        public override List<EmployeeData> GetDelegateEmployeeForSentMail(string DelegateFromID, string DelegateFromPositionID, int RequestTypeID, DateTime CheckDate)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_DelegateGetEmployeeForSentMail", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            oParam = new SqlParameter("@p_DelegateFromID", SqlDbType.VarChar);
            oParam.Value = DelegateFromID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_DelegateFromPositionID", SqlDbType.VarChar);
            oParam.Value = DelegateFromPositionID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_RequestTypeID", SqlDbType.Int);
            oParam.Value = RequestTypeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("RECEIPIENT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();

            EmployeeData emp;
            string strEmployeeID;
            string strEmployeePositionID;
            foreach (DataRow dr in oTable.Rows)
            {
                strEmployeeID = (string)dr["DelegateTo"];
                strEmployeePositionID = (string)dr["DelegateToPosition"];
                emp = new EmployeeData(strEmployeeID, strEmployeePositionID, this.CompanyCode, DateTime.Now);
                oReturn.Add(emp);
            }
            oTable.Dispose();
            return oReturn;
        }

        //AddBy: Ratchatawan W. (2012-11-07)
        public override List<EmployeeData> GetAllActiveEmployeeInINFOTYPE0001(DateTime CheckDate)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlParameter oParam;
            SqlDataAdapter oAdapter;
            DataTable oTable;
            oCommand = new SqlCommand("SELECT * FROM dbo.INFOTYPE0001 WHERE (@CheckDate BETWEEN BeginDate AND EndDate)", oConnection);
            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);
            oAdapter = new SqlDataAdapter(oCommand);
            oTable = new DataTable("EMPLOYEEDATA");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();

            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(new EmployeeData((string)dr["EmployeeID"], CheckDate));
            }

            oAdapter.Dispose();
            oCommand.Dispose();
            oTable.Dispose();

            oConnection.Dispose();

            return oReturn;
        }

        //AddBy: Ratchatawan W. (2012-11-09)
        public override List<EmployeeData> GetManagerInSameOraganizationAndSameEmpSubGroup(string EmployeeID, string PositionID, DateTime CheckDate)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();
            EmployeeData empf = new EmployeeData() { CompanyCode = CompanyCode };
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlParameter oParam;
            SqlDataAdapter oAdapter;
            DataTable oTable;
            oCommand = new SqlCommand("sp_ApproverInSameOrganizeAndEmpsubgroupGet", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_EmployeePositionID", SqlDbType.VarChar);
            oParam.Value = PositionID;
            oCommand.Parameters.Add(oParam);
            oAdapter = new SqlDataAdapter(oCommand);
            oTable = new DataTable("EMPLOYEEDATA");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();

            foreach (DataRow dr in oTable.Rows)
            {
                EmployeeData emp = empf.GetEmployeeByPositionID((string)dr["PositionID"], CheckDate);
                if (emp != null)
                    oReturn.Add(emp);
            }

            oAdapter.Dispose();
            oCommand.Dispose();
            oTable.Dispose();

            oConnection.Dispose();

            return oReturn;
        }

        //AddBy: Ratchatawan W. (2012-12-24)
        public override void SaveInfotype0032(string EmployeeID1, string EmployeeID2, List<INFOTYPE0032> data, string profile)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand1 = new SqlCommand("delete from INFOTYPE0032 where EmployeeID between @EmpID1 and @EmpID2", oConnection, tx);
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand1.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand1.Parameters.Add(oParam);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE0032 where EmployeeID between @EmpID1 and @EmpID2", oConnection, tx);
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oAdapter.UpdateBatchSize = 10000;
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("INFOTYPE0032");
            try
            {
                oCommand1.ExecuteNonQuery();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                foreach (INFOTYPE0032 item in data)
                {
                    item.EndDate = item.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);
                    item.LoadDataToTable(oTable);
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand1.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }

        public override INFOTYPE0032 GetInfotype0032ByEmployeeID(string EmployeeID, DateTime CheckDate)
        {
            INFOTYPE0032 oReturn = new INFOTYPE0032();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_INFOTYPE0032GET", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE0032");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
                oReturn.ParseToObject(dr);
            oTable.Dispose();

            return oReturn;
        }

        public override List<EmployeeData> GetOTSummaryPermissionByEmployeeID(string EmployeeID, DateTime CheckDate)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_OTSummaryPermissionGetByEmployeeID", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataSet ds = new DataSet();
            oConnection.Open();
            oAdapter.Fill(ds);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();

            EmployeeData oEmp;
            foreach (DataRow dr in ds.Tables[1].Rows)
            {
                oEmp = new EmployeeData(dr[0].ToString(), CheckDate);
                if (oEmp != null)
                    oReturn.Add(oEmp);
            }
            ds.Dispose();

            return oReturn;
        }

        public override bool IsHaveOTSummaryPermissionByEmployeeID(string EmployeeID, DateTime CheckDate)
        {
            bool oReturn = false;

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_OTSummaryPermissionGetByEmployeeID", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataSet ds = new DataSet();
            oConnection.Open();
            oAdapter.Fill(ds);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();

            if (ds.Tables[0].Rows.Count > 0)
                oReturn = true;
            ds.Dispose();

            return oReturn;
        }

        public override List<INFOTYPE0001> GetAllINFOTYPE0001(DateTime CheckDate)
        {
            List<INFOTYPE0001> oReturn = new List<INFOTYPE0001>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_INFOTYPE0001GetAll", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            oParam = new SqlParameter("@p_CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE0001");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE0001 oInf1 = new INFOTYPE0001();
                oInf1.ParseToObject(dr);
                oReturn.Add(oInf1);
            }
            oTable.Dispose();

            return oReturn;
        }

        public override List<INFOTYPE0001> GetAllEmployeeName(string Language)
        {
            List<INFOTYPE0001> oReturn = new List<INFOTYPE0001>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_EmployeeNameGetAll", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            oParam = new SqlParameter("@p_LanguageCode", SqlDbType.VarChar);
            oParam.Value = Language;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE0001");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE0001 oInf1 = new INFOTYPE0001();
                oInf1.ParseToObject(dr);
                oReturn.Add(oInf1);
            }
            oTable.Dispose();

            return oReturn;
        }

        public override List<INFOTYPE0001> GetAllEmployeeNameForUserRole(string Language)
        {
            List<INFOTYPE0001> oReturn = new List<INFOTYPE0001>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_EmployeeNameGetAllForUserRole", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            oParam = new SqlParameter("@p_LanguageCode", SqlDbType.VarChar);
            oParam.Value = Language;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE0001");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE0001 oInf1 = new INFOTYPE0001();
                oInf1.ParseToObject(dr);
                oReturn.Add(oInf1);
            }
            oTable.Dispose();

            return oReturn;
        }

        public override List<INFOTYPE0001> GetAllInfotype0001ForReport(DateTime oCheckDate, string oLanguage)
        {
            //sp_INFOTYPE0001_ReportGetAll
            oSqlManage["ProcedureName"] = "sp_INFOTYPE0001_ReportGetAll";
            List<INFOTYPE0001> oResult = new List<INFOTYPE0001>();
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_CheckDate"] = oCheckDate;
            oParamRequest["@p_Language"] = oLanguage;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<INFOTYPE0001>(oSqlManage, oParamRequest));
            return oResult;
        }

        public override DataSet GetUserInResponseForActionOfInstead(string EmployeeID, int SubjectID, DateTime CheckDate, string ResponseCompanyCode, string SearhText)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_UserInResponseForActionOfInsteadGet", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;

            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_SubjectID", SqlDbType.Int);
            oParam.Value = SubjectID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_ResponseCompanyCode", SqlDbType.VarChar);
            oParam.Value = ResponseCompanyCode;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_SearhText", SqlDbType.VarChar);
            oParam.Value = SearhText;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataSet oTable = new DataSet("USER");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            return oTable;
        }

        public override DataTable GetUserInResponseForActionOfInsteadByResponseType(string ResponseType, string ResponseCode, string ResponseCompanyCode, bool IncludeSub, DateTime CheckDate, string SearchText)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_UserInResponseForActionOfInsteadByResponseType", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            oCommand.Parameters.Add(new SqlParameter("@p_ResponseType", SqlDbType.VarChar)).Value = ResponseType;
            oCommand.Parameters.Add(new SqlParameter("@p_ResponseCode", SqlDbType.VarChar)).Value = ResponseCode;
            oCommand.Parameters.Add(new SqlParameter("@p_ResponseCompanyCode", SqlDbType.VarChar)).Value = ResponseCompanyCode;
            oCommand.Parameters.Add(new SqlParameter("@p_IncludeSub", SqlDbType.Bit)).Value = IncludeSub;
            oCommand.Parameters.Add(new SqlParameter("@p_CheckDate", SqlDbType.DateTime)).Value = CheckDate;
            oCommand.Parameters.Add(new SqlParameter("@p_SearchText", SqlDbType.VarChar)).Value = SearchText;

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EmployeeInResponse");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            return oTable;
        }

        #region " Infotype0030 "

        public override void SaveInfotype0030(string EmployeeID1, string EmployeeID2, List<INFOTYPE0030> data, string profile)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand1 = new SqlCommand("delete from INFOTYPE0030 where EmployeeID between @EmpID1 and @EmpID2", oConnection, tx);
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand1.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand1.Parameters.Add(oParam);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE0030 where EmployeeID between @EmpID1 and @EmpID2", oConnection, tx);
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oAdapter.UpdateBatchSize = 10000;
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("INFOTYPE0030");
            try
            {
                oCommand1.ExecuteNonQuery();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                foreach (INFOTYPE0030 item in data)
                {
                    //AddBy: Ratchatawan W. (2012-02-10)
                    item.EndDate = item.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);
                    item.LoadDataToTable(oTable);
                }

                oAdapter.Update(oTable);
                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand1.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }

        public override INFOTYPE0030 GetInfotype0030(string EmployeeID, DateTime CheckDate)
        {
            INFOTYPE0030 oReturn = new INFOTYPE0030();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_INFOTYPE0030GET", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE0032");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
                oReturn.ParseToObject(dr);
            oTable.Dispose();

            return oReturn;
        }

        #endregion " Infotype0030 "

        #region "Employee Data Setting "

        #region " Get Data "
        public override List<INFOTYPE0001> INFOTYPE0001GetAllEmployeeHistory(string Language)
        {
            List<INFOTYPE0001> oReturn = new List<INFOTYPE0001>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            oCommand = new SqlCommand("sp_INFOTYPE0001GetAllHistory", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_Language", Language);
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE0001 item = new INFOTYPE0001();
                item.ParseToObject(dr);
                item.CompanyCode = CompanyCode;
                oReturn.Add(item);
            }
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            if (oAdapter != null)
            {
                oAdapter.Dispose();
            }
            oTable.Dispose();
            return oReturn;
        }

        public override Dictionary<string, object> INFOTYPE0002And0182GetAllHistory(string EmployeeID)
        {
            Dictionary<string, object> oReturn = new Dictionary<string, object>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            oCommand = new SqlCommand("sp_INFOTYPE0002And0182GetAllHistory", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_EmployeeID", EmployeeID);
            oCommand.Parameters.AddWithValue("@p_BeginDate", new DateTime(9999, 12, 31));
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            List<INFOTYPE0002> oLstINF2 = new List<INFOTYPE0002>();
            List<INFOTYPE0182> oLstINF182 = new List<INFOTYPE0182>();
            //List<string> lang = ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetLanguageDropdownData();
            oConnection.Open();
            oAdapter.Fill(oTable);
            foreach (DataRow dr in oTable.Rows)
            {
                oLstINF2.Add(new INFOTYPE0002
                {
                    BeginDate = DateTime.ParseExact(dr["BeginDate"].ToString(), "dd/MM/yyyy", null),
                    EndDate = DateTime.ParseExact(dr["EndDate"].ToString(), "dd/MM/yyyy", null),
                    TitleID = dr["Prefix"].ToString(),
                    FirstName = dr["FirstName"].ToString(),
                    LastName = dr["LastName"].ToString(),
                    NickName = dr["NickName"].ToString(),
                    DOB = string.IsNullOrEmpty(dr["BirthDay"].ToString()) ? DateTime.MinValue : DateTime.ParseExact(dr["BirthDay"].ToString(), "dd/MM/yyyy", null),
                    Gender = dr["Gender"].ToString(),
                    MaritalStatus = dr["MaritalStatus"].ToString(),
                    Nationality = dr["Nationality"].ToString(),
                    //Language = lang[lang.IndexOf(lang.Find(x => x.Contains(dr["Language"].ToString())))],
                    Language = dr["Language"].ToString(),
                    Religion = dr["Religion"].ToString()
                });
                oLstINF182.Add(new INFOTYPE0182
                {
                    BeginDate = DateTime.ParseExact(dr["BeginDate"].ToString(), "dd/MM/yyyy", null),
                    EndDate = DateTime.ParseExact(dr["EndDate"].ToString(), "dd/MM/yyyy", null),
                    AlternateName = dr["FullNameEn"].ToString()
                });
            }
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            if (oAdapter != null)
            {
                oAdapter.Dispose();
            }
            oReturn.Add("INFOTYPE0002", oLstINF2);
            //oReturn["INFOTYPE0002"] = oLstINF2;
            oReturn.Add("INFOTYPE0182", oLstINF182);
            //oReturn["INFOTYPE0182"] = oLstINF182;
            oTable.Dispose();
            return oReturn;
        }

        public override List<INFOTYPE0001> INFOTYPE0001GetAllHistory(string EmployeeID, string Language)
        {
            List<INFOTYPE0001> oReturn = new List<INFOTYPE0001>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            string cmdStr = "sp_INFOTYPE0001Get";
            oCommand = new SqlCommand(cmdStr, oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_EmployeeID", EmployeeID);
            oCommand.Parameters.AddWithValue("@p_Language", Language);
            oCommand.Parameters.AddWithValue("@p_BeginDate", new DateTime(9999, 12, 31));
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE0001 item = new INFOTYPE0001();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oTable.Dispose();
            return oReturn;
        }

        public override List<INFOTYPE0105> INFOTYPE0105GetAllHistory(string EmployeeID)
        {
            List<INFOTYPE0105> oReturn = new List<INFOTYPE0105>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            oCommand = new SqlCommand("sp_INFOTYPE0105GetAllHistory", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_EmployeeID", EmployeeID);
            oCommand.Parameters.AddWithValue("@p_BeginDate", new DateTime(9999, 12, 31));
            oCommand.Parameters.AddWithValue("@p_CategoryCode", "");
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE0105 item = new INFOTYPE0105();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oTable.Dispose();
            return oReturn;
        }

        public override Dictionary<string, object> INFOTYPE0002And0182Get(string EmployeeID, DateTime BeginDate)
        {
            Dictionary<string, object> oReturn = new Dictionary<string, object>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            oCommand = new SqlCommand("sp_INFOTYPE0002And0182GetAllHistory", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_EmployeeID", EmployeeID);
            if (BeginDate != DateTime.MinValue) oCommand.Parameters.AddWithValue("@p_BeginDate", BeginDate);
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            List<INFOTYPE0002> oLstINF2 = new List<INFOTYPE0002>();
            List<INFOTYPE0182> oLstINF182 = new List<INFOTYPE0182>();
            //List<INFOTYPE1000> lang = ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetLanguageDropdownData();
            oConnection.Open();
            oAdapter.Fill(oTable);
            foreach (DataRow dr in oTable.Rows)
            {
                oLstINF2.Add(new INFOTYPE0002
                {
                    BeginDate = DateTime.ParseExact(dr["BeginDate"].ToString(), "dd/MM/yyyy", null),
                    EndDate = DateTime.ParseExact(dr["EndDate"].ToString(), "dd/MM/yyyy", null),
                    TitleID = dr["Prefix"].ToString(),
                    FirstName = dr["FirstName"].ToString(),
                    LastName = dr["LastName"].ToString(),
                    NickName = dr["NickName"].ToString(),
                    DOB = string.IsNullOrEmpty(dr["BirthDay"].ToString()) ? DateTime.MinValue : DateTime.ParseExact(dr["BirthDay"].ToString(), "dd/MM/yyyy", null),
                    Gender = dr["Gender"].ToString(),
                    MaritalStatus = dr["MaritalStatus"].ToString(),
                    Nationality = dr["Nationality"].ToString(),
                    //Language = lang[lang.IndexOf(lang.Find(x => x.Contains(dr["Language"].ToString())))],
                    Language = dr["Language"].ToString(),
                    Religion = dr["Religion"].ToString()
                });


                oLstINF182.Add(new INFOTYPE0182
                {
                    BeginDate = DateTime.ParseExact(dr["BeginDate"].ToString(), "dd/MM/yyyy", null),
                    EndDate = DateTime.ParseExact(dr["EndDate"].ToString(), "dd/MM/yyyy", null),
                    AlternateName = dr["FullNameEn"].ToString()
                });
            }
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            if (oAdapter != null)
            {
                oAdapter.Dispose();
            }
            oReturn.Add("INFOTYPE0002", oLstINF2);
            oReturn.Add("INFOTYPE0182", oLstINF182);
            oTable.Dispose();
            return oReturn;
        }

        public override INFOTYPE0001 INFOTYPE0001Get(string EmployeeID, DateTime BeginDate, string Language)
        {
            INFOTYPE0001 oReturn = new INFOTYPE0001();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            string cmdStr = "sp_INFOTYPE0001Get";
            oCommand = new SqlCommand(cmdStr, oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_EmployeeID", EmployeeID);
            if (BeginDate != DateTime.MinValue) oCommand.Parameters.AddWithValue("@p_BeginDate", BeginDate);
            oCommand.Parameters.AddWithValue("@p_Language", Language);
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE0001 item = new INFOTYPE0001();
                item.ParseToObject(dr);
                oReturn = item;
            }

            oTable.Dispose();
            return oReturn;
        }

        public override INFOTYPE0105 INFOTYPE0105Get(string EmployeeID, DateTime BeginDate, string CategoryCode)
        {
            INFOTYPE0105 oReturn = new INFOTYPE0105();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            oCommand = new SqlCommand("sp_INFOTYPE0105GetAllHistory", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_EmployeeID", EmployeeID);
            if (BeginDate != DateTime.MinValue) oCommand.Parameters.AddWithValue("@p_BeginDate", BeginDate);
            oCommand.Parameters.AddWithValue("@p_CategoryCode", CategoryCode);
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE0105 item = new INFOTYPE0105();
                item.ParseToObject(dr);
                oReturn = item;
            }

            oTable.Dispose();
            return oReturn;
        }
        #endregion

        #region " Delete Employee Data "
        public override void DeleteINFOTYPE0002And0182And0001And0105And1001(string EmployeeID)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            oCommand = new SqlCommand("sp_INFOTYPE0002And0182And0001And0105And1001Delete", oConnection, tx);
            oCommand.Parameters.AddWithValue("@p_EmployeeID", EmployeeID);
            oCommand.CommandType = CommandType.StoredProcedure;
            oAdapter = new SqlDataAdapter(oCommand);
            string oAction = "DELETE EMPLOYEE ";
            bool status = false;
            try
            {
                oCommand.ExecuteNonQuery();
                tx.Commit();
                status = true;
                oAction += "Complete";
            }
            catch (Exception ex)
            {
                tx.Rollback();
                oAction += "Failed : " + ex.Message;
                throw new Exception("delete data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                CallActionLog(null, null, oAction + " Where EmployeeID = " + EmployeeID, status);
            }
        }

        public override void DeleteINFOTYPE0002And0182(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand;
            oCommand = new SqlCommand("sp_INFOTYPE0002And0182Delete", oConnection, tx);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_EmployeeID", EmployeeID);
            oCommand.Parameters.AddWithValue("@p_BeginDate", BeginDate);
            oCommand.Parameters.AddWithValue("@p_EndDate", EndDate);
            string oAction = "DELETE INFOTYPE0002And0182 ";
            bool status = false;
            try
            {
                oCommand.ExecuteNonQuery();
                tx.Commit();
                status = true;
                oAction += "Complete";
            }
            catch (Exception ex)
            {
                tx.Rollback();
                oAction += "Failed : " + ex.Message;
                throw new Exception("delete data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                CallActionLog(null, null, oAction + " Where EmployeeID = " + EmployeeID + " and BeginDate = " + BeginDate + " and EndDate = " + EndDate, status);
            }
        }

        public override void DeleteINFOTYPE0001(string EmployeeID, DateTime BeginDate)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand;
            oCommand = new SqlCommand("sp_INFOTYPE0001Delete", oConnection, tx);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_EmployeeID", EmployeeID);
            oCommand.Parameters.AddWithValue("@p_BeginDate", BeginDate);
            string oAction = "DELETE INFOTYPE0001 ";
            bool status = false;
            try
            {
                oCommand.ExecuteNonQuery();
                tx.Commit();
                status = true;
                oAction += "Complete";
            }
            catch (Exception ex)
            {
                tx.Rollback();
                oAction += "Failed : " + ex.Message;
                throw new Exception("delete data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                CallActionLog(null, null, oAction + " Where EmployeeID = " + EmployeeID + "and BeginDate = " + BeginDate, status);
            }
        }

        public override void DeleteINFOTYPE0105(string EmployeeID, DateTime BeginDate, string SubType)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand;
            oCommand = new SqlCommand("sp_INFOTYPE0105Delete", oConnection, tx);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_EmployeeID", EmployeeID);
            oCommand.Parameters.AddWithValue("@p_BeginDate", BeginDate);
            oCommand.Parameters.AddWithValue("@p_SubType", SubType);
            string oAction = "DELETE INFOTYPE0105 ";
            bool status = false;
            try
            {
                oCommand.ExecuteNonQuery();
                tx.Commit();
                status = true;
                oAction += "Complete";
            }
            catch (Exception ex)
            {
                tx.Rollback();
                oAction += "Failed : " + ex.Message;
                throw new Exception("delete data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                CallActionLog(null, null, oAction + " Where EmployeeID = " + EmployeeID + " and BeginDate = " + BeginDate + " and CategoryCode = " + SubType, status);
            }
        }

        public override void Resign(string EmployeeID, DateTime EndDate)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand;
            oCommand = new SqlCommand("sp_INFOTYPE_ForResign", oConnection, tx);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_EmployeeID", EmployeeID);
            oCommand.Parameters.AddWithValue("@p_EndDate", EndDate);
            string oAction = "RESIGN ";
            bool status = false;
            try
            {
                oCommand.ExecuteNonQuery();
                tx.Commit();
                status = true;
                oAction += "Complete";
            }
            catch (Exception ex)
            {
                tx.Rollback();
                oAction += "Failed : " + ex.Message;
                throw new Exception("delete data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                CallActionLog(null, null, oAction + " Where EmployeeID = " + EmployeeID + " and EndDate = " + EndDate, status);
            }
        }
        #endregion

        #region " Insert & Update Data "
        public override string InsertINFOTYPE0002And0182(INFOTYPE0002 oINF2, INFOTYPE0182 oINF182)
        {
            string oReturn = "";
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            string queryStr = "sp_INFOTYPE0002And0182Insert";
            oCommand = new SqlCommand(queryStr, oConnection, tx);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_EmployeeID", oINF2.EmployeeID == null ? "" : oINF2.EmployeeID);
            oCommand.Parameters.AddWithValue("@p_BeginDate", oINF2.BeginDate);
            oCommand.Parameters.AddWithValue("@p_EndDate", oINF2.EndDate == DateTime.MaxValue.AddTicks(-9999999) ? oINF2.EndDate : oINF2.EndDate.TimeOfDay.Seconds == 59 ? oINF2.EndDate : oINF2.EndDate.AddSeconds(-1).AddDays(1));
            oCommand.Parameters.AddWithValue("@p_TitleID", oINF2.TitleID);
            oCommand.Parameters.AddWithValue("@p_FirstName", oINF2.FirstName);
            oCommand.Parameters.AddWithValue("@p_LastName", oINF2.LastName);
            oCommand.Parameters.AddWithValue("@p_FullNameEn", oINF182.AlternateName);
            oCommand.Parameters.AddWithValue("@p_NickName", oINF2.NickName);
            oCommand.Parameters.AddWithValue("@p_DOB", oINF2.DOB);
            oCommand.Parameters.AddWithValue("@p_Gender", oINF2.Gender);
            oCommand.Parameters.AddWithValue("@p_Nationality", oINF2.Nationality);
            oCommand.Parameters.AddWithValue("@p_MaritalStatus", oINF2.MaritalStatus);
            oCommand.Parameters.AddWithValue("@p_Religion", oINF2.Religion);
            oCommand.Parameters.AddWithValue("@p_Language", oINF2.Language.Split(':')[0]);
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            string oAction = "INSERT INFOTYPE0002 INFOTYPE0182 ";
            bool oStatus = false;
            try
            {
                oAdapter.Fill(oTable);
                oReturn = oTable.Rows[0].ItemArray[0].ToString();
                tx.Commit();
                oAction += "Completed";
                oStatus = true;
            }
            catch (Exception ex)
            {
                tx.Rollback();
                oAction += "Failed : " + ex.Message;
                throw new Exception("insert data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                oTable.Dispose();
                CallActionLog(oINF2, oINF182, oAction, oStatus);
            }
            return oReturn;
        }

        public override void UpdateINFOTYPE0002And0182(INFOTYPE0002 oINF2, INFOTYPE0182 oINF182)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            string queryStr = "sp_INFOTYPE0002And0182Update";
            oCommand = new SqlCommand(queryStr, oConnection, tx);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_EmployeeID", oINF2.EmployeeID);
            oCommand.Parameters.AddWithValue("@p_BeginDate", oINF2.BeginDate);
            oCommand.Parameters.AddWithValue("@p_EndDate", oINF2.EndDate == DateTime.MaxValue.AddTicks(-9999999) ? oINF2.EndDate : oINF2.EndDate.TimeOfDay.Seconds == 59 ? oINF2.EndDate : oINF2.EndDate.AddSeconds(-1).AddDays(1));
            oCommand.Parameters.AddWithValue("@p_TitleID", oINF2.TitleID);
            oCommand.Parameters.AddWithValue("@p_FirstName", oINF2.FirstName);
            oCommand.Parameters.AddWithValue("@p_LastName", oINF2.LastName);
            oCommand.Parameters.AddWithValue("@p_FullNameEn", oINF182.AlternateName);
            oCommand.Parameters.AddWithValue("@p_NickName", oINF2.NickName);
            oCommand.Parameters.AddWithValue("@p_DOB", oINF2.DOB);
            oCommand.Parameters.AddWithValue("@p_Gender", oINF2.Gender);
            oCommand.Parameters.AddWithValue("@p_Nationality", oINF2.Nationality);
            oCommand.Parameters.AddWithValue("@p_MaritalStatus", oINF2.MaritalStatus);
            oCommand.Parameters.AddWithValue("@p_Religion", oINF2.Religion);
            oCommand.Parameters.AddWithValue("@p_Language", oINF2.Language.Split(':')[0]);
            oAdapter = new SqlDataAdapter(oCommand);
            string oAction = "UPDATE INFOTYPE0002 INFOTYPE0182 ";
            bool oStatus = false;
            try
            {
                oCommand.ExecuteNonQuery();
                tx.Commit();
                oAction += "Completed";
                oStatus = true;
            }
            catch (Exception ex)
            {
                tx.Rollback();
                oAction += "Failed : " + ex.Message;
                throw new Exception("update data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                CallActionLog(oINF2, oINF182, oAction, oStatus);
            }
        }

        public override void InsertINFOTYPE0001(INFOTYPE0001 oINF1)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand;
            string queryStr = "sp_INFOTYPE0001Insert";
            oCommand = new SqlCommand(queryStr, oConnection, tx);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_EmployeeID", oINF1.EmployeeID);
            oCommand.Parameters.AddWithValue("@p_BeginDate", oINF1.BeginDate);
            oCommand.Parameters.AddWithValue("@p_EndDate", oINF1.EndDate == DateTime.MaxValue.AddTicks(-9999999) ? oINF1.EndDate : oINF1.EndDate.TimeOfDay.Seconds == 59 ? oINF1.EndDate : oINF1.EndDate.AddSeconds(-1).AddDays(1));
            oCommand.Parameters.AddWithValue("@p_CompanyCode", oINF1.CompanyCode);
            oCommand.Parameters.AddWithValue("@p_Name", oINF1.Name);
            oCommand.Parameters.AddWithValue("@p_Area", oINF1.Area);
            oCommand.Parameters.AddWithValue("@p_SubArea", oINF1.SubArea);
            oCommand.Parameters.AddWithValue("@p_EmpGroup", oINF1.EmpGroup);
            oCommand.Parameters.AddWithValue("@p_EmpSubGroup", oINF1.EmpSubGroup);
            oCommand.Parameters.AddWithValue("@p_OrgUnit", oINF1.OrgUnit);
            oCommand.Parameters.AddWithValue("@p_Position", oINF1.Position);
            oCommand.Parameters.AddWithValue("@p_CostCenter", oINF1.CostCenter);
            string oAction = "INSERT INFOTYPE00001 ";
            bool oStatus = false;
            try
            {
                oCommand.ExecuteNonQuery();
                tx.Commit();
                oAction += "Completed";
                oStatus = true;
            }
            catch (Exception ex)
            {
                tx.Rollback();
                oAction += "Failed : " + ex.Message;
                throw new Exception("insert data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                CallActionLog(null, oINF1, oAction, oStatus);
            }
        }

        public override void UpdateINFOTYPE0001(INFOTYPE0001 oINF1)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            string queryStr = "sp_INFOTYPE0001Update";
            oCommand = new SqlCommand(queryStr, oConnection, tx);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_EmployeeID", oINF1.EmployeeID);
            oCommand.Parameters.AddWithValue("@p_BeginDate", oINF1.BeginDate);
            oCommand.Parameters.AddWithValue("@p_EndDate", oINF1.EndDate == DateTime.MaxValue.AddTicks(-9999999) ? oINF1.EndDate : oINF1.EndDate.TimeOfDay.Seconds == 59 ? oINF1.EndDate : oINF1.EndDate.AddSeconds(-1).AddDays(1));
            oCommand.Parameters.AddWithValue("@p_Name", oINF1.Name);
            oCommand.Parameters.AddWithValue("@p_Area", oINF1.Area);
            oCommand.Parameters.AddWithValue("@p_SubArea", oINF1.SubArea);
            oCommand.Parameters.AddWithValue("@p_EmpGroup", oINF1.EmpGroup);
            oCommand.Parameters.AddWithValue("@p_EmpSubGroup", oINF1.EmpSubGroup);
            oCommand.Parameters.AddWithValue("@p_OrgUnit", oINF1.OrgUnit);
            oCommand.Parameters.AddWithValue("@p_Position", oINF1.Position);
            oCommand.Parameters.AddWithValue("@p_CostCenter", oINF1.CostCenter);
            oAdapter = new SqlDataAdapter(oCommand);
            string oAction = "UPDATE INFOTYPE0001 ";
            bool oStatus = false;
            try
            {
                oCommand.ExecuteNonQuery();
                tx.Commit();
                oAction += "Completed";
                oStatus = true;
            }
            catch (Exception ex)
            {
                tx.Rollback();
                oAction += "Failed : " + ex.Message;
                throw new Exception("update data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                CallActionLog(null, oINF1, oAction, oStatus);
            }
        }

        public override void InsertINFOTYPE0105(INFOTYPE0105 oINF105)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand;
            string queryStr = "sp_INFOTYPE0105Insert";
            oCommand = new SqlCommand(queryStr, oConnection, tx);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_EmployeeID", oINF105.EmployeeID);
            oCommand.Parameters.AddWithValue("@p_BeginDate", oINF105.BeginDate);
            oCommand.Parameters.AddWithValue("@p_EndDate", oINF105.EndDate == DateTime.MaxValue.AddTicks(-9999999) ? oINF105.EndDate : oINF105.EndDate.TimeOfDay.Seconds == 59 ? oINF105.EndDate : oINF105.EndDate.AddSeconds(-1).AddDays(1));
            oCommand.Parameters.AddWithValue("@p_SubType", oINF105.CategoryCode);
            oCommand.Parameters.AddWithValue("@p_DataText_Long", oINF105.DataText);
            string oAction = "INSERT INFOTYPE0105 ";
            bool oStatus = false;
            try
            {
                oCommand.ExecuteNonQuery();
                tx.Commit();
                oAction += "Completed";
                oStatus = true;
            }
            catch (Exception ex)
            {
                tx.Rollback();
                oAction += "Failed : " + ex.Message;
                throw new Exception("insert data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                CallActionLog(null, oINF105, oAction, oStatus);
            }
        }

        public override void UpdateINFOTYPE0105(INFOTYPE0105 oINF105)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            string queryStr = "sp_INFOTYPE0105Update";
            oCommand = new SqlCommand(queryStr, oConnection, tx);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_EmployeeID", oINF105.EmployeeID);
            oCommand.Parameters.AddWithValue("@p_BeginDate", oINF105.BeginDate);
            oCommand.Parameters.AddWithValue("@p_EndDate", oINF105.EndDate == DateTime.MaxValue.AddTicks(-9999999) ? oINF105.EndDate : oINF105.EndDate.TimeOfDay.Seconds == 59 ? oINF105.EndDate : oINF105.EndDate.AddSeconds(-1).AddDays(1));
            oCommand.Parameters.AddWithValue("@p_SubType", oINF105.CategoryCode);
            oCommand.Parameters.AddWithValue("@p_DataText_Long", oINF105.DataText);
            oAdapter = new SqlDataAdapter(oCommand);
            string oAction = "UPDATE INFOTYPE0105 ";
            bool oStatus = false;
            try
            {
                oCommand.ExecuteNonQuery();
                tx.Commit();
                oAction += "Completed";
                oStatus = true;
            }
            catch (Exception ex)
            {
                tx.Rollback();
                oAction += "Failed : " + ex.Message;
                throw new Exception("update data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                CallActionLog(null, oINF105, oAction, oStatus);
            }
        }

        public override string InsertINFOTYPE0002And0182And0001And0105(INFOTYPE0002 oINF2, INFOTYPE0182 oINF182, INFOTYPE0001 oINF1, INFOTYPE0105 oINF105)
        {
            string oReturn = "";
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand, oCommand1, oCommand2;
            SqlDataAdapter oAdapter;
            oCommand = new SqlCommand("sp_INFOTYPE0002And0182Insert", oConnection, tx);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_EmployeeID", string.IsNullOrEmpty(oINF2.EmployeeID) ? "" : oINF2.EmployeeID);
            oCommand.Parameters.AddWithValue("@p_BeginDate", oINF2.BeginDate);
            oCommand.Parameters.AddWithValue("@p_EndDate", oINF2.EndDate == DateTime.MaxValue.AddTicks(-9999999) ? oINF2.EndDate : oINF2.EndDate.TimeOfDay.Seconds == 59 ? oINF2.EndDate : oINF2.EndDate.AddSeconds(-1).AddDays(1));
            oCommand.Parameters.AddWithValue("@p_TitleID", oINF2.TitleID);
            oCommand.Parameters.AddWithValue("@p_FirstName", oINF2.FirstName);
            oCommand.Parameters.AddWithValue("@p_LastName", oINF2.LastName);
            oCommand.Parameters.AddWithValue("@p_FullNameEn", oINF182.AlternateName);
            oCommand.Parameters.AddWithValue("@p_NickName", oINF2.NickName);
            oCommand.Parameters.AddWithValue("@p_DOB", oINF2.DOB);
            oCommand.Parameters.AddWithValue("@p_Gender", oINF2.Gender);
            oCommand.Parameters.AddWithValue("@p_Nationality", oINF2.Nationality);
            oCommand.Parameters.AddWithValue("@p_MaritalStatus", oINF2.MaritalStatus);
            oCommand.Parameters.AddWithValue("@p_Religion", oINF2.Religion);
            oCommand.Parameters.AddWithValue("@p_Language", oINF2.Language);
            //oCommand.Parameters.AddWithValue("@p_IsNewEmployee", true);
            oAdapter = new SqlDataAdapter(oCommand);
            oCommand1 = new SqlCommand("sp_INFOTYPE0001Insert", oConnection, tx);
            oCommand1.CommandType = CommandType.StoredProcedure;
            oCommand1.Parameters.AddWithValue("@p_BeginDate", oINF1.BeginDate);
            oCommand1.Parameters.AddWithValue("@p_EndDate", oINF1.EndDate == DateTime.MaxValue.AddTicks(-9999999) ? oINF1.EndDate : oINF1.EndDate.TimeOfDay.Seconds == 59 ? oINF1.EndDate : oINF1.EndDate.AddSeconds(-1).AddDays(1));
            oCommand1.Parameters.AddWithValue("@p_CompanyCode", oINF1.CompanyCode);
            oCommand1.Parameters.AddWithValue("@p_Name", oINF1.Name);
            oCommand1.Parameters.AddWithValue("@p_Area", oINF1.Area);
            oCommand1.Parameters.AddWithValue("@p_SubArea", oINF1.SubArea);
            oCommand1.Parameters.AddWithValue("@p_EmpGroup", oINF1.EmpGroup);
            oCommand1.Parameters.AddWithValue("@p_EmpSubGroup", oINF1.EmpSubGroup);
            oCommand1.Parameters.AddWithValue("@p_OrgUnit", oINF1.OrgUnit);
            oCommand1.Parameters.AddWithValue("@p_Position", oINF1.Position);
            oCommand1.Parameters.AddWithValue("@p_CostCenter", oINF1.CostCenter);
            oCommand2 = new SqlCommand("sp_INFOTYPE0105Insert", oConnection, tx);
            oCommand2.CommandType = CommandType.StoredProcedure;
            oCommand2.Parameters.AddWithValue("@p_BeginDate", oINF105.BeginDate);
            oCommand2.Parameters.AddWithValue("@p_EndDate", oINF105.EndDate == DateTime.MaxValue.AddTicks(-9999999) ? oINF105.EndDate : oINF105.EndDate.TimeOfDay.Seconds == 59 ? oINF105.EndDate : oINF105.EndDate.AddSeconds(-1).AddDays(1));
            oCommand2.Parameters.AddWithValue("@p_SubType", oINF105.CategoryCode);
            oCommand2.Parameters.AddWithValue("@p_DataText_Long", oINF105.DataText);
            DataTable oTable = new DataTable();
            string oAction = "ADD NEW EMPLOYEE ";
            bool oStatus = false;
            try
            {
                oAdapter.Fill(oTable);
                oReturn = oTable.Rows[0].ItemArray[0].ToString();
                if (oReturn.Length >= 8)
                {
                    oCommand1.Parameters.AddWithValue("@p_EmployeeID", oReturn);
                    oCommand2.Parameters.AddWithValue("@p_EmployeeID", oReturn);
                    oCommand1.ExecuteNonQuery();
                    oCommand2.ExecuteNonQuery();
                    tx.Commit();
                    oAction += "Completed EmployeeID = " + oReturn;
                    oStatus = true;
                }
                else
                {
                    tx.Rollback();
                    oAction += "Failed";
                }
            }
            catch (Exception ex)
            {
                tx.Rollback();
                oAction += "Failed : " + ex.Message;
                throw new Exception("insert data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                oCommand1.Dispose();
                oCommand2.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                oTable.Dispose();
                CallActionLog(null, null, oAction, oStatus);
            }
            return oReturn;
        }
        #endregion
        #endregion


        #region " OM "

        #region " Get Data "
        public override List<INFOTYPE1000> INFOTYPE1000GetAllHistory()
        {
            List<INFOTYPE1000> oReturn = new List<INFOTYPE1000>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            oCommand = new SqlCommand("sp_INFOTYPE1000GetAllHistory", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_ObjectType", "");
            oCommand.Parameters.AddWithValue("@p_ObjectID", "");
            oCommand.Parameters.AddWithValue("@p_BeginDate", new DateTime(9999, 12, 31));
            oCommand.Parameters.AddWithValue("@p_EndDate", new DateTime(9999, 12, 31));
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1000 item = new INFOTYPE1000();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            if (oAdapter != null)
            {
                oAdapter.Dispose();
            }
            oTable.Dispose();
            return oReturn;
        }

        public override INFOTYPE1000 INFOTYPE1000Get(string ObjectType, string ObjectID, DateTime BeginDate, DateTime EndDate)
        {
            INFOTYPE1000 oReturn = new INFOTYPE1000();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            oCommand = new SqlCommand("sp_INFOTYPE1000GetAllHistory", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_ObjectType", ObjectType);
            oCommand.Parameters.AddWithValue("@p_ObjectID", ObjectID);
            oCommand.Parameters.AddWithValue("@p_BeginDate", BeginDate);
            oCommand.Parameters.AddWithValue("@p_EndDate", EndDate == DateTime.MaxValue.AddTicks(-9999999) ? EndDate : EndDate.TimeOfDay.Seconds == 59 ? EndDate : EndDate.AddSeconds(-1).AddDays(1));
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            oReturn.ParseToObject(oTable.Rows[0]);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            if (oAdapter != null)
            {
                oAdapter.Dispose();
            }
            oTable.Dispose();
            return oReturn;
        }

        public override List<INFOTYPE1001> INFOTYPE1001GetAllHistory()
        {
            List<INFOTYPE1001> oReturn = new List<INFOTYPE1001>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            oCommand = new SqlCommand("sp_INFOTYPE1001GetAllHistory", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_ObjectType", "");
            oCommand.Parameters.AddWithValue("@p_ObjectID", "");
            oCommand.Parameters.AddWithValue("@p_NextObjectType", "");
            oCommand.Parameters.AddWithValue("@p_NextObjectID", "");
            oCommand.Parameters.AddWithValue("@p_Relation", "");
            oCommand.Parameters.AddWithValue("@p_BeginDate", new DateTime(9999, 12, 31));
            oCommand.Parameters.AddWithValue("@p_EndDate", new DateTime(9999, 12, 31));
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1001 item = new INFOTYPE1001();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            if (oAdapter != null)
            {
                oAdapter.Dispose();
            }
            oTable.Dispose();
            return oReturn;
        }

        public override INFOTYPE1000 INFOTYPE1000GetByObjectID(string ObjectType, string ObjectID)
        {
            INFOTYPE1000 oReturn = new INFOTYPE1000();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            oCommand = new SqlCommand("sp_INFOTYPE1000GetByObjectID", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_ObjectType", ObjectType);
            oCommand.Parameters.AddWithValue("@p_ObjectID", ObjectID);
            oCommand.Parameters.AddWithValue("@p_CheckDate", DateTime.Now);
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            oReturn.ParseToObject(oTable.Rows[0]);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            if (oAdapter != null)
            {
                oAdapter.Dispose();
            }
            oTable.Dispose();
            return oReturn;
        }

        public override List<INFOTYPE1001> GetPositionForPerson(string NextObjectID)
        {
            List<INFOTYPE1001> oReturn = new List<INFOTYPE1001>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            oCommand = new SqlCommand("sp_INFOTYPE1001GetAllHistory", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_ObjectType", "");
            oCommand.Parameters.AddWithValue("@p_ObjectID", "");
            oCommand.Parameters.AddWithValue("@p_NextObjectType", "");
            oCommand.Parameters.AddWithValue("@p_NextObjectID", NextObjectID);
            oCommand.Parameters.AddWithValue("@p_Relation", "A008");
            oCommand.Parameters.AddWithValue("@p_BeginDate", new DateTime(9999, 12, 31));
            oCommand.Parameters.AddWithValue("@p_EndDate", new DateTime(9999, 12, 31));
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1001 item = new INFOTYPE1001();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            if (oAdapter != null)
            {
                oAdapter.Dispose();
            }
            oTable.Dispose();
            return oReturn;
        }

        public override List<INFOTYPE1001> GetBelongToRelation()
        {
            List<INFOTYPE1001> oReturn = new List<INFOTYPE1001>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            oCommand = new SqlCommand("sp_INFOTYPE1001GetAllHistory", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_ObjectType", "");
            oCommand.Parameters.AddWithValue("@p_ObjectID", "");
            oCommand.Parameters.AddWithValue("@p_NextObjectType", "");
            oCommand.Parameters.AddWithValue("@p_NextObjectID", "");
            oCommand.Parameters.AddWithValue("@p_Relation", "A003");
            oCommand.Parameters.AddWithValue("@p_BeginDate", new DateTime(9999, 12, 31));
            oCommand.Parameters.AddWithValue("@p_EndDate", new DateTime(9999, 12, 31));
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1001 item = new INFOTYPE1001();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            if (oAdapter != null)
            {
                oAdapter.Dispose();
            }
            oTable.Dispose();
            return oReturn;
        }

        public override INFOTYPE1001 INFOTYPE1001Get(string ObjectType, string ObjectID, string NextObjectType, string NextObjectID, string Relation, DateTime BeginDate, DateTime EndDate)
        {
            INFOTYPE1001 oReturn = new INFOTYPE1001();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            oCommand = new SqlCommand("sp_INFOTYPE1000GetAllHistory", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_ObjectType", ObjectType);
            oCommand.Parameters.AddWithValue("@p_ObjectID", ObjectID);
            oCommand.Parameters.AddWithValue("@p_NextObjectType", NextObjectType);
            oCommand.Parameters.AddWithValue("@p_NextObjectID", NextObjectID);
            oCommand.Parameters.AddWithValue("@p_Relation", Relation);
            oCommand.Parameters.AddWithValue("@p_BeginDate", BeginDate);
            oCommand.Parameters.AddWithValue("@p_EndDate", EndDate == DateTime.MaxValue.AddTicks(-9999999) ? EndDate : EndDate.TimeOfDay.Seconds == 59 ? EndDate : EndDate.AddSeconds(-1).AddDays(1));
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            oReturn.ParseToObject(oTable.Rows[0]);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            if (oAdapter != null)
            {
                oAdapter.Dispose();
            }
            oTable.Dispose();
            return oReturn;
        }

        public override List<INFOTYPE1013> INFOTYPE1013GetAllHistory()
        {
            List<INFOTYPE1013> oReturn = new List<INFOTYPE1013>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            oCommand = new SqlCommand("sp_INFOTYPE1013GetAllHistory", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_ObjectID", "");
            oCommand.Parameters.AddWithValue("@p_BeginDate", new DateTime(9999, 12, 31));
            oCommand.Parameters.AddWithValue("@p_EndDate", new DateTime(9999, 12, 31));
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1013 item = new INFOTYPE1013();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            if (oAdapter != null)
            {
                oAdapter.Dispose();
            }
            oTable.Dispose();
            return oReturn;
        }

        public override INFOTYPE1013 INFOTYPE1013Get(string ObjectID, DateTime BeginDate, DateTime EndDate)
        {
            INFOTYPE1013 oReturn = new INFOTYPE1013();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            oCommand = new SqlCommand("sp_INFOTYPE1013GetAllHistory", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_ObjectID", ObjectID);
            oCommand.Parameters.AddWithValue("@p_BeginDate", BeginDate);
            oCommand.Parameters.AddWithValue("@p_EndDate", EndDate == DateTime.MaxValue.AddTicks(-9999999) ? EndDate : EndDate.TimeOfDay.Seconds == 59 ? EndDate : EndDate.AddSeconds(-1).AddDays(1));
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.Fill(oTable);
            oReturn.ParseToObject(oTable.Rows[0]);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            if (oAdapter != null)
            {
                oAdapter.Dispose();
            }
            oTable.Dispose();
            return oReturn;
        }
        #endregion

        #region " Delete Employee Data "
        public override void DeleteINFOTYPE1000(string ObjectType, string ObjectID, DateTime BeginDate, DateTime EndDate)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand;
            oCommand = new SqlCommand("sp_INFOTYPE1000Delete", oConnection, tx);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_ObjectType", Convert2ObjectTypeOneBit(ObjectType));
            oCommand.Parameters.AddWithValue("@p_ObjectID", ObjectID);
            oCommand.Parameters.AddWithValue("@p_BeginDate", BeginDate);
            oCommand.Parameters.AddWithValue("@p_EndDate", EndDate == DateTime.MaxValue.AddTicks(-9999999) ? EndDate : EndDate.TimeOfDay.Seconds == 59 ? EndDate : EndDate.AddSeconds(-1).AddDays(1));
            string oAction = "DELETE INFOTYPE1000 ";
            bool status = false;
            try
            {
                oCommand.ExecuteNonQuery();
                tx.Commit();
                status = true;
                oAction += "Failed : Data Active";
            }
            catch (Exception ex)
            {
                tx.Rollback();
                oAction += "Failed : " + ex.Message;
                throw new Exception("delete data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                CallActionLog(null, null, oAction + " Where ObjectType = " + ObjectType + " and ObjectID = " + ObjectID + " and BeginDate = " + BeginDate + " and EndDate = " + EndDate, status);
            }
        }

        public override void DeleteINFOTYPE1001(INFOTYPE1001 oINF1001)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand;
            oCommand = new SqlCommand("sp_INFOTYPE1001Delete", oConnection, tx);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_ObjectType", oINF1001.ObjectType);
            oCommand.Parameters.AddWithValue("@p_ObjectID", oINF1001.ObjectID);
            oCommand.Parameters.AddWithValue("@p_BeginDate", oINF1001.BeginDate);
            oCommand.Parameters.AddWithValue("@p_EndDate", oINF1001.EndDate == DateTime.MaxValue.AddTicks(-9999999) ? oINF1001.EndDate : oINF1001.EndDate.TimeOfDay.Seconds == 59 ? oINF1001.EndDate : oINF1001.EndDate.AddSeconds(-1).AddDays(1));
            oCommand.Parameters.AddWithValue("@p_NextObjectType", oINF1001.NextObjectType);
            oCommand.Parameters.AddWithValue("@p_NextObjectID", oINF1001.NextObjectID);
            oCommand.Parameters.AddWithValue("@p_Relation", oINF1001.Relation);
            string oAction = "DELETE INFOTYPE1001 ";
            bool oStatus = false;
            try
            {
                oCommand.ExecuteNonQuery();
                tx.Commit();
                oAction += "Completed";
                oStatus = true;
            }
            catch (Exception ex)
            {
                tx.Rollback();
                oAction += "Failed : " + ex.Message;
                throw new Exception("delete data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                CallActionLog(null, oINF1001, oAction, oStatus);
                /*ActionLog oActionLog = new ActionLog();
                oActionLog.LogAction = oAction;
                oActionLog.LogActionBy = string.Format("{0}", "SYSTEM");
                oActionLog.LogData = LogMgr.GetSerialize<INFOTYPE1001>(oINF1001);
                oActionLog.LogActionDate = DateTime.Now; //  generate from db
                oActionLog.LogID = Guid.NewGuid().ToString();//  generate from db
                oActionLog.LogStatus = oStatus;
                InsertActionLog(oActionLog);*/
            }
        }

        public override void DeleteINFOTYPE1013(INFOTYPE1013 oINF1013)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand;
            oCommand = new SqlCommand("sp_INFOTYPE1013Delete", oConnection, tx);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_ObjectID", oINF1013.ObjectID);
            oCommand.Parameters.AddWithValue("@p_BeginDate", oINF1013.BeginDate);
            oCommand.Parameters.AddWithValue("@p_EndDate", oINF1013.EndDate == DateTime.MaxValue.AddTicks(-9999999) ? oINF1013.EndDate : oINF1013.EndDate.TimeOfDay.Seconds == 59 ? oINF1013.EndDate : oINF1013.EndDate.AddSeconds(-1).AddDays(1));
            string oAction = "DELETE INFOTYPE1013 ";
            bool oStatus = false;
            try
            {
                oCommand.ExecuteNonQuery();
                tx.Commit();
                oAction += "Completed";
                oStatus = true;
            }
            catch (Exception ex)
            {
                tx.Rollback();
                oAction += "Failed : " + ex.Message;
                throw new Exception("delete data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                CallActionLog(null, oINF1013, oAction, oStatus);
                /*ActionLog oActionLog = new ActionLog();
                oActionLog.LogAction = oAction;
                oActionLog.LogActionBy = string.Format("{0}", "SYSTEM");
                oActionLog.LogData = LogMgr.GetSerialize<INFOTYPE1013>(oINF1013);
                oActionLog.LogActionDate = DateTime.Now; //  generate from db
                oActionLog.LogID = Guid.NewGuid().ToString();//  generate from db
                oActionLog.LogStatus = oStatus;
                InsertActionLog(oActionLog);*/
            }
        }
        #endregion

        #region " Insert & Update Data "
        public override void InsertINFOTYPE1000(INFOTYPE1000 oINF1000)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand;
            string queryStr = "sp_INFOTYPE1000Insert";
            oCommand = new SqlCommand(queryStr, oConnection, tx);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_ObjectType", oINF1000.ObjectType);
            oCommand.Parameters.AddWithValue("@p_ObjectID", oINF1000.ObjectID);
            oCommand.Parameters.AddWithValue("@p_BeginDate", oINF1000.BeginDate);
            oCommand.Parameters.AddWithValue("@p_EndDate", oINF1000.EndDate == DateTime.MaxValue.AddTicks(-9999999) ? oINF1000.EndDate : oINF1000.EndDate.TimeOfDay.Seconds == 59 ? oINF1000.EndDate : oINF1000.EndDate.AddSeconds(-1).AddDays(1));
            oCommand.Parameters.AddWithValue("@p_ShortText", oINF1000.ShortText);
            oCommand.Parameters.AddWithValue("@p_Text", oINF1000.Text);
            oCommand.Parameters.AddWithValue("@p_ShortTextEn", oINF1000.ShortTextEn);
            oCommand.Parameters.AddWithValue("@p_TextEn", oINF1000.TextEn);
            string oAction = "INSERT INFOTYPE1000 ";
            bool oStatus = false;
            try
            {
                oCommand.ExecuteNonQuery();
                tx.Commit();
                oAction += "Completed";
                oStatus = true;
            }
            catch (Exception ex)
            {
                tx.Rollback();
                oAction += "Failed : " + ex.Message;
                throw new Exception("insert data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                CallActionLog(null, oINF1000, oAction, oStatus);
            }
        }

        public override void UpdateINFOTYPE1000(INFOTYPE1000 oINF1000)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            string queryStr = "sp_INFOTYPE1000Update";
            oCommand = new SqlCommand(queryStr, oConnection, tx);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_ObjectType", oINF1000.ObjectType);
            oCommand.Parameters.AddWithValue("@p_ObjectID", oINF1000.ObjectID);
            oCommand.Parameters.AddWithValue("@p_BeginDate", oINF1000.BeginDate);
            oCommand.Parameters.AddWithValue("@p_EndDate", oINF1000.EndDate == DateTime.MaxValue.AddTicks(-9999999) ? oINF1000.EndDate : oINF1000.EndDate.TimeOfDay.Seconds == 59 ? oINF1000.EndDate : oINF1000.EndDate.AddSeconds(-1).AddDays(1));
            oCommand.Parameters.AddWithValue("@p_ShortText", oINF1000.ShortText);
            oCommand.Parameters.AddWithValue("@p_Text", oINF1000.Text);
            oCommand.Parameters.AddWithValue("@p_ShortTextEn", oINF1000.ShortTextEn);
            oCommand.Parameters.AddWithValue("@p_TextEn", oINF1000.TextEn);
            oAdapter = new SqlDataAdapter(oCommand);
            string oAction = "UPDATE INFOTYPE1000 ";
            bool oStatus = false;
            try
            {
                oCommand.ExecuteNonQuery();
                tx.Commit();
                oAction += "Completed";
                oStatus = true;
            }
            catch (Exception ex)
            {
                tx.Rollback();
                oAction += "Failed : " + ex.Message;
                throw new Exception("update data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                CallActionLog(null, oINF1000, oAction, oStatus);
            }
        }

        public override void InsertINFOTYPE1001(INFOTYPE1001 oINF1001, INFOTYPE1001 oOldINF1001)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand;
            string queryStr = "sp_INFOTYPE1001Insert";
            oCommand = new SqlCommand(queryStr, oConnection, tx);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_Relation", oINF1001.Relation);
            oCommand.Parameters.AddWithValue("@p_ObjectType", oINF1001.ObjectType);
            oCommand.Parameters.AddWithValue("@p_ObjectID", oINF1001.ObjectID);
            oCommand.Parameters.AddWithValue("@p_BeginDate", oINF1001.BeginDate);
            oCommand.Parameters.AddWithValue("@p_EndDate", oINF1001.EndDate == DateTime.MaxValue.AddTicks(-9999999) ? oINF1001.EndDate : oINF1001.EndDate.TimeOfDay.Seconds == 59 ? oINF1001.EndDate : oINF1001.EndDate.AddSeconds(-1).AddDays(1));
            oCommand.Parameters.AddWithValue("@p_NextObjectType", oINF1001.NextObjectType);
            oCommand.Parameters.AddWithValue("@p_NextObjectID", oINF1001.NextObjectID);
            oCommand.Parameters.AddWithValue("@p_PercentValue", oINF1001.PercentValue);
            if (oOldINF1001.BeginDate != DateTime.MinValue) oCommand.Parameters.AddWithValue("@p_OldObjectType", oOldINF1001.ObjectType);
            if (oOldINF1001.BeginDate != DateTime.MinValue) oCommand.Parameters.AddWithValue("@p_OldObjectID", oOldINF1001.ObjectID);
            if (oOldINF1001.BeginDate != DateTime.MinValue) oCommand.Parameters.AddWithValue("@p_OldNextObjectType", oOldINF1001.NextObjectType);
            if (oOldINF1001.BeginDate != DateTime.MinValue) oCommand.Parameters.AddWithValue("@p_OldNextObjectID", oOldINF1001.NextObjectID);
            string oAction = "INSERT INFOTYPE1001 ";
            bool oStatus = false;
            try
            {
                oCommand.ExecuteNonQuery();
                tx.Commit();
                oAction += "Completed";
                oStatus = true;
            }
            catch (Exception ex)
            {
                tx.Rollback();
                oAction += "Failed : " + ex.Message;
                throw new Exception("insert data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                CallActionLog(oOldINF1001.BeginDate == DateTime.MinValue ? null : oOldINF1001, oINF1001, oAction, oStatus);
                /*ActionLog oActionLog = new ActionLog();
                oActionLog.LogAction = oAction;
                oActionLog.LogActionBy = string.Format("{0}", "SYSTEM");
                oActionLog.LogData = LogMgr.GetSerialize<INFOTYPE1001>(oINF1001) + LogMgr.GetSerialize<INFOTYPE1001>(oOldINF1001);
                oActionLog.LogActionDate = DateTime.Now; //  generate from db
                oActionLog.LogID = Guid.NewGuid().ToString();//  generate from db
                oActionLog.LogStatus = oStatus;
                InsertActionLog(oActionLog);*/
            }
        }

        public override void InsertINFOTYPEAllForEmployee(List<INFOTYPE0001> INF0001Lst, List<INFOTYPE0002> INF0002Lst, List<INFOTYPE0182> INF0182Lst, List<INFOTYPE0105> INF0105Lst, List<INFOTYPE1001> INF1001Lst)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand = new SqlCommand("sp_INFOTYPE0002And0182Insert", oConnection, tx); ;
            int count = 0;
            string oAction = "ADD ALL EMPLOYEE DATA ";
            bool oStatus = false;
            try
            {
                oCommand = new SqlCommand("sp_EmployeeManagementDelete", oConnection, tx);
                oCommand.CommandType = CommandType.StoredProcedure;
                oCommand.Parameters.AddWithValue("@p_EmployeeID", INF0001Lst[0].EmployeeID);
                oCommand.ExecuteNonQuery();

                foreach (INFOTYPE0002 oINF2 in INF0002Lst)
                {
                    oCommand = new SqlCommand("sp_INFOTYPE0002And0182Insert", oConnection, tx);
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.Parameters.AddWithValue("@p_EmployeeID", oINF2.EmployeeID);
                    oCommand.Parameters.AddWithValue("@p_BeginDate", oINF2.BeginDate);
                    oCommand.Parameters.AddWithValue("@p_EndDate", oINF2.EndDate == DateTime.MaxValue.AddTicks(-9999999) ? oINF2.EndDate : oINF2.EndDate.TimeOfDay.Seconds == 59 ? oINF2.EndDate : oINF2.EndDate.AddSeconds(-1).AddDays(1));
                    oCommand.Parameters.AddWithValue("@p_TitleID", oINF2.TitleID);
                    oCommand.Parameters.AddWithValue("@p_FirstName", oINF2.FirstName);
                    oCommand.Parameters.AddWithValue("@p_LastName", oINF2.LastName);
                    oCommand.Parameters.AddWithValue("@p_FullNameEn", INF0182Lst[count].AlternateName);
                    oCommand.Parameters.AddWithValue("@p_NickName", oINF2.NickName);
                    oCommand.Parameters.AddWithValue("@p_DOB", oINF2.DOB);
                    oCommand.Parameters.AddWithValue("@p_Gender", oINF2.Gender);
                    oCommand.Parameters.AddWithValue("@p_Nationality", oINF2.Nationality);
                    oCommand.Parameters.AddWithValue("@p_MaritalStatus", oINF2.MaritalStatus);
                    oCommand.Parameters.AddWithValue("@p_Religion", oINF2.Religion);
                    oCommand.Parameters.AddWithValue("@p_Language", oINF2.Language);
                    oCommand.ExecuteNonQuery();
                    count = count + 1;
                }

                foreach (INFOTYPE0001 oINF1 in INF0001Lst)
                {
                    oCommand = new SqlCommand("sp_INFOTYPE0001Insert", oConnection, tx);
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.Parameters.AddWithValue("@p_EmployeeID", oINF1.EmployeeID);
                    oCommand.Parameters.AddWithValue("@p_BeginDate", oINF1.BeginDate);
                    oCommand.Parameters.AddWithValue("@p_EndDate", oINF1.EndDate == DateTime.MaxValue.AddTicks(-9999999) ? oINF1.EndDate : oINF1.EndDate.TimeOfDay.Seconds == 59 ? oINF1.EndDate : oINF1.EndDate.AddSeconds(-1).AddDays(1));
                    oCommand.Parameters.AddWithValue("@p_CompanyCode", oINF1.CompanyCode);
                    oCommand.Parameters.AddWithValue("@p_Name", oINF1.Name);
                    oCommand.Parameters.AddWithValue("@p_Area", oINF1.Area);
                    oCommand.Parameters.AddWithValue("@p_SubArea", oINF1.SubArea);
                    oCommand.Parameters.AddWithValue("@p_EmpGroup", oINF1.EmpGroup);
                    oCommand.Parameters.AddWithValue("@p_EmpSubGroup", oINF1.EmpSubGroup);
                    oCommand.Parameters.AddWithValue("@p_OrgUnit", oINF1.OrgUnit);
                    oCommand.Parameters.AddWithValue("@p_Position", oINF1.Position);
                    oCommand.Parameters.AddWithValue("@p_CostCenter", oINF1.CostCenter);
                    oCommand.ExecuteNonQuery();
                }
                foreach (INFOTYPE0105 oINF105 in INF0105Lst)
                {
                    oCommand = new SqlCommand("sp_INFOTYPE0105Insert", oConnection, tx);
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.Parameters.AddWithValue("@p_EmployeeID", oINF105.EmployeeID);
                    oCommand.Parameters.AddWithValue("@p_BeginDate", oINF105.BeginDate);
                    oCommand.Parameters.AddWithValue("@p_EndDate", oINF105.EndDate == DateTime.MaxValue.AddTicks(-9999999) ? oINF105.EndDate : oINF105.EndDate.TimeOfDay.Seconds == 59 ? oINF105.EndDate : oINF105.EndDate.AddSeconds(-1).AddDays(1));
                    oCommand.Parameters.AddWithValue("@p_SubType", oINF105.CategoryCode);
                    oCommand.Parameters.AddWithValue("@p_DataText_Long", oINF105.DataText);
                    oCommand.ExecuteNonQuery();
                }

                foreach (INFOTYPE1001 oINF1001 in INF1001Lst)
                {
                    oCommand = new SqlCommand("sp_INFOTYPE1001Insert", oConnection, tx);
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.Parameters.AddWithValue("@p_Relation", oINF1001.Relation);
                    oCommand.Parameters.AddWithValue("@p_ObjectType", oINF1001.ObjectType);
                    oCommand.Parameters.AddWithValue("@p_ObjectID", oINF1001.ObjectID);
                    oCommand.Parameters.AddWithValue("@p_BeginDate", oINF1001.BeginDate);
                    oCommand.Parameters.AddWithValue("@p_EndDate", oINF1001.EndDate == DateTime.MaxValue.AddTicks(-9999999) ? oINF1001.EndDate : oINF1001.EndDate.TimeOfDay.Seconds == 59 ? oINF1001.EndDate : oINF1001.EndDate.AddSeconds(-1).AddDays(1));
                    oCommand.Parameters.AddWithValue("@p_NextObjectType", oINF1001.NextObjectType);
                    oCommand.Parameters.AddWithValue("@p_NextObjectID", oINF1001.NextObjectID);
                    oCommand.Parameters.AddWithValue("@p_PercentValue", oINF1001.PercentValue);
                    oCommand.ExecuteNonQuery();
                }

                tx.Commit();
                oAction += "Completed";
                oStatus = true;
            }
            catch (Exception ex)
            {
                tx.Rollback();
                oAction += "Failed : " + ex.Message;
                throw new Exception("insert data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                string DataAll = string.Format("INFOTYPE0001 : {0}\r\n,INFOTYPE0002 : {1}\r\n,INFOTYPE0182 : {2}\r\n,INFOTYPE0105 : {3}\r\n,INFOTYPE1001 : {4}", INF0001Lst, INF0002Lst, INF0182Lst, INF0105Lst, INF1001Lst);
                CallActionLog(null, DataAll, oAction, oStatus);
            }
        }

        public static string GetSerialize(object obj)
        {
            string result = string.Empty;
            try
            {
                if (obj != null)
                {
                    XmlSerializer serializer = new XmlSerializer(obj.GetType());
                    MemoryStream memoryStream = new MemoryStream();
                    using (StreamReader stReader = new StreamReader(memoryStream))
                    {
                        serializer.Serialize(memoryStream, obj);
                        memoryStream.Position = 0;
                        result = stReader.ReadToEnd();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public override void UpdateINFOTYPE1001(INFOTYPE1001 oINF1001New)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            string queryStr = "sp_INFOTYPE1001Update";
            oCommand = new SqlCommand(queryStr, oConnection, tx);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_ObjectType", oINF1001New.ObjectType);
            oCommand.Parameters.AddWithValue("@p_ObjectID", oINF1001New.ObjectID);
            oCommand.Parameters.AddWithValue("@p_BeginDate", oINF1001New.BeginDate);
            oCommand.Parameters.AddWithValue("@p_EndDate", oINF1001New.EndDate == DateTime.MaxValue.AddTicks(-9999999) ? oINF1001New.EndDate : oINF1001New.EndDate.TimeOfDay.Seconds == 59 ? oINF1001New.EndDate : oINF1001New.EndDate.AddSeconds(-1).AddDays(1));
            oCommand.Parameters.AddWithValue("@p_NextObjectType", oINF1001New.NextObjectType);
            oCommand.Parameters.AddWithValue("@p_NextObjectID", oINF1001New.NextObjectID);
            oCommand.Parameters.AddWithValue("@p_Relation", oINF1001New.Relation);
            oCommand.Parameters.AddWithValue("@p_PercentValue", oINF1001New.PercentValue);
            oAdapter = new SqlDataAdapter(oCommand);
            string oAction = "UPDATE INFOTYPE1001 ";
            bool oStatus = false;
            try
            {
                oCommand.ExecuteNonQuery();
                tx.Commit();
                oAction += "Completed";
                oStatus = true;
            }
            catch (Exception ex)
            {
                tx.Rollback();
                oAction += "Failed : " + ex.Message;
                throw new Exception("update data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                CallActionLog(null, oINF1001New, oAction, oStatus);
                /*ActionLog oActionLog = new ActionLog();
                oActionLog.LogAction = oAction;
                oActionLog.LogActionBy = string.Format("{0}", "SYSTEM");
                oActionLog.LogData = LogMgr.GetSerialize<INFOTYPE1001>(oINF1001New);
                oActionLog.LogActionDate = DateTime.Now; //  generate from db
                oActionLog.LogID = Guid.NewGuid().ToString();//  generate from db
                oActionLog.LogStatus = oStatus;
                InsertActionLog(oActionLog);*/
            }
        }

        public override void InsertINFOTYPE1013(INFOTYPE1013 oINF1013)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand;
            string queryStr = "sp_INFOTYPE1013Insert";
            oCommand = new SqlCommand(queryStr, oConnection, tx);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_ObjectID", oINF1013.ObjectID);
            oCommand.Parameters.AddWithValue("@p_BeginDate", oINF1013.BeginDate);
            oCommand.Parameters.AddWithValue("@p_EndDate", oINF1013.EndDate == DateTime.MaxValue.AddTicks(-9999999) ? oINF1013.EndDate : oINF1013.EndDate.TimeOfDay.Seconds == 59 ? oINF1013.EndDate : oINF1013.EndDate.AddSeconds(-1).AddDays(1));
            oCommand.Parameters.AddWithValue("@p_EmpGroup", oINF1013.EmpGroup);
            oCommand.Parameters.AddWithValue("@p_EmpSubGroup", oINF1013.EmpSubGroup);
            string oAction = "INSERT INFOTYPE1013 ";
            bool oStatus = false;
            try
            {
                oCommand.ExecuteNonQuery();
                tx.Commit();
                oAction += "Completed";
                oStatus = true;
            }
            catch (Exception ex)
            {
                tx.Rollback();
                oAction += "Failed : " + ex.Message;
                throw new Exception("insert data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                CallActionLog(null, oINF1013, oAction, oStatus);
                /*ActionLog oActionLog = new ActionLog();
                oActionLog.LogAction = oAction;
                oActionLog.LogActionBy = string.Format("{0}", "SYSTEM");
                oActionLog.LogData = LogMgr.GetSerialize<INFOTYPE1013>(oINF1013);
                oActionLog.LogActionDate = DateTime.Now; //  generate from db
                oActionLog.LogID = Guid.NewGuid().ToString();//  generate from db
                oActionLog.LogStatus = oStatus;
                InsertActionLog(oActionLog);*/
            }
        }

        public override void UpdateINFOTYPE1013(INFOTYPE1013 oINF1013)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            string queryStr = "sp_INFOTYPE1013Update";
            oCommand = new SqlCommand(queryStr, oConnection, tx);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_ObjectID", oINF1013.ObjectID);
            oCommand.Parameters.AddWithValue("@p_BeginDate", oINF1013.BeginDate);
            oCommand.Parameters.AddWithValue("@p_EndDate", oINF1013.EndDate == DateTime.MaxValue.AddTicks(-9999999) ? oINF1013.EndDate : oINF1013.EndDate.TimeOfDay.Seconds == 59 ? oINF1013.EndDate : oINF1013.EndDate.AddSeconds(-1).AddDays(1));
            oCommand.Parameters.AddWithValue("@p_EmpGroup", oINF1013.EmpGroup);
            oCommand.Parameters.AddWithValue("@p_EmpSubGroup", oINF1013.EmpSubGroup);
            oAdapter = new SqlDataAdapter(oCommand);
            string oAction = "UPDATE INFOTYPE1013 ";
            bool oStatus = false;
            try
            {
                oCommand.ExecuteNonQuery();
                tx.Commit();
                oAction += "Completed";
                oStatus = true;
            }
            catch (Exception ex)
            {
                tx.Rollback();
                oAction += "Failed : " + ex.Message;
                throw new Exception("update data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                CallActionLog(null, oINF1013, oAction, oStatus);
                /*ActionLog oActionLog = new ActionLog();
                oActionLog.LogAction = oAction;
                oActionLog.LogActionBy = string.Format("{0}", "SYSTEM");
                oActionLog.LogData = LogMgr.GetSerialize<INFOTYPE1013>(oINF1013);
                oActionLog.LogActionDate = DateTime.Now; //  generate from db
                oActionLog.LogID = Guid.NewGuid().ToString();//  generate from db
                oActionLog.LogStatus = oStatus;
                InsertActionLog(oActionLog);*/
            }
        }
        #endregion
        #endregion

        #region Common
        private IList<T> ExecuteQuery<T>(string oProcedureName, Dictionary<string, object> oParamRequest)
        {
            IList<T> oResult = new List<T>();
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (SqlCommand oCommand = new SqlCommand(oProcedureName, oConnection))
                {
                    oCommand.CommandType = CommandType.StoredProcedure;
                    if (oParamRequest.Count > 0)
                    {
                        foreach (string oCurrnetKey in oParamRequest.Keys)
                        {
                            oCommand.Parameters.AddWithValue(oCurrnetKey, oParamRequest[oCurrnetKey]);
                        }
                    }
                    SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                    DataTable oTable = new DataTable("TableName");
                    try
                    {
                        oAdapter.Fill(oTable);
                        if (oTable != null && oTable.Rows.Count > 0)
                        {
                            oResult = Convert<T>.ConvertFrom(oTable);
                        }
                    }
                    catch (Exception ex)
                    {
                        //May be log 
                    }
                    finally
                    {
                        oConnection.Close();
                        oConnection.Dispose();
                        oCommand.Dispose();
                    }
                }
            }
            return oResult;
        }
        private bool ExecuteNoneQuery(string oProcedureName, Dictionary<string, object> oParamRequest)
        {
            bool oResult = false;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (SqlCommand oCommand = new SqlCommand(oProcedureName, oConnection))
                {
                    oCommand.CommandType = CommandType.StoredProcedure;
                    if (oParamRequest.Count > 0)
                    {
                        foreach (string oCurrnetKey in oParamRequest.Keys)
                        {
                            oCommand.Parameters.AddWithValue(oCurrnetKey, oParamRequest[oCurrnetKey]);
                        }
                    }
                    //SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                    //DataTable oTable = new DataTable("TableName");
                    try
                    {
                        oConnection.Open();
                        if (oCommand.ExecuteNonQuery() > 0)
                        {
                            oResult = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        //May be log 
                    }
                    finally
                    {
                        oConnection.Close();
                        oConnection.Dispose();
                        oCommand.Dispose();
                    }
                }
            }
            return oResult;
        }
        private string ExecuteScalar(string oProcedureName, Dictionary<string, object> oParamRequest)
        {
            string oResult = string.Empty;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (SqlCommand oCommand = new SqlCommand(oProcedureName, oConnection))
                {
                    oCommand.CommandType = CommandType.StoredProcedure;
                    if (oParamRequest.Count > 0)
                    {
                        foreach (string oCurrnetKey in oParamRequest.Keys)
                        {
                            oCommand.Parameters.AddWithValue(oCurrnetKey, oParamRequest[oCurrnetKey]);
                        }
                    }
                    //SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                    //DataTable oTable = new DataTable("TableName");
                    try
                    {
                        oConnection.Open();
                        oResult = Convert.ToString(oCommand.ExecuteScalar());
                    }
                    catch (Exception ex)
                    {
                        //May be log 
                    }
                    finally
                    {
                        oConnection.Close();
                        oConnection.Dispose();
                        oCommand.Dispose();
                    }
                }
            }
            return oResult;
        }
        #endregion

        #region " INFOTYPE0027 "
        public override void SaveInfotype0027(string EmployeeID1, string EmployeeID2, List<INFOTYPE0027> data, string profile)
        {
            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand1 = new SqlCommand("delete from INFOTYPE0027", oConnection, tx);
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand1.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand1.Parameters.Add(oParam);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE0027 where EmployeeID between @EmpID1 and @EmpID2", oConnection, tx);
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oAdapter.UpdateBatchSize = 10000;
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("INFOTYPE0027");
            try
            {
                oCommand1.ExecuteNonQuery();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                foreach (INFOTYPE0027 item in data)
                {
                    item.LoadDataToTable(oTable);
                }

                oAdapter.Update(oTable);
                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand1.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }

        public override List<INFOTYPE0027> GetInfotype0027List(string EmployeeID, DateTime CheckDate)
        {
            List<INFOTYPE0027> oReturn = new List<INFOTYPE0027>();

            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_INFOTYPE0027Get", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            oCommand.Parameters.Add(new SqlParameter("@p_EmployeeID", EmployeeID));
            oCommand.Parameters.Add(new SqlParameter("@p_CheckDate", CheckDate));

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow oRow in oTable.Rows)
            {
                INFOTYPE0027 item = new INFOTYPE0027();
                item.ParseToObject(oRow);
                oReturn.Add(item);
            }
            return oReturn;
        }
        #endregion

        public string Convert2ObjectTypeString(string ObjectType)
        {
            string oReturn = "";
            switch (ObjectType)
            {
                case "P":
                    oReturn = "EMPLOYEE";
                    break;
                case "S":
                    oReturn = "POSITION";
                    break;
                case "O":
                    oReturn = "ORGANIZE";
                    break;
                case "C":
                    oReturn = "JOB";
                    break;
                case "K":
                    oReturn = "COSTCENTER";
                    break;
                case "Q":
                    oReturn = "QUALIFICATION";
                    break;
                default:
                    oReturn = "";
                    break;
            }
            return oReturn;
        }

        public string Convert2ObjectTypeOneBit(string ObjectType)
        {
            string oReturn = "";
            switch (ObjectType)
            {
                case "EMPLOYEE":
                    oReturn = "P";
                    break;
                case "POSITION":
                    oReturn = "S";
                    break;
                case "ORGANIZE":
                    oReturn = "O";
                    break;
                case "JOB":
                    oReturn = "C";
                    break;
                case "COSTCENTER":
                    oReturn = "K";
                    break;
                case "QUALIFICATION":
                    oReturn = "Q";
                    break;
                default:
                    oReturn = "";
                    break;
            }
            return oReturn;
        }

        public override DataTable GetOrganizationStructure(string EmployeeID, string PositionID)
        {
            oSqlManage["ProcedureName"] = "sp_EmployeeOrganizationStructureGet";
            DataTable oResult = new DataTable();
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_EmployeeID"] = EmployeeID;
            oParamRequest["@p_PositionID"] = PositionID;
            return DatabaseHelper.ExecuteQuery(oSqlManage, oParamRequest);
        }

        public override DataTable ValidateData(string EmployeeID)
        {

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            oCommand = new SqlCommand("sp_ValidateData", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_EmployeeID", EmployeeID);
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oConnection.Open();
            try
            {
                oAdapter.Fill(oTable);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            if (oAdapter != null)
            {
                oAdapter.Dispose();
            }
            oTable.Dispose();

            return oTable;
        }

        public bool CallActionLog(object objOld, object objNew, string oAction, bool oStatus)
        {
            bool flg = false;
            ActionLog oActionLog = new ActionLog();
            oActionLog.LogAction = oAction;
            oActionLog.LogActionBy = string.Format("{0}", "SYSTEM");
            oActionLog.LogData = string.Format("OLD : {0}\r\n,NEW : {1}", (objOld != null) ? LogMgr.GetSerialize(objOld) : "", (objNew != null) ? LogMgr.GetSerialize(objNew) : "");
            oActionLog.LogActionDate = DateTime.Now; //  generate from db
            oActionLog.LogID = Guid.NewGuid().ToString();//  generate from db
            oActionLog.LogStatus = oStatus;

            flg = InsertActionLog(oActionLog);
            return flg;
        }

        public override bool InsertActionLog(ActionLog oActionLog)
        {
            bool flg = false;
            oSqlManage["ProcedureName"] = "sp_ActionLogSet";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_LogAction"] = oActionLog.LogAction;
            oParamRequest["@p_LogData"] = oActionLog.LogData;
            oParamRequest["@p_LogActionBy"] = oActionLog.LogActionBy;
            oParamRequest["@p_LogStatus"] = oActionLog.LogStatus;
            flg = DatabaseHelper.ExecuteNoneQuery(oSqlManage, oParamRequest);
            return flg;
        }

        public override DataTable GetMonthlyWorkSchedule(string EmployeeID)
        {
            oSqlManage["ProcedureName"] = "sp_MonthlyWorkScheduleGet";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@EmployeeID"] = EmployeeID;
            return DatabaseHelper.ExecuteQuery(oSqlManage, oParamRequest);
        }
        public override List<INFOTYPE0182> GetInfotype0182List(string oEmployeeID)
        {
            oSqlManage["ProcedureName"] = "sp_Infotype0182Get";
            List<INFOTYPE0182> oResult = new List<INFOTYPE0182>();
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_EmployeeID"] = oEmployeeID;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<INFOTYPE0182>(oSqlManage, oParamRequest));
            return oResult;
        }

        public override DataTable GetExternalUserbyID(string UserID, string oLanguage)
        {
            oSqlManage["ProcedureName"] = "sp_ExternalUserGetbyID";
            List<INFOTYPE0182> oResult = new List<INFOTYPE0182>();
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_UserID"] = UserID;
            return DatabaseHelper.ExecuteQuery(oSqlManage, oParamRequest);
        }

        public override DataTable GetExternalUserSnapshotbyRequestID(int RequestID, int ApproverID)
        {
            oSqlManage["ProcedureName"] = "sp_RequestFlowMultipleReceipient_ExternalListGet";
            List<INFOTYPE0182> oResult = new List<INFOTYPE0182>();
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@RequestID"] = RequestID;
            if (ApproverID > 0)
                oParamRequest["@ApproverID"] = ApproverID;
            return DatabaseHelper.ExecuteQuery(oSqlManage, oParamRequest);
        }


        public override DataTable GetPettyCustodianGetByCode(string PettyCode)
        {
            oSqlManage["ProcedureName"] = "sp_TR_PettyCustodianGetByCode";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_PettyCode"] = PettyCode;
            DataTable oResult = DatabaseHelper.ExecuteQuery(oSqlManage, oParamRequest);
            oResult.TableName = "PettyCashCustodian";
            return oResult;
        }

        public override DataSet GetContractDetailByEmployeeID(string EmployeeID, string CostCenter, DateTime CheckDate)
        {
            oSqlManage["ProcedureName"] = "sp_ContractMemberGetByID";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_EmployeeID"] = EmployeeID;
            oParamRequest["@p_CostCenter"] = CostCenter;
            oParamRequest["@p_CheckDate"] = CheckDate;

            DataSet oResult = DatabaseHelper.ExecuteQueryToDataSet(oSqlManage, oParamRequest);
            return oResult;
        }

        public override bool IsContractUser(string EmployeeID)
        {
            oSqlManage["ProcedureName"] = "sp_ContractMemberValidateByID";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_EmployeeID"] = EmployeeID;
            bool oResult = Convert.ToBoolean(DatabaseHelper.ExecuteScalar(oSqlManage, oParamRequest));
            return oResult;
        }

        public override bool IsExternalUser(string EmployeeID)
        {
            oSqlManage["ProcedureName"] = "sp_ExternalUserValidateByID";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_EmployeeID"] = EmployeeID;
            bool oResult = Convert.ToBoolean(DatabaseHelper.ExecuteScalar(oSqlManage, oParamRequest));
            return oResult;
        }

        public override string GetNameFromInfotype0002Name(string EmployeeID)
        {
            oSqlManage["ProcedureName"] = "sp_Infotype0002GetNameByEmployeeID";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_EmployeeID"] = EmployeeID;
            string oResult = DatabaseHelper.ExecuteScalar(oSqlManage, oParamRequest).ToString();
            return oResult;
        }

        public override bool ValidateActiveEmployeeForTravel(string EmployeeID, DateTime BeginTravelDate)
        {
            oSqlManage["ProcedureName"] = "sp_ValidateActiveEmployeeForTravel";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_EmployeeID"] = EmployeeID;
            oParamRequest["@p_CheckDate"] = BeginTravelDate;
            return bool.Parse(DatabaseHelper.ExecuteScalar(oSqlManage, oParamRequest));
        }

        public override DateSpecificData GetDateSpecific(string EmployeeID)
        {
            List<DateSpecificData> oResult = new List<DateSpecificData>();
            oSqlManage["ProcedureName"] = "sp_GetDateSpecificGet";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_EmployeeID"] = EmployeeID;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<DateSpecificData>(oSqlManage, oParamRequest));
            return oResult[0];
        }

        public override List<EmployeeData> GetAllEmployeeInINFOTYPE0001(DateTime CheckDate)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_INFOTYPE0001GetAllByCheckDate", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            oParam = new SqlParameter("@p_CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EMPLOYEEDATA");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(new EmployeeData((string)dr["EmployeeID"], (string)dr["Position"], (string)dr["CompanyCode"], CheckDate));
            }

            oTable.Dispose();

            return oReturn;
        }

        public override DataTable GetEmployeeByEmpID(string EmployeeID, DateTime CheckDate, string oLanguage)
        {
            DataTable oReturn = new DataTable();
            oReturn.Columns.Add("EmployeeID", typeof(string));
            oReturn.Columns.Add("EmployeeName", typeof(string));
            oReturn.Columns.Add("PositionID", typeof(string));
            oReturn.Columns.Add("PositionName", typeof(string));
            oReturn.Columns.Add("OrgUnitID", typeof(string));
            oReturn.Columns.Add("OrgUnitName", typeof(string));

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_INFOTYPE0001GetAllByEmpID", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            oParam = new SqlParameter("@p_CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EMPLOYEEDATA");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();

            if (oTable != null && oTable.Rows.Count > 0)
            {
                EmployeeData oEmp = new EmployeeData();
                oEmp = new EmployeeData(oTable.Rows[0]["EmployeeID"].ToString(), oTable.Rows[0]["Position"].ToString(), oTable.Rows[0]["CompanyCode"].ToString(), CheckDate);
                string sPositionName = oEmp.OrgAssignment.PositionData.AlternativeName(oLanguage);
                string sOrgUnitName = oEmp.OrgAssignment.OrgUnitData.AlternativeName(oLanguage);

                oReturn.Rows.Add(oTable.Rows[0]["EmployeeID"].ToString(), oEmp.MultiLanguageName, oTable.Rows[0]["Position"].ToString(), sPositionName, oTable.Rows[0]["OrgUnit"].ToString(), sOrgUnitName);
                //oReturn.Rows.Add(oTable.Rows[0]["EmployeeID"].ToString(), oTable.Rows[0]["Name"].ToString(), oTable.Rows[0]["Position"].ToString(), sPositionName, oTable.Rows[0]["OrgUnit"].ToString(), sOrgUnitName);
            }

            oTable.Dispose();

            return oReturn;
        }

        public override DataSet GetEmployeeActiveGetAllSearchText(string SearhText, DateTime CheckDate, string LanguageCode)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_EmployeeActiveGetAllSearchText", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;

            oParam = new SqlParameter("@p_SearhText", SqlDbType.VarChar);
            oParam.Value = SearhText;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_LanguageCode", SqlDbType.VarChar);
            oParam.Value = LanguageCode;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataSet oTable = new DataSet("USER");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            return oTable;
        }


        public override DataSet GetEmployeeGetAllSearchText(string SearhText, DateTime CheckDate, string LanguageCode)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_EmployeeGetAllSearchText", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;

            oParam = new SqlParameter("@p_SearhText", SqlDbType.VarChar);
            oParam.Value = SearhText;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_LanguageCode", SqlDbType.VarChar);
            oParam.Value = LanguageCode;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataSet oTable = new DataSet("USER");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            return oTable;
        }

        public override DateTime GetCalendarEndDate(string EmployeeID)
        {
            EmployeeData oEmp = new EmployeeData(EmployeeID, DateTime.Now);
            DateTime oReturnEndDate = oEmp.DateSpecific.HiringDate;

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_GetCalendarEndDate", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EMPLOYEEDATA");
            oConnection.Open();
            

            try
            {
                oAdapter.Fill(oTable);
                if (oTable != null && oTable.Rows.Count > 0)
                {
                    oReturnEndDate = Convert.ToDateTime(oTable.Rows[0]["CalendarEndDate"]);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Get Calendar EndDate error", ex);
            }
            finally
            {
                oConnection.Close();
                oAdapter.Dispose();
                oConnection.Dispose();
                oCommand.Dispose();
                oTable.Dispose();
            }
            return oReturnEndDate;
        }

        public override List<DelegatePosition> GetAllPositionByEmployee(string EmployeeId)
        {
            List<DelegatePosition> list_position_all = new List<DelegatePosition>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_GetPositionByEmpID", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter oParam;
            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeId;

            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_ActiveDate", SqlDbType.DateTime);
            oParam.Value = DateTime.Now.Date;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("PositionAll");
            oConnection.Open();

            try
            {
                oAdapter.Fill(oTable);
                if(oTable.Rows.Count > 0)
                {
                    foreach(DataRow row in oTable.Rows)
                    {
                        DelegatePosition position = new DelegatePosition();
                        position.ParseToObject(row);
                        list_position_all.Add(position);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Get Calendar EndDate error", ex);
            }
            finally
            {
                oConnection.Close();
                oAdapter.Dispose();
                oConnection.Dispose();
                oCommand.Dispose();
                oTable.Dispose();
            }

            return list_position_all;
        }

        //AddBy: Ratchatawan W. (2013-06-04)
        public override DataTable GetEmployeeListByOrganizationXML(string XML, DateTime CheckDate)
        {
            DataTable oReturn = new DataTable("Data");

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_EmployeeGetByOrganizationXML", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            oParam = new SqlParameter("@p_Organization", SqlDbType.Xml);
            oParam.Value = XML;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_PeriodDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);


            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.Fill(oReturn);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();

            return oReturn;
        }

        public override EmployeeINFOTYPE0001 GetDataEmployeeINFOTYPE001(string employeeId,DateTime beginDate)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_GetEmpSubGroupByOtLogPerHour", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter oParam;
            oParam = new SqlParameter("@p_EmployeeId", SqlDbType.VarChar);
            oParam.Value = employeeId;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_BeginDateOt", SqlDbType.DateTime);
            oParam.Value = beginDate.Date;
            oCommand.Parameters.Add(oParam);

            DataTable data_table = new DataTable("EmployeeINFOTYPE001");

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.Fill(data_table);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();

            EmployeeINFOTYPE0001 emp_infotype001 = new EmployeeINFOTYPE0001();
            if (data_table.Rows.Count > 0)
            {
                foreach(DataRow row in data_table.Rows)
                {
                    emp_infotype001.ParseToObject(row);
                }
            }


            return emp_infotype001;
        }

        public override EmployeeINFOTYPE0001 GetDataEmployeeINFOTYPE001(string employeeId,string year)
        {
            EmployeeINFOTYPE0001 emp_infotype001 = new EmployeeINFOTYPE0001();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);

            string sql_cmd = "Select TOP (1) * From INFOTYPE0001";
            sql_cmd += " Where EmployeeID = @p_EmployeeId And (@p_Year >= Year(BeginDate) And @p_Year <= Year(EndDate))";
            sql_cmd += " order by EndDate desc";

            SqlCommand oCommand = new SqlCommand(sql_cmd, oConnection);
            oCommand.CommandType = CommandType.Text;

            SqlParameter oParam;
            oParam = new SqlParameter("@p_EmployeeId", SqlDbType.VarChar);
            oParam.Value = employeeId;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_Year", SqlDbType.NVarChar);
            oParam.Value = year;
            oCommand.Parameters.Add(oParam);

            DataTable data_table = new DataTable("EmployeeINFOTYPE001");

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);

            oConnection.Open();

            oAdapter.Fill(data_table);
            if (data_table.Rows.Count > 0)
            {
                foreach (DataRow row in data_table.Rows)
                {
                    emp_infotype001.ParseToObject(row);
                }
            }


            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();


            return emp_infotype001;
        }

        public override List<EmployeeAllActive> GetAllEmployeeActive(DateTime date_time,string language)
        {
            var employee_all = new List<EmployeeAllActive>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_EmployeeActiveGetAll", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            oCommand.Parameters.Add("@p_CheckDate", SqlDbType.DateTime).Value = date_time.Date;
            oCommand.Parameters.Add("@p_LanguageCode", SqlDbType.VarChar).Value = language;

            DataTable db_employee = new DataTable("EmployeeActive");
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);

            oConnection.Open();

            oAdapter.Fill(db_employee);

            if(db_employee.Rows.Count > 0)
            {
                foreach(DataRow dr in db_employee.Rows)
                {
                    EmployeeAllActive employee = new EmployeeAllActive();
                    employee.ParseToObject(dr);
                    employee_all.Add(employee);
                }
            }

            
            oAdapter.Dispose();
            oCommand.Dispose();
            oConnection.Dispose();

            if (oConnection.State == ConnectionState.Closed)
                oConnection.Close();

            return employee_all;

        }

        public override List<DbDelegateGroupping> GetListDelegateGroupping()
        {
            var list_groupping = new List<DbDelegateGroupping>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_PA_GetGroupDelegate", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);

            DataTable data_table = new DataTable("tbGroupDelegate");

            oConnection.Open();
            oAdapter.Fill(data_table);
            oAdapter.Dispose();
            oCommand.Dispose();

            if(oConnection.State == ConnectionState.Open)
            {
                oConnection.Dispose();
                oConnection.Close();
            }

            if(data_table.Rows.Count > 0)
            {
                foreach(DataRow dr in data_table.Rows)
                {
                    DbDelegateGroupping data = new DbDelegateGroupping();
                    data.ParseToObject(dr);
                    list_groupping.Add(data);
                }
            }


            return list_groupping;
        }

        public override DataEmployeeInfotype0002 GetDateOfBirthByEmployee(string employeeId)
        {
            DataEmployeeInfotype0002 data_info0002 = new DataEmployeeInfotype0002();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);

            oConnection.Open();

            SqlCommand oCommand = new SqlCommand("sp_EmployeeGetDOF", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            oCommand.Parameters.Add("@Employee_ID", SqlDbType.VarChar).Value = employeeId;


            DataTable db_employee = new DataTable("DOFEmployee");
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);

            
            oAdapter.Fill(db_employee);
            oAdapter.Dispose();

            if(db_employee.Rows.Count > 0)
            {
                data_info0002.ParseToObject(db_employee);
            }
            

            oConnection.Dispose();
            if (oConnection.State == ConnectionState.Open)
                oConnection.Close();

            oCommand.Dispose();

            return data_info0002;
        }

        public override NotifyBirthDay GetNotifyBirthDayCard(string employeeId,int year)
        {
            NotifyBirthDay notify_birthday = new NotifyBirthDay();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);

            string sql_cmd = "Select TOP (1) * From BirthDayCard";
            sql_cmd += " Where EmployeeID = @p_EmployeeId And Year = @p_Year";

            SqlCommand oCommand = new SqlCommand(sql_cmd, oConnection);
            oCommand.CommandType = CommandType.Text;

            oCommand.Parameters.Add("@p_EmployeeId", SqlDbType.VarChar).Value = employeeId;
            oCommand.Parameters.Add("@p_Year", SqlDbType.Int).Value = year;


            DataTable data_table = new DataTable("BirthDay");

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.Fill(data_table);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();

            if(data_table.Rows.Count > 0)
            {
                notify_birthday.ParseToObject(data_table);
            }


            return notify_birthday;
        }

        public override void SaveAlertNotifyBirthDayCardByEmployee(string employeeId,int year,int month ,int day)
        {
            //[sp_EmployeeSaveNotifyBirthdayCard]

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction trn = oConnection.BeginTransaction();
            SqlCommand oCommand = new SqlCommand("sp_EmployeeSaveNotifyBirthdayCard", oConnection, trn);
            oCommand.CommandType = CommandType.StoredProcedure;

            DateTime date_now = DateTime.Now;

            oCommand.Parameters.Add("@p_EmployeeID", SqlDbType.VarChar).Value = employeeId;
            oCommand.Parameters.Add("@p_Year", SqlDbType.Int).Value = year;
            oCommand.Parameters.Add("@p_Month", SqlDbType.Int).Value = month;
            oCommand.Parameters.Add("@p_Day", SqlDbType.Int).Value = day;
            oCommand.Parameters.Add("@p_NotifyDate", SqlDbType.DateTime).Value = date_now;
            oCommand.Parameters.Add("@p_CreateDate", SqlDbType.DateTime).Value = date_now;
            oCommand.Parameters.Add("@p_CreateBy", SqlDbType.VarChar).Value = employeeId;
            oCommand.Parameters.Add("@p_UpdateDate", SqlDbType.DateTime).Value = date_now;
            oCommand.Parameters.Add("@p_UpdateBy", SqlDbType.VarChar).Value = employeeId;

            try
            {
                oCommand.ExecuteNonQuery();
                trn.Commit();
            }
            catch(Exception ex)
            {
                trn.Rollback();
                throw new Exception("Save user setting error", ex);
            }
            finally
            {
                oConnection.Close();
                oCommand.Dispose();
                oConnection.Dispose();

                if (oConnection.State == ConnectionState.Open)
                    oConnection.Close();
            }
        }

        public override DataSet GetEmployeeGetAllSearchTextByManager(string ManagerID, string SearhText, DateTime CheckDate, string LanguageCode)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_EmployeeGetAllSearchTextByManager", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;

            oParam = new SqlParameter("@p_ManagerID", SqlDbType.VarChar);
            oParam.Value = ManagerID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_SearhText", SqlDbType.VarChar);
            oParam.Value = SearhText;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_LanguageCode", SqlDbType.VarChar);
            oParam.Value = LanguageCode;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataSet oTable = new DataSet("USER");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            return oTable;
        }
    }
}