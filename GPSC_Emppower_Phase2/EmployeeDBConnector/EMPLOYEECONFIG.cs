using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Text;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.CONFIG;
using System.Globalization;

namespace ESS.EMPLOYEE.DB
{
    public class EMPLOYEECONFIG:AbstractEmployeeConfig
    {
        public EMPLOYEECONFIG()
        {
        }

        private CultureInfo oCL = new CultureInfo("th-TH");

        private static Configuration __config;

        #region " Private Data "
        private Configuration config
        {
            get
            {
                if (__config == null)
                {
                    __config = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().CodeBase.Replace("file:///", "").Replace("/", "\\"));
                }
                return __config;
            }
        }
        private string ConnectionString
        {
            get
            {
                if (config == null || config.AppSettings.Settings["BaseConnStr"] == null)
                {
                    return "";
                }
                else
                {
                    return config.AppSettings.Settings["BaseConnStr"].Value;
                }
            }
        }

        private string GetConnStr(string Profile)
        {
            if (config == null || config.AppSettings.Settings[Profile] == null)
            {
                return "";
            }
            else
            {
                return config.AppSettings.Settings[Profile].Value;
            }
        }
        #endregion

        #region " ConfigData "
        private delegate void GetData<T>(DataTable Table, List<T> Data);
        private delegate List<T> ParseObject<T>(DataTable dt);

        private void SaveConfig<T>(string ConfigName, List<T> Data, GetData<T> Method)
        {
            SaveConfig<T>(ConfigName, Data, Method, "BaseConnStr");
        }
        private void SaveConfig<T>(string ConfigName, List<T> Data, GetData<T> Method, string Profile)
        {
            SqlConnection oConnection = new SqlConnection(this.GetConnStr(Profile));
            SqlCommand oCommand;
            SqlTransaction tx;
            SqlDataAdapter oAdapter;
            SqlCommandBuilder oCB;
            DataTable oTable = new DataTable(ConfigName.ToUpper());
            oConnection.Open();
            tx = oConnection.BeginTransaction();
            oCommand = new SqlCommand(string.Format("Delete from {0}", ConfigName), oConnection);
            try
            {
                oCommand.Transaction = tx;
                oCommand.ExecuteNonQuery();
                oCommand.CommandText = string.Format("select * from {0}", ConfigName);
                oCommand.Transaction = tx;
                oAdapter = new SqlDataAdapter(oCommand);
                oCB = new SqlCommandBuilder(oAdapter);
                oAdapter.FillSchema(oTable, SchemaType.Source);
                Method(oTable, Data);
                oAdapter.Update(oTable);
                tx.Commit();
                oConnection.Close();
            }
            catch (Exception e)
            {
                tx.Rollback();
                throw new Exception(string.Format("Save {0} Error",ConfigName), e);
            }
            finally
            {
                oConnection.Dispose();
                oCommand.Dispose();
                oTable.Dispose();
            }
        }
        private List<T> LoadConfig<T>(string ConfigName, ParseObject<T> Method)
        {
            return LoadConfig<T>(ConfigName, Method, false);
        }
        private List<T> LoadConfig<T>(string ConfigName, ParseObject<T> Method, bool CaseSensitive)
        {
            return LoadConfig<T>(ConfigName, Method, CaseSensitive, "");
        }
        private List<T> LoadConfig<T>(string ConfigName, ParseObject<T> Method, bool CaseSensitive, string LanguageCode)
        {
            bool MultiLanguage = LanguageCode != "";
            List<T> oReturn = new List<T>();
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable(ConfigName.ToUpper());
            oTable.CaseSensitive = CaseSensitive;
            if (MultiLanguage)
            {
                oCommand = new SqlCommand(string.Format("select *,dbo.GetCommonTextItem('{0}',{0}Key,'{1}') as MultiLanguageName from {0}", ConfigName, LanguageCode), oConnection);
            }
            else
            {
                oCommand = new SqlCommand(string.Format("select * from {0}", ConfigName), oConnection);
            }
            oAdapter = new SqlDataAdapter(oCommand);
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            oReturn.AddRange(Method(oTable));
            oTable.Dispose();
            return oReturn;
        }
        private T LoadConfigSingle<T>(string ConfigName, ParseObject<T> Method, string Code)
        {
            return LoadConfigSingle<T>(ConfigName, Method, Code, "");
        }
        private T LoadConfigSingle<T>(string ConfigName, ParseObject<T> Method, string Code, string LanguageCode)
        {
            bool MultiLanguage = LanguageCode != "";
            T oReturn = default(T);
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable(ConfigName);
            oTable.CaseSensitive = false;
            if (MultiLanguage)
            {
                oCommand = new SqlCommand(string.Format("select *,dbo.GetCommonTextItem('{0}',{0}Key,'{1}') as MultiLanguageName from {0} Where {0}Key = @Key", ConfigName, LanguageCode), oConnection);
            }
            else
            {
                oCommand = new SqlCommand(string.Format("select * from {0} Where {0}Key = @Key", ConfigName), oConnection);
            }
            SqlParameter oParam = new SqlParameter("@Key", SqlDbType.VarChar);
            oParam.Value = Code;
            oCommand.Parameters.Add(oParam);
            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            List<T> list = Method(oTable);
            if (list.Count > 0)
            {
                oReturn = list[0];
            }
            oTable.Dispose();
            return oReturn;
        }
        #endregion

        #region IHRPAConfig Members

        #region " PersonalSubAreaSetting "
        public override List<PersonalSubAreaSetting> GetPersonalSubAreaSettingList(string Profile)
        {
            return this.LoadConfig<PersonalSubAreaSetting>("PersonalSubAreaSetting", new ParseObject<PersonalSubAreaSetting>(ParseToPersonalSubAreaSetting));
        }
        public override PersonalSubAreaSetting GetPersonalSubAreaSetting(string PersonalArea, string PersonalSubArea)
        {
            PersonalSubAreaSetting oReturn = new PersonalSubAreaSetting();
            if (PersonalArea == null || PersonalSubArea == null)
            {
                return null;
            }
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("PersonalSubAreaSetting");
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("select * from PersonalSubAreaSetting Where PersonalArea = @Key1 and PersonalSubArea = @Key2", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@Key1", SqlDbType.VarChar);
            oParam.Value = PersonalArea;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Key2", SqlDbType.VarChar);
            oParam.Value = PersonalSubArea;
            oCommand.Parameters.Add(oParam);
            
            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            List<PersonalSubAreaSetting> list = ParseToPersonalSubAreaSetting(oTable);
            if (list.Count > 0)
            {
                oReturn = list[0];
            }
            oTable.Dispose();
            return oReturn;

        }

        public override List<PersonalSubAreaSetting> GetPersonalSubAreaByArea(string strArea)
        {
            List<PersonalSubAreaSetting> oReturn = new List<PersonalSubAreaSetting>();
            if (String.IsNullOrEmpty(strArea))
            {
                return null;
            }
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("PersonalSubAreaSetting");
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("select * from PersonalSubAreaSetting Where PersonalArea = @Key1", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@Key1", SqlDbType.VarChar);
            oParam.Value = strArea;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            oReturn = ParseToPersonalSubAreaSetting(oTable);
            return oReturn;
        }

        private List<PersonalSubAreaSetting> ParseToPersonalSubAreaSetting(DataTable dt)
        {
            List<PersonalSubAreaSetting> oReturn = new List<PersonalSubAreaSetting>();
            foreach (DataRow dr in dt.Rows)
            {
                PersonalSubAreaSetting Item = new PersonalSubAreaSetting();
                Item.PersonalArea = (string)dr["PersonalArea"];
                Item.PersonalSubArea = (string)dr["PersonalSubArea"];
                Item.Description = (string)dr["Description"];
                Item.CountryGrouping = (string)dr["CountryGrouping"];
                Item.AbsAttGrouping = (string)dr["AbsAttGrouping"];
                Item.TimeQuotaGrouping = (string)dr["TimeQuotaGrouping"];
                Item.HolidayCalendar = (string)dr["HolidayCalendar"];
                Item.WorkScheduleGrouping = (string)dr["WorkScheduleGrouping"];
                Item.DailyWorkScheduleGrouping = (string)dr["DailyWorkScheduleGrouping"];
                oReturn.Add(Item);
            }
            return oReturn;
        }

        public override void SavePersonalSubAreaSettingList(List<PersonalSubAreaSetting> data, string Profile)
        {
            this.SaveConfig<PersonalSubAreaSetting>("PersonalSubAreaSetting", data, new GetData<PersonalSubAreaSetting>(ParsePersonalSubAreaSettingToTable), Profile);
        }

        private void ParsePersonalSubAreaSettingToTable(DataTable oTable, List<PersonalSubAreaSetting> Data)
        {
            foreach (PersonalSubAreaSetting item in Data)
            {
                DataRow oNewRow = oTable.NewRow();
                oNewRow["PersonalArea"] = item.PersonalArea;
                oNewRow["PersonalSubArea"] = item.PersonalSubArea;
                oNewRow["Description"] = item.Description;
                oNewRow["CountryGrouping"] = item.CountryGrouping;
                oNewRow["AbsAttGrouping"] = item.AbsAttGrouping;
                oNewRow["TimeQuotaGrouping"] = item.TimeQuotaGrouping;
                oNewRow["HolidayCalendar"] = item.HolidayCalendar;
                oNewRow["WorkScheduleGrouping"] = item.WorkScheduleGrouping;
                oNewRow["DailyWorkScheduleGrouping"] = item.DailyWorkScheduleGrouping;
                oTable.LoadDataRow(oNewRow.ItemArray, false);
            }
        }

        #endregion

        #region " PersonalSubGroupSetting "
        public override List<PersonalSubGroupSetting> GetPersonalSubGroupSettingList(string Profile)
        {
            return this.LoadConfig<PersonalSubGroupSetting>("PersonalSubGroupSetting", new ParseObject<PersonalSubGroupSetting>(ParseToPersonalSubGroupSetting));
        }
        public override PersonalSubGroupSetting GetPersonalSubGroupSetting(string EmpGroup, string EmpSubGroup)
        {
            PersonalSubGroupSetting oReturn = new PersonalSubGroupSetting();
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("PersonalSubGroupSetting");
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("select * from PersonalSubGroupSetting Where EmpGroup = @Key1 and EmpSubGroup = @Key2", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@Key1", SqlDbType.VarChar);
            oParam.Value = EmpGroup;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Key2", SqlDbType.VarChar);
            oParam.Value = EmpSubGroup;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            List<PersonalSubGroupSetting> list = ParseToPersonalSubGroupSetting(oTable);
            if (list.Count > 0)
            {
                oReturn = list[0];
            }
            oTable.Dispose();
            return oReturn;

        }
        private List<PersonalSubGroupSetting> ParseToPersonalSubGroupSetting(DataTable dt)
        {
            List<PersonalSubGroupSetting> oReturn = new List<PersonalSubGroupSetting>();
            foreach (DataRow dr in dt.Rows)
            {
                PersonalSubGroupSetting Item = new PersonalSubGroupSetting();
                Item.EmpGroup = (string)dr["EmpGroup"];
                Item.EmpSubGroup = (string)dr["EmpSubGroup"];
                Item.WorkScheduleGrouping = (string)dr["WorkScheduleGrouping"];
                Item.TimeQuotaTypeGrouping = (string)dr["TimeQuotaTypeGrouping"];
                oReturn.Add(Item);
            }
            return oReturn;
        }

        public override void SavePersonalSubGroupSettingList(List<PersonalSubGroupSetting> data,string Profile)
        {
            this.SaveConfig<PersonalSubGroupSetting>("PersonalSubGroupSetting", data, new GetData<PersonalSubGroupSetting>(ParsePersonalSubGroupSettingToTable), Profile);
        }

        private void ParsePersonalSubGroupSettingToTable(DataTable oTable, List<PersonalSubGroupSetting> Data)
        {
            foreach (PersonalSubGroupSetting item in Data)
            {
                DataRow oNewRow = oTable.NewRow();
                oNewRow["EmpGroup"] = item.EmpGroup;
                oNewRow["EmpSubGroup"] = item.EmpSubGroup;
                oNewRow["WorkScheduleGrouping"] = item.WorkScheduleGrouping;
                oNewRow["TimeQuotaTypeGrouping"] = item.TimeQuotaTypeGrouping;
                oTable.LoadDataRow(oNewRow.ItemArray, false);
            }
        }
        #endregion

        #region " MonthlyWorkSchedule "
        public override void SaveMonthlyWorkscheduleList(int Year, List<MonthlyWS> Data, string Profile)
        {
            SqlConnection oConnection = new SqlConnection(this.GetConnStr(Profile));
            SqlCommand oCommand;
            SqlTransaction tx;
            SqlDataAdapter oAdapter;
            SqlCommandBuilder oCB;
            SqlParameter oParam;
            DataTable oTable = new DataTable("MonthlyWorkSchedule");
            oConnection.Open();
            tx = oConnection.BeginTransaction();
            oCommand = new SqlCommand(string.Format("Delete from MonthlyWorkSchedule where WS_Year = @Year"), oConnection);
            oParam = new SqlParameter("@Year", SqlDbType.Int);
            oParam.Value = Year;
            oCommand.Parameters.Add(oParam);
            try
            {
                oCommand.Transaction = tx;
                oCommand.ExecuteNonQuery();
                oCommand.CommandText = string.Format("select * from MonthlyWorkSchedule where WS_Year = @Year");
                oCommand.Transaction = tx;
                oAdapter = new SqlDataAdapter(oCommand);
                oCB = new SqlCommandBuilder(oAdapter);
                oAdapter.FillSchema(oTable, SchemaType.Source);
                ParseMonthlyWorkscheduleToTable(oTable, Data);
                oAdapter.Update(oTable);
                tx.Commit();
                oConnection.Close();
            }
            catch (Exception e)
            {
                tx.Rollback();
                throw new Exception(string.Format("Save MonthlyWorkSchedule Error"), e);
            }
            finally
            {
                oConnection.Dispose();
                oCommand.Dispose();
                oTable.Dispose();
            }
        }

        private void ParseMonthlyWorkscheduleToTable(DataTable oTable, List<MonthlyWS> Data)
        {
            foreach (MonthlyWS item in Data)
            {
                DataRow oNewRow = oTable.NewRow();
                foreach (PropertyInfo oProp in item.GetType().GetProperties())
                {
                    if (oTable.Columns.Contains(oProp.Name))
                    {
                        oNewRow[oProp.Name] = oProp.GetValue(item, null);
                    }
                }
                oTable.LoadDataRow(oNewRow.ItemArray, false);
            }
        }

        private List<MonthlyWS> ParseToMonthlyWS(DataTable dt)
        {
            List<MonthlyWS> oReturn = new List<MonthlyWS>();
            Type oType = typeof(MonthlyWS);
            PropertyInfo oProp;
            foreach (DataRow dr in dt.Rows)
            {
                MonthlyWS Item = new MonthlyWS();
                Item.EmpSubGroupForWorkSchedule = (string)dr["EmpSubGroupForWorkSchedule"];
                Item.PublicHolidayCalendar = (string)dr["PublicHolidayCalendar"];
                Item.EmpSubAreaForWorkSchedule = (string)dr["EmpSubAreaForWorkSchedule"];
                Item.WorkScheduleRule = (string)dr["WorkScheduleRule"];
                Item.ValuationClass = (string)dr["ValuationClass"];
                Item.WS_Year = (int)dr["WS_Year"];
                Item.WS_Month = (int)dr["WS_Month"];
                string cDayName;
                for (int index = 1; index <= 31; index++)
                {
                    cDayName = string.Format("Day{0}", index.ToString("00"));
                    oProp = oType.GetProperty(cDayName);
                    oProp.SetValue(Item, dr[cDayName], null);

                    cDayName = string.Format("Day{0}_H", index.ToString("00"));
                    oProp = oType.GetProperty(cDayName);
                    oProp.SetValue(Item, dr[cDayName], null);
                }
                oReturn.Add(Item);
            }
            return oReturn;
        }

        public override MonthlyWS GetMonthlyWorkschedule(string EmpSubGroupForWorkSchedule, string EmpSubAreaForWorkSchedule, string PublicHolidayCalendar, string WorkScheduleRule, int Year, int Month)
        {
            MonthlyWS oReturn = new MonthlyWS();
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("MonthlyWS");
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("select * from MonthlyWorkSchedule Where EmpSubGroupForWorkSchedule = @Key1 and PublicHolidayCalendar = @Key2 and EmpSubAreaForWorkSchedule = @Key3 and WorkScheduleRule = @Key4 and WS_Year = @Key5 and WS_Month = @Key6", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@Key1", SqlDbType.VarChar);
            oParam.Value = EmpSubGroupForWorkSchedule;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Key2", SqlDbType.VarChar);
            oParam.Value = PublicHolidayCalendar;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Key3", SqlDbType.VarChar);
            oParam.Value = EmpSubAreaForWorkSchedule;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Key4", SqlDbType.VarChar);
            oParam.Value = WorkScheduleRule;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Key5", SqlDbType.Int);
            oParam.Value = Year;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Key6", SqlDbType.Int);
            oParam.Value = Month;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            List<MonthlyWS> list = ParseToMonthlyWS(oTable);
            if (list.Count > 0)
            {
                oReturn = list[0];
            }
            oTable.Dispose();
            return oReturn;
        }
        #endregion

        #region " SaveDailyWorkscheduleList "
        public override void SaveDailyWorkscheduleList(List<DailyWS> Data, string Profile)
        {
            SqlConnection oConnection = new SqlConnection(this.GetConnStr(Profile));
            SqlCommand oCommand;
            SqlTransaction tx;
            SqlDataAdapter oAdapter;
            SqlCommandBuilder oCB;
            DataTable oTable = new DataTable("DailyWorkschedule");
            oConnection.Open();
            tx = oConnection.BeginTransaction();
            oCommand = new SqlCommand(string.Format("Delete from DailyWorkschedule"), oConnection);
            try
            {
                oCommand.Transaction = tx;
                oCommand.ExecuteNonQuery();
                oCommand.CommandText = string.Format("select * from DailyWorkschedule");
                oAdapter = new SqlDataAdapter(oCommand);
                oCB = new SqlCommandBuilder(oAdapter);
                oAdapter.FillSchema(oTable, SchemaType.Source);
                foreach (DailyWS item in Data)
                {
                    item.LoadDataToTable(oTable);
                }
                oAdapter.Update(oTable);
                tx.Commit();
                oConnection.Close();
            }
            catch (Exception e)
            {
                tx.Rollback();
                throw new Exception(string.Format("Save DailyWorkschedule Error"), e);
            }
            finally
            {
                oConnection.Dispose();
                oCommand.Dispose();
                oTable.Dispose();
            }
        }
        #endregion

        #region " GetDailyWorkschedule "
        public override DailyWS GetDailyWorkschedule(string DailyGroup, string DailyCode, DateTime CheckDate)
        {
            DailyWS oReturn = new DailyWS();
            if (DailyCode.Contains("-"))
            {
                oReturn.WorkBeginTime = DateTime.ParseExact(DailyCode.Split('-')[0],"HH:mm:ss",oCL).TimeOfDay;
                oReturn.WorkEndTime = DateTime.ParseExact(DailyCode.Split('-')[1], "HH:mm:ss", oCL).TimeOfDay;
                oReturn.DailyWorkscheduleCode = String.Empty;
                oReturn.BeginDate = CheckDate;
                oReturn.EndDate = CheckDate;
                oReturn.IsHoliday = false;
                oReturn.WorkscheduleClass = "1";
                oReturn.WorkHours = 8;
            }
            else
            {
                SqlConnection oConnection = new SqlConnection(this.ConnectionString);
                SqlCommand oCommand;
                SqlDataAdapter oAdapter;
                DataTable oTable = new DataTable("DailyWS");
                oTable.CaseSensitive = false;
                oCommand = new SqlCommand("select * from DailyWorkSchedule Where DailyWorkscheduleGrouping = @DailyGroup and DailyWorkscheduleCode = @DailyCode and @CheckDate between BeginDate and EndDate", oConnection);
                SqlParameter oParam;
                oParam = new SqlParameter("@DailyGroup", SqlDbType.VarChar);
                oParam.Value = DailyGroup;
                oCommand.Parameters.Add(oParam);

                oParam = new SqlParameter("@DailyCode", SqlDbType.VarChar);
                oParam.Value = DailyCode;
                oCommand.Parameters.Add(oParam);

                oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
                oParam.Value = CheckDate;
                oCommand.Parameters.Add(oParam);

                oAdapter = new SqlDataAdapter(oCommand);
                oConnection.Open();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                foreach (DataRow dr in oTable.Rows)
                {
                    oReturn.ParseToObject(dr);
                    break;
                }
                oTable.Dispose();
            }
            return oReturn;
        }
        #endregion

        public override BreakPattern GetBreakPattern(string DWSGroup, string BreakCode)
        {
            BreakPattern oReturn = null;
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("BreakPattern");
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("select * from BreakPattern Where DailyWorkscheduleGrouping = @DWSGroup and BreakCode = @BreakCode", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@DWSGroup", SqlDbType.VarChar);
            oParam.Value = DWSGroup;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BreakCode", SqlDbType.VarChar);
            oParam.Value = BreakCode;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn = new BreakPattern();
                oReturn.ParseToObject(dr);
                break;
            }
            oTable.Dispose();
            return oReturn;
        }
        #endregion

        public override WFRuleSetting GetWFRuleSetting(string WFRule)
        {
            WFRuleSetting oReturn = null;
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("WFRuleSetting");
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("select * from WFRuleSetting Where WFRule = @WFRule", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@WFRule", SqlDbType.VarChar);
            oParam.Value = WFRule;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn = new WFRuleSetting();
                oReturn.ParseToObject(dr);
                break;
            }
            oTable.Dispose();
            return oReturn;
        }


        public override List<MonthlyWS> GetMonthlyWorkscheduleGroup(string EmpSubGroupForWorkSchedule, string EmpSubAreaForWorkSchedule, string PublicHolidayCalendar)
        {
            return GetMonthlyWorkscheduleGroup(EmpSubGroupForWorkSchedule, EmpSubAreaForWorkSchedule, PublicHolidayCalendar, false, false);
        }

        public override List<MonthlyWS> GetMonthlyWorkscheduleGroup(string EmpSubGroupForWorkSchedule, string EmpSubAreaForWorkSchedule, string PublicHolidayCalendar, bool IncludeMonth, bool IncludeYear)
        {
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("MonthlyWS");
            oTable.CaseSensitive = false;
            string Include = string.Empty;
            if (IncludeMonth)
                Include += ",WS_Month";
            if (IncludeYear)
                Include += ",WS_Year";
            oCommand = new SqlCommand(string.Format("SELECT distinct WorkScheduleRule {0} from MonthlyWorkSchedule Where  EmpSubGroupForWorkSchedule= @Key1 and EmpSubAreaForWorkSchedule = @Key2 and PublicHolidayCalendar = @Key3", Include), oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@Key1", SqlDbType.VarChar);
            oParam.Value = EmpSubGroupForWorkSchedule;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Key2", SqlDbType.VarChar);
            oParam.Value = EmpSubAreaForWorkSchedule;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Key3", SqlDbType.VarChar);
            oParam.Value = PublicHolidayCalendar;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            return ParseToMonthlyWorkScheduleRule(oTable);
        }

        private List<MonthlyWS> ParseToMonthlyWorkScheduleRule(DataTable dt)
        {
            List<MonthlyWS> oReturn = new List<MonthlyWS>();
            MonthlyWS item;
            foreach (DataRow dr in dt.Rows)
            {
                item = new MonthlyWS();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }
            return oReturn;
        }


        public override List<MonthlyWS> SimulateMonthlyWorkschedule(string EmpSubGroupForWorkSchedule, string PublicHolidayCalendar, string EmpSubAreaForWorkSchedule, int Year, int Month, Dictionary<string, string> DayOption)
        {
            List<MonthlyWS> oReturn = new List<MonthlyWS>();
            MonthlyWS item;

            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("MonthlyWS");
            string sql = "SELECT * from MonthlyWorkSchedule Where  EmpSubGroupForWorkSchedule= @EmpSubGroupForWorkSchedule and PublicHolidayCalendar = @PublicHolidayCalendar and EmpSubAreaForWorkSchedule = @EmpSubAreaForWorkSchedule and WS_Year = @WS_Year and WS_Month = @WS_Month";
            foreach (KeyValuePair<string, string> pair in DayOption)
                sql += String.Format(" and {0} = @{0}", pair.Value.Split('|')[0]);

            oCommand = new SqlCommand(sql, oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpSubGroupForWorkSchedule", SqlDbType.VarChar);
            oParam.Value = EmpSubGroupForWorkSchedule;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@PublicHolidayCalendar", SqlDbType.VarChar);
            oParam.Value = PublicHolidayCalendar;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EmpSubAreaForWorkSchedule", SqlDbType.VarChar);
            oParam.Value = EmpSubAreaForWorkSchedule;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@WS_Year", SqlDbType.Int);
            oParam.Value = Year;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@WS_Month", SqlDbType.Int);
            oParam.Value = Month;
            oCommand.Parameters.Add(oParam);

            string[] stringSeparators = new string[] { "[and]" };
            foreach (KeyValuePair<string, string> pair in DayOption)
            {
                oParam = new SqlParameter("@" + pair.Value.Split('|')[0], SqlDbType.VarChar);
                oParam.Value = pair.Value.Split('|')[1];
                oCommand.Parameters.Add(oParam);
            }

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            try
            {
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
            }

            foreach (DataRow row in oTable.Rows)
            {
                item = new MonthlyWS();
                item.ParseToObject(row);
                oReturn.Add(item);
            }

            return oReturn;
        }

        public override List<DailyWS> GetDailyWSByWorkscheduleGrouping(string WorkScheduleGrouping, DateTime CheckDate)
        {
            List<DailyWS> oReturn = new List<DailyWS>();
            DailyWS oitem = new DailyWS();

            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("DailyWS");
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("select * from DailyWorkSchedule Where DailyWorkscheduleGrouping = @DailyWorkscheduleGrouping and @CheckDate between BeginDate and EndDate", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@DailyWorkscheduleGrouping", SqlDbType.VarChar);
            oParam.Value = WorkScheduleGrouping;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oitem = new DailyWS();
                oitem.ParseToObject(dr);
                oReturn.Add(oitem);
            }
            oTable.Dispose();
            return oReturn;
        }

        public override List<string> GetPersonalSubAreaWSRMapping(string strSubArea)
        {
            List<string> oReturn = new List<string>();
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("PersonalSubAreaWSRMapping");
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("SELECT * FROM PersonalSubAreaWSRMapping WHERE PersonalSubArea = @SubArea", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@SubArea", SqlDbType.VarChar);
            oParam.Value = strSubArea;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(dr["WorkScheduleRule"].ToString());
            }
            oTable.Dispose();
            return oReturn;
        }
    }
}
