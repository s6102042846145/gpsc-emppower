using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Reflection;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.OM;

namespace ESS.EMPLOYEE.DB
{
    public class OMSERVICE : AbstractOMService
    {
        #region " private data "
        private static Configuration __config;
        private static Configuration config
        {
            get
            {
                if (__config == null)
                {
                    __config = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().CodeBase.Replace("file:///", "").Replace("/", "\\"));
                }
                return __config;
            }
        }
        private string ConnectionString
        {
            get
            {
                return config.AppSettings.Settings["BaseConnStr"].Value;
            }
        }
        private string GetConnectionString(string profile)
        {
            return config.AppSettings.Settings[profile].Value;
        }
        #endregion

        public OMSERVICE()
        {
        }

        public override List<INFOTYPE1001> GetAllRelation(List<ObjectType> objectTypes, List<string> relationCodes, string Profile)
        {
            List<INFOTYPE1001> returnValue = new List<INFOTYPE1001>();
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString("WORKFLOW"));
            string sub = "";
            SqlCommand oCommand = new SqlCommand();
            oCommand.Connection = oConnection;
            if (objectTypes.Count > 0)
            {
                SqlParameter oParam;
                for (int index = 0; index < objectTypes.Count; index++)
                {
                    sub += string.Format(",@ObjectType{0}", index);
                    oParam = new SqlParameter(string.Format("@ObjectType{0}", index), SqlDbType.VarChar);
                    oParam.Value = ((char)objectTypes[index]).ToString();
                    oCommand.Parameters.Add(oParam);
                }
                sub = "(" + sub.TrimStart(',') + ")";
                oCommand.CommandText = "select * from INFOTYPE1001 Where ObjectType in " + sub;
            }
            else
            {
                oCommand.CommandText = "select * from INFOTYPE1001";
            }

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<INFOTYPE1001>();
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1001 ret = new INFOTYPE1001();
                ret.ParseToObject(dr);
                returnValue.Add(ret);
            }
            oTable.Dispose();
            return returnValue;
        }

        public override List<INFOTYPE1001> GetAllRelation(string objId1, string objId2, List<ObjectType> objectTypes, List<string> relationCodes, string Profile)
        {
            List<INFOTYPE1001> returnValue = new List<INFOTYPE1001>();
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString("WORKFLOW"));
            string sub = "";
            SqlCommand oCommand = new SqlCommand();
            oCommand.Connection = oConnection;
            if (objectTypes.Count > 0)
            {
                SqlParameter oParam;
                for (int index = 0; index < objectTypes.Count; index++)
                {
                    sub += string.Format(",@ObjectType{0}", index);
                    oParam = new SqlParameter(string.Format("@ObjectType{0}", index), SqlDbType.VarChar);
                    oParam.Value = ((char)objectTypes[index]).ToString();
                    oCommand.Parameters.Add(oParam);
                }
                sub = "(" + sub.TrimStart(',') + ")";
                oCommand.CommandText = "select * from INFOTYPE1001 Where ObjectType in " + sub + " AND";
            }
            else
            {
                oCommand.CommandText = "select * from INFOTYPE1001 Where";
            }

            oCommand.CommandText += string.Format(" ObjectID >= '{0}' AND ObjectID <= '{1}'", objId1, objId2);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<INFOTYPE1001>();
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1001 ret = new INFOTYPE1001();
                ret.ParseToObject(dr);
                returnValue.Add(ret);
            }
            oTable.Dispose();
            return returnValue;
        }

        public override List<INFOTYPE1000> GetAllObject(List<ObjectType> objectTypes, string Mode)
        {
            List<INFOTYPE1000> returnValue = new List<INFOTYPE1000>();
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString("WORKFLOW"));
            string sub = "";
            SqlCommand oCommand = new SqlCommand();
            oCommand.Connection = oConnection;
            if (objectTypes.Count > 0)
            {
                SqlParameter oParam;
                for (int index = 0; index < objectTypes.Count; index++)
                {
                    sub += string.Format(",@ObjectType{0}", index);
                    oParam = new SqlParameter(string.Format("@ObjectType{0}", index), SqlDbType.VarChar);
                    oParam.Value = ((char)objectTypes[index]).ToString();
                    oCommand.Parameters.Add(oParam);
                }
                sub = "(" + sub.TrimStart(',') + ")";
                oCommand.CommandText = "select * from INFOTYPE1000 Where ObjectType in " + sub;
            }
            else
            {
                oCommand.CommandText = "select * from INFOTYPE1000";
            }

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<INFOTYPE1000>();
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1000 ret = new INFOTYPE1000();
                ret.ParseToObject(dr);
                returnValue.Add(ret);
            }
            oTable.Dispose();
            return returnValue;
        }

        public override List<INFOTYPE1000> GetAllObject(string objId1, string objId2, List<ObjectType> objectTypes, string Mode)
        {
            List<INFOTYPE1000> returnValue = new List<INFOTYPE1000>();
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString("WORKFLOW"));
            string sub = "";
            SqlCommand oCommand = new SqlCommand();
            oCommand.Connection = oConnection;
            if (objectTypes.Count > 0)
            {
                SqlParameter oParam;
                for (int index = 0; index < objectTypes.Count; index++)
                {
                    sub += string.Format(",@ObjectType{0}", index);
                    oParam = new SqlParameter(string.Format("@ObjectType{0}", index), SqlDbType.VarChar);
                    oParam.Value = ((char)objectTypes[index]).ToString();
                    oCommand.Parameters.Add(oParam);
                }
                sub = "(" + sub.TrimStart(',') + ")";
                oCommand.CommandText = "select * from INFOTYPE1000 Where ObjectType in " + sub + " AND";
            }
            else
            {
                oCommand.CommandText = "select * from INFOTYPE1000 Where";
            }

            oCommand.CommandText += string.Format(" ObjectID >= '{0}' AND ObjectID <= '{1}'", objId1, objId2);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<INFOTYPE1000>();
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1000 ret = new INFOTYPE1000();
                ret.ParseToObject(dr);
                returnValue.Add(ret);
            }
            oTable.Dispose();
            return returnValue;
        }


        #region " GetObjectData "
        public override INFOTYPE1000 GetObjectData(ObjectType objectType, string objectID, DateTime CheckDate, string LanguageCode)
        {
            INFOTYPE1000 returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString("WORKFLOW"));
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE1000 Where ObjectType = @ObjectType and ObjectID = @ObjectID and @CheckDate Between BeginDate and EndDate", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ObjectType", SqlDbType.VarChar);
            oParam.Value = ((char)objectType).ToString();
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@ObjectID", SqlDbType.VarChar);
            oParam.Value = objectID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            if (oTable.Rows.Count > 0)
            {
            returnValue = new INFOTYPE1000();
            returnValue.ParseToObject(oTable);
            if (LanguageCode != "TH")
            {
                returnValue.Text = returnValue.TextEn;
                returnValue.ShortText = returnValue.ShortTextEn;
            }
            }
            oTable.Dispose();
            return returnValue;
        }
        #endregion

        #region " SaveAllObject "
        public override void SaveAllObject(List<ObjectType> objectTypes, List<INFOTYPE1000> data, string Profile)
        {
            string cOtype = "";

            foreach (ObjectType item in objectTypes)
            {
                cOtype += string.Format(",'{0}'", (char)item);
            }

            cOtype = cOtype.TrimStart(',');

            SqlConnection oConnection = new SqlConnection(GetConnectionString(Profile));
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand1 = new SqlCommand(string.Format("delete from INFOTYPE1000 where ObjectType in ({0})", cOtype), oConnection, tx);
            SqlCommand oCommand = new SqlCommand(string.Format("select * from INFOTYPE1000 where ObjectType in ({0})", cOtype), oConnection, tx);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("INFOTYPE1000");
            try
            {
                oCommand1.ExecuteNonQuery();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                foreach (INFOTYPE1000 item in data)
                {
                    //AddBy: Ratchatawan W. (2012-02-29)
                    item.EndDate = item.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                    item.LoadDataToTable(oTable);
                }

                oAdapter.UpdateBatchSize = 1000;
                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand1.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }
        #endregion

        #region " SaveAllRelation "
        public override void SaveAllRelation(List<ObjectType> objectTypes, List<string> relationCodes, List<INFOTYPE1001> data, string Profile)
        {
            string cOtype = "";

            foreach (ObjectType item in objectTypes)
            {
                cOtype += string.Format(",'{0}'", (char)item);
            }
            
            string cRelation = "";

            foreach (string item in relationCodes)
            {
                cRelation += string.Format(",'{0}'", item);
            }

            cOtype = cOtype.TrimStart(',');
            cRelation = cRelation.TrimStart(',');

            SqlConnection oConnection = new SqlConnection(GetConnectionString(Profile));
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand1 = new SqlCommand(string.Format("delete from INFOTYPE1001 where ObjectType in ({0}) AND Relation in ({1})", cOtype, cRelation), oConnection, tx);
            SqlCommand oCommand = new SqlCommand(string.Format("select * from INFOTYPE1001 where ObjectType in ({0}) AND Relation in ({1})", cOtype, cRelation), oConnection, tx);
            SqlCommand oCommand2 = new SqlCommand("CreateSnapShot", oConnection, tx);
            SqlCommand oCommand3 = new SqlCommand("CreateTempSnapshot", oConnection, tx);
            oCommand2.CommandType = CommandType.StoredProcedure;
            oCommand3.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("INFOTYPE1001");
            try
            {
                oCommand1.ExecuteNonQuery();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                foreach (INFOTYPE1001 item in data)
                {
                    //AddBy: Ratchatawan W. (2012-02-29)
                    item.EndDate = item.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                    item.LoadDataToTable(oTable);
                }

                oAdapter.UpdateBatchSize = 1000;
                oAdapter.Update(oTable);

                oCommand2.ExecuteNonQuery();
                oCommand3.ExecuteNonQuery();

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand1.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }
        #endregion

        #region " GetOrgRoot "
        public override INFOTYPE1000 GetOrgRoot(string OrgID, int Level, DateTime CheckDate, string LanguageCode)
        {
            INFOTYPE1000 returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString("WORKFLOW"));
            SqlCommand oCommand = new SqlCommand("select @temp = dbo.GetRootOrg(@ObjectID,@Level,@CheckDate);select * from INFOTYPE1000 where objectID = @temp and @CheckDate1 between begindate and enddate", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ObjectID", SqlDbType.VarChar);
            oParam.Value = OrgID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Level", SqlDbType.Int);
            oParam.Value = Level;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate1", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@temp", SqlDbType.VarChar);
            oParam.Value = "";
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new INFOTYPE1000();
            returnValue.ParseToObject(oTable);
            if (LanguageCode != "TH")
            {
                returnValue.Text = returnValue.TextEn;
                returnValue.ShortText = returnValue.ShortTextEn;
            }
            oTable.Dispose();
            return returnValue;
        }
        #endregion

        #region " GetRelationObjects "
        public override List<INFOTYPE1000> GetRelationObjects(string ObjectID, ObjectType type, string relation, ObjectType nextObjectType, bool isReverse, string relation1, ObjectType nextObjectType1, bool isReverse1, DateTime CheckDate, string LanguageCode)
        {
            List<INFOTYPE1000> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString("WORKFLOW"));
            string cSQL = "select A.ObjectType,A.ObjectID,dbo.GetMaxDate(A.BeginDate,B.BeginDate) As BeginDate,dbo.GetMinDate(A.EndDate,B.EndDate) As EndDate,A.ShortText,A.[Text],A.ShortTextEn,A.TextEn,A.[Level],Count(C.ObjectID) CountChildren";
            cSQL += "\nfrom INFOTYPE1000 A inner join INFOTYPE1001 B";
            if (isReverse)
            {
                cSQL += "\non B.objecttype = A.ObjectType and B.ObjectID = A.ObjectID";
                if (isReverse1)
                {
                    cSQL += "\nleft outer join INFOTYPE1001 C on C.NextObjectType = B.ObjectType and C.NextObjectID = B.ObjectID and C.Relation = @relation1 and @CheckDate between C.BeginDate and C.EndDate";
                }
                else
                {
                    cSQL += "\nleft outer join INFOTYPE1001 C on C.ObjectType = B.ObjectType and C.ObjectID = B.ObjectID and C.Relation = @relation1 and @CheckDate between C.BeginDate and C.EndDate";
                }
                cSQL += "\nwhere B.relation = @relation and B.nextobjectid = @ObjectID and B.nextobjecttype = @nextObjectType and B.ObjectType = @ObjectType and @CheckDate between A.BeginDate and A.EndDate and @CheckDate between B.BeginDate and B.EndDate";
            }
            else
            {
                cSQL += "\non B.nextobjecttype = A.ObjectType and B.nextObjectID = A.ObjectID";
                if (isReverse1)
                {
                    cSQL += "\nleft outer join INFOTYPE1001 C on C.NextObjectType = B.NextObjectType and C.NextObjectID = B.NextObjectID and C.Relation = @relation1 and @CheckDate between C.BeginDate and C.EndDate";
                }
                else
                {
                    cSQL += "\nleft outer join INFOTYPE1001 C on C.ObjectType = B.ObjectType and C.NextObjectID = B.NextObjectID and C.Relation = @relation1 and @CheckDate between C.BeginDate and C.EndDate";
                }
                cSQL += "\nwhere B.relation = @relation and B.objectid = @ObjectID and B.objecttype = @nextObjectType and B.nextObjectType = @ObjectType and @CheckDate between A.BeginDate and A.EndDate and @CheckDate between B.BeginDate and B.EndDate";
            }
            cSQL += "\ngroup by A.ObjectType,A.ObjectID,A.BeginDate,B.BeginDate,A.EndDate,B.EndDate,A.ShortText,A.[Text],A.ShortTextEn,A.TextEn,A.[Level],B.OrderKey";
            cSQL += "\nOrder By B.OrderKey";
            SqlCommand oCommand = new SqlCommand(cSQL, oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@relation", SqlDbType.VarChar);
            oParam.Value = relation;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@ObjectID", SqlDbType.VarChar);
            oParam.Value = ObjectID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@nextObjectType", SqlDbType.VarChar);
            oParam.Value = (Char)nextObjectType;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@ObjectType", SqlDbType.VarChar);
            oParam.Value = (Char)type;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@relation1", SqlDbType.VarChar);
            oParam.Value = relation1;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<INFOTYPE1000>();
            INFOTYPE1000 item;
            foreach (DataRow dr in oTable.Rows)
            {
                item = new INFOTYPE1000();
                item.ParseToObject(dr);
                if (LanguageCode != "TH")
                {
                    item.Text = item.TextEn;
                    item.ShortText = item.ShortTextEn;
                }
                item.HaveChild = (int)dr["CountChildren"] > 0;
                returnValue.Add(item);
            }
            oTable.Dispose();
            return returnValue;
            //return new List<INFOTYPE1000>();
        }
        #endregion

        #region " GetEmployeeUnder "
        public override List<EmployeeData> GetEmployeeUnder(string OrgID, DateTime CheckDate)
        {
            List<EmployeeData> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString("WORKFLOW"));
            string cSQL = "select B.NextObjectID from INFOTYPE1001 A ";
            cSQL += "\ninner join INFOTYPE1001 B on B.ObjectType = A.ObjectType and B.ObjectID = A.ObjectID and B.Relation = 'A008' and B.NextObjectType = 'P'";
            cSQL += "\nwhere A.relation = 'A003' and A.NextObjectType = 'O' and A.NextObjectID = @ObjectID and @CheckDate between A.BeginDate and A.EndDate and @CheckDate between B.BeginDate and B.EndDate";
            cSQL = "select EmployeeID from INFOTYPE1001_P_O where OrgUnit = @ObjectID and @CheckDate between BeginDate and EndDate";
            SqlCommand oCommand = new SqlCommand(cSQL, oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ObjectID", SqlDbType.VarChar);
            oParam.Value = OrgID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<EmployeeData>();
            foreach (DataRow dr in oTable.Rows)
            {
                returnValue.Add(new EmployeeData((string)dr[0]));
            }
            oTable.Dispose();
            return returnValue;
        }
        #endregion

        #region " IsHaveSubOrdinate "
        public override bool IsHaveSubOrdinate(string PositionID, DateTime CheckDate)
        {
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString("WORKFLOW"));
            SqlCommand oCommand = new SqlCommand("select count(*) as SubOrdinateCount from GetApprovalDetail(@positionid , @CheckDate)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@positionid", SqlDbType.VarChar);
            oParam.Value = PositionID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            int nCount;
            oConnection.Open();
            nCount = (int)oCommand.ExecuteScalar();
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            return nCount > 0;
        }
        #endregion

        #region " SaveAllPositionBandLevel "
        public override void SaveAllPositionBandLevel(List<INFOTYPE1013> data, string Profile)
        {
            SqlConnection oConnection = new SqlConnection(GetConnectionString(Profile));
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand1 = new SqlCommand(string.Format("delete from INFOTYPE1013"), oConnection, tx);
            SqlCommand oCommand = new SqlCommand(string.Format("select * from INFOTYPE1013"), oConnection, tx);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("INFOTYPE1013");
            try
            {
                oCommand1.ExecuteNonQuery();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                foreach (INFOTYPE1013 item in data)
                {
                    //AddBy: Ratchatawan W. (2012-02-29)
                    item.EndDate = item.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                    item.LoadDataToTable(oTable);
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand1.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }
        #endregion

        #region " GetAllPositionBandLevel "
        public override List<INFOTYPE1013> GetAllPositionBandLevel(string Profile)
        {
            List<INFOTYPE1013> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString("WORKFLOW"));
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE1013", oConnection);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE1013");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<INFOTYPE1013>();
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1013 ret = new INFOTYPE1013();
                ret.ParseToObject(dr);
                returnValue.Add(ret);
            }
            oTable.Dispose();
            return returnValue;
        }

        public override List<INFOTYPE1013> GetAllPositionBandLevel(string objId1, string objId2, string Profile)
        {
            List<INFOTYPE1013> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString("WORKFLOW"));
            SqlCommand oCommand = new SqlCommand(String.Format("select * from INFOTYPE1013 Where ObjectID >= '{0}' AND ObjectID <= '{1}'",objId1,objId2), oConnection);
          
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE1013");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<INFOTYPE1013>();
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1013 ret = new INFOTYPE1013();
                ret.ParseToObject(dr);
                returnValue.Add(ret);
            }
            oTable.Dispose();
            return returnValue;
        }
        #endregion

        #region " GetPositionBandLevel "
        public override INFOTYPE1013 GetPositionBandLevel(string PositionID, DateTime CheckDate)
        {
            INFOTYPE1013 returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString("WORKFLOW"));
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE1013 Where ObjectID = @ObjectID and @CheckDate Between BeginDate and EndDate", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ObjectID", SqlDbType.VarChar);
            oParam.Value = PositionID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE1013");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new INFOTYPE1013();
            returnValue.ParseToObject(oTable);
            oTable.Dispose();
            return returnValue;
        }
        #endregion

        #region " GetEmployeeByPositionID "
        public override EmployeeData GetEmployeeByPositionID(string PositionID, DateTime CheckDate)
        {
            EmployeeData returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString("WORKFLOW"));
            string cSQL = "select A.NextObjectID As EmployeeID from INFOTYPE1001_A008 A";
            cSQL += "\nwhere A.ObjectID = @positionid and A.ObjectType = 'S' and A.Relation = 'A008' and A.NextObjectType = 'P' and @CheckDate between A.BeginDate and A.EndDate";
            SqlCommand oCommand = new SqlCommand(cSQL, oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@positionid", SqlDbType.VarChar);
            oParam.Value = PositionID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                returnValue = new EmployeeData((string)dr[0]);
                break;
            }
            oTable.Dispose();
            return returnValue;
        }
        #endregion

        #region " SaveAllApprovalData "
        public override void SaveAllApprovalData(List<INFOTYPE1003> data, string Profile)
        {
            SqlConnection oConnection = new SqlConnection(GetConnectionString(Profile));
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand1 = new SqlCommand(string.Format("delete from INFOTYPE1003"), oConnection, tx);
            SqlCommand oCommand = new SqlCommand(string.Format("select * from INFOTYPE1003"), oConnection, tx);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("INFOTYPE1003");
            try
            {
                oCommand1.ExecuteNonQuery();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                foreach (INFOTYPE1003 item in data)
                {
                    //AddBy: Ratchatawan W. (2012-02-29)
                    item.EndDate = item.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                    item.LoadDataToTable(oTable);
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand1.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }
        #endregion

        #region " GetAllApprovalData "
        public override List<INFOTYPE1003> GetAllApprovalData(string Profile)
        {
            List<INFOTYPE1003> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString("WORKFLOW"));
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE1003", oConnection);
           
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE1003");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<INFOTYPE1003>();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1003 ret = new INFOTYPE1003();
                ret.ParseToObject(dr);
                returnValue.Add(ret);
            }

            oTable.Dispose();
            return returnValue;
        }

        public override List<INFOTYPE1003> GetAllApprovalData(string objId1, string objId2, string Profile)
        {
            List<INFOTYPE1003> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString("WORKFLOW"));
            SqlCommand oCommand = new SqlCommand(String.Format("select * from INFOTYPE1003 Where ObjectID >= '{0}' AND ObjectID <= '{1}'", objId1, objId2), oConnection);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE1003");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<INFOTYPE1003>();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1003 ret = new INFOTYPE1003();
                ret.ParseToObject(dr);
                returnValue.Add(ret);
            }

            oTable.Dispose();
            return returnValue;
        }
        #endregion

        #region " GetApprovalData "
        public override INFOTYPE1003 GetApprovalData(string PositionID, DateTime CheckDate)
        {
            INFOTYPE1003 returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString("WORKFLOW"));
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE1003 Where ObjectType = 'S' and ObjectID = @ObjectID and @CheckDate Between BeginDate and EndDate", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ObjectID", SqlDbType.VarChar);
            oParam.Value = PositionID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE1003");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new INFOTYPE1003();
            returnValue.ParseToObject(oTable);
            oTable.Dispose();
            return returnValue;
        }
        #endregion

        #region " GetOrgUnder "
        public override List<INFOTYPE1000> GetOrgUnder(string OrgID, DateTime CheckDate, string LanguageCode)
        {
            List<INFOTYPE1000> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString("WORKFLOW"));
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE1000 where objecttype = 'O' and objectID in (select objectid from infotype1001_a002 where nextobjectid = @objectid and nextobjecttype = 'O' and @CheckDate1 between begindate and enddate) and @CheckDate1 between begindate and enddate", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ObjectID", SqlDbType.VarChar);
            oParam.Value = OrgID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate1", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<INFOTYPE1000>();
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1000 item = new INFOTYPE1000();
                item.ParseToObject(dr);
                if (LanguageCode != "TH")
                {
                    item.Text = item.TextEn;
                    item.ShortText = item.ShortTextEn;
                }
                returnValue.Add(item);
            }
            oTable.Dispose();
            return returnValue;
        }
        #endregion

        #region " GetOrgForMEUserSetting "
        public override List<INFOTYPE1000> GetAllOrg(List<ObjectType> list, string CompanyCode)
        {
            List<INFOTYPE1000> returnValue = new List<INFOTYPE1000>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable;
            SqlCommandBuilder oBuilder;
            SqlParameter oParam;
            string conStr = "select * from infotype1000 where ObjectType = @ObjectType and getdate() between begindate and enddate and objectid in (select orgunit from infotype0001 where CompanyCode = @CompanyCode)";
            using (oConnection = new SqlConnection(this.ConnectionString))
            {
                using (oCommand = new SqlCommand(conStr, oConnection))
                {
                    foreach (ObjectType obj in list)
                    {
                        if (obj == ObjectType.ORGANIZE)
                        {
                            oParam = new SqlParameter("@ObjectType", SqlDbType.VarChar);
                            oParam.Value = 'O';
                            oCommand.Parameters.Add(oParam);
                        }
                    }

                    oParam = new SqlParameter("@CompanyCode", SqlDbType.VarChar);
                    oParam.Value = CompanyCode;
                    oCommand.Parameters.Add(oParam);

                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("INFOTYPE1000");
                    oBuilder = new SqlCommandBuilder(oAdapter);

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        INFOTYPE1000 item = new INFOTYPE1000();
                        item.ParseToObject(dr);
                        returnValue.Add(item);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }
        #endregion

        #region " GetAllPosition "
        public override List<INFOTYPE1000> GetAllPosition(string EmployeeID, DateTime CheckDate, string LanguageCode)
        {
            List<INFOTYPE1000> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString("WORKFLOW"));
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE1000 where objecttype = 'S' and @CheckDate1 between begindate and enddate and objectID in (select objectid from infotype1001_a008 where NextObjectID = @EmployeeID and @CheckDate1 between begindate and enddate)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate1", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<INFOTYPE1000>();
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE1000 item = new INFOTYPE1000();
                item.ParseToObject(dr);
                if (LanguageCode != "TH")
                {
                    item.Text = item.TextEn;
                    item.ShortText = item.ShortTextEn;
                }
                returnValue.Add(item);
            }
            oTable.Dispose();
            return returnValue;
        }
        #endregion

        public override INFOTYPE1000 GetOrgUnit(string PositionID, DateTime CheckDate, string Language)
        {
            INFOTYPE1000 returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString("WORKFLOW"));
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE1000 Where ObjectID in (select NextObjectID from INFOTYPE1001_A003 where @CheckDate between BeginDate and EndDate and ObjectID = @PositionID) and @CheckDate Between BeginDate and EndDate", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@PositionID", SqlDbType.VarChar);
            oParam.Value = PositionID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                returnValue = new INFOTYPE1000();
                returnValue.ParseToObject(dr);
                if (Language != "TH")
                {
                    returnValue.Text = returnValue.TextEn;
                    returnValue.ShortText = returnValue.ShortTextEn;
                }
                break;
            }
            oTable.Dispose();
            return returnValue;
        }

        public override INFOTYPE1000 FindNextPosition(string CurrentPositionID, DateTime CheckDate, string LanguageCode)
        {
            INFOTYPE1000 returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString("WORKFLOW"));
            SqlCommand oCommand = new SqlCommand("select NextObjectID from INFOTYPE1001 where ObjectID = @CurrentID and ObjectType = 'S' and Relation = 'A002' and NextObjectType = 'S' and @CheckDate between BeginDate and EndDate", oConnection);
            SqlCommand oCommand1;
            SqlParameter oParam;
            SqlDataAdapter oAdapter1;
            DataTable oTable1;
            oParam = new SqlParameter("@CurrentID", SqlDbType.VarChar);
            oParam.Value = CurrentPositionID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate.Date;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("RELATION");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            string reportToID = "";

            if (oTable.Rows.Count > 0)
            {
                reportToID = (string)oTable.Rows[0]["NextObjectID"];
            }

            if (reportToID == "")
            {
                oCommand.CommandText = "select NextObjectID from INFOTYPE1001 where ObjectID = @CurrentID and ObjectType = 'S' and Relation = 'A003' and NextObjectType = 'O' and @CheckDate between BeginDate and EndDate";
                oTable = new DataTable("RELATION");
                oConnection.Open();
                oAdapter.Fill(oTable);
                oConnection.Close();

                string currentOrgUnit = "";

                if (oTable.Rows.Count > 0)
                {
                    currentOrgUnit = (string)oTable.Rows[0]["NextObjectID"];
                }
                while (currentOrgUnit.Trim() != "")
                {
                    oCommand1 = new SqlCommand("select ObjectID from INFOTYPE1001 where NextObjectID = @CurrentID and ObjectType = 'S' and Relation = 'A012' and NextObjectType = 'O' and @CheckDate between BeginDate and EndDate", oConnection);
                    oParam = new SqlParameter("@CurrentID", SqlDbType.VarChar);
                    oParam.Value = currentOrgUnit;
                    oCommand1.Parameters.Add(oParam);

                    oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
                    oParam.Value = CheckDate.Date;
                    oCommand1.Parameters.Add(oParam);

                    oAdapter1 = new SqlDataAdapter(oCommand1);
                    oTable1 = new DataTable("RELATION1");
                    oConnection.Open();
                    oAdapter1.Fill(oTable1);
                    oConnection.Close();
                    string managerPosID = "";
                    if (oTable1.Rows.Count > 0)
                    {
                        managerPosID = (string)oTable1.Rows[0]["ObjectID"];
                    }
                    if (managerPosID != "" && managerPosID != CurrentPositionID)
                    {
                        oCommand1.CommandText = "select NextObjectID from INFOTYPE1001 where ObjectID = @CurrentID and ObjectType = 'S' and Relation = 'A008' and NextObjectType = 'P' and @CheckDate between BeginDate and EndDate";
                        oCommand1.Parameters["@CurrentID"].Value = managerPosID;

                        oTable1 = new DataTable("RELATION1");
                        oConnection.Open();
                        oAdapter1.Fill(oTable1);
                        oConnection.Close();
                        if (oTable1.Rows.Count > 0)
                        {
                            reportToID = managerPosID;
                            break;
                        }
                    }

                    oCommand1.CommandText = "select NextObjectID from INFOTYPE1001 where ObjectID = @CurrentID and ObjectType = 'O' and Relation = 'A002' and NextObjectType = 'O' and @CheckDate between BeginDate and EndDate";
                    oCommand1.Parameters["@CurrentID"].Value = currentOrgUnit;

                    oTable1 = new DataTable("RELATION1");
                    oConnection.Open();
                    oAdapter1.Fill(oTable1);
                    oConnection.Close();
                    if (oTable1.Rows.Count > 0)
                    {
                        currentOrgUnit = (string)oTable1.Rows[0]["NextObjectID"];
                    }
                    else
                    {
                        currentOrgUnit = "";
                    }
                }

            }

            oConnection.Dispose();
            oCommand.Dispose();
            if (reportToID != "")
            {
                returnValue = INFOTYPE1000.GetObjectData(ObjectType.POSITION, reportToID, CheckDate, LanguageCode);
            }
            return returnValue;
        }

        public override INFOTYPE1000 GetOrgParent(string OrgID, DateTime CheckDate)
        {
            INFOTYPE1000 returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString("WORKFLOW"));
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE1001 Where ObjectID = @OrgID and ObjectType='O' and NextObjectType='O' and @CheckDate Between BeginDate and EndDate", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@OrgID", SqlDbType.VarChar);
            oParam.Value = OrgID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                returnValue = this.GetObjectData(ObjectType.ORGANIZE, (string)dr["NextObjectID"], CheckDate, "TH");
                break;
            }
            oTable.Dispose();
            return returnValue;
        }

        public override List<EmployeeData> GetEmployeeByOrg(string OrgID, DateTime CheckDate)
        {
            List<EmployeeData> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString("WORKFLOW"));
            String cSQL = "select EmployeeID from INFOTYPE1001_P_O where OrgUnit = @ObjectID and @CheckDate between BeginDate and EndDate";
            SqlCommand oCommand = new SqlCommand(cSQL, oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ObjectID", SqlDbType.VarChar);
            oParam.Value = OrgID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<EmployeeData>();
            foreach (DataRow dr in oTable.Rows)
            {
                returnValue.Add(new EmployeeData((string)dr[0]));
            }
            oTable.Dispose();
            return returnValue;
        }


        public override List<OrganizationWithLevel> GetAllOrganizationWithLevel(string Orgunit, string LanguageCode)
        {
            List<OrganizationWithLevel> oReturn = new List<OrganizationWithLevel>();

            SqlConnection oConnection = new SqlConnection(this.GetConnectionString("WORKFLOW"));
            SqlCommand oCommand = new SqlCommand("sp_UserRoleResponseAllOrganizationWithLevelGet", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter oParam;
            oParam = new SqlParameter("@p_Orgunit", SqlDbType.VarChar);
            oParam.Value = Orgunit;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_Orglevel", SqlDbType.Int);
            oParam.Value = 0;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_Orgparent", SqlDbType.VarChar);
            oParam.Value = string.Empty;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_Language", SqlDbType.VarChar);
            oParam.Value = LanguageCode;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            OrganizationWithLevel item;
            foreach (DataRow dr in oTable.Rows)
            {
                item = new OrganizationWithLevel();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }
            oTable.Dispose();
            return oReturn;
        }

        //AddBy: Ratchatawan W. (2012-11-07)
        public override List<EmployeeData> GetEmployeeByOrg(string OrgID, DateTime BeginDate, DateTime EndDate)
        {
            List<EmployeeData> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString("WORKFLOW"));
            SqlCommand oCommand = new SqlCommand("sp_EmployeeGetByOrganizationID", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            oParam = new SqlParameter("@p_OrganizationID", SqlDbType.VarChar);
            oParam.Value = OrgID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<EmployeeData>();
            foreach (DataRow dr in oTable.Rows)
            {
                returnValue.Add(new EmployeeData((string)dr[0]));
            }
            oTable.Dispose();
            return returnValue;
        }

        #region " SaveOrgUnitLevel "
        public override void SaveOrgUnitLevel(List<INFOTYPE1010> data, string Profile)
        {
            SqlConnection oConnection = new SqlConnection(GetConnectionString(Profile));
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand1 = new SqlCommand(string.Format("delete from INFOTYPE1010"), oConnection, tx);
            SqlCommand oCommand = new SqlCommand(string.Format("select * from INFOTYPE1010"), oConnection, tx);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("INFOTYPE1010");
            try
            {
                oCommand1.ExecuteNonQuery();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                foreach (INFOTYPE1010 item in data)
                {
                    item.LoadDataToTable(oTable);
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand1.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }
        #endregion

        public override INFOTYPE1010 GetLevelByOrg(string ObjectID, DateTime CheckDate)
        {
            INFOTYPE1010 oReturn = null;

            SqlConnection oConnection = new SqlConnection(this.GetConnectionString("WORKFLOW"));
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE1010 where ObjectID = @ObjectID", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ObjectID", SqlDbType.VarChar);
            oParam.Value = ObjectID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE1010");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            if (oTable.Rows.Count > 0)
            {
                DataRow row = oTable.Rows[0];
                oReturn = new INFOTYPE1010();
                oReturn.ParseToObject(row);
            }
            return oReturn;
        }

        public override INFOTYPE1000 GetJobByPositionID(string PositionID, DateTime CheckDate)
        {
            INFOTYPE1000 returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString("WORKFLOW"));
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("SELECT job.*");
            sb.AppendLine("FROM dbo.INFOTYPE1001_a007 rel INNER JOIN dbo.INFOTYPE1000 job");
            sb.AppendLine("	ON rel.ObjectID = job.ObjectID");
            sb.AppendLine("		AND rel.ObjectType = job.ObjectType");
            sb.AppendLine("		AND @CheckDate BETWEEN rel.BeginDate AND rel.EndDate");
            sb.AppendLine("		AND @CheckDate BETWEEN job.BeginDate AND job.EndDate");
            sb.AppendLine("WHERE rel.NextObjectID=@PositionID AND rel.NextObjectType = 'S'");
            SqlCommand oCommand = new SqlCommand(sb.ToString(), oConnection);
            oCommand.Parameters.AddWithValue("@PositionID", PositionID);
            oCommand.Parameters.AddWithValue("@CheckDate", CheckDate);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OBJECT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            if (oTable.Rows.Count > 0)
            {
                returnValue = new INFOTYPE1000();
                returnValue.ParseToObject(oTable);
            }
            oTable.Dispose();
            return returnValue;
        }
    }
}
