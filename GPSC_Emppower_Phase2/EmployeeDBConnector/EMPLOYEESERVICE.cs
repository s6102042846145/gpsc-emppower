using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Reflection;
using System.Security;
using System.Security.Cryptography;
using ESS.EMPLOYEE;

namespace ESS.EMPLOYEE.DB
{
    public class EMPLOYEESERVICE : AbstractEmployeeService
    {
        public EMPLOYEESERVICE()
        {
        }

        #region " private data "
        private static Configuration __config;
        private static Configuration config
        {
            get
            {
                if (__config == null)
                {
                    __config = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().CodeBase.Replace("file:///", "").Replace("/", "\\"));
                }
                return __config;
            }
        }
        private string ConnectionString
        {
            get
            {
                if (config == null || config.AppSettings.Settings["BaseConnStr"] == null)
                {
                    return "";
                }
                else
                {
                    return config.AppSettings.Settings["BaseConnStr"].Value;
                }
            }
        }
        private string PINConnectionString
        {
            get
            {
                if (config == null || config.AppSettings.Settings["PINConnStr"] == null)
                {
                    return "";
                }
                else
                {
                    return config.AppSettings.Settings["PINConnStr"].Value;
                }
            }
        }
        private string APPPATH
        {
            get
            {
                if (config == null || config.AppSettings.Settings["APPPATH"] == null)
                {
                    return "";
                }
                else
                {
                    return config.AppSettings.Settings["APPPATH"].Value;
                }
            }
        }

        private string EXCHANGESERVER
        {
            get
            {
                if (config == null || config.AppSettings.Settings["EXCHANGESERVER"] == null)
                {
                    return "";
                }
                else
                {
                    return config.AppSettings.Settings["EXCHANGESERVER"].Value;
                }
            }
        }

        private string SystemEmail
        {
            get
            {
                if (config == null || config.AppSettings.Settings["SystemEmail"] == null)
                {
                    return "";
                }
                else
                {
                    return config.AppSettings.Settings["SystemEmail"].Value;
                }
            }
        }


        private string GetConnectionString(string profile)
        {
            return config.AppSettings.Settings[profile].Value;
        }
        #endregion

        #region " ParseToINFOTYPE0001 "

        #endregion

        #region " ParseToUserSetting "
        private UserSetting ParseToUserSetting(DataRow dr)
        {
            UserSetting Item = new UserSetting((string)dr["UserID"]);
            Item.Language = (string)dr["LanguageCode"];
            Item.ReceiveMail = (bool)dr["ReceiveMail"];
            return Item;
        }
        #endregion

        #region " ParseToUserRoleSetting "
        private UserRoleSetting ParseToUserRoleSetting(DataRow dr)
        {
            UserRoleSetting Item = new UserRoleSetting((string)dr["UserRole"]);
            Item.Language = (string)dr["LanguageCode"];
            return Item;
        }
        #endregion

        #region IEmployeeService Members

        #region " FindManager "
        public override EmployeeData FindManager(string EmployeeID, string ManagerCode, DateTime CheckDate)
        {
            EmployeeData oReturn = null;
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("select * from GetSpecialReceipient(@EmployeeID, @Code, @CheckDate)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Code", SqlDbType.VarChar);
            oParam.Value = ManagerCode;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("RECEIPIENT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                string cManagerType = (string)dr["RECEIPIENTTYPE"];
                string cManager = (string)dr["RECEIPIENTCODE"];
                switch (cManagerType.Trim().ToUpper())
                {
                    case "S":
                        oReturn = EmployeeData.GetEmployeeByPositionID(cManager, CheckDate);
                        break;
                    case "P":
                        oReturn = new EmployeeData(cManager, CheckDate);
                        break;
                }
                break;
            }
            oTable.Dispose();
            return oReturn;
        }
        #endregion

        #region " FindManagerPos "
        public string FindManagerPos(string EmployeeID, string ManagerCode, DateTime CheckDate)
        {
            string oReturn = null;
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("select * from GetSpecialReceipient(@EmployeeID, @Code, @CheckDate)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Code", SqlDbType.VarChar);
            oParam.Value = ManagerCode;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("RECEIPIENT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                string cManagerType = (string)dr["RECEIPIENTTYPE"];
                string cManager = (string)dr["RECEIPIENTCODE"];
                oReturn = cManager;
            }
            oTable.Dispose();
            return oReturn;
        }
        #endregion


        #region " GetEmployeeIDFromUsername "
        public override string GetEmployeeIDFromUserID(string UserID)
        {
            string returnValue;
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("select EmployeeID from INFOTYPE0105 Where SubType = '0001' and DataText = @UserID", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@UserID", SqlDbType.VarChar);
            oParam.Value = UserID;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EMPLOYEE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            if (oTable.Rows.Count > 0)
            {
                DataRow dr = oTable.Rows[0];
                returnValue = (string)dr["EmployeeID"];
            }
            else
            {
                returnValue = "";
            }
            oTable.Dispose();
            return returnValue;
        }
        #endregion


        #region " ValidateEmployeeID "

        public override bool ValidateEmployeeID(string EmployeeID, DateTime CheckDate)
        {
            bool returnValue = false;
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("select EmployeeID from v_current_INFOTYPE0001 Where EmployeeID = @EmpID", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EMPLOYEE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            if (oTable.Rows.Count > 0)
            {
                returnValue = true;
            }
            oTable.Dispose();
            return returnValue;
        }

        #endregion

        public override List<ESS.EMPLOYEE.CARDSETTING> GetCardSettingList(string EmployeeID, DateTime CheckDate, string Profile)
        {
            List<CARDSETTING> returnValue = new List<CARDSETTING>();
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString(Profile));
            SqlCommand oCommand;

            oCommand = new SqlCommand("select * from CardSetting Where EmployeeID = @EmpID1 and @CheckDate Between BeginDate and EndDate", oConnection);

            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate.Date;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("CardSetting");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                CARDSETTING item = new CARDSETTING();
                item.ParseToObject(dr);
                returnValue.Add(item);
            }
            oTable.Dispose();
            return returnValue;
        }

        #region " INFOTYPE0001 "

        #region " GetInfotype0001 "

        public override List<INFOTYPE0001> GetInfotype0001List(string EmployeeID1, string EmployeeID2, DateTime CheckDate, string Profile)
        {
            List<INFOTYPE0001> returnValue = new List<INFOTYPE0001>();
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString(Profile));
            SqlCommand oCommand;
            if (CheckDate == DateTime.MinValue)
            {
                oCommand = new SqlCommand("select * from INFOTYPE0001 Where EmployeeID between @EmpID1 and @EmpID2 and getdate() Between BeginDate and EndDate", oConnection);
            }
            else
            {
                oCommand = new SqlCommand("select * from INFOTYPE0001 Where EmployeeID between @EmpID1 and @EmpID2 and @CheckDate Between BeginDate and EndDate", oConnection);
            }
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand.Parameters.Add(oParam);

            if (CheckDate != DateTime.MinValue)
            {
                oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
                oParam.Value = CheckDate.Date;
                oCommand.Parameters.Add(oParam);
            }

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EMPLOYEE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE0001 item = new INFOTYPE0001();
                item.ParseToObject(dr);
                returnValue.Add(item);
            }
            oTable.Dispose();
            return returnValue;
        }

        #endregion

        #region " SaveInfotype0001 "
        public override void SaveInfotype0001(string EmployeeID1, string EmployeeID2, List<INFOTYPE0001> data, string profile)
        {
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString(profile));
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand1 = new SqlCommand("delete from INFOTYPE0001 where EmployeeID between @EmpID1 and @EmpID2", oConnection, tx);
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand1.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand1.Parameters.Add(oParam);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE0001 where EmployeeID between @EmpID1 and @EmpID2", oConnection, tx);
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("INFOTYPE0001");
            try
            {
                oCommand1.ExecuteNonQuery();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                foreach (INFOTYPE0001 item in data)
                {
                    //AddBy: Ratchatawan W. (2012-02-29)
                    item.EndDate = item.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                    item.LoadDataToTable(oTable);
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand1.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }
        #endregion

        #endregion

        #region " INFOTYPE0182 "

        #region " GetInfoType0182 "
        public override INFOTYPE0182 GetInfotype0182(string EmployeeID, string Language)
        {
            INFOTYPE0182 returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString("WORKFLOW"));
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE0182 Where EmployeeID = @EmpID and SubType = @Language and GetDate() between BeginDate and EndDate", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            string langcode = "";
            switch (Language.ToUpper())
            {
                case "EN":
                    langcode = "2";
                    break;
            }
            oParam = new SqlParameter("@Language", SqlDbType.VarChar);
            oParam.Value = langcode;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EMPLOYEE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new INFOTYPE0182();
            returnValue.ParseToObject(oTable);
            oTable.Dispose();
            return returnValue;
        }
        #endregion

        #region " SaveInfotype0182 "
        public override void SaveInfotype0182(string EmployeeID1, string EmployeeID2, List<INFOTYPE0182> data, string Profile)
        {
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString(Profile));
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand1 = new SqlCommand("delete from INFOTYPE0182 where EmployeeID between @EmpID1 and @EmpID2", oConnection, tx);
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand1.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand1.Parameters.Add(oParam);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE0182 where EmployeeID between @EmpID1 and @EmpID2", oConnection, tx);
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("INFOTYPE0182");
            try
            {
                oCommand1.ExecuteNonQuery();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                foreach (INFOTYPE0182 item in data)
                {
                    //AddBy: Ratchatawan W. (2012-02-29)
                    item.EndDate = item.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                    item.LoadDataToTable(oTable);
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand1.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }
        #endregion

        #endregion

        #region " INFOTYPE0105 "

        public override List<INFOTYPE0105> GetInfotype0105List(string EmployeeID1, string EmployeeID2, string Profile)
        {
            List<INFOTYPE0105> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString(Profile));
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE0105 Where EmployeeID >= @EmpID1 and EmployeeID <= @EmpID2", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EMPLOYEE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<INFOTYPE0105>();
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE0105 item = new INFOTYPE0105();
                item.ParseToObject(dr);
                returnValue.Add(item);
            }
            oTable.Dispose();
            return returnValue;
        }

        public override INFOTYPE0105 GetInfotype0105(string EmployeeID, string SubType)
        {
            INFOTYPE0105 returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString("WORKFLOW"));
            SqlCommand oCommand = new SqlCommand("select TOP 1 * from INFOTYPE0105 Where EmployeeID = @EmpID and SubType = @SubType and dateadd(dd,-1,GetDate()) between dateadd(dd,-1,BeginDate) and EndDate ORDER BY BeginDate DESC", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            string subtypeCode = "";
            switch (SubType.ToUpper())
            {
                case "EMAIL":
                    subtypeCode = "9008";
                    break;
            }

            oParam = new SqlParameter("@SubType", SqlDbType.VarChar);
            oParam.Value = subtypeCode;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EMPLOYEE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new INFOTYPE0105();
            returnValue.ParseToObject(oTable);
            oTable.Dispose();
            return returnValue;
        }

        public override void SaveInfotype0105(string EmployeeID1, string EmployeeID2, List<INFOTYPE0105> data, string profile)
        {
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString(profile));
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand1 = new SqlCommand("delete from INFOTYPE0105 where EmployeeID between @EmpID1 and @EmpID2", oConnection, tx);
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand1.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand1.Parameters.Add(oParam);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE0105 where EmployeeID between @EmpID1 and @EmpID2", oConnection, tx);
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("INFOTYPE0105");
            try
            {
                oCommand1.ExecuteNonQuery();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                foreach (INFOTYPE0105 item in data)
                {
                    //AddBy: Ratchatawan W. (2012-02-29)
                    item.EndDate = item.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                    item.LoadDataToTable(oTable);
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand1.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }

        #endregion

        #region " INFOTYPE0007 "

        #region " GetInfotype0007List "
        public override List<INFOTYPE0007> GetInfotype0007List(int Year, int Month, string TimeEvaluationClass)
        {
            List<INFOTYPE0007> returnValue = new List<INFOTYPE0007>();
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString("WORKFLOW"));
            DateTime Date1, Date2;
            Date1 = new DateTime(Year, Month, 1);
            Date2 = Date1.AddMonths(1).AddDays(-1);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE0007 Where BeginDate <= @Date2 and EndDate >= @Date1 and TimeEvaluateClass = @TimeEvaluationClass", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@Date1", SqlDbType.DateTime);
            oParam.Value = Date1;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Date2", SqlDbType.DateTime);
            oParam.Value = Date2;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@TimeEvaluationClass", SqlDbType.VarChar);
            oParam.Value = TimeEvaluationClass;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE0007");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE0007 item = new INFOTYPE0007();
                item.ParseToObject(dr);
                returnValue.Add(item);
            }
            oTable.Dispose();
            return returnValue;
        }
        public override List<INFOTYPE0007> GetInfotype0007List(string EmployeeID1, string EmployeeID2, int Year, int Month, string Profile)
        {
            List<INFOTYPE0007> returnValue = new List<INFOTYPE0007>();
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString(Profile));
            SqlCommand oCommand;
            SqlParameter oParam;

            if (Year == -1)
            {
                oCommand = new SqlCommand("select * from INFOTYPE0007 Where EmployeeID Between @EmpID and @EmpID1 ", oConnection);
                oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
                oParam.Value = EmployeeID1;
                oCommand.Parameters.Add(oParam);

                oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
                oParam.Value = EmployeeID2;
                oCommand.Parameters.Add(oParam);
            }
            else
            {
                DateTime Date1, Date2;
                Date1 = new DateTime(Year, Month, 1);
                Date2 = Date1.AddMonths(1).AddDays(-1);
                oCommand = new SqlCommand("select * from INFOTYPE0007 Where EmployeeID Between @EmpID and @EmpID1 and BeginDate <= @Date2 and EndDate >= @Date1", oConnection);
                oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
                oParam.Value = EmployeeID1;
                oCommand.Parameters.Add(oParam);

                oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
                oParam.Value = EmployeeID2;
                oCommand.Parameters.Add(oParam);

                oParam = new SqlParameter("@Date1", SqlDbType.DateTime);
                oParam.Value = Date1;
                oCommand.Parameters.Add(oParam);

                oParam = new SqlParameter("@Date2", SqlDbType.DateTime);
                oParam.Value = Date2;
                oCommand.Parameters.Add(oParam);
            }

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE0007");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE0007 item = new INFOTYPE0007();
                item.ParseToObject(dr);
                returnValue.Add(item);
            }
            oTable.Dispose();
            return returnValue;
        }

        public override List<INFOTYPE0007> GetInfotype0007List(DateTime BeginDate, DateTime EndDate, string TimeEvaluateClass)
        {
            List<INFOTYPE0007> returnValue = new List<INFOTYPE0007>();
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString("WORKFLOW"));
            DateTime Date1, Date2;
            Date1 = BeginDate;
            Date2 = EndDate;
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE0007 Where BeginDate <= @Date2 and EndDate >= @Date1 and TimeEvaluateClass = @TimeEvaluationClass", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@Date1", SqlDbType.DateTime);
            oParam.Value = Date1;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Date2", SqlDbType.DateTime);
            oParam.Value = Date2;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@TimeEvaluationClass", SqlDbType.VarChar);
            oParam.Value = TimeEvaluateClass;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE0007");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE0007 item = new INFOTYPE0007();
                item.ParseToObject(dr);
                returnValue.Add(item);
            }
            oTable.Dispose();
            return returnValue;
        }
        #endregion

        #region " SaveInfotype0007 "
        public override void SaveInfotype0007(string EmployeeID1, string EmployeeID2, List<INFOTYPE0007> data, string profile)
        {
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString(profile));
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand1 = new SqlCommand("delete from INFOTYPE0007 where EmployeeID between @EmpID1 and @EmpID2 ", oConnection, tx);
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand1.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand1.Parameters.Add(oParam);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE0007 where EmployeeID between @EmpID1 and @EmpID2", oConnection, tx);
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("INFOTYPE0007");
            try
            {
                oCommand1.ExecuteNonQuery();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                foreach (INFOTYPE0007 item in data)
                {
                    //AddBy: Ratchatawan W. (2012-02-29)
                    item.EndDate = item.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                    item.LoadDataToTable(oTable);
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand1.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }
        #endregion

        #endregion

        #region " GetDelegatePersons "
        public override List<EmployeeData> GetDelegatePersons(string EmployeeID)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("select DelegateFrom from DelegateData Where DelegateTo = @EmployeeID and GetDate() between BeginDate and EndDate", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EMPLOYEE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            EmployeeData oEmp;
            foreach (DataRow dr in oTable.Rows)
            {
                oEmp = new EmployeeData((string)dr["DelegateFrom"]);
                oReturn.Add(oEmp);
            }
            oTable.Dispose();
            return oReturn;
        }
        #endregion

        #region " SaveWorkPlaceData "
        public override void SaveWorkPlaceData(string EmployeeID, List<WorkPlaceCommunication> workplaceList, string Profile)
        {
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString(Profile));
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;

            SqlCommand oCommand1 = new SqlCommand("delete from WorkPlaceData where EmployeeID = @EmpID1 or @EmpID1 = ''", oConnection, tx);
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand1.Parameters.Add(oParam);

            SqlCommand oCommand = new SqlCommand("select * from WorkPlaceData where EmployeeID = @EmpID1", oConnection, tx);
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("WORKPLACEDATA");
            try
            {
                oCommand1.ExecuteNonQuery();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                foreach (WorkPlaceCommunication item in workplaceList)
                {
                    item.LoadDataToTable(oTable);
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand1.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }
        #endregion

        #endregion

        #region " User "

        #region " GetUserInResponse "
        public override List<EmployeeData> GetUserInResponse(UserRoleResponseSetting setting)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("select distinct * from GetUserInResponse(@EmpID,@UserRole,@IncludeSub,@CheckDate)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = WORKFLOW.WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@UserRole", SqlDbType.VarChar);
            oParam.Value = setting.UserRole;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@IncludeSub", SqlDbType.Bit);
            oParam.Value = setting.IncludeSub;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = DateTime.Now;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("USER");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(new EmployeeData((string)dr["EmployeeID"]));
            }
            oTable.Dispose();
            return oReturn;
        }

        public override List<EmployeeData> GetUserInResponse(UserRoleResponseSetting setting, DateTime oCheckDate)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("select distinct * from GetUserInResponse(@EmpID,@UserRole,@IncludeSub,@CheckDate)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = WORKFLOW.WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@UserRole", SqlDbType.VarChar);
            oParam.Value = setting.UserRole;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@IncludeSub", SqlDbType.Bit);
            oParam.Value = setting.IncludeSub;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = oCheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("USER");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(new EmployeeData((string)dr["EmployeeID"]));
            }
            oTable.Dispose();
            return oReturn;
        }

        public override List<EmployeeData> GetUserInResponse(UserRoleResponseSetting setting, DateTime BeginDate, DateTime EndDate)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("select distinct * from GetUserInResponseInRange(@EmpID,@UserRole,@IncludeSub,@BeginDate,@EndDate)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = WORKFLOW.WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@UserRole", SqlDbType.VarChar);
            oParam.Value = setting.UserRole;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@IncludeSub", SqlDbType.Bit);
            oParam.Value = setting.IncludeSub;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("USER");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                if (EMPLOYEE.ServiceManager.GPSC_STARTDATE > BeginDate)
                {
                    if (string.IsNullOrEmpty(dr["NewEmployeeID"].ToString()))
                        oReturn.Add(new EmployeeData((string)dr["EmployeeID"]));
                    else
                        oReturn.Add(new EmployeeData((string)dr["NewEmployeeID"]));
                }
                else
                    oReturn.Add(new EmployeeData((string)dr["EmployeeID"]));
            }
            oTable.Dispose();
            return oReturn;
        }

        public override List<EmployeeData> GetUserInResponse(UserRoleResponseSetting setting, string EmpID)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("select distinct * from GetUserInResponse(@EmpID,@UserRole,@IncludeSub,@CheckDate)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmpID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@UserRole", SqlDbType.VarChar);
            oParam.Value = setting.UserRole;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@IncludeSub", SqlDbType.Bit);
            oParam.Value = setting.IncludeSub;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = DateTime.Now;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("USER");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(new EmployeeData((string)dr["EmployeeID"]));
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion

        #region " GetUserSetting "
        public override UserSetting GetUserSetting(string EmployeeID)
        {
            UserSetting oReturn = null;
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("select * from GetUserSetting(@UserID)", oConnection);
            SqlParameter oParam = new SqlParameter("@UserID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("USERSETTING");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();

            oAdapter.Dispose();
            oCommand.Dispose();
            oConnection.Dispose();

            if (oTable.Rows.Count > 0)
            {
                oReturn = this.ParseToUserSetting(oTable.Rows[0]);
            }
            else
            {
                oReturn = new UserSetting(EmployeeID); ;
            }

            oTable.Dispose();
            return oReturn;
        }
        #endregion

        public override void SaveUserSetting(UserSetting userSetting)
        {
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand;
            SqlParameter oParam;
            oCommand = new SqlCommand("select * from UserSetting where UserID = @UserID", oConnection, tx);
            oParam = new SqlParameter("@UserID", SqlDbType.VarChar);
            oParam.Value = userSetting.Employee.EmployeeID;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            SqlCommandBuilder oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("USERSETTING");

            oCommand = new SqlCommand("select * from UserRole where UserID = @UserID", oConnection, tx);
            oParam = new SqlParameter("@UserID", SqlDbType.VarChar);
            oParam.Value = userSetting.Employee.EmployeeID;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter1 = new SqlDataAdapter(oCommand);
            SqlCommandBuilder oCB1 = new SqlCommandBuilder(oAdapter1);
            DataTable oTable1 = new DataTable("USERROLE");

            try
            {
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);
                oAdapter1.FillSchema(oTable1, SchemaType.Source);
                oAdapter1.Fill(oTable1);

                DataRow oNewRow;
                oNewRow = oTable.NewRow();
                oNewRow["UserID"] = userSetting.Employee.EmployeeID;
                oNewRow["LanguageCode"] = userSetting.Language;
                oNewRow["ReceiveMail"] = userSetting.ReceiveMail;
                oTable.LoadDataRow(oNewRow.ItemArray, false);

                List<string> notSaveList = new List<string>();
                notSaveList.Add("GROUPREQUESTOR");
                notSaveList.Add("GROUPEMPLOYEE");
                foreach (string UserRole in userSetting.Roles)
                {
                    if (UserRole.StartsWith("#"))
                        continue;
                    if (notSaveList.Contains(UserRole))
                        continue;
                    oNewRow = oTable1.NewRow();
                    oNewRow["UserID"] = userSetting.Employee.EmployeeID;
                    oNewRow["UserRole"] = UserRole;
                    oTable1.LoadDataRow(oNewRow.ItemArray, false);
                }
                DataView oDV = new DataView(oTable1);
                oDV.RowStateFilter = DataViewRowState.Unchanged;
                foreach (DataRowView drv in oDV)
                {
                    drv.Row.Delete();
                }

                oAdapter.Update(oTable);
                oAdapter1.Update(oTable1);

                if (!userSetting.ReceiveMail)
                {
                    oCommand = new SqlCommand("sp_MailToSetDisabled", oConnection, tx);
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
                    oParam.Value = userSetting.Employee.EmployeeID;
                    oCommand.Parameters.Add(oParam);
                    oParam = new SqlParameter("@p_DisabledDate", SqlDbType.DateTime);
                    oParam.Value = DateTime.Now;
                    oCommand.Parameters.Add(oParam);
                    oCommand.ExecuteNonQuery();
                }

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("Save user setting error", ex);
            }
            finally
            {
                oConnection.Close();
                oAdapter.Dispose();
                oCommand.Dispose();
                oConnection.Dispose();
                oTable.Dispose();
            }


        }

        #region " GetUserRole "
        public override List<string> GetUserRole(string EmployeeID)
        {
            List<string> oReturn = new List<string>();
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("select UserRole from GetUserRoles(@ID) ", oConnection);
            SqlParameter oParam = new SqlParameter("@ID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("USERROLE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            foreach (DataRow dr in oTable.Rows)
            {
                string userRole = (string)dr["UserRole"];
                if (!oReturn.Contains(userRole))
                {
                    oReturn.Add(userRole);
                }
            }

            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            oTable.Dispose();
            return oReturn;
        }
        #endregion

        public override List<string> GetUserInRole(string UserRole)
        {
            List<string> oReturn = new List<string>();
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("select UserID from UserRole where userid in ( select B.UserID from v_current_INFOTYPE0001 A inner join v_communication B on B.EmployeeID = A.EmployeeID where A.position != '99999999') and UserRole = @UserRole", oConnection);
            SqlParameter oParam = new SqlParameter("@UserRole", SqlDbType.VarChar);
            oParam.Value = UserRole;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("USERROLE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            foreach (DataRow dr in oTable.Rows)
            {
                string UserID = (string)dr["UserID"];
                if (!oReturn.Contains(UserID))
                {
                    oReturn.Add(UserID);
                }
            }

            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            oTable.Dispose();
            return oReturn;
        }

        #region " GetUserRoleSetting "
        public override UserRoleSetting GetUserRoleSetting(string UserRole)
        {
            UserRoleSetting oReturn = new UserRoleSetting(UserRole);
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("select * from GetUserRoleSetting(@UserRole)", oConnection);
            SqlParameter oParam = new SqlParameter("@UserRole", SqlDbType.VarChar);
            oParam.Value = UserRole;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("USERSETTING");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();

            oAdapter.Dispose();
            oCommand.Dispose();
            oConnection.Dispose();

            if (oTable.Rows.Count > 0)
            {
                oReturn = this.ParseToUserRoleSetting(oTable.Rows[0]);
            }

            oTable.Dispose();
            return oReturn;
        }
        #endregion

        #region " GetUserResponse "
        public override List<EmployeeData> GetUserResponse(string Role, string AdminGroup)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("select * from GetUserResponse(@UserRole,@responsecode)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@UserRole", SqlDbType.VarChar);
            oParam.Value = Role;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@responsecode", SqlDbType.VarChar);
            oParam.Value = AdminGroup;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("USER");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(new EmployeeData((string)dr["UserID"]));
            }
            oTable.Dispose();
            return oReturn;
        }
        #endregion

        #region " IsUserInResponse "
        public override bool IsUserInResponse(UserRoleResponseSetting role, string EmployeeID)
        {
            bool oReturn = false;
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("select dbo.IsUserInResponse(@EmpID,@UserRole,@IncludeSub,@CheckDate,@CheckUser) ", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = WORKFLOW.WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@UserRole", SqlDbType.VarChar);
            oParam.Value = role.UserRole;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@IncludeSub", SqlDbType.Bit);
            oParam.Value = role.IncludeSub;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = DateTime.Now;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckUser", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("USER");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn = (bool)dr[0];
                break;
            }
            oTable.Dispose();
            return oReturn;
        }
        #endregion

        #endregion

        #region " GetTasks "
        public override List<TaskCopyEmployeeConfig> GetTasks()
        {
            List<TaskCopyEmployeeConfig> oReturn = new List<TaskCopyEmployeeConfig>();
            TaskCopyEmployeeConfig task;
            task = new TaskCopyEmployeeConfig();
            task.SourceMode = "SAP";
            task.SourceProfile = "DEFAULT";
            task.TargetMode = "DB";
            task.TargetProfile = "WORKFLOW";
            task.TaskID = 0;
            task.ConfigName = "PERSONALSUBAREASETTING";
            oReturn.Add(task);

            task = new TaskCopyEmployeeConfig();
            task.SourceMode = "SAP";
            task.SourceProfile = "DEFAULT";
            task.TargetMode = "DB";
            task.TargetProfile = "WORKFLOW";
            task.TaskID = 1;
            task.ConfigName = "PERSONALSUBGROUPSETTING";
            oReturn.Add(task);

            task = new TaskCopyEmployeeConfig();
            task.SourceMode = "SAP";
            task.SourceProfile = "DEFAULT";
            task.TargetMode = "DB";
            task.TargetProfile = "WORKFLOW";
            task.TaskID = 2;
            task.ConfigName = "MONTHLYWS";
            task.Param1 = DateTime.Now.Year.ToString();
            oReturn.Add(task);

            task = new TaskCopyEmployeeConfig();
            task.SourceMode = "SAP";
            task.SourceProfile = "DEFAULT";
            task.TargetMode = "DB";
            task.TargetProfile = "WORKFLOW";
            task.TaskID = 3;
            task.ConfigName = "DAILYWS";
            oReturn.Add(task);
            return oReturn;
        }
        #endregion

        #region " PIN "
        #region " Encrypt "
        private string Encrypt(string employeid, string pws)
        {
            PasswordDeriveBytes PDB;
            RC2CryptoServiceProvider RC2CSP;
            Int32 IVSize;
            Byte[] IV;
            Int32 Counter;
            Byte[] Seed = { 0x05 , 0x02 , 0x06 ,0x04,
							  0x01 , 0x07 , 0x03 ,0x08 };
            PDB = new PasswordDeriveBytes("MN" + employeid + pws, Seed, "MD5", 5);
            RC2CSP = new RC2CryptoServiceProvider();
            IVSize = RC2CSP.BlockSize / 8;
            IV = new Byte[IVSize];
            for (Counter = 0; Counter < IV.Length; Counter++)
            {
                IV[Counter] = Convert.ToByte(Counter);
            }
            RC2CSP.Key = PDB.CryptDeriveKey("RC2", "MD5", RC2CSP.KeySize, IV);
            string strRet = "";
            for (Counter = 0; Counter < RC2CSP.Key.Length; Counter++)
            {
                strRet += int.Parse(RC2CSP.Key.GetValue(Counter).ToString()).ToString("000");
            }
            return strRet;
        }
        #endregion

        #region " CreateTicket "
        public override string CreateTicket(string TicketClass, TimeSpan LifeTime, string PINcode)
        {
            string empID = WORKFLOW.WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID;
            if (!this.VerifyPinCode(empID, PINcode))
            {
                throw new Exception("PINCODE Incorrect");
            }
            return CreateTicket(empID, TicketClass, LifeTime);
        }

        private string CreateTicket(string EmployeeID, string TicketClass, TimeSpan LifeTime)
        {
            string returnValue = "";
            SqlConnection oConnection = new SqlConnection(this.PINConnectionString);
            SqlCommand oCommand = new SqlCommand("select * from TicketMaster Where EmployeeID = @EmpID and TicketClass = @TicketClass", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@TicketClass", SqlDbType.VarChar);
            oParam.Value = TicketClass;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            SqlCommandBuilder oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("TICKETMASTER");
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();

            returnValue = Guid.NewGuid().ToString();
            DataRow oNewRow = oTable.NewRow();
            oNewRow["EmployeeID"] = EmployeeID;
            oNewRow["TicketClass"] = TicketClass;
            oNewRow["TicketExpired"] = DateTime.Now.Add(LifeTime);
            oNewRow["TicketID"] = returnValue;
            oTable.LoadDataRow(oNewRow.ItemArray, false);

            oConnection.Open();
            oAdapter.Update(oTable);
            oConnection.Close();

            oTable.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            return returnValue;
        }
        #endregion

        #region " RemoveTicket "
        private void RemoveTicket(string EmployeeID, string TicketClass)
        {
            SqlConnection oConnection = new SqlConnection(this.PINConnectionString);
            SqlCommand oCommand = new SqlCommand("Delete from TicketMaster Where EmployeeID = @EmpID and TicketClass = @TicketClass", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@TicketClass", SqlDbType.VarChar);
            oParam.Value = TicketClass;
            oCommand.Parameters.Add(oParam);
            oConnection.Open();
            oCommand.ExecuteNonQuery();
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
        }
        #endregion

        #region " HavePinCode "
        public override bool HavePinCode(string EmployeeID)
        {
            bool returnValue = false;
            SqlConnection oConnection = new SqlConnection(this.PINConnectionString);
            SqlCommand oCommand = new SqlCommand("select EmployeeID from EmployeePIN Where EmployeeID = @EmpID ", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EMPLOYEEPIN");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            if (oTable.Rows.Count > 0)
            {
                returnValue = true;
            }
            oTable.Dispose();
            return returnValue;
        }
        #endregion

        #region " VerifyPinCode "
        public override bool VerifyPinCode(string EmployeeID, string pincode)
        {
            bool returnValue = false;
            SqlConnection oConnection = new SqlConnection(this.PINConnectionString);
            SqlCommand oCommand = new SqlCommand("select EmployeeID from EmployeePIN Where EmployeeID = @EmpID and PINData = @pincode", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@pincode", SqlDbType.VarChar);
            oParam.Value = Encrypt(EmployeeID, pincode);
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EMPLOYEEPIN");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            if (oTable.Rows.Count > 0)
            {
                returnValue = true;
            }
            oTable.Dispose();
            return returnValue;
        }
        #endregion

        #region " ChangePinCode "
        public override void ChangePINCode(string EmployeeID, string pincode, string NewPINcode)
        {
            if (!VerifyPinCode(EmployeeID, pincode))
            {
                return;
            }
            SetNewPin(EmployeeID, NewPINcode);
        }
        #endregion

        #region " RequestNEWPin "
        public override void RequestNewPIN(string Employeeid)
        {
            string ticket = this.CreateTicket(Employeeid, "NEWPIN", new TimeSpan(3, 0, 0));
            string cPath = this.APPPATH;

            #region " send mail "
            System.Net.Mail.SmtpClient oClient = new System.Net.Mail.SmtpClient(this.EXCHANGESERVER);
            System.Net.Mail.MailAddress oSender = new System.Net.Mail.MailAddress(SystemEmail, "WORKFLOW SYSTEM");
            System.Net.Mail.MailMessage oMessage = new System.Net.Mail.MailMessage();
            System.Net.Mail.MailAddress oTo;

            EmployeeData oEmp = new EmployeeData(Employeeid);
            oTo = new System.Net.Mail.MailAddress(oEmp.EmailAddress, oEmp.Name);
            oMessage.To.Add(oTo);
            //oTo = new System.Net.Mail.MailAddress("somchai.san@ESS.com", "SOMCHAI");
            //oMessage.To.Add(oTo);
            //oTo = new System.Net.Mail.MailAddress("chct0121@ESS.com", "AMORNSAK");
            //oMessage.To.Add(oTo);
            //oTo = new System.Net.Mail.MailAddress("somchai.san@ESS.com", "SOMCHAI");

            oMessage.ReplyTo = oTo;
            oMessage.From = oSender;
            oMessage.Sender = oSender;
            oMessage.Subject = "CHANGE NEW PIN CODE";
            oMessage.Body = string.Format("<a href=\"{0}\">Click here to create new PINCODE</a>", string.Format(cPath, ticket));
            oMessage.BodyEncoding = Encoding.UTF8;
            oMessage.IsBodyHtml = true;
            oClient.Send(oMessage);
            oMessage.Dispose();

            #endregion
        }
        #endregion

        #region " CreateNEWPin "
        public override void CreateNewPIN(string EmployeeID, string TicketID, string NewPINcode)
        {
            if (!ValidateTicket(EmployeeID, "NEWPIN", TicketID))
            {
                throw new Exception("TICKET_INCORRECT");
            }
            RemoveTicket(EmployeeID, "NEWPIN");
            SetNewPin(EmployeeID, NewPINcode);
        }
        #endregion

        #region " SetNewPin "
        private void SetNewPin(string EmployeeID, string NewPINcode)
        {
            SqlConnection oConnection = new SqlConnection(this.PINConnectionString);
            SqlCommand oCommand = new SqlCommand("select * from EmployeePIN Where EmployeeID = @EmpID", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            SqlCommandBuilder oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("EmployeePIN");
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();

            DataRow oNewRow = oTable.NewRow();
            oNewRow["EmployeeID"] = EmployeeID;
            oNewRow["PinData"] = Encrypt(EmployeeID, NewPINcode);
            oTable.LoadDataRow(oNewRow.ItemArray, false);

            oConnection.Open();
            oAdapter.Update(oTable);
            oConnection.Close();

            oTable.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
        }
        #endregion

        #region " ValidateTicket "
        public override bool ValidateTicket(string EmployeeID, string TicketClass, string TicketID)
        {
            bool returnValue = false;
            SqlConnection oConnection = new SqlConnection(this.PINConnectionString);
            SqlCommand oCommand = new SqlCommand("select EmployeeID from TicketMaster Where EmployeeID = @EmpID and TicketClass = @TicketClass and TicketID = @TicketID and GetDate() < TicketExpired", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@TicketClass", SqlDbType.VarChar);
            oParam.Value = TicketClass;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@TicketID", SqlDbType.VarChar);
            oParam.Value = TicketID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("TICKETMASTER");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            if (oTable.Rows.Count > 0)
            {
                returnValue = true;
            }
            oTable.Dispose();
            return returnValue;
        }
        #endregion

        #region " GetActiveTicket "
        public override List<TicketMaster> GetActiveTicket(string TicketClass)
        {
            List<TicketMaster> returnValue = new List<TicketMaster>();
            SqlConnection oConnection = new SqlConnection(this.PINConnectionString);
            SqlCommand oCommand = new SqlCommand("select * from TicketMaster where TicketClass = @TicketClass and TicketExpired > GetDate()", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@TicketClass", SqlDbType.VarChar);
            oParam.Value = TicketClass;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("TICKETMASTER");
            oAdapter.Fill(oTable);
            oAdapter.Dispose();
            oCommand.Dispose();
            oConnection.Dispose();
            // parsetoobject
            foreach (DataRow dr in oTable.Rows)
            {
                TicketMaster newItem = new TicketMaster();
                newItem.ParseToObject(dr);
                returnValue.Add(newItem);
            }

            oTable.Dispose();
            return returnValue;
        }
        #endregion

        #endregion

        #region Get InfoType1001_A002

        public DataTable getInfoType1001_A002(string OrgUnit)
        {
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("select ObjectID from INFOTYPE1001_A002 Where ObjectType ='O' And NextObjectID = @OrgUnit and BeginDate >= @Date1 and EndDate <= @Date1", oConnection);


            SqlParameter oParam;
            oParam = new SqlParameter("@OrgUnit", SqlDbType.VarChar);
            oParam.Value = OrgUnit;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Date1", SqlDbType.DateTime);
            oParam.Value = DateTime.Now;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE1001_A002");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            return oTable;
        }

        #endregion

        public override List<Substitution> GetInfotype2003(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            List<Substitution> returnValue = new List<Substitution>();
            SqlConnection oConnection = new SqlConnection(this.PINConnectionString);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE2003_Log Where @EmpID IN (EmployeeID,Substitute) and BeginDate < @Date2 and EndDate >= @Date1 ORDER BY RequestNo ASC", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Date1", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Date2", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE2003");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                Substitution item = new Substitution();
                item.ParseToObject(dr);
                returnValue.Add(item);
            }
            oTable.Dispose();
            return returnValue;
        }

        public override List<WorkPlaceCommunication> GetWorkplaceData(string EmployeeID)
        {
            List<WorkPlaceCommunication> oReturn = new List<WorkPlaceCommunication>();
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand;
            SqlParameter oParam;
            if (EmployeeID.Trim() == "")
            {
                oCommand = new SqlCommand("select * from WorkPlaceData where GetDate() between BeginDate and EndDate + '23:59:59'", oConnection);
            }
            else
            {
                oCommand = new SqlCommand("select * from WorkPlaceData where GetDate() between BeginDate and EndDate + '23:59:59' and EmployeeID = @EmployeeID", oConnection);
                oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
                oParam.Value = EmployeeID;
                oCommand.Parameters.Add(oParam);
            }
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EMPLOYEE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            WorkPlaceCommunication oWorkplace;
            foreach (DataRow dr in oTable.Rows)
            {
                oWorkplace = new WorkPlaceCommunication();
                oWorkplace.ParseToObject(dr);
                oReturn.Add(oWorkplace);
            }
            oTable.Dispose();
            return oReturn;
        }

        //CHAT 2011-10-05 For MassPayslip & MassTaxReport
        //CHAT 2011-10-05 ����Ѻ�֧���������ʾ�ѡ�ҹ��������˹��§ҹ�������
        #region "GetEmployeeIDByOrgUnit"
        public override DataTable GetEmployeeIDByOrgUnit(string OrgUnit)
        {
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("select employeeid from infotype0001 where orgunit = @OrgUnit and BeginDate < GETDATE() AND EndDate > GETDATE() order by employeeid", oConnection);


            SqlParameter oParam;
            oParam = new SqlParameter("@OrgUnit", SqlDbType.VarChar);
            oParam.Value = OrgUnit;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE1001_A002");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            return oTable;
        }
        #endregion

        //CHAT 2011-10-05 ����Ѻ�֧������ ResponseType �ͧ��������͵�Ǩ�ͺ�Է�������ҹ��� Type
        #region " GetUserResponseType "
        public override List<string> GetUserResponseType(string EmpID, string UserRole)
        {
            List<string> oReturn = new List<string>();
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("select distinct ResponseType from UserRoleResponse Where UserID = @UserID and UserRole in (@UserRole) ", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@UserID", SqlDbType.VarChar);
            oParam.Value = EmpID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@UserRole", SqlDbType.VarChar);
            oParam.Value = UserRole;
            oCommand.Parameters.Add(oParam);


            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("USERROLERESPONSE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add((string)dr["ResponseType"]);
            }
            oTable.Dispose();
            return oReturn;
        }
        #endregion

        //CHAT 2011-10-10 ����Ѻ�֧������ Performance �ͧ��ѡ�ҹ
        #region " GetRequestorPerformance "
        public override DataTable GetRequestorPerformance(string EmpID)
        {
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("SELECT EmployeeID,Year,Grade FROM PERFORMANCE_INFOTYPE0147 WHERE EmployeeID = @EmpID ORDER BY Year DESC", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmpID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EMPLOYEEPERFORMANCE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();

            return oTable;
        }
        #endregion

        //CHAT 2011-10-10 ����Ѻ�֧������ Competency �ͧ��ѡ�ҹ
        #region " GetRequestorCompetency "
        public override DataTable GetRequestorCompetency(string EmpID)
        {
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("SELECT EmployeeID,Year,CompetencyID,Competency_Name,Percentage FROM COMPETENCY_INFOTYPE0024 WHERE EmployeeID = @EmpID ORDER BY Year DESC", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmpID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EMPLOYEECOMPETENCY");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();

            return oTable;
        }
        #endregion


        //AddBy: Ratchatawan W. (2012-02-22)
        public override INFOTYPE0007 GetInfotype0007(string EmployeeID, DateTime CheckDate, string Profile)
        {
            INFOTYPE0007 returnValue = new INFOTYPE0007();
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString(Profile));
            SqlCommand oCommand = new SqlCommand("sp_INFOTYPE0007GET", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE0007");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE0007 item = new INFOTYPE0007();
                item.ParseToObject(dr);
                returnValue = item;
            }

            oTable.Dispose();
            return returnValue;
        }

        //AddBy: Ratchatawan W. (2012-14-23)
        public override bool ValidateManager(string EmployeeID)
        {
            bool returnValue = false;
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("sp_ValidateManager", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter oParam;
            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oConnection.Open();
            returnValue = ((int)oCommand.ExecuteScalar() > 0);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            return returnValue;
        }

        public override List<EmployeeData> GetDelegateEmployeeOMByPosition(string EmployeeID, string PositionID)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();

            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("sp_DelegateGetEmployeeOMByPosition", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_Position", SqlDbType.VarChar);
            oParam.Value = PositionID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("RECEIPIENT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();

            EmployeeData emp;
            foreach (DataRow dr in oTable.Rows)
            {

                string cManagerType = (string)dr["RECEIPIENTTYPE"];
                string cManager = (string)dr["RECEIPIENTCODE"];
                switch (cManagerType.Trim().ToUpper())
                {
                    case "S":
                        emp = EmployeeData.GetEmployeeByPositionID(cManager, DateTime.Now);
                        if (emp != null)
                        {
                            if (!oReturn.Exists(delegate(EmployeeData empDat) { return (empDat.EmployeeID == emp.EmployeeID); }))
                                oReturn.Add(emp);
                        }
                        break;
                    case "P":
                        emp = new EmployeeData(cManager, DateTime.Now);
                        if (emp != null)
                        {
                            if (!oReturn.Exists(delegate(EmployeeData empDat) { return (empDat.EmployeeID == emp.EmployeeID); }))
                                oReturn.Add(emp);
                        }
                        break;
                }
            }
            oTable.Dispose();
            return oReturn;
        }

        public override List<EmployeeData> GetDelegateEmployeeForSentMail(string DelegateFromID, string DelegateFromPositionID, int RequestTypeID, DateTime CheckDate)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();

            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("sp_DelegateGetEmployeeForSentMail", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            oParam = new SqlParameter("@p_DelegateFromID", SqlDbType.VarChar);
            oParam.Value = DelegateFromID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_DelegateFromPositionID", SqlDbType.VarChar);
            oParam.Value = DelegateFromPositionID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_RequestTypeID", SqlDbType.Int);
            oParam.Value = RequestTypeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("RECEIPIENT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();

            EmployeeData emp;
            string strEmployeeID;
            string strEmployeePositionID;
            foreach (DataRow dr in oTable.Rows)
            {

                strEmployeeID = (string)dr["DelegateTo"];
                strEmployeePositionID = (string)dr["DelegateToPosition"];
                emp = new EmployeeData(strEmployeeID, DateTime.Now);
                oReturn.Add(emp);
            }
            oTable.Dispose();
            return oReturn;
        }

        //AddBy: Ratchatawan W. (2012-11-07)
        public override List<EmployeeData> GetAllActiveEmployeeInINFOTYPE0001(DateTime CheckDate)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();


            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand;
            SqlParameter oParam;
            SqlDataAdapter oAdapter;
            DataTable oTable;
            oCommand = new SqlCommand("SELECT * FROM dbo.INFOTYPE0001 WHERE (@CheckDate BETWEEN BeginDate AND EndDate)", oConnection);
            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);
            oAdapter = new SqlDataAdapter(oCommand);
            oTable = new DataTable("EMPLOYEEDATA");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();

            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(new EmployeeData((string)dr["EmployeeID"], CheckDate));
            }

            oAdapter.Dispose();
            oCommand.Dispose();
            oTable.Dispose();

            oConnection.Dispose();



            return oReturn;
        }

        //AddBy: Ratchatawan W. (2012-11-09)
        public override List<EmployeeData> GetManagerInSameOraganizationAndSameEmpSubGroup(string EmployeeID, string PositionID,DateTime CheckDate)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();


            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand;
            SqlParameter oParam;
            SqlDataAdapter oAdapter;
            DataTable oTable;
            oCommand = new SqlCommand("sp_ApproverInSameOrganizeAndEmpsubgroupGet", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_EmployeePositionID", SqlDbType.VarChar);
            oParam.Value = PositionID;
            oCommand.Parameters.Add(oParam);
            oAdapter = new SqlDataAdapter(oCommand);
            oTable = new DataTable("EMPLOYEEDATA");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();

            foreach (DataRow dr in oTable.Rows)
            {
                EmployeeData emp = EmployeeData.GetEmployeeByPositionID((string)dr["PositionID"], CheckDate);
                if (emp != null)
                    oReturn.Add(emp);
            }

            oAdapter.Dispose();
            oCommand.Dispose();
            oTable.Dispose();

            oConnection.Dispose();

            return oReturn;
        }

        //AddBy: Ratchatawan W. (2012-12-24)
        public override void SaveInfotype0032(string EmployeeID1, string EmployeeID2, List<INFOTYPE0032> data, string profile)
        {
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString(profile));
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand1 = new SqlCommand("delete from INFOTYPE0032 where EmployeeID between @EmpID1 and @EmpID2", oConnection, tx);
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand1.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand1.Parameters.Add(oParam);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE0032 where EmployeeID between @EmpID1 and @EmpID2", oConnection, tx);
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("INFOTYPE0032");
            try
            {
                oCommand1.ExecuteNonQuery();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                foreach (INFOTYPE0032 item in data)
                {
                    item.EndDate = item.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);
                    item.LoadDataToTable(oTable);
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand1.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }

        public override INFOTYPE0032 GetInfotype0032ByEmployeeID(string EmployeeID, DateTime CheckDate)
        {
            INFOTYPE0032 oReturn = new INFOTYPE0032();

            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("sp_INFOTYPE0032GET", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE0032");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
                oReturn.ParseToObject(dr);
            oTable.Dispose();

            return oReturn;
        }

        public override List<EmployeeData> GetOTSummaryPermissionByEmployeeID(string EmployeeID, DateTime CheckDate)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();

            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("sp_OTSummaryPermissionGetByEmployeeID", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataSet ds = new DataSet();
            oConnection.Open();
            oAdapter.Fill(ds);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();

            EmployeeData oEmp;
            foreach (DataRow dr in ds.Tables[1].Rows)
            {
                oEmp = new EmployeeData(dr[0].ToString(), CheckDate);
                if (oEmp != null)
                    oReturn.Add(oEmp);
            }
            ds.Dispose();

            return oReturn;
        }

        public override bool IsHaveOTSummaryPermissionByEmployeeID(string EmployeeID, DateTime CheckDate)
        {
            bool oReturn = false;

            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("sp_OTSummaryPermissionGetByEmployeeID", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataSet ds = new DataSet();
            oConnection.Open();
            oAdapter.Fill(ds);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();

            if (ds.Tables[0].Rows.Count > 0)
                oReturn = true;
            ds.Dispose();

            return oReturn;
        }

        public override List<EmployeeData> GetAllEmployeeInINFOTYPE0001(DateTime CheckDate)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();

            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("sp_INFOTYPE0001GetAll", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            oParam = new SqlParameter("@p_CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EMPLOYEEDATA");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                oReturn.Add(new EmployeeData((string)dr["EmployeeID"], CheckDate));
            }
            oTable.Dispose();

            return oReturn;
        }


        public override DataTable GetUserInResponseForActionOfInstead(string EmployeeID, int SubjectID, DateTime CheckDate, int FilterEmployee)
        {
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("sp_UserInResponseForActionOfInsteadGet", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;

            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_SubjectID", SqlDbType.Int);
            oParam.Value = SubjectID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_FilterEmployee", SqlDbType.Int);
            oParam.Value = FilterEmployee;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("USER");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            return oTable;
        }

        #region " Infotype0030 "
        public override void SaveInfotype0030(string EmployeeID1, string EmployeeID2, List<INFOTYPE0030> data, string profile)
        {
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString(profile));
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand1 = new SqlCommand("delete from INFOTYPE0030 where EmployeeID between @EmpID1 and @EmpID2", oConnection, tx);
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand1.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand1.Parameters.Add(oParam);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE0030 where EmployeeID between @EmpID1 and @EmpID2", oConnection, tx);
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("INFOTYPE0030");
            try
            {
                oCommand1.ExecuteNonQuery();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                foreach (INFOTYPE0030 item in data)
                {
                    //AddBy: Ratchatawan W. (2012-02-10)
                    item.EndDate = item.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);
                    item.LoadDataToTable(oTable);
                }

                oAdapter.Update(oTable);
                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand1.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }

        public override INFOTYPE0030 GetInfotype0030(string EmployeeID, DateTime CheckDate)
        {
            INFOTYPE0030 oReturn = new INFOTYPE0030();

            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("sp_INFOTYPE0030GET", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE0032");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
                oReturn.ParseToObject(dr);
            oTable.Dispose();

            return oReturn;
        }
        #endregion    
    }
}
