﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.FB.DATACLASS
{
    public class FBReimburse : AbstractObject
    {
        public string RequestNo { get; set; }
        public string EmployeeID { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual decimal Vat { get; set; }
        public virtual decimal AmountReimburse { get; set; }
        public string ReceiptNo { get; set; }
        public DateTime ReceiptDate { get; set; }
        public string ReceiptCompany { get; set; }
        public string ReceiptAddress { get; set; }
        public string ReceiptPostcode { get; set; }
        public string ReceiptTaxID { get; set; }
        public int FSAList { get; set; }
        public int FSASubList { get; set; }
        public string ReimburseStatus { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime PostingDate { get; set; }
        public string PostingMessage { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public virtual decimal FlexPoints { get; set; }
        public int BeginDate_Year { get; set; }
        public int EndDate_Year { get; set; }
        public string FSAText { get; set; }
        public string FSASubText { get; set; }

        public string RequestorCompanyCode { get; set; }
        public string KeyMaster { get; set; }

        public string RoleAdmin { get; set; }
        public string RoleAdminName { get; set; }

    }

    public class ReportFBReimburseByAdmin
    {
        public string  RequestNo { get; set; }
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string WorkLocationName { get; set; }
        public string County { get; set; }
        public string Fbr_Main { get; set; }
        public string Fbr_SubMain { get; set; }
        public string ReceiptCompany { get; set; }
        public decimal Amount { get; set; }
        public decimal AmountReimburse { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? SubmitDate { get; set; }
        public DateTime? ActionDate { get; set; }
        public DateTime? PostingDate { get; set; }
    }
}
