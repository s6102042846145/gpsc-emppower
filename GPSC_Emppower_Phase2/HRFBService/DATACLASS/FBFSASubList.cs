﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.FB.DATACLASS
{
    public class FBFSASubList : AbstractObject
    {
        public int FSAID { get; set; }
        public int FSASubID { get; set; }
        public string FSASubText { get; set; }
    }
}
