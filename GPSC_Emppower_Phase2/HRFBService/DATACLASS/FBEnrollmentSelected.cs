﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.FB.DATACLASS
{
    public class FBEnrollmentSelected : AbstractObject
    {
        public int EnrollmentID { get; set; }
        public int SelectionItemID { get; set; }
        public bool isMainBenefit { get; set; }

        public string RequestNo { get; set; }
    }
}
