﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.FB.DATACLASS
{
    public class FBSelectionItem : AbstractObject
    {
        public int SelectionItemID { get; set; }
        public string SelectionItemText { get; set; }
    }
}
