﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.FB.DATACLASS
{
    public class FBYearlySelection : AbstractObject
    {
        public int SelectionYear { get; set; }
        public int SelectionItemID { get; set; }
    }
}
