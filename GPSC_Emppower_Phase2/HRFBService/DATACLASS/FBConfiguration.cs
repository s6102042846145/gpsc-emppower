﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.FB.DATACLASS
{
    public class FBConfiguration : AbstractObject
    {
        public string Constant { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
    }


}
