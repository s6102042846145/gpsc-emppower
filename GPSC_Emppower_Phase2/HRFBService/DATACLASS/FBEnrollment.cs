﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.FB.DATACLASS
{
    public class FBEnrollment : AbstractObject
    {
        public int EnrollmentID { get; set; }
        public string EmployeeID { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public virtual decimal FlexPoints { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Status { get; set; }
        public string RequestNo { get; set; }

        public int SelectionItemID { get; set; }
        public string SelectionItemText { get; set; }
        public bool isMainBenefit { get; set; }
        public virtual decimal MainBenefitSelected { get; set; }
        public int BeginDate_Year { get; set; }
        public int EndDate_Year { get; set; }
        public string RequestorCompanyCode { get; set; }
        public string KeyMaster { get; set; }

        public string RoleAdmin { get; set; }
        public string RoleAdminName { get; set; }
    }
}
