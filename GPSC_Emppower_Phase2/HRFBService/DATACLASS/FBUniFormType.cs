﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.FB.DATACLASS
{
    public class FBUniFormType
    {
		public int UniFormTypeID { get; set; }
		public string UniFormTypeName { get; set; }
		public string Status { get; set; }
		public string CreateDate { get; set; }
		public string UpdateDate { get; set; }
		public string Description { get; set; }
		public string CreateBy { get; set; }
		public string UpdateBy { get; set; }
		public string StartDate { get; set; }
		public string EndDate { get; set; }
	}
}
