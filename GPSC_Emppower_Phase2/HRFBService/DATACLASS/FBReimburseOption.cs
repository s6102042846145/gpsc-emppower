﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.FB.DATACLASS
{
    public class FBReimburseOption : AbstractObject
    {
        
        private FBReimburse __FBReimburseData = new FBReimburse();
        public FBReimburse FBReimburseData
        {
            get
            {
                return __FBReimburseData;
            }
            set
            {
                __FBReimburseData = value;
            }
        }

       

        private List<FBFSAList> __FBFSAList = new List<FBFSAList>();
        public List<FBFSAList> FBFSAList
        {
            get
            {
                return __FBFSAList;
            }
            set
            {
                __FBFSAList = value;
            }
        }

        private List<FBFSASubList> __FBFSASubList = new List<FBFSASubList>();
        public List<FBFSASubList> FBFSASubList
        {
            get
            {
                return __FBFSASubList;
            }
            set
            {
                __FBFSASubList = value;
            }
        }

        private List<FBEnrollment> __FBEnrollment = new List<FBEnrollment>();
        public List<FBEnrollment> FBEnrollment
        {
            get
            {
                return __FBEnrollment;
            }
            set
            {
                __FBEnrollment = value;
            }
        }


        public string Year { get; set; }

        public string ErrorText { get; set; }

        public string RoleAdmin { get; set; }
        public string RoleAdminName { get; set; }
    }
}
