﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.FB.DATACLASS
{
    public class FBEnrollmentOption : AbstractObject
    {
        
        private FBEnrollment __FBEnrollmentData = new FBEnrollment();
        public FBEnrollment FBEnrollmentData
        {
            get
            {
                return __FBEnrollmentData;
            }
            set
            {
                __FBEnrollmentData = value;
            }
        }

        private FBEnrollmentSelected[] __FBEnrollmentSelected = new FBEnrollmentSelected[10];
        public FBEnrollmentSelected[] FBEnrollmentSelected
        {
            get
            {
                return __FBEnrollmentSelected;
            }
            set
            {
                __FBEnrollmentSelected = value;
            }
        }

        private List<FBYearlySelection> __FBYearlySelection = new List<FBYearlySelection>();
        public List<FBYearlySelection> FBYearlySelection
        {
            get
            {
                return __FBYearlySelection;
            }
            set
            {
                __FBYearlySelection = value;
            }
        }


        private List<FBSelectionItemRate> __FBSelectionItemRate = new List<FBSelectionItemRate>();
        public List<FBSelectionItemRate> FBSelectionItemRate
        {
            get
            {
                return __FBSelectionItemRate;
            }
            set
            {
                __FBSelectionItemRate = value;
            }
        }

        public string Year { get; set; }

        public string ErrorText { get; set; }

        public string EmpSubGroup { get; set; }

        public string AgreeStatus { get; set; }

        public bool ProbationCutOff { get; set; }

    }
}
