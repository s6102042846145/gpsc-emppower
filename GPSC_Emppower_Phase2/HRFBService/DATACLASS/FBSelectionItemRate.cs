﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.FB.DATACLASS
{
    public class FBSelectionItemRate : AbstractObject
    {
        public int SelectionItemID { get; set; }
        public string EmpSubGroup { get; set; }
        public virtual decimal Rate { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
