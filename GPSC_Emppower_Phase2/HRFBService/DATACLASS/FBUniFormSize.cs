﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.FB.DATACLASS
{
    public class FBUniFormSize
    {
        public int SizeID { get; set; }
        public int UniFormID { get; set; }

        public int UniFormTypeID { get; set; }
        public decimal CWLength { get; set; }
        public decimal Length { get; set; }
        public string Size { get; set; }
        public string Status { get; set; }
        public string CreateDate { get; set; }
        public string UpdateDate { get; set; }
        public string Images { get; set; }
        public string CreateBy { get; set; }
        public string UpdateBy { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}
