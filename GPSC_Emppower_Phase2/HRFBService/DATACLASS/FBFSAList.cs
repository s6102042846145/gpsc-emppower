﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.FB.DATACLASS
{
    public class FBFSAList : AbstractObject
    {
        public int FSAID { get; set; }
        public string FSAText { get; set; }
    }
}
