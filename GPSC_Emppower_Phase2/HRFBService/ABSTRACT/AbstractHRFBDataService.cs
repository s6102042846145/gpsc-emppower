﻿using ESS.HR.FB.DATACLASS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.DATACLASS;
using ESS.HR.FB.INTERFACE;

namespace ESS.HR.FB.ABSTRACT
{
    public abstract class AbstractHRFBDataService : IHRFBDataService
    {
        public virtual bool FB_CheckEnrollmentDuplicate(string Year, string EmployeeID, String RequestNo)
        {
            throw new NotImplementedException();
        }

        public virtual bool FB_CheckPeriodSetting(string Year)
        {
            throw new NotImplementedException();
        }

        public virtual List<FBConfiguration> FB_ConfigurationGet(string Constant)
        {
            throw new NotImplementedException();
        }

        public virtual void FB_EnrollmentSave(FBEnrollment oFBEnrollment)
        {
            throw new NotImplementedException();
        }

        public virtual void FB_EnrollmentSelectedSave(FBEnrollmentSelected oFBEnrollmentSelected)
        {
            throw new NotImplementedException();
        }

        public virtual void FB_EnrollmentSelectedDelete(string RequestNo)
        {
            throw new NotImplementedException();
        }

        public virtual List<FBSelectionItemRate> FB_SelectionItemRateGet()
        {
            throw new NotImplementedException();
        }

        public virtual List<FBYearlySelection> FB_YearlySelectionGet(string Year)
        {
            throw new NotImplementedException();
        }

        public virtual List<FBEnrollment> GetEnrollmentData (String EmployeeID, String EmpSubGroup,String RequestNo, String KeyYear)
        {
            throw new NotImplementedException();
        }

        public virtual List<FBReimburse> GetReimburseQuota(String EmployeeID, String EmpSubGroup, String KeyYear)
        {
            throw new NotImplementedException();
        }

        public virtual List<FBReimburse> GetReimburseSelected(String EmployeeID, String EmpSubGroup, String RequestNo, String KeyYear)
        {
            throw new NotImplementedException();
        }

        public virtual List<FBConfiguration> GetTextDetail(String EmployeeID, String KeyCode)
        {
            throw new NotImplementedException();
        }

        public virtual List<FBConfiguration> GETFreeText(String KeyType, String KeyCode, String KeyValue)
        {
            throw new NotImplementedException();
        }

        public virtual List<FBPeriodSetting> GetFlexibleBenefitSetting(String EmployeeID, String KeyType, int KeyYear, String KeyValue)
        {
            throw new NotImplementedException();
        }

        public virtual List<YearsConfig> GetNextYears()
        {
            throw new NotImplementedException();
        }

        public virtual List<FBPeriodSetting> CheckPeriodSetting(int KeyYear)
        {
            throw new NotImplementedException();
        }

        public virtual List<FBFSAList> FB_FSAListGet()
        {
            throw new NotImplementedException();
        }

        public virtual List<FBFSASubList> FB_FSASubListGet()
        {
            throw new NotImplementedException();
        }

        public virtual void FB_ReimburseSave(FBReimburse oFBReimburse)
        {
            throw new NotImplementedException();
        }

       
        public virtual decimal FB_ReimburseCheckQuota(string EmployeeID, string Year, string RequestNo)
        {
            throw new NotImplementedException();
        }

        public virtual List<FBEnrollment> FB_EnrollmentGet(string EmployeeID, string Year)
        {
            throw new NotImplementedException();
        }

        public virtual List<ReportFBReimburseByAdmin> GetReportFBReimburseByAdmin(DateTime begin_date, DateTime end_date)
        {
            throw new NotImplementedException();
        }

        public virtual DataTable GetTrackingRegisterFlexBenReport(string sYear, string EmpID, string OrgUnitList, string CompanyCode, string LanguageCode)
        {
            throw new NotImplementedException();
        }

        #region yun-20210904

        public virtual void UploadSizeChart(string path, string sImagesChart)
        {
            throw new NotImplementedException();
        }

        public virtual void UniFormTypeSave(FBUniFormType oFBUniFormType)
        {
            throw new NotImplementedException();
        }

        public virtual void UniFormSave(FBUniForm oFBUniForm, string path)
        {
            throw new NotImplementedException();
        }

        public virtual void UniFormSizeSave(FBUniFormSize oFBUniFormSize)
        {
            throw new NotImplementedException();
        }
        public virtual List<FBUniFormType> GetUniFormType()
        {
            throw new NotImplementedException();
        }


        public virtual List<FBUniForm> GetUniForm(string path)
        {
            throw new NotImplementedException();
        }


        public virtual List<FBUniFormSize> GetUniFormSize()
        {
            throw new NotImplementedException();
        }

        #endregion

    }
}
