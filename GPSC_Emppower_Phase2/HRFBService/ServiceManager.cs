﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using ESS.HR.FB.INTERFACE;
using ESS.SHAREDATASERVICE;

namespace ESS.HR.FB
{
    public class ServiceManager
    {
        #region Constructor
        private ServiceManager()
        {

        }
        #endregion

        #region MultiCompany  Framework
        private static Dictionary<string, ServiceManager> Cache = new Dictionary<string, ServiceManager>();

        private static string ModuleID = "ESS.HR.FB";

        public string CompanyCode { get; set; }

        public static ServiceManager CreateInstance(string oCompanyCode)
        {
            ServiceManager oServiceManager = new ServiceManager()
            {
                CompanyCode = oCompanyCode
            };
            return oServiceManager;
        }
        #endregion MultiCompany  Framework

        #region " privatedata "        
        private Type GetService(string Mode, string ClassName)
        {
            Type oReturn = null;
            Assembly oAssembly;
            string assemblyName = string.Format("{0}.{1}", ModuleID, Mode.ToUpper());
            string typeName = string.Format("{0}.{1}.{2}", ModuleID, Mode.ToUpper(), ClassName);// CultureInfo.CurrentCulture.TextInfo.ToTitleCase(ClassName.ToLower()));
            oAssembly = Assembly.Load(assemblyName);    // Load assembly (dll)
            oReturn = oAssembly.GetType(typeName);      // Load class
            return oReturn;
        }

        #endregion " privatedata "


        internal IHRFBDataService GetMirrorService(string Mode)
        {
            Type oType = GetService(Mode, "HRFBDataServiceImpl");
            if (oType == null)
            {
                return null;
            }
            else
            {
                return (IHRFBDataService)Activator.CreateInstance(oType, CompanyCode);
            }
        }

        private string ESSCONNECTOR
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ESSCONNECTOR");
            }
        }

        private string ERPCONNECTOR
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ERPCONNECTOR");
            }
        }

        public IHRFBDataService ESSData
        {
            get
            {
                Type oType = GetService(ESSCONNECTOR, "HRFBDataServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IHRFBDataService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }

        public IHRFBDataService ERPData
        {
            get
            {
                Type oType = GetService(ERPCONNECTOR, "HRFBDataServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IHRFBDataService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }


    }
}