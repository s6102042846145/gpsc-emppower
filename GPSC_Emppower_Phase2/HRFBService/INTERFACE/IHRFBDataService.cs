﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.HR.FB.DATACLASS;

namespace ESS.HR.FB.INTERFACE
{
    public interface IHRFBDataService
    {
        List<FBSelectionItemRate> FB_SelectionItemRateGet();

        List<FBYearlySelection> FB_YearlySelectionGet(String Year);
        List<FBEnrollment> GetEnrollmentData(String EmployeeID, String EmpSubGroup, String RequestNo, String KeyYear);
        List<FBReimburse> GetReimburseQuota(String EmployeeID, String EmpSubGroup, String KeyYear);
        List<FBReimburse> GetReimburseSelected(String EmployeeID, String EmpSubGroup, String RequestNo, String KeyYear);
        List<FBConfiguration> GetTextDetail(String EmployeeID, String KeyCode);
        List<FBConfiguration> GETFreeText(String KeyType, String KeyCode, String KeyValue);
        List<FBPeriodSetting> GetFlexibleBenefitSetting(String EmployeeID, String KeyType, int KeyYear, String KeyValue);
        List<YearsConfig> GetNextYears();

        List<FBPeriodSetting> CheckPeriodSetting(int KeyYear);
        bool FB_CheckPeriodSetting(String Year);

        bool FB_CheckEnrollmentDuplicate(String Year, String EmployeeID,String RequestNo);

        List<FBConfiguration> FB_ConfigurationGet(String Constant);

        void FB_EnrollmentSave(FBEnrollment oFBEnrollment);
        void FB_EnrollmentSelectedSave(FBEnrollmentSelected oFBEnrollmentSelected);
        void FB_EnrollmentSelectedDelete(string RequestNo);
        List<FBFSAList> FB_FSAListGet();
        List<FBFSASubList> FB_FSASubListGet();

        void FB_ReimburseSave(FBReimburse oFBReimburse);

        decimal FB_ReimburseCheckQuota(string EmployeeID, string Year,string RequestNo);

        List<FBEnrollment>  FB_EnrollmentGet(string EmployeeID, string Year);
        List<ReportFBReimburseByAdmin> GetReportFBReimburseByAdmin(DateTime begin_date, DateTime end_date);
        DataTable GetTrackingRegisterFlexBenReport(string sYear, string EmpID, string OrgUnitList, string CompanyCode, string LanguageCode);

        #region yun-2021-09-05

        void UploadSizeChart(string path, string sImagesChart);
        List<FBUniFormType> GetUniFormType();
        List<FBUniForm> GetUniForm(string path);
        List<FBUniFormSize> GetUniFormSize();
        void UniFormTypeSave(FBUniFormType oFBUniForm);
        void UniFormSave(FBUniForm oFBUniForm, string path);
        void UniFormSizeSave(FBUniFormSize oFBUniForm);
        #endregion
    }
}
