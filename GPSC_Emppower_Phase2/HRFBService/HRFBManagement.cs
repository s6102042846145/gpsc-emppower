﻿using ESS.HR.FB.DATACLASS;
using System;
using System.Collections.Generic;
using System.Data;
using ESS.UTILITY.DATACLASS;
using ESS.UTILITY.LOG;
using ClosedXML.Excel;
using ESS.PORTALENGINE;

namespace ESS.HR.FB
{
    public class HRFBManagement
    {
        #region Constructor
        private HRFBManagement()
        {
        }

        #endregion

        #region MultiCompany  Framework
        private static Dictionary<string, HRFBManagement> Cache = new Dictionary<string, HRFBManagement>();

        private static string ModuleID = "ESS.HR.FB";
        public string CompanyCode { get; set; }

        public static HRFBManagement CreateInstance(string oCompanyCode)
        {
            HRFBManagement oHRFBManagement = new HRFBManagement()
            {
                CompanyCode = oCompanyCode
            };
            return oHRFBManagement;
        }
        #endregion MultiCompany  Framework


        public List<FBSelectionItemRate> FB_SelectionItemRateGet()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.FB_SelectionItemRateGet();
        }

        public List<FBYearlySelection> FB_YearlySelectionGet(string Year)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.FB_YearlySelectionGet(Year);
        }

        public List<FBEnrollment> GetEnrollmentData(String EmployeeID, String EmpSubGroup, String RequestNo, String KeyYear)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetEnrollmentData(EmployeeID, EmpSubGroup, RequestNo, KeyYear);
        }

        public List<FBReimburse> GetReimburseQuota(String EmployeeID, String EmpSubGroup, String KeyYear)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetReimburseQuota(EmployeeID, EmpSubGroup, KeyYear);
        }

        public List<FBReimburse> GetReimburseSelected(String EmployeeID, String EmpSubGroup, String RequestNo, String KeyYear)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetReimburseSelected(EmployeeID, EmpSubGroup, RequestNo, KeyYear);
        }

        public List<FBConfiguration> GetTextDetail(String EmployeeID, String KeyCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetTextDetail(EmployeeID, KeyCode);
        }

        public List<FBConfiguration> GETFreeText(String KeyType, String KeyCode, String KeyValue)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GETFreeText(KeyType, KeyCode, KeyValue);
        }

        public List<FBPeriodSetting> GetFlexibleBenefitSetting(String EmployeeID, String KeyType, int KeyYear, String KeyValue)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetFlexibleBenefitSetting(EmployeeID, KeyType, KeyYear, KeyValue);
        }

        public List<YearsConfig> GetNextYears()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetNextYears();
        }

        public List<FBPeriodSetting> CheckPeriodSetting(int KeyYear)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.CheckPeriodSetting(KeyYear);
        }

        public bool FB_CheckPeriodSetting(string Year)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.FB_CheckPeriodSetting(Year);
        }

        public bool FB_CheckEnrollmentDuplicate(string Year, string EmployeeID, String RequestNo)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.FB_CheckEnrollmentDuplicate(Year, EmployeeID, RequestNo);
        }

        public List<FBConfiguration> FB_ConfigurationGet(string Constant)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.FB_ConfigurationGet(Constant);
        }

        public void FB_EnrollmentSave(FBEnrollment oFBEnrollment)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.FB_EnrollmentSave(oFBEnrollment);
        }

        public void FB_EnrollmentSelectedSave(FBEnrollmentSelected oFBEnrollmentSelected)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.FB_EnrollmentSelectedSave(oFBEnrollmentSelected);
        }

        public void FB_EnrollmentSelectedDelete(string RequestNo)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.FB_EnrollmentSelectedDelete(RequestNo);
        }

        public List<FBFSAList> FB_FSAListGet()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.FB_FSAListGet();
        }

        public List<FBFSASubList> FB_FSASubListGet()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.FB_FSASubListGet();
        }

        public void FB_ReimburseSave(FBReimburse oFBReimburse)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.FB_ReimburseSave(oFBReimburse);
        }
        public decimal FB_ReimburseCheckQuota(string EmployeeID, string Year, string RequestNo)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.FB_ReimburseCheckQuota(EmployeeID, Year, RequestNo);
        }

        public List<FBEnrollment> FB_EnrollmentGet(string EmployeeID, string Year)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.FB_EnrollmentGet(EmployeeID, Year);
        }

        // รายงานการสวัสดิการทางเลือก
        public void GetSheetExcelReimburse(XLWorkbook workbook, DateTime begin_date, DateTime end_date)
        {
            var sheet = workbook.Worksheets.Add("สวัสดิการทางเลือก");

            #region Header Excel

            int header_row = 1;
            int header_column = 1;

            sheet.Row(header_row).Cell(header_column).Value = "รายงานการสวัสดิการทางเลือก";
            var range2 = sheet.Range("A1:M1");
            range2.Style.Font.FontSize = 11;
            range2.Style.Font.FontName = "Tahoma";
            var rowInRange = range2.Row(1);
            rowInRange.Merge();
            rowInRange.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            rowInRange.Style.Fill.BackgroundColor = XLColor.LightBlue;

            header_row++;

            sheet.Row(header_row).Cell(header_column).Value = "ลำดับ";
            sheet.Column(header_column).Width = 10;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "รหัสพนักงาน";
            sheet.Column(header_column).Width = 20;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "ชื่อ-สกุลพนักงาน";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "สถานที่ปฏิบัติงาน";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "จังหวัดที่ปฏิบัติงาน";
            sheet.Column(header_column).Width = 25;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "หมวดหลัก";
            sheet.Column(header_column).Width = 50;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "หมวดย่อย";
            sheet.Column(header_column).Width = 70;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "ชื่อสถานประกอบการ";
            sheet.Column(header_column).Width = 30;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "จำนวนเงินที่เบิก(บาท)";
            sheet.Column(header_column).Width = 15;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "ยอดเงินที่เบิกได้จริง(บาท)";
            sheet.Column(header_column).Width = 15;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "วันที่ขออนุมัติ";
            sheet.Column(header_column).Width = 15;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "วันที่อนุมัติ";
            sheet.Column(header_column).Width = 15;
            header_column++;

            sheet.Row(header_row).Cell(header_column).Value = "วันที่ Post ลง SAP";
            sheet.Column(header_column).Width = 15;
            header_column++;

            var rang_header2 = sheet.Range("A2:M2");
            rang_header2.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            rang_header2.Style.Font.FontSize = 11;
            rang_header2.Style.Font.FontName = "Tahoma";
            rang_header2.Style.Fill.BackgroundColor = XLColor.LightBlue;
            rang_header2.Style.Border.SetOutsideBorder(XLBorderStyleValues.Thick);
            rang_header2.Style.Border.SetInsideBorder(XLBorderStyleValues.Thick);

            #endregion

            #region Data Excel
            var list_report_reimburse = ServiceManager.CreateInstance(CompanyCode).ESSData.GetReportFBReimburseByAdmin(begin_date, end_date);
            if (list_report_reimburse.Count > 0)
            {
                int data_row = 3;
                int data_column;
                int order = 0;
                foreach (var item in list_report_reimburse)
                {
                    data_column = 1;

                    sheet.Row(data_row).Cell(data_column).Value = ++order; // ลำดับ
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.EmployeeID; // รหัสพนักงาน
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.EmployeeName; // ชื่อสกุล - พนักงาน
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.WorkLocationName; // สถานที่ปฎิบัติงาน
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.County; // จังหวัดปฎิบัติงาน
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.Fbr_Main; // หมวดหลัก
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.Fbr_SubMain; // หมวดย่อย
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.ReceiptCompany; // ชื่อสถานประกอบการ
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.Amount; // จำนวนเงินที่เบิก
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                    sheet.Row(data_row).Cell(data_column).Style.NumberFormat.Format = "#,##0.00";
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.AmountReimburse; // ยอดเงินที่เบิกได้จริง(บาท)
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                    sheet.Row(data_row).Cell(data_column).Style.NumberFormat.Format = "#,##0.00";
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.SubmitDate.HasValue ? item.SubmitDate.Value.Date.ToString("yyyy/MM/dd") : ""; // วันที่ขออนุมัติ
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.ActionDate.HasValue ? item.ActionDate.Value.ToString("yyyy/MM/dd") : ""; // วันที่อนุมัติ
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = item.PostingDate.HasValue ? item.PostingDate.Value.ToString("yyyy/MM/dd") : ""; // วันที่อนุมัติ
                    sheet.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    data_row++;
                }
            }
            #endregion
        }

        public DataTable GetTrackingRegisterFlexBenReport(string sYear, string EmpID, string OrgUnitList, string CompanyCode, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetTrackingRegisterFlexBenReport(sYear, EmpID, OrgUnitList, CompanyCode, LanguageCode);
        }

        public XLWorkbook ExportExcelTrackingRegisterFlexBenReport(DataTable oDT, string sLanguageCode)
        {
            var workbook = new XLWorkbook();
            var sheet1 = workbook.Worksheets.Add("Sheet1");

            int header_row = 1;
            int header_column = 1;

            #region Header Excel
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("TrackingRegisterFlexBenReport", sLanguageCode, "COL_EmployeeID"); // 1
            //sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("TrackingRegisterFlexBenReport", sLanguageCode, "COL_EmployeeName");  // 2
            //sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("TrackingRegisterFlexBenReport", sLanguageCode, "COL_Position");  // 3
            //sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("TrackingRegisterFlexBenReport", sLanguageCode, "COL_OrgUnitFullName");  // 4
            //sheet1.Column(header_column).Width = 50;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("TrackingRegisterFlexBenReport", sLanguageCode, "COL_OrgUnitShortName");  // 5
            //sheet1.Column(header_column).Width = 50;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("TrackingRegisterFlexBenReport", sLanguageCode, "COL_WorkLocation");  // 6
            //sheet1.Column(header_column).Width = 50;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("TrackingRegisterFlexBenReport", sLanguageCode, "COL_Province");  // 7
            //sheet1.Column(header_column).Width = 50;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("TrackingRegisterFlexBenReport", sLanguageCode, "COL_RequestNo");  // 8
            //sheet1.Column(header_column).Width = 50;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("TrackingRegisterFlexBenReport", sLanguageCode, "COL_FlexPoints");  // 9
            //sheet1.Column(header_column).Width = 50;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("TrackingRegisterFlexBenReport", sLanguageCode, "COL_DocumentStatus");  // 10
            //sheet1.Column(header_column).Width = 50;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("TrackingRegisterFlexBenReport", sLanguageCode, "COL_CreatedDate");  // 11
            //sheet1.Column(header_column).Width = 50;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("TrackingRegisterFlexBenReport", sLanguageCode, "COL_RegisterStatus");  // 12
            //sheet1.Column(header_column).Width = 50;
            header_column++;


            #endregion

            var range = sheet1.Range("A1:L1");
            range.Style.Fill.BackgroundColor = XLColor.Yellow;
            range.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            range.Style.Font.FontName = "Angsana New";
            range.Style.Font.FontSize = 14;
            range.Style.Border.SetOutsideBorder(XLBorderStyleValues.Thick).Border.SetInsideBorder(XLBorderStyleValues.Thick);

            #region Data Excel


            if (oDT.Rows.Count > 0)
            {
                int column;
                int row = 2;
                int last_column = 0;
                int last_row = 0;
                foreach (DataRow dRow in oDT.Rows)
                {
                    column = 1;

                    sheet1.Row(row).Cell(column).Value = dRow["EmployeeID"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["EmployeeName"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["Position"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["OrgUnitFullName"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["OrgUnitShortName"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["WorkLocation"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["Province"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["RequestNo"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["FlexPoints"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["DocumentStatus"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["CreatedDate"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["RegisterStatus"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;


                    if (last_column == 0)
                        last_column = column;

                    row++;
                }

                last_row = row - 1;
                var rang_detail = sheet1.Range(2, 1, last_row, last_column);
                rang_detail.Style.Font.FontName = "Angsana New";
                rang_detail.Style.Font.FontSize = 14;
            }

            #endregion

            range.SetAutoFilter();
            range.Style.Alignment.SetShrinkToFit(true);
            sheet1.Columns().AdjustToContents();
            sheet1.SheetView.Freeze(1, 1);

            return workbook;
        }

        public string GetCommonText(string Category, string Language, string Code)
        {
            CommonText oCommonText = new CommonText(CompanyCode);
            return oCommonText.LoadText(Category, Language, Code);
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetCommonText(Category, Language, Code);
        }


        #region yun-20210905

        public void UploadSizeChart(string path, string sImagesChart)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.UploadSizeChart(path, sImagesChart);
        }
        public List<FBUniFormType> GetUniFormType()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetUniFormType();
        }

        public List<FBUniForm> GetUniForm(string path)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetUniForm(path);
        }
        public List<FBUniFormSize> GetUniFormSize()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetUniFormSize();
        }

        public void UniFormTypeSave(FBUniFormType oFBUniFormType)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.UniFormTypeSave(oFBUniFormType);
        }
        public void UniFormSave(FBUniForm oFBUniForm, string path)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.UniFormSave(oFBUniForm, path);
        }
        public void UniFormSizeSave(FBUniFormSize oFBUniFormSize)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.UniFormSizeSave(oFBUniFormSize);
        }

        #endregion
    }
}
