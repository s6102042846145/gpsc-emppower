﻿using ESS.DATA.ABSTRACT;
using ESS.EMPLOYEE;
using ESS.HR.FB.DATACLASS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Newtonsoft.Json;
using ESS.HR.FB;
using ESS.SHAREDATASERVICE;
using ESS.DATA.EXCEPTION;
using System.Globalization;

namespace ESS.HR.FB.DATASERVICE
{
    class EnrollmentService : AbstractDataService
    {
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {

            var EnrollmentOption = new FBEnrollmentOption();

            var Year = CreateParam;
            if (!string.IsNullOrEmpty(CreateParam))
            {
                EnrollmentOption.Year = Year;
                EnrollmentOption.FBSelectionItemRate = HRFBManagement.CreateInstance(Requestor.CompanyCode).FB_SelectionItemRateGet();
                EnrollmentOption.FBYearlySelection = HRFBManagement.CreateInstance(Requestor.CompanyCode).FB_YearlySelectionGet(EnrollmentOption.Year);

                //Set Enrollment Opject
                for (var i = 0; i < EnrollmentOption.FBYearlySelection.Count; i++)
                {
                    EnrollmentOption.FBEnrollmentSelected[i] = new FBEnrollmentSelected();
                    EnrollmentOption.FBEnrollmentSelected[i].isMainBenefit = true;

                }

                //Get Config
                var config = HRFBManagement.CreateInstance(Requestor.CompanyCode).FB_ConfigurationGet("ProbationCutOffDate");
                DateTime ProbationCutOffDate = DateTime.ParseExact(config[0].Value + " 23:59:59", "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                //เช็ควันที่บรรจุ
                EmployeeData oEmpData = new EmployeeData(Requestor.EmployeeID, Requestor.PositionID, Requestor.CompanyCode, DateTime.Now);
                EnrollmentOption.ProbationCutOff = false;
                if (oEmpData.DateSpecific.PassProbation.Date > ProbationCutOffDate.Date)
                {
                    EnrollmentOption.ProbationCutOff = true;
                }
            }

            //Get Role Admin
            EnrollmentOption.FBEnrollmentData.RoleAdminName = ShareDataManagement.LookupCache(Requestor.CompanyCode, "ESS.HR.FB", "ADMIN");


            return EnrollmentOption;
        }

        public override void PrepareData(EmployeeData Requestor, object Data)
        {
            
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {

            FBEnrollmentOption oEnrollmentOption = JsonConvert.DeserializeObject<FBEnrollmentOption>(newData.ToString());
            
            //กดยอมรับเงื่อนไข
            if (oEnrollmentOption.AgreeStatus != "true") {
                throw new DataServiceException("FLEXIBLEBENEFIT", "AGREE_STATUS_CANTBE_NULL");
            }

            //เช็คเวลาในการลงทะเบียนกับปีที่เลือกต้องไม่เกินวันที่กำหนด
            if (Convert.ToInt32(oEnrollmentOption.FBEnrollmentData.RoleAdmin) == 0)
            {
                var CheckPeriodSetting = HRFBManagement.CreateInstance(Requestor.CompanyCode).FB_CheckPeriodSetting(oEnrollmentOption.Year);
                if (!CheckPeriodSetting)
                {
                    //ไม่อยู่ใน Period ที่กำหนด
                    throw new DataServiceException("FLEXIBLEBENEFIT", "DEADLINE_DATE");
                }
            }
            //เช็คการลงทะเบียนซ้ำ
            //var CheckDuplicate = HRFBManagement.CreateInstance(Requestor.CompanyCode).FB_CheckEnrollmentDuplicate(oEnrollmentOption.Year, Requestor.EmployeeID, RequestNo);
            //if (CheckDuplicate)
            //{
            //    throw new DataServiceException("FLEXIBLEBENEFIT", "DATA_DUPLICATE");
            //}

           
        }


        public override void CalculateInfoData(EmployeeData Requestor, object Data, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {

        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {

            FBEnrollmentOption oFBEnrollmentOption = JsonConvert.DeserializeObject<FBEnrollmentOption>(Data.ToString());
            HRFBManagement oHRBEManagement = HRFBManagement.CreateInstance(Requestor.CompanyCode);

            oFBEnrollmentOption.FBEnrollmentData.Status = State;
            oFBEnrollmentOption.FBEnrollmentData.RequestNo = RequestNo;
            oFBEnrollmentOption.FBEnrollmentData.EmployeeID = Requestor.EmployeeID;

            //set Begin End Date
            DateTime BeginDate = DateTime.ParseExact("01/01/"+ oFBEnrollmentOption.Year + " 00:00:00", "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            DateTime EndDate = DateTime.ParseExact("31/12/" + oFBEnrollmentOption.Year + " 23:59:59", "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            oFBEnrollmentOption.FBEnrollmentData.BeginDate = BeginDate;
            oFBEnrollmentOption.FBEnrollmentData.EndDate = EndDate;

            //Save Enrollment
            oHRBEManagement.FB_EnrollmentSave(oFBEnrollmentOption.FBEnrollmentData);

            // Enrollment Selected
            oHRBEManagement.FB_EnrollmentSelectedDelete(RequestNo);

            for (var i = 0; i < oFBEnrollmentOption.FBYearlySelection.Count; i++)
            {
                oFBEnrollmentOption.FBEnrollmentSelected[i].RequestNo = RequestNo;
                oHRBEManagement.FB_EnrollmentSelectedSave(oFBEnrollmentOption.FBEnrollmentSelected[i]); 
            }


            DataRow dr = Info.NewRow();
            dr["YEAR"] = oFBEnrollmentOption.Year;
            dr["FLEXIBLEPOINT"] = oFBEnrollmentOption.FBEnrollmentData.FlexPoints;
            Info.Rows.Add(dr);

        }

        public override void ValidateDataInAction(object newData, DataTable Info, EmployeeData Requestor, string RequestNo, string State, string Action, bool HaveComment, bool HaveComment2, DateTime SubmitDate)
        {
            bool chkPass = HRFBManagement.CreateInstance(Requestor.CompanyCode).FB_CheckPeriodSetting(DateTime.Now.Year.ToString());

            if (Action != "CANCEL")
            {
                if (!chkPass)
                {
                    throw new DataServiceException("HRTM_EXCEPTION", "TIMEOUT_PERIOD_SETTING");
                }
            }
        }
    }
}
