﻿using ESS.DATA.ABSTRACT;
using ESS.EMPLOYEE;
using ESS.HR.FB.DATACLASS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Newtonsoft.Json;
using ESS.HR.FB;
using ESS.SHAREDATASERVICE;
using ESS.DATA.EXCEPTION;
using System.Globalization;

namespace ESS.HR.FB.DATASERVICE
{
    class ReimburseService : AbstractDataService
    {
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            var FBReimburseOption = new FBReimburseOption();

            //Get Config 
            FBReimburseOption.FBFSAList = HRFBManagement.CreateInstance(Requestor.CompanyCode).FB_FSAListGet();
            FBReimburseOption.FBFSASubList = HRFBManagement.CreateInstance(Requestor.CompanyCode).FB_FSASubListGet();

            //Set Calenda
            FBReimburseOption.FBReimburseData.ReceiptDate = DateTime.Now;

            //Get Role Admin
            FBReimburseOption.RoleAdminName = ShareDataManagement.LookupCache(Requestor.CompanyCode, "ESS.HR.FB", "ADMIN");

            return FBReimburseOption;
        }

        public override void PrepareData(EmployeeData Requestor, object Data)
        {
           
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            FBReimburseOption oReimburseOption = JsonConvert.DeserializeObject<FBReimburseOption>(newData.ToString());

            //หมวดหลัก
            if (oReimburseOption.FBReimburseData.FSAList == 0)
            {
                throw new DataServiceException("FLEXIBLEBENEFIT", "FSALIST_CANTBE_NULL");
            }

            //หมวดย่อย
            if (oReimburseOption.FBReimburseData.FSASubList == 0)
            {
                throw new DataServiceException("FLEXIBLEBENEFIT", "FSASUBLIST_CANTBE_NULL");
            }

            //ชื่อสถานประกอบการ 
            if (string.IsNullOrEmpty(oReimburseOption.FBReimburseData.ReceiptCompany))
            {
                throw new DataServiceException("FLEXIBLEBENEFIT", "RECEIPTCOMPANY_CANTBE_NULL");
            }

            //เลขที่ใบเสร็จ  
            if (string.IsNullOrEmpty(oReimburseOption.FBReimburseData.ReceiptNo))
            {
                throw new DataServiceException("FLEXIBLEBENEFIT", "RECEIPTNO_CANTBE_NULL");
            }

            //จำนวนเงิน(บาท)  
            if (oReimburseOption.FBReimburseData.Amount == 0)
            {
                throw new DataServiceException("FLEXIBLEBENEFIT", "AMOUNT_CANTBE_NULL");
            }

            //ยอดเงินที่เบิกได้จริง
            if (oReimburseOption.FBReimburseData.AmountReimburse == 0)
            {
                throw new DataServiceException("FLEXIBLEBENEFIT", "AMOUNTREIMBURSE_CANTBE_NULL");
            }

            //พนักงานเบิกได้ไม่เกิน 180 วัน (นับจากวันที่ใบเสร็จ) 
            var datelimit = Convert.ToInt32(ShareDataManagement.LookupCache(Requestor.CompanyCode, "ESS.HR.FB", "FLEXIBLEBENEFIT_LIMIT"));
            //ใบเสร็จของปีที่แล้วสามารถเบิกได้ภายในวันที่ xx/xx ของปีถัดไปเท่านั้น
            if (!string.IsNullOrEmpty(oReimburseOption.FBReimburseData.ReceiptDate.ToString()))
            {
                if (Convert.ToInt32(oReimburseOption.RoleAdmin) == 0)
                {
                    if (oReimburseOption.FBReimburseData.ReceiptDate.AddDays(datelimit).Date <= DateTime.Now.Date)
                    {
                        throw new DataServiceException("FLEXIBLEBENEFIT", "VALIDETE_PAYDATE180");
                    }

                    //เบิกล่วงหน้าไม่ได้
                    if (oReimburseOption.FBReimburseData.ReceiptDate.Date > DateTime.Now.Date)
                    {

                        throw new DataServiceException("FLEXIBLEBENEFIT", "VALIDDATE");
                    }

                    var YearInt = Convert.ToInt32(oReimburseOption.FBReimburseData.ReceiptDate.ToString("yyyy"));
                    if (DateTime.Now.Year > YearInt)
                    {
                        var LastYearLimit = ShareDataManagement.LookupCache(Requestor.CompanyCode, "ESS.HR.FB", "FLEXIBLEBENEFIT_LASTYEAR_LIMIT");
                        DateTime Limit = DateTime.ParseExact(LastYearLimit + "/" + DateTime.Now.Year + " 23:59:59", "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                        if (DateTime.Now > Limit)
                        {
                            throw new DataServiceException("FLEXIBLEBENEFIT", "LIMITDATE");
                        }
                    }



                }
            }

            //แนบเอกสารอย่างน้อย 1 ฉบับ
            if (NoOfFileAttached < 1)
            {
                throw new DataServiceException("FLEXIBLEBENEFIT", "FILE_CANTBE_NULL");
            }

        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            FBReimburseOption oFBReimburseOption = JsonConvert.DeserializeObject<FBReimburseOption>(Data.ToString());
            HRFBManagement oHRBEManagement = HRFBManagement.CreateInstance(Requestor.CompanyCode);

            oFBReimburseOption.FBReimburseData.ReimburseStatus = State;
            oFBReimburseOption.FBReimburseData.RequestNo = RequestNo;
            oFBReimburseOption.FBReimburseData.EmployeeID = Requestor.EmployeeID;

            oHRBEManagement.FB_ReimburseSave(oFBReimburseOption.FBReimburseData);

           
        }

        public override void ValidateDataInAction(object newData, DataTable Info, EmployeeData Requestor, string RequestNo, string State, string Action, bool HaveComment, bool HaveComment2, DateTime SubmitDate)
        {
            if (Action != "CANCEL")
            {
                if (DateTime.Now.Year != SubmitDate.Year)
                {
                    throw new DataServiceException("HRTM_EXCEPTION", "TIMEOUT_PERIOD_SETTING");
                }
            }
        }

    }
}
