﻿using ESS.HR.PY.ABSTRACT;
using ESS.HR.PY.DATACLASS;
using ESS.SHAREDATASERVICE;
using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ESS.HR.PY.DB
{
    public class HRPYDataServiceImpl : AbstractHRPYDataService
    {
        #region Constructor
        public HRPYDataServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
            oSqlManage["BaseConnStr"] = BaseConnStr;
        }

        #endregion Constructor

        #region Member
        //private Dictionary<string, string> Config { get; set; }
        private Dictionary<string, string> oSqlManage = new Dictionary<string, string>();
        private static string ModuleID = "ESS.HR.PY.DB";

        public string CompanyCode { get; set; }
        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }

        #endregion Member


        public override List<PF_Graph> GetFundQuarter(String EmployeeID, String KeyType, int KeyYear, String KeyValue)
        {
            List<PF_Graph> oResult = new List<PF_Graph>();
            oSqlManage["ProcedureName"] = "sp_PY_Graph";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@TYPE"] = KeyType;
            oParamRequest["@YEAR"] = KeyYear;
            oParamRequest["@VALUES"] = KeyValue;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<PF_Graph>(oSqlManage, oParamRequest));

            return oResult;
        }


        public override List<PF_PeriodSetting> GetPeriodSetting(String EmployeeID, String KeyType, int KeyYear, String KeyValue)
        {
            List<PF_PeriodSetting> oResult = new List<PF_PeriodSetting>();
            oSqlManage["ProcedureName"] = "sp_PY_PeriodSetting";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@TYPE"] = KeyType;
            oParamRequest["@YEAR"] = KeyYear;
            oParamRequest["@VALUES"] = KeyValue;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<PF_PeriodSetting>(oSqlManage, oParamRequest));

            return oResult;
        }

        public override List<PF_ProvidentFundDetail> GetTextProvidentFundDetail(String KeyType, String KeyCode, String KeyValue)
        {
            List<PF_ProvidentFundDetail> oResult = new List<PF_ProvidentFundDetail>();
            oSqlManage["ProcedureName"] = "sp_PY_TextProvidentFundDetail";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@TYPE"] = KeyType;
            oParamRequest["@Code"] = KeyCode;
            oParamRequest["@Value"] = KeyValue;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<PF_ProvidentFundDetail>(oSqlManage, oParamRequest));

            return oResult;
        }




        public override List<PF_Selection> GetPF_Selection()
        {
            List<PF_Selection> oResult = new List<PF_Selection>();
            oSqlManage["ProcedureName"] = "sp_PY_GetPF_Selection";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oResult.AddRange(DatabaseHelper.ExecuteQuery<PF_Selection>(oSqlManage, oParamRequest));
            return oResult;
        }



        public override List<PF_Percentage> GetPF_Percentage()
        {
            List<PF_Percentage> oResult = new List<PF_Percentage>();
            oSqlManage["ProcedureName"] = "sp_PY_GetPF_Percentage";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oResult.AddRange(DatabaseHelper.ExecuteQuery<PF_Percentage>(oSqlManage, oParamRequest));
            return oResult;
        }

        public override void SaveProvidentFundLog(PF_ProvidentFundLog data)
        {
            oSqlManage["ProcedureName"] = "sp_PY_ProvidentFundLogSave";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@EmployeeID"] = data.EmployeeID;
            oParamRequest["@RequestNo"] = data.RequestNo;
            oParamRequest["@OldFundID"] = data.OldFundID;
            oParamRequest["@FundID"] = data.FundID;
            oParamRequest["@OldEmployeeSaving"] = data.OldEmployeeSaving;
            oParamRequest["@EmployeeSaving"] = data.EmployeeSaving;
            oParamRequest["@CompanySaving"] = data.CompanySaving;
            oParamRequest["@CreateBy"] = data.CreateBy;
            oParamRequest["@Status"] = data.Status;
            oParamRequest["@EffectiveDate"] = data.EffectiveDate;
            oParamRequest["@Type"] = data.Type;
            DatabaseHelper.ExecuteNoneQuery(oSqlManage, oParamRequest);
        }

        public override List<PF_PeriodSetting> GetEffectiveDate()
        {
            List<PF_PeriodSetting> oResult = new List<PF_PeriodSetting>();
            oSqlManage["ProcedureName"] = "sp_PY_GetEffectiveDate";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oResult.AddRange(DatabaseHelper.ExecuteQuery<PF_PeriodSetting>(oSqlManage, oParamRequest));
            return oResult;
        }

        public override List<TimeAwareLink> GetAllTimeAwareLink()
        {
            List<TimeAwareLink> oResult = new List<TimeAwareLink>();
            oSqlManage["ProcedureName"] = "sp_TimeAwareLinkGetAll";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oResult.AddRange(DatabaseHelper.ExecuteQuery<TimeAwareLink>(oSqlManage, oParamRequest));
            return oResult;
        }

        public override void UpdateTimeAwareLink(int LinkID, String BeginDate, String EndDate, String EmpID)
        {
            oSqlManage["ProcedureName"] = "sp_TimeAwareLinkUpdate";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_LinkID"] = LinkID;
            oParamRequest["@p_BeginDate"] = BeginDate;
            oParamRequest["@p_EndDate"] = EndDate;
            oParamRequest["@p_EmployeeID"] = EmpID;
            DatabaseHelper.ExecuteNoneQuery(oSqlManage, oParamRequest);
        }

        #region " GetLetter "
        public override Letter GetLetter(string strRequestNo)
        {
            DataTable dt = new DataTable();
            Letter oReturn = new Letter();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_LE_LetterGetByRequestNo", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            oCommand.Parameters.Add(new SqlParameter("@p_RequestNo", strRequestNo));

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.Fill(dt);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in dt.Rows)
            {
                oReturn.ParseToObject(dr);
            }

            return oReturn;
        }
        public override List<Letter> GetLetter(string strEmployeeID, DateTime dtBeginDate, DateTime dtEndDate)
        {
            DataTable dt = new DataTable();
            List<Letter> oReturn = new List<Letter>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_LE_LetterGet", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            oCommand.Parameters.Add(new SqlParameter("@p_EmployeeID", strEmployeeID));
            oCommand.Parameters.Add(new SqlParameter("@p_BeginDate", dtBeginDate));
            oCommand.Parameters.Add(new SqlParameter("@p_EndDate", dtEndDate));

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.Fill(dt);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            Letter oLetter;

            foreach (DataRow dr in dt.Rows)
            {
                oLetter = new Letter();
                oLetter.ParseToObject(dr);
                oReturn.Add(oLetter);
            }

            return oReturn;
        }
        #endregion

        #region " GetLetterByAdmin "
        public override List<Letter> GetLetterByAdmin(DateTime dtBeginDate, DateTime dtEndDate)
        {
            DataTable dt = new DataTable();
            List<Letter> oReturn = new List<Letter>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_LE_LetterAdminGet", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            oCommand.Parameters.Add(new SqlParameter("@p_BeginDate", dtBeginDate));
            oCommand.Parameters.Add(new SqlParameter("@p_EndDate", dtEndDate));

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.Fill(dt);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            Letter oLetter;

            foreach (DataRow dr in dt.Rows)
            {
                oLetter = new Letter();
                oLetter.ParseToObject(dr);
                oReturn.Add(oLetter);
            }

            return oReturn;
        }
        #endregion

        #region " InsertLetterLog "
        public override void InsertLetterLog(string strEmployeeID, string strRequestNo)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_LE_LetterLogInsert", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            oCommand.Parameters.Add(new SqlParameter("@p_EmployeeID", strEmployeeID));
            oCommand.Parameters.Add(new SqlParameter("@p_RequestNo", strRequestNo));

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();

            try
            {
                oCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
            }
        }
        #endregion

        #region " SaveLetter "
        public override void SaveLetter(Letter oLetter)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_LE_LetterSave", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            oCommand.Parameters.Add(new SqlParameter("@p_EmployeeID", oLetter.EmployeeID));
            oCommand.Parameters.Add(new SqlParameter("@p_RequestNo", oLetter.RequestNo));
            oCommand.Parameters.Add(new SqlParameter("@p_LetterTypeID", oLetter.LetterTypeID));
            oCommand.Parameters.Add(new SqlParameter("@p_Reason", oLetter.Reason));
            oCommand.Parameters.Add(new SqlParameter("@p_Status", oLetter.Status));
            if (oLetter.EmbassyID >= 0)
            {
                oCommand.Parameters.Add(new SqlParameter("@p_VacationBegin", oLetter.VacationBegin));
                oCommand.Parameters.Add(new SqlParameter("@p_VacationEnd", oLetter.VacationEnd));
                oCommand.Parameters.Add(new SqlParameter("@p_EmbassyID", oLetter.EmbassyID));
            }
            else
            {
                oCommand.Parameters.Add(new SqlParameter("@p_VacationBegin", DBNull.Value));
                oCommand.Parameters.Add(new SqlParameter("@p_VacationEnd", DBNull.Value));
                oCommand.Parameters.Add(new SqlParameter("@p_EmbassyID", DBNull.Value));
            }

            if (oLetter.EmbassyID2 >= 0)
            {
                oCommand.Parameters.Add(new SqlParameter("@p_VacationBegin2", oLetter.VacationBegin2));
                oCommand.Parameters.Add(new SqlParameter("@p_VacationEnd2", oLetter.VacationEnd2));
                oCommand.Parameters.Add(new SqlParameter("@p_EmbassyID2", oLetter.EmbassyID2));
            }
            else
            {
                oCommand.Parameters.Add(new SqlParameter("@p_VacationBegin2", DBNull.Value));
                oCommand.Parameters.Add(new SqlParameter("@p_VacationEnd2", DBNull.Value));
                oCommand.Parameters.Add(new SqlParameter("@p_EmbassyID2", DBNull.Value));
            }

            if (oLetter.EmbassyID3 >= 0)
            {
                oCommand.Parameters.Add(new SqlParameter("@p_VacationBegin3", oLetter.VacationBegin3));
                oCommand.Parameters.Add(new SqlParameter("@p_VacationEnd3", oLetter.VacationEnd3));
                oCommand.Parameters.Add(new SqlParameter("@p_EmbassyID3", oLetter.EmbassyID3));
            }
            else
            {
                oCommand.Parameters.Add(new SqlParameter("@p_VacationBegin3", DBNull.Value));
                oCommand.Parameters.Add(new SqlParameter("@p_VacationEnd3", DBNull.Value));
                oCommand.Parameters.Add(new SqlParameter("@p_EmbassyID3", DBNull.Value));
            }

            if (!string.IsNullOrEmpty(oLetter.SignID))
            {
                oCommand.Parameters.Add(new SqlParameter("@p_SignID", oLetter.SignID));
            }

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();

            try
            {
                oCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
            }
        }
        #endregion

        #region " UpdateLetterNo "
        public override void UpdateLetterNo(string strEmployeeID, string strRequestNo, DateTime dtBeginDate, DateTime dtEndDate, bool bUpdateLog)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_LE_LetterUpdateLetterNo", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            oCommand.Parameters.Add(new SqlParameter("@p_EmployeeID", strEmployeeID));
            oCommand.Parameters.Add(new SqlParameter("@p_RequestNo", strRequestNo));
            oCommand.Parameters.Add(new SqlParameter("@p_BeginDate", dtBeginDate));
            oCommand.Parameters.Add(new SqlParameter("@p_EndDate", dtEndDate));
            oCommand.Parameters.Add(new SqlParameter("@p_IsUpdateLog", bUpdateLog));

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();

            try
            {
                oCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
            }
        }
        #endregion


        #region " MarkUpdate "
        public override void MarkUpdate(string EmployeeID, string DataCategory, string RequestNo, bool isMarkIn)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select * from MarkData where EmployeeID = @EmpID and DataCategory = @DataCategory ", oConnection, tx);
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@DataCategory", SqlDbType.VarChar);
            oParam.Value = DataCategory;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("MARKDATA");
            try
            {
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                if (oTable.Rows.Count == 0 && isMarkIn)
                {
                    DataRow oRow = oTable.NewRow();
                    oRow["EmployeeID"] = EmployeeID;
                    oRow["DataCategory"] = DataCategory;
                    oRow["RequestNo"] = RequestNo;
                    oTable.Rows.Add(oRow);
                }
                else if (oTable.Rows.Count > 0 && !isMarkIn)
                {
                    foreach (DataRow dr in oTable.Select())
                    {
                        dr.Delete();
                    }
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }

        #endregion " MarkUpdate "

        #region " CheckMark "

        public override bool CheckMark(string EmployeeID, string DataCategory)
        {
            bool oReturn = false;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select Count(*) from MarkData where EmployeeID =" +
                                                 " @EmpID and DataCategory = @DataCategory", oConnection);
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@DataCategory", SqlDbType.VarChar);
            oParam.Value = DataCategory;
            oCommand.Parameters.Add(oParam);
            try
            {
                oConnection.Open();
                oReturn = ((int)oCommand.ExecuteScalar()) > 0;

            }
            catch (Exception ex)
            {
                throw new Exception("CheckMark error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
            }
            return oReturn;
        }

        #endregion " CheckMark "

        #region Check MarkData

        public override bool CheckMark(string EmployeeID, string DataCategory, string ReqNo)
        {
            bool oReturn = false;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select Count(*) from MarkData where EmployeeID =" +
                                                 " @EmpID and DataCategory = @DataCategory and RequestNo <> @ReqNo", oConnection);
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@DataCategory", SqlDbType.VarChar);
            oParam.Value = DataCategory;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@ReqNo", SqlDbType.VarChar);
            oParam.Value = ReqNo;
            oCommand.Parameters.Add(oParam);
            try
            {
                oConnection.Open();
                oReturn = ((int)oCommand.ExecuteScalar()) > 0;
            }
            catch (Exception ex)
            {
                throw new Exception("CheckMark error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
            }
            return oReturn;
        }

        #endregion Check MarkData

        public override List<PFChangePlanReport> GetPFChangePlanList(DateTime effectiveDate, string companyCode)
        {
            List<PFChangePlanReport> oReturn = new List<PFChangePlanReport>();

            #region ของเดิม
            //            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            //            SqlCommand oCommand = new SqlCommand(@"
            //SELECT
            //       pflg.EffectiveDate EffectiveDate	-- วันที่มีผล
            //       , ift01.EmployeeID EmployeeID -- รหัสสมาชิก
            //       , ift01.[name] EmployeeName -- ชื่อ-นามสกุล สมาชิก
            //       , IsNull(td1.TextDescription,'') ProvidentFund_OldOption -- นโยบายการลงทุนเดิม
            //       , IsNull(td2.TextDescription,'') ProvidentFund_NewOption -- นโยบายการลงทุนใหม่
            //       , max(pflg.RequestNo) RequestNo -- เลขที่เอกสารที่แจ้งในระบบ
            //       , max(rd.SubmitDate) SubmitDate -- วันที่พนักงานแจ้งในระบบ
            //       , max(rd1.ActionDate) ActionDate -- วันที่อนุมัติเอกสาร
            //FROM[ESS_GPSC].[dbo].[PF_ProvidentFundLog] pflg
            //       left join[ESS_GPSC].[dbo].[RequestDocument] rd on rd.RequestNo = pflg.RequestNo and rd.FlowItemCode = 'COMPLETED'
            //       left join[ESS_GPSC].[dbo].[RequestFlow] rd1 on rd1.RequestID = rd.RequestID and rd1.ItemID = rd.ItemID
            //       left join[ESS_GPSC].[dbo].[INFOTYPE0001] ift01 on ift01.EmployeeID = pflg.EmployeeID and pflg.UpdateDate between ift01.BeginDate and ift01.EndDate
            //       left join[ESS_GPSC].[dbo].[PF_Selection] sl on sl.FundID = pflg.FundID and pflg.UpdateDate between sl.BeginDate and sl.EndDate
            //       left join[ESS_GPSC].[dbo].[PF_ProvidentFundLog] pfpv on pfpv.EmployeeID = pflg.EmployeeID  and pfpv.[Status] = 'COMPLETED' and pfpv.EffectiveDate < pflg.EffectiveDate and pflg.FundID <> pfpv.FundID
            //       left join[ESS_GPSC].[dbo].[PF_Selection] slpv on slpv.FundID = pfpv.FundID and pfpv.UpdateDate between slpv.BeginDate and slpv.EndDate
            //       left join[ESS_GPSC].[dbo].[TextDescription] td1 on td1.CategoryCode = 'PROVIDENTFUND' and td1.LanguageCode = 'TH' and td1.TextCode = 'FUND_' + slpv.FundID
            //       left join[ESS_GPSC].[dbo].[TextDescription] td2 on td2.CategoryCode = 'PROVIDENTFUND' and td2.LanguageCode = 'TH' and td2.TextCode = 'FUND_' + sl.FundID
            //WHERE 1 = 1
            //       and pflg.[Status] = 'COMPLETED'
            //       and pflg.EffectiveDate = @EffectiveDate
            //       and pflg.[Type] = 1
            //Group By pflg.EffectiveDate, ift01.EmployeeID, ift01.[name], td1.TextDescription, td2.TextDescription
            //Order By pflg.EffectiveDate, ift01.EmployeeID, ift01.[name], td1.TextDescription, td2.TextDescription
            //        ", oConnection);

            //            SqlParameter oParam;
            //            oParam = new SqlParameter("@EffectiveDate", SqlDbType.DateTime);
            //            oParam.Value = effectiveDate;
            //            oCommand.Parameters.Add(oParam);

            //            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            //            DataTable oTable = new DataTable("PFChangePlan");
            //            oConnection.Open();
            //            oAdapter.Fill(oTable);
            //            oConnection.Close();

            //            foreach (DataRow dr in oTable.Rows)
            //            {
            //                PFChangePlanReport item = new PFChangePlanReport();

            //                item.ParseToObject(dr);
            //                oReturn.Add(item);
            //            }

            //            oConnection.Dispose();
            //            oAdapter.Dispose();
            //            oCommand.Dispose();
            //            oTable.Dispose();
            #endregion

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_getPFChangeInvestmentPolicy", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            oCommand.Parameters.Add(new SqlParameter("@p_CompanyCode", companyCode));
            oCommand.Parameters.Add(new SqlParameter("@p_EffectiveDate", effectiveDate));

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("PFChangePlan");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();

            foreach (DataRow dr in oTable.Rows)
            {
                PFChangePlanReport item = new PFChangePlanReport();

                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oConnection.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();
            oTable.Dispose();

            return oReturn;
        }

        public override List<PFChangeRateAmountPlanReport> GetPFChangeRateAmountPlanList(DateTime effectiveDate, string companyCode)
        {
            List<PFChangeRateAmountPlanReport> oReturn = new List<PFChangeRateAmountPlanReport>();

            #region ของเดิม
            //            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            //            SqlCommand oCommand = new SqlCommand(@"
            //SELECT
            //       pflg.EffectiveDate EffectiveDate -- วันที่มีผล
            //       , ift01.EmployeeID EmployeeID -- รหัสสมาชิก
            //       , ift01.[name] EmployeeName -- ชื่อ-นามสกุล สมาชิก
            //       , pfpv.EmployeeSaving EmployeeSavingRateOld -- อัตราเงินสะสมเดิม (%)
            //       , pflg.EmployeeSaving EmployeeSavingRateNew -- อัตราเงินสะสมใหม่ (%)
            //       , max(pflg.RequestNo) RequestNo -- เลขที่เอกสารที่แจ้งในระบบ
            //       , max(rd.SubmitDate) SubmitDate -- วันที่พนักงานแจ้งในระบบ
            //       , max(rd1.ActionDate) ActionDate -- วันที่อนุมัติเอกสาร
            //FROM [ESS_GPSC].[dbo].[PF_ProvidentFundLog] pflg
            //       left join [ESS_GPSC].[dbo].[RequestDocument] rd on rd.RequestNo = pflg.RequestNo and rd.FlowItemCode = 'COMPLETED'
            //       left join [ESS_GPSC].[dbo].[RequestFlow] rd1 on rd1.RequestID = rd.RequestID and rd1.ItemID = rd.ItemID
            //       left join [ESS_GPSC].[dbo].[INFOTYPE0001] ift01 on ift01.EmployeeID=pflg.EmployeeID and pflg.UpdateDate between ift01.BeginDate and ift01.EndDate
            //       left join [ESS_GPSC].[dbo].[PF_Selection] sl on sl.FundID=pflg.FundID and pflg.UpdateDate between sl.BeginDate and sl.EndDate
            //       left join [ESS_GPSC].[dbo].[PF_ProvidentFundLog] pfpv on pfpv.EmployeeID = pflg.EmployeeID  and pfpv.[Status] = 'COMPLETED' and pfpv.EffectiveDate < pflg.EffectiveDate and pflg.FundID<>pfpv.FundID
            //       left join [ESS_GPSC].[dbo].[PF_Selection] slpv on slpv.FundID=pfpv.FundID and pfpv.UpdateDate between slpv.BeginDate and slpv.EndDate
            //       left join [ESS_GPSC].[dbo].[TextDescription] td1 on td1.CategoryCode = 'PROVIDENTFUND' and td1.LanguageCode = 'TH' and td1.TextCode = 'FUND_'+slpv.FundID
            //       left join [ESS_GPSC].[dbo].[TextDescription] td2 on td2.CategoryCode = 'PROVIDENTFUND' and td2.LanguageCode = 'TH' and td2.TextCode = 'FUND_'+sl.FundID
            //WHERE 1=1
            //       and pflg.[Status] = 'COMPLETED'
            //       and pflg.EffectiveDate = @EffectiveDate
            //       and pflg.[Type] = 2
            //Group By pflg.EffectiveDate, ift01.EmployeeID, ift01.[name] , pflg.EmployeeSaving, pfpv.EmployeeSaving
            //Order By pflg.EffectiveDate, ift01.EmployeeID, ift01.[name] , pflg.EmployeeSaving, pfpv.EmployeeSaving
            //        ", oConnection);

            //            SqlParameter oParam;
            //            oParam = new SqlParameter("@EffectiveDate", SqlDbType.DateTime);
            //            oParam.Value = effectiveDate;
            //            oCommand.Parameters.Add(oParam);

            //            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            //            DataTable oTable = new DataTable("PFChangeRateAmountPlan");
            //            oConnection.Open();
            //            oAdapter.Fill(oTable);
            //            oConnection.Close();

            //            foreach (DataRow dr in oTable.Rows)
            //            {
            //                PFChangeRateAmountPlanReport item = new PFChangeRateAmountPlanReport();

            //                item.ParseToObject(dr);
            //                oReturn.Add(item);
            //            }

            //            oConnection.Dispose();
            //            oAdapter.Dispose();
            //            oCommand.Dispose();
            //            oTable.Dispose();
            #endregion

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_getPFChangeSavingRate", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            oCommand.Parameters.Add(new SqlParameter("@p_CompanyCode", companyCode));
            oCommand.Parameters.Add(new SqlParameter("@p_EffectiveDate", effectiveDate));

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("PFChangeRateAmountPlan");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();

            foreach (DataRow dr in oTable.Rows)
            {
                PFChangeRateAmountPlanReport item = new PFChangeRateAmountPlanReport();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oConnection.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();
            oTable.Dispose();

            return oReturn;
        }

        #region Import Providentfund (นำเข้าข้อมูลกองทุนสำรองเลี้ยงชีพ)
        public override ResponeKeyProvident SaveImportProvidentFund(SaveProvidentFundKtam model)
        {
            ResponeKeyProvident result = new ResponeKeyProvident();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;

            oCommand = new SqlCommand("sp_PY_SaveCheckDuplicateHeaderImportProvidentFund", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            // 1. หาว่า Header มีข้อมูล Fund Id แล้วหรือยัง
            oCommand.Parameters.Add(new SqlParameter("@p_FundID", model.FundId));
            oCommand.Parameters.Add(new SqlParameter("@p_Year", model.Year));
            oCommand.Parameters.Add(new SqlParameter("@p_Month", model.Month));
            //oCommand.Parameters.Add(new SqlParameter("@p_CompanyId", model.CompanyId));
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("ProvidentHeader");
            oConnection.Open();
            oAdapter.Fill(oTable);

            oAdapter.Dispose();

            SqlTransaction oTransaction = oConnection.BeginTransaction();
            if (oTable.Rows.Count > 0)
            {
                TbHeaderProvidentFundImport item = new TbHeaderProvidentFundImport();
                foreach (DataRow dr in oTable.Rows)
                {
                    item.ParseToObject(dr);
                }

                // Update Header Providentfund
                oCommand = new SqlCommand("sp_PY_SaveUpdateHeaderImportProvidentFund", oConnection, oTransaction);
                oCommand.CommandType = CommandType.StoredProcedure;
                oCommand.Parameters.Clear();

                oCommand.Parameters.Add("@p_PF_HeaderID", SqlDbType.Int).Value = item.PF_HeaderID;
                oCommand.Parameters.Add("@p_NAV_Date", SqlDbType.DateTime).Value = model.NavDate.Date;
                oCommand.Parameters.Add("@p_NAV_Value", SqlDbType.Decimal).Value = model.NavValue;
                oCommand.Parameters.Add("@p_PF_Year", SqlDbType.Int).Value = model.Year;
                oCommand.Parameters.Add("@p_PF_Month", SqlDbType.Int).Value = model.Month;
                oCommand.Parameters.Add("@p_ImportedFileName", SqlDbType.VarChar).Value = model.File;
                oCommand.Parameters.Add("@p_UpdateDate", SqlDbType.DateTime).Value = DateTime.Now;
                oCommand.Parameters.Add("@p_UpdateBy", SqlDbType.VarChar).Value = model.EmployeeId;
                oCommand.ExecuteNonQuery();

                // Delete Detail
                string cmd = "DELETE FROM PF_ProvidentFundImport_Detail WHERE PF_HeaderID = @p_PF_HeaderID";
                oCommand = new SqlCommand(cmd, oConnection, oTransaction);
                oCommand.Parameters.Add("@p_PF_HeaderID", SqlDbType.Int).Value = item.PF_HeaderID;
                oCommand.ExecuteNonQuery();

                // Insert New Detail Providentfund
                oCommand = new SqlCommand("sp_PY_SaveImportProvidentFund", oConnection, oTransaction);
                oCommand.CommandType = CommandType.StoredProcedure;
                SaveImportProvidentFundDetail(item.PF_HeaderID, model, oCommand);

                oTransaction.Commit();

                result.PF_HeaderId = item.PF_HeaderID;
                result.CompanyId = item.CompanyID;
                result.FundId = item.FundID;
                result.Month = model.Month;
                result.Year = model.Year;
            }
            else
            {
                try
                {
                    // 2. Insert Header Provident fund
                    oCommand = new SqlCommand("sp_PY_SaveHeaderImportProvidentFund", oConnection, oTransaction);
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.Parameters.Clear();

                    //รองรับ PK ที่ Return ออกมา
                    oCommand.Parameters.Add("@p_PFHeaderID", SqlDbType.Int).Direction = ParameterDirection.Output;
                    oCommand.Parameters.Add("@p_FundID", SqlDbType.VarChar).Value = model.FundId;
                    oCommand.Parameters.Add("@p_CompanyId", SqlDbType.VarChar).Value = model.CompanyId;
                    oCommand.Parameters.Add("@p_NAV_Date", SqlDbType.DateTime).Value = model.NavDate.Date;
                    oCommand.Parameters.Add("@p_NAV_Value", SqlDbType.Decimal).Value = model.NavValue;
                    oCommand.Parameters.Add("@p_PF_Year", SqlDbType.Int).Value = model.Year;
                    oCommand.Parameters.Add("@p_PF_Month", SqlDbType.Int).Value = model.Month;
                    oCommand.Parameters.Add("@p_ImportedDate", SqlDbType.DateTime).Value = DateTime.Now;
                    oCommand.Parameters.Add("@p_ImportedBy", SqlDbType.VarChar).Value = model.EmployeeId;
                    oCommand.Parameters.Add("@p_ImportedFileName", SqlDbType.VarChar).Value = model.File;
                    oCommand.ExecuteNonQuery();

                    string pf_id = oCommand.Parameters["@p_PFHeaderID"].Value.ToString();
                    int pf_int_id = int.Parse(pf_id);

                    // Insert New Detail Providentfund
                    oCommand = new SqlCommand("sp_PY_SaveImportProvidentFund", oConnection, oTransaction);
                    oCommand.CommandType = CommandType.StoredProcedure;
                    SaveImportProvidentFundDetail(pf_int_id, model, oCommand);

                    oTransaction.Commit();

                    result.PF_HeaderId = pf_int_id;
                    result.CompanyId = model.CompanyId;
                    result.FundId = model.FundId;
                    result.Month = model.Month;
                    result.Year = model.Year;
                }
                catch
                {
                    oTransaction.Rollback();

                    oCommand.Dispose();
                    oTable.Dispose();
                    if (oConnection.State == ConnectionState.Open)
                        oConnection.Close();
                    oConnection.Dispose();
                    oTransaction.Dispose();
                    throw;
                }
                finally
                {
                    oCommand.Dispose();
                    oTable.Dispose();
                    if (oConnection.State == ConnectionState.Open)
                        oConnection.Close();
                    oConnection.Dispose();
                    oTransaction.Dispose();
                }
            }

            var split_name = model.File.Split(',');
            if (split_name.Length > 0)
            {
                result.FileName = split_name[0];
            }

            return result;
        }

        public void SaveImportProvidentFundDetail(int fk, SaveProvidentFundKtam model, SqlCommand oCommand)
        {
            if (model.ListProvidentfundEmployee.Count > 0)
            {
                var date_now = DateTime.Now;
                foreach (var item in model.ListProvidentfundEmployee)
                {
                    oCommand.Parameters.Clear();
                    oCommand.Parameters.Add("@p_PF_HeaderID", SqlDbType.Int).Value = fk;
                    oCommand.Parameters.Add("@EmployeeID", SqlDbType.VarChar).Value = item.EmployeeId;  // 1
                    oCommand.Parameters.Add("@TitleName", SqlDbType.VarChar).Value = item.TitleName;  // 2
                    oCommand.Parameters.Add("@FirstName", SqlDbType.VarChar).Value = item.FirstName;  // 3
                    oCommand.Parameters.Add("@LastName", SqlDbType.VarChar).Value = item.LastName;  // 4
                    oCommand.Parameters.Add("@DeptCode", SqlDbType.VarChar).Value = item.DeptCode;  // 5

                    oCommand.Parameters.Add("@Old_Employee_Amount", SqlDbType.Decimal).Value = item.Old_employee_amount;  // 6
                    oCommand.Parameters.Add("@Old_Employee_Benefit_Amount", SqlDbType.Decimal).Value = item.Old_employee_benefit_amount;  // 7
                    oCommand.Parameters.Add("@Old_Employee_Unit", SqlDbType.Decimal).Value = item.Old_employee_unit;  // 8

                    oCommand.Parameters.Add("@Old_Company_Amount", SqlDbType.Decimal).Value = item.Old_company_amount;  // 9
                    oCommand.Parameters.Add("@Old_Company_Benefit_Amount", SqlDbType.Decimal).Value = item.Old_company_benefit_amount;  // 10
                    oCommand.Parameters.Add("@Old_Company_Unit", SqlDbType.Decimal).Value = item.Old_company_unit;  // 11

                    oCommand.Parameters.Add("@Old_Forward_Amount_Employee", SqlDbType.Decimal).Value = item.Old_forward_amount_employee;  // 12
                    oCommand.Parameters.Add("@Old_forward_benefit_amount_employee", SqlDbType.Decimal).Value = item.Old_forward_benefit_amount_employee;  // 13
                    oCommand.Parameters.Add("@Old_forward_unit_employee", SqlDbType.Decimal).Value = item.Old_forward_unit_employee;  // 14

                    oCommand.Parameters.Add("@Old_forward_amount_company", SqlDbType.Decimal).Value = item.Old_forward_amount_company;  // 15
                    oCommand.Parameters.Add("@Old_forward_benefit_amount_company", SqlDbType.Decimal).Value = item.Old_forward_benefit_amount_company;  // 16
                    oCommand.Parameters.Add("@Old_forward_unit_company", SqlDbType.Decimal).Value = item.Old_forward_unit_company;  // 17

                    oCommand.Parameters.Add("@Employee_amount_transfer_in", SqlDbType.Decimal).Value = item.Employee_amount_transfer_in;  // 18
                    oCommand.Parameters.Add("@Employee_benefit_amount_transfer_in", SqlDbType.Decimal).Value = item.Employee_benefit_amount_transfer_in;  // 19
                    oCommand.Parameters.Add("@Employee_unit_transfer_in", SqlDbType.Decimal).Value = item.Employee_unit_transfer_in;  // 20

                    oCommand.Parameters.Add("@Company_amount_transfer_in", SqlDbType.Decimal).Value = item.Company_amount_transfer_in;  // 21
                    oCommand.Parameters.Add("@Company_benefit_amount_transfer_in", SqlDbType.Decimal).Value = item.Company_benefit_amount_transfer_in;  // 22
                    oCommand.Parameters.Add("@Company_unit_transfer_in", SqlDbType.Decimal).Value = item.Company_unit_transfer_in;  // 23

                    oCommand.Parameters.Add("@Forward_amount_employee_transfer_in", SqlDbType.Decimal).Value = item.Forward_amount_employee_transfer_in;  // 24
                    oCommand.Parameters.Add("@Forward_benefit_amount_employee_transfer_in", SqlDbType.Decimal).Value = item.Forward_benefit_amount_employee_transfer_in;  // 25
                    oCommand.Parameters.Add("@Forward_unit_employee_transfer_in", SqlDbType.Decimal).Value = item.Forward_unit_employee_transfer_in;  // 26

                    oCommand.Parameters.Add("@Forward_amount_company_transfer_in", SqlDbType.Decimal).Value = item.Forward_amount_company_transfer_in;  // 27
                    oCommand.Parameters.Add("@Forward_benefit_amount_company_transfer_in", SqlDbType.Decimal).Value = item.Forward_benefit_amount_company_transfer_in;  // 28
                    oCommand.Parameters.Add("@Forward_unit_company_transfer_in", SqlDbType.Decimal).Value = item.Forward_unit_company_transfer_in;  // 29

                    oCommand.Parameters.Add("@Year_employee_amount", SqlDbType.Decimal).Value = item.Year_employee_amount;  // 30
                    oCommand.Parameters.Add("@Year_employee_benefit_amount", SqlDbType.Decimal).Value = item.Year_employee_benefit_amount;  // 31
                    oCommand.Parameters.Add("@Year_employee_unit", SqlDbType.Decimal).Value = item.Year_employee_unit;  // 32

                    oCommand.Parameters.Add("@Year_company_amount", SqlDbType.Decimal).Value = item.Year_company_amount;  // 33
                    oCommand.Parameters.Add("@Year_company_benefit_amount", SqlDbType.Decimal).Value = item.Year_company_benefit_amount;  // 34
                    oCommand.Parameters.Add("@Year_company_unit", SqlDbType.Decimal).Value = item.Year_company_unit;  // 35

                    oCommand.Parameters.Add("@Year_forward_amount_employee", SqlDbType.Decimal).Value = item.Year_forward_amount_employee;  // 36
                    oCommand.Parameters.Add("@Year_forward_benefit_amount_employee", SqlDbType.Decimal).Value = item.Year_forward_benefit_amount_employee;  // 37
                    oCommand.Parameters.Add("@Year_forward_unit_employee", SqlDbType.Decimal).Value = item.Year_forward_unit_employee;  // 38

                    oCommand.Parameters.Add("@Year_forward_amount_company", SqlDbType.Decimal).Value = item.Year_forward_amount_company;  // 39
                    oCommand.Parameters.Add("@Year_forward_benefit_amount_company", SqlDbType.Decimal).Value = item.Year_forward_benefit_amount_company;  // 40
                    oCommand.Parameters.Add("@Year_forward_unit_company", SqlDbType.Decimal).Value = item.Year_forward_unit_company;  // 41

                    oCommand.Parameters.Add("@Employee_amount_transfer_out", SqlDbType.Decimal).Value = item.Employee_amount_transfer_out;  // 42
                    oCommand.Parameters.Add("@Employee_benfit_amount_transfer_out", SqlDbType.Decimal).Value = item.Employee_benfit_amount_transfer_out;  // 43
                    oCommand.Parameters.Add("@Employee_unit_transfer_out", SqlDbType.Decimal).Value = item.Employee_unit_transfer_out;  // 44

                    oCommand.Parameters.Add("@Company_amount_transfer_out", SqlDbType.Decimal).Value = item.Company_amount_transfer_out;  // 45
                    oCommand.Parameters.Add("@Company_benefit_amount_transfer_out", SqlDbType.Decimal).Value = item.Company_benefit_amount_transfer_out;  // 46
                    oCommand.Parameters.Add("@Company_unit_transfer_out", SqlDbType.Decimal).Value = item.Company_unit_transfer_out;  // 47

                    oCommand.Parameters.Add("@Forward_amount_employee_transfer_out", SqlDbType.Decimal).Value = item.Forward_amount_employee_transfer_out;  // 48
                    oCommand.Parameters.Add("@Forward_benefit_amount_employee_transfer_out", SqlDbType.Decimal).Value = item.Forward_benefit_amount_employee_transfer_out;  // 49
                    oCommand.Parameters.Add("@Forward_unit_employee_transfer_out", SqlDbType.Decimal).Value = item.Forward_unit_employee_transfer_out;  // 50

                    oCommand.Parameters.Add("@Forward_amount_company_transfer_out", SqlDbType.Decimal).Value = item.Forward_amount_company_transfer_out;  // 51
                    oCommand.Parameters.Add("@Forward_benefit_amount_company_transfer_out", SqlDbType.Decimal).Value = item.Forward_benefit_amount_company_transfer_out;  // 52
                    oCommand.Parameters.Add("@Forward_unit_company_transfer_out", SqlDbType.Decimal).Value = item.Forward_unit_company_transfer_out;  // 53

                    oCommand.Parameters.Add("@Total_employee_amount", SqlDbType.Decimal).Value = item.Total_employee_amount;  // 54
                    oCommand.Parameters.Add("@Total_employee_benefit_amount", SqlDbType.Decimal).Value = item.Total_employee_benefit_amount;  // 55
                    oCommand.Parameters.Add("@Total_employee_unit", SqlDbType.Decimal).Value = item.Total_employee_unit;  // 56

                    oCommand.Parameters.Add("@Total_company_amount", SqlDbType.Decimal).Value = item.Total_company_amount;  // 57
                    oCommand.Parameters.Add("@Total_company_benefit_amount", SqlDbType.Decimal).Value = item.Total_company_benefit_amount;  // 58
                    oCommand.Parameters.Add("@Total_company_unit", SqlDbType.Decimal).Value = item.Total_company_unit;  // 59

                    oCommand.Parameters.Add("@Total_forward_amount_employee", SqlDbType.Decimal).Value = item.Total_forward_amount_employee;  // 60
                    oCommand.Parameters.Add("@Total_forward_benefit_amount_employee", SqlDbType.Decimal).Value = item.Total_forward_benefit_amount_employee;  // 61
                    oCommand.Parameters.Add("@Total_forward_unit_employee", SqlDbType.Decimal).Value = item.Total_forward_unit_employee;  // 62

                    oCommand.Parameters.Add("@Total_forward_amount_company", SqlDbType.Decimal).Value = item.Total_forward_amount_company;  // 63
                    oCommand.Parameters.Add("@Total_forward_benefit_amount_company", SqlDbType.Decimal).Value = item.Total_forward_benefit_amount_company;  // 64
                    oCommand.Parameters.Add("@Total_forward_unit_company", SqlDbType.Decimal).Value = item.Total_forward_unit_company;  // 65

                    oCommand.Parameters.Add("@ImportedDate", SqlDbType.DateTime).Value = date_now;
                    oCommand.Parameters.Add("@ImportedBy", SqlDbType.VarChar).Value = model.EmployeeId;

                    oCommand.Parameters.Add("@External_trans_emp_amount", SqlDbType.Decimal).Value = item.External_trans_emp_amount;  // 66
                    oCommand.Parameters.Add("@External_trans_emp_benefit_amount", SqlDbType.Decimal).Value = item.External_trans_emp_benefit_amount;  // 67
                    oCommand.Parameters.Add("@External_trans_comp_amount", SqlDbType.Decimal).Value = item.External_trans_comp_amount;  // 68
                    oCommand.Parameters.Add("@External_trans_comp_benefit_amount", SqlDbType.Decimal).Value = item.External_trans_comp_benefit_amount;  // 69

                    oCommand.ExecuteNonQuery();
                }

            }
        }

        public override List<string> GetYearProvidentfund()
        {
            List<string> list_year_string = new List<string>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;

            string cmd = "SELECT PF_Year FROM PF_ProvidentFundImport_Header GROUP BY PF_Year ORDER BY PF_Year DESC";
            oCommand = new SqlCommand(cmd, oConnection);
            oCommand.CommandType = CommandType.Text;
            oAdapter = new SqlDataAdapter(oCommand);

            oConnection.Open();

            DataTable oTable = new DataTable("ListYearProvidentFund");
            oAdapter.Fill(oTable);

            if (oTable.Rows.Count > 0)
            {
                foreach (DataRow row in oTable.Rows)
                {
                    list_year_string.Add(row[0].ToString());
                }
            }

            return list_year_string;
        }

        public override List<string> GetMonthProvidentfund(int year)
        {
            List<string> list_month_string = new List<string>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;

            string cmd = "SELECT PF_Month FROM PF_ProvidentFundImport_Header WHERE PF_Year = @year GROUP BY PF_Month ORDER BY PF_Month ASC";
            oCommand = new SqlCommand(cmd, oConnection);
            oCommand.CommandType = CommandType.Text;
            oCommand.Parameters.Add("@year", SqlDbType.Int).Value = year;
            oAdapter = new SqlDataAdapter(oCommand);

            oConnection.Open();

            DataTable oTable = new DataTable("ListMonthProvidentFund");
            oAdapter.Fill(oTable);

            if (oTable.Rows.Count > 0)
            {
                foreach (DataRow row in oTable.Rows)
                {
                    list_month_string.Add(row[0].ToString());
                }
            }

            oAdapter.Dispose();
            oCommand.Dispose();
            oTable.Dispose();
            if (oConnection.State == ConnectionState.Open)
            {
                oConnection.Dispose();
                oConnection.Close();
            }

            return list_month_string;
        }

        public override List<TypePVD> GetTextProvidentfund(int year, int month)
        {
            List<TypePVD> list_type_pvd = new List<TypePVD>();
            //List<string> list_txt_provident = new List<string>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;

            string cmd = "SELECT FundID,CompanyID FROM PF_ProvidentFundImport_Header WHERE PF_Year = @Year AND PF_Month = @Month";
            oCommand = new SqlCommand(cmd, oConnection);
            oCommand.CommandType = CommandType.Text;
            oCommand.Parameters.Add("@Year", SqlDbType.Int).Value = year;
            oCommand.Parameters.Add("@Month", SqlDbType.Int).Value = month;
            oAdapter = new SqlDataAdapter(oCommand);

            oConnection.Open();

            DataTable oTable = new DataTable("ListTextProvident");
            oAdapter.Fill(oTable);

            if (oTable.Rows.Count > 0)
            {
                foreach (DataRow row in oTable.Rows)
                {
                    var pvd = new TypePVD();
                    pvd.ParseToObject(row);
                    list_type_pvd.Add(pvd);
                    //list_txt_provident.Add(row[0].ToString());
                }

            }

            oAdapter.Dispose();
            oCommand.Dispose();
            oTable.Dispose();
            if (oConnection.State == ConnectionState.Open)
            {
                oConnection.Dispose();
                oConnection.Close();
            }

            return list_type_pvd;
        }

        public override DataProvidentfund GetDataProvidentfund(int year, int month, string fundId)
        {
            DataProvidentfund result = new DataProvidentfund();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;

            // 1. header providentfund
            string cmd = "SELECT * FROM PF_ProvidentFundImport_Header WHERE PF_Year = @Year AND PF_Month = @Month AND FundID = @p_FundID";
            oCommand = new SqlCommand(cmd, oConnection);
            oCommand.CommandType = CommandType.Text;
            oCommand.Parameters.Add("@Year", SqlDbType.Int).Value = year;
            oCommand.Parameters.Add("@Month", SqlDbType.Int).Value = month;
            oCommand.Parameters.Add("@p_FundID", SqlDbType.VarChar).Value = fundId;

            oConnection.Open();

            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("ProvidentfundHeader");
            oAdapter.Fill(oTable);

            if (oTable.Rows.Count > 0)
            {
                foreach (DataRow row in oTable.Rows)
                {
                    result.ParseToObject(row);
                }
            }

            if (result.PF_HeaderID > 0)
            {
                // 2. รายละเอียดของกองทุนสำรองเลี้ยงชีพ
                string cmd_sql = "SELECT * FROM PF_ProvidentFundImport_Detail WHERE PF_HeaderID = @p_PF_HeaderID";
                oCommand = new SqlCommand(cmd_sql, oConnection);
                oCommand.CommandType = CommandType.Text;
                oCommand.Parameters.Add("@p_PF_HeaderID", SqlDbType.Int).Value = result.PF_HeaderID;
                oAdapter = new SqlDataAdapter(oCommand);

                DataTable oTable_detail = new DataTable("ProvidentfundHeader");
                oAdapter.Fill(oTable_detail);

                if (oTable_detail.Rows.Count > 0)
                {
                    foreach (DataRow item in oTable_detail.Rows)
                    {
                        DataProvidentDetail detail = new DataProvidentDetail();
                        detail.ParseToObject(item);
                        result.list_detail_provident.Add(detail);
                    }
                }
                oTable_detail.Dispose();
            }

            oAdapter.Dispose();
            oCommand.Dispose();
            oTable.Dispose();
            if (oConnection.State == ConnectionState.Open)
            {
                oConnection.Dispose();
                oConnection.Close();
            }

            return result;
        }

        #endregion

        #region Load Payroll Employee

        public override PeriodSettingLatest GetSettingPeriodPayroll()
        {
            PeriodSettingLatest setting = new PeriodSettingLatest();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_PayslipPeriodSettingGetLatest", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("PeriodSettingPaySlip");
            oConnection.Open();
            oAdapter.Fill(oTable);

            setting.ParseToObject(oTable);

            oConnection.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();
            oTable.Dispose();

            if (oConnection.State == ConnectionState.Connecting)
                oConnection.Close();

            return setting;
        }

        public override void SaveOrUpdatePayrollEmployee(List<SapPayrollEmployee> list_payroll_employee)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            if (list_payroll_employee.Count > 0)
            {
                oConnection.Open();
                SqlTransaction trn = oConnection.BeginTransaction();

                SqlDataAdapter oAdapter = new SqlDataAdapter();
                DataTable oTable = new DataTable();
                SqlCommand oCommand = new SqlCommand();
                try
                {
                    foreach (var item in list_payroll_employee)
                    {
                        // 1. ตรวจสอบก่อนว่ามีการเพิ่มข้อมูล Payroll ไปแล้วหรือยัง
                        string sql_cmd = "SELECT * FROM PayslipImportHeader";
                        sql_cmd += " WHERE EmployeeID = @p_EmployeeID";
                        sql_cmd += " AND PeriodYear = @p_PeriodYear AND PeriodMonth = @p_PeriodMonth";
                        sql_cmd += " AND OffCycleFlag = @p_OffCycleFlag";
                        oCommand = new SqlCommand(sql_cmd, oConnection, trn);
                        oCommand.CommandType = CommandType.Text;
                        oCommand.Parameters.Clear();

                        oCommand.Parameters.Add("@p_EmployeeID", SqlDbType.VarChar).Value = item.EmployeeID;
                        oCommand.Parameters.Add("@p_PeriodYear", SqlDbType.Int).Value = item.PeriodYear;
                        oCommand.Parameters.Add("@p_PeriodMonth", SqlDbType.Int).Value = item.PeriodMonth;
                        oCommand.Parameters.Add("@p_OffCycleFlag", SqlDbType.Char).Value = item.OffCycleFlag;

                        oAdapter = new SqlDataAdapter(oCommand);
                        oTable = new DataTable("PayrollHeader");
                        oAdapter.Fill(oTable);
                        if (oTable.Rows.Count > 0)
                        {
                            // 1.1 Update ข้อมูลใหม่อีกครั้ง
                            int header_id = Convert.ToInt32(oTable.Rows[0]["HeaderID"]);
                            if (item.ListPayrollDetail.Count > 0)
                            {
                                foreach (var item_detail in item.ListPayrollDetail)
                                {
                                    // 1.2 Update Or Insert  รายละเอียดของ Payroll พนักงาน
                                    oCommand = new SqlCommand("sp_PY_PayrollEmployeeSaveOrUpdateDetail", oConnection, trn);
                                    oCommand.CommandType = CommandType.StoredProcedure;
                                    oCommand.Parameters.Clear();

                                    oCommand.Parameters.Add("@p_HeaderID", SqlDbType.Int).Value = header_id;
                                    oCommand.Parameters.Add("@p_ForPeriod", SqlDbType.VarChar).Value = item_detail.ForPeriod;
                                    oCommand.Parameters.Add("@p_InPeriod", SqlDbType.VarChar).Value = item_detail.InPeriod;
                                    oCommand.Parameters.Add("@p_WageTypeCode", SqlDbType.Char).Value = item_detail.WageTypeCode;
                                    oCommand.Parameters.Add("@p_WageTypeName", SqlDbType.VarChar).Value = item_detail.WageTypeName.Trim();
                                    //oCommand.Parameters.Add("@p_EvalType", SqlDbType.Char).Value = item_detail.TimeType;
                                    oCommand.Parameters.Add("@p_TimeAmount", SqlDbType.Decimal).Value = item_detail.TimeAmount;
                                    oCommand.Parameters.Add("@p_TimeUnit", SqlDbType.VarChar).Value = item_detail.TimeUnit;
                                    oCommand.Parameters.Add("@p_CurrencyAmount", SqlDbType.NVarChar).Value = item_detail.CurrencyAmount;
                                    oCommand.Parameters.Add("@p_CurrencyUnit", SqlDbType.VarChar).Value = item_detail.CurrencyUnit;
                                    oCommand.Parameters.Add("@p_ImportDate", SqlDbType.DateTime).Value = DateTime.Now;
                                    oCommand.ExecuteNonQuery();
                                }
                            }
                        }
                        else
                        {
                            // 1.2 Insert ข้อมูล
                            oCommand = new SqlCommand("sp_PY_PayrollEmployeeSave", oConnection, trn);
                            oCommand.CommandType = CommandType.StoredProcedure;
                            oCommand.Parameters.Clear();

                            oCommand.Parameters.Add("@p_HeaderID", SqlDbType.Int).Direction = ParameterDirection.Output;
                            oCommand.Parameters.Add("@p_EmployeeID", SqlDbType.VarChar).Value = item.EmployeeID;
                            oCommand.Parameters.Add("@p_PeriodYear", SqlDbType.Int).Value = item.PeriodYear;
                            oCommand.Parameters.Add("@p_PeriodMonth", SqlDbType.Int).Value = item.PeriodMonth;
                            oCommand.Parameters.Add("@p_AccumType", SqlDbType.Char).Value = item.AccumType;
                            oCommand.Parameters.Add("@p_OffCycleFlag", SqlDbType.Char).Value = item.OffCycleFlag;
                            oCommand.Parameters.Add("@p_PYDate", SqlDbType.DateTime).Value = item.PYDate;
                            oCommand.Parameters.Add("@p_ImportDate", SqlDbType.DateTime).Value = DateTime.Now;

                            oCommand.ExecuteNonQuery();

                            string id = oCommand.Parameters["@p_HeaderID"].Value.ToString();
                            int header_id = int.Parse(id);

                            if (item.ListPayrollDetail.Count > 0)
                            {
                                foreach (var item_detail in item.ListPayrollDetail)
                                {
                                    oCommand = new SqlCommand("sp_PY_PayrollEmployeeSaveDetail", oConnection, trn);
                                    oCommand.CommandType = CommandType.StoredProcedure;
                                    oCommand.Parameters.Clear();

                                    oCommand.Parameters.Add("@p_HeaderID", SqlDbType.Int).Value = header_id;
                                    oCommand.Parameters.Add("@p_ForPeriod", SqlDbType.VarChar).Value = item_detail.ForPeriod;
                                    oCommand.Parameters.Add("@p_InPeriod", SqlDbType.VarChar).Value = item_detail.InPeriod;
                                    oCommand.Parameters.Add("@p_WageTypeCode", SqlDbType.Char).Value = item_detail.WageTypeCode;
                                    oCommand.Parameters.Add("@p_WageTypeName", SqlDbType.VarChar).Value = item_detail.WageTypeName.Trim();
                                    oCommand.Parameters.Add("@p_TimeAmount", SqlDbType.Decimal).Value = item_detail.TimeAmount;
                                    oCommand.Parameters.Add("@p_TimeUnit", SqlDbType.VarChar).Value = item_detail.TimeUnit;
                                    oCommand.Parameters.Add("@p_CurrencyAmount", SqlDbType.NVarChar).Value = item_detail.CurrencyAmount;
                                    oCommand.Parameters.Add("@p_CurrencyUnit", SqlDbType.VarChar).Value = item_detail.CurrencyUnit;
                                    oCommand.Parameters.Add("@p_ImportDate", SqlDbType.DateTime).Value = DateTime.Now;
                                    oCommand.ExecuteNonQuery();
                                }
                            }
                        }
                    }
                    trn.Commit();
                }
                catch
                {
                    trn.Rollback();
                    throw;
                }
                finally
                {
                    oCommand.Dispose();
                    oAdapter.Dispose();
                    oTable.Dispose();
                    trn.Dispose();
                    if (oConnection.State == ConnectionState.Open)
                        oConnection.Close();
                }
            }

        }

        public override List<PeriodSettingLatest> GetDbPeriodSettingAll()
        {
            List<PeriodSettingLatest> list_period = new List<PeriodSettingLatest>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);

            string cmd = "select *  from PayslipPeriodSetting pps " +
                "where pps.Active = 1 " +
                "order by pps.PeriodYear desc, pps.PeriodMonth desc";

            SqlCommand oCommand = new SqlCommand(cmd, oConnection);
            oCommand.CommandType = CommandType.Text;

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("ListPeriodSettingPaySlip");
            oConnection.Open();
            oAdapter.Fill(oTable);

            if (oTable.Rows.Count > 0)
            {
                foreach (DataRow dr in oTable.Rows)
                {
                    PeriodSettingLatest data = new PeriodSettingLatest();
                    data.ParseToObject(dr);
                    list_period.Add(data);
                }
            }

            oAdapter.Dispose();
            oCommand.Dispose();
            oTable.Dispose();
            oConnection.Dispose();
            if (oConnection.State == ConnectionState.Connecting)
                oConnection.Close();

            return list_period;
        }
        #endregion

        public override string GetRootOrgForReport(string sEmpID, DateTime dDateTime)
        {
            DataTable dt = new DataTable();
            string sRootOrg = string.Empty;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_GetRootOrgForReport", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            oCommand.Parameters.Add(new SqlParameter("@p_EmployeeID", sEmpID));
            oCommand.Parameters.Add(new SqlParameter("@p_CheckDate", dDateTime));

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.Fill(dt);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            if(dt != null && dt.Rows.Count > 0)
            {
                sRootOrg = dt.Rows[0]["ROOT_ORGUNIT"].ToString();
            }

            return sRootOrg;
        }

        public override void SaveConfigurationLog(ConfigurationLog oConfigurationLog)
        {
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                oConnection.Open();
                SqlCommand oCommand = new SqlCommand("sp_ConfigurationLogSave", oConnection);
                try
                {
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.Parameters.AddWithValue("@p_EmployeeID", oConfigurationLog.EmployeeID);
                    oCommand.Parameters.AddWithValue("@p_ConfigTable", oConfigurationLog.ConfigTable);
                    oCommand.Parameters.AddWithValue("@p_OldData", ParseToXML(oConfigurationLog.OldData));
                    oCommand.Parameters.AddWithValue("@p_NewData", ParseToXML(oConfigurationLog.NewData));
                    oCommand.Parameters.AddWithValue("@p_Remark", oConfigurationLog.Remark);

                    oCommand.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    //throw new Exception("Save ConfigurationLog error", ex);
                }
                finally
                {
                    oCommand.Dispose();
                    oConnection.Close();
                    oConnection.Dispose();
                }
            }
        }
    }
}
