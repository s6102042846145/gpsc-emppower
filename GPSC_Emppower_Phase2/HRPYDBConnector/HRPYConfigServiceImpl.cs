﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using ESS.HR.PY.ABSTRACT;
using ESS.HR.PY.CONFIG;
using ESS.SHAREDATASERVICE;
using System.Globalization;
using ESS.UTILITY.EXTENSION;
using ESS.HR.PY.DATACLASS;
using System.IO;

namespace ESS.HR.PY.DB
{
    public class HRPYConfigServiceImpl : AbstractHRPYConfigService
    {
        #region Constructor
        public HRPYConfigServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
            oSqlManage["BaseConnStr"] = BaseConnStr;
        }

        #endregion Constructor

        #region Member
        //private Dictionary<string, string> Config { get; set; }
        private Dictionary<string, string> oSqlManage = new Dictionary<string, string>();

        private CultureInfo DefaultCultureInfo = CultureInfo.GetCultureInfo("en-GB");

        private static string ModuleID = "ESS.HR.PY.DB";
        public string CompanyCode { get; set; }
        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }

        #endregion Member


        public override string GetCommonText(string Category, string Language, string Code)
        {
            string returnValue = "";
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("SELECT dbo.GetCommonTextItem(@category,@code,@language)", oConnection);
            SqlParameter oParam;

            oParam = new SqlParameter("@category", SqlDbType.VarChar);
            oParam.Value = Category;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@language", SqlDbType.VarChar);
            oParam.Value = Language;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@code", SqlDbType.VarChar);
            oParam.Value = Code;
            oCommand.Parameters.Add(oParam);

            DataTable oTable = new DataTable("TEXTTABLE");
            oConnection.Open();
            returnValue = oCommand.ExecuteScalar().ToString();
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            return returnValue;
        }

        public override List<SpouseAllowance> GetAllSpouseAllowanceDropdownData()
        {
            List<SpouseAllowance> returnValue = new List<SpouseAllowance>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            SqlCommandBuilder oBuilder;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from SpouseAllowance", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("SpouseAllowance");
                    oBuilder = new SqlCommandBuilder(oAdapter);

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        SpouseAllowance Item = new SpouseAllowance();
                        Item.Key = (string)dr["SpouseAllowanceKey"];
                        Item.Description = (string)dr["SpouseAllowanceDescription"];
                        returnValue.Add(Item);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        public override List<PayslipPeriodSetting> GetAllPayslipPeriod()
        {
            List<PayslipPeriodSetting> oReturn = new List<PayslipPeriodSetting>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_GetAllPayslipPeriod", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("PayslipPeriodSetting");
            oTable.CaseSensitive = false;
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                PayslipPeriodSetting item = new PayslipPeriodSetting();
                item.ParseToObject(dr);

                if (!string.IsNullOrEmpty(dr["IsEvaluationHistory"].ToString()))
                {
                    item.isEvaluationHistory = Convert.ToBoolean(dr["IsEvaluationHistory"].ToString());
                }

                oReturn.Add(item);
            }
            oTable.Dispose();
            return oReturn;
        }

        public override List<WageType> GetWageTypeAll()
        {
            List<WageType> oReturn = new List<WageType>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_PayslipWageTypeGetAll", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("WageType");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                WageType item = new WageType();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oTable.Dispose();
            return oReturn;
        }

        public override WebWageType GetWebWageTypeByCode(int Code)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_PayslipWebWageTypeGetByCode", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter oParam;
            oParam = new SqlParameter("@p_WebCode", SqlDbType.Int);
            oParam.Value = Code;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("WebWageType");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            WebWageType item = new WebWageType();
            foreach (DataRow dr in oTable.Rows)
                item.ParseToObject(dr);

            oTable.Dispose();
            return item;
        }

        public override void SavePeriodSetting(List<PayslipPeriodSetting> periods)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand = new SqlCommand("select * from PayslipPeriodSetting", oConnection, tx);
            SqlCommand oCommand1 = new SqlCommand("delete from PayslipPeriodSetting", oConnection, tx);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            SqlCommandBuilder oCB = new SqlCommandBuilder(oAdapter);

            DataTable oTable = new DataTable("PayslipPeriodSetting");

            try
            {
                oCommand1.ExecuteNonQuery();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                for (int index = 0; index < periods.Count; index++)
                {
                    PayslipPeriodSetting period = periods[index];
                    DataRow oNewRow = oTable.NewRow();
                    oNewRow["PeriodID"] = index;
                    oNewRow["IsOffCycle"] = period.IsOffCycle;
                    oNewRow["PeriodYear"] = period.PeriodYear;
                    oNewRow["PeriodMonth"] = period.PeriodMonth;
                    if (period.IsOffCycle)
                    {
                        oNewRow["OffCycleDate"] = period.OffCycleDate;
                    }
                    else
                    {
                        oNewRow["OffCycleDate"] = DBNull.Value;
                    }
                    if (period.EffectiveDate == DateTime.MinValue)
                        oNewRow["EffectiveDate"] = DBNull.Value;
                    else
                        oNewRow["EffectiveDate"] = period.EffectiveDate;
                    oNewRow["Active"] = period.Active;
                    oNewRow["IsDraft"] = period.IsDraft;
                    oNewRow["IsEvaluationHistory"] = period.isEvaluationHistory;
                    oTable.Rows.Add(oNewRow);
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw ex;
            }
            finally
            {
                oTable.Dispose();
                oCB.Dispose();
                if (oConnection.State == ConnectionState.Open)
                    oConnection.Close();
                oConnection.Dispose();
                oAdapter.Dispose();
                oCommand.Dispose();
                oCommand1.Dispose();
            }
        }

        public override List<PayslipPeriodSetting> GetActivePayslipPeriod()
        {
            List<PayslipPeriodSetting> oReturn = new List<PayslipPeriodSetting>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("PayslipPeriodSetting");
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("select * from PayslipPeriodSetting Where Active = 1 order by PeriodYear Desc, PeriodMonth Desc", oConnection);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                PayslipPeriodSetting item = new PayslipPeriodSetting();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }
            oTable.Dispose();
            return oReturn;
        }

        public override List<PayslipPeriodSetting> GetPeriodSettingByYear(int year)
        {
            List<PayslipPeriodSetting> oReturn = new List<PayslipPeriodSetting>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_PayslipPeriodSettingGetByYear", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter oParam = new SqlParameter("@p_Year", SqlDbType.Int);
            oParam.Value = year;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("PayslipPeriodSetting");

            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                PayslipPeriodSetting item = new PayslipPeriodSetting();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }
            oTable.Dispose();
            return oReturn;
        }

        public override List<PayslipPeriodSetting> GetPeriodSettingByYearForAdmin(int year)
        {
            List<PayslipPeriodSetting> oReturn = new List<PayslipPeriodSetting>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_PayslipPeriodSettingGetByYearForAdmin", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter oParam = new SqlParameter("@p_Year", SqlDbType.Int);
            oParam.Value = year;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("PayslipPeriodSetting");

            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                PayslipPeriodSetting item = new PayslipPeriodSetting();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }
            oTable.Dispose();
            return oReturn;
        }

        public override List<Tavi50PeriodSetting> GetActiveTavi50Period()
        {

            List<Tavi50PeriodSetting> oReturn = new List<Tavi50PeriodSetting>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_Tavi50PeriodSettingGet", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("Tavi50PeriodSetting");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                Tavi50PeriodSetting item = new Tavi50PeriodSetting();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }
            oTable.Dispose();
            return oReturn;
        }

        public override List<Tavi50PeriodSetting> GetAllTavi50Period(string path)
        {
            List<Tavi50PeriodSetting> oReturn = new List<Tavi50PeriodSetting>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_Tavi50PeriodSettingGetAll", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("Tavi50PeriodSetting");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                Tavi50PeriodSetting item = new Tavi50PeriodSetting();
                item.ParseToObject(dr);
                string fileName = CompanyCode + "_" + "Signature_" + item.PeriodYear + ".jpeg";
                //string fileName = "Signature_" + item.PeriodYear + ".jpeg";
                string directoryPath = path + item.PeriodYear;

                if (File.Exists(directoryPath + "/" + fileName))
                {
                    Byte[] bytes = File.ReadAllBytes(directoryPath + "/" + fileName);
                    item.SignatureImageBase64 = "data:image/jpeg;base64," + Convert.ToBase64String(bytes);
                }

                oReturn.Add(item);
            }
            oTable.Dispose();
            return oReturn;
        }

        public override void SaveTavi50PeriodSetting(List<Tavi50PeriodSetting> periods, string path)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand = new SqlCommand("select * from Tavi50PeriodSetting", oConnection, tx);
            SqlCommand oCommand1 = new SqlCommand("delete from Tavi50PeriodSetting", oConnection, tx);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            SqlCommandBuilder oCB = new SqlCommandBuilder(oAdapter);

            DataTable oTable = new DataTable("Tavi50PeriodSetting");

            try
            {
                oCommand1.ExecuteNonQuery();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                for (int index = 0; index < periods.Count; index++)
                {
                    Tavi50PeriodSetting period = periods[index];
                    DataRow oNewRow = oTable.NewRow();
                    oNewRow["PeriodID"] = index;
                    oNewRow["PeriodYear"] = period.PeriodYear;
                    if (period.EffectiveDate == DateTime.MinValue)
                        oNewRow["EffectiveDate"] = DBNull.Value;
                    else
                        oNewRow["EffectiveDate"] = period.EffectiveDate;
                    oNewRow["IsActive"] = period.IsActive;
                    oTable.Rows.Add(oNewRow);

                    if (!string.IsNullOrEmpty(period.SignatureImageBase64))
                    {
                        var spl = period.SignatureImageBase64.Split('/')[1];
                        var format = spl.Split(';')[0];
                        string stringconvert = period.SignatureImageBase64.Replace($"data:image/" + format + ";base64,", String.Empty);

                        string fileName = CompanyCode + "_" + "Signature_" + period.PeriodYear + ".jpeg";
                        //string fileName = "Signature_" + period.PeriodYear + ".jpeg";
                        string directoryPath = path + period.PeriodYear;
                        if (!Directory.Exists(directoryPath))
                        {
                            Directory.CreateDirectory(directoryPath);
                        }

                        if (File.Exists(directoryPath + "/" + fileName))
                        {
                            File.Delete(directoryPath + "/" + fileName);
                        }

                        File.WriteAllBytes(directoryPath + "/" + fileName, Convert.FromBase64String(stringconvert));
                    }
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw ex;
            }
            finally
            {
                oTable.Dispose();
                oCB.Dispose();
                if (oConnection.State == ConnectionState.Open)
                    oConnection.Close();
                oConnection.Dispose();
                oAdapter.Dispose();
                oCommand.Dispose();
                oCommand1.Dispose();
            }
        }


        public override List<SpouseAllowance> GetSpouseAllowanceList()
        {
            List<SpouseAllowance> returnValue = new List<SpouseAllowance>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            SqlCommandBuilder oBuilder;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from SpouseAllowance", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("SpouseAllowance");
                    oBuilder = new SqlCommandBuilder(oAdapter);

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        SpouseAllowance Item = new SpouseAllowance();
                        Item.Key = (string)dr["SpouseAllowanceKey"];
                        Item.Description = (string)dr["SpouseAllowanceDescription"];
                        returnValue.Add(Item);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        public override SpouseAllowance GetSpouseAllowance(string Code)
        {
            SpouseAllowance returnValue = new SpouseAllowance();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            SqlCommandBuilder oBuilder;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from SpouseAllowance Where SpouseAllowanceKey = @SpouseAllowanceKey", oConnection))
                {
                    SqlParameter oParam;
                    oParam = new SqlParameter("@SpouseAllowanceKey", SqlDbType.VarChar);
                    oParam.Value = Code;
                    oCommand.Parameters.Add(oParam);

                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("SpouseAllowance");
                    oBuilder = new SqlCommandBuilder(oAdapter);

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        returnValue.Key = (string)dr["SpouseAllowanceKey"];
                        returnValue.Description = (string)dr["SpouseAllowanceDescription"];
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }
        public override List<WageType> WageTypeAdditionalGetAll()
        {
            List<WageType> oReturn = new List<WageType>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_WageTypeAdditionalGetAll", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("WageTypeAdditional");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            WageType item;
            foreach (DataRow dr in oTable.Rows)
            {
                item = new WageType();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oTable.Dispose();
            return oReturn;
        }

        public override Company GetCompanyData(string Area, string SubArea)
        {
            Company returnValue = new Company();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            SqlCommandBuilder oBuilder;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from CompanyData Where Area = @Area and SubArea = @SubArea", oConnection))
                {
                    SqlParameter oParam;
                    oParam = new SqlParameter("@Area", SqlDbType.VarChar);
                    oParam.Value = Area;
                    oCommand.Parameters.Add(oParam);

                    oParam = new SqlParameter("@SubArea", SqlDbType.VarChar);
                    oParam.Value = SubArea;
                    oCommand.Parameters.Add(oParam);

                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("CompanyData");
                    oBuilder = new SqlCommandBuilder(oAdapter);

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        returnValue.ParseToObject(dr);

                        //AddBy: Ratchatawan W. (2012-12-24)
                        SqlCommand oCommand2 = new SqlCommand("select * from CompanyLogo where CompanyID = @CompanyID", oConnection);
                        oParam = new SqlParameter("@CompanyID", SqlDbType.Int);
                        oParam.Value = returnValue.CompanyID;
                        oCommand2.Parameters.Add(oParam);
                        DataTable oTable2 = new DataTable();
                        oAdapter = new SqlDataAdapter(oCommand2);
                        oAdapter.Fill(oTable2);
                        foreach (DataRow dr2 in oTable2.Rows)
                        {
                            returnValue.LogoName = dr2["LogoName"].ToString();
                            returnValue.SignatureName = dr2["SignatureName"].ToString();
                            returnValue.SignatureLogo = dr2["SignatureLogo"].ToString();
                        }

                        break;
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        public override List<string> GetResponseCodeByOrganizationOnly(string Role, string EmpID, string RootOrg)
        {
            List<string> oReturn = new List<string>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from UserRoleResponseSnapShot where EmployeeID = @EmpID and UserRole = @Role and @Now between BeginDate and EndDate AND ResponseType='O'", oConnection);

            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmpID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Role", SqlDbType.VarChar);
            oParam.Value = Role;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Now", SqlDbType.DateTime);
            oParam.Value = DateTime.Now;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("USERROLERESPONSESNAPSHOT");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();

            string ResponseCode = string.Empty;
            foreach (DataRow dr in oTable.Rows)
            {
                ResponseCode = (string)dr["ResponseCode"] == "*" ? RootOrg : (string)dr["ResponseCode"];
                if (!oReturn.Exists(delegate (string s) { return s == ResponseCode; }))
                    oReturn.Add(ResponseCode);
            }
            oTable.Dispose();
            return oReturn;
        }

        public override Dictionary<int, LetterType> GetLetterType(string strPermissionCode)
        {
            DataTable dt = new DataTable();
            Dictionary<int, LetterType> oReturn = new Dictionary<int, LetterType>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_LE_LetterTypeGet", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter param;
            param = new SqlParameter("@p_PermissionCode", SqlDbType.VarChar);
            param.Value = strPermissionCode;
            oCommand.Parameters.Add(param);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.Fill(dt);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            LetterType oLetterType;

            foreach (DataRow dr in dt.Rows)
            {
                oLetterType = new LetterType();
                oLetterType.ParseToObject(dr);
                if (!oReturn.ContainsKey(oLetterType.LetterTypeID))
                    oReturn.Add(oLetterType.LetterTypeID, oLetterType);
            }

            return oReturn;
        }

        public override List<LetterEmbassy> GetLetterEmbassy()
        {
            DataTable dt = new DataTable();
            List<LetterEmbassy> oReturn = new List<LetterEmbassy>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_LE_LetterEmbassyGet", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.Fill(dt);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            LetterEmbassy oLetterEmbassy;

            foreach (DataRow dr in dt.Rows)
            {
                oLetterEmbassy = new LetterEmbassy();
                oLetterEmbassy.ParseToObject(dr);
                oReturn.Add(oLetterEmbassy);
            }

            return oReturn;
        }

        public override LetterType GetSignPositionByLetterType(int iLetterType)
        {
            DataTable dt = new DataTable();
            LetterType oLetterType = new LetterType();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from LE_LetterType where LetterTypeID = @LetterTypeID ", oConnection);

            SqlParameter param;
            param = new SqlParameter("@LetterTypeID", SqlDbType.Int);
            param.Value = iLetterType;
            oCommand.Parameters.Add(param);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("LETTERTYPE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();

            if (oTable != null && oTable.Rows.Count > 0)
            {
                oLetterType.ParseToObject(oTable.Rows[0]);
            }

            oTable.Dispose();
            return oLetterType;
        }

        public override LetterEmbassy GetLetterEmbassyByID(int iEmbassyID)
        {
            DataTable dt = new DataTable();
            LetterEmbassy oReturn = new LetterEmbassy();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_LE_LetterEmbassyGetByID", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter param;
            param = new SqlParameter("@p_EmbassyID", SqlDbType.Int);
            param.Value = iEmbassyID;
            oCommand.Parameters.Add(param);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.Fill(dt);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();


            foreach (DataRow dr in dt.Rows)
            {
                oReturn.ParseToObject(dr);
            }

            return oReturn;
        }

        public override LetterType GetLetterTypeByID(int iLetterTypeID)
        {
            DataTable dt = new DataTable();
            LetterType oReturn = new LetterType();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_LE_LetterTypeGetByID", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter param;
            param = new SqlParameter("@p_LetterTypeID", SqlDbType.Int);
            param.Value = iLetterTypeID;
            oCommand.Parameters.Add(param);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.Fill(dt);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();


            foreach (DataRow dr in dt.Rows)
            {
                oReturn.ParseToObject(dr);
            }

            return oReturn;
        }

        public override DateTime GetPreviosEffectiveDatePayslip()
        {
            DateTime dtResult = DateTime.MinValue;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataReader oReader = null;
            oCommand = new SqlCommand("sp_PayslipPeriodSettingGetPrevEffectiveDate", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            try
            {
                oConnection.Open();
                oReader = oCommand.ExecuteReader(CommandBehavior.SingleRow);
                if (oReader.HasRows)
                {
                    while (oReader.Read())
                    {
                        dtResult = (DateTime)oReader["PrevEffectiveDate"];
                    }
                }

            }
            catch (Exception ex)
            {
                //throw ex;
            }
            finally
            {
                if (oConnection.State != ConnectionState.Closed)
                    oConnection.Close();
                oConnection.Dispose();

                if (oReader != null && !oReader.IsClosed)
                    oReader.Close();
                oReader.Dispose();
            }

            return dtResult;

        }

        public override bool ShowTimeAwareLink(int LinkID)
        {
            bool oReturn = false;

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_GetTimeAwareLink", oConnection);

            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_LinkID", LinkID);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("TIMEAWARELINK");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();

            if (oTable.Rows.Count > 0)
                oReturn = Convert.ToBoolean(oTable.Rows[0][0]);

            oConnection.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();
            oTable.Dispose();

            return oReturn;
        }
    }
}
