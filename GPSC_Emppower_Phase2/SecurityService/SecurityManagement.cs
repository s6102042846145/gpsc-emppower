using System.Configuration;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using ESS.SHAREDATASERVICE.DATACLASS;
using ESS.SHAREDATASERVICE;
namespace ESS.SECURITY
{
    public class SecurityManagement
    {
        #region Constructor
        private SecurityManagement()
        {

        }
        #endregion

        #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        //private static Dictionary<string, SecurityManagement> Cache = new Dictionary<string, SecurityManagement>();

        private static string ModuleID = "ESS.SECURITY";
        public string CompanyCode { get; set; }

        public static SecurityManagement CreateInstance(string oCompanyCode)
        {
            //if (!Cache.ContainsKey(oCompanyCode))
            //{
            //    Cache[oCompanyCode] = new SecurityManagement()
            //    {
            //        CompanyCode = oCompanyCode
            //        ,
            //        Config = ShareDataManager.GetModuleSettings(oCompanyCode, AssamblyName)
            //    };
            //}
            //return Cache[oCompanyCode];
            SecurityManagement oSecurityManagement = new SecurityManagement()
            {
                CompanyCode = oCompanyCode
            };
            return oSecurityManagement;
        }
        #endregion MultiCompany  Framework

        #region Public Property
        public bool ALLOWSINGLESIGNON
        {
            get { return bool.Parse(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ALLOWSINGLESIGNON")); }

        }
        public bool ALLOWSYSTEMUSER
        {
            get { return bool.Parse(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ALLOWSYSTEMUSER")); }

        }
        public string TicketID { get; set; }
        public string Mode { get; set; }
        public bool IsAuthorize { get; set; } 
        #endregion
        public SecurityManagement CheckAuthorize(string profile, string Username, string Password)
        {
            SecurityManagement oSecurityManagement = CreateInstance(CompanyCode);
            bool lResult = false;
            /* HARDCODE */
            if (profile.ToUpper() == "SYSTEM")
            {
                oSecurityManagement.Mode = "SYSTEM";
                if (Username == "#SYSTEM" && Password == "MYADMIN")
                {
                    lResult = true;
                }
            }
            else
            {
                oSecurityManagement.Mode = ServiceManager.CreateInstance(CompanyCode).AUTHORIZEMODE;
                lResult = ServiceManager.CreateInstance(CompanyCode).AuthorizeService(profile).CheckAuthroize(Username, Password);
            }
            /* HARDCODE */
            oSecurityManagement.IsAuthorize = lResult;
            if (lResult)
            {
                //create ticket
                oSecurityManagement.TicketID = "testticket";
            }
            return oSecurityManagement;
        }

        public SecurityManagement CheckAuthorizeWithoutPassword(string profile, string EmployeeID)
        {
            SecurityManagement oAuthorize = CreateInstance(CompanyCode);
            oAuthorize.Mode = ServiceManager.CreateInstance(CompanyCode).AUTHORIZEMODE;
            oAuthorize.TicketID = "testticket";
            oAuthorize.IsAuthorize = true;
            return oAuthorize;
        }

        public SecurityManagement CheckAuthorizeForNoneEmployee(string profile, string EmployeeID)
        {
            SecurityManagement oAuthorize = CreateInstance(CompanyCode);
            oAuthorize.Mode = ServiceManager.CreateInstance(CompanyCode).AUTHORIZEMODE;
            oAuthorize.TicketID = "NoneEmployee";
            oAuthorize.IsAuthorize = true;
            return oAuthorize;
        }

        public SecurityManagement CheckAuthorizeByTicket(string profile, string TicketID)
        {
            SecurityManagement oAuthorize = new SecurityManagement();
            oAuthorize.Mode = ServiceManager.CreateInstance(CompanyCode).AUTHORIZEMODE;
            oAuthorize.TicketID = TicketID;
            oAuthorize.IsAuthorize = true;
            return oAuthorize;
        }


        public bool ValidateDomain(string strProfile, string strDomain)
        {
            return ServiceManager.CreateInstance(CompanyCode).AuthorizeService(strProfile).ValidateDomain(strDomain);
        }
    }
}