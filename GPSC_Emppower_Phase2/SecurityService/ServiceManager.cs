using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using ESS.SHAREDATASERVICE;

namespace ESS.SECURITY
{
    public class ServiceManager
    {
        #region Constructor
        private ServiceManager()
        {

        }
        #endregion

        #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        //private static Dictionary<string, ServiceManager> Cache = new Dictionary<string, ServiceManager>();

        private static string ModuleID = "ESS.SECURITY";

        public string CompanyCode { get; set; }

        public static ServiceManager CreateInstance(string oCompanyCode)
        {
            //if (!Cache.ContainsKey(oCompanyCode))
            //{
            //    Cache[oCompanyCode] = new ServiceManager()
            //    {
            //        CompanyCode = oCompanyCode
            //        ,
            //        Config = ShareDataManager.GetModuleSettings(oCompanyCode, AssamblyName)
            //    };
            //}
            //return Cache[oCompanyCode];

            ServiceManager oServiceManager = new ServiceManager()
            {
                CompanyCode = oCompanyCode
            };
            return oServiceManager;
        }
        #endregion MultiCompany  Framework

        #region " PrivateData "

        internal string AUTHORIZEMODE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "AUTHORIZEMODE");
            }
        }
        private Type GetService(string Mode, string ClassName)
        {
            Type oReturn = null;
            Assembly oAssembly;
            string assemblyName = string.Format("ESS.AUTHORIZE.{0}", Mode.ToUpper());
            string typeName = string.Format("ESS.AUTHORIZE.{0}.{1}", Mode.ToUpper(), ClassName);// CultureInfo.CurrentCulture.TextInfo.ToTitleCase(ClassName.ToLower()));
            oAssembly = Assembly.Load(assemblyName);    // Load assembly (dll)
            oReturn = oAssembly.GetType(typeName);      // Load class
            return oReturn;
        }

        #endregion " PrivateData "

        public IAuthorizeService AuthorizeService(string Profile)
        {
            Type oType = GetService(AUTHORIZEMODE, "AuthorizeServiceImpl");
            if (oType == null)
            {
                return null;
            }
            else
            {
                return (IAuthorizeService)Activator.CreateInstance(oType,CompanyCode);
            }
        }
    }
}