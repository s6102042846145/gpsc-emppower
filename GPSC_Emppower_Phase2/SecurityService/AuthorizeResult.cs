using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.SECURITY
{
    public class AuthorizeResult
    {
        private bool __result = false;
        private string __ticketID = "";
        internal AuthorizeResult(bool result, string ticket)
        {
            __result = result;
            __ticketID = ticket;
        }
        
        public bool Result
        {
            get
            {
                return __result;
            }
        }
        public string TicketID
        {
            get
            {
                return __ticketID;
            }
        }
    }
}
