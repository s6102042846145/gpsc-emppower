using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Reflection;


namespace ESS.SECURITY
{
    public class Authorization
    {
        private static Configuration __config;
        private static Configuration config
        {
            get
            {
                if (__config == null)
                {
                    __config = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().CodeBase.Replace("file:///", "").Replace("/", "\\"));
                }
                return __config;
            }
        }

        private string __mode;
        private string __ticketID = "";
        private bool __isAuthorize;
        private Authorization()
        { 
        }
        public static bool ALLOWSINGLESIGNON
        {
            get
            {
                bool flag = false;
                if (config == null || config.AppSettings.Settings["ALLOWSINGLESIGNON"] == null)
                    return flag;
                else
                {
                    flag = config.AppSettings.Settings["ALLOWSINGLESIGNON"].Value == bool.TrueString ? true : false;
                    return flag;
                }
            }
        }

        public static Authorization CheckAuthorize(string profile, string Username, string Password)
        {
            Authorization oAuthorize = new Authorization();
            bool lResult = false;
            /* HARDCODE */
            if (profile.ToUpper() == "SYSTEM")
            {
                oAuthorize.__mode = "SYSTEM";
                if (Username == "#SYSTEM" && Password == "MYADMIN")
                {
                    lResult = true;
                }
            }
            else
            {
                oAuthorize.__mode = ServiceManager.AUTHORIZEMODE(profile);
                lResult = ServiceManager.AuthorizeService(profile).CheckAuthroize(Username, Password);
            }
            /* HARDCODE */
            oAuthorize.__isAuthorize = lResult;
            if (lResult)
            {
                //create ticket
                oAuthorize.__ticketID = "testticket";
            }
            return oAuthorize;
        }

        public static Authorization CheckAuthorizeWithoutPassword(string profile, string EmployeeID)
        {
            Authorization oAuthorize = new Authorization();
            oAuthorize.__mode = ServiceManager.AUTHORIZEMODE(profile);
            oAuthorize.__isAuthorize = true;
            oAuthorize.__ticketID = "testticket";

            return oAuthorize;
        }

        public static Authorization CheckAuthorizeForNoneEmployee(string profile, string EmployeeID)
        {
            Authorization oAuthorize = new Authorization();
            oAuthorize.__mode = ServiceManager.AUTHORIZEMODE(profile);
            oAuthorize.__ticketID = "NoneEmployee";
            oAuthorize.__isAuthorize = true;
            return oAuthorize;
        }

        public static Authorization CheckAuthorizeByTicket(string profile, string TicketID)
        {
            Authorization oAuthorize = new Authorization();
            oAuthorize.__mode = ServiceManager.AUTHORIZEMODE(profile);
            oAuthorize.__isAuthorize = true;
            oAuthorize.__ticketID = TicketID;

            return oAuthorize;
        }
        
        public string TicketID
        {
            get
            {
                return __ticketID;
            }
        }
        public string Mode
        {
            get
            {
                return __mode;
            }
        }
        public bool IsAuthorize
        {
            get
            {
                return __isAuthorize;
            }
        }
    }
}
