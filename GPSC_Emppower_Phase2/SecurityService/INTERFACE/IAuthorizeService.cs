using System;

namespace ESS.SECURITY
{
    public interface IAuthorizeService
    {
        bool CheckAuthroize(string UserName, string Password);

        bool HavePassword(string UserName);

        void ChangePassword(string UserName, String NewPassword);

        void ChangePassword(string UserName, string OldPassword, string NewPassword);

        void ClearPassword(string UserName);

        bool ValidateDomain(string Domain);
    }
}