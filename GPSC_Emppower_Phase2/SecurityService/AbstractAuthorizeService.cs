using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.SECURITY
{
    public class AbstractAuthorizeService : IAuthorizeService
    {
        #region IAuthorizeService Members

        public virtual bool CheckAuthroize(string UserName, string Password)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual bool HavePassword(string UserName)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void ChangePassword(string UserName, string NewPassword)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void ChangePassword(string UserName, string OldPassword, string NewPassword)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void ClearPassword(string UserName)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion
    }
}
