using System;

namespace ESS.SECURITY
{
    public class NoAuthorizeException : Exception
    {
        private string __AttributeName;

        public NoAuthorizeException(string AttributeName)
        {
            __AttributeName = AttributeName;
        }

        public string AttributeName
        {
            get
            {
                return __AttributeName;
            }
        }

        public override string ToString()
        {
            return string.Format("You not have authorize for '{0}'", this.AttributeName);
        }
    }
}