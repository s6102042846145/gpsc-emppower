﻿using ESS.HR.FB.ABSTRACT;
using ESS.HR.FB.DATACLASS;
using ESS.SHAREDATASERVICE;
using System;
using System.Collections.Generic;
using ESS.UTILITY.EXTENSION;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using ESS.UTILITY.DATACLASS;
using ESS.UTILITY.LOG;
using System.IO;

namespace ESS.HR.FB.DB
{
    public class HRFBDataServiceImpl : AbstractHRFBDataService
    {
        #region Constructor
        public HRFBDataServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
            oSqlManage["BaseConnStr"] = BaseConnStr;
        }

        #endregion Constructor

        #region MemFBr
        private Dictionary<string, string> oSqlManage = new Dictionary<string, string>();
        private static string ModuleID = "ESS.HR.FB.DB";


        public string CompanyCode { get; set; }
        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }

        #endregion Member


        public override List<FBSelectionItemRate> FB_SelectionItemRateGet()
        {
            List<FBSelectionItemRate> oResult = new List<FBSelectionItemRate>();
            oSqlManage["ProcedureName"] = "sp_FB_SelectionItemRateGet";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oResult.AddRange(DatabaseHelper.ExecuteQuery<FBSelectionItemRate>(oSqlManage, oParamRequest));

            return oResult;
        }

        public override List<FBYearlySelection> FB_YearlySelectionGet(string Year)
        {
            List<FBYearlySelection> oResult = new List<FBYearlySelection>();
            oSqlManage["ProcedureName"] = "sp_FB_YearlySelectionGet";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@Year"] = Year;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<FBYearlySelection>(oSqlManage, oParamRequest));

            return oResult;
        }

        public override List<FBEnrollment> GetEnrollmentData(String EmployeeID, String EmpSubGroup, String RequestNo, String KeyYear)
        {
            List<FBEnrollment> oResult = new List<FBEnrollment>();
            oSqlManage["ProcedureName"] = "sp_FB_EnrollmentSelected";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@i_EmployeeID"] = EmployeeID;
            oParamRequest["@i_EmpSubGroup"] = EmpSubGroup;
            oParamRequest["@i_RequestNo"] = RequestNo;
            oParamRequest["@i_YEAR"] = KeyYear;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<FBEnrollment>(oSqlManage, oParamRequest));
            return oResult;
        }

        public override List<FBReimburse> GetReimburseQuota(String EmployeeID, String EmpSubGroup, String KeyYear)
        {
            List<FBReimburse> oResult = new List<FBReimburse>();
            oSqlManage["ProcedureName"] = "sp_FB_ReimburseQuota";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@i_EmployeeID"] = EmployeeID;
            oParamRequest["@i_EmpSubGroup"] = EmpSubGroup;
            oParamRequest["@i_YEAR"] = KeyYear;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<FBReimburse>(oSqlManage, oParamRequest));

            return oResult;
        }

        public override List<FBReimburse> GetReimburseSelected(String EmployeeID, String EmpSubGroup, String RequestNo, String KeyYear)
        {
            List<FBReimburse> oResult = new List<FBReimburse>();
            oSqlManage["ProcedureName"] = "sp_FB_ReimburseSelected";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@i_EmployeeID"] = EmployeeID;
            oParamRequest["@i_EmpSubGroup"] = EmpSubGroup;
            oParamRequest["@i_RequestNo"] = RequestNo;
            oParamRequest["@i_YEAR"] = KeyYear;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<FBReimburse>(oSqlManage, oParamRequest));

            return oResult;
        }

        public override List<FBConfiguration> GetTextDetail(String EmployeeID, String KeyCode)
        {
            List<FBConfiguration> oResult = new List<FBConfiguration>();
            oSqlManage["ProcedureName"] = "sp_FB_GetTextDetail";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@i_KeyCode"] = KeyCode;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<FBConfiguration>(oSqlManage, oParamRequest));

            return oResult;
        }

        public override List<FBConfiguration> GETFreeText(String KeyType, String KeyCode, String KeyValue)
        {
            List<FBConfiguration> oResult = new List<FBConfiguration>();
            oSqlManage["ProcedureName"] = "sp_FB_FreeTextSave";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@TYPE"] = KeyType;
            oParamRequest["@Code"] = KeyCode;
            oParamRequest["@Value"] = KeyValue;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<FBConfiguration>(oSqlManage, oParamRequest));

            return oResult;
        }

        public override List<FBPeriodSetting> GetFlexibleBenefitSetting(String EmployeeID, String KeyType, int KeyYear, String KeyValue)
        {
            List<FBPeriodSetting> oResult = new List<FBPeriodSetting>();
            oSqlManage["ProcedureName"] = "sp_FB_FlexibleBenefitSetting";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@TYPE"] = KeyType;
            oParamRequest["@EmployeeID"] = EmployeeID;
            oParamRequest["@YEAR"] = KeyYear;
            oParamRequest["@VALUES"] = KeyValue;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<FBPeriodSetting>(oSqlManage, oParamRequest));

            return oResult;
        }

        public override List<YearsConfig> GetNextYears()
        {
            List<YearsConfig> oResult = new List<YearsConfig>();
            oSqlManage["ProcedureName"] = "sp_BE_GetYearsNext";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oResult.AddRange(DatabaseHelper.ExecuteQuery<YearsConfig>(oSqlManage, oParamRequest));

            return oResult;
        }

        public override List<FBPeriodSetting> CheckPeriodSetting(int KeyYear)
        {
            List<FBPeriodSetting> oResult = new List<FBPeriodSetting>();
            oSqlManage["ProcedureName"] = "sp_FB_checkPeriod";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@YEAR"] = KeyYear;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<FBPeriodSetting>(oSqlManage, oParamRequest));

            return oResult;
        }

        public override bool FB_CheckPeriodSetting(string Year)
        {
            var result = false;
            List<FBPeriodSetting> oResult = new List<FBPeriodSetting>();
            oSqlManage["ProcedureName"] = "sp_FB_CheckPeriodSetting";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@Year"] = Year;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<FBPeriodSetting>(oSqlManage, oParamRequest));
            if (oResult.Count > 0)
            {
               result = true;
            }

            return result;
        }

        public override bool FB_CheckEnrollmentDuplicate(string Year,string EmployeeID,String RequestNo)
        {
            var result = false;
            List<FBEnrollment> oResult = new List<FBEnrollment>();
            oSqlManage["ProcedureName"] = "sp_FB_CheckEnrollmentDuplicate";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@Year"] = Year;
            oParamRequest["@EmployeeID"] = EmployeeID;
            oParamRequest["@RequestNo"] = RequestNo;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<FBEnrollment>(oSqlManage, oParamRequest));
            if (oResult.Count > 0)
            {
                result = true;
            }

            return result;
        }

        public override List<FBConfiguration> FB_ConfigurationGet(string Constant)
        {
            List<FBConfiguration> oResult = new List<FBConfiguration>();
            oSqlManage["ProcedureName"] = "sp_FB_ConfigurationGet";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@Constant"] = Constant;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<FBConfiguration>(oSqlManage, oParamRequest));

            return oResult;
        }

        public override void FB_EnrollmentSave(FBEnrollment oFBEnrollment)
        {
            oSqlManage["ProcedureName"] = "sp_FB_EnrollmentSave";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@EmployeeID"] = oFBEnrollment.EmployeeID;
            oParamRequest["@RequestNo"] = oFBEnrollment.RequestNo;
            oParamRequest["@BeginDate"] = oFBEnrollment.BeginDate;
            oParamRequest["@EndDate"] = oFBEnrollment.EndDate;
            oParamRequest["@FlexPoints"] = oFBEnrollment.FlexPoints;
            oParamRequest["@status"] = oFBEnrollment.Status;

            DatabaseHelper.ExecuteNoneQuery(oSqlManage, oParamRequest);
        }

        public override void FB_EnrollmentSelectedSave(FBEnrollmentSelected oFBEnrollmentSelected)
        {
            oSqlManage["ProcedureName"] = "sp_FB_EnrollmentSelectedSave";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@RequestNo"] = oFBEnrollmentSelected.RequestNo;
            oParamRequest["@SelectionItemID"] = oFBEnrollmentSelected.SelectionItemID;
            oParamRequest["@isMainBenefit"] = oFBEnrollmentSelected.isMainBenefit;

            DatabaseHelper.ExecuteNoneQuery(oSqlManage, oParamRequest);
        }

        public override void FB_EnrollmentSelectedDelete(string RequestNo)
        {
            oSqlManage["ProcedureName"] = "sp_FB_EnrollmentSelectedDelete";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@RequestNo"] = RequestNo;

            DatabaseHelper.ExecuteNoneQuery(oSqlManage, oParamRequest);
        }


        public override List<FBFSAList> FB_FSAListGet()
        {
            List<FBFSAList> oResult = new List<FBFSAList>();
            oSqlManage["ProcedureName"] = "sp_FB_FSAListGet";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oResult.AddRange(DatabaseHelper.ExecuteQuery<FBFSAList>(oSqlManage, oParamRequest));

            return oResult;
        }


        public override List<FBFSASubList> FB_FSASubListGet()
        {
            List<FBFSASubList> oResult = new List<FBFSASubList>();
            oSqlManage["ProcedureName"] = "sp_FB_FSASubListGet";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oResult.AddRange(DatabaseHelper.ExecuteQuery<FBFSASubList>(oSqlManage, oParamRequest));

            return oResult;
        }

        public override void FB_ReimburseSave(FBReimburse oFBReimburse)
        {
            oSqlManage["ProcedureName"] = "sp_FB_ReimburseSave";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();

            oParamRequest["@EmployeeID"] = oFBReimburse.EmployeeID;
            oParamRequest["@RequestNo"] = oFBReimburse.RequestNo;
            oParamRequest["@Amount"] = oFBReimburse.Amount;
            oParamRequest["@Vat"] = oFBReimburse.Vat;
            oParamRequest["@AmountReimburse"] = oFBReimburse.AmountReimburse;
            oParamRequest["@ReceiptNo"] = oFBReimburse.ReceiptNo;
            oParamRequest["@ReceiptDate"] = oFBReimburse.ReceiptDate;
            oParamRequest["@ReceiptCompany"] = oFBReimburse.ReceiptCompany;
            oParamRequest["@ReceiptAddress"] = oFBReimburse.ReceiptAddress;
            oParamRequest["@ReceiptPostcode"] = oFBReimburse.ReceiptPostcode;
            oParamRequest["@ReceiptTaxID"] = oFBReimburse.ReceiptTaxID;
            oParamRequest["@FSAList"] = oFBReimburse.FSAList;
            oParamRequest["@FSASubList"] = oFBReimburse.FSASubList;
            oParamRequest["@ReimburseStatus"] = oFBReimburse.ReimburseStatus;
            oParamRequest["@CreateBy"] = oFBReimburse.CreateBy;

            DatabaseHelper.ExecuteNoneQuery(oSqlManage, oParamRequest);
        }

        public override List<FBEnrollment> FB_EnrollmentGet(string EmployeeID, string Year)
        {
            List<FBEnrollment> oResult = new List<FBEnrollment>();
            oSqlManage["ProcedureName"] = "sp_FB_EnrollmentGet";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@EmployeeID"] = EmployeeID;
            oParamRequest["@Year"] = Year;
            oResult.AddRange(DatabaseHelper.ExecuteQuery<FBEnrollment>(oSqlManage, oParamRequest));

            return oResult;
        }

        public override decimal FB_ReimburseCheckQuota(string EmployeeID, string Year, string RequestNo)
        {
            oSqlManage["ProcedureName"] = "sp_FB_ReimburseCheckQuota";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@EmployeeID"] = EmployeeID;
            oParamRequest["@Year"] = Year;
            oParamRequest["@RequestNo"] = RequestNo;
            var oResult = DatabaseHelper.ExecuteScalar(oSqlManage, oParamRequest);
            var Result = Convert.ToDecimal(oResult);
            return Result;
        }

        public override List<ReportFBReimburseByAdmin> GetReportFBReimburseByAdmin(DateTime begin_date,DateTime end_date)
        {
            List<ReportFBReimburseByAdmin> list_data = new List<ReportFBReimburseByAdmin>();

            oSqlManage["ProcedureName"] = "sp_FB_GetReportReimburseByAdmin";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_BeginDate"] = begin_date;
            oParamRequest["@p_EndDate"] = end_date;
            var oResult = DatabaseHelper.ExecuteQuery<ReportFBReimburseByAdmin>(oSqlManage, oParamRequest);
            list_data.AddRange(oResult);
            return list_data;
        }

        public override DataTable GetTrackingRegisterFlexBenReport(string sYear, string EmpID, string OrgUnitList, string CompanyCode, string LanguageCode)
        {
            DataSet ds = new DataSet();
            oSqlManage["ProcedureName"] = "sp_GetRegisterStatusFlexBenReport"; 
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@p_Year"] = sYear;
            oParamRequest["@p_RequestorID"] = EmpID;
            oParamRequest["@p_OrgUnitList"] = OrgUnitList;
            oParamRequest["@p_CompanyCode"] = CompanyCode;
            oParamRequest["@p_LanguageCode"] = LanguageCode;
            ds = DatabaseHelper.ExecuteQueryToDataSet(oSqlManage, oParamRequest);
            return ds.Tables[0];
        }

        #region yun-20210904

        public override void UploadSizeChart(string path, string sImagesChart)
        {
            if (!string.IsNullOrEmpty(sImagesChart))
            {
                var spl = sImagesChart.Split('/')[1];
                var format = spl.Split(';')[0];
                string stringconvert = sImagesChart.Replace($"data:image/" + format + ";base64,", String.Empty);
                string fileName = "UniformSizeChart_" + DateTime.Now.Year + ".jpeg";


                string directoryPath = path;
                if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }

                if (File.Exists(directoryPath + "/" + fileName))
                {
                    File.Delete(directoryPath + "/" + fileName);
                }

                File.WriteAllBytes(directoryPath + "/" + fileName, Convert.FromBase64String(stringconvert));
            }
        }


        public override List<FBUniFormType> GetUniFormType()
        {
            List<FBUniFormType> ListData = new List<FBUniFormType>();
            oSqlManage["ProcedureName"] = "sp_FBUniformTypeGet";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            var oResult = DatabaseHelper.ExecuteQuery<FBUniFormType>(oSqlManage, oParamRequest);
            DataSet ds = new DataSet();
            ds = DatabaseHelper.ExecuteQueryToDataSet(oSqlManage, oParamRequest);

            try
            {
                //ListData = ds.Tables[0].ToList<FBUniFormType>();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        FBUniFormType d = new FBUniFormType();
                        d.UniFormTypeID = Convert.ToInt16(ds.Tables[0].Rows[i]["UniFormTypeID"].ToString());
                        d.UniFormTypeName = ds.Tables[0].Rows[i]["UniFormTypeName"].ToString();
                        d.Status = ds.Tables[0].Rows[i]["Status"].ToString();
                        d.Description = ds.Tables[0].Rows[i]["Description"].ToString();
                        d.StartDate = ds.Tables[0].Rows[i]["StartDate"].ToString();
                        d.EndDate = ds.Tables[0].Rows[i]["EndDate"].ToString();
                        ListData.Add(d);
                    }
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            //ListData.AddRange(oResult);
            return ListData;
        }

        public override List<FBUniForm> GetUniForm(string path)
        {
            List<FBUniForm> ListData = new List<FBUniForm>();
            oSqlManage["ProcedureName"] = "sp_FBUniformGet";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            //var oResult = DatabaseHelper.ExecuteQuery<FBUniForm>(oSqlManage, oParamRequest);
            DataSet ds = new DataSet();
            ds = DatabaseHelper.ExecuteQueryToDataSet(oSqlManage, oParamRequest);
            //ListData = ds.Tables[0].ToList<FBUniForm>();
            try
            {
                //ListData = ds.Tables[0].ToList<FBUniFormType>();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        FBUniForm d = new FBUniForm();
                        d.UniFormID = Convert.ToInt16(ds.Tables[0].Rows[i]["UniFormID"].ToString());
                        d.UniFormTypeID = Convert.ToInt16(ds.Tables[0].Rows[i]["UniFormTypeID"].ToString());
                        d.UniFormName = ds.Tables[0].Rows[i]["UniFormName"].ToString();

                        d.Status = ds.Tables[0].Rows[i]["Status"].ToString();
                        d.Description = ds.Tables[0].Rows[i]["Description"].ToString();
                        d.StartDate = ds.Tables[0].Rows[i]["StartDate"].ToString();
                        d.EndDate = ds.Tables[0].Rows[i]["EndDate"].ToString();

                        string fileName = ds.Tables[0].Rows[i]["Images"].ToString(); ;
                        string directoryPath = path;

                        if (File.Exists(directoryPath + "/" + fileName))
                        {
                            Byte[] bytes = File.ReadAllBytes(directoryPath + "/" + fileName);

                            d.Images = "data:image/jpeg;base64," + Convert.ToBase64String(bytes);
                        }
                        else
                        {
                            d.Images = "";
                        }

                        ListData.Add(d);
                    }
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            return ListData;
        }


        public override List<FBUniFormSize> GetUniFormSize()
        {
            List<FBUniFormSize> ListData = new List<FBUniFormSize>();
            oSqlManage["ProcedureName"] = "sp_FBUniformSizeGet";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            //var oResult = DatabaseHelper.ExecuteQuery<FBUniFormSize>(oSqlManage, oParamRequest);
            DataSet ds = new DataSet();
            ds = DatabaseHelper.ExecuteQueryToDataSet(oSqlManage, oParamRequest);
            //ListData = ds.Tables[0].ToList<FBUniFormSize>();
            try
            {
                //ListData = ds.Tables[0].ToList<FBUniFormType>();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        FBUniFormSize d = new FBUniFormSize();
                        d.SizeID = Convert.ToInt16(ds.Tables[0].Rows[i]["SizeID"].ToString());
                        d.UniFormID = Convert.ToInt16(ds.Tables[0].Rows[i]["UniFormID"].ToString());
                        d.UniFormTypeID = Convert.ToInt16(ds.Tables[0].Rows[i]["UniFormTypeID"].ToString());
                        d.Size = ds.Tables[0].Rows[i]["Size"].ToString();
                        d.CWLength = (decimal)ds.Tables[0].Rows[i]["CWLength"];
                        d.Length = (decimal)ds.Tables[0].Rows[i]["Length"];
                        d.Images = ds.Tables[0].Rows[i]["Images"].ToString();
                        d.Status = ds.Tables[0].Rows[i]["Status"].ToString();
                        d.StartDate = ds.Tables[0].Rows[i]["StartDate"].ToString();
                        d.EndDate = ds.Tables[0].Rows[i]["EndDate"].ToString();
                        ListData.Add(d);
                    }
                }
            }
            catch (Exception err)
            {
                throw err;
            }
            return ListData;
        }

        public override void UniFormTypeSave(FBUniFormType oFBUniForm)
        {
            oSqlManage["ProcedureName"] = "sp_FB_UniFormTypeSave";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            oParamRequest["@UniFormTypeID"] = oFBUniForm.UniFormTypeID;
            oParamRequest["@UniFormTypeName"] = oFBUniForm.UniFormTypeName;
            oParamRequest["@Status"] = oFBUniForm.Status;
            oParamRequest["@Description"] = oFBUniForm.Description;
            oParamRequest["@StartDate"] = oFBUniForm.StartDate;
            oParamRequest["@EndDate"] = oFBUniForm.EndDate;
            oParamRequest["@CreateBy"] = oFBUniForm.CreateBy;
            oParamRequest["@UpdateBy"] = oFBUniForm.UpdateBy;

            DatabaseHelper.ExecuteNoneQuery(oSqlManage, oParamRequest);
        }

        public override void UniFormSave(FBUniForm oFBUniForm, string path)
        {
            oSqlManage["ProcedureName"] = "sp_FB_UniFormSave";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();
            try
            {

                oParamRequest["@UniFormID"] = oFBUniForm.UniFormID;
                oParamRequest["@UniFormName"] = oFBUniForm.UniFormName;
                oParamRequest["@UniFormTypeID"] = oFBUniForm.UniFormTypeID;
                oParamRequest["@Status"] = oFBUniForm.Status;
                oParamRequest["@Description"] = oFBUniForm.Description;
                oParamRequest["@StartDate"] = oFBUniForm.StartDate;
                oParamRequest["@EndDate"] = oFBUniForm.EndDate;
                oParamRequest["@CreateBy"] = oFBUniForm.CreateBy;
                oParamRequest["@UpdateBy"] = oFBUniForm.UpdateBy;

                if (!string.IsNullOrEmpty(oFBUniForm.Images))
                {
                    var spl = oFBUniForm.Images.Split('/')[1];
                    var format = spl.Split(';')[0];
                    string stringconvert = oFBUniForm.Images.Replace($"data:image/" + format + ";base64,", String.Empty);
                    string fileName = "Uniform_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".jpeg";
                    oParamRequest["@Images"] = fileName;

                    string directoryPath = path;
                    if (!Directory.Exists(directoryPath))
                    {
                        Directory.CreateDirectory(directoryPath);
                    }

                    if (File.Exists(directoryPath + "/" + fileName))
                    {
                        File.Delete(directoryPath + "/" + fileName);
                    }

                    File.WriteAllBytes(directoryPath + "/" + fileName, Convert.FromBase64String(stringconvert));
                }
                else
                {
                    oParamRequest["@Images"] = "";
                }

                DatabaseHelper.ExecuteNoneQuery(oSqlManage, oParamRequest);
            }
            catch (Exception err)
            {
                throw err;
            }
        }

        public override void UniFormSizeSave(FBUniFormSize oFBUniFormSize)
        {
            oSqlManage["ProcedureName"] = "sp_FB_UniFormSizeSave";
            Dictionary<string, object> oParamRequest = new Dictionary<string, object>();

            oParamRequest["@SizeID"] = oFBUniFormSize.SizeID;
            oParamRequest["@UniFormID"] = oFBUniFormSize.UniFormID;
            //oParamRequest["@UniFormTypeID"] = oFBUniFormSize.UniFormTypeID;
            oParamRequest["@CWLength"] = oFBUniFormSize.CWLength;
            oParamRequest["@Length"] = oFBUniFormSize.Length;
            oParamRequest["@Size"] = oFBUniFormSize.Size;
            oParamRequest["@Status"] = oFBUniFormSize.Status;
            oParamRequest["@StartDate"] = oFBUniFormSize.StartDate;
            oParamRequest["@EndDate"] = oFBUniFormSize.EndDate;
            oParamRequest["@CreateBy"] = oFBUniFormSize.CreateBy;
            oParamRequest["@UpdateBy"] = oFBUniFormSize.UpdateBy;

            DatabaseHelper.ExecuteNoneQuery(oSqlManage, oParamRequest);
        }
        #endregion

    }
}
