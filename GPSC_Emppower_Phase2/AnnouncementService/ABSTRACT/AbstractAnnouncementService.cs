#region Using

using System;
using System.Collections.Generic;
using System.Data;
using ESS.ANNOUNCEMENT.DATACLASS;
using ESS.ANNOUNCEMENT.INTERFACE;

#endregion Using

namespace ESS.ANNOUNCEMENT.ABSTRACT
{
    public class AbstractAnnouncementService : IAnnouncementService
    {
        #region IAnnouncementInterface Members

        public virtual List<AnnouncementData> GetAnnoncementList()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<AnnouncementGroupByAdmin> GetGroupAnnouncement(int groupId)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<AnnouncementStatus> GetAnnouncementStatus()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveAnnouncementTextByAdmin(AnnouncementTextByAdmin model)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<DisplayAnnouncementTextByAdmin> GetAllAnnouncementTextByAdmin()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual EditAnnouncementTextByAdmin GetAnnouncementInKeyTextByAdmin(int AnnouncementId)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void DeleteAnnouncementTextByAdmin(int AnnouncementId)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void UpdateSequenceAnnouncementTextByAdmin(List<DataEditSequenceAnnouncementTextByAdmin> model)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void EditDataAnnouncementTextByAdmin(EditAnnouncementTextByAdmin model)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveAnnouncementPublicRelations(AnnouncementDataAll model)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<AnnouncementDataAll> GetAllAnnouncementPublicRelations()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void EditSequenceAnnouncementPublicRelations(List<DataEditSequenceAnnouncementPublicRelations> model)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void DeleteAnnouncementPublicRelations(int AnnouncementId)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        
        public virtual AnnouncementDataAll GetAnnouncementPublicRelationsById(int AnnouncementId)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void EditAnnouncementPublicRelations(AnnouncementPublicRelations model)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveAnnouncementMain(AnnouncementDataAll model)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual PaginationAnnouncementMain GetAnnouncementsMainAllByAdmin(int page,int itemPerPage)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        
        public virtual void DeleteAnnouncementsMainAllByAdmin(int announcementId)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DataAnnouncementMainEdit GetAnnouncementMainById(int announcementId)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void EditAnnouncementMain(AnnouncementDataAll model)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveAnnouncementEmployeeWelfareByAdmin(AnnouncementDataAll model)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual  List<AnnouncementDataAll> GetAllAnnouncementWelfareByAdmin()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void DeleteAnnouncementWelfareByAdmin(int announcementId)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void EditSequenceAnnouncementWelfareByAdmin(List<DataEditSequenceAnnouncementPublicRelations> model)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DataAnnouncementEmployeeWelfare GetAnnouncementWelfareById(int announcementId)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void EditAnnouncementEmployeeWelfareByAdmin(AnnouncementDataAll model)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual PaginationAnnouncementMainNotifyAll GetPaginationAnnouncementMainNotify(int page,int itemPerPage, string language,string employeeId)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual AnnouncementNotifyDetail GetAnnouncementMainNotifyById(int announcementId, string language)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<DataAnnouncementNotifyAll> GetPaginationAnnouncementMainNotifyToBell(string language,int limitShowData,string employeeId)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual AlarmNotify CalculateNotifyAnnouncement(string employeeId)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveOrUpdateViewAnnouncementMain(string employeeId,int announcementId)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<ViewPublicRelations> GetViewPublicRelationsAll()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<ViewAnnouncementText> GetViewAnnouncementTextAllEmployee()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual int GetMaxGetMaxSequenceWelfareAnnouncement()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion
    }
}