#region Using

using System.Collections.Generic;
using System.Data;
using ESS.ANNOUNCEMENT.DATACLASS;

#endregion Using

namespace ESS.ANNOUNCEMENT.INTERFACE
{
    public interface IAnnouncementService
    {
        #region Member

        List<AnnouncementData> GetAnnoncementList();
        List<AnnouncementGroupByAdmin> GetGroupAnnouncement(int groupId);
        List<AnnouncementStatus> GetAnnouncementStatus();
        void SaveAnnouncementTextByAdmin(AnnouncementTextByAdmin model);
        List<DisplayAnnouncementTextByAdmin> GetAllAnnouncementTextByAdmin();
        EditAnnouncementTextByAdmin GetAnnouncementInKeyTextByAdmin(int AnnouncementId);
        void DeleteAnnouncementTextByAdmin(int AnnouncementId);
        void UpdateSequenceAnnouncementTextByAdmin(List<DataEditSequenceAnnouncementTextByAdmin> model);
        void EditDataAnnouncementTextByAdmin(EditAnnouncementTextByAdmin model);
        void SaveAnnouncementPublicRelations(AnnouncementDataAll model);
        List<AnnouncementDataAll> GetAllAnnouncementPublicRelations();
        void EditSequenceAnnouncementPublicRelations(List<DataEditSequenceAnnouncementPublicRelations> model);
        void DeleteAnnouncementPublicRelations(int AnnouncementId);
        AnnouncementDataAll GetAnnouncementPublicRelationsById(int AnnouncementId);
        void EditAnnouncementPublicRelations(AnnouncementPublicRelations model);
        void SaveAnnouncementMain(AnnouncementDataAll model);
        PaginationAnnouncementMain GetAnnouncementsMainAllByAdmin(int page, int itemPerPage);
        void DeleteAnnouncementsMainAllByAdmin(int announcementId);
        DataAnnouncementMainEdit GetAnnouncementMainById(int announcementId);
        void EditAnnouncementMain(AnnouncementDataAll model);
        void SaveAnnouncementEmployeeWelfareByAdmin(AnnouncementDataAll model);
        List<AnnouncementDataAll> GetAllAnnouncementWelfareByAdmin();
        void DeleteAnnouncementWelfareByAdmin(int announcementId);
        void EditSequenceAnnouncementWelfareByAdmin(List<DataEditSequenceAnnouncementPublicRelations> model);
        DataAnnouncementEmployeeWelfare GetAnnouncementWelfareById(int announcementId);
        void EditAnnouncementEmployeeWelfareByAdmin(AnnouncementDataAll model);
        PaginationAnnouncementMainNotifyAll GetPaginationAnnouncementMainNotify(int page, int itemPerPage, string language,string employeeId);
        AnnouncementNotifyDetail GetAnnouncementMainNotifyById(int announcementId, string language);
        List<DataAnnouncementNotifyAll> GetPaginationAnnouncementMainNotifyToBell(string language, int limitShowData,string employeeId);
        AlarmNotify CalculateNotifyAnnouncement(string employeeId);
        void SaveOrUpdateViewAnnouncementMain(string employeeId, int announcementId);
        List<ViewPublicRelations> GetViewPublicRelationsAll();
        List<ViewAnnouncementText> GetViewAnnouncementTextAllEmployee();
        int GetMaxGetMaxSequenceWelfareAnnouncement();
        #endregion Member
    }
}