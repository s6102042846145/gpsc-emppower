using System;
using System.Collections.Generic;
using System.Text;
using ESS.DATA;
using System.Data;
using ESS.ANNOUNCEMENT.DATACLASS;

namespace ESS.ANNOUNCEMENT
{
    public class AbstractAnnouncementService : IAnnouncementService
    {

        #region IAnnouncementInterface Members

        public virtual List<AnnouncementData> GetAnnoncementList()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DataTable GetData()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void Update(AnnouncementData data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void Insert(AnnouncementData data)
        {
            throw new Exception("The method or operation is not implemented.");
        }


        public virtual void Delete(int id)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual AnnouncementData AnnouncementGetByID(int id)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<AnnouncementData> AnnouncementGetAll()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<AnnouncementData> AnnouncementGetActive()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DataTable AnnouncementCheckDupplicate(AnnouncementData data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void AnnouncementSave(AnnouncementData data)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        #endregion
    }
}
