#region Using
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using ESS.ANNOUNCEMENT.DATACLASS;
using ESS.SHAREDATASERVICE;

#endregion Using

namespace ESS.ANNOUNCEMENT
{
    public class AnnouncementManagement
    {
        #region Constructor
        private AnnouncementManagement()
        {

        }
        #endregion 

        #region MultiCompany  Framework
        private static string ModuleID = "ESS.ANNOUNCEMENT";

        public string CompanyCode { get; set; }

        public static AnnouncementManagement CreateInstance(string oCompanyCode)
        {
            AnnouncementManagement oAnnouncementManagement = new AnnouncementManagement()
            {
                CompanyCode = oCompanyCode
            };

            return oAnnouncementManagement;
        }


        #endregion MultiCompany  Framework

        #region Properties
        public CultureInfo oCultureInfo 
        { 
            get 
            { 
                return CultureInfo.GetCultureInfo(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "CultureInfo")); 
            } 
        }
        public string SqlConnectionString
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BaseConStr");
            }
        }

        public int BellShowDataANNOUNCEMENT
        {
            get
            {
                return Convert.ToInt16(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BellShowDataANNOUNCEMENT"));
            }
        }

        #endregion Properties

        #region Function
        public List<AnnouncementData> GetAnnoncementList()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetAnnoncementList();
        }
        #endregion Function

        #region Announcement By Admin

        public List<AnnouncementGroupByAdmin> GetGroupAnnoncementList(int groupId)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetGroupAnnouncement(groupId);
        }

        public List<AnnouncementStatus> GetListAnnouncementStatus()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetAnnouncementStatus();
        }

        public void SaveAnnouncementTextByAdmin(AnnouncementTextByAdmin model)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveAnnouncementTextByAdmin(model);
        }
        
        public List<DisplayAnnouncementTextByAdmin> GetAllAnnouncementTextByAdmin()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetAllAnnouncementTextByAdmin();
        }

        public EditAnnouncementTextByAdmin GetEditAnnouncementTextByAdmin(int AnnouncementId)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetAnnouncementInKeyTextByAdmin(AnnouncementId);
        }

        public void DeleteAnnouncementTextByAdmin(int AnnouncementId)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.DeleteAnnouncementTextByAdmin(AnnouncementId);
        }

        public void UpdateSequenceAnnouncementTextByAdmin(List<DataEditSequenceAnnouncementTextByAdmin> model)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.UpdateSequenceAnnouncementTextByAdmin(model);
        }

        public void EditDataAnnouncementTextByAdmin(EditAnnouncementTextByAdmin model)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.EditDataAnnouncementTextByAdmin(model);
        }

        public void SaveAnnouncementPublicRelations(AnnouncementDataAll model)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveAnnouncementPublicRelations(model);
        }

        public List<AnnouncementDataAll> GetAllAnnouncementPublicRelations()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetAllAnnouncementPublicRelations();
        }

        public void EditSequenceAnnouncementPublicRelations(List<DataEditSequenceAnnouncementPublicRelations> model)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.EditSequenceAnnouncementPublicRelations(model);
        }

        public void DeleteAnnouncementPublicRelations(int AnnouncementId)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.DeleteAnnouncementPublicRelations(AnnouncementId);
        }

        public AnnouncementDataAll GetAnnouncementPublicRelationsById(int AnnouncementId)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetAnnouncementPublicRelationsById(AnnouncementId);
        }

        public void EditAnnouncementPublicRelations(AnnouncementPublicRelations model)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.EditAnnouncementPublicRelations(model);
        }

        public void SaveAnnouncementMainByAdmin(AnnouncementDataAll model)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveAnnouncementMain(model);
        }

        public PaginationAnnouncementMain GetPaginationAnnouncementMain(int page, int itemPerPage)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetAnnouncementsMainAllByAdmin(page, itemPerPage);
        }

        public void DeleteAnnouncementMainByAdmin(int announcementId)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.DeleteAnnouncementsMainAllByAdmin(announcementId);
        }

        public DataAnnouncementMainEdit GetAnnouncementMainById(int announcementId)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetAnnouncementMainById(announcementId);
        }

        public void EditAnnouncementMainById(AnnouncementDataAll model)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.EditAnnouncementMain(model);
        }

        public void SaveAnnouncementEmployeeWelfareByAdmin(AnnouncementDataAll model)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveAnnouncementEmployeeWelfareByAdmin(model);
        }

        public List<AnnouncementDataAll> GetAllAnnouncementWelfareByAdmin()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetAllAnnouncementWelfareByAdmin();
        }

        public void DeleteAnnouncementWelfareByAdmin(int announcementId)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.DeleteAnnouncementWelfareByAdmin(announcementId);
        }

        public void EditSequenceAnnouncementWelfareByAdmin(List<DataEditSequenceAnnouncementPublicRelations> model)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.EditSequenceAnnouncementWelfareByAdmin(model);
        }

        public DataAnnouncementEmployeeWelfare GetAnnouncementWelfareById(int announcementId)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetAnnouncementWelfareById(announcementId);
        }

        public void EditAnnouncementEmployeeWelfareByAdmin(AnnouncementDataAll model)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.EditAnnouncementEmployeeWelfareByAdmin(model);
        }

        public PaginationAnnouncementMainNotifyAll GetPaginationAnnouncementMainNotify(int page,int itemPerPage,string language,string employeeId)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetPaginationAnnouncementMainNotify(page, itemPerPage, language, employeeId);
        }

        public AnnouncementNotifyDetail GetAnnouncementMainNotifyDetailById(int announcementId, string language)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetAnnouncementMainNotifyById(announcementId, language);
        }

        public List<DataAnnouncementNotifyAll> GetPaginationAnnouncementMainNotifyToBell(string language,string employeeId)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetPaginationAnnouncementMainNotifyToBell(language, BellShowDataANNOUNCEMENT, employeeId);
        }

        public AlarmNotify CalculateNotifyAnnouncement(string employeeId)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.CalculateNotifyAnnouncement(employeeId);
        }

        public void SaveOrUpdateViewAnnouncementMain(string employeeId, int announcementId)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveOrUpdateViewAnnouncementMain(employeeId, announcementId);
        }

        public HomeAnnouncementDisplayEmployee GetDataAllAnnouncementDisplayHome()
        {
            var homeAnnouncement = new HomeAnnouncementDisplayEmployee();
            // 1. ��С�Ȼ�Ъ�����ѹ������Ѻ��ѡ�ҹ��
            List<ViewPublicRelations> list_PublicRelations = ServiceManager.CreateInstance(CompanyCode).ESSData.GetViewPublicRelationsAll();

            // 2. ��С�Ȣ�ͤ�������Ѻ��ѡ�ҹ��
            List<ViewAnnouncementText> list_announcementText = ServiceManager.CreateInstance(CompanyCode).ESSData.GetViewAnnouncementTextAllEmployee();

            homeAnnouncement.ListPublicRelations = list_PublicRelations;
            homeAnnouncement.ListAnnouncementText = list_announcementText;
            return homeAnnouncement;
        }

        public int GetMaxGetMaxSequenceWelfareAnnouncement()
        {
            int sequence = 0;
            sequence = ServiceManager.CreateInstance(CompanyCode).ESSData.GetMaxGetMaxSequenceWelfareAnnouncement();
            return sequence;
        }

        #endregion
    }
}