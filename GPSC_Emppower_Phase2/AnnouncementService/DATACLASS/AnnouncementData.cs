using System;
using System.Collections.Generic;
using System.ComponentModel;
using ESS.UTILITY.EXTENSION;
namespace ESS.ANNOUNCEMENT.DATACLASS
{
    public class AnnouncementData : AbstractObject
    {
        #region Constructor

        public AnnouncementData()
        {
            Type oPropertyType = null;
            foreach (PropertyDescriptor oCurrentProperty in TypeDescriptor.GetProperties(this))
            {
                oPropertyType = oCurrentProperty.PropertyType;
                if (oPropertyType.Name.ToLower() == "string")
                {
                    oCurrentProperty.SetValue(this, "");
                }
                else if (oPropertyType.Name.ToLower() == "boolean")
                {
                    oCurrentProperty.SetValue(this, false);
                }
                else if (oPropertyType.Name.IndexOf("int", StringComparison.CurrentCultureIgnoreCase) >= 0)
                {
                    oCurrentProperty.SetValue(this, 0);
                }
                else if (oPropertyType.Name.ToLower() == "decimal")
                {
                    oCurrentProperty.SetValue(this, Convert.ToDecimal(0.00));
                }
                else if (oPropertyType.Name.ToLower() == "double")
                {
                    oCurrentProperty.SetValue(this, 0.0);
                }
                else if (oPropertyType.Name.ToLower() == "datetime")
                {
                    oCurrentProperty.SetValue(this, DateTime.MinValue);
                }
            }
        }

        #endregion Constructor

        #region Properties

        public int ID { get; set; }

        public string Name { get; set; }

        public string Content { get; set; }

        public string Image { get; set; }

        public string NewImage { get; set; }

        public int Priority { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public DateTime NewFlagDate { get; set; }

        public string Type { get; set; }

        public string RequestNo { get; set; }

        #endregion Properties
    }

    public class AnnouncementGroupByAdmin : AbstractObject
    {
        public int SubjectReportGroupID { get; set; }
        public int SubjectReportSubID { get; set; }
        public string SubjectCode { get; set; }
    }

    public class ClientAnnouncementGroupByAdmin
    {
        public ClientAnnouncementGroupByAdmin()
        {
            LeftReport = new List<AnnouncementGroupByAdmin>();
            RightReport = new List<AnnouncementGroupByAdmin>();
        }
        public List<AnnouncementGroupByAdmin> LeftReport { get; set; }
        public List<AnnouncementGroupByAdmin> RightReport { get; set; }
    }

    public class AnnouncementStatus : AbstractObject
    {
        public int AnnouncementStatusId { get; set; }
        public string Name { get; set; }
    }

    public class AnnouncementTextByAdmin
    {
        public int AnnouncementTypeId { get; set; }
        public int AnnouncementStatusId { get; set; }
        public string Subject_TH { get; set; }
        public string HtmlEditor_TH { get; set; }
        public string Subject_EN { get; set; }
        public string HtmlEditor_EN { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime ExpireDate { get; set; }
        public bool IsUnlimitTime { get; set; }
        public int Sequence { get; set; }
        public string CreateBy { get; set; }
    }

    public class DisplayAnnouncementTextByAdmin : AbstractObject
    {
        public bool IsChecked { get; set; }
        public int AnnouncementId { get; set; }
        public int AnnouncementTypeId { get; set; }
        public int AnnouncementStatusId { get; set; }
        public int Sequence { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime ExpireDate { get; set; }
        public bool IsUnlimitedExpire { get; set; }
        public string Title_TH { get; set; }
        public string Title_EN { get; set; }
        public string Description_TH { get; set; }
        public string Description_EN { get; set; }
        public string Remark_TH { get; set; }
        public string Remark_EN { get; set; }
    }

    public class EditAnnouncementTextByAdmin : AbstractObject
    {
        public int AnnouncementId { get; set; }
        public int AnnouncementTypeId { get; set; }
        public int AnnouncementStatusId { get; set; }
        public int Sequence { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime ExpireDate { get; set; }
        public bool IsUnlimitedExpire { get; set; }
        public string Title_TH { get; set; }
        public string Title_EN { get; set; }
        public string Description_TH { get; set; }
        public string Description_EN { get; set; }
        public string Remark_TH { get; set; }
        public string Remark_EN { get; set; }
        public string UpdateBy { get; set; }
    }

    public class DataEditSequenceAnnouncementTextByAdmin
    {
        public int AnnouncementId { get; set; }
        public int Sequence { get; set; }
    }

    public class AnnouncementDataAll : AbstractObject
    {
        public AnnouncementDataAll()
        {
            ListFile = new List<AnnouncementFile>();
        }
        public bool IsChecked { get; set; }
        public int AnnouncementId { get; set; }
        public int AnnouncementTypeId { get; set; }
        public int AnnouncementStatusId { get; set; }
        public int Sequence { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime ExpireDate { get; set; }
        public bool IsUnlimitedExpire { get; set; }
        public string Title_TH { get; set; }
        public string Title_EN { get; set; }
        public string Description_TH { get; set; }
        public string Description_EN { get; set; }
        public string Remark_TH { get; set; }
        public string Remark_EN { get; set; }
        public string TextLink { get; set; }
        public bool IsDelete { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public int File_TH_Id { get; set; }
        public string File_TH { get; set; }
        public int File_EN_Id { get; set; }
        public string File_EN { get; set; }
        public List<AnnouncementFile> ListFile { get; set; }
    }

    public class AnnouncementFile : AbstractObject
    {
        public int AnnouncementFileId { get; set; }
        public int AnnouncementId { get; set; }
        public string TextFile { get; set; }
        public string Language { get; set; }
        public int Sequence { get; set; }
    }

    public class DataEditSequenceAnnouncementPublicRelations : DataEditSequenceAnnouncementTextByAdmin
    {

    }

    public class AnnouncementPublicRelations
    {
        public int AnnouncementId { get; set; }
        public int AnnouncementTypeId { get; set; }
        public int AnnouncementStatusId { get; set; }
        public int Sequence { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime ExpireDate { get; set; }
        public bool IsUnlimitedExpire { get; set; }
        public string Title_TH { get; set; }
        public string Title_EN { get; set; }
        public string TextLink { get; set; }
        public string UpdateBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public int AnnouncementFileId { get; set; }
        public string TextFile { get; set; }
        public int File_TH_Id { get; set; }
        public string File_TH { get; set; }
        public int File_EN_Id { get; set; }
        public string File_EN { get; set; }
    }

    public class PaginationAnnouncementMain
    {
        public PaginationAnnouncementMain()
        {
            List_AnnouncementMain = new List<AnnouncementDataAll>();
        }
        public int Total { get; set; }
        public int ItemPerPage { get; set; }
        public int Page { get; set; }
        public List<AnnouncementDataAll> List_AnnouncementMain { get; set; }
    }

    public class DataAnnouncementMainEdit
    {
        public DataAnnouncementMainEdit()
        {
            ListPicture_TH = new List<DataFileAnnouncementMain>();
            ListPicture_EN = new List<DataFileAnnouncementMain>();
        }

        public int AnnouncementId { get; set; }
        public int AnnouncementStatusId { get; set; }
        public string Title_TH { get; set; }
        public string Description_TH { get; set; }
        public string Remark_TH { get; set; }
        public string Title_EN { get; set; }
        public string Description_EN { get; set; }
        public string Remark_EN { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime ExpireDate { get; set; }
        public bool IsUnlimitedExpire { get; set; }
        public List<DataFileAnnouncementMain> ListPicture_TH { get; set; }
        public List<DataFileAnnouncementMain> ListPicture_EN { get; set; }
    }

    public class DataFileAnnouncementMain
    {
        public int AnnouncementFileId { get; set; }
        public string  FileName { get; set; }
        public int FileSequence { get; set; }
        public string Language { get; set; }
    }

    public class DataAnnouncementEmployeeWelfare
    {
        public DataAnnouncementEmployeeWelfare()
        {
            ListPicture_TH = new List<DataFileAnnouncementEmployeeWelfare>();
            ListPicture_EN = new List<DataFileAnnouncementEmployeeWelfare>();
        }

        public int AnnouncementId { get; set; }
        public int AnnouncementStatusId { get; set; }
        public string Title_TH { get; set; }
        public string Description_TH { get; set; }
        public string Remark_TH { get; set; }
        public string Title_EN { get; set; }
        public string Description_EN { get; set; }
        public string Remark_EN { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime ExpireDate { get; set; }
        public bool IsUnlimitedExpire { get; set; }
        public int Sequence { get; set; }
        public List<DataFileAnnouncementEmployeeWelfare> ListPicture_TH { get; set; }
        public List<DataFileAnnouncementEmployeeWelfare> ListPicture_EN { get; set; }
    }

    public class DataFileAnnouncementEmployeeWelfare
    {
        public int AnnouncementFileId { get; set; }
        public string FileName { get; set; }
        public int FileSequence { get; set; }
        public string Language { get; set; }
    }


    public class PaginationAnnouncementMainNotifyAll
    {
        public PaginationAnnouncementMainNotifyAll()
        {
            List_AnnouncementMain = new List<DataAnnouncementNotifyAll>();
        }
        public int Total { get; set; }
        public int ItemPerPage { get; set; }
        public int Page { get; set; }
        public List<DataAnnouncementNotifyAll> List_AnnouncementMain { get; set; }
    }

    public class DataAnnouncementNotifyAll : AbstractObject
    {
        public int AnnouncementId { get; set; }
        public DateTime EffectiveDate { get; set; }
        public string Title_TH { get; set; }
        public string Title_EN { get; set; }
        public string Description_TH { get; set; }
        public string Description_EN { get; set; }
        public string TextFile { get; set; }
        public bool IsRead { get; set; }
    }

    public class AnnouncementNotifyDetail : AbstractObject
    {
        public AnnouncementNotifyDetail()
        {
            TextFile = new List<string>();
        }
        public bool IsHaveAnnouncement { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime ExpireDate { get; set; }
        public string Title_TH { get; set; }
        public string Title_EN { get; set; }
        public string Description_TH { get; set; }
        public string Description_EN { get; set; }
        public List<string> TextFile { get; set; }
        public DateTime CreateDate { get; set; }
        public string Remark_TH { get; set; }
        public string Remark_EN { get; set; }
    }

    public class AlarmNotify
    {
        public int Notify { get; set; }
    }

    public class LogViewAnnouncement : AbstractObject
    {
        public int AnnouncementViewId { get; set; }
        public int AnnouncementId { get; set; }
        public string EmployeeId { get; set; }
        public DateTime ViewDate { get; set; }
        public string Category { get; set; }
    }

    public class ViewPublicRelations : AbstractObject
    {
        public int AnnouncementId { get; set; }
        public string Title_TH { get; set; }
        public string Title_EN { get; set; }
        public string TextLink { get; set; }
        public string TextFile_TH { get; set; }
        public string TextFile_EN { get; set; }
        public DateTime EffectiveDate { get; set; }
    }

    public class ViewAnnouncementText : AbstractObject
    {
        public int AnnouncementId { get; set; }
        public int Sequence { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime ExpireDate { get; set; }
        public string Title_TH { get; set; }
        public string Title_EN { get; set; }
        public string Description_TH { get; set; }
        public string Description_EN { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
    }

    public class HomeAnnouncementDisplayEmployee
    {
        public HomeAnnouncementDisplayEmployee()
        {
            ListPublicRelations = new List<ViewPublicRelations>();
            ListAnnouncementText = new List<ViewAnnouncementText>();
        }
        public List<ViewPublicRelations> ListPublicRelations { get; set; }
        public List<ViewAnnouncementText> ListAnnouncementText { get; set; }
    }
}