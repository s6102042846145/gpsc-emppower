using System.Collections.Generic;

namespace ESS.HR.PA.CONFIG
{
    public class NameFormat
    {
        public NameFormat()
        {
        }
        public string Key{ get; set; }
        public string Description
        {
            get
            {
                string cReturn = "";
                foreach (string item in Fields)
                {
                    cReturn += "," + item;
                }
                return cReturn.TrimStart(',');
            }
        }
        public List<string> Fields { get; set; } = new List<string>();
    }
}