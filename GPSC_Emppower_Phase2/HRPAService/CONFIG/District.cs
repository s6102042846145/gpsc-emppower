﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PA.CONFIG
{
    public class District : AbstractObject
    {
        public District()
        {

        }
        public string CountryCode { get; set; } 
        public string ProvinceCode { get; set; } 
        public string DistrictCode { get; set; } 
        public string DistrictName { get; set; }
    }
}
