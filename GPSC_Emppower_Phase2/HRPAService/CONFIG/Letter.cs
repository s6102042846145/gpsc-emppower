using System;
using System.Collections.Generic;
using System.Text;
using ESS.DATA;

namespace ESS.HR
{
    public class Letter : AbstractObject
    {
        public Letter()
        {
        }
        public int LetterID{ get; set; }
        public int LetterTypeID{ get; set; }
        public string Requestor{ get; set; }
        public string LanguageCode{ get; set; }
        public string Status{ get; set; }
        public string RequestNo{ get; set; }
        public DateTime RequestDate{ get; set; }
        public DateTime CloseJobDate{ get; set; }
    }

    public class LetterData : AbstractObject
    {
        public LetterData()
        { 
        }
        public int LetterID{ get; set; }
        public int FieldID{ get; set; }
        public string FieldDataText{ get; set; }
        public DateTime FieldDataDate{ get; set; }
    }
}
