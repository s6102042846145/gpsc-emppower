using System.Collections.Generic;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA.CONFIG
{
    public class EduLevelCert : AbstractObject
    {
		public EduLevelCert()
		{
			
		}
        public string EducationLevelCode{ get; set; }
        public string CertificateCode{ get; set; }
    }
}