using System.Collections.Generic;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA.CONFIG
{
    public class CompanyBusiness : AbstractObject
    {
        public CompanyBusiness()
        {
        }

        public string BusinessCode { get; set; }

        public string BusinessText { get; set; }
    }
}