using System;

namespace ESS.HR.PA.CONFIG
{
    public class FieldRequiredException : Exception
    {
		public string DocTypeCode{ get; set; }
        public string FieldCode{ get; set; }
        public FieldRequiredException(string oDocTypeCode, string oFieldCode)
        {
            DocTypeCode = oDocTypeCode;
            FieldCode = oFieldCode;
        }

    }
}