using System.Collections.Generic;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA.CONFIG
{
    public class JobType : AbstractObject
    {
        public JobType()
        {
        }
        public string JobCode { get; set; }
        public string JobText { get; set; }

    }
}