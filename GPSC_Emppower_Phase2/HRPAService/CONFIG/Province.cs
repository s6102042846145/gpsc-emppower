using System.Collections.Generic;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA.CONFIG
{
    public class Province : AbstractObject
    {
        public string CountryCode{ get; set; }
        public string ProvinceCode{ get; set; }
        public string ProvinceName{ get; set; }
        public override int GetHashCode()
        {
            string cCode = string.Format("PROVINCE_{0}", ProvinceCode);
            return cCode.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return this.GetHashCode() == obj.GetHashCode();
        }
    }
}