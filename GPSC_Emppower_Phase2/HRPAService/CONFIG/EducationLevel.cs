using System.Collections.Generic;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA.CONFIG
{
    public class EducationLevel : AbstractObject
    {
        public EducationLevel()
        {
        }
        public string EducationLevelCode { get; set; }
        public string EducationLevelText { get; set; }
        public bool IsActive { get; set; }

    }
}