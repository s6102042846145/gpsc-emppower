using System.Collections.Generic;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA.CONFIG
{
    public class FamilyMember : AbstractObject
    {
        public FamilyMember()
        {
        }
        public string Key{ get; set; }
        public string Description{ get; set; }
        public override int GetHashCode()
        {
            string cCode = string.Format("FAMILYMEMBER_{0}", Key);
            return cCode.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return this.GetHashCode() == obj.GetHashCode();
        }

    }
}