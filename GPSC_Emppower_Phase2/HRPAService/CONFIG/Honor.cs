using System.Collections.Generic;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA.CONFIG
{
    public class Honor : AbstractObject
    {
        public Honor()
        {
        }
        public string HonorCode { get; set; }
        public string HonorText { get; set; }
    }
}