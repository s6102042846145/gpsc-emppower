﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PA.CONFIG
{
    public class Argument
    {
        private string __Text;
        private string __Value;

        public string text
        {
            get { return __Text; }
            set { __Text = value; }
        }
        public string Value
        {
            get { return __Value; }
            set { __Value = value; }
        }
    }
}
