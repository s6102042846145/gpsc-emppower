using System.Collections.Generic;

namespace ESS.HR.PA.CONFIG
{
    public class MaritalStatus
    {
        public MaritalStatus()
        {
        }

        public string Key{ get; set; }
        public string Description{ get; set; }
        public override int GetHashCode()
        {
            string cCode = string.Format("MARITALSTATUS_{0}", Key);
            return cCode.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return this.GetHashCode() == obj.GetHashCode();
        }
    }
}