using System.Collections.Generic;

namespace ESS.HR.PA.CONFIG
{
    public class CountrySortingName : IComparer<Country>
    {
        private CharCompare oCompare;

        public CountrySortingName()
        {
            oCompare = new CharCompare();
        }

        #region IComparer<Country> Members

        public int Compare(Country x, Country y)
        {
            int result = 0;
            for (int i = 0; i < (x.CountryName.Length < y.CountryName.Length ? x.CountryName.Length : y.CountryName.Length); i++)
            {
                result = oCompare.Compare(x.CountryName.ToCharArray()[i], y.CountryName.ToCharArray()[i]);
                if (result == 1 || result == -1)
                    break;
            }
            return result;
        }

        #endregion IComparer<Country> Members
    }
}