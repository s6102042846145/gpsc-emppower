using System;
using System.Collections.Generic;
using System.Text;
using ESS.DATA;

namespace ESS.HR
{
    public class LetterType : AbstractObject
    {
        //private int __letterTypeID = -1;
       // private string __letterTypeCode = "";
       // private int __parentID = -1;
       // private int __level = -0;
       // private bool __hasChild = false;
       // private List<string> __languageList = new List<string>();
       // private List<LetterTypeField> __fieldList = new List<LetterTypeField>();
        public LetterType()
        { 
        }
        public int LetterTypeID{ get; set; }
        public string LetterTypeCode{ get; set; }
        public int ParentID{ get; set; }
        public int Level{ get; set; }
        public bool HasChild{ get; set; }
        public List<string> LanguageList{ get; set; }
        public List<LetterTypeField> FieldList{ get; set; }
    }

    public class LetterTypeField : AbstractObject
    {
        //private int __fieldID = -1;
        //private string __fieldCode = "";
        //private string __fieldType = "";

        public LetterTypeField()
        {
        }

        public int FieldID{ get; set; }
        public string FieldCode{ get; set; }
        public string FieldType{ get; set; }
    }

}
