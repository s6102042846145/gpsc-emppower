using System.Collections.Generic;

//CreatedBy: Ratchatawan W. (2012-02-17)
namespace ESS.HR.PA.CONFIG
{
    public class Nationality
    {
        public Nationality()
        {
        }

        public string Key{ get; set; }
        public string Name{ get; set; }
    }
}