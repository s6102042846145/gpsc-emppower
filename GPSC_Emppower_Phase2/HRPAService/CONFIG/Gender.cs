using System.Collections.Generic;

namespace ESS.HR.PA.CONFIG
{
    public class Gender
    {
        public Gender()
        {
        }
        public string Key{ get; set; }
        public string Description{ get; set; }
        public override int GetHashCode()
        {
            string cCode = string.Format("GENDER_{0}", Key);
            return cCode.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return this.GetHashCode() == obj.GetHashCode();
        }
    }
}