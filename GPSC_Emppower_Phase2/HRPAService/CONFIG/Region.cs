using System.Collections.Generic;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA.CONFIG
{
    public class Region : AbstractObject
    {
        public string CountryCode{ get; set; }
        public string RegionCode{ get; set; }
        public string RegionName{ get; set; }
        public override int GetHashCode()
        {
            string cCode = string.Format("REGION_{0}", RegionCode);
            return cCode.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return this.GetHashCode() == obj.GetHashCode();
        }
    }
}