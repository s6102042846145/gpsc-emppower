using System;
using System.Collections.Generic;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA.INFOTYPE
{
    public class INFOTYPE9004 : AbstractObject
    {
        //private string __employeeID = "";
        //private DateTime __beginDate = DateTime.MinValue;
        //private DateTime __endDate = DateTime.MinValue;
        //private string __level = "";

        public INFOTYPE9004()
        { }

        public string EmployeeID { get; set; }

        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }

        public string Level{ get; set; }

        //public static List<INFOTYPE9004> GetAllJobGrade(string EmployeeID)
        //{
        //    return HRPAManagement.GetAllJobGrade(EmployeeID);
        //}
    }
}