﻿using System;
using System.Collections.Generic;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA.INFOTYPE
{
    public class AppraisalData : AbstractObject
    {
        public AppraisalData()
        {
        }

        #region "Property"
        public string EmployeeId { get; set; }
        public Decimal IncreaseSalaryPercent { get; set; }
        public string AppraisalGrade { get; set; }
        public string Ranking { get; set; }
        public string Year { get; set; }
        #endregion
    }
}
