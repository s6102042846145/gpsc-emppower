using System;
using System.Collections.Generic;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA.INFOTYPE
{
    public class INFOTYPE9002 : AbstractObject
    {
        //private string __employeeID = "";
        //private DateTime __beginDate = DateTime.MinValue;
        //private DateTime __endDate = DateTime.MinValue;
        //private string __positionName = "";
        //private string __divisionName = "";
        //private string __departmentName = "";
        //private string __companyName = "";
        //private string __level = "";
        //private string _UNIT;

        //private string __roleID = "";
        //private string __roleName = "";
        //private string __projectID = "";
        //private string __projectName = "";
        //private string __projectManager = "";
        //private string __workTime = "";
        //private string __remark = "";

        public INFOTYPE9002()
        {
        }


        public string EmployeeID { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }
        public string PositionName { get; set; }
        public string DivisionName { get; set; }

        public string DepartmentName { get; set; }
        public string CompanyName { get; set; }
        public string Level { get; set; }

        public string UNIT { get; set; }

        public string ProjectID { get; set; }

        public string ProjectName { get; set; }

        public string RoleID { get; set; }

        public string RoleName { get; set; }

        public string ProjectManager { get; set; }

        public string WorkTime { get; set; }
        public string Remark { get; set; }
        //public static List<INFOTYPE9002> GetAllWorkHistory(string EmployeeID)
        //{
        //    return HRPAManagement.GetAllWorkHistory(EmployeeID);
        //}
    }
}