using System;
using System.Collections.Generic;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA.INFOTYPE
{
    public class INFOTYPE0008 : AbstractObject
    {
        public INFOTYPE0008()
        { 
        }

        public string EmployeeID { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal Salary { get; set; }
        //public static List<INFOTYPE0008> GetSalaryList(string EmployeeID)
        //{
        //    return HRPAManagement.GetSalaryList(EmployeeID);
        //}
    }
}