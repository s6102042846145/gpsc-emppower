﻿using System;
using System.Collections.Generic;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA.INFOTYPE
{
    public class INFOTYPE0348 : AbstractObject
    {
        //private string __employeeId = "";
        //private decimal __increaseSalaryPercent = 0.0M;
        //private decimal __appraisalScore = 0.0M;
        //private decimal __kpiScore = 0.0M;
        //private decimal __nonWorkingRelatedKPI = 0.0M;
        //private decimal __variableBonusRate = 0.0M;
        //private string __appraisalType = "";
        //private DateTime __beginDate = DateTime.MinValue;
        //private DateTime __endDate = DateTime.MinValue;


        public INFOTYPE0348()
        {
        }
       
        #region "Property"
        public string EmployeeId { get; set; }
        public decimal KPIScore { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public Decimal IncreaseSalaryPercent { get; set; }
        public Decimal VariableBonusRate { get; set; }
        public decimal AppraisalScore { get; set; }
        public decimal NonWorkingRelatedKPI { get; set; }
        public string AppraisalGrade { get; set; }
        public string AppraisalType { get; set; }
        public string Ranking { get; set; }
        #endregion
    }
}
