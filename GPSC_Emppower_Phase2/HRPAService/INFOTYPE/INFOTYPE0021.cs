using System;
using System.Collections.Generic;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.ABSTRACT;
using ESS.EMPLOYEE.INTERFACE;
using ESS.WORKFLOW;

namespace ESS.HR.PA.INFOTYPE
{
    public class INFOTYPE0021 : AbstractInfoType
    {
        //private string __remark = string.Empty;
        //private string __familyMember = string.Empty;
        //private string __childNo = string.Empty;
        //private string __titleName = string.Empty;
        //private string __titleRank = string.Empty;
        //private string __name = string.Empty;
        //private string __surname = string.Empty;
        //private DateTime __birthDate = DateTime.MinValue;
        //private string __sex = string.Empty;
        //private string __birthPlace = string.Empty;
        //private string __cityOfBirth = string.Empty;
        //private string __nationality = string.Empty;
        //private string __fatherID = string.Empty;
        //private string __motherID = string.Empty;
        //private string __spouseID = string.Empty;
        //private string __motherSpouseID = string.Empty;
        //private string __fatherSpouseID = string.Empty;
        //private string __jobTitle = string.Empty;
        //private string __dead = string.Empty;

        //Addby:Nuttapon.a 11.02.2013
        //Addition address of family data (infotype0187)
        //private string __address = string.Empty;

        //private string __street = string.Empty;
        //private string __district = string.Empty;
        //private string __city = string.Empty;
        //private string __postcode = string.Empty;
        //private string __country = string.Empty;
        //private string __telephoneNo = string.Empty;

        public INFOTYPE0021()
        {
        }

        public override string InfoType
        {
            get { return "0021"; }
        }
        public Boolean Ischeck { get; set; }
        public string FamilyMember { get; set; }
        public string ChildNo { get; set; }
        public string TitleName { get; set; }

        public string TitleRank { get; set; }
        public string Name { get; set; }

        public string Surname { get; set; }

        public DateTime BirthDate { get; set; }

        public string Sex { get; set; }

        public string BirthPlace { get; set; }
        public string CityOfBirth { get; set; }
        public string Nationality { get; set; }

        public string FatherID { get; set; }
        public string MotherID { get; set; }
        public string SpouseID { get; set; }
        public string MotherSpouseID { get; set; }
        public string FatherSpouseID { get; set; }
        public string JobTitle { get; set; }

        public string Employer { get; set; }
        //AddBy: Ratchatawan W. (2011-10-14)
        public string Dead { get; set; }
        public string Remark { get; set; }

        public string Address { get; set; }

        public string Street { get; set; }
        public string District { get; set; }

        public string City { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string TelephoneNo { get; set; }

        public int Age { get; set; }

        public static List<INFOTYPE0021> GetData(EmployeeData Requestor)
        {
            List<INFOTYPE0021> oReturn = ServiceManager.CreateInstance(Requestor.OrgAssignment.CompanyCode).ERPData.GetFamilyData(Requestor.EmployeeID);

            //Addby:Nuttapon.a 12.03.2013  PTT requirement ONLY
            //Fillter subtype 6 (����Ѻ�Ż���ª��) �͡���������� user maintain ��ҹ�к��ͧ�ع���ͧ
            // 2015-10-16 : filter subtype = 7 ��� subtype = 11 ���੾�СѺ module ����Ѻ�Ż��⪹��Сѹ���Ե/�غѵ��˵�

            oReturn = oReturn.FindAll(
                delegate (INFOTYPE0021 temp)
                {
                    return temp.FamilyMember != ServiceManager.CreateInstance(Requestor.OrgAssignment.CompanyCode).ProvidentFundHeir && temp.FamilyMember != ServiceManager.CreateInstance(Requestor.OrgAssignment.CompanyCode).Surety
                        && temp.FamilyMember != ServiceManager.CreateInstance(Requestor.OrgAssignment.CompanyCode).LifeInsuranceHeir && temp.FamilyMember != ServiceManager.CreateInstance(Requestor.OrgAssignment.CompanyCode).AccidentInsuranceHeir;
                });
            return oReturn;
        }

        public static List<INFOTYPE0021> GetProvidentMemberData(EmployeeData Requestor)
        {
            List<INFOTYPE0021> oReturn = ServiceManager.CreateInstance(Requestor.OrgAssignment.CompanyCode).ERPData.GetFamilyData(Requestor.EmployeeID);

            //Fillter ���੾�� subtype 6 (����Ѻ�Ż���ª��) 
            oReturn = oReturn.FindAll(delegate (INFOTYPE0021 temp) { return temp.FamilyMember == "6"; });
            return oReturn;
        }

        public override List<IInfoType> GetList(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            List<IInfoType> data = new List<IInfoType>();
            data.AddRange(ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).ERPData.GetFamilyData(EmployeeID1).ToArray());
            return data;
        }

        public override void SaveToSAP()
        {
            ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).ERPData.SaveFamilyData(this);
        }
    }
}