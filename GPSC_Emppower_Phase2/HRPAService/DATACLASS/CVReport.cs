﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.EMPLOYEE.CONFIG.PA;

namespace ESS.HR.PA.DATACLASS
{
    public class CVReport : AbstractObject
    {
       public string Remark { get; set; }
    }

    public class CVPersonal : AbstractObject
    {
        public CVPersonal()
        {
            Salary = new List<SALARYRATE>();
            AssessmentHistory = new List<INFOTYPE0348SAP>();
        }

        public bool IsPincodeValid { get; set; }
        public bool IsExportPptx { get; set; }
        public object Personal { get; set; }
        public object Education { get; set; }
        public PaginationWorkingPeriod Working { get; set; }
        public List<SALARYRATE> Salary { get; set; }
        public List<INFOTYPE0348SAP> AssessmentHistory { get; set; }
    }
    
    public class INFOTYPE0348SAP
    {
        public string EmployeeId { get; set; }
        public Decimal IncreaseSalaryPercent { get; set; }
        public string AppraisalGrade { get; set; }
        public string Ranking { get; set; }
        public string Year { get; set; }
    }

    public class DbPeriodSettingPaySlipByAdmin : AbstractObject
    {
        public int PeriodID { get; set; }
        public bool IsOffCycle { get; set; }
        public int PeriodYear { get; set; }
        public int PeriodMonth { get; set; }
        public DateTime OffCycleDate { get; set; }
        public bool Active { get; set; }
        public DateTime EffectiveDate { get; set; }
        public bool IsDraft { get; set; }
        public bool IsEvaluationHistory { get; set; }
    }

    public class DbPaySlipByEmployee : AbstractObject
    {
        public string EmployeeID { get; set; }
        public int PeriodYear { get; set; }
        public int PeriodMonth { get; set; }
        public char OffCycleFlag { get; set; }
        public DateTime PYDate { get; set; }
        public string CurrencyAmount { get; set; }
    }

}
