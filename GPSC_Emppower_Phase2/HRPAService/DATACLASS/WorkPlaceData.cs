using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA.DATACLASS
{
    public class WorkPlaceData : AbstractObject
    {
        private string __workPlaceCode = "";

        public string WorkPlaceCode
        {
            get
            {
                return __workPlaceCode;
            }
            set
            {
                __workPlaceCode = value;
            }
        }
    }
}