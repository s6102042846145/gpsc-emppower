﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PA.DATACLASS
{
    public class EmployeeWorklocationSAP
    {
        public string EmployeeId { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime EndDate { get; set; }
        public string CompanyId { get; set; }
        public string LocationId { get; set; }
    }

    public class MasterWorkLocationSAP
    {
        public DateTime EffectiveDate { get; set; }
        public DateTime EndDate { get; set; }
        public string LocationName { get; set; }
        public string LocationId { get; set; }
    }
}
