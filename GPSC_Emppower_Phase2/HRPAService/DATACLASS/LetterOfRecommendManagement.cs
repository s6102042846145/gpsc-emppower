using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.HR.PA
{
    public class LetterOfRecommendManagement
    {
        public static List<LetterType> GetAllLetterTypes()
        {
            Dictionary<int, LetterType> __item = new Dictionary<int, LetterType>();
            Dictionary<int, List<LetterType>> __child = new Dictionary<int, List<LetterType>>();
            List<LetterType> res = new List<LetterType>();
            foreach (LetterType oType in ServiceManager.HRPAConfig.GetAllLetterTypes())
            {
                if (oType.ParentID == -1)
                {
                    __item.Add(oType.LetterTypeID, oType);
                }
                else
                {
                    if (!__child.ContainsKey(oType.ParentID))
                    {
                        __child.Add(oType.ParentID, new List<LetterType>());
                    }
                    __child[oType.ParentID].Add(oType);
                }
            }
            foreach (LetterType oType in __item.Values)
            {
                FillUp(res, __child, oType, 0);
            }
            return res;
        }

        private static void FillUp(List<LetterType> res, Dictionary<int, List<LetterType>> __child, LetterType oType, int Level)
        {
            oType.Level = Level;
            res.Add(oType);
            if (__child.ContainsKey(oType.LetterTypeID))
            {
                oType.HasChild = true;
                foreach (LetterType sub in __child[oType.LetterTypeID])
                {
                    FillUp(res, __child, sub, Level + 1);
                }
            }
            else
            {
                oType.HasChild = false;
            }
        }

        public static LetterType GetLetterType(int LetterTypeID)
        {
            return ServiceManager.HRPAConfig.GetLetterType(LetterTypeID);
        }
    }
}
