using ESS.DATA;

namespace ESS.HR.PA.DATACLASS
{
    public class INFOTYPE0002Filter
    {
        private FilterData __dobFilter = null;

        public INFOTYPE0002Filter()
        {
        }

        public FilterData DOBFilter
        {
            get
            {
                if (__dobFilter == null)
                {
                    __dobFilter = new FilterData();
                    __dobFilter.FilterName = "INFOTYPE0002.DOB";
                }
                return __dobFilter;
            }
        }
    }
}