﻿using ESS.HR.PA.INFOTYPE;
using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PA.DATACLASS
{
    public class ContactInfo : AbstractObject
    {
        private CommunicationData __contData = new CommunicationData();
        public CommunicationData ContData
        {
            get
            {
                return __contData;
            }
            set
            {
                __contData = value;
            }
        }

        private CommunicationData __contDataNew = new CommunicationData();
        public CommunicationData ContDataNew
        {
            get
            {
                return __contDataNew;
            }
            set
            {
                __contDataNew = value;
            }
        }
        
    }
}
