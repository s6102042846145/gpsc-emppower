﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;
using ESS.EMPLOYEE.CONFIG.PA;

namespace ESS.HR.PA.DATACLASS
{
    public class InfotypeAutomaticManagement : AbstractObject
    {
        private string __infMngID;
        private string __infotypeObject;
        private string __infotypeClass;
        private string __infotypeAssembly;
        private string __infotypeSubtype;
        private string __infotypeField;
        private string __infotypeValue;
        private string __operation;
        private string __effectInfotypeObject;
        private string __effectInfotypeClass;
        private string __effectInfotypeAssembly;
        private string __effectInfotypeSubtype;
        private string __effectInfotypeField;
        private string __effectInfotypeValue;
        private string __effectInfotypeValueType;


        public string InfMngID
        {
            get
            {
                return __infMngID;
            }
            set
            {
                __infMngID = value;
            }
        }

        public string InfotypeObject
        {
            get
            {
                return __infotypeObject;
            }
            set
            {
                __infotypeObject = value;
            }
        }
        public string InfotypeClass
        {
            get
            {
                return __infotypeClass;
            }
            set
            {
                __infotypeClass = value;
            }
        }
        public string InfotypeAssembly
        {
            get
            {
                return __infotypeAssembly;
            }
            set
            {
                __infotypeAssembly = value;
            }
        }

        public string InfotypeSubtype
        {
            get
            {
                return __infotypeSubtype;
            }
            set
            {
                __infotypeSubtype = value;
            }
        }

        public string InfotypeField
        {
            get
            {
                return __infotypeField;
            }
            set
            {
                __infotypeField = value;
            }
        }

        public string InfotypeValue
        {
            get
            {
                return __infotypeValue;
            }
            set
            {
                __infotypeValue = value;
            }
        }

        public string Operation
        {
            get
            {
                return __operation;
            }
            set
            {
                __operation = value;
            }
        }

        public string EffectInfotypeObject
        {
            get
            {
                return __effectInfotypeObject;
            }
            set
            {
                __effectInfotypeObject = value;
            }
        }
        public string EffectInfotypeClass
        {
            get
            {
                return __effectInfotypeClass;
            }
            set
            {
                __effectInfotypeClass = value;
            }
        }
        public string EffectInfotypeAssembly
        {
            get
            {
                return __effectInfotypeAssembly;
            }
            set
            {
                __effectInfotypeAssembly = value;
            }
        }

        public string EffectInfotypeSubtype
        {
            get
            {
                return __effectInfotypeSubtype;
            }
            set
            {
                __effectInfotypeSubtype = value;
            }
        }

        public string EffectInfotypeField
        {
            get
            {
                return __effectInfotypeField;
            }
            set
            {
                __effectInfotypeField = value;
            }
        }

        public string EffectInfotypeValue
        {
            get
            {
                return __effectInfotypeValue;
            }
            set
            {
                __effectInfotypeValue = value;
            }
        }

        public string EffectInfotypeValueType
        {
            get
            {
                return __effectInfotypeValueType;
            }
            set
            {
                __effectInfotypeValueType = value;
            }
        }

        //public List<InfotypeAutomaticManagement> GetInfotypeAutomaticData(string InfotypeObject, string InfotypeSubtype, string InfotypeField, string InfotypeValue)
        //{
        //    return HR.PA.ServiceManager.HRPABuffer.GetInfotypeAutomaticData(InfotypeObject, InfotypeSubtype, InfotypeField, InfotypeValue);
        //}
        //public static string GetPersonalID(string employeeID)
        //{
        //    List<INFOTYPE0185> lsInfotype0185 = INFOTYPE0185.GetInfotype0185ByEmpID(employeeID);

        //    return lsInfotype0185[0].CardID.Trim().Replace("-", "");
        //}
    }
}
