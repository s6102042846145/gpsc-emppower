﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA.DATACLASS
{
    public class SALARYRATE : AbstractObject
    {
        private string __employeeID;
        private decimal __minimum;
        private decimal __average;
        private decimal __maximum;

        public SALARYRATE()
        {
        }

        public string EmployeeID
        {
            get
            {
                return __employeeID;
            }
            set
            {
                __employeeID = value;
            }
        }
        public decimal Minimum
        {
            get
            {
                return __minimum;
            }
            set
            {
                __minimum = value;
            }
        }
        public decimal Average
        {
            get
            {
                return __average;
            }
            set
            {
                __average = value;
            }
        }
        public decimal Maximum
        {
            get
            {
                return __maximum;
            }
            set
            {
                __maximum = value;
            }
        }
        public decimal Salary { get; set; }
        public decimal PercentageSalary { get; set; }
    }
}
