﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA.DATACLASS
{
    public class PersonalDriverLicenseData : AbstractObject
    {
        public int ItemID { get; set; }
        public string LicenseID { get; set; }
        public string EmployeeID { get; set; }
        public string FullNameTH { get; set; }
        public string FullNameEN { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime ExpireDate { get; set; }
    }

    public class PaginationPersonalDriverLicenseData
    {
        public PaginationPersonalDriverLicenseData()
        {
            ListPersonalDriverLicenseData = new List<PersonalDriverLicenseData>();
            Page = 1;
            ItemPerPage = 20;
        }
        public int Total { get; set; }
        public int Page { get; set; }
        public int ItemPerPage { get; set; }
        public List<PersonalDriverLicenseData> ListPersonalDriverLicenseData { get; set; }
    }
}
