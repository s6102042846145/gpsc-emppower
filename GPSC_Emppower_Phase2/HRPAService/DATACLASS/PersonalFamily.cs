﻿using ESS.HR.PA.INFOTYPE;
using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PA.DATACLASS
{
    public class PersonalFamily : AbstractObject
    {
        private INFOTYPE0021 __familyData = new INFOTYPE0021();
        public INFOTYPE0021 FamilyData
        {
            get
            {
                return __familyData;
            }
            set
            {
                __familyData = value;
            }
        }

        private INFOTYPE0021 __familyData_OLD = new INFOTYPE0021();
        public INFOTYPE0021 FamilyData_OLD
        {
            get
            {
                return __familyData_OLD;
            }
            set
            {
                __familyData_OLD = value;
            }
        }
        
    }
}
