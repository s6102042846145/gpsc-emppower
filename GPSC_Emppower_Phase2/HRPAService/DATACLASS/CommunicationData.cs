using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA.DATACLASS
{
    public class CommunicationData : AbstractObject
    {
        private string _Remark;
        private string __telephoneNo = "";
        private string __workPlace = "";
        private string __employeeID = "";
        private string __mobilePhone = "";
        private string __mobileNotPublic = "";
        private string __email = "";
        private string __homePhone = "";

        public string TelephoneNo
        {
            get
            {
                if (string.IsNullOrEmpty(__telephoneNo) || __telephoneNo.Trim() == "")
                {
                    return "-";
                }
                return __telephoneNo;
            }
            set
            {
                __telephoneNo = value;
            }
        }

        public string WorkPlace
        {
            get
            {
                if (string.IsNullOrEmpty(__workPlace) || __workPlace.Trim() == "")
                {
                    return "-";
                }
                return __workPlace;
            }
            set
            {
                __workPlace = value;
            }
        }

        public string EmployeeID
        {
            get
            {
                return __employeeID;
            }
            set
            {
                __employeeID = value;
            }
        }

        public string MobilePhone
        {
            get
            {
                if (string.IsNullOrEmpty(__mobilePhone) || __mobilePhone.Trim() == "")
                {
                    return "-";
                }
                return __mobilePhone;
            }
            set
            {
                __mobilePhone = value;
            }
        }

        public string MobileNotPublic
        {
            get
            {
                if (string.IsNullOrEmpty(__mobileNotPublic) || __mobileNotPublic.Trim() == "")
                {
                    return "-";
                }
                return __mobileNotPublic;
            }
            set
            {
                __mobileNotPublic = value;
            }
        }

        public string Email
        {
            get
            {
                if (string.IsNullOrEmpty(__email) || __email.Trim() == "")
                {
                    return "-";
                }
                return __email;
            }
            set
            {
                __email = value;
            }
        }

        public string HomePhone
        {
            get
            {
                if (string.IsNullOrEmpty(__homePhone) || __homePhone.Trim() == "")
                {
                    return "-";
                }
                return __homePhone;
            }
            set
            {
                __homePhone = value;
            }
        }

        public string Remark
        {
            get { return _Remark; }
            set
            {
                _Remark = value;
            }
        }
    }
}