﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PA.DATACLASS
{
    public class PowerpointProfileModel
    {
        public class EducationProfile
        {
            public string Level { get; set; } /// ระดับการศึกษา
            public string Grade { get; set; } ///วุฒิการศึกษา
            public string Field { get; set; } /// สาขาวิชาเอก
            public string Institute { get; set; } /// สถาบันการศึกษา
            public int Year { get; set; } /// ปีที่จบ
            public string Country { get; set; } /// ประเทศที่จบ
        }
        public class EmployementProfile
        {
            public DateTime Start { get; set; }/// วันที่เริ่มต้น
            public DateTime? End { get; set; }/// วันที่สิ้นสุด
            public String Position { get; set; }/// ตำแหน่ง
            public String Unit { get; set; }/// หน่วยงาน
            public String Company { get; set; }/// บริษัท
            public String AgeWorkTime { get; set; }
        }
        public class PerformanceProfile
        {
            public int Year { get; set; }/// ปีประเมิน
            public String Result { get; set; } /// ผลการประเมิน
            public String Ranking { get; set; } /// Ranking
        }
        public string Title { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public DateTime EmployeeDate { get; set; } /// -> อายุงานในบริษัทต้นสังกัด
        public DateTime PttPositionDate { get; set; } /// -> อายุงานในกลุ่ม ปตท.
        public int RetireYear { get; set; } /// ปีเกษียณ
        public DateTime RetireDate { get; set; } /// ปีเกษียณ
        public string JobGrade { get; set; } /// ระดับงาน
        public DateTime JobGradeDate { get; set; } /// -> วันที่ระดับงาน
        public DateTime Birthdate { get; set; } /// -> อายุตัว
        public String Company { get; set; } /// บริษัทต้นสังกัด
        public String Position { get; set; } /// ตำแหน่ง
        public String Unit { get; set; } /// หน่วยงาน
        public string ImagePath { get; set; }
        public DateTime AsOfDate { get; set; }
        public string ageOfCompany { get; set; }
        public string ageOfLevel { get; set; }
        public string agePersonal { get; set; }

        public List<EducationProfile> Educations { get; set; } = new List<EducationProfile>();
        public List<EmployementProfile> Employements { get; set; } = new List<EmployementProfile>();
        public List<PerformanceProfile> Performances { get; set; } = new List<PerformanceProfile>();

    }
}
