﻿using ESS.DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PA.DATACLASS
{
    public class Col
    {
        public string id { get; set; }
        public string label { get; set; }
        public string type { get; set; }
    }

    public class C
    {
        public string v { get; set; }
    }

    public class Row
    {
        public Row()
        {
            c = new List<C>();
        }
        public List<C> c { get; set; }

    }

    public class Table
    {
        public Table()
        {
            cols = new List<Col>();
            rows = new List<Row>();
        }

        public List<Col> cols { get; set; }
        public List<Row> rows { get; set; }
        public int parsedNumHeaders { get; set; }

    }

    public class DriverLicense
    {
        public string version { get; set; }
        public string reqId { get; set; }
        public string status { get; set; }
        public string sig { get; set; }
        public Table table { get; set; }
    }

    public class DataDriverLicenseByJob : AbstractObject
    {
        public string LicenseID { get; set; }
        public string EmployeeID { get; set; }
        public string FullNameTH { get; set; }
        public string FullNameEN { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime ExpireDate { get; set; }
    }

}
