using System;
using System.Collections.Generic;
using ESS.DATA;
using ESS.HR.PA.DATACLASS;
using ESS.HR.PA.INFOTYPE;
using ESS.HR.PA.INTERFACE;

namespace ESS.HR.PA.ABSTRACT
{
    public abstract class AbstractHRPADataService : IHRPADataService
    {
        #region IHRPAService Members

        public abstract INFOTYPE0002 GetPersonalData(string EmployeeID, DateTime CheckDate);

        public INFOTYPE0002 GetPersonalData(string EmployeeID)
        {
            return GetPersonalData(EmployeeID, DateTime.Now);
        }

        public virtual void MarkUpdate(string EmployeeID, string DataCategory, string RequestNo, bool isMarkIn)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual bool CheckMark(string EmployeeID, string DataCategory)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual bool CheckMark(string EmployeeID, string DataCategory, string ReqNo)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SavePersonalData(INFOTYPE0002 data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0002> GetInfotype0002List(string EmployeeID1, string EmployeeID2)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveInfotype0002(string EmployeeID1, string EmployeeID2, List<INFOTYPE0002> List, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public List<INFOTYPE0006> GetPersonalAddress(string EmployeeID)
        {
            return GetPersonalAddress(EmployeeID, DateTime.Now);
        }

        public virtual List<INFOTYPE0006> GetPersonalAddress(string EmployeeID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SavePersonalAddress(List<INFOTYPE0006> data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public INFOTYPE0009 GetBankDetail(string EmployeeID)
        {
            return GetBankDetail(EmployeeID, DateTime.Now);
        }

        public virtual INFOTYPE0009 GetBankDetail(string EmployeeID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveBankDetail(INFOTYPE0009 data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0021> GetFamilyData(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveFamilyData(INFOTYPE0021 data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveFamilyData(List<INFOTYPE0021> data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE0021 GetFamilyData(string EmployeeID, string FamilyMember, string ChildNo)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0022> GetAllEducationHistory(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0023> GetAllPreviousEmployers(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SearchInfotype0002(FilterData filter)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE9002> GetAllWorkHistory(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE9001> GetAllSalaryHistory(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0008> GetSalaryList(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0008> GetSalaryList(string EmployeeID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE9003> GetAllEvaluation(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE9004> GetAllJobGrade(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0022> GetAllCertificateHistory(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void DeleteEducation(List<INFOTYPE0022> data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveEducation(List<INFOTYPE0022> data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void DeleteFamilyData(List<INFOTYPE0021> data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE0008 GetInfotype0008(string EmployeeID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<WorkPlaceData> GetWorkPlace()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveWorkPlace(List<WorkPlaceData> list)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual CommunicationData GetCommunicationData(string employeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveCommunicationData(CommunicationData savedData, CommunicationData oldData)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<WorkPeriod> GetWorkHistory(string EmployeeID,string CompCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual RelatedAge GetRelatedAge(string EmployeeID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<SALARYRATE> GetSalaryRate(string EmployeeID, string EmpSubGroup)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<SALARYRATE> GetSalaryRate(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0105> GetInfotype0105ByCategoryCode(string EmployeeID, string CategoryCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<PersonalAttachmentFileSet> GetAttachmentFileSet(string EmployeeID, int RequestTypeID, string RequestSubType)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<PersonalAttachmentFileSet> GetAttachmentExport(string EmployeeID, int RequestTypeID, string RequestSubType)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual void FileAttachmentSubTypeSave(string RequestNo, string RequestSubType)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual void UpdateINFOTYPE0105(INFOTYPE0105 oINF105)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual void InsertINFOTYPE0105(INFOTYPE0105 oINF105)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual void SaveINFOTYPE0105(List<INFOTYPE0105> oListINF105Update, List<INFOTYPE0105> oListINF105Insert)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void DeleteInsertEducation(List<INFOTYPE0022> data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<string> GetIDCardData(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void ChangeFamilyPrefixByAge(string FamilyDataSubtype, List<string> SourceKey, List<string> MappingKey, int AgetoUpdate, string EmployeeID1, string EmployeeID2)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<InfotypeAutomaticManagement> GetAllInfotypeAutomaticList()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DateTime? GetMaritalEffectiveDate(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual int CalculateChildNo(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion IHRPAService Members


        #region Driver License ������㺢Ѻ���

        public virtual List<PersonalDriverLicenseData> GetDriverLicenseByEmployee(string EmployeeCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<PersonalDriverLicenseData> GetDriverLicenseNotifyExpiredByMonth(int iMonth, int iYear)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<DataDriverLicenseByJob> CheckDuplicateDriverLicense(List<DataDriverLicenseByJob> EmployeeCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveDriverLicenseByJob(List<DataDriverLicenseByJob> model)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<PersonalDriverLicenseData> GetDriverLicenseEmployeeAll()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual int GetRequestByBoxId(int boxId,string employeeId,string positionId)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion


        public virtual List<EmployeeWorklocationSAP> LoadEmployeeInWorkLocation(string CompCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }


        public virtual List<MasterWorkLocationSAP> LoadWorkLocationMaster(string CompCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<AppraisalData> GetAppraisalData(string EmployeeID, int yearOffset, int chkYear, string CompCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveOrUpdateEmployeeInWorkLocation(List<EmployeeWorklocationSAP> model)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveOrUpdateWorkLocationMaster(List<MasterWorkLocationSAP> model)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DbPeriodSettingPaySlipByAdmin GetConfigPeroidSettingPayslipByAdmin()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual decimal GetSalaryByEmployee(string employeeId)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual string GetWorkLocationNameByEmpID(string EmployeeID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<DateTime> GetStartWorkAge(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual string GetMailSubjectNotifyExpiredDriverLicense(int MailID, string Language)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<DbPeriodSettingPaySlipByAdmin> GetAllPayslipPeriodSetting()
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }
}