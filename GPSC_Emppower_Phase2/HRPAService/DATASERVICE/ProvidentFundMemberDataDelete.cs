using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ESS.EMPLOYEE;
using ESS.DATA;
using ESS.WORKFLOW;

namespace ESS.HR.DATASERVICE
{
    class ProvidentFundMemberDataDelete : AbstractDataService
    {
        public override DataSet GenerateAdditionalData(EmployeeData Requestor)
        {
            DataSet DS = new DataSet();
            DataTable oTable;
            INFOTYPE0021 classTemplate = new INFOTYPE0021();
            oTable = classTemplate.ToADODataTable(true);
            oTable.TableName = "INFOTYPE0021";
            DS.Tables.Add(oTable);

            oTable = new DataTable("FILECOUNT");
            oTable.Columns.Add("FILECOUNT", typeof(int));
            DS.Tables.Add(oTable);
            return DS;
        }

        public override void PrepareData(EmployeeData Requestor, System.Data.DataSet Data)
        {

        }

        public override void ValidateData(DataSet newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate)
        {

        }
        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, DataSet Data, string State, string RequestNo)
        {
            if (State == "COMPLETED")
            {
                DataTable oData = Data.Tables["INFOTYPE0021"];
                List<INFOTYPE0021> saved = new List<INFOTYPE0021>();
                INFOTYPE0021 item = new INFOTYPE0021();
                item.ParseToObject(oData.Rows[0]);
                //delimit records
                item.EndDate = DateTime.Now.AddDays(-1);
                saved.Add(item);
                try
                {
                    HR.PA.ServiceManager.HRPAService.DeleteFamilyData(saved);
                }
                catch (Exception ex)
                {
                    throw new SaveExternalDataException("SaveExternalData Error", true, ex);
                }
                finally
                {
                    if (Data.Tables.Contains("POSTDATA"))
                        Data.Tables.Remove("POSTDATA");
                    INFOTYPE0021 item1 = new INFOTYPE0021();
                    DataTable oTempTable;
                    oTempTable = item1.ToADODataTable(true);
                    oTempTable.TableName = "POSTDATA";
                    Data.Tables.Add(oTempTable);
                    foreach (INFOTYPE0021 p_item in saved)
                    {
                        DataRow oNewRow = oTempTable.NewRow();
                        p_item.LoadDataToTableRow(oNewRow);
                        oTempTable.LoadDataRow(oNewRow.ItemArray, false);
                    }
                }
            }
        }

    }
}
