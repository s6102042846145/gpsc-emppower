﻿using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE;
using ESS.HR.PA.DATACLASS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.DATASERVICE
{
    public class CVErrorService : AbstractDataService
    {

        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            CVReport oCVReport = new CVReport();
            return oCVReport;
        }


        public override void PrepareData(EmployeeData Requestor, object Data)
        {

        }

        //public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info)
        //{
        //    string ReturnKey = "";
        //    if (Creator.IsInRole("PAADMIN"))
        //    {
        //        ReturnKey = "ADMIN_CREATE";
        //    }
        //    else
        //    {
        //        ReturnKey = "EMPLOYEE_CREATE";
        //    }

        //    return ReturnKey;
        //    //return base.GenerateFlowKey(Requestor, Creator, Info);
        //}
        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {

            CVReport oCVReport = JsonConvert.DeserializeObject<CVReport>(newData.ToString());
            if (string.IsNullOrEmpty(oCVReport.Remark))
            {
                throw new DataServiceException("CVREPORT", "Required_Remark");
            }
            
        }
    }
}
