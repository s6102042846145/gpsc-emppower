﻿using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE;
using ESS.HR.PA.DATACLASS;
using ESS.HR.PA.INFOTYPE;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


namespace ESS.HR.PA.DATASERVICE
{
    class AddressService : AbstractDataService
    {
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            PersonalAddress oPersonalAddrData = new PersonalAddress();
            string ParamAddressType = string.Empty;
            if (!string.IsNullOrEmpty(CreateParam))
            {
                string[] tempParam = CreateParam.Split('|');
                ParamAddressType = tempParam[1];
            }

            List<INFOTYPE0006> oINFOTYPE0006 = HRPAManagement.CreateInstance(Requestor.CompanyCode).GetPersonalAddress(Requestor.EmployeeID,DateTime.Now);
            List<PersonalAddressControl> oListPersonalAddressControl = new List<PersonalAddressControl>();
            PersonalAddressControl oPersonalAddressControl = new PersonalAddressControl();
            oPersonalAddressControl.Requestor = Requestor.EmployeeID;
            oPersonalAddressControl.Requestor_Effective = DateTime.Now;
            oPersonalAddressControl.FileCount = 1;

            oListPersonalAddressControl.Add(oPersonalAddressControl);

            List<PersonalAddressType> oListPersonalAddressType = new List<PersonalAddressType>();
            PersonalAddressType oPersonalAddressType = new PersonalAddressType();
            oPersonalAddressType.Key = ParamAddressType;
            oListPersonalAddressType.Add(oPersonalAddressType);

            oPersonalAddrData.PersonalAddressControl = oListPersonalAddressControl;
            oPersonalAddrData.PersonalAddressType = oListPersonalAddressType;

            oPersonalAddrData.PersonalAddr = oINFOTYPE0006;
            oPersonalAddrData.PersonalAddrNew = oINFOTYPE0006;
            return oPersonalAddrData;
        }

        public override void PrepareData(EmployeeData Requestor, object Data)
        {

        }

        public override void CalculateInfoData(EmployeeData Requestor, object Data, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
            Info.Rows.Clear();
            DataSet ds = JsonConvert.DeserializeObject<DataSet>(Data.ToString());       
            DataRow dr = Info.NewRow();
            Info.Rows.Add(dr);
        }

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info)
        {
            string ReturnKey = "";
            if (Creator.IsInRole("PAADMIN"))
            {
                ReturnKey = "ADMIN_CREATE";
            }
            else
            {
                ReturnKey = "EMPLOYEE_CREATE";
            }

            return ReturnKey;
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            DataSet ds = JsonConvert.DeserializeObject<DataSet>(newData.ToString());
            DataTable oControlRecord = ds.Tables["PersonalAddressControl"];
            if (oControlRecord == null || oControlRecord.Rows.Count == 0 || oControlRecord.Rows[0]["Requestor_Effective"].Equals(DateTime.MinValue))
            {
                throw new DataServiceException("HRPA", "RECORD_CONTROL_NOT_FOUND", "Can not find 'RECORDCONTROL' table");
            }

            //if (Convert.ToInt32(oControlRecord.Rows[0]["FileCount"]) < 1
            if (NoOfFileAttached < 1
                && ds.Tables["PersonalAddressType"].Rows[0]["Key"].ToString() != "3")
            {
                throw new DataServiceException("HRPA", "FILE_NOT_ENOUGH", "Files are not enough");
            }

            string dataCategory = string.Empty;
            INFOTYPE0006 data;
            for (int i = 1; i < ds.Tables["PersonalAddrNew"].Rows.Count; i++)
            {
                data = new INFOTYPE0006();
                data.ParseToObject(ds.Tables["PersonalAddrNew"].Rows[i]);
                dataCategory = string.Format("HRPAPERSONALADDRESS_{0}", data.AddressType);

                if (i == 2)
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, true);

                if (HRPAManagement.CreateInstance(Requestor.CompanyCode).CheckMark(Requestor.EmployeeID, dataCategory))
                {
                    if (!HRPAManagement.CreateInstance(Requestor.CompanyCode).CheckMark(Requestor.EmployeeID, dataCategory, RequestNo))
                    {
                        throw new DataServiceException("HRPA", "REQUEST_DUPLICATE", "Request duplicate");
                    }
                }
            }
        }
        public override void ValidateDataInAction(object newData, DataTable Info, EmployeeData Requestor, string RequestNo, string State, string Action, bool HaveComment, bool HaveComment2, DateTime SubmitDate)
        {
            //Call from viewer
        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            //Save data to sql / sap
            string dataCategory = string.Empty;
            string subType = string.Empty;
            INFOTYPE0006 data;
            DataSet ds = JsonConvert.DeserializeObject<DataSet>(Data.ToString());
            for (int i = 1; i < ds.Tables["PersonalAddrNew"].Rows.Count; i++)
            {
                data = new INFOTYPE0006();
                data.ParseToObject(ds.Tables["PersonalAddrNew"].Rows[i]);
                dataCategory = string.Format("HRPAPERSONALADDRESS_{0}", data.AddressType);
                subType = data.AddressType;
                HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
            }


            if (State == "COMPLETED")
            {
                DataTable oControlRecord = ds.Tables["PersonalAddressControl"];
                DataView oKey = new DataView(ds.Tables["PersonalAddressType"]);
                DataTable oDataAddr = ds.Tables["PersonalAddr"];
                DataTable oDataAddrNew = ds.Tables["PersonalAddrNew"];
                List<INFOTYPE0006> list = new List<INFOTYPE0006>();
                foreach (DataRow dr in oDataAddr.Rows)
                {
                    INFOTYPE0006 item = new INFOTYPE0006();
                    item.ParseToObject(dr);
                    if (!item.IsDelete)
                    {
                        item.BeginDate = (DateTime)oControlRecord.Rows[0]["Requestor_Effective"];
                        item.EndDate = DateTime.MaxValue;
                    }
                    item.EmployeeID = (string)oControlRecord.Rows[0]["Requestor"];
                    oKey.RowFilter = string.Format("Key = '{0}'", item.AddressType);
                    if (oKey.Count > 0)
                    {
                        list.Add(item);
                    }
                }
                foreach (DataRow dr in oDataAddrNew.Rows)
                {
                    INFOTYPE0006 item = new INFOTYPE0006();
                    item.ParseToObject(dr);
                    if (!item.IsDelete)
                    {
                        item.BeginDate = (DateTime)oControlRecord.Rows[0]["Requestor_Effective"];
                        item.EndDate = DateTime.MaxValue;
                    }
                    item.EmployeeID = (string)oControlRecord.Rows[0]["Requestor"];

                    item.AddressNo = ValidateCharacter(item.AddressNo);
                    item.Street = ValidateCharacter(item.Street);
                    item.District = ValidateCharacter(item.District);
                    item.Province = ValidateCharacter(item.Province);
                    item.Postcode = ValidateCharacter(item.Postcode);
                    item.Telephone = ValidateCharacter(item.Telephone);

                    oKey.RowFilter = string.Format("Key = '{0}'", item.AddressType);
                    if (oKey.Count > 0)
                    {
                        list.Add(item);
                    }
                }
                try
                {
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).SavePersonalAddressData(list);
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).FileAttachmentSubTypeSave(RequestNo, subType);
                }
                catch (Exception ex)
                {
                    string sError = string.Empty;
                    foreach (INFOTYPE0006 p_item in list)
                    {
                        if (!string.IsNullOrEmpty(p_item.Remark))
                        {
                            sError += p_item.Remark;
                        }
                    }
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, true);
                    //throw new SaveExternalDataException("SaveExternalData Error" + " " + sError, true, ex);
                    throw new Exception(ex.Message.ToString());
                }
            }
        }

        public static string ValidateCharacter(string str)
        {
            string strReturn = string.Empty;
            string strPattern = @"[^0-9A-Za-zก-๙ ()/.,\-+’_]";
            byte[] enByte = Encoding.UTF8.GetBytes(str);
            string sInput = Encoding.UTF8.GetString(enByte);
            strReturn = Regex.Replace(sInput, strPattern, "", RegexOptions.None);
            return strReturn;
        }

        public override void PostProcess(EmployeeData Requestor, DataTable Info, object Data, string PreviousState, string State, string RequestNo, string Comment, string ActionCode)
        {
            //CAll after save requestdocument to sql 
        }

        public override string GenerateRequestSubtype(EmployeeData Requestor, object Additional)
        {
            return "";
        }
    }
}
