﻿using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE;
using ESS.HR.PA.CONFIG;
using ESS.HR.PA.DATACLASS;
using ESS.HR.PA.INFOTYPE;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PA.DATASERVICE
{
    class PersonalDataService : AbstractDataService
    {
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            PersonalInformation oPersonalInformation = new PersonalInformation();
            INFOTYPE0002 tmp = HRPAManagement.CreateInstance(Requestor.CompanyCode).GetPersonalData(Requestor.EmployeeID, DateTime.Now.Date);
            oPersonalInformation.PersonalData = tmp;
            oPersonalInformation.PersonalData_OLD = tmp;
            return oPersonalInformation;
        }

        public override void PrepareData(EmployeeData Requestor, object Data)
        {

        }

        public override void CalculateInfoData(EmployeeData Requestor, object Data, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
            Info.Rows.Clear();
            PersonalInformation oPersonalInformation = JsonConvert.DeserializeObject<PersonalInformation>(Data.ToString());
            DataRow dr = Info.NewRow();
            Info.Rows.Add(dr);
        }

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info)
        {
            string ReturnKey = "";
            if (Creator.IsInRole("PAADMIN"))
            {
                ReturnKey = "ADMIN_CREATE";
            }
            else
            {
                ReturnKey = "EMPLOYEE_CREATE";
            }

            return ReturnKey;
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            //Call from editor
            string dataCategory = "PERSONALDATA";
            if (HRPAManagement.CreateInstance(Requestor.CompanyCode).CheckMark(Requestor.EmployeeID, dataCategory, RequestNo))
            {
                throw new DataServiceException("HRPA", "REQUEST_DUPLICATE", "Request duplicate");
            }

            PersonalInformation oPersonalInformation = JsonConvert.DeserializeObject<PersonalInformation>(newData.ToString());
            INFOTYPE0002 oPersonalData = oPersonalInformation.PersonalData;
            INFOTYPE0002 oPersonalDataOld = oPersonalInformation.PersonalData_OLD;

            List<Argument> FieldEdit = new List<Argument>();
            List<PAConfiguration> PAConfigurationList = HRPAManagement.CreateInstance(Requestor.CompanyCode).GetPAConfigurationList().FindAll(delegate (PAConfiguration item)
            { return item.CategoryName == "PersonalData" || item.CategoryName == "PersonalDataAdditional"; });

            int iFileCount = 0;
            #region Personal Data
            //FirstName	ชื่อ
            if (oPersonalData.FirstName != oPersonalDataOld.FirstName) { ValidateFileCount("FirstName", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //Initial	ตัวย่อ
            if (oPersonalData.Initial != oPersonalDataOld.Initial) { ValidateFileCount("Initial", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //LastName	นามสกุล
            if (oPersonalData.LastName != oPersonalDataOld.LastName) { ValidateFileCount("LastName", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //MilitaryTitle	คำนำหน้าชื่อรอง
            if (oPersonalData.MilitaryTitle != oPersonalDataOld.MilitaryTitle) { ValidateFileCount("MilitaryTitle", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //NameFormat	    รูปแบบการแสดงชื่อ
            if (oPersonalData.NameFormat != oPersonalDataOld.NameFormat) { ValidateFileCount("NameFormat", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //NickName		ชื่อเล่น
            if (oPersonalData.NickName != oPersonalDataOld.NickName) { ValidateFileCount("NickName", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //PrefixName	    ฐานันดรศักดิ์
            if (oPersonalData.Prefix != oPersonalDataOld.Prefix) { ValidateFileCount("PrefixName", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //PrefixName1		ยศทางวิชาการ
            if (oPersonalData.NamePrefix1 != oPersonalDataOld.NamePrefix1) { ValidateFileCount("PrefixName1", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //PrefixName2		ยศทางการแพทย์
            if (oPersonalData.NamePrefix2 != oPersonalDataOld.NamePrefix2) { ValidateFileCount("PrefixName2", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //SecondTitle		คำนำหน้าชื่อรอง
            if (oPersonalData.SecondTitle != oPersonalDataOld.SecondTitle) { ValidateFileCount("SecondTitle", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //TitleName	    คำนำหน้าชื่อ
            if (oPersonalData.TitleID != oPersonalDataOld.TitleID) { ValidateFileCount("TitleName", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            #endregion

            #region Personal Data Additional
            //BirthDate				วันเกิด
            if (oPersonalData.DOB != oPersonalDataOld.DOB) { ValidateFileCount("BirthDate", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //BirthCity				ประเทศที่เกิด
            if (oPersonalData.BirthCity != oPersonalDataOld.BirthCity) { ValidateFileCount("BirthCity", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //BirthPlace				รหัสบัตรประจำตัวประชาชน
            if (oPersonalData.BirthPlace != oPersonalDataOld.BirthPlace) { ValidateFileCount("BirthPlace", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //Gender				เพศ
            if (oPersonalData.Gender != oPersonalDataOld.Gender) { ValidateFileCount("Gender", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //Language				ภาษา
            if (oPersonalData.Language != oPersonalDataOld.Language) { ValidateFileCount("Language", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //MaritalStatus				สถานะสมรส
            if (oPersonalData.MaritalStatus != oPersonalDataOld.MaritalStatus) { ValidateFileCount("MaritalStatus", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //Nationality				สัญชาติหลัก
            if (oPersonalData.Nationality != oPersonalDataOld.Nationality) { ValidateFileCount("Nationality", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //Nationality2				สัญชาติที่ 2
            if (oPersonalData.SecondNationality != oPersonalDataOld.SecondNationality) { ValidateFileCount("Nationality2", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //Nationality3				สัญชาติที่ 3
            if (oPersonalData.ThirdNationality != oPersonalDataOld.ThirdNationality) { ValidateFileCount("Nationality3", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //NoOfChild				จำนวนบุตร
            if (oPersonalData.NoOfChild != oPersonalDataOld.NoOfChild) { ValidateFileCount("NoOfChild", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //Religion				ศาสนา
            if (oPersonalData.Religion != oPersonalDataOld.Religion) { ValidateFileCount("Religion", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            #endregion

            if(oPersonalData.NoOfChild > 20)
            {
                throw new DataServiceException("HRPAPERSONALDATA", "VALIDATE_NOOFCHILD_20");
            }

            if (NoOfFileAttached < iFileCount)
            {
                throw new DataServiceException("HRPAPERSONALDATA", "PAFILEATTATCHMENTWARNING", iFileCount.ToString());
            }

        }

        private void ValidateFileCount(string FieldName, List<PAConfiguration> PAConfigurationList, ref List<Argument> ReturnList, ref int iFileCount)
        {
            Argument arg = new Argument();
            PAConfiguration oPAConfiguration = PAConfigurationList.Find(delegate (PAConfiguration item) { return item.FieldName == FieldName; });
            if (oPAConfiguration != null && oPAConfiguration.GroupAttachFile > 0)
            {
                arg.text = oPAConfiguration.GroupFileName + "_File";
                arg.Value = oPAConfiguration.GroupAttachFile.ToString();
                if (!ReturnList.Exists(delegate (Argument filegroup) { return filegroup.text == arg.text; }))
                {
                    iFileCount += oPAConfiguration.GroupAttachFile;
                    ReturnList.Add(arg);
                }
            }
        }

        public override void ValidateDataInAction(object newData, DataTable Info, EmployeeData Requestor, string RequestNo, string State, string Action, bool HaveComment, bool HaveComment2, DateTime SubmitDate)
        {
            //Call from viewer
        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            //Save data to sql / sap
            string dataCategory = "PERSONALDATA";
            HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
            if (State == "COMPLETED")
            {
                try
                {
                    PersonalInformation oPersonalInformation = JsonConvert.DeserializeObject<PersonalInformation>(Data.ToString());
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).SavePersonalData(oPersonalInformation.PersonalData);

                    HRPAManagement.CreateInstance(Requestor.CompanyCode).ProcessInfoTypeAuto(oPersonalInformation.PersonalData_OLD, oPersonalInformation.PersonalData);
                }
                catch(Exception ex)
                {
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, true);
                    throw new Exception(ex.Message.ToString());
                    //throw new SaveExternalDataException("SaveExternalData Error", true, ex);
                }
            }
        }

        public override void PostProcess(EmployeeData Requestor, DataTable Info, object Data, string PreviousState, string State, string RequestNo, string Comment, string ActionCode)
        {
            //CAll after save requestdocument to sql 
        }

    }
}
