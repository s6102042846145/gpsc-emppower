﻿using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE;
using ESS.HR.PA.CONFIG;
using ESS.HR.PA.DATACLASS;
using ESS.HR.PA.INFOTYPE;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ESS.HR.PA.DATASERVICE
{
    class FamilyDataService : AbstractDataService
    {
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            PersonalFamily oPersonalFamily = new PersonalFamily();
            INFOTYPE0021 tmp = new INFOTYPE0021();
            if (!string.IsNullOrEmpty(CreateParam))
            {
                string[] buffer = CreateParam.Split('|');
                tmp = HRPAManagement.CreateInstance(Requestor.CompanyCode).GetFamilyData(Requestor.EmployeeID, buffer[1], buffer[2]);
            }
            else
            {
                tmp.EmployeeID = Requestor.EmployeeID;
                tmp.FamilyMember = "";
                tmp.BirthDate = DateTime.Now;
                tmp.Dead = "";
                tmp.BeginDate = DateTime.Now;
            }
            oPersonalFamily.FamilyData = tmp;
            oPersonalFamily.FamilyData_OLD = tmp;
            return oPersonalFamily;
        }

        public override void PrepareData(EmployeeData Requestor, object Data)
        {

        }

        public override void CalculateInfoData(EmployeeData Requestor, object Data, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
            Info.Rows.Clear();
            PersonalFamily oPersonalFamily = JsonConvert.DeserializeObject<PersonalFamily>(Data.ToString());
            DataRow dr = Info.NewRow();
            Info.Rows.Add(dr);
        }

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info)
        {
            string ReturnKey = "";
            if (Creator.IsInRole("PAADMIN"))
            {
                ReturnKey = "ADMIN_CREATE";
            }
            else
            {
                ReturnKey = "EMPLOYEE_CREATE";
            }

            return ReturnKey;
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            //Call from editor
            PersonalFamily oPersonalFamily = JsonConvert.DeserializeObject<PersonalFamily>(newData.ToString());
            if (string.IsNullOrEmpty(oPersonalFamily.FamilyData.Name.Trim()) || string.IsNullOrEmpty(oPersonalFamily.FamilyData.Surname.Trim()))
            {
                throw new DataServiceException("HRPA", "PLEASE_INPUT_NAME_AND_SURNAME", "Please input name and surname");
            }

            if (oPersonalFamily.FamilyData.FamilyMember == "1")
            {
                //long num;
                //bool isNum = long.TryParse(oPersonalFamily.FamilyData.SpouseID, out num);
                //if (oPersonalFamily.FamilyData.SpouseID.Length != 13 || !isNum)
                if (oPersonalFamily.FamilyData.SpouseID.Length == 0)
                {
                    throw new DataServiceException("FAMILY", "INVALID_IDCARDNO", "INVALID_IDCARDNO");
                }
            }
            if (oPersonalFamily.FamilyData.FamilyMember == "11")
            {
                //long num;
                //bool isNum = long.TryParse(oPersonalFamily.FamilyData.FatherID, out num);
                //if (oPersonalFamily.FamilyData.FatherID.Length != 13 || !isNum)
                if (oPersonalFamily.FamilyData.FatherID.Length == 0)
                {
                    throw new DataServiceException("FAMILY", "INVALID_IDCARDNO", "INVALID_IDCARDNO");
                }
            }
            if (oPersonalFamily.FamilyData.FamilyMember == "12")
            {
                //long num;
                //bool isNum = long.TryParse(oPersonalFamily.FamilyData.MotherID, out num);
                //if (oPersonalFamily.FamilyData.MotherID.Length != 13 || !isNum)
                if (oPersonalFamily.FamilyData.MotherID.Length == 0)
                {
                    throw new DataServiceException("FAMILY", "INVALID_IDCARDNO", "INVALID_IDCARDNO");
                }
            }

            //List<string> listIgnoreFamilyMember = new List<string>();
            //listIgnoreFamilyMember.Add("7");
            //if (NoOfFileAttached < 2 && !listIgnoreFamilyMember.Contains(oPersonalFamily.FamilyData.FamilyMember))
            //{
            //    throw new DataServiceException("HRPA", "FILE_NOT_ENOUGH", "Files are not enough");
            //}
            string dataCategory = "FAMILY_" + GenerateRequestSubtype(Requestor, newData);
            if (HRPAManagement.CreateInstance(Requestor.CompanyCode).CheckMark(Requestor.EmployeeID, dataCategory, RequestNo))
            {
                throw new DataServiceException("HRPA", "REQUEST_DUPLICATE", "Request duplicate");
            }

            List<Argument> FieldEdit = new List<Argument>();
            List<PAConfiguration> PAConfigurationList = HRPAManagement.CreateInstance(Requestor.CompanyCode).GetPAConfigurationList().FindAll(delegate (PAConfiguration oPAConfiguration) { return oPAConfiguration.CategoryName == "Family"; });
            INFOTYPE0021 Object = oPersonalFamily.FamilyData;
            INFOTYPE0021 ObjectOld = oPersonalFamily.FamilyData_OLD;

            int iFileCount = 0;
            //BirthDate					วันเกิด
            if (Object.BirthDate != ObjectOld.BirthDate) { ValidateFileCount("BirthDate", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //BirthPlace					สถานที่เกิด
            if (Object.BirthPlace != ObjectOld.BirthPlace) { ValidateFileCount("BirthPlace", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //ChdNo					บุตรคนที่
            if (Object.ChildNo != ObjectOld.ChildNo) { ValidateFileCount("ChdNo", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //CountryBirth					ประเทศที่เกิด
            if (Object.CityOfBirth != ObjectOld.CityOfBirth) { ValidateFileCount("CountryBirth", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //Dead					สถานะ
            if (Object.Dead != ObjectOld.Dead) { ValidateFileCount("Dead", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //Employer					สิทธิ์การเบิกค่ารักษาพยาบาล
            if (Object.Employer != ObjectOld.Employer) { ValidateFileCount("Employer", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //FamilyMember 					ความสัมพันธ์
            if (Object.FamilyMember != ObjectOld.FamilyMember) { ValidateFileCount("FamilyMember", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //FatherID					เลขประจำตัวประชาชนบิดา
            if (Object.FatherID != ObjectOld.FatherID) { ValidateFileCount("FatherID", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //FatherSpouseID					เลขประจำตัวประชาชนบิดาของคู่สมรส
            if (Object.FatherSpouseID != ObjectOld.FatherSpouseID) { ValidateFileCount("FatherSpouseID", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //JobTitle					สิทธิ์การเบิกค่าเล่าเรียน
            if (Object.JobTitle != ObjectOld.JobTitle) { ValidateFileCount("JobTitle", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //MotherID					เลขประจำตัวประชาชนมารดา
            if (Object.MotherID != ObjectOld.MotherID) { ValidateFileCount("MotherID", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //MotherSpouseID					เลขประจำตัวประชาชนมารดาของคู่สมรส
            if (Object.MotherSpouseID != ObjectOld.MotherSpouseID) { ValidateFileCount("MotherSpouseID", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //Nationality					สัญชาติ
            if (Object.Nationality != ObjectOld.Nationality) { ValidateFileCount("Nationality", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //SpouseID					เลขประจำตัวประชาชนคู่สมรส
            if (Object.SpouseID != ObjectOld.SpouseID) { ValidateFileCount("SpouseID", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //TitleName					คำนำหน้าชื่อ
            if (Object.TitleName != ObjectOld.TitleName) { ValidateFileCount("TitleName", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //Name					ชื่อ
            if (Object.Name != ObjectOld.Name) { ValidateFileCount("Name", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //Surname					นามสกุล
            if (Object.Surname != ObjectOld.Surname) { ValidateFileCount("Surname", PAConfigurationList, ref FieldEdit, ref iFileCount); }
            //TitleRank					ฐานันดรศักดิ์
            if (Object.TitleRank != ObjectOld.TitleRank) { ValidateFileCount("TitleRank", PAConfigurationList, ref FieldEdit, ref iFileCount); }

            if (NoOfFileAttached < iFileCount)
            {
                throw new DataServiceException("FAMILY", "PAFILEATTATCHMENTWARNING", iFileCount.ToString());
            }
        }

        private void ValidateFileCount(string FieldName, List<PAConfiguration> PAConfigurationList, ref List<Argument> ReturnList, ref int iFileCount)
        {
            Argument arg = new Argument();
            PAConfiguration oPAConfiguration = PAConfigurationList.Find(delegate (PAConfiguration item) { return item.FieldName == FieldName; });
            if (oPAConfiguration != null && oPAConfiguration.GroupAttachFile > 0)
            {
                arg.text = oPAConfiguration.GroupFileName + "_File";
                arg.Value = oPAConfiguration.GroupAttachFile.ToString();
                if (!ReturnList.Exists(delegate (Argument filegroup) { return filegroup.text == arg.text; }))
                {
                    iFileCount += oPAConfiguration.GroupAttachFile;
                    ReturnList.Add(arg);
                }
            }
        }

        public override void ValidateDataInAction(object newData, DataTable Info, EmployeeData Requestor, string RequestNo, string State, string Action, bool HaveComment, bool HaveComment2, DateTime SubmitDate)
        {
            //Call from viewer
        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            //Save data to sql / sap
            string subType = GenerateRequestSubtype(Requestor, Data);
            string dataCategory = "FAMILY_" + subType;

            HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
            if (State == "COMPLETED")
            {
                try
                {
                    PersonalFamily oPersonalFamily = JsonConvert.DeserializeObject<PersonalFamily>(Data.ToString());

                    //Add 23/7/2020
                    INFOTYPE0021 oldData = new INFOTYPE0021();
                    oldData = oPersonalFamily.FamilyData_OLD;
                    //

                    List<INFOTYPE0021> FamilyList = new List<INFOTYPE0021>();
                    //oPersonalFamily.FamilyData.BeginDate = DateTime.Now;

                    if (oPersonalFamily.FamilyData.BeginDate == DateTime.MinValue)
                    {
                        oPersonalFamily.FamilyData.BeginDate = DateTime.Now;
                    }
                    oPersonalFamily.FamilyData.EndDate = new DateTime(9999, 12, 31);
                    FamilyList.Add(oPersonalFamily.FamilyData);


                    HRPAManagement.CreateInstance(Requestor.CompanyCode).SaveFamilyData(FamilyList);
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).FileAttachmentSubTypeSave(RequestNo, subType);


                    //Add 23/7/2020
                    if (FamilyList != null && FamilyList.Count > 0)
                    {
                        INFOTYPE0021 infotype0021 = FamilyList[0];
                        infotype0021.SubType = infotype0021.FamilyMember;
                        //กรณีที่เป็นการเพิ่ม olddata จะเป็น blank
                        //if (string.IsNullOrEmpty(oldData.FamilyMember))
                        if (string.IsNullOrEmpty(oldData.FamilyMember) || oldData.FamilyMember == "2" || oldData.FamilyMember == "1")
                        {
                            HRPAManagement.CreateInstance(Requestor.CompanyCode).ProcessInfoTypeAuto(infotype0021);
                        }
                        else
                        {
                            HRPAManagement.CreateInstance(Requestor.CompanyCode).ProcessInfoTypeAuto(oldData, infotype0021);
                        }
                    }
                    //


                }
                catch (Exception ex)
                {
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, true);
                    //throw new SaveExternalDataException("Post data error", true, ex);
                    throw new Exception(ex.Message.ToString());
                }
            }
        }

        public override void PostProcess(EmployeeData Requestor, DataTable Info, object Data, string PreviousState, string State, string RequestNo, string Comment, string ActionCode)
        {
            //CAll after save requestdocument to sql 
        }

        public override string GenerateRequestSubtype(EmployeeData Requestor, object Additional)
        {
            PersonalFamily oPersonalFamily = JsonConvert.DeserializeObject<PersonalFamily>(Additional.ToString());
            string sChildNo = oPersonalFamily.FamilyData.ChildNo;
            if (!string.IsNullOrEmpty(sChildNo))
            {
                if(sChildNo.Length != 2)
                {
                    sChildNo = sChildNo.PadLeft(2,'0');
                }
            }
            return oPersonalFamily.FamilyData.FamilyMember + sChildNo;
        }
    }
}
