﻿using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE;
using ESS.HR.PA.DATACLASS;
using ESS.WORKFLOW;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ESS.HR.PA.DATASERVICE
{
    public class ReportOMService : AbstractDataService
    {
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            EmployeeManagement oEmployeeManagement = EmployeeManagement.CreateInstance(Requestor.CompanyCode);
            ReportOM oReportOM = new ReportOM();
            DataTable oDT = oEmployeeManagement.GetOrganizationStructure(Requestor.EmployeeID, Requestor.PositionID);
            oDT.Columns.Add("OrgUnitName", typeof(System.String));
            foreach (DataRow dr in oDT.Rows)
            {
                if (!string.IsNullOrEmpty(dr["Manager_ID"].ToString()) && !string.IsNullOrEmpty(dr["Manager_PositionID"].ToString()))
                {
                    EmployeeData oEmpData = new EmployeeData(dr["Manager_ID"].ToString(), dr["Manager_PositionID"].ToString(), Requestor.CompanyCode, DateTime.Now);
                    dr["OrgUnitName"] = oEmpData.OrgAssignment.OrgUnitData.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                }
            }
            oReportOM.OrgStructureSnapshot = oDT;
            return oReportOM;
        }

      
        public override void PrepareData(EmployeeData Requestor, object Data)
        {
            
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            ReportOM oReportOM = JsonConvert.DeserializeObject<ReportOM>(newData.ToString());
            if (string.IsNullOrEmpty(oReportOM.Remark))
            {
                throw new DataServiceException("REPORTOM", "Required_Remark");
            }
        }
    }
}
