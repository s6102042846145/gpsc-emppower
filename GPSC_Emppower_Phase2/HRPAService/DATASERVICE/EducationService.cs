﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;
using ESS.DATA.ABSTRACT;
using ESS.EMPLOYEE;
using ESS.HR.PA.DATACLASS;
using ESS.HR.PA.INFOTYPE;
using Newtonsoft.Json;
using System.Data;
using ESS.DATA.EXCEPTION;

namespace ESS.HR.PA.DATASERVICE
{
    public class EducationService : AbstractDataService
    {
        public EducationService()
        { }
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            EducationInfo oEducationInfo = new EducationInfo();
            INFOTYPE0022 tmp = new INFOTYPE0022();
            if (!string.IsNullOrEmpty(CreateParam))
            {
                string[] param = CreateParam.Split('|');
                tmp = HRPAManagement.CreateInstance(Requestor.CompanyCode).GetEducation(Requestor.EmployeeID).Where(x=> x.EducationLevelCode == param[1] && x.BeginDate ==Convert.ToDateTime(param[2])).ToList()[0];
            }
            else
            {
                tmp.EmployeeID = Requestor.EmployeeID;
                tmp.EducationLevelCode = "";
                tmp.InstituteCode = "";
                tmp.CountryCode = "";
                tmp.CertificateCode = "";
                tmp.EducationGroupCode = "";
                tmp.Branch1 = "";
                tmp.Branch2 = "";
                tmp.BeginDate = DateTime.Now;
                tmp.EndDate = DateTime.Now;
               
            }
            
            oEducationInfo.Education = tmp;
            oEducationInfo.EducationOld = tmp;
            return oEducationInfo;
        }

        public override void CalculateInfoData(EmployeeData Requestor, object Data, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
            Info.Rows.Clear();
            EducationInfo oEducation = JsonConvert.DeserializeObject<EducationInfo>(Data.ToString());
            DataRow dr = Info.NewRow();
            Info.Rows.Add(dr);
        }

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info)
        {
            string ReturnKey = "";
            if (Creator.IsInRole("PAADMIN"))
            {
                ReturnKey = "ADMIN_CREATE";
            }
            else
            {
                ReturnKey = "EMPLOYEE_CREATE";
            }

            return ReturnKey;
        }
        public override string GenerateRequestSubtype(EmployeeData Requestor, object Additional)
        {
            EducationInfo oEducationInfo = JsonConvert.DeserializeObject<EducationInfo>(Additional.ToString());
            return oEducationInfo.Education.EducationLevelCode;
            
        }

       
        public override void PrepareData(EmployeeData Requestor, object Data)
        {
            
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            EducationInfo oEducationInfo = JsonConvert.DeserializeObject<EducationInfo>(newData.ToString());
                       
            if (NoOfFileAttached < 1)
            {
                throw new DataServiceException("HRPA", "FILE_NOT_ENOUGH");
            }

            string dataCategory = "PERSONALEDUCATION_" + GenerateRequestSubtype(Requestor, newData);
            if (HRPAManagement.CreateInstance(Requestor.CompanyCode).CheckMark(Requestor.EmployeeID, dataCategory, RequestNo))
            {
                throw new DataServiceException("HRPA", "REQUEST_DUPLICATE");
            }
           
        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            //Save data to sql / sap
            string subType = GenerateRequestSubtype(Requestor, Data);
            string dataCategory = "PERSONALEDUCATION_" + subType;
            HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
            if (State == "COMPLETED")
            {
                try
                {
                    EducationInfo oEducationInfo = JsonConvert.DeserializeObject<EducationInfo>(Data.ToString());
                    List<INFOTYPE0022> educationList = new List<INFOTYPE0022>();
                    educationList.Add(oEducationInfo.Education);
                    if(!string.IsNullOrEmpty(oEducationInfo.EducationOld.EducationLevelCode) && (oEducationInfo.Education.EducationLevelCode == oEducationInfo.EducationOld.EducationLevelCode))
                    {
                        //update by deleting Old data education
                        List<INFOTYPE0022> educationUpdate = new List<INFOTYPE0022>();
                        educationUpdate.Add(oEducationInfo.EducationOld);
                        educationUpdate.Add(oEducationInfo.Education);
                        
                        HRPAManagement.CreateInstance(Requestor.CompanyCode).DeleteInsertEducation(educationUpdate);

                    }
                    else
                    {
                        HRPAManagement.CreateInstance(Requestor.CompanyCode).SaveEducation(educationList);
                    }
                    
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).FileAttachmentSubTypeSave(RequestNo, subType);
                }
                catch (Exception ex)
                {
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, true);
                    //throw new SaveExternalDataException("Post data error", true, ex);
                    throw new Exception(ex.Message.ToString());
                }
            }

        }

    }
}
