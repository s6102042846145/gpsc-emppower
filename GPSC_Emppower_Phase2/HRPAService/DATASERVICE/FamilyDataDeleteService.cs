﻿using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE;
using ESS.HR.PA.DATACLASS;
using ESS.HR.PA.INFOTYPE;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ESS.HR.PA.DATASERVICE
{
    class FamilyDataDeleteService : AbstractDataService
    {
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            PersonalFamily oPersonalFamily = new PersonalFamily();
            INFOTYPE0021 tmp = new INFOTYPE0021();
            if (!string.IsNullOrEmpty(CreateParam))
            {
                string[] buffer = CreateParam.Split('|');
                tmp = HRPAManagement.CreateInstance(Requestor.CompanyCode).GetFamilyData(Requestor.EmployeeID, buffer[1], buffer[2]);
            }
            oPersonalFamily.FamilyData = tmp;

            return oPersonalFamily;
        }

        public override void PrepareData(EmployeeData Requestor, object Data)
        {

        }

        public override void CalculateInfoData(EmployeeData Requestor, object Data, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
            Info.Rows.Clear();
            PersonalFamily oPersonalFamily = JsonConvert.DeserializeObject<PersonalFamily>(Data.ToString());
            DataRow dr = Info.NewRow();
            Info.Rows.Add(dr);
        }

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info)
        {
            string ReturnKey = "";
            if (Creator.IsInRole("PAADMIN"))
            {
                ReturnKey = "ADMIN_CREATE";
            }
            else
            {
                ReturnKey = "EMPLOYEE_CREATE";
            }

            return ReturnKey;
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            //Call from editor
            string dataCategory = "FAMILYDEL_" + GenerateRequestSubtype(Requestor, newData);
            if (HRPAManagement.CreateInstance(Requestor.CompanyCode).CheckMark(Requestor.EmployeeID, dataCategory, RequestNo))
            {
                throw new DataServiceException("HRPA", "REQUEST_DUPLICATE", "Request duplicate");
            }
        }

        public override void ValidateDataInAction(object newData, DataTable Info, EmployeeData Requestor, string RequestNo, string State, string Action, bool HaveComment, bool HaveComment2, DateTime SubmitDate)
        {
            //Call from viewer
        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            //Save data to sql / sap
            string subType = GenerateRequestSubtype(Requestor, Data);
            string dataCategory = "FAMILYDEL_" + subType;
            HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
            if (State == "COMPLETED")
            {
                try
                {
                    PersonalFamily oPersonalFamily = JsonConvert.DeserializeObject<PersonalFamily>(Data.ToString());

                    //Add 23/7/2020
                    INFOTYPE0021 oldData = new INFOTYPE0021();
                    oldData = oPersonalFamily.FamilyData_OLD;
                    //

                    List<INFOTYPE0021> FamilyList = new List<INFOTYPE0021>();
                    //delimit records
                    oPersonalFamily.FamilyData.EndDate = DateTime.Now.AddDays(-1);
                    if (oPersonalFamily.FamilyData.BeginDate > oPersonalFamily.FamilyData.EndDate)
                        oPersonalFamily.FamilyData.EndDate = oPersonalFamily.FamilyData.BeginDate;
                    FamilyList.Add(oPersonalFamily.FamilyData);
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).DeleteFamilyData(FamilyList);
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).FileAttachmentSubTypeSave(RequestNo, subType);

                    //Add 23/7/2020
                    if (FamilyList != null && FamilyList.Count > 0)
                    {
                        INFOTYPE0021 infotype0021 = FamilyList[0];
                        infotype0021.SubType = infotype0021.FamilyMember;
                        //กรณีที่เป็นการเพิ่ม olddata จะเป็น blank
                        if (string.IsNullOrEmpty(oldData.FamilyMember))
                        {
                            HRPAManagement.CreateInstance(Requestor.CompanyCode).ProcessInfoTypeAuto(infotype0021);
                        }
                        else
                        {
                            HRPAManagement.CreateInstance(Requestor.CompanyCode).ProcessInfoTypeAuto(oldData, infotype0021);
                        }
                    }
                    //
                }
                catch (Exception ex)
                {
                    HRPAManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, true);
                    throw new SaveExternalDataException("SaveExternalData Error", true, ex);
                }
            }
        }

        public override void PostProcess(EmployeeData Requestor, DataTable Info, object Data, string PreviousState, string State, string RequestNo, string Comment, string ActionCode)
        {
            //CAll after save requestdocument to sql 
        }

        public override string GenerateRequestSubtype(EmployeeData Requestor, object Additional)
        {
            PersonalFamily oPersonalFamily = JsonConvert.DeserializeObject<PersonalFamily>(Additional.ToString());
            return oPersonalFamily.FamilyData.FamilyMember + oPersonalFamily.FamilyData.ChildNo;
        }
    }
}
