using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using ESS.DATA;
using ESS.EMPLOYEE;
using ESS.HR;

namespace ESS.HR.DATASERVICE
{
    public class LetterOfRecommendation : AbstractDataService
    {
        public LetterOfRecommendation()
        { 
        }

        public override DataSet GenerateAdditionalData(EmployeeData Requestor)
        {
            DataSet DS = new DataSet("ADDITIONAL");
            DataTable oTable;
            Letter oLetter;
            oLetter = new Letter();
            oLetter.RequestDate = DateTime.Now;
            oLetter.Requestor = Requestor.EmployeeID;
            oTable = oLetter.ToADODataTable();
            oTable.TableName = "LETTER";
            DS.Tables.Add(oTable);

            oLetter = new Letter();
            oTable = oLetter.ToADODataTable(true);
            oTable.TableName = "LETTERVALUE";
            DS.Tables.Add(oTable);

            LetterData oLetterData;
            oLetterData = new LetterData();
            oTable = oLetterData.ToADODataTable(true);
            oTable.TableName = "LETTERDATA";
            DS.Tables.Add(oTable);
            return DS;
        }

        public override void PrepareData(EmployeeData Requestor, DataSet Data)
        {
        }

        public override void ValidateData(DataSet newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate)
        {
            List<LetterType> oTypeList = ESS.HR.PA.LetterOfRecommendManagement.GetAllLetterTypes();
            Dictionary<int, LetterType> letterTypeDict = new Dictionary<int, LetterType>();
            foreach (LetterType oType in oTypeList)
            {
                letterTypeDict.Add(oType.LetterTypeID, oType);
            }

            DataTable oTable;

            oTable = newData.Tables["LETTERDATA"];
            Dictionary<int, Dictionary<int, LetterData>> letterDataList = new Dictionary<int, Dictionary<int, LetterData>>();
            foreach (DataRow dr in oTable.Rows)
            {
                LetterData oLetterData = new LetterData();
                oLetterData.ParseToObject(dr);
                if (!letterDataList.ContainsKey(oLetterData.LetterID))
                {
                    letterDataList.Add(oLetterData.LetterID, new Dictionary<int, LetterData>());
                }
                if (!letterDataList[oLetterData.LetterID].ContainsKey(oLetterData.FieldID))
                {
                    letterDataList[oLetterData.LetterID].Add(oLetterData.FieldID, oLetterData);
                }
            }

            oTable = newData.Tables["LETTERVALUE"];
            foreach (DataRow dr in oTable.Rows)
            {
                Letter oLetter = new Letter();
                oLetter.ParseToObject(dr);
                LetterType oType;
                if (letterTypeDict.ContainsKey(oLetter.LetterTypeID))
                {
                    oType = letterTypeDict[oLetter.LetterTypeID];
                    foreach (LetterTypeField oField in oType.FieldList)
                    {
                        if (letterDataList.ContainsKey(oLetter.LetterTypeID) && letterDataList[oLetter.LetterTypeID].ContainsKey(oField.FieldID))
                        {
                            LetterData fdata = letterDataList[oLetter.LetterTypeID][oField.FieldID];
                            switch (oField.FieldType.ToUpper())
                            {
                                case "STRING":
                                    if (fdata.FieldDataText == "")
                                    {
                                        throw new FieldRequiredException(oType.LetterTypeCode, oField.FieldCode);
                                    }
                                    break;
                                case "DATETIME":
                                    if (fdata.FieldDataDate == DateTime.MinValue)
                                    {
                                        throw new FieldRequiredException(oType.LetterTypeCode, oField.FieldCode);
                                    }
                                    break;
                            }
                            
                        }
                        else
                        {
                            throw new Exception("Schema error");
                        }
                    }
                }
            }

        }
    }
}
