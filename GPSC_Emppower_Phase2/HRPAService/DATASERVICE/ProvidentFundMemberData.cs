using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using ESS.DATA;
using ESS.EMPLOYEE;

namespace ESS.HR.DATASERVICE
{
    public class ProvidentFundMemberData:AbstractDataService
    {
        public ProvidentFundMemberData()
        { 
        }

        public override DataSet GenerateAdditionalData(EmployeeData Requestor)
        {
            DataSet DS = new DataSet("ADDITIONAL");
            DataTable oTable;
            DataRow oRow;
            oTable = new DataTable("RECORDCONTROL");
            oTable.Columns.Add("INFOTYPE0021_EFFECTIVE", typeof(DateTime));
            oRow = oTable.NewRow();
            oRow["INFOTYPE0021_EFFECTIVE"] = DateTime.Now;
            oTable.Rows.Add(oRow);
            DS.Tables.Add(oTable);

            INFOTYPE0021 classTemplate = new INFOTYPE0021();
            oTable = classTemplate.ToADODataTable(true);
            oTable.TableName = "INFOTYPE0021";
            DS.Tables.Add(oTable);

            oTable = new DataTable("FILECOUNT");
            oTable.Columns.Add("FILECOUNT", typeof(int));
            DS.Tables.Add(oTable);
            return DS;
        }

        public override void PrepareData(EmployeeData Requestor, DataSet Data)
        {
            DataTable oTable;
            DataRow oRow;
            if (!Data.Tables.Contains("RECORDCONTROL"))
            {
                oTable = new DataTable("RECORDCONTROL");
                oTable.Columns.Add("INFOTYPE0021_EFFECTIVE", typeof(DateTime));
                oRow = oTable.NewRow();
                oTable.Rows.Add(oRow);
                Data.Tables.Add(oTable);
            }
            else if (!Data.Tables["RECORDCONTROL"].Columns.Contains("INFOTYPE0021_EFFECTIVE"))
            {
                Data.Tables["RECORDCONTROL"].Columns.Add("INFOTYPE0021_EFFECTIVE", typeof(DateTime));
                if (Data.Tables["RECORDCONTROL"].Rows.Count > 0)
                {
                    oRow = Data.Tables["RECORDCONTROL"].Rows[0];
                    oRow["INFOTYPE0021_EFFECTIVE"] = DateTime.Now;
                }
            }

            if (!Data.Tables.Contains("INFOTYPE0021"))
            {
                INFOTYPE0009 oItem = INFOTYPE0009.GetData(Requestor);
                oTable = oItem.ToADODataTable();
                oTable.TableName = "INFOTYPE0021";
                Data.Tables.Add(oTable);
            }
        }

        public override void ValidateData(DataSet newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate)
        {
            DataTable oControlRecord = newData.Tables["RECORDCONTROL"];
            if (oControlRecord == null || oControlRecord.Rows.Count == 0)
            {
                throw new DataServiceException("HRPA", "RECORD_CONTROL_NOT_FOUND", "Can not find 'RECORDCONTROL' table");
            }
            INFOTYPE0021 item = new INFOTYPE0021();
            item.ParseToObject(newData.Tables["INFOTYPE0021"].Rows[1]);
            if (item.Name.Trim() == "" || item.Surname.Trim() == "")
            {
                throw new DataServiceException("HRPA", "PLEASE_INPUT_NAME_AND_SURNAME", "Please input name and surname");
            }
            if (newData.Tables.Contains("Exception"))
            {
                newData.Tables.Remove("Exception");
            }
        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, DataSet Data, string State, string RequestNo)
        {
            if (State == "COMPLETED")
            {
                DataTable oControlRecord = Data.Tables["RECORDCONTROL"];
                DataTable oData = Data.Tables["INFOTYPE0021"];
            
                List<INFOTYPE0021> data = new  List<INFOTYPE0021>();
                List<INFOTYPE0021> saved = new List<INFOTYPE0021>();
                foreach (DataRow dr in oData.Rows)
                {
                    INFOTYPE0021 item = new INFOTYPE0021();
                    item.ParseToObject(dr);
                    item.BeginDate = DateTime.Now;
                    item.EndDate = new DateTime(9999, 12, 31);
                    saved.Add(item);
                    break;
                }
                try
                {
                    HR.PA.ServiceManager.HRPAService.SaveFamilyData(saved);
                }
                catch (Exception ex)
                {
                    DataTable oException = new DataTable("Exception");
                    oException.Columns.Add("ErrorDetail", typeof(string));
                    oException.LoadDataRow(new object[] { ex.Message }, false);
                    if (Data.Tables.Contains("Exception"))
                    {
                        Data.Tables.Remove("Exception");
                    }
                    Data.Tables.Add(oException);
                    throw new ESS.WORKFLOW.SaveExternalDataException("Post data error", true, ex);
                }
            }
        }
    }
}
