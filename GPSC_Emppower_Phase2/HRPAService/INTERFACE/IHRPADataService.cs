using System;
using System.Collections.Generic;
using ESS.DATA;
using ESS.HR.PA.DATACLASS;
using ESS.HR.PA.INFOTYPE;

namespace ESS.HR.PA.INTERFACE
{
    public interface IHRPADataService
    {
        List<INFOTYPE0006> GetPersonalAddress(string EmployeeID);

        List<INFOTYPE0006> GetPersonalAddress(string EmployeeID, DateTime CheckDate);

        INFOTYPE0002 GetPersonalData(string EmployeeID, DateTime CheckDate);

        INFOTYPE0002 GetPersonalData(string EmployeeID);

        void MarkUpdate(string EmployeeID, string DataCategory, string RequestNo, bool isMarkIn);

        bool CheckMark(string EmployeeID, string DataCategory);

        bool CheckMark(string EmployeeID, string DataCategory, string ReqNo);

        void SavePersonalData(INFOTYPE0002 data);

        List<INFOTYPE0002> GetInfotype0002List(string EmployeeID1, string EmployeeID2);

        void SaveInfotype0002(string EmployeeID1, string EmployeeID2, List<INFOTYPE0002> List, string Profile);

        void SavePersonalAddress(List<INFOTYPE0006> data);

        INFOTYPE0009 GetBankDetail(string EmployeeID);

        void SaveBankDetail(INFOTYPE0009 data);

        List<INFOTYPE0021> GetFamilyData(string EmployeeID);

        void SaveFamilyData(INFOTYPE0021 data);

        void SaveFamilyData(List<INFOTYPE0021> data);

        INFOTYPE0021 GetFamilyData(string EmployeeID, string FamilyMember, string ChildNo);

        List<INFOTYPE0022> GetAllEducationHistory(string EmployeeID);

        List<INFOTYPE0023> GetAllPreviousEmployers(string EmployeeID);

        void SearchInfotype0002(FilterData filter);

        List<INFOTYPE9002> GetAllWorkHistory(string EmployeeID);

        List<INFOTYPE9001> GetAllSalaryHistory(string EmployeeID);

        List<INFOTYPE0008> GetSalaryList(string EmployeeID);

        List<INFOTYPE0008> GetSalaryList(string EmployeeID, DateTime CheckDate);

        List<INFOTYPE9003> GetAllEvaluation(string EmployeeID);

        List<INFOTYPE9004> GetAllJobGrade(string EmployeeID);

        List<INFOTYPE0022> GetAllCertificateHistory(string EmployeeID);

        void DeleteEducation(List<INFOTYPE0022> data);

        void SaveEducation(List<INFOTYPE0022> data);

        void DeleteFamilyData(List<INFOTYPE0021> data);

        INFOTYPE0008 GetInfotype0008(string EmployeeID, DateTime CheckDate);

        List<WorkPlaceData> GetWorkPlace();

        void SaveWorkPlace(List<WorkPlaceData> list);

        CommunicationData GetCommunicationData(string employeeID);

        void SaveCommunicationData(CommunicationData savedData, CommunicationData oldData);

        List<WorkPeriod> GetWorkHistory(string EmployeeID, string CompCode);

        RelatedAge GetRelatedAge(string EmployeeID, DateTime CheckDate);

        List<SALARYRATE> GetSalaryRate(string EmployeeID, string EmpSubGroup);
        List<SALARYRATE> GetSalaryRate(string EmployeeID);

        List<INFOTYPE0105> GetInfotype0105ByCategoryCode(string EmployeeID, string CategoryCode);

        List<PersonalAttachmentFileSet> GetAttachmentFileSet(string EmployeeID, int RequestTypeID, string RequestSubType);

        List<PersonalAttachmentFileSet> GetAttachmentExport(string EmployeeID, int RequestTypeID, string RequestSubType);

        void FileAttachmentSubTypeSave(string RequestNo, string RequestSubType);

        void UpdateINFOTYPE0105(INFOTYPE0105 oINF105);

        void InsertINFOTYPE0105(INFOTYPE0105 oINF105);

        void SaveINFOTYPE0105(List<INFOTYPE0105> oListINF105Update, List<INFOTYPE0105> oListINF105Insert);

        void DeleteInsertEducation(List<INFOTYPE0022> data);

        List<string> GetIDCardData(string EmployeeID);

        void ChangeFamilyPrefixByAge(string FamilyDataSubtype, List<string> SourceKey, List<string> MappingKey, int AgetoUpdate, string EmployeeID1, string EmployeeID2);

        List<InfotypeAutomaticManagement> GetAllInfotypeAutomaticList();

        DateTime? GetMaritalEffectiveDate(string EmployeeID);

        int CalculateChildNo(string EmployeeID);

        #region Driver License ������㺢Ѻ���

        List<PersonalDriverLicenseData> GetDriverLicenseByEmployee(string EmployeeCode);

        List<PersonalDriverLicenseData> GetDriverLicenseNotifyExpiredByMonth(int iMonth, int iYear);

        List<DataDriverLicenseByJob> CheckDuplicateDriverLicense(List<DataDriverLicenseByJob> model);

        void SaveDriverLicenseByJob(List<DataDriverLicenseByJob> model);

        List<PersonalDriverLicenseData> GetDriverLicenseEmployeeAll();

        #endregion

        List<EmployeeWorklocationSAP> LoadEmployeeInWorkLocation(string CompCode);
        List<MasterWorkLocationSAP> LoadWorkLocationMaster(string CompCode);
        int GetRequestByBoxId(int boxId, string employeeId, string positionId);
        void SaveOrUpdateEmployeeInWorkLocation(List<EmployeeWorklocationSAP> model);
        void SaveOrUpdateWorkLocationMaster(List<MasterWorkLocationSAP> model);

        List<AppraisalData> GetAppraisalData(string EmployeeID, int yearOffset, int chkYear, string CompCode);

        DbPeriodSettingPaySlipByAdmin GetConfigPeroidSettingPayslipByAdmin();
        decimal GetSalaryByEmployee(string employeeId);
        string GetWorkLocationNameByEmpID(string EmployeeID, DateTime CheckDate);
        List<DateTime> GetStartWorkAge(string EmployeeID);
        string GetMailSubjectNotifyExpiredDriverLicense(int MailID, string Language);
        List<DbPeriodSettingPaySlipByAdmin> GetAllPayslipPeriodSetting();
    }
}