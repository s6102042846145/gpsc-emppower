using System.Collections.Generic;
using ESS.HR.PA.CONFIG;

namespace ESS.HR.PA.INTERFACE
{
    public interface IHRPAConfigService
    {
        List<PAConfiguration> GetPAConfigurationList();

        List<TitleName> GetTitleList(string LanguageCode);

        TitleName GetTitle(string Code);

        TitleName GetTitle(string Code, string LanguageCode);

        void SaveTitleList(List<TitleName> data);

        List<PrefixName> GetPrefixNameList();

        PrefixName GetPrefixName(string Code);

        void SavePrefixNameList(List<PrefixName> data);

        List<NameFormat> GetNameFormatList();

        NameFormat GetNameFormat(string Code);

        void SaveNameFormatList(List<NameFormat> data);

        List<Gender> GetGenderList(string LanguageCode);

        List<Gender> GetGenderList();

        Gender GetGender(string Code);

        Gender GetGender(string Code, string LanguageCode);

        void SaveGenderList(List<Gender> data);

        List<Country> GetCountryList();

        //AddBy: Ratchatawan W. (2012-02-16)
        List<Country> GetCountryList(string LanguageCode);

        Country GetCountry(string Code, string LanguageCode);

        //AddBy: Ratchatawan W. (2012-02-17)
        List<Nationality> GetAllNationality(string LanguageCode);

        Nationality GetNationality(string Code, string LanguageCode);

        Country GetCountry(string Code);
        List<Region> GetRegionByCountryCode(string Code, string LanguageCode);

        void SaveCountryList(List<Country> data);

        List<MaritalStatus> GetMaritalStatusList();

        List<MaritalStatus> GetMaritalStatusList(string LanguageCode);

        MaritalStatus GetMaritalStatus(string Code);

        MaritalStatus GetMaritalStatus(string Code, string LanguageCode);

        void SaveMaritalStatusList(List<MaritalStatus> Data);

        List<Religion> GetReligionList(string LanguageCode);

        Religion GetReligion(string Code);

        Religion GetReligion(string Code, string LanguageCode);

        void SaveReligionList(List<Religion> Data);

        List<Language> GetLanguageList();

        Language GetLanguage(string Code);

        void SaveLanguageList(List<Language> Data);

        List<AddressType> GetAddressTypeList();

        List<AddressType> GetAddressTypeList(string LanguageCode);

        void SaveAddressTypeList(List<AddressType> Data);

        List<Bank> GetBankList();

        Bank GetBank(string Code);

        void SaveBankList(List<Bank> Data);

        FamilyMember GetFamilyMember(string Code);

        void SaveFamilyMemberList(List<FamilyMember> Data);

        List<FamilyMember> GetFamilyMemberList();

        //List<Province> GetProvinceList();

        //Province GetProvince(string Code);

        //List<Province> GetProvinceList(string LanguageCode);

        //List<Province> GetProvinceByCountryCode(string Code, string LanguageCode);
        //Province GetProvince(string Code, string LanguageCode);

        void SaveProvinceList(List<Province> Data);

        List<EducationLevel> GetAllEducationLevel();

        void SaveAllEducationLevel(List<EducationLevel> list);

        List<Vocation> GetAllVocation();

        void SaveAllVocation(List<Vocation> list);

        List<Institute> GetAllInstitute();

        void SaveAllInstitute(List<Institute> list);

        List<Honor> GetAllHonor();

        void SaveAllHonor(List<Honor> list);

        List<Branch> GetAllBranch();

        void SaveAllBranch(List<Branch> list);

        List<CompanyBusiness> GetAllCompanyBusiness();

        void SaveAllCompanyBusiness(List<CompanyBusiness> list);

        List<JobType> GetAllJobType();

        void SaveAllJobType(List<JobType> list);

        List<Certificate> GetAllCertificate();

        List<Certificate> GetCertificateByEducationLevel(string EducationLevel);

        void SaveAllCertificate(List<Certificate> list);

        Branch GetBranchData(string BranchCode);

        EducationLevel GetEducationLevel(string EducationLevelCode);

        Certificate GetCertificateCode(string CertificateCode);

        Vocation GetVocation(string VocationCode);

        CompanyBusiness GetCompanyBusiness(string CompanyBusinessCode);

        JobType GetJobType(string JobCode);

        Institute GetInstitute(string InstituteCode);

        List<EducationGroup> GetAllEducationGroup();

        EducationGroup GetEducationGroupByCode(string EducationGroupCode);

        List<CourseDurationUnit> GetAllUnit();

        List<string> GetCertCodeFromEduLevelCode(string educationLevelCode);

        List<string> GetBranchFromEduLevelCode(string educationLevelCode);

        List<Relationship> GetAllRelationship();

        void SaveAllRelationship(List<Relationship> list);

        List<Province> GetProvinceByCountryCode(string Code, string LanguageCode);
        List<Province> GetProvinceByCountryCodeForTravel(string Code, string LanguageCode);
        //string GetProvinceByCode(string oProvinceCode, string oLanguage);
        List<District> GetDistrictByProvinceCodeForTravel(string oCountryCode, string oProvinceCode, string oLanguageCode);

        bool ShowTimeAwareLink(int LinkID);
    }
}