﻿using System.Text;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ESS.HR.PA.DATACLASS;
using ESS.HR.PA.PptxTemplater;
using System.Drawing;

namespace ESS.HR.PA
{
    /*
     * ถ้าเจอ error Unable to cast COM object of type 'Microsoft.Office.Interop.PowerPoint.ApplicationClass' to interface type 'Microsoft.Office.Interop.PowerPoint._Application ให้ทำตามลิงค์นี้
     * https://social.technet.microsoft.com/wiki/contents/articles/18919.c-troubleshooting-unable-to-cast-com-object-of-type-microsoft-office-interop-powerpoint-applicationclass.aspx
     */
    public class PowerpointProfileBuilder
    {
        public enum Language
        {
            Thai,
            English
        }
        private string GetContentType(string path)
        {
            switch (Path.GetExtension(path).ToLower())
            {
                case ".jpg": return "image/jpeg";
                case ".jpeg": return "image/jpeg";
                case ".png": return "image/png";
            }
            return "image/jpeg";
        }
        private string AgeInYearsMonths(DateTime? from, DateTime? to, Language lang)
        {
            if (from == null) return "N/A";
            if (to == null) to = DateTime.Now;
            if (from >= to)
                throw new ArgumentException("DateOfBirth cannot be in future!");

            DateTime d = from.Value;
            int monthCount = 0;
            while ((d = d.AddMonths(1)) <= to)
            {
                monthCount++;
            }
            return string.Format("{0:d}.{1:d2}", monthCount / 12, monthCount % 12);
        }

        public void CreateProfileSlide(string path,Language lang, PowerpointProfileModel model,string path_template,byte[] byte_path)
        {
            string templateFile = "None";
            int yearModifier = 0;
            switch (lang)
            {
                case Language.English:
                    //templateFile = "template/GPSC Leadership_EN.PPTX";
                    templateFile = path_template;
                    break;
                case Language.Thai:
                    //templateFile = "template/GPSC Leadership_TH.PPTX";
                    templateFile = path_template;
                    yearModifier = 543;
                    break;
            }

            if (File.Exists(path))
            {
                File.Delete(path);
            }
            File.Copy(templateFile, path);

            var pptx = new Pptx(path, FileAccess.ReadWrite);
            PptxSlide slide = pptx.GetSlide(0);
            slide.ReplaceTag("{{name}}", model.Title + " " + model.Firstname + " " + model.Lastname, PptxSlide.ReplacementType.Global);
            slide.ReplaceTag("{{level}}", model.JobGrade, PptxSlide.ReplacementType.Global);
            slide.ReplaceTag("{{ageLevel}}", AgeInYearsMonths(model.JobGradeDate, DateTime.Now, lang), PptxSlide.ReplacementType.Global);
            //slide.ReplaceTag("{{age}}", AgeInYearsMonths(model.Birthdate, DateTime.Now, lang), PptxSlide.ReplacementType.Global);
            slide.ReplaceTag("{{age}}", model.agePersonal, PptxSlide.ReplacementType.Global);
            slide.ReplaceTag("{{company}}", model.Company, PptxSlide.ReplacementType.Global);
            slide.ReplaceTag("{{position}}", model.Position, PptxSlide.ReplacementType.Global);
            slide.ReplaceTag("{{position}}", model.Unit, PptxSlide.ReplacementType.Global);
            //slide.ReplaceTag("{{ageCompany}}", AgeInYearsMonths(model.EmployeeDate, DateTime.Now, lang), PptxSlide.ReplacementType.Global);
            slide.ReplaceTag("{{ageCompany}}", model.ageOfCompany, PptxSlide.ReplacementType.Global);
            //slide.ReplaceTag("{{ageJob}}", AgeInYearsMonths(model.PttPositionDate, DateTime.Now, lang), PptxSlide.ReplacementType.Global);
            slide.ReplaceTag("{{ageJob}}", model.ageOfLevel, PptxSlide.ReplacementType.Global);
            slide.ReplaceTag("{{retireYear}}", (model.RetireYear + yearModifier).ToString(), PptxSlide.ReplacementType.Global);
            slide.ReplaceTag("{{unit}}", model.Unit, PptxSlide.ReplacementType.Global);
            slide.ReplaceTag("{{retireDate}}", model.RetireDate.ToString("dd'/'MM'/'") + (model.RetireDate.Year + yearModifier).ToString(), PptxSlide.ReplacementType.Global);
            slide.ReplaceTag("{{asOfDate}}", model.AsOfDate.ToString("dd'/'MM'/'") + (model.AsOfDate.Year + yearModifier).ToString(), PptxSlide.ReplacementType.Global);
            if (model.ImagePath != null && byte_path != null)
            {
                //resize image
                Bitmap bitmap = new Bitmap(new MemoryStream(byte_path));

                // border size =  0.79 x 1.57

                Bitmap newImage = new Bitmap(790, 1570);
                using (var g = Graphics.FromImage(newImage))
                {
                    g.Clear(Color.White);
                    // เช็คว่า fit แนวไหน
                    var ratioImage = (double)bitmap.Width / bitmap.Height;
                    var ratioTarget = (double)newImage.Width / newImage.Height;

                    if (ratioImage > ratioTarget)
                    {
                        // fit แนววนอน
                        var newHeight = newImage.Width / ratioImage;
                        g.DrawImage(bitmap, 0, 0, newImage.Width, (float)newHeight);
                    } else
                    {
                        // fit แนวตั้ง
                        var newWidth = newImage.Height * ratioImage;
                        g.DrawImage(bitmap, (float)((newImage.Width - newWidth) / 2.0), 0, (float)newWidth, (float)newImage.Height);
                    }
                }
                using (var stream = new MemoryStream())
                {
                    newImage.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                    byte_path = stream.ToArray();
                }
                    
                //slide.ReplacePicture("{{picture1}}", model.ImagePath, GetContentType(model.ImagePath));
                slide.ReplacePicture("{{picture1}}",byte_path, GetContentType(model.ImagePath));
            }

            {
                var rows = new List<List<string>>();
                foreach (var edu in model.Educations.OrderByDescending(a => a.Year).Take(2).ToList())
                {
                    rows.Add(new List<string>()
                    {
                        edu.Level,
                        edu.Grade,
                        edu.Field,
                        edu.Institute,
                        (edu.Year + yearModifier).ToString(),
                        edu.Country

                    });
                }
                while (rows.Count() < 4)
                {
                    rows.Add(new List<string>() { "", "", "", "", "", "" });
                }
                for (int row = 0; row < rows.Count(); ++row)
                {
                    for (int col = 0; col < 6; ++col)
                    {
                        slide.ReplaceTag("{{edu" + (row + 1) + "_" + (col + 1) + "}}", rows[row][col], PptxSlide.ReplacementType.Global);
                    }
                }
            }

            {
                var rows = new List<List<string>>();
                foreach (var emp in model.Employements.OrderByDescending(a => a.End).Take(7).ToList())
                {
                    rows.Add(new List<string>()
                    {
                        emp.Start.ToString("dd'/'MM'/'") + (emp.Start.Year + yearModifier).ToString(),
                        emp.End != null ?
                            emp.End.Value.ToString("dd'/'MM'/'") + (emp.End.Value.Year + yearModifier).ToString():
                            lang == Language.English ? "Now" : "ปัจจุบัน",
                        //AgeInYearsMonths(emp.Start, emp.End, lang),
                        emp.AgeWorkTime,
                        emp.Position,
                        emp.Unit,
                        emp.Company,
                        
                    });
                }
                while (rows.Count() < 8)
                {
                    rows.Add(new List<string>() { "", "", "", "", "", "" });
                }
                for (int row = 0; row < rows.Count(); ++row)
                {
                    for (int col = 0; col < 6; ++col)
                    {
                        slide.ReplaceTag("{{emp" + (row + 1) + "_" + (col + 1) + "}}", rows[row][col], PptxSlide.ReplacementType.Global);
                    }
                }
            }
            {
                var rows = new List<List<string>>();
                foreach (var perf in model.Performances.OrderBy(a => a.Year).Take(5).ToList())
                {
                    rows.Add(new List<string>()
                    {
                        perf.Year.ToString(),
                        perf.Ranking,
                        perf.Result
                    });
                }
                while (rows.Count() < 5)
                {
                    rows.Add(new List<string>() { "", "", "", "", "" });
                }
                for (int row = 0; row < rows.Count(); ++row)
                {
                    for (int col = 0; col < 3; ++col)
                    {
                        slide.ReplaceTag("{{p" + (row + 1) + "_" + (col + 1) + "}}", rows[row][col], PptxSlide.ReplacementType.Global);
                    }
                }
            }

            pptx.Close();
        }
    }
}
