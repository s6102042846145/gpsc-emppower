using System;
using System.Collections.Generic;
using ESS.DATA;
using ESS.HR.PA.CONFIG;
using ESS.HR.PA.INFOTYPE;

namespace ESS.HR.PA
{
    public class PAServiceManagement
    {
        internal static INFOTYPE0002 GetPersonalData(string EmployeeID, DateTime checkDate)
        {
            return ServiceManager.HRPAService.GetPersonalData(EmployeeID, checkDate);
        }

        internal static List<EducationLevel> GetAllEducationLevel()
        {
            return ServiceManager.HRPAConfigService.GetAllEducationLevel();
        }

        internal static List<Relationship> GetAllRelationship()
        {
            return ServiceManager.HRPAConfigService.GetAllRelationship();
        }

        internal static void CopyEducationLevel()
        {
            ServiceManager.HRPAConfigService.SaveAllEducationLevel(ServiceManager.HRPAConfigSrc.GetAllEducationLevel());
        }

        internal static List<Certificate> GetAllCertificate()
        {
            return ServiceManager.HRPAConfigService.GetAllCertificate();
        }

        internal static void CopyAllCertificate()
        {
            ServiceManager.HRPAConfigService.SaveAllCertificate(ServiceManager.HRPAConfigSrc.GetAllCertificate());
        }

        internal static List<Vocation> GetAllVocation()
        {
            return ServiceManager.HRPAConfigService.GetAllVocation();
        }

        internal static void CopyAllVocation()
        {
            ServiceManager.HRPAConfigService.SaveAllVocation(ServiceManager.HRPAConfigSrc.GetAllVocation());
        }

        internal static List<Institute> GetAllInstitute()
        {
            return ServiceManager.HRPAConfigService.GetAllInstitute();
        }

        internal static Institute GetInstitute(string InstituteCode)
        {
            return ServiceManager.HRPAConfigService.GetInstitute(InstituteCode);
        }

        internal static void CopyInstitute()
        {
            ServiceManager.HRPAConfigService.SaveAllInstitute(ServiceManager.HRPAConfigSrc.GetAllInstitute());
        }

        internal static List<Honor> GetAllHonor()
        {
            return ServiceManager.HRPAConfigService.GetAllHonor();
        }

        internal static void CopyAllHonor()
        {
            ServiceManager.HRPAConfigService.SaveAllHonor(ServiceManager.HRPAConfigSrc.GetAllHonor());
        }

        internal static List<Branch> GetAllBranch()
        {
            return ServiceManager.HRPAConfigService.GetAllBranch();
        }

        internal static void CopyAllBranch()
        {
            ServiceManager.HRPAConfigService.SaveAllBranch(ServiceManager.HRPAConfigSrc.GetAllBranch());
        }

        public static List<INFOTYPE0022> GetEducation(string EmployeeID)
        {
            return ServiceManager.HRPAService.GetAllEducationHistory(EmployeeID);
        }

        internal static void SaveEducation(List<INFOTYPE0022> data)
        {
            ServiceManager.HRPAService.SaveEducation(data);
        }

        internal static void DeleteEducation(List<INFOTYPE0022> data)
        {
            ServiceManager.HRPAService.DeleteEducation(data);
        }

        internal static List<INFOTYPE0023> GetAllPreviousEmployers(string EmployeeID)
        {
            return ServiceManager.HRPAService.GetAllPreviousEmployers(EmployeeID);
        }

        internal static List<CompanyBusiness> GetAllCompanyBusiness()
        {
            return ServiceManager.HRPAConfigService.GetAllCompanyBusiness();
        }

        internal static void CopyAllCompanyBusiness()
        {
            ServiceManager.HRPAConfigService.SaveAllCompanyBusiness(ServiceManager.HRPAConfigSrc.GetAllCompanyBusiness());
        }

        internal static List<JobType> GetAllJobType()
        {
            return ServiceManager.HRPAConfigService.GetAllJobType();
        }

        internal static void CopyAllJobType()
        {
            ServiceManager.HRPAConfigService.SaveAllJobType(ServiceManager.HRPAConfigSrc.GetAllJobType());
        }

        internal static List<INFOTYPE9002> GetAllWorkHistory(string EmployeeID)
        {
            return ServiceManager.HRPAService.GetAllWorkHistory(EmployeeID);
        }

        internal static void Search(List<FilterData> filters)
        {
            foreach (FilterData filter in filters)
            {
                Search(filter);
            }
        }

        internal static void Search(FilterData filter)
        {
            if (filter.FilterName.StartsWith("INFOTYPE0002"))
            {
                ServiceManager.HRPAService.SearchInfotype0002(filter);
            }
        }

        internal static List<INFOTYPE9001> GetAllSalaryHistory(string EmployeeID)
        {
            return ServiceManager.HRPAService.GetAllSalaryHistory(EmployeeID);
        }

        internal static List<INFOTYPE0008> GetSalaryList(string EmployeeID)
        {
            return ServiceManager.HRPAService.GetSalaryList(EmployeeID);
        }

        internal static List<INFOTYPE9003> GetAllEvaluation(string EmployeeID)
        {
            return ServiceManager.HRPAService.GetAllEvaluation(EmployeeID);
        }

        internal static List<INFOTYPE9004> GetAllJobGrade(string EmployeeID)
        {
            return ServiceManager.HRPAService.GetAllJobGrade(EmployeeID);
        }

        internal static List<INFOTYPE0022> GetCertificate(string EmployeeID)
        {
            return ServiceManager.HRPAService.GetAllCertificateHistory(EmployeeID);
        }

        internal static Branch GetBranchData(string BranchCode)
        {
            return ServiceManager.HRPAConfigService.GetBranchData(BranchCode);
        }

        internal static EducationLevel GetEducationLevel(string EducationLevelCode)
        {
            return ServiceManager.HRPAConfigService.GetEducationLevel(EducationLevelCode);
        }

        internal static Certificate GetCertificateCode(string CertificateCode)
        {
            return ServiceManager.HRPAConfigService.GetCertificateCode(CertificateCode);
        }

        internal static Vocation GetVocation(string VocationCode)
        {
            return ServiceManager.HRPAConfigService.GetVocation(VocationCode);
        }

        internal static CompanyBusiness GetCompanyBusiness(string CompanyBusinessCode)
        {
            return ServiceManager.HRPAConfigService.GetCompanyBusiness(CompanyBusinessCode);
        }

        internal static JobType GetJobType(string JobCode)
        {
            return ServiceManager.HRPAConfigService.GetJobType(JobCode);
        }

        internal static List<CourseDurationUnit> GetAllUnit()
        {
            return ServiceManager.HRPAConfigService.GetAllUnit();
        }

        internal static List<string> GetCertCodeFromEduLevelCode(string educationLevelCode)
        {
            return ServiceManager.HRPAConfigService.GetCertCodeFromEduLevelCode(educationLevelCode);
        }

        internal static List<string> GetBranchFromEduLevelCode(string eduLevelCode)
        {
            return ServiceManager.HRPAConfigService.GetBranchFromEduLevelCode(eduLevelCode);
        }
    }
}