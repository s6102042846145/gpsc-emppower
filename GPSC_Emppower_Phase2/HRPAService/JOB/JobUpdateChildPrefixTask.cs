﻿using System;
using System.Collections.Generic;
using System.Globalization;
using ESS.JOB.ABSTRACT;
using System.Text;
using System.Threading.Tasks;


namespace ESS.HR.PA.JOB
{
    public class JobUpdateChildPrefixTask : AbstractTaskWorker
    {
        private string __familyDataSubtype = "";
        private List<string> __sourceKey = new List<string>();
        private List<string> __mappingKey = new List<string>();
        private string __agetoUpdate = "15";
        private string __empID1 = "";
        private string __empID2 = "";

        public JobUpdateChildPrefixTask()
        {
        }

        public string FamilyDataSubtype
        {
            get
            {
                return __familyDataSubtype;
            }
            set
            {
                __familyDataSubtype = value;
            }
        }

        public List<string> SourceKey
        {
            get
            {
                return __sourceKey;
            }
            set
            {
                __sourceKey = value;
            }
        }

        public List<string> MappingKey
        {
            get
            {
                return __mappingKey;
            }
            set
            {
                __mappingKey = value;
            }
        }

        public string AgetoUpdate
        {
            get
            {
                return __agetoUpdate;
            }
            set
            {
                __agetoUpdate = value;
            }
        }

        public string EmployeeID1
        {
            get
            {
                return __empID1;
            }
            set
            {
                __empID1 = value;
            }
        }

        public string EmployeeID2
        {
            get
            {
                return __empID2;
            }
            set
            {
                __empID2 = value;
            }
        }

        public override void Run()
        {
            HRPAManagement.CreateInstance(CompanyCode).ChangeFamilyPrefixByAge(FamilyDataSubtype, SourceKey, MappingKey, Convert.ToInt32(AgetoUpdate), EmployeeID1, EmployeeID2);
        }
    }
}
