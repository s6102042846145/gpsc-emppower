﻿using System;
using System.Collections.Generic;
using System.Globalization;
using ESS.JOB.ABSTRACT;
using ESS.JOB.INTERFACE;

namespace ESS.HR.PA.JOB
{
    public class JobUpdateChildPrefix : AbstractJobWorker
    {
        private CultureInfo oCL = new CultureInfo("en-US");

        public JobUpdateChildPrefix()
        {
        }

        public override List<ITaskWorker> LoadTasks()
        {
            List<ITaskWorker> oReturn = new List<ITaskWorker>();

            Console.WriteLine("Update Child Prefix");
            Console.WriteLine("========================================");
            JobUpdateChildPrefixTask oTask = new JobUpdateChildPrefixTask();

            //oTask.TaskID = 0;
            //oReturn.Add(oTask);
            oTask.FamilyDataSubtype = Params[0].Value;
            oTask.SourceKey.AddRange(Params[1].Value.Split(','));
            oTask.MappingKey.AddRange(Params[2].Value.Split(','));
            oTask.AgetoUpdate = Params[3].Value;
            oTask.EmployeeID1 = Params[4].Value;
            oTask.EmployeeID2 = Params[5].Value;
            oTask.TaskID = 0;
            oReturn.Add(oTask);

            return oReturn;
        }
    }
}
