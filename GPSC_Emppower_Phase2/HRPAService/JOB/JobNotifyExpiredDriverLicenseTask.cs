﻿using System;
using System.Collections.Generic;
using System.Globalization;
using ESS.JOB.ABSTRACT;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PA.JOB
{
    public class JobNotifyExpiredDriverLicenseTask : AbstractTaskWorker
    {
        private string __adminRole = "";
        private int __monthBeforeExpire;
        private int __requestMailID;

        private const string SUCCESS = "SUCCESS";
        private const string FINISH = "FINISH";
        private const string ERROR = "ERROR";
        private const string FromMode = "SAP";
        private const string ToMode = "DB";

        public JobNotifyExpiredDriverLicenseTask()
        {
        }

        public string AdminRole
        {
            get
            {
                return __adminRole;
            }
            set
            {
                __adminRole = value;
            }
        }

        public int MonthBeforeExpire
        {
            get
            {
                return __monthBeforeExpire;
            }
            set
            {
                __monthBeforeExpire = value;
            }
        }

        public int RequestMailID
        {
            get
            {
                return __requestMailID;
            }
            set
            {
                __requestMailID = value;
            }
        }

        public override void Run()
        {
            HRPAManagement oHRPAManagement = HRPAManagement.CreateInstance(CompanyCode);

            try
            {
                Console.WriteLine(":: Send Mail Notify Expired DriverLicense");
                HRPAManagement.CreateInstance(CompanyCode).SendMailDriverLicenseNotifyExpired(AdminRole, MonthBeforeExpire, RequestMailID, CompanyCode);

                Console.WriteLine(SUCCESS);
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
            }
            finally
            {
                Console.WriteLine(FINISH);
            }
                     
        }
    }
}
