using System;
using System.Collections.Generic;
using System.Globalization;
using ESS.JOB.ABSTRACT;
using ESS.JOB.INTERFACE;

namespace ESS.HR.PA.JOB
{
    public class JobCopyPAConfig : AbstractJobWorker
    {
        private CultureInfo oCL = new CultureInfo("en-US");

        public JobCopyPAConfig()
        {
        }

        public override List<ITaskWorker> LoadTasks()
        {
            List<ITaskWorker> oReturn = new List<ITaskWorker>();

            Console.WriteLine("Copy basic PA data");
            Console.WriteLine("========================================");
            JobCopyPAConfigTask oTask = new JobCopyPAConfigTask();
            oTask.TaskID = 0;
            oReturn.Add(oTask);

            return oReturn;
        }
    }
}