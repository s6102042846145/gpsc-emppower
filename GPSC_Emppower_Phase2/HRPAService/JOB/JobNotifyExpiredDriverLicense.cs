﻿using System;
using System.Collections.Generic;
using System.Globalization;
using ESS.JOB.ABSTRACT;
using ESS.JOB.INTERFACE;

namespace ESS.HR.PA.JOB
{
    public class JobNotifyExpiredDriverLicense : AbstractJobWorker
    {
        private CultureInfo oCL = new CultureInfo("en-US");

        public JobNotifyExpiredDriverLicense()
        {
        }

        public override List<ITaskWorker> LoadTasks()
        {
            List<ITaskWorker> oReturn = new List<ITaskWorker>();

            Console.WriteLine("Send Mail Notify Expired DriverLicense");
            Console.WriteLine("========================================");
            JobNotifyExpiredDriverLicenseTask oTask = new JobNotifyExpiredDriverLicenseTask();

            oTask.AdminRole = Params[0].Value;
            oTask.MonthBeforeExpire = Convert.ToInt32(Params[1].Value);
            oTask.RequestMailID = Convert.ToInt32(Params[2].Value);
            oTask.TaskID = 0;
            oReturn.Add(oTask);

            return oReturn;
        }
    }
}
