﻿using ESS.JOB.ABSTRACT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PA.JOB
{
    public class JobLoadDriverLicenseConfigTask : AbstractTaskWorker
    {
        private const string SUCCESS = "SUCCESS";
        private const string FINISH = "FINISH";
        private const string ERROR = "ERROR";
        private const string FromMode = "SAP";
        private const string ToMode = "DB";

        public override void Run()
        {
            HRPAManagement oHRPAManagement = HRPAManagement.CreateInstance(CompanyCode);

            try
            {
                Console.WriteLine(":: Load Driver License");
                oHRPAManagement.LoadDriverLicenseEmployeeGPSC();
                Console.WriteLine(SUCCESS);
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
            }
            finally
            {
                Console.WriteLine(FINISH);
            }
        }
    }
}
