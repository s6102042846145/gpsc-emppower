using System;
using ESS.HR.PA.CONFIG;
using ESS.JOB.ABSTRACT;
using ESS.WORKFLOW;

namespace ESS.HR.PA.JOB
{
    public class JobCopyPAConfigTask : AbstractTaskWorker
    {
        #region Variable

        private const string SUCCESS = "SUCCESS";
        private const string FINISH = "FINISH";
        private const string ERROR = "ERROR";
        private const string FromMode = "SAP";
        private const string ToMode = "DB";

        #endregion Variable

        public override void Run()
        {
            HRPAManagement oHRPAManagement = HRPAManagement.CreateInstance(CompanyCode);

            #region TITLENAME

            try
            {
                Console.WriteLine(":: Copy data TITLENAME");
                oHRPAManagement.SaveTo(oHRPAManagement.GetAllTitleNameList(FromMode), ToMode);
                Console.WriteLine(SUCCESS);
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion TITLENAME

            #region PREFIXNAME

            try
            {
                Console.WriteLine(":: Copy data PREFIXNAME");
                oHRPAManagement.SaveTo(oHRPAManagement.GetAllPrefixName(FromMode), ToMode);
                Console.WriteLine(SUCCESS);
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion PREFIXNAME

            #region NAMEFORMAT

            try
            {
                Console.WriteLine(":: Copy data NAMEFORMAT");
                oHRPAManagement.SaveTo(oHRPAManagement.GetAllNameFormat(FromMode), ToMode);
                Console.WriteLine(SUCCESS);
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion NAMEFORMAT

            #region GENDER

            try
            {
                Console.WriteLine(":: Copy data GENDER");
                oHRPAManagement.SaveTo(oHRPAManagement.GetAllGenderList(FromMode), ToMode);
                Console.WriteLine(SUCCESS);
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion GENDER

            #region COUNTRY

            try
            {
                Console.WriteLine(":: Copy data COUNTRY");
                oHRPAManagement.SaveTo(oHRPAManagement.GetAllCountry(FromMode), ToMode);
                Console.WriteLine(SUCCESS);
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion COUNTRY

            #region MARITALSTATUS

            try
            {
                Console.WriteLine(":: Copy data MARITALSTATUS");
                oHRPAManagement.SaveTo(oHRPAManagement.GetAllMaritalStatus("", FromMode), ToMode);
                Console.WriteLine(SUCCESS);
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion MARITALSTATUS

            #region RELIGION

            try
            {
                Console.WriteLine(":: Copy data RELIGION");
                oHRPAManagement.SaveTo(oHRPAManagement.GetAllReligion("", FromMode), ToMode);
                Console.WriteLine(SUCCESS);
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion RELIGION

            #region LANGUAGE

            try
            {
                Console.WriteLine(":: Copy data LANGUAGE");
                oHRPAManagement.SaveTo(oHRPAManagement.GetAllLanguage(FromMode), ToMode);
                Console.WriteLine(SUCCESS);
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion LANGUAGE

            #region ADDRESSTYPE

            try
            {
                Console.WriteLine(":: Copy data ADDRESSTYPE");
                oHRPAManagement.SaveTo(oHRPAManagement.GetAllAddressType(FromMode), ToMode);
                Console.WriteLine(SUCCESS);
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion ADDRESSTYPE

            //Move BANK to JobCopyEmployeeConfig by Koissares 20200417
            #region BANK 

            //try
            //{
            //    Console.WriteLine(":: Copy data BANK");
            //    oHRPAManagement.SaveTo(oHRPAManagement.GetAllBank(FromMode), ToMode);
            //    Console.WriteLine(SUCCESS);
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
            //}
            //finally
            //{
            //    Console.WriteLine(FINISH);
            //}

            #endregion BANK

            #region CERTIFICATE

            try
            {
                Console.WriteLine(":: Copy data CERTIFICATE");
                oHRPAManagement.SaveTo(oHRPAManagement.GetAllCertificate(FromMode), ToMode);
                Console.WriteLine(SUCCESS);
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion CERTIFICATE

            #region BRANCH

            try
            {
                Console.WriteLine(":: Copy data BRANCH");
                oHRPAManagement.SaveTo(oHRPAManagement.GetAllBranch(FromMode), ToMode);
                Console.WriteLine(SUCCESS);
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion BRANCH

            #region EDUCATIONLEVEL

            try
            {
                Console.WriteLine("Copy data EDUCATIONLEVEL");
                oHRPAManagement.SaveTo(oHRPAManagement.GetAllEducationLevel(FromMode), ToMode);
                Console.WriteLine(SUCCESS);
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion EDUCATIONLEVEL

            //Move INSTITUTE to JobCopyEmployeeConfig by Koissares 20200417
            #region INSTITUTE

            //try
            //{
            //    Console.WriteLine(":: Copy data INSTITUTE");
            //    oHRPAManagement.SaveTo(oHRPAManagement.GetAllInstitute(FromMode), ToMode);
            //    Console.WriteLine(SUCCESS);
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
            //}
            //finally
            //{
            //    Console.WriteLine(FINISH);
            //}

            #endregion INSTITUTE

            #region FAMILYMEMBER

            try
            {
                Console.WriteLine(":: Copy data FAMILYMEMBER");
                oHRPAManagement.SaveTo(oHRPAManagement.GetAllFamilyMember(FromMode), ToMode);
                Console.WriteLine(SUCCESS);
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion FAMILYMEMBER

            #region RELATHIONSHIP BETWEEN EDUCATIONLEVEL AND CERTIFICATE

            try
            {
                Console.WriteLine("Copy data RELATHIONSHIP BETWEEN EDUCATIONLEVEL AND CERTIFICATE");
                oHRPAManagement.SaveTo(oHRPAManagement.GetAllRelationship(FromMode), ToMode);
                Console.WriteLine(SUCCESS);
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("{0} : {1}", ERROR, ex.Message));
            }
            finally
            {
                Console.WriteLine(FINISH);
            }

            #endregion RELATHIONSHIP BETWEEN EDUCATIONLEVEL AND CERTIFICATE
        }
    }
}