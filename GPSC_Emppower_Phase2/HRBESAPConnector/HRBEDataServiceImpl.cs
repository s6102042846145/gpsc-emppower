using System;
using System.Collections.Generic;
using System.Globalization;
using ESS.HR.BE.ABSTRACT;
using ESS.HR.BE.DATACLASS;
using ESS.HR.BE.INFOTYPE;
using ESS.SHAREDATASERVICE;
using SAP.Connector;
using SAPInterface;
using ESS.DATA;
using SAPInterfaceExt;
using System.Linq;

namespace ESS.HR.BE.SAP
{
    public class HRBEDataServiceImpl : AbstractHRBEDataService
    {
        CultureInfo oCL = new CultureInfo("en-US");
        int Start_Age;
        int End_Age;
        int Person_Num;
        int HealthInsAllowanceBeginAge;
        int HealthInsAllowanceEndAge;

        #region Constructor
        public HRBEDataServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
            Start_Age = Convert.ToInt32(ShareDataManagement.LookupCache(oCompanyCode, "ESS.HR.BE.SAP", "START_AGE"));
            End_Age = Convert.ToInt32(ShareDataManagement.LookupCache(oCompanyCode, "ESS.HR.BE.SAP", "END_AGE"));
            Person_Num = Convert.ToInt32(ShareDataManagement.LookupCache(oCompanyCode, "ESS.HR.BE.SAP", "PERSON_NUM"));

            HealthInsAllowanceBeginAge = Convert.ToInt32(ShareDataManagement.LookupCache(oCompanyCode, "ESS.HR.BE.SAP", "HealthInsAllowanceBeginAge"));
            HealthInsAllowanceEndAge = Convert.ToInt32(ShareDataManagement.LookupCache(oCompanyCode, "ESS.HR.BE.SAP", "HealthInsAllowanceEndAge"));
        }

        #endregion Constructor

        #region Member
        private CultureInfo DefaultCultureInfo = CultureInfo.GetCultureInfo("en-GB");
        //private Dictionary<string, string> Config { get; set; }

        private static string ModuleID = "ESS.HR.BE.SAP";
        public string CompanyCode { get; set; }
        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }
        private string Sap_Version
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAP_VERSION");

                //if (config == null || config.AppSettings.Settings["SAP_VERSION"] == null)
                //{
                //    return "6";
                //}
                //else
                //{
                //    return config.AppSettings.Settings["SAP_VERSION"].Value;
                //}
            }
        }

        #endregion Member


        public override List<INFOTYPE0021> GetChildsData(string EmployeeID)
        {
            List<INFOTYPE0021> returnValue = new List<INFOTYPE0021>();
            List<INFOTYPE0021> returnValue_select = new List<INFOTYPE0021>();
            Connection oConnect = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnect;

            TAB512Table oTable = new TAB512Table();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();

            TAB512Table oTable2 = new TAB512Table();
            RFC_DB_OPTTable Options2 = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields2 = new RFC_DB_FLDTable();

            RFC_DB_OPT opt, opt2;
            RFC_DB_FLD fld, fld2;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FAMSA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OBJPS";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FNMZU";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FAVOR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FANAM";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FGBDT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FASEX";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FGBOT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FGBLD";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FANAT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SUBTY";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OBJPS";
            Fields.Add(fld);

            //Get Begda
            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            //Get Endda
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            #region INF0187

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "FTXID"; //Identity Number for Father
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "MTXID"; //Identity Number for Mother
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "STXID"; //Identity Number for Spouse
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "SFXID"; //Identity Number for Spouse's Father
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "SMXID"; //Identity Number for Spouse's Mother
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "ANRED"; //Form-of-Address Key
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "EFAML"; //Employer of Family Member (TH)
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "JOBTL"; //Job Title
            Fields2.Add(fld2);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SUBTY"; //Subtype
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OBJPS"; //Object Identification
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ALDEC"; //Deceased Indicator
            Fields2.Add(fld);


            fld = new RFC_DB_FLD();
            fld.Fieldname = "STRAS"; // Address Number, Soi Trok (TH)
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "LOCAT"; //Street/Road, Tambol
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ORT02"; // District (TH)
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ORT01"; // Province
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PSTLZ"; // Postal Code
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "LAND1"; // Country Key
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "TELNR"; // Telephone Number
            Fields2.Add(fld);

            #endregion INF0187

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR = '{0}' AND SUBTY ='2'", EmployeeID);
            Options.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND BEGDA <= '{0}' and ENDDA >= '{0}'", DateTime.Now.ToString("yyyyMMdd", DefaultCultureInfo));
            Options.Add(opt);

            opt2 = new RFC_DB_OPT();
            opt2.Text = string.Format("PERNR = '{0}'", EmployeeID);
            Options2.Add(opt2);
            opt2 = new RFC_DB_OPT();
            opt2.Text = string.Format("AND BEGDA <= '{0}' and ENDDA >= '{0}'", DateTime.Now.ToString("yyyyMMdd", DefaultCultureInfo));
            Options2.Add(opt2);

            #endregion " Options "

            oFunction.Zrfc_Read_Table("^", "", "PA0021", 0, 0, ref oTable, ref Fields, ref Options);
            oFunction.Zrfc_Read_Table("^", "", "PA0187", 0, 0, ref oTable2, ref Fields2, ref Options2);

            Connection.ReturnConnection(oConnect);
            oFunction.Dispose();
            foreach (TAB512 item in oTable)
            {
                string tempSub1, temp1;
                INFOTYPE0021 IF21 = new INFOTYPE0021();
                string[] buffer = item.Wa.Split('^');
                //IF21.EmployeeID = EmployeeID;
                IF21.FamilyMember = buffer[0].Trim();
                IF21.ChildNo = buffer[1].Trim();
                IF21.TitleRank = buffer[2].Trim();
                IF21.Name = buffer[3].Trim();
                IF21.Surname = buffer[4].Trim();
                DateTime bufferDate;
                DateTime.TryParseExact(buffer[5].Trim(), "yyyyMMdd", DefaultCultureInfo, DateTimeStyles.None, out bufferDate);
                IF21.BirthDate = bufferDate;
                IF21.Age = CalculateAge(bufferDate);
                IF21.Sex = buffer[6].Trim();
                IF21.BirthPlace = buffer[7].Trim();
                IF21.CityOfBirth = buffer[8].Trim();
                IF21.Nationality = buffer[9].Trim();
                //IF21.SubType = buffer[10].Trim();

                DateTime oTempDatetime;
                DateTime.TryParseExact(buffer[12].Trim(), "yyyyMMdd", DefaultCultureInfo, DateTimeStyles.None, out oTempDatetime);
                //IF21.BeginDate = oTempDatetime;
                DateTime.TryParseExact(buffer[13].Trim(), "yyyyMMdd", DefaultCultureInfo, DateTimeStyles.None, out oTempDatetime);
                //IF21.EndDate = oTempDatetime;

                tempSub1 = buffer[10].Trim();
                temp1 = buffer[11].Trim();
                string tempSub2, temp2;
                foreach (TAB512 item2 in oTable2)
                {
                    string[] buffer2 = item2.Wa.Split('^');
                    tempSub2 = buffer2[8].Trim();
                    temp2 = buffer2[9].Trim();
                    if (tempSub1 == tempSub2 && temp1 == temp2)
                    {
                        IF21.FatherID = buffer2[0].Trim();
                        IF21.MotherID = buffer2[1].Trim();
                        IF21.SpouseID = buffer2[2].Trim();
                        IF21.FatherSpouseID = buffer2[3].Trim();
                        IF21.MotherSpouseID = buffer2[4].Trim();
                        IF21.TitleName = buffer2[5].Trim();
                        IF21.Employer = buffer2[6].Trim();
                        IF21.JobTitle = buffer2[7].Trim();
                        IF21.Dead = buffer2[10].Trim();
                        IF21.Address = buffer2[11].Trim();
                        IF21.Street = buffer2[12].Trim();
                        IF21.District = buffer2[13].Trim();
                        IF21.City = buffer2[14].Trim();
                        IF21.Postcode = buffer2[15].Trim();
                        IF21.Country = buffer2[16].Trim();
                        IF21.TelephoneNo = buffer2[17].Trim();
                    }
                }

                returnValue_select.Add(IF21);
            }

            //�ص��ѧ�ժ��Ե����
            int check_person = 1;
            foreach (var item in returnValue_select.OrderBy(x => x.BirthDate).ToList())
            {
                if (item.Dead == "")
                {
                    // �ص������������ҧ 3-25 ��
                    var Age = CalculateAge(item.BirthDate);
                    if (Age >= Start_Age && Age <= End_Age)
                    {
                        //�ص� 3 ���á
                        if (check_person <= Person_Num)
                        {
                            returnValue.Add(item);
                            check_person++;
                        }
                    }
                    else if (Age > End_Age)
                    {
                        check_person++;
                    }
                }
            }

            return returnValue;
        }


        public override INFOTYPE0002 GetPersonalData(string EmployeeID, DateTime CheckDate)
        {
            List<INFOTYPE0002> list = this.GetInfotype0002Data(EmployeeID, EmployeeID, CheckDate);
            if (list.Count > 0)
            {
                return list[0];
            }
            else
            {
                return null;
            }
        }

        public override List<INFOTYPE0002> GetInfotype0002List(string EmployeeID1, string EmployeeID2)
        {
            return GetInfotype0002Data(EmployeeID1, EmployeeID2, DateTime.Now);
        }

        private List<INFOTYPE0002> GetInfotype0002Data(string EmployeeID1, string EmployeeID2, DateTime CheckDate)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            List<INFOTYPE0002> oReturn = new List<INFOTYPE0002>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SUBTY";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ANRED";
            Fields.Add(fld);

            if (Sap_Version == "6")
            {
                fld = new RFC_DB_FLD();
                fld.Fieldname = "NAMZU";
                Fields.Add(fld);
            }
            else
            {
                fld = new RFC_DB_FLD();
                fld.Fieldname = "TITEL";
                Fields.Add(fld);
            }

            fld = new RFC_DB_FLD();
            fld.Fieldname = "VORNA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "NACHN";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "INITS";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "RUFNM";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "KNZNM";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "GBDAT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "GESCH";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "GBORT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "GBLND";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "NATIO";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FAMST";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FAMDT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ANZKD";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "KONFE";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SPRSL";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "GBDEP";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "GBORT";

            //Add By Kiattiwat 13-02-2012
            //22
            fld = new RFC_DB_FLD();
            fld.Fieldname = "TITEL";
            Fields.Add(fld);

            //23
            fld = new RFC_DB_FLD();
            fld.Fieldname = "TITL2";
            Fields.Add(fld);

            //24
            fld = new RFC_DB_FLD();
            fld.Fieldname = "VORSW";
            Fields.Add(fld);

            //25
            fld = new RFC_DB_FLD();
            fld.Fieldname = "VORS2";
            Fields.Add(fld);

            //26
            fld = new RFC_DB_FLD();
            fld.Fieldname = "NATI2";
            Fields.Add(fld);

            //27
            fld = new RFC_DB_FLD();
            fld.Fieldname = "NATI3";
            Fields.Add(fld);

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR >= '{0}' AND PERNR <= '{1}'", EmployeeID1, EmployeeID2);
            Options.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND BEGDA <= '{0}' and ENDDA >= '{0}'", CheckDate.ToString("yyyyMMdd", DefaultCultureInfo));
            Options.Add(opt);

            #endregion " Options "

            oFunction.Zrfc_Read_Table("^", "", "PA0002", 0, 0, ref Data, ref Fields, ref Options);
            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            #region " Parse Object "

            INFOTYPE0002 data;
            foreach (TAB512 item in Data)
            {
                data = new INFOTYPE0002();
                string[] arrTemp = item.Wa.Split('^');
                //fld.Fieldname = "PERNR";
                data.EmployeeID = arrTemp[0].Trim();
                //fld.Fieldname = "SUBTY";
                data.SubType = arrTemp[1].Trim();
                //fld.Fieldname = "BEGDA";
                data.BeginDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", oCL);
                //fld.Fieldname = "ENDDA";
                data.EndDate = DateTime.ParseExact(arrTemp[3], "yyyyMMdd", oCL);
                //fld.Fieldname = "ANRED";
                data.TitleID = arrTemp[4].Trim();
                //fld.Fieldname = "TITEL";
                data.Prefix = arrTemp[5].Trim();
                //fld.Fieldname = "VORNA";
                data.FirstName = arrTemp[6].Trim();
                //fld.Fieldname = "NACHN";
                data.LastName = arrTemp[7].Trim();
                //fld.Fieldname = "INITS";
                data.Initial = arrTemp[8].Trim();
                //fld.Fieldname = "RUFNM";
                data.NickName = arrTemp[9].Trim();
                //fld.Fieldname = "KNZNM";
                data.NameFormat = arrTemp[10].Trim();
                //fld.Fieldname = "GBDAT";
                data.DOB = DateTime.ParseExact(arrTemp[11], "yyyyMMdd", oCL);
                //fld.Fieldname = "GESCH";
                data.Gender = arrTemp[12].Trim();
                //fld.Fieldname = "GBORT";
                data.BirthPlace = arrTemp[13].Trim();
                //fld.Fieldname = "GBLND";
                data.BirthCity = arrTemp[14].Trim();
                //fld.Fieldname = "NATIO";
                data.Nationality = arrTemp[15].Trim();
                //fld.Fieldname = "FAMST";
                data.MaritalStatus = arrTemp[16].Trim();
                //fld.Fieldname = "FAMDT";
                if (arrTemp[17].Trim() == "00000000")
                {
                    data.MaritalEffectiveDate = DateTime.MinValue;
                }
                else
                {
                    data.MaritalEffectiveDate = DateTime.ParseExact(arrTemp[17], "yyyyMMdd", oCL);
                }
                //fld.Fieldname = "ANZKD";
                data.NoOfChild = int.Parse(arrTemp[18].Trim());
                //fld.Fieldname = "KONFE";
                data.Religion = arrTemp[19].Trim();
                //fld.Fieldname = "SPRSL";
                data.Language = arrTemp[20].Trim();
                //Add By Kiattiwat 13-02-2012
                //fld.Fieldname = "TITEL";
                data.MilitaryTitle = arrTemp[22].Trim();
                //fld.Fieldname = "TITL2";
                data.SecondTitle = arrTemp[23].Trim();
                //fld.Fieldname = "VORSW";
                data.NamePrefix1 = arrTemp[24].Trim();
                //fld.Fieldname = "VORS2";
                data.NamePrefix2 = arrTemp[25].Trim();
                //fld.Fieldname = "NATI2";
                data.SecondNationality = arrTemp[26].Trim();
                //fld.Fieldname = "NATI3";
                data.ThirdNationality = arrTemp[27].Trim();
                oReturn.Add(data);
            }

            #endregion " Parse Object "

            return oReturn;
        }


        public override List<INFOTYPE0021> GetFamilyDataB(string EmployeeID)
        {
            List<INFOTYPE0021> returnValue = new List<INFOTYPE0021>();
            Connection oConnect = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnect;

            TAB512Table oTable = new TAB512Table();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();

            TAB512Table oTable2 = new TAB512Table();
            RFC_DB_OPTTable Options2 = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields2 = new RFC_DB_FLDTable();

            RFC_DB_OPT opt, opt2;
            RFC_DB_FLD fld, fld2;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FAMSA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OBJPS";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FNMZU";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FAVOR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FANAM";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FGBDT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FASEX";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FGBOT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FGBLD";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FANAT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SUBTY";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OBJPS";
            Fields.Add(fld);

            //Get Begda
            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            //Get Endda
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            #region INF0187

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "FTXID"; //Identity Number for Father
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "MTXID"; //Identity Number for Mother
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "STXID"; //Identity Number for Spouse
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "SFXID"; //Identity Number for Spouse's Father
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "SMXID"; //Identity Number for Spouse's Mother
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "ANRED"; //Form-of-Address Key
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "EFAML"; //Employer of Family Member (TH)
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "JOBTL"; //Job Title
            Fields2.Add(fld2);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SUBTY"; //Subtype
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OBJPS"; //Object Identification
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ALDEC"; //Deceased Indicator
            Fields2.Add(fld);

            //Addby:Nuttapon.a 11.02.2013 adress
            fld = new RFC_DB_FLD();
            fld.Fieldname = "STRAS"; // Address Number, Soi Trok (TH)
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "LOCAT"; //Street/Road, Tambol
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ORT02"; // District (TH)
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ORT01"; // Province
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PSTLZ"; // Postal Code
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "LAND1"; // Country Key
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "TELNR"; // Telephone Number
            Fields2.Add(fld);

            #endregion INF0187

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR = '{0}' AND SUBTY <> '10'", EmployeeID);
            Options.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND BEGDA <= '{0}' and ENDDA >= '{0}'", DateTime.Now.ToString("yyyyMMdd", DefaultCultureInfo));
            Options.Add(opt);

            opt2 = new RFC_DB_OPT();
            opt2.Text = string.Format("PERNR = '{0}'", EmployeeID);
            Options2.Add(opt2);
            opt2 = new RFC_DB_OPT();
            opt2.Text = string.Format("AND BEGDA <= '{0}' and ENDDA >= '{0}'", DateTime.Now.ToString("yyyyMMdd", DefaultCultureInfo));
            Options2.Add(opt2);

            #endregion " Options "

            oFunction.Zrfc_Read_Table("^", "", "PA0021", 0, 0, ref oTable, ref Fields, ref Options);
            oFunction.Zrfc_Read_Table("^", "", "PA0187", 0, 0, ref oTable2, ref Fields2, ref Options2);

            Connection.ReturnConnection(oConnect);
            oFunction.Dispose();
            foreach (TAB512 item in oTable)
            {
                string tempSub1, temp1;
                INFOTYPE0021 IF21 = new INFOTYPE0021();
                string[] buffer = item.Wa.Split('^');
                IF21.EmployeeID = EmployeeID;
                IF21.FamilyMember = buffer[0].Trim();
                IF21.ChildNo = buffer[1].Trim();
                IF21.TitleRank = buffer[2].Trim();
                IF21.Name = buffer[3].Trim();
                IF21.Surname = buffer[4].Trim();
                DateTime bufferDate;
                DateTime.TryParseExact(buffer[5].Trim(), "yyyyMMdd", DefaultCultureInfo, DateTimeStyles.None, out bufferDate);
                IF21.BirthDate = bufferDate;
                IF21.Sex = buffer[6].Trim();
                IF21.BirthPlace = buffer[7].Trim();
                IF21.CityOfBirth = buffer[8].Trim();
                IF21.Nationality = buffer[9].Trim();
                IF21.SubType = buffer[10].Trim();
                IF21.Age = CalculateAge(bufferDate);
                //Addby:Nuttapon.a 07.02.2013
                DateTime oTempDatetime;
                DateTime.TryParseExact(buffer[12].Trim(), "yyyyMMdd", DefaultCultureInfo, DateTimeStyles.None, out oTempDatetime);
                IF21.BeginDate = oTempDatetime;
                DateTime.TryParseExact(buffer[13].Trim(), "yyyyMMdd", DefaultCultureInfo, DateTimeStyles.None, out oTempDatetime);
                IF21.EndDate = oTempDatetime;

                tempSub1 = buffer[10].Trim();
                temp1 = buffer[11].Trim();
                string tempSub2, temp2;
                foreach (TAB512 item2 in oTable2)
                {
                    string[] buffer2 = item2.Wa.Split('^');
                    tempSub2 = buffer2[8].Trim();
                    temp2 = buffer2[9].Trim();
                    if (tempSub1 == tempSub2 && temp1 == temp2)
                    {
                        IF21.FatherID = buffer2[0].Trim();
                        IF21.MotherID = buffer2[1].Trim();
                        IF21.SpouseID = buffer2[2].Trim();
                        IF21.FatherSpouseID = buffer2[3].Trim();
                        IF21.MotherSpouseID = buffer2[4].Trim();
                        IF21.TitleName = buffer2[5].Trim();
                        IF21.Employer = buffer2[6].Trim();
                        IF21.JobTitle = buffer2[7].Trim();
                        //AddBY: Ratchatawan W. (2011-10-14)
                        IF21.Dead = buffer2[10].Trim();
                        //AddBy: Nuttapon.a 11.02.2013
                        IF21.Address = buffer2[11].Trim();
                        IF21.Street = buffer2[12].Trim();
                        IF21.District = buffer2[13].Trim();
                        IF21.City = buffer2[14].Trim();
                        IF21.Postcode = buffer2[15].Trim();
                        IF21.Country = buffer2[16].Trim();
                        IF21.TelephoneNo = buffer2[17].Trim();
                    }
                }

                returnValue.Add(IF21);
            }
            return returnValue;
        }

        public override List<INFOTYPE0021> GetFamilyData(string EmployeeID, string SubType)
        {
            List<INFOTYPE0021> returnValue = new List<INFOTYPE0021>();
            List<INFOTYPE0021> returnValue_select = new List<INFOTYPE0021>();
            Connection oConnect = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnect;

            TAB512Table oTable = new TAB512Table();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();

            TAB512Table oTable2 = new TAB512Table();
            RFC_DB_OPTTable Options2 = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields2 = new RFC_DB_FLDTable();

            RFC_DB_OPT opt, opt2;
            RFC_DB_FLD fld, fld2;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FAMSA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OBJPS";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FNMZU";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FAVOR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FANAM";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FGBDT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FASEX";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FGBOT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FGBLD";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FANAT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SUBTY";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OBJPS";
            Fields.Add(fld);

            //Get Begda
            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            //Get Endda
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            #region INF0187

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "FTXID"; //Identity Number for Father
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "MTXID"; //Identity Number for Mother
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "STXID"; //Identity Number for Spouse
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "SFXID"; //Identity Number for Spouse's Father
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "SMXID"; //Identity Number for Spouse's Mother
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "ANRED"; //Form-of-Address Key
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "EFAML"; //Employer of Family Member (TH)
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "JOBTL"; //Job Title
            Fields2.Add(fld2);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SUBTY"; //Subtype
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OBJPS"; //Object Identification
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ALDEC"; //Deceased Indicator
            Fields2.Add(fld);


            fld = new RFC_DB_FLD();
            fld.Fieldname = "STRAS"; // Address Number, Soi Trok (TH)
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "LOCAT"; //Street/Road, Tambol
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ORT02"; // District (TH)
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ORT01"; // Province
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PSTLZ"; // Postal Code
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "LAND1"; // Country Key
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "TELNR"; // Telephone Number
            Fields2.Add(fld);

            #endregion INF0187

            #endregion " Fields "

            #region " Options "


            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR = '{0}'", EmployeeID);
            Options.Add(opt);

            string[] SubTypeItem = SubType.Split(',');
            var OptionSubType = "";
            foreach (string Item in SubTypeItem)
            {
                OptionSubType += " SUBTY = '" + Item + "' OR";
            }
            OptionSubType = OptionSubType.Remove(OptionSubType.Length - 1);
            OptionSubType = OptionSubType.Remove(OptionSubType.Length - 1);
            OptionSubType = " AND (" + OptionSubType + ")";

            opt = new RFC_DB_OPT();
            opt.Text = string.Format(OptionSubType);
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND BEGDA <= '{0}' and ENDDA >= '{0}'", DateTime.Now.ToString("yyyyMMdd", DefaultCultureInfo));
            Options.Add(opt);

            opt2 = new RFC_DB_OPT();
            opt2.Text = string.Format("PERNR = '{0}'", EmployeeID);
            Options2.Add(opt2);
            opt2 = new RFC_DB_OPT();
            opt2.Text = string.Format("AND BEGDA <= '{0}' and ENDDA >= '{0}'", DateTime.Now.ToString("yyyyMMdd", DefaultCultureInfo));
            Options2.Add(opt2);

            #endregion " Options "

            oFunction.Zrfc_Read_Table("^", "", "PA0021", 0, 0, ref oTable, ref Fields, ref Options);
            oFunction.Zrfc_Read_Table("^", "", "PA0187", 0, 0, ref oTable2, ref Fields2, ref Options2);

            Connection.ReturnConnection(oConnect);
            oFunction.Dispose();
            foreach (TAB512 item in oTable)
            {
                string tempSub1, temp1;
                INFOTYPE0021 IF21 = new INFOTYPE0021();
                string[] buffer = item.Wa.Split('^');
                //IF21.EmployeeID = EmployeeID;
                IF21.FamilyMember = buffer[0].Trim();
                IF21.ChildNo = buffer[1].Trim();
                IF21.TitleRank = buffer[2].Trim();
                IF21.Name = buffer[3].Trim();
                IF21.Surname = buffer[4].Trim();
                DateTime bufferDate;
                DateTime.TryParseExact(buffer[5].Trim(), "yyyyMMdd", DefaultCultureInfo, DateTimeStyles.None, out bufferDate);
                IF21.BirthDate = bufferDate;
                IF21.Age = CalculateAge(bufferDate);
                IF21.Age0101 = CalculateYourAge(bufferDate);
                IF21.Sex = buffer[6].Trim();
                IF21.BirthPlace = buffer[7].Trim();
                IF21.CityOfBirth = buffer[8].Trim();
                IF21.Nationality = buffer[9].Trim();
                //IF21.SubType = buffer[10].Trim();

                DateTime oTempDatetime;
                DateTime.TryParseExact(buffer[12].Trim(), "yyyyMMdd", DefaultCultureInfo, DateTimeStyles.None, out oTempDatetime);
                //IF21.BeginDate = oTempDatetime;
                DateTime.TryParseExact(buffer[13].Trim(), "yyyyMMdd", DefaultCultureInfo, DateTimeStyles.None, out oTempDatetime);
                //IF21.EndDate = oTempDatetime;

                tempSub1 = buffer[10].Trim();
                temp1 = buffer[11].Trim();
                string tempSub2, temp2;
                foreach (TAB512 item2 in oTable2)
                {
                    string[] buffer2 = item2.Wa.Split('^');
                    tempSub2 = buffer2[8].Trim();
                    temp2 = buffer2[9].Trim();
                    if (tempSub1 == tempSub2 && temp1 == temp2)
                    {
                        IF21.FatherID = buffer2[0].Trim();
                        IF21.MotherID = buffer2[1].Trim();
                        IF21.SpouseID = buffer2[2].Trim();
                        IF21.FatherSpouseID = buffer2[3].Trim();
                        IF21.MotherSpouseID = buffer2[4].Trim();
                        IF21.TitleName = buffer2[5].Trim();
                        IF21.Employer = buffer2[6].Trim();
                        IF21.JobTitle = buffer2[7].Trim();
                        IF21.Dead = buffer2[10].Trim();
                        IF21.Address = buffer2[11].Trim();
                        IF21.Street = buffer2[12].Trim();
                        IF21.District = buffer2[13].Trim();
                        IF21.City = buffer2[14].Trim();
                        IF21.Postcode = buffer2[15].Trim();
                        IF21.Country = buffer2[16].Trim();
                        IF21.TelephoneNo = buffer2[17].Trim();
                    }
                }


                if (IF21.Dead == "")
                {
                    returnValue.Add(IF21);
                }
            }

            

            return returnValue;
        }


        public override void SavePersonalData(INFOTYPE0002 data)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            UPLOADINFOTYPE6 oFunction = new UPLOADINFOTYPE6();
            oFunction.Connection = oConnection;

            ZHRHRS001Table Updatetab = new ZHRHRS001Table();

            ZHRHRS001Table Updatetab_Ret = new ZHRHRS001Table();

            ZHRHRS001 item = new ZHRHRS001();
            item.Begda = data.BeginDate.ToString("yyyyMMdd", oCL);
            item.Endda = data.EndDate.ToString("yyyyMMdd", oCL);
            item.Infty = data.InfoType;
            item.Msg = "";
            item.Msgtype = "";
            item.Objps = "";
            item.Operation = "INS";
            item.Pernr = data.EmployeeID;
            item.Reqnr = Guid.NewGuid().ToString();
            item.Seqnr = "";
            item.Status = "N";
            item.Subty = data.SubType;
            item.T01 = data.TitleID;
            item.T02 = data.Prefix;
            item.T03 = data.FirstName;
            item.T04 = data.LastName;
            item.T05 = data.Initial;
            item.T06 = data.NickName;
            item.T07 = data.NameFormat;
            item.T08 = data.DOB.ToString("yyyyMMdd", oCL);
            item.T09 = data.Gender;
            item.T10 = data.BirthPlace;
            item.T11 = data.BirthCity;
            item.T12 = data.Nationality;
            item.T13 = data.MaritalStatus;

            //if (data.MaritalEffectiveDate == DateTime.MinValue)
            if (data.MaritalEffectiveDate == DateTime.MinValue || data.MaritalEffectiveDate == null)
            {
                item.T14 = "00000000";
            }
            else
            {
                item.T14 = data.MaritalEffectiveDate.Value.ToString("yyyyMMdd", oCL);
            }
            item.T15 = data.NoOfChild.ToString();
            item.T16 = data.Religion;
            item.T17 = data.Language;
            //Add By Kiattiwat 13-02-2012
            item.T18 = data.MilitaryTitle;
            item.T19 = data.NamePrefix1;
            item.T20 = data.NamePrefix2;
            item.T21 = data.SecondTitle;
            item.T22 = data.SecondNationality;
            item.T23 = data.ThirdNationality;
            Updatetab.Add(item);

            oFunction.Zhrhri001(ref Updatetab, ref Updatetab_Ret);

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            bool lError = false;
            ZHRHRS001 result;
            if (Updatetab_Ret.Count > 0)
            {
                result = Updatetab_Ret[0];
                if (result.Msgtype == "E")
                {
                    data.Remark = result.Msg;
                    lError = true;
                }
            }
            if (lError)
            {
                throw new Exception("Post data error");
            }
        }


        public override void SaveFamilyData(List<INFOTYPE0021> data)
        {
            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            UPLOADINFOTYPE6 oFunction = new UPLOADINFOTYPE6();
            oFunction.Connection = oConnection;

            ZHRHRS001Table Updatetab = new ZHRHRS001Table();
            ZHRHRS001Table Updatetab_Ret = new ZHRHRS001Table();

            foreach (INFOTYPE0021 info21 in data)
            {
                ZHRHRS001 item = new ZHRHRS001();
                item.Begda = info21.BeginDate.ToString("yyyyMMdd", DefaultCultureInfo);
                item.Endda = info21.EndDate.ToString("yyyyMMdd", DefaultCultureInfo);
                item.Infty = info21.InfoType;

                item.Msg = "";
                item.Msgtype = "";
                item.Objps = info21.ChildNo;
                item.Operation = "INS";
                item.Pernr = info21.EmployeeID;
                //�繡������Ţ����ͧ�Ҥӹǳ�������ͧ�ѹ�����ū�ӡѹ ����ѹ�� gen key �������շҧ��ӡѹ�����
                item.Reqnr = Guid.NewGuid().ToString();
                item.Seqnr = "";
                item.Status = "N";
                item.Subty = info21.SubType;
                //T ��ҧ � ����Ҩҡ function �������� sap se37
                item.T01 = info21.FamilyMember;
                item.T02 = info21.TitleRank;
                item.T03 = info21.Name;
                item.T04 = info21.Surname;
                if (info21.BirthDate == DateTime.MinValue)
                {
                    item.T05 = "00000000";
                }
                else
                {
                    item.T05 = info21.BirthDate.ToString("yyyyMMdd", DefaultCultureInfo);
                }
                item.T06 = info21.Sex;
                //Province province = Province.GetProvince(info21.BirthPlace);
                //item.T07 = province.Description;
                //ModifyBy: Ratchatawan W. (2011-10-14)
                item.T07 = info21.BirthPlace;

                item.T08 = info21.CityOfBirth;
                item.T09 = info21.Nationality;
                item.T10 = info21.FatherID;
                item.T11 = info21.MotherID;
                item.T12 = info21.SpouseID;
                item.T13 = info21.FatherSpouseID;
                item.T14 = info21.MotherSpouseID;
                item.T15 = info21.TitleName;
                item.T16 = info21.Employer;
                item.T17 = info21.JobTitle;
                //AddBy: Ratchatawan W. (2011-10-14)
                item.T18 = info21.Dead;
                //AddBy: Nuttapon.a 2013-02-19
                item.T19 = info21.Address;
                item.T20 = info21.Street;
                item.T21 = info21.District;
                item.T22 = info21.City;
                item.T23 = info21.Postcode;
                item.T24 = info21.Country;
                item.T25 = info21.TelephoneNo;
                Updatetab.Add(item);
            }
            oFunction.Zhrhri001(ref Updatetab, ref Updatetab_Ret);
            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            bool lError = false;
            string message = "";
            ZHRHRS001 result;
            for (int i = 0; i < Updatetab_Ret.Count; i++)
            {
                result = Updatetab_Ret[i];
                if (result.Msgtype == "E")
                {
                    message = result.Msg;
                    data[i].Remark = result.Msg;
                    lError = true;
                }
            }

            if (lError)
            {
                throw new Exception("Post data error: " + message);
            }
        }


        private static int CalculateAge(DateTime dateOfBirth)
        {
            int age = 0;
            age = DateTime.Now.Year - dateOfBirth.Year;
            if (DateTime.Now.DayOfYear < dateOfBirth.DayOfYear)
                age = age - 1;

            return age;
        }

        static int CalculateYourAge(DateTime Dob)
        {
            DateTime Now = Convert.ToDateTime(DateTime.Now.Year+"/01/01");
            int Years = new DateTime(DateTime.Now.Subtract(Dob).Ticks).Year - 1;
            DateTime PastYearDate = Dob.AddYears(Years);
            int Months = 0;
            for (int i = 1; i <= 12; i++)
            {
                if (PastYearDate.AddMonths(i) == Now)
                {
                    Months = i;
                    break;
                }
                else if (PastYearDate.AddMonths(i) >= Now)
                {
                    Months = i - 1;
                    break;
                }
            }
           
            return Years;
        }

        public override void SaveWageAdditionalData(BEPostingData data)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            UPLOADINFOTYPE6 oFunction = new UPLOADINFOTYPE6();

            oFunction.Connection = oConnection;

            ZHRHRS001Table Updatetab = new ZHRHRS001Table();

            ZHRHRS001Table Updatetab_Ret = new ZHRHRS001Table();

            #region " set item "
            ZHRHRS001 item = new ZHRHRS001();
            item.Pernr = data.EmployeeID;
            item.Begda = data.BeginDate.ToString("yyyyMMdd", oCL);
            item.Endda = data.EndDate.ToString("yyyyMMdd", oCL);
            item.Subty = data.WageType.ToString();
            item.T01 = data.AssignmentNumber.ToString();
            item.T02 = (data.Amount > -1 ? data.Amount.ToString() : "");
            if (data.WageType == "5005" || data.WageType == "5006")
                item.T03 = "THB";
            else
                item.T03 = (data.Amount > -1 ? data.Currency : "THB");//item.T03 = (data.Amount > -1 ? data.Currency : "");
            item.T04 = (data.Number > -1 ? data.Number.ToString() : "");
            item.T05 = (data.Number > -1 ? data.Unit : "");
            item.T06 = "02";
            item.Infty = "0015";
            item.Operation = "INS";
            Updatetab.Add(item);
            #endregion

            oFunction.Zhrhri001(ref Updatetab, ref Updatetab_Ret);
            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            if (Updatetab_Ret[0].Msgtype == "E")
            {
                throw new Exception(Updatetab_Ret[0].Msg);
            }
        }
    }
}