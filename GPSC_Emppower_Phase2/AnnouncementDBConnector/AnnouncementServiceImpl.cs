#region Using

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using ESS.ANNOUNCEMENT.ABSTRACT;
using ESS.ANNOUNCEMENT.DATACLASS;
using ESS.SHAREDATASERVICE;
//using ESS.SHAREDATASERVICE.DATACLASS;

#endregion Using

namespace ESS.ANNOUNCEMENT.DB
{
    public class AnnouncementServiceImpl : AbstractAnnouncementService
    {
        #region Constructor
        public AnnouncementServiceImpl(string oCompanyCode)
        {
            //Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID);
            CompanyCode = oCompanyCode;
        }

        #endregion Constructor

        #region Member
        //private Dictionary<string, string> Config { get; set; }
        public string CompanyCode { get; set; }

        private static string ModuleID = "ESS.ANNOUNCEMENT.DB";

        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }

        #endregion Member

        #region Override Function

        public override List<AnnouncementData> GetAnnoncementList()
        {
            List<AnnouncementData> returnValue = new List<AnnouncementData>();
            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_AnnouncementGetUptodate", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("Announcement");
            oConnection.Open();
            oAdapter.Fill(oTable);
            foreach (DataRow dr in oTable.Rows)
            {
                AnnouncementData data = new AnnouncementData();
                data.ParseToObject(dr);
                returnValue.Add(data);
            }
            oConnection.Close();

            return returnValue;
        }

        #endregion Override Function

        public override List<AnnouncementGroupByAdmin> GetGroupAnnouncement(int groupId)
        {
            List<AnnouncementGroupByAdmin> list_group = new List<AnnouncementGroupByAdmin>();

            SqlConnection oConnection = new SqlConnection(BaseConnStr);

            string sql_command = "select a.SubjectReportSubID,a.SubjectReportGroupID,b.SubjectCode";
            sql_command += " from AdminGroupMapping a inner join ApplicationSubject b on a.SubjectReportSubID = b.SubjectID";
            sql_command += " where a.SubjectReportGroupID = @groupId";
            sql_command += " ORDER BY SubjectReportSubID ASC";

            SqlCommand oCommand = new SqlCommand(sql_command, oConnection);
            oCommand.CommandType = CommandType.Text;

            SqlParameter oParam;
            oParam = new SqlParameter("@groupId", SqlDbType.Int);
            oParam.Value = groupId;
            oCommand.Parameters.Add(oParam);

            oConnection.Open();

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("GroupReport");

            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose(); // Clear memory
            oCommand.Dispose(); // Clear memory

            if (oTable.Rows.Count > 0)
            {
                foreach (DataRow row in oTable.Rows)
                {
                    AnnouncementGroupByAdmin item = new AnnouncementGroupByAdmin();
                    item.ParseToObject(row);
                    list_group.Add(item);
                }
            }

            return list_group;
        }

        public override List<AnnouncementStatus> GetAnnouncementStatus()
        {
            List<AnnouncementStatus> list_status = new List<AnnouncementStatus>();
            SqlConnection oConnection = new SqlConnection(BaseConnStr);

            string sql_command = "Select * FROM AnnouncementDataStatus";
            SqlCommand oCommand = new SqlCommand(sql_command, oConnection);
            oCommand.CommandType = CommandType.Text;

            oConnection.Open();

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("GroupReport");

            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose(); // Clear memory
            oCommand.Dispose(); // Clear memory

            if (oTable.Rows.Count > 0)
            {
                foreach (DataRow row in oTable.Rows)
                {
                    AnnouncementStatus item = new AnnouncementStatus();
                    item.ParseToObject(row);
                    list_status.Add(item);
                }
            }
            return list_status;
        }

        #region ��С�Ȣ�ͤ���
        public override void SaveAnnouncementTextByAdmin(AnnouncementTextByAdmin model)
        {
            SqlConnection oConnection = new SqlConnection(BaseConnStr);

            oConnection.Open();

            SqlTransaction oTransaction = oConnection.BeginTransaction();
            SqlCommand oCommand = new SqlCommand("sp_AnnouncementTextCreateByAdmin", oConnection, oTransaction);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.Clear();

            SqlParameter param;
            param = new SqlParameter("@p_AnnouncementTypeId", SqlDbType.Int);
            param.Value = model.AnnouncementTypeId;
            oCommand.Parameters.Add(param);

            param = new SqlParameter("@p_AnnouncementStatusId", SqlDbType.Int);
            param.Value = model.AnnouncementStatusId;
            oCommand.Parameters.Add(param);

            param = new SqlParameter("@p_Sequence", SqlDbType.Int);
            param.Value = model.Sequence;
            oCommand.Parameters.Add(param);

            param = new SqlParameter("@p_EffectiveDate", SqlDbType.DateTime);
            param.Value = model.EffectiveDate.Date;
            oCommand.Parameters.Add(param);

            param = new SqlParameter("@p_ExpireDate", SqlDbType.DateTime);
            param.Value = model.ExpireDate.Date;
            oCommand.Parameters.Add(param);

            param = new SqlParameter("@p_IsUnlimitedExpire", SqlDbType.Bit);
            param.Value = model.IsUnlimitTime;
            oCommand.Parameters.Add(param);

            param = new SqlParameter("@p_Title_TH", SqlDbType.NVarChar);
            param.Value = model.Subject_TH;
            oCommand.Parameters.Add(param);

            param = new SqlParameter("@p_Title_EN", SqlDbType.NVarChar);
            param.Value = model.Subject_EN;
            oCommand.Parameters.Add(param);

            param = new SqlParameter("@p_Description_TH", SqlDbType.NVarChar);
            param.Value = model.HtmlEditor_TH;
            oCommand.Parameters.Add(param);

            param = new SqlParameter("@p_Description_EN", SqlDbType.NVarChar);
            param.Value = model.HtmlEditor_EN;
            oCommand.Parameters.Add(param);

            param = new SqlParameter("@p_IsDelete", SqlDbType.Bit);
            param.Value = false;
            oCommand.Parameters.Add(param);

            param = new SqlParameter("@p_CreateBy", SqlDbType.NVarChar);
            param.Value = model.CreateBy;
            oCommand.Parameters.Add(param);

            param = new SqlParameter("@p_CreateDate", SqlDbType.DateTime);
            param.Value = DateTime.Now;
            oCommand.Parameters.Add(param);

            param = new SqlParameter("@p_UpdateBy", SqlDbType.NVarChar);
            param.Value = model.CreateBy;
            oCommand.Parameters.Add(param);

            param = new SqlParameter("@p_UpdateDate", SqlDbType.DateTime);
            param.Value = DateTime.Now;
            oCommand.Parameters.Add(param);

            try
            {
                oCommand.ExecuteNonQuery();
                oTransaction.Commit();
            }
            catch
            {
                oTransaction.Rollback();
                oCommand.Dispose();
                if (oConnection.State == ConnectionState.Open)
                    oConnection.Close();
                oConnection.Dispose();
                oTransaction.Dispose();

                throw;
            }
            finally
            {
                oCommand.Dispose();
                if (oConnection.State == ConnectionState.Open)
                    oConnection.Close();
                oConnection.Dispose();
                oTransaction.Dispose();
            }
        }

        public override void EditDataAnnouncementTextByAdmin(EditAnnouncementTextByAdmin model)
        {
            SqlConnection oConnection = new SqlConnection(BaseConnStr);

            oConnection.Open();

            SqlTransaction oTransaction = oConnection.BeginTransaction();
            SqlCommand oCommand = new SqlCommand("sp_AnnouncementTextUpdateDataByAdmin", oConnection, oTransaction);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.Clear();

            oCommand.Parameters.Add("@p_AnnouncementId", SqlDbType.Int).Value = model.AnnouncementId;
            oCommand.Parameters.Add("@p_AnnouncementStatusId", SqlDbType.Int).Value = model.AnnouncementStatusId;
            oCommand.Parameters.Add("@p_Sequence", SqlDbType.Int).Value = model.Sequence;
            oCommand.Parameters.Add("@p_EffectiveDate", SqlDbType.DateTime).Value = model.EffectiveDate;
            oCommand.Parameters.Add("@p_ExpireDate", SqlDbType.DateTime).Value = model.ExpireDate;
            oCommand.Parameters.Add("@p_IsUnlimitedExpire", SqlDbType.Bit).Value = model.IsUnlimitedExpire;
            oCommand.Parameters.Add("@p_Title_TH", SqlDbType.NVarChar).Value = model.Title_TH;
            oCommand.Parameters.Add("@p_Title_EN", SqlDbType.NVarChar).Value = model.Title_EN;
            oCommand.Parameters.Add("@p_Description_TH", SqlDbType.NVarChar).Value = model.Description_TH;
            oCommand.Parameters.Add("@p_Description_EN", SqlDbType.NVarChar).Value = model.Description_EN;
            oCommand.Parameters.Add("@p_UpdateBy", SqlDbType.NVarChar).Value = model.UpdateBy;
            oCommand.Parameters.Add("@p_UpdateDate", SqlDbType.NVarChar).Value = DateTime.Now;

            try
            {
                oCommand.ExecuteNonQuery();
                oTransaction.Commit();
            }
            catch
            {
                oTransaction.Rollback();
                oCommand.Dispose();
                if (oConnection.State == ConnectionState.Open)
                    oConnection.Close();
                oConnection.Dispose();
                oTransaction.Dispose();

                throw;
            }
            finally
            {
                oCommand.Dispose();
                if (oConnection.State == ConnectionState.Open)
                    oConnection.Close();
                oConnection.Dispose();
                oTransaction.Dispose();
            }
        }

        public override List<DisplayAnnouncementTextByAdmin> GetAllAnnouncementTextByAdmin()
        {
            var list_result = new List<DisplayAnnouncementTextByAdmin>();

            SqlConnection oConnection = new SqlConnection(BaseConnStr);

            string sql_command = "Select * FROM AnnouncementData ad " +
                "Where IsDelete = 0 " +
                "AND AnnouncementTypeId = 3 " +
                "ORDER BY ad.[Sequence] asc , ad.EffectiveDate desc , ad.CreateDate asc";
            SqlCommand oCommand = new SqlCommand(sql_command, oConnection);
            oCommand.CommandType = CommandType.Text;

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("AnnouncementText");

            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose(); // Clear memory
            oCommand.Dispose(); // Clear memory

            if (oTable.Rows.Count > 0)
            {
                foreach (DataRow dr in oTable.Rows)
                {
                    var data = new DisplayAnnouncementTextByAdmin();
                    data.ParseToObject(dr);
                    list_result.Add(data);
                }
            }

            return list_result;
        }

        public override EditAnnouncementTextByAdmin GetAnnouncementInKeyTextByAdmin(int AnnouncementId)
        {
            var announcement_text = new EditAnnouncementTextByAdmin();

            SqlConnection oConnection = new SqlConnection(BaseConnStr);

            oConnection.Open();

            SqlCommand oCommand = new SqlCommand("sp_AnnouncementTextGetByAdmin", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.Clear();

            SqlParameter param;
            param = new SqlParameter("@p_AnnouncementId", SqlDbType.Int);
            param.Value = AnnouncementId;
            oCommand.Parameters.Add(param);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("AnnouncementText");

            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose(); // Clear memory
            oCommand.Dispose(); // Clear memory

            if (oTable.Rows.Count > 0)
            {
                foreach (DataRow dr in oTable.Rows)
                {
                    announcement_text.ParseToObject(dr);
                }
            }
            return announcement_text;
        }

        public override void DeleteAnnouncementTextByAdmin(int AnnouncementId)
        {
            SqlConnection oConnection = new SqlConnection(BaseConnStr);

            oConnection.Open();

            SqlCommand oCommand = new SqlCommand("sp_AnnouncementTextDeleteByAdmin", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter param;
            param = new SqlParameter("@p_AnnouncementId", SqlDbType.Int);
            param.Value = AnnouncementId;
            oCommand.Parameters.Add(param);

            param = new SqlParameter("@p_IsDelete", SqlDbType.Bit);
            param.Value = true;
            oCommand.Parameters.Add(param);

            try
            {
                oCommand.ExecuteNonQuery();
            }
            catch
            {
                throw;
            }

            oCommand.Dispose();
            if (oConnection.State == ConnectionState.Open)
                oConnection.Close();
            oConnection.Dispose();
        }

        public override void UpdateSequenceAnnouncementTextByAdmin(List<DataEditSequenceAnnouncementTextByAdmin> model)
        {
            if (model.Count > 0)
            {
                SqlConnection oConnection = new SqlConnection(BaseConnStr);
                oConnection.Open();

                SqlTransaction oTransaction = oConnection.BeginTransaction();
                SqlCommand oCommand = new SqlCommand("sp_AnnouncementTextUpdateSequenceByAdmin", oConnection, oTransaction);
                oCommand.CommandType = CommandType.StoredProcedure;

                foreach (var item in model)
                {
                    oCommand.Parameters.Clear();

                    SqlParameter param;
                    param = new SqlParameter("@p_AnnouncementId", SqlDbType.Int);
                    param.Value = item.AnnouncementId;
                    oCommand.Parameters.Add(param);

                    param = new SqlParameter("@p_Sequence", SqlDbType.Int);
                    param.Value = item.Sequence;
                    oCommand.Parameters.Add(param);

                    try
                    {
                        oCommand.ExecuteNonQuery();
                    }
                    catch
                    {
                        oTransaction.Rollback();
                        oCommand.Dispose();
                        if (oConnection.State == ConnectionState.Open)
                            oConnection.Close();
                        oConnection.Dispose();
                        oTransaction.Dispose();
                        throw;
                    }
                }
                oTransaction.Commit();

                oCommand.Dispose();
                if (oConnection.State == ConnectionState.Open)
                    oConnection.Close();
                oConnection.Dispose();
                oTransaction.Dispose();
            }
        }

        #endregion

        #region ��С�Ȼ�Ъ�����ѹ��

        public override void SaveAnnouncementPublicRelations(AnnouncementDataAll model)
        {
            SqlConnection oConnection = new SqlConnection(BaseConnStr);

            oConnection.Open();

            SqlTransaction oTransaction = oConnection.BeginTransaction();
            SqlCommand oCommand = new SqlCommand();
            oCommand = new SqlCommand("sp_AnnouncementPublicRelationsSaveByAdmin", oConnection, oTransaction);
            oCommand.CommandType = CommandType.StoredProcedure;
            try
            {
                //�ͧ�Ѻ PK ��� Return �͡��
                oCommand.Parameters.Add("@p_AnnouncementId", SqlDbType.Int).Direction = ParameterDirection.Output;
                oCommand.Parameters.Add("@p_AnnouncementTypeId", SqlDbType.Int).Value = 2;
                oCommand.Parameters.Add("@p_AnnouncementStatusId", SqlDbType.Int).Value = 1;
                oCommand.Parameters.Add("@p_Sequence", SqlDbType.Int).Value = model.Sequence;
                oCommand.Parameters.Add("@p_EffectiveDate", SqlDbType.DateTime).Value = model.EffectiveDate;
                oCommand.Parameters.Add("@p_ExpireDate", SqlDbType.DateTime).Value = model.ExpireDate;
                oCommand.Parameters.Add("@p_IsUnlimitedExpire", SqlDbType.Bit).Value = model.IsUnlimitedExpire;
                oCommand.Parameters.Add("@p_Title_TH", SqlDbType.NVarChar).Value = model.Title_TH;
                oCommand.Parameters.Add("@p_Title_EN", SqlDbType.NVarChar).Value = model.Title_EN;
                oCommand.Parameters.Add("@p_TextLink", SqlDbType.NVarChar).Value = model.TextLink;
                oCommand.Parameters.Add("@p_CreateBy", SqlDbType.NVarChar).Value = model.CreateBy;
                oCommand.Parameters.Add("@p_CreateDate", SqlDbType.DateTime).Value = DateTime.Now;
                oCommand.Parameters.Add("@p_UpdateBy", SqlDbType.NVarChar).Value = model.CreateBy;
                oCommand.Parameters.Add("@p_UpdateDate", SqlDbType.DateTime).Value = DateTime.Now;
                oCommand.ExecuteNonQuery();


                string p_AnnouncementId = oCommand.Parameters["@p_AnnouncementId"].Value.ToString();
                int cv_p_AnnouncementId = int.Parse(p_AnnouncementId);

                if (model.ListFile.Count > 0)
                {
                    oCommand.Parameters.Clear();
                    oCommand = new SqlCommand("sp_AnnouncementPublicRelationsSaveFileByAdmin", oConnection, oTransaction);
                    oCommand.CommandType = CommandType.StoredProcedure;

                    foreach (var item in model.ListFile)
                    {
                        oCommand.Parameters.Clear();
                        oCommand.Parameters.Add("@p_AnnouncementId", SqlDbType.Int).Value = cv_p_AnnouncementId;
                        oCommand.Parameters.Add("@p_TextFile", SqlDbType.NVarChar).Value = item.TextFile;
                        oCommand.Parameters.Add("@P_Sequence", SqlDbType.Int).Value = item.Sequence;
                        oCommand.Parameters.Add("@p_Language", SqlDbType.NVarChar).Value = item.Language;
                        oCommand.Parameters.Add("@p_CreateBy", SqlDbType.NVarChar).Value = model.CreateBy;
                        oCommand.Parameters.Add("@p_CreateDate", SqlDbType.DateTime).Value = DateTime.Now;
                        oCommand.Parameters.Add("@_UpdateBy", SqlDbType.NVarChar).Value = model.CreateBy;
                        oCommand.Parameters.Add("@_UpdateDate", SqlDbType.DateTime).Value = DateTime.Now;
                        oCommand.ExecuteNonQuery();

                    }
                }
                oTransaction.Commit();

            }
            catch
            {
                oTransaction.Rollback();
                throw;
            }
            finally
            {
                oCommand.Dispose();
                oConnection.Dispose();
                oTransaction.Dispose();
                if (oConnection.State == ConnectionState.Open)
                    oConnection.Close();
                
            }
        }

        public override List<AnnouncementDataAll> GetAllAnnouncementPublicRelations()
        {
            var list_AnnouncementPublicRelations = new List<AnnouncementDataAll>();

            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            oConnection.Open();

            SqlCommand oCommand;
            SqlParameter param;

            oCommand = new SqlCommand("sp_AnnouncementGetAllByAdmin", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            param = new SqlParameter("@p_AnnouncementTypeId", SqlDbType.Int);
            param.Value = 2; // ���������ǻ�Ъ�����ѹ��
            oCommand.Parameters.Add(param);

            SqlDataAdapter oAdapter;
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("AnnouncementPublicRelations");
            oAdapter.Fill(oTable);

            if (oTable.Rows.Count > 0)
            {
                oCommand = new SqlCommand("sp_AnnouncementGetPictureByAdmin", oConnection);
                oCommand.CommandType = CommandType.StoredProcedure;
                oCommand.Parameters.Clear();

                DataTable oTable_Picture = new DataTable("AnnouncementPicture");

                foreach (DataRow dr in oTable.Rows)
                {
                    AnnouncementDataAll data = new AnnouncementDataAll();
                    data.ParseToObject(dr);

                    oCommand.Parameters.Clear();
                    oTable_Picture.Clear();
                    param = new SqlParameter("@p_AnnouncementId", SqlDbType.Int);
                    param.Value = data.AnnouncementId;
                    oCommand.Parameters.Add(param);

                    var oAdapter2 = new SqlDataAdapter(oCommand);
                    oAdapter2.Fill(oTable_Picture);

                    if (oTable_Picture.Rows.Count > 0)
                    {
                        foreach (DataRow item_pic in oTable_Picture.Rows)
                        {
                            AnnouncementFile data_picture = new AnnouncementFile();
                            data_picture.ParseToObject(item_pic);
                            data.ListFile.Add(data_picture);
                        }
                    }
                    oAdapter2.Dispose();
                    list_AnnouncementPublicRelations.Add(data);
                }
            }

            oAdapter.Dispose();
            oConnection.Close();
            oConnection.Dispose(); // Clear memory
            oCommand.Dispose(); // Clear memory

            return list_AnnouncementPublicRelations;
        }

        public override void EditSequenceAnnouncementPublicRelations(List<DataEditSequenceAnnouncementPublicRelations> model)
        {
            if (model.Count > 0)
            {
                if (model.Count > 0)
                {
                    SqlConnection oConnection = new SqlConnection(BaseConnStr);
                    oConnection.Open();

                    SqlTransaction oTransaction = oConnection.BeginTransaction();
                    SqlCommand oCommand = new SqlCommand("sp_AnnouncementPublicRelationsUpdateSequenceByAdmin", oConnection, oTransaction);
                    oCommand.CommandType = CommandType.StoredProcedure;

                    foreach (var item in model)
                    {
                        oCommand.Parameters.Clear();

                        SqlParameter param;
                        param = new SqlParameter("@p_AnnouncementId", SqlDbType.Int);
                        param.Value = item.AnnouncementId;
                        oCommand.Parameters.Add(param);

                        param = new SqlParameter("@p_Sequence", SqlDbType.Int);
                        param.Value = item.Sequence;
                        oCommand.Parameters.Add(param);

                        try
                        {
                            oCommand.ExecuteNonQuery();
                        }
                        catch
                        {
                            oTransaction.Rollback();
                            oCommand.Dispose();
                            if (oConnection.State == ConnectionState.Open)
                                oConnection.Close();
                            oConnection.Dispose();
                            oTransaction.Dispose();
                            throw;
                        }
                    }
                    oTransaction.Commit();

                    oCommand.Dispose();
                    if (oConnection.State == ConnectionState.Open)
                        oConnection.Close();
                    oConnection.Dispose();
                    oTransaction.Dispose();
                }
            }
        }

        public override void DeleteAnnouncementPublicRelations(int AnnouncementId)
        {
            SqlConnection oConnection = new SqlConnection(BaseConnStr);

            oConnection.Open();

            SqlCommand oCommand = new SqlCommand("sp_AnnouncementPublicRelationsDeleteByAdmin", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter param;
            param = new SqlParameter("@p_AnnouncementId", SqlDbType.Int);
            param.Value = AnnouncementId;
            oCommand.Parameters.Add(param);

            param = new SqlParameter("@p_IsDelete", SqlDbType.Bit);
            param.Value = true;
            oCommand.Parameters.Add(param);

            try
            {
                oCommand.ExecuteNonQuery();
            }
            catch
            {
                throw;
            }

            oCommand.Dispose();
            if (oConnection.State == ConnectionState.Open)
                oConnection.Close();
            oConnection.Dispose();
        }

        public override AnnouncementDataAll GetAnnouncementPublicRelationsById(int AnnouncementId)
        {
            var result = new AnnouncementDataAll();

            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            oConnection.Open();

            SqlCommand oCommand;
            oCommand = new SqlCommand("sp_AnnouncementPublicRelationsGetDataByAdmin", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            oCommand.Parameters.Add("@p_AnnouncementId", SqlDbType.Int).Value = AnnouncementId;

            SqlDataAdapter oAdapter;
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("AnnouncementPublicRelations");
            oAdapter.Fill(oTable);

            if (oTable.Rows.Count > 0)
            {
                result.ParseToObject(oTable);

                oCommand = new SqlCommand("sp_AnnouncementGetPictureByAdmin", oConnection);
                oCommand.CommandType = CommandType.StoredProcedure;
                oCommand.Parameters.Clear();

                oCommand.Parameters.Add("@p_AnnouncementId", SqlDbType.Int).Value = AnnouncementId;
                oAdapter = new SqlDataAdapter(oCommand);
                DataTable oTable_Pic = new DataTable("AnnouncementPublicRelations_File");
                oAdapter.Fill(oTable_Pic);

                if (oTable_Pic.Rows.Count > 0)
                {
                    foreach(DataRow dr in oTable_Pic.Rows)
                    {
                        if(dr["Language"].ToString() == "TH")
                        {
                            result.File_TH_Id = Convert.ToInt32(dr["AnnouncementFileId"].ToString());
                            result.File_TH = dr["TextFile"].ToString();
                        }

                        if (dr["Language"].ToString() == "EN")
                        {
                            result.File_EN_Id = Convert.ToInt32(dr["AnnouncementFileId"].ToString());
                            result.File_EN = dr["TextFile"].ToString();
                        }
                        //AnnouncementFile file = new AnnouncementFile();
                        //file.ParseToObject(dr);
                        //result.ListFile.Add(file);
                    }

                }
            }

            oAdapter.Dispose();
            oConnection.Close();
            oConnection.Dispose(); // Clear memory
            oCommand.Dispose(); // Clear memory

            return result;
        }

        public override void EditAnnouncementPublicRelations(AnnouncementPublicRelations model)
        {
            var date_now = DateTime.Now;

            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            oConnection.Open();

            SqlCommand oCommand;
            SqlTransaction oTransaction = oConnection.BeginTransaction();
            oCommand = new SqlCommand("sp_AnnouncementPublicRelationsEditDataByAdmin", oConnection, oTransaction);
            oCommand.CommandType = CommandType.StoredProcedure;

            oCommand.Parameters.Add("@p_AnnouncementId", SqlDbType.Int).Value = model.AnnouncementId;
            oCommand.Parameters.Add("@p_AnnouncementTypeId", SqlDbType.Int).Value = 2;
            oCommand.Parameters.Add("@p_AnnouncementStatusId", SqlDbType.Int).Value = model.AnnouncementStatusId;
            oCommand.Parameters.Add("@p_Sequence", SqlDbType.Int).Value = model.Sequence;
            oCommand.Parameters.Add("@p_EffectiveDate", SqlDbType.DateTime).Value = model.EffectiveDate;
            oCommand.Parameters.Add("@p_ExpireDate", SqlDbType.DateTime).Value = model.ExpireDate;
            oCommand.Parameters.Add("@p_IsUnlimitedExpire", SqlDbType.Bit).Value = model.IsUnlimitedExpire;
            oCommand.Parameters.Add("@p_Title_TH", SqlDbType.NVarChar).Value = model.Title_TH;
            oCommand.Parameters.Add("@p_Title_EN", SqlDbType.NVarChar).Value = model.Title_EN;
            oCommand.Parameters.Add("@p_TextLink", SqlDbType.NVarChar).Value = model.TextLink;
            oCommand.Parameters.Add("@p_UpdateBy", SqlDbType.NVarChar).Value = model.UpdateBy;
            oCommand.Parameters.Add("@p_UpdateDate", SqlDbType.DateTime).Value = date_now;

            try
            {
                oCommand.ExecuteNonQuery();
                // �ٻ������
                if (string.IsNullOrEmpty(model.File_TH))
                {
                    if(model.File_TH_Id > 0)
                    {
                        string cmd_pic = "DELETE FROM [dbo].[AnnouncementDataFile] WHERE [AnnouncementFileId] = @p_AnnouncementFileId";
                        oCommand = new SqlCommand(cmd_pic, oConnection, oTransaction);
                        oCommand.CommandType = CommandType.Text;
                        oCommand.Parameters.Clear();
                        oCommand.Parameters.Add("@p_AnnouncementFileId", SqlDbType.Int).Value = model.File_TH_Id;
                        oCommand.ExecuteNonQuery();
                    }
                }
                else
                {
                    oCommand = new SqlCommand("sp_AnnouncementPublicRelationsEditFileByAdmin", oConnection, oTransaction);
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.Parameters.Clear();
                    oCommand.Parameters.Add("@p_TextFile", SqlDbType.NVarChar).Value = model.File_TH;
                    oCommand.Parameters.Add("@p_Sequence", SqlDbType.Int).Value = 1;
                    oCommand.Parameters.Add("@p_UpdateBy", SqlDbType.NVarChar).Value = model.UpdateBy;
                    oCommand.Parameters.Add("@p_UpdateDate", SqlDbType.DateTime).Value = DateTime.Now;
                    oCommand.Parameters.Add("@p_AnnouncementId", SqlDbType.Int).Value = model.AnnouncementId;
                    oCommand.Parameters.Add("@p_Language", SqlDbType.NVarChar).Value = "TH";
                    oCommand.Parameters.Add("@p_AnnouncementFileId", SqlDbType.Int).Value = model.File_TH_Id;
                    oCommand.ExecuteNonQuery();
                }
                

                // �ٻ�����ѧ���
                if (string.IsNullOrEmpty(model.File_EN))
                {
                    string cmd_pic = "DELETE FROM [dbo].[AnnouncementDataFile] WHERE [AnnouncementFileId] = @p_AnnouncementFileId";
                    oCommand = new SqlCommand(cmd_pic, oConnection, oTransaction);
                    oCommand.CommandType = CommandType.Text;
                    oCommand.Parameters.Clear();
                    oCommand.Parameters.Add("@p_AnnouncementFileId", SqlDbType.Int).Value = model.File_EN_Id;

                }
                else
                {
                    oCommand = new SqlCommand("sp_AnnouncementPublicRelationsEditFileByAdmin", oConnection, oTransaction);
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.Parameters.Clear();
                    oCommand.Parameters.Add("@p_TextFile", SqlDbType.NVarChar).Value = model.File_EN;
                    oCommand.Parameters.Add("@p_Sequence", SqlDbType.Int).Value = 1;
                    oCommand.Parameters.Add("@p_UpdateBy", SqlDbType.NVarChar).Value = model.UpdateBy;
                    oCommand.Parameters.Add("@p_UpdateDate", SqlDbType.DateTime).Value = DateTime.Now;
                    oCommand.Parameters.Add("@p_AnnouncementId", SqlDbType.Int).Value = model.AnnouncementId;
                    oCommand.Parameters.Add("@p_Language", SqlDbType.NVarChar).Value = "EN";
                    oCommand.Parameters.Add("@p_AnnouncementFileId", SqlDbType.Int).Value = model.File_EN_Id;
                }

                oCommand.ExecuteNonQuery();
                oTransaction.Commit();
            }
            catch
            {
                oTransaction.Rollback();
                throw;
            }
            finally
            {
                oCommand.Dispose();
                if (oConnection.State == ConnectionState.Open)
                    oConnection.Close();
                oConnection.Dispose();
                oTransaction.Dispose();
            }
        }

        #endregion

        #region ��С����ѡ

        public override void SaveAnnouncementMain(AnnouncementDataAll model)
        {
            var date_now = DateTime.Now;
            SqlConnection oConnection = new SqlConnection(BaseConnStr);

            oConnection.Open();

            SqlTransaction oTransaction = oConnection.BeginTransaction();
            SqlCommand oCommand;
            oCommand = new SqlCommand("sp_AnnouncementMainSaveByAdmin", oConnection, oTransaction);
            oCommand.CommandType = CommandType.StoredProcedure;

            //�ͧ�Ѻ PK ��� Return �͡��
            oCommand.Parameters.Add("@p_AnnouncementId", SqlDbType.Int).Direction = ParameterDirection.Output;
            oCommand.Parameters.Add("@p_AnnouncementTypeId", SqlDbType.Int).Value = 1;  // ��������С����ѡ = 1
            oCommand.Parameters.Add("@p_AnnouncementStatusId", SqlDbType.Int).Value = 1;
            oCommand.Parameters.Add("@p_EffectiveDate", SqlDbType.DateTime).Value = model.EffectiveDate;
            oCommand.Parameters.Add("@p_ExpireDate", SqlDbType.DateTime).Value = model.ExpireDate;
            oCommand.Parameters.Add("@p_IsUnlimitedExpire", SqlDbType.Bit).Value = model.IsUnlimitedExpire;
            oCommand.Parameters.Add("@p_Title_TH", SqlDbType.NVarChar).Value = model.Title_TH;
            oCommand.Parameters.Add("@p_Description_TH", SqlDbType.NVarChar).Value = model.Description_TH;
            oCommand.Parameters.Add("@p_Remark_TH", SqlDbType.NVarChar).Value = model.Remark_TH;
            oCommand.Parameters.Add("@p_Title_EN", SqlDbType.NVarChar).Value = model.Title_EN;
            oCommand.Parameters.Add("@p_Description_EN", SqlDbType.NVarChar).Value = model.Description_EN;
            oCommand.Parameters.Add("@p_Remark_EN", SqlDbType.NVarChar).Value = model.Remark_EN;
            oCommand.Parameters.Add("@p_CreateBy", SqlDbType.NVarChar).Value = model.CreateBy;
            oCommand.Parameters.Add("@p_CreateDate", SqlDbType.NVarChar).Value = date_now;
            oCommand.Parameters.Add("@p_UpdateBy", SqlDbType.NVarChar).Value = model.CreateBy;
            oCommand.Parameters.Add("@p_UpdateDate", SqlDbType.NVarChar).Value = date_now;

            try
            {
                oCommand.ExecuteNonQuery();
                string p_AnnouncementId = oCommand.Parameters["@p_AnnouncementId"].Value.ToString();
                int cv_p_AnnouncementId = int.Parse(p_AnnouncementId);

                if (model.ListFile.Count > 0)
                {
                    oCommand = new SqlCommand("sp_AnnouncementMainSaveFileByAdmin", oConnection, oTransaction);
                    oCommand.CommandType = CommandType.StoredProcedure;
                    foreach (var item in model.ListFile)
                    {
                        oCommand.Parameters.Clear();
                        oCommand.Parameters.Add("@p_AnnouncementId", SqlDbType.Int).Value = cv_p_AnnouncementId;
                        oCommand.Parameters.Add("@p_TextFile", SqlDbType.NVarChar).Value = item.TextFile;
                        oCommand.Parameters.Add("@P_Sequence", SqlDbType.Int).Value = item.Sequence;
                        oCommand.Parameters.Add("@p_Language", SqlDbType.NVarChar).Value = item.Language;
                        oCommand.Parameters.Add("@p_CreateBy", SqlDbType.NVarChar).Value = model.CreateBy;
                        oCommand.Parameters.Add("@p_CreateDate", SqlDbType.DateTime).Value = DateTime.Now;
                        oCommand.ExecuteNonQuery();
                    }
                }
                oTransaction.Commit();
            }
            catch
            {
                oTransaction.Rollback();
                throw;
            }
            finally
            {
                oCommand.Dispose();
                if (oConnection.State == ConnectionState.Open)
                    oConnection.Close();
                oConnection.Dispose();
                oTransaction.Dispose();
            }
        }

        public override PaginationAnnouncementMain GetAnnouncementsMainAllByAdmin(int page, int itemPerPage)
        {
            var list_AnnouncementMain = new List<AnnouncementDataAll>();

            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            oConnection.Open();

            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable;
            //SqlParameter param;

            int total_transaction = 0;

            string sql_cmd = "Select Count(*) as count_all  From AnnouncementData Where AnnouncementTypeId = 1 And IsDelete = 0";
            oCommand = new SqlCommand(sql_cmd, oConnection);
            oCommand.CommandType = CommandType.Text;

            oAdapter = new SqlDataAdapter(oCommand);
            oTable = new DataTable("CountAnnouncementMain");
            oAdapter.Fill(oTable);

            if (oTable.Rows.Count > 0)
            {
                foreach (DataRow item in oTable.Rows)
                {
                    total_transaction = Convert.ToInt32(item["count_all"]);
                }
            }

            if (total_transaction > 0)
            {
                oCommand = new SqlCommand("sp_AnnouncementMainGetAllByAdmin", oConnection);
                oCommand.CommandType = CommandType.StoredProcedure;
                oCommand.Parameters.Clear();

                oCommand.Parameters.Add("@p_AnnouncementTypeId", SqlDbType.Int).Value = 1;  // ������ѡ fix ������

                int skip_data = (page - 1) * itemPerPage; // Skip ������㹵��ҧ
                oCommand.Parameters.Add("@p_Skip", SqlDbType.Int).Value = skip_data;
                oCommand.Parameters.Add("@p_Take", SqlDbType.Int).Value = itemPerPage;

                oAdapter = new SqlDataAdapter(oCommand);

                oTable.Clear();
                oTable = new DataTable("AnnouncementMain");
                oAdapter.Fill(oTable);

                if (oTable.Rows.Count > 0)
                {
                    foreach (DataRow item in oTable.Rows)
                    {
                        var data = new AnnouncementDataAll();
                        data.ParseToObject(item);
                        list_AnnouncementMain.Add(data);
                    }
                }
            }

            oAdapter.Dispose();
            oCommand.Dispose();
            if (oConnection.State == ConnectionState.Open)
                oConnection.Close();


            return new PaginationAnnouncementMain
            {
                Page = page,
                ItemPerPage = itemPerPage,
                List_AnnouncementMain = list_AnnouncementMain,
                Total = total_transaction
            };
        }

        public override void DeleteAnnouncementsMainAllByAdmin(int AnnouncementId)
        {
            SqlConnection oConnection = new SqlConnection(BaseConnStr);

            oConnection.Open();

            SqlCommand oCommand = new SqlCommand("sp_AnnouncementMainDeleteByAdmin", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter param;
            param = new SqlParameter("@p_AnnouncementId", SqlDbType.Int);
            param.Value = AnnouncementId;
            oCommand.Parameters.Add(param);

            param = new SqlParameter("@p_IsDelete", SqlDbType.Bit);
            param.Value = true;
            oCommand.Parameters.Add(param);

            try
            {
                oCommand.ExecuteNonQuery();
            }
            catch
            {
                throw;
            }

            oCommand.Dispose();
            if (oConnection.State == ConnectionState.Open)
                oConnection.Close();
            oConnection.Dispose();
        }

        public override DataAnnouncementMainEdit GetAnnouncementMainById(int announcementId)
        {
            var result = new DataAnnouncementMainEdit();

            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            oConnection.Open();

            SqlCommand oCommand;
            oCommand = new SqlCommand("sp_AnnouncementMainGetDataIdByAdmin", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            oCommand.Parameters.Add("@p_AnnouncementId", SqlDbType.Int).Value = announcementId;

            SqlDataAdapter oAdapter;
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("AnnouncementMain");
            oAdapter.Fill(oTable);

            if (oTable.Rows.Count > 0)
            {
                foreach (DataRow item in oTable.Rows)
                {
                    result.AnnouncementId = Convert.ToInt32(item["AnnouncementId"]);
                    result.AnnouncementStatusId = Convert.ToInt32(item["AnnouncementStatusId"]);

                    result.Title_TH = item["Title_TH"].ToString();
                    result.Description_TH = item["Description_TH"].ToString();
                    result.Remark_TH = item["Remark_TH"].ToString();

                    result.Title_EN = item["Title_EN"].ToString();
                    result.Description_EN = item["Description_EN"].ToString();
                    result.Remark_EN = item["Remark_EN"].ToString();

                    result.EffectiveDate = Convert.ToDateTime(item["EffectiveDate"]);
                    result.ExpireDate = Convert.ToDateTime(item["ExpireDate"]);
                    result.IsUnlimitedExpire = Convert.ToBoolean(item["IsUnlimitedExpire"]);
                }

                // Load Picture
                oCommand = new SqlCommand("sp_AnnouncementGetPictureByAdmin", oConnection);
                oCommand.CommandType = CommandType.StoredProcedure;

                oCommand.Parameters.Clear();
                oCommand.Parameters.Add("@p_AnnouncementId", SqlDbType.Int).Value = announcementId;

                oAdapter = new SqlDataAdapter(oCommand);
                DataTable oTablePicture = new DataTable("AnnouncementMainPicture");
                oAdapter.Fill(oTablePicture);

                if (oTablePicture.Rows.Count > 0)
                {
                    foreach (DataRow item in oTablePicture.Rows)
                    {
                        DataFileAnnouncementMain picture = new DataFileAnnouncementMain();

                        if (item["Language"].ToString().ToLower() == "en")
                        {
                            picture.AnnouncementFileId = Convert.ToInt32(item["AnnouncementFileId"]);
                            picture.FileName = item["TextFile"].ToString();
                            picture.Language = item["Language"].ToString();
                            picture.FileSequence = Convert.ToInt32(item["Sequence"]);
                            result.ListPicture_EN.Add(picture);
                        }

                        if (item["Language"].ToString().ToLower() == "th")
                        {
                            picture.AnnouncementFileId = Convert.ToInt32(item["AnnouncementFileId"]);
                            picture.FileName = item["TextFile"].ToString();
                            picture.Language = item["Language"].ToString();
                            picture.FileSequence = Convert.ToInt32(item["Sequence"]);
                            result.ListPicture_TH.Add(picture);
                        }
                    }
                }

                if (result.ListPicture_EN.Count < 4)
                {
                    int count_en = 3 - result.ListPicture_EN.Count;

                    for (int i = 0; i < count_en; i++)
                    {
                        DataFileAnnouncementMain picture = new DataFileAnnouncementMain()
                        {
                            FileName = "",
                            FileSequence = 1,
                            Language = "EN"
                        };
                        result.ListPicture_EN.Add(picture);
                    }
                }

                if (result.ListPicture_TH.Count < 4)
                {
                    int count_th = 3 - result.ListPicture_TH.Count;

                    for (int i = 0; i < count_th; i++)
                    {
                        DataFileAnnouncementMain picture = new DataFileAnnouncementMain()
                        {
                            FileName = "",
                            FileSequence = 1,
                            Language = "TH"
                        };
                        result.ListPicture_TH.Add(picture);
                    }
                }
            }
            return result;
        }

        public override void EditAnnouncementMain(AnnouncementDataAll model)
        {
            var date_now = DateTime.Now;
            SqlConnection oConnection = new SqlConnection(BaseConnStr);

            oConnection.Open();

            SqlTransaction oTransaction = oConnection.BeginTransaction();
            SqlCommand oCommand;
            oCommand = new SqlCommand("sp_AnnouncementMainEditDataByAdmin", oConnection, oTransaction);
            oCommand.CommandType = CommandType.StoredProcedure;

            oCommand.Parameters.Clear();
            oCommand.Parameters.Add("@p_AnnouncementId", SqlDbType.Int).Value = model.AnnouncementId;
            oCommand.Parameters.Add("@p_AnnouncementTypeId", SqlDbType.Int).Value = model.AnnouncementTypeId;
            oCommand.Parameters.Add("@p_AnnouncementStatusId", SqlDbType.Int).Value = model.AnnouncementStatusId;
            oCommand.Parameters.Add("@p_EffectiveDate", SqlDbType.DateTime).Value = model.EffectiveDate;
            oCommand.Parameters.Add("@p_ExpireDate", SqlDbType.DateTime).Value = model.ExpireDate;
            oCommand.Parameters.Add("@p_IsUnlimitedExpire", SqlDbType.Bit).Value = model.IsUnlimitedExpire;
            oCommand.Parameters.Add("@p_Title_TH", SqlDbType.NVarChar).Value = model.Title_TH;
            oCommand.Parameters.Add("@p_Description_TH", SqlDbType.NVarChar).Value = model.Description_TH;
            oCommand.Parameters.Add("@p_Remark_TH", SqlDbType.NVarChar).Value = model.Remark_TH;
            oCommand.Parameters.Add("@p_Title_EN", SqlDbType.NVarChar).Value = model.Title_EN;
            oCommand.Parameters.Add("@p_Description_EN", SqlDbType.NVarChar).Value = model.Description_EN;
            oCommand.Parameters.Add("@p_Remark_EN", SqlDbType.NVarChar).Value = model.Remark_EN;
            oCommand.Parameters.Add("@p_UpdateBy", SqlDbType.NVarChar).Value = model.UpdateBy;
            oCommand.Parameters.Add("@p_UpdateDate", SqlDbType.DateTime).Value = date_now;

            try
            {
                oCommand.ExecuteNonQuery();

                string sql_delete = "DELETE FROM AnnouncementDataFile WHERE AnnouncementId = @p_AnnouncementId;";
                oCommand = new SqlCommand(sql_delete, oConnection, oTransaction);
                oCommand.CommandType = CommandType.Text;

                oCommand.Parameters.Clear();
                oCommand.Parameters.Add("@p_AnnouncementId", SqlDbType.Int).Value = model.AnnouncementId;
                oCommand.ExecuteNonQuery();

                if (model.ListFile.Count > 0)
                {
                    oCommand = new SqlCommand("sp_AnnouncementMainSaveFileByAdmin", oConnection, oTransaction);
                    oCommand.CommandType = CommandType.StoredProcedure;
                    foreach (var item in model.ListFile)
                    {
                        oCommand.Parameters.Clear();
                        oCommand.Parameters.Add("@p_AnnouncementId", SqlDbType.Int).Value = model.AnnouncementId;
                        oCommand.Parameters.Add("@p_TextFile", SqlDbType.NVarChar).Value = item.TextFile;
                        oCommand.Parameters.Add("@P_Sequence", SqlDbType.Int).Value = item.Sequence;
                        oCommand.Parameters.Add("@p_Language", SqlDbType.NVarChar).Value = item.Language;
                        oCommand.Parameters.Add("@p_CreateBy", SqlDbType.NVarChar).Value = model.CreateBy;
                        oCommand.Parameters.Add("@p_CreateDate", SqlDbType.DateTime).Value = DateTime.Now;
                        oCommand.ExecuteNonQuery();
                    }
                }

                oTransaction.Commit();
            }
            catch
            {
                oTransaction.Rollback();
                oCommand.Dispose();
                if (oConnection.State == ConnectionState.Open)
                    oConnection.Close();
                oConnection.Dispose();
                oTransaction.Dispose();

                throw;
            }
            finally
            {
                oCommand.Dispose();
                if (oConnection.State == ConnectionState.Open)
                    oConnection.Close();
                oConnection.Dispose();
                oTransaction.Dispose();
            }
        }

        #endregion

        #region ��С�����ʴԡ�þ�ѡ�ҹ����
        
        public override void SaveAnnouncementEmployeeWelfareByAdmin(AnnouncementDataAll model)
        {
            var date_now = DateTime.Now;
            SqlConnection oConnection = new SqlConnection(BaseConnStr);

            oConnection.Open();

            SqlTransaction oTransaction = oConnection.BeginTransaction();
            SqlCommand oCommand;
            oCommand = new SqlCommand("sp_AnnouncementEmployeeWelfareSaveByAdmin", oConnection, oTransaction);
            oCommand.CommandType = CommandType.StoredProcedure;

            //�ͧ�Ѻ PK ��� Return �͡��
            oCommand.Parameters.Add("@p_AnnouncementId", SqlDbType.Int).Direction = ParameterDirection.Output;
            oCommand.Parameters.Add("@p_AnnouncementTypeId", SqlDbType.Int).Value = 4;  // ��������С����ѡ = 4
            oCommand.Parameters.Add("@p_AnnouncementStatusId", SqlDbType.Int).Value = 1;
            oCommand.Parameters.Add("@p_Sequence", SqlDbType.Int).Value = model.Sequence;
            oCommand.Parameters.Add("@p_EffectiveDate", SqlDbType.DateTime).Value = model.EffectiveDate;
            oCommand.Parameters.Add("@p_ExpireDate", SqlDbType.DateTime).Value = model.ExpireDate;
            oCommand.Parameters.Add("@p_IsUnlimitedExpire", SqlDbType.Bit).Value = model.IsUnlimitedExpire;
            oCommand.Parameters.Add("@p_Title_TH", SqlDbType.NVarChar).Value = model.Title_TH;
            oCommand.Parameters.Add("@p_Description_TH", SqlDbType.NVarChar).Value = model.Description_TH;
            oCommand.Parameters.Add("@p_Remark_TH", SqlDbType.NVarChar).Value = model.Remark_TH;
            oCommand.Parameters.Add("@p_Title_EN", SqlDbType.NVarChar).Value = model.Title_EN;
            oCommand.Parameters.Add("@p_Description_EN", SqlDbType.NVarChar).Value = model.Description_EN;
            oCommand.Parameters.Add("@p_Remark_EN", SqlDbType.NVarChar).Value = model.Remark_EN;
            oCommand.Parameters.Add("@p_CreateBy", SqlDbType.NVarChar).Value = model.CreateBy;
            oCommand.Parameters.Add("@p_CreateDate", SqlDbType.NVarChar).Value = date_now;
            oCommand.Parameters.Add("@p_UpdateBy", SqlDbType.NVarChar).Value = model.CreateBy;
            oCommand.Parameters.Add("@p_UpdateDate", SqlDbType.NVarChar).Value = date_now;

            try
            {
                oCommand.ExecuteNonQuery();
                string p_AnnouncementId = oCommand.Parameters["@p_AnnouncementId"].Value.ToString();
                int cv_p_AnnouncementId = int.Parse(p_AnnouncementId);

                if (model.ListFile.Count > 0)
                {
                    oCommand = new SqlCommand("sp_AnnouncementEmployeeWelfareSaveFileByAdmin", oConnection, oTransaction);
                    oCommand.CommandType = CommandType.StoredProcedure;
                    foreach (var item in model.ListFile)
                    {
                        oCommand.Parameters.Clear();
                        oCommand.Parameters.Add("@p_AnnouncementId", SqlDbType.Int).Value = cv_p_AnnouncementId;
                        oCommand.Parameters.Add("@p_TextFile", SqlDbType.NVarChar).Value = item.TextFile;
                        oCommand.Parameters.Add("@P_Sequence", SqlDbType.Int).Value = item.Sequence;
                        oCommand.Parameters.Add("@p_Language", SqlDbType.NVarChar).Value = item.Language;
                        oCommand.Parameters.Add("@p_CreateBy", SqlDbType.NVarChar).Value = model.CreateBy;
                        oCommand.Parameters.Add("@p_CreateDate", SqlDbType.DateTime).Value = DateTime.Now;
                        oCommand.ExecuteNonQuery();
                    }
                }
                oTransaction.Commit();
            }
            catch
            {
                oTransaction.Rollback();

                oCommand.Dispose();
                if (oConnection.State == ConnectionState.Open)
                    oConnection.Close();
                oConnection.Dispose();
                oTransaction.Dispose();
                throw;
            }
            finally
            {
                oCommand.Dispose();
                if (oConnection.State == ConnectionState.Open)
                    oConnection.Close();
                oConnection.Dispose();
                oTransaction.Dispose();
            }
        }

        public override List<AnnouncementDataAll> GetAllAnnouncementWelfareByAdmin()
        {
            var result = new List<AnnouncementDataAll>();

            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            oConnection.Open();

            SqlCommand oCommand;
            SqlDataAdapter oAdapter;


            string cmd = "Select * From AnnouncementData ad";
            cmd += " Where ad.AnnouncementTypeId = @p_AnnouncementTypeId And ad.IsDelete = 0";
            cmd += " ORDER BY ad.[Sequence] asc";
            oCommand = new SqlCommand(cmd, oConnection);
            oCommand.CommandType = CommandType.Text;

            oCommand.Parameters.Add("@p_AnnouncementTypeId", SqlDbType.Int).Value = 4; //��������С�����ʴԡ������
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("AnnouncementWelfare");
            oAdapter.Fill(oTable);
            
            if(oTable.Rows.Count > 0)
            {
                foreach(DataRow item in oTable.Rows)
                {
                    AnnouncementDataAll data = new AnnouncementDataAll();

                    data.ParseToObject(item);

                    result.Add(data);
                }
            }


            oAdapter.Dispose();
            oCommand.Dispose();
            if (oConnection.State == ConnectionState.Open)
                oConnection.Close();

            return result;
        }

        public override void DeleteAnnouncementWelfareByAdmin(int announcementId)
        {

            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            oConnection.Open();

            SqlTransaction oTransaction = oConnection.BeginTransaction();
            SqlCommand oCommand;

            string sql_cmd = "UPDATE AnnouncementData SET IsDelete = 1";
            sql_cmd += " WHERE AnnouncementId = @p_AnnouncementId And AnnouncementTypeId = @p_AnnouncementTypeId";
            oCommand = new SqlCommand(sql_cmd, oConnection, oTransaction);
            oCommand.CommandType = CommandType.Text;

            oCommand.Parameters.Add("@p_AnnouncementId", SqlDbType.Int).Value = announcementId;
            oCommand.Parameters.Add("@p_AnnouncementTypeId", SqlDbType.Int).Value = 4;

            try
            {
                oCommand.ExecuteNonQuery();
                oTransaction.Commit();
            }
            catch
            {
                oTransaction.Rollback();

                oCommand.Dispose();
                if (oConnection.State == ConnectionState.Open)
                    oConnection.Close();
                oConnection.Dispose();
                oTransaction.Dispose();
                throw;
            }
            finally
            {
                oCommand.Dispose();
                if (oConnection.State == ConnectionState.Open)
                    oConnection.Close();
                oConnection.Dispose();
                oTransaction.Dispose();
            }
        }

        public override void EditSequenceAnnouncementWelfareByAdmin(List<DataEditSequenceAnnouncementPublicRelations> model)
        {
            if (model.Count > 0)
            {
                if (model.Count > 0)
                {
                    SqlConnection oConnection = new SqlConnection(BaseConnStr);
                    oConnection.Open();

                    SqlTransaction oTransaction = oConnection.BeginTransaction();

                    string sql_cmd = "UPDATE AnnouncementData SET Sequence = @p_Sequence";
                    sql_cmd += " WHERE AnnouncementId = @p_AnnouncementId";

                    SqlCommand oCommand = new SqlCommand(sql_cmd, oConnection, oTransaction);
                    oCommand.CommandType = CommandType.Text;

                    foreach (var item in model)
                    {
                        oCommand.Parameters.Clear();

                        SqlParameter param;
                        param = new SqlParameter("@p_AnnouncementId", SqlDbType.Int);
                        param.Value = item.AnnouncementId;
                        oCommand.Parameters.Add(param);

                        param = new SqlParameter("@p_Sequence", SqlDbType.Int);
                        param.Value = item.Sequence;
                        oCommand.Parameters.Add(param);

                        try
                        {
                            oCommand.ExecuteNonQuery();
                        }
                        catch
                        {
                            oTransaction.Rollback();
                            oCommand.Dispose();
                            if (oConnection.State == ConnectionState.Open)
                                oConnection.Close();
                            oConnection.Dispose();
                            oTransaction.Dispose();
                            throw;
                        }
                    }
                    oTransaction.Commit();

                    oCommand.Dispose();
                    if (oConnection.State == ConnectionState.Open)
                        oConnection.Close();
                    oConnection.Dispose();
                    oTransaction.Dispose();
                }
            }
        }

        public override DataAnnouncementEmployeeWelfare GetAnnouncementWelfareById(int announcementId)
        {
            DataAnnouncementEmployeeWelfare result = new DataAnnouncementEmployeeWelfare();

            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            oConnection.Open();

            SqlCommand oCommand;
            string cmd = "Select * From AnnouncementData";
            cmd += " Where AnnouncementId = @p_AnnouncementId And AnnouncementTypeId = @p_AnnouncementTypeId And IsDelete = 0";
            oCommand = new SqlCommand(cmd, oConnection);
            oCommand.CommandType = CommandType.Text;

            oCommand.Parameters.Add("@p_AnnouncementId", SqlDbType.Int).Value = announcementId;
            oCommand.Parameters.Add("@p_AnnouncementTypeId", SqlDbType.Int).Value = 4;

            SqlDataAdapter oAdapter;
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("AnnouncementWelfare");
            oAdapter.Fill(oTable);


            if (oTable.Rows.Count > 0)
            {
                foreach (DataRow item in oTable.Rows)
                {
                    result.AnnouncementId = Convert.ToInt32(item["AnnouncementId"]);
                    result.AnnouncementStatusId = Convert.ToInt32(item["AnnouncementStatusId"]);
                    result.Sequence = Convert.ToInt32(item["Sequence"]);

                    result.Title_TH = item["Title_TH"].ToString();
                    result.Description_TH = item["Description_TH"].ToString();
                    result.Remark_TH = item["Remark_TH"].ToString();

                    result.Title_EN = item["Title_EN"].ToString();
                    result.Description_EN = item["Description_EN"].ToString();
                    result.Remark_EN = item["Remark_EN"].ToString();

                    result.EffectiveDate = Convert.ToDateTime(item["EffectiveDate"]);
                    result.ExpireDate = Convert.ToDateTime(item["ExpireDate"]);
                    result.IsUnlimitedExpire = Convert.ToBoolean(item["IsUnlimitedExpire"]);
                }

                // Load Picture
                oCommand = new SqlCommand("sp_AnnouncementGetPictureByAdmin", oConnection);
                oCommand.CommandType = CommandType.StoredProcedure;

                oCommand.Parameters.Clear();
                oCommand.Parameters.Add("@p_AnnouncementId", SqlDbType.Int).Value = announcementId;

                oAdapter = new SqlDataAdapter(oCommand);
                DataTable oTablePicture = new DataTable("AnnouncementWelfarePicture");
                oAdapter.Fill(oTablePicture);

                if (oTablePicture.Rows.Count > 0)
                {
                    foreach (DataRow item in oTablePicture.Rows)
                    {
                        DataFileAnnouncementEmployeeWelfare picture = new DataFileAnnouncementEmployeeWelfare();

                        if (item["Language"].ToString().ToLower() == "en")
                        {
                            picture.AnnouncementFileId = Convert.ToInt32(item["AnnouncementFileId"]);
                            picture.FileName = item["TextFile"].ToString();
                            picture.Language = item["Language"].ToString();
                            picture.FileSequence = Convert.ToInt32(item["Sequence"]);
                            result.ListPicture_EN.Add(picture);
                        }

                        if (item["Language"].ToString().ToLower() == "th")
                        {
                            picture.AnnouncementFileId = Convert.ToInt32(item["AnnouncementFileId"]);
                            picture.FileName = item["TextFile"].ToString();
                            picture.Language = item["Language"].ToString();
                            picture.FileSequence = Convert.ToInt32(item["Sequence"]);
                            result.ListPicture_TH.Add(picture);
                        }
                    }
                }
            }

            if (result.ListPicture_EN.Count < 4)
            {
                int count_en = 3 - result.ListPicture_EN.Count;

                for (int i = 0; i < count_en; i++)
                {
                    DataFileAnnouncementEmployeeWelfare picture = new DataFileAnnouncementEmployeeWelfare()
                    {
                        FileName = "",
                        FileSequence = 1,
                        Language = "EN"
                    };
                    result.ListPicture_EN.Add(picture);
                }
            }

            if (result.ListPicture_TH.Count < 4)
            {
                int count_th = 3 - result.ListPicture_TH.Count;

                for (int i = 0; i < count_th; i++)
                {
                    DataFileAnnouncementEmployeeWelfare picture = new DataFileAnnouncementEmployeeWelfare()
                    {
                        FileName = "",
                        FileSequence = 1,
                        Language = "TH"
                    };
                    result.ListPicture_TH.Add(picture);
                }
            }

            return result;
        }

        public override void EditAnnouncementEmployeeWelfareByAdmin(AnnouncementDataAll model)
        {
            var date_now = DateTime.Now;
            SqlConnection oConnection = new SqlConnection(BaseConnStr);

            oConnection.Open();

            SqlTransaction oTransaction = oConnection.BeginTransaction();
            SqlCommand oCommand;
            oCommand = new SqlCommand("sp_AnnouncementEmployeeWelfareEditDataByAdmin", oConnection, oTransaction);
            oCommand.CommandType = CommandType.StoredProcedure;

            oCommand.Parameters.Clear();
            oCommand.Parameters.Add("@p_AnnouncementId", SqlDbType.Int).Value = model.AnnouncementId;
            oCommand.Parameters.Add("@p_AnnouncementTypeId", SqlDbType.Int).Value = model.AnnouncementTypeId;
            oCommand.Parameters.Add("@p_AnnouncementStatusId", SqlDbType.Int).Value = model.AnnouncementStatusId;
            oCommand.Parameters.Add("@p_EffectiveDate", SqlDbType.DateTime).Value = model.EffectiveDate;
            oCommand.Parameters.Add("@p_ExpireDate", SqlDbType.DateTime).Value = model.ExpireDate;
            oCommand.Parameters.Add("@p_IsUnlimitedExpire", SqlDbType.Bit).Value = model.IsUnlimitedExpire;
            oCommand.Parameters.Add("@p_Title_TH", SqlDbType.NVarChar).Value = model.Title_TH;
            oCommand.Parameters.Add("@p_Description_TH", SqlDbType.NVarChar).Value = model.Description_TH;
            oCommand.Parameters.Add("@p_Remark_TH", SqlDbType.NVarChar).Value = model.Remark_TH;
            oCommand.Parameters.Add("@p_Title_EN", SqlDbType.NVarChar).Value = model.Title_EN;
            oCommand.Parameters.Add("@p_Description_EN", SqlDbType.NVarChar).Value = model.Description_EN;
            oCommand.Parameters.Add("@p_Remark_EN", SqlDbType.NVarChar).Value = model.Remark_EN;
            oCommand.Parameters.Add("@p_UpdateBy", SqlDbType.NVarChar).Value = model.UpdateBy;
            oCommand.Parameters.Add("@p_UpdateDate", SqlDbType.DateTime).Value = date_now;
            oCommand.Parameters.Add("@p_Sequence", SqlDbType.Int).Value = model.Sequence;

            try
            {
                oCommand.ExecuteNonQuery();

                string sql_delete = "DELETE FROM AnnouncementDataFile WHERE AnnouncementId = @p_AnnouncementId;";
                oCommand = new SqlCommand(sql_delete, oConnection, oTransaction);
                oCommand.CommandType = CommandType.Text;

                oCommand.Parameters.Clear();
                oCommand.Parameters.Add("@p_AnnouncementId", SqlDbType.Int).Value = model.AnnouncementId;
                oCommand.ExecuteNonQuery();

                if (model.ListFile.Count > 0)
                {
                    oCommand = new SqlCommand("sp_AnnouncementEmployeeWelfareSaveFileByAdmin", oConnection, oTransaction);
                    oCommand.CommandType = CommandType.StoredProcedure;
                    foreach (var item in model.ListFile)
                    {
                        oCommand.Parameters.Clear();
                        oCommand.Parameters.Add("@p_AnnouncementId", SqlDbType.Int).Value = model.AnnouncementId;
                        oCommand.Parameters.Add("@p_TextFile", SqlDbType.NVarChar).Value = item.TextFile;
                        oCommand.Parameters.Add("@P_Sequence", SqlDbType.Int).Value = item.Sequence;
                        oCommand.Parameters.Add("@p_Language", SqlDbType.NVarChar).Value = item.Language;
                        oCommand.Parameters.Add("@p_CreateBy", SqlDbType.NVarChar).Value = model.CreateBy;
                        oCommand.Parameters.Add("@p_CreateDate", SqlDbType.DateTime).Value = DateTime.Now;
                        oCommand.ExecuteNonQuery();
                    }
                }

                oTransaction.Commit();
            }
            catch
            {
                oTransaction.Rollback();
                oCommand.Dispose();
                if (oConnection.State == ConnectionState.Open)
                    oConnection.Close();
                oConnection.Dispose();
                oTransaction.Dispose();

                throw;
            }
            finally
            {
                oCommand.Dispose();
                if (oConnection.State == ConnectionState.Open)
                    oConnection.Close();
                oConnection.Dispose();
                oTransaction.Dispose();
            }
        }

        public override int GetMaxGetMaxSequenceWelfareAnnouncement()
        {
            int sequence = 0;

            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            oConnection.Open();

            SqlCommand oCommand;
            oCommand = new SqlCommand("sp_AnnouncementEmployeeWelfareGetMaxSequence", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter oAdapter = new SqlDataAdapter();
            oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("MaxSequence");
            oAdapter.Fill(oTable);
            
            if(oTable.Rows.Count > 0)
            {
                foreach(DataRow dr in oTable.Rows)
                {
                    if (string.IsNullOrEmpty(dr["MaxSequence"].ToString()))
                    {
                        sequence = 1;
                    }
                    else
                    {
                        int max = Convert.ToInt16(dr["MaxSequence"].ToString());
                        sequence = max + 1;
                    }
                }
            }


            return sequence;
        }

        #endregion

        #region ��С����ѡ������ (��ѡ�ҹ����ٻ�С�ȷ�����)

        public override PaginationAnnouncementMainNotifyAll GetPaginationAnnouncementMainNotify(int page, int itemPerPage, string language,string employeeId)
        {
            PaginationAnnouncementMainNotifyAll pagination_all = new PaginationAnnouncementMainNotifyAll();

            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            oConnection.Open();

            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable;
            //SqlParameter param;

            int total_transaction = 0;
            string sql_cmd = "Select Count(*) as count_all  From AnnouncementData ad";
            sql_cmd += " Where ad.AnnouncementTypeId = 1 And ad.IsDelete = 0 AND ad.AnnouncementStatusId = 1";
            sql_cmd += " AND (ad.IsUnlimitedExpire = 1 OR ( @p_datedata BETWEEN ad.EffectiveDate AND ad.[ExpireDate]))";

            oCommand = new SqlCommand(sql_cmd, oConnection);
            oCommand.CommandType = CommandType.Text;
            oCommand.Parameters.Add("@p_datedata", SqlDbType.DateTime).Value = DateTime.Now.Date;

            oAdapter = new SqlDataAdapter(oCommand);
            oTable = new DataTable("CountAnnouncementMain");
            oAdapter.Fill(oTable);

            if (oTable.Rows.Count > 0)
            {
                foreach (DataRow item in oTable.Rows)
                {
                    total_transaction = Convert.ToInt32(item["count_all"]);
                }
            }

            if (total_transaction > 0)
            {
                oCommand = new SqlCommand("sp_AnnouncementMainNotifyGetAllByPeriod", oConnection);
                oCommand.CommandType = CommandType.StoredProcedure;
                oCommand.Parameters.Clear();

                oCommand.Parameters.Add("@p_AnnouncementTypeId", SqlDbType.Int).Value = 1;  // ������ѡ fix ������

                int skip_data = (page - 1) * itemPerPage; // Skip ������㹵��ҧ
                oCommand.Parameters.Add("@p_Skip", SqlDbType.Int).Value = skip_data;
                oCommand.Parameters.Add("@p_Take", SqlDbType.Int).Value = itemPerPage;
                oCommand.Parameters.Add("@p_datedata", SqlDbType.DateTime).Value = DateTime.Now.Date;
                oCommand.Parameters.Add("@p_language", SqlDbType.NVarChar).Value = language.ToUpper();

                oAdapter = new SqlDataAdapter(oCommand);

                oTable.Clear();
                oTable = new DataTable("AnnouncementMain");
                oAdapter.Fill(oTable);

                if (oTable.Rows.Count > 0)
                {
                    foreach (DataRow item in oTable.Rows)
                    {
                        DataAnnouncementNotifyAll data = new DataAnnouncementNotifyAll();
                        data.ParseToObject(item);
                        pagination_all.List_AnnouncementMain.Add(data);
                    }
                }

                // ���ǡ�д�觷�边ѡ�ҹ��ҹ���Ƿ�����
                oCommand = new SqlCommand("sp_AnnouncementMainNotifyGetDataToBellByEmployeeRead", oConnection);
                oCommand.CommandType = CommandType.StoredProcedure;

                oCommand.Parameters.Add("@p_EmployeeId", SqlDbType.VarChar).Value = employeeId;  // ������ѡ fix ������
                oCommand.Parameters.Add("@p_Category", SqlDbType.VarChar).Value = "AnnouncementMain";

                oAdapter = new SqlDataAdapter(oCommand);
                oTable = new DataTable("AnnouncementMainBellRead");
                oAdapter.Fill(oTable);

                // ���Ƿ���ա����ҹ���Ƿ�����
                if (oTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in oTable.Rows)
                    {
                        LogViewAnnouncement log_view = new LogViewAnnouncement();
                        log_view.ParseToObject(dr);

                        var data_read = pagination_all.List_AnnouncementMain.Where(s => s.AnnouncementId == log_view.AnnouncementId).FirstOrDefault();
                        if (data_read != null)
                        {
                            data_read.IsRead = true;
                        }
                    }
                }

            }




            pagination_all.Page = page;
            pagination_all.ItemPerPage = itemPerPage;
            pagination_all.Total = total_transaction;

            oTable.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();
            oConnection.Dispose();
            if (oConnection.State == ConnectionState.Open)
                oConnection.Close();
            

            return pagination_all;
        }

        public override AnnouncementNotifyDetail GetAnnouncementMainNotifyById(int announcementId, string language)
        {
            AnnouncementNotifyDetail result = new AnnouncementNotifyDetail();
            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            oConnection.Open();

            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable;

            oCommand = new SqlCommand("sp_AnnouncementMainNotifyGetByIdPeriod", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            oCommand.Parameters.Add("@p_AnnouncementId", SqlDbType.Int).Value = announcementId;
            oCommand.Parameters.Add("@p_AnnouncementTypeId", SqlDbType.Int).Value = 1;
            oCommand.Parameters.Add("@p_datedata", SqlDbType.DateTime).Value = DateTime.Now.Date;

            oAdapter = new SqlDataAdapter(oCommand);
            oTable = new DataTable("AnnouncementMain");
            oAdapter.Fill(oTable);

            if (oTable.Rows.Count > 0)
            {
                foreach (DataRow item in oTable.Rows)
                {
                    result.ParseToObject(item);
                }

                // �ٻ�Ҿ
                string cmd = "SELECT * FROM AnnouncementDataFile " +
                    "WHERE AnnouncementId = @p_AnnouncementId " +
                    "AND Language = @p_Language";
                oCommand = new SqlCommand(cmd, oConnection);
                oCommand.CommandType = CommandType.Text;
                oCommand.Parameters.Clear();

                oCommand.Parameters.Add("@p_AnnouncementId", SqlDbType.Int).Value = announcementId;
                oCommand.Parameters.Add("@p_Language", SqlDbType.NVarChar).Value = language.ToUpper();
                oAdapter = new SqlDataAdapter(oCommand);
                oTable = new DataTable("AnnouncementMainFile");
                oAdapter.Fill(oTable);
                if(oTable.Rows.Count > 0)
                {
                    foreach (DataRow item in oTable.Rows)
                    {
                        string txt_file = item["TextFile"].ToString();
                        result.TextFile.Add(txt_file);
                    }
                }
                result.IsHaveAnnouncement = true;
            }

            oTable.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();
            oConnection.Dispose();
            if (oConnection.State == ConnectionState.Open)
                oConnection.Close();

            return result;
        }

        public override List<DataAnnouncementNotifyAll> GetPaginationAnnouncementMainNotifyToBell(string language,int BellShowDataANNOUNCEMENT,string employeeId)
        {
            var list_AnnouncementNotify = new List<DataAnnouncementNotifyAll>();

            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            oConnection.Open();

            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable;

            oCommand = new SqlCommand("sp_AnnouncementMainNotifyGetDataToBellByPeriod", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            oCommand.Parameters.Add("@p_AnnouncementTypeId", SqlDbType.Int).Value = 1;  // ������ѡ fix ������
            oCommand.Parameters.Add("@p_datedata", SqlDbType.DateTime).Value = DateTime.Now.Date;  
            oCommand.Parameters.Add("@p_language", SqlDbType.NVarChar).Value = language; // ����

            oAdapter = new SqlDataAdapter(oCommand);
            oTable = new DataTable("AnnouncementMainBell");
            oAdapter.Fill(oTable);

            // ���ǡ�д�觷�����
            if(oTable.Rows.Count > 0)
            {
                foreach(DataRow dr in oTable.Rows)
                {
                    DataAnnouncementNotifyAll announcement = new DataAnnouncementNotifyAll();
                    announcement.ParseToObject(dr);
                    list_AnnouncementNotify.Add(announcement);
                }
            }


            // ���ǡ�д�觷�边ѡ�ҹ��ҹ���Ƿ�����
            oCommand = new SqlCommand("sp_AnnouncementMainNotifyGetDataToBellByEmployeeRead", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            oCommand.Parameters.Add("@p_EmployeeId", SqlDbType.VarChar).Value = employeeId;  // ������ѡ fix ������
            oCommand.Parameters.Add("@p_Category", SqlDbType.VarChar).Value = "AnnouncementMain";

            oAdapter = new SqlDataAdapter(oCommand);
            oTable = new DataTable("AnnouncementMainBellRead");
            oAdapter.Fill(oTable);

            // ���Ƿ���ա����ҹ���Ƿ�����
            if (oTable.Rows.Count > 0)
            {
                foreach (DataRow dr in oTable.Rows)
                {
                    LogViewAnnouncement log_view = new LogViewAnnouncement();
                    log_view.ParseToObject(dr);

                    var data_read = list_AnnouncementNotify.Where(s => s.AnnouncementId == log_view.AnnouncementId).FirstOrDefault();
                    if(data_read != null)
                    {
                        data_read.IsRead = true;
                    }
                }
            }


            oTable.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();
            oConnection.Dispose();
            if (oConnection.State == ConnectionState.Open)
                oConnection.Close();

            return list_AnnouncementNotify.Take(BellShowDataANNOUNCEMENT).ToList();
        }

        public override AlarmNotify CalculateNotifyAnnouncement(string employeeId)
        {
            AlarmNotify result = new AlarmNotify();

            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            oConnection.Open();

            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable;

            // 1.��¡�û�С�ȷ����������ҹ����ѧ����������
            string sql_cmd = "SELECT * FROM AnnouncementData ad";
            sql_cmd += " WHERE ad.AnnouncementTypeId = @p_AnnouncementTypeId And ad.IsDelete = 0 " +
                "AND ad.AnnouncementStatusId = 1 " +
                "AND(ad.IsUnlimitedExpire = 1 OR (@p_datedata BETWEEN ad.EffectiveDate AND ad.[ExpireDate]))";

            oCommand = new SqlCommand(sql_cmd, oConnection);
            oCommand.CommandType = CommandType.Text;
            oCommand.Parameters.Add("@p_AnnouncementTypeId", SqlDbType.Int).Value = 1;
            oCommand.Parameters.Add("@p_datedata", SqlDbType.DateTime).Value = DateTime.Now.Date;

            oAdapter = new SqlDataAdapter(oCommand);
            oTable = new DataTable("AnnouncementMainAll");
            oAdapter.Fill(oTable);

            if(oTable.Rows.Count > 0)
            {
                List<int> arr_announcementId = new List<int>();
                foreach (DataRow dr in oTable.Rows)
                {
                    string str_id = dr["AnnouncementId"].ToString();
                    int id;
                    if (int.TryParse(str_id, out id))
                    {
                        result.Notify = result.Notify + 1;
                        arr_announcementId.Add(id);
                    }
                }

                // 2.Get ���Ƿ��ҧ��ѡ�ҹ������������º��º
                sql_cmd = "SELECT * FROM AnnouncementDataView adv";
                sql_cmd += " WHERE EmployeeId = @p_EmployeeId AND Category = @p_Category";
                oCommand = new SqlCommand(sql_cmd, oConnection);
                oCommand.CommandType = CommandType.Text;
                oCommand.Parameters.Clear();

                oCommand.Parameters.Add("@p_EmployeeId", SqlDbType.NVarChar).Value = employeeId;
                oCommand.Parameters.Add("@p_Category", SqlDbType.NVarChar).Value = "AnnouncementMain";

                oAdapter = new SqlDataAdapter(oCommand);
                oTable = new DataTable("AnnouncementMainView");
                oAdapter.Fill(oTable);

                List<LogViewAnnouncement> list_view_announcement = new List<LogViewAnnouncement>();
                if (oTable.Rows.Count > 0)
                {
                    foreach(DataRow dr in oTable.Rows)
                    {
                        LogViewAnnouncement data_log_view = new LogViewAnnouncement();
                        data_log_view.ParseToObject(dr);
                        list_view_announcement.Add(data_log_view);
                    }

                    if(result.Notify > 0)
                    {
                        foreach(int ann_id in arr_announcementId)
                        {
                            var view_announcement = list_view_announcement.Where(s => s.AnnouncementId == ann_id).FirstOrDefault();
                            if(view_announcement != null)
                            {
                                result.Notify = result.Notify - 1;
                            }
                        }
                    }
                }
                
            }
            else
            {
                result.Notify = 0;
            }

            oTable.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();
            oConnection.Dispose();
            if (oConnection.State == ConnectionState.Open)
                oConnection.Close();

            return result;
        }

        public override void SaveOrUpdateViewAnnouncementMain(string employeeId, int announcementId)
        {
            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            oConnection.Open();

            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable;

            SqlTransaction oTransaction = oConnection.BeginTransaction();

            string sql_cmd = "Select * FROM AnnouncementDataView ";
            sql_cmd += " WHERE AnnouncementId = @p_AnnouncementId " +
                "AND EmployeeId = @p_EmployeeId " +
                "AND Category = 'AnnouncementMain'";

            oCommand = new SqlCommand(sql_cmd, oConnection, oTransaction);
            oCommand.CommandType = CommandType.Text;
            oCommand.Parameters.Add("@p_AnnouncementId", SqlDbType.Int).Value = announcementId;
            oCommand.Parameters.Add("@p_EmployeeId", SqlDbType.NVarChar).Value = employeeId;

            oAdapter = new SqlDataAdapter(oCommand);
            oTable = new DataTable("DuplicateAnnouncement");
            oAdapter.Fill(oTable);

            try
            {
                if (oTable.Rows.Count > 0)
                {
                    // Update �ѹ���٢�������ش
                    string sql_update = "UPDATE AnnouncementDataView " +
                        "SET ViewDate = @p_dateView " +
                        "WHERE AnnouncementId = @p_AnnouncementId AND EmployeeId = @p_EmployeeId " +
                        "AND Category = 'AnnouncementMain'";

                    oCommand = new SqlCommand(sql_update, oConnection, oTransaction);
                    oCommand.CommandType = CommandType.Text;
                    oCommand.Parameters.Clear();

                    oCommand.Parameters.Add("@p_AnnouncementId", SqlDbType.Int).Value = announcementId;
                    oCommand.Parameters.Add("@p_EmployeeId", SqlDbType.NVarChar).Value = employeeId;
                    oCommand.Parameters.Add("@p_dateView", SqlDbType.DateTime).Value = DateTime.Now;
                    oCommand.ExecuteNonQuery();
                }
                else
                {
                    // Insert
                    string sql_insert = "INSERT INTO AnnouncementDataView (AnnouncementId, EmployeeId, ViewDate, Category)";
                    sql_insert += " VALUES (@p_AnnouncementId, @p_EmployeeId, @p_ViewDate, 'AnnouncementMain');";
                    oCommand = new SqlCommand(sql_insert, oConnection, oTransaction);
                    oCommand.CommandType = CommandType.Text;
                    oCommand.Parameters.Clear();

                    oCommand.Parameters.Add("@p_AnnouncementId", SqlDbType.Int).Value = announcementId;
                    oCommand.Parameters.Add("@p_EmployeeId", SqlDbType.NVarChar).Value = employeeId;
                    oCommand.Parameters.Add("@p_ViewDate", SqlDbType.DateTime).Value = DateTime.Now;
                    oCommand.ExecuteNonQuery();
                }

                oTransaction.Commit();
            }
            catch
            {
                oTransaction.Rollback();
                throw;
            }
            finally
            {
                oTable.Dispose();
                oAdapter.Dispose();
                oCommand.Dispose();
                oConnection.Dispose();
                oTransaction.Dispose();
                if (oConnection.State == ConnectionState.Open)
                    oConnection.Close();
            }
        }
        #endregion

        #region �����Ż�С�Ȼ�Ъ�����ѹ�� ��л�С�Ȣ�ͤ��������� (�ʴ���С����龹ѡ�ҹ���)

        public override List<ViewPublicRelations> GetViewPublicRelationsAll()
        {
            var result = new List<ViewPublicRelations>();
            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            SqlCommand oCommand = new SqlCommand();
            SqlDataAdapter oAdapter = new SqlDataAdapter();
            DataTable oTable = new DataTable();

            oCommand = new SqlCommand("sp_AnnouncementPublicRelationsGetAllByEmployee", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.Add("@p_datetime", SqlDbType.DateTime).Value = DateTime.Now.Date;

            oConnection.Open();
            oAdapter = new SqlDataAdapter(oCommand);
            oTable = new DataTable("AnnouncementPublicRelations");
            oAdapter.Fill(oTable);

            if(oTable.Rows.Count > 0)
            {
                foreach(DataRow dr in oTable.Rows)
                {
                    ViewPublicRelations data = new ViewPublicRelations();
                    data.ParseToObject(dr);
                    result.Add(data);
                }
            }

            oCommand.Dispose();
            oAdapter.Dispose();
            oTable.Dispose();
            if (oConnection.State == ConnectionState.Open)
                oConnection.Close();

            return result;
        }

        public override List<ViewAnnouncementText> GetViewAnnouncementTextAllEmployee()
        {
            var result = new List<ViewAnnouncementText>();
            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            SqlCommand oCommand = new SqlCommand();
            SqlDataAdapter oAdapter = new SqlDataAdapter();
            DataTable oTable = new DataTable();

            oCommand = new SqlCommand("sp_AnnouncementTextGetAllByEmployee", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.Add("@p_datetime", SqlDbType.DateTime).Value = DateTime.Now.Date;

            oConnection.Open();
            oAdapter = new SqlDataAdapter(oCommand);
            oTable = new DataTable("AnnouncementText");
            oAdapter.Fill(oTable);

            if (oTable.Rows.Count > 0)
            {
                foreach (DataRow dr in oTable.Rows)
                {
                    ViewAnnouncementText data = new ViewAnnouncementText();
                    data.ParseToObject(dr);
                    result.Add(data);
                }
            }

            oCommand.Dispose();
            oAdapter.Dispose();
            oTable.Dispose();
            if (oConnection.State == ConnectionState.Open)
                oConnection.Close();

            return result;
        }
        #endregion
    }
}