using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.TM.CONFIG
{
    public class AbsenceType : AbstractObject
    {
        private string __absAttGrouping = "";
        private string __key = "";
        private string __description = "";
        private string __quotaDayCount = "";
        private string __countingRule = "";
        private bool __useQuota;
        private int __monitorType;
        public string CompanyCode { get; set; }
        public bool IsChecked { get; set; }
        public AbsenceType()
        {
        }
        public string AbsAttGrouping
        {
            get
            {
                return __absAttGrouping;
            }
            set
            {
                __absAttGrouping = value;
            }
        }
        public string Key
        {
            get
            {
                return __key;
            }
            set
            {
                __key = value;
            }
        }
        public string Description
        {
            get
            {
                return __description;
            }
            set
            {
                __description = value;
            }
        }
        public string QuotaDayCount
        {
            get
            {
                return __quotaDayCount;
            }
            set
            {
                __quotaDayCount = value;
            }
        }
        public string CountingRule
        {
            get
            {
                return __countingRule;
            }
            set
            {
                __countingRule = value;
            }
        }
        public bool IsUseQuota
        {
            get
            {
                return __useQuota;
            }
            set
            {
                __useQuota = value;
            }
        }

        public int MonitorType
        {
            get
            {
                return __monitorType;
            }
            set
            {
                __monitorType = value;
            }
        }

        protected override void LoadObjectToDataRow(PropertyInfo prop, System.Data.DataRow oNewRow, System.Data.DataColumn oDC)
        {
            if (prop.Name == "Key")
            {
                oNewRow["AbsenceKey"] = this.Key;
            }
            base.LoadObjectToDataRow(prop, oNewRow, oDC);
        }

        protected override void SetDataRowToObject(PropertyInfo prop, System.Data.DataRow dr)
        {
            if (prop.Name == "Key")
            {
                this.Key = (string)dr["AbsenceKey"];
            }
            base.SetDataRowToObject(prop, dr);
        }

        public override int GetHashCode()
        {
            string cCode = string.Format("ABSENCETYPE_{0}", Key);
            return cCode.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return this.GetHashCode() == obj.GetHashCode();
        }

        public  List<AbsenceType> GetAllAbsenceType()
        {
            //return ESS.HR.TM.ServiceManager.HRTMConfig.GetAllAbsenceTypeList();
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAllAbsenceTypeList();
        }

        public List<AbsenceType> GetAbsenceTypeList(string EmployeeID, string LanguageCode)
        {
            //return ESS.HR.TM.ServiceManager.HRTMConfig.GetAbsenceTypeList(EmployeeID, LanguageCode);
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAbsenceTypeList(EmployeeID, LanguageCode);
        }

        public  void SaveTo(List<AbsenceType> Data, string Mode)
        {
            //ESS.HR.TM.ServiceManager.GetMirrorConfig(Mode).SaveAbsenceTypeList(Data);
            ServiceManager.CreateInstance(CompanyCode).ESSConfig.SaveAbsenceTypeList(Data);
        }

        public  AbsenceType GetAbsenceType(string EmployeeID, string AbsenceType, string LanguageCode)
        {
            //return ESS.HR.TM.ServiceManager.HRTMConfig.GetAbsenceType(EmployeeID, AbsenceType, LanguageCode);
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAbsenceType(EmployeeID, AbsenceType, LanguageCode);
        }

    }
}
