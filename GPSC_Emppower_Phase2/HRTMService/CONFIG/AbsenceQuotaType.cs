using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.HR.TM.CONFIG
{
    public class AbsenceQuotaType
    {
        private string __subGroupSetting = "";
        private string __subAreaSetting = "";
        private string __key = "";
        private string __description = "";
        private bool __isShowDashboard = false;
        private string __defaultAbsenceType = "";

        public string CompanyCode { get; set; }
        public AbsenceQuotaType()
        { 
        }
        public string SubGroupSetting
        {
            get
            {
                return __subGroupSetting;
            }
            set 
            {
                __subGroupSetting = value;
            }
        }
        public string SubAreaSetting
        {
            get
            {
                return __subAreaSetting;
            }
            set
            {
                __subAreaSetting = value;
            }
        }
        public string Key
        {
            get
            {
                return __key;
            }
            set
            {
                __key = value;
            }
        }
        public string Description
        {
            get
            {
                return __description;
            }
            set
            {
                __description = value;
            }
        }
        public bool IsShowDashboard
        {
            get
            {
                return __isShowDashboard;
            }
            set
            {
                __isShowDashboard = value;
            }
        }
        public string DefaultAbsenceType
        {
            get
            {
                return __defaultAbsenceType;
            }
            set
            {
                __defaultAbsenceType = value;
            }
        }

        public override int GetHashCode()
        {
            string cCode = string.Format("ABSENCEQUOTATYPE_{0}", Key);
            return cCode.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return this.GetHashCode() == obj.GetHashCode();
        }

        public  List<AbsenceQuotaType> GetAllAbsenceQuotaType()
        {
            //return ESS.HR.TM.ServiceManager.HRTMConfig.GetAbsenceQuotaTypeList("");
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAbsenceQuotaTypeList("");

        }

        public  List<AbsenceQuotaType> GetAllAbsenceQuotaType(string LanguageCode)
        {
            //return ESS.HR.TM.ServiceManager.HRTMConfig.GetAbsenceQuotaTypeList(LanguageCode);
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAbsenceQuotaTypeList(LanguageCode);
        }

        public  void SaveTo(List<AbsenceQuotaType> Data, string Mode)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSConfig.SaveAbsenceQuotaTypeList(Data);
            //ESS.HR.TM.ServiceManager.GetMirrorConfig(Mode).SaveAbsenceQuotaTypeList(Data);
        }

        public static AbsenceQuotaType GetAbsenceQuotaType(string EmployeeID, string QuotaTypeCode, string LanguageCode)
        {
            return GetAbsenceQuotaType(EmployeeID, QuotaTypeCode, LanguageCode);
        }
    }
}
