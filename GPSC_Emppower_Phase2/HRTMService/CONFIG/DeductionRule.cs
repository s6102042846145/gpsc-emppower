using System;
using System.Collections.Generic;
using System.Text;
using ESS.EMPLOYEE;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.TM.CONFIG
{
    public class DeductionRule : AbstractObject
    {
        private string __subGroupSetting = "";
        private string __subAreaSetting = "";
        private string __deductionRule = "";
        private string __quota1 = "";
        private string __quota2 = "";
        private string __quota3 = "";
        public string CompanyCode { get; set; }
        public DeductionRule()
        {
 
        }
        public string SubGroupSetting
        {
            get
            {
                return __subGroupSetting;
            }
            set
            {
                __subGroupSetting = value;
            }
        }
        public string SubAreaSetting
        {
            get
            {
                return __subAreaSetting;
            }
            set
            {
                __subAreaSetting = value;
            }
        }
        public string DeductionCode
        {
            get
            {
                return __deductionRule;
            }
            set
            {
                __deductionRule = value;
            }
        }
        public string Quota1
        {
            get
            {
                return __quota1;
            }
            set
            {
                __quota1 = value;
            }
        }
        public string Quota2
        {
            get
            {
                return __quota2;
            }
            set
            {
                __quota2 = value;
            }
        }
        public string Quota3
        {
            get
            {
                return __quota3;
            }
            set
            {
                __quota3 = value;
            }
        }

        public override int GetHashCode()
        {
            string cCode = string.Format("DEDUCTIONRULE_{0}_{1}_{2}", SubGroupSetting, SubAreaSetting, DeductionCode);
            return cCode.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return this.GetHashCode() == obj.GetHashCode();
        }

        public static List<DeductionRule> GetAllDeductionRule()
        {
            return GetAllDeductionRule();
        }

        public  void SaveTo(List<DeductionRule> Data, string Mode)
        {
           ServiceManager.CreateInstance(CompanyCode).ESSConfig.SaveDeductionRule(Data);
        }

        public  DeductionRule GetDeductionRule(EmployeeData employeeData, string AbsenceType)
        {
            //return HR.TM.ServiceManager.HRTMConfig.GetDeductionRule(employeeData.OrgAssignment.SubGroupSetting.TimeQuotaTypeGrouping, employeeData.OrgAssignment.SubAreaSetting.TimeQuotaGrouping, employeeData.OrgAssignment.SubAreaSetting.AbsAttGrouping, AbsenceType);
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetDeductionRule(employeeData.OrgAssignment.SubGroupSetting.TimeQuotaTypeGrouping, employeeData.OrgAssignment.SubAreaSetting.TimeQuotaGrouping, employeeData.OrgAssignment.SubAreaSetting.AbsAttGrouping, AbsenceType);
        }
    }
}
