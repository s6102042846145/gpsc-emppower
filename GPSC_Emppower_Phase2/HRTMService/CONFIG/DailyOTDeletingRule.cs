using System;
using System.Collections.Generic;
using System.Text;
using ESS.HR.TM;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.TM.CONFIG
{
    public class DailyOTDeletingRule : AbstractObject
    {
        private string __offsetFlag = "";
        private int __offsetValue = -1;
        private int __dayCutoff = -1;
        public string CompanyCode { get; set; }
        public DailyOTDeletingRule()
        { 
        }

        public string OffsetFlag
        {
            get
            {
                return __offsetFlag;
            }
            set
            {
                __offsetFlag = value;
            }
        }

        public int OffsetValue
        {
            get
            {
                return __offsetValue;
            }
            set
            {
                __offsetValue = value;
            }
        }

        public int DayCutoff
        {
            get
            {
                return __dayCutoff;
            }
            set
            {
                __dayCutoff = value;
            }
        }

        public  DailyOTDeletingRule GetDeletingRule()
        {
            //return HR.TM.ServiceManager.HRTMExtraConfig.GetDailyOTDeletingRule();
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetDailyOTDeletingRule();
        }

        private static void CheckData(string v_employeeID, DateTime v_now, DateTime v_checkDate, int v_offsetCutoff, string v_offsetFlag, int v_offsetValue, bool v_countOnDayOff)
        {
            if (v_offsetFlag == "P")
            {
                DateTime oMonthNow;
                oMonthNow = v_now.Date.AddMonths(v_offsetValue);
                if (oMonthNow > v_checkDate)
                {
                    throw new Exception("CONFLICT_RULE");
                }
            }
            else
            {
                throw new Exception("CONFIG_ERROR");
            }
        }

        public void IsCanDelete(string EmployeeID, DateTime OTDate, DateTime DocumentDate)
        {
            DateTime oNow = DocumentDate.Date;
            DateTime oCheckDate = OTDate;
            try
            {
                CheckData(EmployeeID, oNow, oCheckDate, DayCutoff, OffsetFlag, OffsetValue, false);
            }
            catch (Exception)
            {
                throw new DeleteDailyOTException(this);
            }
        }
    }
}
