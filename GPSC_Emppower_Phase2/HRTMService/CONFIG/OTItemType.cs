using System;
using System.Collections.Generic;
using System.Text;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.TM.CONFIG
{
    public class OTItemType : AbstractObject
    {
        private int __otItemTypeID = -1;
        private string __otItemTypeCode = "";
        public string CompanyCode { get; set; }
        public OTItemType()
        { 
        }

        public int OTItemTypeID
        {
            get
            {
                return __otItemTypeID;
            }
            set
            {
                __otItemTypeID = value;
            }
        }

        public string OTItemTypeCode
        {
            get
            {
                return __otItemTypeCode;
            }
            set
            {
                __otItemTypeCode = value;
            }
        }

        public List<OTItemType> GetAllOTItemType()
        {
            //return HR.TM.ServiceManager.HRTMExtraConfig.GetAllOTItemType();
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAllOTItemType();
        }

        public OTItemType GetOTItemType(int OTItemTypeID)
        {
            //return HR.TM.ServiceManager.HRTMExtraConfig.GetOTItemType(OTItemTypeID);
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetOTItemType(OTItemTypeID);
        }

    }
}
