using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Text;
using ESS.UTILITY.EXTENSION;
using ESS.EMPLOYEE;
using ESS.HR.TM;

namespace ESS.HR.TM.CONFIG
{
    public class AbsenceCancelRule : AbstractObject
    {
        private string __absAttGrouping = "";
        private string __absenceKey = "";
        private string __offsetFlag = "";
        private int __offsetValue = -1;
        private int __dayCutoff = -1;
        public string CompanyCode { get; set; }
        public AbsenceCancelRule()
        { 
        }

        protected override void LoadObjectToDataRow(PropertyInfo prop, DataRow oNewRow, DataColumn oDC)
        {
            if (prop.Name == "AbsenceType")
            {
                oNewRow["AbsenceKey"] = this.AbsenceType;
                return;
            }
            base.LoadObjectToDataRow(prop, oNewRow, oDC);
        }

        protected override void SetDataRowToObject(PropertyInfo prop, DataRow dr)
        {
            if (prop.Name == "AbsenceType")
            {
                this.AbsenceType = (string)dr["AbsenceKey"];
                return;
            }
            base.SetDataRowToObject(prop, dr);
        }

        public string AbsAttGrouping
        {
            get
            {
                return __absAttGrouping;
            }
            set
            {
                __absAttGrouping = value;
            }
        }

        public string AbsenceType
        {
            get
            {
                return __absenceKey;
            }
            set
            {
                __absenceKey = value;
            }
        }

        public string OffsetFlag
        {
            get
            {
                return __offsetFlag;
            }
            set
            {
                __offsetFlag = value;
            }
        }

        public int OffsetValue
        {
            get
            {
                return __offsetValue;
            }
            set
            {
                __offsetValue = value;
            }
        }

        public int DayCutoff
        {
            get
            {
                return __dayCutoff;
            }
            set
            {
                __dayCutoff = value;
            }
        }

        //public  AbsenceCancelRule GetCancelRule(string EmployeeID, string AbsenceTypeCode)
        //{
        //    //return HR.TM.ServiceManager.HRTMExtraConfig.GetAbsenceCancelRule(EmployeeID, AbsenceTypeCode);
        //    return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAbsenceCancelRule(EmployeeID, AbsenceTypeCode);

        //}

        private static void CheckData(string v_employeeID, DateTime v_now, DateTime v_checkDate, int v_offsetCutoff, string v_offsetFlag, int v_offsetValue, bool v_countOnDayOff)
        {
            if (v_offsetFlag == "P")
            {
                DateTime oMonthNow;
                oMonthNow = v_now.Date.AddMonths(v_offsetValue);
                if (oMonthNow > v_checkDate)
                {
                    throw new Exception("CONFLICT_RULE");
                }
            }
            else
            {
                throw new Exception("CONFIG_ERROR");
            }
        }

        public void IsCanCancel(string EmployeeID, DateTime BeginDate, DateTime EndDate, DateTime DocumentDate)
        {
            DateTime oNow = DocumentDate.Date;
            DateTime oCheckDate = this.OffsetValue > 0 ? BeginDate.Date : EndDate.Date;
            try
            {
                CheckData(EmployeeID, oNow, oCheckDate, DayCutoff, OffsetFlag, OffsetValue, false);
            }
            catch (Exception)
            {
                throw new AbsenceCancelException(this);
            }
        }
    }
}
