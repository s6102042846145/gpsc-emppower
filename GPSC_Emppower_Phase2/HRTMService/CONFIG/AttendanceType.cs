using System;
using System.Collections.Generic;
using System.Text;
using ESS.EMPLOYEE;

namespace ESS.HR.TM.CONFIG
{
    public class AttendanceType
    {
        private string __absAttGrouping = "";
        private string __key = "";
        private string __description = "";
        private string __flowSetting = "";
        public string CompanyCode { get; set; }
        public enum Type
        {
            Working_Local = 2010,
            Working_Abroad = 2015,
            Seminar_Local = 2020,
            Seminar_Abroad = 2025,
            Observation_Local = 2030,
            Observation_Abroad = 2035,
            WorkingLate = 2040,
            StopWorkingEarly = 2050,
            ConfirmComeToWork = 2060
        }

        public AttendanceType()
        {
        }
        public string AbsAttGrouping
        {
            get
            {
                return __absAttGrouping;
            }
            set
            {
                __absAttGrouping = value;
            }
        }
        public string Key
        {
            get
            {
                return __key;
            }
            set
            {
                __key = value;
            }
        }
        public string Description
        {
            get
            {
                return __description;
            }
            set
            {
                __description = value;
            }
        }

        public string FlowSetting
        {
            get
            {
                return __flowSetting;
            }
            set
            {
                __flowSetting = value;
            }
        }

        public override int GetHashCode()
        {
            string cCode = string.Format("ATTEDANCETYPE_{0}", Key);
            return cCode.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return this.GetHashCode() == obj.GetHashCode();
        }

        public List<AttendanceType> GetAllAttendanceType()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAllAttendanceTypeList();
        }

        public List<AttendanceType> GetAttendanceTypeList(string EmployeeID, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAttendanceTypeList(EmployeeID, LanguageCode);
        }

        public void SaveTo(List<AttendanceType> Data, string Mode)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSConfig.SaveAttendanceTypeList(Data);
        }

        public  AttendanceType GetAttendanceType(string EmployeeID, string AttendanceType, string LanguageCode)
        {
            //return ESS.HR.TM.ServiceManager.HRTMConfig.GetAttendanceType(EmployeeID, AttendanceType, LanguageCode);
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAttendanceType(EmployeeID, AttendanceType, LanguageCode);
        }

        public  string GetAttendanceFlowSetting(string EmployeeID, string AttendanceTypeCode)
        {
            EmployeeData oEmp = new EmployeeData(EmployeeID);
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAttendanceFlowSetting(oEmp.OrgAssignment.SubAreaSetting.AbsAttGrouping, AttendanceTypeCode);
            //return HR.TM.ServiceManager.HRTMConfig.GetAttendanceFlowSetting(oEmp.OrgAssignment.SubAreaSetting.AbsAttGrouping, AttendanceTypeCode);
        }
    }
}
