﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.TM.CONFIG
{
    public class AreaWorkschedule : AbstractObject
    {
        private int __areaWorkscheduleID;
        private string __area = "";
        private string __subArea = "";
        private string __textCode = "";
        private string __wfRule = "";
        private DateTime __begindate = DateTime.MinValue;
        private DateTime __enddate = DateTime.MinValue;
        private string _textname = "";


        public int AreaWorkscheduleID
        {
            get
            {
                return __areaWorkscheduleID;
            }
            set
            {
                __areaWorkscheduleID = value;
            }
        }
        public string Area
        {
            get
            {
                return __area;
            }
            set
            {
                __area = value;
            }
        }
        public string SubArea
        {
            get
            {
                return __subArea;
            }
            set
            {
                __subArea = value;
            }
        }
        public string TextCode
        {
            get
            {
                return __textCode;
            }
            set
            {
                __textCode = value;
            }
        }
        public string WFRule
        {
            get
            {
                return __wfRule;
            }
            set
            {
                __wfRule = value;
            }
        }
        public DateTime BeginDate
        {
            get
            {
                return __begindate;
            }
            set
            {
                __begindate = value;
            }
        }
        public DateTime EndDate
        {
            get
            {
                return __enddate;
            }
            set
            {
                __enddate = value;
            }
        }

        public string TextName
        {
            get
            {
                return _textname;
            }
            set
            {
                _textname = value;
            }
        }
    }
}
