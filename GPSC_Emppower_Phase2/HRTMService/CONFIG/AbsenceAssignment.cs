using System;
using System.Collections.Generic;
using System.Text;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.TM.CONFIG
{
    public class AbsenceAssignment : AbstractObject
    {
        private string __itemAssign = "";
        private int __value1 = -1;
        private int __value2 = -1;
        public string CompanyCode { get; set; }

        public AbsenceAssignment()
        { 
        }

        public string ItemAssign
        {
            get
            {
                return __itemAssign;
            }
            set
            {
                __itemAssign = value;
            }
        }

        public int Value1
        {
            get
            {
                return __value1;
            }
            set
            {
                __value1 = value;
            }
        }

        public int Value2
        {
            get
            {
                return __value2;
            }
            set
            {
                __value2 = value;
            }
        }

        //public List<AbsenceAssignment> GetAbsenceAssignment(string AbsAttGrouping, string AbsenceType)
        //{
        //    return GetAbsenceAssignment(AbsAttGrouping,AbsenceType,"*");

        //}
        //public  List<AbsenceAssignment> GetAbsenceAssignment(string AbsAttGrouping, string AbsenceType, string SubKey)
        //{
        //    //return HR.TM.ServiceManager.HRTMExtraConfig.GetAbsenceAssignmentList(AbsAttGrouping, AbsenceType, SubKey);
        //    return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAbsenceAssignmentList(AbsAttGrouping, AbsenceType, SubKey);
        //}
    }
}
