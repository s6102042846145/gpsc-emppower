using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using ESS.HR.TM;
using ESS.TIMESHEET;
using ESS.WORKFLOW;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.TM.CONFIG
{
    public class SubstituteCreatingRule : AbstractObject
    {
        private int __offsetCutoff = 10;

        private int __offsetValue = -1;
        private string __offsetFlag = "D";
        private int __managerOffsetValue = -1;
        private string __managerOffsetFlag = "D";
        private int __timeAdminOffsetValue = -1;
        private string __timeAdminOffsetFlag = "D";
        public string CompanyCode { get; set; }
        public SubstituteCreatingRule()
        { 
        }

        public int OffsetCutoff
        {
            get
            {
                return __offsetCutoff;
            }
            set
            {
                __offsetCutoff = value;
            }
        }

        public int OffsetValue
        {
            get
            {
                return __offsetValue;
            }
            set
            {
                __offsetValue = value;
            }
        }

        public string OffsetFlag
        {
            get
            {
                return __offsetFlag;
            }
            set
            {
                __offsetFlag = value;
            }
        }

        public int ManagerOffsetValue
        {
            get
            {
                return __managerOffsetValue;
            }
            set
            {
                __managerOffsetValue = value;
            }
        }

        public string ManagerOffsetFlag
        {
            get
            {
                return __managerOffsetFlag;
            }
            set
            {
                __managerOffsetFlag = value;
            }
        }

        public int TimeAdminOffsetValue
        {
            get
            {
                return __timeAdminOffsetValue;
            }
            set
            {
                __timeAdminOffsetValue = value;
            }
        }

        public string TimeAdminOffsetFlag
        {
            get
            {
                return __timeAdminOffsetFlag;
            }
            set
            {
                __timeAdminOffsetFlag = value;
            }
        }

        public  SubstituteCreatingRule GetCreatingRule()
        {
            //return TM.ServiceManager.HRTMExtraConfig.GetSubstituteCreatingRule();
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetSubstituteCreatingRule();
        }
    }
}
