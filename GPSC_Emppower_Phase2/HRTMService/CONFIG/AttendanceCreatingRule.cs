using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using ESS.UTILITY.EXTENSION;
using ESS.HR.TM;
using ESS.TIMESHEET;
using ESS.WORKFLOW;
using ESS.EMPLOYEE.CONFIG;
using ESS.EMPLOYEE;
using System.Globalization;
using ESS.EMPLOYEE.CONFIG.TM;

namespace ESS.HR.TM.CONFIG
{
    public class AttendanceCreatingRule : AbstractObject
    {
        private string __absAttGrouping = "";
        private string __attendanceKey = "";
        private string __offsetFlag = "";
        private int __offsetValue = -1;
        private int __dayCutoff = -1;
        private bool __canCreateInAdvance = true;
        private string __advanceFlag = "";
        private int __advanceValue = -1;
        private int __advanceCutoff = -1;
        private string __clockFlag = "";
        private int __hourDiffMin = -1;
        private int __hourDiffMax = -1;
        private string __employeeID = "";
        private DateTime __begindate = DateTime.MinValue;
        private DateTime __enddate = DateTime.MinValue;
        private List<TimeElement> list = null;
        private List<TimePair> pair = null;
        private byte __AttendanceInterval;
        private int __managerOffsetValue = -1;
        private string __managerOffsetFlag = "";
        private int __timeAdminOffsetValue = -1;
        private string __timeAdminOffsetFlag = "";
        public string CompanyCode { get; set; }
        //AddBy: Ratchatawan W. (2012-02-21)
        public string EmployeeID
        {
            get
            {
                return __employeeID;
            }
            set
            {
                __employeeID = value;
            }
        }
        public DateTime BeginDate
        {
            get
            {
                return __begindate;
            }
            set
            {
                __begindate = value;
            }
        }
        //AddBy: Ratchatawan W. (2012-02-21)
        public DateTime EndDate
        {
            get
            {
                return __enddate;
            }
            set
            {
                __enddate = value;
            }
        }

        public AttendanceCreatingRule()
        { 
        }

        public string AbsAttGrouping
        {
            get
            {
                return __absAttGrouping;
            }
            set
            {
                __absAttGrouping = value;
            }
        }

        public string AttendanceKey
        {
            get
            {
                return __attendanceKey;
            }
            set
            {
                __attendanceKey = value;
            }
        }

        public string OffsetFlag
        {
            get
            {
                return __offsetFlag;
            }
            set
            {
                __offsetFlag = value;
            }
        }

        public int OffsetValue
        {
            get
            {
                return __offsetValue;
            }
            set
            {
                __offsetValue = value;
            }
        }

        public int DayCutoff
        {
            get
            {
                return __dayCutoff;
            }
            set
            {
                __dayCutoff = value;
            }
        }

        public bool CanCreateInAdvance
        {
            get
            {
                return __canCreateInAdvance;
            }
            set
            {
                __canCreateInAdvance = value;
            }
        }

        public string AdvanceFlag
        {
            get
            {
                return __advanceFlag;
            }
            set
            {
                __advanceFlag = value;
            }
        }

        public int AdvanceValue
        {
            get
            {
                return __advanceValue;
            }
            set
            {
                __advanceValue = value;
            }
        }

        public int AdvanceCutoff
        {
            get
            {
                return __advanceCutoff;
            }
            set
            {
                __advanceCutoff = value;
            }
        }

        /// <summary>
        /// Represent how to load clock for this attendance, 
        /// I - only IN, 
        /// O - only OUT, 
        /// A - All (IN and OUT), 
        /// D - only OUT and IN = PLAN, 
        /// S - try to Swap IN-OUT,
        /// N - No clock load
        /// </summary>
        public string ClockFlag
        {
            get
            {
                return __clockFlag;
            }
            set
            {
                __clockFlag = value;
            }
        }

        public int HourDiffMin
        {
            get
            {
                return __hourDiffMin;
            }
            set
            {
                __hourDiffMin = value;
            }
        }

        public int HourDiffMax
        {
            get
            {
                return __hourDiffMax;
            }
            set
            {
                __hourDiffMax = value;
            }
        }

        public int ManagerOffsetValue
        {
            get
            {
                return __managerOffsetValue;
            }
            set
            {
                __managerOffsetValue = value;
            }
        }

        public string ManagerOffsetFlag
        {
            get
            {
                return __managerOffsetFlag;
            }
            set
            {
                __managerOffsetFlag = value;
            }
        }

        public int TimeAdminOffsetValue
        {
            get
            {
                return __timeAdminOffsetValue;
            }
            set
            {
                __timeAdminOffsetValue = value;
            }
        }

        public string TimeAdminOffsetFlag
        {
            get
            {
                return __timeAdminOffsetFlag;
            }
            set
            {
                __timeAdminOffsetFlag = value;
            }
        }

        public AttendanceCreatingRule GetCreatingRule(string EmployeeID, string AttendanceTypeCode,DateTime BeginDate, DateTime EndDate)
        {
            AttendanceCreatingRule ACR = ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAttendanceCreatinigRule(EmployeeID, AttendanceTypeCode);
            ACR.__employeeID = EmployeeID;
            ACR.__begindate = BeginDate;
            ACR.__enddate = EndDate;
            return ACR;
        }

        public AttendanceCreatingRule GetCreatingRule(EmployeeData oEmp, string AttendanceTypeCode, DateTime BeginDate, DateTime EndDate)
        {
            AttendanceCreatingRule ACR = ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAttendanceCreatinigRule(oEmp, AttendanceTypeCode);
            ACR.__employeeID = EmployeeID;
            ACR.__begindate = BeginDate;
            ACR.__enddate = EndDate;
            return ACR;
        }

        public bool IsLoadClockIN
        {
            get
            {
                return this.ClockFlag.Trim() == "I";
            }
        }

        public bool IsLoadClockOUT
        {
            get
            {
                return this.ClockFlag.Trim() == "O";
            }
        }

        public bool IsSwapClock
        {
            get
            {
                return this.ClockFlag.Trim() == "S";
            }
        }

        public bool IsTryAutoPair
        {
            get
            {
                return this.ClockFlag.Trim() == "A" || this.ClockFlag.Trim() == "B";
            }
        }
        public Byte AttendanceInterval
        {
            get
            {
                return __AttendanceInterval;
            }
            set
            {
                __AttendanceInterval = value;
            }
        }

        private List<TimeElement> loadPossible()
        {
            List<TimeElement> list = new List<TimeElement>();
            list.AddRange(loadPossible(DoorType.IN));
            list.AddRange(loadPossible(DoorType.OUT));
            return list;
        }
        private List<TimeElement> loadPossible(DoorType type)
        {
            if (list == null)
            {
                LoadArchiveTimeElementRelative();
            }
            List<TimeElement> oReturn = new List<TimeElement>();
            foreach (TimeElement item in list)
            {
                if (item.DoorType == DoorType.IN && (item.EventTime.Date < this.__begindate || item.EventTime.Date > this.__enddate))
                {
                    continue;
                }
                else if (item.DoorType == DoorType.OUT && item.EventTime.Date < this.__begindate)
                {
                    continue;
                }
                if (item.DoorType == type)
                {
                    oReturn.Add(item);
                }
            }
            return oReturn;
        }

        private List<TimePair> trySwap()
        {
            List<TimePair> oReturn = new List<TimePair>();
            if (list == null)
            {
                LoadArchiveTimeElementRelative();
            }
            foreach (TimeElement item in list)
            {
                bool isSwaped = false;
                if (item.DoorType == DoorType.OUT)
                {
                    isSwaped = true;
                }
                foreach (TimeElement item1 in list)
                {
                    if (item1 <= item)
                        continue;
                    if (isSwaped || item1.DoorType == DoorType.IN)
                    {
                        TimeSpan oTS = item1.EventTime.Subtract(item.EventTime);
                        if (oTS.TotalHours >= this.HourDiffMin && oTS.TotalHours < this.HourDiffMax)
                        {
                            oReturn.Add(new TimePair(item, item1));
                        }
                    }
                }
            }
            return oReturn;
        }

        private List<TimePair> tryAutoPair()
        {
            List<TimePair> oReturn = new List<TimePair>();
            if (pair == null)
            {
                LoadArchiveTimepair();
            }
            for (int index1 = 0; index1 < pair.Count; index1++)
            {
                //if (pair[index1].ClockIN != null && pair.Count > 1)
                  if (!(pair[index1].ClockIN is null) && pair.Count > 1)
                    {
                    for (int index = index1 + 1; index < pair.Count; index++)
                    {
                        TimePair pairItem = pair[index];
                        //if (pairItem.ClockOUT != null))
                        if (!(pairItem.ClockOUT is null))
                        {
                            TimeSpan oSpan = pairItem.ClockOUT.EventTime.Subtract(pair[index1].ClockIN.EventTime);
                            if (oSpan.TotalHours >= this.HourDiffMin && oSpan.TotalHours <= this.HourDiffMax)
                            {
                                TimePair newPair = new TimePair();
                                newPair.ClockIN = pair[index1].ClockIN;
                                newPair.ClockOUT = pairItem.ClockOUT;
                                newPair.Date = pair[index1].Date;
                                newPair.DWSCode = pair[index1].DWSCode;
                                newPair.EmployeeID = pair[index1].EmployeeID;
                                newPair.IsDayOff = pair[index1].IsDayOff;
                                newPair.IsHoliday = pair[index1].IsHoliday;
                                oReturn.Add(newPair);
                                break;
                            }
                        }
                    }
                }
            }

            return oReturn;
        }
        
        public List<TimeElement> PossibleIN
        {
            get
            {
                if (!this.IsLoadClockIN)
                    return new List<TimeElement>();
                return loadPossible();
            }
        }

        public List<TimeElement> PossibleOUT
        {
            get
            {
                if (!this.IsLoadClockOUT)
                    return new List<TimeElement>();
                return loadPossible();
            }
        }

        public List<TimePair> PossibleSWAP
        {
            get
            {
                if (!this.IsSwapClock)
                    return new List<TimePair>();
                return trySwap();
            }
        }

        public List<TimePair> PossibleAutoPair
        {
            get
            {
                if (!this.IsTryAutoPair)
                    return new List<TimePair>();
                return tryAutoPair();
            }
        }

        public List<TimeElement> LoadArchiveTimeElementRelative()
        {
            list = HRTMManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).LoadArchiveTimeElementRelative(this.__employeeID, this.__begindate, this.__enddate);
            list.Sort(new TimeElementSortingTime(true));
            return list;
        }

        public List<TimePair> LoadArchiveTimepair()
        {
            pair = HRTMManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).LoadArchiveTimepair(this.__employeeID, this.__begindate, this.__enddate);
            pair.Sort(new TimePairSortingDate());
            if (pair.Count == 0)
            {
                TimePair item = new TimePair();
                item.EmployeeID = this.__employeeID;
                pair.Add(item);
            }
            TimesheetManagement.FillDWS(this.__employeeID, pair, true, false, this.__begindate, this.__enddate, false);
            return pair;
        }

        public void IsCanCreate()
        {
            //if (WorkflowPrinciple.Current.IsInRole("TIMEADMIN") || WorkflowPrinciple.Current.IsInRole("#MANAGER"))
            //{
            //    return;
            //}
            DateTime oNow = DateTime.Now.Date;
            if (this.__enddate.Date > oNow)
            {
                // future record
                #region " FUTURE RECORD "
                if (!this.CanCreateInAdvance)
                {
                    //ModifiedBy: Ratchatawan W. (2012-03-21)
                    throw new AttendanceCreatingException(this, "ATTENDANCE_ADVANCE_CANNOT", false);
                    //throw new Exception("CAN_NOT_CREATE_FUTURE_DATE");
                }
                else
                {
                    switch (this.AdvanceFlag.Trim())
                    {
                        case "D":
                            if (this.__enddate.Date >= oNow.AddDays(this.AdvanceValue))
                            {
                                throw new AttendanceCreatingException(this,"ATTENDANCE_ADVANCE_MORETHAN_LIMIT_D",false);
                            }
                            break;
                        case "M":
                        default:
                            // use 'M' as default
                            if (this.__enddate.Date >= oNow.AddMonths(this.AdvanceValue))
                            {
                                throw new AttendanceCreatingException(this, "ATTENDANCE_ADVANCE_MORETHAN_LIMIT_M", false);
                            }
                            break;
                    }
                }
                #endregion
            }
            else
            {
                if(WorkflowPrinciple.Current.IsInRole("#MANAGER") && this.ManagerOffsetValue > 0)
                {
                    this.OffsetValue = this.ManagerOffsetValue;
                }
                else if(WorkflowPrinciple.Current.IsInRole("TIMEADMIN") && this.TimeAdminOffsetValue > 0)
                {
                    this.OffsetValue = this.TimeAdminOffsetValue;
                }
                #region " PAST RECORD "
                //AddBy: Ratchatawan W. (2012-03-21)
                if (this.OffsetValue < 0)
                    throw new AttendanceCreatingException(this, "ATTENDANCE_PAST_CANNOT", true);
                else
                {

                    switch (this.OffsetFlag.Trim())
                    {
                        case "D":
                            if (this.__begindate.Date < oNow.AddDays(0 - this.OffsetValue))
                            {
                                if (this.OffsetValue == 0)
                                    throw new AttendanceCreatingException(this, "ATTENDANCE_PAST_CANNOT", true);
                                else
                                    //ModifiedBy: Ratchatawan W. (2012-03-21)
                                    throw new AttendanceCreatingException(this, "ATTENDANCE_PAST_MORETHAN_LIMIT_D", true);
                                    //throw new Exception("CAN_NOT_CREATE_BEFORE_RESTROSPECTIVE");
                            }
                            break;
                        case "M":
                        default:
                            // use 'M' as default
                            DateTime oFirstDay = oNow.AddDays(1 - oNow.Day);
                            DateTime oOffsetMonth = oFirstDay.AddDays(this.DayCutoff);
                            int nOffsetMonth = 1;
                            if (oNow < oOffsetMonth)
                            {
                                nOffsetMonth = 0;
                            }
                            if (this.__begindate.Date < oFirstDay.AddMonths(nOffsetMonth - this.OffsetValue))
                            {
                                //ModifiedBy: Ratchatawan W. (2012-03-21)
                                throw new AttendanceCreatingException(this, "ATTENDANCE_PAST_MORETHAN_LIMIT_M", true);
                                //throw new Exception("CAN_NOT_CREATE_BEFORE_RESTROSPECTIVE");
                            }
                            break;
                    }
                }
                #endregion
            }

        }

        //AddBy: Ratchatawan W. (2012-03-21)
        //We have to validate begindate(input) that it is morethan OffsetValue or not
        //If flag "CountOnDayoff" in creatingrule is 
        //False: It means we have to count totaldays previous or next ignoring dayoff
        //True : It means we have to count totaldays previous or next including dayoff
        private bool ValidateOnCountDayOff(DateTime CheckedDate,int LimitNumber,bool IsPast)
        {
            
            CultureInfo oCL = new CultureInfo("en-US");
            DateTime CurrentDate = DateTime.Now;
            EmployeeData emp = new EmployeeData(this.__employeeID, CurrentDate);
            DateTime LimitDate = DateTime.Now;
            int iLimitCount = 0;
            MonthlyWS MWS = MonthlyWS.GetCalendar(this.__employeeID, CurrentDate.Year, CurrentDate.Month);

            if (IsPast)
            {
                for (DateTime iDate = CurrentDate; iDate >= CheckedDate; iDate = iDate.AddDays(-1))
                {
                    if (MWS.WS_Year != iDate.Year || MWS.WS_Month != iDate.Month)
                        MWS = MonthlyWS.GetCalendar(this.__employeeID, iDate.Year, iDate.Month);
                    if (!emp.GetDailyWorkSchedule(iDate).IsDayOff && MWS.GetWSCode(iDate.Day, false) != "1")
                        iLimitCount++;
                }
            }
            else
            {
                for (DateTime iDate = CurrentDate; iDate <= CheckedDate; iDate = iDate.AddDays(1))
                {
                    if (MWS.WS_Year != iDate.Year || MWS.WS_Month != iDate.Month)
                        MWS = MonthlyWS.GetCalendar(this.__employeeID, iDate.Year, iDate.Month);
                    if (!emp.GetDailyWorkSchedule(iDate).IsDayOff && MWS.GetWSCode(iDate.Day, false) != "1")
                        iLimitCount++;
                }
            }
            if (DateTime.Now <= CheckedDate)
                return true;

            if (iLimitCount > LimitNumber)
                return false;
            return true;
        }

        public bool CheckClock(DoorType type,DateTime checkDate)
        {
            bool lFound = false;
            foreach (TimeElement item in loadPossible(type))
            {
                if (item.EventTime.Date == checkDate.Date && item.EventTime.Hour == checkDate.Hour && item.EventTime.Minute == checkDate.Minute)
                {
                    lFound = true;
                    break;
                }
                else if (type == DoorType.OUT && item.EventTime.Date == checkDate.Date.AddDays(1) && item.EventTime.Hour == checkDate.Hour && item.EventTime.Minute == checkDate.Minute)
                {
                    lFound = true;
                    break;
                }
            }
            return lFound;
        }
    }
}
