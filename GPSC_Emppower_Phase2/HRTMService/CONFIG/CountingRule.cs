using System;
using System.Collections.Generic;
using System.Text;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.TM.CONFIG
{
    public class CountingRule : AbstractObject
    {
        private string __subGroupSetting = "";
        private string __subAreaSetting = "";
        private string __countingRule = "";
        private string __deductionRule = "";
        public string CompanyCode { get; set; }
        public CountingRule()
        {
        }
        public string SubGroupSetting
        {
            get
            {
                return __subGroupSetting;
            }
            set
            {
                __subGroupSetting = value;
            }
        }
        public string SubAreaSetting
        {
            get
            {
                return __subAreaSetting;
            }
            set
            {
                __subAreaSetting = value;
            }
        }
        public string CountingCode
        {
            get
            {
                return __countingRule;
            }
            set
            {
                __countingRule = value;
            }
        }
        public string DeductionRule
        {
            get
            {
                return __deductionRule;
            }
            set
            {
                __deductionRule = value;
            }
        }
        public override int GetHashCode()
        {
            string cCode = string.Format("COUNTINGRULE_{0}_{1}_{2}", SubGroupSetting, SubAreaSetting, CountingCode);
            return cCode.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return this.GetHashCode() == obj.GetHashCode();
        }

        public  List<CountingRule> GetAllCountingRule()
        {
            //return HR.TM.ServiceManager.HRTMConfig.GetAllCountingRuleList();
            return ServiceManager.CreateInstance(CompanyCode).ERPConfig.GetAllCountingRuleList();
        }

        public  void SaveTo(List<CountingRule> Data, string Mode)
        {
           ServiceManager.CreateInstance(CompanyCode).ESSConfig.SaveCountingRuleList(Data);
        }

        public  AbsenceQuotaType GetAbsenceQuotaType(string EmployeeID, string QuotaTypeCode, string LanguageCode)
        {
            //return ESS.HR.TM.ServiceManager.HRTMConfig.GetAbsenceQuotaType(EmployeeID, QuotaTypeCode, LanguageCode);
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAbsenceQuotaType(EmployeeID, QuotaTypeCode, LanguageCode);
        }

    }
}
