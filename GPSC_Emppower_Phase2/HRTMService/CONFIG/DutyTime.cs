using System;
using System.Collections.Generic;
using System.Text;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.TM.CONFIG
{
    [Serializable]
    public class DutyTime : AbstractObject
    {
        private string __dutyTimeID = string.Empty;
        private string __beginTime = string.Empty;
        private string __endTime = string.Empty;
        private bool __dayWork = false;
        private bool __isActive = false;

        public string DutyTimeID
        {
            get
            {
                return __dutyTimeID;
            }
            set
            {
                __dutyTimeID = value;
            }
        }

        public string BeginTime
        {
            get { return __beginTime; }
            set { __beginTime = value; }
        }

        public string EndTime
        {
            get { return __endTime; }
            set { __endTime = value; }
        }

        public string DutyTimeText
        {
            get { return string.Format("{0} - {1}", __beginTime, __endTime); }
        }

        public bool DayWork
        {
            get { return __dayWork; }
            set { __dayWork = value; }
        }

        public bool IsActive
        {
            get { return __isActive; }
            set { __isActive = value; }
        }
    }
}
