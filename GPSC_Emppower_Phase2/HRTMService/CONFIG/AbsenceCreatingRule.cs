using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using ESS.UTILITY.EXTENSION;
using ESS.HR.TM;
using ESS.TIMESHEET;
using ESS.WORKFLOW;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.CONFIG.TM;

namespace ESS.HR.TM.CONFIG
{
    public class AbsenceCreatingRule : AbstractObject
    {
        private string __absAttGrouping;
        private string __absenceType;
        private int __offsetCutoff;
        
        //private int __offsetValue;
        //private string __offsetFlag;
        private int __managerOffsetValue;
        private string __managerOffsetFlag;
        private int __timeAdminOffsetValue;
        private string __timeAdminOffsetFlag;

        private bool __countOnDayOff;
        private bool __countOnAbsence;
        private int __requireDocDay;
        private bool __checkQuotaWhenCreate;
        private bool __truncateDayOff;

        private bool __canCreateFuture = false;
        private bool __canCreatePast = false;
        private int __absanceInterval;

        private decimal __minAbsenceDays;
        private decimal __maxAbsenceDays;
        private int __advanceInform;
        private int __lateInform;
        private string __timeUnit;
        private int __absenceIndex;

        public string CompanyCode { get; set; }

        public AbsenceCreatingRule()
        { 
        }

        protected override void LoadObjectToDataRow(PropertyInfo prop, System.Data.DataRow oNewRow, System.Data.DataColumn oDC)
        {
            if (prop.Name == "AbsenceType")
            {
                oNewRow["AbsenceKey"] = this.AbsenceType;
                return;
            }
            base.LoadObjectToDataRow(prop, oNewRow, oDC);
        }

        protected override void SetDataRowToObject(PropertyInfo prop, System.Data.DataRow dr)
        {
            if (prop.Name == "AbsenceType")
            {
                this.AbsenceType = (string)dr["AbsenceKey"];
                return;
            }
            base.SetDataRowToObject(prop, dr);
        }

        public string AbsAttGrouping
        {
            get
            {
                return __absAttGrouping;
            }
            set
            {
                __absAttGrouping = value;
            }
        }

        public string AbsenceType
        {
            get
            {
                return __absenceType;
            }
            set
            {
                __absenceType = value;
            }
        }

        public int OffsetCutoff
        {
            get
            {
                return __offsetCutoff;
            }
            set
            {
                __offsetCutoff = value;
            }
        }

        //public int OffsetValue
        //{
        //    get
        //    {
        //        return __offsetValue;
        //    }
        //    set
        //    {
        //        __offsetValue = value;
        //    }
        //}

        //public string OffsetFlag
        //{
        //    get
        //    {
        //        return __offsetFlag;
        //    }
        //    set
        //    {
        //        __offsetFlag = value;
        //    }
        //}

        public int AbsenceInterval
        {
            get
            {
                return __absanceInterval;
            }
            set
            {
                __absanceInterval = value;
            }
        }

        public int ManagerOffsetValue
        {
            get
            {
                return __managerOffsetValue;
            }
            set
            {
                __managerOffsetValue = value;
            }
        }

        public string ManagerOffsetFlag
        {
            get
            {
                return __managerOffsetFlag;
            }
            set
            {
                __managerOffsetFlag = value;
            }
        }

        public int TimeAdminOffsetValue
        {
            get
            {
                return __timeAdminOffsetValue;
            }
            set
            {
                __timeAdminOffsetValue = value;
            }
        }

        public string TimeAdminOffsetFlag
        {
            get
            {
                return __timeAdminOffsetFlag;
            }
            set
            {
                __timeAdminOffsetFlag = value;
            }
        }

        public bool CountOnDayOff
        {
            get
            {
                return __countOnDayOff;
            }
            set
            {
                __countOnDayOff = value;
            }
        }

        public bool CountOnAbsence
        {
            get
            {
                return __countOnAbsence;
            }
            set
            {
                __countOnAbsence = value;
            }
        }

        public int RequireDocDay
        {
            get
            {
                return __requireDocDay;
            }
            set
            {
                __requireDocDay = value;
            }
        }

        public bool CheckQuotaWhenCreate
        {
            get
            {
                return __checkQuotaWhenCreate;
            }
            set
            {
                __checkQuotaWhenCreate = value;
            }
        }

        public bool TruncateDayOff
        {
            get
            {
                return __truncateDayOff;
            }
            set
            {
                __truncateDayOff = value;
            }
        }

        public bool CanCreateFuture
        {
            get
            {
                return __canCreateFuture;
            }
            set
            {
                __canCreateFuture = value;
            }
        }

        public bool CanCreatePast
        {
            get
            {
                return __canCreatePast;
            }
            set
            {
                __canCreatePast = value;
            }
        }

        public decimal MinAbsenceDays
        {
            get
            {
                return __minAbsenceDays;
            }
            set
            {
                __minAbsenceDays = value;
            }
        }

        public decimal MaxAbsenceDays
        {
            get
            {
                return __maxAbsenceDays;
            }
            set
            {
                __maxAbsenceDays = value;
            }
        }

        public int AdvanceInform
        {
            get
            {
                return __advanceInform;
            }
            set
            {
                __advanceInform = value;
            }
        }

        public int LateInform
        {
            get
            {
                return __lateInform;
            }
            set
            {
                __lateInform = value;
            }
        }

        public string TimeUnit
        {
            get
            {
                return __timeUnit;
            }
            set
            {
                __timeUnit = value;
            }
        }

        public int AbsenceIndex
        {
            get
            {
                return __absenceIndex;
            }
            set
            {
                __absenceIndex = value;
            }
        }

        public AbsenceCreatingRule GetCreatingRule(EmployeeData oEmp, string AbsenceTypeCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAbsenceCreatingRule(oEmp, AbsenceTypeCode);
        }

        public static List<TimeElement> LoadArchiveTimeElementRelative(string Employee, DateTime BeginDate, DateTime EndDate)
        {
            List<TimeElement> list = LoadArchiveTimeElementRelative(Employee, BeginDate, EndDate);
            list.Sort(new TimeElementSortingTime(true));
            return list;
        }

        public static List<TimePair> LoadArchiveTimepair(string Employee, DateTime BeginDate, DateTime EndDate)
        {
            List<TimePair> pair = LoadArchiveTimepair(Employee, BeginDate, EndDate);
            pair.Sort(new TimePairSortingDate());
            if (pair.Count == 0)
            {
                TimePair item = new TimePair();
                item.EmployeeID = Employee;
                pair.Add(item);
            }
            TimesheetManagement.FillDWS(Employee, pair, true, false, BeginDate, EndDate);
            return pair;
        }

        private static void CheckData(string v_employeeID,DateTime v_now, DateTime v_checkDate, int v_offsetCutoff,string v_offsetFlag ,int v_offsetValue,bool v_countOnDayOff)
        {
            if (v_offsetFlag == "D")
            {
                #region " Days "
                if (v_countOnDayOff)
                {
                    if (v_now.AddDays(v_offsetValue) > v_checkDate)
                    {
                        throw new Exception("CONFLICT_RULE");
                    }
                }
                else if (!v_countOnDayOff)
                {
                    int nRange;
                    nRange = (v_now.Year * 12 + v_now.Month) - (v_checkDate.Year * 12 + v_checkDate.Month);
                    DateTime oRunMonth;
                    DateTime oFirstDate = new DateTime(v_checkDate.Year, v_checkDate.Month, 1);
                    string cBuffer;
                    bool isDayOff;
                    int nDayOff = 0;
                    int nLastDayOff = 0;
                    for (int index = 0; index <= nRange; index++)
                    {
                        oRunMonth = oFirstDate.AddMonths(index);
                        EMPLOYEE.CONFIG.TM.MonthlyWS oWS = EMPLOYEE.CONFIG.TM.MonthlyWS.GetCalendar(v_employeeID, oRunMonth.Year, oRunMonth.Month);
                        DateTime oBegin, oEnd;
                        if (v_offsetValue > 0)
                        {
                            //oBegin = index == 0 ? v_now : oFirstDate;
                            //oEnd = index == nRange ? v_checkDate : oFirstDate.AddMonths(1).AddDays(-1);
                            ////Edit by Koissares 20160907
                            oBegin = index == 0 ? v_now : oRunMonth;
                            oEnd = index == nRange ? v_checkDate : oRunMonth.AddMonths(1).AddDays(-1);
                        }
                        else
                        {
                            //oBegin = index == 0 ? v_checkDate : oFirstDate;
                            //oEnd = index == nRange ? v_now : oFirstDate.AddMonths(1).AddDays(-1);
                            ////Edit by Koissares 20160907
                            oBegin = index == 0 ? v_checkDate : oRunMonth;
                            oEnd = index == nRange ? v_now : oRunMonth.AddMonths(1).AddDays(-1);
                        }
                        for (DateTime oRunDate = oBegin; oRunDate <= oEnd; oRunDate = oRunDate.AddDays(1))
                        {
                            cBuffer = oWS.GetWSCode(oRunDate.Day, true);
                            isDayOff = cBuffer == "OFF" || cBuffer == "FW" || cBuffer == "SW";
                            if (!isDayOff)
                            {
                                cBuffer = oWS.GetWSCode(oRunDate.Day, false);
                                isDayOff = !(cBuffer == "" || cBuffer == "0");
                            }
                            if (isDayOff)
                            {
                                nDayOff++;
                                nLastDayOff++;
                            }
                            else
                            {
                                nLastDayOff = 0;
                            }
                        }
                    }
                    nDayOff += nLastDayOff;
                    nDayOff = v_offsetValue > 0 ? nDayOff - 1 : -nDayOff;
                    if (v_now.AddDays((v_offsetValue + nDayOff)) >= v_checkDate)
                    {
                        throw new Exception("CONFLICT_RULE");
                    }
                }
                else
                {
                    throw new Exception("CONFIG_ERROR");
                }
                #endregion
            }
            else if (v_offsetFlag == "M")
            {
                DateTime oMonthNow = v_now.AddDays(1 - v_offsetCutoff);
                oMonthNow = new DateTime(oMonthNow.Year, oMonthNow.Month, 1);
                oMonthNow = oMonthNow.AddMonths(v_offsetValue + 1);
                if (oMonthNow > v_checkDate)
                {
                    throw new Exception("CONFLICT_RULE");
                }
            }
            else
            {
                throw new Exception("CONFIG_ERROR");
            }
        }

        private static void CheckEmployeeData(string v_employeeID, DateTime v_now, DateTime v_checkDate, int v_offsetCutoff, string v_timeUnit, int v_advanceInform, int v_lateInform, bool v_countOnDayOff)
        {
            if (v_timeUnit == "D")
            {
                #region Old
                /*
                int nDayOff = 0;
                int nLastDayOff = 0;

                if (!v_countOnDayOff)
                {
                    int nRange;
                    nRange = (v_now.Year * 12 + v_now.Month) - (v_checkDate.Year * 12 + v_checkDate.Month);
                    DateTime oRunMonth;
                    DateTime oFirstDate = new DateTime(v_checkDate.Year, v_checkDate.Month, 1);
                    string cBuffer;
                    string hBuffer;
                    bool isDayOff;
                    bool isHoliday;
                    for (int index = 0; index <= nRange; index++)
                    {
                        oRunMonth = oFirstDate.AddMonths(index);
                        EMPLOYEE.CONFIG.MonthlyWS oWS = EMPLOYEE.CONFIG.MonthlyWS.GetCalendar(v_employeeID, oRunMonth.Year, oRunMonth.Month);
                        DateTime oBegin = new DateTime();
                        DateTime oEnd = new DateTime();
                        if (v_advanceInform > 0 && v_lateInform == 0)
                        {
                            oBegin = index == 0 ? v_now : oFirstDate;
                            oEnd = index == nRange ? v_checkDate : oFirstDate.AddMonths(1).AddDays(-1);
                        }
                        else if (v_advanceInform == 0 || v_advanceInform == 1000 && v_lateInform > 0)
                        {
                            oBegin = index == 0 ? v_checkDate : oFirstDate;
                            oEnd = index == nRange ? v_now : oFirstDate.AddMonths(1).AddDays(-1);
                        }
                        for (DateTime oRunDate = oBegin; oRunDate <= oEnd; oRunDate = oRunDate.AddDays(1))
                        {
                            cBuffer = oWS.GetWSCode(oRunDate.Day, true);
                            isDayOff = cBuffer == "OFF" || cBuffer == "FW" || cBuffer == "SW";
                            if (!isDayOff)
                            {
                                cBuffer = oWS.GetWSCode(oRunDate.Day, false);
                                isDayOff = !(cBuffer == "" || cBuffer == "0");
                            }

                            hBuffer = oWS.GetWSCode(oRunDate.Day, false);
                            isHoliday = hBuffer == "1";

                            if (isDayOff || isHoliday)
                            {
                                nDayOff++;
                                nLastDayOff++;
                            }
                            else
                            {
                                nLastDayOff = 0;
                            }
                        }
                    }
                    nDayOff += nLastDayOff;              
                }
                 */
                #endregion

                EmployeeData emp = new EmployeeData(v_employeeID);

                // check late inform.
                if (0 < v_lateInform && v_lateInform < 100)
                {
                    int count = v_lateInform;
                    DateTime latestDate = DateTime.Now.Date;
                    while (count > 0 && v_checkDate < latestDate)
                    {
                        latestDate = latestDate.AddDays(-1);
                        DailyWS dws = emp.GetDailyWorkSchedule(latestDate);
                        if (!(dws.IsDayOff || dws.IsHoliday))
                        {
                            count--;
                        }
                    }
                    if (v_checkDate < latestDate)
                    {
                        throw new Exception("CONFLICT_RULE");
                    }
                }
                else
                {
                    // could not absence in the past
                    if (v_lateInform == 0)
                    {
                        if (v_checkDate < DateTime.Now.Date)
                        {
                            throw new Exception("CONFLICT_RULE");
                        }
                    }
                }

                // check advance inform
                if (0 < v_advanceInform && v_advanceInform < 100)
                {
                    int count = v_advanceInform;
                    DateTime latestDate = DateTime.Now.Date.AddDays(v_advanceInform);
                    /*
                    while (count > 0 && latestDate < v_checkDate)
                    {
                        DailyWS dws = emp.GetDailyWorkSchedule(latestDate);
                        if (!(dws.IsDayOff || dws.IsHoliday))
                        {
                            count--;
                        }
                        latestDate = latestDate.AddDays(1);
                    }
                     */
                    if (v_checkDate < latestDate)
                    {
                        throw new Exception("CONFLICT_RULE");
                    }
                }
                else
                {
                    // could not absence in the future
                    if (v_advanceInform >= 100)
                    {
                        if (v_checkDate > DateTime.Now.Date)
                            throw new Exception("CONFLICT_RULE");
                    }
                }
            }
            else if (v_timeUnit == "M")
            {
                DateTime oMonthNow = v_now.AddDays(1 - v_offsetCutoff);
                oMonthNow = new DateTime(oMonthNow.Year, oMonthNow.Month, 1);

                if (v_advanceInform > 0 && v_lateInform == 0)
                    oMonthNow = oMonthNow.AddMonths(v_advanceInform + 1);
                else
                    oMonthNow = oMonthNow.AddMonths(-v_lateInform + 1);

                if (oMonthNow > v_checkDate)
                {
                    throw new Exception("CONFLICT_RULE");
                }
            }
            else
            {
                throw new Exception("CONFIG_ERROR");
            }
        }

        public void IsCanCreate(string EmployeeID, DateTime BeginDate, DateTime EndDate, DateTime DocumentDate)
        {
            DateTime oNow = DocumentDate.Date;
            //DateTime oCheckDate = this.OffsetValue > 0 ? BeginDate.Date : EndDate.Date;

            DateTime oCheckDate = new DateTime();
            if (AdvanceInform > 0 && LateInform == 0)
                oCheckDate = BeginDate.Date;
            else if (AdvanceInform == 0 || AdvanceInform == 1000 && LateInform > 0)
                oCheckDate = EndDate.Date;
            else
                oCheckDate = BeginDate.Date;

            if (WorkflowPrinciple.CurrentIdentity.EmployeeID != EmployeeID)
            {
                if (WorkflowPrinciple.Current.IsInRole("#PRESIDENT"))
                {
                    return;
                }
                else if (WorkflowPrinciple.Current.IsInRole("#MANAGER"))
                {
                    try
                    {
                        CheckData(EmployeeID, oNow, oCheckDate, OffsetCutoff, this.ManagerOffsetFlag, this.ManagerOffsetValue, CountOnDayOff);
                    }
                    catch (Exception)
                    {
                        throw new AbsenceCreatingException("#MANAGER", this);
                    }
                    return;
                }
                else if (WorkflowPrinciple.Current.IsInRole("TIMEADMIN"))
                {
                    try
                    {
                        CheckData(EmployeeID, oNow, oCheckDate, OffsetCutoff, this.TimeAdminOffsetFlag, this.TimeAdminOffsetValue, CountOnDayOff);
                    }
                    catch (Exception)
                    {
                        throw new AbsenceCreatingException("TIMEADMIN", this);
                    }
                    return;
                }
            }

            if (WorkflowPrinciple.CurrentIdentity.EmployeeID == EmployeeID)
            {
                if (WorkflowPrinciple.Current.IsInRole("TIMEADMIN"))
                {
                    try
                    {
                        CheckData(EmployeeID, oNow, oCheckDate, OffsetCutoff, this.TimeAdminOffsetFlag, this.TimeAdminOffsetValue, CountOnDayOff);
                    }
                    catch (Exception)
                    {
                        throw new AbsenceCreatingException("TIMEADMIN", this);
                    }
                    return;
                } 
                else if (WorkflowPrinciple.Current.IsInRole("#MANAGER"))
                {
                    try
                    {
                        CheckData(EmployeeID, oNow, oCheckDate, OffsetCutoff, this.ManagerOffsetFlag, this.ManagerOffsetValue, CountOnDayOff);
                    }
                    catch (Exception)
                    {
                        throw new AbsenceCreatingException("#MANAGER", this);
                    }
                    return;
                }
                //if (this.OffsetValue < 0 && oCheckDate > oNow && !CanCreateFuture)
                //if (AdvanceInform == 1000 && oCheckDate > oNow && !CanCreateFuture)
                else if (!WorkflowPrinciple.Current.IsInRole("TIMEADMIN") && !WorkflowPrinciple.Current.IsInRole("#MANAGER") && oCheckDate > oNow && !CanCreateFuture)
                {
                    throw new Exception("CREATE_FUTURE");
                }
                //else if (this.OffsetValue > 0 && oCheckDate < oNow && !CanCreatePast)
                //else if (LateInform == 0 && oCheckDate < oNow && !CanCreatePast)
                else if (!WorkflowPrinciple.Current.IsInRole("TIMEADMIN") && !WorkflowPrinciple.Current.IsInRole("#MANAGER") && oCheckDate < oNow && !CanCreatePast)
                {
                    throw new Exception("CREATE_PAST");
                }
                try
                {
                        //CheckData(EmployeeID, oNow, oCheckDate, OffsetCutoff, OffsetFlag, OffsetValue, CountOnDayOff);
                        CheckEmployeeData(EmployeeID, oNow, oCheckDate, OffsetCutoff, TimeUnit, AdvanceInform, LateInform, CountOnDayOff);
                }
                catch (Exception e)
                {
                    throw new AbsenceCreatingException("#GROUPEMPLOYEE", this);
                }
            }

            #region Old
            ////if (this.OffsetValue < 0 && oCheckDate > oNow && !CanCreateFuture)
            ////if (AdvanceInform == 1000 && oCheckDate > oNow && !CanCreateFuture)
            //if (oCheckDate > oNow && !CanCreateFuture)
            //{
            //    throw new Exception("CREATE_FUTURE");
            //}
            ////else if (this.OffsetValue > 0 && oCheckDate < oNow && !CanCreatePast)
            ////else if (LateInform == 0 && oCheckDate < oNow && !CanCreatePast)
            //else if (oCheckDate < oNow && !CanCreatePast)
            //{
            //    throw new Exception("CREATE_PAST");
            //}
            //try
            //{
            //    if (!WorkflowPrinciple.Current.IsInRole("TIMEADMIN") && !WorkflowPrinciple.Current.IsInRole("#MANAGER"))
            //    {
            //        //CheckData(EmployeeID, oNow, oCheckDate, OffsetCutoff, OffsetFlag, OffsetValue, CountOnDayOff);
            //        CheckEmployeeData(EmployeeID, oNow, oCheckDate, OffsetCutoff, TimeUnit, AdvanceInform, LateInform, CountOnDayOff);
            //    }
            //}
            //catch (Exception e)
            //{
            //    throw new AbsenceCreatingException("#GROUPEMPLOYEE", this);
            //}
            #endregion
        }
    }
}