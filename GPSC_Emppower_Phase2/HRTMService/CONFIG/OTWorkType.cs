using System;
using System.Collections.Generic;
using System.Text;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.TM.CONFIG
{
    [Serializable]
    public class OTWorkType : AbstractObject
    {
        private string __otWorkTypeID;
        private string __otWorkTypeDesc;
        private bool __isActive;

        public OTWorkType()
        { 
        }

        public string OTWorkTypeID
        {
            get
            {
                return __otWorkTypeID;
            }
            set
            {
                __otWorkTypeID = value;
            }
        }

        public string OTWorkTypeDesc
        {
            get
            {
                return __otWorkTypeDesc;
            }
            set
            {
                __otWorkTypeDesc = value;
            }
        }

        public bool IsActive
        {
            get
            {
                return __isActive;
            }
            set
            {
                __isActive = value;
            }
        }
    }
}
