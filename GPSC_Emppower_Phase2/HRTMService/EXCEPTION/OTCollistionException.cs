using System;
using System.Collections.Generic;
using System.Text;
using ESS.DATA;
using ESS.DATA.EXCEPTION;
using ESS.TIMESHEET;

namespace ESS.HR.TM
{
    public class OTCollistionException : DataServiceException
    {
        private int __index1;
        private int __index2;
        private TimesheetData __data = null;
        public OTCollistionException(int index1, int index2)
            : base("HRTM_EXCEPTION", "COLLISION_OCCUR")
        {
            __index1 = index1;
            __index2 = index2;
        }

        public OTCollistionException(int index1, TimesheetData data)
            : base("HRTM_EXCEPTION", "COLLISION_OCCUR_EXTERNAL")
        {
            __index1 = index1;
            __data = data;
        }

        public new TimesheetData Data
        {
            get
            {
                return __data;
            }
        }

        public int Index1
        {
            get
            {
                return __index1;
            }
        }
        public int Index2
        {
            get
            {
                return __index2;
            }
        }

    }
}
