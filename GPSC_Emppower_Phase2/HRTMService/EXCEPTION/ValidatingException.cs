using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.HR.TM
{
    public class ValidatingException : Exception
    {
        public ValidatingException(string message)
            : base(message)
        { 
        }
    }
}
