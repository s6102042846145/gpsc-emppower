using System;
using System.Collections.Generic;
using System.Text;
using ESS.HR.TM.CONFIG;

namespace ESS.HR.TM
{
    public class DeleteDailyOTException : Exception
    {
        private DailyOTDeletingRule __rule;
        public DeleteDailyOTException(DailyOTDeletingRule rule)
        {
            __rule = rule;
        }
        public DailyOTDeletingRule Rule
        {
            get
            {
                return __rule;
            }
        }

    }
}
