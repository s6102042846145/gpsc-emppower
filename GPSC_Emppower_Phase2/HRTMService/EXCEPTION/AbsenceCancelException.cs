using System;
using System.Collections.Generic;
using System.Text;
using ESS.HR.TM.CONFIG;

namespace ESS.HR.TM
{
    public class AbsenceCancelException : Exception
    {
        private AbsenceCancelRule __rule;
        public AbsenceCancelException(AbsenceCancelRule rule)
        {
            __rule = rule;
        }
        public AbsenceCancelRule Rule
        {
            get
            {
                return __rule;
            }
        }
    }
}
