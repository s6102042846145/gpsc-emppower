using System;
using System.Collections.Generic;
using System.Text;
using ESS.DATA;
using ESS.DATA.EXCEPTION;

namespace ESS.HR.TM
{
    public class PlannedOTConflictException : DataServiceException 
    {
        private int __dayOffset;
        private DateTime __otDate;
        public PlannedOTConflictException(DateTime PlannedOTDate, int DayOffset)
            : base("HRTM_EXCEPTION", DayOffset == 0 ? "PLANNEDOT_CONFLICT_0" : "PLANNEDOT_CONFLICT")
        {
            __dayOffset = DayOffset;
            __otDate = PlannedOTDate;
        }

        public DateTime PlannedOTDate
        {
            get
            {
                return __otDate;
            }
        }

        public int DayOffset
        {
            get
            {
                return __dayOffset;
            }
        }
    }
}
