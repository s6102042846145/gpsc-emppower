using System;
using System.Collections.Generic;
using System.Text;
using ESS.HR.TM.CONFIG;

namespace ESS.HR.TM
{
    public class AbsenceCreatingException : Exception
    {
        private string __userRole = "";
        private string __absAttGrouping = "";
        private string __absenceType = "";
        private int __offsetValue = 0;
        private int __offsetCutoff = 0;
        private string __offsetFlag = "";
        private bool __countOnDayOff = false;

        private int __advanceInform = -1;
        private int __lateInform = -1;
        private decimal __minAbsenceDays = 0;
        private decimal __maxAbsenceDays = 999;

        public AbsenceCreatingException(string userRole, AbsenceCreatingRule rule)
        {
            __userRole = userRole;
            __absAttGrouping = rule.AbsAttGrouping;
            __absenceType = rule.AbsenceType;
            __countOnDayOff = rule.CountOnAbsence;
            __offsetCutoff = rule.OffsetCutoff;
            switch (userRole.ToUpper())
            {
                case "#MANAGER":
                    __offsetFlag = rule.ManagerOffsetFlag;
                    __offsetValue = rule.ManagerOffsetValue;
                    break;
                case "TIMEADMIN":
                    __offsetFlag = rule.TimeAdminOffsetFlag;
                    __offsetValue = rule.TimeAdminOffsetValue;
                    break;
                default:
                    //__offsetFlag = rule.OffsetFlag;
                    //__offsetValue = rule.OffsetValue;
                    //break;

                    __offsetFlag = rule.TimeUnit;
                    if (rule.AdvanceInform == 0 || rule.AdvanceInform == 1000 && rule.LateInform > 0)
                        __offsetValue = -rule.LateInform;
                    else if (rule.AdvanceInform > 0 && rule.LateInform == 0 || rule.LateInform == 1000)
                    {
                        __offsetValue = rule.AdvanceInform;
                        __minAbsenceDays = rule.MinAbsenceDays;
                        __maxAbsenceDays = rule.MaxAbsenceDays;
                    }
                    else if (rule.AdvanceInform > 0 && rule.LateInform > 0)
                    {
                        __offsetValue = 999;
                        __advanceInform = rule.AdvanceInform;
                        __lateInform = rule.LateInform;
                    }
                    break;
            }
        }
        public string UserRole
        {
            get
            {
                return __userRole;
            }
        }
        public string AbsAttGrouping
        {
            get
            {
                return __absAttGrouping;
            }
        }
        public string AbsenceType
        {
            get
            {
                return __absenceType;
            }
        }
        public int OffsetValue
        {
            get
            {
                return __offsetValue;
            }
        }
        public int OffsetCutoff
        {
            get
            {
                return __offsetCutoff;
            }
        }
        public string OffsetFlag
        {
            get
            {
                return __offsetFlag;
            }
        }
        public bool CountOnDayOff
        {
            get
            {
                return __countOnDayOff;
            }
        }
        public int AdvanceInform
        {
            get
            {
                return __advanceInform;
            }
        }
        public int LateInform
        {
            get
            {
                return __lateInform;
            }
        }

        public decimal MinAbsenceDays
        {
            get
            {
                return __minAbsenceDays;
            }
        }

        public decimal MaxAbsenceDays
        {
            get
            {
                return __maxAbsenceDays;
            }
        }

    }
}
