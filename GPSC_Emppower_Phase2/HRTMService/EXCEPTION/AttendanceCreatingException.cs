using System;
using System.Collections.Generic;
using System.Text;
using ESS.HR.TM.CONFIG;

namespace ESS.HR.TM
{
    public class AttendanceCreatingException : Exception
    {
        private string __absAttGrouping = string.Empty;
        private string __attendanceType = string.Empty;
        private string __errMessage = string.Empty;
        private bool __isPast = false;
        private string __offsetFlag = string.Empty;
        private int __offsetValue = 0;
        private string __advanceFlag = string.Empty;
        private int __advanceValue = 0;

       // private bool __countOnDayOff = false;
        //private int __offsetValue = 0;
        //private int __offsetCutoff = 0;
        //private string __offsetFlag = "";
        

        public AttendanceCreatingException(AttendanceCreatingRule rule,string strErrorMessage,bool IsPast)
        {
            __absAttGrouping = rule.AbsAttGrouping;
            __attendanceType = rule.AttendanceKey;
            __errMessage = strErrorMessage;
            __offsetFlag = rule.OffsetFlag;
            __offsetValue = rule.OffsetValue;
            __advanceFlag = rule.AdvanceFlag;
            __advanceValue = rule.AdvanceValue;
            __isPast = IsPast;
        }

        public string AbsAttGrouping
        {
            get
            {
                return __absAttGrouping;
            }
        }
        public string AttendanceType
        {
            get
            {
                return __attendanceType;
            }
        }
        public string ErrMessage
        {
            get
            {
                return __errMessage;
            }
        }
        public string OffsetFlag
        {
            get
            {
                return __offsetFlag;
            }
        }
        public int OffsetValue
        {
            get
            {
                return __offsetValue;
            }
        }
        public string AdvanceFlag
        {
            get
            {
                return __advanceFlag;
            }
        }
        public int AdvanceValue
        {
            get
            {
                return __advanceValue;
            }
        }
        public bool IsPast
        {
            get
            {
                return __isPast;
            }
        }
    }
}
