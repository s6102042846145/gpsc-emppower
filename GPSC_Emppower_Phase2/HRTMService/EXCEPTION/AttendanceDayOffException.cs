using System;
using System.Collections.Generic;
using System.Text;
using ESS.HR.TM;
using ESS.HR;
using ESS.HR.TM.CONFIG;

namespace ESS.HR.TM
{
    public class AttendanceDayOffException : Exception
    {
        private DateTime __dtDayOff;
        public AttendanceDayOffException(DateTime dtDayOff)
            : base("Cannot use day-off with data")
        {
            __dtDayOff = dtDayOff;
        }
        public DateTime DayOffData
        {
            get
            {
                return __dtDayOff;
            }
        }
    }
}
