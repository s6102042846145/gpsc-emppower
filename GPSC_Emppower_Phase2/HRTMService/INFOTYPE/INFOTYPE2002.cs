using System;
using System.Collections.Generic;
using System.Text;
using ESS.DATA;
using ESS.EMPLOYEE;
using ESS.HR.TM;
using ESS.HR.TM.CONFIG;
using ESS.TIMESHEET;
using ESS.WORKFLOW;
using ESS.EMPLOYEE.CONFIG;
using ESS.EMPLOYEE.ABSTRACT;
using ESS.HR.TM.DATACLASS;

namespace ESS.HR.TM.INFOTYPE
{
    public class INFOTYPE2002 : AbstractInfoType, ICollisionable
    {
        private TimeSpan __beginTime = TimeSpan.Zero;
        private TimeSpan __endTime = TimeSpan.Zero;
        private decimal __absenceDays;
        private decimal __absenceHours;
        private decimal __payrollDays;
        private bool __allDayFlag = false;
        private string __status = "";
        private string __remark = "";
        private bool __isMark = true;
        private bool __isPost = false;
        private bool __isPrevDay = false;
        private string __requestNo = "";
        private string __IOnumber = "";
        private string __activityCode = "";

        public INFOTYPE2002()
        {
        }

        public override string InfoType
        {
            get { return "2002"; }
        }

        #region Properties 

        public string RequestNo
        {
            get
            {
                return __requestNo;
            }
            set
            {
                __requestNo = value;
            }
        }

        public string AttendanceType
        {
            get
            {
                return SubType;
            }
            set
            {
                SubType = value;
            }
        }

        public TimeSpan BeginTime
        {
            get
            {
                return __beginTime;
            }
            set
            {
                __beginTime = value;
            }
        }

        public TimeSpan EndTime
        {
            get
            {
                return __endTime;
            }
            set
            {
                __endTime = value;
            }
        }

        public decimal AttendanceDays
        {
            get
            {
                return __absenceDays;
            }
            set
            {
                __absenceDays = value;
            }
        }

        public decimal AttendanceHours
        {
            get
            {
                return __absenceHours;
            }
            set
            {
                __absenceHours = value;
            }
        }
        public decimal PayrollDays
        {
            get
            {
                if (__payrollDays == 0.0M && this.AllDayFlag)
                {
                    return 0.0M;
                }
                return __payrollDays;
            }
            set
            {
                __payrollDays = value;
            }
        }
        public bool AllDayFlag
        {
            get
            {
                return __allDayFlag;
            }
            set
            {
                __allDayFlag = value;
            }
        }

        public string Status
        {
            get
            {
                return __status;
            }
            set
            {
                __status = value;
            }
        }

        public string Remark
        {
            get
            {
                return __remark;
            }
            set
            {
                __remark = value;
            }
        }


        public bool IsMark
        {
            get
            {
                return __isMark;
            }
            set
            {
                __isMark = value;
            }
        }

        public bool IsPost
        {
            get
            {
                return __isPost;
            }
            set
            {
                __isPost = value;
            }
        }

        public bool IsPrevDay
        {
            get
            {
                return __isPrevDay;
            }
            set
            {
                __isPrevDay = value;
            }
        }

        public string IONumber
        {
            get
            {
                return __IOnumber;
            }
            set
            {
                __IOnumber = value;
            }
        }

        public string ActivityCode
        {
            get
            {
                return __activityCode;
            }
            set
            {
                __activityCode = value;
            }
        }

        #endregion

        public int CountData(EmployeeData employeeData, DateTime BeginDate, DateTime EndDate, bool OnlyRecordedData)
        {
            return GetData(employeeData, BeginDate, EndDate, OnlyRecordedData).Count;
        }
        public List<INFOTYPE2002> GetData(EmployeeData employeeData, DateTime BeginDate, DateTime EndDate)
        {
            return GetData(employeeData, BeginDate, EndDate, false);
        }
        public List<INFOTYPE2002> GetData(EmployeeData employeeData, DateTime BeginDate, DateTime EndDate, bool OnlyRecordedData)
        {
            List<INFOTYPE2002> oReturn = new List<INFOTYPE2002>();
            List<INFOTYPE2002> list;
            if (!OnlyRecordedData)
            {
                list = ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).ESSData.GetAttendanceList(employeeData.EmployeeID, BeginDate, EndDate);
                foreach (INFOTYPE2002 item in list)
                {
                    item.Status = "INPROCESS";
                    oReturn.Add(item);
                }
            }
            list = ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).ERPData.GetAttendanceList(employeeData.EmployeeID, BeginDate, EndDate);
            foreach (INFOTYPE2002 item in list)
            {
                item.Status = "RECORDED";
                oReturn.Add(item);
            }
            oReturn.Sort(new INFOTYPE2002_Comparer());
            return oReturn;
        }

        public List<INFOTYPE2002> GetDataFromDB(string EmployeeID, DateTime BeginDate, DateTime EndDate, bool OnlyRecordedData = false)
        {
            List<INFOTYPE2002> oReturn = new List<INFOTYPE2002>();
            List<INFOTYPE2002> list;
            if (!OnlyRecordedData)
            {
                list = ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).ESSData.GetAttendanceList(EmployeeID, BeginDate, EndDate);
                foreach (INFOTYPE2002 item in list)
                {
                    item.Status = "INPROCESS";
                    oReturn.Add(item);
                }
            }
            list = ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).ESSData.GetTM_AttendanceList(EmployeeID, BeginDate, EndDate);
            foreach (INFOTYPE2002 item in list)
            {
                item.Status = "RECORDED";
                oReturn.Add(item);
            }
            oReturn.Sort(new INFOTYPE2002_Comparer());
            return oReturn;
        }

        public List<INFOTYPE2002> GetDataOnlyRecorded(EmployeeData employeeData, DateTime BeginDate, DateTime EndDate)
        {
            List<INFOTYPE2002> oReturn = new List<INFOTYPE2002>();
            List<INFOTYPE2002> list;
            list = ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).ERPData.GetAttendanceList(employeeData.EmployeeID, BeginDate, EndDate);
            foreach (INFOTYPE2002 item in list)
            {
                item.Status = "RECORDED";
                oReturn.Add(item);
            }
            return oReturn;
        }


        public void ValidateData(DateTime DocumentDate)
        {
            // validate begin - end
            DateTime date1, date2;
            if (this.AllDayFlag)
            {
                date1 = this.BeginDate;
                date2 = this.EndDate;
            }
            else
            {
                date1 = this.BeginDate.Add(this.BeginTime);
                date2 = this.EndDate.Add(this.EndTime);
                if (date2 < date1)
                {
                    date2 = date2.AddDays(1);
                }
            }
            if (date1 > date2)
            {
                throw new Exception("BEGINDATE_GREATER_THAN_ENDDATE");
            }



            // validate AbsenceType
            //AttendanceType oAttendanceType = HRTMManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetAttendanceType(this.EmployeeID, this.AttendanceType, "");
            AttendanceType oAttendanceType = HRTMManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetAttendanceType(this.EmployeeID, this.AttendanceType, "TH");
            if (oAttendanceType.Key == null)
            {
                throw new Exception(string.Format("AttendanceType '{0}' for EmpID '{1}' not found", this.AttendanceType, this.EmployeeID));
            }
                
            EmployeeData empData = new EmployeeData(EmployeeID);

            ////AddBy: Ratchatawan W. (2012-02-21)
            ////Cannot created attendance in day-off
            ////Start

            //DailyWS DailyWS = empData.GetDailyWorkSchedule(BeginDate);
            //MonthlyWS MonthWS = MonthlyWS.GetCalendar(EmployeeID, BeginDate.Year, BeginDate.Month);
            //if (DailyWS.IsDayOff || MonthWS.GetWSCode(BeginDate.Day, false) == "1")
            //    throw new AttendanceDayOffException(BeginDate);

            //DailyWS = empData.GetDailyWorkSchedule(EndDate);
            //MonthWS = MonthlyWS.GetCalendar(EmployeeID, EndDate.Year, EndDate.Month);
            //if (DailyWS.IsDayOff || MonthWS.GetWSCode(EndDate.Day, false) == "1")
            //    throw new AttendanceDayOffException(EndDate);


            AttendanceCreatingRule oACR = HRTMManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetCreatingRule(empData, this.AttendanceType, this.BeginDate, this.EndDate);

            //AttendanceCreatingRule oACR = HRTMManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetCreatingRule(this.EmployeeID, this.AttendanceType, this.BeginDate, this.EndDate);

            #region Check requested absence or attendance is in process?
            DateTime tmpBeginDate = this.BeginDate.Add(this.BeginTime);
            DateTime tmpEndDate = this.EndDate.Add(this.EndTime);
            if (tmpEndDate < tmpBeginDate)
                tmpEndDate = tmpEndDate.AddDays(1);
            else if (this.AllDayFlag)
                tmpEndDate = tmpEndDate.AddDays(1).AddSeconds(-1);

            string ExistingDate = string.Empty;
            //List<INFOTYPE2001> listInfotype2001 =new INFOTYPE2001().GetData(empData, tmpBeginDate, tmpEndDate);
            List<INFOTYPE2001> listInfotype2001 = new INFOTYPE2001().GetData(empData, tmpBeginDate.Date, tmpEndDate.Date);
            if (listInfotype2001.Exists(delegate(INFOTYPE2001 item) { return item.RequestNo != this.RequestNo; }))
            {
                INFOTYPE2001 ExistingItem = listInfotype2001.Find(delegate(INFOTYPE2001 item) { return item.RequestNo != this.RequestNo; });
                if (ExistingItem.AllDayFlag)
                {
                    if (ExistingItem.BeginDate <= this.EndDate && ExistingItem.EndDate >= this.BeginDate)
                    {
                        //����¡�ë�ӡѺ
                        if (ExistingItem.BeginDate.Date == ExistingItem.EndDate.Date)//���ѹ����
                        {
                            ExistingDate = ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US"));
                        }
                        else//�������ѹ
                        {
                            ExistingDate = string.Format("{0} - {1}", ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")), ExistingItem.EndDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")));
                        }
                        throw new Exception("ABSENCE_IN_PROCESS|" + ExistingDate);
                    }
                }
                else
                {
                    DateTime ExistingBegin = ExistingItem.BeginDate.Add(ExistingItem.BeginTime);
                    DateTime ExistingEnd = ExistingItem.EndDate.Add(ExistingItem.EndTime);
                    if (ExistingEnd < ExistingBegin)
                        ExistingEnd = ExistingEnd.AddDays(1);

                    if (tmpBeginDate <= ExistingEnd && tmpEndDate >= ExistingBegin)   //Check Overlap 20171214 Koissares
                    {
                        //����¡�ë�ӡѺ
                        if (ExistingItem.BeginDate.Date == ExistingItem.EndDate.Date)//���ѹ����
                        {
                            ExistingDate = ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")) + " " + ExistingItem.BeginTime.ToString() + "-" + ExistingItem.EndTime.ToString(); ;
                        }
                        else//�������ѹ
                        {
                            ExistingDate = string.Format("{0} - {1}", ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")), ExistingItem.EndDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")));
                        }
                        throw new Exception("ABSENCE_IN_PROCESS|" + ExistingDate);
                    }
                }
                
            }

            //List<INFOTYPE2002> listInfotype2002 = GetData(empData, tmpBeginDate, tmpEndDate);
            List<INFOTYPE2002> listInfotype2002 = GetData(empData, tmpBeginDate.Date, tmpEndDate.Date);
            if (listInfotype2002.Exists(delegate(INFOTYPE2002 item) { return item.RequestNo != this.RequestNo; }))
            {
                INFOTYPE2002 ExistingItem = listInfotype2002.Find(delegate(INFOTYPE2002 item) { return item.RequestNo != this.RequestNo; });
                if (ExistingItem.AllDayFlag)
                {
                    if (ExistingItem.BeginDate <= this.EndDate && ExistingItem.EndDate >= this.BeginDate)
                    {
                        //����¡�ë�ӡѺ
                        if (ExistingItem.BeginDate.Date == ExistingItem.EndDate.Date)//�Ѻ�ͧ�����ѹ����
                        {
                            ExistingDate = ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US"));
                        }
                        else//�Ѻ�ͧ���������ѹ
                        {
                            ExistingDate = string.Format("{0} - {1}", ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")), ExistingItem.EndDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")));
                        }
                        throw new Exception("ATTENDANCE_IN_PROCESS|" + ExistingDate);
                    }
                }
                else
                {
                    DateTime ExistingBegin = ExistingItem.BeginDate.Add(ExistingItem.BeginTime);
                    DateTime ExistingEnd = ExistingItem.EndDate.Add(ExistingItem.EndTime);
                    if (ExistingEnd < ExistingBegin)
                        ExistingEnd = ExistingEnd.AddDays(1);

                    if (tmpBeginDate <= ExistingEnd && tmpEndDate >= ExistingBegin)   //Check Overlap 20171214 Koissares
                    {
                        //����¡�ë�ӡѺ
                        if (ExistingItem.BeginDate.Date == ExistingItem.EndDate.Date)//�Ѻ�ͧ�����ѹ����
                        {
                            ExistingDate = ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")) + " " + ExistingItem.BeginTime.ToString() + "-" + ExistingItem.EndTime.ToString();
                        }
                        else//�Ѻ�ͧ���������ѹ
                        {
                            ExistingDate = string.Format("{0} - {1}", ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")), ExistingItem.EndDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")));
                        }
                        throw new Exception("ATTENDANCE_IN_PROCESS|" + ExistingDate);
                    }
                }
            }
            #endregion

            //ModifiedBy: Ratchatawan W. (2012-03-21)
            if (!(WorkflowPrinciple.Current.IsInRole("TIMEADMIN") || WorkflowPrinciple.Current.IsInRole("#MANAGER")))
                oACR.IsCanCreate();
            else if (this.EmployeeID == WorkflowPrinciple.CurrentIdentity.EmployeeID)
                oACR.IsCanCreate();

            //if (!WorkflowPrinciple.Current.IsInRole("TIMEADMIN") && !WorkflowPrinciple.Current.IsInRole("#MANAGER"))
            //{
            //    oACR.IsCanCreate();
            //}

            #region " check Time "
            if (oACR.IsLoadClockIN)
            {
                if (this.AllDayFlag)
                {
                    throw new Exception("THIS_TYPE_CAN_NOT_USE_FULL_DAY");
                }
                else
                {
                    if (!oACR.CheckClock(DoorType.IN, this.BeginDate.Add(this.BeginTime)) && !oACR.CheckClock(DoorType.OUT, this.BeginDate.Add(this.BeginTime)))
                    {
                        throw new Exception("NOT_FOUND_EVIDANCE");
                    }
                }
            }
            if (oACR.IsLoadClockOUT)
            {
                if (this.AllDayFlag)
                {
                    throw new Exception("THIS_TYPE_CAN_NOT_USE_FULL_DAY");
                }
                else
                {
                    if (!oACR.CheckClock(DoorType.IN, this.EndDate.Add(this.EndTime)) && !oACR.CheckClock(DoorType.OUT, this.EndDate.Add(this.EndTime)))
                    {
                        throw new Exception("NOT_FOUND_EVIDANCE");
                    }
                }
            }
            if (oACR.IsSwapClock)
            {
                if (this.AllDayFlag)
                {
                    throw new Exception("THIS_TYPE_CAN_NOT_USE_FULL_DAY");
                }
                else
                {
                    if (oACR.CheckClock(DoorType.IN, this.BeginDate.Add(this.BeginTime)))
                    {
                        if (!oACR.CheckClock(DoorType.IN, this.EndDate.Add(this.EndTime)))
                        {
                            throw new Exception("NOT_FOUND_EVIDANCE");
                        }
                    }
                    else if (oACR.CheckClock(DoorType.OUT, this.BeginDate.Add(this.BeginTime)))
                    {
                        if (!oACR.CheckClock(DoorType.IN, this.EndDate.Add(this.EndTime)))
                        {
                            if (!oACR.CheckClock(DoorType.OUT, this.EndDate.Add(this.EndTime)))
                            {
                                throw new Exception("NOT_FOUND_EVIDANCE");
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("NOT_FOUND_EVIDANCE");
                    }
                }
            }
            #endregion

            AbsAttCalculateResult oResult;
            if (BeginDate != EndDate && !AllDayFlag)
            {
                this.AttendanceDays = 0.0M;
                this.AttendanceHours = 0.0M;
                this.PayrollDays = 0.0M;
                for (DateTime rundate = BeginDate; rundate <= EndDate; rundate = rundate.AddDays(1))
                {
                    if (rundate == EndDate)
                    {
                        oResult = ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).ERPData.CalculateAbsAttEntry(EmployeeID, SubType, rundate, rundate, BeginTime, EndTime, false, false, false);
                        this.EndTime = oResult.EndTime;
                        this.AttendanceDays += oResult.AbsenceDays;
                        this.AttendanceHours += oResult.AbsenceHours;
                        this.PayrollDays += oResult.PayrollDays;
                    }
                    else if (rundate == EndDate.AddDays(-1) && BeginTime >= EndTime)
                    {
                        oResult = ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).ERPData.CalculateAbsAttEntry(EmployeeID, SubType, rundate, rundate, BeginTime, EndTime, false, false, false);
                        this.EndTime = oResult.EndTime;
                        this.AttendanceDays += oResult.AbsenceDays;
                        this.AttendanceHours += oResult.AbsenceHours;
                        this.PayrollDays += oResult.PayrollDays;
                        break;
                    }
                    else
                    {
                        oResult = ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).ERPData.CalculateAbsAttEntry(EmployeeID, SubType, rundate, rundate.AddDays(1), BeginTime, BeginTime, true, false, false);
                        this.AttendanceDays += 1;
                        this.AttendanceHours += 24;
                        this.PayrollDays += 1;
                    }
                }
            }
            else
            {
                oResult = ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).ERPData.CalculateAbsAttEntry(EmployeeID, SubType, BeginDate, EndDate, BeginTime, EndTime, AllDayFlag, false, false);
                this.AttendanceDays = oResult.AbsenceDays;
                this.AttendanceHours = oResult.AbsenceHours;
                //this.AllDayFlag = oResult.FullDay;
                this.BeginTime = oResult.BeginTime;
                this.EndTime = oResult.EndTime;
                this.PayrollDays = oResult.PayrollDays;
            }
        }

        public void ValidateData(DateTime DocumentDate, string CompanyCode)
        {
            // validate begin - end
            DateTime date1, date2;
            if (this.AllDayFlag)
            {
                date1 = this.BeginDate;
                date2 = this.EndDate;
            }
            else
            {
                date1 = this.BeginDate.Add(this.BeginTime);
                date2 = this.EndDate.Add(this.EndTime);
                if (date2 < date1)
                {
                    date2 = date2.AddDays(1);
                }
            }
            if (date1 > date2)
            {
                throw new Exception("BEGINDATE_GREATER_THAN_ENDDATE");
            }



            // validate AbsenceType
            //AttendanceType oAttendanceType = HRTMManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetAttendanceType(this.EmployeeID, this.AttendanceType, "");
            AttendanceType oAttendanceType = HRTMManagement.CreateInstance(CompanyCode).GetAttendanceType(this.EmployeeID, this.AttendanceType, "TH");
            if (oAttendanceType.Key == null)
            {
                throw new Exception(string.Format("AttendanceType '{0}' for EmpID '{1}' not found", this.AttendanceType, this.EmployeeID));
            }

            EmployeeData empData = new EmployeeData(EmployeeID) { CompanyCode = CompanyCode };

            ////AddBy: Ratchatawan W. (2012-02-21)
            ////Cannot created attendance in day-off
            ////Start

            //DailyWS DailyWS = empData.GetDailyWorkSchedule(BeginDate);
            //MonthlyWS MonthWS = MonthlyWS.GetCalendar(EmployeeID, BeginDate.Year, BeginDate.Month);
            //if (DailyWS.IsDayOff || MonthWS.GetWSCode(BeginDate.Day, false) == "1")
            //    throw new AttendanceDayOffException(BeginDate);

            //DailyWS = empData.GetDailyWorkSchedule(EndDate);
            //MonthWS = MonthlyWS.GetCalendar(EmployeeID, EndDate.Year, EndDate.Month);
            //if (DailyWS.IsDayOff || MonthWS.GetWSCode(EndDate.Day, false) == "1")
            //    throw new AttendanceDayOffException(EndDate);


            AttendanceCreatingRule oACR = HRTMManagement.CreateInstance(CompanyCode).GetCreatingRule(empData, this.AttendanceType, this.BeginDate, this.EndDate);

            //AttendanceCreatingRule oACR = HRTMManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetCreatingRule(this.EmployeeID, this.AttendanceType, this.BeginDate, this.EndDate);

            #region Check requested absence or attendance is in process?
            DateTime tmpBeginDate = this.BeginDate.Add(this.BeginTime);
            DateTime tmpEndDate = this.EndDate.Add(this.EndTime);
            if (tmpEndDate < tmpBeginDate)
                tmpEndDate = tmpEndDate.AddDays(1);
            else if (this.AllDayFlag)
                tmpEndDate = tmpEndDate.AddDays(1).AddSeconds(-1);

            string ExistingDate = string.Empty;
            //List<INFOTYPE2001> listInfotype2001 =new INFOTYPE2001().GetData(empData, tmpBeginDate, tmpEndDate);
            List<INFOTYPE2001> listInfotype2001 = new INFOTYPE2001().GetData(empData, tmpBeginDate.Date, tmpEndDate.Date);
            if (listInfotype2001.Exists(delegate (INFOTYPE2001 item) { return item.RequestNo != this.RequestNo; }))
            {
                INFOTYPE2001 ExistingItem = listInfotype2001.Find(delegate (INFOTYPE2001 item) { return item.RequestNo != this.RequestNo; });
                if (ExistingItem.AllDayFlag)
                {
                    if (ExistingItem.BeginDate <= this.EndDate && ExistingItem.EndDate >= this.BeginDate)
                    {
                        //����¡�ë�ӡѺ
                        if (ExistingItem.BeginDate.Date == ExistingItem.EndDate.Date)//���ѹ����
                        {
                            ExistingDate = ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US"));
                        }
                        else//�������ѹ
                        {
                            ExistingDate = string.Format("{0} - {1}", ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")), ExistingItem.EndDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")));
                        }
                        throw new Exception("ABSENCE_IN_PROCESS|" + ExistingDate);
                    }
                }
                else
                {
                    DateTime ExistingBegin = ExistingItem.BeginDate.Add(ExistingItem.BeginTime);
                    DateTime ExistingEnd = ExistingItem.EndDate.Add(ExistingItem.EndTime);
                    if (ExistingEnd < ExistingBegin)
                        ExistingEnd = ExistingEnd.AddDays(1);

                    if (tmpBeginDate <= ExistingEnd && tmpEndDate >= ExistingBegin)   //Check Overlap 20171214 Koissares
                    {
                        //����¡�ë�ӡѺ
                        if (ExistingItem.BeginDate.Date == ExistingItem.EndDate.Date)//���ѹ����
                        {
                            ExistingDate = ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")) + " " + ExistingItem.BeginTime.ToString() + "-" + ExistingItem.EndTime.ToString(); ;
                        }
                        else//�������ѹ
                        {
                            ExistingDate = string.Format("{0} - {1}", ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")), ExistingItem.EndDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")));
                        }
                        throw new Exception("ABSENCE_IN_PROCESS|" + ExistingDate);
                    }
                }

            }

            //List<INFOTYPE2002> listInfotype2002 = GetData(empData, tmpBeginDate, tmpEndDate);
            List<INFOTYPE2002> listInfotype2002 = GetData(empData, tmpBeginDate.Date, tmpEndDate.Date);
            if (listInfotype2002.Exists(delegate (INFOTYPE2002 item) { return item.RequestNo != this.RequestNo; }))
            {
                INFOTYPE2002 ExistingItem = listInfotype2002.Find(delegate (INFOTYPE2002 item) { return item.RequestNo != this.RequestNo; });
                if (ExistingItem.AllDayFlag)
                {
                    if (ExistingItem.BeginDate <= this.EndDate && ExistingItem.EndDate >= this.BeginDate)
                    {
                        //����¡�ë�ӡѺ
                        if (ExistingItem.BeginDate.Date == ExistingItem.EndDate.Date)//�Ѻ�ͧ�����ѹ����
                        {
                            ExistingDate = ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US"));
                        }
                        else//�Ѻ�ͧ���������ѹ
                        {
                            ExistingDate = string.Format("{0} - {1}", ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")), ExistingItem.EndDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")));
                        }
                        throw new Exception("ATTENDANCE_IN_PROCESS|" + ExistingDate);
                    }
                }
                else
                {
                    DateTime ExistingBegin = ExistingItem.BeginDate.Add(ExistingItem.BeginTime);
                    DateTime ExistingEnd = ExistingItem.EndDate.Add(ExistingItem.EndTime);
                    if (ExistingEnd < ExistingBegin)
                        ExistingEnd = ExistingEnd.AddDays(1);

                    if (tmpBeginDate <= ExistingEnd && tmpEndDate >= ExistingBegin)   //Check Overlap 20171214 Koissares
                    {
                        //����¡�ë�ӡѺ
                        if (ExistingItem.BeginDate.Date == ExistingItem.EndDate.Date)//�Ѻ�ͧ�����ѹ����
                        {
                            ExistingDate = ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")) + " " + ExistingItem.BeginTime.ToString() + "-" + ExistingItem.EndTime.ToString();
                        }
                        else//�Ѻ�ͧ���������ѹ
                        {
                            ExistingDate = string.Format("{0} - {1}", ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")), ExistingItem.EndDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")));
                        }
                        throw new Exception("ATTENDANCE_IN_PROCESS|" + ExistingDate);
                    }
                }
            }
            #endregion

            //ModifiedBy: Ratchatawan W. (2012-03-21)
            if (!(WorkflowPrinciple.Current.IsInRole("TIMEADMIN") || WorkflowPrinciple.Current.IsInRole("#MANAGER")))
                oACR.IsCanCreate();
            else if (this.EmployeeID == WorkflowPrinciple.CurrentIdentity.EmployeeID)
                oACR.IsCanCreate();

            //if (!WorkflowPrinciple.Current.IsInRole("TIMEADMIN") && !WorkflowPrinciple.Current.IsInRole("#MANAGER"))
            //{
            //    oACR.IsCanCreate();
            //}

            #region " check Time "
            if (oACR.IsLoadClockIN)
            {
                if (this.AllDayFlag)
                {
                    throw new Exception("THIS_TYPE_CAN_NOT_USE_FULL_DAY");
                }
                else
                {
                    if (!oACR.CheckClock(DoorType.IN, this.BeginDate.Add(this.BeginTime)) && !oACR.CheckClock(DoorType.OUT, this.BeginDate.Add(this.BeginTime)))
                    {
                        throw new Exception("NOT_FOUND_EVIDANCE");
                    }
                }
            }
            if (oACR.IsLoadClockOUT)
            {
                if (this.AllDayFlag)
                {
                    throw new Exception("THIS_TYPE_CAN_NOT_USE_FULL_DAY");
                }
                else
                {
                    if (!oACR.CheckClock(DoorType.IN, this.EndDate.Add(this.EndTime)) && !oACR.CheckClock(DoorType.OUT, this.EndDate.Add(this.EndTime)))
                    {
                        throw new Exception("NOT_FOUND_EVIDANCE");
                    }
                }
            }
            if (oACR.IsSwapClock)
            {
                if (this.AllDayFlag)
                {
                    throw new Exception("THIS_TYPE_CAN_NOT_USE_FULL_DAY");
                }
                else
                {
                    if (oACR.CheckClock(DoorType.IN, this.BeginDate.Add(this.BeginTime)))
                    {
                        if (!oACR.CheckClock(DoorType.IN, this.EndDate.Add(this.EndTime)))
                        {
                            throw new Exception("NOT_FOUND_EVIDANCE");
                        }
                    }
                    else if (oACR.CheckClock(DoorType.OUT, this.BeginDate.Add(this.BeginTime)))
                    {
                        if (!oACR.CheckClock(DoorType.IN, this.EndDate.Add(this.EndTime)))
                        {
                            if (!oACR.CheckClock(DoorType.OUT, this.EndDate.Add(this.EndTime)))
                            {
                                throw new Exception("NOT_FOUND_EVIDANCE");
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("NOT_FOUND_EVIDANCE");
                    }
                }
            }
            #endregion

            AbsAttCalculateResult oResult;
            if (BeginDate != EndDate && !AllDayFlag)
            {
                this.AttendanceDays = 0.0M;
                this.AttendanceHours = 0.0M;
                this.PayrollDays = 0.0M;
                for (DateTime rundate = BeginDate; rundate <= EndDate; rundate = rundate.AddDays(1))
                {
                    if (rundate == EndDate)
                    {
                        oResult = ServiceManager.CreateInstance(CompanyCode).ERPData.CalculateAbsAttEntry(EmployeeID, SubType, rundate, rundate, BeginTime, EndTime, false, false, false);
                        this.EndTime = oResult.EndTime;
                        this.AttendanceDays += oResult.AbsenceDays;
                        this.AttendanceHours += oResult.AbsenceHours;
                        this.PayrollDays += oResult.PayrollDays;
                    }
                    else if (rundate == EndDate.AddDays(-1) && BeginTime >= EndTime)
                    {
                        oResult = ServiceManager.CreateInstance(CompanyCode).ERPData.CalculateAbsAttEntry(EmployeeID, SubType, rundate, rundate, BeginTime, EndTime, false, false, false);
                        this.EndTime = oResult.EndTime;
                        this.AttendanceDays += oResult.AbsenceDays;
                        this.AttendanceHours += oResult.AbsenceHours;
                        this.PayrollDays += oResult.PayrollDays;
                        break;
                    }
                    else
                    {
                        oResult = ServiceManager.CreateInstance(CompanyCode).ERPData.CalculateAbsAttEntry(EmployeeID, SubType, rundate, rundate.AddDays(1), BeginTime, BeginTime, true, false, false);
                        this.AttendanceDays += 1;
                        this.AttendanceHours += 24;
                        this.PayrollDays += 1;
                    }
                }
            }
            else
            {
                oResult = ServiceManager.CreateInstance(CompanyCode).ERPData.CalculateAbsAttEntry(EmployeeID, SubType, BeginDate, EndDate, BeginTime, EndTime, AllDayFlag, false, false);
                this.AttendanceDays = oResult.AbsenceDays;
                this.AttendanceHours = oResult.AbsenceHours;
                //this.AllDayFlag = oResult.FullDay;
                this.BeginTime = oResult.BeginTime;
                this.EndTime = oResult.EndTime;
                this.PayrollDays = oResult.PayrollDays;
            }
        }
        public string DateTimeText
        {
            get
            {
                string cReturn = "";
                if (this.BeginDate == this.EndDate)
                {
                    cReturn = this.BeginDate.ToString("dd/MM/yyyy");
                    if (!this.AllDayFlag)
                    {
                        cReturn += string.Format(" {0} - {1}", INFOTYPE2002.TimeString(this.BeginTime), INFOTYPE2002.TimeString(this.EndTime));
                    }
                }
                else
                {
                    cReturn = this.BeginDate.ToString("dd/MM/yyyy - ") + this.EndDate.ToString("dd/MM/yyyy");
                }
                return cReturn;
            }
        }
        public static string GenerateText(DateTime BeginDate, DateTime EndDate, TimeSpan BeginTime, TimeSpan EndTime, bool AllDayFlag)
        {
            string cReturn = "";
            if (BeginDate == EndDate)
            {
                cReturn = BeginDate.ToString("dd/MM/yyyy");
                if (!AllDayFlag)
                {
                    cReturn += string.Format(" {0} - {1}", INFOTYPE2002.TimeString(BeginTime), INFOTYPE2002.TimeString(EndTime));
                }
            }
            else
            {
                cReturn = BeginDate.ToString("dd/MM/yyyy - ") + EndDate.ToString("dd/MM/yyyy");
            }
            return cReturn;
        }
        private static string TimeString(TimeSpan data)
        {
            return string.Format("{0}:{1}", data.Hours.ToString("00"), data.Minutes.ToString("00"));
        }

        #region ICollisionable Members

        public List<TimesheetData> LoadCollision(string EmployeeID, DateTime BeginDate, DateTime EndDate, string SubKey1, string SubKey2)
        {
            List<INFOTYPE2002> list = ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).ERPData.GetAttendanceList(EmployeeID, BeginDate, EndDate);
            List<TimesheetData> oReturn = new List<TimesheetData>();
            foreach (INFOTYPE2002 item in list)
            {
                TimesheetData Data = new TimesheetData();
                Data.EmployeeID = item.EmployeeID;
                Data.ApplicationKey = "ATTENDANCE";
                if (item.AllDayFlag)
                {
                    Data.BeginDate = item.BeginDate;
                    Data.EndDate = item.EndDate;
                }
                else
                {
                    Data.BeginDate = item.BeginDate.Add(item.BeginTime);
                    Data.EndDate = item.EndDate.Add(item.EndTime);
                }
                Data.FullDay = item.AllDayFlag;
                Data.SubKey1 = item.AttendanceType;
                Data.SubKey2 = "";
                oReturn.Add(Data);
            }
            return oReturn;
        }

        #endregion

        public override string ToString()
        {
            INFOTYPE2002_Convertor oConverter = new INFOTYPE2002_Convertor();
            return (string)oConverter.ConvertTo(this, typeof(string));
        }

        public INFOTYPE2002 Parse(string value)
        {
            INFOTYPE2002_Convertor oConverter = new INFOTYPE2002_Convertor();
            return (INFOTYPE2002)oConverter.ConvertFrom(value);
        }

        public bool CheckExistingData(EmployeeData Requestor, INFOTYPE2002 oAttendance)
        {
            List<INFOTYPE2002> list = GetDataOnlyRecorded(Requestor, oAttendance.BeginDate, oAttendance.EndDate.AddDays(1));
            bool lFound = false;
            foreach (INFOTYPE2002 item in list)
            {
                if (item.ToString() == oAttendance.ToString())
                {
                    lFound = true;
                    break;
                }
            }
            return lFound;
        }
    }
}
