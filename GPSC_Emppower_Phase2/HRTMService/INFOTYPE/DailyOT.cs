using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Text;
using ESS.DATA;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.TIMESHEET;
using ESS.HR.TM.DATACLASS;
using ESS.WORKFLOW;

namespace ESS.HR.TM.INFOTYPE
{
    public class DailyOT : BaseOTData, ICollisionable
    {
        private static CultureInfo oCL = new CultureInfo("en-US");
        private DailyWS __dws = null;
        private PlannedOT __plannedOT = null;
        private INFOTYPE2002 __attendance = null;
        private TimePair __pair = null;
        private TimeEvidance __evidance = null;
        private int __OTItemTypeID = 0;
        private bool __isPlanned = false;
        private bool __isAttendance = false;
        private bool __isPrevDay = false;
        private bool __isTNOM = false;
        private string __rule = "";
        private string __assignFrom = "";
        private bool __isPost = false;
        private bool __isSummary = false;
        private bool __isCompleted = false;
        private string __requestNo = "";
        private string __ioNumber = "";
        private string __activityCode = "";
        private string __status = "";

        public DailyOT()
        {
        }

        #region " Properties "
        public string CompanyCode { get; set; }
        public string AssignFrom
        {
            get
            {
                return __assignFrom;
            }
            set
            {
                __assignFrom = value;
            }
        }

        public string Rule
        {
            get
            {
                return __rule;
            }
            set
            {
                __rule = value;
            }
        }

        public bool IsPrevDay
        {
            get
            {
                return __isPrevDay;
            }
            set
            {
                __isPrevDay = value;
            }
        }

        public bool IsPlanned
        {
            get
            {
                return __isPlanned;
            }
            set
            {
                __isPlanned = value;
            }
        }

        public bool IsAttendance
        {
            get
            {
                return __isAttendance;
            }
            set
            {
                __isAttendance = value;
            }
        }

        public bool IsTNOM
        {
            get
            {
                return __isTNOM;
            }
            set
            {
                __isTNOM = value;
            }
        }

        public bool IsPosted
        {
            get
            {
                return __isPost;
            }
            set
            {
                __isPost = value;
            }
        }

        public bool IsSummary
        {
            get
            {
                return __isSummary;
            }
            set
            {
                __isSummary = value;
            }
        }

        public bool IsCompleted
        {
            get
            {
                return __isCompleted;
            }
            set
            {
                __isCompleted = value;
            }
        }

        public int OTItemTypeID
        {
            get
            {
                return __OTItemTypeID;
            }
            set
            {
                __OTItemTypeID = value;
            }
        }

        public string RequestNo
        {
            get
            {
                return __requestNo;
            }
            set
            {
                __requestNo = value;
            }
        }

        public string IONumber
        {
            get
            {
                return __ioNumber;
            }
            set
            {
                __ioNumber = value;
            }
        }

        public string ActivityCode
        {
            get
            {
                return __activityCode;
            }
            set
            {
                __activityCode = value;
            }
        }

        public DailyWS DWS
        {
            get
            {
                return __dws;
            }
        }

        public PlannedOT PlannedOT
        {
            get
            {
                return __plannedOT;
            }
        }

        public TimeEvidance TimeEvidance
        {
            get
            {
                return __evidance;
            }
        }

        public string Status
        {
            get
            {
                return __status;
            }
            set
            {
                __status = value;
            }
        }
        #endregion

        #region " Calculate "
        public void Calculate(MonthlyWS MWS)
        {
            Calculate(MWS, false);
        }
        
        public void FillWorkSchedule()
        {
            Decimal otHours = this.OTHours;
            MonthlyWS mws = MonthlyWS.GetCalendar(this.EmployeeID, this.OTDate.Year, this.OTDate.Month);
            Calculate(mws, false);
            this.OTHours = otHours;
        }

        public void Calculate(MonthlyWS MWS, bool isCalculateOnly)
        {
            EmployeeData oEmp = new EmployeeData(this.EmployeeID, this.OTDate);
            string DWSCode = MWS.GetWSCode(this.OTDate.Day, true);

            __dws = EmployeeManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetDailyWorkschedule(oEmp.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, DWSCode, this.OTDate);

            // �Ѵ Plan time
            TimeSpan maxEndTime;

            HRTMManagement.CreateInstance(CompanyCode).CalculatePlannedOTHours(this.EmployeeID, this.OTDate, MWS, this.DWS, ref __beginTime, ref __endTime);
            string valuationClass = MWS.GetValuationClassExactly(this.OTDate.Day);
            __isTNOM = HRTMManagement.CreateInstance(CompanyCode).Is_TNOM(valuationClass);
            if (__endTime <= __beginTime)
            {
                maxEndTime = __endTime.Add(new TimeSpan(1, 0, 0, 0));
            }
            else
            {
                maxEndTime = __endTime;
            }

            if (__beginTime != TimeSpan.MinValue && __endTime != TimeSpan.MinValue)
            {
                DateTime date1, date2;
                TimeSpan ts1, ts2, ts2_1;
                DateTime mydate1, mydate2;
                date1 = this.OTDate.Add(__beginTime);
                date2 = this.OTDate.Add(__endTime);
                if (date2 <= date1)
                {
                    date2 = date2.AddDays(1);
                }
                __plannedOT = HRTMManagement.CreateInstance(CompanyCode).LoadFirstPlannedOT(this.EmployeeID, date1, date2);
                if (__plannedOT != null && !isCalculateOnly)
                {
                    ts1 = __plannedOT.BeginTime;
                    ts2 = __plannedOT.EndTime;
                    if (ts2 <= ts1)
                    {
                        ts2 = ts2.Add(new TimeSpan(1, 0, 0, 0));
                    }
                    if (__beginTime < ts1)
                    {
                        if (__endTime >= ts1)
                        {
                            __plannedOT = null;
                        }
                    }
                    else if (__beginTime < ts2)
                    {
                        if (__endTime > ts2)
                        {
                            __endTime = ts2;
                        }
                    }
                    else
                    {
                        __beginTime = ts2;
                    }
                }
                date1 = this.OTDate.Add(__beginTime);
                date2 = this.OTDate.Add(__endTime);
                if (date2 <= date1)
                {
                    date2 = date2.AddDays(1);
                }

                bool isSkipEvidance = false;
                List<INFOTYPE0007> inf0007List = oEmp.GetWorkSchedule(MWS.WS_Year, MWS.WS_Month);
                foreach (INFOTYPE0007 item0007 in inf0007List)
                {
                    if (item0007.BeginDate.Date <= OTDate && item0007.EndDate.Date >= OTDate)
                    {
                        if (item0007.TimeEvaluateClass.Trim() == "9")
                        {
                            isSkipEvidance = true;
                            break;
                        }
                    }
                }
                if (!isSkipEvidance)
                {
                    bool lKillFromAtten = false;
                    TimeSpan __saveOut = TimeSpan.MinValue;
                    List<INFOTYPE2002> attendanceList = new INFOTYPE2002().GetData(oEmp, this.OTDate.AddDays(-1), this.OTDate, true);
                    List<string> filterSubType = new List<string>();
                    filterSubType.Add("5000");
                    filterSubType.Add("5500");
                    filterSubType.Add("5501");
                    if (attendanceList.Count > 0)
                    {
                        foreach (INFOTYPE2002 item in attendanceList)
                        {
                            if (filterSubType.Contains(item.SubType))
                            {
                                continue;
                            }
                            mydate1 = item.BeginDate.Add(item.BeginTime);
                            mydate2 = item.EndDate.Add(item.EndTime);
                            if (mydate2 <= mydate1)
                            {
                                mydate2 = mydate2.AddDays(1);
                            }
                            if (mydate1 < date2 && mydate2 > date1)
                            {
                                ts1 = mydate1.Subtract(this.OTDate);
                                ts2 = mydate2.Subtract(this.OTDate);
                                __evidance = new TimeEvidance(ts1, ts2);
                                __attendance = item;
                                if (!isCalculateOnly)
                                {
                                    if (ts2 <= ts1)
                                    {
                                        ts2 = ts2.Add(new TimeSpan(1, 0, 0, 0));
                                    }
                                    if (__beginTime < ts1)
                                    {
                                        if (__endTime > ts1)
                                        {
                                            __saveOut = __endTime;
                                            __endTime = ts1;
                                            __evidance = null;
                                            __attendance = null;
                                            lKillFromAtten = true;
                                            break;
                                        }
                                    }
                                    if (__beginTime < ts2)
                                    {
                                        if (__endTime > ts2)
                                        {
                                            __endTime = ts2;
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        __beginTime = ts2;
                                    }
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                    }
                    if (TimeEvidance == null)
                    {
                        List<TimePair> pairList = HRTMManagement.CreateInstance(CompanyCode).LoadArchiveTimepair(this.EmployeeID, this.OTDate.AddDays(-1), this.OTDate.AddDays(1));
                        pairList.Sort(new TimePairSortingDate());
                        foreach (TimePair item in pairList)
                        {
                            if (item.ClockIN is null && item.ClockOUT is null)
                            {
                                mydate1 = item.ClockIN.EventTime.Subtract(new TimeSpan(0, 0, item.ClockIN.EventTime.Second));
                                mydate2 = item.ClockOUT.EventTime.Subtract(new TimeSpan(0, 0, item.ClockOUT.EventTime.Second));
                                if (mydate1 < date2 && mydate2 > date1)
                                {
                                    __pair = item;
                                    //if (lKillFromAtten)
                                    //{
                                    //    __endTime = __saveOut;
                                    //}
                                    ts1 = mydate1.TimeOfDay;
                                    ts2 = mydate2.TimeOfDay;
                                    ts2_1 = mydate2.TimeOfDay;
                                    __evidance = new TimeEvidance(ts1, ts2);
                                    if (!isCalculateOnly)
                                    {
                                        if (ts2 <= ts1)
                                        {
                                            ts2 = ts2.Add(new TimeSpan(1, 0, 0, 0));
                                        }
                                        if (__beginTime < ts1)
                                        {
                                            if (__endTime > ts1)
                                            {
                                                __endTime = ts1;
                                                __evidance = null;
                                            }
                                            else
                                            {
                                                if (__endTime > ts2_1)
                                                {
                                                    __endTime = ts2_1;
                                                }
                                            }
                                        }
                                        else if (__beginTime < ts2)
                                        {
                                            if (__endTime > ts2)
                                            {
                                                __endTime = ts2;
                                            }
                                        }
                                        else
                                        {
                                            __beginTime = ts2;
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
                if (__endTime > maxEndTime)
                {
                    __endTime = maxEndTime;
                }

                #region " Old Code "
                //TimeSpan CutoffTime = DailyWS.CutoffTime(MWS.ValuationClass);
                //TimeSpan refCutoffTime;
                //if (EndTime <= BeginTime || EndTime.Days > 0)
                //{
                //    refCutoffTime = CutoffTime.Add(new TimeSpan(1, 0, 0, 0));
                //    if (EndTime <= BeginTime && EndTime.Add(new TimeSpan(1, 0, 0, 0)) > refCutoffTime && refCutoffTime > BeginTime)
                //    {
                //        // is yesterday
                //        __endTime = refCutoffTime;
                //        //__isPrevDay = true;
                //    }
                //    else if (EndTime > BeginTime && EndTime > refCutoffTime && refCutoffTime > BeginTime)
                //    {
                //        __endTime = refCutoffTime;
                //        //__isPrevDay = true;
                //    }
                //    //else if (BeginTime > refCutoffTime)
                //    //{
                //    //    // is today
                //    //    if (__dws.IsDayOff)
                //    //    {
                //    //        if (DailyWS.IsNormOrTNorm(MWS.ValuationClass))
                //    //        {
                //    //            if (BeginTime >= OTManagement.NormalBeginTime_NORM)
                //    //            {
                //    //                this.Rule = "R2";
                //    //            }
                //    //            else
                //    //            {
                //    //                this.Rule = "R3";
                //    //            }
                //    //        }
                //    //    }
                //    //}
                //}
                //else if (EndTime > CutoffTime && CutoffTime > BeginTime)
                //{
                //    __endTime = CutoffTime;
                //    //__isPrevDay = true;
                //}
                #endregion

                if (TimeEvidance == null && !isSkipEvidance)
                {
                    this.OTHours = 0.0M;
                }
                else
                {
                    int nBreak = 0;
                    if (__dws.IsDayOff)
                    {
                        if (HRTMManagement.CreateInstance(CompanyCode).IsNORM(MWS.GetValuationClassExactly(OTDate.Day)))
                        {
                            TimeSpan time1, time2;
                            time1 = new TimeSpan(12, 0, 0);
                            time2 = new TimeSpan(13, 0, 0);
                            if (time1 < EndTime && time2 > BeginTime)
                            {
                                if (BeginTime > time1)
                                {
                                    time1 = BeginTime;
                                }
                                if (EndTime < time2)
                                {
                                    time2 = EndTime;
                                }
                                nBreak = (int)time2.Subtract(time1).TotalMinutes;
                            }
                        }
                    }
                    Decimal nHours = (decimal)((EndTime.Subtract(BeginTime).TotalMinutes - nBreak) / 60); // -(this.IsTNOM ? 9 : 0);
                    this.OTHours = nHours < 0 ? 0.0M : nHours;
                }
            }

            this.IsPlanned = this.PlannedOT != null;
            this.IsAttendance = __pair is null;
        }
        #endregion

        #region ICollisionable Members

        public List<TimesheetData> LoadCollision(string EmployeeID, DateTime BeginDate, DateTime EndDate, string SubKey1, string SubKey2)
        {
            List<TimesheetData> oReturn = new List<TimesheetData>();
            //List<SaveOTData> list = HR.TM.ServiceManager.HRTMBuffer.LoadOTData(EmployeeID, BeginDate, EndDate, "D", false);

            List<SaveOTData> list = ServiceManager.CreateInstance(CompanyCode).ESSData.LoadOTData(EmployeeID, BeginDate, EndDate, "D", false);
            foreach (SaveOTData item in list)
            {
                TimesheetData Data = new TimesheetData();
                Data.EmployeeID = item.EmployeeID;
                Data.ApplicationKey = "DAILYOT";
                Data.BeginDate = item.OTDate.Add(item.BeginTime);
                Data.EndDate = item.OTDate.Add(item.EndTime);
                if (Data.EndDate <= Data.BeginDate)
                {
                    Data.EndDate = Data.EndDate.AddDays(1);
                }
                Data.FullDay = false;
                Data.SubKey1 = "";
                Data.SubKey2 = "";
                Data.Detail = item.RequestNo;
                if (Data.BeginDate < EndDate && Data.EndDate > BeginDate)
                {
                    oReturn.Add(Data);
                }
            }
            //list = HR.TM.ServiceManager.HRTMService.LoadOTData(EmployeeID, BeginDate, EndDate, "D", false);

            list = ServiceManager.CreateInstance(CompanyCode).ERPData.LoadOTData(EmployeeID, BeginDate, EndDate, "D", false);
            foreach (SaveOTData item in list)
            {
                TimesheetData Data = new TimesheetData();
                Data.EmployeeID = item.EmployeeID;
                Data.ApplicationKey = "DAILYOT";
                Data.BeginDate = item.OTDate.Add(item.BeginTime);
                Data.EndDate = item.OTDate.Add(item.EndTime);
                if (Data.EndDate <= Data.BeginDate)
                {
                    Data.EndDate = Data.EndDate.AddDays(1);
                }
                Data.FullDay = false;
                Data.SubKey1 = "";
                Data.SubKey2 = "";
                Data.Detail = item.RequestNo;
                if (Data.BeginDate < EndDate && Data.EndDate > BeginDate)
                {
                    oReturn.Add(Data);
                }
            }
            return oReturn;
        }

        #endregion

        public override string ToString()
        {
            return string.Format("{0}|{1}|{2}|{3}", this.EmployeeID, this.OTDate.ToString("yyyyMMdd", oCL), this.TimeSpanToString(this.BeginTime), this.TimeSpanToString(this.EndTime));
        }

        public DailyOT Parse(string input)
        {
            DailyOT oReturn1 = null;
            DailyOT oReturn = null;
            Regex oRegex = new Regex(@"(?<EmployeeID>\w+)\|(?<OTDate>(\d+){8})\|(?<BeginTime>(\d+){2}:(\d+){2})\|(?<EndTime>(\d+){2}:(\d+){2})");
            if (oRegex.IsMatch(input))
            {
                Match oMatch = oRegex.Match(input);
                oReturn = new DailyOT();
                oReturn.EmployeeID = oMatch.Groups["EmployeeID"].Value;
                oReturn.OTDate = DateTime.ParseExact(oMatch.Groups["OTDate"].Value, "yyyyMMdd", oCL);
                oReturn.BeginTime = StringToTimeSpan(oMatch.Groups["BeginTime"].Value);
                oReturn.EndTime = StringToTimeSpan(oMatch.Groups["EndTime"].Value);
                MonthlyWS MWS = MonthlyWS.GetCalendar(oReturn.EmployeeID, oReturn.OTDate.Year, oReturn.OTDate.Month);
                //List<DailyOT> list = OTManagement.LoadDailyOT(oReturn.EmployeeID, MWS, oReturn.OTDate);

                List<DailyOT> list = HRTMManagement.CreateInstance(CompanyCode).LoadDailyOT(oReturn.EmployeeID, MWS, oReturn.OTDate);
                foreach (DailyOT item in list)
                {
                    if (item.ToString() == input)
                    {
                        oReturn1 = item;
                        break;
                    }
                    //if (item.EmployeeID == oReturn.EmployeeID && item.OTDate == oReturn.OTDate && item.BeginTime == oReturn.BeginTime && item.EndTime == oReturn.EndTime)
                    //{
                    //    oReturn1 = item;
                    //    break;
                    //}
                }
                if (oReturn1 == null)
                {
                    throw new Exception("NOT_FOUND_DATA");
                }
            }
            else
            {
                throw new FormatException();
            }
            return oReturn1;
        }

        private string TimeSpanToString(TimeSpan input)
        {
            DateTime oDate = DateTime.Now.Date.Add(input);
            return oDate.ToString("HH:mm", oCL);
        }
        private static TimeSpan StringToTimeSpan(string input)
        {
            TimeSpan oReturn = TimeSpan.Parse(input);
            return oReturn;
        }

        public bool CheckExistingData(EmployeeData Requestor, DailyOT daily)
        {
            MonthlyWS MWS = MonthlyWS.GetCalendar(Requestor.EmployeeID, daily.OTDate.Year, daily.OTDate.Month);
            List<DailyOT> list = HRTMManagement.CreateInstance(CompanyCode).LoadDailyOT(Requestor.EmployeeID, MWS, daily.OTDate);
            bool lFound = false;
            foreach (DailyOT item in list)
            {
                if (item.ToString() == daily.ToString())
                {
                    lFound = true;
                    break;
                }
            }
            return lFound;

        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() == this.GetType())
            {
                return obj.ToString() == this.ToString();
            }
            else
            {
                return false;
            }
        }
    }

}
