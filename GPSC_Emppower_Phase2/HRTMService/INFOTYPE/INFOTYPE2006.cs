using System;
using System.Collections.Generic;
using System.Text;
using ESS.DATA;
using ESS.EMPLOYEE;
using ESS.HR.TM;
using ESS.HR.TM.CONFIG;
using ESS.EMPLOYEE.ABSTRACT;
using ESS.WORKFLOW;

namespace ESS.HR.TM.INFOTYPE
{
    public class INFOTYPE2006:AbstractInfoType
    {
        private DateTime __deductionStart;
        private DateTime __deductionEnd;
        private Decimal __quotaAmount;
        private Decimal __usedAmount;
        private Decimal __inprocessAmont = 0.0M;
        private bool __isShowDashboard = false;
        private string __defaultAbsenceType = "";

        public INFOTYPE2006()
        { 
        }

        public string CompanyCode { get; set; }
        public override string InfoType
        {
            get { return "2006"; }
        }

        public string AbsenceType
        {
            get
            {
                return this.SubType;
            }
            set
            {
                this.SubType = value;
            }
        }

        public DateTime DeductionStart
        {
            get
            {
                return __deductionStart;
            }
            set
            {
                __deductionStart = value;
            }
        }

        public DateTime DeductionEnd
        {
            get
            {
                return __deductionEnd;
            }
            set
            {
                __deductionEnd = value;
            }
        }

        public Decimal QuotaAmount
        {
            get
            {
                return __quotaAmount;
            }
            set
            {
                __quotaAmount = value;
            }
        }

        public Decimal UsedAmount
        {
            get
            {
                return __usedAmount;
            }
            set
            {
                __usedAmount = value;
            }
        }

        public Decimal InprocessAmount
        {
            get
            {
                return __inprocessAmont;
            }
        }

        public Decimal Remaining
        {
            get
            {
                return QuotaAmount - UsedAmount - InprocessAmount;
            }
        }

        public bool IsShowDashboard
        {
            get
            {
                return __isShowDashboard;
            }
            set
            {
                __isShowDashboard = value;
            }
        }
        public string DefaultAbsenceType
        {
            get
            {
                return __defaultAbsenceType;
            }
            set
            {
                __defaultAbsenceType = value;
            }
        }

        public static INFOTYPE2006_Comparer Comparer
        {
            get
            {
                return new INFOTYPE2006_Comparer();
            }
        }

        public static Dictionary<string, List<INFOTYPE2006>> GenerateDictionary(List<INFOTYPE2006> list)
        {
            List<INFOTYPE2006> buffer;
            Dictionary<string, List<INFOTYPE2006>> dict = new Dictionary<string, List<INFOTYPE2006>>();
            foreach (INFOTYPE2006 quota in list)
            {
                if (!dict.ContainsKey(quota.AbsenceType))
                {
                    buffer = new List<INFOTYPE2006>();
                    dict.Add(quota.AbsenceType, buffer);
                }
                else
                {
                    buffer = dict[quota.AbsenceType];
                }
                buffer.Add(quota);
            }
            foreach (string cKey in dict.Keys)
            {
                buffer = dict[cKey];
                buffer.Sort(new INFOTYPE2006_Comparer(true));
            }
            return dict;
        }

        public int TryDeductQuota(EmployeeData employeeData, Dictionary<string, List<INFOTYPE2006>> dict, Dictionary<string, DeductionRule> deductRule, INFOTYPE2001 item, bool isCheck)
        {
            List<INFOTYPE2006> buffer = null;
            DeductionRule rule;
            if (!deductRule.ContainsKey(item.AbsenceType))
            {
                rule = HRTMManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetDeductionRule(employeeData, item.AbsenceType);
                deductRule.Add(item.AbsenceType, rule);
            }
            else
            {
                rule = deductRule[item.AbsenceType];
            }
            bool lDeduct = false;
            Decimal myValue = item.PayrollDays;
            if (dict.ContainsKey(rule.Quota1))
            {
                buffer = dict[rule.Quota1];
                foreach (INFOTYPE2006 quota in buffer)
                {
                    if (item.EndDate >= quota.DeductionStart && item.BeginDate <= quota.DeductionEnd)
                    {
                        if (quota.Remaining < myValue)
                        {
                            myValue -= quota.Remaining;
                            if (!isCheck)
                            {
                                quota.__inprocessAmont += Math.Floor(quota.Remaining);
                            }
                        }
                        else
                        {
                            if (!isCheck)
                            {
                                quota.__inprocessAmont += myValue;
                            }
                            lDeduct = true;
                            break;
                        }
                    }
                }
            }
            if (!lDeduct && dict.ContainsKey(rule.Quota2))
            {
                buffer = dict[rule.Quota2];
                foreach (INFOTYPE2006 quota in buffer)
                {
                    if (item.EndDate >= quota.DeductionStart && item.BeginDate <= quota.DeductionEnd)
                    {
                        if (quota.Remaining < myValue)
                        {
                            myValue -= quota.Remaining;
                            if (!isCheck)
                            {
                                quota.__inprocessAmont += Math.Floor(quota.Remaining);
                            }
                        }
                        else
                        {
                            if (!isCheck)
                            {
                                quota.__inprocessAmont += myValue;
                            } 
                            lDeduct = true;
                            break;
                        }
                    }
                }
            }
            if (!lDeduct && dict.ContainsKey(rule.Quota3))
            {
                buffer = dict[rule.Quota3];
                foreach (INFOTYPE2006 quota in buffer)
                {
                    if (item.EndDate >= quota.DeductionStart && item.BeginDate <= quota.DeductionEnd)
                    {
                        if (quota.Remaining < myValue)
                        {
                            myValue -= quota.Remaining;
                            if (!isCheck)
                            {
                                quota.__inprocessAmont += Math.Floor(quota.Remaining);
                            }
                        }
                        else
                        {
                            if (!isCheck)
                            {
                                quota.__inprocessAmont += myValue;
                            }
                            lDeduct = true;
                            break;
                        }
                    }
                }
            }
            if (isCheck && !lDeduct)
            {
                throw new Exception("NOT_ENOUGH_QUOTA");
            }
            return 0;
        }

        public List<INFOTYPE2006> GetData(EmployeeData employeeData)
        { 
            return GetData(employeeData, "");
        }
        
        public List<INFOTYPE2006> GetData(EmployeeData employeeData, string RequestNo)
        {
            List<INFOTYPE2006> oReturn =  HRTMManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetAbsenceQuota(employeeData.EmployeeID);
            Dictionary<string, List<INFOTYPE2006>> dict = GenerateDictionary(oReturn);
            Dictionary<string, DeductionRule> deductRule = new Dictionary<string, DeductionRule>();

            List<INFOTYPE2001> list;
            list = HRTMManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetAbsenceList(employeeData.EmployeeID, employeeData.EmployeeID, new DateTime(1900, 1, 1), new DateTime(9999, 12, 31));
            foreach (INFOTYPE2001 item in list)
            {
                if (item.RequestNo == RequestNo)
                    continue;
                TryDeductQuota(employeeData, dict, deductRule, item, false);
            }

            foreach (INFOTYPE2006 oINF2006 in oReturn)
            {
               AbsenceQuotaType oAbsenceQuotaType = HRTMManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetAbsenceQuotaType(oINF2006.AbsenceType);
               if (oAbsenceQuotaType != null)
               {
                    oINF2006.IsShowDashboard = oAbsenceQuotaType.IsShowDashboard;
                    oINF2006.DefaultAbsenceType = oAbsenceQuotaType.DefaultAbsenceType;
               }
            }

            return oReturn;
        }

        public decimal CalculateMustUseQuota(EmployeeData Requestor, decimal quotaReceive)
        {
            int nextYearQuota = HRTMManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetNextYearAbsenceQuota(Requestor.EmployeeID);
            // Commulative of quota not more than 2 times of nextyearQuota
            return quotaReceive - nextYearQuota;
        }
    }
}
