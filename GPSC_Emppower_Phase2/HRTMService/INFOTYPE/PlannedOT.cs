using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Text;
using ESS.DATA;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.TIMESHEET;
using ESS.HR.TM.DATACLASS;

namespace ESS.HR.TM.INFOTYPE
{
    public class PlannedOT : BaseOTData, ICollisionable
    {
        private DailyWS __dws = null;
        private CultureInfo oCL = new CultureInfo("en-US");

        public PlannedOT()
        { 
        }

        public string CompanyCode { get; set; }
        public DailyWS DWS
        {
            get
            {
                return __dws;
            }
        }

        public void Calculate(MonthlyWS MWS)
        {
            EmployeeData oEmp = new EmployeeData(this.EmployeeID, this.OTDate);
            string DWSCode = MWS.GetWSCode(this.OTDate.Day, true);

            __dws = EmployeeManagement.CreateInstance(CompanyCode).GetDailyWorkschedule(oEmp.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, DWSCode, this.OTDate);

            this.OTHours = HRTMManagement.CreateInstance(CompanyCode).CalculatePlannedOTHours(this.EmployeeID, this.OTDate, MWS, this.DWS, ref __beginTime, ref __endTime);
        }

        public override bool Equals(object obj)
        {
            return this.GetHashCode() == obj.GetHashCode();
        }

        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("PLANNEDOT_{0}{1}{2}", this.OTDate.ToString("yyyyMMdd"), TimeSpanToString(this.BeginTime), TimeSpanToString(this.EndTime));
        }

        private string TimeSpanToString(TimeSpan input)
        {
            DateTime data = DateTime.Now.Date.Add(input);
            return data.ToString("HH:mm",oCL);
        }

        #region ICollisionable Members

        public List<TimesheetData> LoadCollision(string EmployeeID, DateTime BeginDate, DateTime EndDate, string SubKey1, string SubKey2)
        {
            List<TimesheetData> oReturn = new List<TimesheetData>();
            //List<SaveOTData> list = HR.TM.ServiceManager.HRTMBuffer.LoadOTData(EmployeeID, BeginDate.Date, EndDate.Date.AddDays(1), "P", false);

            List<SaveOTData> list = ServiceManager.CreateInstance(CompanyCode).ESSData.LoadOTData(EmployeeID, BeginDate.Date, EndDate.Date.AddDays(1), "P", false);
            foreach (SaveOTData item in list)
            {
                DateTime date1, date2;
                date1 = item.OTDate.Add(item.BeginTime);
                date2 = item.OTDate.Add(item.EndTime);
                if (date2 < date1)
                {
                    date2 = date2.AddDays(1);
                }
                if (date1 <= EndDate && date2 >= BeginDate)
                {
                    TimesheetData Data = new TimesheetData();
                    Data.EmployeeID = item.EmployeeID;
                    Data.ApplicationKey = "PLANNEDOT";
                    Data.BeginDate = item.OTDate.Add(item.BeginTime);
                    Data.EndDate = item.OTDate.Add(item.EndTime);
                    if (Data.EndDate <= Data.BeginDate)
                    {
                        Data.EndDate = Data.EndDate.AddDays(1);
                    }
                    Data.FullDay = false;
                    Data.SubKey1 = "";
                    Data.SubKey2 = "";
                    Data.Detail = item.RequestNo;
                    oReturn.Add(Data);
                }
            }
            return oReturn;
        }

        #endregion
    }
}
