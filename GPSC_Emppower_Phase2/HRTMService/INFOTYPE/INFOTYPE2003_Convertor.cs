using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace ESS.HR.TM.INFOTYPE
{
    public class INFOTYPE2003_Convertor : TypeConverter
    {
        private CultureInfo oCL = new CultureInfo("en-US");

        public INFOTYPE2003_Convertor()
        {
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == typeof(string))
            {
                INFOTYPE2003 myValue = (INFOTYPE2003)value;

                return string.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}", myValue.EmployeeID, myValue.BeginDate.ToString("yyyyMMdd", oCL), myValue.EndDate.ToString("yyyyMMdd", oCL), myValue.Substitute,myValue.RequestNo,myValue.SubstituteBeginDate,myValue.EmpDWSCodeOld,myValue.EmpDWSCodeNew,myValue.SubDWSCodeOld,myValue.SubDWSCodeNew);

            }
            return base.ConvertTo(context, culture, value, destinationType);
        }

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
            {
                return true;
            }
            return base.CanConvertFrom(context, sourceType);
        }

        
    }
}
