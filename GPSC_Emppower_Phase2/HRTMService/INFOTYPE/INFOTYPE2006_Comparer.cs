using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.HR.TM.INFOTYPE
{
    public class INFOTYPE2006_Comparer:IComparer<INFOTYPE2006>
    {
        private bool __isUseForDeduction = false;
        public INFOTYPE2006_Comparer()
        { 
        }
        public INFOTYPE2006_Comparer(bool isUseForDeduction)
        {
            __isUseForDeduction = isUseForDeduction;
        }

        #region IComparer<INFOTYPE2006> Members

        public int Compare(INFOTYPE2006 x, INFOTYPE2006 y)
        {
            if (__isUseForDeduction)
            {
                int cp1 = x.AbsenceType.CompareTo(y.AbsenceType);
                if (cp1 == 0)
                {
                    cp1 = x.DeductionStart.CompareTo(y.DeductionStart);
                    if (cp1 == 0)
                    {
                        cp1 = x.DeductionEnd.CompareTo(y.DeductionEnd);
                    }
                    cp1 = -cp1;
                }
                return cp1;
            }
            else
            {
                int cp1 = x.AbsenceType.CompareTo(y.AbsenceType);
                if (cp1 == 0)
                {
                    cp1 = x.DeductionStart.CompareTo(y.DeductionStart);
                }
                return cp1;
            }
        }

        #endregion
    }
}
