using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Text;
using ESS.DATA;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.CONFIG;
using ESS.TIMESHEET;
using ESS.HR.TM.DATACLASS;
using System.Data;

namespace ESS.HR.TM.INFOTYPE
{
    [Serializable]
    public class DailyOTLog:BaseOTLog
    {
        private List<TimeEval> _OTimeEval;
        private String _ClockTimePairs;
        private String _FinalTimePairs;
        private String _EvaTimePairs;
        private String _Remark;
        private List<OTClock> _OTClockINOUT;
        private string _RefRequestNo;
        private string _Description;
        private string _RequestNo;
        private static CultureInfo oCL = new CultureInfo("en-US");
                // Fields...
        private bool _IsPayroll;
        private decimal _FinalOTHour30;
        private decimal _FinalOTHour15;
        private decimal _FinalOTHour10;
        private decimal _EvaOTHour30;
        private decimal _EvaOTHour15;
        private decimal _EvaOTHour10;
        private byte _Status;
        private string _OTWorkTypeID;
        private decimal _RequestOTHour30;
        private decimal _RequestOTHour15;
        private decimal _RequestOTHour10;
        private DateTime _EndDate;
        private DateTime _BeginDate;
        private string _EmployeeID;
        private string _Concat;
        private int _ApproveRecord;
        private int _PayrollRecord;
        private bool _DayOff;
        private bool _OverLimit;
        private string _TOTALAMOUNT;
        private bool _IsView;
        private DateTime _ClockIn;
        private DateTime _ClockOut;
        private string _ColorIn;
        private string _ColorOut;
        private string _DefaultIsPayroll;
        private string _Type;

        #region "Public Properties"
        public string TOTALAMOUNT
        {
            get { return _TOTALAMOUNT; }
            set { _TOTALAMOUNT = value; }
        }
        public bool OverLimit
        {
            get { return _OverLimit; }
            set { _OverLimit = value; }
        }
        public bool DayOff
        {
            get { return _DayOff; }
            set { _DayOff = value; }
        }

        public string Concat
        {
            get { return _Concat; }
            set { _Concat = value; }
        }
        public int ApproveRecord
        {
            get { return _ApproveRecord; }
            set { _ApproveRecord = value; }
        }
        public int PayrollRecord
        {
            get { return _PayrollRecord; }
            set { _PayrollRecord = value; }
        }

        public List<TimeEval> oTimeEval
        {
            get { return _OTimeEval; }
            set
            {
                _OTimeEval = value;
            }
        }
        
        public String ClockTimePairs
        {
            get { return _ClockTimePairs; }
            set
            {
                _ClockTimePairs = value;
            }
        }
        
        public String FinalTimePairs
        {
            get { return _FinalTimePairs; }
            set
            {
                _FinalTimePairs = value;
            }
        }
        
        public String EvaTimePairs
        {
            get { return _EvaTimePairs; }
            set
            {
                _EvaTimePairs = value;
            }
        }
        
        public String Remark
        {
            get { return _Remark; }
            set
            {
                _Remark = value;
            }
        }
        
        public List<OTClock> OTClockINOUT
        {
            get { return _OTClockINOUT; }
            set
            {
                _OTClockINOUT = value;
            }
        }
        
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set
            {
                _EmployeeID = value;
            }
        }

        public DateTime BeginDate
        {
            get { return _BeginDate; }
            set
            {
                _BeginDate = value;
            }
        }

        public DateTime EndDate
        {
            get { return _EndDate; }
            set
            {
                _EndDate = value;
            }
        }

        public decimal RequestOTHour10
        {
            get { return _RequestOTHour10; }
            set
            {
                _RequestOTHour10 = value;
            }
        }

        public decimal RequestOTHour15
        {
            get { return _RequestOTHour15; }
            set
            {
                _RequestOTHour15 = value;
            }
        }

        public decimal RequestOTHour30
        {
            get { return _RequestOTHour30; }
            set
            {
                _RequestOTHour30 = value;
            }
        }

        public byte Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
            }
        }

        public decimal EvaOTHour10
        {
            get { return _EvaOTHour10; }
            set
            {
                _EvaOTHour10 = value;
            }
        }

        public decimal EvaOTHour15
        {
            get { return _EvaOTHour15; }
            set
            {
                _EvaOTHour15 = value;
            }
        }

        public decimal EvaOTHour30
        {
            get { return _EvaOTHour30; }
            set
            {
                _EvaOTHour30 = value;
            }
        }

        public decimal FinalOTHour10
        {
            get { return _FinalOTHour10; }
            set
            {
                _FinalOTHour10 = value;
            }
        }

        public decimal FinalOTHour15
        {
            get { return _FinalOTHour15; }
            set
            {
                _FinalOTHour15 = value;
            }
        }

        public decimal FinalOTHour30
        {
            get { return _FinalOTHour30; }
            set
            {
                _FinalOTHour30 = value;
            }
        }

        public bool IsPayroll
        {
            get { return _IsPayroll; }
            set
            {
                _IsPayroll = value;
            }
        }

        public string RequestNo
        {
            get { return _RequestNo; }
            set
            {
                _RequestNo = value;
            }
        }

        public string Description
        {
            get { return _Description; }
            set
            {
                _Description = value;
            }
        }

        public string RefRequestNo
        {
            get { return _RefRequestNo; }
            set
            {
                _RefRequestNo = value;
            }
        }

        public string OTWorkTypeID
        {
            get { return _OTWorkTypeID; }
            set
            {
                _OTWorkTypeID = value;
            }
        }
        public bool IsView
        {
            get { return _IsView; }
            set { _IsView = value; }
        }
        public DateTime ClockIn
        {
            get { return _ClockIn; }
            set
            {
                _ClockIn = value;
            }
        }

        public DateTime ClockOut
        {
            get { return _ClockOut; }
            set
            {
                _ClockOut = value;
            }
        }
        public string ColorIn
        {
            get { return _ColorIn; }
            set
            {
                _ColorIn = value;
            }
        }
        public string ColorOut
        {
            get { return _ColorOut; }
            set
            {
                _ColorOut = value;
            }
        }
        public string DefaultIsPayroll
        {
            get { return _DefaultIsPayroll; }
            set
            {
                _DefaultIsPayroll = value;
            }
        }

        public string Type
        {
            get { return _Type; }
            set
            {
                _Type = value;
            }
        }
        #endregion

        public DailyOTLog()
        {
            this._OTClockINOUT = new List<OTClock>();
        }
    }
}
