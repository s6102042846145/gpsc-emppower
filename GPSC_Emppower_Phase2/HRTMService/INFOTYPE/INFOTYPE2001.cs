using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using ESS.DATA;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.HR.TM.CONFIG;
using ESS.HR.TM.DATACLASS;
using ESS.TIMESHEET;
using ESS.WORKFLOW;
using ESS.EMPLOYEE.ABSTRACT;

namespace ESS.HR.TM.INFOTYPE
{
    [TypeConverter(typeof(INFOTYPE2001_Convertor))]
    public class INFOTYPE2001 : AbstractInfoType, ICollisionable
    {
        private TimeSpan __beginTime = TimeSpan.Zero;
        private TimeSpan __endTime = TimeSpan.Zero;
        private decimal __absenceDays;
        private decimal __absenceHours;
        private decimal __payrollDays;
        private bool __allDayFlag = true;
        private string __status = "";
        private string __remark = "";
        private bool __isRequiredDoc = true;
        private bool __isSkip = false;
        private bool __isMark = true;
        private bool __isPost = false;
        private string __requestNo = "";

        public INFOTYPE2001()
        {
            
        }

        #region " Properties "
        public override string InfoType
        {
            get { return "2001"; }
        }

        public string RequestNo
        {
            get
            {
                return __requestNo;
            }
            set
            {
                __requestNo = value;
            }
        }

        public string AbsenceType
        {
            get
            {
                return SubType;
            }
            set
            {
                SubType = value;
            }
        }

        public TimeSpan BeginTime
        {
            get
            {
                return __beginTime;
            }
            set
            {
                __beginTime = value;
            }
        }

        public TimeSpan EndTime
        {
            get
            {
                return __endTime;
            }
            set
            {
                __endTime = value;
            }
        }

        public decimal AbsenceDays
        {
            get
            {
                return __absenceDays;
            }
            set
            {
                __absenceDays = value;
            }
        }

        public decimal AbsenceHours
        {
            get
            {
                return __absenceHours;
            }
            set
            {
                __absenceHours = value;
            }
        }
        public decimal PayrollDays
        {
            get
            {
                if (__payrollDays == 0.0M && this.AllDayFlag)
                {
                    return 0.0M;
                }
                return __payrollDays;
            }
            set
            {
                __payrollDays = value;
            }
        }
        public bool AllDayFlag
        {
            get
            {
                return __allDayFlag;
            }
            set
            {
                __allDayFlag = value;
            }
        }

        public string Status
        {
            get
            {
                return __status;
            }
            set
            {
                __status = value;
            }
        }

        public string Remark
        {
            get
            {
                return __remark;
            }
            set
            {
                __remark = value;
            }
        }

        public bool IsRequireDocument
        {
            get
            {
                return __isRequiredDoc;
            }
            set
            {
                __isRequiredDoc = value;
            }
        }

        public bool IsMark
        {
            get
            {
                return __isMark;
            }
            set
            {
                __isMark = value;
            }
        }

        public bool IsPost
        {
            get
            {
                return __isPost;
            }
            set
            {
                __isPost = value;
            }
        }

        public bool IsSkip
        {
            get
            {
                return __isSkip;
            }
            set
            {
                __isSkip = value;
            }
        }
        #endregion

        public List<INFOTYPE2001> GetData(EmployeeData employeeData, DateTime BeginDate, DateTime EndDate)
        {
            List<INFOTYPE2001> oReturn = new List<INFOTYPE2001>();
            List<INFOTYPE2001> list;
            //list = HRTMManagement.CreateInstance(CompanyCode).GetAbsenceList(employeeData.EmployeeID, BeginDate, EndDate);
            list = ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).ESSData.GetAbsenceList(employeeData.EmployeeID, BeginDate, EndDate);
            foreach (INFOTYPE2001 item in list)
            {
                item.Status = "INPROCESS";
                oReturn.Add(item);
            }
            list = ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).ERPData.GetAbsenceList(employeeData.EmployeeID, BeginDate, EndDate);
            foreach (INFOTYPE2001 item in list)
            {
                item.Status = "RECORDED";
                oReturn.Add(item);
            }
            return oReturn;
        }

        public List<INFOTYPE2001> GetDataFromDB(string EmployeeID, DateTime BeginDate, DateTime EndDate, bool OnlyRecordedData = false)
        {
            List<INFOTYPE2001> oReturn = new List<INFOTYPE2001>();
            List<INFOTYPE2001> list;
            if (!OnlyRecordedData)
            {
                list = ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).ESSData.GetAbsenceList(EmployeeID, BeginDate, EndDate);
                foreach (INFOTYPE2001 item in list)
                {
                    item.Status = "INPROCESS";
                    oReturn.Add(item);
                }
            }
            list = ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).ESSData.GetTM_AbsenceList(EmployeeID, BeginDate, EndDate);
            foreach (INFOTYPE2001 item in list)
            {
                item.Status = "RECORDED";
                oReturn.Add(item);
            }
            return oReturn;
        }

        public List<INFOTYPE2001> GetDataOnlyRecorded(EmployeeData employeeData, DateTime BeginDate, DateTime EndDate)
        {
            List<INFOTYPE2001> oReturn = new List<INFOTYPE2001>();
            List<INFOTYPE2001> list;
            //list = HRTMManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetAbsenceList(employeeData.EmployeeID, BeginDate, EndDate);
            list = HRTMManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetPostAbsenceList(employeeData.EmployeeID, BeginDate, EndDate);
            foreach (INFOTYPE2001 item in list)
            {
                item.Status = "RECORDED";
                oReturn.Add(item);
            }
            return oReturn;
        }

        #region " ValidateData "
        public void ValidateData(DateTime DocumentDate)
        { 
            ValidateData(DocumentDate, false);
        }
        public void ValidateData(DateTime DocumentDate, bool CalculateOnly)
        {
            // validate begin - end
            DateTime date1, date2;
            System.TimeSpan datediff; //���� datediff ����Ѻ�礨ӹǹ�ѹ��
            if (this.AllDayFlag)
            {
                date1 = this.BeginDate;
                date2 = this.EndDate;
            }
            else
            {
                date1 = this.BeginDate.Add(this.BeginTime);
                date2 = this.BeginDate.Add(this.EndTime);
                if (date2 < date1)
                {
                    date2 = date2.AddDays(1);
                }
            }
            if (date1 > date2)
            {
                throw new ValidatingException("BEGINDATE_GREATER_THAN_ENDDATE");
            }
            else
            {
                if (date1 == date2)
                    datediff = TimeSpan.Parse("1");
                else
                    datediff = TimeSpan.Parse(Convert.ToString(DateTime.Compare(date2 ,date1) + 1));
            }
            
            // validate AbsenceType
            AbsenceType oAbsenceType = HRTMManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetAbsenceType(this.EmployeeID, this.AbsenceType, "");
            if (oAbsenceType.Key == null)
            {
                throw new ValidatingException("ABSENCETYPE_NOTFOUND");
            }
            
            // check Creating Rule
            AbsenceCreatingRule oCreatingRule = HRTMManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetCreatingRule(this.EmployeeID, this.AbsenceType);

            #region Check requested absence or attendance is in process?
            DateTime tmpBeginDate =this.BeginTime.Ticks > 0? this.BeginDate.Add(this.BeginTime):this.BeginDate;
            DateTime tmpEndDate = this.EndTime.Ticks > 0 ? this.EndDate.Add(this.EndTime):this.EndDate ;
            if (tmpEndDate < tmpBeginDate)
                tmpEndDate = tmpEndDate.AddDays(1);
            else if (this.AllDayFlag)
                tmpEndDate = tmpEndDate.AddDays(1).AddSeconds(-1);

            EmployeeData empData = new EmployeeData(EmployeeID);
            string ExistingDate = string.Empty;
            List<INFOTYPE2001> listInfotype2001 = GetData(empData, tmpBeginDate, tmpEndDate);
            if (listInfotype2001.Exists(delegate(INFOTYPE2001 item) { return item.RequestNo != this.RequestNo; }))
            {
                INFOTYPE2001 ExistingItem = listInfotype2001.Find(delegate(INFOTYPE2001 item) { return item.RequestNo != this.RequestNo; });
                if (ExistingItem.AllDayFlag)
                {
                    if (ExistingItem.BeginDate <= this.EndDate && ExistingItem.EndDate >= this.BeginDate)
                    {
                        //����¡�ë�ӡѺ
                        if (ExistingItem.BeginDate.Date == ExistingItem.EndDate.Date)//���ѹ����
                        {
                            ExistingDate = ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US"));
                        }
                        else//�������ѹ
                        {
                            ExistingDate = string.Format("{0} - {1}", ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")), ExistingItem.EndDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")));
                        }
                        throw new Exception("ABSENCE_IN_PROCESS|" + ExistingDate);
                    }
                }
                else
                {
                    DateTime ExistingBegin = ExistingItem.BeginDate.Add(ExistingItem.BeginTime);
                    DateTime ExistingEnd = ExistingItem.EndDate.Add(ExistingItem.EndTime);
                    if (ExistingEnd < ExistingBegin)
                        ExistingEnd = ExistingEnd.AddDays(1);
                    if (tmpBeginDate <= ExistingEnd && tmpEndDate >= ExistingBegin)   //Check Overlap 20171214 Koissares
                    {
                        //����¡�ë�ӡѺ
                        if (ExistingItem.BeginDate.Date == ExistingItem.EndDate.Date)//���ѹ����
                        {
                            ExistingDate = ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")) + " " + ExistingItem.BeginTime.ToString() + "-" + ExistingItem.EndTime.ToString();
                        }
                        else//�������ѹ
                        {
                            ExistingDate = string.Format("{0} - {1}", ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")), ExistingItem.EndDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")));
                        }
                        throw new Exception("ABSENCE_IN_PROCESS|" + ExistingDate);
                    }
                }
            }

            List<INFOTYPE2002> listInfotype2002 =new INFOTYPE2002().GetData(empData, tmpBeginDate, tmpEndDate);
            if (listInfotype2002.Exists(delegate(INFOTYPE2002 item) { return item.RequestNo != this.RequestNo; }))
            {
                INFOTYPE2002 ExistingItem = listInfotype2002.Find(delegate(INFOTYPE2002 item) { return item.RequestNo != this.RequestNo; });
                if (ExistingItem.AllDayFlag)
                {
                    if (ExistingItem.BeginDate <= this.EndDate && ExistingItem.EndDate >= this.BeginDate)
                    {
                        //����¡�ë�ӡѺ
                        if (ExistingItem.BeginDate.Date == ExistingItem.EndDate.Date)//�Ѻ�ͧ�����ѹ����
                        {
                            ExistingDate = ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US"));
                        }
                        else//�Ѻ�ͧ���������ѹ
                        {
                            ExistingDate = string.Format("{0} - {1}", ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")), ExistingItem.EndDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")));
                        }
                        throw new Exception("ATTENDANCE_IN_PROCESS|" + ExistingDate);
                    }
                }
                else
                {
                    DateTime ExistingBegin = ExistingItem.BeginDate.Add(ExistingItem.BeginTime);
                    DateTime ExistingEnd = ExistingItem.EndDate.Add(ExistingItem.EndTime);
                    if (ExistingEnd < ExistingBegin)
                        ExistingEnd = ExistingEnd.AddDays(1);
                    if (tmpBeginDate <= ExistingEnd && tmpEndDate >= ExistingBegin)   //Check Overlap 20171214 Koissares
                    {
                        //����¡�ë�ӡѺ
                        if (ExistingItem.BeginDate.Date == ExistingItem.EndDate.Date)//�Ѻ�ͧ�����ѹ����
                        {
                            ExistingDate = ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")) + " " + ExistingItem.BeginTime.ToString() + "-" + ExistingItem.EndTime.ToString(); ;
                        }
                        else//�Ѻ�ͧ���������ѹ
                        {
                            ExistingDate = string.Format("{0} - {1}", ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")), ExistingItem.EndDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")));
                        }
                        throw new Exception("ATTENDANCE_IN_PROCESS|" + ExistingDate);
                    }
                }
            }
            #endregion

            if (!CalculateOnly)
            {
                oCreatingRule.IsCanCreate(this.EmployeeID, this.BeginDate, this.EndDate, DocumentDate);
            }

            // validate quota
            if (this.AllDayFlag && !CalculateOnly)
            {
                List<INFOTYPE2001> buffer = GenerateAssignmentList(this, oCreatingRule, this.RequestNo, true);
                this.AbsenceDays = 0.0M;
                this.AbsenceHours = 0.0M;
                this.PayrollDays = 0.0M;
                foreach (INFOTYPE2001 data in buffer)
                {
                    this.AbsenceDays += data.AbsenceDays;
                    this.AbsenceHours += data.AbsenceHours;
                    this.PayrollDays += data.PayrollDays;
                }
                this.IsRequireDocument = Math.Ceiling(this.AbsenceDays) >= oCreatingRule.RequireDocDay;

               
                if (this.AbsenceDays == 0 && oCreatingRule.CheckQuotaWhenCreate && !CalculateOnly)
                {
                    throw new ValidatingException("ZERO_HOUR");
                }
            }
            else
            {
                // comment in 2009-10-12 cause BAPI has problem //
                //AbsAttCalculateResult oResult = ESS.HR.TM.ServiceManager.HRTMService.CalculateAbsAttEntry(EmployeeID, SubType, BeginDate, EndDate, BeginTime, EndTime, AllDayFlag, oCreatingRule.CheckQuotaWhenCreate && !CalculateOnly, oCreatingRule.TruncateDayOff);
                // edit to --->
                // calculate quota in Application instead SAP , becuase must check inprocess item.
                AbsAttCalculateResult oResult = HRTMManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).CalculateAbsAttEntry(EmployeeID, SubType, BeginDate, EndDate, BeginTime, EndTime, AllDayFlag, false, oCreatingRule.TruncateDayOff);
                EmployeeData employeeData = new EmployeeData(this.EmployeeID);
                List<INFOTYPE2006> quotalist =new INFOTYPE2006().GetData(employeeData, RequestNo);
                Dictionary<string, List<INFOTYPE2006>> dict = INFOTYPE2006.GenerateDictionary(quotalist);
                Dictionary<string, DeductionRule> deductRule = new Dictionary<string, DeductionRule>();
                AbsenceType oAT = HRTMManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetAbsenceType(this.EmployeeID, this.AbsenceType, "");
                if (oAT.IsUseQuota && oCreatingRule.CheckQuotaWhenCreate)
                {
                    new INFOTYPE2006().TryDeductQuota(employeeData, dict, deductRule, this, true);
                }
                // end edit
                if (oResult.AbsenceDays == 0 && oCreatingRule.CheckQuotaWhenCreate && !CalculateOnly)
                {
                    throw new ValidatingException("ZERO_HOUR");
                }
                this.AbsenceDays = oResult.AbsenceDays;
                this.AbsenceHours = oResult.AbsenceHours;
                if (!this.AllDayFlag)
                {
                    if (oResult.BeginTime == TimeSpan.MinValue)
                    {
                        this.AllDayFlag = true;
                    }
                    else
                    {
                        this.AllDayFlag = false;
                    }
                }
                this.BeginTime = oResult.BeginTime;
                this.EndTime = oResult.EndTime;
                this.PayrollDays = oResult.PayrollDays;
                this.IsRequireDocument = Math.Ceiling(oResult.AbsenceDays) >= oCreatingRule.RequireDocDay;
                this.BeginDate = oResult.BeginDate;
                this.EndDate = oResult.EndDate;
            }

            if (!CalculateOnly)
            {
                oCreatingRule = HRTMManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetAbsenceCreatingRule(empData, this.AbsenceType, this.AbsenceDays);
                oCreatingRule.IsCanCreate(this.EmployeeID, this.BeginDate, this.EndDate, DocumentDate);
            }
        }
        public void ValidateData(DateTime DocumentDate, bool CalculateOnly, EmployeeData oEmp)
        {
            // validate begin - end
            DateTime date1, date2;
            System.TimeSpan datediff; //���� datediff ����Ѻ�礨ӹǹ�ѹ��
            if (this.AllDayFlag)
            {
                date1 = this.BeginDate;
                date2 = this.EndDate;
            }
            else
            {
                date1 = this.BeginDate.Add(this.BeginTime);
                date2 = this.BeginDate.Add(this.EndTime);
                if (date2 < date1)
                {
                    date2 = date2.AddDays(1);
                }
            }
            if (date1 > date2)
            {
                throw new ValidatingException("BEGINDATE_GREATER_THAN_ENDDATE");
            }
            else
            {
                if (date1 == date2)
                    datediff = TimeSpan.Parse("1");
                else
                    datediff = TimeSpan.Parse(Convert.ToString(DateTime.Compare(date2, date1) + 1));
            }

            // validate AbsenceType
            AbsenceType oAbsenceType = HRTMManagement.CreateInstance(oEmp.CompanyCode).GetAbsenceType(this.EmployeeID, this.AbsenceType, "");
            if (oAbsenceType.Key == null)
            {
                throw new ValidatingException("ABSENCETYPE_NOTFOUND");
            }

            // check Creating Rule
            //AbsenceCreatingRule oCreatingRule = HRTMManagement.CreateInstance(oEmp.CompanyCode).GetCreatingRule(oEmp, this.AbsenceType);
            AbsenceCreatingRule oCreatingRule = HRTMManagement.CreateInstance(oEmp.CompanyCode).GetCreatingRule(oEmp, this.AbsenceType, 0);

            #region Check requested absence or attendance is in process?
            DateTime tmpBeginDate = this.BeginTime.Ticks > 0 ? this.BeginDate.Add(this.BeginTime) : this.BeginDate;
            DateTime tmpEndDate = this.EndTime.Ticks > 0 ? this.EndDate.Add(this.EndTime) : this.EndDate;
            if (tmpEndDate < tmpBeginDate)
                tmpEndDate = tmpEndDate.AddDays(1);
            else if (this.AllDayFlag)
                tmpEndDate = tmpEndDate.AddDays(1).AddSeconds(-1);

            EmployeeData empData = new EmployeeData(EmployeeID) { CompanyCode = oEmp.CompanyCode };
            string ExistingDate = string.Empty;
            //List<INFOTYPE2001> listInfotype2001 = GetData(empData, tmpBeginDate, tmpEndDate);
            List<INFOTYPE2001> listInfotype2001 = GetData(empData, tmpBeginDate.Date, tmpEndDate.Date);
            if (listInfotype2001.Exists(delegate (INFOTYPE2001 item) { return item.RequestNo != this.RequestNo; }))
            {
                INFOTYPE2001 ExistingItem = listInfotype2001.Find(delegate (INFOTYPE2001 item) { return item.RequestNo != this.RequestNo; });
                if (ExistingItem.AllDayFlag)
                {
                    if (ExistingItem.BeginDate <= this.EndDate && ExistingItem.EndDate >= this.BeginDate)
                    {
                        //����¡�ë�ӡѺ
                        if (ExistingItem.BeginDate.Date == ExistingItem.EndDate.Date)//���ѹ����
                        {
                            ExistingDate = ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US"));
                        }
                        else//�������ѹ
                        {
                            ExistingDate = string.Format("{0} - {1}", ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")), ExistingItem.EndDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")));
                        }
                        throw new Exception("ABSENCE_IN_PROCESS|" + ExistingDate);
                    }
                }
                else
                {
                    DateTime ExistingBegin = ExistingItem.BeginDate.Add(ExistingItem.BeginTime);
                    DateTime ExistingEnd = ExistingItem.EndDate.Add(ExistingItem.EndTime);
                    if (ExistingEnd < ExistingBegin)
                        ExistingEnd = ExistingEnd.AddDays(1);
                    if (tmpBeginDate <= ExistingEnd && tmpEndDate >= ExistingBegin)   //Check Overlap 20171214 Koissares
                    {
                        //����¡�ë�ӡѺ
                        if (ExistingItem.BeginDate.Date == ExistingItem.EndDate.Date)//���ѹ����
                        {
                            ExistingDate = ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")) + " " + ExistingItem.BeginTime.ToString() + "-" + ExistingItem.EndTime.ToString();
                        }
                        else//�������ѹ
                        {
                            ExistingDate = string.Format("{0} - {1}", ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")), ExistingItem.EndDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")));
                        }
                        throw new Exception("ABSENCE_IN_PROCESS|" + ExistingDate);
                    }
                }
            }

            //List<INFOTYPE2002> listInfotype2002 = new INFOTYPE2002().GetData(empData, tmpBeginDate, tmpEndDate);
            List<INFOTYPE2002> listInfotype2002 = new INFOTYPE2002().GetData(empData, tmpBeginDate.Date, tmpEndDate.Date);
            if (listInfotype2002.Exists(delegate (INFOTYPE2002 item) { return item.RequestNo != this.RequestNo; }))
            {
                INFOTYPE2002 ExistingItem = listInfotype2002.Find(delegate (INFOTYPE2002 item) { return item.RequestNo != this.RequestNo; });
                if (ExistingItem.AllDayFlag)
                {
                    if (ExistingItem.BeginDate <= this.EndDate && ExistingItem.EndDate >= this.BeginDate)
                    {
                        //����¡�ë�ӡѺ
                        if (ExistingItem.BeginDate.Date == ExistingItem.EndDate.Date)//�Ѻ�ͧ�����ѹ����
                        {
                            ExistingDate = ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US"));
                        }
                        else//�Ѻ�ͧ���������ѹ
                        {
                            ExistingDate = string.Format("{0} - {1}", ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")), ExistingItem.EndDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")));
                        }
                        throw new Exception("ATTENDANCE_IN_PROCESS|" + ExistingDate);
                    }
                }
                else
                {
                    DateTime ExistingBegin = ExistingItem.BeginDate.Add(ExistingItem.BeginTime);
                    DateTime ExistingEnd = ExistingItem.EndDate.Add(ExistingItem.EndTime);
                    if (ExistingEnd < ExistingBegin)
                        ExistingEnd = ExistingEnd.AddDays(1);
                    if (tmpBeginDate <= ExistingEnd && tmpEndDate >= ExistingBegin)   //Check Overlap 20171214 Koissares
                    {
                        //����¡�ë�ӡѺ
                        if (ExistingItem.BeginDate.Date == ExistingItem.EndDate.Date)//�Ѻ�ͧ�����ѹ����
                        {
                            ExistingDate = ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")) + " " + ExistingItem.BeginTime.ToString() + "-" + ExistingItem.EndTime.ToString(); ;
                        }
                        else//�Ѻ�ͧ���������ѹ
                        {
                            ExistingDate = string.Format("{0} - {1}", ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")), ExistingItem.EndDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")));
                        }
                        throw new Exception("ATTENDANCE_IN_PROCESS|" + ExistingDate);
                    }
                }
            }
            #endregion

            //if (!CalculateOnly)
            //{
            //    oCreatingRule.IsCanCreate(this.EmployeeID, this.BeginDate, this.EndDate, DocumentDate);
            //}

            // validate quota
            if (this.AllDayFlag && !CalculateOnly)
            {
                List<INFOTYPE2001> buffer = GenerateAssignmentList(this, oCreatingRule, this.RequestNo, true, oEmp.CompanyCode);
                this.AbsenceDays = 0.0M;
                this.AbsenceHours = 0.0M;
                this.PayrollDays = 0.0M;
                foreach (INFOTYPE2001 data in buffer)
                {
                    this.AbsenceDays += data.AbsenceDays;
                    this.AbsenceHours += data.AbsenceHours;
                    this.PayrollDays += data.PayrollDays;
                }
                this.IsRequireDocument = Math.Ceiling(this.AbsenceDays) >= oCreatingRule.RequireDocDay;


                if (this.AbsenceDays == 0 && oCreatingRule.CheckQuotaWhenCreate && !CalculateOnly)
                {
                    throw new ValidatingException("ZERO_HOUR");
                }
            }
            else
            {
                // comment in 2009-10-12 cause BAPI has problem //
                //AbsAttCalculateResult oResult = ESS.HR.TM.ServiceManager.HRTMService.CalculateAbsAttEntry(EmployeeID, SubType, BeginDate, EndDate, BeginTime, EndTime, AllDayFlag, oCreatingRule.CheckQuotaWhenCreate && !CalculateOnly, oCreatingRule.TruncateDayOff);
                // edit to --->
                // calculate quota in Application instead SAP , becuase must check inprocess item.
                AbsAttCalculateResult oResult = HRTMManagement.CreateInstance(oEmp.CompanyCode).CalculateAbsAttEntry(EmployeeID, SubType, BeginDate, EndDate, BeginTime, EndTime, AllDayFlag, false, oCreatingRule.TruncateDayOff);
                EmployeeData employeeData = new EmployeeData(this.EmployeeID) { CompanyCode = oEmp.CompanyCode };
                List<INFOTYPE2006> quotalist = new INFOTYPE2006().GetData(employeeData, RequestNo);
                Dictionary<string, List<INFOTYPE2006>> dict = INFOTYPE2006.GenerateDictionary(quotalist);
                Dictionary<string, DeductionRule> deductRule = new Dictionary<string, DeductionRule>();
                AbsenceType oAT = HRTMManagement.CreateInstance(oEmp.CompanyCode).GetAbsenceType(this.EmployeeID, this.AbsenceType, "");
                if (oAT.IsUseQuota && oCreatingRule.CheckQuotaWhenCreate)
                {
                    new INFOTYPE2006().TryDeductQuota(employeeData, dict, deductRule, this, true);
                }
                // end edit
                if (oResult.AbsenceDays == 0 && oCreatingRule.CheckQuotaWhenCreate && !CalculateOnly)
                {
                    throw new ValidatingException("ZERO_HOUR");
                }
                this.AbsenceDays = oResult.AbsenceDays;
                this.AbsenceHours = oResult.AbsenceHours;
                if (!this.AllDayFlag)
                {
                    if (oResult.BeginTime == TimeSpan.MinValue)
                    {
                        this.AllDayFlag = true;
                    }
                    else
                    {
                        this.AllDayFlag = false;
                    }
                }
                this.BeginTime = oResult.BeginTime;
                this.EndTime = oResult.EndTime;
                this.PayrollDays = oResult.PayrollDays;
                this.IsRequireDocument = Math.Ceiling(oResult.AbsenceDays) >= oCreatingRule.RequireDocDay;
                this.BeginDate = oResult.BeginDate;
                this.EndDate = oResult.EndDate;
            }

            if (!CalculateOnly)
            {
                oCreatingRule = HRTMManagement.CreateInstance(oEmp.CompanyCode).GetCreatingRule(empData, this.AbsenceType, this.PayrollDays);
                oCreatingRule.IsCanCreate(this.EmployeeID, this.BeginDate, this.EndDate, DocumentDate);
            }
        }
        #endregion

        public List<INFOTYPE2001> GenerateAssignmentList(INFOTYPE2001 item, HR.TM.CONFIG.AbsenceCreatingRule oCreatingRule, string RequestNo, bool CheckData)
        {
            List<INFOTYPE2001> oReturn = new List<INFOTYPE2001>();
            EmployeeData employeeData = new EmployeeData(item.EmployeeID);
            List<INFOTYPE2006> quotalist =new INFOTYPE2006().GetData(employeeData, RequestNo);
            Dictionary<string, List<INFOTYPE2006>> dict = INFOTYPE2006.GenerateDictionary(quotalist);
            Dictionary<string, DeductionRule> deductRule = new Dictionary<string, DeductionRule>();

            int dateRange = 0;
            int dayCount = 0;
            MonthlyWS oMWS = null;
            DailyWS oDWS;
            TimeSpan oTS = item.EndDate.Subtract(item.BeginDate);
            dateRange = oTS.Days + 1;
            if (oCreatingRule.TruncateDayOff)
            {


                for (DateTime rundate = item.BeginDate; rundate <= item.EndDate; rundate = rundate.AddDays(1))
                {
                    if (employeeData.OrgAssignment.EndDate.Date < rundate || employeeData.OrgAssignment.BeginDate.Date > rundate)
                    {
                        employeeData = new EmployeeData(employeeData.EmployeeID, rundate);
                    }

                    if (oMWS == null || oMWS.WS_Month != rundate.Month || oMWS.WS_Year != rundate.Year)
                    {
                        oMWS = MonthlyWS.GetCalendar(item.EmployeeID, rundate);
                    }
                    string cHolidayClass = oMWS.GetWSCode(rundate.Day, false).Trim();
                    if (cHolidayClass == "")
                    {
                        oDWS = EmployeeManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetDailyWorkschedule(employeeData.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, oMWS.GetWSCode(rundate.Day, true), rundate);
                        if (!oDWS.IsDayOff)
                        {
                            dayCount++;
                        }

                        if (rundate == item.BeginDate && oDWS.IsDayOff)
                        {
                            throw new Exception("CANNOT_CREATE_IN_HOLIDAY");
                        }
                        else if (rundate == item.EndDate && oDWS.IsDayOff)
                        {
                            throw new Exception("CANNOT_CREATE_IN_HOLIDAY");
                        }
                    }

                }
                //dayCount++;
            }
            else
            {
                dayCount = dateRange;
            }

            foreach (AbsenceAssignment assign in HRTMManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetAbsenceAssignment(oCreatingRule.AbsAttGrouping, oCreatingRule.AbsenceType))
            {
                if (dayCount >= assign.Value1)
                {
                    int valueMin;
                    if (assign.Value2 >= dayCount)
                    {
                        valueMin = dateRange;
                    }
                    else
                    {
                        valueMin = assign.Value2;
                    }

                    DateTime myDate1, myDate2;
                    myDate1 = item.BeginDate.AddDays(assign.Value1 - 1);
                    myDate2 = item.BeginDate.AddDays(valueMin - 1);
                    INFOTYPE2001 dummy = new INFOTYPE2001();
                    dummy.EmployeeID = item.EmployeeID;
                    dummy.AbsenceType = assign.ItemAssign;
                    dummy.BeginDate = myDate1;
                    dummy.EndDate = myDate2;
                    AbsAttCalculateResult oResult1;
                    if (item.AllDayFlag)
                    {
                        dummy.AllDayFlag = true;
                        dummy.BeginTime = TimeSpan.MinValue;
                        dummy.EndTime = TimeSpan.MinValue;
                        //oResult1 = ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).ERPData.CalculateAbsAttEntry(employeeData.EmployeeID, dummy.AbsenceType, dummy.BeginDate, dummy.EndDate, TimeSpan.MinValue, TimeSpan.MinValue, true, false, false);
                        oResult1 = HRTMManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).CalculateAbsAttEntry(employeeData.EmployeeID, dummy.AbsenceType, dummy.BeginDate, dummy.EndDate, TimeSpan.MinValue, TimeSpan.MinValue, true, false, false);
                    }
                    else
                    {
                        dummy.AllDayFlag = false;
                        dummy.BeginTime = item.BeginTime;
                        dummy.EndTime = item.EndTime;
                        //oResult1 = ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).ERPData.CalculateAbsAttEntry(employeeData.EmployeeID, dummy.AbsenceType, dummy.BeginDate, dummy.EndDate, dummy.BeginTime, dummy.EndTime, false, false, false);
                        oResult1 = HRTMManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).CalculateAbsAttEntry(employeeData.EmployeeID, dummy.AbsenceType, dummy.BeginDate, dummy.EndDate, dummy.BeginTime, dummy.EndTime, false, false, false);
                    }
                    dummy.AbsenceDays = oResult1.AbsenceDays;
                    dummy.AbsenceHours = oResult1.AbsenceHours;
                    dummy.PayrollDays = oResult1.PayrollDays;
                    if (CheckData)
                    {
                        AbsenceType oAT = HRTMManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetAbsenceType(item.EmployeeID, dummy.AbsenceType, "");
                        if (oAT.IsUseQuota && oCreatingRule.CheckQuotaWhenCreate)
                        {
                           new INFOTYPE2006().TryDeductQuota(employeeData, dict, deductRule, dummy, true);
                        }
                    }
                    oReturn.Add(dummy);
                }
            }
            return oReturn;
        }

        public List<INFOTYPE2001> GenerateAssignmentList(INFOTYPE2001 item, HR.TM.CONFIG.AbsenceCreatingRule oCreatingRule, string RequestNo, bool CheckData, string CompCode)
        {
            List<INFOTYPE2001> oReturn = new List<INFOTYPE2001>();
            EmployeeData employeeData = new EmployeeData(item.EmployeeID) { CompanyCode = CompCode };
            List<INFOTYPE2006> quotalist = new INFOTYPE2006().GetData(employeeData, RequestNo);
            Dictionary<string, List<INFOTYPE2006>> dict = INFOTYPE2006.GenerateDictionary(quotalist);
            Dictionary<string, DeductionRule> deductRule = new Dictionary<string, DeductionRule>();

            int dateRange = 0;
            int dayCount = 0;
            MonthlyWS oMWS = null;
            DailyWS oDWS;
            TimeSpan oTS = item.EndDate.Subtract(item.BeginDate);
            dateRange = oTS.Days + 1;
            if (oCreatingRule.TruncateDayOff)
            {


                for (DateTime rundate = item.BeginDate; rundate <= item.EndDate; rundate = rundate.AddDays(1))
                {
                    if (employeeData.OrgAssignment.EndDate.Date < rundate || employeeData.OrgAssignment.BeginDate.Date > rundate)
                    {
                        employeeData = new EmployeeData(employeeData.EmployeeID, rundate) { CompanyCode = CompCode };
                    }

                    if (oMWS == null || oMWS.WS_Month != rundate.Month || oMWS.WS_Year != rundate.Year)
                    {
                        oMWS = MonthlyWS.GetCalendar(item.EmployeeID, rundate, CompCode);
                    }
                    string cHolidayClass = oMWS.GetWSCode(rundate.Day, false).Trim();
                    if (cHolidayClass == "")
                    {
                        oDWS = EmployeeManagement.CreateInstance(CompCode).GetDailyWorkschedule(employeeData.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, oMWS.GetWSCode(rundate.Day, true), rundate);
                        if (!oDWS.IsDayOff)
                        {
                            dayCount++;
                        }

                        if (rundate == item.BeginDate && oDWS.IsDayOff)
                        {
                            throw new Exception("CANNOT_CREATE_IN_HOLIDAY");
                        }
                        else if (rundate == item.EndDate && oDWS.IsDayOff)
                        {
                            throw new Exception("CANNOT_CREATE_IN_HOLIDAY");
                        }
                    }

                }
                //dayCount++;
            }
            else
            {
                dayCount = dateRange;
            }

            foreach (AbsenceAssignment assign in HRTMManagement.CreateInstance(CompCode).GetAbsenceAssignment(oCreatingRule.AbsAttGrouping, oCreatingRule.AbsenceType))
            {
                if (dayCount >= assign.Value1)
                {
                    int valueMin;
                    if (assign.Value2 >= dayCount)
                    {
                        valueMin = dateRange;
                    }
                    else
                    {
                        valueMin = assign.Value2;
                    }

                    DateTime myDate1, myDate2;
                    myDate1 = item.BeginDate.AddDays(assign.Value1 - 1);
                    myDate2 = item.BeginDate.AddDays(valueMin - 1);
                    INFOTYPE2001 dummy = new INFOTYPE2001();
                    dummy.EmployeeID = item.EmployeeID;
                    dummy.AbsenceType = assign.ItemAssign;
                    dummy.BeginDate = myDate1;
                    dummy.EndDate = myDate2;
                    AbsAttCalculateResult oResult1;
                    if (item.AllDayFlag)
                    {
                        dummy.AllDayFlag = true;
                        dummy.BeginTime = TimeSpan.MinValue;
                        dummy.EndTime = TimeSpan.MinValue;
                        //oResult1 = ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).ERPData.CalculateAbsAttEntry(employeeData.EmployeeID, dummy.AbsenceType, dummy.BeginDate, dummy.EndDate, TimeSpan.MinValue, TimeSpan.MinValue, true, false, false);
                        oResult1 = HRTMManagement.CreateInstance(CompCode).CalculateAbsAttEntry(employeeData.EmployeeID, dummy.AbsenceType, dummy.BeginDate, dummy.EndDate, TimeSpan.MinValue, TimeSpan.MinValue, true, false, false, CompCode);
                    }
                    else
                    {
                        dummy.AllDayFlag = false;
                        dummy.BeginTime = item.BeginTime;
                        dummy.EndTime = item.EndTime;
                        //oResult1 = ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).ERPData.CalculateAbsAttEntry(employeeData.EmployeeID, dummy.AbsenceType, dummy.BeginDate, dummy.EndDate, dummy.BeginTime, dummy.EndTime, false, false, false);
                        oResult1 = HRTMManagement.CreateInstance(CompCode).CalculateAbsAttEntry(employeeData.EmployeeID, dummy.AbsenceType, dummy.BeginDate, dummy.EndDate, dummy.BeginTime, dummy.EndTime, false, false, false, CompCode);
                    }
                    dummy.AbsenceDays = oResult1.AbsenceDays;
                    dummy.AbsenceHours = oResult1.AbsenceHours;
                    dummy.PayrollDays = oResult1.PayrollDays;
                    if (CheckData)
                    {
                        AbsenceType oAT = HRTMManagement.CreateInstance(CompCode).GetAbsenceType(item.EmployeeID, dummy.AbsenceType, "");
                        if (oAT.IsUseQuota && oCreatingRule.CheckQuotaWhenCreate)
                        {
                            new INFOTYPE2006().TryDeductQuota(employeeData, dict, deductRule, dummy, true);
                        }
                    }
                    oReturn.Add(dummy);
                }
            }
            return oReturn;
        }

        public List<INFOTYPE2001> SplitForPost(List<INFOTYPE2001> input)
        {
            List<INFOTYPE2001> oReturn = new List<INFOTYPE2001>();
            foreach (INFOTYPE2001 item in input)
            {
                for (DateTime rundate = item.BeginDate.Date; rundate <= item.EndDate.Date; rundate = rundate.AddDays(1))
                {
                    INFOTYPE2001 dummy = new INFOTYPE2001();
                    dummy.EmployeeID = item.EmployeeID;
                    dummy.AbsenceType = item.AbsenceType;
                    dummy.BeginDate = rundate;
                    dummy.EndDate = rundate;
                    if (item.AllDayFlag)
                    {
                        dummy.AllDayFlag = true;
                        dummy.BeginTime = TimeSpan.MinValue;
                        dummy.EndTime = TimeSpan.MinValue;
                    }
                    else
                    {
                        dummy.AllDayFlag = false;
                        dummy.BeginTime = item.BeginTime;
                        dummy.EndTime = item.EndTime;
                    }
                    oReturn.Add(dummy);
                }
            }
            return oReturn;
        }

        public string DateTimeText
        {
            get
            {
                string cReturn = "";
                if (this.BeginDate == this.EndDate)
                {
                    cReturn = this.BeginDate.ToString("dd/MM/yyyy");
                    if (!this.AllDayFlag)
                    {
                        cReturn += string.Format(" {0} - {1}", TimeString(this.BeginTime),TimeString(this.EndTime));
                    } 
                }
                else
                {
                    cReturn = this.BeginDate.ToString("dd/MM/yyyy - ") + this.EndDate.ToString("dd/MM/yyyy");
                }
                return cReturn;
            }
        }
        public string GenerateText(DateTime BeginDate, DateTime EndDate, TimeSpan BeginTime, TimeSpan EndTime, bool AllDayFlag)
        {
            string cReturn = "";
            if (BeginDate == EndDate)
            {
                cReturn = BeginDate.ToString("dd/MM/yyyy");
                if (!AllDayFlag)
                {
                    cReturn += string.Format(" {0} - {1}", TimeString(BeginTime), TimeString(EndTime));
                }
            }
            else
            {
                cReturn = BeginDate.ToString("dd/MM/yyyy - ") + EndDate.ToString("dd/MM/yyyy");
            }
            return cReturn;
        }
        private string TimeString(TimeSpan data)
        {
            return string.Format("{0}:{1}", data.Hours.ToString("00"), data.Minutes.ToString("00"));
        }

        #region ICollisionable Members

        public List<TimesheetData> LoadCollision(string EmployeeID, DateTime BeginDate, DateTime EndDate, string SubKey1, string SubKey2)
        {
            List<TimesheetData> oReturn = new List<TimesheetData>();
            List<INFOTYPE2001> list = ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).ESSData.GetAbsenceList(EmployeeID, BeginDate.Date, EndDate.Date);
            foreach (INFOTYPE2001 item in list)
            {
                TimesheetData Data = new TimesheetData();
                Data.EmployeeID = item.EmployeeID;
                Data.ApplicationKey = "LEAVE";
                if (item.AllDayFlag)
                {
                    Data.BeginDate = item.BeginDate;
                    Data.EndDate = item.EndDate;
                }
                else
                {
                    Data.BeginDate = item.BeginDate.Add(item.BeginTime);
                    Data.EndDate = item.EndDate.Add(item.EndTime);
                    if (!(Data.BeginDate <= EndDate && Data.EndDate >= BeginDate))
                    {
                        continue;
                    }
                }
                Data.FullDay = item.AllDayFlag;
                Data.SubKey1 = item.AbsenceType;
                Data.SubKey2 = "";
                Data.Detail = item.RequestNo;
                oReturn.Add(Data);
            }

            list = ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).ERPData.GetAbsenceList(EmployeeID, BeginDate, EndDate);
            foreach (INFOTYPE2001 item in list)
            {
                TimesheetData Data = new TimesheetData();
                Data.EmployeeID = item.EmployeeID;
                Data.ApplicationKey = "LEAVE";
                if (item.AllDayFlag)
                {
                    Data.BeginDate = item.BeginDate;
                    Data.EndDate = item.EndDate;
                }
                else
                {
                    Data.BeginDate = item.BeginDate.Add(item.BeginTime);
                    Data.EndDate = item.EndDate.Add(item.EndTime);
                }
                Data.FullDay = item.AllDayFlag;
                Data.SubKey1 = item.AbsenceType;
                Data.SubKey2 = "";
                oReturn.Add(Data);
            }

            return oReturn;
        }

        #endregion

        public override string ToString()
        {
            INFOTYPE2001_Convertor oConverter = new INFOTYPE2001_Convertor();
            return (string)oConverter.ConvertTo(this, typeof(string));
        }

        public INFOTYPE2001 Parse(string value)
        {
            INFOTYPE2001_Convertor oConverter = new INFOTYPE2001_Convertor();
            return (INFOTYPE2001)oConverter.ConvertFrom(value);
        }

        public bool CheckExistingData(EmployeeData Requestor, INFOTYPE2001 oAbsence)
        {
            List<INFOTYPE2001> list = GetDataOnlyRecorded(Requestor, oAbsence.BeginDate, oAbsence.EndDate.AddDays(1));
            bool lFound = false;
            foreach (INFOTYPE2001 item in list)
            {
                if (item.ToString() == oAbsence.ToString())
                {
                    lFound = true;
                    break;
                }
            }
            return lFound;
        }
    }
}