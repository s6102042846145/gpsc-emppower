using System;
using System.Collections.Generic;
using System.Text;
using ESS.EMPLOYEE;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.TM.INFOTYPE
{
    public class LeavePlan : AbstractObject
    {
        private string __employeeID = "";
        private DateTime __planDate = DateTime.MinValue;
        private bool __isCompleted = false;
        public LeavePlan()
        { 
        }
        public string EmployeeID
        {
            get
            {
                return __employeeID;
            }
            set
            {
                __employeeID = value;
            }
        }
        public DateTime PlanDate
        {
            get
            {
                return __planDate;
            }
            set
            {
                __planDate = value;
            }
        }
        public bool IsCompleted
        {
            get
            {
                return __isCompleted;
            }
            set
            {
                __isCompleted = value;
            }
        }
    }
}
