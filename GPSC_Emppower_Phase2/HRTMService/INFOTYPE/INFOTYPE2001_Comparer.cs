using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.HR.TM.INFOTYPE
{
    public class INFOTYPE2001_Comparer : IComparer<INFOTYPE2001>
    {
        public INFOTYPE2001_Comparer()
        { 
        }

        #region IComparer<INFOTYPE2001> Members

        public int Compare(INFOTYPE2001 x, INFOTYPE2001 y)
        {
            if (x.AbsenceType.CompareTo(y.AbsenceType) == 0)
            {
                if (x.BeginDate < y.BeginDate)
                {
                    return -1;
                }
                else if (x.BeginDate == y.BeginDate)
                {
                    return 0;
                }
                else
                {
                    return 1;
                }
            }
            else
            {
                return x.AbsenceType.CompareTo(y.AbsenceType);
            }
        }
 
        #endregion
    }
}
