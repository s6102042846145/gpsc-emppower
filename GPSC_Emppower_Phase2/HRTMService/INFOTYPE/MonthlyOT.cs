using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Text;
using ESS.HR.TM;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.TM.INFOTYPE
{
    public class MonthlyOT : AbstractObject
    {
        private DailyOT __dailyOT = null;
        private TimeSpan __beginTime = TimeSpan.MinValue;
        private TimeSpan __endTime = TimeSpan.MinValue;
        private string __description = "";
        private bool __isDeleted = false;
        private string __dailyRequestNo = "";
        private string __dailyAssignFrom = "";
        public MonthlyOT()
        {
        }

        protected override DataColumn CreateComplexTypeColumn(PropertyInfo prop)
        {
            if (prop.Name == "DailyOT")
            {
                DataColumn oDC = new DataColumn(prop.Name, typeof(DailyOT));
                return oDC;
            }
            return base.CreateComplexTypeColumn(prop);
        }

        internal DataTable ToADODataTable(bool v)
        {
            throw new NotImplementedException();
        }

        protected override void SetDataRowToObject(PropertyInfo prop, DataRow dr)
        {
            if (prop.Name == "DailyOT")
            {
                DataColumn oDC = dr.Table.Columns[prop.Name];
                if (oDC != null)
                {
                    if (oDC.DataType == typeof(DailyOT))
                    {
                        this.DailyOT = (DailyOT)dr[prop.Name];
                        this.DailyOT.BeginTime = (TimeSpan)dr["BeginTime"];
                        this.DailyOT.EndTime = (TimeSpan)dr["EndTime"];
                    }
                    else
                    {
                        DailyOT item = new DailyOT();
                        item.ParseToObject((DataTable)dr[prop.Name]);
                        this.DailyOT = item;
                    }
                    return;
                }
            }
            base.SetDataRowToObject(prop, dr);
        }

        protected override void LoadObjectToDataRow(PropertyInfo prop, DataRow oNewRow, DataColumn oDC)
        {
            if (prop.Name == "DailyOT")
            {
                if (this.DailyOT == null)
                {
                    oNewRow[prop.Name] = DBNull.Value;
                }
                else
                {
                    if (oDC.DataType == typeof(DailyOT))
                    {
                        oNewRow[prop.Name] = this.DailyOT;
                    }
                    else
                    {
                        DataTable oTable = this.DailyOT.ToADODataTable();
                        oTable.TableName = "DAILYOT";
                        oNewRow[prop.Name] = oTable;
                    }
                }
                return;
            }
            base.LoadObjectToDataRow(prop, oNewRow, oDC);
        }

        public string DailyRequestNo
        {
            get
            {
                return __dailyRequestNo;
            }
            set
            {
                __dailyRequestNo = value;
            }
        }

        public string DailyAssignFrom
        {
            get
            {
                return __dailyAssignFrom;
            }
            set
            {
                __dailyAssignFrom = value;
            }
        }

        public string Description
        {
            get
            {
                return __description;
            }
            set
            {
                __description = value;
            }
        }

        public TimeSpan BeginTime
        {
            get
            {
                return __beginTime;
            }
            set
            {
                __beginTime = value;
            }
        }

        public TimeSpan EndTime
        {
            get
            {
                return __endTime;
            }
            set
            {
                __endTime = value;
            }
        }

        public bool IsDeleted
        {
            get
            {
                return __isDeleted;
            }
            set
            {
                __isDeleted = value;
            }
        }

        public DailyOT DailyOT
        {
            get
            {
                return __dailyOT;
            }
            set
            {
                __dailyOT = value;
            }
        }

        internal void ParseToObject(DataRow dr)
        {
            throw new NotImplementedException();
        }

        internal void LoadDataToTableRow(DataRow dr)
        {
            throw new NotImplementedException();
        }
    }
}
