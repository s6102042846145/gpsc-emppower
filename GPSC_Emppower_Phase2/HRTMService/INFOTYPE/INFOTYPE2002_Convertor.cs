using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace ESS.HR.TM.INFOTYPE
{
   public class INFOTYPE2002_Convertor : TypeConverter
    {
        private CultureInfo oCL = new CultureInfo("en-US");

        public INFOTYPE2002_Convertor()
        {
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == typeof(string))
            {
                INFOTYPE2002 myValue = (INFOTYPE2002)value;
               
                return string.Format("{0}|{1}|{2}|{3}|{4}|{5}", myValue.EmployeeID, myValue.AttendanceType, myValue.BeginDate.ToString("yyyyMMdd", oCL), myValue.EndDate.ToString("yyyyMMdd", oCL), myValue.BeginTime.ToString(), myValue.EndTime.ToString());
               
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
            {
                return true;
            }
            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (value is string)
            {
                INFOTYPE2002 oReturn = null;
                string myVal = (string)value;
                Regex reg1 = new Regex(@"(?<EmployeeID>[0-9]{8})\|(?<AttendanceType>[0-9]{4})\|(?<BeginDate>[0-9]{8})\|(?<EndDate>[0-9]{8})(\|(?<BeginTime>([0-9]{0,14}\.)?[0-9]{2}:[0-9]{2}:[0-9]{2})\|(?<EndTime>([0-9]{0,14}\.)?[0-9]{2}:[0-9]{2}:[0-9]{2}))?");
                //?(\|(?<BeginTime>?([0-9]{0,14}\.)[0-9]{2}:[0-9]{2}:[0-9]{2}))\|(?<EndTime>?([0-9]{0,14}\.[0-9]{2}:[0-9]{2}:[0-9]{2})))
                if (reg1.IsMatch(myVal))
                {
                    oReturn = new INFOTYPE2002();
                    Match oMatch = reg1.Match(myVal);
                    oReturn.EmployeeID = oMatch.Groups["EmployeeID"].Value;
                    oReturn.AttendanceType = oMatch.Groups["AttendanceType"].Value;
                    oReturn.BeginDate = DateTime.ParseExact(oMatch.Groups["BeginDate"].Value, "yyyyMMdd", oCL);
                    oReturn.EndDate = DateTime.ParseExact(oMatch.Groups["EndDate"].Value, "yyyyMMdd", oCL);
                    if (oMatch.Groups["BeginTime"].Value != "")
                    {
                        oReturn.AllDayFlag = false;
                        oReturn.BeginTime = TimeSpan.Parse(oMatch.Groups["BeginTime"].Value);
                        oReturn.EndTime = TimeSpan.Parse(oMatch.Groups["EndTime"].Value);
                    }
                    else
                    {
                        oReturn.AllDayFlag = true;
                        oReturn.BeginTime = TimeSpan.MinValue;
                        oReturn.EndTime = TimeSpan.MinValue;
                    }
                }
                else
                {
                    throw new Exception("Incorrect format for INFOTYPE2002");
                }
                return oReturn;
            }
            return base.ConvertFrom(context, culture, value);
        }
    }
}
