using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.HR.TM.INFOTYPE
{
    public class INFOTYPE2002_Comparer : IComparer<INFOTYPE2002>
    {
        public INFOTYPE2002_Comparer()
        {
        }

        #region IComparer<INFOTYPE2002> Members

        public int Compare(INFOTYPE2002 x, INFOTYPE2002 y)
        {
            if (x.AttendanceType.CompareTo(y.AttendanceType) == 0)
            {
                if (x.BeginDate < y.BeginDate)
                {
                    return -1;
                }
                else if (x.BeginDate == y.BeginDate)
                {
                    if (x.BeginTime < y.BeginTime)
                    {
                        return -1;
                    }
                    else if (x.BeginTime > y.BeginTime)
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    return 1;
                }
            }
            else
            {
                return x.AttendanceType.CompareTo(y.AttendanceType);
            }
        }

        #endregion
    }
}
