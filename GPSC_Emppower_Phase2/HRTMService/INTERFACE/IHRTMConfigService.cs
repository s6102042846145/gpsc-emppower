﻿using System.Collections.Generic;
using System;
using System.Data;
using ESS.HR.TM.CONFIG;
using ESS.HR.TM.DATACLASS;
using ESS.EMPLOYEE;

namespace ESS.HR.TM.INTERFACE
{
    public interface IHRTMConfigService
    {
        List<OTWorkType> GetAllOTWorkType(string Language);
        List<AbsenceType> GetAllAbsenceTypeList();
        List<AbsenceType> GetAbsenceTypeList(string EmployeeID);
        List<AbsenceType> GetAbsenceTypeList(string EmployeeID, string LanguageCode);
        List<AbsenceType> GetAbsenceTypeList(EmployeeData oEmp, string LanguageCode);
        AbsenceType GetAbsenceType(string EmployeeID, string AbsenceType, string LanguageCode);
        void SaveAbsenceTypeList(List<AbsenceType> data);
        List<AbsenceQuotaType> GetAbsenceQuotaTypeList();
        List<AbsenceQuotaType> GetAbsenceQuotaTypeList(string LanguageCode);
        void SaveAbsenceQuotaTypeList(List<AbsenceQuotaType> Data);
        AbsenceQuotaType GetAbsenceQuotaType(string EmployeeID, string QuotaTypeCode, string LanguageCode);
        AbsenceCreatingRule GetAbsenceCreatingRule(EmployeeData oEmp, string AbsenceTypeCode);
        AbsenceCreatingRule GetAbsenceCreatingRule(EmployeeData oEmp, string AbsenceTypeCode, decimal AbsenceDays);
        AbsenceCreatingRule GetAbsenceCreatingRule(string EmpID, string AbsenceTypeCode);
        List<AbsenceAssignment> GetAbsenceAssignmentList(string AbsAttGrouping, string AbsenceType, string SubKey);
        AbsenceCancelRule GetAbsenceCancelRule(string EmployeeID, string AbsenceTypeCode);
        AbsenceCancelRule GetAbsenceCancelRule(EmployeeData oEmp, string AbsenceTypeCode);
        List<AttendanceType> GetAllAttendanceTypeList();
        List<AttendanceType> GetAttendanceTypeList(string EmployeeID, string LanguageCode);
        List<AttendanceType> GetAttendanceTypeList(EmployeeData oEmp, string LanguageCode);
        void SaveAttendanceTypeList(List<AttendanceType> Data);
        AttendanceType GetAttendanceType(string EmployeeID, string AttendanceType, string LanguageCode);
        AttendanceCreatingRule GetAttendanceCreatinigRule(EmployeeData oEmp, string AttendanceTypeCode);
        AttendanceCreatingRule GetAttendanceCreatinigRule(string EmpID, string AttendanceTypeCode);

        List<OTItemType> GetAllOTItemType();
        OTItemType GetOTItemType(int OTItemTypeID);

        List<CountingRule> GetAllCountingRuleList();
        void SaveCountingRuleList(List<CountingRule> Data);

        List<DeductionRule> GetAllDeductionRule();
        void SaveDeductionRule(List<DeductionRule> Data);
        DeductionRule GetDeductionRule(string subGroupSetting, string subAreaSetting, string absAttGrouping, string AbsenceType);
        AbsenceQuotaType GetAbsenceQuotaType(string sAbsenceQuotaKey);

        DailyOTDeletingRule GetDailyOTDeletingRule();
        string GetAttendanceFlowSetting(string AbsAttGrouping, string AttendanceTypeCode);

        SubstituteCreatingRule GetSubstituteCreatingRule();

        //AddBy: Ratchatawan W. (2012-02-22)
        string GetCommonText(string Category, string Code, string Language);

        //AddBy: Ratchatawan W. (2012-11-08)
        DataTable GetClockinLateClockoutEarly(string EmployeeID, DateTime BeginDate, DateTime EndDate);
        //AddBy: Ratchatawan W. (2012-11-08)
        void SaveClockinLateClockoutEarly(List<ClockinLateClockoutEarly> List, DateTime BeginDate, DateTime EndDate);
        //AddBy: Ratchatawan W. (2012-11-16)
        void DeleteSubstitution(string EmployeeID, DateTime BeginDate, DateTime EndDate);
        //AddBy: Ratchatawan W. (2012-12-03)
        List<DailyFlexTime> GetDailyFlexTimeByFlexCode(string FlexCode);
        DailyFlexTime GetDailyFlexTimeMinForCalculateByFlexCode(string FlexCode);
        DailyFlexTime GetTimesheetChangeFlexTime(string FlexCode, string ClockInTime);

        List<DutyTime> GetActivedDutyTime();
        int GetTimeManagementSubjectID();
        List<AreaWorkschedule> GetAreaWorkscheduleAllByArea(string Area, string SubArea);
        List<AreaWorkschedule> GetAreaWorkscheduleAreaCode(string Area, string SubArea);
        List<AreaWorkschedule> GetAreaWorkscheduleAllByWFRule(string Area, string SubArea, string WFRule);
        bool ShowTimeAwareLink(int LinkID);
    }
}
