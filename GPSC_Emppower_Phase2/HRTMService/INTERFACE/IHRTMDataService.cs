﻿using System;
using System.Collections.Generic;
using ESS.HR.TM.INFOTYPE;
using ESS.HR.TM.DATACLASS;
using System.Data;
using ESS.EMPLOYEE;
using ESS.TIMESHEET;
using ESS.EMPLOYEE.CONFIG.TM;

namespace ESS.HR.TM.INTERFACE
{
    public interface IHRTMDataService
    {
        void MarkUpdate(string EmployeeID, string DataCategory, string RequestNo, bool isMarkIn);
        bool CheckMark(string EmployeeID, string DataCategory);
        bool CheckMark(string EmployeeID, string DataCategory, string ReqNo);
        List<INFOTYPE2006> GetAbsenceQuota(string EmployeeID);
        List<INFOTYPE2006> GetAbsenceQuota(string EmployeeID, bool IncludeFuture);
        List<INFOTYPE2006> GetAbsenceQuota(string EmployeeID, DateTime CheckDate, bool IncludeFuture);
        List<INFOTYPE2006> GetAbsenceQuota(string EmployeeID1, string EmployeeID2, DateTime CheckDate, bool IncludeFuture);
        List<INFOTYPE2001> GetAbsenceList(string EmployeeID, DateTime BeginDate, DateTime EndDate);
        List<INFOTYPE2001> GetAbsenceList(string EmployeeID, string EmployeeID2, DateTime BeginDate, DateTime EndDate);
        List<INFOTYPE2001> GetAbsenceListForCheckData(string EmployeeID, DateTime BeginDate, DateTime EndDate);
        List<INFOTYPE2006> GetAbsenceQuotaForDeduction(string EmployeeID1, string Employee2, DateTime CheckDate);
        AbsAttCalculateResult CalculateAbsAttEntry(string EmployeeID, string SubType, DateTime BeginDate, DateTime EndDate, TimeSpan BeginTime, TimeSpan EndTime, bool FullDay, bool CheckQuota, bool TruncateDayOff);
        AbsAttCalculateResult CalculateAbsAttEntry(string EmployeeID, string SubType, DateTime BeginDate, DateTime EndDate, TimeSpan BeginTime, TimeSpan EndTime, bool FullDay, bool CheckQuota, bool TruncateDayOff, string CompCode);
        AbsAttCalculateResult CalculateAbsAttEntry(string EmployeeID, string SubType, DateTime BeginDate, DateTime EndDate, TimeSpan BeginTime, TimeSpan EndTime, bool FullDay, bool CheckQuota, bool TruncateDayOff, DailyWS oDailyWS);
        INFOTYPE2001 GetINFOTYPE2001ByRequestNo(string RequestNo);
        void SaveAbsence(List<INFOTYPE2001> data);
        void SaveAbsence(List<INFOTYPE2001> data, string CompCode);

        List<INFOTYPE2002> GetAttendanceList(string EmployeeID, DateTime BeginDate, DateTime EndDate);
        void SaveAttendance(List<INFOTYPE2002> data);
        void SaveAttendance(List<INFOTYPE2002> data, string CompCode);

        void SaveSubstitutionLog(System.Data.DataRow data);
        void SaveSubstitutionLog(List<INFOTYPE2003> data);
        void CancelSubstitutionLog(System.Data.DataRow data);

        void SaveSubstitution(List<INFOTYPE2003> data);
        void SaveOTList1(List<SaveOTData> data, bool IsMark, List<string> RequestNoList, bool deleteItemNotInList);
        void SaveOTList(List<SaveOTData> data, bool IsMark);
        void SaveOTList(List<SaveOTData> data, bool IsMark, List<string> requestNoList);
        void SaveOTList(List<SaveOTData> data, bool IsMark, List<string> requestNoList, bool deleteItemNotInList);
        PlannedOT LoadFirstPlannedOT(string EmployeeID, DateTime BeginDate, DateTime EndDate);
        List<PlannedOT> LoadPlannedOT(string EmployeeID, DateTime BeginDate, DateTime EndDate);
        List<DailyOT> LoadDailyOT(string EmployeeID, DateTime BeginDate, DateTime EndDate);
        List<SaveOTData> LoadOTData(string EmployeeID, DateTime BeginDate, DateTime EndDate, string OnlyType, bool IsOnlyCompleted);
        List<SaveOTData> LoadOTData(string EmployeeID, DateTime BeginDate, DateTime EndDate, string OnlyType, bool IsOnlyCompleted, bool IsOnlyNotSummary);
        List<DailyOTLog> LoadDailyOTLog(string RequestNo, string EmployeeID, DateTime BeginDate, DateTime EndDate);
        List<DailyOTLog> LoadDailyOTLog(string EmployeeID, DateTime BeginDate, DateTime EndDate);
        List<DailyOTLog> LoadDailyOTLog(string EmployeeID, string BeginDate);
        List<DailyOTLog> LoadDailyOTLog(string EmployeeID, DateTime BeginDate, DateTime EndDate, DailyOTLogStatus _DailyOTLogStatus);
        List<DailyOTLog> LoadDailyOTLog(string EmployeeID, string BeginDate, string EndDate);
        List<SaveOTData> LoadOTDataByAssigner(string AssignerID, DateTime BeginDate, DateTime EndDate, string OnlyType, bool IsOnlyCompleted, bool IsOnlyNotSummary);

        List<DailyOT> LoadOTFromPM(string EmployeeID, string Period, int DayStart, int DayEnd);
        void DeleteDailyOT(DailyOT daily);
        List<LeavePlan> GetLeavePlanList(string EmployeeID, DateTime BeginDate, DateTime EndDate);
        int GetNextYearAbsenceQuota(string EmployeeID);
        void SaveLeavePlanList(string EmployeeID, int Year, List<LeavePlan> leavePlanList);

        //CHAT 2011-12-01
        void SaveOTLog(System.Data.DataTable data, bool IsMark);
        void SaveDailyOTLogService(DataTable data);
        void DeleteOTLogByRefRequestNo(System.Data.DataTable data);
        bool OverlapDialyOTLog(string EmployeeID, DateTime BeginDate, DateTime EndDate, String RequestNo);
        void PostOTLog(List<DailyOTLog> oDailyOTLog);
        bool IsCompleteCancleDailyOT(String EmployeeID, DateTime BeginDate, DateTime EndDate);
        void SaveOTLogStatus(System.Data.DataTable data);
        List<DailyOTLog> LoadDailyOTLog(string EmployeeID);
        List<TimeEval> LoadTimeEval(DateTime BeginDate, DateTime EndDate);
        void SaveInLateOutEarly(List<ClockinLateClockoutEarly> List, string EmployeeID, DateTime BeginDate, DateTime EndDate);
        void SaveTimeEval(List<TimeEval> TimeEvalList);
        List<DailyOTLog> LoadDailyOTLog(int Month, int Year);
        void UpdateDailyOTLog(List<DailyOTLog> DailyOTLogList);
        List<TimeEval> LoadTimeEval(string EmployeeID, DateTime ClockIN, DateTime ClockOUT);

        //AddBy: Ratchatawan W. (2012-02-23)
        List<DailyOTLog> LoadOTLogByCriteria(string EmployeeList, string StatusList, DateTime BeginDate, DateTime EndDate);
        DataTable GetSummaryOTInMonth(string EmployeeID, DateTime BeginDate, DateTime EndDate, string RequestNo);
        DataTable GetOTOverLimitPerWeek(string Employeeid, DateTime BeginDate); /* Add by Han on 15/03/2019 : created interface method */
        DataTable GetOTOverLimitPerMonth(string Employeeid, DateTime BeginDate); /* Add by Han on 10/05/2019 : created interface method */
        DataTable GetOTOnHoliday(string Employeeid, DateTime BeginDate);

        //AddBy: Ratchatawan W. (2012-11-16)
        void DeleteSubstitution(string EmployeeID, DateTime BeginDate, DateTime EndDate);

        void SaveDelegateByAbsence(DataTable data);
        void DeleteDelegateByAbsence(INFOTYPE2001 data);
        List<DelegateData> GetDelegateByAbsence(string EmployeeId, string DelegateToId, DateTime BeginDate, DateTime EndDate);

        void SaveSubstitutionCustomTime(INFOTYPE2003 data);
        List<INFOTYPE2006> GetAbsenceQuotaList(string EmployeeID1, string EmployeeID2, DateTime BeginDate, DateTime EndDate);

        void SaveDutyPaymentLog(DataTable data, bool IsDeleteOnly);
        List<DutyPaymentLog> GetDutyPaymentLog(string strEmployeeID, DateTime dtBeginDate, DateTime dtEndDate);
        void PostDutyPaymentDayWork(string strEmployeeID, Dictionary<DateTime, decimal> dictData);
        void PostDutyPaymentDayOff(string strEmployeeID, Dictionary<DateTime, decimal> dictData);

        void MarkTimesheet(string EmployeeID, string ApplicationKey, string SubKey1, string SubKey2, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark, bool isCheck, string Detail);
        void MarkTimesheet(string Employee, string Application, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark, bool isCheck, string Detail);
        void MarkTimesheet(List<TimesheetData> oTimesheetData, bool isMark, bool isCheck);

        List<INFOTYPE2003_LOG> LoadINFOTYPE2003LOG(string RequestNo);
        List<DATACLASS.CollisionControl> LoadApplicationCollision(string ApplicationKey, string SubKey1, string SubKey2);
        List<TimesheetData> LoadTimesheetCollision(string EmployeeID, DateTime BeginDate, DateTime EndDate, DATACLASS.CollisionControl Key);
        ICollisionable LoadAdditionalDataCollisionable(string ApplicationKey);
        List<TimeElement> LoadArchiveEvent(string EmployeeID, DateTime BeginDate, DateTime EndDate, int RecordCount);
        List<CardSetting> LoadCardSetting(string EmployeeID, DateTime BeginDate, DateTime EndDate);
        List<CardSetting> LoadCardSetting(string EmployeeID);
        List<INFOTYPE2001> GetTM_AbsenceList(string EmployeeID, DateTime BeginDate, DateTime EndDate);
        List<INFOTYPE2002> GetTM_AttendanceList(string EmployeeID, DateTime BeginDate, DateTime EndDate);
        List<TimePair> GetTM_TimePairList(string EmployeeID, DateTime BeginDate, DateTime EndDate);
        List<TM_Timesheet> GetTM_Timesheet(string EmployeeID, DateTime BeginDate, DateTime EndDate);
        List<TimePair> LoadTimePairArchiveMappingByUser(string EmployeeID, DateTime BeginDate, DateTime EndDate);
        List<TimePairArchive> LoadTimePairArchive(DateTime BeginDate, DateTime EndDate);
        List<TimePairArchive> LoadTimePairArchive(string EmpID, DateTime BeginDate, DateTime EndDate);
        void MappingTimePairInLateOutEarlyID(string EmployeeID, DateTime BeginDate, DateTime EndDate);
        void SaveTimePair(string EmployeeID, DateTime BeginDate, DateTime EndDate, List<TimePair> list);
        void SaveTimePairArchiveMappingByUser(List<TimePair> TimePairList);
        void SaveTimePairArchiveMappingByUser(List<TimePair> TimePairList,string RequestNo);
        void SaveArchiveEvent(string EmployeeID, DateTime BeginDate, DateTime EndDate, List<TimeElement> Data);
        void SaveArchiveTimePair(string EmployeeID, DateTime BeginDate, DateTime EndDate, List<TimePair> list);
        DateTime LoadArchiveDate(string EmployeeID);
        void PostTimePair(List<TimePair> Data);
        List<TimeElement> GetTimeElement(string CardNo, DateTime BeginDate, DateTime EndDate, int RecordCount);
        void SaveLG_TIMEREQUEST(List<INFOTYPE2001> data, string type);
        void SaveLG_TIMEREQUEST(List<INFOTYPE2002> data, string type);
        bool CheckDailyOTLogIsView(string RequestNo);
        List<AreaWorkscheduleLog> GetAreaWorkscheduleLogByRequestNo(string RequestNo);
        List<AreaWorkscheduleLog> GetAreaWorkscheduleLogByPeriod(DateTime BeginDate, DateTime EndDate,string CreatorID);
        void SaveAreaWorkscheduleLog(List<AreaWorkscheduleLog> data, bool IsMark);
        List<ReportGroupAdmin> GetGroupReportAdmin(int reportGroupId);
        DataTable GetAbsenceReportByEmployeeXML(string EmployeeXML, string AType, DateTime BeginDate, DateTime EndDate, EmployeeData oEmp);
        List<OTLogAll> GetOTLogAll(string year,string month);
        List<PeriodOTSumary> GetPeiodReportOTSummary(string category, DateTime date_now);
        List<DbAbsenceSickLeave> GetAbsenceSickLeaveSummary(int year, string abs_group);
        List<MasterWorkLocation> GetMasterWorklocation(int year);
        List<OrgUnitEmployeeByRole> GetListOrgUnitByUserRole(string employeeId, string userRole, string companyCode);
        List<OrgUnitEmployeeByRole> GetListOrgUnitNameByUserRole(List<OrgUnitEmployeeByRole> list_org_unit);
        List<string> GetEmployeeInOrganization(string[] list_org);
        List<string> GetEmployeeInOrganization(string orgId);
        List<PaySlipEmployeeInOrganization> GetPaySlipEmployeeInOrganization(List<string> list_employee);
        List<DbOTReason> GetDashbaordOTReason(List<string> list_emp, int year, int month);
        List<DbOTWorkType> GetListOTWorkType();
        OrganizationInfotype1000 GetNameOrganizationInfotype1000(string orgId);
        List<DbOvertimeInMonth> GetTransactionPayOvertimeByEmployeeInOrganization(List<string> list_emp, int year, int month);
        DashbaordOvertimeAndSalaryByMonth GetSalaryOrOvertimeInYear(List<string> list_emp, int year);
        DashbaordOTSalary6Months GetDashbaordOTSalary6Months(List<DateTime> list_date, List<string> list_emp);
        DashbaordOTHours6Months GetDashbaordOTHours6Months(List<DateTime> list_date, List<string> list_emp);
        DashboardSummarySalaryOvertimePayByYear GetDashboardSummarySalaryOvertimePayByYear(List<string> list_emp, int begin_year, int end_year);
        DashboardSummarySalaryOvertimeHourByYear GetDashboardSummarySalaryOvertimeHourByYear(List<string> list_emp, int begin_year, int end_year);
        OTEmployeeByLevel GetDashboardOTEmployeeByLevel(List<string> list_emp, int begin_year, int end_year);
        OTEmployeeByBath GetDashboardOTEmployeeByBath(List<string> list_emp, int begin_year, int end_year);
        List<OrgUnitTree> GetOrgOTUnitTree(string employeeId, DateTime begin_date,DateTime end_date);
        decimal GetOTSummaryInMonth(string EmployeeID, DateTime BeginDate);
        DataTable GetOTTrackingStatusReport(DateTime BeginDate, DateTime EndDate, string EmpID, string OrgUnitList, string CompanyCode, string LanguageCode);
        DataTable GetDutyTrackingStatusReport(DateTime BeginDate, DateTime EndDate, string EmpID, string OrgUnitList, string CompanyCode, string LanguageCode);
        DataTable GetAttendanceMonthlyReport(DateTime BeginDate, DateTime EndDate, string EmpID, string OrgUnitList, string CompanyCode, string LanguageCode);
        DataTable GetTimeScanReport(DateTime BeginDate, DateTime EndDate, string EmpID, string OrgUnitList, string CompanyCode, string LanguageCode);
        DataTable GetWorkingSummaryReport(DateTime BeginDate, DateTime EndDate, string EmpID, string OrgUnitList, string CompanyCode, string LanguageCode);
    }
}
