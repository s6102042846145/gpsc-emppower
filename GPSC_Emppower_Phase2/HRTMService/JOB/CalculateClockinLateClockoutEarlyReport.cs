﻿using System;
using System.Collections.Generic;
using ESS.JOB.ABSTRACT;
using ESS.JOB.INTERFACE;
using System.Globalization;
using ESS.WORKFLOW;
using System.Text;
using System.Threading.Tasks;
using ESS.EMPLOYEE;

namespace ESS.HR.TM.JOB
{
    public class CalculateClockinLateClockoutEarlyReport : AbstractJobWorker
    {
        CultureInfo oCL = new CultureInfo("en-US");
        public CalculateClockinLateClockoutEarlyReport()
        {
        }

        public override List<ITaskWorker> LoadTasks()
        {
            HRTMManagement oHRTMManagement = HRTMManagement.CreateInstance(CompanyCode);

            List<ITaskWorker> oReturn = new List<ITaskWorker>();
            DateTime oArchiveDate = DateTime.Now;
            DateTime BeginDate, EndDate;
            List<string> EmployeeIDList = new List<string>();
            List<EmployeeData> EmployeeList = new List<EmployeeData>();

            BeginDate = oArchiveDate.Date.AddDays(Convert.ToInt32(Params[0].Value));
            EndDate = oArchiveDate.Date.AddDays(1).AddSeconds(-1);

            if (!CheckForceEmployeeParam(Params[1].Value, out EmployeeIDList))
            {
                foreach (EmployeeData emp in EmployeeManagement.CreateInstance(CompanyCode).GetAllActiveEmployeeInINFOTYPE0001(DateTime.Now))
                    EmployeeIDList.Add(emp.EmployeeID);
            }


            CalculateClockinLateClockoutEarlyReportTask oTask;
            int index = 0;
            foreach (string EmployeeID in EmployeeIDList)
            {
                oTask = new CalculateClockinLateClockoutEarlyReportTask(EmployeeID, BeginDate, EndDate);
                oTask.TaskID = index++;
                oReturn.Add(oTask);
            }
            return oReturn;
        }

        public bool CheckForceEmployeeParam(string param, out List<string> ListEmployeeID)
        {
            bool oReturn = true;
            ListEmployeeID = new List<string>();
            if (param == "*")
                oReturn = false;
            else
            {
                try
                {
                    foreach (string item in param.Split(','))
                        if (item.Contains("-"))
                        {
                            for (int i = Convert.ToInt32(item.Split('-')[0]); i <= Convert.ToInt32(item.Split('-')[1]); i++)
                            {
                                ListEmployeeID.Add(i.ToString("00000000"));
                            }
                        }
                        else
                        {
                            ListEmployeeID.Add(item.Trim());
                        }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return oReturn;
        }
    }
}
