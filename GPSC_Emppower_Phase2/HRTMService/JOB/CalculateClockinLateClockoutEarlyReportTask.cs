﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.DATA;
using ESS.JOB.ABSTRACT;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.TIMESHEET;
using ESS.HR.TM.INFOTYPE;
using ESS.HR.TM.DATACLASS;

namespace ESS.HR.TM.JOB
{
    public class CalculateClockinLateClockoutEarlyReportTask : AbstractTaskWorker
    {
        static CultureInfo oCL = new CultureInfo("en-US");

        DateTime BeginDate;
        DateTime EndDate;
        List<EmployeeData> EmployeeList;
        List<string> EmployeeIDList;

        List<TimePair> timePairArchiveList;
        List<INFOTYPE2002> attendanceList;
        List<INFOTYPE2001> absenceList;
        MonthlyWS MWS;
        DailyWS DWS;
        bool AbsenceOrAttendanceAllday = false;
        INFOTYPE2002 attendance;
        INFOTYPE2001 absence;
        List<TimePair> timePairArchiveItems;
        string EmployeeID;

        public CalculateClockinLateClockoutEarlyReportTask(string __employeeID, DateTime _beginDate, DateTime _endDate)
        {
            BeginDate = _beginDate;
            EndDate = _endDate;
            EmployeeID = __employeeID;
        }


        public override void Run()
        {
            WORKFLOW.WorkflowPrinciple.SetCurrentPrincipal(new EmployeeData(EmployeeID, BeginDate) { CompanyCode = CompanyCode });
            HRTMManagement oHRTMManagement = HRTMManagement.CreateInstance(CompanyCode);

            Console.WriteLine(string.Format("+++ EmployeeID: {0} From {1} To {2} +++", EmployeeID, BeginDate.ToString("dd/MM/yyyy", oCL), EndDate.ToString("dd/MM/yyyy", oCL)));
            List<ClockinLateClockoutEarly> oReturn;
            DateTime dtCurrent;
            bool flag = true;
            ClockinLateClockoutEarly newData;
            INFOTYPE0007 info0007;
            EmployeeData empData;

            List<TimeElement> oTimeElement =  HRTMManagement.CreateInstance(CompanyCode) .LoadTimeElement(EmployeeID, BeginDate, EndDate);
            List<TimePair> pairs = HRTMManagement.CreateInstance(CompanyCode).MatchingClockNew(EmployeeID, oTimeElement, true, BeginDate, EndDate, new Dictionary<DateTime, string>());

            for (dtCurrent = BeginDate; dtCurrent <= EndDate; dtCurrent = dtCurrent.AddDays(1))
            {
                oReturn = new List<ClockinLateClockoutEarly>();

                //Reset variable
                MWS = null;
                DWS = null;
                flag = true;
                empData = new EmployeeData(EmployeeID, dtCurrent);
                newData = new ClockinLateClockoutEarly();
                newData.Date = dtCurrent;
                newData.EmployeeID = empData.EmployeeID;
                AbsenceOrAttendanceAllday = false;


                #region Load Monthly Work Schedule
                if (MWS == null || MWS.WS_Month != dtCurrent.Month || MWS.WS_Year != dtCurrent.Year)
                {
                    try
                    {
                        MWS = MonthlyWS.GetCalendar(empData.EmployeeID, dtCurrent, true);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("### No MonthlyWS OF" + dtCurrent.ToString("MM/yyyy", oCL) + " ###");
                        MWS = MonthlyWS.GetCalendar(EmployeeID, dtCurrent.Year, dtCurrent.Month);
                    }
                }
                #endregion

                //Load Daily Work Schedule
                try
                {
                    DWS = empData.GetDailyWorkSchedule(dtCurrent);
                    info0007 = new EmployeeData().GetEmployeeWF(empData.EmployeeID, dtCurrent);


                    timePairArchiveList = pairs.FindAll(delegate (TimePair t) { return (t.Date >= dtCurrent && t.Date <= dtCurrent.AddDays(1).AddSeconds(-1)); });//TimesheetManagement.LoadTimepairArchive(empData.EmployeeID, dtCurrent, dtCurrent.AddDays(1).AddSeconds(-1));

                    #region Set Employee Type
                    if (DWS.DailyWorkscheduleCode != null)
                    {
                        if (DWS.DailyWorkscheduleCode == "OFF")
                            newData.Type = string.Format("{0}", DWS.DailyWorkscheduleCode);
                        else if (DWS.DailyWorkscheduleCode.Contains("FL"))
                        {
                            if (!string.IsNullOrEmpty(MWS.GetWSCode(dtCurrent.Day, false)))
                                newData.Type = string.Format("{0}", "HOL");
                            else
                                newData.Type = string.Format("{0} ({1}:{2}-{3}:{4})", info0007.WFRule, DWS.WorkBeginTime.Hours.ToString("00"), DWS.WorkBeginTime.Minutes.ToString("00"), DWS.WorkEndTime.Hours.ToString("00"), DWS.WorkEndTime.Minutes.ToString("00"));
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(MWS.GetWSCode(dtCurrent.Day, false)))
                                newData.Type = string.Format("{0}", "HOL");
                            else
                                newData.Type = string.Format("{0} ({1}:{2}-{3}:{4})", DWS.DailyWorkscheduleCode, DWS.WorkBeginTime.Hours.ToString("00"), DWS.WorkBeginTime.Minutes.ToString("00"), DWS.WorkEndTime.Hours.ToString("00"), DWS.WorkEndTime.Minutes.ToString("00"));
                        }
                    }
                    #endregion

                    INFOTYPE2001 oINFOTYPE2001 = new INFOTYPE2001();
                    INFOTYPE2002 oINFOTYPE2002 = new INFOTYPE2002();

                    //Get all attendance
                    List<INFOTYPE2002> attendanceAll = oINFOTYPE2002.GetData(empData, dtCurrent.Date, dtCurrent.Date);
                    INFOTYPE2002 attendanceMatchAllDay; List<INFOTYPE2002> attendanceMatchHalfDay;
                    //Get all absence
                    List<INFOTYPE2001> absenceAll = oINFOTYPE2001.GetData(empData, dtCurrent.Date, dtCurrent.Date.AddDays(1).AddMinutes(-1));
                    INFOTYPE2001 absenceMatchAllDay; List<INFOTYPE2001> absenceMatchHalfDay;

                    AbsenceOrAttendanceAllday = false;
                    #region Absence or Attendance Allday
                    attendanceMatchAllDay = attendanceAll.Find(delegate (INFOTYPE2002 e) { return (e.AttendanceDays > Convert.ToDecimal(0.5)) && e.AttendanceType != "2040" && e.AttendanceType != "2050"; });
                    absenceMatchAllDay = absenceAll.Find(delegate (INFOTYPE2001 e) { return e.AbsenceDays > Convert.ToDecimal(0.5); });
                    if (attendanceMatchAllDay != null)
                    {
                        newData.IsRemark = true;
                        newData.CategoryCode = "ATTENDANCETYPE";
                        newData.TextCode = attendanceMatchAllDay.AttendanceType;
                        newData.BeginDate = attendanceMatchAllDay.BeginDate.Add(attendanceMatchAllDay.BeginTime).ToString("ddMMyyyy HH:mm:ss", oCL);
                        newData.EndDate = attendanceMatchAllDay.EndDate.Add(attendanceMatchAllDay.EndTime).ToString("ddMMyyyy HH:mm:ss", oCL);
                        newData.PayrollDays = attendanceMatchAllDay.PayrollDays;
                        AbsenceOrAttendanceAllday = true;
                    }
                    if (absenceMatchAllDay != null)
                    {
                        newData.IsRemark = true;
                        newData.CategoryCode = "ABSENCETYPE";
                        newData.TextCode = absenceMatchAllDay.AbsenceType;
                        newData.BeginDate = absenceMatchAllDay.BeginDate.Add(absenceMatchAllDay.BeginTime).ToString("ddMMyyyy HH:mm:ss", oCL);
                        newData.EndDate = absenceMatchAllDay.EndDate.Add(absenceMatchAllDay.EndTime).ToString("ddMMyyyy HH:mm:ss", oCL);
                        newData.PayrollDays = absenceMatchAllDay.PayrollDays;
                        AbsenceOrAttendanceAllday = true;
                    }
                    #endregion

                    #region Absence or Attendance Half-Day
                    attendanceMatchHalfDay = attendanceAll.FindAll(delegate (INFOTYPE2002 e) { return (e.AttendanceDays == Convert.ToDecimal(0.5)) || e.AttendanceType == "2040" || e.AttendanceType == "2050"; });
                    absenceMatchHalfDay = absenceAll.FindAll(delegate (INFOTYPE2001 e) { return e.AbsenceDays == Convert.ToDecimal(0.5); });
                    if (attendanceMatchHalfDay != null)
                    {
                        foreach (INFOTYPE2002 item in attendanceMatchHalfDay)
                        {
                            newData.IsRemark = true;
                            newData.CategoryCode += "ATTENDANCETYPE" + ",";
                            newData.TextCode += item.AttendanceType + ",";
                            newData.BeginDate += item.BeginDate.Add(item.BeginTime).ToString("ddMMyyyy HH:mm:ss", oCL) + ",";
                            if (item.EndDate.Add(item.EndTime).Date > item.BeginDate.Date)
                                newData.EndDate += item.BeginDate.Add(item.EndTime).AddDays(-1).ToString("ddMMyyyy HH:mm:ss", oCL) + ",";
                            else
                                newData.EndDate += item.EndDate.Add(item.EndTime).ToString("ddMMyyyy HH:mm:ss", oCL) + ",";
                            newData.PayrollDays += item.PayrollDays;
                        }
                    }
                    if (absenceMatchHalfDay != null)
                    {
                        if (attendanceMatchHalfDay != null)
                        {
                            newData.CategoryCode += ",";
                            newData.TextCode += ",";
                            newData.BeginDate += ",";
                            newData.EndDate += ",";
                        }
                        foreach (INFOTYPE2001 item in absenceMatchHalfDay)
                        {
                            newData.IsRemark = true;
                            newData.CategoryCode += "ABSENCETYPE" + ",";
                            newData.TextCode += item.AbsenceType + ",";
                            newData.BeginDate += item.BeginDate.Add(item.BeginTime).ToString("ddMMyyyy HH:mm:ss", oCL) + ",";
                            if (item.EndDate.Add(item.EndTime).Date > item.BeginDate.Date)
                                newData.EndDate += item.BeginDate.Add(item.EndTime).AddDays(-1).ToString("ddMMyyyy HH:mm:ss", oCL) + ",";
                            else
                                newData.EndDate += item.EndDate.Add(item.EndTime).ToString("ddMMyyyy HH:mm:ss", oCL) + ",";
                            newData.PayrollDays += item.PayrollDays;
                        }
                    }

                    if (attendanceMatchHalfDay.Count > 1 || absenceMatchHalfDay.Count > 1)
                        AbsenceOrAttendanceAllday = true;

                    if (attendanceMatchHalfDay.Count == 1 && absenceMatchHalfDay.Count == 1)
                        AbsenceOrAttendanceAllday = true;
                    #endregion

                    //Time Evaluate Class
                    //2: Using Clock time
                    //0: Not Using Clock time
                    if (info0007.TimeEvaluateClass == "1")
                    {
                        if (timePairArchiveList.Count > 0)
                        {
                            #region If employee have Timepair Achive
                            timePairArchiveItems = timePairArchiveList.FindAll(delegate (TimePair e) { return e.Date == dtCurrent.Date; });
                            if (timePairArchiveItems != null)
                            {
                                foreach (TimePair item in timePairArchiveItems)
                                {

                                    if (item.ClockIN != null)
                                    {
                                        newData.ClockIn = DateTime.ParseExact(item.ClockIN.EventTime.ToString("dd/MM/yyyy HH:mm", oCL), "dd/MM/yyyy HH:mm", oCL);
                                        newData.IsPlusClockIn = (item.ClockIN.EventTime.Date < dtCurrent.Date);
                                    }

                                    if (item.ClockOUT != null)
                                    {
                                        newData.ClockOut = DateTime.ParseExact(item.ClockOUT.EventTime.ToString("dd/MM/yyyy HH:mm", oCL), "dd/MM/yyyy HH:mm", oCL);
                                        newData.IsPlusClockOut = (item.ClockOUT.EventTime.Date > dtCurrent.Date);
                                    }

                                    #region If clock date is not dayoff
                                    if (!DWS.IsDayOff)
                                    {
                                        if (!CheckFlex(dtCurrent, DWS, item, info0007, absenceMatchHalfDay, attendanceMatchHalfDay, ref newData))
                                        {
                                            #region Check ClockInLate,ClockoutEarly,ClockAbnormal for employee that is not flex
                                            //If clock-in is null
                                            if (item.ClockIN == null || newData.ClockIn == DateTime.MinValue)
                                            {
                                                newData.ClockAbnormal = true;
                                                newData.ClockInLate = false;
                                                newData.ClockoutEarly = false;
                                            }
                                            else
                                            {
                                                //If clock-in late than work begin time in daily work schedule, means LATE
                                                if (!newData.IsPlusClockIn && newData.ClockIn.TimeOfDay > DWS.WorkBeginTime)
                                                    newData.ClockInLate = true;
                                            }
                                            //If clock-out is null
                                            if (item.ClockOUT == null || newData.ClockOut == DateTime.MinValue)
                                            {
                                                newData.ClockAbnormal = true;
                                                newData.ClockoutEarly = false;
                                            }
                                            else
                                            {
                                                //If clock-out early than work end time in daily work schedule, means EARLY
                                                DateTime checkEndDateTime = dtCurrent.Date.Add(DWS.WorkEndTime);

                                                if (newData.ClockOut < checkEndDateTime)
                                                    newData.ClockoutEarly = true;
                                            }
                                            #endregion
                                            IsHaveAttendanceOrAbsenceInHalfDay(absenceMatchHalfDay, attendanceMatchHalfDay, DWS, ref newData);
                                        }
                                    }
                                    #endregion
                                    #region If clock date is dayoff
                                    else
                                    {
                                        ClockinLateClockoutEarly newData2 = new ClockinLateClockoutEarly();
                                        newData2.EmployeeID = empData.EmployeeID;
                                        newData2.Date = dtCurrent;
                                        newData2.ClockIn = newData.ClockIn;
                                        newData2.ClockOut = newData.ClockOut;
                                        newData2.IsPlusClockIn = (newData.ClockIn.Date < dtCurrent.Date && newData.ClockIn.Date > DateTime.MinValue);
                                        newData2.IsPlusClockOut = (newData.ClockOut.Date > dtCurrent.Date && newData.ClockOut.Date > DateTime.MinValue);
                                        newData2.Type = string.Format("{0}", DWS.DailyWorkscheduleCode);

                                        if (!(newData2.ClockIn == DateTime.MinValue && newData2.ClockOut == DateTime.MinValue) && !(newData2.ClockIn > DateTime.MinValue && newData2.ClockOut > DateTime.MinValue))
                                            newData2.ClockAbnormal = true;

                                        oReturn.Add(newData2);
                                        flag = false;
                                    }
                                    #endregion
                                }
                            }
                            #endregion
                        }

                        //Clock none work (ClockIn && ClockOut is null && No Absence and Attendance)
                        if (newData.ClockIn == DateTime.MinValue && newData.ClockOut == DateTime.MinValue && !DWS.IsDayOffOrHoliday && !newData.IsRemark && string.IsNullOrEmpty(MWS.GetWSCode(dtCurrent.Day, false)))
                        {
                            newData.ClockNoneWork = true;
                            newData.ClockAbnormal = false;
                            newData.ClockInLate = false;
                            newData.ClockoutEarly = false;
                        }
                        //Clock Abnormal
                        if ((newData.ClockIn == DateTime.MinValue || newData.ClockOut == DateTime.MinValue) && !DWS.IsDayOffOrHoliday && string.IsNullOrEmpty(MWS.GetWSCode(dtCurrent.Day, false)) && newData.ClockNoneWork == false)
                            newData.ClockAbnormal = true;

                        //If employee have absence/attendance all day, set clock abnormal to false
                        if (AbsenceOrAttendanceAllday)
                        {
                            newData.ClockAbnormal = false;
                            newData.ClockInLate = false;
                            newData.ClockoutEarly = false;
                            newData.ClockNoneWork = false;
                        }

                        if (!string.IsNullOrEmpty(MWS.GetWSCode(dtCurrent.Day, false)))
                        {
                            newData.ClockInLate = false;
                            newData.ClockoutEarly = false;
                            newData.ClockNoneWork = false;
                        }
                    }

                    if (flag)
                    {
                        newData.CategoryCode = TrimDelimitFirstAndLast(newData.CategoryCode);
                        newData.TextCode = TrimDelimitFirstAndLast(newData.TextCode);
                        newData.BeginDate = TrimDelimitFirstAndLast(newData.BeginDate);
                        newData.EndDate = TrimDelimitFirstAndLast(newData.EndDate);
                        oReturn.Add(newData);
                    }


                }
                catch (Exception e)
                {
                    Console.WriteLine("FAIL >>> EmployeeID: " + empData.EmployeeID + "\r\nError: " + e.Message);
                    //break;
                }

                //Save ClockinLateClockoutEarly to database
                if (oReturn.Count > 0)
                {
                    try
                    {
                        HRTMManagement.CreateInstance(CompanyCode).SaveClockinLateClockoutEarly(oReturn, dtCurrent, dtCurrent.AddDays(1).AddSeconds(-1));
                        //Console.WriteLine(string.Format("SUCCESS >>> Date: {0}", dtCurrent.ToString("dd/MM/yyyy", oCL)));
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(string.Format("FAIL >>> Date: {0} \r\nError:{1}", dtCurrent.ToString("dd/MM/yyyy", oCL), ex.Message));
                    }
                }
            }

        }

        private string TrimDelimitFirstAndLast(string text)
        {
            text = text.Trim();
            text = text.Replace(",,", ",");
            if (text.LastIndexOf(',') == text.Length - 1)
                text = text.Remove(text.LastIndexOf(','));
            if (text.IndexOf(',') == 0)
                text = text.Substring(1, text.Length - 1);


            return text;
        }

        private static void IsHaveAttendanceOrAbsenceInHalfDay(List<INFOTYPE2001> info2001s, List<INFOTYPE2002> info2002s, DailyWS DWS, ref ClockinLateClockoutEarly data)
        {
            //เวลาพักเที่ยง
            DateTime dtNoon = data.Date.Add(DWS.WorkBeginTime);
            //เวลาเริ่มงานหลังพักเที่ยง
            DateTime dtAfterNoon = data.Date.Add(DWS.WorkEndTime);

            if (DWS.BreakCode == "NOON")
            {
                dtNoon = data.Date.Add(new TimeSpan(12, 0, 0));
                dtAfterNoon = data.Date.Add(new TimeSpan(13, 0, 0));
            }
            else
            {
                decimal _workingHours = DWS.WorkHours / 2;
                dtNoon = data.Date.Add(DWS.WorkBeginTime.Add(new TimeSpan(Convert.ToInt16(_workingHours), 0, 0)));
                dtAfterNoon = data.Date.Add(DWS.WorkBeginTime.Add(new TimeSpan(Convert.ToInt16(_workingHours), 0, 0)));
            }

            //If have attendance
            if (info2002s != null)
            {
                foreach (INFOTYPE2002 info2002 in info2002s)
                {
                    if (info2002.AttendanceType == "2040")// || info2002.AttendanceType == "2050")
                    {
                        info2002.BeginTime = DWS.WorkBeginTime;
                        info2002.EndTime = dtNoon.TimeOfDay;
                    }
                    if (info2002.AttendanceType == "2050")// || info2002.AttendanceType == "2050")
                    {
                        info2002.BeginTime = dtAfterNoon.TimeOfDay;
                        info2002.EndTime = DWS.WorkEndTime;
                    }

                    if (info2002.BeginTime == DWS.WorkBeginTime && data.ClockIn.TimeOfDay <= dtAfterNoon.TimeOfDay)
                        data.ClockInLate = false;
                    if (info2002.BeginTime == dtAfterNoon.TimeOfDay && data.ClockOut.TimeOfDay >= dtNoon.TimeOfDay)
                        data.ClockoutEarly = false;
                }
            }
            //If have absence
            if (info2001s != null)
            {
                foreach (INFOTYPE2001 info2001 in info2001s)
                {
                    //มีการรับรองเวลาช่วงเช้า แปลว่าเค้าไม่ได้มาทำงานสาย
                    if (info2001.BeginTime == DWS.WorkBeginTime && (data.ClockIn.TimeOfDay <= dtAfterNoon.TimeOfDay || data.ClockIn.TimeOfDay >= dtAfterNoon.TimeOfDay))
                        data.ClockInLate = false;

                    //มีการรับรองเวลาช่วงบ่าย และเวลาในการ Clockout > info2001.BeginTime(เวลาเริ่มต้น)
                    if (info2001.BeginTime == dtAfterNoon.TimeOfDay && data.ClockOut.TimeOfDay >= dtNoon.TimeOfDay)
                        data.ClockoutEarly = false;
                }
            }
        }


        public bool CheckFlex(DateTime dtCurrent, DailyWS DWS, TimePair timePair, INFOTYPE0007 info0007, List<INFOTYPE2001> info2001s, List<INFOTYPE2002> info2002s, ref ClockinLateClockoutEarly data)
        {
            DateTime checkEndDateTime;
            if (timePair.ClockIN != null)
                timePair.ClockIN.EventTime = DateTime.ParseExact(timePair.ClockIN.EventTime.ToString("dd/MM/yyyy HH:mm", oCL), "dd/MM/yyyy HH:mm", oCL);
            else
            {
                TimeElement te = new TimeElement();
                te.EventTime = DateTime.MinValue;
                timePair.ClockIN = te;
            }

            if (timePair.ClockOUT != null)
                timePair.ClockOUT.EventTime = DateTime.ParseExact(timePair.ClockOUT.EventTime.ToString("dd/MM/yyyy HH:mm", oCL), "dd/MM/yyyy HH:mm", oCL);
            else
            {
                TimeElement te = new TimeElement();
                te.EventTime = DateTime.MinValue;
                timePair.ClockOUT = te;
            }

            //TimeSpan tmFLX1ClockIN = new TimeSpan(8, 0, 0);
            //TimeSpan tmFLX1ClockOUT = new TimeSpan(17, 0, 0);
            //TimeSpan tmFLX2ClockIN = new TimeSpan(8, 30, 0);
            //TimeSpan tmFLX2ClockOUT = new TimeSpan(17, 30, 0);
            //TimeSpan tmFLX3ClockIN = new TimeSpan(9, 0, 0);
            //TimeSpan tmFLX3ClockOUT = new TimeSpan(18, 0, 0);
            data.ClockInLate = false;
            data.ClockoutEarly = false;
            data.ClockAbnormal = false;
            data.ClockAbnormal = false;

            if (DWS.DailyWorkscheduleCode.Contains("FL") && data.Type != "HOL")
            {
                DailyFlexTime oDailyFlexTime = HRTMManagement.CreateInstance(CompanyCode).GetWorkingTimeOfFlex(dtCurrent, info0007, ref timePair);
                data.Type = string.Format("{0} ({1}-{2})", info0007.WFRule, oDailyFlexTime.BeginTime, oDailyFlexTime.EndTime);

                if (timePair.ClockIN == null || timePair.ClockIN.EventTime == DateTime.MinValue)
                    data.ClockAbnormal = true;
                else
                {
                    if (timePair.ClockIN.EventTime.TimeOfDay > TimeSpan.Parse(oDailyFlexTime.BeginTime))
                        data.ClockInLate = true;
                }


                if (timePair.ClockOUT == null || timePair.ClockOUT.EventTime == DateTime.MinValue)
                    data.ClockAbnormal = true;
                else
                {
                    //If clock-out early than work end time in daily work schedule, means EARLY
                    checkEndDateTime = dtCurrent.Date.Add(TimeSpan.Parse(oDailyFlexTime.EndTime));

                    if (timePair.ClockOUT.EventTime < checkEndDateTime)
                        data.ClockoutEarly = true;
                }

                IsHaveAttendanceOrAbsenceInHalfDay(info2001s, info2002s, DWS, ref data);

                #region Old Code Not Used
                //FLX1
                //if (timePair.ClockIN.EventTime.TimeOfDay <= tmFLX1ClockIN || timePair.ClockIN.EventTime.TimeOfDay > tmFLX3ClockIN)
                //{
                //    data.Type = string.Format("{0} ({1}:{2}-{3}:{4})", info0007.WFRule, tmFLX1ClockIN.Hours.ToString("00"), tmFLX1ClockIN.Minutes.ToString("00"), tmFLX1ClockOUT.Hours.ToString("00"), tmFLX1ClockOUT.Minutes.ToString("00"));

                //    if (timePair.ClockIN == null || timePair.ClockIN.EventTime == DateTime.MinValue)
                //        data.ClockAbnormal = true;
                //    else
                //    {
                //        if (timePair.ClockIN.EventTime.TimeOfDay > tmFLX1ClockIN)
                //            data.ClockInLate = true;
                //    }


                //    if (timePair.ClockOUT == null || timePair.ClockOUT.EventTime == DateTime.MinValue)
                //        data.ClockAbnormal = true;
                //    else
                //    {
                //        //If clock-out early than work end time in daily work schedule, means EARLY
                //        checkEndDateTime = dtCurrent.Date.Add(tmFLX1ClockOUT);

                //        if (timePair.ClockOUT.EventTime < checkEndDateTime)
                //            data.ClockoutEarly = true;
                //    }

                //    IsHaveAttendanceOrAbsenceInHalfDay(info2001s, info2002s, DWS, ref data);
                //}
                ////FLX2
                //else if (timePair.ClockIN.EventTime.TimeOfDay > tmFLX1ClockIN && timePair.ClockIN.EventTime.TimeOfDay < tmFLX3ClockIN)
                //{
                //    data.Type = string.Format("{0} ({1}:{2}-{3}:{4})", info0007.WFRule, tmFLX2ClockIN.Hours.ToString("00"), tmFLX2ClockIN.Minutes.ToString("00"), tmFLX2ClockOUT.Hours.ToString("00"), tmFLX2ClockOUT.Minutes.ToString("00"));

                //    if (timePair.ClockIN == null || timePair.ClockIN.EventTime == DateTime.MinValue)
                //        data.ClockAbnormal = true;
                //    else
                //    {
                //        if (timePair.ClockIN.EventTime.TimeOfDay > tmFLX2ClockIN)
                //            data.ClockInLate = true;
                //    }

                //    if (timePair.ClockOUT == null || timePair.ClockOUT.EventTime == DateTime.MinValue)
                //        data.ClockAbnormal = true;
                //    else
                //    {
                //        //If clock-out early than work end time in daily work schedule, means EARLY
                //        checkEndDateTime = dtCurrent.Date.Add(tmFLX2ClockOUT);

                //        if (timePair.ClockOUT.EventTime < checkEndDateTime)
                //            data.ClockoutEarly = true;
                //    }

                //    IsHaveAttendanceOrAbsenceInHalfDay(info2001s, info2002s, DWS, ref data);
                //}
                ////FLX3
                //else
                //{
                //    data.Type = string.Format("{0} ({1}:{2}-{3}:{4})", info0007.WFRule, tmFLX3ClockIN.Hours.ToString("00"), tmFLX3ClockIN.Minutes.ToString("00"), tmFLX3ClockOUT.Hours.ToString("00"), tmFLX3ClockOUT.Minutes.ToString("00"));

                //    if (timePair.ClockIN == null || timePair.ClockIN.EventTime == DateTime.MinValue)
                //        data.ClockAbnormal = true;
                //    else
                //    {
                //        if (timePair.ClockIN.EventTime.TimeOfDay > tmFLX3ClockIN)
                //            data.ClockInLate = true;
                //    }
                //    if (timePair.ClockOUT == null || timePair.ClockOUT.EventTime == DateTime.MinValue)
                //        data.ClockAbnormal = true;
                //    else
                //    {
                //        //If clock-out early than work end time in daily work schedule, means EARLY
                //        checkEndDateTime = dtCurrent.Date.Add(tmFLX3ClockOUT);

                //        if (timePair.ClockOUT.EventTime < checkEndDateTime)
                //            data.ClockoutEarly = true;
                //    }

                //    IsHaveAttendanceOrAbsenceInHalfDay(info2001s, info2002s, DWS, ref data);
                //}
                #endregion
                return true;
            }
            else
                return false;
        }
    }
}
