using System;
using System.Collections.Generic;
using System.Globalization;
using System.Data;
using System.Text;
using ESS.DATA;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.CONFIG;
using ESS.HR.TM.DATACLASS;
using ESS.HR.TM.INFOTYPE;
using ESS.TIMESHEET;
using ESS.WORKFLOW;
using ESS.DATA.ABSTRACT;
using Newtonsoft.Json;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE.CONFIG.TM;

namespace ESS.HR.TM.DATASERVICE
{
    public class SummaryOTService : AbstractDataService
    {
        private CultureInfo oCL = new CultureInfo("en-US");
        public SummaryOTService()
        {
        }

        #region " CreateTemplateTable "
        private DataTable CreateMonthlyOTTable()
        {
            DataTable oTable;
            MonthlyOT oItem = new MonthlyOT();
            oTable = oItem.ToADODataTable(true);
            oTable.TableName = "MONTHLYOT";
            return oTable;
        }

        private DataTable CreateHeaderTable()
        {
            DataTable oTable;
            DataColumn oDC;
            oTable = new DataTable("HEADER");
            oDC = oTable.Columns.Add("OTPERIOD", typeof(string));
            oDC.DefaultValue = "";
            oDC = oTable.Columns.Add("PAYPERIOD", typeof(string));
            oDC.DefaultValue = "";

            return oTable;
        }

        private DataTable CreateRequestTable()
        {
            DataTable oTable;
            oTable = new DataTable("REQUEST");
            oTable.Columns.Add("REQUESTNO", typeof(string));
            return oTable;
        }

        private DataTable CreateApprovedOTTable()
        {
            DataTable oTable;
            DailyOT oItem = new DailyOT();
            oTable = oItem.ToADODataTable(true);
            oTable.TableName = "APPROVEDOT";
            return oTable;
        }
        #endregion

        #region " GenerateAdditionalData "
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            DataSet DS = new DataSet("ADDITIONAL");
            DS.Tables.Add(CreateMonthlyOTTable());
            DS.Tables.Add(CreateHeaderTable());
            DS.Tables.Add(CreateRequestTable());
            DS.Tables.Add(CreateApprovedOTTable());

            DataRow dr = DS.Tables["HEADER"].NewRow();
            dr["OTPERIOD"] = DateTime.Now.ToString("yyyyMM");
            DS.Tables["HEADER"].Rows.Add(dr);
            return DS;
        }

        #endregion

        #region " PrepareData "
        public override void PrepareData(EmployeeData Requestor, object ds)
        {
            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            if (!Data.Tables.Contains("MONTHLYOT"))
            {
                Data.Tables.Add(this.CreateMonthlyOTTable());
            }
            else
            {
                List<MonthlyOT> list = new List<MonthlyOT>();
                foreach (DataRow dr in Data.Tables["MONTHLYOT"].Rows)
                {
                    MonthlyOT item = new MonthlyOT();
                    item.ParseToObject(dr);
                    list.Add(item);
                }
                Data.Tables.Remove("MONTHLYOT");
                Data.Tables.Add(this.CreateMonthlyOTTable());
                foreach (MonthlyOT item in list)
                {
                    item.LoadDataToTable(Data.Tables["MONTHLYOT"]);
                }
            }
            if (!Data.Tables.Contains("APPROVEDOT"))
            {
                Data.Tables.Add(this.CreateApprovedOTTable());
            }
            else
            {
                List<DailyOT> list = new List<DailyOT>();
                foreach (DataRow dr in Data.Tables["APPROVEDOT"].Rows)
                {
                    DailyOT item = new DailyOT();
                    item.ParseToObject(dr);
                    list.Add(item);
                }
                Data.Tables.Remove("APPROVEDOT");
                Data.Tables.Add(this.CreateApprovedOTTable());
                foreach (DailyOT item in list)
                {
                    item.LoadDataToTable(Data.Tables["APPROVEDOT"]);
                }
            }
            if (!Data.Tables.Contains("HEADER"))
            {
                Data.Tables.Add(this.CreateHeaderTable());
            }
            else
            {
                DataTable oNewHeader = this.CreateHeaderTable();
                foreach (DataRow dr in Data.Tables["HEADER"].Rows)
                {
                    DataRow oNewRow = oNewHeader.NewRow();
                    foreach (DataColumn oDC in Data.Tables["HEADER"].Columns)
                    {
                        if (oNewHeader.Columns.Contains(oDC.ColumnName))
                        {
                            oNewRow[oDC.ColumnName] = dr[oDC];
                        }
                    }
                    oNewHeader.Rows.Add(oNewRow);
                }
                Data.Tables.Remove("HEADER");
                Data.Tables.Add(oNewHeader);
            }
            if (!Data.Tables.Contains("REQUEST"))
            {
                Data.Tables.Add(this.CreateRequestTable());
            }
        }
        #endregion

        #region " ValidateData "
        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            //Call from editor
            DataSet Ds = JsonConvert.DeserializeObject<DataSet>(newData.ToString());

            if (!Ds.Tables.Contains("MONTHLYOT") || Ds.Tables["MONTHLYOT"].Rows.Count == 0)
            {
                throw new DataServiceException("HRTM_EXCEPTION", "CAN_NOT_FIND_OT_DATA");
            }
            DataTable oTable = Ds.Tables["MONTHLYOT"];
            DataTable oHeader = Ds.Tables["HEADER"];

            string cOTPERIOD = (string)oHeader.Rows[0]["OTPERIOD"];
            DateTime myPeriod = DateTime.MinValue;

            string cPeriod = (string)oHeader.Rows[0]["OTPERIOD"];
            DateTime.TryParseExact(cPeriod, "yyyyMM", oCL, DateTimeStyles.None, out myPeriod);
            if (myPeriod == DateTime.MinValue)
            {
                throw new DataServiceException("HRTM_EXCEPTION", "PERIOD_NOTFOUND");
            }
            else
            {
                //check have other monthly ot in same period???
                try
                {
                    HRTMManagement.CreateInstance(Requestor.CompanyCode).MarkTimesheet(Requestor.EmployeeID, "MONTHLYOT", true, myPeriod, myPeriod.AddMonths(1).AddDays(-1), true, true, RequestNo);
                }
                catch (Exception e)
                {
                    throw new DataServiceException("HRTM_EXCEPTION", e.Message, "", e);
                }

                #region " Calculation "
                MonthlyOT oItem;
                MonthlyWS MWS = MonthlyWS.GetCalendar(Requestor.EmployeeID, myPeriod.Year, myPeriod.Month);
                DataTable oCheckTable = oTable.Copy();
                DataColumn oDC;
                oDC = new DataColumn("BeginDate", typeof(DateTime));
                oCheckTable.Columns.Add(oDC);
                oDC = new DataColumn("EndDate", typeof(DateTime));
                oCheckTable.Columns.Add(oDC);
                oDC = new DataColumn("Index", typeof(int));
                oCheckTable.Columns.Add(oDC);
                for (int index = 0; index < oCheckTable.Rows.Count; index++)
                {
                    DataRow dr = oCheckTable.Rows[index];
                    oItem = new MonthlyOT();
                    oItem.ParseToObject(dr);
                    dr["BeginDate"] = oItem.DailyOT.OTDate.Add(oItem.DailyOT.BeginTime);
                    dr["EndDate"] = oItem.DailyOT.OTDate.Add(oItem.DailyOT.EndTime);
                    dr["Index"] = index;
                }
                DataView oDV = new DataView(oCheckTable);
                string exp = "BeginDate < '{0}' And EndDate > '{1}' And Index <> {2} and IsDeleted = 0";
                for (int index = 0; index < oTable.Rows.Count; index++)
                {
                    DataRow dr = oTable.Rows[index];
                    oItem = new MonthlyOT();
                    oItem.ParseToObject(dr);
                    if (oItem.IsDeleted)
                    {
                        continue;
                    }
                    DateTime BeginDate, EndDate;
                    EndDate = oItem.DailyOT.OTDate.Add(oItem.DailyOT.EndTime);
                    BeginDate = oItem.DailyOT.OTDate.Add(oItem.DailyOT.BeginTime);
                    oDV.RowFilter = string.Format(exp, EndDate, BeginDate, index);
                    if (oDV.Count > 0)
                    {
                        throw new OTCollistionException(index, (int)oDV[0]["index"]);
                    }
                    //if (oItem.DailyOT.OTDate < DocumentDate.Date.AddMonths(-OTManagement.MonthlyOTOffsetBefore))
                    //{
                    //    throw new DataServiceException("HRTM_EXCEPTION", "MONTHLYOT_CONFLICT");
                    //}
                    if (oItem.DailyOT.Description == "")
                    {
                        throw new DataServiceException("HRTM_EXCEPTION", "NOT_FOUND_DESCRIPTION");
                    }
                    //oItem.DailyOT.Calculate(MWS);
                    if (oItem.BeginTime == TimeSpan.MinValue || oItem.EndTime == TimeSpan.MinValue)
                    {
                        throw new DataServiceException("HRTM_EXCEPTION", "OT_TIME_NOT_CORRECT");
                    }
                    else if (oItem.DailyOT.OTHours == 0 && !oItem.IsDeleted)
                    {
                        throw new DataServiceException("HRTM_EXCEPTION", "OT_ZERO_HOUR");
                    }
                    oItem.LoadDataToTableRow(dr);
                }
                #endregion
                //for (int index = 0; index < oTable.Rows.Count; index++)
                //{
                //    DataRow dr = oTable.Rows[index];
                //    oItem = new MonthlyOT();
                //    oItem.ParseToObject(dr);
                //    if (oItem.IsDeleted)
                //    {
                //        continue;
                //    }
                //    try
                //    {
                //        TimesheetManagement.MarkTimesheet(Requestor.EmployeeID, "DAILYOT", false, oItem.DailyOT.OTDate.Add(oItem.BeginTime).AddSeconds(1), oItem.DailyOT.OTDate.Add(oItem.EndTime).AddSeconds(-1), true, true, oItem.DailyRequestNo);
                //    }
                //    catch (TimesheetCollisionException e)
                //    {
                //        throw new OTCollistionException(index, e.CollissionData[0]);
                //    }
                //}
            }
        }
     
       #endregion

        private List<SaveOTData> splitCutoffTime(List<SaveOTData> list)
        {
            List<SaveOTData> oReturn = new List<SaveOTData>();
            foreach (SaveOTData item in list)
            {
                string empID;
                DateTime calDate;
                TimeSpan time1, time2, saveEndTime, calcTime;
                bool isPrevDay = false;
                empID = item.EmployeeID;
                calDate = item.OTDate;
                time1 = item.BeginTime;
                time2 = item.EndTime;
                saveEndTime = item.EndTime;
                do
                {
                    //PlanTimeManagement.SplitCutoffTime(empID, calDate, ref time1, ref time2);
                    //PlanTimeManagement.SplitPrevDay(empID, calDate, ref time1, ref time2, ref isPrevDay);

                    HRTMManagement.CreateInstance("").SplitCutoffTime(empID, calDate, ref time1, ref time2);
                    HRTMManagement.CreateInstance("").SplitPrevDay(empID, calDate, ref time1, ref time2, ref isPrevDay);
                    calcTime = new TimeSpan(time2.Hours, time2.Minutes, time2.Seconds);
                    SaveOTData data = new SaveOTData();
                    data.EmployeeID = empID;
                    data.OTDate = calDate;
                    data.BeginTime = time1;
                    data.EndTime = calcTime;
                    data.IsPrevDay = isPrevDay;
                    data.IsPost = false;
                    data.AssignFrom = item.AssignFrom;
                    data.OTItemTypeID = item.OTItemTypeID;
                    data.IsSummary = item.IsSummary;
                    data.IONumber = item.IONumber;
                    data.ActivityCode = item.ActivityCode;
                    oReturn.Add(data);
                    if (time2.Days > 0)
                    {
                        calDate = calDate.AddDays(1);
                    }
                    time1 = calcTime;
                    time2 = saveEndTime;
                } while (calcTime != saveEndTime);
            }
            return oReturn;
        }

        #region " SaveExternalData "
        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object ds, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            DataTable oAdditional = Data.Tables["MONTHLYOT"];
            DataTable oHeader = Data.Tables["HEADER"];
            DataTable oRequest = Data.Tables["REQUEST"];
            string Period = (string)oHeader.Rows[0]["OTPERIOD"];
            DateTime myPeriod = DateTime.MinValue;
            DateTime.TryParseExact(Period, "yyyyMM", oCL, DateTimeStyles.None, out myPeriod);
            if (myPeriod == DateTime.MinValue)
            {
                throw new DataServiceException("HRTM_EXCEPTION", "PERIOD_NOTFOUND");
            }
            if (State == "COMPLETED")
            {
                oHeader.Rows[0]["PAYPERIOD"] = HRTMManagement.CreateInstance(Requestor.CompanyCode).FindOTPeriod(DateTime.Now);
            }
            else
            {
                oHeader.Rows[0]["PAYPERIOD"] = "";
            }

            List<string> unmarkList = new List<string>();
            //unmarkList.Add("COMPLETED");
            unmarkList.Add("CANCELLED");

            List<string> unmarkList1 = new List<string>();
            unmarkList1.Add("COMPLETED");
            unmarkList1.Add("CANCELLED");

            List<string> postList = new List<string>();
            postList.Add("COMPLETED");

            SaveOTData item;

            List<SaveOTData> saveList = new List<SaveOTData>();

            List<MonthlyOT> motList = new List<MonthlyOT>();
            string EmployeeID = "";
            foreach (DataRow dr in oAdditional.Rows)
            {
                MonthlyOT mot = new MonthlyOT();
                mot.ParseToObject(dr);
                motList.Add(mot);
                if (EmployeeID == "")
                {
                    EmployeeID = mot.DailyOT.EmployeeID;
                }
            }


            List<string> checkList = new List<string>();

            foreach (DataRow dr in oAdditional.Rows)
            {
                MonthlyOT mot = new MonthlyOT();
                mot.ParseToObject(dr);
                item = new SaveOTData();
                item.AssignFrom = mot.DailyAssignFrom;
                item.BeginTime = mot.DailyOT.BeginTime;
                item.EndTime = new TimeSpan(mot.DailyOT.EndTime.Hours, mot.DailyOT.EndTime.Minutes, mot.DailyOT.EndTime.Seconds);
                item.Description = mot.DailyOT.Description;
                item.EmployeeID = mot.DailyOT.EmployeeID;
                item.IsCompleted = true;
                item.IsSummary = !(mot.IsDeleted || unmarkList.Contains(State.ToUpper()));
                item.OTDate = mot.DailyOT.OTDate;
                item.OTHours = mot.DailyOT.OTHours;
                item.OTItemTypeID = mot.DailyOT.OTItemTypeID;
                item.IsPrevDay = mot.DailyOT.IsPrevDay;
                item.OTPeriod = Period;
                item.OTType = "D";
                item.RequestNo = mot.DailyRequestNo;
                item.IONumber = mot.DailyOT.IONumber;
                item.ActivityCode = mot.DailyOT.ActivityCode;
                string cKey = string.Format("{0}#{1}#{2}#{3}#{4}", item.EmployeeID, item.OTType, item.OTDate.ToString("yyyyMMdd",oCL), item.BeginTime.ToString(), item.EndTime.ToString());
                if (!checkList.Contains(cKey))
                {
                    saveList.Add(item);
                    checkList.Add(cKey);
                }
            }

            if (postList.Contains(State))
            {
                #region " POST "
                // POST DATA
                try
                {
                    if (Data.Tables.Contains("POSTDATA"))
                    {
                        Data.Tables.Remove("POSTDATA");
                    }
                    List<SaveOTData> postdata = splitCutoffTime(saveList);
                    try
                    {
                        //HR.TM.ServiceManager.HRTMService.SaveOTList(postdata, true, null);
                        ServiceManager.CreateInstance(Requestor.CompanyCode).ERPData.SaveOTList(postdata, true, null);
                    }
                    catch (Exception ex1)
                    {
                        throw ex1;
                    }
                    finally
                    {
                        SaveOTData item1 = new SaveOTData();
                        DataTable oTempTable;
                        oTempTable = item1.ToADODataTable(true);
                        oTempTable.TableName = "POSTDATA";
                        Data.Tables.Add(oTempTable);
                        foreach (SaveOTData otItem in postdata)
                        {
                            DataRow oNewRow = oTempTable.NewRow();
                            otItem.LoadDataToTableRow(oNewRow);
                            oTempTable.LoadDataRow(oNewRow.ItemArray, false);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new SaveExternalDataException("SaveExternalData Error", true, ex);
                }
                #endregion
            }


            List<string> requestList = new List<string>();
            foreach (DataRow dr in oRequest.Rows)
            {
                requestList.Add((string)dr["REQUESTNO"]);
            }

            HRTMManagement.CreateInstance(Requestor.CompanyCode).MarkTimesheet(Requestor.EmployeeID, "MONTHLYOT", true, myPeriod, myPeriod.AddMonths(1).AddDays(-1), !unmarkList1.Contains(State.ToUpper()), false, RequestNo);
            //HR.TM.ServiceManager.HRTMBuffer.SaveOTList(saveList, !postList.Contains(State.ToUpper()), requestList, true);
            ServiceManager.CreateInstance(Requestor.CompanyCode).ESSData.SaveOTList(saveList, !postList.Contains(State.ToUpper()), requestList, true);
        }
        #endregion

        #region " CalculateInfoData "
        public override void CalculateInfoData(EmployeeData Requestor, object ds, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            DataTable oHeader = Data.Tables["HEADER"];
            string Period = (string)oHeader.Rows[0]["OTPERIOD"];
            DataRow dr;
            Info.Rows.Clear();
            dr = Info.NewRow();
            Decimal nSum = SumOT(Data.Tables["MONTHLYOT"]);
            if (nSum <= 0)
            {
                throw new DataServiceException("HRTM_EXCEPTION", "ZERO_HOUR");
            }
            List<DailyOT> approvedOT = HRTMManagement.CreateInstance(Requestor.CompanyCode).GenerateApprovedMonthlyOT(Requestor.EmployeeID, Period);
            nSum += SumOT(approvedOT);
            dr["TOTALHOURS"] = nSum;
            dr["OTPERIOD"] = Period;
            Info.Rows.Add(dr);
        }
        #endregion

        #region " SumOT "
        private Decimal SumOT(DataTable otTable)
        {
            Decimal oReturn = 0.0M;
            MonthlyOT oItem;
            foreach (DataRow dr in otTable.Rows)
            {
                oItem = new MonthlyOT();
                oItem.ParseToObject(dr);
                if (oItem.IsDeleted)
                {
                    continue;
                }
                oReturn += oItem.DailyOT.OTHours;
            }
            return oReturn;
        }
        private Decimal SumOT(List<DailyOT> data)
        {
            Decimal oReturn = 0.0M;
            foreach (DailyOT item in data)
            {
                oReturn += item.OTHours;
            }
            return oReturn;
        }      
        #endregion

        #region " GenEmailBody "
        public override string GenEmailBody(EmployeeData Requestor, DataTable Info, string State, string Language)
        {
            string cReturn = "";
            string Period = (string)Info.Rows[0]["OTPERIOD"];
            DateTime myPeriod = DateTime.MinValue;
            DateTime.TryParseExact(Period, "yyyyMM", oCL, DateTimeStyles.None, out myPeriod);
            if (State.ToUpper() == "COMPLETED")
            {
            }
            else
            {
                cReturn = "";
            }
            return cReturn;
        }

        #endregion
    }
}
