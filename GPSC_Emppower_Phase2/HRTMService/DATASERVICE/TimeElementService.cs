using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using ESS.DATA;
using ESS.EMPLOYEE;
using ESS.WORKFLOW;
using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using Newtonsoft.Json;
using ESS.TIMESHEET;

namespace ESS.HR.TM.DATASERVICE
{
    public class TimeElementService : AbstractDataService
    {
        public TimeElementService()
        { 
        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object ds, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            bool lPost = State == "COMPLETED";
            bool lCancelled = State == "CANCELLED";
            bool lMark = !lPost && !lCancelled;
            DateTime BeginDate, EndDate;
            DateTime BeginDatePair, EndDatePair;
            HRTMManagement.CreateInstance(Requestor.CompanyCode).MarkTimesheet(Requestor.EmployeeID, "ACCEPTTIMESHEET", lMark, RequestNo);
            List<TimeElement> archiveData = new List<TimeElement>();
            List<TimePair> pairs = new List<TimePair>();
            DataTable oElements = Data.Tables["TIMEELEMENT"];
            TimeElement oItem;
            bool lSavePair = false;
            if (lPost || lCancelled)
            {
                foreach (DataRow dr in oElements.Rows)
                {
                    oItem = new TimeElement();
                    oItem.ParseToObject(dr);
                    archiveData.Add(oItem);
                }
            }
            if (lPost)
            {
                if (archiveData.Count > 0)
                {
                    BeginDate = archiveData[0].EventTime;
                    EndDate = archiveData[archiveData.Count - 1].EventTime;
                    try
                    {
                        HRTMManagement.CreateInstance(Requestor.CompanyCode).ArchiveData(Requestor.EmployeeID, BeginDate, EndDate, archiveData);
                        if (Data.Tables.Contains("POSTDATA"))
                        {
                            // Load last post data result and try again only data with not succress
                            foreach (DataRow dr in Data.Tables["POSTDATA"].Rows)
                            {
                                TimePair item = new TimePair();
                                item.ParseToObject(dr);
                                item.PostIN = false;
                                item.PostOUT = false;
                                pairs.Add(item);

                            }
                            Data.Tables.Remove("POSTDATA");
                        }
                        else
                        {
                            pairs = TimesheetManagement.MatchingClock(Requestor.EmployeeID, archiveData);
                        }
                        if (pairs.Count > 0)
                        {
                            BeginDatePair = pairs[0].Date;
                            EndDatePair = pairs[pairs.Count - 1].Date;
                            //TimesheetManagement.ArchiveData(Requestor.EmployeeID, BeginDatePair, EndDatePair, pairs);
                            //isArchivePairSuccess = true;
                            try
                            {
                                lSavePair = true;
                                // try to post
                                HRTMManagement.CreateInstance(Requestor.CompanyCode).PostTimePair(pairs);
                                // archive pair again -> mark status of postin-postout
                                //throw new Exception("test");
                            }
                            catch (TimePairPostingException ex1)
                            {
                                lSavePair = false;
                                throw ex1;
                            }
                            catch (Exception ex1)
                            {
                                lSavePair = false;
                                throw ex1;
                            }
                            finally
                            {
                                TimePair item1 = new TimePair();
                                DataTable oTempTable;
                                oTempTable = item1.ToADODataTable(true);
                                oTempTable.TableName = "POSTDATA";
                                Data.Tables.Add(oTempTable);
                                foreach (TimePair item in pairs)
                                {
                                    DataRow oNewRow = oTempTable.NewRow();
                                    item.LoadDataToTableRow(oNewRow);
                                    oTempTable.LoadDataRow(oNewRow.ItemArray, false);
                                }
                                if (lSavePair)
                                {
                                    HRTMManagement.CreateInstance(Requestor.CompanyCode).ArchiveData(Requestor.EmployeeID, BeginDatePair, EndDatePair, pairs);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        HRTMManagement.CreateInstance(Requestor.CompanyCode).ArchiveData(Requestor.EmployeeID, BeginDate, EndDate, new List<TimeElement>());
                        HRTMManagement.CreateInstance(Requestor.CompanyCode).MarkTimesheet(Requestor.EmployeeID, "ACCEPTTIMESHEET", !lMark, RequestNo);
                        throw new SaveExternalDataException("Save archive timeevent error.", true, ex);
                    }
                }
            }
            else if (lCancelled)
            {
                if (archiveData.Count > 0)
                {
                    BeginDate = archiveData[0].EventTime;
                    EndDate = archiveData[archiveData.Count - 1].EventTime;
                    HRTMManagement.CreateInstance(Requestor.CompanyCode).ArchiveData(Requestor.EmployeeID, BeginDate, EndDate, new List<TimeElement>());
                    HRTMManagement.CreateInstance(Requestor.CompanyCode).ArchiveData(Requestor.EmployeeID, BeginDate, EndDate, new List<TimePair>());
                }
            }

        }

        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            DataSet DS = new DataSet("ADDITIONAL");
            DataTable oTable;
            DateTime oArchiveDate = HRTMManagement.CreateInstance(Requestor.CompanyCode).LoadArchiveDate(Requestor.EmployeeID);
            DateTime oNow = DateTime.Now;
            TimeElement oItem = new TimeElement();
            oTable = oItem.ToADODataTable(true);
            oTable.TableName = "TIMEELEMENT";
            if (oArchiveDate < oNow)
            {
                DateTime oBreakDate = DateTime.MaxValue;
                List<TimeElement> list = HRTMManagement.CreateInstance(Requestor.CompanyCode).LoadTimeElement(Requestor.EmployeeID, oArchiveDate, oNow, -1, true);
                list.Sort(new TimeElementSortingTime());
                List<TimePair> pairs1 = TimesheetManagement.MatchingClock(Requestor.EmployeeID, list);
                if (pairs1.Count > 0)
                {
                    TimePair oLastPair = pairs1[pairs1.Count - 1];
                    if (oLastPair.ClockOUT is null && !(oLastPair.ClockIN is null))
                    {
                        list.Remove(oLastPair.ClockIN);
                        oBreakDate = oLastPair.ClockIN.EventTime;
                    }
                }

                foreach (TimeElement item in list)
                {
                    if (item.EventTime <= oBreakDate)
                    {
                        item.LoadDataToTable(oTable);
                    }
                }
            }
            DS.Tables.Add(oTable);
            return DS;
        }

        public override void PrepareData(EmployeeData Requestor, object ds)
        {
            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            if (!Data.Tables.Contains("TIMEELEMENT"))
            {
                DataTable oTable;
                TimeElement oItem = new TimeElement();
                oTable = oItem.ToADODataTable(true);
                oTable.TableName = "TIMEELEMENT";
                Data.Tables.Add(oTable);
            }
        }

        public override void ValidateData(object ds, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            DataSet newData = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            if (newData == null || !newData.Tables.Contains("TIMEELEMENT") || newData.Tables["TIMEELEMENT"].Rows.Count == 0)
            {
                string Err = HRTMManagement.CreateInstance(Requestor.CompanyCode).ShowError("TIMESHEET", "NO_ELEMENT_FOR_ARCHIVE", WorkflowPrinciple.Current.UserSetting.Language);
                throw new Exception(Err);
                //throw new DataServiceException("TIMESHEET", "NO_ELEMENT_FOR_ARCHIVE");
            }
            if (newData.Tables.Contains("POSTDATA"))
            {
                newData.Tables.Remove("POSTDATA");
            }
            try
            {
                HRTMManagement.CreateInstance(Requestor.CompanyCode).MarkTimesheet(Requestor.EmployeeID, "ACCEPTTIMESHEET", true, true, RequestNo);
            }
            catch (Exception e)
            {
                throw new DataServiceException("TIMESHEET", "COLLISION_OCCUR", "", e);
            }
        }
    }
}
