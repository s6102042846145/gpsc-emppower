using System;
using System.Collections.Generic;
using System.Globalization;
using System.Data;
using System.Text;
using ESS.HR.TM.CONFIG;
using ESS.HR.TM.INFOTYPE;
using ESS.DATA;
using ESS.EMPLOYEE;
using ESS.DATA.ABSTRACT;
using ESS.TIMESHEET;
using Newtonsoft.Json;
using System.Linq;

namespace ESS.HR.TM.DATASERVICE
{
    public class DeleteDailyOTLog : AbstractDataService
    {
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            DataSet DS = new DataSet("ADDITIONAL");
            DataTable oTable;
            DailyOTLog oItem = new DailyOTLog();
            oTable = oItem.ToADODataTable(true);
            oTable.TableName = "DailyOTLog";

            if(!string.IsNullOrEmpty(CreateParam))
            {
                DateTime BeginDate;
                DateTime EndDate;
                DateTime.TryParseExact(CreateParam.Split('|')[2], "yyyyMMdd Hmmss", new System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None, out BeginDate);
                DateTime.TryParseExact(CreateParam.Split('|')[3], "yyyyMMdd Hmmss", new System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None, out EndDate);
             
                String RequestNo = CreateParam.Split('|')[0];
                String EmployeeID = CreateParam.Split('|')[1];

               var _DailyOTLog = HRTMManagement.CreateInstance(Requestor.CompanyCode).LoadDailyOTLog(RequestNo, EmployeeID, BeginDate, EndDate);

                oTable.Rows.Clear();
                foreach (DailyOTLog oDailyOTLog in _DailyOTLog)
                {
                    DataRow dr = oTable.NewRow();
                    oDailyOTLog.LoadDataToTableRow(dr);
                    oTable.Rows.Add(dr);
                }
            }


            DS.Tables.Add(oTable);
            return DS;
        }

        public override void CalculateInfoData(EmployeeData Requestor, object ds, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            Info.Clear();
            DataRow NewRow;

            foreach (DataRow row in Data.Tables["DailyOTLog"].Rows)
            {
                NewRow = Info.NewRow();
                NewRow["OTDATE"] = ((DateTime)row["BeginDate"]).Date;
                NewRow["BEGINTIME"] = ((DateTime)row["BeginDate"]).ToString("HH:mm");
                NewRow["ENDTIME"] = ((DateTime)row["EndDate"]).ToString("HH:mm");
                Info.Rows.Add(NewRow);
            }
        }

        public override void PrepareData(EmployeeData Requestor, object Data)
        {

        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {

        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object ds, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            DataTable oAdditional = Data.Tables["DailyOTLog"];
            bool IsDelete = true;
            if (State.ToUpper() == "DRAFT")
                foreach (DataRow row in oAdditional.Rows)
                {
                    row.BeginEdit();
                    row["RefRequestNo"] = RequestNo;
                    row["Status"] = (int)DailyOTLogStatus.DRAFT;
                    row.EndEdit();
                }
            else if (State.ToUpper() == "WAITFORAPPROVE")
            {
                foreach (DataRow row in oAdditional.Rows)
                {
                    row.BeginEdit();
                    row["Status"] = (int)DailyOTLogStatus.WAITFORCANCEL;
                    row.EndEdit();
                }
            }
            else if (State.ToUpper() == "COMPLETED")
            {
                IsDelete = false;
                foreach (DataRow row in oAdditional.Rows)
                    {
                        row.BeginEdit();
                        row["RefRequestNo"] = DBNull.Value;
                        row["Status"] = (int)DailyOTLogStatus.CANCELLED;
                        row.EndEdit();
                    }
            }
            else if (State.ToUpper() == "CANCELLED")
            {
                foreach (DataRow row in oAdditional.Rows)
                {
                    row.BeginEdit();
                    row["RefRequestNo"] = DBNull.Value;
                    row["Status"] = (int)DailyOTLogStatus.WAITFORMAKESUMMARY;
                    row.EndEdit();
                }
            }

            //HR.TM.ServiceManager.HRTMBuffer.SaveOTLog(Data.Tables["DailyOTLog"], IsDelete);
            //ServiceManager.CreateInstance(Requestor.CompanyCode).ESSData.SaveOTLog(Data.Tables["DailyOTLog"], IsDelete);
            HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveOTLog(Data.Tables["DailyOTLog"], IsDelete);
        }

        public override string GenerateFlowKey(EmployeeData Requestor, DataTable Info)
        {
            decimal dMaxLevelMD = HRTMManagement.CreateInstance(Requestor.CompanyCode).GetMaxLevelMD();
            if (dMaxLevelMD != -1 && Requestor.EmpSubGroup == dMaxLevelMD)
            {
                return "MD_CREATE";
            }
            return "NORMAL";
        }
    }
}
