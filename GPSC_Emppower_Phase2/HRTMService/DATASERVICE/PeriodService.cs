using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ESS.DATA.ABSTRACT;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.CONFIG.PA;
using ESS.TIMESHEET;
using System.Globalization;
using Newtonsoft.Json;
using ESS.DATA.EXCEPTION;
using ESS.HR.TM.DATACLASS;
using System.Linq;
using ESS.WORKFLOW;

namespace ESS.HR.TM.DATASERVICE
{
    public class PeriodService : AbstractDataService
    {
        private CultureInfo oCL = new CultureInfo("en-US");
        //private string strApplication = "CHANGEWORKTIME";

        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            #region  Code Old
            //DataSet ds = new DataSet();

            ////�֧������ Infotype0007 ����ͧ��ѡ�ҹ
            //List<INFOTYPE0007> dataInfo7 =new INFOTYPE0007().GetWorkSchedule(Requestor.EmployeeID);

            ////ModifiedBy: Ratchatawan W. (2013-06-04)
            //DataTable oTableInfo7 = (new INFOTYPE0007().GetWorkSchedule(Requestor.EmployeeID, DateTime.Now)).ToADODataTable(true);
            //oTableInfo7.TableName = "INFOTYPE0007";

            //DataRow row = null;
            //int SeqNo = 0;
            //DateTime oCheckDate = DateTime.ParseExact("1/1/"+DateTime.Now.ToString("yyyy", oCL),"d/M/yyyy",oCL);
            //foreach(INFOTYPE0007 list in dataInfo7)
            //{
            //    if (list.BeginDate >= oCheckDate || list.EndDate >= oCheckDate)
            //    {
            //        row = oTableInfo7.NewRow();
            //        row["EmployeeID"] = list.EmployeeID;
            //        row["BeginDate"] = list.BeginDate;
            //        row["EndDate"] = list.EndDate;
            //        row["WFRule"] = list.WFRule;
            //        row["TimeEvaluateClass"] = list.TimeEvaluateClass;
            //        row["SeqNo"] = SeqNo++;
            //        oTableInfo7.Rows.Add(row);
            //    }
            //}
            //ds.Tables.Add(oTableInfo7);
            ////Table ����Ѻ����� User ��� Add �����������Һ�ҧ
            //DataTable oTableEditInfo7 = new DataTable();
            //oTableEditInfo7 = oTableInfo7.Clone();
            //oTableEditInfo7.TableName = "INFOTYPE0007EDIT";
            //ds.Tables.Add(oTableEditInfo7);

            //List<INFOTYPE0001> dataInfo1 =new INFOTYPE0001().GetListFromSAP(Requestor.EmployeeID);
            //DataTable oTableInfo1 = dataInfo1[0].ToADODataTable(true);
            //oTableInfo1.TableName = "INFOTYPE0001";
            //foreach (INFOTYPE0001 item in dataInfo1)
            //{
            //    if (item.BeginDate >= oCheckDate || item.EndDate >= oCheckDate)
            //    {
            //        row = oTableInfo1.NewRow();
            //        item.LoadDataToTableRow(row);
            //        oTableInfo1.Rows.Add(row);
            //    }
            //}
            //ds.Tables.Add(oTableInfo1);

            //DataTable oTableEditInfo1 = new DataTable();
            //oTableEditInfo1 = oTableInfo1.Clone();
            //oTableEditInfo1.TableName = "INFOTYPE0001EDIT";
            //ds.Tables.Add(oTableEditInfo1);

            //DataTable oTableInfo = new DataTable();
            //oTableInfo.TableName = "AdditionalInfo";
            //oTableInfo.Columns.Add("SelectedSubArea");
            //ds.Tables.Add(oTableInfo);
            //return ds;
            #endregion

            //  1 = ���§��� effectivedate , 2 = ���§��� employee
            PeriodData PeriodGroup = new PeriodData();
            PeriodGroup.PeriodGroupType = "1";
            return PeriodGroup;
        }

        public override void PrepareData(EmployeeData Requestor, object Data)
        {

        }

        public class EmployeeInDatetime
        {
            public string EmployeeId { get; set; }
            public DateTime EffectiveDate { get; set; }
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            PeriodData json_convert = JsonConvert.DeserializeObject<PeriodData>(newData.ToString());

            var language_data = WorkflowPrinciple.Current.UserSetting.Language;

            DateTime datetime_now = DateTime.Now;

            // Group By ��� �ѹ������������ҹ
            if (json_convert.PeriodGroupType == "1")
            {
                if (json_convert.PeriodGroupTypeEffectiveDates.Count == 0)
                {
                    // ����բ����� Effective Date
                    string txt = null;
                    if (language_data.ToLower() == "th")
                        txt = "�Ѵ���������ѹ������������ҹ";

                    if (language_data.ToLower() == "en")
                        txt = "grou by effective date";

                    string Err = HRTMManagement.CreateInstance(Requestor.CompanyCode)
                            .ShowError("HRTM_EXCEPTION"
                            , String.Format("CHANGEWORKTIME_NOT_FOUND_EMPLOYEE_BEGINDATE|{0}"
                            , txt), WorkflowPrinciple.Current.UserSetting.Language);
                    throw new Exception(Err);
                }

                List<DateTime> temp_datetime = new List<DateTime>();

                foreach (var effectiveDateData in json_convert.PeriodGroupTypeEffectiveDates)
                {
                    if (effectiveDateData.PeriodTypeGroupEffectiveDateDetails.Count == 0)
                    {
                        string Err = HRTMManagement.CreateInstance(Requestor.CompanyCode)
                            .ShowError("HRTM_EXCEPTION"
                            , String.Format("CHANGEWORKTIME_NOT_FOUND_DATA_ON_DATE|{0}"
                            , effectiveDateData.EffectiveDate.Date.ToString("dd/MM/yyyy")), WorkflowPrinciple.Current.UserSetting.Language);
                        throw new Exception(Err);
                    }

                    // ���͡�ѹ����ӡѺ�Ѩ�غѹ
                    //if (datetime_now.Date >= effectiveDateData.EffectiveDate.Date)
                    //{
                    //    throw new DataServiceException("HRTM_EXCEPTION", "CANNOT_CREATE_BEFORE_CURRENTDATE");
                    //}

                    if (temp_datetime.Count > 0)
                    {
                        foreach (DateTime dataDate in temp_datetime)
                        {
                            if (dataDate.Date == effectiveDateData.EffectiveDate.Date)
                            {
                                string txt_date = null;
                                txt_date = effectiveDateData.EffectiveDate.Date.ToString("dd/MM/yyyy");

                                string Err = HRTMManagement.CreateInstance(Requestor.CompanyCode).ShowError("HRTM_EXCEPTION"
                                    , String.Format("CHANGEWORKTIME_CANNOT_SELECT_DATE_COLLISION |{0}"
                                    , txt_date), WorkflowPrinciple.Current.UserSetting.Language);
                                throw new Exception(Err);
                            }
                        }
                    }
                    temp_datetime.Add(effectiveDateData.EffectiveDate);
                }

            }
            else if (json_convert.PeriodGroupType == "2") // Group By ��ѡ�ҹ
            {
                // ����բ����ž�ѡ�ҹ
                if (json_convert.PeriodGroupTypeEmployees.Count == 0)
                {
                    string txt = null;
                    if (language_data.ToLower() == "th")
                        txt = "��ѡ�ҹ";

                    if (language_data.ToLower() == "en")
                        txt = "Employee";

                    string Err = HRTMManagement.CreateInstance(Requestor.CompanyCode)
                            .ShowError("HRTM_EXCEPTION"
                            , String.Format("CHANGEWORKTIME_NOT_FOUND_EMPLOYEE_DATA|{0}"
                            , txt), WorkflowPrinciple.Current.UserSetting.Language);
                    throw new Exception(Err);
                }

                List<EmployeeInDatetime> tempEmployeeInDatetime = new List<EmployeeInDatetime>();

                foreach (var employee in json_convert.PeriodGroupTypeEmployees)
                {
                    if (employee.EmployeeWorkingDetails.Count == 0)
                    {
                        // ����բ����� Row effective date
                        string Err = HRTMManagement.CreateInstance(Requestor.CompanyCode)
                            .ShowError("HRTM_EXCEPTION"
                            , String.Format("CHANGEWORKTIME_NOT_FOUND_EMPLOYEE_BEGINDATE|{0}"
                            , employee.EmployeeName), WorkflowPrinciple.Current.UserSetting.Language);
                        throw new Exception(Err);
                    }

                    // Order By Asc
                    var data = employee.EmployeeWorkingDetails.OrderBy(s => s.EffectiveDate).ToList();
                    employee.EmployeeWorkingDetails = data;

                    int AreaWorkScheduleId = 0;

                    foreach (var working in employee.EmployeeWorkingDetails)
                    {
                        if (working.EffectiveDate == null)
                        {
                            string Err = HRTMManagement.CreateInstance(Requestor.CompanyCode)
                            .ShowError("HRTM_EXCEPTION"
                            , String.Format("CHANGEWORKTIME_NOT_FOUND_EMPLOYEE_BEGINDATE|{0}"
                            , employee.EmployeeName), WorkflowPrinciple.Current.UserSetting.Language);
                            throw new Exception(Err);
                        }

                        DateTime date_convert = (DateTime)working.EffectiveDate;
                        //if(datetime_now.Date >= date_convert.Date)
                        //{
                        //    // ���͡�ѹ����ӡѺ�ѹ�Ѩ�غѹ
                        //    throw new DataServiceException("HRTM_EXCEPTION", "CANNOT_CREATE_BEFORE_CURRENTDATE");
                        //}

                        // �ѡ���͹��ҡ���͡���������ҡ�÷ӧҹ�Դ�ѹ ���������ö����
                        if (AreaWorkScheduleId == 0)
                            AreaWorkScheduleId = working.AreaWorkScheduleId;
                        else
                        {
                            if (AreaWorkScheduleId == working.AreaWorkScheduleId)
                            {
                                string Err = HRTMManagement.CreateInstance(Requestor.CompanyCode).ShowError("HRTM_EXCEPTION"
                                    , String.Format("CHANGEWORKTIME_CANNOT_SELECT_DATE_CONTINUOUS|{0}"
                                    , employee.EmployeeName), WorkflowPrinciple.Current.UserSetting.Language);
                                throw new Exception(Err);
                            }
                            else
                                AreaWorkScheduleId = working.AreaWorkScheduleId;
                        }

                        DateTime convert_datetime = (DateTime)working.EffectiveDate;
                        var datetime_duplicate = tempEmployeeInDatetime
                            .Where(s => s.EmployeeId == employee.EmployeeId && s.EffectiveDate.Date == convert_datetime.Date)
                            .FirstOrDefault();

                        if (datetime_duplicate != null)
                        {
                            // ��ѡ�ҹ���͡�ѹ���������鹷ӧҹ��ӡѹ
                            string Err = HRTMManagement.CreateInstance(Requestor.CompanyCode).ShowError("HRTM_EXCEPTION"
                                    , String.Format("CHANGEWORKTIME_CANNOT_SELECT_DATE_COLLISION|{0}"
                                    , employee.EmployeeName), WorkflowPrinciple.Current.UserSetting.Language);
                            throw new Exception(Err);
                        }
                        else
                        {
                            EmployeeInDatetime employeeInDatetime = new EmployeeInDatetime
                            {
                                EmployeeId = employee.EmployeeId,
                                EffectiveDate = convert_datetime
                            };
                            tempEmployeeInDatetime.Add(employeeInDatetime);
                        }
                    }
                }

            }

            #region " CheckCollision "
            if (HRTMManagement.CreateInstance(Requestor.CompanyCode).CheckMark(Requestor.EmployeeID, "CHANGEWORKTIME", RequestNo))
            {
                //throw new DataServiceException("HRPA", "REQUEST_DUPLICATE", "Request duplicate");
                throw new DataServiceException("HRTM_EXCEPTION", "REQUEST_DUPLICATE");
            }
            #endregion

        }


        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object ds, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            #region Old Code
            //DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            //bool IsDelete = true;
            //List<AreaWorkscheduleLog> oListAreaWorkscheduleLog = new List<AreaWorkscheduleLog>();
            //if (State == "COMPLETED")
            //{
            //    bool isSAPLock = false;
            //    DataTable oTempTable;
            //    List<INFOTYPE0007> oINFOTYPE0007List = new List<INFOTYPE0007>();
            //    INFOTYPE0007 oINFOTYPE0007;
            //    DataTable oTableInfo7 = Data.Tables["INFOTYPE0007"];

            //    try
            //    {
            //        foreach (DataRow row in oTableInfo7.Select(string.Format("EndDate >= '{0}'", DateTime.Now.ToString("yyyy/MM/dd"))))
            //        {
            //            oINFOTYPE0007 = new INFOTYPE0007();
            //            oINFOTYPE0007.ParseToObject(row);
            //            oINFOTYPE0007List.Add(oINFOTYPE0007);
            //        }
            //        if (oINFOTYPE0007List.Count > 0)
            //        {
            //            oINFOTYPE0007List.Sort(new INFOTYPE0007_Comparer());
            //            //Save ������ŧ SAP
            //EmployeeManagement.CreateInstance(Requestor.CompanyCode).SaveAreaWorkscheduleInfotype0007(oINFOTYPE0007List, RequestNo);
            //        }
            //        //Load data from SAP and save to DB
            //        oINFOTYPE0007List = new INFOTYPE0007().GetFromSAP(Requestor.EmployeeID);
            //        EmployeeManagement.CreateInstance(Requestor.CompanyCode).SavePeriod(Requestor.EmployeeID, Requestor.EmployeeID, oINFOTYPE0007List, "DEFAULT");

            //    }
            //    catch (Exception e)
            //    {
            //        if (Data.Tables.Contains("POSTDATA"))
            //            Data.Tables.Remove("POSTDATA");
            //        oINFOTYPE0007 = new INFOTYPE0007();
            //        oTempTable = oINFOTYPE0007.ToADODataTable(true);
            //        oTempTable.TableName = "POSTDATA";
            //        Data.Tables.Add(oTempTable);
            //        foreach (INFOTYPE0007 item in oINFOTYPE0007List)
            //        {
            //            DataRow oNewRow = oTempTable.NewRow();
            //            item.LoadDataToTableRow(oNewRow);
            //            oTempTable.LoadDataRow(oNewRow.ItemArray, false);
            //            isSAPLock = isSAPLock || (item.Remark.ToUpper().Contains("LOCK") || item.Remark.ToUpper().Contains("PROCESS"));
            //        }

            //        throw new SaveExternalDataException(oINFOTYPE0007List[0].Remark, !isSAPLock, e); // if false don't sent to request type owner
            //    }
            //}
            //HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveAreaWorkscheduleLog(oListAreaWorkscheduleLog, IsDelete);
            //HRTMManagement.CreateInstance(Requestor.CompanyCode).MarkTimesheet(Requestor.EmployeeID, "CHANGEWORKTIME", !(State == "COMPLETED" || State == "CANCELLED"), false, RequestNo);
            #endregion

            PeriodData json_convert = JsonConvert.DeserializeObject<PeriodData>(ds.ToString());

            List<AreaWorkscheduleLog> oListAreaWorkscheduleLog = new List<AreaWorkscheduleLog>();

            List<INFOTYPE0007> oINFOTYPE0007List = new List<INFOTYPE0007>();
            List<INFOTYPE0001> oINFOTYPE0001List = new List<INFOTYPE0001>();

            if (json_convert.PeriodGroupType == "1")
            {
                // Group by  Effective Date
                foreach (var effectiveDate in json_convert.PeriodGroupTypeEffectiveDates)
                {
                    foreach (var employeeAreaWorkSchedlue in effectiveDate.PeriodTypeGroupEffectiveDateDetails)
                    {
                        AreaWorkscheduleLog data = new AreaWorkscheduleLog()
                        {
                            AreaWorkscheduleID = employeeAreaWorkSchedlue.AreaWorkScheduleId,
                            EffectiveDate = (DateTime)effectiveDate.EffectiveDate,
                            RequestNo = RequestNo,
                            EmployeeID = employeeAreaWorkSchedlue.EmployeeId,
                            Status = State,
                            CreatedDate = DateTime.Now,
                            CreatorID = Requestor.EmployeeID
                        };
                        oListAreaWorkscheduleLog.Add(data);

                        if (State == "COMPLETED")
                        {
                            // State  COMPLETED ǹ��Ң����ŧ仺ѹ�֡

                            // InfoType0001
                            var data_infotype0001 = EmployeeManagement.CreateInstance(Requestor.CompanyCode)
                                .GetAreaWorkscheduleInfotype0001(employeeAreaWorkSchedlue.EmployeeId, effectiveDate.EffectiveDate);

                            data_infotype0001.BeginDate = effectiveDate.EffectiveDate;
                            data_infotype0001.Area = employeeAreaWorkSchedlue.Area;
                            data_infotype0001.SubArea = employeeAreaWorkSchedlue.SubArea;
                            oINFOTYPE0001List.Add(data_infotype0001);

                            // InfoType0007
                            DateTime datetimeData = effectiveDate.EffectiveDate;
                            var data_infotype0007 = EmployeeManagement.CreateInstance(Requestor.CompanyCode).GetAreaWorkscheduleInfotype0007List(employeeAreaWorkSchedlue.EmployeeId, datetimeData.Year, datetimeData.Month);
                            if (data_infotype0007.Count > 0)
                            {
                                foreach (var temp_data in data_infotype0007)
                                {
                                    temp_data.BeginDate = datetimeData;
                                    temp_data.WFRule = employeeAreaWorkSchedlue.WFRule;
                                    oINFOTYPE0007List.Add(temp_data);
                                }
                            }

                        }
                    }
                }
            }
            else if (json_convert.PeriodGroupType == "2")
            {
                // Group by  Employee ��Ѻ Model �����������Ѻ��� Insert
                foreach (var employee in json_convert.PeriodGroupTypeEmployees)
                {
                    // Employee 1 ���յ��ҧ�������¡�
                    foreach (var areaWorkSchedule in employee.EmployeeWorkingDetails)
                    {
                        AreaWorkscheduleLog data = new AreaWorkscheduleLog()
                        {
                            AreaWorkscheduleID = areaWorkSchedule.AreaWorkScheduleId,
                            EffectiveDate = (DateTime)areaWorkSchedule.EffectiveDate,
                            RequestNo = RequestNo,
                            EmployeeID = employee.EmployeeId,
                            Status = State,
                            CreatedDate = DateTime.Now,
                            CreatorID = Requestor.EmployeeID
                        };
                        oListAreaWorkscheduleLog.Add(data);


                        if (State == "COMPLETED")
                        {
                            // State  COMPLETED ǹ��Ң����ŧ仺ѹ�֡

                            // InfoType0001
                            var data_infotype0001 = EmployeeManagement.CreateInstance(Requestor.CompanyCode)
                                .GetAreaWorkscheduleInfotype0001(employee.EmployeeId, (DateTime)areaWorkSchedule.EffectiveDate);

                            data_infotype0001.BeginDate = (DateTime)areaWorkSchedule.EffectiveDate;
                            data_infotype0001.Area = areaWorkSchedule.Area;
                            data_infotype0001.SubArea = areaWorkSchedule.SubArea;
                            oINFOTYPE0001List.Add(data_infotype0001);

                            DateTime datetimeData = (DateTime)areaWorkSchedule.EffectiveDate;
                            var tempData = EmployeeManagement.CreateInstance(Requestor.CompanyCode).GetAreaWorkscheduleInfotype0007List(employee.EmployeeId, datetimeData.Year, datetimeData.Month);
                            if (tempData.Count > 0)
                            {
                                foreach (var dataInfoType0007 in tempData)
                                {
                                    dataInfoType0007.BeginDate = datetimeData;
                                    dataInfoType0007.WFRule = areaWorkSchedule.WFRule;
                                    oINFOTYPE0007List.Add(dataInfoType0007);
                                }
                            }
                        }
                    }
                }
            }

            // Post Sap ��������
            HRTMManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, "CHANGEWORKTIME", RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
            if (State == "COMPLETED")
            {
                try
                {
                    if(oINFOTYPE0001List.Count > 0)
                    {
                        EmployeeManagement.CreateInstance(Requestor.CompanyCode).SaveAreaWorkscheduleINFOTYPE0001(oINFOTYPE0001List, RequestNo); 
                    }

                    if (oINFOTYPE0007List.Count > 0)
                    {
                        EmployeeManagement.CreateInstance(Requestor.CompanyCode).SaveAreaWorkscheduleInfotype0007(oINFOTYPE0007List, RequestNo);
                    }

                }
                catch (Exception ex)
                {
                    HRTMManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, "CHANGEWORKTIME", RequestNo, true);
                    //throw new SaveExternalDataException("Post data error", true, ex);

                    //string Err = HRTMManagement.CreateInstance(Requestor.CompanyCode).ShowError("HRTM_EXCEPTION", ex.Message.ToString(), WorkflowPrinciple.Current.UserSetting.Language);
                    throw new Exception(ex.Message.ToString());
                }
            }

            bool IsDelete = true;
            HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveAreaWorkscheduleLog(oListAreaWorkscheduleLog, IsDelete);

        }
    }
}
