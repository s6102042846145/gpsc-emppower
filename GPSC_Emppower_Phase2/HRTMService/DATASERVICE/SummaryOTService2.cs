using System;
using System.Collections.Generic;
using System.Globalization;
using System.Data;
using System.Text;
using ESS.DATA;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.CONFIG;
using ESS.HR.TM;
using ESS.TIMESHEET;
using ESS.WORKFLOW;
using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using ESS.HR.TM.INFOTYPE;
using Newtonsoft.Json;
using static ESS.HR.TM.HRTMManagement;

namespace ESS.HR.TM.DATASERVICE
{
    public class SummaryOTService2 : AbstractDataService
    {
        private CultureInfo oCL = new CultureInfo("en-US");

        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            DataSet DS = new DataSet("ADDITIONAL");
            DataTable oTable;


            DailyOTLog oItem = new DailyOTLog();
            oTable = oItem.ToADODataTable(true);
            oTable.TableName = "DailyOTLog";
           
            SummaryOTModel result = new SummaryOTModel();
            if (!string.IsNullOrEmpty(CreateParam))
            {
                var employeeId = CreateParam.Split('|')[0];
                var period = CreateParam.Split('|')[1];
                result = HRTMManagement.CreateInstance(Requestor.CompanyCode).GetListSummaryOT(employeeId, DS, period, "");
                oTable.Rows.Clear();
                
                var _OT_Limit = new List<List<int>>();
                List<int> tmp;
                foreach (string item in HRTMManagement.CreateInstance(Requestor.CompanyCode).SUMMARYOTLIMITREQUEST.Split('|'))
                {
                    tmp = new List<int>();
                    foreach (string str in item.Split(','))
                        tmp.Add(Convert.ToInt16(str));
                    _OT_Limit.Add(tmp);
                }

                var LimitOT = 0;
                decimal SumOT = 0;
                foreach (DailyOTLog DailyOTLog in result.DailyOTLog)
                {
                    //var EmpID = new EmployeeData(Requestor.EmployeeID, DailyOTLog.BeginDate);
                    var EmpID = new EmployeeData(DailyOTLog.EmployeeID, DailyOTLog.BeginDate);

                    foreach (List<int> iList in _OT_Limit)
                        if (iList[0] <= EmpID.EmpSubGroup && iList[1] >= EmpID.EmpSubGroup)
                            LimitOT = iList[2];

                    var DWS = EmpID.GetDailyWorkSchedule(DailyOTLog.BeginDate);
                     SumOT += DailyOTLog.RequestOTHour10 + DailyOTLog.RequestOTHour15 + DailyOTLog.RequestOTHour30;

                    if (DWS.IsDayOff)
                        DailyOTLog.DayOff = true;

                    if (TimeSpan.FromMinutes(Convert.ToDouble(SumOT)).TotalHours > LimitOT)
                        DailyOTLog.OverLimit = true;

                    DataRow dr = oTable.NewRow();
                    DailyOTLog.LoadDataToTableRow(dr);
                    oTable.Rows.Add(dr);
                }
            }
            DS.Tables.Add(oTable);


            oTable = new DataTable();
            oTable = oItem.ToADODataTable(true);
            oTable.TableName = "AllDailyOTLog";
            if (!string.IsNullOrEmpty(CreateParam))
            {
                oTable.Rows.Clear();
                foreach (DailyOTLog DailyOTLog in result.AllDailyOTLog)
                {
                    DataRow dr = oTable.NewRow();
                    DailyOTLog.LoadDataToTableRow(dr);
                    oTable.Rows.Add(dr);
                }
            }
            DS.Tables.Add(oTable);

            oTable = new DataTable();
            oTable = oItem.ToADODataTable(true);
            oTable.TableName = "CancellationDialyOTLog";
            DS.Tables.Add(oTable);
            
            return DS;
        }

        public override void CalculateInfoData(EmployeeData Requestor, object ds, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            String OTPERIOD = String.Empty;

            EmployeeData emp;
            foreach (DataRow row in Data.Tables["DailyOTLog"].Rows)
                if ((Boolean)row["IsPayroll"])
                {
                    OTPERIOD = ((DateTime)Data.Tables["DailyOTLog"].Rows[0]["BeginDate"]).ToString("yyyyMM");

                    Info.Clear();
                    DataRow NewRow = Info.NewRow();
                    NewRow.BeginEdit();
                    NewRow["OTPERIOD"] = OTPERIOD;

                    emp = new EmployeeData(row["EmployeeID"].ToString());
                    if (emp != null)
                        NewRow["EMPLOYEE"] = string.Format("{0} {1}", emp.EmployeeID, emp.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language));
                    else
                        NewRow["EMPLOYEE"] = string.Empty;
                    NewRow.EndEdit();
                    Info.Rows.Add(NewRow);

                    

                    break;
                }

        }

        public override void PrepareData(EmployeeData Requestor, object Data)
        {

        }

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info)
        {
            if ((Creator.EmployeeID != Requestor.EmployeeID))
            {
                return "D";
            }
            else
            {
                return "N";
            }
        }

        public override void ValidateData(object ds, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            DataSet newData = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            DataTable oTable = newData.Tables["DailyOTLog"];
            Boolean IsAtLeast1Payroll = false;
            if (oTable.Rows.Count == 0)
            {
                throw new DataServiceException("HRTM_EXCEPTION", "NOT_FOUND_ORIGINAL_RECORD");
            }

            foreach (DataRow row in oTable.Rows)
            {
                if (Convert.ToBoolean(row["IsPayroll"])  &&
                    (DBNull.Value.Equals(row["FinalOTHour10"]) | Convert.ToDecimal(row["FinalOTHour10"]) == 0) &&
                    (DBNull.Value.Equals(row["FinalOTHour15"]) | Convert.ToDecimal(row["FinalOTHour15"]) == 0) &&
                    (DBNull.Value.Equals(row["FinalOTHour30"]) | Convert.ToDecimal(row["FinalOTHour30"]) == 0))
                {
                    throw new DataServiceException("HRTM_EXCEPTION", "FINALOTHOUR_IS_ZERO");
                }
            }

            foreach (DataRow row in oTable.Rows)
            {
                if ((DailyOTLogStatus)Convert.ToInt16(row["Status"]) == DailyOTLogStatus.SENTSUMMARY &&
                    (DBNull.Value.Equals(row["FinalOTHour10"]) | Convert.ToDecimal(row["FinalOTHour10"]) == 0) &&
                    (DBNull.Value.Equals(row["FinalOTHour15"]) | Convert.ToDecimal(row["FinalOTHour15"]) == 0) &&
                    (DBNull.Value.Equals(row["FinalOTHour30"]) | Convert.ToDecimal(row["FinalOTHour30"]) == 0))
                {
                    throw new DataServiceException("HRTM_EXCEPTION", "INVALID_DATA");
                }
                IsAtLeast1Payroll = (Boolean)row["IsPayroll"] || IsAtLeast1Payroll;
            }
            if (!IsAtLeast1Payroll)
                throw new DataServiceException("HRTM_EXCEPTION", "ATLEAST_ONEPAYROLL");
            foreach (DataRow row in oTable.Rows)
            {
                if ((DailyOTLogStatus)Convert.ToInt16(row["Status"]) == DailyOTLogStatus.SENTSUMMARY &&
                    (Convert.ToDouble(row["FinalOTHour10"]) % 0.5 != 0) ||
                    (Convert.ToDouble(row["FinalOTHour15"]) % 0.5 != 0) ||
                    (Convert.ToDouble(row["FinalOTHour30"]) % 0.5 != 0))
                {
                    throw new DataServiceException("HRTM_EXCEPTION", "INVALID_OTHOUR");
                }
            }
            DateTime BeginDate = (DateTime)oTable.Rows[0]["BeginDate"];
            BeginDate = new DateTime(BeginDate.Year, BeginDate.Month, 1);
            DateTime EndDate = BeginDate.AddMonths(1).AddMinutes(-1);
            //if (!HR.TM.ServiceManager.HRTMBuffer.IsCompleteCancleDailyOT(oTable.Rows[0]["EmployeeID"].ToString(), BeginDate, EndDate))
            if (!HRTMManagement.CreateInstance(Requestor.CompanyCode).IsCompleteCancleDailyOT(oTable.Rows[0]["EmployeeID"].ToString(), BeginDate, EndDate))
                throw new DataServiceException("HRTM_EXCEPTION", "UNCOMPLETE_CANCLE_DAILYOT");

        }
        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object ds, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {

            #region "Old Version"
            //DataTable oAdditional = Data.Tables["DailyOTLog"];
            //if (State.ToUpper() == "WAITFORVERIFY")
            //{
            //    foreach (DataRow row in oAdditional.Rows)
            //    {
            //        row.BeginEdit();
            //        if ((Boolean)row["IsPayroll"])
            //            row["Status"] = (int)DailyOTLogStatus.MAKESUMMARY;
            //        else
            //            row["Status"] = (int)DailyOTLogStatus.WAITFORMAKESUMMARY;
            //        row.EndEdit();
            //    }
            //    HR.TM.ServiceManager.HRTMBuffer.SaveOTLogStatus(oAdditional);
            //}
            //else if (State.ToUpper() == "WAITFOREDIT")
            //{
            //    foreach (DataRow row in oAdditional.Rows)
            //    {
            //        row.BeginEdit();
            //        row["Status"] = (int)DailyOTLogStatus.WAITFORMAKESUMMARY;
            //        row.EndEdit();
            //    }
            //    HR.TM.ServiceManager.HRTMBuffer.SaveOTLogStatus(oAdditional);
            //}
            //else if (State.ToUpper() == "COMPLETED")
            //{
            //    List<DailyOTLog> oDailyOTLog = new List<DailyOTLog>();
            //    DailyOTLog item;
            //    foreach (DataRow row in oAdditional.Rows)
            //    {
            //        row.BeginEdit();
            //        if ((Boolean)row["IsPayroll"])
            //            row["Status"] = (int)DailyOTLogStatus.SENTSUMMARY;
            //        else
            //            row["Status"] = (int)DailyOTLogStatus.WAITFORMAKESUMMARY;
            //        row.EndEdit();

            //        item = new DailyOTLog();
            //        item.ParseToObject(row);
            //        oDailyOTLog.Add(item);
            //    }

            //    try
            //    {
            //        HR.TM.ServiceManager.HRTMService.PostOTLog(oDailyOTLog);
            //        HR.TM.ServiceManager.HRTMBuffer.SaveOTLog(oAdditional, true);
            //    }
            //    catch (Exception e)
            //    {
            //        throw new SaveExternalDataException("SaveExternalData Error", true, e);
            //    }
            //    finally
            //    {
            //        if (Data.Tables.Contains("POSTDATA"))
            //            Data.Tables.Remove("POSTDATA");
            //        DailyOTLog item1 = new DailyOTLog();
            //        DataTable oTempTable;
            //        oTempTable = item1.ToADODataTable(true);
            //        oTempTable.TableName = "POSTDATA";
            //        Data.Tables.Add(oTempTable);
            //        foreach (DailyOTLog p_item in oDailyOTLog)
            //        {
            //            DataRow oNewRow = oTempTable.NewRow();
            //            p_item.LoadDataToTableRow(oNewRow);
            //            oTempTable.LoadDataRow(oNewRow.ItemArray, false);
            //        }
            //    }

            //}
            //else if (State.ToUpper() == "CANCELLED")
            //{
            //    foreach (DataRow row in oAdditional.Rows)
            //        row["Status"] = (int)DailyOTLogStatus.WAITFORMAKESUMMARY;
            //    HR.TM.ServiceManager.HRTMBuffer.SaveOTLogStatus(oAdditional);
            //}
            #endregion

            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            DataTable oAdditional = Data.Tables["DailyOTLog"];
            if (State.ToUpper() == "WAITFORVERIFY")
            {
               foreach (DataRow row in oAdditional.Rows)
                {
                    row.BeginEdit();
                    row["RefRequestNo"] = RequestNo;
                    row["Status"] = (int)DailyOTLogStatus.MAKESUMMARY;
                    row.EndEdit();
                }
                HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveOTLogStatus(oAdditional);
            }
            else if (State.ToUpper() == "WAITFOREDIT")
            {
                foreach (DataRow row in oAdditional.Rows)
                {
                    row.BeginEdit();
                    row["RefRequestNo"] = RequestNo;
                    row["Status"] = (int)DailyOTLogStatus.WAITFORMAKESUMMARY;
                    row.EndEdit();
                }
                HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveOTLogStatus(oAdditional);
            }
            else if (State.ToUpper() == "COMPLETED")
            {
                List<DailyOTLog> oDailyOTLog = new List<DailyOTLog>();
                List<DailyOTLog> ApvDailyOTLog = new List<DailyOTLog>();
                DailyOTLog item, tmp ;
                foreach (DataRow row in oAdditional.Rows)
                {
                    row.BeginEdit();
                    if ((Boolean)row["IsPayroll"])
                        row["Status"] = (int)DailyOTLogStatus.SENTSUMMARY;
                    else
                        row["Status"] = (int)DailyOTLogStatus.WAITFORMAKESUMMARY;
                    row["RefRequestNo"] = RequestNo;
                    row.EndEdit();

                    item = new DailyOTLog();

                    item.ParseToObject(row);


                    if (oDailyOTLog.Exists(delegate(DailyOTLog ot)
                    {
                        return ot.BeginDate.ToString("ddMMyyyy", oCL) == item.BeginDate.ToString("ddMMyyyy", oCL) &&
                                  //ot.EndDate.ToString("ddMMyyyy", oCL) == item.EndDate.ToString("ddMMyyyy", oCL) &&
                                  ot.IsPayroll;
                    }) && item.IsPayroll)
                    {
                        tmp = oDailyOTLog.FindAll(delegate(DailyOTLog ot)
                        { return ot.BeginDate.ToString("ddMMyyyy", oCL) == item.BeginDate.ToString("ddMMyyyy", oCL) && 
                            //ot.EndDate.ToString("ddMMyyyy", oCL) == item.EndDate.ToString("ddMMyyyy", oCL) && 
                            ot.IsPayroll; })[0];
                        tmp.EndDate = tmp.BeginDate;
                        tmp.FinalOTHour10 += item.FinalOTHour10;
                        tmp.FinalOTHour15 += item.FinalOTHour15;
                        tmp.FinalOTHour30 += item.FinalOTHour30;
                    }
                    else
                        oDailyOTLog.Add(item);
                }

                foreach (DailyOTLog oitem in oDailyOTLog)
                {
                    decimal FinalOTHour10, FinalOTHour15, FinalOTHour30;
                    FinalOTHour10 = FinalOTHour15 = FinalOTHour30 = 0;
                    //ApvDailyOTLog = HR.TM.ServiceManager.HRTMBuffer.LoadDailyOTLog(oitem.EmployeeID, oitem.BeginDate.ToString("dd/MM/yyyy", oCL), oitem.EndDate.ToString("dd/MM/yyyy", oCL));
                    ApvDailyOTLog = HRTMManagement.CreateInstance(Requestor.CompanyCode).LoadDailyOTLog(oitem.EmployeeID, oitem.BeginDate.ToString("dd/MM/yyyy", oCL));
                    ApvDailyOTLog = ApvDailyOTLog.FindAll(delegate(DailyOTLog ot) { return ot.IsPayroll && ot.Status == (int)DailyOTLogStatus.SENTSUMMARY && ot.RefRequestNo != RequestNo; });
                    foreach (DailyOTLog ApvItem in ApvDailyOTLog)
                    {    
                            FinalOTHour10 += ApvItem.FinalOTHour10;
                            FinalOTHour15 += ApvItem.FinalOTHour15;
                            FinalOTHour30 += ApvItem.FinalOTHour30;
                    }
                    oitem.FinalOTHour10 += FinalOTHour10;
                    oitem.FinalOTHour15 += FinalOTHour15;
                    oitem.FinalOTHour30 += FinalOTHour30;
                }

                try
                {
                    HRTMManagement.CreateInstance(Requestor.CompanyCode).PostOTLog(oDailyOTLog);
                    HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveOTLog(oAdditional, true);
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message.ToString());
                    //throw new SaveExternalDataException("SaveExternalData Error", true, e);
                }
                finally
                {
                    if (Data.Tables.Contains("POSTDATA"))
                        Data.Tables.Remove("POSTDATA");
                    DailyOTLog item1 = new DailyOTLog();
                    DataTable oTempTable;
                    oTempTable = item1.ToADODataTable(true);
                    oTempTable.TableName = "POSTDATA";
                    Data.Tables.Add(oTempTable);
                    foreach (DailyOTLog p_item in oDailyOTLog)
                    {
                        DataRow oNewRow = oTempTable.NewRow();
                        p_item.LoadDataToTableRow(oNewRow);
                        oTempTable.LoadDataRow(oNewRow.ItemArray, false);
                    }
                }

            }
            else if (State.ToUpper() == "CANCELLED")
            {
                foreach (DataRow row in oAdditional.Rows)
                {
                    row["Status"] = (int)DailyOTLogStatus.WAITFORMAKESUMMARY;
                    row["IsPayroll"] = false;
                }
                HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveOTLogStatus(oAdditional);
            }
            else if (State.ToUpper() == "DRAFT")
            {
                foreach (DataRow row in oAdditional.Rows)
                {
                    row["Status"] = (int)DailyOTLogStatus.SUMMARYDRAFT;
                }
                HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveOTLogStatus(oAdditional);
            }
        }
        public override void ValidateDataInAction(object newData, DataTable Info, EmployeeData Requestor, string RequestNo, string State, string Action, bool HaveComment, bool HaveComment2, DateTime SubmitDate)
        {
            bool isPassValidate = HRTMManagement.CreateInstance(Requestor.CompanyCode).ShowTimeAwareLink(0);

            if (Action != "CANCEL")
            {
                if (!isPassValidate || DateTime.Now.Month != SubmitDate.Month)
                {
                    throw new DataServiceException("HRTM_EXCEPTION", "TIMEOUT_PERIOD_SETTING");
                }
            }
        }
    }
}
