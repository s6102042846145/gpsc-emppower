using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using ESS.DATA;
using ESS.EMPLOYEE;
using ESS.WORKFLOW;
using ESS.DATA.ABSTRACT;
using ESS.HR.TM.INFOTYPE;
using Newtonsoft.Json;
using ESS.DATA.EXCEPTION;
using ESS.HR.TM.DATACLASS;

namespace ESS.HR.TM.DATASERVICE
{
    public class LeavePlanService : AbstractDataService
    {
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            DataSet oDS = new DataSet(Guid.NewGuid().ToString());
            DataTable oTable;
            DataColumn oCol, oCol1;
            DataRow oRow;
            oTable = new DataTable("PERIOD");
            oDS.Tables.Add(oTable);
            oCol = new DataColumn("YEAR",typeof(int));
            oTable.Columns.Add(oCol);
            oRow = oTable.NewRow();
            oRow[oCol] = DateTime.Now.Year;
            oTable.Rows.Add(oRow);

            oTable = new DataTable("SELECTEDDATE");
            oDS.Tables.Add(oTable);
            oCol = new DataColumn("VALUE", typeof(DateTime));
            oTable.Columns.Add(oCol);

            DateTime begin = new DateTime(DateTime.Now.Year, 1, 1);
            List<LeavePlan> leavePlanList = HRTMManagement.CreateInstance(Requestor.CompanyCode).GetLeavePlanList(Requestor.EmployeeID, begin, begin.AddYears(1).AddDays(-1));
            foreach (LeavePlan lp in leavePlanList)
            {
                oRow = oTable.NewRow();
                oRow[oCol] = lp.PlanDate;
                oTable.Rows.Add(oRow);
            }

            oTable = new DataTable("SELECTEDDATE_DISABLE");
            oDS.Tables.Add(oTable);
            oCol = new DataColumn("VALUE", typeof(DateTime));
            oTable.Columns.Add(oCol);
            oCol1 = new DataColumn("ISANNUALLEAVE", typeof(bool));
            oTable.Columns.Add(oCol1);

            List<INFOTYPE2001> leaveList =new INFOTYPE2001().GetData(Requestor, begin, begin.AddYears(1));
            foreach (INFOTYPE2001 lp in leaveList)
            {
                if (lp.BeginDate != lp.EndDate && lp.AllDayFlag)
                {
                    for (DateTime rundate = lp.BeginDate; rundate <= lp.EndDate; rundate = rundate.AddDays(1))
                    {
                        oRow = oTable.NewRow();
                        oRow[oCol] = rundate;
                        oRow[oCol1] = lp.AbsenceType == "1100";
                        oTable.Rows.Add(oRow);
                    }
                }
                else
                {
                    oRow = oTable.NewRow();
                    oRow[oCol] = lp.BeginDate;
                    oRow[oCol1] = lp.AbsenceType == "1100";
                    oTable.Rows.Add(oRow);
                }
            }

            oTable = new DataTable("QUOTA");
            oDS.Tables.Add(oTable);
            oCol = new DataColumn("CURRENT_QUOTA", typeof(decimal));
            oTable.Columns.Add(oCol);
            oCol = new DataColumn("MUSTUSE_QUOTA", typeof(decimal));
            oTable.Columns.Add(oCol);

            FillUpQuotaData(Requestor, oTable);

            return oDS;
        }

        public override void PrepareData(EmployeeData Requestor, object ds)
        {
            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            DataTable oPeriodTable = Data.Tables["PERIOD"];
            DataRow oRow;
            if (oPeriodTable.Rows.Count == 0)
            {
                oRow = oPeriodTable.NewRow();
                oRow["YEAR"] = DateTime.Now.Year;
                oPeriodTable.Rows.Add(oRow);
            }
        }

        public override void CalculateInfoData(EmployeeData Requestor, object ds, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());

            DataTable oPeriodTable = Data.Tables["PERIOD"];
            DataRow oRow;
            DataRow oNewRow;
            if (Info.Rows.Count == 0)
            {
                oNewRow = Info.NewRow();
                Info.Rows.Add(oNewRow);
            }
            else
            {
                oNewRow = Info.Rows[0];
            }
            if (oPeriodTable.Rows.Count > 0)
            {
                oRow = oPeriodTable.Rows[0];
                oNewRow["YEAR"] = oRow["YEAR"];
            }
        }

        private void FillUpQuotaData(EmployeeData Requestor, DataTable quotaTable)
        {
            quotaTable.Rows.Clear();
            DataRow oRow = quotaTable.NewRow();
            List<INFOTYPE2006> quotaList =new INFOTYPE2006().GetData(Requestor);
            Decimal quotaAmount = 0.0M;
            foreach (INFOTYPE2006 item in quotaList)
            {
                if (item.SubType.Trim() == "06")
                {
                    quotaAmount += item.QuotaAmount;
                }
            }
            Decimal mustUse =new INFOTYPE2006().CalculateMustUseQuota(Requestor, quotaAmount);
            oRow["CURRENT_QUOTA"] = quotaAmount.ToString("0.00");
            oRow["MUSTUSE_QUOTA"] = mustUse.ToString("0.00");
            quotaTable.Rows.Add(oRow);
        }

        public override void ValidateData(object ds, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            DataSet newData = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            DataTable oTable = newData.Tables["SELECTEDDATE"];
            if (oTable == null)
            {
                throw new DataServiceException("HRTM_EXCEPTION","NOT_FOUND_DATA_[SELECTEDDATE]");
            }
            DataTable oQuotaTable = newData.Tables["QUOTA"];
            FillUpQuotaData(Requestor, oQuotaTable);

            decimal mustHave = (decimal)oQuotaTable.Rows[0]["MUSTUSE_QUOTA"];
            if (oTable.Rows.Count < mustHave)
            {
                throw new DataServiceException("HRTM_EXCEPTION","NOT_ENOUGH_SELECTED_VALUE");
            }

        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object ds, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            if (State == "COMPLETED")
            {
                DataRow oRow = Info.Rows[0];
                int Year = (int)oRow["YEAR"];
                List<LeavePlan> leavePlanList = new List<LeavePlan>();
                foreach (DataRow row in Data.Tables["SELECTEDDATE"].Rows)
                {
                    LeavePlan lp = new LeavePlan();
                    lp.PlanDate = (DateTime)row["VALUE"];
                    lp.EmployeeID = Requestor.EmployeeID;
                    lp.IsCompleted = true;
                    leavePlanList.Add(lp);
                }
                try
                {
                    HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveLeavePlanList(Requestor.EmployeeID ,Year, leavePlanList);
                }
                catch (Exception ex)
                {
                    throw new SaveExternalDataException("Save error", true, ex);
                }
            }
        }
    }
}
