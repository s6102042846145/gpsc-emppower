using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using ESS.DATA;
using ESS.EMPLOYEE;
using ESS.HR.TM.CONFIG;
using ESS.HR.TM.INFOTYPE;
using ESS.WORKFLOW;
using ESS.TIMESHEET;
using ESS.EMPLOYEE.CONFIG;
using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using Newtonsoft.Json;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.HR.TM.DATACLASS;

namespace ESS.HR.TM.DATASERVICE
{
    public class ChangeAbsence : AbstractDataService
    {
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {

            DataSet DS = new DataSet("ADDITIONAL");
            DataTable oTable;
            INFOTYPE2001 oItem = new INFOTYPE2001();
            oTable = oItem.ToADODataTable(true);
            oTable.TableName = "INFOTYPE2001";
            DS.Tables.Add(oTable);

            INFOTYPE2001 objAbsence = new INFOTYPE2001();
            if (!string.IsNullOrEmpty(CreateParam))
            {
                INFOTYPE2001 absence = new INFOTYPE2001();
                absence = absence.Parse(CreateParam);
                objAbsence = absence;
                DataRow dr = DS.Tables["INFOTYPE2001"].NewRow();
                absence.LoadDataToTableRow(dr);
                DS.Tables["INFOTYPE2001"].Rows.Add(dr);
            }


            //INFOTYPE2003 oItem2003 = new INFOTYPE2003();
            //oTable = oItem2003.ToADODataTable();
            //oTable.TableName = "INFOTYPE2003";
            //oTable.Rows.Clear();


            return DS;
        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object ds, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            DataTable oAdditional = Data.Tables["INFOTYPE2001"];
            INFOTYPE2001 oAbsence = new INFOTYPE2001();
            oAbsence.ParseToObject(oAdditional.Rows[0]);

            List<string> postList = new List<string>();
            postList.Add("COMPLETED");

            List<string> cancelList = new List<string>();
            cancelList.Add("CANCELLED");

            List<string> unmarkList = new List<string>();
            unmarkList.Add("CANCELLED");
            unmarkList.Add("COMPLETED");

            // check existing
            if (!cancelList.Contains(State.ToUpper()) && !HRTMManagement.CreateInstance(Requestor.CompanyCode).CheckAbsenceExistingData(Requestor, oAbsence)) //INFOTYPE2001.CheckExistingData(Requestor, oAbsence)
            {
                throw new Exception("Data for cancel can't been found");
            }

            List<INFOTYPE2001> postingList = new List<INFOTYPE2001>();
            if (postList.Contains(State))
            {
                #region " POST "
                // POST DATA
                try
                {
                    if (Data.Tables.Contains("POSTDATA"))
                    {
                        foreach (DataRow dr in Data.Tables["POSTDATA"].Rows)
                        {
                            INFOTYPE2001 item = new INFOTYPE2001();
                            item.ParseToObject(dr);
                            HRTMManagement.CreateInstance(Requestor.CompanyCode).DeleteDelegateByAbsence(item);
                            postingList.Add(item);
                        }


                        Data.Tables.Remove("POSTDATA");
                    }
                    else
                    {
                        postingList = new List<INFOTYPE2001>();
                        oAbsence.IsMark = false;
                        postingList.Add(oAbsence);
                        HRTMManagement.CreateInstance(Requestor.CompanyCode).DeleteDelegateByAbsence(oAbsence);
                    }
                    try
                    {
                        HRTMManagement.CreateInstance(Requestor.CompanyCode).SavePostAbsence(postingList);
                    }
                    catch (Exception ex1)
                    {
                        throw ex1;
                    }
                    finally
                    {
                        INFOTYPE2001 item1 = new INFOTYPE2001();
                        DataTable oTempTable;
                        oTempTable = item1.ToADODataTable(true);
                        oTempTable.TableName = "POSTDATA";
                        Data.Tables.Add(oTempTable);
                        foreach (INFOTYPE2001 item in postingList)
                        {
                            DataRow oNewRow = oTempTable.NewRow();
                            item.LoadDataToTableRow(oNewRow);
                            oTempTable.LoadDataRow(oNewRow.ItemArray, false);
                        }
                    }

                    try
                    {
                        if (postList.Contains(State))
                        {

                            foreach (INFOTYPE2001 oAb in postingList)
                            {
                                if (oAb.BeginTime == TimeSpan.Zero && oAb.EndTime == TimeSpan.Zero)
                                {
                                    oAb.AllDayFlag = true;
                                }
                                AbsAttCalculateResult oResult1;
                                if (oAb.AllDayFlag)
                                {
                                    oResult1 = HRTMManagement.CreateInstance(Requestor.CompanyCode).CalculateAbsAttEntry(oAbsence.EmployeeID, oAbsence.AbsenceType, oAbsence.BeginDate, oAbsence.EndDate, TimeSpan.MinValue, TimeSpan.MinValue, true, false, false);
                                }
                                else
                                {
                                    oResult1 = HRTMManagement.CreateInstance(Requestor.CompanyCode).CalculateAbsAttEntry(oAbsence.EmployeeID, oAbsence.AbsenceType, oAbsence.BeginDate, oAbsence.EndDate, oAbsence.BeginTime, oAbsence.EndTime, false, false, false);
                                }
                                oAb.AbsenceDays = oResult1.AbsenceDays;
                                oAb.AbsenceHours = oResult1.AbsenceHours;
                                oAb.PayrollDays = oResult1.PayrollDays;
                                oAb.RequestNo = RequestNo;
                            }

                            HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveLG_TIMEREQUEST(postingList, "CHANGEABSENCE");
                        }
                    }
                    catch (Exception er)
                    {

                    }

                }
                catch (Exception ex)
                {
                    throw new SaveExternalDataException("SaveExternalData Error", true, ex);
                }
                #endregion
            }
            if (oAbsence.AllDayFlag)
            {
                HRTMManagement.CreateInstance(Requestor.CompanyCode).MarkTimesheet(Requestor.EmployeeID, "CANCELLEAVE", oAbsence.AllDayFlag, oAbsence.BeginDate.Date, oAbsence.EndDate.Date, !unmarkList.Contains(State.ToUpper()), false, RequestNo);
            }
            else
            {
                HRTMManagement.CreateInstance(Requestor.CompanyCode).MarkTimesheet(Requestor.EmployeeID, "CANCELLEAVE", oAbsence.AllDayFlag, oAbsence.BeginDate.Date.Add(oAbsence.BeginTime), oAbsence.EndDate.Add(oAbsence.EndTime), !unmarkList.Contains(State.ToUpper()), false, RequestNo);
            }
        }

        public override void PrepareData(EmployeeData Requestor, object Data)
        {
        }

        public override void ValidateData(object ds, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            DataSet newData = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            if (newData.Tables["INFOTYPE2001"].Rows.Count == 0)
            {
                throw new DataServiceException("HRTM_EXCEPTION", "NOT_FOUND_ORIGINAL_RECORD");
            }
            DataTable oAdditional = newData.Tables["INFOTYPE2001"];
            INFOTYPE2001 oAbsence = new INFOTYPE2001();
            oAbsence.ParseToObject(oAdditional.Rows[0]);
            try
            {
                #region " CheckCollision "
                if (newData.Tables.Contains("POSTDATA"))
                {
                    newData.Tables.Remove("POSTDATA");
                }
                if (oAbsence.AllDayFlag)
                {
                    HRTMManagement.CreateInstance(Requestor.CompanyCode).MarkTimesheet(Requestor.EmployeeID, "CANCELLEAVE", true, oAbsence.BeginDate, oAbsence.EndDate, true, true, RequestNo);
                }
                else
                {
                    HRTMManagement.CreateInstance(Requestor.CompanyCode).MarkTimesheet(Requestor.EmployeeID, "CANCELLEAVE", false, oAbsence.BeginDate.Add(oAbsence.BeginTime), oAbsence.EndDate.Add(oAbsence.EndTime), true, true, RequestNo);
                }
                #endregion

                // check existing
                if (!HRTMManagement.CreateInstance(Requestor.CompanyCode).CheckAbsenceExistingData(Requestor, oAbsence))
                {
                    throw new DataServiceException("HRTM_EXCEPTION", "NOT_FOUND_ORIGINAL_RECORD");
                }

                // check cancel Rule
                AbsenceCancelRule oCancelRule = HRTMManagement.CreateInstance(Requestor.CompanyCode).GetCancelRule(Requestor, oAbsence.AbsenceType);// AbsenceCancelRule.GetCancelRule(oAbsence.EmployeeID, oAbsence.AbsenceType);
                oCancelRule.IsCanCancel(oAbsence.EmployeeID, oAbsence.BeginDate, oAbsence.EndDate, DocumentDate);
            }
            catch (AbsenceCancelException e)
            {
                throw new DataServiceException("HRTM_EXCEPTION", "CONFLICT_RULE", "", e);
            }
            catch (TimesheetCollisionException e)
            {
                throw new DataServiceException("HRTM_EXCEPTION", "COLLISION_OCCUR", "", e);
            }
            catch (Exception e)
            {
                string Err = HRTMManagement.CreateInstance(Requestor.CompanyCode).ShowError("HRTM_EXCEPTION", e.Message.ToString(), WorkflowPrinciple.Current.UserSetting.Language);
                throw new Exception(Err);
                //throw new DataServiceException("HRTM_EXCEPTION", e.Message, "");
            }
        }

        public override void CalculateInfoData(EmployeeData Requestor, object ds, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            Info.Rows.Clear();
            DataRow oRow = Info.NewRow();
            INFOTYPE2001 item = new INFOTYPE2001();
            item.ParseToObject(Data.Tables["INFOTYPE2001"].Rows[0]);
            oRow["ABSENCETYPECODE"] = string.Format("{0}#{1}", Requestor.OrgAssignment.SubAreaSetting.AbsAttGrouping, item.AbsenceType);
            oRow["ABSENCETYPE"] = item.AbsenceType;
            oRow["BEGINDATE"] = item.BeginDate;
            oRow["ENDDATE"] = item.EndDate;
            oRow["PAYROLLDAYS"] = item.PayrollDays;
            Info.Rows.Add(oRow);
        }

        public override string GenerateFlowKey(EmployeeData Requestor, DataTable Info)
        {
            decimal dMaxLevelMD = HRTMManagement.CreateInstance(Requestor.CompanyCode).GetMaxLevelMD();
            if (dMaxLevelMD != -1 && Requestor.EmpSubGroup == dMaxLevelMD)
            {
                return "MD_CREATE";
            }
            return "NORMAL";
        }
    }
}
