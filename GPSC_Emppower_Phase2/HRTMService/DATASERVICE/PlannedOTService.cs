using System;
using System.Globalization;
using System.Collections.Generic;
using System.Data;
using System.Text;
using ESS.DATA;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.CONFIG;
using ESS.HR.TM.DATACLASS;
using ESS.TIMESHEET;
using ESS.DATA.ABSTRACT;
using ESS.HR.TM.INFOTYPE;
using Newtonsoft.Json;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE.CONFIG.TM;

namespace ESS.HR.TM.DATASERVICE
{
    public class PlannedOTService : AbstractDataService
    {
        CultureInfo oCL = new CultureInfo("en-US");
        public PlannedOTService()
        {
        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object ds, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            DataTable oAdditional = Data.Tables["PLANNEDOT"];
            DataTable oHeader = Data.Tables["HEADER"];
            string AssignFrom = (string)oHeader.Rows[0]["ASSIGNFROM"];
            string Period = (string)oHeader.Rows[0]["OTPERIOD"];
            SaveOTData item;

            List<string> unmarkList = new List<string>();
            unmarkList.Add("CANCELLED");

            List<string> completedList = new List<string>();
            completedList.Add("COMPLETED");

            List<SaveOTData> saveList = new List<SaveOTData>();

            foreach (DataRow dr in oAdditional.Rows)
            {
                item = new SaveOTData();
                item.AssignFrom = AssignFrom;
                item.IsCompleted = completedList.Contains(State.ToUpper());
                item.OTPeriod = Period;
                item.RequestNo = RequestNo;
                item.OTType = "P";
                item.OTItemTypeID = 0;
                item.ParseToObject(dr);
                saveList.Add(item);
            }

            //HR.TM.ServiceManager.HRTMBuffer.SaveOTList(saveList, !unmarkList.Contains(State.ToUpper()));
            
            HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveOTList(saveList, !unmarkList.Contains(State.ToUpper()));
        }

        #region " GenerateAdditionalData "
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            DataSet DS = new DataSet("ADDITIONAL");
            DS.Tables.Add(CreatePlannedOTTable());
            DS.Tables.Add(CreateHeaderTable(Requestor));
            return DS;
        }
        #endregion

        #region " CreateTemplateTable "
        private DataTable CreatePlannedOTTable()
        {
            DataTable oTable;
            PlannedOT oItem = new PlannedOT();
            oTable = oItem.ToADODataTable();
            oTable.TableName = "PLANNEDOT";
            oTable.Rows.Clear();
            return oTable;
        }

        private DataTable CreateHeaderTable(EmployeeData Requestor)
        {
            DataTable oTable;
            oTable = new DataTable("HEADER");
            oTable.Columns.Add("ASSIGNFROM", typeof(string));
            oTable.Columns.Add("OTPERIOD", typeof(string));

            DataRow dr = oTable.NewRow();
            EmployeeData oManager_Band_D = Requestor.FindManager("#MANAGER_BAND_D");
            if (oManager_Band_D != null)
            {
                dr["ASSIGNFROM"] = oManager_Band_D.EmployeeID;
            }
            else
            {
                dr["ASSIGNFROM"] = "";
            }
            dr["OTPERIOD"] = DateTime.Now.ToString("yyyyMM");
            oTable.Rows.Add(dr);
            return oTable;
        }
        #endregion

        #region " PrepareData "
        public override void PrepareData(EmployeeData Requestor, object ds)
        {
            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            if (!Data.Tables.Contains("PLANNEDOT"))
            {
                Data.Tables.Add(CreatePlannedOTTable());
            }
            if (!Data.Tables.Contains("HEADER"))
            {
                Data.Tables.Add(CreateHeaderTable(Requestor));
            }
        }
        #endregion

        #region " CalculateInfoData "
        public override void CalculateInfoData(EmployeeData Requestor, object ds, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            DataTable oHeader = Data.Tables["HEADER"];
            DataRow dr;
            Info.Rows.Clear();
            dr = Info.NewRow();
            dr["TOTALAMOUNT"] = SumOT(Data.Tables["PLANNEDOT"]);
            dr["ASSIGNFROM"] = oHeader.Rows[0]["ASSIGNFROM"];
            dr["OTPERIOD"] = oHeader.Rows[0]["OTPERIOD"];
            Info.Rows.Add(dr);
        }
        #endregion

        #region " SumOT "
        private Decimal SumOT(DataTable otTable)
        {
            Decimal oReturn = 0.0M;
            PlannedOT oItem;
            foreach (DataRow dr in otTable.Rows)
            {
                oItem = new PlannedOT();
                oItem.ParseToObject(dr);
                oReturn += oItem.OTHours;
            }
            return oReturn;
        }
        #endregion

        #region " ValidateData "
        public override void ValidateData(object ds, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            DataSet newData = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            if (!newData.Tables.Contains("PLANNEDOT"))
            {
                throw new DataServiceException("HRTM_EXCEPTION", "CAN_NOT_FIND_PLANNED_OT_DATA");
            }
            DataTable oTable = newData.Tables["PLANNEDOT"];
            DataTable oHeader = newData.Tables["HEADER"];
            DateTime myPeriod = DateTime.MinValue;
            string cAssignFrom = (string)oHeader.Rows[0]["ASSIGNFROM"];
            if (!HRTMManagement.CreateInstance(Requestor.CompanyCode).IsCanOTAssignFrom(new EmployeeData(cAssignFrom)))
            {
                throw new DataServiceException("HRTM_EXCEPTION", "OT_ASSIGN_FROM_NOT_CORRECT");
            }
            string cPeriod = (string)oHeader.Rows[0]["OTPERIOD"];
            DateTime.TryParseExact(cPeriod,"yyyyMM",oCL,DateTimeStyles.None,out myPeriod);
            if (myPeriod == DateTime.MinValue)
            {
                throw new DataServiceException("HRTM_EXCEPTION", "PERIOD_NOTFOUND");
            }
            else
            {
                PlannedOT oItem;
                MonthlyWS MWS = MonthlyWS.GetCalendar(Requestor.EmployeeID, myPeriod.Year, myPeriod.Month);
                DataTable oCheckTable = oTable.Copy();
                DataColumn oDC;
                oDC = new DataColumn("BeginDate", typeof(DateTime));
                oCheckTable.Columns.Add(oDC);
                oDC = new DataColumn("EndDate", typeof(DateTime));
                oCheckTable.Columns.Add(oDC);
                oDC = new DataColumn("Index", typeof(int));
                oCheckTable.Columns.Add(oDC);
                for (int index = 0; index < oCheckTable.Rows.Count; index++ )
                {
                    DataRow dr = oCheckTable.Rows[index];
                    oItem = new PlannedOT();
                    oItem.ParseToObject(dr);
                    dr["BeginDate"] = oItem.OTDate.Add(oItem.BeginTime);
                    dr["EndDate"] = oItem.OTDate.Add(oItem.EndTime);
                    dr["Index"] = index;
                }
                DataView oDV = new DataView(oCheckTable);
                string exp = "BeginDate < '{0}' And EndDate > '{1}' And Index <> {2} ";
                for (int index = 0; index < oTable.Rows.Count; index++)
                {
                    DataRow dr = oTable.Rows[index];
                    oItem = new PlannedOT();
                    oItem.ParseToObject(dr);
                    DateTime BeginDate, EndDate;
                    EndDate = oItem.OTDate.Add(oItem.EndTime);
                    BeginDate = oItem.OTDate.Add(oItem.BeginTime);
                    oDV.RowFilter = string.Format(exp, EndDate, BeginDate, index);
                    if (oDV.Count > 0)
                    {
                        throw new OTCollistionException(index,(int)oDV[0]["index"]);
                    }
                    if (oItem.OTDate < DocumentDate.Date.AddDays(OTManagement.PlannedOTOffset))
                    {
                        throw new PlannedOTConflictException(oItem.OTDate, OTManagement.PlannedOTOffset);
                    }
                    if (oItem.Description == "")
                    {
                        throw new DataServiceException("HRTM_EXCEPTION", "NOT_FOUND_DESCRIPTION");
                    }
                    oItem.Calculate(MWS);
                    if (oItem.BeginTime == TimeSpan.MinValue || oItem.EndTime == TimeSpan.MinValue)
                    {
                        throw new DataServiceException("HRTM_EXCEPTION", "OT_TIME_NOT_CORRECT");
                    }
                    else if (oItem.OTHours == 0)
                    {
                        throw new DataServiceException("HRTM_EXCEPTION", "OT_ZERO_HOUR");
                    }
                    try
                    {
                        HRTMManagement.CreateInstance(Requestor.CompanyCode).MarkTimesheet(Requestor.EmployeeID, "PLANNEDOT", false, oItem.OTDate.Add(oItem.BeginTime), oItem.OTDate.Add(oItem.EndTime), true, true, RequestNo);
                    }
                    catch (TimesheetCollisionException e)
                    {
                        //throw new OTCollistionException(index,e.CollissionData[0]);
                    }
                    oItem.LoadDataToTableRow(dr);
                }
            }
        }
        #endregion
    }
}
