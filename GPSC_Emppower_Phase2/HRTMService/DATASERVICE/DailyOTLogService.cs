using System;
using System.Collections.Generic;
using System.Globalization;
using System.Data;
using System.Text;
using ESS.DATA;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.CONFIG;
using ESS.HR.TM.INFOTYPE;
using ESS.TIMESHEET;
using System.Configuration;
using System.Reflection;
using ESS.DATA.ABSTRACT;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.DATA.EXCEPTION;
using Newtonsoft.Json;
using ESS.HR.TM.DATACLASS;
using System.Linq;
using ESS.WORKFLOW;

namespace ESS.HR.TM.DATASERVICE
{
    public class DailyOTLogService : AbstractDataService
    {
        private static Configuration config
        {
            get
            {
                return ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().CodeBase.Replace("file:///", "").Replace("/", "\\"));
            }
        }
        private static int TIMEOUTDATE
        {
            get
            {
                return Convert.ToInt32(config.AppSettings.Settings["OTTIMEOUTDATE"].Value);
            }
        }

        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            DataSet DS = new DataSet("ADDITIONAL");
            DataTable oTable, oInfo;
            DataColumn column;
            DailyOTLog oItem = new DailyOTLog();
            oTable = oItem.ToADODataTable(true);
            oTable.TableName = "DailyOTLog";

            column = new DataColumn();
            column.ColumnName = "CREATOR";
            column.DataType = Type.GetType("System.String");
            oTable.Columns.Add(column);
            oTable.Rows.Add(oTable.NewRow());
            DS.Tables.Add(oTable);

            oInfo = new DataTable();
            oInfo.TableName = "InfoTable";

            column = new DataColumn();
            column.ColumnName = "TOTALAMOUNT";
            column.DataType = Type.GetType("System.Decimal");
            oInfo.Columns.Add(column);

            column = new DataColumn();
            column.ColumnName = "OTDATE";
            column.DataType = Type.GetType("System.DateTime");
            oInfo.Columns.Add(column);

            column = new DataColumn();
            column.ColumnName = "REQUESTORLEVEL";
            column.DataType = Type.GetType("System.Decimal");
            oInfo.Columns.Add(column);

            column = new DataColumn();
            column.ColumnName = "OTTYPE";
            column.DataType = Type.GetType("System.String");
            oInfo.Columns.Add(column);

            column = new DataColumn();
            column.ColumnName = "BEGINTIME";
            column.DataType = Type.GetType("System.String");
            oInfo.Columns.Add(column);

            column = new DataColumn();
            column.ColumnName = "ENDTIME";
            column.DataType = Type.GetType("System.String");
            oInfo.Columns.Add(column);

            column = new DataColumn();
            column.ColumnName = "DESCRIPTION";
            column.DataType = Type.GetType("System.String");
            oInfo.Columns.Add(column);

            column = new DataColumn();
            column.ColumnName = "selectedBeginH";
            column.DataType = Type.GetType("System.String");
            oInfo.Columns.Add(column);
            column = new DataColumn();
            column.ColumnName = "selectedBeginM";
            column.DataType = Type.GetType("System.String");
            oInfo.Columns.Add(column);
            column = new DataColumn();
            column.ColumnName = "selectedEndH";
            column.DataType = Type.GetType("System.String");
            oInfo.Columns.Add(column);
            column = new DataColumn();
            column.ColumnName = "selectedEndM";
            column.DataType = Type.GetType("System.String");
            oInfo.Columns.Add(column);


            column = new DataColumn();
            column.ColumnName = "REPORTTO";
            column.DataType = Type.GetType("System.String");
            oInfo.Columns.Add(column);
            oInfo.Rows.Add(oInfo.NewRow());
            oInfo.Rows[0]["REQUESTORLEVEL"] = Requestor.EmpSubGroup.ToString();
            oInfo.Rows[0]["selectedBeginH"] = "00";
            oInfo.Rows[0]["selectedBeginM"] = "00";
            oInfo.Rows[0]["selectedEndH"] = "00";
            oInfo.Rows[0]["selectedEndM"] = "00";
            oInfo.Rows[0]["OTDATE"] = DateTime.Now.Date;
            DS.Tables.Add(oInfo);

            return DS;
        }

        public override void CalculateInfoData(EmployeeData Requestor, object ds, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            Info.Clear();
            DataRow NewRow;
            foreach (DataRow row in Data.Tables["InfoTable"].Rows)
            {
                NewRow = Info.NewRow();
                NewRow.BeginEdit();
                //NewRow["TOTALAMOUNT"] = row["TOTALAMOUNT"];
                NewRow["TOTALAMOUNT"] = HRTMManagement.CreateInstance(Requestor.CompanyCode).GetOTSummaryInMonth(Requestor.EmployeeID, Convert.ToDateTime(row["BEGINTIME"]));
                NewRow["OTDATE"] = row["OTDATE"];
                NewRow["REQUESTORLEVEL"] = row["REQUESTORLEVEL"];
                NewRow["OTTYPE"] = row["OTTYPE"];
                NewRow["BEGINTIME"] = row["BEGINTIME"];
                NewRow["ENDTIME"] = row["ENDTIME"];
                NewRow["REPORTTO"] = row["REPORTTO"];
                NewRow.EndEdit();
                Info.Rows.Add(NewRow);
            }
        }

        public override void PrepareData(EmployeeData Requestor, object Data)
        {

        }

        public class DailyOTLogModel
        {
            public int Id { get; set; }
            public DateTime Date { get; set; }
            public TimeSpan BeginTime { get; set; }
            public TimeSpan EndTime { get; set; }
        }
        public class DailyOTDuplicate
        {
            public DateTime DateTimeOTStart { get; set; }
            public DateTime DateTimeOTEnd { get; set; }
            public TimeSpan StartTime { get; set; }
            public TimeSpan EndTime { get; set; }
        }
        public override void ValidateData(object ds, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            DataSet newData = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            DataTable oTable = newData.Tables["DailyOTLog"];
            var info = newData.Tables["InfoTable"];

            var i = 0;

            //validate Date
            var validateDailys = new List<DailyOTLogModel>();
            int index = 1;
            foreach (DataRow ot in info.Rows)
            {
                if (string.IsNullOrEmpty(ot["OTDATE"].ToString()))
                {
                    throw new DataServiceException("HRTM_EXCEPTION", "NOT_FOUND_ORIGINAL_RECORD");
                }
                validateDailys.Add(new DailyOTLogModel()
                {
                    Id = index,
                    Date = DateTime.Parse(ot["OTDATE"].ToString()),
                    BeginTime = TimeSpan.Parse(String.Format("{0}:{1}", ot["selectedBeginH"].ToString(), ot["selectedBeginM"].ToString())),
                    EndTime = TimeSpan.Parse(String.Format("{0}:{1}", ot["selectedEndH"].ToString(), ot["selectedEndM"].ToString()))
                });
                index++;
            }
            index = 0;


            if (oTable.Rows.Count == info.Rows.Count)
            {
                for (int idx = 0; idx < info.Rows.Count; idx++)
                {
                    if (oTable.Rows[idx]["BeginDate"] == null ||
                        Convert.ToDateTime(info.Rows[idx]["OTDATE"]) != Convert.ToDateTime(oTable.Rows[idx]["BeginDate"]).Date ||
                        TimeSpan.Parse(info.Rows[idx]["BEGINTIME"].ToString()) != Convert.ToDateTime(oTable.Rows[idx]["BeginDate"]).TimeOfDay ||
                        TimeSpan.Parse(info.Rows[idx]["ENDTIME"].ToString()) != Convert.ToDateTime(oTable.Rows[idx]["EndDate"]).TimeOfDay ||
                        info.Rows[idx]["DESCRIPTION"].ToString() != oTable.Rows[idx]["DESCRIPTION"].ToString())
                    {
                        DateTime infOTDate = Convert.ToDateTime(info.Rows[idx]["OTDATE"]);
                        TimeSpan infBEGINTIME = TimeSpan.Parse(info.Rows[idx]["BEGINTIME"].ToString());
                        TimeSpan infENDTIME = TimeSpan.Parse(info.Rows[idx]["ENDTIME"].ToString());
                        oTable.Rows[idx]["BeginDate"] = infOTDate.Add(infBEGINTIME);
                        if (infENDTIME >= infBEGINTIME)
                        {
                            oTable.Rows[idx]["EndDate"] = infOTDate.Add(infENDTIME);
                        }
                        else
                        {
                            oTable.Rows[idx]["EndDate"] = infOTDate.AddDays(1).Add(infENDTIME);
                        }
                        oTable.Rows[idx]["DESCRIPTION"] = info.Rows[idx]["DESCRIPTION"].ToString();
                    }
                }
            }


            List<DailyOTDuplicate> ListDuplicateOT = new List<DailyOTDuplicate>();
            decimal totalOT = 0;
            foreach (DataRow ot in info.Rows)
            {
                decimal monthlyOTHours = HRTMManagement.CreateInstance(Requestor.CompanyCode).GetSummaryOTInMonth(Requestor.EmployeeID, ot["Period"].ToString(), RequestNo);
                DateTime period = DateTime.ParseExact(ot["Period"].ToString(), "yyyyMM", new CultureInfo("en-US"));
                if (String.IsNullOrEmpty(ot["selectedBeginH"].ToString()) || String.IsNullOrEmpty(ot["selectedBeginM"].ToString())
              || String.IsNullOrEmpty(ot["selectedEndH"].ToString()) || String.IsNullOrEmpty(ot["selectedEndM"].ToString()))
                    throw new DataServiceException("HRTM_EXCEPTION", "INVALID_DATETIME");

                var beginTime = TimeSpan.Parse(String.Format("{0}:{1}", ot["selectedBeginH"].ToString(), ot["selectedBeginM"].ToString()));
                var endTime = TimeSpan.Parse(String.Format("{0}:{1}", ot["selectedEndH"].ToString(), ot["selectedEndM"].ToString()));
                var employeeId = Requestor.EmployeeID;

                var otDate = DateTime.Parse(ot["OTDATE"].ToString());
                if (otDate == DateTime.MinValue || beginTime == TimeSpan.MinValue || endTime == TimeSpan.MinValue)
                {
                    throw new DataServiceException("HRTM_EXCEPTION", "INVALID_DATETIME");
                }

                var item = HRTMManagement.CreateInstance(Requestor.CompanyCode).CalculateOT(employeeId, otDate.Add(beginTime), otDate.Add(endTime), ot["description"].ToString(), "", false, false, period.Year, period.Month);
                oTable.Rows[i]["ClockTimePairs"] = item.ClockTimePairs;
                oTable.Rows[i]["FinalTimePairs"] = item.FinalTimePairs;
                oTable.Rows[i]["EvaTimePairs"] = item.EvaTimePairs;
                oTable.Rows[i]["Remark"] = item.Remark;
                if (item.OTClockINOUT.Count > 0)
                {
                    oTable.Rows[i]["OTClockINOUT"] = item.OTClockINOUT;
                }

                oTable.Rows[i]["RefRequestNo"] = item.RefRequestNo;
                oTable.Rows[i]["Description"] = item.Description;
                oTable.Rows[i]["RequestNo"] = item.RequestNo;
                oTable.Rows[i]["IsPayroll"] = item.IsPayroll;
                oTable.Rows[i]["FinalOTHour30"] = item.FinalOTHour30;
                oTable.Rows[i]["FinalOTHour15"] = item.FinalOTHour15;
                oTable.Rows[i]["FinalOTHour10"] = item.FinalOTHour10;
                oTable.Rows[i]["EvaOTHour30"] = item.EvaOTHour30;
                oTable.Rows[i]["EvaOTHour15"] = item.EvaOTHour15;
                oTable.Rows[i]["EvaOTHour10"] = item.EvaOTHour10;
                oTable.Rows[i]["Status"] = item.Status;
                oTable.Rows[i]["OTWorkTypeID"] = ot["OTTYPE"].ToString();
                oTable.Rows[i]["RequestOTHour30"] = item.RequestOTHour30;
                oTable.Rows[i]["RequestOTHour15"] = item.RequestOTHour15;
                oTable.Rows[i]["RequestOTHour10"] = item.RequestOTHour10;
                oTable.Rows[i]["EndDate"] = item.EndDate;
                oTable.Rows[i]["BeginDate"] = item.BeginDate;
                oTable.Rows[i]["EmployeeID"] = item.EmployeeID;
                oTable.Rows[i]["CREATOR"] = item.EmployeeID;

                totalOT += item.RequestOTHour10 + item.RequestOTHour15 + item.RequestOTHour30;
                ot["TOTALAMOUNT"] = Convert.ToString(Math.Round((totalOT + monthlyOTHours) / 60, 2, MidpointRounding.ToEven));

                i++;

                DailyOTDuplicate newDuplicateOt = new DailyOTDuplicate()
                {
                    DateTimeOTStart = item.BeginDate,
                    DateTimeOTEnd = item.EndDate
                };

                ListDuplicateOT.Add(newDuplicateOt);
            }

            bool overlap = ListDuplicateOT.Any(r => ListDuplicateOT.Where(q => q != r).Any(q => q.DateTimeOTEnd > r.DateTimeOTStart && q.DateTimeOTStart < r.DateTimeOTEnd));
            if(overlap)
            {
                throw new DataServiceException("HRTM_EXCEPTION", "OTLIST_DUPLICATE");
            }


            if (oTable.Rows.Count == 0)
            {
                throw new DataServiceException("HRTM_EXCEPTION", "NOT_FOUND_ORIGINAL_RECORD");
            }

            foreach (DataRow row in oTable.Rows)
            {
                if (DateTime.Parse(row["BeginDate"].ToString()) >= DateTime.Parse(row["EndDate"].ToString()))
                {
                    throw new DataServiceException("HRTM_EXCEPTION", "INVALID_DATETIME");
                    break;
                }

                //if (HR.TM.ServiceManager.HRTMBuffer.OverlapDialyOTLog(row["EmployeeID"].ToString(), (DateTime)row["BeginDate"], (DateTime)row["EndDate"],RequestNo))
                if (HRTMManagement.CreateInstance(Requestor.CompanyCode).OverlapDialyOTLog(row["EmployeeID"].ToString(), DateTime.Parse(row["BeginDate"].ToString()), DateTime.Parse(row["EndDate"].ToString()), RequestNo))
                {
                    string Err = HRTMManagement.CreateInstance(Requestor.CompanyCode).ShowError("HRTM_EXCEPTION", String.Format("OVERLAP_REQUEST|{0}", DateTime.Parse(row["BeginDate"].ToString()).ToString("d/M/yyyy HH:mm") + " - " + DateTime.Parse(row["EndDate"].ToString()).ToString("HH:mm")), WorkflowPrinciple.Current.UserSetting.Language);
                    throw new Exception(Err);
                    break;
                }

                if (WorkflowPrinciple.Current.IsInRole("#MANAGER"))
                {
                    if (!ValidateManager(Requestor, DateTime.Parse(row["EndDate"].ToString())))
                    {
                        throw new DataServiceException("HRTM_EXCEPTION", "INVALID_DATETIME_PREVIOUSDAILYOTMANAGER");
                        break;
                    }
                }
                else if ((!ValidateBegin(Requestor, DateTime.Parse(row["EndDate"].ToString()),RequestNo))
                        && !new EmployeeData().GetUserSetting(row["CREATOR"].ToString()).Employee.IsInRole("TIMEADMIN"))
                {
                    throw new DataServiceException("HRTM_EXCEPTION", "INVALID_DATETIME");
                    break;
                }

                if (String.IsNullOrEmpty(row["Description"].ToString()))
                {
                    throw new DataServiceException("HRTM_EXCEPTION", "INCOMPLETE_DATA");
                    break;
                }
            }
        }

        DateTime GenValidateBegin(EmployeeData Requestor)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            int PreviousDay = 0;
            DateTime DateNow = DateTime.Now.AddDays(-1).Date;
            MonthlyWS MWS = MonthlyWS.GetCalendar(Requestor.EmployeeID, DateNow.Year, DateNow.Month);
            do
            {
                if (!Requestor.GetDailyWorkSchedule(DateNow).IsDayOff && MWS.GetWSCode(DateNow.Day, false) != "1")
                    PreviousDay++;
                DateNow = DateNow.AddDays(-1);

            } while (PreviousDay < HRTMManagement.CreateInstance(Requestor.CompanyCode).VALIDATEPREVIOUSREQUESTDAILYOT && DateNow > DateTime.Now.AddMonths(-2));
            if (PreviousDay == 0)
                throw new DataServiceException("HRTM_EXCEPTION", "NOTFOUND_WORK_PREVIOUSDAY");
            else
                return DateTime.Now.AddDays(-PreviousDay);
        }

        bool ValidateBegin(EmployeeData Requestor, DateTime begindate, string RequestNo)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            DateTime DateNow = DateTime.Now.AddDays(-1).Date;


            if (RequestNo != "DUMMY")
            {
                RequestDocument doc = RequestDocument.LoadDocumentReadOnly(RequestNo, Requestor.CompanyCode, Requestor, true, false);
                if (doc != null && doc.CurrentFlowItem.Code != "DRAFT")
                {
                    DateNow = doc.SubmittedDate.Date;
                }
            }


            int PreviousDay = 0;
            MonthlyWS MWS = MonthlyWS.GetCalendar(Requestor.EmployeeID, DateNow);
            EmployeeData oEmp = new EmployeeData(Requestor.EmployeeID, DateNow);

            for (DateTime iDate = DateNow; iDate.Date >= begindate.Date; iDate = iDate.AddDays(-1))
            {
                if (oEmp.OrgAssignment.EndDate.Date < iDate || oEmp.OrgAssignment.BeginDate.Date > iDate)
                {
                    oEmp = new EmployeeData(oEmp.EmployeeID, iDate);
                }

                if (MWS.WS_Year != iDate.Year || MWS.WS_Month != iDate.Month)
                    MWS = MonthlyWS.GetCalendar(Requestor.EmployeeID, iDate);
                //นับ PreviousDay ก็ต่อเมื่อ (วันนั้นๆ ไม่ใช่วันหยุด) หรือ (วันที่ขอตรงกับวันนั้นๆ และ วันที่ขอเป็นวันหยุด)
                if ((!oEmp.GetDailyWorkSchedule(iDate).IsDayOff && MWS.GetWSCode(iDate.Day, false) != "1")
                    || (iDate.Date == begindate.Date && (MWS.GetWSCode(iDate.Day, false) == "1" || oEmp.GetDailyWorkSchedule(iDate).IsDayOff)))
                    PreviousDay++;
            }
            if (DateTime.Now <= begindate)
                return true;

            if (PreviousDay == 0 && begindate.Date < DateNow.Date)
                throw new DataServiceException("HRTM_EXCEPTION", "NOTFOUND_WORK_PREVIOUSDAY");
            else if (PreviousDay > HRTMManagement.CreateInstance(Requestor.CompanyCode).VALIDATEPREVIOUSREQUESTDAILYOT)
                return false;
            return true;
        }


        bool ValidateManager(EmployeeData Requestor, DateTime begindate)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            DateTime DateNow = DateTime.Now.AddDays(-1).Date;
            int PreviousDay = 0;
            MonthlyWS MWS = MonthlyWS.GetCalendar(Requestor.EmployeeID, DateNow);
            EmployeeData oEmp = new EmployeeData(Requestor.EmployeeID, DateNow);

            for (DateTime iDate = DateNow; iDate.Date >= begindate.Date; iDate = iDate.AddDays(-1))
            {
                if (oEmp.OrgAssignment.EndDate.Date < iDate || oEmp.OrgAssignment.BeginDate.Date > iDate)
                {
                    oEmp = new EmployeeData(oEmp.EmployeeID, iDate);
                }

                if (MWS.WS_Year != iDate.Year || MWS.WS_Month != iDate.Month)
                    MWS = MonthlyWS.GetCalendar(Requestor.EmployeeID, iDate);
                //นับ PreviousDay ก็ต่อเมื่อ (วันนั้นๆ ไม่ใช่วันหยุด) หรือ (วันที่ขอตรงกับวันนั้นๆ และ วันที่ขอเป็นวันหยุด)
                if ((!oEmp.GetDailyWorkSchedule(iDate).IsDayOff && MWS.GetWSCode(iDate.Day, false) != "1")
                    || (iDate.Date == begindate.Date && (MWS.GetWSCode(iDate.Day, false) == "1" || oEmp.GetDailyWorkSchedule(iDate).IsDayOff)))
                    PreviousDay++;
            }
            if (DateTime.Now <= begindate)
                return true;

            if (PreviousDay == 0 && begindate.Date < DateNow.Date)
                throw new DataServiceException("HRTM_EXCEPTION", "NOTFOUND_WORK_PREVIOUSDAY");
            else if (PreviousDay > HRTMManagement.CreateInstance(Requestor.CompanyCode).VALIDATEPREVIOUSDAILYOTMANAGER)
                return false;
            return true;
        }

        //AddBy: Ratchatawan W. (2011-09-29)
        public override string CheckRequestIsTimeout(DateTime CreatedDate, DataTable Info)
        {
            string strResult = string.Empty;
            if (Info.Rows.Count > 0)
            {
                DateTime OTDATE = DateTime.Now;
                //For TEST
                if (!string.IsNullOrEmpty(Info.Rows[0]["OTDATE"].ToString()))
                    DateTime.TryParse(Info.Rows[0]["OTDATE"].ToString(), out OTDATE);

                //For some absencetype that can't create request backward
                DateTime NextMonth2nd = new DateTime(OTDATE.AddMonths(1).Year, OTDATE.AddMonths(1).Month, TIMEOUTDATE, 23, 59, 59);
                DateTime Month1Ago = (OTDATE.AddMonths(-1).Date);

                //For some absencetype that can create request backward 3 days
                DateTime NextMonth2ndBackward = new DateTime(CreatedDate.AddMonths(1).Year, CreatedDate.AddMonths(1).Month, TIMEOUTDATE, 23, 59, 59);
                DateTime Month1AgoBackward = (CreatedDate.AddMonths(-1).Date);


                if (DateTime.Now > NextMonth2ndBackward)
                    strResult = "ACTIONBEFOREDAY2OFNEXTMONTH";
            }
            return strResult;
        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object ds, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            DataTable oAdditional = Data.Tables["DailyOTLog"];


            var info = Data.Tables["InfoTable"];

            if (oAdditional.Rows.Count == info.Rows.Count)
            {
                for (int idx = 0; idx < info.Rows.Count; idx++)
                {
                    if (oAdditional.Rows[idx]["BeginDate"] == null || 
                        Convert.ToDateTime(info.Rows[idx]["OTDATE"]) != Convert.ToDateTime(oAdditional.Rows[idx]["BeginDate"]).Date ||
                        TimeSpan.Parse(info.Rows[idx]["BEGINTIME"].ToString()) != Convert.ToDateTime(oAdditional.Rows[idx]["BeginDate"]).TimeOfDay ||
                        TimeSpan.Parse(info.Rows[idx]["ENDTIME"].ToString()) != Convert.ToDateTime(oAdditional.Rows[idx]["EndDate"]).TimeOfDay ||
                        info.Rows[idx]["DESCRIPTION"].ToString() != oAdditional.Rows[idx]["DESCRIPTION"].ToString())
                    {
                        DateTime infOTDate = Convert.ToDateTime(info.Rows[idx]["OTDATE"]);
                        TimeSpan infBEGINTIME = TimeSpan.Parse(info.Rows[idx]["BEGINTIME"].ToString());
                        TimeSpan infENDTIME = TimeSpan.Parse(info.Rows[idx]["ENDTIME"].ToString());
                        oAdditional.Rows[idx]["BeginDate"] = infOTDate.Add(infBEGINTIME);
                        if (infENDTIME >= infBEGINTIME)
                        {
                            oAdditional.Rows[idx]["EndDate"] = infOTDate.Add(infENDTIME);
                        }
                        else
                        {
                            oAdditional.Rows[idx]["EndDate"] = infOTDate.AddDays(1).Add(infENDTIME);
                        }
                        oAdditional.Rows[idx]["DESCRIPTION"] = info.Rows[idx]["DESCRIPTION"].ToString();
                    }
                }
            }


            bool IsDelete = true;
            if (State.ToUpper() == "DRAFT")
                foreach (DataRow row in oAdditional.Rows)
                {
                    row.BeginEdit();
                    row["RequestNo"] = RequestNo;
                    row["Status"] = (int)DailyOTLogStatus.DRAFT;
                    row.EndEdit();
                }
            else if (State.ToUpper().Contains("WAITFORAPPROVE"))
            {
                foreach (DataRow row in oAdditional.Rows)
                {
                    row.BeginEdit();
                    row["RequestNo"] = RequestNo;
                    row["Status"] = (int)DailyOTLogStatus.WAITFORAPPROVE;
                    row.EndEdit();
                }
            }
            else if (State.ToUpper() == "COMPLETED")
            {
                foreach (DataRow row in oAdditional.Rows)
                //if ((DailyOTLogStatus)Convert.ToInt16(row["Status"]) == DailyOTLogStatus.WAITFORAPPROVE)
                {
                    row.BeginEdit();
                    row["RequestNo"] = RequestNo;
                    row["Status"] = (int)DailyOTLogStatus.WAITFORMAKESUMMARY;
                    row.EndEdit();
                }
                //HR.TM.ServiceManager.HRTMBuffer.DeleteOTLogByRefRequestNo(oAdditional);
                HRTMManagement.CreateInstance(Requestor.CompanyCode).DeleteOTLogByRefRequestNo(oAdditional);
            }
            else if (State.ToUpper() == "CANCELLED")
            {
                foreach (DataRow row in oAdditional.Rows)
                {
                    row.BeginEdit();
                    row["RequestNo"] = RequestNo;
                    row["Status"] = (int)DailyOTLogStatus.CANCELLED;
                    row.EndEdit();
                    //              if ((DailyOTLogStatus)Convert.ToInt16(row["Status"]) == DailyOTLogStatus.DRAFT)
                    //                  IsDelete = false;
                    //              else
                    //              {
                    //                  row.BeginEdit();
                    //row["RequestNo"] = RequestNo;
                    //                  row["Status"] = (int)DailyOTLogStatus.CANCELLED;
                    //                  row.EndEdit();
                    //              }
                }

            }
            else if (State.ToUpper() == "WAITFOREDIT")
            {
                foreach (DataRow row in oAdditional.Rows)
                {
                    row.BeginEdit();
                    row["RequestNo"] = RequestNo;
                    row["Status"] = (int)DailyOTLogStatus.WAITFOREDIT;
                    row.EndEdit();
                }
            }

            try
            {
                HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveDailyOTLogService(Data.Tables["DailyOTLog"]);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveOTLog(Data.Tables["DailyOTLog"], IsDelete);
        }

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info)
        {
            /* ====== Add by Han 
                15/03/2019 : เช็คเพิ่มเติม ถ้าขอ OT เกิน 36 ชม.ต่อสัปดาห์ ให้สร้าง Flow ไปถึงระดับ MD
                10/05/2019 : แก้ไขจาก ให้สร้าง Flow ไปถึงระดับ MD เปลี่ยนเป็น ส่งไปถึงระดับสูงสุดในสายงาน (ฝ่าย/รอง กจญ.)
                           : เช็คเพิ่มเติม ถ้าพนักงานขอ OT เกิน 120 ชม.ต่อเดือน ให้สร้าง Flow ไปถึงระดับ MD 
            */

            decimal dMaxLevelMD = HRTMManagement.CreateInstance(Requestor.CompanyCode).GetMaxLevelMD();
            if (dMaxLevelMD != -1 && Requestor.EmpSubGroup == dMaxLevelMD)
            {
                return "MD_CREATE";
            }

            DateTime OTDateTime = Convert.ToDateTime(Convert.ToDateTime(Info.Rows[0]["OTDate"]).ToString("yyyy-MM-dd") + " " + Info.Rows[0]["BEGINTIME"]);
            DataTable dtOLPM = HRTMManagement.CreateInstance(Requestor.CompanyCode).GetOTOverLimitPerMonth(Requestor.EmployeeID, OTDateTime);
            if (dtOLPM.Rows.Count > 0 && Convert.ToString(dtOLPM.Rows[0]["IsSendToMD"]) == "1")
            {
                return "OLPM_2MD";
            }

            DataTable dtOT = HRTMManagement.CreateInstance(Requestor.CompanyCode).GetOTOverLimitPerWeek(Requestor.EmployeeID, OTDateTime);
            if (dtOT.Rows.Count > 0 && Convert.ToString(dtOT.Rows[0]["IsSendToMD"]) == "1")
            {
                return "2MD";
            }

            DataTable dtOTHoliday = HRTMManagement.CreateInstance(Requestor.CompanyCode).GetOTOnHoliday(Requestor.EmployeeID, OTDateTime);
            if (dtOTHoliday.Rows.Count > 0)
            {
                return "2MD";
            }

            /* ====================================================================================== */

            if (Info.Rows.Count > 0 && !string.IsNullOrEmpty(Info.Rows[0]["REPORTTO"].ToString()))
            {
                return "CM";
            }
            else
                return "M";
        }
    }
}
