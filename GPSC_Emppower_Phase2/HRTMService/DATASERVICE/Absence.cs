using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using ESS.DATA;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.HR.TM.INFOTYPE;
using ESS.HR.TM.CONFIG;
using ESS.HR.TM.DATACLASS;
using ESS.TIMESHEET;
using ESS.WORKFLOW;
using ESS.UTILITY.EXTENSION;
using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using Newtonsoft.Json;

namespace ESS.HR.TM.DATASERVICE
{
    public class Absence : AbstractDataService
    {
        public Absence()
        {
        }

        public string CompanyCode { get; set; }


        #region " GenerateAdditionalData "
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            DataSet DS = new DataSet("ADDITIONAL");
            DataTable oTable;
            INFOTYPE2001 oItem = new INFOTYPE2001();
            oItem.EmployeeID = Requestor.EmployeeID;
            oItem.BeginDate = DateTime.Now;
            oItem.EndDate = DateTime.Now;
            oTable = oItem.ToADODataTable();
            oTable.TableName = "INFOTYPE2001";
            DS.Tables.Add(oTable);

            DelegateData oDelegate = new DelegateData();
            oDelegate.EmployeeID = Requestor.EmployeeID;
            oDelegate.BeginDate = DateTime.Now;
            oDelegate.EndDate = DateTime.Now;
            oTable = oDelegate.ToADODataTable();
            oTable.TableName = "DELEGATEDATA";
            DS.Tables.Add(oTable);

            //INFOTYPE2003 oItem2003 = new INFOTYPE2003();
            //oItem2003.EmployeeID = Requestor.EmployeeID;
            //oItem2003.BeginDate = DateTime.Now;
            //oItem2003.EndDate = DateTime.Now;
            //oTable = oItem2003.ToADODataTable();
            //oTable.TableName = "INFOTYPE2003";
            //DS.Tables.Add(oTable);

            oTable = new DataTable("ATTACHMENTDATA");
            DataColumn oDC = new DataColumn("HasAttachment", typeof(bool));
            oDC.DefaultValue = false;
            oTable.Columns.Add(oDC);
            oTable.Rows.Add(oTable.NewRow());
            DS.Tables.Add(oTable);

            return DS;
        }
        #endregion

      
        #region " SaveExternalData "
        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object ds, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            DataTable oAdditional = Data.Tables["INFOTYPE2001"];
            DataTable oDelegate = Data.Tables["DELEGATEDATA"];
            INFOTYPE2001 oAbsence = new INFOTYPE2001();

            oAbsence.ParseToObject(oAdditional.Rows[0]);
            oAbsence.BeginDate = Convert.ToDateTime(oAdditional.Rows[0]["BeginDate"]);
            oAbsence.EndDate = Convert.ToDateTime(oAdditional.Rows[0]["EndDate"]);

            oAbsence.AbsenceDays = Convert.ToDecimal(oAdditional.Rows[0]["AbsenceDays"]);
            oAbsence.AbsenceHours = Convert.ToDecimal(oAdditional.Rows[0]["AbsenceHours"]);

            if(oAbsence.AllDayFlag)
            {
                TimeSpan duration = oAbsence.EndDate - oAbsence.BeginDate;
                oAbsence.PayrollDays = duration.Days + 1;
            }
            else
            {
                oAbsence.PayrollDays = 0.5M;
            }
            //oAbsence.PayrollDays = Convert.ToDecimal(oAdditional.Rows[0]["PayrollDays"]);


            List<string> unmarkList = new List<string>();
            unmarkList.Add("COMPLETED");
            unmarkList.Add("CANCELLED");

            List<string> postList = new List<string>();
            postList.Add("COMPLETED");

            List<INFOTYPE2001> postingList = new List<INFOTYPE2001>();
            if (postList.Contains(State))
            {
                #region " POST "
                // POST DATA
                try
                {
                    if (Data.Tables.Contains("POSTDATA"))
                    {
                        Data.Tables.Remove("POSTDATA");
                    }
                    AbsenceCreatingRule oCreatingRule = HRTMManagement.CreateInstance(Requestor.CompanyCode).GetCreatingRule(Requestor, oAbsence.AbsenceType, oAbsence.PayrollDays);
                    try
                    {                       
                        INFOTYPE2001 INF2001 = new INFOTYPE2001();
                        postingList = INF2001.SplitForPost(INF2001.GenerateAssignmentList(oAbsence, oCreatingRule, RequestNo, false, Requestor.CompanyCode));
                    }
                    catch (Exception ex1)
                    {
                        DataTable oTempTable;
                        oAbsence.Remark = ex1.Message;
                        oTempTable = oAbsence.ToADODataTable();
                        oTempTable.TableName = "POSTDATA";
                        Data.Tables.Add(oTempTable);
                        throw ex1;
                    }
                    try
                    {
                        //-Set RequestNo
                        if (oDelegate.Rows.Count > 0 && !String.IsNullOrEmpty(oDelegate.Rows[0]["DelegateTo"].ToString()))
                        {
                            oDelegate.Rows[0]["RequestNo"] = RequestNo;


                            HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveDelegateByAbsence(oDelegate);
                        }
                        
                        HRTMManagement.CreateInstance(Requestor.CompanyCode).SavePostAbsence(postingList, Requestor.CompanyCode);

                        List<ClockinLateClockoutEarly> ClockinLateClockoutEarlyList = HRTMManagement.CreateInstance(Requestor.CompanyCode).CalculateTM_InLateOutEarly(oAbsence.EmployeeID, oAbsence.BeginDate, oAbsence.EndDate);
                        if (ClockinLateClockoutEarlyList.Count > 0)
                        {
                            HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveInLateOutEarly(ClockinLateClockoutEarlyList, oAbsence.EmployeeID, oAbsence.BeginDate, oAbsence.EndDate.AddDays(1).AddSeconds(-1));
                            //HRTMManagement.MappingTimePairInLateOutEarlyID(oAbsence.EmployeeID, oAbsence.BeginDate, oAbsence.EndDate.AddDays(1).AddSeconds(-1));
                        }

                    }
                    catch (Exception ex1)
                    {
                        INFOTYPE2001 item1 = new INFOTYPE2001();
                        DataTable oTempTable;
                        oTempTable = item1.ToADODataTable(true);
                        oTempTable.TableName = "POSTDATA";
                        Data.Tables.Add(oTempTable);
                        foreach (INFOTYPE2001 item in postingList)
                        {
                            DataRow oNewRow = oTempTable.NewRow();
                            item.LoadDataToTableRow(oNewRow);
                            oTempTable.LoadDataRow(oNewRow.ItemArray, false);
                        }
                        throw ex1;
                    }
                }
                catch (Exception ex)
                {
                    //throw new Exception(ex.Message);
                    throw new SaveExternalDataException("SaveExternalData Error", true, ex);
                }
                #endregion
            }

            List<INFOTYPE2001> tempList = new List<INFOTYPE2001>();
            oAbsence.IsMark = !unmarkList.Contains(State.ToUpper());
            oAbsence.RequestNo = RequestNo;


            AbsAttCalculateResult oResult1;
            if (oAbsence.AllDayFlag)
            {
                oResult1 = HRTMManagement.CreateInstance(Requestor.CompanyCode).CalculateAbsAttEntry(oAbsence.EmployeeID, oAbsence.AbsenceType, oAbsence.BeginDate, oAbsence.EndDate, TimeSpan.MinValue, TimeSpan.MinValue, true, false, false, Requestor.CompanyCode);
            }
            else
            {
                oResult1 = HRTMManagement.CreateInstance(Requestor.CompanyCode).CalculateAbsAttEntry(oAbsence.EmployeeID, oAbsence.AbsenceType, oAbsence.BeginDate, oAbsence.EndDate, oAbsence.BeginTime, oAbsence.EndTime, false, false, false, Requestor.CompanyCode);
            }
            oAbsence.AbsenceDays = oResult1.AbsenceDays;
            oAbsence.AbsenceHours = oResult1.AbsenceHours;
            oAbsence.PayrollDays = oResult1.PayrollDays;

            tempList.Add(oAbsence);
            try
            {
                HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveAbsence(tempList);
            }
            catch (Exception e)
            {

                string Err = HRTMManagement.CreateInstance(Requestor.CompanyCode).ShowError("HRTM_EXCEPTION", "DB_SAVE_ERROR", WorkflowPrinciple.Current.UserSetting.Language);
                throw new Exception(Err);

            }



            try
            {
                if (postList.Contains(State))
                {
                    HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveLG_TIMEREQUEST(tempList, "ABSENCE");
                }
            }
            catch (Exception er)
            {

            }

        }
        #endregion

       

        #region " CalculateInfoData "
        public override void CalculateInfoData(EmployeeData Requestor, object ds, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            DataRow dr;
            DataRow dataRow = Data.Tables["INFOTYPE2001"].Rows[0];
            INFOTYPE2001 oAbsence = new INFOTYPE2001();
            oAbsence.ParseToObject(dataRow);

            oAbsence.AbsenceDays = Convert.ToDecimal(dataRow["AbsenceDays"]);
            oAbsence.AbsenceHours = Convert.ToDecimal(dataRow["AbsenceHours"]);
            oAbsence.PayrollDays = Convert.ToDecimal(dataRow["PayrollDays"]);

            if (Info.Rows.Count == 0)
            {
                dr = Info.NewRow();
            }
            else
            {
                dr = Info.Rows[0];
            }

            dr["ABSENCETYPECODE"] = string.Format("{0}#{1}", Requestor.OrgAssignment.SubAreaSetting.AbsAttGrouping, oAbsence.AbsenceType);
            dr["ABSENCETYPE"] = dataRow["ABSENCETYPE"];
            dr["PAYROLLDAYS"] = dataRow["PAYROLLDAYS"];
            dr["BEGINDATE"] = dataRow["BEGINDATE"];
            dr["ENDDATE"] = dataRow["ENDDATE"];
            dr["FULLDAY"] = dataRow["ALLDAYFLAG"];
            dr["ISREQUIREDOCUMENT"] = dataRow["ISREQUIREDOCUMENT"];

            if (Data.Tables["DELEGATEDATA"].Rows.Count == 0)
            {
                dr["ISDELEGATE"] = false;
                dr["DELEGATETO"] = "";
            }
            else
            {
                dataRow = Data.Tables["DELEGATEDATA"].Rows[0];
                DelegateData oDelegate = new DelegateData();
                oDelegate.ParseToObject(dataRow);

                string delegateTo = dataRow.IsNull("DELEGATETO") ? "" : (string)dataRow["DELEGATETO"];
                if (new EmployeeData().ValidateEmployeeID(delegateTo))
                {
                    dr["ISDELEGATE"] = true;
                    dr["DELEGATETO"] = delegateTo;
                }
                else
                {
                    dr["ISDELEGATE"] = false;
                    dr["DELEGATETO"] = "";
                }
            }

            if (dr.RowState == DataRowState.Detached)
            {
                Info.Rows.Add(dr);
            }
        }
        #endregion

        #region " ValidateData "
        public override void ValidateData(object ds, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {

            DailyWS oDWS;
            EmployeeData oEmp = null;
            DataSet newData = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            DataTable oAdditional = newData.Tables["INFOTYPE2001"];
            INFOTYPE2001 oAbsence = new INFOTYPE2001();
            oAbsence.ParseToObject(oAdditional.Rows[0]);
            DateTime dtWorkBegin, dtWorkEnd;

            oAbsence.BeginDate = Convert.ToDateTime(oAdditional.Rows[0]["BeginDate"]);
            oAbsence.EndDate = Convert.ToDateTime(oAdditional.Rows[0]["EndDate"]);


            if(oAbsence.Remark == string.Empty)
            {
                throw new DataServiceException("HRTM_EXCEPTION", "REASON_CAN_NOT_BE_NULL"); 
            }

            try
            {
                #region " CheckCollision "
                if (newData.Tables.Contains("POSTDATA"))
                {
                    newData.Tables.Remove("POSTDATA");
                }
                if (oAbsence.AllDayFlag)
                {
                    //TimesheetManagement.MarkTimesheet(Requestor.EmployeeID, "LEAVE", true, oAbsence.BeginDate, oAbsence.EndDate, true, true, RequestNo);
                    HRTMManagement.CreateInstance(Requestor.CompanyCode).MarkTimesheet(Requestor.EmployeeID, "LEAVE", true, oAbsence.BeginDate, oAbsence.EndDate, true, true, RequestNo);
                }
                else
                {
                    HRTMManagement.CreateInstance(Requestor.CompanyCode).MarkTimesheet(Requestor.EmployeeID, "LEAVE", false, oAbsence.BeginDate.Add(oAbsence.BeginTime), oAbsence.EndDate.Add(oAbsence.EndTime), true, true, RequestNo);
                    //TimesheetManagement.MarkTimesheet(Requestor.EmployeeID, "LEAVE", false, oAbsence.BeginDate.Add(oAbsence.BeginTime), oAbsence.EndDate.Add(oAbsence.EndTime), true, true, RequestNo);
                }

                List<DutyPaymentLog> lstDuty = HRTMManagement.CreateInstance(Requestor.CompanyCode).GetDutyPaymentLog(Requestor.EmployeeID, oAbsence.BeginDate, oAbsence.EndDate);
                if (lstDuty.Count > 0)
                {
                    if (oAbsence.AllDayFlag)
                    {
                        for (DateTime dt = oAbsence.BeginDate; dt <= oAbsence.EndDate; dt = dt.AddDays(1))
                        {
                            if (oEmp == null || !(oEmp.OrgAssignment.BeginDate <= dt && oEmp.OrgAssignment.EndDate >= dt))
                                oEmp = new EmployeeData(Requestor.EmployeeID, dt);

                            oDWS = oEmp.GetDailyWorkSchedule(dt, true);
                            if (!oDWS.IsDayOffOrHoliday)
                            {
                                dtWorkBegin = dt.Date.Add(oDWS.WorkBeginTime);
                                dtWorkEnd = dt.Date.Add(oDWS.WorkEndTime);
                                if (dtWorkBegin >= dtWorkEnd)
                                    dtWorkEnd = dtWorkEnd.AddDays(1);
                                if (lstDuty.FindAll(delegate (DutyPaymentLog log) { return dtWorkBegin < log.EndDate && dtWorkEnd > log.BeginDate; }).Count > 0)
                                {
                                    lstDuty.Clear();
                                    throw new DataServiceException("HRTM_EXCEPTION", String.Format("OVERLAP_DUTYPAYMENT_REQUEST|{0}", lstDuty[0].DutyDate));
                                }
                            }
                            oDWS = null;
                        }
                    }
                    else
                    {
                        oAbsence.BeginDate = oAbsence.BeginDate.Add(oAbsence.BeginTime);
                        oAbsence.EndDate = oAbsence.EndDate.Add(oAbsence.EndTime);
                        if (oAbsence.BeginDate >= oAbsence.EndDate)
                            oAbsence.EndDate = oAbsence.EndDate.AddDays(1);

                        if (lstDuty.FindAll(delegate (DutyPaymentLog log) { return oAbsence.BeginDate < log.EndDate && oAbsence.EndDate > log.BeginDate; }).Count > 0)
                        {
                            lstDuty.Clear();
                            throw new DataServiceException("HRTM_EXCEPTION", String.Format("OVERLAP_DUTYPAYMENT_REQUEST|{0}", lstDuty[0].DutyDate));
                        }
                    }

                }
                #endregion

                oAbsence.RequestNo = RequestNo;

                //bool IsPassValidateHoliday = false;
                //IsPassValidateHoliday = HRTMManagement.CreateInstance(Requestor.CompanyCode).ValidateAbsenceHoliday(Requestor, oAbsence.AbsenceType, oAbsence.BeginDate, oAbsence.EndDate);
                //if (!IsPassValidateHoliday)
                //{
                //    throw new Exception("NON_WORKING_PERIOD");
                //}

                oAbsence.ValidateData(Requestor.CheckDate, false,Requestor);

                if (oAbsence.IsRequireDocument)
                {
                    if (NoOfFileAttached < 1)
                    {
                        throw new RequiredDocumentException();
                    }

                    //DataTable oTable = newData.Tables["ATTACHMENTDATA"];
                    //if (!(bool)oTable.Rows[0]["HasAttachment"])
                    //{
                    //    throw new RequiredDocumentException();
                    //}
                }
                newData.Tables.Remove(oAdditional);
                
                
                oAdditional = oAbsence.ToADODataTable();
                oAdditional.TableName = "INFOTYPE2001";
                newData.Tables.Add(oAdditional);
            }
            //catch (ValidatingException e)
            //{
            //    throw new DataServiceException("HRTM_EXCEPTION", "VALIDATING_ERROR", "", e);
            //}
            catch (AbsenceCreatingException e)
            {
                //throw new DataServiceException("HRTM_EXCEPTION", "CONFLICT_RULE", "", e);

                bool setAbsenceDays = false;
                object[] arguments = new object[10];
                AbsenceType oAT = HRTMManagement.CreateInstance(Requestor.CompanyCode).GetAbsenceType(Requestor.EmployeeID, e.AbsenceType, WorkflowPrinciple.Current.UserSetting.Language);
                arguments[0] = oAT.Description;
                arguments[1] = Math.Abs(e.OffsetValue);
                if (e.AdvanceInform > -1 && e.LateInform > -1)
                {
                    arguments[1] = e.AdvanceInform;
                    arguments[2] = e.LateInform;
                }
                if (e.MinAbsenceDays == 0 && e.MaxAbsenceDays < 999)
                {
                    arguments[3] = Convert.ToInt32(e.MaxAbsenceDays);
                    setAbsenceDays = true;

                    //if (e.OffsetValue > 0 && e.OffsetFlag == "D")
                    //    throw new DataServiceException("HRTM_EXCEPTION", "CONFLICT_RULE,CONFLICT_RULE_8", arguments);

                    if (e.OffsetValue > 0 && e.OffsetFlag == "D" && (e.MinAbsenceDays != 0 || e.MaxAbsenceDays != 0))
                        throw new DataServiceException("HRTM_EXCEPTION", "CONFLICT_RULE,CONFLICT_RULE_8", arguments);
                    else if(e.OffsetValue > 0 && e.OffsetFlag == "D" && (e.MinAbsenceDays == 0 && e.MaxAbsenceDays == 0))
                        throw new DataServiceException("HRTM_EXCEPTION", "CONFLICT_RULE,CONFLICT_RULE_11", arguments);
                }
                else if (e.MinAbsenceDays > 0 && e.MaxAbsenceDays == 999)
                {
                    arguments[3] = Convert.ToInt32(e.MinAbsenceDays);
                    setAbsenceDays = true;

                    if (e.OffsetValue > 0 && e.OffsetFlag == "D")
                        throw new DataServiceException("HRTM_EXCEPTION", "CONFLICT_RULE,CONFLICT_RULE_9", arguments);
                }
                else if (e.MinAbsenceDays > 0 && e.MaxAbsenceDays < 999)
                {
                    arguments[3] = Convert.ToInt32(e.MinAbsenceDays);
                    arguments[4] = Convert.ToInt32(e.MaxAbsenceDays);
                    setAbsenceDays = true;

                    if (e.OffsetValue > 0 && e.OffsetFlag == "D")
                        throw new DataServiceException("HRTM_EXCEPTION", "CONFLICT_RULE,CONFLICT_RULE_10", arguments);
                }


                if (!setAbsenceDays)
                {
                    if (e.UserRole.Equals("CREATE_FUTURE") || e.UserRole.Equals("CREATE_PAST"))
                        throw new DataServiceException("HRTM_EXCEPTION", e.UserRole, "");
                    else if (e.OffsetValue == 999 && e.OffsetFlag == "D")
                        throw new DataServiceException("HRTM_EXCEPTION", "CONFLICT_RULE,CONFLICT_RULE_7", arguments);
                    else if (e.OffsetValue == 999 && e.OffsetFlag == "M")
                        throw new DataServiceException("HRTM_EXCEPTION", "CONFLICT_RULE,CONFLICT_RULE_7", arguments);
                    else if (e.OffsetValue == -1 && e.CountOnDayOff && e.OffsetFlag == "D")
                        throw new DataServiceException("HRTM_EXCEPTION", "CONFLICT_RULE,CONFLICT_RULE_1", arguments);
                    else if (e.OffsetValue < 0 && e.OffsetFlag == "D")
                        throw new DataServiceException("HRTM_EXCEPTION", "CONFLICT_RULE,CONFLICT_RULE_2", arguments);
                    else if (e.OffsetValue > 0 && e.OffsetFlag == "D")
                        throw new DataServiceException("HRTM_EXCEPTION", "CONFLICT_RULE,CONFLICT_RULE_3", arguments);
                    else if (e.OffsetValue == 0 && e.OffsetFlag == "D")
                        throw new DataServiceException("HRTM_EXCEPTION", "CONFLICT_RULE,CONFLICT_RULE_4", arguments);
                    else if (e.OffsetValue < 0 && e.OffsetFlag == "M")
                        throw new DataServiceException("HRTM_EXCEPTION", "CONFLICT_RULE,CONFLICT_RULE_5", arguments);
                    else if (e.OffsetValue > 0 && e.OffsetFlag == "M")
                        throw new DataServiceException("HRTM_EXCEPTION", "CONFLICT_RULE,CONFLICT_RULE_6", arguments);
                    else
                        throw new DataServiceException("HRTM_EXCEPTION", "CONFLICT_RULE", arguments);
                    //{
                    //    lblError.Text = string.Format("{0}{1}", CacheManager.GetCommonText("HRTM_EXCEPTION", this.LanguageCode, "CONFLICT_RULE"), e.Message == "" ? "" : " : " + e.Message);
                    //    cellError.Attributes["title"] = e.ToString();
                    //}
                }


            }
            catch (TimesheetCollisionException e)
            {
                throw new DataServiceException("HRTM_EXCEPTION", "COLLISION_OCCUR", "", e);
            }
            catch (RequiredDocumentException e)
            {
                throw new DataServiceException("HRTM_EXCEPTION", "REQUIREDOCUMENT", "", e);
            }
            catch (Exception e)
            {
                //string Err = HRTMManagement.CreateInstance(Requestor.CompanyCode).DataServiceExceptionProcessError(Requestor, "TH", e);
                //throw new Exception(Err);
                string Err = HRTMManagement.CreateInstance(Requestor.CompanyCode).ShowError("HRTM_EXCEPTION", e.Message.ToString(), WorkflowPrinciple.Current.UserSetting.Language);
                throw new Exception(Err);
                //throw new DataServiceException("HRTM_EXCEPTION", e.Message, "");
            }

        }
        #endregion

        #region " PrepareData "
        public override void PrepareData(EmployeeData Requestor, object ds)
        {
            DataTable oTable;
            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            if (!Data.Tables.Contains("INFOTYPE2001"))
            {
                INFOTYPE2001 oItem = new INFOTYPE2001();
                oTable = oItem.ToADODataTable();
                oTable.TableName = "INFOTYPE2001";
                Data.Tables.Add(oTable);
            }
            if (!Data.Tables.Contains("DELEGATEDATA"))
            {
                DelegateData oDelegate = new DelegateData();
                oTable = oDelegate.ToADODataTable();
                oTable.TableName = "DELEGATEDATA";
                Data.Tables.Add(oTable);
            }
            if (!Data.Tables.Contains("ATTACHMENTDATA"))
            {
                oTable = new DataTable("ATTACHMENTDATA");
                DataColumn oDC = new DataColumn("HasAttachment", typeof(bool));
                oDC.DefaultValue = false;
                oTable.Columns.Add(oDC);
                oTable.Rows.Add(oTable.NewRow());
                Data.Tables.Add(oTable);
            }

        }
        #endregion

        #region " GenerateFlowKey "
        public override string GenerateFlowKey(EmployeeData Requestor, DataTable Info)
        {
            string cIsDelegate = "";
            string cIsRequireDocument = "";
            string cIsUsedQuotaAndNotCheckOnCreate = "";
            string cAbsenceType = (string)Info.Rows[0]["ABSENCETYPE"];
            AbsenceType oAbsenceType = HRTMManagement.CreateInstance(Requestor.CompanyCode).GetAbsenceType(Requestor.EmployeeID, cAbsenceType, "");
            //AbsenceCreatingRule oACR = HRTMManagement.CreateInstance(Requestor.CompanyCode).GetCreatingRule(Requestor.EmployeeID, oAbsenceType.Key);

            AbsenceCreatingRule oACR = HRTMManagement.CreateInstance(Requestor.CompanyCode).GetCreatingRule(Requestor, oAbsenceType.Key, 0);

            decimal dMaxLevelMD = HRTMManagement.CreateInstance(Requestor.CompanyCode).GetMaxLevelMD();
            if (dMaxLevelMD != -1 && Requestor.EmpSubGroup == dMaxLevelMD)
            {
                return "MD_CREATE";
            }

            if (oAbsenceType.MonitorType == 1)
            {
                cIsUsedQuotaAndNotCheckOnCreate = "X";
            }
            else if (oAbsenceType.MonitorType == 2)
            {
                cIsUsedQuotaAndNotCheckOnCreate = "Z";
            }
            else if (oAbsenceType.IsUseQuota && !oACR.CheckQuotaWhenCreate)
            {
                    cIsUsedQuotaAndNotCheckOnCreate = "Y";
            }
            else
            {
                    cIsUsedQuotaAndNotCheckOnCreate = "N";
            }
            switch ((bool)Info.Rows[0]["ISDELEGATE"])
            {
                case false:
                    cIsDelegate = "N";
                    break;
                default:
                    cIsDelegate = "Y";
                    break;
            }
            switch ((bool)Info.Rows[0]["ISREQUIREDOCUMENT"])
            {
                case false:
                    cIsRequireDocument = "N";
                    break;
                default:
                    cIsRequireDocument = "Y";
                    break;
            }
            return string.Format("{0}{1}{2}", cIsDelegate, cIsRequireDocument, cIsUsedQuotaAndNotCheckOnCreate);
        }
        #endregion

        #region " GenerateAssignmentList "
        private List<INFOTYPE2001> GenerateAssignmentList(INFOTYPE2001 item)
        {
            List<INFOTYPE2001> oReturn = new List<INFOTYPE2001>();
            if (!item.AllDayFlag)
            {
                item.EndDate = item.BeginDate;
                oReturn.Add(item);
            }
            else
            {
                EmployeeData oEmp = new EmployeeData(item.EmployeeID);
                MonthlyWS oMWS = null;
                DailyWS oDWS = null;
                TimeSpan oTS = item.EndDate.Subtract(item.BeginDate);
                int dateRange = oTS.Days + 1;
                //AbsenceCreatingRule oACR = AbsenceCreatingRule.GetCreatingRule(item.EmployeeID, item.AbsenceType);

                AbsenceCreatingRule oACR = HRTMManagement.CreateInstance(CompanyCode).GetCreatingRule(item.EmployeeID, item.AbsenceType);
                int countDayoff = 0;
                //foreach (AbsenceAssignment assign in AbsenceAssignment.GetAbsenceAssignment(oEmp.OrgAssignment.SubAreaSetting.AbsAttGrouping, item.AbsenceType))
                foreach (AbsenceAssignment assign in HRTMManagement.CreateInstance(CompanyCode).GetAbsenceAssignment(oEmp.OrgAssignment.SubAreaSetting.AbsAttGrouping, item.AbsenceType))
                {
                    if (assign.Value1 <= dateRange)
                    {
                        int valueMin;
                        if (assign.Value2 >= dateRange)
                        {
                            valueMin = dateRange;
                        }
                        else
                        {
                            valueMin = assign.Value2;
                        }
                        for (int index = assign.Value1; index <= valueMin; index++)
                        {
                            DateTime rundate = item.BeginDate.Date.AddDays(index + countDayoff - 1);
                            if (rundate > item.EndDate)
                                break;
                            if (oACR.TruncateDayOff)
                            {
                                if (oMWS == null || oMWS.WS_Month != rundate.Month || oMWS.WS_Year != rundate.Year)
                                {
                                    oMWS = MonthlyWS.GetCalendar(item.EmployeeID, rundate.Year, rundate.Month);
                                }
                                oDWS = EmployeeManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetDailyWorkschedule(oEmp.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, oMWS.GetWSCode(rundate.Day, true), rundate);
                                if (oDWS.IsDayOff)
                                {
                                    countDayoff++;
                                    index--;
                                    continue;
                                }
                            }
                            INFOTYPE2001 data = new INFOTYPE2001();
                            data.AbsenceType = assign.ItemAssign;
                            data.AllDayFlag = true;
                            data.BeginDate = rundate;
                            data.BeginTime = TimeSpan.MinValue;
                            data.EmployeeID = item.EmployeeID;
                            data.EndDate = rundate;
                            data.EndTime = TimeSpan.MinValue;
                            data.IsMark = true;
                            data.IsPost = false;
                            data.IsSkip = false;
                            oReturn.Add(data);
                        }
                    }
                }
            }
            return oReturn;
        }
        #endregion

        #region " GenEmailBody "
        public override string GenEmailBody(EmployeeData Requestor,DataTable Info, string State, string Language)
        {
            DataRow dr = Info.Rows[0];
            string cEmpID = Requestor.EmployeeID;
            string cAbsenceType = (string)dr["ABSENCETYPE"];
            DateTime dBeginDate = (DateTime)dr["BeginDate"];
            DateTime dEndDate = (DateTime)dr["EndDate"];
            AbsenceType oAT = HRTMManagement.CreateInstance(Requestor.CompanyCode).GetAbsenceType(cEmpID, cAbsenceType, Language);
            System.Globalization.CultureInfo oCL = new System.Globalization.CultureInfo("en-US");
            if (dBeginDate == dEndDate)
            {
                return string.Format("{0} {1}", oAT.Description, dBeginDate.ToString("dd/MM/yyyy", oCL));
            }
            else
            {
                return string.Format("{0} {1}-{2}", oAT.Description, dBeginDate.ToString("dd/MM/yyyy", oCL), dEndDate.ToString("dd/MM/yyyy", oCL));
            }
        }
        #endregion
    }
}
