using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.HR.TM.DATASERVICE
{
    public class SummaryOTServiceV100 : SummaryOTService
    {
        public SummaryOTServiceV100()
        { 
        }

        public override string GenerateFlowKey(ESS.EMPLOYEE.EmployeeData Requestor, System.Data.DataTable Info)
        {
            return Requestor.OrgAssignment.CompanyCode.Trim();
        }
    }
}
