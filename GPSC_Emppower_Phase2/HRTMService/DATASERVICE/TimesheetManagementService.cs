﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Data;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.HR.TM.INFOTYPE;
using ESS.HR.TM.CONFIG;
using ESS.HR.TM.DATACLASS;
using ESS.WORKFLOW;
using ESS.UTILITY.EXTENSION;
using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using Newtonsoft.Json;
using ESS.TIMESHEET;
using System.Linq;

namespace ESS.HR.TM.DATASERVICE
{
    public class TimesheetManagementService : AbstractDataService
    {
        CultureInfo oCL = new CultureInfo("en-US");
        string dataCategory = "TIMESHEETBYUSER";
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            TimesheetManagementModel oTimesheetManagementModel = new TimesheetManagementModel();
            if (!string.IsNullOrEmpty(CreateParam))
            {
                HRTMManagement.ClockinLateClockoutEarlyMatching oClockinLateClockoutEarlyMatching = HRTMManagement.CreateInstance(Requestor.CompanyCode).GetClockinLateClockoutEarlyMatching(Requestor.EmployeeID, CreateParam);
                oTimesheetManagementModel.TimePair = oClockinLateClockoutEarlyMatching.TimePair;
                oTimesheetManagementModel.TimeElement = oClockinLateClockoutEarlyMatching.TimeElement;

                DateTime date_now = DateTime.Now;

                // Add Logic หา Remark Sprint#3  จับคู่เวลา
                if(oTimesheetManagementModel.TimePair.Count > 0)
                {
                    DateTime BeginDate = DateTime.ParseExact(CreateParam, "yyyyMM", oCL);
                    DateTime EndDate = BeginDate.AddMonths(1).AddMinutes(-1);

                    INFOTYPE2001 oINFOTYPE2001 = new INFOTYPE2001();
                    INFOTYPE2002 oINFOTYPE2002 = new INFOTYPE2002();
                    List<INFOTYPE2001> oAbsenceList = oINFOTYPE2001.GetData(Requestor, BeginDate.AddDays(-1), EndDate.AddDays(2));
                    List<INFOTYPE2002> oAttendanceList = oINFOTYPE2002.GetData(Requestor, BeginDate.AddDays(-1), EndDate.AddDays(2));

                    Dictionary<DateTime, string> DictAttendanceType0192 = new Dictionary<DateTime, string>();
                    foreach (INFOTYPE2002 item in oAttendanceList)
                        if (item.AttendanceType == "0192")
                            if (!DictAttendanceType0192.ContainsKey(item.BeginDate.Date))
                                DictAttendanceType0192.Add(item.BeginDate.Date, "0");

                    foreach (var item in oTimesheetManagementModel.TimePair)
                    {
                        var absence = oAbsenceList.Where(a => a.BeginDate.Date == item.Date).OrderBy(s => s.BeginTime).ToList();
                        // ไม่มีการลา 
                        if (absence.Count == 0)
                        {
                            // ตรวจสอบการรับรองเวลา
                            var list_attendance = oAttendanceList.Where(a => a.BeginDate.Date <= item.Date && a.EndDate.Date >= item.Date).ToList();
                            if(list_attendance.Count > 0)
                            {
                                TextRemarkAttendanceOnly(Requestor,item, list_attendance);
                            }
                        }
                        else
                        {
                            TextRemarkAttendanceAndAbsence(Requestor, item, absence, oAttendanceList);
                        }

                        // Text Remark พนักงานที่ไม่ต้อง Scan บัตรเข้าทำงาน
                        EmployeeData _EmployeeData = new EmployeeData(Requestor.EmployeeID, item.Date);
                        INFOTYPE0007 oInfotype0007 = _EmployeeData.GetEmployeeWF(Requestor.EmployeeID, item.Date);
                        if (!string.IsNullOrEmpty(oInfotype0007.WFRule))
                        {
                            // เพิ่ม Logic หากเป็นพนักงานที่ไม่ต้องรูดบัตร
                            if (oInfotype0007.TimeEvaluateClass == "0")
                            {
                                
                                item.IsNoScan = true;

                                // ไม่ใช่วันหยุด หรือ พักร้อน
                                if (!(item.DWSCode.StartsWith("OFF") || item.DWSCode.StartsWith("HOL")))
                                {
                                    if (string.IsNullOrEmpty(item.Remark))
                                    {
                                        if (date_now.Date >= item.Date.Date)
                                        {
                                            item.Remark = HRTMManagement
                                           .CreateInstance(Requestor.CompanyCode)
                                           .GetCommonText("TIMESHEETMANAGETIME", WorkflowPrinciple.Current.UserSetting.Language, "LELVE_11_STAFF_UP");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return oTimesheetManagementModel;

            #region " DataSet "
            //DataSet DS = new DataSet("ADDITIONAL");
            //DataTable oTable, oInfo;
            //DataColumn column;
            //TimePair oTimePairByUser = new TimePair();
            //oTable = oTimePairByUser.ToADODataTable(true);
            //oTable.TableName = "TIMEPAIRBYUSER";
            //DS.Tables.Add(oTable);

            //if (!string.IsNullOrEmpty(CreateParam))
            //{
            //    var temp = HRTMManagement.CreateInstance(Requestor.CompanyCode).GetClockinLateClockoutEarlyMatching(Requestor.EmployeeID, CreateParam);

            //    var timePair_Table = temp.TimePair.ToADOTable();
            //    timePair_Table.TableName = "TIMEPAIR";
            //    DS.Tables.Add(timePair_Table);

            //    var timeElement_Table = temp.TimeElement.ToADOTable();
            //    timeElement_Table.TableName = "TIMEELEMENT";
            //    DS.Tables.Add(timeElement_Table);
            //}
            //else
            //{
            //    TimePair oTimePair = new TimePair();
            //    oTable = oTimePair.ToADODataTable(true);
            //    oTable.TableName = "TIMEPAIR";
            //    DS.Tables.Add(oTable);

            //    TimeElement oTimeElement = new TimeElement();
            //    oTable = oTimeElement.ToADODataTable(true);
            //    oTable.TableName = "TIMEELEMENT";
            //    DS.Tables.Add(oTable);
            //}

            //TimePair oOldTimePair = new TimePair();
            //oTable = oOldTimePair.ToADODataTable(true);
            //oTable.TableName = "OLDTIMEPAIR";
            //DS.Tables.Add(oTable);


            //TimeElement oOldTimeElement = new TimeElement();
            //oTable = oOldTimeElement.ToADODataTable(true);
            //oTable.TableName = "OLDTIMEELEMENT";
            //DS.Tables.Add(oTable);

            //return DS;

            #endregion
        }

        // หา Remark สำหรับรับรองเวลา
        public void TextRemarkAttendanceOnly(EmployeeData Requestor,TimePair item,List<INFOTYPE2002> list_attendance)
        {
            int count_attendance = 0;
            int count_comma_attendance = 0;
            foreach (var data_attendance in list_attendance)
            {
                string concat_remark_attendance = HRTMManagement.CreateInstance(Requestor.CompanyCode).GetAttendanceTypeText(data_attendance.AttendanceType, Requestor.OrgAssignment.SubAreaSetting.AbsAttGrouping);
                if (!data_attendance.AllDayFlag)
                {
                    string stringFlex = HRTMManagement.CreateInstance(Requestor.CompanyCode).ConvertFlexTimeText(item.EmployeeID, item.DWS, data_attendance.BeginDate, data_attendance.BeginTime);
                    if (string.IsNullOrEmpty(stringFlex))
                    {
                        concat_remark_attendance += " " + data_attendance.BeginTime.ToString().Substring(0, 5) + "-" + data_attendance.EndTime.ToString().Substring(0, 5);
                    }
                    else
                    {
                        //item.Remark += " " + stringFlex;
                        concat_remark_attendance += " " + stringFlex;
                    }

                    // Check Clok In - Out จากสาเหตุ
                    if (item.ClockIN != null || item.ClockOUT != null)
                        item.IsCheckRemark = true;

                    // รับรองเวลามากกว่า 1 ครั้งใน 1 วันเช่น รับรองเวลาเช้า - บ่าย
                    if (count_attendance > 0)
                        item.IsCheckRemark = true;

                    count_attendance++;
                }
                else
                {
                    // รับรองเวลาเต็มวัน 
                    item.IsCheckRemark = true;
                    item.Remark += " " + HRTMManagement.CreateInstance(Requestor.CompanyCode).GetCommonText("HRTMABSENCE", WorkflowPrinciple.Current.UserSetting.Language, "FULLDAY");
                }

                // รองรับ comma รับรองเวลาตั้งแต่ 2 รายการ
                if (count_comma_attendance > 0)
                    item.Remark = item.Remark + " , " + concat_remark_attendance;
                else
                    item.Remark = concat_remark_attendance;

                count_comma_attendance++;
            }
        }

        // หา Remark การลา และรับรองเวลา
        public void TextRemarkAttendanceAndAbsence(EmployeeData Requestor, TimePair item, List<INFOTYPE2001> absence, List<INFOTYPE2002> oAttendanceList)
        {
            // Absence
            int countMixAbsenceAttendance = 0;
            int ischeck_comma = 0;
            foreach (var item_absence in absence)
            {
                if (item_absence.Status == "RECORDED")
                {
                    string concat_text_absence = HRTMManagement.CreateInstance(Requestor.CompanyCode).GetAbsenceTypeText(item_absence.AbsenceType, Requestor.OrgAssignment.SubAreaSetting.AbsAttGrouping);
                    //item.Remark = GetAbsenceTypeText(item_absence.AbsenceType);
                    if (!item_absence.AllDayFlag)
                    {
                        string stringFlex = HRTMManagement.CreateInstance(Requestor.CompanyCode).ConvertFlexTimeText(item.EmployeeID, item.DWS, item_absence.BeginDate, item_absence.BeginTime);
                        if (string.IsNullOrEmpty(stringFlex))
                        {
                            //item.Remark += " " + item_absence.BeginTime.ToString().Substring(0, 5) + "-" + item_absence.EndTime.ToString().Substring(0, 5);
                            concat_text_absence += " " + item_absence.BeginTime.ToString().Substring(0, 5) + "-" + item_absence.EndTime.ToString().Substring(0, 5);
                        }
                        else
                        {
                            concat_text_absence += " " + stringFlex;
                        }

                        if (item.ClockIN != null || item.ClockOUT != null)
                        {
                            item.IsCheckRemark = true;
                        }
                    }
                    else
                    {
                        // ลาเต็มวัน 
                        item.IsCheckRemark = true;
                        concat_text_absence += " " + HRTMManagement.CreateInstance(Requestor.CompanyCode).GetCommonText("HRTMABSENCE", WorkflowPrinciple.Current.UserSetting.Language, "FULLDAY");
                    }

                    if (ischeck_comma > 0)
                        item.Remark = item.Remark + " , " + concat_text_absence;
                    else
                        item.Remark = concat_text_absence;

                    ischeck_comma++;
                    countMixAbsenceAttendance++;
                }
            }

            // Attendance
            var list_attendance = oAttendanceList.Where(a => a.BeginDate.Date <= item.Date && a.EndDate.Date >= item.Date).ToList();
            if (list_attendance.Count > 0)
            {
                foreach (var data_attendance in list_attendance)
                {
                    if (data_attendance.Status == "RECORDED")
                    {
                        string concat_remark_attendance = HRTMManagement.CreateInstance(Requestor.CompanyCode).GetAttendanceTypeText(data_attendance.AttendanceType, Requestor.OrgAssignment.SubAreaSetting.AbsAttGrouping);
                        if (!data_attendance.AllDayFlag)
                        {
                            string stringFlex = HRTMManagement.CreateInstance(Requestor.CompanyCode).ConvertFlexTimeText(item.EmployeeID, item.DWS, data_attendance.BeginDate, data_attendance.BeginTime);
                            if (string.IsNullOrEmpty(stringFlex))
                            {
                                // รับรองเวลาครึ่งวัน
                                //item.Remark += " " + data_attendance.BeginTime.ToString().Substring(0, 5) + "-" + data_attendance.EndTime.ToString().Substring(0, 5);
                                concat_remark_attendance += " " + data_attendance.BeginTime.ToString().Substring(0, 5) + "-" + data_attendance.EndTime.ToString().Substring(0, 5);
                            }
                            else
                            {
                                //item.Remark += " " + stringFlex;
                                concat_remark_attendance += " " + stringFlex;
                            }

                            // Check Clok In - Out จากสาเหตุ
                            if (item.ClockIN != null || item.ClockOUT != null)
                                item.IsCheckRemark = true;
                        }
                        else
                        {
                            // รับรองเวลาเต็มวัน 
                            item.IsCheckRemark = true;
                            item.Remark += " " + HRTMManagement.CreateInstance(Requestor.CompanyCode).GetCommonText("HRTMABSENCE", WorkflowPrinciple.Current.UserSetting.Language, "FULLDAY");
                        }

                        // รองรับ comma รับรองเวลาตั้งแต่ 2 รายการ
                        if (ischeck_comma > 0)
                            item.Remark = item.Remark + " , " + concat_remark_attendance;
                        else
                            item.Remark = concat_remark_attendance;

                        // มีการลาครึ่งวันแล้ว และมารับรอบรองเวลาต่ออีกครึ่งวัน ให้สถานะ Policy เป็น true
                        if (countMixAbsenceAttendance > 0)
                            item.IsCheckRemark = true;

                        ischeck_comma++;
                    }
                }
            }
        }

        public override void PrepareData(EmployeeData Requestor, object Data)
        {

        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            TimesheetManagementModel oTimesheetManagementModel = JsonConvert.DeserializeObject<TimesheetManagementModel>(newData.ToString());
            foreach (TimePair oTimePair in oTimesheetManagementModel.TimePairByUser)
            {
                if (oTimePair.ClockIN != null && oTimePair.ClockOUT != null)
                {
                    if (oTimePair.ClockIN >= oTimePair.ClockOUT)
                    {
                        string Err = HRTMManagement.CreateInstance(Requestor.CompanyCode).ShowError("TIMESHEETMANAGETIME", "TIMEERROR_MORETHAN_CLOCKOUT", WorkflowPrinciple.Current.UserSetting.Language);
                        throw new Exception(Err);
                        //throw new DataServiceException("TIMESHEETMANAGETIME", "TIMEERROR_MORETHAN_CLOCKOUT");
                    }
                    else if (oTimesheetManagementModel.TimePair.Exists(delegate (TimePair tm) { return oTimePair.Date != tm.Date && oTimePair.ClockIN < tm.ClockOUT && oTimePair.ClockOUT > tm.ClockIN; }))
                    {
                        object[] Args = new object[1];
                        Args[0] = oTimePair.Date.ToString("dd/MM/yyyy", oCL);
                        throw new DataServiceException("TIMESHEETMANAGETIME", "TIMEERROR_OVERLAP", Args);
                    }
                }
            }

            #region " DataSet "
            //DataSet newData = JsonConvert.DeserializeObject<DataSet>(newData.ToString());
            //List<TimePair> TimePairList = new List<TimePair>();
            //foreach (DataRow dr in newData.Tables["TIMEPAIR"].Rows)
            //{
            //    TimePair oTimePair = new TimePair();
            //    oTimePair.ParseToObject(dr);
            //    TimePairList.Add(oTimePair);
            //}

            //List<TimePair> TimePairListByUser = new List<TimePair>();
            //foreach (DataRow dr in newData.Tables["TIMEPAIRBYUSER"].Rows)
            //{
            //    TimePair oTimePair = new TimePair();
            //    oTimePair.ParseToObject(dr);
            //    TimePairListByUser.Add(oTimePair);
            //}
            //foreach (TimePair oTimePair in TimePairListByUser)
            //{
            //    if (oTimePair.ClockIN != null && oTimePair.ClockOUT != null)
            //    {
            //        if (oTimePair.ClockIN >= oTimePair.ClockOUT)
            //            throw new DataServiceException("TIMESHEETMANAGETIME", "TIMEERROR_MORETHAN_CLOCKOUT");
            //        else if (TimePairList.Exists(delegate (TimePair tm) { return oTimePair.Date != tm.Date && oTimePair.ClockIN < tm.ClockOUT && oTimePair.ClockOUT > tm.ClockIN; }))
            //        {
            //            object[] Args = new object[1];
            //            Args[0] = oTimePair.Date.ToString("dd/MM/yyyy", oCL);
            //            throw new DataServiceException("TIMESHEETMANAGETIME", "TIMEERROR_OVERLAP", Args);
            //        }
            //    }
            //}
            #endregion

            if (HRTMManagement.CreateInstance(Requestor.CompanyCode).CheckMark(Requestor.EmployeeID, dataCategory, RequestNo))
            {
                throw new DataServiceException("HRPA", "REQUEST_DUPLICATE", "Request duplicate");
            }

        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object newData, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            TimesheetManagementModel oTimesheetManagementModel = JsonConvert.DeserializeObject<TimesheetManagementModel>(newData.ToString());
            HRTMManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
            if (State == "COMPLETED")
            {
                try
                {
                    HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveTimePairArchiveMappingByUser(oTimesheetManagementModel.TimePairByUser,RequestNo);

                    try
                    {
                        foreach (TimePair oTP in oTimesheetManagementModel.TimePairByUser)
                        {
                            //HRTMManagement.CreateInstance(Requestor.CompanyCode).UpdateTimePair(oTP.EmployeeID, oTP.Date, oTP.Date);
                            List<ClockinLateClockoutEarly> ClockinLateClockoutEarlyList_emp = HRTMManagement.CreateInstance(Requestor.CompanyCode).CalculateTM_InLateOutEarly(oTP.EmployeeID, oTP.Date, oTP.Date);
                            if (ClockinLateClockoutEarlyList_emp.Count > 0)
                            {
                                HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveInLateOutEarly(ClockinLateClockoutEarlyList_emp, oTP.EmployeeID, oTP.Date, oTP.Date.AddDays(1).AddSeconds(-1));
                                HRTMManagement.CreateInstance(Requestor.CompanyCode).MappingTimePairInLateOutEarlyID(oTP.EmployeeID, oTP.Date, oTP.Date);
                            }
                        }
                    }
                    catch (Exception ex) { }
                }
                catch (Exception ex)
                {
                    HRTMManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, true);
                    throw ex;
                }
            }

            #region " DataSet "
            //DataSet Data = JsonConvert.DeserializeObject<DataSet>(newData.ToString());
            //HRTMManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);

            //if (State == "COMPLETED")
            //{
            //    try
            //    {
            //        List<TimePair> TimePairListByUser = new List<TimePair>();
            //        foreach (DataRow dr in Data.Tables["TIMEPAIRBYUSER"].Rows)
            //        {
            //            TimePair oTimePair = new TimePair();
            //            oTimePair.ParseToObject(dr);
            //            TimePairListByUser.Add(oTimePair);
            //        }
            //        HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveTimePairArchiveMappingByUser(TimePairListByUser);

            //        try
            //        {
            //            foreach (TimePair oTP in TimePairListByUser)
            //            {
            //                HRTMManagement.CreateInstance(Requestor.CompanyCode).UpdateTimePair(oTP.EmployeeID, oTP.Date, oTP.Date);
            //                List<ClockinLateClockoutEarly> ClockinLateClockoutEarlyList_emp = HRTMManagement.CreateInstance(Requestor.CompanyCode).CalculateTM_InLateOutEarly(oTP.EmployeeID, oTP.Date, oTP.Date);
            //                if (ClockinLateClockoutEarlyList_emp.Count > 0)
            //                {
            //                    HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveInLateOutEarly(ClockinLateClockoutEarlyList_emp, oTP.EmployeeID, oTP.Date, oTP.Date.AddDays(1).AddSeconds(-1));
            //                    HRTMManagement.CreateInstance(Requestor.CompanyCode).MappingTimePairInLateOutEarlyID(oTP.EmployeeID, oTP.Date, oTP.Date);
            //                }
            //            }
            //        }
            //        catch (Exception ex) { }

            //        if (Data.Tables.Contains("POSTDATA"))
            //        {
            //            Data.Tables.Remove("POSTDATA");
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        HRTMManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, true);
            //        throw ex;
            //    }
            //}
            #endregion

        }
    }
}
