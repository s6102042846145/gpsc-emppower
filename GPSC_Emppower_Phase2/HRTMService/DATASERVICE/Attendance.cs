using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using ESS.DATA;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.HR.TM.CONFIG;
using ESS.HR.TM.INFOTYPE;
using ESS.TIMESHEET;
using ESS.WORKFLOW;
using ESS.HR.TM;
using ESS.DATA.ABSTRACT;
using Newtonsoft.Json;
using ESS.DATA.EXCEPTION;
using ESS.HR.TM.DATACLASS;

namespace ESS.HR.DATASERVICE
{
    public class Attendance : AbstractDataService
    {
        
        public Attendance()
        {
        }

        public static List<INFOTYPE2002> GenerateAssignmentList(INFOTYPE2002 item, EmployeeData Requestor)
        {
            List<INFOTYPE2002> oReturn = new List<INFOTYPE2002>();
            if (!item.AllDayFlag)
            {
                item.IsMark = true;
                item.IsPost = false;
                string empID;
                DateTime calDate;
                TimeSpan time1, time2, saveEndTime, calcTime;
                INFOTYPE2002 data;
                DateTime lastRundate = item.EndDate;
                for (DateTime rundate = item.BeginDate; rundate < item.EndDate; rundate = rundate.AddDays(1))
                {
                    if (rundate == item.BeginDate && item.BeginTime > item.EndTime)
                    {
                        lastRundate = item.BeginDate;
                        continue;
                    }
                    data = new INFOTYPE2002();
                    data.AttendanceType = item.AttendanceType;
                    data.AllDayFlag = false;
                    data.BeginDate = rundate;
                    data.BeginTime = item.BeginTime;
                    data.EmployeeID = item.EmployeeID;
                    data.EndDate = rundate;
                    data.EndTime = item.BeginTime;
                    data.IsMark = true;
                    data.IsPost = false;
                    data.IsPrevDay = false;
                    oReturn.Add(data);
                }

                empID = item.EmployeeID;
                calDate = lastRundate;
                time1 = item.BeginTime;
                time2 = item.EndTime;
                saveEndTime = item.EndTime;
                bool isPrevDay = false;
                do
                {
                    //PlanTimeManagement.SplitCutoffTime(empID, calDate, ref time1, ref time2);
                    //PlanTimeManagement.SplitPrevDay(empID, calDate, ref time1, ref time2, ref isPrevDay);

                    HRTMManagement.CreateInstance("").SplitCutoffTime(empID, calDate, ref time1, ref time2);
                    HRTMManagement.CreateInstance("").SplitPrevDay(empID, calDate, ref time1, ref time2, ref isPrevDay);

                    calcTime = new TimeSpan(time2.Hours, time2.Minutes, time2.Seconds);
                    data = new INFOTYPE2002();
                    data.AttendanceType = item.AttendanceType;
                    data.AllDayFlag = false;
                    data.BeginDate = calDate;
                    data.BeginTime = time1;
                    data.EmployeeID = item.EmployeeID;
                    data.EndDate = calDate;
                    data.EndTime = calcTime;
                    data.IsMark = true;
                    data.IsPost = false;
                    if (item.BeginDate != calDate)
                    {
                        data.IsPrevDay = true;
                    }
                    else
                    {
                        data.IsPrevDay = isPrevDay;
                    }
                    oReturn.Add(data);
                    if (time2.Days > 0)
                    {
                        calDate = calDate.AddDays(1);
                    }
                    time1 = calcTime;
                    time2 = saveEndTime;
                } while (calcTime != saveEndTime);
            }
            else
            {
                EmployeeData oEmp = new EmployeeData(item.EmployeeID);
                TimeSpan oTS = item.EndDate.Subtract(item.BeginDate);
                int dateRange = oTS.Days + 1;
                //foreach (AbsenceAssignment assign in AbsenceAssignment.GetAbsenceAssignment(oEmp.OrgAssignment.SubAreaSetting.AbsAttGrouping, item.AttendanceType))
                foreach (AbsenceAssignment assign in HRTMManagement.CreateInstance(Requestor.CompanyCode).GetAbsenceAssignment(oEmp.OrgAssignment.SubAreaSetting.AbsAttGrouping, item.AttendanceType))
                    {
                        if (assign.Value1 <= dateRange)
                    {
                        int valueMin;
                        if (assign.Value2 >= dateRange)
                        {
                            valueMin = dateRange;
                        }
                        else
                        {
                            valueMin = assign.Value2;
                        }
                        for (int index = assign.Value1; index <= valueMin; index++)
                        {
                            DateTime rundate = item.BeginDate.Date.AddDays(index - 1);
                            INFOTYPE2002 data = new INFOTYPE2002();
                            data.AttendanceType = assign.ItemAssign;
                            data.AllDayFlag = true;
                            data.BeginDate = rundate;
                            data.BeginTime = TimeSpan.MinValue;
                            data.EmployeeID = item.EmployeeID;
                            data.EndDate = rundate;
                            data.EndTime = TimeSpan.MinValue;
                            data.IsMark = true;
                            data.IsPost = false;
                            oReturn.Add(data);
                        }
                    }
                }
            }
            return oReturn;
        }

        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            DataSet DS = new DataSet("ADDITIONAL");
            DataTable oTable;
            INFOTYPE2002 oItem = new INFOTYPE2002();
            oItem.EmployeeID = Requestor.EmployeeID;
            oItem.BeginDate = DateTime.Now.Date;
            oItem.EndDate = DateTime.Now.Date;
            oTable = oItem.ToADODataTable();
            oTable.TableName = "INFOTYPE2002";
            oTable.Clear();
            DS.Tables.Add(oTable);
            return DS;
        }

        public override void PrepareData(EmployeeData Requestor, object ds)
        {
            DataTable oTable;
            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            if (!Data.Tables.Contains("INFOTYPE2002"))
            {
                INFOTYPE2002 oItem = new INFOTYPE2002();
                oTable = oItem.ToADODataTable();
                oTable.TableName = "INFOTYPE2002";
                Data.Tables.Add(oTable);
            }
        }

        public override void ValidateData(object ds, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            DataSet newData = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            DataTable oAdditional = newData.Tables["INFOTYPE2002"];
            //INFOTYPE2002 oAttendance = new INFOTYPE2002();
            //oAttendance.ParseToObject(oAdditional);

            List<INFOTYPE2002> oAttendanceList = new List<INFOTYPE2002>();

            if(oAdditional.Rows.Count==0)
            {
                throw new DataServiceException("HRTM_EXCEPTION", "NO_RECORD");
            }
            foreach (DataRow row in oAdditional.Rows)
            {
                INFOTYPE2002 oAttendance = new INFOTYPE2002();
                oAttendance.ParseToObject(row);
                oAttendanceList.Add(oAttendance);
                
            }
            oAdditional.Clear();


            try
            {
                if (newData.Tables.Contains("POSTDATA"))
                {
                    newData.Tables.Remove("POSTDATA");
                }
                foreach (INFOTYPE2002 oAttendance in oAttendanceList)
                {
                    if ((oAttendance.BeginTime == TimeSpan.Zero && oAttendance.EndTime != TimeSpan.Zero) ||
                        (oAttendance.BeginTime != TimeSpan.Zero && oAttendance.EndTime == TimeSpan.Zero))
                    {
                        throw new DataServiceException("HRTM_EXCEPTION", "TIME_INVALID");
                    }
                    if (oAttendance.EmployeeID != Requestor.EmployeeID)
                    {
                        throw new DataServiceException("HRTM_EXCEPTION", "REQUEST_DATA_INCORRECT");
                    }

                    if (oAttendance.AllDayFlag)
                    {
                        HRTMManagement.CreateInstance(Requestor.CompanyCode).MarkTimesheet(Requestor.EmployeeID, "ATTENDANCE", true, oAttendance.BeginDate, oAttendance.EndDate, true, true, RequestNo);
                    }
                    else
                    {
                        HRTMManagement.CreateInstance(Requestor.CompanyCode).MarkTimesheet(Requestor.EmployeeID, "ATTENDANCE", false, oAttendance.BeginDate.Add(oAttendance.BeginTime), oAttendance.EndDate.Add(oAttendance.EndTime), true, true, RequestNo);
                    }
                    oAttendance.RequestNo = RequestNo;

                    //HRTMManagement.CreateInstance(Requestor.CompanyCode).ValidateData(Requestor.CheckDate, oAttendance, Requestor);
                    oAttendance.ValidateData(Requestor.CheckDate, Requestor.CompanyCode);

                    DataRow drAdditional;
                    drAdditional = oAdditional.NewRow();
                    oAttendance.LoadDataToTableRow(drAdditional);
                    oAdditional.Rows.Add(drAdditional);

                    //newData.Tables.Remove(oAdditional);
                    //oAdditional = oAttendance.ToADODataTable();
                    //oAdditional.TableName = "INFOTYPE2002";
                    //newData.Tables.Add(oAdditional);
                }              
            }
            catch (TimesheetCollisionException e)
            {
                throw new DataServiceException("HRTM_EXCEPTION", "COLLISION_OCCUR", "", e);
            }
            catch (AttendanceDayOffException e)
            {
                throw new DataServiceException("HRTM_EXCEPTION", "VALIDATE_DAYOFF", "", e);
            }
            //AddBy: Ratchatawan W. (2012-03-21)
            catch (AttendanceCreatingException ex)
            {
                string Err = HRTMManagement.CreateInstance(Requestor.CompanyCode).ShowErrorAttendanceCreating(Requestor.EmployeeID, WorkflowPrinciple.Current.UserSetting.Language, ex);
                throw new Exception(Err);
            }
            catch (Exception e)
            {
                string Err = HRTMManagement.CreateInstance(Requestor.CompanyCode).ShowError("HRTM_EXCEPTION",e.Message.ToString(), WorkflowPrinciple.Current.UserSetting.Language);
                throw new Exception(Err);

                //throw new DataServiceException("HRTM", "VALIDATE_DATA_ERROR", e.Message);
            }
        }

        

        public override void CalculateInfoData(EmployeeData Requestor, object ds, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            DataRow dr;
            DataRow dataRow = Data.Tables["INFOTYPE2002"].Rows[0];
            if (Info.Rows.Count == 0)
            {
                dr = Info.NewRow();
            }
            else
            {
                dr = Info.Rows[0];
            }

            INFOTYPE2002 item = new INFOTYPE2002();
            item.ParseToObject(dataRow);


            dr["ATTENDANCETYPECODE"] = string.Format("{0}#{1}", Requestor.OrgAssignment.SubAreaSetting.AbsAttGrouping, item.AttendanceType);
            dr["ATTENDANCEGROUP"] = Requestor.OrgAssignment.SubAreaSetting.AbsAttGrouping;
            dr["ATTENDANCETYPE"] = item.AttendanceType;
            dr["ATTENDANCEHOUR"] = item.AttendanceHours;
            //ModifiedBy: Ratchatawan W. (2012-02-21)
            //dr["ATTENDANCEDAYS"] = item.AttendanceDays;
            dr["ATTENDANCEDAYS"] = item.PayrollDays;
            dr["BEGINDATE"] = item.BeginDate;
            dr["ENDDATE"] = item.EndDate;
            dr["BEGINTIME"] = item.BeginTime.ToString();
            if (item.BeginTime > item.EndTime)
            {
                dr["ENDTIME"] = item.EndTime.Add(new TimeSpan(1, 0, 0, 0)).ToString();
            }
            else
            {
                dr["ENDTIME"] = item.EndTime.ToString();
            }

            if (dr.RowState == DataRowState.Detached)
            {
                Info.Rows.Add(dr);
            }
        }



        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object ds, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            DataTable oAdditional = Data.Tables["INFOTYPE2002"];
            List<INFOTYPE2002> oAttendanceList = new List<INFOTYPE2002>();
            foreach (DataRow row in oAdditional.Rows)
            {
                INFOTYPE2002 oAttendance = new INFOTYPE2002();
                oAttendance.ParseToObject(row);
                oAttendanceList.Add(oAttendance);
            }

            List<string> unmarkList = new List<string>();
            unmarkList.Add("COMPLETED");
            unmarkList.Add("CANCELLED");

            List<string> postList = new List<string>();
            postList.Add("COMPLETED");

            List<string> cancelledPostList = new List<string>();
            cancelledPostList.Add("CANCELLED");

            //List<INFOTYPE2002> postingList = new List<INFOTYPE2002>();
            if (postList.Contains(State))
            {
                #region " POST "
                // POST DATA
                try
                {
                    if (Data.Tables.Contains("POSTDATA"))
                    {
                        Data.Tables.Remove("POSTDATA");
                    }


                    try
                    {
                         HRTMManagement.CreateInstance(Requestor.CompanyCode).SavePostAttendance(oAttendanceList, Requestor.CompanyCode);
                        // ESS.HR.TM.ServiceManager.CreateInstance(Requestor.CompanyCode).ESSData.SaveAttendance(postingList);

                        foreach (INFOTYPE2002 oAttendance in oAttendanceList)
                        {
                            var temp = HRTMManagement.CreateInstance(Requestor.CompanyCode).CalculateAttendanceEntry(Requestor, oAttendance.SubType, oAttendance.BeginDate, oAttendance.EndDate, oAttendance.BeginTime, oAttendance.EndTime, oAttendance.AllDayFlag); ;
                            oAttendance.PayrollDays = temp.PayrollDays;
                            oAttendance.AttendanceHours = temp.AbsenceHours;
                            oAttendance.AttendanceDays = temp.AbsenceDays;
                            oAttendance.IsPrevDay = false; 
                            oAttendance.RequestNo = RequestNo;


                            List<ClockinLateClockoutEarly> ClockinLateClockoutEarlyList = HRTMManagement.CreateInstance(Requestor.CompanyCode).CalculateTM_InLateOutEarly(oAttendance.EmployeeID, oAttendance.BeginDate, oAttendance.EndDate);
                            if (ClockinLateClockoutEarlyList.Count > 0)
                            {
                                HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveInLateOutEarly(ClockinLateClockoutEarlyList, oAttendance.EmployeeID, oAttendance.BeginDate, oAttendance.EndDate.AddDays(1).AddSeconds(-1));
                                //HRTMManagement.MappingTimePairInLateOutEarlyID(oAttendance.EmployeeID, oAttendance.BeginDate, oAttendance.EndDate.AddDays(1).AddSeconds(-1));
                            }

                            oAttendance.IsMark = !unmarkList.Contains(State.ToUpper());
                            oAttendance.RequestNo = RequestNo;
                        }

                    }
                    catch (Exception ex1)
                    {
                        //string Err = HRTMManagement.CreateInstance(Requestor.CompanyCode).ShowError("HRTM_EXCEPTION", ex1.Message.ToString(), WorkflowPrinciple.Current.UserSetting.Language);
                        //throw new Exception(Err);
                        //throw new SaveExternalDataException("SaveExternalData Error", true, ex1);
                        throw ex1;
                    }
                    finally
                    {
                        Data.Tables["INFOTYPE2002"].Clear();
                        INFOTYPE2002 item1 = new INFOTYPE2002();
                        DataTable oTempTable;
                        oTempTable = item1.ToADODataTable(true);
                        oTempTable.TableName = "POSTDATA";
                        Data.Tables.Add(oTempTable);
                        foreach (INFOTYPE2002 item in oAttendanceList)
                        {
                            DataRow oNewRow = oTempTable.NewRow();
                            item.LoadDataToTableRow(oNewRow);
                            oTempTable.LoadDataRow(oNewRow.ItemArray, false);
                        }
                    }
                }
                catch (Exception ex)
                {
                    //throw new Exception(ex.Message);
                    throw new SaveExternalDataException("SaveExternalData Error", true, ex);
                }
                #endregion
            }
            else
            {
                foreach (INFOTYPE2002 oAttendance in oAttendanceList)
                {
                    var temp = HRTMManagement.CreateInstance(Requestor.CompanyCode).CalculateAttendanceEntry(Requestor, oAttendance.SubType, oAttendance.BeginDate, oAttendance.EndDate, oAttendance.BeginTime, oAttendance.EndTime, oAttendance.AllDayFlag); ;
                    oAttendance.PayrollDays = temp.PayrollDays;
                    oAttendance.AttendanceHours = temp.AbsenceHours;
                    oAttendance.AttendanceDays = temp.AbsenceDays;
                    oAttendance.IsMark = !unmarkList.Contains(State.ToUpper());
                    oAttendance.RequestNo = RequestNo;
                }
              
                //ESS.HR.TM.ServiceManager.HRTMBuffer.SaveAttendance(tempList);
            }
            HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveAttendance(oAttendanceList);



            try
            {
                if (postList.Contains(State))
                {
                    HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveLG_TIMEREQUEST(oAttendanceList, "ATTENDANCE");
                }
            }
            catch (Exception er)
            {

            }
        }

        public override string GenerateFlowKey(EmployeeData Requestor, DataTable Info)
        {
            string attTypeID = (string)Info.Rows[0]["ATTENDANCETYPE"];
            #region " OldVersion "

            //DateTime checkDate = (DateTime)Info.Rows[0]["BEGINDATE"];
            //TimeSpan beginTime = TimeSpan.Parse((string)Info.Rows[0]["BEGINTIME"]);
            //TimeSpan endTime = TimeSpan.Parse((string)Info.Rows[0]["ENDTIME"]);

            //// if checkdate is dayoff
            //DailyWS oDWS = Requestor.GetDailyWorkSchedule(checkDate);
            //if (",0,9,".IndexOf(oDWS.WorkscheduleClass.Trim()) > -1)
            //{
            //    return "Y";
            //}
            //else 
            //{
            //    List<TimePair> pairs = TimesheetManagement.LoadArchiveTimepair(Requestor.EmployeeID, checkDate, checkDate);
            //    TimePair firstPair, lastPair;

            //    if (pairs.Count > 1)
            //    {
            //        return "Y";
            //    } 
            //    else if (IsOutOfPlanTime(oDWS, beginTime, endTime))
            //    {
            //        if (pairs.Count == 0)
            //        {
            //            // no clock but request more than planned time
            //            return "Y";
            //        }
            //        else
            //        {
            //            firstPair = pairs[0];
            //            lastPair = pairs[pairs.Count - 1];

            //            DateTime date1, date2;
            //            date1 = checkDate.Date.Add(beginTime);
            //            date2 = checkDate.Date.Add(endTime);
            //            if (date2 < date1)
            //            {
            //                date2 = date2.AddDays(1);
            //            }
            //            if ((firstPair.ClockIN != null && date1 < firstPair.ClockIN.EventTime) || (lastPair.ClockOUT != null && date2 > lastPair.ClockOUT.EventTime))
            //            {
            //                return "Y";
            //            }
            //        }
            //    } 
            //} 

            //return "N";
            #endregion

            decimal dMaxLevelMD = HRTMManagement.CreateInstance(Requestor.CompanyCode).GetMaxLevelMD();
            if (dMaxLevelMD != -1 && Requestor.EmpSubGroup == dMaxLevelMD)
            {
                return "MD_CREATE";
            }

            // return value
            // A = admin check only
            // Y = admin check before goto authorizationmodel
            // N = go to authorizemodel
            // D = admin key directly
            //return AttendanceType.GetAttendanceFlowSetting(Requestor.EmployeeID, attTypeID);
            if (Requestor.EmpSubGroup >= Convert.ToDecimal(18))
                return "D";
            else
                return "N";

        }

        private bool IsOutOfPlanTime(DailyWS DWS, TimeSpan BeginTime, TimeSpan EndTime)
        {
            TimeSpan workEndTime;
            if (DWS.WorkBeginTime == TimeSpan.MinValue)
            {
                // case TNOM 
                return false;
            }
            else
            {
                if (DWS.WorkBeginTime > DWS.WorkEndTime)
                {
                    workEndTime = DWS.WorkEndTime.Add(new TimeSpan(1, 0, 0, 0));
                }
                else
                {
                    workEndTime = DWS.WorkEndTime;
                }
                if (BeginTime < DWS.WorkBeginTime || BeginTime > workEndTime)
                {
                    return true;
                }
                else if (EndTime < DWS.WorkBeginTime || EndTime > workEndTime)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
