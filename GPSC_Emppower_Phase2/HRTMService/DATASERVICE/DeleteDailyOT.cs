using System;
using System.Collections.Generic;
using System.Globalization;
using System.Data;
using System.Text;
using ESS.HR.TM.CONFIG;
using ESS.HR.TM.INFOTYPE;
using ESS.HR.TM;
using ESS.DATA;
using ESS.EMPLOYEE;
using ESS.TIMESHEET;
using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using Newtonsoft.Json;
using ESS.HR.TM.DATACLASS;

namespace ESS.HR.TM.DATASERVICE
{
    public class DeleteDailyOT : AbstractDataService
    {
        public DeleteDailyOT()
        { 
        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object ds, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            DataTable oAdditional = Data.Tables["DAILYOT"];
            DailyOT daily = new DailyOT();
            daily.ParseToObject(oAdditional.Rows[0]);

            List<string> postList = new List<string>();
            postList.Add("COMPLETED");

            List<string> cancelList = new List<string>();
            cancelList.Add("CANCELLED");

            List<string> unmarkList = new List<string>();
            unmarkList.Add("CANCELLED");
            unmarkList.Add("COMPLETED");

            // check existing
            if (!cancelList.Contains(State.ToUpper()) && !daily.CheckExistingData(Requestor, daily)) //DailyOT.CheckExistingData(Requestor, daily)
            {
                throw new Exception("Data for cancel can't been found");
            }

            List<DailyOT> postingList = new List<DailyOT>();
            if (postList.Contains(State))
            {
               //OTManagement.DeleteDailyOT(daily);
               HRTMManagement.CreateInstance(Requestor.CompanyCode).DeleteDailyOT(daily);
            }
            HRTMManagement.CreateInstance(Requestor.CompanyCode).MarkTimesheet(Requestor.EmployeeID, "CANCELDAILYOT", false, daily.OTDate.Add(daily.BeginTime), daily.OTDate.Add(daily.EndTime), !unmarkList.Contains(State.ToUpper()), false, RequestNo);
        }

        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            DataSet DS = new DataSet("ADDITIONAL");
            DataTable oTable;
            DailyOT oItem = new DailyOT();
            oTable = oItem.ToADODataTable(true);
            oTable.TableName = "DAILYOT";
            DS.Tables.Add(oTable);
            return DS;
        }

        public override void PrepareData(EmployeeData Requestor, object Data)
        {
        }

        public override void ValidateData(object ds, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            DataSet newData = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            if (newData.Tables["DAILYOT"].Rows.Count == 0)
            {
                throw new DataServiceException("HRTM_EXCEPTION", "NOT_FOUND_ORIGINAL_RECORD");
            }
            DataTable oAdditional = newData.Tables["DAILYOT"];
            DailyOT daily = new DailyOT();
            daily.ParseToObject(oAdditional.Rows[0]);
            try
            {
                #region " CheckCollision "
                if (newData.Tables.Contains("POSTDATA"))
                {
                    newData.Tables.Remove("POSTDATA");
                }
                HRTMManagement.CreateInstance(Requestor.CompanyCode).MarkTimesheet(Requestor.EmployeeID, "CANCELDAILYOT", false, daily.OTDate.Add(daily.BeginTime), daily.OTDate.Add(daily.EndTime), true, true, RequestNo);
                #endregion

                // check existing
                if (!daily.CheckExistingData(Requestor, daily))
                {
                    throw new DataServiceException("HRTM_EXCEPTION", "NOT_FOUND_ORIGINAL_RECORD");
                }

                // check cancel Rule
                DailyOTDeletingRule oDeletingRule =new DailyOTDeletingRule().GetDeletingRule();
                oDeletingRule.IsCanDelete(daily.EmployeeID, daily.OTDate, DocumentDate);
            }
            catch (DeleteDailyOTException e)
            {
                throw new DataServiceException("HRTM_EXCEPTION", "CONFLICT_RULE", "", e);
            }
            catch (TimesheetCollisionException e)
            {
                throw new DataServiceException("HRTM", "COLLISION_OCCUR", "", e);
            }
            catch (Exception e)
            {
                throw new DataServiceException("HRTM_EXCEPTION", e.Message, "");
            }
        }

        public override void CalculateInfoData(EmployeeData Requestor, object ds, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            Info.Rows.Clear();
            DataRow oRow = Info.NewRow();
            DailyOT item = new DailyOT();
            item.ParseToObject(Data.Tables["DAILYOT"].Rows[0]);
            oRow["OTDATE"] = item.OTDate;
            oRow["BEGINTIME"] = TimeSpanToString(item.BeginTime);
            oRow["ENDTIME"] = TimeSpanToString(item.EndTime);
            oRow["ASSIGNFROM"] = item.AssignFrom;
            Info.Rows.Add(oRow);
        }

        private string TimeSpanToString(TimeSpan input)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            DateTime oDate = DateTime.Now.Date.Add(input);
            return oDate.ToString("HH:mm", oCL);
        }
    }
}
