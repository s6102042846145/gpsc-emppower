﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Globalization;
using ESS.DATA;
using ESS.DATA.ABSTRACT;
using ESS.EMPLOYEE;
using ESS.HR.TM.INFOTYPE;
using Newtonsoft.Json;
using ESS.DATA.EXCEPTION;
using ESS.TIMESHEET;
using ESS.HR.TM.DATACLASS;

namespace ESS.HR.TM.DATASERVICE
{
    public class SubstitutionDeleteService2 : AbstractDataService
    {
        CultureInfo oCL = new CultureInfo("en-US");
        string ApplicationKey = "CANCELSUBSTITUTION";

        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            DataSet DS = new DataSet("ADDITIONAL");
            DataTable oTable;
            INFOTYPE2003 oItem = new INFOTYPE2003();
            oTable = oItem.ToADODataTable(true);
            oTable.TableName = "INFOTYPE2003";
            DS.Tables.Add(oTable);


            DataTable oInfo = new DataTable();
            DataColumn column;
            oInfo.TableName = "InfoTable";
            column = new DataColumn("ISAllDatePeriod", typeof(Boolean));
            oInfo.Columns.Add(column);
            column = new DataColumn("BeginDate", typeof(DateTime));
            oInfo.Columns.Add(column);
            column = new DataColumn("EndDate", typeof(DateTime));
            oInfo.Columns.Add(column);

            DataRow NewRow = oInfo.NewRow();
            NewRow["ISAllDatePeriod"] = false;
            NewRow["BeginDate"] = DateTime.MinValue;
            NewRow["EndDate"] = DateTime.MinValue;
            oInfo.Rows.Add(NewRow);
            DS.Tables.Add(oInfo);

            DataTable oInfoSubstitute = new DataTable();
            DataColumn column1;
            oInfoSubstitute.TableName = "InfoSubstitute";
            column1 = new DataColumn("SUBSTITUTE_APPROVERPOSITION", typeof(string));
            oInfoSubstitute.Columns.Add(column1);
            column1 = new DataColumn("SubstituteId", typeof(string));
            oInfoSubstitute.Columns.Add(column1);
            column1 = new DataColumn("SubstituteName", typeof(string));
            oInfoSubstitute.Columns.Add(column1);
            column1 = new DataColumn("PostionId", typeof(string));
            oInfoSubstitute.Columns.Add(column1);
            column1 = new DataColumn("PostionName", typeof(string));
            oInfoSubstitute.Columns.Add(column1);
            column1 = new DataColumn("OrganizationId", typeof(string));
            oInfoSubstitute.Columns.Add(column1);
            column1 = new DataColumn("OrganizationName", typeof(string));
            oInfoSubstitute.Columns.Add(column1);


            DataRow NewRow1 = oInfoSubstitute.NewRow();
            NewRow1["SUBSTITUTE_APPROVERPOSITION"] = "";
            NewRow1["SUBSTITUTE_APPROVERPOSITION"] = "";
            NewRow1["SubstituteId"] = "";
            NewRow1["SubstituteName"] = "";
            NewRow1["PostionId"] = "";
            NewRow1["PostionName"] = "";
            NewRow1["OrganizationId"] = "";
            NewRow1["OrganizationName"] = "";
            oInfoSubstitute.Rows.Add(NewRow1);

            DS.Tables.Add(oInfoSubstitute);

            return DS;
        }

        public override void CalculateInfoData(EmployeeData Requestor, object ds, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {

            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            DataTable oInfoTable = Data.Tables["InfoTable"];
            bool ISAllDate = false;
            DateTime BeginDate = new DateTime();
            DateTime EndDate = new DateTime();

            if (oInfoTable != null && oInfoTable.Rows.Count > 0)
            {
                BeginDate = (DateTime)oInfoTable.Rows[0]["BeginDate"];
                EndDate = (DateTime)oInfoTable.Rows[0]["EndDate"];
                ISAllDate = (bool)oInfoTable.Rows[0]["ISAllDatePeriod"];
            }

            INFOTYPE2003 item = new INFOTYPE2003();
            item.ParseToObject(Data.Tables["INFOTYPE2003"]);
            DataRow dataRow;
            Info.Rows.Clear();
            dataRow = Info.NewRow();
            dataRow["SUBSTITUTEWITH"] = item.Substitute;
            dataRow["SUBSTITUTE_APPROVERPOSITION"] = Data.Tables["InfoSubstitute"].Rows[0]["SUBSTITUTE_APPROVERPOSITION"].ToString();

            if (BeginDate.Date == EndDate.Date)
            {
                dataRow["DATE"] = BeginDate.ToString("dd/MM/yyyy", oCL);
            }
            else
            {
                dataRow["DATE"] = string.Format("{0} - {1}", BeginDate.ToString("dd/MM/yyyy", oCL), EndDate.ToString("dd/MM/yyyy", oCL));
            }

            //if (ISAllDate)
            //    dataRow["DATE"] = string.Format("{0} => {1}", BeginDate.ToString("dd/MM/yyyy", oCL), EndDate.ToString("dd/MM/yyyy", oCL));
            //else if (BeginDate.Date == EndDate.Date)
            //    dataRow["DATE"] = BeginDate.ToString("dd/MM/yyyy", oCL);
            //else
            //    dataRow["DATE"] = string.Format("{0}, {1}", BeginDate.ToString("dd/MM/yyyy", oCL), EndDate.ToString("dd/MM/yyyy", oCL));
            Info.Rows.Add(dataRow);
        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object ds, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            INFOTYPE2003 oINFOTYPE2003 = new INFOTYPE2003();
            eState CurrentState = (eState)Enum.Parse(typeof(eState), State.ToUpper(), true);

            List<TimesheetData> oTimesheetDataList = new List<TimesheetData>();
            TimesheetData oTimesheetData;

            List<string> postList = new List<string>();
            postList.Add("COMPLETED");

            List<string> unmarkList = new List<string>();
            unmarkList.Add("CANCELLED");
            unmarkList.Add("COMPLETED");

            foreach (DataRow row in Data.Tables["INFOTYPE2003"].Rows)
            {
                oINFOTYPE2003 = new INFOTYPE2003();
                oINFOTYPE2003.ParseToObject(row);

                if (CurrentState == eState.DRAFT)
                    row["Status"] = SubstituteStatus.DRAFTFORCANCEL;
                else if (CurrentState == eState.WAITFORACCEPT)
                    row["Status"] = SubstituteStatus.WAITFORACCEPTFORCANCEL;
                else if (CurrentState == eState.WAITFORAPPROVE)
                    row["Status"] = SubstituteStatus.WAITFORAPPROVEFORCANCEL;
                else if (CurrentState == eState.WAITFOREDIT)
                    row["Status"] = SubstituteStatus.WAITFOREDITFORCANCEL;
                else if (CurrentState == eState.WAITFORPOSTDATA)
                    row["Status"] = SubstituteStatus.WAITFORPOSTDATAFORCANCEL;
                HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveSubstitutionLog(row);

                if (postList.Contains(State))
                {
                    try
                    {
                        HRTMManagement.CreateInstance(Requestor.CompanyCode).DeleteSubstitution(oINFOTYPE2003.EmployeeID, oINFOTYPE2003.BeginDate, oINFOTYPE2003.EndDate);
                        HRTMManagement.CreateInstance(Requestor.CompanyCode).DeleteSubstitution(oINFOTYPE2003.Substitute, oINFOTYPE2003.SubstituteBeginDate, oINFOTYPE2003.SubstituteEndDate);
                    }
                    catch (Exception ex)
                    {
                        row["Status"] = SubstituteStatus.WAITFORPOSTDATAFORCANCEL;
                        HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveSubstitutionLog(row);

                        if (Data.Tables.Contains("POSTDATA"))
                            Data.Tables.Remove("POSTDATA");

                        DataTable oTempTable;
                        oTempTable = oINFOTYPE2003.ToADODataTable(true);
                        oTempTable.TableName = "POSTDATA";
                        Data.Tables.Add(oTempTable);
                        DataRow oNewRow = oTempTable.NewRow();
                        oINFOTYPE2003.LoadDataToTableRow(oNewRow);
                        oNewRow["Remark"] = ex.InnerException.Message;
                        oTempTable.LoadDataRow(oNewRow.ItemArray, false);

                        throw new SaveExternalDataException("SaveExternalData Error", true, ex);
                    }

                    try
                    {
                        HRTMManagement.CreateInstance(Requestor.CompanyCode).UpdateTimePair(oINFOTYPE2003.EmployeeID, oINFOTYPE2003.BeginDate, oINFOTYPE2003.EndDate);
                        List<ClockinLateClockoutEarly> ClockinLateClockoutEarlyList_emp = HRTMManagement.CreateInstance(Requestor.CompanyCode).CalculateTM_InLateOutEarly(oINFOTYPE2003.EmployeeID, oINFOTYPE2003.BeginDate, oINFOTYPE2003.EndDate);
                        if (ClockinLateClockoutEarlyList_emp.Count > 0)
                        {
                            HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveInLateOutEarly(ClockinLateClockoutEarlyList_emp, oINFOTYPE2003.EmployeeID, oINFOTYPE2003.BeginDate, oINFOTYPE2003.EndDate.AddDays(1).AddSeconds(-1));
                            HRTMManagement.CreateInstance(Requestor.CompanyCode).MappingTimePairInLateOutEarlyID(oINFOTYPE2003.EmployeeID, oINFOTYPE2003.BeginDate, oINFOTYPE2003.EndDate);
                        }
                        HRTMManagement.CreateInstance(Requestor.CompanyCode).UpdateTimePair(oINFOTYPE2003.Substitute, oINFOTYPE2003.BeginDate, oINFOTYPE2003.EndDate);
                        List<ClockinLateClockoutEarly> ClockinLateClockoutEarlyList_sub = HRTMManagement.CreateInstance(Requestor.CompanyCode).CalculateTM_InLateOutEarly(oINFOTYPE2003.Substitute, oINFOTYPE2003.BeginDate, oINFOTYPE2003.EndDate);
                        if (ClockinLateClockoutEarlyList_sub.Count > 0)
                        {
                            HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveInLateOutEarly(ClockinLateClockoutEarlyList_sub, oINFOTYPE2003.Substitute, oINFOTYPE2003.BeginDate, oINFOTYPE2003.EndDate.AddDays(1).AddSeconds(-1));
                            HRTMManagement.CreateInstance(Requestor.CompanyCode).MappingTimePairInLateOutEarlyID(oINFOTYPE2003.Substitute, oINFOTYPE2003.BeginDate, oINFOTYPE2003.EndDate);
                        }
                    }
                    catch (Exception ex) { }
                }

                oTimesheetData = new TimesheetData();
                oTimesheetData.ApplicationKey = ApplicationKey;
                oTimesheetData.SubKey1 = string.Empty;
                oTimesheetData.SubKey2 = string.Empty;
                oTimesheetData.FullDay = true;
                oTimesheetData.EmployeeID = oINFOTYPE2003.EmployeeID;
                oTimesheetData.BeginDate = oINFOTYPE2003.BeginDate;
                oTimesheetData.EndDate = oINFOTYPE2003.EndDate;
                oTimesheetData.Detail = RequestNo;
                oTimesheetDataList.Add(oTimesheetData);

                oTimesheetData = new TimesheetData();
                oTimesheetData.ApplicationKey = ApplicationKey;
                oTimesheetData.SubKey1 = string.Empty;
                oTimesheetData.SubKey2 = string.Empty;
                oTimesheetData.FullDay = true;
                oTimesheetData.EmployeeID = oINFOTYPE2003.Substitute;
                oTimesheetData.BeginDate = oINFOTYPE2003.SubstituteBeginDate;
                oTimesheetData.EndDate = oINFOTYPE2003.SubstituteEndDate;
                oTimesheetData.Detail = RequestNo;
                oTimesheetDataList.Add(oTimesheetData);
            }

            try
            {
                HRTMManagement.CreateInstance(Requestor.CompanyCode).MarkTimesheet(oTimesheetDataList, !unmarkList.Contains(State.ToUpper()), false);
            }
            catch (Exception ex)
            {
                if (postList.Contains(State))
                    throw new SaveExternalDataException("SaveExternalData Error", true, ex);
                else
                    throw ex;
            }
        }

        public override void PrepareData(EmployeeData Requestor, object Data)
        {

        }

        public override void ValidateData(object ds, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            DataSet newData = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            List<TimesheetData> oTimesheetDataList = new List<TimesheetData>();
            TimesheetData oTimesheetData;
            INFOTYPE2003 item;
            foreach (DataRow dr in newData.Tables["INFOTYPE2003"].Rows)
            {
                item = new INFOTYPE2003();
                item.ParseToObject(dr);

                oTimesheetData = new TimesheetData();
                oTimesheetData.ApplicationKey = ApplicationKey;
                oTimesheetData.SubKey1 = string.Empty;
                oTimesheetData.SubKey2 = string.Empty;
                oTimesheetData.FullDay = true;
                oTimesheetData.EmployeeID = item.EmployeeID;
                oTimesheetData.BeginDate = item.BeginDate;
                oTimesheetData.EndDate = item.EndDate;
                oTimesheetData.Detail = RequestNo;
                oTimesheetDataList.Add(oTimesheetData);

                oTimesheetData = new TimesheetData();
                oTimesheetData.ApplicationKey = ApplicationKey;
                oTimesheetData.SubKey1 = string.Empty;
                oTimesheetData.SubKey2 = string.Empty;
                oTimesheetData.FullDay = true;
                oTimesheetData.EmployeeID = item.Substitute;
                oTimesheetData.BeginDate = item.SubstituteBeginDate;
                oTimesheetData.EndDate = item.SubstituteEndDate;
                oTimesheetData.Detail = RequestNo;
                oTimesheetDataList.Add(oTimesheetData);
            }
            try
            {
                HRTMManagement.CreateInstance(Requestor.CompanyCode).MarkTimesheet(oTimesheetDataList, true, true);
            }
            catch (TimesheetCollisionException e)
            {
                throw new DataServiceException("HRTM_EXCEPTION", "COLLISION_OCCUR", "", e);
            }
            catch (Exception e)
            {
                throw new DataServiceException("HRTM_EXCEPTION", e.Message, "");
            }
        }

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info)
        {
            string empSubWITH = (string)Info.Rows[0]["SUBSTITUTEWITH"];
            EmployeeData oEmpSubWITH = new EmployeeData(empSubWITH, DateTime.Now);

            if (Requestor.OrgAssignment.OrgUnit == oEmpSubWITH.OrgAssignment.OrgUnit)
                return "SO";
            else
                return "DO";
        }
    }
}
