using System;
using System.Collections.Generic;
using System.Text;
using ESS.DATA;
using ESS.EMPLOYEE;
using System.Data;
using ESS.HR.TM.INFOTYPE;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using Newtonsoft.Json;
using ESS.HR.TM.DATACLASS;
using ESS.WORKFLOW;

namespace ESS.HR.TM.DATASERVICE
{
    public class DutyPaymentService : AbstractDataService
    {
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            DataSet DS = new DataSet("ADDITIONAL");
            DataTable oTable, oInfo,oPeriod;
            DataColumn column;
            DutyPaymentLog oItem = new DutyPaymentLog();
            oTable = oItem.ToADODataTable(true);
            oTable.TableName = "DutyPaymentLog";

            DS.Tables.Add(oTable);

            oInfo = new DataTable();
            oInfo.TableName = "InfoTable";

            column = new DataColumn();
            column.ColumnName = "TOTALAMOUNT";
            column.DataType = Type.GetType("System.Decimal");
            oInfo.Columns.Add(column);

            column = new DataColumn();
            column.ColumnName = "DUTYDATE";
            column.DataType = Type.GetType("System.DateTime");
            oInfo.Columns.Add(column);

            column = new DataColumn();
            column.ColumnName = "REQUESTORLEVEL";
            column.DataType = Type.GetType("System.Decimal");
            oInfo.Columns.Add(column);
            DS.Tables.Add(oInfo);

            oPeriod = new DataTable();
            oPeriod.TableName = "PeriodDutyPayment";
            DS.Tables.Add(oPeriod);

            return DS;
        }

        public override void CalculateInfoData(EmployeeData Requestor, object ds, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            Info.Clear();
            DataRow NewRow;
            foreach (DataRow row in Data.Tables["InfoTable"].Rows)
            {
                NewRow = Info.NewRow();
                NewRow.BeginEdit();
                NewRow["TOTALAMOUNT"] = row["TOTALAMOUNT"];
                NewRow["DUTYDATE"] = row["DUTYDATE"];
                NewRow["REQUESTORLEVEL"] = row["REQUESTORLEVEL"];
                NewRow.EndEdit();
                Info.Rows.Add(NewRow);
            }
        }

        public override void PrepareData(EmployeeData Requestor, object Data)
        {

        }

        public override void ValidateData(object ds, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            DataSet newData = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            DataTable oTable = newData.Tables["DutyPaymentLog"];



            //DataRow NewRow;
            //newData.Tables["InfoTable"].Rows.Clear();
            //NewRow = newData.Tables["InfoTable"].NewRow();
            //NewRow.BeginEdit();
            //NewRow["TOTALAMOUNT"] = DutyManagement.CalculateSummaryDutyPayment(newData.Tables["DutyPaymentLog"]);
            //NewRow["DUTYDATE"] = newData.Tables["DutyPaymentLog"].Rows.Count > 0 ? ((DateTime)newData.Tables["DutyPaymentLog"].Rows[0]["DutyDate"]).Date : DateTime.MinValue;
            //NewRow["REQUESTORLEVEL"] = Requestor.EmpSubGroup;
            //NewRow.EndEdit();
            //newData.Tables["InfoTable"].Rows.Add(NewRow);




            //- ��ͧ�ըӹǹ�������ҡ����������ҡѺ 1 Record
            if (oTable.Rows.Count == 0)
            {
                string Err = HRTMManagement.CreateInstance(Requestor.CompanyCode).ShowError("DUTYPAYMENT_EXCEPTION", "NOT_FOUND_ORIGINAL_RECORD", WorkflowPrinciple.Current.UserSetting.Language);
                throw new Exception(Err);
                //throw new DataServiceException("DUTYPAYMENT_EXCEPTION", "NOT_FOUND_ORIGINAL_RECORD");
            }
            else
            {
                DataTable oInfoTable = newData.Tables["InfoTable"];
                DateTime dtDutyDate, dtDutyBeginDate, dtDutyEndDate, dtBeginDate, dtEndDate, dtWorkBegin, dtWorkEnd;
                DailyWS oDWS;

                dtDutyDate = Convert.ToDateTime(oInfoTable.Rows[0]["DUTYDATE"]);
                dtBeginDate = new DateTime(dtDutyDate.Year, dtDutyDate.Month, 1);
                dtEndDate = dtBeginDate.AddMonths(1).AddDays(-1);

                EmployeeData emp = new EmployeeData(Requestor.EmployeeID, dtBeginDate);

                List<DutyPaymentLog> lstDuty = HRTMManagement.CreateInstance(Requestor.CompanyCode).GetDutyPaymentLog(emp.EmployeeID, dtBeginDate, dtEndDate);//DutyManagement.GetDutyPaymentLog(Requestor.EmployeeID, dtBeginDate, dtEndDate);
                lstDuty = lstDuty.FindAll(delegate(DutyPaymentLog log) { return !log.RequestNo.Equals(RequestNo); });
                EmployeeData oEmp = null;
                List<INFOTYPE2001> lstAbsence = null;
                foreach (DataRow row in oTable.Rows)
                {
                    dtDutyDate = Convert.ToDateTime(row["DUTYDATE"]);
                    dtDutyBeginDate = Convert.ToDateTime(row["BEGINDATE"]);
                    dtDutyEndDate = Convert.ToDateTime(row["ENDDATE"]);
                    //- ��� Request ��㹤��駹����������ҫ�͹�ѹ���� Document ���
                    if (oTable.Select(String.Format("ItemNo <> '{0}' AND EndDate > '{1}' AND BeginDate < '{2}'", row["ItemNo"], row["BeginDate"], row["EndDate"])).Length > 0)
                    {
                        string Err = HRTMManagement.CreateInstance(Requestor.CompanyCode).ShowError("DUTYPAYMENT_EXCEPTION", String.Format("OVERLAP_REQUEST|{0}", DateTime.Parse(row["DutyDate"].ToString()).ToString("d/M/yyyy")), WorkflowPrinciple.Current.UserSetting.Language);
                        throw new Exception(Err);
                        //throw new DataServiceException("DUTYPAYMENT_EXCEPTION", String.Format("OVERLAP_REQUEST|{0}", row["DutyDate"]));
                    }
                    //- ��� Request ��㹤��駹����������ҫ�͹�ѹ�Ѻ Request ��� �
                    if (lstDuty.FindAll(delegate(DutyPaymentLog log) { return log.EndDate > Convert.ToDateTime(row["BeginDate"]) && log.BeginDate < Convert.ToDateTime(row["EndDate"]); }).Count > 0)
                    {
                        string Err = HRTMManagement.CreateInstance(Requestor.CompanyCode).ShowError("DUTYPAYMENT_EXCEPTION", String.Format("OVERLAP_REQUEST|{0}", DateTime.Parse(row["DutyDate"].ToString()).ToString("d/M/yyyy")), WorkflowPrinciple.Current.UserSetting.Language);
                        throw new Exception(Err);
                        //throw new DataServiceException("DUTYPAYMENT_EXCEPTION", String.Format("OVERLAP_REQUEST|{0}", row["DutyDate"]));
                    }


                    //- ����ԡ��һ�Ժѵԧҹ�����èе�ͧ���ç�Ѻ���ҷӧҹ�ͧ���ͧ
                    for (DateTime dt = dtDutyBeginDate.Date; dt <= dtDutyEndDate.Date; dt = dt.AddDays(1))
                    {
                        //if (oEmp == null || !(oEmp.OrgAssignment.BeginDate <= dt && oEmp.OrgAssignment.EndDate >= dt))
                        //    oEmp = new EmployeeData(row["EmployeeID"].ToString(), dt);

                        //oDWS = Requestor.GetDailyWorkSchedule(dt, true);

                        if (emp == null || !(emp.OrgAssignment.BeginDate <= dt && emp.OrgAssignment.EndDate >= dt))
                            emp = new EmployeeData(row["EmployeeID"].ToString(), dt);

                        oDWS = HRTMManagement.CreateInstance(Requestor.CompanyCode).GetDailyWorkSchedule(emp, dt, true);


                        if (emp.EmpSubAreaType == EmployeeSubAreaType.Flex)
                        {
                            INFOTYPE0007 oInfotype0007 = emp.GetEmployeeWF(emp.EmployeeID, dt);
                            if (!string.IsNullOrEmpty(oInfotype0007.WFRule))
                            {
                                TimeSpan oBeginTimeDailyFlexTime;
                                TimeSpan oEndTimeDailyFlexTime;
                                DailyFlexTime oDFX = HRTMManagement.CreateInstance(Requestor.CompanyCode).GetDailyFlexTimeMinForCalculateByFlexCode(oInfotype0007.WFRule);

                                BreakPattern oBreak = new BreakPattern();
                                oBreak = EmployeeManagement.CreateInstance(Requestor.CompanyCode).GetBreakPattern(oDWS.DailyWorkscheduleGrouping, oDFX.BreakCode);

                                TimeSpan TBegin;
                                if (!TimeSpan.TryParse(oDFX.BeginTime, out TBegin))
                                {
                                    // handle validation error
                                }
                                oBeginTimeDailyFlexTime = new TimeSpan(TBegin.Hours, TBegin.Minutes, 0);

                                TimeSpan TEnd;
                                if (!TimeSpan.TryParse(oDFX.EndTime, out TEnd))
                                {
                                    // handle validation error
                                }
                                oEndTimeDailyFlexTime = new TimeSpan(TEnd.Hours, TEnd.Minutes, 0);
                                oDWS.WorkBeginTime = oBeginTimeDailyFlexTime;
                                oDWS.WorkEndTime = oEndTimeDailyFlexTime;
                            }
                        }


                        if (!oDWS.IsDayOffOrHoliday)
                        {
                            dtWorkBegin = dt.Add(oDWS.WorkBeginTime);
                            dtWorkEnd = dt.Add(oDWS.WorkEndTime);

                            if (dtWorkBegin > dtWorkEnd)
                                dtWorkEnd = dtWorkEnd.AddDays(1);

                            if (dtWorkBegin < dtDutyEndDate && dtWorkEnd > dtDutyBeginDate)
                            {
                                string Err = HRTMManagement.CreateInstance(Requestor.CompanyCode).ShowError("DUTYPAYMENT_EXCEPTION", String.Format("OVERLAP_WORKTIME|{0}", DateTime.Parse(row["DutyDate"].ToString()).ToString("d/M/yyyy")), WorkflowPrinciple.Current.UserSetting.Language);
                                throw new Exception(Err);
                                //throw new DataServiceException("DUTYPAYMENT_EXCEPTION", String.Format("OVERLAP_WORKTIME|{0}", row["DutyDate"]));
                            }
                        }
                    }


                    //- ��� Request ��㹤��駹���ͧ���ç�Ѻ�ѹ�����
                    INFOTYPE2001 INF2001 = new INFOTYPE2001();
                    lstAbsence = INF2001.GetData(emp, dtDutyBeginDate, dtDutyEndDate);
                    if (lstAbsence.Count > 0)
                    {
                        foreach (INFOTYPE2001 oAbsence in lstAbsence)
                        {
                            //if (oAbsence.BeginDate.Date.Equals(oAbsence.EndDate.Date) && oAbsence.AllDayFlag)
                            if (oAbsence.AllDayFlag)
                            {
                                for (DateTime dt = oAbsence.BeginDate; dt <= oAbsence.EndDate; dt = dt.AddDays(1))
                                {
                                    //if (oEmp == null || !(oEmp.OrgAssignment.BeginDate <= dt && oEmp.OrgAssignment.EndDate >= dt))
                                    //    oEmp = new EmployeeData(row["EmployeeID"].ToString(), dt);

                                    //oDWS = oEmp.GetDailyWorkSchedule(dt, true);

                                    if (emp == null || !(emp.OrgAssignment.BeginDate <= dt && emp.OrgAssignment.EndDate >= dt))
                                        emp = new EmployeeData(row["EmployeeID"].ToString(), dt);

                                    oDWS = HRTMManagement.CreateInstance(Requestor.CompanyCode).GetDailyWorkSchedule(emp, dt, true);

                                    if (emp.EmpSubAreaType == EmployeeSubAreaType.Flex)
                                    {
                                        INFOTYPE0007 oInfotype0007 = emp.GetEmployeeWF(emp.EmployeeID, dt);
                                        if (!string.IsNullOrEmpty(oInfotype0007.WFRule))
                                        {
                                            TimeSpan oBeginTimeDailyFlexTime;
                                            TimeSpan oEndTimeDailyFlexTime;
                                            DailyFlexTime oDFX = HRTMManagement.CreateInstance(Requestor.CompanyCode).GetDailyFlexTimeMinForCalculateByFlexCode(oInfotype0007.WFRule);

                                            BreakPattern oBreak = new BreakPattern();
                                            oBreak = EmployeeManagement.CreateInstance(Requestor.CompanyCode).GetBreakPattern(oDWS.DailyWorkscheduleGrouping, oDFX.BreakCode);

                                            TimeSpan TBegin;
                                            if (!TimeSpan.TryParse(oDFX.BeginTime, out TBegin))
                                            {
                                                // handle validation error
                                            }
                                            oBeginTimeDailyFlexTime = new TimeSpan(TBegin.Hours, TBegin.Minutes, 0);

                                            TimeSpan TEnd;
                                            if (!TimeSpan.TryParse(oDFX.EndTime, out TEnd))
                                            {
                                                // handle validation error
                                            }
                                            oEndTimeDailyFlexTime = new TimeSpan(TEnd.Hours, TEnd.Minutes, 0);
                                            oDWS.WorkBeginTime = oBeginTimeDailyFlexTime;
                                            oDWS.WorkEndTime = oEndTimeDailyFlexTime;
                                        }
                                    }

                                    if (!oDWS.IsDayOffOrHoliday)
                                    {
                                        dtWorkBegin = dt.Date.Add(oDWS.WorkBeginTime);
                                        dtWorkEnd = dt.Date.Add(oDWS.WorkEndTime);
                                        if (dtWorkBegin >= dtWorkEnd)
                                            dtWorkEnd = dtWorkEnd.AddDays(1);

                                        if (dtWorkBegin < dtDutyEndDate && dtWorkEnd > dtDutyBeginDate)
                                        {
                                            lstAbsence.Clear();

                                            string Err = HRTMManagement.CreateInstance(Requestor.CompanyCode).ShowError("DUTYPAYMENT_EXCEPTION", String.Format("OVERLAP_LEAVE_REQUEST|{0}", DateTime.Parse(row["DutyDate"].ToString()).ToString("d/M/yyyy")), WorkflowPrinciple.Current.UserSetting.Language);
                                            throw new Exception(Err);
                                            //throw new DataServiceException("DUTYPAYMENT_EXCEPTION", String.Format("OVERLAP_LEAVE_REQUEST|{0}", row["DutyDate"]));
                                        }
                                    }
                                    oDWS = null;
                                }
                            }
                            else
                            {
                                oAbsence.BeginDate = oAbsence.BeginDate.Add(oAbsence.BeginTime);
                                oAbsence.EndDate = oAbsence.EndDate.Add(oAbsence.EndTime);
                                if (oAbsence.BeginDate >= oAbsence.EndDate)
                                    oAbsence.EndDate = oAbsence.EndDate.AddDays(1);

                                if (oAbsence.BeginDate < dtDutyEndDate && oAbsence.EndDate > dtDutyBeginDate)
                                {
                                    lstAbsence.Clear();

                                    string Err = HRTMManagement.CreateInstance(Requestor.CompanyCode).ShowError("DUTYPAYMENT_EXCEPTION", String.Format("OVERLAP_LEAVE_REQUEST|{0}", DateTime.Parse(row["DutyDate"].ToString()).ToString("d/M/yyyy")), WorkflowPrinciple.Current.UserSetting.Language);
                                    throw new Exception(Err);
                                    //throw new DataServiceException("DUTYPAYMENT_EXCEPTION", String.Format("OVERLAP_LEAVE_REQUEST|{0}", row["DutyDate"]));
                                }
                            }
                        }

                        lstAbsence.Clear();
                    }

                }


                oDWS = null;
                oEmp = null;
                lstDuty.Clear();
                lstDuty = null;
            }
        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object ds, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            DataTable oAdditional = Data.Tables["DutyPaymentLog"];
            bool IsDeleteOnly = false;
            if (State.ToUpper() == "CANCELLED")
            {
                IsDeleteOnly = true;
                foreach (DataRow row in oAdditional.Rows)
                {
                    row.BeginEdit();
                    row["RequestNo"] = RequestNo;
                    row["Status"] = State.ToUpper();
                    row.EndEdit();
                }
            }
            else
            {
                foreach (DataRow row in oAdditional.Rows)
                {
                    row.BeginEdit();
                    row["RequestNo"] = RequestNo;
                    row["Status"] = State.ToUpper();
                    row.EndEdit();
                }
                // - ����� Post ŧ SAP �����ʶҹ��� Completed
                if (State.ToUpper() == "COMPLETED")
                {
                    // - �¡�����ŷ�����ѹ Work ��� Off
                    List<DutyPaymentLog> lstOnDayWork = new List<DutyPaymentLog>();
                    List<DutyPaymentLog> lstOnDayOff = new List<DutyPaymentLog>();
                    EmployeeData oEmp = Requestor;
                    DailyWS oDWS = null;
                    DateTime dtDutyDate = Convert.ToDateTime(Info.Rows[0]["DUTYDATE"]);
                    DateTime dtBeginDate = new DateTime(dtDutyDate.Year, dtDutyDate.Month, 1);
                    DateTime dtEndDate = dtBeginDate.AddMonths(1).AddSeconds(-1);

                    EmployeeData emp = new EmployeeData(Requestor.EmployeeID, dtBeginDate);
                    foreach (DataRow row in oAdditional.Rows)
                    {
                        DutyPaymentLog log = new DutyPaymentLog();
                        log.ParseToObject(row);

                        if (!(oEmp.OrgAssignment.BeginDate <= log.DutyDate && oEmp.OrgAssignment.EndDate >= log.DutyDate))
                            oEmp = new EmployeeData(Requestor.EmployeeID, log.DutyDate);

                        oDWS = HRTMManagement.CreateInstance(Requestor.CompanyCode).GetDailyWorkSchedule(oEmp, log.DutyDate, true);

                        if (oDWS.IsDayOffOrHoliday)
                        {
                            lstOnDayOff.Add(log);
                        }
                        else
                        {
                            lstOnDayWork.Add(log);
                        }
                    }

                    List<DutyPaymentLog> lstPrevApproved = HRTMManagement.CreateInstance(Requestor.CompanyCode).GetDutyPaymentLog(Requestor.EmployeeID, dtBeginDate, dtEndDate).FindAll(delegate (DutyPaymentLog log) { return log.Status.ToUpper().Equals("COMPLETED"); });  //DutyManagement.GetDutyPaymentLog(Requestor.EmployeeID, dtBeginDate, dtEndDate).FindAll(delegate(DutyPaymentLog log) { return log.Status.ToUpper().Equals("COMPLETED"); });
                    foreach (DutyPaymentLog log in lstPrevApproved)
                    {
                        if (!(oEmp.OrgAssignment.BeginDate <= log.DutyDate && oEmp.OrgAssignment.EndDate >= log.DutyDate))
                            oEmp = new EmployeeData(Requestor.EmployeeID, log.DutyDate);

                        oDWS = HRTMManagement.CreateInstance(Requestor.CompanyCode).GetDailyWorkSchedule(oEmp, log.DutyDate, true);

                        if (oDWS.IsDayOffOrHoliday)
                        {
                            lstOnDayOff.Add(log);
                        }
                        else
                        {
                            lstOnDayWork.Add(log);
                        }
                    }
                    lstPrevApproved.Clear();
                    lstPrevApproved = null;

                    if (lstOnDayOff.Count > 0)
                    {
                        Dictionary<DateTime, decimal> dictOnDayOff = DutyManagement.CalculateDutyPayment(lstOnDayOff);

                        HRTMManagement.CreateInstance(Requestor.CompanyCode).PostDutyPaymentDayOff(Requestor.EmployeeID, dictOnDayOff);
                        dictOnDayOff.Clear();
                        dictOnDayOff = null;
                    }

                    if (lstOnDayWork.Count > 0)
                    {
                        Dictionary<DateTime, decimal> dictOnDayWork = DutyManagement.CalculateDutyPayment(lstOnDayWork);

                        HRTMManagement.CreateInstance(Requestor.CompanyCode).PostDutyPaymentDayWork(Requestor.EmployeeID, dictOnDayWork);
                        dictOnDayWork.Clear();
                        dictOnDayWork = null;
                    }

                    lstOnDayOff.Clear();
                    lstOnDayWork.Clear();

                    lstOnDayOff = null;
                    lstOnDayWork = null;
                }
            }
            HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveDutyPaymentLog(Data.Tables["DutyPaymentLog"], IsDeleteOnly);
        }

        //public override void ValidateDataInAction(object newData, DataTable Info, EmployeeData Requestor, string RequestNo, string State, string Action, bool HaveComment, bool HaveComment2, DateTime SubmitDate)
        //{
        //    bool isPassValidate = HRTMManagement.CreateInstance(Requestor.CompanyCode).ShowTimeAwareLink(4);
        //    if (!isPassValidate)
        //    {
        //        throw new DataServiceException("HRTM_EXCEPTION", "TIMEOUT_PERIOD_SETTING");
        //    }
        //}
    }
}
