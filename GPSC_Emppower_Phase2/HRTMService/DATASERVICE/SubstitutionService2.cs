﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using ESS.DATA;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.WORKFLOW;
using ESS.TIMESHEET;
using ESS.DATA.ABSTRACT;
using ESS.HR.TM.INFOTYPE;
using ESS.DATA.EXCEPTION;
using System.Globalization;
using Newtonsoft.Json;
using ESS.HR.TM.DATACLASS;

namespace ESS.HR.TM.DATASERVICE
{
    public class SubstitutionService2 : AbstractDataService
    {
        CultureInfo oCL = new CultureInfo("en-US");
        #region " GenerateAdditionalData "
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            DataSet DS = new DataSet("ADDITIONAL");
            DataTable oTable;
            INFOTYPE2003 oItem = new INFOTYPE2003();
            oItem.EmployeeID = Requestor.EmployeeID;
            oTable = oItem.ToADODataTable(false);
            oTable.TableName = "INFOTYPE2003";
            DS.Tables.Add(oTable);

            DataTable oInfo = new DataTable();
            DataColumn column;
            oInfo.TableName = "InfoTable";
            column = new DataColumn("ISAllDatePeriod", typeof(Boolean));
            oInfo.Columns.Add(column);
            column = new DataColumn("BeginDate", typeof(DateTime));
            oInfo.Columns.Add(column);
            column = new DataColumn("EndDate", typeof(DateTime));
            oInfo.Columns.Add(column);

            DataRow NewRow = oInfo.NewRow();
            NewRow["ISAllDatePeriod"] = false;
            NewRow["BeginDate"] = DateTime.MinValue;
            NewRow["EndDate"] = DateTime.MinValue;
            oInfo.Rows.Add(NewRow);
            DS.Tables.Add(oInfo);

            DataTable oInfoSubstitute = new DataTable();
            DataColumn column1;
            oInfoSubstitute.TableName = "InfoSubstitute";
            column1 = new DataColumn("SUBSTITUTE_APPROVERPOSITION", typeof(string));
            oInfoSubstitute.Columns.Add(column1);
            column1 = new DataColumn("SubstituteId", typeof(string));
            oInfoSubstitute.Columns.Add(column1);
            column1 = new DataColumn("SubstituteName", typeof(string));
            oInfoSubstitute.Columns.Add(column1);
            column1 = new DataColumn("PostionId", typeof(string));
            oInfoSubstitute.Columns.Add(column1);
            column1 = new DataColumn("PostionName", typeof(string));
            oInfoSubstitute.Columns.Add(column1);
            column1 = new DataColumn("OrganizationId", typeof(string));
            oInfoSubstitute.Columns.Add(column1);
            column1 = new DataColumn("OrganizationName", typeof(string));
            oInfoSubstitute.Columns.Add(column1);

            DataRow NewRow1 = oInfoSubstitute.NewRow();
            NewRow1["SUBSTITUTE_APPROVERPOSITION"] = "";
            NewRow1["SubstituteId"] = "";
            NewRow1["SubstituteName"] = "";
            NewRow1["PostionId"] = "";
            NewRow1["PostionName"] = "";
            NewRow1["OrganizationId"] = "";
            NewRow1["OrganizationName"] = "";
            oInfoSubstitute.Rows.Add(NewRow1);

            DS.Tables.Add(oInfoSubstitute);

            return DS;
        }
        #endregion

        #region " CalculateInfoData "
        public override void CalculateInfoData(EmployeeData Requestor, object ds, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());

            DataTable oInfoTable = Data.Tables["InfoTable"];
            bool ISAllDate = false;
            DateTime BeginDate = new DateTime();
            DateTime EndDate = new DateTime();

            if (oInfoTable != null && oInfoTable.Rows.Count > 0)
            {
                BeginDate = (DateTime)oInfoTable.Rows[0]["BeginDate"];
                EndDate = (DateTime)oInfoTable.Rows[0]["EndDate"];
                ISAllDate = (bool)oInfoTable.Rows[0]["ISAllDatePeriod"];
            }

            INFOTYPE2003 item = new INFOTYPE2003();
            item.ParseToObject(Data.Tables["INFOTYPE2003"]);
            DataRow dataRow;
            Info.Rows.Clear();
            dataRow = Info.NewRow();
            dataRow["SUBSTITUTEWITH"] = item.Substitute;
            dataRow["SUBSTITUTE_APPROVERPOSITION"] = Data.Tables["InfoSubstitute"].Rows[0]["SUBSTITUTE_APPROVERPOSITION"].ToString();

            if (ISAllDate)
                dataRow["DATE"] = string.Format("{0} => {1}", BeginDate.ToString("dd/MM/yyyy", oCL), EndDate.ToString("dd/MM/yyyy", oCL));
            else if (BeginDate == EndDate)
                dataRow["DATE"] = BeginDate.ToString("dd/MM/yyyy", oCL);
            else
            {
                dataRow["DATE"] = string.Format("{0}, {1}", BeginDate.ToString("dd/MM/yyyy", oCL), EndDate.ToString("dd/MM/yyyy", oCL));
            }

            //dataRow["SUBSTITUTE_APPROVERPOSITION"]


            Info.Rows.Add(dataRow);
        }
        #endregion


        #region " ValidateData "
        public override void ValidateData(object ds, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            DataSet newData = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            if (newData == null || !newData.Tables.Contains("INFOTYPE2003") || newData.Tables["INFOTYPE2003"].Rows.Count == 0)
            {
                throw new DataServiceException("SUBSTITUTION", "NO_DATA");
            }
            if (newData.Tables.Contains("POSTDATA"))
            {
                newData.Tables.Remove("POSTDATA");
            }
            INFOTYPE2003 item = new INFOTYPE2003();
            try
            {
                foreach (DataRow dr in newData.Tables["INFOTYPE2003"].Rows)
                {
                    item = new INFOTYPE2003();
                    item.ParseToObject(dr);
                    item.Validate();

                    //Check dupplicate existing
                    List<Substitution> substitutionList = new List<Substitution>();
                    substitutionList.AddRange(HRTMManagement.CreateInstance(Requestor.CompanyCode).GetSubstitutionList(Requestor.EmployeeID, item.BeginDate, item.EndDate, true, RequestNo));


                    List<Substitution> dupList = substitutionList.FindAll(delegate (Substitution oSubstitution) { return oSubstitution.Status != "CANCELLED"; });
                    if (dupList != null && dupList.Count > 0)
                        throw new DataServiceException("HRTM_EXCEPTION", "DUPPLICATE_EXISTING");
                }
            }
            catch (Exception e)
            {
                throw new DataServiceException("HRTM_EXCEPTION", e.Message, "", e);
            }
            try
            {
                foreach (DataRow dr in newData.Tables["INFOTYPE2003"].Rows)
                {
                    item = new INFOTYPE2003();
                    item.ParseToObject(dr);
                    HRTMManagement.CreateInstance(Requestor.CompanyCode).MarkTimesheet(item.EmployeeID, "SUBSTITUTION", true, item.BeginDate, item.EndDate, true, true, RequestNo);
                    HRTMManagement.CreateInstance(Requestor.CompanyCode).MarkTimesheet(item.Substitute, "SUBSTITUTION", true, item.BeginDate, item.EndDate, true, true, RequestNo);
                }
            }
            catch (TimesheetCollisionException e)
            {
                throw new DataServiceException("HRTM_EXCEPTION", "COLLISION_OCCUR", "", e);
            }
            catch (Exception e)
            {
                throw new DataServiceException("HRTM_EXCEPTION", "COLLISION_OCCUR", "", e);
            }
        }
        #endregion

        #region " SaveExternalData "
        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object ds, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            DataTable oAdditional = Data.Tables["INFOTYPE2003"];
            List<INFOTYPE2003> INFOTYPE2003List = new List<INFOTYPE2003>();
            INFOTYPE2003 oINFOTYPE2003 = new INFOTYPE2003();
            List<INFOTYPE2003> postingList = new List<INFOTYPE2003>();

            List<string> draftList = new List<string>();
            draftList.Add("DRAFT");

            List<string> unmarkList = new List<string>();
            unmarkList.Add("CANCELLED");

            List<string> postList = new List<string>();
            postList.Add("COMPLETED");

            foreach (DataRow row in oAdditional.Rows)
            {
                INFOTYPE2003 sub;
                INFOTYPE2003 inverse;

                row["IsMark"] = !unmarkList.Contains(State);
                row["RequestNo"] = RequestNo;
                row["IsDraft"] = draftList.Contains(State);
                sub = new INFOTYPE2003();
                sub.ParseToObject(row);

                row["Status"] = State.ToUpper();

                if (postList.Contains(State))
                {
                    #region " POST "
                    // POST DATA
                    EmployeeData oEmp;
                    oINFOTYPE2003 = new INFOTYPE2003();
                    oINFOTYPE2003.ParseToObject(row);
                    INFOTYPE2003List.Add(oINFOTYPE2003);

                    row["IsOverride"] = sub.IsOverride;
                    sub.IsMark = !unmarkList.Contains(State);

                    DailyWS oDWS;
                    oEmp = new EmployeeData(sub.IsOverride ? sub.EmployeeID : sub.Substitute, sub.BeginDate);
                    oDWS = oEmp.GetDailyWorkSchedule(sub.EmpDWSCodeNew, sub.BeginDate);
                    sub.DWSGroup = oDWS.DailyWorkscheduleGrouping;
                    sub.DWSCode = oDWS.DailyWorkscheduleCode;
                    postingList.Add(sub);

                    inverse = new INFOTYPE2003();
                    inverse.BeginDate = sub.BeginDate;
                    inverse.EndDate = sub.EndDate;
                    inverse.EmployeeID = sub.Substitute;
                    inverse.Substitute = sub.EmployeeID;
                    inverse.IsMark = sub.IsMark;
                    oEmp = new EmployeeData(inverse.Substitute, inverse.BeginDate);
                    oDWS = oEmp.GetDailyWorkSchedule(sub.SubDWSCodeNew, sub.BeginDate);
                    inverse.DWSGroup = oDWS.DailyWorkscheduleGrouping;
                    inverse.DWSCode = oDWS.DailyWorkscheduleCode;
                    postingList.Add(inverse);
                    #endregion
                }
                else if (unmarkList.Contains(State))
                {
                    //sub.IsMark = false;

                    //Edit by Koissares 20161025
                    oINFOTYPE2003 = new INFOTYPE2003();
                    oINFOTYPE2003.ParseToObject(row);
                    INFOTYPE2003List.Add(oINFOTYPE2003);
                }
                else
                {
                    oINFOTYPE2003 = new INFOTYPE2003();
                    oINFOTYPE2003.ParseToObject(row);
                    INFOTYPE2003List.Add(oINFOTYPE2003);
                }
            }

            /////////////////////////
            if (postList.Contains(State))
            {
                #region " POST "
                try
                {
                    try
                    {
                        HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveSubstitution(postingList);
                        HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveSubstitutionLog(INFOTYPE2003List);
                    }
                    catch (Exception ex1)
                    {
                        throw new SaveExternalDataException("SaveExternalData Error", true, ex1);
                    }
                    finally
                    {
                        if (Data.Tables.Contains("POSTDATA"))
                            Data.Tables.Remove("POSTDATA");

                        INFOTYPE2003 item1 = new INFOTYPE2003();
                        DataTable oTempTable;
                        oTempTable = item1.ToADODataTable(true);
                        oTempTable.TableName = "POSTDATA";
                        Data.Tables.Add(oTempTable);
                        foreach (INFOTYPE2003 item in INFOTYPE2003List)
                        {
                            DataRow oNewRow = oTempTable.NewRow();
                            item.LoadDataToTableRow(oNewRow);
                            oTempTable.LoadDataRow(oNewRow.ItemArray, false);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new SaveExternalDataException("SaveExternalData Error", true, ex);
                }

                try
                {
                    foreach (INFOTYPE2003 inf2003 in postingList)
                    {
                        HRTMManagement.CreateInstance(Requestor.CompanyCode).UpdateTimePair(inf2003.EmployeeID, inf2003.BeginDate, inf2003.EndDate);
                        List<ClockinLateClockoutEarly> ClockinLateClockoutEarlyList_emp = HRTMManagement.CreateInstance(Requestor.CompanyCode).CalculateTM_InLateOutEarly(inf2003.EmployeeID, inf2003.BeginDate, inf2003.EndDate);
                        if (ClockinLateClockoutEarlyList_emp.Count > 0)
                        {
                            HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveInLateOutEarly(ClockinLateClockoutEarlyList_emp, inf2003.EmployeeID, inf2003.BeginDate, inf2003.EndDate.AddDays(1).AddSeconds(-1));
                            HRTMManagement.CreateInstance(Requestor.CompanyCode).MappingTimePairInLateOutEarlyID(inf2003.EmployeeID, inf2003.BeginDate, inf2003.EndDate);
                        }

                        HRTMManagement.CreateInstance(Requestor.CompanyCode).UpdateTimePair(inf2003.Substitute, inf2003.BeginDate, inf2003.EndDate);
                        List<ClockinLateClockoutEarly> ClockinLateClockoutEarlyList_sub = HRTMManagement.CreateInstance(Requestor.CompanyCode).CalculateTM_InLateOutEarly(inf2003.Substitute, inf2003.BeginDate, inf2003.EndDate);
                        if (ClockinLateClockoutEarlyList_sub.Count > 0)
                        {
                            HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveInLateOutEarly(ClockinLateClockoutEarlyList_sub, inf2003.Substitute, inf2003.BeginDate, inf2003.EndDate.AddDays(1).AddSeconds(-1));
                            HRTMManagement.CreateInstance(Requestor.CompanyCode).MappingTimePairInLateOutEarlyID(inf2003.Substitute, inf2003.BeginDate, inf2003.EndDate);
                        }
                    }
                }
                catch (Exception ex) { }
                #endregion
            }
            else if (unmarkList.Contains(State))
            {
                HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveSubstitutionLog(INFOTYPE2003List);
            }
            else
            {
                HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveSubstitutionLog(INFOTYPE2003List);
            }

        }
        #endregion

        public INFOTYPE2003 Substitute(string EmployeeID1, string EmployeeID2, DateTime SubDate, string RequestNo, bool IsMark, bool IsDraft)
        {
            INFOTYPE2003 returnValue = new INFOTYPE2003();
            returnValue.EmployeeID = EmployeeID1;
            returnValue.Substitute = EmployeeID2;
            returnValue.RequestNo = RequestNo;

            returnValue.IsMark = IsMark;
            returnValue.IsDraft = IsDraft;
            DailyWS oDWS;
            EmployeeData oEmp = new EmployeeData(EmployeeID2, SubDate);
            oDWS = oEmp.GetDailyWorkSchedule(SubDate, true);
            returnValue.DWSGroup = oDWS.DailyWorkscheduleGrouping;
            returnValue.DWSCode = oDWS.DailyWorkscheduleCode;
            if (oDWS.IsDayOffOrHoliday)
            {
                returnValue.BeginDate = SubDate;
                returnValue.EndDate = SubDate.AddDays(1);
            }
            else
            {
                returnValue.BeginDate = SubDate.Add(oDWS.WorkBeginTime);
                returnValue.EndDate = SubDate.Add(oDWS.WorkEndTime);
            }

            return returnValue;
        }




        #region " PrepareData "
        public override void PrepareData(EmployeeData Requestor, object ds)
        {

        }
        #endregion

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info)
        {
            string empSubWITH = (string)Info.Rows[0]["SUBSTITUTEWITH"];
            EmployeeData oEmpSubWITH = new EmployeeData(empSubWITH, DateTime.Now);

            if (Requestor.OrgAssignment.OrgUnit == oEmpSubWITH.OrgAssignment.OrgUnit)
                return "SO";
            else
                return "DO";
        }
    }
}
