using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.HR.TM.DATACLASS
{
    public class BaseOTDataComparer : IComparer<BaseOTData>
    {
        #region IComparer<BaseOTData> Members

        public int Compare(BaseOTData x, BaseOTData y)
        {
            int nResult;
            nResult = x.OTDate.CompareTo(y.OTDate);
            if (nResult == 0)
            {
                nResult = x.BeginTime.CompareTo(y.BeginTime);
                if (nResult == 0)
                {
                    return x.EndTime.CompareTo(y.EndTime);
                }
                else
                {
                    return nResult;
                }
            }
            else
            {
                return nResult;
            }
        }

        #endregion
    }
}
