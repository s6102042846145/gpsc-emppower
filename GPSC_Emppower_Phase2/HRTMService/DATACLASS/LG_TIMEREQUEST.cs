﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.TM.DATACLASS
{
    public class LG_TIMEREQUEST : AbstractObject
    {
        private string employeeID = "";
        private string absAttType = "";
        private DateTime beginDate = DateTime.MinValue;
        private DateTime endDate = DateTime.MinValue;
        private TimeSpan beginTime = TimeSpan.Zero;
        private TimeSpan endTime = TimeSpan.Zero;
        private decimal absAttDays;
        private decimal absAttHours;
        private decimal payrollDays;
        private bool allDayFlag = false;
        private string remark = "";
        private string requestNo = "";
        private string categoryCode = "";
        private DateTime createdDate = DateTime.MinValue;
        private DateTime updatedDate = DateTime.MinValue;
        private int updatedStatus = 0;
        private string updatedDesc = "";


       
        #region Properties 
        public string EmployeeID
        {
            get
            {
                return employeeID;
            }
            set
            {
                employeeID = value;
            }
        }
        public string AbsAttType
        {
            get
            {
                return absAttType;
            }
            set
            {
                absAttType = value;
            }
        }

        public DateTime BeginDate
        {
            get
            {
                return beginDate;
            }
            set
            {
                beginDate = value;
            }
        }
        public DateTime EndDate
        {
            get
            {
                return endDate;
            }
            set
            {
                endDate = value;
            }
        }

        public TimeSpan BeginTime
        {
            get
            {
                return beginTime;
            }
            set
            {
                beginTime = value;
            }
        }

        public TimeSpan EndTime
        {
            get
            {
                return endTime;
            }
            set
            {
                endTime = value;
            }
        }

        public decimal AbsAttDays
        {
            get
            {
                return absAttDays;
            }
            set
            {
                absAttDays = value;
            }
        }

        public decimal AbsAttHours
        {
            get
            {
                return absAttHours;
            }
            set
            {
                absAttHours = value;
            }
        }
        public decimal PayrollDays
        {
            get
            {
                if (payrollDays == 0.0M && this.AllDayFlag)
                {
                    return 0.0M;
                }
                return payrollDays;
            }
            set
            {
                payrollDays = value;
            }
        }

        public bool AllDayFlag
        {
            get
            {
                return allDayFlag;
            }
            set
            {
                allDayFlag = value;
            }
        }

        public string Remark
        {
            get
            {
                return remark;
            }
            set
            {
                remark = value;
            }
        }

        public string RequestNo
        {
            get
            {
                return requestNo;
            }
            set
            {
                requestNo = value;
            }
        }

        public string CategoryCode
        {
            get
            {
                return categoryCode;
            }
            set
            {
                categoryCode = value;
            }
        }

        public DateTime CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                createdDate = value;
            }
        }
        public DateTime UpdatedDate
        {
            get
            {
                return updatedDate;
            }
            set
            {
                updatedDate = value;
            }
        }

        public int UpdatedStatus
        {
            get
            {
                return updatedStatus;
            }
            set
            {
                updatedStatus = value;
            }
        }

        public string UpdatedDesc
        {
            get
            {
                return updatedDesc;
            }
            set
            {
                updatedDesc = value;
            }
        }
        #endregion
    }
}
