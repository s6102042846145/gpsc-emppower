﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.TM.DATACLASS
{
    public class TM_Timesheet : AbstractObject
    {
        public TM_Timesheet()
        { }

        private string __employeeID = string.Empty;
        private DateTime __date = DateTime.MinValue;
        private DateTime __clockIn = DateTime.MinValue;
        private DateTime __clockOut = DateTime.MinValue;
        private string __type = string.Empty;
        private bool __clockInLate = false;
        private bool __clockoutEarly = false;
        private bool __clockAbnormal = false;
        private bool __clockNoneWork = false;
        private bool __invalidClockInOut = false;
        private bool __isRemark = false;

        private string __absAttType = string.Empty;
        private string __textDescription = string.Empty;
        private DateTime __beginDate = DateTime.MinValue;
        private DateTime __endDate = DateTime.MinValue;
        private TimeSpan __beginTime = TimeSpan.Zero;
        private TimeSpan __endTime = TimeSpan.Zero;
        private decimal __days;
        private decimal __hours;
        private decimal __payrollDays;
        private bool __allDayFlag = true;
        private int __timePairID;

        public string EmployeeID
        {
            get { return __employeeID; }
            set { __employeeID = value; }
        }
        public DateTime Date
        {
            get { return __date; }
            set { __date = value; }
        }
        public DateTime ClockIn
        {
            get { return __clockIn; }
            set { __clockIn = value; }
        }
        public DateTime ClockOut
        {
            get { return __clockOut; }
            set { __clockOut = value; }
        }
        public string Type
        {
            get { return __type; }
            set { __type = value; }
        }
        public bool ClockInLate
        {
            get { return __clockInLate; }
            set { __clockInLate = value; }
        }
        public bool ClockoutEarly
        {
            get { return __clockoutEarly; }
            set { __clockoutEarly = value; }
        }
        public bool ClockAbnormal
        {
            get { return __clockAbnormal; }
            set { __clockAbnormal = value; }
        }
        public bool ClockNoneWork
        {
            get { return __clockNoneWork; }
            set { __clockNoneWork = value; }
        }
        public bool InvalidClockInOut
        {
            get { return ClockAbnormal && !(ClockIn == DateTime.MinValue && ClockOut == DateTime.MinValue); }
        }
        public bool IsRemark
        {
            get { return __isRemark; }
            set { __isRemark = value; }
        }
        public string AbsAttType
        {
            get { return __absAttType; }
            set { __absAttType = value; }
        }
        public string TextDescription
        {
            get { return __textDescription; }
            set { __textDescription = value; }
        }
        public DateTime BeginDate
        {
            get { return __beginDate; }
            set { __beginDate = value; }
        }
        public DateTime EndDate
        {
            get { return __endDate; }
            set { __endDate = value; }
        }
        public TimeSpan BeginTime
        {
            get { return __beginTime; }
            set { __beginTime = value; }
        }
        public TimeSpan EndTime
        {
            get { return __endTime; }
            set { __endTime = value; }
        }
        public decimal Days
        {
            get { return __days; }
            set { __days = value; }
        }
        public decimal Hours
        {
            get { return __hours; }
            set { __hours = value; }
        }
        public decimal PayrollDays
        {
            get { return __payrollDays; }
            set { __payrollDays = value; }
        }
        public bool AllDayFlag
        {
            get { return __allDayFlag; }
            set { __allDayFlag = value; }
        }
        public int TimePairID
        {
            get { return __timePairID; }
            set { __timePairID = value; }
        }
    }
}
