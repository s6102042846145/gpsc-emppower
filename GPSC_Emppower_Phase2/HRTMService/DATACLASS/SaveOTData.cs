using System;
using System.Collections.Generic;
using System.Text;
using ESS.DATA;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.TM.DATACLASS
{
    public class SaveOTData : BaseOTData
    {
        private string __assignFrom = "";
        private string __requestNo = "";
        private string __remark = "";
        private string __OTPeriod = "";
        private bool __isCompleted = false;
        private bool __isSummary = false;
        private bool __isPrevDay = false;
        private bool __isPost = false;
        private string __OTType = "";
        private int __OTItemTypeID = 0;
        private string __ioNumber = "";
        private string __activityCode = "";

        public SaveOTData()
        { 
        }

        public bool DoNotPayShiftAllowance
        {
            get
            {
                return this.OTItemTypeID == 0;
            }
        }

        public int OTItemTypeID
        {
            get
            {
                return __OTItemTypeID;
            }
            set
            {
                __OTItemTypeID = value;
            }
        }

        public bool IsSummary
        {
            get
            {
                return __isSummary;
            }
            set
            {
                __isSummary = value;
            }
        }

        public bool IsCompleted
        {
            get
            {
                return __isCompleted;
            }
            set
            {
                __isCompleted = value;
            }
        }

        public string OTType
        {
            get
            {
                return __OTType;
            }
            set
            {
                __OTType = value;
            }
        }

        public string AssignFrom
        {
            get
            {
                return __assignFrom;
            }
            set
            {
                __assignFrom = value;
            }
        }

        public string RequestNo
        {
            get
            {
                return __requestNo;
            }
            set
            {
                __requestNo = value;
            }
        }
        public string OTPeriod
        {
            get
            {
                return __OTPeriod;
            }
            set
            {
                __OTPeriod = value;
            }
        }
        public bool IsPrevDay
        {
            get
            {
                return __isPrevDay;
            }
            set
            {
                __isPrevDay = value;
            }
        }
        public bool IsPost
        {
            get
            {
                return __isPost;
            }
            set
            {
                __isPost = value;
            }
        }
        public string Remark
        {
            get
            {
                return __remark;
            }
            set
            {
                __remark = value;
            }
        }

        public string IONumber
        {
            get
            {
                return __ioNumber;
            }
            set
            {
                __ioNumber = value;
            }
        }

        public string ActivityCode
        {
            get
            {
                return __activityCode;
            }
            set
            {
                __activityCode = value;
            }
        }
    }
}
