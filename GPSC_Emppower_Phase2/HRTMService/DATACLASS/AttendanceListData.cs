﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.TM.DATACLASS
{
    [Serializable]
    public class AttendanceListData : AbstractObject
    {
        private DateTime beginDate = DateTime.MinValue;
        private DateTime endDate = DateTime.MinValue;
        private TimeSpan beginTime = TimeSpan.Zero;
        private TimeSpan endTime = TimeSpan.Zero;
        private decimal absenceDays;
        private decimal absenceHours;
        private decimal payrollDays;
        private bool allDayFlag = false;
        private string status = "";
        private string remark = "";
        private bool isMark = true;
        private bool isPost = false;
        private bool isPrevDay = false;
        private string requestNo = "";
        private string IOnumber = "";
        private string activityCode = "";
        public string attendancetype;
        public string attendanceConcat;


        #region Properties 
        public string AttendanceConcat
        {
            get
            {
                return attendanceConcat;
            }
            set
            {
                attendanceConcat = value;
            }
        }
        public string RequestNo
        {
            get
            {
                return requestNo;
            }
            set
            {
                requestNo = value;
            }
        }

        public string AttendanceType
        {
            get
            {
                return attendancetype;
            }
            set
            {
                attendancetype = value;
            }
        }

        public DateTime BeginDate
        {
            get
            {
                return beginDate;
            }
            set
            {
                beginDate = value;
            }
        }
        public DateTime EndDate
        {
            get
            {
                return endDate;
            }
            set
            {
                endDate = value;
            }
        }

        public TimeSpan BeginTime
        {
            get
            {
                return beginTime;
            }
            set
            {
                beginTime = value;
            }
        }

        public TimeSpan EndTime
        {
            get
            {
                return endTime;
            }
            set
            {
                endTime = value;
            }
        }

        public decimal AttendanceDays
        {
            get
            {
                return absenceDays;
            }
            set
            {
                absenceDays = value;
            }
        }

        public decimal AttendanceHours
        {
            get
            {
                return absenceHours;
            }
            set
            {
                absenceHours = value;
            }
        }
        public decimal PayrollDays
        {
            get
            {
                if (payrollDays == 0.0M && this.AllDayFlag)
                {
                    return 0.0M;
                }
                return payrollDays;
            }
            set
            {
                payrollDays = value;
            }
        }
        public bool AllDayFlag
        {
            get
            {
                return allDayFlag;
            }
            set
            {
                allDayFlag = value;
            }
        }

        public string Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }

        public string Remark
        {
            get
            {
                return remark;
            }
            set
            {
                remark = value;
            }
        }


        public bool IsMark
        {
            get
            {
                return isMark;
            }
            set
            {
                isMark = value;
            }
        }

        public bool IsPost
        {
            get
            {
                return isPost;
            }
            set
            {
                isPost = value;
            }
        }

        public bool IsPrevDay
        {
            get
            {
                return isPrevDay;
            }
            set
            {
                isPrevDay = value;
            }
        }

        public string IONumber
        {
            get
            {
                return IOnumber;
            }
            set
            {
                IOnumber = value;
            }
        }

        public string ActivityCode
        {
            get
            {
                return activityCode;
            }
            set
            {
                activityCode = value;
            }
        }
        #endregion
    }
}
