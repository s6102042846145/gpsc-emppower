﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.TM.DATACLASS
{
    public class SubstitutionData
    {
        //public static string ModuleID { get; }
        public string DWSGroup { get; set; }
        public string DWSCode { get; set; }
        public string EmpSubGroupSetting { get; set; }
        public string HolidayCalendar { get; set; }
        public string EmpSubAreaSetting { get; set; }
        public string Status { get; set; }
        public string WSRule { get; set; }
        public string Substitute { get; set; }
        public string RequestNo { get; set; }
        public string WorkingTimeBefore { get; set; }
        public string WorkingTimeAfter { get; set; }
        public string DWSCodeView { get; }
        public string NameSubstitute { get; set; }
        public string WorkingEmpDWSCodeOld { get; set; }
        public string WorkingEmpDWSCodeNew { get; set; }
        public TimeSpan SubstituteEndTime { get; set; }
        public TimeSpan SubstituteBeginTime { get; set; }
        public DateTime SubstituteEndDate { get; set; }
        public DateTime SubstituteBeginDate { get; set; }
        public string CompanyCode { get; set; }
        public string WorkingSubDWSCodeOld { get; set; }
        public string SubDWSCodeOld { get; set; }
        public string SubDWSCodeNew { get; set; }
        public string EmpDWSCodeNew { get; set; }
        public bool IsDraft { get; set; }
        public bool IsOverride { get; set; }
        public string EmployeeID { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public string EmpDWSCodeOld { get; set; }
        public string WorkingSubDWSCodeNew { get; set; }
        public string NameEmoloyee { get; set; }
    }
}
