using System;
using System.Collections.Generic;
using System.Text;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.TM.DATACLASS
{
    public class DailyFlexTime : AbstractObject
    {

        private string __flexType;
        private string __beginTime;
        private string __endTime;
        private string __breakCode;

        public string FlexType
        {
            get { return __flexType; }
            set { __flexType = value; }
        }
        public string BeginTime
        {
            get { return __beginTime; }
            set { __beginTime = value; }
        }
        public string EndTime
        {
            get { return __endTime; }
            set { __endTime = value; }
        }
        public string BreakCode
        {
            get { return __breakCode; }
            set { __breakCode = value; }
        }
    }
}
