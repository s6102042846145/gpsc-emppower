using System;
using System.Collections.Generic;
using System.Text;
using ESS.DATA;

namespace ESS.HR.TM.DATACLASS
{
    [Serializable]
    public abstract class BaseOTLog :AbstractObject
    {
        // Fields...
        private List<OTClock> _OTClockINOUT;
        private string _RefRequestNo;
        private string _Description;
        private string _RequestNo;
        private bool _IsPayroll;
        private decimal _FinalOTHour30;
        private decimal _FinalOTHour15;
        private decimal _FinalOTHour10;
        private decimal _EvaOTHour30;
        private decimal _EvaOTHour15;
        private decimal _EvaOTHour10;
        private byte _Status;
        private decimal _RequestOTHour30;
        private decimal _RequestOTHour15;
        private decimal _RequestOTHour10;
        private DateTime _EndDate;
        private DateTime _BeginDate;
        private string _EmployeeID;

        #region "Public Properties"

        public List<OTClock> OTClockINOUT
        {
            get { return _OTClockINOUT; }
            set
            {
                _OTClockINOUT = value;
            }
        }
        
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set
            {
                _EmployeeID = value;
            }
        }

        public DateTime BeginDate
        {
            get { return _BeginDate; }
            set
            {
                _BeginDate = value;
            }
        }

        public DateTime EndDate
        {
            get { return _EndDate; }
            set
            {
                _EndDate = value;
            }
        }

        public decimal RequestOTHour10
        {
            get { return _RequestOTHour10; }
            set
            {
                _RequestOTHour10 = value;
            }
        }

        public decimal RequestOTHour15
        {
            get { return _RequestOTHour15; }
            set
            {
                _RequestOTHour15 = value;
            }
        }

        public decimal RequestOTHour30
        {
            get { return _RequestOTHour30; }
            set
            {
                _RequestOTHour30 = value;
            }
        }

        public byte Status
        {
            get { return _Status; }
            set
            {
                _Status = value;
            }
        }

        public decimal EvaOTHour10
        {
            get { return _EvaOTHour10; }
            set
            {
                _EvaOTHour10 = value;
            }
        }

        public decimal EvaOTHour15
        {
            get { return _EvaOTHour15; }
            set
            {
                _EvaOTHour15 = value;
            }
        }

        public decimal EvaOTHour30
        {
            get { return _EvaOTHour30; }
            set
            {
                _EvaOTHour30 = value;
            }
        }

        public decimal FinalOTHour10
        {
            get { return _FinalOTHour10; }
            set
            {
                _FinalOTHour10 = value;
            }
        }

        public decimal FinalOTHour15
        {
            get { return _FinalOTHour15; }
            set
            {
                _FinalOTHour15 = value;
            }
        }

        public decimal FinalOTHour30
        {
            get { return _FinalOTHour30; }
            set
            {
                _FinalOTHour30 = value;
            }
        }

        public bool IsPayroll
        {
            get { return _IsPayroll; }
            set
            {
                _IsPayroll = value;
            }
        }

        public string RequestNo
        {
            get { return _RequestNo; }
            set
            {
                _RequestNo = value;
            }
        }

        public string Description
        {
            get { return _Description; }
            set
            {
                _Description = value;
            }
        }

        public string RefRequestNo
        {
            get { return _RefRequestNo; }
            set
            {
                _RefRequestNo = value;
            }
        }
        
        
        #endregion

        public BaseOTLog()
            : base()
        { }
    }
}
