using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Reflection;
using System.Text;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.TIMESHEET;
using ESS.HR.TM.INFOTYPE;

using System.Data;

namespace ESS.HR.TM.DATACLASS
{
    public class OTManagement
    {
        private static Configuration __config;

        private OTManagement()
        { 
        }

        #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        private static Dictionary<string, OTManagement> Cache = new Dictionary<string, OTManagement>();

        private static string ModuleID = "ESS.HR.TM";
        public string CompanyCode { get; set; }

        public static OTManagement CreateInstance(string oCompanyCode)
        {
            OTManagement oOTManagement = new OTManagement()
            {
                CompanyCode = oCompanyCode
            };
            return oOTManagement;
        }
        #endregion MultiCompany  Framework

        private static Configuration config
        {
            get
            {
                if (__config == null)
                {
                    __config = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().CodeBase.Replace("file:///", "").Replace("/", "\\"));
                }
                return __config;
            }
        }

        public static TimeSpan NormalBeginTime_NORM
        {
            get
            {
                return new TimeSpan(8, 0, 0);
            }
        }

        public static TimeSpan NormalEndTime_NORM
        {
            get
            {
                return new TimeSpan(17, 0, 0);
            }
        }

        public static TimeSpan NormalBeginTime_SHIFT
        {
            get
            {
                return new TimeSpan(7, 30, 0);
            }
        }

        public static TimeSpan NormalEndTime_SHIFT
        {
            get
            {
                return new TimeSpan(19, 30, 0);
            }
        }

        public static DateTime FlexLate
        {
            get
            {
                DateTime nValue = DateTime.Today;
                
                return nValue.Add(new TimeSpan(9, 0, 0));
            }
        }

        public static int DailyOTOffsetBefore
        {
            get
            {
                if (config == null || config.AppSettings.Settings["DAILYOTOFFSETBEFORE"] == null)
                {
                    return 0;
                }
                else
                {
                    int nValue;
                    if (int.TryParse(config.AppSettings.Settings["DAILYOTOFFSETBEFORE"].Value, out nValue))
                    {
                        return nValue;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }
        public static int DailyOTOffsetAfter
        {
            get
            {
                if (config == null || config.AppSettings.Settings["DAILYOTOFFSETAFTER"] == null)
                {
                    return 0;
                }
                else
                {
                    int nValue;
                    if (int.TryParse(config.AppSettings.Settings["DAILYOTOFFSETAFTER"].Value, out nValue))
                    {
                        return nValue;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }

        #region TimeSheetViewer Dropdown Set

        public static int TimeSheetSetMonth
        {
            get
            {
                if (config == null || config.AppSettings.Settings["TIMESHEETSET"] == null)
                {
                    return 0;
                }
                else
                {
                    int nValue;
                    if (int.TryParse(config.AppSettings.Settings["TIMESHEETSET"].Value, out nValue))
                    {
                        return nValue;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }
        #endregion

        public static int MonthlyOTOffsetBefore
        {
            get
            {
                #region "OldVersion"
                if (config == null || config.AppSettings.Settings["MONTHLYOTOFFSETBEFORE"] == null)
                {
                    return 0;
                }
                else
                {
                    int nValue;
                    if (int.TryParse(config.AppSettings.Settings["MONTHLYOTOFFSETBEFORE"].Value, out nValue))
                    {
                        return nValue;
                    }
                    else
                    {
                        return 0;
                    }
                }
                #endregion
            }
        }
        public static int MonthlyOTOffsetAfter
        {
            get
            {
                if (config == null || config.AppSettings.Settings["MONTHLYOTOFFSETAFTER"] == null)
                {
                    return 0;
                }
                else
                {
                    int nValue;
                    if (int.TryParse(config.AppSettings.Settings["MONTHLYOTOFFSETAFTER"].Value, out nValue))
                    {
                        return nValue;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }

        //AddBy: Ratchatawan W. (2012-02-28)
        public static decimal SUMMARYOTREPORT_MANAGER_EMPSUBGROUP_LEVEL_BEGIN
        {
            get
            {
                if (config == null || config.AppSettings.Settings["SUMMARYOTREPORT_MANAGER_EMPSUBGROUP_LEVEL_BEGIN"] == null)
                    return 0;
                else
                {
                    decimal nValue = 0;
                    decimal.TryParse(config.AppSettings.Settings["SUMMARYOTREPORT_MANAGER_EMPSUBGROUP_LEVEL_BEGIN"].Value, out nValue);
                    return nValue;
                }
            }
        }

        public static int PlannedOTOffsetBefore
        {
            get
            {
                if (config == null || config.AppSettings.Settings["PLANNEDOTOFFSETBEFORE"] == null)
                {
                    return 0;
                }
                else
                {
                    int nValue;
                    if (int.TryParse(config.AppSettings.Settings["PLANNEDOTOFFSETBEFORE"].Value, out nValue))
                    {
                        return nValue;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }
        public static int PlannedOTOffsetAfter
        {
            get
            {
                #region "OldVersion"
                if (config == null || config.AppSettings.Settings["PLANNEDOTOFFSETAFTER"] == null)
                {
                    return 0;
                }
                else
                {
                    int nValue;
                    if (int.TryParse(config.AppSettings.Settings["PLANNEDOTOFFSETAFTER"].Value, out nValue))
                    {
                        return nValue;
                    }
                    else
                    {
                        return 0;
                    }
                }
                #endregion
            }
        }

        public static int PlannedOTOffset
        {
            get
            {
                if (config == null || config.AppSettings.Settings["PLANNEDOTOFFSET"] == null)
                {
                    return 0;
                }
                else
                {
                    int nValue;
                    if (int.TryParse(config.AppSettings.Settings["PLANNEDOTOFFSET"].Value, out nValue))
                    {
                        return nValue;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }

        public static int BEGINEMPSUBGROUPDAILYOT
        {
            get
            {
                if (config == null || config.AppSettings.Settings["BEGINEMPSUBGROUPDAILYOT"] == null)
                {
                    return 0;
                }
                else
                {
                    int nValue;
                    if (int.TryParse(config.AppSettings.Settings["BEGINEMPSUBGROUPDAILYOT"].Value, out nValue))
                    {
                        return nValue;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }

        public static int ENDEMPSUBGROUPDAILYOT
        {
            get
            {
                if (config == null || config.AppSettings.Settings["ENDEMPSUBGROUPDAILYOT"] == null)
                {
                    return 0;
                }
                else
                {
                    int nValue;
                    if (int.TryParse(config.AppSettings.Settings["ENDEMPSUBGROUPDAILYOT"].Value, out nValue))
                    {
                        return nValue;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }

        public static int BEGINEMPSUBGROUPSUMMARYOT
        {
            get
            {
                if (config == null || config.AppSettings.Settings["BEGINEMPSUBGROUPSUMMARYOT"] == null)
                {
                    return 0;
                }
                else
                {
                    int nValue;
                    if (int.TryParse(config.AppSettings.Settings["BEGINEMPSUBGROUPSUMMARYOT"].Value, out nValue))
                    {
                        return nValue;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }

        public static int ENDEMPSUBGROUPSUMMARYOT
        {
            get
            {
                if (config == null || config.AppSettings.Settings["ENDEMPSUBGROUPSUMMARYOT"] == null)
                {
                    return 0;
                }
                else
                {
                    int nValue;
                    if (int.TryParse(config.AppSettings.Settings["ENDEMPSUBGROUPSUMMARYOT"].Value, out nValue))
                    {
                        return nValue;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }

        public static int VALIDATEPREVIOUSREQUESTDAILYOT
        {
            get
            {
                if (config == null || config.AppSettings.Settings["VALIDATEPREVIOUSREQUESTDAILYOT"] == null)
                {
                    return 0;
                }
                else
                {
                    int nValue;
                    if (int.TryParse(config.AppSettings.Settings["VALIDATEPREVIOUSREQUESTDAILYOT"].Value, out nValue))
                    {
                        return nValue;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }

        public static string SUMMARYOTLIMITREQUEST
        {
            get
            {
                if (config == null || config.AppSettings.Settings["SUMMARYOTLIMITREQUEST"] == null)
                {
                    throw new Exception("can't find configuration for SUMMARYOTLIMITREQUEST");
                }
                else
                {
                    return config.AppSettings.Settings["SUMMARYOTLIMITREQUEST"].Value;
                }
            }
        }

        public static string SPECIFIC_EMPSUBGROUP_FOR_UNIT
        {
            get
            {
                if (config == null || config.AppSettings.Settings["SPECIFIC_EMPSUBGROUP_FOR_UNIT"] == null)
                {
                    throw new Exception("can't find configuration for SPECIFIC_EMPSUBGROUP_FOR_UNIT");
                }
                else
                {
                    return config.AppSettings.Settings["SPECIFIC_EMPSUBGROUP_FOR_UNIT"].Value;
                }
            }
        }

        public static string AREA_OT_SHIFT_DELEGATETO_NORM_ONABSENCE
        {
            get
            {
                if (config == null || config.AppSettings.Settings["AREA_OT_SHIFT_DELEGATETO_NORM_ONABSENCE"] == null)
                {
                    throw new Exception("can't find configuration for AREA_OT_SHIFT_DELEGATETO_NORM_ONABSENCE");
                }
                else
                {
                    return config.AppSettings.Settings["AREA_OT_SHIFT_DELEGATETO_NORM_ONABSENCE"].Value;
                }
            }
        }

        //public static string FindOTPeriod(DateTime actionDate)
        //{
        //    DateTime myDate;
        //    myDate = new DateTime(actionDate.Year, actionDate.Month, 1);
        //    if (actionDate.Day >= 15)
        //    {
        //        myDate = myDate.AddMonths(1);
        //    }
        //    return myDate.ToString("MM/yyyy",new CultureInfo("en-US"));
        //}

        //public static List<DailyOT> LoadOTFromPM(string EmployeeID, string Period, int DayStart, int DayEnd)
        //{
        //    return LoadOTFromPM(EmployeeID, Period, DayStart, DayEnd);
        //}
        //public List<MonthlyOT> GenerateMonthlyOT(string EmployeeID, string Period, MonthlyWS MWS)
        //{
        //    AdjustDailyOT(EmployeeID, Period);
        //    CultureInfo oCL = new CultureInfo("en-US");
        //    List<MonthlyOT> oReturn = new List<MonthlyOT>();
        //    List<SaveOTData> otlist;
        //    DateTime oPeriod = DateTime.ParseExact(Period, "yyyyMM", oCL);
        //    //otlist = HR.TM.ServiceManager.HRTMBuffer.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", true, false);

        //    otlist = ServiceManager.CreateInstance(CompanyCode).ESSData.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", true, false);
        //    List<SaveOTData> buffer = new List<SaveOTData>();
        //    List<string> requestNoList = new List<string>();
        //    foreach (SaveOTData item in otlist)
        //    {
        //        if (item.OTPeriod != Period)
        //        {
        //            continue;
        //        }
        //        if (!requestNoList.Contains(item.RequestNo))
        //        {
        //            requestNoList.Add(item.RequestNo);
        //        }
        //        List<DailyOT> temp = new List<DailyOT>();
        //        DailyOT dailyOT = new DailyOT();
        //        dailyOT.BeginTime = item.BeginTime;
        //        dailyOT.EmployeeID = EmployeeID;
        //        dailyOT.EndTime = item.EndTime;
        //        dailyOT.OTDate = item.OTDate;
        //        dailyOT.IsPosted = false;
        //        dailyOT.Description = item.Description;
        //        dailyOT.OTItemTypeID = item.OTItemTypeID;
        //        dailyOT.OTHours = item.OTHours;
        //        dailyOT.IONumber = item.IONumber;
        //        dailyOT.ActivityCode = item.ActivityCode;
        //        temp.Add(dailyOT);
        //        foreach (DailyOT item1 in temp)
        //        {
        //            //if (item1.OTHours * 100 == 0.0M)
        //            //{
        //            //    continue;
        //            //}
        //            //SaveOTData save = new SaveOTData();
        //            //save.AssignFrom = item.AssignFrom;
        //            //save.BeginTime = item1.BeginTime;
        //            //save.Description = item.Description;
        //            //save.EmployeeID = item.EmployeeID;
        //            //save.EndTime = item1.EndTime;
        //            //save.IsCompleted = item.IsCompleted;
        //            //save.IsPost = item.IsPost;
        //            //save.IsPrevDay = item.IsPrevDay;
        //            //save.IsSummary = item.IsSummary;
        //            //save.OTDate = item.OTDate;
        //            //save.OTHours = item1.OTHours;
        //            //save.OTItemTypeID = item.OTItemTypeID;
        //            //save.OTPeriod = item.OTPeriod;
        //            //save.OTType = item.OTType;
        //            //save.Remark = item.Remark;
        //            //save.RequestNo = item.RequestNo;
        //            //buffer.Add(save);
        //            MonthlyOT monthlyOT = new MonthlyOT();
        //            monthlyOT.BeginTime = item1.BeginTime;
        //            monthlyOT.DailyOT = item1;
        //            monthlyOT.Description = "";
        //            monthlyOT.EndTime = item1.EndTime;
        //            monthlyOT.IsDeleted = false;
        //            monthlyOT.DailyAssignFrom = item.AssignFrom;
        //            monthlyOT.DailyRequestNo = item.RequestNo;
        //            oReturn.Add(monthlyOT);
        //        }
        //    }

        //    oReturn = CalculateMonthlyOT(oReturn, MWS);

        //    //HR.TM.ServiceManager.HRTMBuffer.SaveOTList(buffer, true, requestNoList, true);

        //    //foreach (SaveOTData item in buffer)
        //    //{
        //    //    DailyOT dailyOT = new DailyOT();
        //    //    dailyOT.BeginTime = item.BeginTime;
        //    //    dailyOT.EmployeeID = EmployeeID;
        //    //    dailyOT.EndTime = item.EndTime;
        //    //    dailyOT.OTDate = item.OTDate;
        //    //    dailyOT.IsPosted = false;
        //    //    dailyOT.Description = item.Description;
        //    //    dailyOT.OTItemTypeID = item.OTItemTypeID;
        //    //    dailyOT.Calculate(MWS);
        //    //    MonthlyOT monthlyOT = new MonthlyOT();
        //    //    monthlyOT.BeginTime = dailyOT.BeginTime;
        //    //    monthlyOT.DailyOT = dailyOT;
        //    //    monthlyOT.Description = "";
        //    //    monthlyOT.EndTime = dailyOT.EndTime;
        //    //    monthlyOT.IsDeleted = false;
        //    //    monthlyOT.DailyAssignFrom = item.AssignFrom;
        //    //    monthlyOT.DailyRequestNo = item.RequestNo;
        //    //    oReturn.Add(monthlyOT);
        //    //}
        //    return oReturn;
        //}

        //public void AdjustDailyOT(string EmployeeID, string Period)
        //{
        //    CultureInfo oCL = new CultureInfo("en-US");
        //    List<DailyOT> oReturn = new List<DailyOT>();
        //    List<SaveOTData> otlist;
        //    DateTime oPeriod = DateTime.ParseExact(Period, "yyyyMM", oCL);
        //    MonthlyWS MWS = MonthlyWS.GetCalendar(EmployeeID, oPeriod.Year, oPeriod.Month);
        //    //otlist = HR.TM.ServiceManager.HRTMBuffer.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", false, false);

        //    otlist = ServiceManager.CreateInstance(CompanyCode).ESSData.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", false, false);
        //    foreach (SaveOTData item in otlist)
        //    {
        //        if (item.OTDate.ToString("yyyyMM") != Period)
        //        {
        //            continue;
        //        }
        //        DailyOT dailyOT = new DailyOT();
        //        dailyOT.AssignFrom = item.AssignFrom;
        //        dailyOT.BeginTime = item.BeginTime;
        //        dailyOT.Description = item.Description;
        //        dailyOT.EmployeeID = EmployeeID;
        //        dailyOT.EndTime = item.EndTime;
        //        dailyOT.OTDate = item.OTDate;
        //        dailyOT.IsCompleted = item.IsCompleted;
        //        dailyOT.IsSummary = item.IsSummary;
        //        dailyOT.IsPosted = item.IsPost;
        //        dailyOT.OTItemTypeID = item.OTItemTypeID;
        //        dailyOT.OTHours = item.OTHours;
        //        dailyOT.RequestNo = item.RequestNo;
        //        dailyOT.IONumber = item.IONumber;
        //        dailyOT.ActivityCode = item.ActivityCode;
        //        oReturn.Add(dailyOT);
        //    }
        //    oReturn = CalculateDailyOT(oReturn, MWS);
        //    otlist = new List<SaveOTData>();
        //    foreach (DailyOT item in oReturn)
        //    {
        //        SaveOTData saveOT = new SaveOTData();
        //        saveOT.AssignFrom = item.AssignFrom;
        //        saveOT.BeginTime = item.BeginTime;
        //        saveOT.Description = item.Description;
        //        saveOT.EmployeeID = EmployeeID;
        //        saveOT.EndTime = item.EndTime;
        //        saveOT.OTDate = item.OTDate;
        //        saveOT.IsCompleted = item.IsCompleted;
        //        saveOT.IsSummary = item.IsSummary;
        //        saveOT.IsPost = item.IsPosted;
        //        saveOT.OTItemTypeID = item.OTItemTypeID;
        //        saveOT.OTHours = item.OTHours;
        //        saveOT.RequestNo = item.RequestNo;
        //        saveOT.OTType = "D";
        //        saveOT.OTPeriod = Period;
        //        saveOT.IONumber = item.IONumber;
        //        saveOT.ActivityCode = item.ActivityCode;
        //        otlist.Add(saveOT);
        //    }
        //    //HR.TM.ServiceManager.HRTMBuffer.SaveOTList(otlist, true, null, true);
        //    ServiceManager.CreateInstance(CompanyCode).ESSData.SaveOTList(otlist, true, null, true);
        //}

        //public List<DailyOT> GenerateApprovedMonthlyOT(string EmployeeID, string Period)
        //{
        //    CultureInfo oCL = new CultureInfo("en-US");
        //    List<DailyOT> oReturn = new List<DailyOT>();
        //    List<SaveOTData> otlist;
        //    DateTime oPeriod = DateTime.ParseExact(Period, "yyyyMM", oCL);
        //    MonthlyWS MWS = MonthlyWS.GetCalendar(EmployeeID, oPeriod.Year, oPeriod.Month);
        //    //otlist = HR.TM.ServiceManager.HRTMService.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1).AddDays(-1), "D", true, true);

        //    otlist = ServiceManager.CreateInstance(CompanyCode).ERPData.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1).AddDays(-1), "D", true, true);
        //    foreach (SaveOTData item in otlist)
        //    {
        //        if (item.OTDate.ToString("yyyyMM") != Period)
        //        {
        //            continue;
        //        }
        //        DailyOT dailyOT = new DailyOT();
        //        dailyOT.BeginTime = item.BeginTime;
        //        dailyOT.EmployeeID = EmployeeID;
        //        dailyOT.EndTime = item.EndTime;
        //        dailyOT.OTDate = item.OTDate;
        //        dailyOT.IsPosted = false;
        //        dailyOT.Description = item.Description;
        //        dailyOT.OTItemTypeID = item.OTItemTypeID;
        //        dailyOT.OTHours = item.OTHours;
        //        dailyOT.IONumber = item.IONumber;
        //        dailyOT.ActivityCode = item.ActivityCode;
        //        oReturn.Add(dailyOT);
        //    }
        //    oReturn = CalculateDailyOT(oReturn, MWS);
        //    return oReturn;
        //}

        //public List<DailyOT> GenerateApprovedMonthlyOT1(string EmployeeID, string Period)
        //{
        //    CultureInfo oCL = new CultureInfo("en-US");
        //    List<DailyOT> oReturn = new List<DailyOT>();
        //    List<SaveOTData> otlist;
        //    DateTime oPeriod = DateTime.ParseExact(Period, "yyyyMM", oCL);
        //    MonthlyWS MWS = MonthlyWS.GetCalendar(EmployeeID, oPeriod.Year, oPeriod.Month);
        //    //otlist = HR.TM.ServiceManager.HRTMService.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1).AddDays(-1), "D", true, true);

        //    otlist = ServiceManager.CreateInstance(CompanyCode).ERPData.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1).AddDays(-1), "D", true, true);
        //    foreach (SaveOTData item in otlist)
        //    {
        //        if (item.OTDate.ToString("yyyyMM") != Period)
        //        {
        //            continue;
        //        }
        //        DailyOT dailyOT = new DailyOT();
        //        dailyOT.BeginTime = item.BeginTime;
        //        dailyOT.EmployeeID = EmployeeID;
        //        dailyOT.EndTime = item.EndTime;
        //        dailyOT.OTDate = item.OTDate;
        //        dailyOT.IsPosted = false;
        //        dailyOT.Description = item.Description;
        //        dailyOT.OTItemTypeID = item.OTItemTypeID;
        //        dailyOT.OTHours = item.OTHours;
        //        dailyOT.IONumber = item.IONumber;
        //        dailyOT.ActivityCode = item.ActivityCode;
        //        dailyOT.FillWorkSchedule();
        //        oReturn.Add(dailyOT);
        //    }
        //    //oReturn = OTManagement.CalculateDailyOT(oReturn, MWS);
        //    return oReturn;
        //}

        //public List<MonthlyOT> GenerateMonthlyOT1(string EmployeeID, string Period, MonthlyWS MWS)
        //{
        //    CultureInfo oCL = new CultureInfo("en-US");
        //    List<MonthlyOT> oReturn = new List<MonthlyOT>();
        //    List<SaveOTData> otlist;
        //    DateTime oPeriod = DateTime.ParseExact(Period, "yyyyMM", oCL);
        //    //otlist = HR.TM.ServiceManager.HRTMBuffer.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", true, false);

        //    otlist = ServiceManager.CreateInstance(CompanyCode).ESSData.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", true, false);
        //    List<SaveOTData> buffer = new List<SaveOTData>();
        //    List<string> requestNoList = new List<string>();
        //    foreach (SaveOTData item in otlist)
        //    {
        //        if (item.OTPeriod != Period)
        //        {
        //            continue;
        //        }
        //        if (!requestNoList.Contains(item.RequestNo))
        //        {
        //            requestNoList.Add(item.RequestNo);
        //        }
        //        List<DailyOT> temp = new List<DailyOT>();
        //        DailyOT dailyOT = new DailyOT();
        //        dailyOT.BeginTime = item.BeginTime;
        //        dailyOT.EmployeeID = EmployeeID;
        //        dailyOT.EndTime = item.EndTime;
        //        dailyOT.OTDate = item.OTDate;
        //        dailyOT.IsPosted = false;
        //        dailyOT.Description = item.Description;
        //        dailyOT.OTItemTypeID = item.OTItemTypeID;
        //        dailyOT.OTHours = item.OTHours;
        //        dailyOT.IONumber = item.IONumber;
        //        dailyOT.ActivityCode = item.ActivityCode;
        //        dailyOT.FillWorkSchedule();

        //        MonthlyOT monthlyOT = new MonthlyOT();
        //        monthlyOT.BeginTime = item.BeginTime;
        //        monthlyOT.DailyOT = dailyOT;
        //        monthlyOT.Description = "";
        //        monthlyOT.EndTime = item.EndTime;
        //        monthlyOT.IsDeleted = false;
        //        monthlyOT.DailyAssignFrom = item.AssignFrom;
        //        monthlyOT.DailyRequestNo = item.RequestNo;
        //        oReturn.Add(monthlyOT);
        //    }
        //    return oReturn;
        //}

        //public List<DailyOT> GenerateDailyOT1(string EmployeeID, string Period)
        //{
        //    CultureInfo oCL = new CultureInfo("en-US");
        //    List<DailyOT> oReturn = new List<DailyOT>();
        //    List<SaveOTData> otlist, otlist1;
        //    DateTime oPeriod = DateTime.ParseExact(Period, "yyyyMM", oCL);
        //    MonthlyWS MWS = MonthlyWS.GetCalendar(EmployeeID, oPeriod.Year, oPeriod.Month);
        //    otlist = new List<SaveOTData>();
        //    //otlist1 = HR.TM.ServiceManager.HRTMBuffer.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", true, false);

        //    otlist1 = ServiceManager.CreateInstance(CompanyCode).ESSData.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", true, false);
        //    otlist.AddRange(otlist1);
        //    //otlist1 = HR.TM.ServiceManager.HRTMService.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", true, false);

        //    otlist1 = ServiceManager.CreateInstance(CompanyCode).ERPData.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", true, false);
        //    otlist.AddRange(otlist1);
        //    foreach (SaveOTData item in otlist)
        //    {
        //        if (item.OTDate.ToString("yyyyMM") != Period)
        //        {
        //            continue;
        //        }
        //        DailyOT dailyOT = new DailyOT();
        //        dailyOT.BeginTime = item.BeginTime;
        //        dailyOT.EmployeeID = EmployeeID;
        //        dailyOT.EndTime = item.EndTime;
        //        dailyOT.OTDate = item.OTDate;
        //        dailyOT.IsPosted = false;
        //        dailyOT.Description = item.Description;
        //        dailyOT.OTItemTypeID = item.OTItemTypeID;
        //        dailyOT.OTHours = item.OTHours;
        //        dailyOT.IONumber = item.IONumber;
        //        dailyOT.ActivityCode = item.ActivityCode;
        //        oReturn.Add(dailyOT);
        //    }
        //    return oReturn;
        //}

        //public List<DailyOT> GenerateDailyOT(string EmployeeID, string Period)
        //{
        //    CultureInfo oCL = new CultureInfo("en-US");
        //    List<DailyOT> oReturn = new List<DailyOT>();
        //    List<SaveOTData> otlist, otlist1;
        //    DateTime oPeriod = DateTime.ParseExact(Period, "yyyyMM", oCL);
        //    MonthlyWS MWS = MonthlyWS.GetCalendar(EmployeeID, oPeriod.Year, oPeriod.Month);
        //    otlist = new List<SaveOTData>();
        //    //otlist1 = HR.TM.ServiceManager.HRTMBuffer.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", true, false);

        //    otlist1 = ServiceManager.CreateInstance(CompanyCode).ESSData.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", true, false);
        //    otlist.AddRange(otlist1);
        //    //otlist1 = HR.TM.ServiceManager.HRTMService.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", true, false);

        //    otlist1 = ServiceManager.CreateInstance(CompanyCode).ERPData.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", true, false);
        //    otlist.AddRange(otlist1);
        //    foreach (SaveOTData item in otlist)
        //    {
        //        if (item.OTDate.ToString("yyyyMM") != Period)
        //        {
        //            continue;
        //        }
        //        DailyOT dailyOT = new DailyOT();
        //        dailyOT.BeginTime = item.BeginTime;
        //        dailyOT.EmployeeID = EmployeeID;
        //        dailyOT.EndTime = item.EndTime;
        //        dailyOT.OTDate = item.OTDate;
        //        dailyOT.IsPosted = false;
        //        dailyOT.Description = item.Description;
        //        dailyOT.OTItemTypeID = item.OTItemTypeID;
        //        dailyOT.OTHours = item.OTHours;
        //        dailyOT.IONumber = item.IONumber;
        //        dailyOT.ActivityCode = item.ActivityCode;
        //        oReturn.Add(dailyOT);
        //    }
        //    oReturn = CalculateDailyOT(oReturn, MWS);
        //    return oReturn;
        //}
        //public static bool IsTNOM(string ValuationCode)
        //{
        //    return PlanTimeManagement.IsTNOM(ValuationCode);
        //}

        //public static bool IsNORM(string ValuationCode)
        //{
        //    return PlanTimeManagement.IsNORM(ValuationCode);
        //}

        //public static bool IsCanOTAssignFrom(EmployeeData assignData)
        //{
        //    if ("DEFGH".IndexOf(assignData.EmpGroup) > -1 && assignData.IsHaveSubOrdinate || true)
        //    {
        //        return true;
        //    }
        //    return false;
        //}

        //public static Decimal CalculatePlannedOTHours(string EmployeeID, DateTime inputDate, MonthlyWS MWS, DailyWS DWS, ref TimeSpan BeginTime, ref TimeSpan EndTime)
        //{
        //    if (BeginTime == TimeSpan.MinValue || EndTime == TimeSpan.MinValue)
        //    {
        //        return 0.0M;
        //    }
        //    string valuationClass = MWS.GetValuationClassExactly(inputDate.Day);
        //    if (EndTime <= BeginTime)
        //    {
        //        EndTime = EndTime.Add(new TimeSpan(1, 0, 0, 0));
        //    }
        //    DateTime buffer = new DateTime(MWS.WS_Year, MWS.WS_Month, inputDate.Day);
        //    PlanTimeManagement.SplitCutoffTime(EmployeeID, inputDate, MWS, ref BeginTime, ref EndTime);
        //    PlanTimeManagement.SplitWorkingTime(inputDate.Day, DWS, MWS, ref BeginTime, ref EndTime);
        //    if (EndTime < BeginTime)
        //    {
        //        BeginTime = TimeSpan.MinValue;
        //        EndTime = TimeSpan.MinValue;
        //        return 0.0M;
        //    }
        //    return (decimal)(EndTime.Subtract(BeginTime).TotalMinutes / 60) - (IsTNOM(valuationClass) ? 9 : 0);
        //}

        //public List<DailyOT> CalculateDailyOT(List<DailyOT> listData,MonthlyWS MWS)
        //{
        //    List<DailyOT> list = new List<DailyOT>();
        //    Dictionary<int, Decimal> additionalTNOM = new Dictionary<int, decimal>();
        //    Dictionary<int, Decimal> otTNOM = new Dictionary<int, decimal>();
        //    List<int> isLoadTNOM = new List<int>();
        //    for (int index = 0; index < listData.Count; index++)
        //    {
        //        DailyOT item = listData[index];
        //        TimeSpan time1, time2;
        //        time1 = item.BeginTime;
        //        time2 = item.EndTime;
        //        if (time2 <= time1)
        //        {
        //            time2 = time2.Add(new TimeSpan(1, 0, 0, 0));
        //        }
        //        bool lEnd = false;
        //        do
        //        {
        //            item.Calculate(MWS);
        //            if (item.IsTNOM)
        //            {
        //                #region " ONLY TNOM "
        //                if (!additionalTNOM.ContainsKey(item.OTDate.Day))
        //                {
        //                    additionalTNOM.Add(item.OTDate.Day, 0);
        //                }
        //                if (!otTNOM.ContainsKey(item.OTDate.Day))
        //                {
        //                    otTNOM.Add(item.OTDate.Day, 0);
        //                }
        //                if (!isLoadTNOM.Contains(item.OTDate.Day))
        //                {
        //                    //List<DailyOT> otList = OTManagement.LoadDailyOT(item.EmployeeID, MWS, item.OTDate);

        //                    List<DailyOT> otList = LoadDailyOT(item.EmployeeID, MWS, item.OTDate);
        //                    additionalTNOM[item.OTDate.Day] += Summary(otList, item);
        //                    isLoadTNOM.Add(item.OTDate.Day);
        //                }
        //                additionalTNOM[item.OTDate.Day] += item.OTHours;
        //                if (additionalTNOM[item.OTDate.Day] > 9)
        //                {
        //                    if (item.DWS.IsDayOff || MWS.GetWSCode(item.OTDate.Day,false) != "")
        //                    {
        //                        item.OTHours = additionalTNOM[item.OTDate.Day] - otTNOM[item.OTDate.Day];
        //                    }
        //                    else
        //                    {
        //                        item.OTHours = additionalTNOM[item.OTDate.Day] - 9 - otTNOM[item.OTDate.Day];
        //                    }
        //                }
        //                else
        //                {
        //                    item.OTHours = 0.0M;
        //                }
        //                otTNOM[item.OTDate.Day] += item.OTHours;
        //                if (item.TimeEvidance != null)
        //                {
        //                    list.Add(item);
        //                }
        //                #endregion
        //            }
        //            else
        //            {
        //                //if (item.OTHours > 0)
        //                //{
        //                //    list.Add(item);
        //                //}

        //                if (item.BeginTime.Days > 0)
        //                {
        //                    item.OTDate = item.OTDate.AddDays(item.BeginTime.Days);
        //                    item.BeginTime = new TimeSpan(item.BeginTime.Hours, item.BeginTime.Minutes, item.BeginTime.Seconds);
        //                    item.EndTime = new TimeSpan(item.EndTime.Hours, item.EndTime.Minutes, item.EndTime.Seconds);
        //                    time2 = new TimeSpan(time2.Hours, time2.Minutes, time2.Seconds);
        //                    continue;
        //                }
        //                list.Add(item);
        //            }
        //            if (item.EndTime >= time2)
        //            {
        //                break;
        //            }
        //            else
        //            {
        //                DailyOT buffer = new DailyOT();
        //                buffer.Description = item.Description;
        //                buffer.EmployeeID = item.EmployeeID;
        //                buffer.OTItemTypeID = item.OTItemTypeID;
        //                buffer.IsSummary = item.IsSummary;
        //                buffer.IsCompleted = item.IsCompleted;
        //                buffer.RequestNo = item.RequestNo;
        //                buffer.IONumber = item.IONumber;
        //                buffer.ActivityCode = item.ActivityCode;
        //                if (item.EndTime.Days > 0)
        //                {
        //                    time2 = new TimeSpan(time2.Hours, time2.Minutes, time2.Seconds);
        //                    buffer.BeginTime = new TimeSpan(item.EndTime.Hours, item.EndTime.Minutes, item.EndTime.Seconds);
        //                    buffer.EndTime = time2;
        //                    buffer.OTDate = item.OTDate.AddDays(item.EndTime.Days);
        //                    if (buffer.OTDate.Month != MWS.WS_Month || buffer.OTDate.Year != MWS.WS_Year)
        //                    {
        //                        MWS = MonthlyWS.GetCalendar(buffer.EmployeeID, buffer.OTDate.Year, buffer.OTDate.Month);
        //                    }
        //                }
        //                else
        //                {
        //                    buffer.BeginTime = item.EndTime;
        //                    buffer.EndTime = time2;
        //                    buffer.OTDate = item.OTDate;
        //                }
        //                item = buffer;
        //            }
        //        } while (!lEnd);
        //    }
        //    return list;
        //}

        //public static DailyOTLog CalculateDailyOT(String EmpID, DailyWS _DailyWS, MonthlyWS MWS, DateTime DateBeginRequest, DateTime DateEndRequest,List<DelegateData> lstDelegate)
        //{
        //    return CalculateDailyOTNew(EmpID, _DailyWS, MWS, DateBeginRequest, DateEndRequest, 0, lstDelegate);
        //    //return CalculateDailyOT(EmpID,_DailyWS,MWS,DateBeginRequest,DateEndRequest,0);
        //}
        
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="OTType">
        ///// OTType == 0 ? DailyOTRequest : OTType == 1 ? MonthlyOTEval : OTType == 2 ? MonthlyOTFinal : false
        ///// </param>
        ///// <returns></returns>
        //static DailyOTLog CalculateDailyOT(String EmpID,DailyWS _DailyWS, MonthlyWS MWS, DateTime DateBeginRequest, DateTime DateEndRequest,Byte OTType)
        //{
        //    #region "Old version"
        //    //DateTime WorkBeginDateTime,OffBeginDateTime;
        //    //DateTime WorkEndDateTime,OffEndDateTime;
        //    //DateTime CurrentDate = new DateTime(DateBeginRequest.Year, DateBeginRequest.Month, DateBeginRequest.Day);
        //    //DateTime EndDayToOff = new DateTime();
        //    //DateTime EndDayOffOT1x = new DateTime();
        //    //Double OTx1, OTx1_5, OTx3;
        //    //OTx1 = OTx1_5 = OTx3 = 0;
        //    //DailyOTLog Result = new DailyOTLog();
        //    //EmployeeData EmpData = EmployeeData.GetUserSetting(EmpID).Employee;
        //    //DailyWS DWS,DWS_C,DWS_P,DWS_N;
        //    //CalculateOTType otType;
        //    //List<DateTimeIntersec> OTx1Period = new List<DateTimeIntersec>();
        //    //List<DateTimeIntersec> OTx15Period = new List<DateTimeIntersec>();
        //    //List<DateTimeIntersec> OTx3Period = new List<DateTimeIntersec>();
        //    //DateTimeIntersec OTPeriod;
        //    //Dictionary<DateTime, DailyWS> dictDWS = new Dictionary<DateTime, DailyWS>();
        //    ////ModifiedBy: Ratchatawan W. (2012-01-09)
        //    ////DateTime beginDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        //    ////DateTime endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
        //    //DateTime beginDate = new DateTime(CurrentDate.Year, CurrentDate.Month, 1);
        //    //DateTime endDate = beginDate.AddMonths(1).AddMinutes(-1);

        //    ////CHAT 2011-12-15 Set Work Begin-End Time of Day off
        //    //OffBeginDateTime = CurrentDate.Add(new TimeSpan(8, 30, 0));
        //    //OffEndDateTime = CurrentDate.Add(new TimeSpan(17, 30, 0));

        //    //Result.EmployeeID = EmpID;
        //    //Result.oTimeEval = new List<TimeEval>();

        //    //// find Daily Work Schedule in this period
        //    //for (DateTime runningDate = beginDate.AddDays(-1); runningDate <= endDate.AddDays(1); runningDate = runningDate.AddDays(1))
        //    //{
        //    //    EmployeeData empData = new EmployeeData(EmpID, runningDate.Date);
        //    //    DailyWS dws = empData.GetDailyWorkSchedule1(runningDate.Date);

        //    //    dws.BeginDate = runningDate;

        //    //    // fill BeginDate and EndDate.
        //    //    // EndDate is after BeginDate one day, if DWS is over midnight.
        //    //    if (dws.IsDayOffOrHoliday)
        //    //    {
        //    //        dws.EndDate = runningDate;
        //    //    }
        //    //    else
        //    //    {
        //    //        if (dws.WorkBeginTime <= dws.WorkEndTime)
        //    //        {
        //    //            dws.EndDate = runningDate;
        //    //        }
        //    //        else
        //    //        {
        //    //            dws.EndDate = runningDate.AddDays(1);
        //    //        }
        //    //    }
        //    //    dictDWS[runningDate] = dws;
        //    //}

        //    //DWS_C = dictDWS[CurrentDate.Date];
        //    //DWS_P = dictDWS[CurrentDate.AddDays(-1).Date];
        //    //DWS_N = dictDWS[CurrentDate.AddDays(1).Date];

        //    //if (!_DailyWS.IsDayOff && MWS.GetWSCode(DateBeginRequest.Day, false) != "1")
        //    //#region "WorkDay"
        //    //{
        //    //    WorkBeginDateTime = CurrentDate.Add(_DailyWS.WorkBeginTime);
        //    //    WorkEndDateTime = _DailyWS.WorkEndTime < _DailyWS.WorkBeginTime ? CurrentDate.Add(_DailyWS.WorkEndTime).AddDays(1) : CurrentDate.Add(_DailyWS.WorkEndTime);

        //    //    if (EmpData.EmpSubAreaType == EmployeeSubAreaType.Norm || EmpData.EmpSubAreaType == EmployeeSubAreaType.Flex)
        //    //    {
        //    //        #region "Norm Or Flex"

        //    //        otType = new CalculateOTType();
        //    //        DWS = EmpData.GetDailyWorkSchedule(CurrentDate.AddDays(-1), true);
        //    //        if (DWS.IsDayOff || MWS.GetWSCode(CurrentDate.AddDays(-1).Day, false) == "1")
        //    //        {
        //    //            // Off-Day-?
        //    //            otType = CalculateOTType.OWW;

        //    //            OTPeriod = new DateTimeIntersec();
        //    //            OTPeriod.Begin1 = CurrentDate;
        //    //            OTPeriod.End1 = WorkBeginDateTime;
        //    //            OTx15Period.Add(OTPeriod);

        //    //            OTPeriod = new DateTimeIntersec();
        //    //            OTPeriod.Begin1 = OffEndDateTime.AddDays(-1);
        //    //            OTPeriod.End1 = CurrentDate;
        //    //            OTx3Period.Add(OTPeriod);

        //    //            OTPeriod = new DateTimeIntersec();
        //    //            OTPeriod.Begin1 = OffBeginDateTime.AddDays(-1);
        //    //            OTPeriod.End1 = OffEndDateTime.AddDays(-1).AddHours(-5);
        //    //            OTx1Period.Add(OTPeriod);

        //    //            OTPeriod = new DateTimeIntersec();
        //    //            OTPeriod.Begin1 = OffBeginDateTime.AddDays(-1).AddHours(5);
        //    //            OTPeriod.End1 = OffEndDateTime.AddDays(-1);
        //    //            OTx1Period.Add(OTPeriod);

        //    //            OTPeriod = new DateTimeIntersec();
        //    //            OTPeriod.Begin1 = CurrentDate.AddDays(-1);
        //    //            OTPeriod.End1 = OffBeginDateTime.AddDays(-1);
        //    //            OTx3Period.Add(OTPeriod);
        //    //        }
        //    //        else
        //    //        {
        //    //            // Day-Day-?
        //    //            otType = CalculateOTType.WWW;

        //    //            OTPeriod = new DateTimeIntersec();
        //    //            OTPeriod.Begin1 = CurrentDate.AddDays(-1).Add(DWS.WorkEndTime);
        //    //            OTPeriod.End1 = WorkBeginDateTime;
        //    //            OTx15Period.Add(OTPeriod);

        //    //            OTPeriod = new DateTimeIntersec();
        //    //            OTPeriod.Begin1 = CurrentDate.AddDays(-1);
        //    //            OTPeriod.End1 = CurrentDate.AddDays(-1).Add(DWS.WorkBeginTime);
        //    //            OTx15Period.Add(OTPeriod);
        //    //        }

        //    //        DWS = EmpData.GetDailyWorkSchedule(CurrentDate.AddDays(1), true);
        //    //        if (DWS.IsDayOff || MWS.GetWSCode(CurrentDate.AddDays(1).Day, false) == "1")
        //    //        {
        //    //            // ?-Day-Off
        //    //            otType = otType == CalculateOTType.OWW ? CalculateOTType.OWO : CalculateOTType.WWO;

        //    //            OTPeriod = new DateTimeIntersec();
        //    //            OTPeriod.Begin1 = WorkEndDateTime;
        //    //            OTPeriod.End1 = CurrentDate.AddDays(1);
        //    //            OTx15Period.Add(OTPeriod);

        //    //            OTPeriod = new DateTimeIntersec();
        //    //            OTPeriod.Begin1 = CurrentDate.AddDays(1);
        //    //            OTPeriod.End1 = OffBeginDateTime.AddDays(1);
        //    //            OTx3Period.Add(OTPeriod);

        //    //            OTPeriod = new DateTimeIntersec();
        //    //            OTPeriod.Begin1 = OffBeginDateTime.AddDays(1);
        //    //            OTPeriod.End1 = OffEndDateTime.AddDays(1).AddHours(-5);
        //    //            OTx1Period.Add(OTPeriod);

        //    //            OTPeriod = new DateTimeIntersec();
        //    //            OTPeriod.Begin1 = OffBeginDateTime.AddDays(1).AddHours(5);
        //    //            OTPeriod.End1 = OffEndDateTime.AddDays(1);
        //    //            OTx1Period.Add(OTPeriod);

        //    //            OTPeriod = new DateTimeIntersec();
        //    //            OTPeriod.Begin1 = OffEndDateTime.AddDays(1);
        //    //            OTPeriod.End1 = CurrentDate.AddDays(2);
        //    //            OTx3Period.Add(OTPeriod);
        //    //        }
        //    //        else
        //    //        {
        //    //            // ?-Day-Day
        //    //            otType = otType == CalculateOTType.OWW ? CalculateOTType.OWW : CalculateOTType.WWW;

        //    //            OTPeriod = new DateTimeIntersec();
        //    //            OTPeriod.Begin1 = WorkEndDateTime;
        //    //            OTPeriod.End1 = CurrentDate.AddDays(1).Add(DWS.WorkBeginTime);
        //    //            OTx15Period.Add(OTPeriod);

        //    //            OTPeriod = new DateTimeIntersec();
        //    //            OTPeriod.Begin1 = CurrentDate.AddDays(1).Add(DWS.WorkEndTime);
        //    //            OTPeriod.End1 = CurrentDate.AddDays(1);
        //    //            OTx15Period.Add(OTPeriod);
        //    //        }

        //    //        #endregion
        //    //    }
        //    //    else
        //    //    {
        //    //        #region "Not(Norm Or Flex)"
        //    //        otType = new CalculateOTType();

        //    //        DWS = EmpData.GetDailyWorkSchedule(CurrentDate.AddDays(-1), true);
        //    //        if (DWS.IsDayOff || MWS.GetWSCode(CurrentDate.AddDays(-1).Day, false) == "1")
        //    //        {
        //    //            // Off-Day-?
        //    //            otType = CalculateOTType.OWW;

        //    //            if (WorkBeginDateTime > DateBeginRequest)
        //    //            {
        //    //                OTPeriod = new DateTimeIntersec();
        //    //                OTPeriod.Begin1 = WorkBeginDateTime.AddHours(-6);
        //    //                OTPeriod.End1 = WorkBeginDateTime;
        //    //                OTx15Period.Add(OTPeriod);

        //    //                DateTime chkTime = WorkBeginDateTime.AddHours(-6);
        //    //                TimeSpan subchkTime = chkTime.Subtract(DateBeginRequest);

        //    //                if (DateBeginRequest < chkTime)
        //    //                {
        //    //                    if(subchkTime.Hours > 8)
        //    //                    {
        //    //                        OTPeriod = new DateTimeIntersec();
        //    //                        OTPeriod.Begin1 = CurrentDate.Date;
        //    //                        OTPeriod.End1 = CurrentDate.AddHours(8);
        //    //                        OTx1Period.Add(OTPeriod);

        //    //                        OTPeriod = new DateTimeIntersec();
        //    //                        OTPeriod.Begin1 = CurrentDate.AddHours(8);
        //    //                        OTPeriod.End1 = WorkBeginDateTime.AddHours(-6);
        //    //                        OTx3Period.Add(OTPeriod);
        //    //                    }
        //    //                    else
        //    //                    {
        //    //                        OTPeriod = new DateTimeIntersec();
        //    //                        OTPeriod.Begin1 = DateBeginRequest;
        //    //                        OTPeriod.End1 = WorkBeginDateTime.AddHours(-6);
        //    //                        OTx1Period.Add(OTPeriod);
        //    //                    }
        //    //                }
        //    //            }
        //    //        }
        //    //        else
        //    //        {
        //    //            // Day-Day-?
        //    //            otType = CalculateOTType.WWW;

        //    //            OTPeriod = new DateTimeIntersec();
        //    //            OTPeriod.Begin1 = WorkBeginDateTime.AddHours(-12);
        //    //            OTPeriod.End1 = WorkBeginDateTime;
        //    //            OTx15Period.Add(OTPeriod);
        //    //        }

        //    //        DWS = EmpData.GetDailyWorkSchedule(CurrentDate.AddDays(1), true);
        //    //        if (DWS.IsDayOff || MWS.GetWSCode(CurrentDate.AddDays(1).Day, false) == "1")
        //    //        {
        //    //            // ?-Day-Off
        //    //            otType = otType == CalculateOTType.OWW ? CalculateOTType.OWO : CalculateOTType.WWO;

        //    //            OTPeriod = new DateTimeIntersec();
        //    //            OTPeriod.Begin1 = WorkEndDateTime;
        //    //            OTPeriod.End1 = WorkEndDateTime.AddHours(6);
        //    //            OTx15Period.Add(OTPeriod);

        //    //            if (WorkBeginDateTime.AddHours(24) > OffEndDateTime.AddDays(1))
        //    //            {
        //    //                OTPeriod = new DateTimeIntersec();
        //    //                OTPeriod.Begin1 = WorkEndDateTime.AddDays(-1);
        //    //                OTPeriod.End1 = WorkBeginDateTime.AddHours(-6);
        //    //                OTx15Period.Add(OTPeriod);
        //    //            }
        //    //            else if (WorkBeginDateTime.AddHours(24) > OffBeginDateTime.AddDays(1) && WorkBeginDateTime.AddHours(24) < OffEndDateTime.AddDays(1))
        //    //            {
        //    //                OTPeriod = new DateTimeIntersec();
        //    //                OTPeriod.Begin1 = WorkBeginDateTime.AddHours(24);
        //    //                OTPeriod.End1 = OffEndDateTime.AddDays(1);
        //    //                OTx1Period.Add(OTPeriod);

        //    //                OTPeriod = new DateTimeIntersec();
        //    //                OTPeriod.Begin1 = OffEndDateTime.AddDays(1);
        //    //                OTPeriod.End1 = CurrentDate.AddDays(2);
        //    //                OTx3Period.Add(OTPeriod);
        //    //            }
        //    //            else if (WorkBeginDateTime.AddHours(24) <= OffBeginDateTime.AddDays(1))
        //    //            {
        //    //                OTPeriod = new DateTimeIntersec();
        //    //                OTPeriod.Begin1 = WorkEndDateTime.AddHours(6);
        //    //                EndDayOffOT1x = OTPeriod.Begin1;
        //    //                OTPeriod.End1 = EndDayOffOT1x.AddHours(8);
        //    //                OTx1Period.Add(OTPeriod);

        //    //                TimeSpan subRequest = DateEndRequest.Subtract(DateBeginRequest);
        //    //                if (subRequest.Hours > 8)
        //    //                {
        //    //                    OTPeriod = new DateTimeIntersec();
        //    //                    OTPeriod.Begin1 = EndDayOffOT1x.AddHours(8);
        //    //                    OTPeriod.End1 = DateEndRequest;
        //    //                    OTx3Period.Add(OTPeriod);
        //    //                }
        //    //            }
        //    //        }
        //    //        else
        //    //        {
        //    //            // ?-Day-Day
        //    //            otType = otType == CalculateOTType.OWW ? CalculateOTType.OWW : CalculateOTType.WWW;

        //    //            OTPeriod = new DateTimeIntersec();
        //    //            OTPeriod.Begin1 = WorkEndDateTime;
        //    //            OTPeriod.End1 = WorkEndDateTime.AddHours(12);
        //    //            OTx15Period.Add(OTPeriod);
        //    //        }
        //    //        #endregion
        //    //    }
        //    //}
        //    //#endregion
        //    //else
        //    //#region " DayOff / Holiday "
        //    //{
        //    //    WorkBeginDateTime = OffBeginDateTime;
        //    //    WorkEndDateTime = OffEndDateTime;

        //    //    if (EmpData.EmpSubAreaType == EmployeeSubAreaType.Norm || EmpData.EmpSubAreaType == EmployeeSubAreaType.Flex)
        //    //    {
        //    //        #region "Norm Or Flex"
        //    //        OTPeriod = new DateTimeIntersec();
        //    //        OTPeriod.Begin1 = OffBeginDateTime;
        //    //        OTPeriod.End1 = OffEndDateTime.AddHours(-5);
        //    //        OTx1Period.Add(OTPeriod);

        //    //        OTPeriod = new DateTimeIntersec();
        //    //        OTPeriod.Begin1 = OffBeginDateTime.AddHours(5);
        //    //        OTPeriod.End1 = OffEndDateTime;
        //    //        OTx1Period.Add(OTPeriod);

        //    //        otType = new CalculateOTType();
        //    //        DWS = EmpData.GetDailyWorkSchedule(CurrentDate.AddDays(-1), true);
        //    //        if (DWS.IsDayOff || MWS.GetWSCode(CurrentDate.AddDays(-1).Day, false) == "1")
        //    //        {
        //    //            // off-off-?
        //    //            otType = CalculateOTType.OOW;

        //    //            OTPeriod = new DateTimeIntersec();
        //    //            OTPeriod.Begin1 = OffEndDateTime.AddDays(-1);
        //    //            OTPeriod.End1 = OffBeginDateTime;
        //    //            OTx3Period.Add(OTPeriod);

        //    //            OTPeriod = new DateTimeIntersec();
        //    //            OTPeriod.Begin1 = OffBeginDateTime.AddDays(-1);
        //    //            OTPeriod.End1 = OffEndDateTime.AddDays(-1).AddHours(-5);
        //    //            OTx1Period.Add(OTPeriod);

        //    //            OTPeriod = new DateTimeIntersec();
        //    //            OTPeriod.Begin1 = OffBeginDateTime.AddDays(-1).AddHours(5);
        //    //            OTPeriod.End1 = OffEndDateTime.AddDays(-1);
        //    //            OTx1Period.Add(OTPeriod);

        //    //            OTPeriod = new DateTimeIntersec();
        //    //            OTPeriod.Begin1 = CurrentDate.AddDays(-1);
        //    //            OTPeriod.End1 = OffBeginDateTime.AddDays(-1);
        //    //            OTx3Period.Add(OTPeriod);
        //    //        }
        //    //        else
        //    //        {
        //    //            // Day-off-?
        //    //            otType = CalculateOTType.WOW;

        //    //            OTPeriod = new DateTimeIntersec();
        //    //            OTPeriod.Begin1 = CurrentDate;
        //    //            OTPeriod.End1 = OffBeginDateTime;
        //    //            OTx3Period.Add(OTPeriod);

        //    //            OTPeriod = new DateTimeIntersec();
        //    //            OTPeriod.Begin1 = OffEndDateTime.AddDays(-1);
        //    //            OTPeriod.End1 = CurrentDate;
        //    //            OTx15Period.Add(OTPeriod);

        //    //            OTPeriod = new DateTimeIntersec();
        //    //            OTPeriod.Begin1 = CurrentDate.AddDays(-1);
        //    //            OTPeriod.End1 = OffBeginDateTime.AddDays(-1);
        //    //            OTx15Period.Add(OTPeriod);
        //    //        }

        //    //        DWS = EmpData.GetDailyWorkSchedule(CurrentDate.AddDays(1), true);
        //    //        if (DWS.IsDayOff || MWS.GetWSCode(CurrentDate.AddDays(1).Day, false) == "1")
        //    //        {
        //    //            // ?-Off-Off
        //    //            otType = otType == CalculateOTType.OOW ? CalculateOTType.OOO : CalculateOTType.WOO;

        //    //            OTPeriod = new DateTimeIntersec();
        //    //            OTPeriod.Begin1 = OffEndDateTime;
        //    //            OTPeriod.End1 = OffBeginDateTime.AddDays(1);
        //    //            OTx3Period.Add(OTPeriod);

        //    //            OTPeriod = new DateTimeIntersec();
        //    //            OTPeriod.Begin1 = OffBeginDateTime.AddDays(1);
        //    //            OTPeriod.End1 = OffEndDateTime.AddDays(1).AddHours(-5);
        //    //            OTx1Period.Add(OTPeriod);

        //    //            OTPeriod = new DateTimeIntersec();
        //    //            OTPeriod.Begin1 = OffBeginDateTime.AddDays(1).AddHours(5);
        //    //            OTPeriod.End1 = OffEndDateTime.AddDays(1);
        //    //            OTx1Period.Add(OTPeriod);

        //    //            OTPeriod = new DateTimeIntersec();
        //    //            OTPeriod.Begin1 = OffEndDateTime.AddDays(1);
        //    //            OTPeriod.End1 = CurrentDate.AddDays(2);
        //    //            OTx3Period.Add(OTPeriod);
        //    //        }
        //    //        else
        //    //        {
        //    //            // ?-Off-Day
        //    //            otType = otType == CalculateOTType.OOW ? CalculateOTType.OOW : CalculateOTType.WOW;

        //    //            OTPeriod = new DateTimeIntersec();
        //    //            OTPeriod.Begin1 = OffEndDateTime;
        //    //            OTPeriod.End1 = CurrentDate.AddDays(1);
        //    //            OTx3Period.Add(OTPeriod);

        //    //            OTPeriod = new DateTimeIntersec();
        //    //            OTPeriod.Begin1 = CurrentDate.AddDays(1);
        //    //            OTPeriod.End1 = OffBeginDateTime.AddDays(1);
        //    //            OTx15Period.Add(OTPeriod);

        //    //            OTPeriod = new DateTimeIntersec();
        //    //            OTPeriod.Begin1 = OffEndDateTime.AddDays(1);
        //    //            OTPeriod.End1 = CurrentDate.AddDays(2);
        //    //            OTx15Period.Add(OTPeriod);
        //    //        }
        //    //        #endregion
        //    //    }
        //    //    else
        //    //    {
        //    //        #region "Not(Norm Or Flex)"
        //    //        otType = new CalculateOTType();
        //    //        DWS = EmpData.GetDailyWorkSchedule(CurrentDate.AddDays(-1), true);
        //    //        if (DWS.IsDayOff || MWS.GetWSCode(CurrentDate.AddDays(-1).Day, false) == "1")
        //    //        {
        //    //            // Off-Off-?
        //    //            otType = CalculateOTType.OOW;

        //    //            if (!DWS_N.IsDayOffOrHoliday)
        //    //            {
        //    //                OTPeriod = new DateTimeIntersec();
        //    //                OTPeriod.Begin1 = DateBeginRequest;
        //    //                OTPeriod.End1 = DateBeginRequest.Add(new TimeSpan(8, 0, 0));
        //    //                OTx1Period.Add(OTPeriod);

        //    //                TimeSpan subRequest = DateEndRequest.Subtract(DateBeginRequest);
        //    //                if (subRequest.Hours > 8)
        //    //                {
        //    //                    OTPeriod = new DateTimeIntersec();
        //    //                    OTPeriod.Begin1 = DateBeginRequest.Add(new TimeSpan(8, 0, 0));
        //    //                    OTPeriod.End1 = DWS_N.BeginDate.Add(DWS_N.WorkBeginTime.Add(new TimeSpan(-6,0,0)));
        //    //                    OTx3Period.Add(OTPeriod);
        //    //                }

        //    //                OTPeriod = new DateTimeIntersec();
        //    //                OTPeriod.Begin1 = DWS_N.BeginDate.Add(DWS_N.WorkBeginTime.Add(new TimeSpan(-6, 0, 0)));
        //    //                OTPeriod.End1 = DWS_N.BeginDate.Add(DWS_N.WorkBeginTime);
        //    //                OTx15Period.Add(OTPeriod);
        //    //            }
        //    //        }
        //    //        else
        //    //        {
        //    //            // Day-Off-?
        //    //            otType = CalculateOTType.WOW;

        //    //            OTPeriod = new DateTimeIntersec();
        //    //            OTPeriod.Begin1 = DWS_P.EndDate.Add(DWS_P.WorkEndTime);
        //    //            EndDayToOff = OTPeriod.End1 = DWS_P.EndDate.Add(DWS_P.WorkEndTime.Add(new TimeSpan(24,0,0)));
        //    //            OTx15Period.Add(OTPeriod);

        //    //            if (CurrentDate.AddDays(-1).Add(DWS.WorkBeginTime).AddHours(24) <= OffBeginDateTime)
        //    //            {
        //    //                OTPeriod = new DateTimeIntersec();
        //    //                OTPeriod.Begin1 = DWS_P.EndDate.Add(DWS_P.WorkEndTime.Add(new TimeSpan(6,0,0)));
        //    //                OTPeriod.End1 = OTPeriod.Begin1.Add(new TimeSpan(8,0,0));
        //    //                EndDayOffOT1x = OTPeriod.End1;
        //    //                OTx1Period.Add(OTPeriod);

        //    //                OTPeriod = new DateTimeIntersec();
        //    //                OTPeriod.Begin1 = EndDayOffOT1x;
        //    //                OTPeriod.End1 = DWS_N.BeginDate.Add(DWS_N.WorkBeginTime.Subtract(new TimeSpan(6, 0, 0)));
        //    //                OTx3Period.Add(OTPeriod);
        //    //            }
        //    //            else if (CurrentDate.AddDays(-1).Add(DWS.WorkBeginTime).AddHours(24) > OffBeginDateTime && CurrentDate.AddDays(-1).Add(DWS.WorkBeginTime).AddHours(24) < OffEndDateTime)
        //    //            {
        //    //                OTPeriod = new DateTimeIntersec();
        //    //                OTPeriod.Begin1 = CurrentDate.AddDays(-1).Add(DWS.WorkBeginTime).AddHours(24);
        //    //                OTPeriod.End1 = OffEndDateTime;
        //    //                OTx1Period.Add(OTPeriod);
        //    //            }
        //    //        }

        //    //        DWS = EmpData.GetDailyWorkSchedule(CurrentDate.AddDays(1), true);
        //    //        if (DWS.IsDayOff || MWS.GetWSCode(CurrentDate.AddDays(1).Day, false) == "1")
        //    //        {
        //    //            // ?-Off-Off
        //    //            otType = otType == CalculateOTType.OOW ? CalculateOTType.OOO : CalculateOTType.WOO;

        //    //            if (otType == CalculateOTType.WOO)
        //    //            {
        //    //                OTPeriod = new DateTimeIntersec();
        //    //                OTPeriod.Begin1 = DWS_P.EndDate.Add(DWS_P.WorkEndTime);
        //    //                OTPeriod.End1 = DWS_P.EndDate.Add(DWS_P.WorkEndTime.Add(new TimeSpan(6, 0, 0)));
        //    //                OTx15Period.Add(OTPeriod);

        //    //                OTPeriod = new DateTimeIntersec();
        //    //                OTPeriod.Begin1 = DWS_P.EndDate.Add(DWS_P.WorkEndTime.Add(new TimeSpan(6, 0, 0)));
        //    //                OTPeriod.End1 = DWS_P.EndDate.Add(DWS_P.WorkEndTime.Add(new TimeSpan(6, 0, 0))).AddHours(8);
        //    //                OTx1Period.Add(OTPeriod);

        //    //                OTPeriod = new DateTimeIntersec();
        //    //                OTPeriod.Begin1 = DWS_P.EndDate.Add(DWS_P.WorkEndTime.Add(new TimeSpan(6, 0, 0))).AddHours(8);
        //    //                OTPeriod.End1 = DWS_C.EndDate;
        //    //                OTx3Period.Add(OTPeriod);
        //    //            }
        //    //            else
        //    //            { 
        //    //                //OOO

        //    //                OTPeriod = new DateTimeIntersec();
        //    //                OTPeriod.Begin1 = DateBeginRequest;
        //    //                OTPeriod.End1 = DateBeginRequest.AddHours(8);
        //    //                OTx1Period.Add(OTPeriod);

        //    //                TimeSpan subRequest = DateEndRequest.Subtract(DateBeginRequest);
        //    //                if (subRequest.Hours > 8)
        //    //                {
        //    //                    OTPeriod = new DateTimeIntersec();
        //    //                    OTPeriod.Begin1 = DateBeginRequest.AddHours(8);
        //    //                    OTPeriod.End1 = DateEndRequest;
        //    //                    OTx3Period.Add(OTPeriod);
        //    //                }
        //    //            }
        //    //        }
        //    //        else
        //    //        {
        //    //            // ?-Off-Day
        //    //            otType = otType == CalculateOTType.OOW ? CalculateOTType.OOW : CalculateOTType.WOW;

        //    //            OTPeriod = new DateTimeIntersec();
        //    //            OTPeriod.Begin1 = OffEndDateTime > EndDayToOff ? OffEndDateTime : EndDayToOff;
        //    //            OTPeriod.End1 = OffBeginDateTime.AddDays(1) < CurrentDate.AddDays(1).Add(DWS.WorkBeginTime) ? OffBeginDateTime.AddDays(1) : CurrentDate.AddDays(1).Add(DWS.WorkBeginTime);
        //    //            OTx3Period.Add(OTPeriod);

        //    //            if (OffBeginDateTime.AddDays(1) < CurrentDate.AddDays(1).Add(DWS.WorkBeginTime))
        //    //            {
        //    //                OTPeriod = new DateTimeIntersec();
        //    //                OTPeriod.Begin1 = OffBeginDateTime.AddDays(1);
        //    //                OTPeriod.End1 = CurrentDate.AddDays(1).Add(DWS.WorkBeginTime);
        //    //                OTx1Period.Add(OTPeriod);
        //    //            }
        //    //        }
        //    //        #endregion
        //    //    }
                
        //    //}
        //    //#endregion

        //    //#region "Match OT"
        //    //foreach (DateTimeIntersec item in OTx1Period)
        //    //{
        //    //    item.Begin2 = DateBeginRequest;
        //    //    item.End2 = DateEndRequest;
        //    //    if (item.IsIntersecOT())
        //    //    {
        //    //        item.Intersection();
        //    //        OTx1 += ((TimeSpan)(item.EndBound - item.BeginBound)).TotalMinutes;
        //    //        AddTimeEval(item.BeginBound, item.EndBound, "2010", ref Result);
        //    //    }
        //    //}

        //    //foreach(DateTimeIntersec item in OTx15Period)
        //    //{
        //    //    item.Begin2 = DateBeginRequest;
        //    //    item.End2 = DateEndRequest;
        //    //    if (item.IsIntersecOT())
        //    //    {
        //    //        item.Intersection();
        //    //        OTx1_5 += ((TimeSpan)(item.EndBound - item.BeginBound)).TotalMinutes;
        //    //        AddTimeEval(item.BeginBound, item.EndBound, "2015", ref Result);
        //    //    }
        //    //}

        //    //foreach(DateTimeIntersec item in OTx3Period)
        //    //{
        //    //    item.Begin2 = DateBeginRequest;
        //    //    item.End2 = DateEndRequest;
        //    //    if (item.IsIntersecOT())
        //    //    {
        //    //        item.Intersection();
        //    //        OTx3 += ((TimeSpan)(item.EndBound - item.BeginBound)).TotalMinutes;
        //    //        AddTimeEval(item.BeginBound, item.EndBound, "2030", ref Result);
        //    //    }
        //    //}
        //    //#endregion

        //    //#region "Fill OT"

        //    //#endregion

        //    //if (OTType == 0)
        //    //{
        //    //    Result.RequestOTHour10 = Convert.ToDecimal(OTx1);
        //    //    Result.RequestOTHour15 = Convert.ToDecimal(OTx1_5);
        //    //    Result.RequestOTHour30 = Convert.ToDecimal(OTx3);
        //    //}
        //    //else if (OTType == 1)
        //    //{
        //    //    Result.EvaOTHour10 = Convert.ToDecimal(OTx1);
        //    //    Result.EvaOTHour15 = Convert.ToDecimal(OTx1_5);
        //    //    Result.EvaOTHour30 = Convert.ToDecimal(OTx3);
        //    //}
        //    //else if (OTType == 2)
        //    //{
        //    //    Result.FinalOTHour10 = Convert.ToDecimal(OTx1);
        //    //    Result.FinalOTHour15 = Convert.ToDecimal(OTx1_5);
        //    //    Result.FinalOTHour30 = Convert.ToDecimal(OTx3);
        //    //}
        //    //return Result;
        //    #endregion

        //    DateTime WorkBeginDateTime, OffBeginDateTime;
        //    DateTime WorkEndDateTime, OffEndDateTime;
        //    DateTime CurrentDate = new DateTime(DateBeginRequest.Year, DateBeginRequest.Month, DateBeginRequest.Day);
        //    DateTime EndDayToOff = new DateTime();
        //    Double OTx1, OTx1_5, OTx3;
        //    OTx1 = OTx1_5 = OTx3 = 0;
        //    DailyOTLog Result = new DailyOTLog();
        //    EmployeeData EmpData = new EmployeeData(EmpID, DateBeginRequest);
        //    DailyWS DWS;
        //    CalculateOTType otType;
        //    List<DateTimeIntersec> OTx1Period = new List<DateTimeIntersec>();
        //    List<DateTimeIntersec> OTx15Period = new List<DateTimeIntersec>();
        //    List<DateTimeIntersec> OTx3Period = new List<DateTimeIntersec>();
        //    DateTimeIntersec OTPeriod;
        //    bool IsFirst6HourFromWorkEndTime = true;
        //    bool IsOffToDay = false;
        //    OffBeginDateTime = CurrentDate.Add(new TimeSpan(8, 30, 0));
        //    OffEndDateTime = CurrentDate.Add(new TimeSpan(17, 30, 0));

        //    Result.EmployeeID = EmpID;
        //    Result.oTimeEval = new List<TimeEval>();

        //    if (!_DailyWS.IsDayOff && MWS.GetWSCode(DateBeginRequest.Day, false) != "1")
        //    #region "WorkDay"
        //    {
        //        WorkBeginDateTime = CurrentDate.Add(_DailyWS.WorkBeginTime);
        //        WorkEndDateTime = _DailyWS.WorkEndTime < _DailyWS.WorkBeginTime ? CurrentDate.Add(_DailyWS.WorkEndTime).AddDays(1) : CurrentDate.Add(_DailyWS.WorkEndTime);

        //        if (EmpData.EmpSubAreaType == EmployeeSubAreaType.Norm || EmpData.EmpSubAreaType == EmployeeSubAreaType.Flex)
        //        {
        //            #region "Norm Or Flex"

        //            otType = new CalculateOTType();
        //            DWS = EmpData.GetDailyWorkSchedule(CurrentDate.AddDays(-1), true);
        //            if (EmpData.DateSpecific.HiringDate.Date < CurrentDate.AddDays(-1).Date)
        //                if (DWS.IsDayOff || MWS.GetWSCode(CurrentDate.AddDays(-1).Day, false) == "1")
        //                {
        //                    // Off-Day-?
        //                    otType = CalculateOTType.OWW;

        //                    OTPeriod = new DateTimeIntersec();
        //                    OTPeriod.Begin1 = CurrentDate;
        //                    OTPeriod.End1 = WorkBeginDateTime;
        //                    OTx15Period.Add(OTPeriod);

        //                    OTPeriod = new DateTimeIntersec();
        //                    OTPeriod.Begin1 = OffEndDateTime.AddDays(-1);
        //                    OTPeriod.End1 = CurrentDate;
        //                    OTx3Period.Add(OTPeriod);
        //                }
        //                else
        //                {
        //                    // Day-Day-?
        //                    otType = CalculateOTType.WWW;

        //                    OTPeriod = new DateTimeIntersec();
        //                    OTPeriod.Begin1 = CurrentDate.AddDays(-1).Add(DWS.WorkEndTime);
        //                    OTPeriod.End1 = WorkBeginDateTime;
        //                    OTx15Period.Add(OTPeriod);

        //                }
        //            else 
        //            {
        //                otType = CalculateOTType.OWW;
        //                OTPeriod = new DateTimeIntersec();
        //                OTPeriod.Begin1 = CurrentDate.AddDays(-1);
        //                OTPeriod.End1 = WorkBeginDateTime;
        //                OTx15Period.Add(OTPeriod);
        //            }

        //            DWS = EmpData.GetDailyWorkSchedule(CurrentDate.AddDays(1), true);
        //            if (DWS.IsDayOff || MWS.GetWSCode(CurrentDate.AddDays(1).Day, false) == "1")
        //            {
        //                // ?-Day-Off
        //                otType = otType == CalculateOTType.OWW ? CalculateOTType.OWO : CalculateOTType.WWO;

        //                OTPeriod = new DateTimeIntersec();
        //                OTPeriod.Begin1 = WorkEndDateTime;
        //                OTPeriod.End1 = CurrentDate.AddDays(1);
        //                OTx15Period.Add(OTPeriod);

        //                OTPeriod = new DateTimeIntersec();
        //                OTPeriod.Begin1 = CurrentDate.AddDays(1);
        //                OTPeriod.End1 = OffBeginDateTime.AddDays(1);
        //                OTx3Period.Add(OTPeriod);
        //            }
        //            else
        //            {
        //                // ?-Day-Day
        //                otType = otType == CalculateOTType.OWW ? CalculateOTType.OWW : CalculateOTType.WWW;

        //                OTPeriod = new DateTimeIntersec();
        //                OTPeriod.Begin1 = WorkEndDateTime;
        //                OTPeriod.End1 = CurrentDate.AddDays(1).Add(DWS.WorkBeginTime);
        //                OTx15Period.Add(OTPeriod);
        //            }

        //            #endregion
        //        }
        //        else
        //        {
        //            #region "Not(Norm Or Flex)"

        //            otType = new CalculateOTType();

        //            DWS = EmpData.GetDailyWorkSchedule(CurrentDate.AddDays(-1), true);
        //            if (EmpData.DateSpecific.HiringDate.Date < CurrentDate.AddDays(-1).Date)
        //            {
        //                if (DWS.IsDayOff || MWS.GetWSCode(CurrentDate.AddDays(-1).Day, false) == "1")
        //                {
        //                    // Off-Day-?
        //                    otType = CalculateOTType.OWW;

        //                    if (IsOffToDay = DateBeginRequest > CurrentDate.AddDays(-1) && DateEndRequest <= WorkBeginDateTime) //&& WorkBeginDateTime.AddHours(-6) > DateBeginRequest
        //                    {
        //                        if (((TimeSpan)(WorkBeginDateTime.AddHours(-6) - DateBeginRequest)).TotalHours > 8)
        //                            if (DateEndRequest > WorkBeginDateTime.AddHours(-6))
        //                            {
        //                                OTx1 = 8 * 60;
        //                                OTx3 = ((TimeSpan)(WorkBeginDateTime.AddHours(-6) - DateBeginRequest.AddHours(8))).TotalMinutes;
        //                                OTx1_5 = ((TimeSpan)(DateEndRequest - WorkBeginDateTime.AddHours(-6))).TotalMinutes;
        //                            }
        //                            else
        //                            {
        //                                OTx1 = 8 * 60;
        //                                OTx3 = ((TimeSpan)(DateEndRequest - DateBeginRequest.AddHours(8))).TotalMinutes;
        //                            }
        //                        else
        //                        {
        //                            if (((TimeSpan)(WorkBeginDateTime - DateBeginRequest)).TotalHours <= 6)
        //                                OTx1_5 = ((TimeSpan)(DateEndRequest - DateBeginRequest)).TotalMinutes;
        //                            else
        //                            {
        //                                OTx1 = ((TimeSpan)(WorkBeginDateTime.AddHours(-6) - DateBeginRequest)).TotalMinutes;
        //                                OTx1_5 = ((TimeSpan)( DateEndRequest - WorkBeginDateTime.AddHours(-6))).TotalMinutes;
        //                            }
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    // Day-Day-?
        //                    otType = CalculateOTType.WWW;

        //                    OTPeriod = new DateTimeIntersec();
        //                    OTPeriod.Begin1 = CurrentDate.AddDays(-1).Add(DWS.WorkEndTime);
        //                    OTPeriod.End1 = WorkBeginDateTime;
        //                    OTx15Period.Add(OTPeriod);
        //                }
        //            }

        //            DWS = EmpData.GetDailyWorkSchedule(CurrentDate.AddDays(1), true);
        //            if (DWS.IsDayOff || MWS.GetWSCode(CurrentDate.AddDays(1).Day, false) == "1")
        //            {
        //                // ?-Day-Off
        //                otType = otType == CalculateOTType.OWW ? CalculateOTType.OWO : CalculateOTType.WWO;

        //                OTPeriod = new DateTimeIntersec();
        //                OTPeriod.Begin1 = WorkEndDateTime;
        //                OTPeriod.End1 = WorkEndDateTime.AddHours(6);
        //                OTx15Period.Add(OTPeriod);

        //                OTPeriod = new DateTimeIntersec();
        //                OTPeriod.Begin1 = WorkEndDateTime.AddHours(6); ;
        //                OTPeriod.End1 = WorkEndDateTime.AddHours(6).AddHours(8);
        //                OTx1Period.Add(OTPeriod);

        //                OTPeriod = new DateTimeIntersec();
        //                OTPeriod.Begin1 = WorkEndDateTime.AddHours(6).AddHours(8);
        //                OTPeriod.End1 = CurrentDate.AddDays(2);
        //                OTx3Period.Add(OTPeriod);
        //            }
        //            else
        //            {
        //                // ?-Day-Day
        //                otType = otType == CalculateOTType.OWW ? CalculateOTType.OWW : CalculateOTType.WWW;

        //                OTPeriod = new DateTimeIntersec();
        //                OTPeriod.Begin1 = WorkEndDateTime;
        //                OTPeriod.End1 = CurrentDate.AddDays(1).Add(DWS.WorkBeginTime);
        //                OTx15Period.Add(OTPeriod);
        //            }
        //            #endregion
        //        }
        //    }
        //    #endregion
        //    else
        //    #region " DayOff / Holiday "
        //    {
        //        WorkBeginDateTime = OffBeginDateTime;
        //        WorkEndDateTime = OffEndDateTime;


        //        if (EmpData.EmpSubAreaType == EmployeeSubAreaType.Norm || EmpData.EmpSubAreaType == EmployeeSubAreaType.Flex)
        //        {
        //            #region "Norm Or Flex"
        //            OTPeriod = new DateTimeIntersec();
        //            OTPeriod.Begin1 = OffBeginDateTime;
        //            OTPeriod.End1 = OffEndDateTime.AddHours(-5.5);
        //            OTx1Period.Add(OTPeriod);

        //            OTPeriod = new DateTimeIntersec();
        //            OTPeriod.Begin1 = OffBeginDateTime.AddHours(4.5);
        //            OTPeriod.End1 = OffEndDateTime;
        //            OTx1Period.Add(OTPeriod);

        //            otType = new CalculateOTType();
        //            DWS = EmpData.GetDailyWorkSchedule(CurrentDate.AddDays(-1), true);
        //            if (DWS.IsDayOff || MWS.GetWSCode(CurrentDate.AddDays(-1).Day, false) == "1")
        //            {
        //                // off-off-?
        //                otType = CalculateOTType.OOW;

        //                OTPeriod = new DateTimeIntersec();
        //                OTPeriod.Begin1 = OffEndDateTime.AddDays(-1);
        //                OTPeriod.End1 = OffBeginDateTime;
        //                OTx3Period.Add(OTPeriod);

        //                OTPeriod = new DateTimeIntersec();
        //                OTPeriod.Begin1 = OffBeginDateTime.AddDays(-1);
        //                OTPeriod.End1 = OffEndDateTime.AddDays(-1).AddHours(-5.5);
        //                OTx1Period.Add(OTPeriod);

        //                OTPeriod = new DateTimeIntersec();
        //                OTPeriod.Begin1 = OffBeginDateTime.AddDays(-1).AddHours(4.5);
        //                OTPeriod.End1 = OffEndDateTime.AddDays(-1);
        //                OTx1Period.Add(OTPeriod);

        //                OTPeriod = new DateTimeIntersec();
        //                OTPeriod.Begin1 = CurrentDate.AddDays(-1);
        //                OTPeriod.End1 = OffBeginDateTime.AddDays(-1);
        //                OTx3Period.Add(OTPeriod);
        //            }
        //            else
        //            {
        //                // Day-off-?
        //                otType = CalculateOTType.WOW;

        //                OTPeriod = new DateTimeIntersec();
        //                OTPeriod.Begin1 = CurrentDate;
        //                OTPeriod.End1 = OffBeginDateTime;
        //                OTx3Period.Add(OTPeriod);

        //                OTPeriod = new DateTimeIntersec();
        //                OTPeriod.Begin1 = CurrentDate.AddDays(-1).Add(DWS.WorkEndTime);
        //                OTPeriod.End1 = CurrentDate;
        //                OTx15Period.Add(OTPeriod);

        //            }

        //            DWS = EmpData.GetDailyWorkSchedule(CurrentDate.AddDays(1), true);
        //            if (DWS.IsDayOff || MWS.GetWSCode(CurrentDate.AddDays(1).Day, false) == "1")
        //            {
        //                // ?-Off-Off
        //                otType = otType == CalculateOTType.OOW ? CalculateOTType.OOO : CalculateOTType.WOO;

        //                OTPeriod = new DateTimeIntersec();
        //                OTPeriod.Begin1 = OffEndDateTime;
        //                OTPeriod.End1 = OffBeginDateTime.AddDays(1);
        //                OTx3Period.Add(OTPeriod);

        //                OTPeriod = new DateTimeIntersec();
        //                OTPeriod.Begin1 = OffBeginDateTime.AddDays(1);
        //                OTPeriod.End1 = OffEndDateTime.AddDays(1).AddHours(-5.5);
        //                OTx1Period.Add(OTPeriod);

        //                OTPeriod = new DateTimeIntersec();
        //                OTPeriod.Begin1 = OffBeginDateTime.AddDays(1).AddHours(4.5);
        //                OTPeriod.End1 = OffEndDateTime.AddDays(1);
        //                OTx1Period.Add(OTPeriod);

        //                OTPeriod = new DateTimeIntersec();
        //                OTPeriod.Begin1 = OffEndDateTime.AddDays(1);
        //                OTPeriod.End1 = CurrentDate.AddDays(2);
        //                OTx3Period.Add(OTPeriod);
        //            }
        //            else
        //            {
        //                // ?-Off-Day
        //                otType = otType == CalculateOTType.OOW ? CalculateOTType.OOW : CalculateOTType.WOW;

        //                OTPeriod = new DateTimeIntersec();
        //                OTPeriod.Begin1 = OffEndDateTime;
        //                OTPeriod.End1 = CurrentDate.AddDays(1);
        //                OTx3Period.Add(OTPeriod);

        //                OTPeriod = new DateTimeIntersec();
        //                OTPeriod.Begin1 = CurrentDate.AddDays(1);
        //                OTPeriod.End1 = CurrentDate.AddDays(1).Add(DWS.WorkBeginTime);
        //                OTx15Period.Add(OTPeriod);
        //            }
        //            #endregion
        //        }
        //        else
        //        {
        //            #region "Not(Norm Or Flex)"
        //            otType = new CalculateOTType();
        //            DWS = EmpData.GetDailyWorkSchedule(CurrentDate.AddDays(-1), true);
        //            if (DWS.IsDayOff || MWS.GetWSCode(CurrentDate.AddDays(-1).Day, false) == "1")
        //            {
        //                // Off-Off-?

        //                otType = CalculateOTType.OOW;
        //                IsFirst6HourFromWorkEndTime = false;

        //                //OTPeriod = new DateTimeIntersec();
        //                //OTPeriod.Begin1 = CurrentDate.AddDays(-1);
        //                //OTPeriod.End1 = CurrentDate.AddDays(-1).AddHours(8);
        //                //OTx1Period.Add(OTPeriod);

        //                //OTPeriod = new DateTimeIntersec();
        //                //OTPeriod.Begin1 = CurrentDate.AddDays(-1).AddHours(8);
        //                //OTPeriod.End1 = CurrentDate.AddDays(1);
        //                //OTx3Period.Add(OTPeriod);
        //            }
        //            else
        //            {
        //                // Day-Off-?
        //                otType = CalculateOTType.WOW;

        //                OTPeriod = new DateTimeIntersec();
        //                OTPeriod.Begin1 = DWS.WorkBeginTime > DWS.WorkEndTime ? CurrentDate.Add(DWS.WorkEndTime) : CurrentDate.AddDays(-1).Add(DWS.WorkEndTime);
        //                OTPeriod.End1 = OTPeriod.Begin1.AddHours(6);
        //                OTx15Period.Add(OTPeriod);

        //                IsFirst6HourFromWorkEndTime = DateBeginRequest >= OTPeriod.Begin1 && DateBeginRequest <= OTPeriod.End1;
                     
        //                OTPeriod = new DateTimeIntersec();
        //                OTPeriod.Begin1 = DWS.WorkBeginTime > DWS.WorkEndTime ? CurrentDate.Add(DWS.WorkEndTime).AddHours(6) : CurrentDate.AddDays(-1).Add(DWS.WorkEndTime).AddHours(6);
        //                OTPeriod.End1 = OTPeriod.Begin1.AddHours(8);
        //                OTx1Period.Add(OTPeriod);

        //                OTPeriod = new DateTimeIntersec();
        //                OTPeriod.Begin1 = DWS.WorkBeginTime > DWS.WorkEndTime ? CurrentDate.Add(DWS.WorkEndTime).AddHours(6).AddHours(8) : CurrentDate.AddDays(-1).Add(DWS.WorkEndTime).AddHours(6).AddHours(8);
        //                OTPeriod.End1 = CurrentDate.AddDays(1);
        //                OTx3Period.Add(OTPeriod);
        //            }

        //            DWS = EmpData.GetDailyWorkSchedule(CurrentDate.AddDays(1), true);
        //            if (DWS.IsDayOff || MWS.GetWSCode(CurrentDate.AddDays(1).Day, false) == "1")
        //            {
        //                // ?-Off-Off
        //                otType = otType == CalculateOTType.OOW ? CalculateOTType.OOO : CalculateOTType.WOO;
        //                if (otType == CalculateOTType.WOO)
        //                {
        //                    OTPeriod = new DateTimeIntersec();
        //                    OTPeriod.Begin1 = OTx3Period[0].End1;
        //                    OTPeriod.End1 = CurrentDate.AddDays(2).AddHours(-6);
        //                    OTx3Period.Add(OTPeriod);
        //                }
        //                //else if (otType == CalculateOTType.OOO)
        //                //{
        //                //    OTPeriod = new DateTimeIntersec();
        //                //    OTPeriod.Begin1 = CurrentDate;
        //                //    OTPeriod.End1 = CurrentDate.AddDays(2).AddHours(-6);
        //                //    OTx3Period.Add(OTPeriod);
        //                //}
                       
        //            }
        //            else
        //            {
        //                // ?-Off-Day
        //                otType = otType == CalculateOTType.OOW ? CalculateOTType.OOW : CalculateOTType.WOW;
        //                if (IsOffToDay = DateBeginRequest > CurrentDate && DateEndRequest <= CurrentDate.AddDays(1).Add(DWS.WorkBeginTime) && !IsFirst6HourFromWorkEndTime)
        //                {
        //                    if (((TimeSpan)(CurrentDate.AddDays(1).Add(DWS.WorkBeginTime).AddHours(-6) - DateBeginRequest)).TotalHours > 8)
        //                    {
        //                        if (DateEndRequest > CurrentDate.AddDays(1).Add(DWS.WorkBeginTime).AddHours(-6))
        //                        {
        //                            OTx1 = 8 * 60;
        //                            OTx3 = ((TimeSpan)(CurrentDate.AddDays(1).Add(DWS.WorkBeginTime).AddHours(-6) - DateBeginRequest.AddHours(8))).TotalMinutes;
        //                            OTx1_5 = ((TimeSpan)(DateEndRequest - CurrentDate.AddDays(1).Add(DWS.WorkBeginTime).AddHours(-6))).TotalMinutes;
        //                        }
        //                        else if (((TimeSpan)(DateEndRequest - DateBeginRequest)).TotalHours >= 8)
        //                        {
        //                            OTx1 = 8 * 60;
        //                            OTx3 = ((TimeSpan)(DateEndRequest - DateBeginRequest.AddHours(8))).TotalMinutes;
        //                        }
        //                        else if (((TimeSpan)(DateEndRequest - DateBeginRequest)).TotalHours < 8)
        //                        {
        //                            OTx1 = ((TimeSpan)(DateEndRequest - DateBeginRequest)).TotalMinutes;
        //                        }
        //                    }
        //                    else if (DateEndRequest > CurrentDate.AddDays(1).Add(DWS.WorkBeginTime).AddHours(-6))
        //                    {
        //                        OTx1 = ((TimeSpan)(CurrentDate.AddDays(1).Add(DWS.WorkBeginTime).AddHours(-6) - DateBeginRequest)).TotalMinutes;
        //                        OTx1_5 = ((TimeSpan)(DateEndRequest - CurrentDate.AddDays(1).Add(DWS.WorkBeginTime).AddHours(-6))).TotalMinutes;
        //                    }
        //                    else
        //                        OTx1 = ((TimeSpan)(DateEndRequest - DateBeginRequest)).TotalMinutes;
                            
        //                    IsFirst6HourFromWorkEndTime = !IsFirst6HourFromWorkEndTime;
        //                }

        //                //if (otType == CalculateOTType.WOW)
        //                //{
        //                //    OTPeriod = new DateTimeIntersec();
        //                //    OTPeriod.Begin1 = OTx3Period[0].End1;
        //                //    OTPeriod.End1 = CurrentDate.AddDays(1).Add(DWS.WorkBeginTime).AddHours(-6);
        //                //    OTx3Period.Add(OTPeriod);

        //                //    OTPeriod = new DateTimeIntersec();
        //                //    OTPeriod.Begin1 = OTx3Period[0].End1;
        //                //    OTPeriod.End1 = CurrentDate.AddDays(1).Add(DWS.WorkBeginTime).AddHours(-6);
        //                //    OTx1Period.Add(OTPeriod);
        //                //}
        //                //else if (otType == CalculateOTType.OOW)
        //                //{
        //                //    OTPeriod = new DateTimeIntersec();
        //                //    OTPeriod.Begin1 = CurrentDate;
        //                //    OTPeriod.End1 = CurrentDate.AddDays(1).Add(DWS.WorkBeginTime).AddHours(-6);
        //                //    OTx3Period.Add(OTPeriod);
        //                //}
        //            }
        //            #endregion
        //        }

        //    }
        //    #endregion

        //    #region "Match OT"
        //    if (((EmpData.EmpSubAreaType == EmployeeSubAreaType.Shift8 || EmpData.EmpSubAreaType == EmployeeSubAreaType.Shift12) && !IsFirst6HourFromWorkEndTime))
        //    {
        //        TimeSpan tp = DateEndRequest - DateBeginRequest;
        //        if (tp.TotalHours >= 8)
        //        {
        //            OTx1 = 8*60;
        //            if (tp.TotalHours > 8)
        //                OTx3 = tp.TotalMinutes - 8*60;
        //        }
        //        else
        //            OTx1 = tp.TotalMinutes;

        //    }
        //    else if ((EmpData.EmpSubAreaType == EmployeeSubAreaType.Shift8 || EmpData.EmpSubAreaType == EmployeeSubAreaType.Shift12) && IsOffToDay)
        //    { 
        //     // Do Nothing
        //    }
        //    else
        //    {
        //        foreach (DateTimeIntersec item in OTx1Period)
        //        {
        //            item.Begin2 = DateBeginRequest;
        //            item.End2 = DateEndRequest;
        //            if (item.IsIntersecOT())
        //            {
        //                item.Intersection();
        //                OTx1 += ((TimeSpan)(item.EndBound - item.BeginBound)).TotalMinutes;
        //                AddTimeEval(item.BeginBound, item.EndBound, "2010", ref Result);
        //            }
        //        }

        //        foreach (DateTimeIntersec item in OTx15Period)
        //        {
        //            item.Begin2 = DateBeginRequest;
        //            item.End2 = DateEndRequest;
        //            if (item.IsIntersecOT())
        //            {
        //                item.Intersection();
        //                OTx1_5 += ((TimeSpan)(item.EndBound - item.BeginBound)).TotalMinutes;
        //                AddTimeEval(item.BeginBound, item.EndBound, "2015", ref Result);
        //            }
        //        }

        //        foreach (DateTimeIntersec item in OTx3Period)
        //        {
        //            item.Begin2 = DateBeginRequest;
        //            item.End2 = DateEndRequest;
        //            if (item.IsIntersecOT())
        //            {
        //                item.Intersection();
        //                OTx3 += ((TimeSpan)(item.EndBound - item.BeginBound)).TotalMinutes;
        //                AddTimeEval(item.BeginBound, item.EndBound, "2030", ref Result);
        //            }
        //        }
        //    }
        //    #endregion


        //    if (OTType == 0)
        //    {
        //        Result.RequestOTHour10 = Convert.ToDecimal(OTx1);
        //        Result.RequestOTHour15 = Convert.ToDecimal(OTx1_5);
        //        Result.RequestOTHour30 = Convert.ToDecimal(OTx3);
        //    }
        //    else if (OTType == 1)
        //    {
        //        Result.EvaOTHour10 = Convert.ToDecimal(OTx1);
        //        Result.EvaOTHour15 = Convert.ToDecimal(OTx1_5);
        //        Result.EvaOTHour30 = Convert.ToDecimal(OTx3);
        //    }
        //    else if (OTType == 2)
        //    {
        //        Result.FinalOTHour10 = Convert.ToDecimal(OTx1);
        //        Result.FinalOTHour15 = Convert.ToDecimal(OTx1_5);
        //        Result.FinalOTHour30 = Convert.ToDecimal(OTx3);
        //    }
        //    return Result;
        //}
        
        ////AddBy: Ratchatawan W.
        ///// <param name="OTType">
        ///// OTType == 0 ? DailyOTRequest : OTType == 1 ? MonthlyOTEval : OTType == 2 ? MonthlyOTFinal : false
        ///// </param>
        //public static DailyOTLog CalculateDailyOTNew(string EmployeeID, DailyWS oDailyWS, MonthlyWS oMWS, DateTime oDateBeginRequest, DateTime oDateEndRequest, Byte oOTType,List<DelegateData> lstDelegate)
        //{
        //    DailyOTLog Result = new DailyOTLog();
        //    EmployeeData oEmployeeData = new EmployeeData(EmployeeID, oDateBeginRequest);
                       
        //    DateTime WorkBeginDateTime = new DateTime();
        //    DateTime WorkEndDateTime = new DateTime();
        //    DateTime OTDate = oDateBeginRequest.Date;
        //    INFOTYPE0007 oInfotype0007 = EmployeeData.GetEmployeeWF(EmployeeID, OTDate);
        //    Double OTx1=0, OTx1_5=0, OTx3=0;
        //    List<DateTimeIntersec> OTx1Period = new List<DateTimeIntersec>();
        //    List<DateTimeIntersec> OTx15Period = new List<DateTimeIntersec>();
        //    List<DateTimeIntersec> OTx3Period = new List<DateTimeIntersec>();
        //    DateTimeIntersec OTPeriod;

        //    EmployeeData oDelegateEmployee = null;
        //    DailyWS oldWS = null;
        //    bool isShiftDelegateToNorm = false;
        //    bool isNewBeginOverEnd = false;
        //    if (lstDelegate.Count > 0)
        //    {
        //        oDelegateEmployee = new EmployeeData(lstDelegate[0].EmployeeID, oDateBeginRequest);

        //        if (oEmployeeData.EmpSubAreaType <= EmployeeSubAreaType.Norm && oDelegateEmployee.EmpSubAreaType >= EmployeeSubAreaType.Shift8 && AREA_OT_SHIFT_DELEGATETO_NORM_ONABSENCE.Contains(oEmployeeData.OrgAssignment.Area) && AREA_OT_SHIFT_DELEGATETO_NORM_ONABSENCE.Contains(oDelegateEmployee.OrgAssignment.Area))
        //        {
        //            isShiftDelegateToNorm = true;
        //            oldWS = oEmployeeData.GetDailyWorkSchedule(OTDate, false);
        //            isNewBeginOverEnd = oldWS.WorkEndTime < oDailyWS.WorkBeginTime;
        //        }
        //    }

        //    //2013-01-18 �ҡ��� User �繾�ѡ�ҹ�� ��Тͤ���ͷա�͹���ҷӧҹ �������ҷӧҹ�ͧ�ѹ��͹˹���ҤԴ�ͷ���ѧ��ԡ�ҹ
        //    if (oInfotype0007.WFRule.StartsWith("S") && oDateBeginRequest.TimeOfDay < oDailyWS.WorkBeginTime)
        //    {
        //        OTDate = oDateBeginRequest.AddDays(-1).Date;
        //        EmployeeData oEmp = new EmployeeData(EmployeeID,OTDate);
        //        oDailyWS = oEmp.GetDailyWorkSchedule(OTDate);
        //        oInfotype0007 = EmployeeData.GetEmployeeWF(EmployeeID, OTDate);
        //    }
            
        //    Result.EmployeeID = EmployeeID;
        //    Result.oTimeEval = new List<TimeEval>();

        //    #region �觪�ǧ���ҷ����ӹǳ�ͷ� ������͹䢵�ҧ�
        //    //�������� DayOff/Holiday
        //    if (!oDailyWS.IsDayOff && oMWS.GetWSCode(OTDate.Day, false) != "1")
        //    {
        //        //����繾�ѡ�ҹẺ Flex �е�ͧ�����仵��������ǧ���ҡ�÷ӧҹ�˹
        //        if (oEmployeeData.EmpSubAreaType == EmployeeSubAreaType.Flex)
        //        {
        //            WorkBeginDateTime = new DateTime();
        //            WorkEndDateTime = new DateTime();
        //            //HRTMManagement.GetWorkingTimeOfFlex(OTDate, oInfotype0007, ref WorkBeginDateTime, ref WorkEndDateTime);

        //            HRTMManagement.CreateInstance("zz").GetWorkingTimeOfFlex(OTDate, oInfotype0007, ref WorkBeginDateTime, ref WorkEndDateTime);
        //        }
        //        //else if (isShiftDelegateToNorm)
        //        //{
        //        //    WorkBeginDateTime = OTDate.Add(oDelegateWS.WorkBeginTime);
        //        //    WorkEndDateTime = oDelegateWS.WorkEndTime < oDelegateWS.WorkBeginTime ? OTDate.Add(oDelegateWS.WorkEndTime).AddDays(1) : OTDate.Add(oDelegateWS.WorkEndTime);
        //        //}
        //        //����繾�ѡ�ҹ Norm ��� Shift ��� Set ������ҷӧҹ���Ԣͧ�ѹ���
        //        else
        //        {
        //            WorkBeginDateTime = OTDate.Add(oDailyWS.WorkBeginTime);
        //            WorkEndDateTime = oDailyWS.WorkEndTime < oDailyWS.WorkBeginTime ? OTDate.Add(oDailyWS.WorkEndTime).AddDays(1) : OTDate.Add(oDailyWS.WorkEndTime);
        //        }

        //        //����Ѻ��ѡ�ҹ������ Norm/Flex
        //        //��͹�����ѧ���ҷӧҹ ���� 1.5 ���
        //        if (!isShiftDelegateToNorm && (oEmployeeData.EmpSubAreaType == EmployeeSubAreaType.Norm || oEmployeeData.EmpSubAreaType == EmployeeSubAreaType.Flex))
        //        {
        //            //��͹���ҷӧҹ
        //            OTPeriod = new DateTimeIntersec();
        //            OTPeriod.Begin1 = oDateBeginRequest;
        //            OTPeriod.End1 = WorkBeginDateTime;
        //            OTx15Period.Add(OTPeriod);

        //            //��ѧ���ҷӧҹ
        //            OTPeriod = new DateTimeIntersec();
        //            OTPeriod.Begin1 = WorkEndDateTime;
        //            OTPeriod.End1 = oDateEndRequest;
        //            OTx15Period.Add(OTPeriod);
        //        }
        //        else if (isShiftDelegateToNorm)
        //        {
        //            if (isNewBeginOverEnd)
        //            {
        //               //��͹���ҷӧҹ 价Ѻ��͹�Ѻ���ҷӧҹ��� ��ͧ�觪�ǧ�ѡ���§�͡
        //                TimeSpan Break1200 = new TimeSpan(12, 0, 0);
        //                TimeSpan Break1300 = new TimeSpan(13, 0, 0);

        //                OTPeriod = new DateTimeIntersec();
        //                OTPeriod.Begin1 = oDateBeginRequest;
        //                OTPeriod.End1 = oDateBeginRequest.Date.Add(Break1200);
        //                OTx15Period.Add(OTPeriod);

        //                OTPeriod = new DateTimeIntersec();
        //                OTPeriod.Begin1 = oDateBeginRequest.Date.Add(Break1300);
        //                OTPeriod.End1 = WorkBeginDateTime;
        //                OTx15Period.Add(OTPeriod);
        //            }
        //            else
        //            {
        //                //��͹���ҷӧҹ
        //                OTPeriod = new DateTimeIntersec();
        //                OTPeriod.Begin1 = oDateBeginRequest;
        //                OTPeriod.End1 = WorkBeginDateTime;
        //                OTx15Period.Add(OTPeriod);
        //            }

        //            //��ѧ���ҷӧҹ
        //            OTPeriod = new DateTimeIntersec();
        //            OTPeriod.Begin1 = WorkEndDateTime;
        //            OTPeriod.End1 = oDateEndRequest;
        //            OTx15Period.Add(OTPeriod);
        //        }
        //        //����Ѻ��ѡ�ҹ������ Shift12
        //        //��ѧ���ҷӧҹ 1.5 ���
        //        else
        //        {
        //            //��ѧ���ҷӧҹ
        //            OTPeriod = new DateTimeIntersec();
        //            OTPeriod.Begin1 = WorkEndDateTime;
        //            OTPeriod.End1 = oDateEndRequest;
        //            OTx15Period.Add(OTPeriod);
        //        }
        //    }
        //    //������ѹ DayOff/Holiday
        //    else
        //    {
        //        //����繾�ѡ�ҹ Norm 
        //        //��� Set ���ҷӧҹ�� ���ҷӧҹ�ͧ�ѹ WorkingDay ��͹˹�ҷ��������ش
        //        if (!isShiftDelegateToNorm && oEmployeeData.EmpSubAreaType == EmployeeSubAreaType.Norm)
        //        {
        //            DateTime tmpDate = OTDate;
        //            DailyWS tmpDWS = oEmployeeData.GetDailyWorkSchedule(tmpDate, true);
        //            MonthlyWS tmpMWS = MonthlyWS.GetCalendar(EmployeeID, tmpDate.Year, tmpDate.Month);

        //            while (tmpDWS.IsDayOff || tmpMWS.GetWSCode(tmpDate.Day, false) == "1")
        //            {
        //                tmpDate = tmpDate.AddDays(-1);
        //                if(tmpMWS.WS_Year != tmpDate.Year || tmpMWS.WS_Month != tmpDate.Month)
        //                    tmpMWS = MonthlyWS.GetCalendar(EmployeeID, tmpDate);
        //                EmployeeData tmpEmployee = new EmployeeData(oEmployeeData.EmployeeID, tmpDate);
        //                tmpDWS = tmpEmployee.GetDailyWorkSchedule(tmpDate, true);
        //                tmpEmployee = null;
        //            }

        //            if (tmpDate < oInfotype0007.BeginDate)
        //            {
        //                tmpDate = OTDate.AddDays(1);
        //                tmpDWS = oEmployeeData.GetDailyWorkSchedule(tmpDate, true);
        //                tmpMWS = MonthlyWS.GetCalendar(EmployeeID, tmpDate.Year, tmpDate.Month);
        //                while (tmpDWS.IsDayOff || tmpMWS.GetWSCode(tmpDate.Day, false) == "1")
        //                {
        //                    tmpDate = tmpDate.AddDays(1);
        //                    if (tmpMWS.WS_Year != tmpDate.Year || tmpMWS.WS_Month != tmpDate.Month)
        //                        tmpMWS = MonthlyWS.GetCalendar(EmployeeID, tmpDate);
        //                    EmployeeData tmpEmployee = new EmployeeData(oEmployeeData.EmployeeID, tmpDate);
        //                    tmpDWS = tmpEmployee.GetDailyWorkSchedule(tmpDate, true);
        //                    tmpEmployee = null;
        //                }
        //            }

        //            WorkBeginDateTime = OTDate.Add(tmpDWS.WorkBeginTime);
        //            WorkEndDateTime = tmpDWS.WorkEndTime < tmpDWS.WorkBeginTime ? OTDate.Add(tmpDWS.WorkEndTime).AddDays(1) : OTDate.Add(tmpDWS.WorkEndTime);
        //        }
        //        //����繾�ѡ�ҹ Flex Set ���ҷӧҹ��ѹ ����� 8:30-17:30
        //        if (oEmployeeData.EmpSubAreaType == EmployeeSubAreaType.Flex)
        //        {
        //            WorkBeginDateTime = OTDate.Add(new TimeSpan(8, 00, 0));
        //            WorkEndDateTime = OTDate.Add(new TimeSpan(17, 00, 0));
        //        }
                   

        //        //����繾�ѡ�ҹ Norm/Flex
        //        //��͹���ҷӧҹ�� 3 ���/����ҷӧҹ�� 1 ���/��ѧ���ҷӧҹ�� 3 ���
        //        if (!isShiftDelegateToNorm && (oEmployeeData.EmpSubAreaType == EmployeeSubAreaType.Norm || oEmployeeData.EmpSubAreaType == EmployeeSubAreaType.Flex))
        //        {
        //            //��͹���ҷӧҹ
        //            if (oDateBeginRequest < WorkBeginDateTime)
        //            {
        //                OTPeriod = new DateTimeIntersec();
        //                OTPeriod.Begin1 = oDateBeginRequest;
        //                OTPeriod.End1 = WorkBeginDateTime;
        //                OTx3Period.Add(OTPeriod);
        //            }

        //            //����ҷӧҹ
        //            if (WorkBeginDateTime <= oDateEndRequest && WorkEndDateTime >= oDateBeginRequest)
        //            {
        //                TimeSpan Break1200 = new TimeSpan(12, 0, 0);
        //                TimeSpan Break1300 = new TimeSpan(13, 0, 0);
        //                OTPeriod = new DateTimeIntersec();
   
        //                if (oDateBeginRequest >= WorkBeginDateTime)
        //                    OTPeriod.Begin1 = oDateBeginRequest;
        //                else
        //                    OTPeriod.Begin1 = WorkBeginDateTime;

        //                //������ҷ��͡�͹���§�������ش��ѧ���§ ������͡�� 2 ��ǧ
        //                if (OTPeriod.Begin1 <= OTPeriod.Begin1.Date.Add(Break1200) && oDateEndRequest > OTPeriod.Begin1.Date.Add(Break1200))
        //                {
        //                    //������ҷ�����ҡѺ���§�ʹ� ���Ѵ��ǧ��� 1 ���
        //                    if (OTPeriod.Begin1 < OTPeriod.Begin1.Date.Add(Break1200))
        //                    {
        //                        //��ǧ��� 1 Begin   : oDateBeginRequest/WorkBeginDateTime
        //                        //      End     : Break1200
        //                        OTPeriod.End1 = oDateBeginRequest.Date.Add(Break1200);
        //                        OTx1Period.Add(OTPeriod);
        //                    }

        //                    //��ǧ��� 2 Begin   : Break1300
        //                    //      End     : oDateEndRequest/WorkEndDateTime
        //                    OTPeriod = new DateTimeIntersec();
        //                    OTPeriod.Begin1 = oDateBeginRequest.Date.Add(Break1300);
        //                    if (oDateEndRequest <= WorkEndDateTime)
        //                        OTPeriod.End1 = oDateEndRequest;
        //                    else
        //                        OTPeriod.End1 = WorkEndDateTime;
        //                    OTx1Period.Add(OTPeriod);
        //                }
        //                //������ҷ��͡�͹���§ ������ش��͹������ҡѺ���§ �����ӹǳ�����ԧ
        //                else
        //                {
        //                    if (oDateEndRequest <= WorkEndDateTime)
        //                        OTPeriod.End1 = oDateEndRequest;
        //                    else
        //                        OTPeriod.End1 = WorkEndDateTime;
        //                    OTx1Period.Add(OTPeriod);
        //                }
        //            }

        //            //��ѧ���ҷӧҹ
        //            if (oDateEndRequest > WorkBeginDateTime)
        //            {
        //                OTPeriod = new DateTimeIntersec();
        //                OTPeriod.Begin1 = WorkEndDateTime;
        //                OTPeriod.End1 = oDateEndRequest;
        //                OTx3Period.Add(OTPeriod);
        //            }
        //        }
        //        //����繾�ѡ�ҹ Shift12
        //        //8 ��������á�� 1 ��� ��ѧ�ҡ��� 3 ���
        //        else
        //        {
                    
        //            //�ҡ�ӧҹ�ҡ���� 8 �������
        //            TimeSpan tp = oDateEndRequest - oDateBeginRequest;
        //            if (tp.TotalHours > 8)
        //            {
        //                //8 ��������á �� 1 ���
        //                OTPeriod = new DateTimeIntersec();
        //                OTPeriod.Begin1 = oDateBeginRequest;
        //                OTPeriod.End1 = oDateBeginRequest.Add(new TimeSpan(8,0,0));
        //                OTx1Period.Add(OTPeriod);

        //                //��ѧ�ҡ����� 3 ���
        //                OTPeriod = new DateTimeIntersec();
        //                OTPeriod.Begin1 = oDateBeginRequest.Add(new TimeSpan(8,0,1));
        //                OTPeriod.End1 = oDateEndRequest;
        //                OTx3Period.Add(OTPeriod);
        //            }
        //            //�ҡ�ӧҹ���¡���������ҡѺ 8 �������
        //            else
        //            {
        //                OTPeriod = new DateTimeIntersec();
        //                OTPeriod.Begin1 = oDateBeginRequest;
        //                OTPeriod.End1 = oDateEndRequest;
        //                OTx1Period.Add(OTPeriod);
        //            }
                    
        //        }
        //    }
        //    #endregion

        //    #region �ӹǳ�ӹǹ��������ͷ�
        //    foreach (DateTimeIntersec item in OTx1Period)
        //    {
        //        item.Begin2 = oDateBeginRequest;
        //        item.End2 = oDateEndRequest;
        //        if (item.IsIntersecOT())
        //        {
        //            item.Intersection();
        //            OTx1 += ((TimeSpan)(item.EndBound - item.BeginBound)).TotalMinutes;
        //            AddTimeEval(item.BeginBound, item.EndBound, "2010", ref Result);
        //        }
        //    }

        //    foreach (DateTimeIntersec item in OTx15Period)
        //    {
        //        item.Begin2 = oDateBeginRequest;
        //        item.End2 = oDateEndRequest;
        //        if (item.IsIntersecOT())
        //        {
        //            item.Intersection();
        //            OTx1_5 += ((TimeSpan)(item.EndBound - item.BeginBound)).TotalMinutes;
        //            AddTimeEval(item.BeginBound, item.EndBound, "2015", ref Result);
        //        }
        //    }

        //    foreach (DateTimeIntersec item in OTx3Period)
        //    {
        //        item.Begin2 = oDateBeginRequest;
        //        item.End2 = oDateEndRequest;
        //        if (item.IsIntersecOT())
        //        {
        //            item.Intersection();
        //            OTx3 += ((TimeSpan)(item.EndBound - item.BeginBound)).TotalMinutes;
        //            AddTimeEval(item.BeginBound, item.EndBound, "2030", ref Result);
        //        }
        //    }
        //    #endregion

        //    //����� DailyOTRequest
        //    if (oOTType == 0)
        //    {
        //        Result.RequestOTHour10 = Convert.ToDecimal(OTx1);
        //        Result.RequestOTHour15 = Convert.ToDecimal(OTx1_5);
        //        Result.RequestOTHour30 = Convert.ToDecimal(OTx3);
        //    }
        //    //����� MonthlyOTEval
        //    else if (oOTType == 1)
        //    {
        //        Result.EvaOTHour10 = Convert.ToDecimal(OTx1);
        //        Result.EvaOTHour15 = Convert.ToDecimal(OTx1_5);
        //        Result.EvaOTHour30 = Convert.ToDecimal(OTx3);
        //    }
        //    //����� MonthlyOTFinal
        //    else if (oOTType == 2)
        //    {
        //        Result.FinalOTHour10 = Convert.ToDecimal(OTx1);
        //        Result.FinalOTHour15 = Convert.ToDecimal(OTx1_5);
        //        Result.FinalOTHour30 = Convert.ToDecimal(OTx3);
        //    }

        //    Result.BeginDate = oDateBeginRequest.Date;
        //    Result.EndDate = oDateBeginRequest.Date;

        //    return Result;
        //}

        //enum CalculateOTType
        //{ 
        //    WWW,
        //    WWO,
        //    WOO,
        //    WOW,
        //    OOO,
        //    OOW,
        //    OWW,
        //    OWO
        //}

        //static void AddTimeEval(DateTime BeginDate, DateTime EndDate,String WageType, ref DailyOTLog _DailyOTLog)
        //{
        //    if (EndDate - BeginDate <= TimeSpan.MinValue
        //        || EndDate - BeginDate <= new TimeSpan() || BeginDate == EndDate) return;
        //    TimeEval oTimeEval = new TimeEval();
        //    oTimeEval.EmployeeID = _DailyOTLog.EmployeeID;
        //    oTimeEval.BeginDate = BeginDate;
        //    oTimeEval.EndDate = EndDate;
        //    oTimeEval.WageHour = Convert.ToDecimal(((TimeSpan)(EndDate - BeginDate)).TotalHours);
        //    oTimeEval.WageType = WageType;
        //    _DailyOTLog.oTimeEval.Add(oTimeEval);
        //}

        //static DailyWS PreviousWorkingDay(String EmpID, DailyWS _DailyWS, DateTime CurrentDate, out DateTime DailyWSNormalBeginTime, out DateTime DailyWSNormalEndTime)
        //{
        //    DailyWSNormalBeginTime = DateTime.MinValue;
        //    DailyWSNormalEndTime = DateTime.MinValue;

        //    EmployeeData _EmployeeData = EmployeeData.GetUserSetting(EmpID).Employee;
        //    for (DateTime iDate = CurrentDate; iDate > DateTime.MinValue; iDate = iDate.AddDays(-1))
        //    {
        //        _DailyWS = _EmployeeData.GetDailyWorkSchedule(iDate);
        //        if (_DailyWS.IsDayOff || MonthlyWS.GetCalendar(EmpID, CurrentDate).GetWSCode(CurrentDate.Day, false) == "1") continue;
        //        DailyWSNormalBeginTime = _DailyWS.BeginDate.Add(_DailyWS.WorkBeginTime);
        //        DailyWSNormalEndTime = _DailyWS.BeginDate.Add(_DailyWS.WorkEndTime);
        //        return _DailyWS;
        //    }
        //    return null;
        //}

        //static DailyWS PreviousWorkingDay(String EmpID, DailyWS _DailyWS, DateTime CurrentDate)
        //{
        //    DateTime DailyWSNormalBeginTime;
        //    DateTime DailyWSNormalEndTime;
        //    return PreviousWorkingDay(EmpID, _DailyWS, CurrentDate, out DailyWSNormalBeginTime, out DailyWSNormalEndTime);
        //}

        //static void MatchingOT(String EmpID,MonthlyWS MWS,DailyWS _DailyWS, DateTime BeginDate, DateTime EndDate,DateTime EvalBegin,DateTime EvalEnd, DailyOTLog oDailyOTLog)
        //{
        //    OTClock otClock = new OTClock();
        //    DailyOTLog tmpDailyOTLog = new DailyOTLog();
        //    DateTimeIntersec _DateTimeIntersec = new DateTimeIntersec();
        //    _DateTimeIntersec.Begin1 = BeginDate;
        //    _DateTimeIntersec.End1 = EndDate;
        //    _DateTimeIntersec.Begin2 = EvalBegin;
        //    _DateTimeIntersec.End2 = EvalEnd;
            
        //    if (_DateTimeIntersec.IsIntersecOT())
        //    {
        //        otClock.MachineBegin = EvalBegin;
        //        otClock.MachineEnd = EvalEnd;

        //        // use for job6 retrive timeEval from SAP else Plase Comment!!!!! Kiattiwat 20-10-2011
        //        //tmpDailyOTLog = CalculateDailyOT(EmpID, _DailyWS, MWS, EvalBegin, EvalEnd, 1);
        //        //oDailyOTLog.EvaOTHour10 += tmpDailyOTLog.EvaOTHour10;
        //        //oDailyOTLog.EvaOTHour15 += tmpDailyOTLog.EvaOTHour15;
        //        //oDailyOTLog.EvaOTHour30 += tmpDailyOTLog.EvaOTHour30;

        //        _DateTimeIntersec.Intersection();
        //        if (_DateTimeIntersec.BeginBound != _DateTimeIntersec.EndBound)
        //        {
        //            tmpDailyOTLog = CalculateDailyOT(EmpID, _DailyWS, MWS, _DateTimeIntersec.BeginBound, _DateTimeIntersec.EndBound, 2);
        //            oDailyOTLog.FinalOTHour10 += tmpDailyOTLog.FinalOTHour10;
        //            oDailyOTLog.FinalOTHour15 += tmpDailyOTLog.FinalOTHour15;
        //            oDailyOTLog.FinalOTHour30 += tmpDailyOTLog.FinalOTHour30;

        //            otClock.IntersecBegin = _DateTimeIntersec.BeginBound;
        //            otClock.IntersecEnd = _DateTimeIntersec.EndBound;
        //            oDailyOTLog.OTClockINOUT.Add(otClock);
        //        }
        //    }

        //}

        //public static void MatchingOT(String EmpID, List<TimePairArchive> oTimePairArchive, DateTime BeginDate, DateTime EndDate, DailyOTLog oDailyOTLog)
        //{
        //    MonthlyWS MWS = MonthlyWS.GetCalendar(EmpID, BeginDate, BeginDate.Year, BeginDate.Month, true);
        //    EmployeeData EmpData = EmployeeData.GetUserSetting(EmpID).Employee;

        //    DailyWS _DailyWS = EmpData.GetDailyWorkSchedule(BeginDate,true);
        //    DateTime WorkBegin, WorkEnd;
        //    foreach (TimePairArchive timePairArchive in oTimePairArchive)
        //    {
        //        _DailyWS = EmpData.GetDailyWorkSchedule(timePairArchive.Date, true);
        //        if (_DailyWS.IsDayOff || MWS.GetWSCode(timePairArchive.Date.Day, false) == "1")
        //        {
        //            WorkBegin = timePairArchive.Date.Add(new TimeSpan(8, 0, 0));
        //            WorkEnd = timePairArchive.Date.Add(new TimeSpan(17, 0, 0));
        //        }
        //        else
        //        {
        //            WorkBegin = timePairArchive.Date.Add(_DailyWS.WorkBeginTime);
        //            WorkEnd = _DailyWS.WorkBeginTime > _DailyWS.WorkEndTime ? timePairArchive.Date.AddDays(1).Add(_DailyWS.WorkEndTime) : timePairArchive.Date.Add(_DailyWS.WorkEndTime);    
        //        }

        //        MatchingOT(EmpID, MWS, _DailyWS,WorkBegin,WorkEnd, timePairArchive.ClockIN, timePairArchive.ClockOUT, oDailyOTLog);
        //    }
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="EmpID"></param>
        ///// <param name="oTimeEval"></param>
        ///// <param name="BeginDate">Request OT BeginDate</param>
        ///// <param name="EndDate">Request OT  EndDate</param>
        ///// <param name="oDailyOTLog"></param>
        //public static void MatchingOT(String EmpID, List<TimeEval> oTimeEval, DateTime BeginDate, DateTime EndDate, DailyOTLog oDailyOTLog)
        //{
        //    MonthlyWS MWS = MonthlyWS.GetCalendar(EmpID, BeginDate, BeginDate.Year, BeginDate.Month, true);
        //    EmployeeData EmpData = EmployeeData.GetUserSetting(EmpID).Employee;
        //    DailyWS _DailyWS = EmpData.GetDailyWorkSchedule(BeginDate, true);
        //    foreach (TimeEval timeEval in oTimeEval)
        //        MatchingOT(EmpID, MWS, _DailyWS, BeginDate, EndDate, timeEval.BeginDate, timeEval.EndDate, oDailyOTLog);
            
        //}

        //public static void MatchingOT(List<TimeEval> oTimeEval, DateTime BeginDate, DateTime EndDate, DailyOTLog oDailyOTLog)
        //{
        //    DateTimeIntersec oDateTimeIntersec = new DateTimeIntersec();
        //    oDateTimeIntersec.Begin1 = BeginDate;
        //    oDateTimeIntersec.End1 = EndDate;

        //    foreach (TimeEval timeEval in oTimeEval)
        //    {
        //        oDateTimeIntersec.Begin2 = timeEval.BeginDate;
        //        oDateTimeIntersec.End2 = timeEval.EndDate;
        //        if (oDateTimeIntersec.IsIntersecOT())
        //        {
        //            oDateTimeIntersec.Intersection();
        //            switch (timeEval.WageType)
        //            {
        //                case "2010":
        //                    oDailyOTLog.FinalOTHour10 += Convert.ToDecimal(((TimeSpan)(oDateTimeIntersec.EndBound - oDateTimeIntersec.BeginBound)).TotalMinutes);
        //                    break;
        //                case "2015":
        //                    oDailyOTLog.FinalOTHour15 += Convert.ToDecimal(((TimeSpan)(oDateTimeIntersec.EndBound - oDateTimeIntersec.BeginBound)).TotalMinutes);
        //                    break;
        //                case "2030":
        //                    oDailyOTLog.FinalOTHour30 += Convert.ToDecimal(((TimeSpan)(oDateTimeIntersec.EndBound - oDateTimeIntersec.BeginBound)).TotalMinutes);
        //                    break;
        //            }
        //        }
        //    }
        //}

        //public static void MatchingOT(String EmpID, DateTime BeginDate, DateTime EndDate, DailyOTLog oDailyOTLog)
        //{
        //    if (oDailyOTLog.EvaTimePairs == null) return;
        //    MonthlyWS MWS = MonthlyWS.GetCalendar(EmpID, BeginDate, BeginDate.Year, BeginDate.Month, true);
        //    EmployeeData EmpData = EmployeeData.GetUserSetting(EmpID).Employee;
        //    DailyWS _DailyWS = EmpData.GetDailyWorkSchedule(BeginDate, true);
        //    DateTime EvalBegin, EvalEnd;
        //    foreach (String strEvalPair in oDailyOTLog.EvaTimePairs.Split('|'))
        //    {
        //        if (DateTime.TryParseExact(strEvalPair.Split('-')[0], "yyyyMMdd HHmm", new CultureInfo("en-US"),DateTimeStyles.None,out EvalBegin) && 
        //            DateTime.TryParseExact(strEvalPair.Split('-')[1], "yyyyMMdd HHmm", new CultureInfo("en-US"), DateTimeStyles.None,out EvalEnd))
        //            MatchingOT(EmpID, MWS, _DailyWS, BeginDate, EndDate, EvalBegin, EvalEnd, oDailyOTLog);
        //    }
        //}

        //public static List<TimeEval> SimulateTimeEval(DateTime beginDate, DateTime endDate)
        //{
        //    List<TimePairArchive> oTimePairArchive = TimesheetManagement.LoadTimePairArchive(beginDate, endDate);
            
        //    MonthlyWS MWS;
        //    EmployeeData EmpData;
        //    DateTime CurrentDate;
        //    DailyWS DWS;
        //    DailyOTLog DOTLog;
        //    List<TimeEval> TimeEvalList = new List<TimeEval>();
        //    for (int i = beginDate.Month; i <= endDate.Month; i++)
        //    {
        //        CurrentDate = new DateTime(beginDate.AddMonths(-beginDate.Month).AddMonths(i).Year, beginDate.AddMonths(-beginDate.Month).AddMonths(i).Month, 1);
        //        foreach (TimePairArchive item in oTimePairArchive)
        //        {
        //            if (item.ClockIN == null || item.ClockOUT == null
        //                || item.ClockIN == DateTime.MinValue || item.ClockOUT == DateTime.MinValue
        //                || item.ClockIN==item.ClockOUT) continue;
        //            MWS = MonthlyWS.GetCalendar(item.EmployeeID, CurrentDate, true);
        //            EmpData = new EmployeeData(item.EmployeeID);
        //            DWS = EmpData.GetDailyWorkSchedule(item.Date, true);
        //            DOTLog = CalculateDailyOT(item.EmployeeID, DWS, MWS, item.ClockIN, item.ClockOUT, 9);
        //            foreach(TimeEval iTimeEval in DOTLog.oTimeEval)
        //                if (!String.IsNullOrEmpty(iTimeEval.WageType))
        //                    TimeEvalList.Add(iTimeEval);
        //        }
        //    }
        //    return TimeEvalList;
        //}

        //static double CalculateBreakCode(string BreakCode, DateTime DateBeginRequest, DateTime DateEndRequest, DateTime BeginDate)
        //{
        //    if (BreakCode.ToUpper() == "NOON")
        //    {
        //        DateTime BreakStart = BeginDate.AddHours(-BeginDate.Hour + 4);
        //        DateTime BreakFinish = BeginDate.AddHours(-BeginDate.Hour + 5);

        //        if (DateBeginRequest < BreakStart && BreakFinish < DateEndRequest)
        //            return 60;
        //        else if (DateBeginRequest < BreakStart && DateEndRequest > BreakStart)
        //            return 60;
        //        else if (DateBeginRequest < BreakFinish && DateEndRequest > BreakFinish)
        //            return 60;
        //        else if (DateBeginRequest > BreakStart && DateEndRequest < BreakFinish)
        //            return 60;
        //        else
        //            return 0;
        //    }
        //    return 0;
        //}

        //public List<MonthlyOT> CalculateMonthlyOT(List<MonthlyOT> listData, MonthlyWS MWS)
        //{
        //    List<MonthlyOT> list = new List<MonthlyOT>();
        //    Dictionary<int, Decimal> additionalTNOM = new Dictionary<int, decimal>();
        //    Dictionary<int, Decimal> otTNOM = new Dictionary<int, decimal>();
        //    List<int> isLoadTNOM = new List<int>();
        //    for (int index = 0; index < listData.Count; index++)
        //    {
        //        MonthlyOT item = listData[index];
        //        TimeSpan time1, time2;
        //        time1 = item.DailyOT.BeginTime;
        //        time2 = item.DailyOT.EndTime;
        //        if (time2 <= time1)
        //        {
        //            time2 = time2.Add(new TimeSpan(1, 0, 0, 0));
        //        }
        //        bool lEnd = false;
        //        do
        //        {
        //            item.DailyOT.Calculate(MWS);
        //            item.BeginTime = item.DailyOT.BeginTime;
        //            item.EndTime = item.DailyOT.EndTime;
        //            if (item.DailyOT.IsTNOM)
        //            {
        //                #region " ONLY TNOM "
        //                if (!additionalTNOM.ContainsKey(item.DailyOT.OTDate.Day))
        //                {
        //                    additionalTNOM.Add(item.DailyOT.OTDate.Day, 0);
        //                }
        //                if (!otTNOM.ContainsKey(item.DailyOT.OTDate.Day))
        //                {
        //                    otTNOM.Add(item.DailyOT.OTDate.Day, 0);
        //                }
        //                if (!isLoadTNOM.Contains(item.DailyOT.OTDate.Day))
        //                {
        //                    List<DailyOT> otList = LoadDailyOT(item.DailyOT.EmployeeID, MWS, item.DailyOT.OTDate);
        //                    additionalTNOM[item.DailyOT.OTDate.Day] += Summary(otList, item.DailyOT);
        //                    isLoadTNOM.Add(item.DailyOT.OTDate.Day);
        //                }
        //                additionalTNOM[item.DailyOT.OTDate.Day] += item.DailyOT.OTHours;
        //                if (item.DailyOT.DWS.IsDayOff || MWS.GetWSCode(item.DailyOT.OTDate.Day, false) != "")
        //                {
        //                    item.DailyOT.OTHours = additionalTNOM[item.DailyOT.OTDate.Day] - otTNOM[item.DailyOT.OTDate.Day];
        //                }
        //                else if (additionalTNOM[item.DailyOT.OTDate.Day] > 9)
        //                {
        //                    item.DailyOT.OTHours = additionalTNOM[item.DailyOT.OTDate.Day] - 9 - otTNOM[item.DailyOT.OTDate.Day];
        //                }
        //                else
        //                {
        //                    item.DailyOT.OTHours = 0.0M;
        //                }
        //                otTNOM[item.DailyOT.OTDate.Day] += item.DailyOT.OTHours;

        //                list.Add(item);
        //                #endregion
        //            }
        //            else
        //            {
        //                //if (item.OTHours > 0)
        //                //{
        //                //    list.Add(item);
        //                //}

        //                if (item.DailyOT.BeginTime.Days > 0)
        //                {
        //                    item.DailyOT.OTDate = item.DailyOT.OTDate.AddDays(item.DailyOT.BeginTime.Days);
        //                    item.DailyOT.BeginTime = new TimeSpan(item.DailyOT.BeginTime.Hours, item.DailyOT.BeginTime.Minutes, item.DailyOT.BeginTime.Seconds);
        //                    item.DailyOT.EndTime = new TimeSpan(item.DailyOT.EndTime.Hours, item.DailyOT.EndTime.Minutes, item.DailyOT.EndTime.Seconds);
        //                    time2 = new TimeSpan(time2.Hours, time2.Minutes, time2.Seconds);
        //                    continue;
        //                }
        //                list.Add(item);
        //            }
        //            if (item.DailyOT.EndTime >= time2)
        //            {
        //                break;
        //            }
        //            else
        //            {
        //                MonthlyOT mot = new MonthlyOT();
        //                DailyOT buffer = new DailyOT();
        //                buffer.Description = item.DailyOT.Description;
        //                buffer.EmployeeID = item.DailyOT.EmployeeID;
        //                buffer.OTItemTypeID = item.DailyOT.OTItemTypeID;
        //                buffer.IONumber = item.DailyOT.IONumber;
        //                buffer.ActivityCode = item.DailyOT.ActivityCode;
        //                if (item.DailyOT.EndTime.Days > 0)
        //                {
        //                    time2 = new TimeSpan(time2.Hours, time2.Minutes, time2.Seconds);
        //                    buffer.BeginTime = new TimeSpan(item.DailyOT.EndTime.Hours, item.DailyOT.EndTime.Minutes, item.DailyOT.EndTime.Seconds);
        //                    buffer.EndTime = time2;
        //                    buffer.OTDate = item.DailyOT.OTDate.AddDays(item.DailyOT.EndTime.Days);
        //                    if (buffer.OTDate.Month != MWS.WS_Month || buffer.OTDate.Year != MWS.WS_Year)
        //                    {
        //                        MWS = MonthlyWS.GetCalendar(buffer.EmployeeID, buffer.OTDate.Year, buffer.OTDate.Month);
        //                    }
        //                }
        //                else
        //                {
        //                    buffer.BeginTime = item.DailyOT.EndTime;
        //                    buffer.EndTime = time2;
        //                    buffer.OTDate = item.DailyOT.OTDate;
        //                }
        //                mot.BeginTime = buffer.BeginTime;
        //                mot.DailyAssignFrom = item.DailyAssignFrom;
        //                mot.DailyOT = buffer;
        //                mot.DailyRequestNo = item.DailyRequestNo;
        //                mot.Description = item.Description;
        //                mot.EndTime = buffer.EndTime;
        //                item = mot;
        //            }
        //        } while (!lEnd);
        //    }
        //    return list;
        //}

        //private static decimal Summary(List<DailyOT> otList, DailyOT current)
        //{
        //    Decimal oReturn = 0.0M;
        //    foreach (DailyOT item in otList)
        //    {
        //        if (item.Equals(current))
        //        {
        //            continue;
        //        }
        //        if (item.OTDate != current.OTDate)
        //        {
        //            continue;
        //        }
        //        oReturn += item.OTHours;
        //    }
        //    return oReturn;
        //}

        //public static PlannedOT LoadFirstPlannedOT(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        //{
        //   return LoadFirstPlannedOT(EmployeeID, BeginDate, EndDate);
        //}

        //public static List<PlannedOT> LoadPlannedOT(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        //{
        //    return LoadPlannedOT(EmployeeID, BeginDate, EndDate);
        //}

        //public List<DailyOT> LoadDailyOT(string EmployeeID, string Period, MonthlyWS MWS)
        //{
        //    CultureInfo oCL = new CultureInfo("en-US");
        //    List<DailyOT> oReturn = new List<DailyOT>();
        //    List<SaveOTData> otlist;
        //    DateTime oPeriod = DateTime.ParseExact(Period, "yyyyMM", oCL);
        //    //otlist =  HR.TM.ServiceManager.HRTMBuffer.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", true, true);

        //    otlist = ServiceManager.CreateInstance(CompanyCode).ESSData.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", true, true);
        //    List<SaveOTData> buffer = new List<SaveOTData>();
        //    List<string> requestNoList = new List<string>();
        //    List<DailyOT> temp = new List<DailyOT>();
        //    foreach (SaveOTData item in otlist)
        //    {
        //        if (!requestNoList.Contains(item.RequestNo))
        //        {
        //            requestNoList.Add(item.RequestNo);
        //        }
        //        if (item.OTPeriod != Period)
        //        {
        //            continue;
        //        }
        //        DailyOT dailyOT = new DailyOT();
        //        dailyOT.BeginTime = item.BeginTime;
        //        dailyOT.EmployeeID = EmployeeID;
        //        dailyOT.EndTime = item.EndTime;
        //        dailyOT.OTDate = item.OTDate;
        //        dailyOT.IsPosted = false;
        //        dailyOT.Description = item.Description;
        //        dailyOT.OTItemTypeID = item.OTItemTypeID;
        //        dailyOT.IONumber = item.IONumber;
        //        dailyOT.ActivityCode = item.ActivityCode;
        //        temp.Add(dailyOT);
        //    }
        //    oReturn = CalculateDailyOT(temp, MWS);
        //    return oReturn;
        //}

        //public List<DailyOT> LoadDailyOT1(string EmployeeID, string Period)
        //{
        //    CultureInfo oCL = new CultureInfo("en-US");
        //    List<DailyOT> oReturn = new List<DailyOT>();
        //    List<SaveOTData> otlist;
        //    DateTime oPeriod = DateTime.ParseExact(Period, "yyyyMM", oCL);
        //    //otlist = HR.TM.ServiceManager.HRTMBuffer.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", true, true);

        //    otlist = ServiceManager.CreateInstance(CompanyCode).ESSData.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", true, true);
        //    List<SaveOTData> buffer = new List<SaveOTData>();
        //    List<string> requestNoList = new List<string>();
        //    foreach (SaveOTData item in otlist)
        //    {
        //        if (!requestNoList.Contains(item.RequestNo))
        //        {
        //            requestNoList.Add(item.RequestNo);
        //        }
        //        if (item.OTPeriod != Period)
        //        {
        //            continue;
        //        }
        //        DailyOT dailyOT = new DailyOT();
        //        dailyOT.BeginTime = item.BeginTime;
        //        dailyOT.EmployeeID = item.EmployeeID;
        //        dailyOT.EndTime = item.EndTime;
        //        dailyOT.OTDate = item.OTDate;
        //        dailyOT.OTHours = item.OTHours;
        //        dailyOT.IsPosted = false;
        //        dailyOT.Description = item.Description;
        //        dailyOT.OTItemTypeID = item.OTItemTypeID;
        //        dailyOT.IONumber = item.IONumber;
        //        dailyOT.ActivityCode = item.ActivityCode;
        //        oReturn.Add(dailyOT);
        //    }
        //    return oReturn;
        //}

        //public List<DailyOT> LoadDailyOT(string EmployeeID, MonthlyWS MWS, DateTime LoadDate)
        //{
        //    return LoadDailyOT(EmployeeID, MWS, LoadDate, LoadDate.AddDays(1));
        //}
        //public List<DailyOT> LoadDailyOT(string EmployeeID, MonthlyWS MWS, DateTime BeginDate, DateTime EndDate)
        //{
        //    List<DailyOT> oReturn = new List<DailyOT>();
        //    List<SaveOTData> otlist;
        //    //otlist = HR.TM.ServiceManager.HRTMBuffer.LoadOTData(EmployeeID, BeginDate, EndDate, "D", true);

        //    otlist = ServiceManager.CreateInstance(CompanyCode).ESSData.LoadOTData(EmployeeID, BeginDate, EndDate, "D", true);
        //    foreach (SaveOTData item in otlist)
        //    {
        //        DailyOT dailyOT = new DailyOT();
        //        dailyOT.BeginTime = item.BeginTime;
        //        dailyOT.EmployeeID = EmployeeID;
        //        dailyOT.EndTime = item.EndTime;
        //        dailyOT.OTDate = item.OTDate;
        //        dailyOT.IsPosted = false;
        //        dailyOT.IsSummary = item.IsSummary;
        //        dailyOT.Description = item.Description;
        //        dailyOT.AssignFrom = item.AssignFrom;
        //        dailyOT.IONumber = item.IONumber;
        //        dailyOT.ActivityCode = item.ActivityCode;
        //        dailyOT.Calculate(MWS,true);
        //        oReturn.Add(dailyOT);
        //    }

        //    //otlist = HR.TM.ServiceManager.HRTMService.LoadOTData(EmployeeID, BeginDate, EndDate, "D", true);

        //    otlist = ServiceManager.CreateInstance(CompanyCode).ERPData.LoadOTData(EmployeeID, BeginDate, EndDate, "D", true);
        //    foreach (SaveOTData item in otlist)
        //    {
        //        DailyOT dailyOT = new DailyOT();
        //        dailyOT.BeginTime = item.BeginTime;
        //        dailyOT.EmployeeID = EmployeeID;
        //        dailyOT.EndTime = item.EndTime;
        //        dailyOT.OTDate = item.OTDate;
        //        dailyOT.IsPosted = true;
        //        dailyOT.IONumber = item.IONumber;
        //        dailyOT.ActivityCode = item.ActivityCode;
        //        dailyOT.Calculate(MWS,true);
        //        oReturn.Add(dailyOT);
        //    } 
        //    return oReturn;
        //}


        //public static void DeleteDailyOT(DailyOT daily)
        //{
        //   DeleteDailyOT(daily);
        //}

        //public void SaveDailyOT(List<SaveOTData> data, bool isMark, List<string> requestNoList, bool deleteItemNotInList)
        //{
        //    //HR.TM.ServiceManager.HRTMBuffer.SaveOTList(data, isMark, requestNoList, deleteItemNotInList);
        //    ServiceManager.CreateInstance(CompanyCode).ESSData.SaveOTList(data, isMark, requestNoList, deleteItemNotInList);
        //}

        //public static List<DailyOTLog> LoadDailyOTLog(string RequestNo, string EmployeeID,DateTime BeginDate,DateTime EndDate)
        //{
        //    return LoadDailyOTLog(RequestNo, EmployeeID, BeginDate, EndDate);
        //}

        //public static List<DailyOTLog> LoadDailyOTLog(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        //{
        //    return LoadDailyOTLog(EmployeeID, BeginDate, EndDate);
        //}

        //public static List<DailyOTLog> LoadDailyOTLog(string EmployeeID, DateTime BeginDate, DateTime EndDate, DailyOTLogStatus _DailyOTLogStatus)
        //{
        //    return LoadDailyOTLog(EmployeeID, BeginDate, EndDate, _DailyOTLogStatus);
        //}

        //public static List<DailyOTLog> LoadDailyOTLog(string EmployeeID)
        //{
        //    return LoadDailyOTLog(EmployeeID);
        //}


        //public static List<DailyOTLog> LoadOTLogByCriteria(string EmployeeList, string StatusList, DateTime BeginDate, DateTime EndDate)
        //{
        //    return LoadOTLogByCriteria(EmployeeList, StatusList, BeginDate, EndDate);
        //}

        //public static DataTable GetSummaryOTInMonth(string EmployeeID, DateTime BeginDate, DateTime EndDate, string RequestNo)
        //{
        //    return GetSummaryOTInMonth(EmployeeID, BeginDate, EndDate, RequestNo);
        //}
        ///* ====== Add by Han on 15/03/2019 : Create method for transfer data ==================== */
        //public static DataTable GetOTOverLimitPerWeek(string Employeeid, DateTime BeginDate)
        //{
        //    return GetOTOverLimitPerWeek(Employeeid, BeginDate);
        //}
        ///* ====== Add by Han on 10/05/2019 : Create method for transfer data ==================== */
        //public static DataTable GetOTOverLimitPerMonth(string Employeeid, DateTime BeginDate)
        //{
        //    return GetOTOverLimitPerMonth(Employeeid, BeginDate);
        //}
        /* ====================================================================================== */

    }
}