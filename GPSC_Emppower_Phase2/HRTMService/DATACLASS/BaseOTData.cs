using System;
using System.Collections.Generic;
using System.Text;
using ESS.DATA;

namespace ESS.HR.TM.DATACLASS
{
    public abstract class BaseOTData : AbstractObject
    {
        private int __id = 0;
        private string __employeeID = "";
        private DateTime __OTDate = DateTime.MinValue;
        protected TimeSpan __beginTime = TimeSpan.MinValue;
        protected TimeSpan __endTime = TimeSpan.MinValue;
        private string __description = "";
        private Decimal __otHours = 0.0M;

        public BaseOTData()
            : base()
        {
        }

        public int ID
        {
            get
            {
                return __id;
            }
            set
            {
                __id = value;
            }
        }
        public string EmployeeID
        {
            get
            {
                return __employeeID;
            }
            set
            {
                __employeeID = value;
            }
        }
        public DateTime OTDate
        {
            get
            {
                return __OTDate;
            }
            set
            {
                __OTDate = value;
            }
        }
        public TimeSpan BeginTime
        {
            get
            {
                return __beginTime;
            }
            set
            {
                __beginTime = value;
            }
        }
        public TimeSpan EndTime
        {
            get
            {
                return __endTime;
            }
            set
            {
                __endTime = value;
            }
        }
        public string Description
        {
            get
            {
                return __description;
            }
            set
            {
                __description = value;
            }
        }
        public Decimal OTHours
        {
            get
            {
                return __otHours;
            }
            set
            {
                __otHours = value;
            }
        }

    }
}
