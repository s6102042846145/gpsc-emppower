using System;
using System.Collections.Generic;
using System.Text;
using ESS.EMPLOYEE.CONFIG.TM;

namespace ESS.HR.TM.DATACLASS
{
    public class PlanTimeManagement 
    {
        private PlanTimeManagement()
        { 
        }

        public static bool IsTNOM(string ValuationCode)
        {
            return ValuationCode.Trim() == "2";
        }

        public static bool IsNORM(string ValuationCode)
        {
            return ValuationCode.Trim() == "1";
        }

        //public static TimeSpan GetCutoffTime(string ValuationClass)
        //{
        //    if (IsTNOM(ValuationClass) || IsNORM(ValuationClass))
        //    {
        //        return new TimeSpan(0, 0, 0);
        //    }
        //    else
        //    {
        //        return new TimeSpan(7, 30, 0);
        //    }
        //}

        //public static TimeSpan GetPrevCutoffTime(string ValuationClass)
        //{
        //    if (IsTNOM(ValuationClass) || IsNORM(ValuationClass))
        //    {
        //        return new TimeSpan(8, 0, 0);
        //    }
        //    else
        //    {
        //        return new TimeSpan(7, 30, 0);
        //    }
        //}

        //public static void SplitPrevDay(string EmployeeID, DateTime inputDate, ref TimeSpan BeginTime, ref TimeSpan EndTime, ref bool IsPrevDay)
        //{
        //    MonthlyWS MWS = MonthlyWS.GetCalendar(EmployeeID, inputDate.Year, inputDate.Month);
        //    SplitPrevDay(EmployeeID, inputDate, MWS, ref BeginTime, ref EndTime, ref IsPrevDay);
        //}

        //public static void SplitPrevDay(string EmployeeID, DateTime inputDate, MonthlyWS MWS, ref TimeSpan BeginTime, ref TimeSpan EndTime, ref bool IsPrevDay)
        //{
        //    MonthlyWS mws1;
        //    if (inputDate.Year == MWS.WS_Year && inputDate.Month == MWS.WS_Month)
        //    {
        //        mws1 = MWS;
        //    }
        //    else
        //    {
        //        mws1 = MonthlyWS.GetCalendar(EmployeeID, inputDate.Year, inputDate.Month);
        //    }
        //    string valuationClass1 = mws1.GetValuationClassExactly(inputDate.Day);
        //    TimeSpan PrevCutoffTime = GetPrevCutoffTime(valuationClass1);
        //    IsPrevDay = false;
        //    if (EndTime > BeginTime && BeginTime < PrevCutoffTime)
        //    {
        //        if (EndTime > PrevCutoffTime)
        //        {
        //            EndTime = PrevCutoffTime;
        //        }
        //        IsPrevDay = true;
        //    }
        //}

        //#region " SplitCutoffTime "
        //public static void SplitCutoffTime(string EmployeeID, DateTime inputDate, ref TimeSpan BeginTime, ref TimeSpan EndTime)
        //{
        //    MonthlyWS MWS = MonthlyWS.GetCalendar(EmployeeID, inputDate.Year, inputDate.Month);
        //    SplitCutoffTime(EmployeeID, inputDate, MWS, ref BeginTime, ref EndTime);
        //}
        //public static void SplitCutoffTime(string EmployeeID, DateTime inputDate, MonthlyWS MWS, ref TimeSpan BeginTime, ref TimeSpan EndTime)
        //{
        //    DateTime nextDate = inputDate.AddDays(1);
        //    MonthlyWS mws1 = null;
        //    MonthlyWS mws2 = null;
        //    string valuationClass1, valuationClass2;
        //    if (inputDate.Year == MWS.WS_Year && inputDate.Month == MWS.WS_Month)
        //    {
        //        mws1 = MWS;
        //    }
        //    else
        //    {
        //        mws1 = MonthlyWS.GetCalendar(EmployeeID, inputDate.Year, inputDate.Month);
        //    }
        //    valuationClass1 = mws1.GetValuationClassExactly(inputDate.Day);
        //    if (!IsTNOM(valuationClass1))
        //    {
        //        if (nextDate.Year == MWS.WS_Year && nextDate.Month == MWS.WS_Month)
        //        {
        //            mws2 = MWS;
        //        }
        //        else if (nextDate.Year == mws1.WS_Year && nextDate.Month == mws1.WS_Month)
        //        {
        //            mws2 = mws1;
        //        }
        //        else 
        //        {
        //            mws2 = MonthlyWS.GetCalendar(EmployeeID, nextDate.Year, nextDate.Month);
        //        }
        //        valuationClass2 = mws2.GetValuationClassExactly(nextDate.Day);
        //        if (valuationClass1 != valuationClass2)
        //        {
        //            TimeSpan CutoffTime = GetCutoffTime(valuationClass1);
        //            TimeSpan refCutoffTime;

        //            if (BeginTime >= CutoffTime)
        //            {
        //                refCutoffTime = CutoffTime.Add(new TimeSpan(1, 0, 0, 0));
        //            }
        //            else
        //            {
        //                refCutoffTime = CutoffTime;
        //            }
        //            if (refCutoffTime > BeginTime && refCutoffTime < EndTime)
        //            {
        //                EndTime = refCutoffTime;
        //            }
        //        }
        //    }
        //}
        //#endregion

        //#region " SplitWorkingTime "
        //public static void SplitWorkingTime(int DayNo, DailyWS DWS, MonthlyWS MWS, ref TimeSpan BeginTime, ref TimeSpan EndTime)
        //{
        //    string valuationClass = MWS.GetValuationClassExactly(DayNo);
        //    if (!IsTNOM(valuationClass))
        //    {
        //        if (!DWS.IsDayOff && MWS.GetWSCode(DayNo,false) == "")
        //        {
        //            //cut plan time
        //            TimeSpan timeend;
        //            if (DWS.WorkBeginTime > DWS.WorkEndTime)
        //            {
        //                timeend = DWS.WorkEndTime.Add(new TimeSpan(1, 0, 0, 0));
        //            }
        //            else
        //            {
        //                timeend = DWS.WorkEndTime;
        //            }
        //            if (BeginTime >= DWS.WorkBeginTime && BeginTime < timeend)
        //            {
        //                BeginTime = timeend;
        //            }
        //            else if (BeginTime < DWS.WorkBeginTime && EndTime > DWS.WorkBeginTime)
        //            {
        //                EndTime = DWS.WorkBeginTime;
        //            }
        //        }
        //    }
        //}
        //#endregion
    }
}
