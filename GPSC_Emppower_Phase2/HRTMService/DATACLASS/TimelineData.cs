using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.HR.TM.DATACLASS
{
    public class TimelineData
    {
        private string __employeeID = "";
        public TimelineData()
        { 
        }
        public string EmployeeID
        {
            get
            {
                return __employeeID;
            }
            set
            {
                __employeeID = value;
            }
        }
    }
}
