﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.TM.DATACLASS
{
    public class DashbaordYTDOvertimeandSalary
    {
        public decimal Overtime { get; set; }
        public decimal Salary { get; set; }
    }

    public class PaySlipEmployeeInOrganization : AbstractObject
    {
        public string EmployeeID { get; set; }
        public int PeriodYear { get; set; }
        public int PeriodMonth { get; set; }
        public string AccumType { get; set; }
        public string OffCycleFlag { get; set; }
        public DateTime PYDate { get; set; }
        public string WageTypeCode { get; set; }
        public string WageTypeName { get; set; }
        //public string CurrencyAmount { get; set; }
        public decimal Amount { get; set; }
    }

    public class DbOTReason : AbstractObject
    {
        public string OTWorkTypeID { get; set; }
        public string OTWorkTypeDesc { get; set; }
        public int Count_OT { get; set; }
    }

    public class DashboardOTReason
    {
        public DashboardOTReason()
        {
            ListTextOT = new List<string>();
            ListPercentageOT = new List<decimal>();
        }

        public List<string> ListTextOT { get; set; }
        public List<decimal> ListPercentageOT { get; set; }
    }

    public class DbOTWorkType : AbstractObject
    {
        public string OTWorkTypeID { get; set; }
        public string OTWorkTypeDesc { get; set; }
        public decimal PercentageWorkType { get; set; }
    }

    public class DashboardOvertimebyBusinessUnitinmonth
    {
        public DashboardOvertimebyBusinessUnitinmonth()
        {
            Amount = new List<decimal>();
            UnitName = new List<string>();
            PercentageOverTime = new List<decimal>();
            CodeColor = new List<string>();
        }
        public List<string> UnitName { get; set; }
        public List<decimal> PercentageOverTime { get; set; }
        public List<string> CodeColor { get; set; }
        public List<decimal> Amount { get; set; }
    }

    public class DbOvertimeInMonth : AbstractObject
    {
        public string EmployeeID { get; set; }
        public int PeriodYear { get; set; }
        public int PeriodMonth { get; set; }
        public string WageTypeCode { get; set; }
        public string WageTypeName { get; set; }
        public string CurrencyAmount { get; set; }
        public decimal Amount { get; set; }
    }

    public class OrganizationInfotype1000 : AbstractObject
    {
        public string ObjectType { get; set; }
        public string ObjectID { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public string ShortText { get; set; }
        public string Text { get; set; }
    }

    public class PayOvertimeInOrganization
    {
        public string OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        public decimal AmountOvertimeByOrg { get; set; }
    }

    public class DashbaordOvertimeAndSalaryByMonth
    {
        public DashbaordOvertimeAndSalaryByMonth()
        {
            Month = new List<string>();
            Salary = new List<decimal>();
            Overtime = new List<decimal>();
            ProportionOvertime = new List<decimal>();
        }
        public List<string> Month { get; set; }
        public List<decimal> Salary { get; set; }
        public List<decimal> Overtime { get; set; }
        public List<decimal> ProportionOvertime { get; set; }
    }

    public class DataPaySlipInYear : AbstractObject
    {
        public string EmployeeID { get; set; }
        public int PeriodYear { get; set; }
        public int PeriodMonth { get; set; }
        public string OffCycleFlag { get; set; }
        public DateTime PYDate { get; set; }
        public string WageTypeCode { get; set; }
        public string WageTypeName { get; set; }
        public string CurrencyAmount { get; set; }
    }

    public class DashbaordOTSalary6Months
    {
        public DashbaordOTSalary6Months()
        {
            Month = new List<string>();
            Salary = new List<decimal>();
            Overtime = new List<decimal>();
            ProportionOvertime = new List<decimal>();
        }
        public List<string> Month { get; set; }
        public List<decimal> Salary { get; set; }
        public List<decimal> Overtime { get; set; }
        public List<decimal> ProportionOvertime { get; set; }
    }

    public class DashbaordOTHours6Months
    {
        public DashbaordOTHours6Months()
        {
            Month = new List<string>();
            Salary = new List<decimal>();
            OvertimePay = new List<decimal>();
            OvertimeHour = new List<decimal>();
        }
        public List<string> Month { get; set; }
        public List<decimal> Salary { get; set; }
        public List<decimal> OvertimePay { get; set; }
        public List<decimal> OvertimeHour { get; set; }
    }

    public class DashboardSummarySalaryOvertimePayByYear
    {
        public DashboardSummarySalaryOvertimePayByYear()
        {
            TotalYear = new List<string>();
            TotalSalaryYear = new List<decimal>();
            TotalOvertimePayYear = new List<decimal>();
            TotalRatioOvertimePayYear = new List<decimal>();
        }
        public List<string> TotalYear { get; set; }
        public List<decimal> TotalSalaryYear { get; set; }
        public List<decimal> TotalOvertimePayYear { get; set; }
        public List<decimal> TotalRatioOvertimePayYear { get; set; }
    }

    public class DashboardSummarySalaryOvertimeHourByYear
    {
        public DashboardSummarySalaryOvertimeHourByYear()
        {
            TotalYear = new List<string>();
            TotalSalaryYear = new List<decimal>();
            TotalOvertimePayYear = new List<decimal>();
            TotalOvertimeHourYear = new List<decimal>();
        }
        public List<string> TotalYear { get; set; }
        public List<decimal> TotalSalaryYear { get; set; }
        public List<decimal> TotalOvertimePayYear { get; set; }
        public List<decimal> TotalOvertimeHourYear { get; set; }
    }

    public class OTEmployeeByLevel
    {
        public OTEmployeeByLevel()
        {
            EmployeeLevel1To2 = new List<int>();
            EmployeeLevel3To8 = new List<int>();
            EmployeeLevel9To10 = new List<int>();

            PercentageEmployeeLevel1To2 = new List<decimal>();
            PercentageEmployeeLevel3To8 = new List<decimal>();
            PercentageEmployeeLevel9To10 = new List<decimal>();
        }
        public List<int> EmployeeLevel1To2 { get; set; }
        //public List<int> EmployeeLevel3To4 { get; set; }
        public List<int> EmployeeLevel3To8 { get; set; }
        public List<int> EmployeeLevel9To10 { get; set; }

        public List<decimal> PercentageEmployeeLevel1To2 { get; set; }
        //public List<decimal> PercentageEmployeeLevel3To4 { get; set; }
        public List<decimal> PercentageEmployeeLevel3To8 { get; set; }
        public List<decimal> PercentageEmployeeLevel9To10 { get; set; }
    }

    public class dbOTEmployeeByLevel : AbstractObject
    {
        public string EmployeeID { get; set; }
        public DateTime BeginDate { get; set; }
        public decimal FinalOTHour10 { get; set; }
        public decimal FinalOTHour15 { get; set; }
        public decimal FinalOTHour30 { get; set; }
        public string Position { get; set; }
        public string EmpSubGroup { get; set; }
    }

    public class dbOTEmployeeByBath : AbstractObject
    {
        public string EmployeeID { get; set; }
        public int PeriodYear { get; set; }
        public int PeriodMonth { get; set; }
        public string OffCycleFlag { get; set; }
        public DateTime PYDate { get; set; }
        public string WageTypeCode { get; set; }
        public string WageTypeName { get; set; }
        public string CurrencyAmount { get; set; }
        public string EmpSubGroup { get; set; }
    }

    public class BaseOTEmployeeLevel
    {
        public decimal Level1To2 { get; set; }
        //public decimal Level3To4 { get; set; }
        public decimal Level3To8 { get; set; }
        public decimal Level9To10 { get; set; }

        public decimal Bath1To2 { get; set; }
        //public decimal Bath3To4 { get; set; }
        public decimal Bath3To8 { get; set; }
        public decimal Bath9To10 { get; set; }
    }

    public class BaseOTEmployeeLevel_January : BaseOTEmployeeLevel
    {

    }

    public class BaseOTEmployeeLevel_February : BaseOTEmployeeLevel
    {

    }

    public class BaseOTEmployeeLevel_March : BaseOTEmployeeLevel
    {

    }

    public class BaseOTEmployeeLevel_April : BaseOTEmployeeLevel
    {

    }

    public class BaseOTEmployeeLevel_May : BaseOTEmployeeLevel
    {

    }

    public class BaseOTEmployeeLevel_June : BaseOTEmployeeLevel
    {

    }

    public class BaseOTEmployeeLevel_July : BaseOTEmployeeLevel
    {

    }

    public class BaseOTEmployeeLevel_August : BaseOTEmployeeLevel
    {

    }

    public class BaseOTEmployeeLevel_September : BaseOTEmployeeLevel
    {

    }

    public class BaseOTEmployeeLevel_October : BaseOTEmployeeLevel
    {

    }

    public class BaseOTEmployeeLevel_November : BaseOTEmployeeLevel
    {

    }

    public class BaseOTEmployeeLevel_December : BaseOTEmployeeLevel
    {

    }

    public class OTEmployeeByBath
    {
        public OTEmployeeByBath()
        {
            OTAmountLevel1To2 = new List<decimal>();
            OTAmountLevel3To8 = new List<decimal>();
            OTAmountLevel9To10 = new List<decimal>();

            PercentageOTAmountLevel1To2 = new List<decimal>();
            PercentageOTAmountLevel3To8 = new List<decimal>();
            PercentageOTAmountLevel9To10 = new List<decimal>();
        }
        public List<decimal> OTAmountLevel1To2 { get; set; }
        public List<decimal> OTAmountLevel3To8 { get; set; }
        public List<decimal> OTAmountLevel9To10 { get; set; }

        public List<decimal> PercentageOTAmountLevel1To2 { get; set; }
        public List<decimal> PercentageOTAmountLevel3To8 { get; set; }
        public List<decimal> PercentageOTAmountLevel9To10 { get; set; }
    }

    public class OrgUnitTree : AbstractObject
    {
        public string OrgUnit { get; set; }
        public string ParentOrgUnit { get; set;}
        public string OrgName { get; set; }
        public string OrgFullName { get; set; }
        public string OrgLevel { get; set; }
        public string OrgPath { get; set; }
        public string OrgUnitPath { get; set; }
        public string OrgDisplay { get; set; }
    }

    public class AbsenceOther
    {
        public decimal QuotaAbsence { get; set; }
        public decimal UseAbsence { get; set; }
        public decimal InProcessAbsence { get; set; }
        public decimal PercentageUse { get; set; }
        public string AbsenceType { get; set; }
        public string AbsenceDefalutType { get; set; }
    }
}
