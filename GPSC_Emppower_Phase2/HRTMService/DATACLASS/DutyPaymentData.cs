﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using ESS.UTILITY.EXTENSION;
using ESS.HR.TM.CONFIG;

namespace ESS.HR.TM.DATACLASS
{
    public class DutyPaymentData : AbstractObject
    {
        public DutyPaymentData()
        {
            DutyTimes = new List<DutyTime>();
        }

        private string __requestNo;
        private string __itemNo;
        private string __employeeID;
        private DateTime __dutyDate;
        private DateTime __beginDate;
        private DateTime __endDate;
        private string __status;
        private string __refRequestNo;
        private CultureInfo oCl = new CultureInfo("th-TH");

        private List<DutyTime> _listduty;
        private string _DailyWorkscheduleCode;

        public string RequestNo
        {
            get { return __requestNo; }
            set { __requestNo = value; }
        }

        public string ItemNo
        {
            get { return __itemNo; }
            set { __itemNo = value; }
        }

        public string EmployeeID
        {
            get { return __employeeID; }
            set { __employeeID = value; }
        }

        public DateTime DutyDate
        {
            get { return __dutyDate; }
            set { __dutyDate = value; }
        }

        public DateTime BeginDate
        {
            get { return __beginDate; }
            set { __beginDate = value; }
        }

        public DateTime EndDate
        {
            get { return __endDate; }
            set { __endDate = value; }
        }

        public string DutyTime
        {
            get { return string.Format("{0} - {1}", __beginDate.ToString("HH:mm", oCl), __endDate.ToString("HH:mm", oCl)); }
        }

        public string Status
        {
            get { return __status; }
            set { __status = value; }
        }

        public string RefRequestNo
        {
            get { return __refRequestNo; }
            set { __refRequestNo = value; }
        }

        public List<DutyTime> DutyTimes
        {
            get { return _listduty; }
            set { _listduty = value; }
        }

        public string DailyWorkscheduleCode
        {
            get { return _DailyWorkscheduleCode; }
            set { _DailyWorkscheduleCode = value; }
        }
    }
}
