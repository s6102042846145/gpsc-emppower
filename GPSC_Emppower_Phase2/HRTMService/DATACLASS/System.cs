﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.TM.DATACLASS
{
    public class GroupSetupSystemForAdmin
    {
        public GroupSetupSystemForAdmin()
        {
            LeftSystem = new List<ReportGroupAdmin>();
            ReightSystem = new List<ReportGroupAdmin>();
        }
        public List<ReportGroupAdmin> LeftSystem { get; set; }
        public List<ReportGroupAdmin> ReightSystem { get; set; }
    }
}
