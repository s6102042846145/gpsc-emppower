using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.HR.TM.DATACLASS
{
    public class AbsAttCalculateResult
    {
        private decimal __hours;
        private decimal __days;
        private decimal __payrollDays;
        private DateTime __begindate;
        private DateTime __enddate;
        private TimeSpan __begintime;
        private TimeSpan __endtime;
        private Boolean __fullDay;
        private bool __prevDay;
        public AbsAttCalculateResult()
        { 
        }
        public decimal AbsenceHours
        {
            get
            {
                return __hours;
            }
            set
            {
                __hours = value;
            }
        }
        public decimal AbsenceDays
        {
            get
            {
                return __days;
            }
            set
            {
                __days = value;
            }
        }
        public decimal PayrollDays
        {
            get
            {
                return __payrollDays;
            }
            set
            {
                __payrollDays = value;
            }
        }

        public DateTime BeginDate
        {
            get
            {
                return __begindate;
            }
            set
            {
                __begindate = value;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return __enddate;
            }
            set
            {
                __enddate = value;
            }
        }

        public TimeSpan BeginTime
        {
            get
            {
                return __begintime;
            }
            set
            {
                __begintime = value;
            }
        }
        public TimeSpan EndTime
        {
            get
            {
                return __endtime;
            }
            set
            {
                __endtime = value;
            }
        }
        public bool FullDay
        {
            get
            {
                return __fullDay;
            }
            set
            {
                __fullDay = value;
            }
        }
        public bool PrevDay
        {
            get
            {
                return __prevDay;
            }
            set
            {
                __prevDay = value;
            }
        }
    }
}
