using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using ESS.HR.TM.INFOTYPE;
using System.Reflection;
using System.Data;

namespace ESS.HR.TM.DATACLASS
{
    public class DutyManagement
    {
         private static Configuration __config;

        #region Properties

        private static Configuration config
        {
            get
            {
                if (__config == null)
                {
                    __config = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().CodeBase.Replace("file:///", "").Replace("/", "\\"));
                }
                return __config;
            }
        }

        public static int DutyOffsetBefore
        {
            get
            {
                if (config == null || config.AppSettings.Settings["DUTYOFFSETBEFORE"] == null)
                {
                    return 0;
                }
                else
                {
                    int nValue;
                    if (int.TryParse(config.AppSettings.Settings["DUTYOFFSETBEFORE"].Value, out nValue))
                    {
                        return nValue;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }

        public static int DutyOffsetAfter
        {
            get
            {
                if (config == null || config.AppSettings.Settings["DUTYOFFSETAFTER"] == null)
                {
                    return 0;
                }
                else
                {
                    int nValue;
                    if (int.TryParse(config.AppSettings.Settings["DUTYOFFSETAFTER"].Value, out nValue))
                    {
                        return nValue;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }
        #endregion

        public static Dictionary<DateTime, decimal> CalculateDutyPayment(List<DutyPaymentLog> data)
        {
            Dictionary<DateTime, decimal> oResult = new Dictionary<DateTime, decimal>();
            foreach (DutyPaymentLog item in data)
            {
                if (oResult.ContainsKey(item.DutyDate.Date))
                {
                    oResult[item.DutyDate.Date] += 1;
                }
                else
                {
                    oResult.Add(item.DutyDate.Date, 1);
                }
            }
            return oResult;
        }

        public static decimal CalculateSummaryDutyPayment(DataTable data)
        {
            return data.Rows.Count;
        }

        public static decimal CalculateSummaryDutyPayment(List<DutyPaymentLog> data)
        {
            return data.Count;
        }

        //public static List<DutyPaymentLog> GetDutyPaymentLog(String strEmployeeID,DateTime dtBeginDate,DateTime dtEndDate)
        //{
        //    return HR.TM.ServiceManager.HRTMBuffer.GetDutyPaymentLog(strEmployeeID, dtBeginDate, dtEndDate);
        //}
    }
}
