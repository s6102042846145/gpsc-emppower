using System;
using System.Collections.Generic;
using System.Text;
using ESS.DATA;

namespace ESS.HR.TM.DATACLASS
{
    [Serializable]
    public class OTClock
    {
        // Fields...
        private Decimal _OTValue;
        private DateTime _IntersecEnd;
        private DateTime _IntersecBegin;
        private DateTime _MachineEnd;
        private DateTime _MachineBegin;


        public Decimal OTValue
        {
            get { return _OTValue; }
            set
            {
                _OTValue = value;
            }
        }
        
        public DateTime IntersecEnd
        {
            get { return _IntersecEnd; }
            set
            {
                _IntersecEnd = value;
            }
        }
        
        public DateTime IntersecBegin
        {
            get { return _IntersecBegin; }
            set
            {
                _IntersecBegin = value;
            }
        }
        
        public DateTime MachineEnd
        {
            get { return _MachineEnd; }
            set
            {
                _MachineEnd = value;
            }
        }
        
        public DateTime MachineBegin
        {
            get { return _MachineBegin; }
            set
            {
                _MachineBegin = value;
            }
        }
        
    }
}
