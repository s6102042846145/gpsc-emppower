﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.TM.DATACLASS
{
    public class AreaWorkscheduleLog : AbstractObject
    {
        private int __wsChangeID;
        private string __requestNo = "";
        private string __employeeID = "";
        private int __areaWorkscheduleID;
        private DateTime __effectiveDate = DateTime.MinValue;
        private string __status = "";

        public int WSChangeID
        {
            get
            {
                return __wsChangeID;
            }
            set
            {
                __wsChangeID = value;
            }
        }
        public string RequestNo
        {
            get
            {
                return __requestNo;
            }
            set
            {
                __requestNo = value;
            }
        }
        public string EmployeeID
        {
            get
            {
                return __employeeID;
            }
            set
            {
                __employeeID = value;
            }
        }
        public int AreaWorkscheduleID
        {
            get
            {
                return __areaWorkscheduleID;
            }
            set
            {
                __areaWorkscheduleID = value;
            }
        }
        public DateTime EffectiveDate
        {
            get
            {
                return __effectiveDate;
            }
            set
            {
                __effectiveDate = value;
            }
        }
        public string Status
        {
            get
            {
                return __status;
            }
            set
            {
                __status = value;
            }
        }
        public string CreatorID { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Area { get; set; }
        public string SubArea { get; set; }
        public string TextCode { get; set; }
        public string WFRule { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public string EmployeeName { get; set; }
        public string TextName { get; set; }
        public string BeforeTextName { get; set; }
    }

    public class GroupAreaWorkscheduleLog : AbstractObject
    {
        public GroupAreaWorkscheduleLog()
        {
            List_AreaWorkscheduleLog = new List<AreaWorkscheduleLog>();
        }

        public DateTime CreateDate { get; set; }
        public List<AreaWorkscheduleLog> List_AreaWorkscheduleLog { get; set; }
    }
}
