﻿using ESS.TIMESHEET;
using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.TM.DATACLASS
{
    public class TimesheetManagementModel : AbstractObject
    {
        private List<TimePair> __timePairByUser = new List<TimePair>();
        public List<TimePair> TimePairByUser
        {
            get { return __timePairByUser; }
            set { __timePairByUser = value; }
        }

        private List<TimePair> __timePair = new List<TimePair>();
        public List<TimePair> TimePair
        {
            get { return __timePair; }
            set { __timePair = value; }
        }

        private List<TimeElement> __timeElement = new List<TimeElement>();
        public List<TimeElement> TimeElement
        {
            get { return __timeElement; }
            set { __timeElement = value; }
        }


        private List<TimePair> __oldTimePair = new List<TimePair>();
        public List<TimePair> OldTimePair
        {
            get { return __oldTimePair; }
            set { __oldTimePair = value; }
        }

        private List<TimeElement> __oldTimeElement = new List<TimeElement>();
        public List<TimeElement> OldTimeElement
        {
            get { return __oldTimeElement; }
            set { __oldTimeElement = value; }
        }
    }
}
