using System;
using System.Collections.Generic;
using System.Text;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.TM.DATACLASS
{
    public class TimeEval : AbstractObject
    {

        // Fields...
        private string __employeeID;
        //private DateTime _date;
        //private TimeSpan __beginTime;
        //private TimeSpan __endTime;
        private string __wageType;
        private decimal __wageHour;
        private DateTime __beginDate;
        private DateTime __endDate;

        public string EmployeeID
        {
            get{return __employeeID;}
            set {__employeeID = value;}
        }

        //public DateTime Date
        //{
        //    get { return _date; }
        //    set
        //    {
        //        _date = value;
        //    }
        //}

        //public TimeSpan BeginTime
        //{
        //    get { return __beginTime; }
        //    set
        //    {
        //        __beginTime = value;
        //    }
        //}

        //public TimeSpan EndTime
        //{
        //    get { return __endTime; }
        //    set
        //    {
        //        __endTime = value;
        //    }
        //}

        public string WageType
        {
            get { return __wageType; }
            set
            {
                __wageType = value;
            }
        }

        public decimal WageHour
        {
            get { return __wageHour; }
            set
            {
                __wageHour = value;
            }
        }

        public DateTime BeginDate
        {
            get { return __beginDate; }
            set { __beginDate = value; }
        }

        public DateTime EndDate
        {
            get { return __endDate; }
            set { __endDate = value; }
        }
    }
}
