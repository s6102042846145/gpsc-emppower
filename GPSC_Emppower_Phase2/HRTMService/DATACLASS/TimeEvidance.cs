using System;
using System.Collections.Generic;
using System.Text;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.TM.DATACLASS
{
    public class TimeEvidance
    {
        private TimeSpan __beginTime = TimeSpan.MinValue;
        private TimeSpan __endTime = TimeSpan.MinValue;
        public TimeEvidance(TimeSpan begin, TimeSpan end)
        {
            __beginTime = begin;
            __endTime = end;
        }

        public TimeSpan BeginTime
        {
            get
            {
                return __beginTime;
            }
        }

        public TimeSpan EndTime
        {
            get
            {
                return __endTime;
            }
        }
    }
}
