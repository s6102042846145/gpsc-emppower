﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.TM.DATACLASS
{
    [Serializable]
    public class AbsenceListData : AbstractObject
    {
        private string __requestNo = "";
        private DateTime __beginDate = DateTime.MinValue;
        private DateTime __endDate = DateTime.MinValue;
        private TimeSpan __beginTime = TimeSpan.Zero;
        private TimeSpan __endTime = TimeSpan.Zero;
        private decimal __absenceDays;
        private decimal __absenceHours;
        private decimal __payrollDays;
        private bool __allDayFlag = true;
        private string __status = "";
        private string __remark = "";
        private bool __isRequiredDoc = true;       
        private bool __isMark = true;
        private bool __isPost = false;
        private bool __isSkip = false;
        public string __absencetype;
        public string __absenceConcat;
        public string _DefaultAbsenceType;
        public string _AbsenceGroupData;


        #region " Properties "

        public string AbsenceGroupData
        {
            get
            {
                return _AbsenceGroupData;
            }
            set
            {
                _AbsenceGroupData = value;
            }
        }

        public string DefaultAbsenceType
        {
            get
            {
                return _DefaultAbsenceType;
            }
            set
            {
                _DefaultAbsenceType = value;
            }
        }

        public string Absencetype
        {
            get
            {
                return __absencetype;
            }
            set
            {
                __absencetype = value;
            }
        }
        public string AbsenceConcat
        {
            get
            {
                return __absenceConcat;
            }
            set
            {
                __absenceConcat = value;
            }
        }
        public string RequestNo
        {
            get
            {
                return __requestNo;
            }
            set
            {
                __requestNo = value;
            }
        }
        public DateTime BeginDate
        {
            get
            {
                return __beginDate;
            }
            set
            {
                __beginDate = value;
            }
        }
        public DateTime EndDate
        {
            get
            {
                return __endDate;
            }
            set
            {
                __endDate = value;
            }
        }
        public TimeSpan BeginTime
        {
            get
            {
                return __beginTime;
            }
            set
            {
                __beginTime = value;
            }
        }
        public TimeSpan EndTime
        {
            get
            {
                return __endTime;
            }
            set
            {
                __endTime = value;
            }
        }
        public decimal AbsenceDays
        {
            get
            {
                return __absenceDays;
            }
            set
            {
                __absenceDays = value;
            }
        }
        public decimal AbsenceHours
        {
            get
            {
                return __absenceHours;
            }
            set
            {
                __absenceHours = value;
            }
        }
        public decimal PayrollDays
        {
            get
            {
                if (__payrollDays == 0.0M && this.AllDayFlag)
                {
                    return 0.0M;
                }
                return __payrollDays;
            }
            set
            {
                __payrollDays = value;
            }
        }
        public bool AllDayFlag
        {
            get
            {
                return __allDayFlag;
            }
            set
            {
                __allDayFlag = value;
            }
        }
        public string Status
        {
            get
            {
                return __status;
            }
            set
            {
                __status = value;
            }
        }
        public string Remark
        {
            get
            {
                return __remark;
            }
            set
            {
                __remark = value;
            }
        }
        public bool IsRequireDocument
        {
            get
            {
                return __isRequiredDoc;
            }
            set
            {
                __isRequiredDoc = value;
            }
        }
        public bool IsMark
        {
            get
            {
                return __isMark;
            }
            set
            {
                __isMark = value;
            }
        }
        public bool IsPost
        {
            get
            {
                return __isPost;
            }
            set
            {
                __isPost = value;
            }
        }
        public bool IsSkip
        {
            get
            {
                return __isSkip;
            }
            set
            {
                __isSkip = value;
            }
        }
        #endregion
    }

    public class DataArchiveTimepair
    {
        public bool visibleTimeElement { get; set; }
        public bool visibleTimePair { get; set; }
        public List<DataTimeElement> ListDataTimeElement { get; set; }
    }

    public class DataTimeElement
    {

    }
}
