﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.TM.DATACLASS
{
    public class ReportGroupAdmin  : AbstractObject
    {
        public int SubjectReportGroupID { get; set; }
        public int SubjectReportSubID { get; set; }
        public string SubjectCode { get; set; }
    }

    public class ClientReportGroupAdmin
    {
        public ClientReportGroupAdmin()
        {
            LeftReport = new List<ReportGroupAdmin>();
            RightReport = new List<ReportGroupAdmin>();
        }
        public List<ReportGroupAdmin> LeftReport { get; set; }
        public List<ReportGroupAdmin> RightReport { get; set; }
    }

    public class OTLogAll : AbstractObject
    {
        public string RequestNo { get; set; }
        public string EmployeeID { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public decimal FinalOTHour10 { get; set; }
        public decimal FinalOTHour15 { get; set; }
        public decimal FinalOTHour30 { get; set; }
        public bool IsPayroll { get; set; }
        public string RefRequestNo { get; set; }
        public string LevelEmployee { get; set; }
    }

    public class OTLogMonthByHour
    {
        public string Name { get; set; }
        public int? Count_Jan { get; set; }
        public int? Count_Feb { get; set; }
        public int? Count_Mar { get; set; }
        public int? Count_Apr { get; set; }
        public int? Count_May { get; set; }
        public int? Count_Jun { get; set; }
        public int? Count_Jul { get; set; }
        public int? Count_Aug { get; set; }
        public int? Count_Sep { get; set; }
        public int? Count_Oct { get; set; }
        public int? Count_Nov { get; set; }
        public int? Count_Dec { get; set; }
        public TypePeriodReportOTLogByMonth TypePeriod { get; set; }
    }

    public enum TypePeriodReportOTLogByMonth
    {
        SummaryOTByMonth = 8,
        SummaryOTByMonthOver36_PerWeek = 9, //สรุปจำนวนคนที่เบิกเกิน 36 ช.ม ต่อสัปดาห์
        SummaryOTEmployee_Level4_And_Level10_Over120_PerMonth = 10, // สรุปจำนวนพนักงานระดับ 4 -10 ที่เบิกเกิน 120 ชั่วโมง / เดือน
        SummaryOTEmployee_Level9_And_Level10_Over48_PerMonth = 11 // สรุปจำนวนพนักงานระดับ 9 – 10 ที่เบิกเกิน 48 ชั่วโมง/เดือน
    }

    public class PeriodOTSumary : AbstractObject
    {
        public int RptID { get; set; }
        public string RptName { get; set; }
        public string RptCategory { get; set; }
        public string RptDesc { get; set; }
        public string RptStatus { get; set; }
        public int RptSeqNo { get; set; }
        public string RptSeqDesc { get; set; }
        public string RptTextDescCode { get; set; }
        public decimal MinValue { get; set; }
        public decimal MaxValue { get; set; }
        public int? Count_Jan { get; set; }
        public int? Count_Feb { get; set; }
        public int? Count_Mar { get; set; }
        public int? Count_Apr { get; set; }
        public int? Count_May { get; set; }
        public int? Count_Jun { get; set; }
        public int? Count_Jul { get; set; }
        public int? Count_Aug { get; set; }
        public int? Count_Sep { get; set; }
        public int? Count_Oct { get; set; }
        public int? Count_Nov { get; set; }
        public int? Count_Dec { get; set; }
        public decimal MinMinute { get; set; }
        public decimal MaxMinute { get; set; }
    }

    public class DbAbsenceSickLeave : AbstractObject
    {
        public string EmployeeID { get; set; }
        public string AbsAttType { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public TimeSpan BeginTime { get; set; }
        public TimeSpan EndTime { get; set; }
        public decimal AbsAttDays { get; set; }
        public decimal PayrollDays { get; set; }
        public bool AllDayFlag { get; set; }
        public DateTime ActionDate { get; set; }
        public string Description { get; set; }
        public string RequestNo { get; set; }
        public string WorkLocationCode { get; set; }
    }

    public class MasterWorkLocation : AbstractObject
    {
        public string WorkLocationId { get; set; }
        public string Name { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime ExpireDate { get; set; }
    }

    public class MonthAbsenceSickLeave
    {
        public decimal Jan { get; set; }
        public decimal Feb { get; set; }
        public decimal Mar { get; set; }
        public decimal Apr { get; set; }
        public decimal May { get; set; }
        public decimal Jun { get; set; }
        public decimal Jul { get; set; }
        public decimal Aug { get; set; }
        public decimal Sep { get; set; }
        public decimal Oct { get; set; }
        public decimal Nov { get; set; }
        public decimal Dec { get; set; }
        public decimal SickLeaveTotal { get; set; }
    }

    public class MonthAbsenceSickLeaveEmployee
    {
        public int CountEmp_Jan { get; set; }
        public int CountEmp_Feb { get; set; }
        public int CountEmp_Mar { get; set; }
        public int CountEmp_Apr { get; set; }
        public int CountEmp_May { get; set; }
        public int CountEmp_Jun { get; set; }
        public int CountEmp_Jul { get; set; }
        public int CountEmp_Aug { get; set; }
        public int CountEmp_Sep { get; set; }
        public int CountEmp_Oct { get; set; }
        public int CountEmp_Nov { get; set; }
        public int CountEmp_Dec { get; set; }
        public int EmpTotal { get; set; }
    }

    public class ReportSummarySickLeave
    {
        public MonthAbsenceSickLeave CountSickLeave { get; set; }
        public MonthAbsenceSickLeaveEmployee CountEmployeeSickLeave { get; set; }
    }

    public class OrgUnitEmployeeByRole : AbstractObject
    {
        public string EmployeeID { get; set; }
        public string CompanyCode { get; set; }
        public string UserRole { get; set; }
        public string ResponseType { get; set; }
        public string OrgUnit { get; set; }
        public string ResponseCompanyCode { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public string OrgNameTH { get; set; }
        public string OrgNameEN { get; set; }
    }

}
