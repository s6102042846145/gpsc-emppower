using System;
using System.Collections.Generic;
using System.Text;
//using ESS.DATA;
using ESS.HR.TM.CONFIG;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.CONFIG;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.TM.DATACLASS
{
    [Serializable]
    public class ClockinLateClockoutEarly : AbstractObject
    {
        public ClockinLateClockoutEarly()
        { }

        private string __employeeID = string.Empty;
        private DateTime __date = DateTime.MinValue;
        private string __type = string.Empty;
        private DateTime __clockIn = DateTime.MinValue;
        private bool __isPlusClockIn = false;
        private DateTime __clockOut = DateTime.MinValue;
        private bool __isPlusClockOut = false;
        private bool __clockInLate = false;
        private bool __clockoutEarly = false;
        private bool __clockAbnormal = false;
        private bool __isRemark = false;
        private bool __clockNoneWork = false;
        private string __categoryCode = string.Empty;
        private string __textCode = string.Empty;
        private string __beginDate = string.Empty;
        private string __endDate = string.Empty;
        private string __remark = string.Empty;
        private decimal __payrollDays = 0;


        public decimal PayrollDays
        {
            get { return __payrollDays; }
            set { __payrollDays = value; }
        }

        public string EmployeeID
        {
            get { return __employeeID; }
            set { __employeeID = value; }
        }
        public DateTime Date
        {
            get { return __date; }
            set { __date = value; }
        }
        public string Type
        {
            get { return __type; }
            set { __type = value; }
        }
        public DateTime ClockIn
        {
            get { return __clockIn; }
            set { __clockIn = value; }
        }
        public DateTime ClockOut
        {
            get { return __clockOut; }
            set { __clockOut = value; }
        }
        public bool ClockInLate
        {
            get { return __clockInLate; }
            set { __clockInLate = value; }
        }
        public bool ClockoutEarly
        {
            get { return __clockoutEarly; }
            set { __clockoutEarly = value; }
        }
        public bool ClockAbnormal
        {
            get { return __clockAbnormal; }
            set { __clockAbnormal = value; }
        }
        public bool ClockNoneWork
        {
            get { return __clockNoneWork; }
            set { __clockNoneWork = value; }
        }
        public bool IsPlusClockIn
        {
            get { return __isPlusClockIn; }
            set { __isPlusClockIn = value; }
        }
        public bool IsPlusClockOut
        {
            get { return __isPlusClockOut; }
            set { __isPlusClockOut = value; }
        }
        public bool IsRemark
        {
            get { return __isRemark; }
            set { __isRemark = value; }
        }
        public string CategoryCode
        {
            get { return __categoryCode; }
            set { __categoryCode = value; }
        }
        public string TextCode
        {
            get { return __textCode; }
            set { __textCode = value; }
        }
        public string BeginDate
        {
            get { return __beginDate; }
            set { __beginDate = value; }
        }
        public string EndDate
        {
            get { return __endDate; }
            set { __endDate = value; }
        }
        public string Remark
        {
            get { return __remark; }
            set { __remark = value; }
        }
    }
}
