﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.TM.DATACLASS
{
    public class PeriodData
    {
        public PeriodData()
        {
            PeriodGroupTypeEffectiveDates = new List<PeriodTypeGroupEffectiveDate>();
            PeriodGroupTypeEmployees = new List<PeriodTypeGroupEmployee>();
        }
        public string PeriodGroupType { get; set; }
        public List<PeriodTypeGroupEffectiveDate> PeriodGroupTypeEffectiveDates { get; set; } // Group ตาม Effective Date
        public List<PeriodTypeGroupEmployee> PeriodGroupTypeEmployees { get; set; }  // Group ตามพนักงาน
    }

    #region จัดกลุ่มตามพนักงาน
    public class PeriodTypeGroupEmployee
    {
        public PeriodTypeGroupEmployee()
        {
            EmployeeWorkingDetails = new List<PeriodTypeGroupEmployeeDetail>();
        }

        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string PositionId { get; set; }
        public string PositionName { get; set; }
        public string OrgName { get; set; }
        public List<PeriodTypeGroupEmployeeDetail> EmployeeWorkingDetails { get; set; }
    }
    public class PeriodTypeGroupEmployeeDetail
    {
        public PeriodTypeGroupEmployeeDetail()
        {
            //ListDataAreaWorkschedule = new List<DataAreaWorkschedule>();
        }

        public DateTime? EffectiveDate { get; set; }
        public int AreaWorkScheduleId { get; set; }
        public string Area { get; set; }
        public string SubArea { get; set; }
        public string TextCode { get; set; }
        public string WFRule { get; set; }
        public string TextName { get; set; }
        public string BeforeTextName { get; set; }
        //public List<DataAreaWorkschedule> ListDataAreaWorkschedule { get; set; }
    }
    #endregion

    #region จัดกลุ่มตามวันที่เริ่มทำงาน
    public class PeriodTypeGroupEffectiveDate
    {
        public PeriodTypeGroupEffectiveDate()
        {
            PeriodTypeGroupEffectiveDateDetails = new List<PeriodTypeGroupEffectiveDateDetail>();
        }
        public DateTime EffectiveDate { get; set; }
        public List<PeriodTypeGroupEffectiveDateDetail> PeriodTypeGroupEffectiveDateDetails { get; set; }
    }

    public class PeriodTypeGroupEffectiveDateDetail
    {
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string PositionId { get; set; }
        public string PositionName { get; set; }
        public string OrgName { get; set; }
        public int AreaWorkScheduleId { get; set; }
        public string Area { get; set; }
        public string SubArea { get; set; }
        public string TextCode { get; set; }
        public string WFRule { get; set; }
        public string TextName { get; set; }
        public string BeforeTextName { get; set; }
    }
    #endregion

    public class DataAreaWorkschedule
    {
        public int AreaWorkscheduleID { get; set; }
        public string Area { get; set; }
        public string SubArea { get; set; }
        public string TextCode { get; set; }
        public string WFRule { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public string TextName { get; set; }
        public string BeforeTextName { get; set; }
    }



}
