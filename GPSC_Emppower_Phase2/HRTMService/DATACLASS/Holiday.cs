﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.TM.DATACLASS
{
    public class Holiday
    {
        private string _Day;
        private string _Month;
        private string _Year;
        private string _Description;

        public string Day
        {
            get { return _Day; }
            set { _Day = value; }
        }
        public string Month
        {
            get { return _Month; }
            set { _Month = value; }
        }
        public string Year
        {
            get { return _Year; }
            set { _Year = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public Holiday()
        {
            _Day = null;
            _Month = null;
            _Year = null;
            _Description = null;
        }
    }

    public class HolidayMaxMin
    {
        public HolidayMaxMin()
        {
            Holiday = new List<Holiday>();
        }

        public string HireDate { get; set; }
        public string MaxDate { get; set; }
        public List<Holiday> Holiday { get; set; } 
    }
}
