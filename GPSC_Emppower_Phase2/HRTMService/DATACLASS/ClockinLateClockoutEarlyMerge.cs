using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.HR.TM.DATACLASS
{
    [Serializable]
    public class ClockinLateClockoutEarlyMerge
    {
        public ClockinLateClockoutEarlyMerge()
        { }

        private DateTime __date = DateTime.MinValue;
        private string __type = string.Empty;
        private string __clockIn = string.Empty;
        private string __clockOut = string.Empty;
        private bool __clockInLate = false;
        private bool __clockoutEarly = false;
        private bool __clockAbnormal = false;
        private string __remark = string.Empty;

        public DateTime Date
        {
            get { return __date; }
            set { __date = value; }
        }
        public string Type
        {
            get { return __type; }
            set { __type = value; }
        }
        public string ClockIn
        {
            get { return __clockIn; }
            set { __clockIn = value; }
        }
        public string ClockOut
        {
            get { return __clockOut; }
            set { __clockOut = value; }
        }
        public bool ClockInLate
        {
            get { return __clockInLate; }
            set { __clockInLate = value; }
        }
        public bool ClockoutEarly
        {
            get { return __clockoutEarly; }
            set { __clockoutEarly = value; }
        }
        public bool ClockAbnormal
        {
            get { return __clockAbnormal; }
            set { __clockAbnormal = value; }
        }
        public string Remark
        {
            get { return __remark; }
            set { __remark = value; }
        }
    }
}
