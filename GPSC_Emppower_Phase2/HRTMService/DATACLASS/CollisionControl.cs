﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.TM.DATACLASS
{
    public class CollisionControl : AbstractObject
    {
        private string __applicationKeyExists;
        private string __subKey1Exists;
        private string __subKey2Exists;
        private string __applicationKeyNew;
        private string __subKey1New;
        private string __subKey2New;
        private bool __isCheckOnlyPlanTime;
        private bool __ignoreSameDetail = false;
        public CollisionControl()
        {
        }
        public string ApplicationKeyExists
        {
            get
            {
                return __applicationKeyExists;
            }
            set
            {
                __applicationKeyExists = value;
            }
        }
        public string SubKey1Exists
        {
            get
            {
                return __subKey1Exists;
            }
            set
            {
                __subKey1Exists = value;
            }
        }
        public string SubKey2Exists
        {
            get
            {
                return __subKey2Exists;
            }
            set
            {
                __subKey2Exists = value;
            }
        }
        public string ApplicationKeyNew
        {
            get
            {
                return __applicationKeyNew;
            }
            set
            {
                __applicationKeyNew = value;
            }
        }
        public string SubKey1New
        {
            get
            {
                return __subKey1New;
            }
            set
            {
                __subKey1New = value;
            }
        }
        public string SubKey2New
        {
            get
            {
                return __subKey2New;
            }
            set
            {
                __subKey2New = value;
            }
        }
        public bool IsCheckOnlyPlanTime
        {
            get
            {
                return __isCheckOnlyPlanTime;
            }
            set
            {
                __isCheckOnlyPlanTime = value;
            }
        }
        public bool IgnoreSameDetail
        {
            get
            {
                return __ignoreSameDetail;
            }
            set
            {
                __ignoreSameDetail = value;
            }
        }
    }
}
