﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.TM
{
    public enum SubstituteStatus
    {
        DRAFT = 0,
        WAITFORACCEPT = 1,
        WAITFORAPPROVE = 2,
        COMPLETED = 3,
        VIEW = 4,
        CANCELLED = 5,
        WAITFOREDIT = 6,
        WAITFORPOSTDATA = 7,

        DRAFTFORCANCEL = 8,
        WAITFORACCEPTFORCANCEL = 9,
        WAITFORAPPROVEFORCANCEL = 10,
        WAITFOREDITFORCANCEL = 11,
        WAITFORPOSTDATAFORCANCEL = 12
    }
}
