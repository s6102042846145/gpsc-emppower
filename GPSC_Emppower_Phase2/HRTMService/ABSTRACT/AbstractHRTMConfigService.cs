using System;
using System.Collections.Generic;
using ESS.HR.TM.CONFIG;
using ESS.HR.TM.INTERFACE;
using ESS.HR.TM.DATACLASS;
using ESS.EMPLOYEE;
using System.Data;

namespace ESS.HR.TM.ABSTRACT
{
    public abstract class AbstractHRTMConfigService : IHRTMConfigService
    {
        #region IHRTMConfig Members

        public AbstractHRTMConfigService()
        {
        }

        #region IHRTMConfig Members

        public virtual List<AbsenceType> GetAllAbsenceTypeList()
        {
            return GetAbsenceTypeList("");
        }

        public List<AbsenceType> GetAbsenceTypeList(string EmployeeID)
        {
            return GetAbsenceTypeList(EmployeeID, "");
        }

        public virtual AbsenceType GetAbsenceType(string EmployeeID, string AbsenceType, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveAbsenceTypeList(List<AbsenceType> data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<AbsenceType> GetAbsenceTypeList(string EmployeeID, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<AbsenceType> GetAbsenceTypeList(EmployeeData oEmp, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public List<AbsenceQuotaType> GetAbsenceQuotaTypeList()
        {
            return GetAbsenceQuotaTypeList("");
        }

        public virtual List<AbsenceQuotaType> GetAbsenceQuotaTypeList(string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveAbsenceQuotaTypeList(List<AbsenceQuotaType> Data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual AbsenceQuotaType GetAbsenceQuotaType(string EmployeeID, string QuotaTypeCode, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual AbsenceCreatingRule GetAbsenceCreatingRule(EmployeeData oEmp, string AbsenceTypeCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual AbsenceCreatingRule GetAbsenceCreatingRule(EmployeeData oEmp, string AbsenceTypeCode, decimal AbsenceDays)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual AbsenceCreatingRule GetAbsenceCreatingRule(string EmpID, string AbsenceTypeCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<AbsenceAssignment> GetAbsenceAssignmentList(string AbsAttGrouping, string AbsenceType, string SubKey)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<AttendanceType> GetAllAttendanceTypeList()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<AttendanceType> GetAttendanceTypeList(string EmployeeID, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<AttendanceType> GetAttendanceTypeList(EmployeeData oEmp, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }


        public virtual void SaveAttendanceTypeList(List<AttendanceType> Data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual AttendanceType GetAttendanceType(string EmployeeID, string AttendanceType, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual AttendanceCreatingRule GetAttendanceCreatinigRule(EmployeeData oEmp, string AttendanceTypeCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual AttendanceCreatingRule GetAttendanceCreatinigRule(string EmployeeID, string AttendanceTypeCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<OTItemType> GetAllOTItemType()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual OTItemType GetOTItemType(int OTItemTypeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<CountingRule> GetAllCountingRuleList()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveCountingRuleList(List<CountingRule> Data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<DeductionRule> GetAllDeductionRule()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveDeductionRule(List<DeductionRule> Data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DeductionRule GetDeductionRule(string subGroupSetting, string subAreaSetting, string absAttGrouping, string AbsenceType)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual AbsenceQuotaType GetAbsenceQuotaType(string sAbsenceQuotaKey)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual AbsenceCancelRule GetAbsenceCancelRule(string EmployeeID, string AbsenceTypeCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual AbsenceCancelRule GetAbsenceCancelRule(EmployeeData oEmp, string AbsenceTypeCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DailyOTDeletingRule GetDailyOTDeletingRule()
        {
            throw new Exception("The method 'GetDailyOTDeletingRule()' is not implemented.");
        }

        public virtual string GetAttendanceFlowSetting(string AbsAttGrouping, string AttendanceTypeCode)
        {
            throw new Exception("The method 'GetAttendanceFlowSetting(string AbsAttGrouping, string AttendanceTypeCode)' is not implemented.");
        }

        public virtual SubstituteCreatingRule GetSubstituteCreatingRule()
        {
            throw new Exception("The method 'GetSubstituteCreatingRule()' is not implemented.");
        }

        #endregion

        public virtual string GetCommonText(string Category, string Code, string Language)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual System.Data.DataTable GetClockinLateClockoutEarly(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveClockinLateClockoutEarly(List<ClockinLateClockoutEarly> List, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #region IHRTMConfig Members


        public virtual void DeleteSubstitution(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        public virtual List<DailyFlexTime> GetDailyFlexTimeByFlexCode(string FlexCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DailyFlexTime GetDailyFlexTimeMinForCalculateByFlexCode(string FlexCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DailyFlexTime GetTimesheetChangeFlexTime(string FlexCode, string ClockInTime)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<OTWorkType> GetAllOTWorkType(string Language)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<DutyTime> GetActivedDutyTime()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual int GetTimeManagementSubjectID()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<AreaWorkschedule> GetAreaWorkscheduleAllByArea(string Area, string SubArea)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<AreaWorkschedule> GetAreaWorkscheduleAreaCode(string Area, string SubArea)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<AreaWorkschedule> GetAreaWorkscheduleAllByWFRule(string Area, string SubArea, string WFRule)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual bool ShowTimeAwareLink(int LinkID)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        #endregion

    }
}