using System;
using System.Data;
using System.Collections.Generic;
using ESS.DATA;
using ESS.HR.TM.DATACLASS;
using ESS.HR.TM.INFOTYPE;
using ESS.HR.TM.INTERFACE;
using ESS.EMPLOYEE;
using ESS.TIMESHEET;
using ESS.EMPLOYEE.CONFIG.TM;

namespace ESS.HR.TM.ABSTRACT
{
    public abstract class AbstractHRTMDataService : IHRTMDataService
    {
        public AbstractHRTMDataService()
        {
        }

        #region IHRTMService Members

        public virtual void MarkUpdate(string EmployeeID, string DataCategory, string RequestNo, bool isMarkIn)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual bool CheckMark(string EmployeeID, string DataCategory)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual bool CheckMark(string EmployeeID, string DataCategory, string ReqNo)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE2006> GetAbsenceQuota(string EmployeeID)
        {
            return GetAbsenceQuota(EmployeeID, DateTime.Now, false);
        }

        public virtual List<INFOTYPE2006> GetAbsenceQuota(string EmployeeID, bool IncludeFuture)
        {
            return GetAbsenceQuota(EmployeeID, DateTime.Now, IncludeFuture);
        }

        public virtual List<INFOTYPE2006> GetAbsenceQuota(string EmployeeID, DateTime CheckDate, bool IncludeFuture)
        {
            return GetAbsenceQuota(EmployeeID, EmployeeID, CheckDate, IncludeFuture);
        }

        public virtual List<INFOTYPE2006> GetAbsenceQuota(string EmployeeID1, string EmployeeID2, DateTime CheckDate, bool IncludeFuture)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE2006> GetAbsenceQuotaList(string EmployeeID1, string EmployeeID2, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE2001> GetAbsenceList(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            return GetAbsenceList(EmployeeID, EmployeeID, BeginDate, EndDate);
        }

        public virtual List<INFOTYPE2001> GetAbsenceList(string EmployeeID, string EmployeeID2, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE2001> GetAbsenceListForCheckData(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual AbsAttCalculateResult CalculateAbsAttEntry(string EmployeeID, string SubType, DateTime BeginDate, DateTime EndDate, TimeSpan BeginTime, TimeSpan EndTime, bool FullDay, bool CheckQuota, bool TruncateDayOff)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual AbsAttCalculateResult CalculateAbsAttEntry(string EmployeeID, string SubType, DateTime BeginDate, DateTime EndDate, TimeSpan BeginTime, TimeSpan EndTime, bool FullDay, bool CheckQuota, bool TruncateDayOff, string CompCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE2001 GetINFOTYPE2001ByRequestNo(string RequestNo)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual AbsAttCalculateResult CalculateAbsAttEntry(string EmployeeID, string SubType, DateTime BeginDate, DateTime EndDate, TimeSpan BeginTime, TimeSpan EndTime, bool FullDay, bool CheckQuota, bool TruncateDayOff, DailyWS oDailyWS)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveAbsence(List<INFOTYPE2001> data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveAbsence(List<INFOTYPE2001> data, string CompCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE2002> GetAttendanceList(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveAttendance(List<INFOTYPE2002> data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveAttendance(List<INFOTYPE2002> data, string CompCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveSubstitution(List<INFOTYPE2003> data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveOTList1(List<SaveOTData> data, bool IsMark, List<string> RequestNoList, bool deleteItemNotInList)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SaveOTList(List<SaveOTData> data, bool IsMark)
        {
            SaveOTList(data, IsMark, null);
        }

        public void SaveOTList(List<SaveOTData> data, bool IsMark, List<string> requestNoList)
        {
            SaveOTList(data, IsMark, requestNoList, true);
        }

        public virtual void SaveOTList(List<SaveOTData> data, bool IsMark, List<string> requestNoList, bool deleteItemNotInList)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<PlannedOT> LoadPlannedOT(string EmployeeID, DateTime BeginDate, DateTime EndDate, bool OnlyOneRecord, bool OnlyCompletedRecord)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public PlannedOT LoadFirstPlannedOT(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            List<PlannedOT> list = LoadPlannedOT(EmployeeID, BeginDate, EndDate, true, true);
            PlannedOT oReturn = null;
            if (list.Count > 0)
            {
                oReturn = list[0];
            }
            return oReturn;
        }

        public List<PlannedOT> LoadPlannedOT(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            return LoadPlannedOT(EmployeeID, BeginDate, EndDate, false, false);
        }

        public virtual List<DailyOT> LoadDailyOT(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<DailyOTLog> LoadDailyOTLog(string RequestNo, string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<DailyOTLog> LoadDailyOTLog(int Month, int Year)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<DailyOTLog> LoadDailyOTLog(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<DailyOTLog> LoadDailyOTLog(string EmployeeID, DateTime BeginDate, DateTime EndDate, DailyOTLogStatus _DailyOTLogStatus)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public List<SaveOTData> LoadOTData(string EmployeeID, DateTime BeginDate, DateTime EndDate, string OnlyType, bool IsOnlyCompleted)
        {
            return LoadOTData(EmployeeID, BeginDate, EndDate, OnlyType, IsOnlyCompleted, false);
        }

        public virtual List<SaveOTData> LoadOTData(string EmployeeID, DateTime BeginDate, DateTime EndDate, string OnlyType, bool IsOnlyCompleted, bool IsOnlyNotSummary)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<DailyOT> LoadOTFromPM(string EmployeeID, string Period, int DayStart, int DayEnd)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void DeleteDailyOT(DailyOT daily)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<LeavePlan> GetLeavePlanList(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual int GetNextYearAbsenceQuota(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveLeavePlanList(string EmployeeID, int Year, List<LeavePlan> leavePlanList)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<SaveOTData> LoadOTDataByAssigner(string AssignerID, DateTime BeginDate, DateTime EndDate, string OnlyType, bool IsOnlyCompleted, bool IsOnlyNotSummary)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual bool IsCompleteCancleDailyOT(String EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<TimeEval> LoadTimeEval(DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<TimeEval> LoadTimeEval(string EmployeeID, DateTime ClockIN, DateTime ClockOUT)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //CHAT 2011-12-01
        public virtual bool OverlapDialyOTLog(string EmployeeID, DateTime BeginDate, DateTime EndDate, String RequestNo)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void DeleteOTLogByRefRequestNo(System.Data.DataTable data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveOTLog(System.Data.DataTable data, bool IsMark)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveDailyOTLogService(DataTable data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void PostOTLog(List<DailyOTLog> oDailyOTLog)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveOTLogStatus(System.Data.DataTable data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<DailyOTLog> LoadDailyOTLog(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void UpdateDailyOTLog(List<DailyOTLog> DailyOTLogList)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveInLateOutEarly(List<ClockinLateClockoutEarly> List, string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveTimeEval(List<TimeEval> TimeEvalList)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //AddBy: Ratchatawan W. (2012-02-23)
        public virtual List<DailyOTLog> LoadOTLogByCriteria(string EmployeeList, string StatusList, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        #endregion

        #region IHRTMService Members


        public virtual void SaveSubstitutionLog(DataRow data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveSubstitutionLog(List<INFOTYPE2003> data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void CancelSubstitutionLog(DataRow data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        #region IHRTMService Members


        public virtual List<DailyOTLog> LoadDailyOTLog(string EmployeeID, string BeginDate, string EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        #region IHRTMService Members


        public virtual List<DailyOTLog> LoadDailyOTLog(string EmployeeID, string BeginDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        public virtual void DeleteSubstitution(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual DataTable GetSummaryOTInMonth(string EmployeeID, DateTime BeginDate, DateTime EndDate, string RequestNo)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /* ==== Add by Han on 15/03/2019 : created interface method ==== */
        public virtual DataTable GetOTOverLimitPerWeek(string Employeeid, DateTime BeginDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /* ==== Add by Han on 10/05/2019 : created interface method ==== */
        public virtual DataTable GetOTOverLimitPerMonth(string Employeeid, DateTime BeginDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        /* =========================== End of Han ====================== */

        public virtual DataTable GetOTOnHoliday(string Employeeid, DateTime BeginDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE2006> GetAbsenceQuotaForDeduction(string EmployeeID1, string Employee2, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<ReportGroupAdmin> GetGroupReportAdmin(int reportGroupId)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DataTable GetAbsenceReportByEmployeeXML(string EmployeeXML, string AType, DateTime BeginDate, DateTime EndDate, EmployeeData oEmp)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<OTLogAll> GetOTLogAll(string year,string month)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<PeriodOTSumary> GetPeiodReportOTSummary(string category, DateTime date_now)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<DbAbsenceSickLeave> GetAbsenceSickLeaveSummary(int year,string abs_group)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<MasterWorkLocation> GetMasterWorklocation(int year)
        {
            throw new Exception("The method or operation is not implemented.");
        }  

        public virtual List<OrgUnitEmployeeByRole> GetListOrgUnitByUserRole(string employeeId,string userRole,string companyCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<OrgUnitEmployeeByRole> GetListOrgUnitNameByUserRole(List<OrgUnitEmployeeByRole> list_org_unit)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<string> GetEmployeeInOrganization(string[] list_org)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<string> GetEmployeeInOrganization(string orgId)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<PaySlipEmployeeInOrganization> GetPaySlipEmployeeInOrganization(List<string> list_employee)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<DbOTReason> GetDashbaordOTReason(List<string> list_emp,int year,int month)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<DbOTWorkType> GetListOTWorkType()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual OrganizationInfotype1000 GetNameOrganizationInfotype1000(string orgId)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<DbOvertimeInMonth> GetTransactionPayOvertimeByEmployeeInOrganization(List<string> list_emp,int year,int month)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DashbaordOvertimeAndSalaryByMonth GetSalaryOrOvertimeInYear(List<string> list_emp,int year)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DashbaordOTSalary6Months GetDashbaordOTSalary6Months(List<DateTime> list_date, List<string> list_emp)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DashbaordOTHours6Months GetDashbaordOTHours6Months(List<DateTime> list_date, List<string> list_emp)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DashboardSummarySalaryOvertimePayByYear GetDashboardSummarySalaryOvertimePayByYear(List<string> list_emp,int begin_year,int end_year)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DashboardSummarySalaryOvertimeHourByYear GetDashboardSummarySalaryOvertimeHourByYear(List<string> list_emp, int begin_year, int end_year)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual OTEmployeeByLevel GetDashboardOTEmployeeByLevel(List<string> list_emp, int begin_year, int end_year)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual OTEmployeeByBath GetDashboardOTEmployeeByBath(List<string> list_emp, int begin_year, int end_year)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<OrgUnitTree> GetOrgOTUnitTree(string employeeId, DateTime begin_date, DateTime end_date)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual decimal GetOTSummaryInMonth(string EmployeeID, DateTime BeginDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

       

        #region IHRTMService Members


        public virtual void SaveDelegateByAbsence(DataTable data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void DeleteDelegateByAbsence(INFOTYPE2001 data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<DelegateData> GetDelegateByAbsence(string EmployeeId, string DelegateToId, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveSubstitutionCustomTime(INFOTYPE2003 data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveDutyPaymentLog(DataTable data, bool IsDeleteOnly)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<DutyPaymentLog> GetDutyPaymentLog(string strEmployeeID, DateTime dtBeginDate, DateTime dtEndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void PostDutyPaymentDayWork(string strEmployeeID, Dictionary<DateTime, decimal> dictData)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void PostDutyPaymentDayOff(string strEmployeeID, Dictionary<DateTime, decimal> dictData)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void MarkTimesheet(string EmployeeID, string ApplicationKey, string SubKey1, string SubKey2, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark, bool isCheck, string Detail)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void MarkTimesheet(List<TimesheetData> oTimesheetData, bool isMark, bool isCheck)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void MarkTimesheet(string Employee, string Application, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark, bool isCheck, string Detail)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<DATACLASS.CollisionControl> LoadApplicationCollision(string ApplicationKey, string SubKey1, string SubKey2)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<TimesheetData> LoadTimesheetCollision(string EmployeeID, DateTime BeginDate, DateTime EndDate, DATACLASS.CollisionControl Key)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE2003_LOG> LoadINFOTYPE2003LOG(string RequestNo)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual ICollisionable LoadAdditionalDataCollisionable(string ApplicationKey)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<TimeElement> LoadArchiveEvent(string EmployeeID, DateTime BeginDate, DateTime EndDate, int RecordCount)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<CardSetting> LoadCardSetting(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<CardSetting> LoadCardSetting(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE2001> GetTM_AbsenceList(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<INFOTYPE2002> GetTM_AttendanceList(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<TimePair> GetTM_TimePairList(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<TM_Timesheet> GetTM_Timesheet(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<TimePair> LoadTimePairArchiveMappingByUser(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<TimePairArchive> LoadTimePairArchive(DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<TimePairArchive> LoadTimePairArchive(string EmpID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveTimePair(string EmployeeID, DateTime BeginDate, DateTime EndDate, List<TimePair> list)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void MappingTimePairInLateOutEarlyID(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveTimePairArchiveMappingByUser(List<TimePair> TimePairList)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveTimePairArchiveMappingByUser(List<TimePair> TimePairList,string RequestNo)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveArchiveEvent(string EmployeeID, DateTime BeginDate, DateTime EndDate, List<TimeElement> Data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveArchiveTimePair(string EmployeeID, DateTime BeginDate, DateTime EndDate, List<TimePair> list)
        {
            throw new Exception("The method or operation is not implemented.");
        }   
        
        public virtual DateTime LoadArchiveDate(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void PostTimePair(List<TimePair> Data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<TimeElement> GetTimeElement(string CardNo, DateTime BeginDate, DateTime EndDate, int RecordCount)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveLG_TIMEREQUEST(List<INFOTYPE2001> data, string type)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveLG_TIMEREQUEST(List<INFOTYPE2002> data, string type)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual bool CheckDailyOTLogIsView(string RequestNo)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<AreaWorkscheduleLog> GetAreaWorkscheduleLogByRequestNo(string RequestNo)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<AreaWorkscheduleLog> GetAreaWorkscheduleLogByPeriod(DateTime BeginDate, DateTime EndDate,string CreatorID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveAreaWorkscheduleLog(List<AreaWorkscheduleLog> data, bool IsMark)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        #endregion

        public virtual DataTable GetOTTrackingStatusReport(DateTime BeginDate, DateTime EndDate, string EmpID, string OrgUnitList, string CompanyCode, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DataTable GetDutyTrackingStatusReport(DateTime BeginDate, DateTime EndDate, string EmpID, string OrgUnitList, string CompanyCode, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DataTable GetAttendanceMonthlyReport(DateTime BeginDate, DateTime EndDate, string EmpID, string OrgUnitList, string CompanyCode, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DataTable GetTimeScanReport(DateTime BeginDate, DateTime EndDate, string EmpID, string OrgUnitList, string CompanyCode, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DataTable GetWorkingSummaryReport(DateTime BeginDate, DateTime EndDate, string EmpID, string OrgUnitList, string CompanyCode, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }
}