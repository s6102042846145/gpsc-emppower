using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using ESS.HR.TM.INTERFACE;
using ESS.SHAREDATASERVICE;
using ESS.TIMESHEET;

namespace ESS.HR.TM
{
    public class ServiceManager
    {
        #region Constructor
        private ServiceManager()
        {

        }
        #endregion

        #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        private static Dictionary<string, ServiceManager> Cache = new Dictionary<string, ServiceManager>();

        private static string ModuleID = "ESS.HR.TM";

        public string CompanyCode { get; set; }

        public static ServiceManager CreateInstance(string oCompanyCode)
        {
            
            ServiceManager oServiceManager = new ServiceManager()
            {
                CompanyCode = oCompanyCode
            };
            return oServiceManager;
        }
        #endregion MultiCompany  Framework

        #region " privatedata "        
        private Type GetService(string Mode, string ClassName)
        {
            Type oReturn = null;
            Assembly oAssembly;
            string assemblyName = string.Format("{0}.{1}", ModuleID, Mode.ToUpper());
            string typeName = string.Format("{0}.{1}.{2}", ModuleID, Mode.ToUpper(), ClassName);// CultureInfo.CurrentCulture.TextInfo.ToTitleCase(ClassName.ToLower()));
            oAssembly = Assembly.Load(assemblyName);    // Load assembly (dll)
            oReturn = oAssembly.GetType(typeName);      // Load class
            return oReturn;
        }

        #endregion " privatedata "


        internal IHRTMConfigService GetMirrorConfig(string Mode)
        {
            Type oType = GetService(Mode, "HRTMConfigServiceImpl");
            if (oType == null)
            {
                return null;
            }
            else
            {
                return (IHRTMConfigService)Activator.CreateInstance(oType, CompanyCode);
            }
        }

        internal IHRTMDataService GetMirrorService(string Mode)
        {
            Type oType = GetService(Mode, "HRTMDataServiceImpl");
            if (oType == null)
            {
                return null;
            }
            else
            {
                return (IHRTMDataService)Activator.CreateInstance(oType, CompanyCode);
            }
        }

        private string ESSCONNECTOR
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ESSCONNECTOR");
            }
        }

        private string ERPCONNECTOR
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ERPCONNECTOR");
            }
        }

        public IHRTMDataService ESSData
        {
            get
            {
                Type oType = GetService(ESSCONNECTOR, "HRTMDataServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IHRTMDataService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }

        public IHRTMConfigService ESSConfig
        {
            get
            {
                Type oType = GetService(ESSCONNECTOR, "HRTMConfigServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IHRTMConfigService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }

        public IHRTMDataService ERPData
        {
            get
            {
                Type oType = GetService(ERPCONNECTOR, "HRTMDataServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IHRTMDataService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }

        internal IHRTMConfigService ERPConfig
        {
            get
            {
                Type oType = GetService(ERPCONNECTOR, "HRTMConfigServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IHRTMConfigService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }

        internal static ITimeEventService GetTimeEventService(string ServiceCode)
        {
            ITimeEventService oService = null;
            Type oReturn = null;
            Assembly oAssembly;
            string assemblyName = "ESS.TIMESHEET.DB"; //string.Format("ESS.TIMESHEET.{0}", ServiceCode.Trim().ToUpper());
            string typeName = "ESS.TIMESHEET.TIMEEVENT.DB.TIMEEVENTSERVICE"; // string.Format("ESS.TIMESHEET.TIMEEVENT.{0}.TIMEEVENTSERVICE", ServiceCode.Trim().ToUpper());
            oAssembly = Assembly.Load(assemblyName);
            oReturn = oAssembly.GetType(typeName);
            if (oReturn != null)
            {
                oService = (ITimeEventService)Activator.CreateInstance(oReturn);
            }
            return oService;
        }
    }
}
