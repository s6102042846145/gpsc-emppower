using System;
using System.Data;
using System.Collections.Generic;
using System.Globalization;
using System.Configuration;
using System.Reflection;
using ESS.DATA;
using ESS.EMPLOYEE;
using ESS.HR.TM.CONFIG;
using ESS.HR.TM.DATACLASS;
using ESS.HR.TM.INFOTYPE;
using ESS.SHAREDATASERVICE;
using SAPInterfaceExt;
using SAP.Connector;
using ESS.PORTALENGINE;
using ESS.TIMESHEET;
using ESS.EMPLOYEE.CONFIG.TM;
using System.Web.UI.WebControls;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE.EXCEPTION;
using ESS.WORKFLOW;
using System.Linq;
using System.ComponentModel;
using ClosedXML.Excel;
using System.Text.RegularExpressions;
//using System.Linq;


namespace ESS.HR.TM
{
    public class HRTMManagement
    {
        #region Constructor
        private static CultureInfo oCL = new CultureInfo("en-US");
        private HRTMManagement()
        {
        }

        #endregion

        #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        private static Dictionary<string, HRTMManagement> Cache = new Dictionary<string, HRTMManagement>();

        private static string ModuleID = "ESS.HR.TM";
        public string CompanyCode { get; set; }

        public static HRTMManagement CreateInstance(string oCompanyCode)
        {

            HRTMManagement oHRTMManagement = new HRTMManagement()
            {
                CompanyCode = oCompanyCode
            };
            return oHRTMManagement;
        }
        #endregion MultiCompany  Framework


        #region SSODxcare

        public string SSODxcare_API_ID
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SSODxcare_API_ID");
            }
        }
        public string SSODxcare_APIKey
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SSODxcare_APIKey");
            }
        }
        public string SSODxcare_URL
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SSODxcare_URL");
            }
        }
        public string SSODxcare_URL_FUNCTION
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SSODxcare_URL_FUNCTION");
            }
        }

        #endregion

        public DateTime DFlexLate
        {
            get
            {
                DateTime nValue = DateTime.Today;

                return nValue.Add(new TimeSpan(9, 0, 0));
            }
        }

        public string TimeSheetStartDate
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "TIMESHEETSTARTDATE");
            }
        }

        public string TimeSheetSetMonth
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "TIMESHEETSETMONTH");
            }
        }

        public string DutyOffsetBefore
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "DutyOffsetBefore");
            }
        }

        public string DutyOffsetAfter
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "DutyOffsetAfter");
            }
        }

        public string DAILYOTOFFSETBEFORE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "DailyOTOffsetBefore");
            }
        }

        public string DAILYOTOFFSETAFTER
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "DailyOTOffsetAfter");
            }
        }

        public string MonthlyOTOffsetBefore
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "MonthlyOTOffsetBefore");
            }
        }

        public string MonthlyOTOffsetAfter
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "MonthlyOTOffsetAfter");
            }
        }

        public string PlannedOTOffsetBefore
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "PlannedOTOffsetBefore");
            }
        }

        public string PlannedOTOffsetAfter
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "PlannedOTOffsetAfter");
            }
        }

        public int AreaWorkscheduleOffsetBefore
        {
            get
            {
                return Convert.ToInt32(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "AreaWorkscheduleOffsetBefore"));
            }
        }

        public int AreaWorkscheduleOffsetAfter
        {
            get
            {
                return Convert.ToInt32(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "AreaWorkscheduleOffsetAfter"));
            }
        }

        public Boolean CALENDAR_USE_SUBSTITUTE
        {
            get
            {
                return Boolean.Parse(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "CALENDAR_USE_SUBSTITUTE"));
            }
        }

        public int VALIDATEPREVIOUSREQUESTDAILYOT
        {
            get
            {
                return Convert.ToInt32(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "VALIDATEPREVIOUSREQUESTDAILYOT"));
            }
        }

        public int VALIDATEPREVIOUSDAILYOTMANAGER
        {
            get
            {
                return Convert.ToInt32(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "VALIDATEPREVIOUSDAILYOTMANAGER"));
            }
        }

        public string AREA_OT_SHIFT_DELEGATETO_NORM_ONABSENCE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "AREA_OT_SHIFT_DELEGATETO_NORM_ONABSENCE");
            }
        }

        public string SUMMARYOTLIMITREQUEST
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SUMMARYOTLIMITREQUEST");
            }
        }

        public int BEGINEMPSUBGROUPSUMMARYOT
        {
            get
            {
                return Convert.ToInt32(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BEGINEMPSUBGROUPSUMMARYOT"));
            }
        }

        public int ENDEMPSUBGROUPSUMMARYOT
        {
            get
            {
                return Convert.ToInt32(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ENDEMPSUBGROUPSUMMARYOT"));
            }
        }

        public int MASSAPPROVE_TIME_DELAY
        {
            get
            {
                return Convert.ToInt32(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "MASSAPPROVE_TIME_DELAY"));
            }
        }

        public int YEAR_SICK_LEAVE
        {
            get
            {
                return Convert.ToInt16(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SHOW_YEAR_SICKLEAVE"));
            }
        }

        public int YEAR_OTLOG_HOUR
        {
            get
            {
                return Convert.ToInt16(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "YEAR_OTLOG_HOUR"));
            }
        }

        public int YEAR_DASHBOARD_OT_Tab2
        {
            get
            {
                return Convert.ToInt16(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "DASHBOARD_OT_Tab2"));
            }
        }

        public int YEAR_DASHBOARD_OT_Tab3
        {
            get
            {
                return Convert.ToInt16(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "DASHBOARD_OT_Tab3"));
            }
        }

        public int MAXLEVELMD
        {
            get
            {
                return Convert.ToInt32(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "MaxLevelMD"));
            }
        }

        public string GetCommonText(string Category, string Language, string Code)
        {
            CommonText oCommonText = new CommonText(CompanyCode);
            return oCommonText.LoadText(Category, Language, Code);
            //return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetCommonText(Category, Language, Code);
        }

        public void MarkUpdate(string EmployeeID, string DataCategory, string RequestNo, bool isMarkIn)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.MarkUpdate(EmployeeID, DataCategory, RequestNo, isMarkIn);
        }

        public bool CheckMark(string EmployeeID, string DataCategory)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.CheckMark(EmployeeID, DataCategory);
        }

        public bool CheckMark(string EmployeeID, string DataCategory, string ReqNo)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.CheckMark(EmployeeID, DataCategory, ReqNo);
        }

        //�����ż���� � �ѹ��� User Ẻ Flex ������ WorkingTime ������ ���礡Ѻ���ҷ�� User Clock-In �������ѹ���
        public void GetWorkingTimeOfFlex(DateTime CheckDate, INFOTYPE0007 info0007, ref DateTime WorkBeginDateTime, ref DateTime WorkEndDateTime)
        {
            DailyFlexTime oDailyFlexTime;
            TimeSpan tmFLXClockIN;
            TimeSpan tmFLXClockOUT;
            TimeSpan tmFLXClockINLAST;
            DateTime checkEndDateTime;

            //�֧��� TimeElement 㹪�ǧ��鹢���ҨѺ�������
            List<TimeElement> oTimeElement = LoadTimeElement(info0007.EmployeeID, CheckDate, CheckDate.AddDays(1).AddSeconds(-1), -1, true, false);
            //Get flex time list from iservice3 table 'DailyFlexTime'
            List<DailyFlexTime> FlexList = GetDailyFlexTimeByFlexCode(info0007.WFRule);

            //�ҡ����������������͡ � �ѹ������ �кѧ�Ѻ����� FlexList ��� 0 �� WorkingTime
            if (oTimeElement == null || oTimeElement.Count == 0)
            {
                oDailyFlexTime = FlexList[0];
                TimeSpan BeginTime = new TimeSpan(Convert.ToInt32(oDailyFlexTime.BeginTime.Split(':')[0]), Convert.ToInt32(oDailyFlexTime.BeginTime.Split(':')[1]), Convert.ToInt32(oDailyFlexTime.BeginTime.Split(':')[2]));
                TimeSpan EndTime = new TimeSpan(Convert.ToInt32(oDailyFlexTime.EndTime.Split(':')[0]), Convert.ToInt32(oDailyFlexTime.EndTime.Split(':')[1]), Convert.ToInt32(oDailyFlexTime.EndTime.Split(':')[2]));
                WorkBeginDateTime = CheckDate.Add(BeginTime);
                WorkEndDateTime = EndTime < BeginTime ? CheckDate.Add(EndTime).AddDays(1) : CheckDate.Add(EndTime);
            }
            //�ҡ���������/�͡ � �ѹ��� �Ӥӹǳ������ WorkingTime �ͧ Flex ������͹����仵�㹪�ǧ�˹
            else
            {
                //�Ѻ�������
                List<TimePair> pairs = MatchingClockNew(info0007.EmployeeID, oTimeElement, true, CheckDate, CheckDate.AddDays(1).AddSeconds(-1), new Dictionary<DateTime, string>());
                TimePair tmPair = pairs.Find(delegate (TimePair tm) { return tm.Date == CheckDate.Date; });

                //�������դ������ � �ѹ������  �кѧ�Ѻ����� FlexList ��� 0 �� WorkingTime
                //if (tmPair == null)
                if (tmPair is null || tmPair.ClockIN is null || tmPair.ClockOUT is null)
                {
                    oDailyFlexTime = FlexList[0];
                    TimeSpan BeginTime = new TimeSpan(Convert.ToInt32(oDailyFlexTime.BeginTime.Split(':')[0]), Convert.ToInt32(oDailyFlexTime.BeginTime.Split(':')[1]), Convert.ToInt32(oDailyFlexTime.BeginTime.Split(':')[2]));
                    TimeSpan EndTime = new TimeSpan(Convert.ToInt32(oDailyFlexTime.EndTime.Split(':')[0]), Convert.ToInt32(oDailyFlexTime.EndTime.Split(':')[1]), Convert.ToInt32(oDailyFlexTime.EndTime.Split(':')[2]));
                    WorkBeginDateTime = CheckDate.Add(BeginTime);
                    WorkEndDateTime = EndTime < BeginTime ? CheckDate.Add(EndTime).AddDays(1) : CheckDate.Add(EndTime);
                }
                //����դ������ � �ѹ��� �Фӹǳ��͵�����͹�
                else
                {
                    EmployeeData empData = new EmployeeData(info0007.EmployeeID, CheckDate);
                    INFOTYPE2001 oINFOTYPE2001 = new INFOTYPE2001();
                    INFOTYPE2002 oINFOTYPE2002 = new INFOTYPE2002();
                    List<INFOTYPE2001> oAbsenceList = oINFOTYPE2001.GetData(empData, CheckDate, CheckDate);
                    List<INFOTYPE2002> oAttendanceList = oINFOTYPE2002.GetData(empData, CheckDate, CheckDate);

                    TimeSpan TCompareAttendance = tmPair.ClockIN.EventTime.TimeOfDay;

                    var minBeginTimeAbs = oAbsenceList.Where(t => t.Status == "RECORDED")
                   .OrderBy(t => t.BeginTime)
                   .FirstOrDefault();

                    if (minBeginTimeAbs != null && minBeginTimeAbs.BeginTime < TCompareAttendance)
                    {
                        TCompareAttendance = minBeginTimeAbs.BeginTime;
                    }

                    var minBeginTimeAtt = oAttendanceList.Where(t => t.Status == "RECORDED")
                   .OrderBy(t => t.BeginTime)
                   .FirstOrDefault();

                    if (minBeginTimeAtt != null && minBeginTimeAtt.BeginTime < TCompareAttendance)
                    {
                        TCompareAttendance = minBeginTimeAtt.BeginTime;
                    }


                    for (int i = 0; i < FlexList.Count; i++)
                    {
                        oDailyFlexTime = FlexList[i];
                        tmFLXClockIN = new TimeSpan(Convert.ToInt32(oDailyFlexTime.BeginTime.Split(':')[0]), Convert.ToInt32(oDailyFlexTime.BeginTime.Split(':')[1]), Convert.ToInt32(oDailyFlexTime.BeginTime.Split(':')[2]));
                        tmFLXClockOUT = new TimeSpan(Convert.ToInt32(oDailyFlexTime.EndTime.Split(':')[0]), Convert.ToInt32(oDailyFlexTime.EndTime.Split(':')[1]), Convert.ToInt32(oDailyFlexTime.EndTime.Split(':')[2]));
                        tmFLXClockINLAST = new TimeSpan(Convert.ToInt32(FlexList[FlexList.Count - 1].BeginTime.Split(':')[0]), Convert.ToInt32(FlexList[FlexList.Count - 1].BeginTime.Split(':')[1]), Convert.ToInt32(FlexList[FlexList.Count - 1].BeginTime.Split(':')[2]));

                        WorkBeginDateTime = CheckDate.Add(tmFLXClockIN);
                        WorkEndDateTime = tmFLXClockOUT < tmFLXClockIN ? CheckDate.Add(tmFLXClockOUT).AddDays(1) : CheckDate.Add(tmFLXClockOUT);
                        //if (tmPair.ClockIN.EventTime.TimeOfDay <= tmFLXClockIN)
                        if (TCompareAttendance <= tmFLXClockIN)
                            break;
                    }
                }
            }
        }

        //�����ż���� � �ѹ��� User Ẻ Flex ������ WorkingTime ������ ���礡Ѻ���ҷ�� User Clock-In �������ѹ���
        public DailyFlexTime GetWorkingTimeOfFlex(DateTime CheckDate, INFOTYPE0007 info0007, ref TimePair oTimePair)
        {
            DateTime WorkBeginDateTime = oTimePair.ClockIN.EventTime;
            DateTime WorkEndDateTime = oTimePair.ClockOUT.EventTime;
            DailyFlexTime oDailyFlexTime = new DailyFlexTime();
            TimeSpan tmFLXClockIN;
            TimeSpan tmFLXClockOUT;
            TimeSpan tmFLXClockINLAST;
            DateTime checkEndDateTime;

            //�֧��� TimeElement 㹪�ǧ��鹢���ҨѺ�������
            List<TimeElement> oTimeElement = LoadTimeElement(info0007.EmployeeID, CheckDate, CheckDate.AddDays(1).AddSeconds(-1), -1, true, true);
            //Get flex time list from iservice3 table 'DailyFlexTime'
            List<DailyFlexTime> FlexList = GetDailyFlexTimeByFlexCode(info0007.WFRule);

            //�ҡ����������������͡ � �ѹ������ �кѧ�Ѻ����� FlexList ��� 0 �� WorkingTime
            if (oTimeElement == null || oTimeElement.Count == 0)
            {
                oDailyFlexTime = FlexList[0];
                TimeSpan BeginTime = new TimeSpan(Convert.ToInt32(oDailyFlexTime.BeginTime.Split(':')[0]), Convert.ToInt32(oDailyFlexTime.BeginTime.Split(':')[1]), Convert.ToInt32(oDailyFlexTime.BeginTime.Split(':')[2]));
                TimeSpan EndTime = new TimeSpan(Convert.ToInt32(oDailyFlexTime.EndTime.Split(':')[0]), Convert.ToInt32(oDailyFlexTime.EndTime.Split(':')[1]), Convert.ToInt32(oDailyFlexTime.EndTime.Split(':')[2]));
                WorkBeginDateTime = CheckDate.Add(BeginTime);
                WorkEndDateTime = EndTime < BeginTime ? CheckDate.Add(EndTime).AddDays(1) : CheckDate.Add(EndTime);
            }
            //�ҡ���������/�͡ � �ѹ��� �Ӥӹǳ������ WorkingTime �ͧ Flex ������͹����仵�㹪�ǧ�˹
            else
            {
                //�Ѻ�������
                List<TimePair> pairs = MatchingClockNew(info0007.EmployeeID, oTimeElement, true, CheckDate, CheckDate.AddDays(1).AddSeconds(-1), new Dictionary<DateTime, string>());
                TimePair tmPair = pairs.Find(delegate (TimePair tm) { return tm.Date == CheckDate.Date; });

                //�������դ������ � �ѹ������  �кѧ�Ѻ����� FlexList ��� 0 �� WorkingTime
                //if (tmPair == null)
                if (tmPair is null)
                {
                    oDailyFlexTime = FlexList[0];
                    TimeSpan BeginTime = new TimeSpan(Convert.ToInt32(oDailyFlexTime.BeginTime.Split(':')[0]), Convert.ToInt32(oDailyFlexTime.BeginTime.Split(':')[1]), Convert.ToInt32(oDailyFlexTime.BeginTime.Split(':')[2]));
                    TimeSpan EndTime = new TimeSpan(Convert.ToInt32(oDailyFlexTime.EndTime.Split(':')[0]), Convert.ToInt32(oDailyFlexTime.EndTime.Split(':')[1]), Convert.ToInt32(oDailyFlexTime.EndTime.Split(':')[2]));
                    WorkBeginDateTime = CheckDate.Add(BeginTime);
                    WorkEndDateTime = EndTime < BeginTime ? CheckDate.Add(EndTime).AddDays(1) : CheckDate.Add(EndTime);
                }
                //����դ������ � �ѹ��� �Фӹǳ��͵�����͹�
                else
                {
                    for (int i = 0; i < FlexList.Count; i++)
                    {
                        oDailyFlexTime = FlexList[i];
                        tmFLXClockIN = new TimeSpan(Convert.ToInt32(oDailyFlexTime.BeginTime.Split(':')[0]), Convert.ToInt32(oDailyFlexTime.BeginTime.Split(':')[1]), Convert.ToInt32(oDailyFlexTime.BeginTime.Split(':')[2]));
                        tmFLXClockOUT = new TimeSpan(Convert.ToInt32(oDailyFlexTime.EndTime.Split(':')[0]), Convert.ToInt32(oDailyFlexTime.EndTime.Split(':')[1]), Convert.ToInt32(oDailyFlexTime.EndTime.Split(':')[2]));
                        tmFLXClockINLAST = new TimeSpan(Convert.ToInt32(FlexList[FlexList.Count - 1].BeginTime.Split(':')[0]), Convert.ToInt32(FlexList[FlexList.Count - 1].BeginTime.Split(':')[1]), Convert.ToInt32(FlexList[FlexList.Count - 1].BeginTime.Split(':')[2]));


                        WorkBeginDateTime = CheckDate.Add(tmFLXClockIN);
                        WorkEndDateTime = tmFLXClockOUT < tmFLXClockIN ? CheckDate.Add(tmFLXClockOUT).AddDays(1) : CheckDate.Add(tmFLXClockOUT);
                        if (tmPair.ClockIN.EventTime.TimeOfDay <= tmFLXClockIN)
                            break;
                    }
                }

            }
            return oDailyFlexTime;
        }

        public List<TimePair> LoadTimePairArchiveMappingByUser(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.LoadTimePairArchiveMappingByUser(EmployeeID, BeginDate, EndDate);
        }

        public List<TimePairArchive> LoadTimepairArchive(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.LoadTimePairArchive(EmployeeID, BeginDate, EndDate);
        }

        public List<TimePairArchive> LoadTimePairArchive(DateTime BeginDate, DateTime EndDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.LoadTimePairArchive(BeginDate, EndDate);
        }

        public void PostTimePair(List<TimePair> Data)
        {
            ServiceManager.CreateInstance(CompanyCode).ERPData.PostTimePair(Data);
        }

        public List<TimePair> MatchingClockNew(string EmployeeID, List<TimeElement> TimeElementList, bool FillBlankPair, DateTime BeginDate, DateTime EndDate, Dictionary<DateTime, string> optionAttendance)
        {
            List<TimePair> MatchTimePair = new List<TimePair>();

            ////��� TimeElement �� 0 �� Return List Timepair ��ҧ��Ѻ�
            //if (TimeElementList.Count == 0)
            //    return new List<TimePair>();


            //���§�ӴѺ TimeElement ���� 
            TimeElementList.Sort(new TimeElementSortingTime());

            //�ѹ�����������������ش��èѺ�������
            //�ҡ BeginDate �դ����ҡѺ DateTime.MinValue �����ѹ����á� TimeElementList ᷹
            //�ҡ EndDate �դ����ҡѺ DateTime.MinValue �����ѹ����ش����� TimeElementList ᷹
            DateTime oBeginDate = BeginDate != DateTime.MinValue ? BeginDate : TimeElementList[0].EventTime.Date;
            DateTime oEndDate = EndDate != DateTime.MinValue ? EndDate : TimeElementList[TimeElementList.Count - 1].EventTime.Date;

            DateTime oRunningDate = TimeElementList.Count == 0 || oBeginDate < TimeElementList[0].EventTime.Date ? oBeginDate : TimeElementList[0].EventTime.Date, oCheckDateBegin, oCheckDateEnd;
            EmployeeData oEmployeeData;
            DailyWS oDWS;
            TimePair oTimePair;
            List<TimeElement> TimeElementForShift = new List<TimeElement>();
            TimeElementForShift = TimeElementList;

            //- TimeElement ���١ Map �� User ���Ǩ����ӡ�Ѻ�����ա
            List<TimePair> oTimePairList = ServiceManager.CreateInstance(CompanyCode).ESSData.LoadTimePairArchiveMappingByUser(EmployeeID, BeginDate, EndDate);
            if (oTimePairList.Count > 0)
            {
                List<DateTime> oUsedTimeElement = new List<DateTime>();
                foreach (TimePair item in oTimePairList)
                {
                    if (item.ClockIN != null && item.ClockOUT != null)
                    {
                        foreach (TimeElement element in TimeElementForShift.FindAll(delegate (TimeElement TE) { return TE.EventTime >= item.ClockIN.EventTime && TE.EventTime <= item.ClockOUT.EventTime; }))
                        {
                            oUsedTimeElement.Add(element.EventTime);
                        }
                    }
                    else
                    {
                        if (item.ClockIN != null)
                            oUsedTimeElement.Add(item.ClockIN.EventTime);
                        if (item.ClockOUT != null)
                            oUsedTimeElement.Add(item.ClockOUT.EventTime);
                    }
                }

                if (oUsedTimeElement.Count > 0)
                    TimeElementForShift = TimeElementForShift.FindAll(delegate (TimeElement TE) { return !oUsedTimeElement.Contains(TE.EventTime); });

                oUsedTimeElement.Clear();
            }

            List<TimeElement> TimeElementMatchRange = new List<TimeElement>();
            DailyWS oDWSBeforeDayOff = new DailyWS();
            DailyWS oDWSOriginal = new DailyWS();

            while (oRunningDate <= oEndDate)
            {
                DateTime tmpDate = oRunningDate;
                try
                {
                    oTimePair = new TimePair();
                    oTimePair.Date = tmpDate;
                    oTimePair.EmployeeID = EmployeeID;

                    oEmployeeData = new EmployeeData(EmployeeID, tmpDate);
                    if (oEmployeeData.DateSpecific.HiringDate <= tmpDate.Date)
                    {
                        oDWS = oEmployeeData.GetDailyWorkSchedule(tmpDate.Date, true);
                        oDWSOriginal = oDWS;
                        oTimePair.DWS = oDWS;
                        oTimePair.DWSCode = oDWS.DailyWorkscheduleCode;

                        //����ѹ������ѹ OFF ����� DailyWS �ѹ�ӧҹ��͹˹�ҹ�鹷������ش
                        if (oDWS.DailyWorkscheduleCode == "OFF")
                        {
                            oTimePair.IsDayOff = true;
                            oTimePair.DWS.WorkBeginTime = new TimeSpan(0, 0, 0);
                            oTimePair.DWS.WorkEndTime = new TimeSpan(0, 0, 0);
                            DateTime oBackwardDate = tmpDate.AddDays(-1);

                            while (oDWS.DailyWorkscheduleCode == "OFF")
                            {
                                try
                                {
                                    EmployeeData tmpEmployeeData = new EmployeeData(EmployeeID, oBackwardDate);
                                    oDWS = tmpEmployeeData.GetDailyWorkSchedule(oBackwardDate, true);
                                }
                                catch
                                {
                                    EmployeeData tmpEmployeeData = new EmployeeData(EmployeeID);
                                    oDWS.DailyWorkscheduleCode = tmpEmployeeData.OrgAssignment.SubAreaSetting.Description.ToUpper();
                                    break;
                                }
                                oBackwardDate = oBackwardDate.AddDays(-1);
                            }

                        }

                        //��Ҿ�ѡ�ҹ�� Norm/Flex
                        //�� TimeElement 㹪�ǧ 04:00 - 03:59(�ͧ�ѹ�Ѵ�) ���� Record �á�� Clock-In ��� Record �ش������ Clock-Out
                        if (!String.IsNullOrEmpty(oDWS.DailyWorkscheduleCode) && !oDWS.DailyWorkscheduleCode.StartsWith("S"))
                        {
                            oCheckDateBegin = tmpDate.Add(new TimeSpan(4, 0, 0));
                            oCheckDateEnd = oCheckDateBegin.AddDays(1).AddSeconds(-1);
                            TimeElementMatchRange = TimeElementForShift.FindAll(delegate (TimeElement TE) { return TE.EventTime >= oCheckDateBegin && TE.EventTime <= oCheckDateEnd && !TE.IsSystemDelete; });
                            if (TimeElementMatchRange.Count > 0)
                            {
                                if (TimeElementMatchRange.Count > 0)
                                {
                                    oTimePair.ClockIN = TimeElementMatchRange[0];
                                    //�Ӥ�� Element ���١�������� Timepair �͡�ҡ TimeElementForShift
                                    TimeElementForShift.Remove(TimeElementMatchRange[0]);
                                }
                                if (TimeElementMatchRange.Count > 1)
                                {
                                    oTimePair.ClockOUT = TimeElementMatchRange[TimeElementMatchRange.Count - 1];
                                    //�Ӥ�� Element ���١�������� Timepair �͡�ҡ TimeElementForShift
                                    TimeElementForShift.Remove(TimeElementMatchRange[TimeElementMatchRange.Count - 1]);
                                }
                            }

                            MatchTimePair.Add(oTimePair);
                        }
                        //��Ҿ�ѡ�ҹ�� Shift ������� TimeElement � TimeElementForShift
                        else
                        {
                            List<int> lstRemoveIndex = new List<int>();
                            //���͡�ѹ���ç�Ѻ RunningDate �ѹ�á���� ClockIn
                            if (TimeElementForShift.Exists(delegate (TimeElement TE) { return TE.EventTime.Date == tmpDate.Date && !TE.IsSystemDelete; }))
                            {
                                int index = TimeElementForShift.FindIndex(delegate (TimeElement TE) { return TE.EventTime.Date == tmpDate.Date && !TE.IsSystemDelete; });

                                while (index > -1 && TimeElementForShift[index] != null && TimeElementForShift[index].EventTime != null && TimeElementForShift[index].EventTime.Date == tmpDate.Date)
                                {
                                    //oTimePair = new TimePair();
                                    //oTimePair.Date = tmpDate;
                                    //oTimePair.EmployeeID = EmployeeID;
                                    //oTimePair.DWS = oDWSOriginal;
                                    //oTimePair.DWSCode = oDWSOriginal.DailyWorkscheduleCode;
                                    //if (oTimePair.DWSCode == "OFF")
                                    //    oTimePair.IsDayOff = true;

                                    if (oTimePair.ClockOUT != null)// �������͹��ҡ�բ������ٴ�ѵ��͡�������Ǩ��Ѿഷ������������
                                    {
                                        lstRemoveIndex.Add(index);
                                        oTimePair.ClockOUT = TimeElementForShift[index];
                                        TimeElementForShift[index].IsSystemDelete = true;
                                        index++;
                                    }
                                    else
                                    {
                                        lstRemoveIndex.Add(index);
                                        oTimePair.ClockIN = TimeElementForShift[index];
                                        TimeElementForShift[index].IsSystemDelete = true;
                                        index++;
                                        if (index <= TimeElementForShift.Count - 1)
                                        {
                                            while (TimeElementForShift[index].IsSystemDelete)
                                                index++;

                                            //�ѹ���Դ�ѹ��ͧ����Թ���§�׹�ͧ oRunningdate.Date+2
                                            if ((index <= TimeElementForShift.Count - 1) && (TimeElementForShift[index].EventTime <= tmpDate.Date.AddDays(2)))
                                            {
                                                lstRemoveIndex.Add(index);
                                                oTimePair.ClockOUT = TimeElementForShift[index];
                                                TimeElementForShift[index].IsSystemDelete = true;
                                                index++;
                                            }
                                        }
                                        MatchTimePair.Add(oTimePair);

                                        if (index > TimeElementForShift.Count - 1)
                                            break;
                                    }
                                }
                            }
                            else
                            {
                                MatchTimePair.Add(oTimePair);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
                oRunningDate = oRunningDate.AddDays(1);
            }

            //Merge between MatchingClockRealTime with Table TimePairArchiveMappingByUser
            //List<TimePair> oTimePairList = TimesheetManagement.LoadTimePairArchiveMappingByUser(EmployeeID, BeginDate, EndDate);
            foreach (TimePair obj in oTimePairList)
            {
                if (MatchTimePair.Exists(delegate (TimePair otp) { return otp.Date == obj.Date; }))
                {
                    oTimePair = MatchTimePair.Find(delegate (TimePair otp) { return otp.Date == obj.Date; });

                    if (obj.ClockIN != null)
                        oTimePair.ClockIN = obj.ClockIN;//.EventTime = obj.ClockIN.EventTime;
                    else
                        oTimePair.ClockIN = null;
                    if (obj.ClockOUT != null)
                        oTimePair.ClockOUT = obj.ClockOUT; //.EventTime = obj.ClockOUT.EventTime;
                    else
                        oTimePair.ClockOUT = null;

                }
            }

            return MatchTimePair.FindAll(delegate (TimePair tp) { return tp.Date >= BeginDate && tp.Date <= EndDate; });
        }

        public List<TimePair> MatchingClockNew(string EmployeeID, List<TimeElement> TimeElementList, bool FillBlankPair, DateTime BeginDate, DateTime EndDate)
        {
            List<TimePair> MatchTimePair = new List<TimePair>();

            ////��� TimeElement �� 0 �� Return List Timepair ��ҧ��Ѻ�
            //if (TimeElementList.Count == 0)
            //    return new List<TimePair>();


            //���§�ӴѺ TimeElement ���� 
            TimeElementList.Sort(new TimeElementSortingTime());

            //�ѹ�����������������ش��èѺ�������
            //�ҡ BeginDate �դ����ҡѺ DateTime.MinValue �����ѹ����á� TimeElementList ᷹
            //�ҡ EndDate �դ����ҡѺ DateTime.MinValue �����ѹ����ش����� TimeElementList ᷹
            DateTime oBeginDate = BeginDate != DateTime.MinValue ? BeginDate : TimeElementList[0].EventTime.Date;
            DateTime oEndDate = EndDate != DateTime.MinValue ? EndDate : TimeElementList[TimeElementList.Count - 1].EventTime.Date;

            DateTime oRunningDate = TimeElementList.Count == 0 || oBeginDate < TimeElementList[0].EventTime.Date ? oBeginDate : TimeElementList[0].EventTime.Date, oCheckDateBegin, oCheckDateEnd;
            EmployeeData oEmployeeData = null;
            DailyWS oDWS;
            TimePair oTimePair;
            List<TimeElement> TimeElementForShift = new List<TimeElement>();
            TimeElementForShift = TimeElementList;

            //- TimeElement ���١ Map �� User ���Ǩ����ӡ�Ѻ�����ա
            List<TimePair> oTimePairList = LoadTimePairArchiveMappingByUser(EmployeeID, BeginDate, EndDate.AddDays(1));
            if (oTimePairList.Count > 0)
            {
                List<DateTime> oUsedTimeElement = new List<DateTime>();
                foreach (TimePair item in oTimePairList)
                {
                    if (item.ClockIN != null && item.ClockOUT != null)
                    {
                        foreach (TimeElement element in TimeElementForShift.FindAll(delegate (TimeElement TE) { return TE.EventTime >= item.ClockIN.EventTime && TE.EventTime <= item.ClockOUT.EventTime; }))
                        {
                            oUsedTimeElement.Add(element.EventTime);
                        }
                    }
                    else
                    {
                        if (item.ClockIN != null)
                            oUsedTimeElement.Add(item.ClockIN.EventTime);
                        if (item.ClockOUT != null)
                            oUsedTimeElement.Add(item.ClockOUT.EventTime);
                    }
                }

                if (oUsedTimeElement.Count > 0)
                    TimeElementForShift = TimeElementForShift.FindAll(delegate (TimeElement TE) { return !oUsedTimeElement.Contains(TE.EventTime); });

                oUsedTimeElement.Clear();
            }

            List<TimeElement> TimeElementMatchRange = new List<TimeElement>();
            DailyWS oDWSBeforeDayOff = new DailyWS();
            DailyWS oDWSOriginal = new DailyWS();
            DateTime dtTimeElement = DateTime.MinValue;
            Boolean IsNextDay = true;

            while (oRunningDate <= oEndDate.Date.AddDays(1) || IsNextDay)
            {
                DateTime tmpDate = oRunningDate;
                IsNextDay = false;

                if (oEmployeeData == null || oRunningDate.Date > oEmployeeData.OrgAssignment.EndDate || oRunningDate.Date < oEmployeeData.OrgAssignment.BeginDate.Date)
                    oEmployeeData = new EmployeeData(EmployeeID, tmpDate);
                if (oEmployeeData.DateSpecific.HiringDate != DateTime.MinValue && oEmployeeData.DateSpecific.HiringDate <= tmpDate.Date)
                {
                    #region ��Ҿ�ѡ�ҹ�� Norm/Flex
                    if (oEmployeeData.EmpSubAreaType != EmployeeSubAreaType.Shift12)
                    {
                        oTimePair = new TimePair();
                        oTimePair.Date = tmpDate;
                        oTimePair.EmployeeID = EmployeeID;

                        oDWS = oEmployeeData.GetDailyWorkSchedule(tmpDate.Date, true);
                        oDWSOriginal = oDWS;
                        oTimePair.DWS = oDWS;
                        oTimePair.DWSCode = oDWS.DailyWorkscheduleCode;
                        oTimePair.IsDayOff = oDWS.IsDayOff;
                        oTimePair.IsHoliday = oDWS.IsHoliday;
                        //�� TimeElement 㹪�ǧ 04:00 - 03:59(�ͧ�ѹ�Ѵ�) ���� Record �á�� Clock-In ��� Record �ش������ Clock-Out
                        if (!String.IsNullOrEmpty(oDWS.DailyWorkscheduleCode) && !oDWS.DailyWorkscheduleCode.StartsWith("S"))
                        {
                            //����ѹ������ѹ OFF ����� DailyWS �ѹ�ӧҹ��͹˹�ҹ�鹷������ش
                            if (oDWS.IsDayOffOrHoliday)
                            {
                                oTimePair.DWS.WorkBeginTime = new TimeSpan(0, 0, 0);
                                oTimePair.DWS.WorkEndTime = new TimeSpan(0, 0, 0);
                                DateTime oBackwardDate = tmpDate.AddDays(-1);
                                EmployeeData tmpEmployeeData = null;
                                while (oDWS != null && oDWS.IsDayOffOrHoliday)
                                {
                                    if (tmpEmployeeData == null || oBackwardDate.Date > tmpEmployeeData.OrgAssignment.EndDate || oBackwardDate.Date < tmpEmployeeData.OrgAssignment.BeginDate.Date)
                                        tmpEmployeeData = new EmployeeData(EmployeeID, oBackwardDate);
                                    oDWS = tmpEmployeeData.GetDailyWorkSchedule(oBackwardDate, true);

                                    oBackwardDate = oBackwardDate.AddDays(-1);
                                }

                                if (oDWS == null)
                                {
                                    oBackwardDate = tmpDate.AddDays(1);
                                    oDWS = tmpEmployeeData.GetDailyWorkSchedule(oBackwardDate, true);
                                    while (oDWS != null && oDWS.IsDayOffOrHoliday)
                                    {
                                        if (tmpEmployeeData == null || oBackwardDate.Date > tmpEmployeeData.OrgAssignment.EndDate || oBackwardDate.Date < tmpEmployeeData.OrgAssignment.BeginDate.Date)
                                            tmpEmployeeData = new EmployeeData(EmployeeID, oBackwardDate);
                                        oDWS = tmpEmployeeData.GetDailyWorkSchedule(oBackwardDate, true);

                                        oBackwardDate = oBackwardDate.AddDays(1);
                                    }
                                }
                            }
                            oCheckDateBegin = tmpDate.Add(new TimeSpan(4, 0, 0));
                            oCheckDateEnd = oCheckDateBegin.AddDays(1).AddSeconds(-1);
                            TimeElementMatchRange = TimeElementForShift.FindAll(delegate (TimeElement TE) { return TE.EventTime >= oCheckDateBegin && TE.EventTime <= oCheckDateEnd && !TE.IsSystemDelete; });
                            if (TimeElementMatchRange.Count > 0)
                            {
                                if (TimeElementMatchRange.Count > 0)
                                {
                                    oTimePair.ClockIN = TimeElementMatchRange[0];
                                    //�Ӥ�� Element ���١�������� Timepair �͡�ҡ TimeElementForShift
                                    TimeElementForShift.Remove(TimeElementMatchRange[0]);
                                }
                                if (TimeElementMatchRange.Count > 1)
                                {
                                    oTimePair.ClockOUT = TimeElementMatchRange[TimeElementMatchRange.Count - 1];
                                    //�Ӥ�� Element ���١�������� Timepair �͡�ҡ TimeElementForShift
                                    TimeElementForShift.Remove(TimeElementMatchRange[TimeElementMatchRange.Count - 1]);
                                }
                            }
                        }
                        List<TimePair> tmpTP = MatchTimePair.FindAll(delegate (TimePair TP) { return TP.Date == oTimePair.Date; });
                        if (tmpTP.Count <= 0)
                            MatchTimePair.Add(oTimePair);
                        oRunningDate = oRunningDate.AddDays(1);
                    }
                    #endregion
                    #region Shift
                    else
                    {
                        oDWS = oEmployeeData.GetDailyWorkSchedule(tmpDate.Date, true);

                        ////��Ҿ�ѡ�ҹ�� Shift
                        //if(oEmployeeData.EmpSubAreaType == EmployeeSubAreaType.Shift12)
                        //{   
                        //�ó����ѹ�ӧҹ
                        if (oDWS != null && !oDWS.IsDayOffOrHoliday)
                        {
                            DateTime oBackwardDate = tmpDate;
                            EmployeeData tmpEmployeeData = null;
                            DailyWS oDWSBackward;
                            do
                            {
                                oBackwardDate = oBackwardDate.AddDays(-1);
                                if (tmpEmployeeData == null || oBackwardDate.Date > tmpEmployeeData.OrgAssignment.EndDate || oBackwardDate.Date < tmpEmployeeData.OrgAssignment.BeginDate.Date)
                                    tmpEmployeeData = new EmployeeData(EmployeeID, oBackwardDate);
                                oDWSBackward = tmpEmployeeData.GetDailyWorkSchedule(oBackwardDate, true);
                            } while (oDWSBackward != null && oDWSBackward.IsDayOffOrHoliday);

                            if (oDWSBackward == null)
                            {
                                oDWSBackward = tmpEmployeeData.GetDailyWorkSchedule(oBackwardDate, true);
                            }
                            //������������ѹ�Ѩ�غѹ
                            DateTime dtTmpDateTime = tmpDate.Date.Add(oDWS.WorkBeginTime);
                            //���ҷӧҹ����ش�ѹ��͹˹��
                            DateTime dtTmpDateTime2 = oDWSBackward.WorkBeginTime >= oDWSBackward.WorkEndTime ? oBackwardDate.Date.AddDays(1).Add(oDWSBackward.WorkEndTime) : oBackwardDate.Date.Add(oDWSBackward.WorkEndTime);

                            TimeSpan oTmpTimeSpan = (dtTmpDateTime - dtTmpDateTime2);

                            int iDayOff = (tmpDate.Date - oBackwardDate.Date).Days;
                            iDayOff = iDayOff < 1 ? 1 : iDayOff;
                            double dbHours = oTmpTimeSpan.TotalMinutes / 60;
                            double dbTimeDiffOff = (dbHours / (double)iDayOff);
                            #region ��������� 0.5
                            double dbTmpNumerator = dbTimeDiffOff - Math.Truncate(dbTimeDiffOff);
                            dbTimeDiffOff = dbTmpNumerator < 0.5 ? Math.Truncate(dbTimeDiffOff) : dbTmpNumerator == 0.5 ? Math.Truncate(dbTimeDiffOff) + 0.5 : Math.Truncate(dbTimeDiffOff) + 1;
                            #endregion
                            double dbTimeDiff = dbTimeDiffOff / 2;

                            //��ǧ������������ѹ�Ѩ�غѹ
                            dtTmpDateTime = dtTmpDateTime.AddHours(-dbTimeDiff);

                            if (dtTimeElement == DateTime.MinValue)//loop �á����ҨѺ������������ѧ����ժ�ǧ��������ش
                            {
                                dtTimeElement = dtTmpDateTime;
                                oRunningDate = oRunningDate.AddDays(1);
                                continue;
                            }
                            else if (oDWS.IsDayOffOrHoliday)
                            {
                                oRunningDate = oRunningDate.AddDays(1);
                                IsNextDay = true;
                                continue;
                            }
                            else
                            {
                                if ((tmpDate.Date - oBackwardDate.Date).Days == 1) //�ѹ��͹˹�����ѹ�ӧҹ
                                {
                                    oTimePair = new TimePair();
                                    oTimePair.Date = tmpDate.AddDays(-1);
                                    oTimePair.EmployeeID = EmployeeID;

                                    DailyWS otmpDWS = oEmployeeData.GetDailyWorkSchedule(oTimePair.Date, true);
                                    oTimePair.DWS = otmpDWS;
                                    oTimePair.DWSCode = otmpDWS.DailyWorkscheduleCode;

                                    //�����ٴ�ѵ÷������㹪�ǧ
                                    TimeElementMatchRange = TimeElementForShift.FindAll(delegate (TimeElement TE) { return TE.EventTime >= dtTimeElement && TE.EventTime < dtTmpDateTime && !TE.IsSystemDelete; });

                                    if (TimeElementMatchRange.Count > 0)
                                    {
                                        if (TimeElementMatchRange.Count > 0)
                                        {
                                            oTimePair.ClockIN = TimeElementMatchRange[0];
                                            //�Ӥ�� Element ���١�������� Timepair �͡�ҡ TimeElementForShift
                                            TimeElementForShift.Remove(TimeElementMatchRange[0]);
                                        }
                                        if (TimeElementMatchRange.Count > 1)
                                        {
                                            oTimePair.ClockOUT = TimeElementMatchRange[TimeElementMatchRange.Count - 1];
                                            //�Ӥ�� Element ���١�������� Timepair �͡�ҡ TimeElementForShift
                                            TimeElementForShift.Remove(TimeElementMatchRange[TimeElementMatchRange.Count - 1]);
                                        }
                                    }
                                    List<TimePair> tmpTP = MatchTimePair.FindAll(delegate (TimePair TP) { return TP.Date == oTimePair.Date; });
                                    if (tmpTP.Count <= 0)
                                        MatchTimePair.Add(oTimePair);
                                    oRunningDate = oRunningDate.AddDays(1);

                                    dtTimeElement = dtTmpDateTime;
                                }
                                else//�ѹ��͹˹�����ѹ��ش
                                {
                                    //����������ҹ�ѹ�Ѩ�غѹ - ������ԡ�ҹ�ѹ��͹˹������ش
                                    // tsTmpTimeSpan = oDWS.WorkBeginTime - new TimeSpan((int)Math.Truncate(dbTimeDiff), (int)(dbTimeDiff - Math.Truncate(dbTimeDiff)), 0);


                                    DateTime dtTmpTimeSpanEnd = dtTmpDateTime;
                                    TimeSpan TimeSpanY = new TimeSpan((int)Math.Truncate(dbTimeDiffOff), (int)((dbTimeDiffOff - Math.Truncate(dbTimeDiffOff)) * 60), 0);

                                    for (DateTime dt = tmpDate.AddDays(-1); dt >= oBackwardDate.Date; dt = dt.AddDays(-1))
                                    {
                                        DateTime dtTmpTimeSpanBegin;
                                        if (dt.Date == oBackwardDate.Date)
                                            dtTmpTimeSpanBegin = dtTimeElement;
                                        else
                                            dtTmpTimeSpanBegin = dtTmpTimeSpanEnd.Add(-TimeSpanY);

                                        oTimePair = new TimePair();
                                        oTimePair.Date = dt;
                                        oTimePair.EmployeeID = EmployeeID;

                                        //DailyWS otmpDWS = oEmployeeData.GetDailyWorkSchedule(oTimePair.Date, true);
                                        DailyWS otmpDWS = oEmployeeData.GetDailyWorkSchedule(dt, true);
                                        oTimePair.DWS = otmpDWS;
                                        oTimePair.DWSCode = otmpDWS.DailyWorkscheduleCode;
                                        oTimePair.IsDayOff = otmpDWS.IsDayOff;
                                        oTimePair.IsHoliday = otmpDWS.IsHoliday;


                                        //�����ٴ�ѵ÷������㹪�ǧ
                                        TimeElementMatchRange = TimeElementForShift.FindAll(delegate (TimeElement TE) { return TE.EventTime >= dtTmpTimeSpanBegin && TE.EventTime < dtTmpTimeSpanEnd && !TE.IsSystemDelete; });
                                        if (TimeElementMatchRange.Count > 0)
                                        {
                                            if (TimeElementMatchRange.Count > 0)
                                            {
                                                oTimePair.ClockIN = TimeElementMatchRange[0];
                                                //�Ӥ�� Element ���١�������� Timepair �͡�ҡ TimeElementForShift
                                                TimeElementForShift.Remove(TimeElementMatchRange[0]);
                                            }
                                            if (TimeElementMatchRange.Count > 1)
                                            {
                                                oTimePair.ClockOUT = TimeElementMatchRange[TimeElementMatchRange.Count - 1];
                                                //�Ӥ�� Element ���١�������� Timepair �͡�ҡ TimeElementForShift
                                                TimeElementForShift.Remove(TimeElementMatchRange[TimeElementMatchRange.Count - 1]);
                                            }
                                        }
                                        dtTmpTimeSpanEnd = dtTmpTimeSpanBegin;
                                        List<TimePair> tmpTP = MatchTimePair.FindAll(delegate (TimePair TP) { return TP.Date == oTimePair.Date; });
                                        if (tmpTP.Count <= 0)
                                            MatchTimePair.Add(oTimePair);
                                    }

                                    oRunningDate = oRunningDate.AddDays(1);
                                    dtTimeElement = dtTmpDateTime;
                                }
                            }
                        }
                        else //�ѹ��ش
                        {
                            if (dtTimeElement == DateTime.MinValue)//loop �á����ҨѺ������������ѧ����ժ�ǧ��������ش
                                dtTimeElement = oRunningDate;

                            oRunningDate = oRunningDate.AddDays(1);
                            if (oDWS != null)
                            {
                                IsNextDay = true;
                            }
                        }
                    }
                    #endregion
                    //��Ҿ�ѡ�ҹ�� Shift ������� TimeElement � TimeElementForShift
                    //else
                    //{
                    //    //if (!oDWS.IsDayOffOrHoliday)
                    //    //{
                    //    //    if (oDWS.WorkBeginTime.Hours == 8)
                    //    //    {
                    //    //        oCheckDateBegin = tmpDate.Add(new TimeSpan(3, 0, 0));
                    //    //        oCheckDateEnd = oCheckDateBegin.AddDays(1).AddSeconds(-1);
                    //    //    }
                    //    //    else
                    //    //    {
                    //    //        oCheckDateBegin = tmpDate.Add(new TimeSpan(15, 0, 0));
                    //    //        oCheckDateEnd = oCheckDateBegin.AddDays(1).AddSeconds(-1);
                    //    //    }

                    //    //    TimeElementMatchRange = TimeElementForShift.FindAll(delegate(TimeElement TE) { return TE.EventTime >= oCheckDateBegin && TE.EventTime <= oCheckDateEnd && !TE.IsSystemDelete; });
                    //    //    if (TimeElementMatchRange.Count > 0)
                    //    //    {
                    //    //        if (TimeElementMatchRange.Count > 0)
                    //    //        {
                    //    //            oTimePair.ClockIN = TimeElementMatchRange[0];
                    //    //            //�Ӥ�� Element ���١�������� Timepair �͡�ҡ TimeElementForShift
                    //    //            TimeElementForShift.Remove(TimeElementMatchRange[0]);
                    //    //        }
                    //    //        if (TimeElementMatchRange.Count > 1)
                    //    //        {
                    //    //            oTimePair.ClockOUT = TimeElementMatchRange[TimeElementMatchRange.Count - 1];
                    //    //            //�Ӥ�� Element ���١�������� Timepair �͡�ҡ TimeElementForShift
                    //    //            TimeElementForShift.Remove(TimeElementMatchRange[TimeElementMatchRange.Count - 1]);
                    //    //        }
                    //    //    }
                    //    //}
                    //    //MatchTimePair.Add(oTimePair);

                    //}
                }
                else
                {
                    oRunningDate = oRunningDate.AddDays(1);
                    if (oRunningDate.Date <= EndDate.Date.AddDays(1))
                        IsNextDay = true;
                }
            }

            //Merge between MatchingClockRealTime with Table TimePairArchiveMappingByUser
            //List<TimePair> oTimePairList = TimesheetManagement.LoadTimePairArchiveMappingByUser(EmployeeID, BeginDate, EndDate);
            foreach (TimePair obj in oTimePairList)
            {
                if (MatchTimePair.Exists(delegate (TimePair otp) { return otp.Date == obj.Date; }))
                {
                    oTimePair = MatchTimePair.Find(delegate (TimePair otp) { return otp.Date == obj.Date; });

                    if (obj.ClockIN != null)
                        oTimePair.ClockIN = obj.ClockIN;//.EventTime = obj.ClockIN.EventTime;
                    else
                        oTimePair.ClockIN = null;
                    if (obj.ClockOUT != null)
                        oTimePair.ClockOUT = obj.ClockOUT; //.EventTime = obj.ClockOUT.EventTime;
                    else
                        oTimePair.ClockOUT = null;

                }
            }
            MatchTimePair.Sort(new TimePairSortingDate());
            return MatchTimePair.FindAll(delegate (TimePair tp) { return tp.Date >= BeginDate && tp.Date <= EndDate; });
        }

        public void SaveClockinLateClockoutEarly(List<ClockinLateClockoutEarly> items, DateTime BeginDate, DateTime EndDate)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSConfig.SaveClockinLateClockoutEarly(items, BeginDate, EndDate);
        }

        public List<SaveOTData> LoadOTData(string EmployeeID, DateTime BeginDate, DateTime EndDate, string OnlyType, bool IsOnlyCompleted, bool IsOnlyNotSummary)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.LoadOTData(EmployeeID, BeginDate, EndDate, OnlyType, IsOnlyCompleted, IsOnlyNotSummary);
        }

        public void SaveOTList(List<SaveOTData> data, bool IsMark)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveOTList(data, IsMark);
        }

        public List<DailyFlexTime> GetDailyFlexTimeByFlexCode(string FlexCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetDailyFlexTimeByFlexCode(FlexCode);
        }

        public DailyFlexTime GetDailyFlexTimeMinForCalculateByFlexCode(string FlexCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetDailyFlexTimeMinForCalculateByFlexCode(FlexCode);
        }

        public DailyFlexTime GetTimesheetChangeFlexTime(string FlexCode, string ClockInTime)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetTimesheetChangeFlexTime(FlexCode, ClockInTime);
        }

        public List<DelegateData> GetDelegateByAbsence(string EmployeeId, string DelegateToId, DateTime BeginDate, DateTime EndDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetDelegateByAbsence(EmployeeId, DelegateToId, BeginDate, EndDate);
        }

        public List<OTWorkType> GetAllOTWorkType(string Language)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAllOTWorkType(Language);
        }

        public List<DutyTime> GetActivedDutyTime()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetActivedDutyTime();
        }

        public List<INFOTYPE2006> GetAbsenceQuota(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).ERPData.GetAbsenceQuota(EmployeeID, DateTime.Now, true);
        }

        public List<INFOTYPE2001> GetAbsenceList(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            return GetAbsenceList(EmployeeID, EmployeeID, BeginDate, EndDate);
        }

        public List<INFOTYPE2001> GetAbsenceList(string EmployeeID, string EmployeeID2, DateTime BeginDate, DateTime EndDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetAbsenceList(EmployeeID, EmployeeID2, BeginDate, EndDate);
        }

        public List<INFOTYPE2001> GetPostAbsenceList(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            return GetPostAbsenceList(EmployeeID, EmployeeID, BeginDate, EndDate);
        }

        public List<INFOTYPE2001> GetPostAbsenceList(string EmployeeID, string EmployeeID2, DateTime BeginDate, DateTime EndDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ERPData.GetAbsenceList(EmployeeID, EmployeeID2, BeginDate, EndDate);
        }

        public AbsAttCalculateResult CalculateAbsAttEntry(string EmployeeID, string SubType, DateTime BeginDate, DateTime EndDate, TimeSpan BeginTime, TimeSpan EndTime, bool FullDay, bool CheckQuota, bool TruncateDayOff)
        {
            return ServiceManager.CreateInstance(CompanyCode).ERPData.CalculateAbsAttEntry(EmployeeID, SubType, BeginDate, EndDate, BeginTime, EndTime, FullDay, false, TruncateDayOff);
        }

        public AbsAttCalculateResult CalculateAbsAttEntry(string EmployeeID, string SubType, DateTime BeginDate, DateTime EndDate, TimeSpan BeginTime, TimeSpan EndTime, bool FullDay, bool CheckQuota, bool TruncateDayOff, string CompCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).ERPData.CalculateAbsAttEntry(EmployeeID, SubType, BeginDate, EndDate, BeginTime, EndTime, FullDay, false, TruncateDayOff, CompCode);
        }

        public AbsAttCalculateResult CalculateAbsAttEntry(string EmployeeID, string SubType, DateTime BeginDate, DateTime EndDate, TimeSpan BeginTime, TimeSpan EndTime, bool FullDay, bool CheckQuota, bool TruncateDayOff, DailyWS oDailyWS)
        {
            return ServiceManager.CreateInstance(CompanyCode).ERPData.CalculateAbsAttEntry(EmployeeID, SubType, BeginDate, EndDate, BeginTime, EndTime, FullDay, false, TruncateDayOff, oDailyWS);
        }

        public DailyWS GetDailyWorkSchedule(EmployeeData oEmp, DateTime date)
        {

            MonthlyWS oMWS = GetCalendar(oEmp, date.Year, date.Month);
            return EmployeeManagement.CreateInstance(CompanyCode).GetDailyWorkschedule(oEmp.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, oMWS.GetWSCode(date.Day, true), date);
        }

        public AbsAttCalculateResult CalculateAttendanceEntry(EmployeeData oEmp, string SubType, DateTime BeginDate, DateTime EndDate, TimeSpan BeginTime, TimeSpan EndTime, bool AllDayFlag)
        {
            AbsAttCalculateResult oCal;
            AbsAttCalculateResult oResult = new AbsAttCalculateResult();
            Decimal AttendanceDays = 0.0M;
            Decimal AttendanceHours = 0.0M;
            Decimal PayrollDays = 0.0M;
            TimeSpan AttBeginTime;
            TimeSpan AttEndTime;


            EmployeeData emp = new EmployeeData(oEmp.EmployeeID, BeginDate) { CompanyCode = this.CompanyCode };
            DailyWS oDailyWS = GetDailyWorkSchedule(emp, BeginDate);

            //DailyWS oDailyWS = GetDailyWorkSchedule(oEmp, BeginDate);

            if (BeginDate != EndDate && !AllDayFlag)
            {

                for (DateTime rundate = BeginDate; rundate <= EndDate; rundate = rundate.AddDays(1))
                {
                    if (rundate == EndDate)
                    {
                        oCal = CalculateAbsAttEntry(oEmp.EmployeeID, SubType, rundate, rundate, BeginTime, EndTime, false, false, false, oDailyWS);
                        AttendanceDays += oCal.AbsenceDays;
                        AttendanceHours += oCal.AbsenceHours;
                        PayrollDays += oCal.PayrollDays;
                        AttBeginTime = oCal.BeginTime;
                        AttEndTime = oCal.EndTime;
                    }
                    else if (rundate == EndDate.AddDays(-1) && BeginTime >= EndTime)
                    {
                        oCal = CalculateAbsAttEntry(oEmp.EmployeeID, SubType, rundate, rundate, BeginTime, EndTime, false, false, false, oDailyWS);
                        AttendanceDays += oCal.AbsenceDays;
                        AttendanceHours += oCal.AbsenceHours;
                        PayrollDays += oCal.PayrollDays;
                        AttBeginTime = oCal.BeginTime;
                        AttEndTime = oCal.EndTime;
                        break;
                    }
                    else
                    {
                        oCal = CalculateAbsAttEntry(oEmp.EmployeeID, SubType, rundate, rundate.AddDays(1), BeginTime, BeginTime, true, false, false, oDailyWS);
                        AttendanceDays += 1;
                        AttendanceHours += 24;
                        PayrollDays += 1;
                        AttBeginTime = oCal.BeginTime;
                        AttEndTime = oCal.EndTime;
                    }
                }
            }
            else
            {
                oCal = CalculateAbsAttEntry(oEmp.EmployeeID, SubType, BeginDate, EndDate, BeginTime, EndTime, AllDayFlag, false, false, oDailyWS);
                AttendanceDays = oCal.AbsenceDays;
                AttendanceHours = oCal.AbsenceHours;
                PayrollDays = oCal.PayrollDays;
                AttBeginTime = oCal.BeginTime;
                AttEndTime = oCal.EndTime;
            }

            oResult.AbsenceDays = AttendanceDays;
            oResult.AbsenceHours = AttendanceHours;
            oResult.PayrollDays = PayrollDays;
            oResult.BeginDate = BeginDate;
            oResult.EndDate = EndDate;
            oResult.BeginTime = BeginTime;
            oResult.EndTime = EndTime;
            oResult.FullDay = AllDayFlag;

            return oResult;
        }

        public void PostDutyPaymentDayOff(string strEmployeeID, Dictionary<DateTime, decimal> dictData)
        {
            ServiceManager.CreateInstance(CompanyCode).ERPData.PostDutyPaymentDayOff(strEmployeeID, dictData);
        }

        public void PostDutyPaymentDayWork(string strEmployeeID, Dictionary<DateTime, decimal> dictData)
        {
            ServiceManager.CreateInstance(CompanyCode).ERPData.PostDutyPaymentDayWork(strEmployeeID, dictData);
        }

        public void SaveDutyPaymentLog(DataTable data, bool IsDeleteOnly)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveDutyPaymentLog(data, IsDeleteOnly);
        }

        public List<DutyPaymentLog> GetDutyPaymentLog(String strEmployeeID, DateTime dtBeginDate, DateTime dtEndDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetDutyPaymentLog(strEmployeeID, dtBeginDate, dtEndDate);
        }

        public List<DutyTime> GetDutyTime()
        {
            //ddlDutyTime.DataSource = ListDutyTime.FindAll(delegate (DutyTime dtm) { return dtm.DayWork.Equals(isWorkDay); });
            //ddlDutyTime.DataTextField = "DutyTimeText";
            //ddlDutyTime.DataValueField = "DutyTimeText";
            //ddlDutyTime.DataBind();

            List<DutyTime> oListDutyTime = ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetActivedDutyTime();
            return oListDutyTime;
        }

        public object GetListDuty(EmployeeData oEmp, string Period)
        {
            Dictionary<DateTime, DailyWS> DictDailyWS = new Dictionary<DateTime, DailyWS>();
            DataTable DutyPaymentTable = new DataTable();

            List<DutyPaymentLog> duty_payment = new List<DutyPaymentLog>();

            DutyPaymentLog duty = new DutyPaymentLog();
            duty.Status = "HIDDEN";
            DutyPaymentTable = duty.ToADODataTable();

            DateTime dtBeginDate = DateTime.ParseExact(Period, "yyyyMM", oCL);
            DateTime dtEndDate = dtBeginDate.AddMonths(1).AddDays(-1);
            DutyPaymentLog dutyLog;
            DataTable tbDutyTemp = new DataTable();
            tbDutyTemp = DutyPaymentTable.Clone();

            Dictionary<DateTime, DailyWS> dictDWS = new Dictionary<DateTime, DailyWS>();

            EmployeeData emp = new EmployeeData(oEmp.EmployeeID, dtEndDate);
            MonthlyWS oMWS = GetCalendar(emp, dtBeginDate, dtBeginDate.Year, dtBeginDate.Month, true);

            //MonthlyWS oMWS = GetCalendar(oEmp, dtBeginDate, dtBeginDate.Year, dtBeginDate.Month, true);

            List<DutyTime> oListDutyTime = GetDutyTime();

            // �ѡ���͹����� ���ѹ���  28/02/2020 ��ѡ�ҹ�ѧ�����ҷӧҹ��ѹ���͡ Period ����Ҿѹ�� �ѧ��鹢�������¡�� �.� �е�ͧ����ʴ� (�ѡ Error ����) 
            if (emp.OrgAssignment != null)
            {
                for (DateTime dtRunning = dtBeginDate; dtRunning <= dtEndDate; dtRunning = dtRunning.AddDays(1))
                {

                    if (!(emp.OrgAssignment.BeginDate <= dtRunning && emp.OrgAssignment.EndDate >= dtRunning))
                    {
                        oEmp = new EmployeeData(oEmp.EmployeeID, dtRunning);
                        oMWS = GetCalendar(emp, dtRunning, dtRunning.Year, dtRunning.Month, true);
                    }

                    // �ѡ���͹������ҡ��ѡ�ҹ������ҷӧҹ��ҧ��͹ �� ��ҷӧҹ 15/03/2020  �����ԡ��������èе�ͧ�����¡�õ�����ѹ��� 15 ��ҹ��
                    if (oMWS.GetWSCode(dtRunning.Day, true) != null)
                    {
                        DailyWS oDWS = DailyWS.GetDailyWorkschedule(emp.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, oMWS.GetWSCode(dtRunning.Day, true), dtRunning);


                        oDWS.IsHoliday = oMWS.GetWSCode(dtRunning.Day, false) != "";
                        //dictDWS.Add(dtRunning, oDWS);
                        //oDWS = null;
                        //DataRow[] rows = DutyPaymentTable.Select(String.Format("DutyDate='{0}'", dtRunning));
                        //if (rows.Length > 0)
                        //{
                        //    dutyLog.ParseToObject(rows[0]);
                        //}
                        //else
                        //{
                        //    dutyLog.DutyDate = dtRunning;
                        //    dutyLog.ItemNo = Guid.NewGuid().ToString();
                        //    dutyLog.EmployeeID = oEmp.EmployeeID;

                        //    dutyLog.DutyDate = dtRunning;

                        //    dutyLog.Status = "HIDDEN";
                        //}
                        //dutyLog.LoadDataToTable(tbDutyTemp);

                        dutyLog = new DutyPaymentLog();

                        dutyLog.DutyDate = dtRunning;
                        dutyLog.ItemNo = Guid.NewGuid().ToString();
                        dutyLog.EmployeeID = oEmp.EmployeeID;
                        dutyLog.BeginDate = dtRunning;
                        dutyLog.EndDate = dtRunning.AddDays(1);
                        dutyLog.Status = "HIDDEN";

                        duty_payment.Add(dutyLog);
                        dictDWS.Add(dtRunning, oDWS);
                    }
                }


            }

            //foreach (DataRow dr in DutyPaymentTable.Rows)
            //{
            //    dr["BeginDate"] = DBNull.Value;
            //    dr["EndDate"] = DBNull.Value;
            //}
            DictDailyWS = dictDWS;
            dictDWS = null;
            DutyPaymentTable = tbDutyTemp.Copy();

            object oResult = new
            {
                objDutyPaymentTable = duty_payment,
                objDictDailyWS = DictDailyWS,
                objListDutyTime = oListDutyTime  //�� property !IsDayOffOrHoliday �ͧ DictDailyWs Mapping ����ѹ�Ѻ oListDutyTime.DayWork ������� DutyTimeText ���
            };
            return oResult;
        }


        public List<LeavePlan> GetLeavePlanList(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetLeavePlanList(EmployeeID, BeginDate, EndDate);
        }

        public void SaveLeavePlanList(string EmployeeID, int Year, List<LeavePlan> leavePlanList)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveLeavePlanList(EmployeeID, Year, leavePlanList);
        }

        public void SaveAbsence(List<INFOTYPE2001> data)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveAbsence(data);
        }

        public void SaveLG_TIMEREQUEST(List<INFOTYPE2001> data, string type)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveLG_TIMEREQUEST(data, type);
        }

        public void SaveLG_TIMEREQUEST(List<INFOTYPE2002> data, string type)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveLG_TIMEREQUEST(data, type);
        }

        public void SavePostAbsence(List<INFOTYPE2001> postingList)
        {
            ServiceManager.CreateInstance(CompanyCode).ERPData.SaveAbsence(postingList);
        }

        public void SavePostAbsence(List<INFOTYPE2001> postingList, string CompCode)
        {
            ServiceManager.CreateInstance(CompanyCode).ERPData.SaveAbsence(postingList, CompCode);
        }

        public void SaveSubstitutionLog(DataRow dRow)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveSubstitutionLog(dRow);
        }

        public void SaveSubstitutionLog(List<INFOTYPE2003> data)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveSubstitutionLog(data);
        }

        public void SaveSubstitution(List<INFOTYPE2003> data)
        {
            ServiceManager.CreateInstance(CompanyCode).ERPData.SaveSubstitution(data);
        }

        public void CancelSubstitutionLog(DataRow dRow)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.CancelSubstitutionLog(dRow);
        }

        public void DeleteOTLogByRefRequestNo(DataTable data)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.DeleteOTLogByRefRequestNo(data);
        }

        public void DeleteDelegateByAbsence(INFOTYPE2001 oAbsence)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.DeleteDelegateByAbsence(oAbsence);
        }

        public void PostOTLog(List<DailyOTLog> oDailyOTLog)
        {
            ServiceManager.CreateInstance(CompanyCode).ERPData.PostOTLog(oDailyOTLog);
        }

        public void SaveOTLog(DataTable data, bool IsMark)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveOTLog(data, IsMark);
        }

        public void SaveDailyOTLogService(DataTable data)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveDailyOTLogService(data);
        }

        public void SaveSubstitutionCustomTime(INFOTYPE2003 data)
        {
            ServiceManager.CreateInstance(CompanyCode).ERPData.SaveSubstitutionCustomTime(data);
        }

        public void SaveAttendance(List<INFOTYPE2002> data)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveAttendance(data);
        }

        public void SaveDelegateByAbsence(DataTable data)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveDelegateByAbsence(data);
        }
        public void SavePostAttendance(List<INFOTYPE2002> postingList)
        {
            ServiceManager.CreateInstance(CompanyCode).ERPData.SaveAttendance(postingList);
        }
        public void SavePostAttendance(List<INFOTYPE2002> postingList, string CompCode)
        {
            ServiceManager.CreateInstance(CompanyCode).ERPData.SaveAttendance(postingList, CompCode);
        }

        public bool Is_TNOM(string ValuationCode)
        {
            return ValuationCode.Trim() == "2";
        }

        public bool Is_NORM(string ValuationCode)
        {
            return ValuationCode.Trim() == "1";
        }

        public TimeSpan GetCutoffTime(string ValuationClass)
        {
            if (Is_TNOM(ValuationClass) || Is_NORM(ValuationClass))
            {
                return new TimeSpan(0, 0, 0);
            }
            else
            {
                return new TimeSpan(7, 30, 0);
            }
        }

        public TimeSpan GetPrevCutoffTime(string ValuationClass)
        {
            if (Is_TNOM(ValuationClass) || Is_NORM(ValuationClass))
            {
                return new TimeSpan(8, 0, 0);
            }
            else
            {
                return new TimeSpan(7, 30, 0);
            }
        }

        public void SplitPrevDay(string EmployeeID, DateTime inputDate, ref TimeSpan BeginTime, ref TimeSpan EndTime, ref bool IsPrevDay)
        {
            MonthlyWS MWS = MonthlyWS.GetCalendar(EmployeeID, inputDate.Year, inputDate.Month);
            SplitPrevDay(EmployeeID, inputDate, MWS, ref BeginTime, ref EndTime, ref IsPrevDay);
        }

        public void SplitPrevDay(string EmployeeID, DateTime inputDate, MonthlyWS MWS, ref TimeSpan BeginTime, ref TimeSpan EndTime, ref bool IsPrevDay)
        {
            MonthlyWS mws1;
            if (inputDate.Year == MWS.WS_Year && inputDate.Month == MWS.WS_Month)
            {
                mws1 = MWS;
            }
            else
            {
                mws1 = MonthlyWS.GetCalendar(EmployeeID, inputDate.Year, inputDate.Month);
            }
            string valuationClass1 = mws1.GetValuationClassExactly(inputDate.Day);
            TimeSpan PrevCutoffTime = GetPrevCutoffTime(valuationClass1);
            IsPrevDay = false;
            if (EndTime > BeginTime && BeginTime < PrevCutoffTime)
            {
                if (EndTime > PrevCutoffTime)
                {
                    EndTime = PrevCutoffTime;
                }
                IsPrevDay = true;
            }
        }

        #region " SplitCutoffTime "
        public void SplitCutoffTime(string EmployeeID, DateTime inputDate, ref TimeSpan BeginTime, ref TimeSpan EndTime)
        {
            MonthlyWS MWS = MonthlyWS.GetCalendar(EmployeeID, inputDate.Year, inputDate.Month);
            SplitCutoffTime(EmployeeID, inputDate, MWS, ref BeginTime, ref EndTime);
        }
        public void SplitCutoffTime(string EmployeeID, DateTime inputDate, MonthlyWS MWS, ref TimeSpan BeginTime, ref TimeSpan EndTime)
        {
            DateTime nextDate = inputDate.AddDays(1);
            MonthlyWS mws1 = null;
            MonthlyWS mws2 = null;
            string valuationClass1, valuationClass2;
            if (inputDate.Year == MWS.WS_Year && inputDate.Month == MWS.WS_Month)
            {
                mws1 = MWS;
            }
            else
            {
                mws1 = MonthlyWS.GetCalendar(EmployeeID, inputDate.Year, inputDate.Month);
            }
            valuationClass1 = mws1.GetValuationClassExactly(inputDate.Day);
            if (!Is_TNOM(valuationClass1))
            {
                if (nextDate.Year == MWS.WS_Year && nextDate.Month == MWS.WS_Month)
                {
                    mws2 = MWS;
                }
                else if (nextDate.Year == mws1.WS_Year && nextDate.Month == mws1.WS_Month)
                {
                    mws2 = mws1;
                }
                else
                {
                    mws2 = MonthlyWS.GetCalendar(EmployeeID, nextDate.Year, nextDate.Month);
                }
                valuationClass2 = mws2.GetValuationClassExactly(nextDate.Day);
                if (valuationClass1 != valuationClass2)
                {
                    TimeSpan CutoffTime = GetCutoffTime(valuationClass1);
                    TimeSpan refCutoffTime;

                    if (BeginTime >= CutoffTime)
                    {
                        refCutoffTime = CutoffTime.Add(new TimeSpan(1, 0, 0, 0));
                    }
                    else
                    {
                        refCutoffTime = CutoffTime;
                    }
                    if (refCutoffTime > BeginTime && refCutoffTime < EndTime)
                    {
                        EndTime = refCutoffTime;
                    }
                }
            }
        }
        #endregion

        #region " SplitWorkingTime "
        public void SplitWorkingTime(int DayNo, DailyWS DWS, MonthlyWS MWS, ref TimeSpan BeginTime, ref TimeSpan EndTime)
        {
            string valuationClass = MWS.GetValuationClassExactly(DayNo);
            if (!Is_TNOM(valuationClass))
            {
                if (!DWS.IsDayOff && MWS.GetWSCode(DayNo, false) == "")
                {
                    //cut plan time
                    TimeSpan timeend;
                    if (DWS.WorkBeginTime > DWS.WorkEndTime)
                    {
                        timeend = DWS.WorkEndTime.Add(new TimeSpan(1, 0, 0, 0));
                    }
                    else
                    {
                        timeend = DWS.WorkEndTime;
                    }
                    if (BeginTime >= DWS.WorkBeginTime && BeginTime < timeend)
                    {
                        BeginTime = timeend;
                    }
                    else if (BeginTime < DWS.WorkBeginTime && EndTime > DWS.WorkBeginTime)
                    {
                        EndTime = DWS.WorkBeginTime;
                    }
                }
            }
        }
        #endregion

        public string FindOTPeriod(DateTime actionDate)
        {
            DateTime myDate;
            myDate = new DateTime(actionDate.Year, actionDate.Month, 1);
            if (actionDate.Day >= 15)
            {
                myDate = myDate.AddMonths(1);
            }
            return myDate.ToString("MM/yyyy", new CultureInfo("en-US"));
        }

        public List<DailyOT> LoadOTFromPM(string EmployeeID, string Period, int DayStart, int DayEnd)
        {
            return LoadOTFromPM(EmployeeID, Period, DayStart, DayEnd);
        }
        public List<MonthlyOT> GenerateMonthlyOT(string EmployeeID, string Period, MonthlyWS MWS)
        {
            AdjustDailyOT(EmployeeID, Period);
            CultureInfo oCL = new CultureInfo("en-US");
            List<MonthlyOT> oReturn = new List<MonthlyOT>();
            List<SaveOTData> otlist;
            DateTime oPeriod = DateTime.ParseExact(Period, "yyyyMM", oCL);
            //otlist = HR.TM.ServiceManager.HRTMBuffer.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", true, false);

            otlist = ServiceManager.CreateInstance(CompanyCode).ESSData.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", true, false);
            List<SaveOTData> buffer = new List<SaveOTData>();
            List<string> requestNoList = new List<string>();
            foreach (SaveOTData item in otlist)
            {
                if (item.OTPeriod != Period)
                {
                    continue;
                }
                if (!requestNoList.Contains(item.RequestNo))
                {
                    requestNoList.Add(item.RequestNo);
                }
                List<DailyOT> temp = new List<DailyOT>();
                DailyOT dailyOT = new DailyOT();
                dailyOT.BeginTime = item.BeginTime;
                dailyOT.EmployeeID = EmployeeID;
                dailyOT.EndTime = item.EndTime;
                dailyOT.OTDate = item.OTDate;
                dailyOT.IsPosted = false;
                dailyOT.Description = item.Description;
                dailyOT.OTItemTypeID = item.OTItemTypeID;
                dailyOT.OTHours = item.OTHours;
                dailyOT.IONumber = item.IONumber;
                dailyOT.ActivityCode = item.ActivityCode;
                temp.Add(dailyOT);
                foreach (DailyOT item1 in temp)
                {
                    //if (item1.OTHours * 100 == 0.0M)
                    //{
                    //    continue;
                    //}
                    //SaveOTData save = new SaveOTData();
                    //save.AssignFrom = item.AssignFrom;
                    //save.BeginTime = item1.BeginTime;
                    //save.Description = item.Description;
                    //save.EmployeeID = item.EmployeeID;
                    //save.EndTime = item1.EndTime;
                    //save.IsCompleted = item.IsCompleted;
                    //save.IsPost = item.IsPost;
                    //save.IsPrevDay = item.IsPrevDay;
                    //save.IsSummary = item.IsSummary;
                    //save.OTDate = item.OTDate;
                    //save.OTHours = item1.OTHours;
                    //save.OTItemTypeID = item.OTItemTypeID;
                    //save.OTPeriod = item.OTPeriod;
                    //save.OTType = item.OTType;
                    //save.Remark = item.Remark;
                    //save.RequestNo = item.RequestNo;
                    //buffer.Add(save);
                    MonthlyOT monthlyOT = new MonthlyOT();
                    monthlyOT.BeginTime = item1.BeginTime;
                    monthlyOT.DailyOT = item1;
                    monthlyOT.Description = "";
                    monthlyOT.EndTime = item1.EndTime;
                    monthlyOT.IsDeleted = false;
                    monthlyOT.DailyAssignFrom = item.AssignFrom;
                    monthlyOT.DailyRequestNo = item.RequestNo;
                    oReturn.Add(monthlyOT);
                }
            }

            oReturn = CalculateMonthlyOT(oReturn, MWS);

            //HR.TM.ServiceManager.HRTMBuffer.SaveOTList(buffer, true, requestNoList, true);

            //foreach (SaveOTData item in buffer)
            //{
            //    DailyOT dailyOT = new DailyOT();
            //    dailyOT.BeginTime = item.BeginTime;
            //    dailyOT.EmployeeID = EmployeeID;
            //    dailyOT.EndTime = item.EndTime;
            //    dailyOT.OTDate = item.OTDate;
            //    dailyOT.IsPosted = false;
            //    dailyOT.Description = item.Description;
            //    dailyOT.OTItemTypeID = item.OTItemTypeID;
            //    dailyOT.Calculate(MWS);
            //    MonthlyOT monthlyOT = new MonthlyOT();
            //    monthlyOT.BeginTime = dailyOT.BeginTime;
            //    monthlyOT.DailyOT = dailyOT;
            //    monthlyOT.Description = "";
            //    monthlyOT.EndTime = dailyOT.EndTime;
            //    monthlyOT.IsDeleted = false;
            //    monthlyOT.DailyAssignFrom = item.AssignFrom;
            //    monthlyOT.DailyRequestNo = item.RequestNo;
            //    oReturn.Add(monthlyOT);
            //}
            return oReturn;
        }

        public void AdjustDailyOT(string EmployeeID, string Period)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            List<DailyOT> oReturn = new List<DailyOT>();
            List<SaveOTData> otlist;
            DateTime oPeriod = DateTime.ParseExact(Period, "yyyyMM", oCL);
            MonthlyWS MWS = MonthlyWS.GetCalendar(EmployeeID, oPeriod.Year, oPeriod.Month);
            //otlist = HR.TM.ServiceManager.HRTMBuffer.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", false, false);

            otlist = ServiceManager.CreateInstance(CompanyCode).ESSData.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", false, false);
            foreach (SaveOTData item in otlist)
            {
                if (item.OTDate.ToString("yyyyMM") != Period)
                {
                    continue;
                }
                DailyOT dailyOT = new DailyOT();
                dailyOT.AssignFrom = item.AssignFrom;
                dailyOT.BeginTime = item.BeginTime;
                dailyOT.Description = item.Description;
                dailyOT.EmployeeID = EmployeeID;
                dailyOT.EndTime = item.EndTime;
                dailyOT.OTDate = item.OTDate;
                dailyOT.IsCompleted = item.IsCompleted;
                dailyOT.IsSummary = item.IsSummary;
                dailyOT.IsPosted = item.IsPost;
                dailyOT.OTItemTypeID = item.OTItemTypeID;
                dailyOT.OTHours = item.OTHours;
                dailyOT.RequestNo = item.RequestNo;
                dailyOT.IONumber = item.IONumber;
                dailyOT.ActivityCode = item.ActivityCode;
                oReturn.Add(dailyOT);
            }
            oReturn = CalculateDailyOT(oReturn, MWS);
            otlist = new List<SaveOTData>();
            foreach (DailyOT item in oReturn)
            {
                SaveOTData saveOT = new SaveOTData();
                saveOT.AssignFrom = item.AssignFrom;
                saveOT.BeginTime = item.BeginTime;
                saveOT.Description = item.Description;
                saveOT.EmployeeID = EmployeeID;
                saveOT.EndTime = item.EndTime;
                saveOT.OTDate = item.OTDate;
                saveOT.IsCompleted = item.IsCompleted;
                saveOT.IsSummary = item.IsSummary;
                saveOT.IsPost = item.IsPosted;
                saveOT.OTItemTypeID = item.OTItemTypeID;
                saveOT.OTHours = item.OTHours;
                saveOT.RequestNo = item.RequestNo;
                saveOT.OTType = "D";
                saveOT.OTPeriod = Period;
                saveOT.IONumber = item.IONumber;
                saveOT.ActivityCode = item.ActivityCode;
                otlist.Add(saveOT);
            }
            //HR.TM.ServiceManager.HRTMBuffer.SaveOTList(otlist, true, null, true);
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveOTList(otlist, true, null, true);
        }

        public List<DailyOT> GenerateApprovedMonthlyOT(string EmployeeID, string Period)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            List<DailyOT> oReturn = new List<DailyOT>();
            List<SaveOTData> otlist;
            DateTime oPeriod = DateTime.ParseExact(Period, "yyyyMM", oCL);
            MonthlyWS MWS = MonthlyWS.GetCalendar(EmployeeID, oPeriod.Year, oPeriod.Month);
            //otlist = HR.TM.ServiceManager.HRTMService.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1).AddDays(-1), "D", true, true);

            otlist = ServiceManager.CreateInstance(CompanyCode).ERPData.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1).AddDays(-1), "D", true, true);
            foreach (SaveOTData item in otlist)
            {
                if (item.OTDate.ToString("yyyyMM") != Period)
                {
                    continue;
                }
                DailyOT dailyOT = new DailyOT();
                dailyOT.BeginTime = item.BeginTime;
                dailyOT.EmployeeID = EmployeeID;
                dailyOT.EndTime = item.EndTime;
                dailyOT.OTDate = item.OTDate;
                dailyOT.IsPosted = false;
                dailyOT.Description = item.Description;
                dailyOT.OTItemTypeID = item.OTItemTypeID;
                dailyOT.OTHours = item.OTHours;
                dailyOT.IONumber = item.IONumber;
                dailyOT.ActivityCode = item.ActivityCode;
                oReturn.Add(dailyOT);
            }
            oReturn = CalculateDailyOT(oReturn, MWS);
            return oReturn;
        }

        public List<DailyOT> GenerateApprovedMonthlyOT1(string EmployeeID, string Period)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            List<DailyOT> oReturn = new List<DailyOT>();
            List<SaveOTData> otlist;
            DateTime oPeriod = DateTime.ParseExact(Period, "yyyyMM", oCL);
            MonthlyWS MWS = MonthlyWS.GetCalendar(EmployeeID, oPeriod.Year, oPeriod.Month);
            //otlist = HR.TM.ServiceManager.HRTMService.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1).AddDays(-1), "D", true, true);

            otlist = ServiceManager.CreateInstance(CompanyCode).ERPData.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1).AddDays(-1), "D", true, true);
            foreach (SaveOTData item in otlist)
            {
                if (item.OTDate.ToString("yyyyMM") != Period)
                {
                    continue;
                }
                DailyOT dailyOT = new DailyOT();
                dailyOT.BeginTime = item.BeginTime;
                dailyOT.EmployeeID = EmployeeID;
                dailyOT.EndTime = item.EndTime;
                dailyOT.OTDate = item.OTDate;
                dailyOT.IsPosted = false;
                dailyOT.Description = item.Description;
                dailyOT.OTItemTypeID = item.OTItemTypeID;
                dailyOT.OTHours = item.OTHours;
                dailyOT.IONumber = item.IONumber;
                dailyOT.ActivityCode = item.ActivityCode;
                dailyOT.FillWorkSchedule();
                oReturn.Add(dailyOT);
            }
            //oReturn = OTManagement.CalculateDailyOT(oReturn, MWS);
            return oReturn;
        }

        public List<MonthlyOT> GenerateMonthlyOT1(string EmployeeID, string Period, MonthlyWS MWS)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            List<MonthlyOT> oReturn = new List<MonthlyOT>();
            List<SaveOTData> otlist;
            DateTime oPeriod = DateTime.ParseExact(Period, "yyyyMM", oCL);
            //otlist = HR.TM.ServiceManager.HRTMBuffer.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", true, false);

            otlist = ServiceManager.CreateInstance(CompanyCode).ESSData.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", true, false);
            List<SaveOTData> buffer = new List<SaveOTData>();
            List<string> requestNoList = new List<string>();
            foreach (SaveOTData item in otlist)
            {
                if (item.OTPeriod != Period)
                {
                    continue;
                }
                if (!requestNoList.Contains(item.RequestNo))
                {
                    requestNoList.Add(item.RequestNo);
                }
                List<DailyOT> temp = new List<DailyOT>();
                DailyOT dailyOT = new DailyOT();
                dailyOT.BeginTime = item.BeginTime;
                dailyOT.EmployeeID = EmployeeID;
                dailyOT.EndTime = item.EndTime;
                dailyOT.OTDate = item.OTDate;
                dailyOT.IsPosted = false;
                dailyOT.Description = item.Description;
                dailyOT.OTItemTypeID = item.OTItemTypeID;
                dailyOT.OTHours = item.OTHours;
                dailyOT.IONumber = item.IONumber;
                dailyOT.ActivityCode = item.ActivityCode;
                dailyOT.FillWorkSchedule();

                MonthlyOT monthlyOT = new MonthlyOT();
                monthlyOT.BeginTime = item.BeginTime;
                monthlyOT.DailyOT = dailyOT;
                monthlyOT.Description = "";
                monthlyOT.EndTime = item.EndTime;
                monthlyOT.IsDeleted = false;
                monthlyOT.DailyAssignFrom = item.AssignFrom;
                monthlyOT.DailyRequestNo = item.RequestNo;
                oReturn.Add(monthlyOT);
            }
            return oReturn;
        }

        public List<DailyOT> GenerateDailyOT1(string EmployeeID, string Period)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            List<DailyOT> oReturn = new List<DailyOT>();
            List<SaveOTData> otlist, otlist1;
            DateTime oPeriod = DateTime.ParseExact(Period, "yyyyMM", oCL);
            MonthlyWS MWS = MonthlyWS.GetCalendar(EmployeeID, oPeriod.Year, oPeriod.Month);
            otlist = new List<SaveOTData>();
            //otlist1 = HR.TM.ServiceManager.HRTMBuffer.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", true, false);

            otlist1 = ServiceManager.CreateInstance(CompanyCode).ESSData.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", true, false);
            otlist.AddRange(otlist1);
            //otlist1 = HR.TM.ServiceManager.HRTMService.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", true, false);

            otlist1 = ServiceManager.CreateInstance(CompanyCode).ERPData.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", true, false);
            otlist.AddRange(otlist1);
            foreach (SaveOTData item in otlist)
            {
                if (item.OTDate.ToString("yyyyMM") != Period)
                {
                    continue;
                }
                DailyOT dailyOT = new DailyOT();
                dailyOT.BeginTime = item.BeginTime;
                dailyOT.EmployeeID = EmployeeID;
                dailyOT.EndTime = item.EndTime;
                dailyOT.OTDate = item.OTDate;
                dailyOT.IsPosted = false;
                dailyOT.Description = item.Description;
                dailyOT.OTItemTypeID = item.OTItemTypeID;
                dailyOT.OTHours = item.OTHours;
                dailyOT.IONumber = item.IONumber;
                dailyOT.ActivityCode = item.ActivityCode;
                oReturn.Add(dailyOT);
            }
            return oReturn;
        }

        public List<DailyOT> GenerateDailyOT(string EmployeeID, string Period)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            List<DailyOT> oReturn = new List<DailyOT>();
            List<SaveOTData> otlist, otlist1;
            DateTime oPeriod = DateTime.ParseExact(Period, "yyyyMM", oCL);
            MonthlyWS MWS = MonthlyWS.GetCalendar(EmployeeID, oPeriod.Year, oPeriod.Month);
            otlist = new List<SaveOTData>();
            //otlist1 = HR.TM.ServiceManager.HRTMBuffer.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", true, false);

            otlist1 = ServiceManager.CreateInstance(CompanyCode).ESSData.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", true, false);
            otlist.AddRange(otlist1);
            //otlist1 = HR.TM.ServiceManager.HRTMService.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", true, false);

            otlist1 = ServiceManager.CreateInstance(CompanyCode).ERPData.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", true, false);
            otlist.AddRange(otlist1);
            foreach (SaveOTData item in otlist)
            {
                if (item.OTDate.ToString("yyyyMM") != Period)
                {
                    continue;
                }
                DailyOT dailyOT = new DailyOT();
                dailyOT.BeginTime = item.BeginTime;
                dailyOT.EmployeeID = EmployeeID;
                dailyOT.EndTime = item.EndTime;
                dailyOT.OTDate = item.OTDate;
                dailyOT.IsPosted = false;
                dailyOT.Description = item.Description;
                dailyOT.OTItemTypeID = item.OTItemTypeID;
                dailyOT.OTHours = item.OTHours;
                dailyOT.IONumber = item.IONumber;
                dailyOT.ActivityCode = item.ActivityCode;
                oReturn.Add(dailyOT);
            }
            oReturn = CalculateDailyOT(oReturn, MWS);
            return oReturn;
        }

        public bool IsNORM(string ValuationCode)
        {
            return Is_NORM(ValuationCode);
        }

        public bool IsCanOTAssignFrom(EmployeeData assignData)
        {
            if ("DEFGH".IndexOf(assignData.EmpGroup) > -1 && assignData.IsHaveSubOrdinate || true)
            {
                return true;
            }
            return false;
        }

        public Decimal CalculatePlannedOTHours(string EmployeeID, DateTime inputDate, MonthlyWS MWS, DailyWS DWS, ref TimeSpan BeginTime, ref TimeSpan EndTime)
        {
            if (BeginTime == TimeSpan.MinValue || EndTime == TimeSpan.MinValue)
            {
                return 0.0M;
            }
            string valuationClass = MWS.GetValuationClassExactly(inputDate.Day);
            if (EndTime <= BeginTime)
            {
                EndTime = EndTime.Add(new TimeSpan(1, 0, 0, 0));
            }
            DateTime buffer = new DateTime(MWS.WS_Year, MWS.WS_Month, inputDate.Day);
            //PlanTimeManagement.SplitCutoffTime(EmployeeID, inputDate, MWS, ref BeginTime, ref EndTime);
            //PlanTimeManagement.SplitWorkingTime(inputDate.Day, DWS, MWS, ref BeginTime, ref EndTime);
            SplitCutoffTime(EmployeeID, inputDate, MWS, ref BeginTime, ref EndTime);
            SplitWorkingTime(inputDate.Day, DWS, MWS, ref BeginTime, ref EndTime);
            if (EndTime < BeginTime)
            {
                BeginTime = TimeSpan.MinValue;
                EndTime = TimeSpan.MinValue;
                return 0.0M;
            }
            return (decimal)(EndTime.Subtract(BeginTime).TotalMinutes / 60) - (Is_TNOM(valuationClass) ? 9 : 0);
        }

        public List<DailyOT> CalculateDailyOT(List<DailyOT> listData, MonthlyWS MWS)
        {
            List<DailyOT> list = new List<DailyOT>();
            Dictionary<int, Decimal> additionalTNOM = new Dictionary<int, decimal>();
            Dictionary<int, Decimal> otTNOM = new Dictionary<int, decimal>();
            List<int> isLoadTNOM = new List<int>();
            for (int index = 0; index < listData.Count; index++)
            {
                DailyOT item = listData[index];
                TimeSpan time1, time2;
                time1 = item.BeginTime;
                time2 = item.EndTime;
                if (time2 <= time1)
                {
                    time2 = time2.Add(new TimeSpan(1, 0, 0, 0));
                }
                bool lEnd = false;
                do
                {
                    item.Calculate(MWS);
                    if (item.IsTNOM)
                    {
                        #region " ONLY TNOM "
                        if (!additionalTNOM.ContainsKey(item.OTDate.Day))
                        {
                            additionalTNOM.Add(item.OTDate.Day, 0);
                        }
                        if (!otTNOM.ContainsKey(item.OTDate.Day))
                        {
                            otTNOM.Add(item.OTDate.Day, 0);
                        }
                        if (!isLoadTNOM.Contains(item.OTDate.Day))
                        {
                            //List<DailyOT> otList = OTManagement.LoadDailyOT(item.EmployeeID, MWS, item.OTDate);

                            List<DailyOT> otList = LoadDailyOT(item.EmployeeID, MWS, item.OTDate);
                            additionalTNOM[item.OTDate.Day] += Summary(otList, item);
                            isLoadTNOM.Add(item.OTDate.Day);
                        }
                        additionalTNOM[item.OTDate.Day] += item.OTHours;
                        if (additionalTNOM[item.OTDate.Day] > 9)
                        {
                            if (item.DWS.IsDayOff || MWS.GetWSCode(item.OTDate.Day, false) != "")
                            {
                                item.OTHours = additionalTNOM[item.OTDate.Day] - otTNOM[item.OTDate.Day];
                            }
                            else
                            {
                                item.OTHours = additionalTNOM[item.OTDate.Day] - 9 - otTNOM[item.OTDate.Day];
                            }
                        }
                        else
                        {
                            item.OTHours = 0.0M;
                        }
                        otTNOM[item.OTDate.Day] += item.OTHours;
                        if (item.TimeEvidance != null)
                        {
                            list.Add(item);
                        }
                        #endregion
                    }
                    else
                    {
                        //if (item.OTHours > 0)
                        //{
                        //    list.Add(item);
                        //}

                        if (item.BeginTime.Days > 0)
                        {
                            item.OTDate = item.OTDate.AddDays(item.BeginTime.Days);
                            item.BeginTime = new TimeSpan(item.BeginTime.Hours, item.BeginTime.Minutes, item.BeginTime.Seconds);
                            item.EndTime = new TimeSpan(item.EndTime.Hours, item.EndTime.Minutes, item.EndTime.Seconds);
                            time2 = new TimeSpan(time2.Hours, time2.Minutes, time2.Seconds);
                            continue;
                        }
                        list.Add(item);
                    }
                    if (item.EndTime >= time2)
                    {
                        break;
                    }
                    else
                    {
                        DailyOT buffer = new DailyOT();
                        buffer.Description = item.Description;
                        buffer.EmployeeID = item.EmployeeID;
                        buffer.OTItemTypeID = item.OTItemTypeID;
                        buffer.IsSummary = item.IsSummary;
                        buffer.IsCompleted = item.IsCompleted;
                        buffer.RequestNo = item.RequestNo;
                        buffer.IONumber = item.IONumber;
                        buffer.ActivityCode = item.ActivityCode;
                        if (item.EndTime.Days > 0)
                        {
                            time2 = new TimeSpan(time2.Hours, time2.Minutes, time2.Seconds);
                            buffer.BeginTime = new TimeSpan(item.EndTime.Hours, item.EndTime.Minutes, item.EndTime.Seconds);
                            buffer.EndTime = time2;
                            buffer.OTDate = item.OTDate.AddDays(item.EndTime.Days);
                            if (buffer.OTDate.Month != MWS.WS_Month || buffer.OTDate.Year != MWS.WS_Year)
                            {
                                MWS = MonthlyWS.GetCalendar(buffer.EmployeeID, buffer.OTDate.Year, buffer.OTDate.Month);
                            }
                        }
                        else
                        {
                            buffer.BeginTime = item.EndTime;
                            buffer.EndTime = time2;
                            buffer.OTDate = item.OTDate;
                        }
                        item = buffer;
                    }
                } while (!lEnd);
            }
            return list;
        }

        public DailyOTLog CalculateDailyOT(String EmpID, DailyWS _DailyWS, MonthlyWS MWS, DateTime DateBeginRequest, DateTime DateEndRequest, List<DelegateData> lstDelegate)
        {
            return CalculateDailyOTNew(EmpID, _DailyWS, MWS, DateBeginRequest, DateEndRequest, 0, lstDelegate);
            //return CalculateDailyOT(EmpID,_DailyWS,MWS,DateBeginRequest,DateEndRequest,0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="OTType">
        /// OTType == 0 ? DailyOTRequest : OTType == 1 ? MonthlyOTEval : OTType == 2 ? MonthlyOTFinal : false
        /// </param>
        /// <returns></returns>
        public DailyOTLog CalculateDailyOT(String EmpID, DailyWS _DailyWS, MonthlyWS MWS, DateTime DateBeginRequest, DateTime DateEndRequest, Byte OTType)
        {
            #region "Old version"
            //DateTime WorkBeginDateTime,OffBeginDateTime;
            //DateTime WorkEndDateTime,OffEndDateTime;
            //DateTime CurrentDate = new DateTime(DateBeginRequest.Year, DateBeginRequest.Month, DateBeginRequest.Day);
            //DateTime EndDayToOff = new DateTime();
            //DateTime EndDayOffOT1x = new DateTime();
            //Double OTx1, OTx1_5, OTx3;
            //OTx1 = OTx1_5 = OTx3 = 0;
            //DailyOTLog Result = new DailyOTLog();
            //EmployeeData EmpData = EmployeeData.GetUserSetting(EmpID).Employee;
            //DailyWS DWS,DWS_C,DWS_P,DWS_N;
            //CalculateOTType otType;
            //List<DateTimeIntersec> OTx1Period = new List<DateTimeIntersec>();
            //List<DateTimeIntersec> OTx15Period = new List<DateTimeIntersec>();
            //List<DateTimeIntersec> OTx3Period = new List<DateTimeIntersec>();
            //DateTimeIntersec OTPeriod;
            //Dictionary<DateTime, DailyWS> dictDWS = new Dictionary<DateTime, DailyWS>();
            ////ModifiedBy: Ratchatawan W. (2012-01-09)
            ////DateTime beginDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            ////DateTime endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
            //DateTime beginDate = new DateTime(CurrentDate.Year, CurrentDate.Month, 1);
            //DateTime endDate = beginDate.AddMonths(1).AddMinutes(-1);

            ////CHAT 2011-12-15 Set Work Begin-End Time of Day off
            //OffBeginDateTime = CurrentDate.Add(new TimeSpan(8, 30, 0));
            //OffEndDateTime = CurrentDate.Add(new TimeSpan(17, 30, 0));

            //Result.EmployeeID = EmpID;
            //Result.oTimeEval = new List<TimeEval>();

            //// find Daily Work Schedule in this period
            //for (DateTime runningDate = beginDate.AddDays(-1); runningDate <= endDate.AddDays(1); runningDate = runningDate.AddDays(1))
            //{
            //    EmployeeData empData = new EmployeeData(EmpID, runningDate.Date);
            //    DailyWS dws = empData.GetDailyWorkSchedule1(runningDate.Date);

            //    dws.BeginDate = runningDate;

            //    // fill BeginDate and EndDate.
            //    // EndDate is after BeginDate one day, if DWS is over midnight.
            //    if (dws.IsDayOffOrHoliday)
            //    {
            //        dws.EndDate = runningDate;
            //    }
            //    else
            //    {
            //        if (dws.WorkBeginTime <= dws.WorkEndTime)
            //        {
            //            dws.EndDate = runningDate;
            //        }
            //        else
            //        {
            //            dws.EndDate = runningDate.AddDays(1);
            //        }
            //    }
            //    dictDWS[runningDate] = dws;
            //}

            //DWS_C = dictDWS[CurrentDate.Date];
            //DWS_P = dictDWS[CurrentDate.AddDays(-1).Date];
            //DWS_N = dictDWS[CurrentDate.AddDays(1).Date];

            //if (!_DailyWS.IsDayOff && MWS.GetWSCode(DateBeginRequest.Day, false) != "1")
            //#region "WorkDay"
            //{
            //    WorkBeginDateTime = CurrentDate.Add(_DailyWS.WorkBeginTime);
            //    WorkEndDateTime = _DailyWS.WorkEndTime < _DailyWS.WorkBeginTime ? CurrentDate.Add(_DailyWS.WorkEndTime).AddDays(1) : CurrentDate.Add(_DailyWS.WorkEndTime);

            //    if (EmpData.EmpSubAreaType == EmployeeSubAreaType.Norm || EmpData.EmpSubAreaType == EmployeeSubAreaType.Flex)
            //    {
            //        #region "Norm Or Flex"

            //        otType = new CalculateOTType();
            //        DWS = EmpData.GetDailyWorkSchedule(CurrentDate.AddDays(-1), true);
            //        if (DWS.IsDayOff || MWS.GetWSCode(CurrentDate.AddDays(-1).Day, false) == "1")
            //        {
            //            // Off-Day-?
            //            otType = CalculateOTType.OWW;

            //            OTPeriod = new DateTimeIntersec();
            //            OTPeriod.Begin1 = CurrentDate;
            //            OTPeriod.End1 = WorkBeginDateTime;
            //            OTx15Period.Add(OTPeriod);

            //            OTPeriod = new DateTimeIntersec();
            //            OTPeriod.Begin1 = OffEndDateTime.AddDays(-1);
            //            OTPeriod.End1 = CurrentDate;
            //            OTx3Period.Add(OTPeriod);

            //            OTPeriod = new DateTimeIntersec();
            //            OTPeriod.Begin1 = OffBeginDateTime.AddDays(-1);
            //            OTPeriod.End1 = OffEndDateTime.AddDays(-1).AddHours(-5);
            //            OTx1Period.Add(OTPeriod);

            //            OTPeriod = new DateTimeIntersec();
            //            OTPeriod.Begin1 = OffBeginDateTime.AddDays(-1).AddHours(5);
            //            OTPeriod.End1 = OffEndDateTime.AddDays(-1);
            //            OTx1Period.Add(OTPeriod);

            //            OTPeriod = new DateTimeIntersec();
            //            OTPeriod.Begin1 = CurrentDate.AddDays(-1);
            //            OTPeriod.End1 = OffBeginDateTime.AddDays(-1);
            //            OTx3Period.Add(OTPeriod);
            //        }
            //        else
            //        {
            //            // Day-Day-?
            //            otType = CalculateOTType.WWW;

            //            OTPeriod = new DateTimeIntersec();
            //            OTPeriod.Begin1 = CurrentDate.AddDays(-1).Add(DWS.WorkEndTime);
            //            OTPeriod.End1 = WorkBeginDateTime;
            //            OTx15Period.Add(OTPeriod);

            //            OTPeriod = new DateTimeIntersec();
            //            OTPeriod.Begin1 = CurrentDate.AddDays(-1);
            //            OTPeriod.End1 = CurrentDate.AddDays(-1).Add(DWS.WorkBeginTime);
            //            OTx15Period.Add(OTPeriod);
            //        }

            //        DWS = EmpData.GetDailyWorkSchedule(CurrentDate.AddDays(1), true);
            //        if (DWS.IsDayOff || MWS.GetWSCode(CurrentDate.AddDays(1).Day, false) == "1")
            //        {
            //            // ?-Day-Off
            //            otType = otType == CalculateOTType.OWW ? CalculateOTType.OWO : CalculateOTType.WWO;

            //            OTPeriod = new DateTimeIntersec();
            //            OTPeriod.Begin1 = WorkEndDateTime;
            //            OTPeriod.End1 = CurrentDate.AddDays(1);
            //            OTx15Period.Add(OTPeriod);

            //            OTPeriod = new DateTimeIntersec();
            //            OTPeriod.Begin1 = CurrentDate.AddDays(1);
            //            OTPeriod.End1 = OffBeginDateTime.AddDays(1);
            //            OTx3Period.Add(OTPeriod);

            //            OTPeriod = new DateTimeIntersec();
            //            OTPeriod.Begin1 = OffBeginDateTime.AddDays(1);
            //            OTPeriod.End1 = OffEndDateTime.AddDays(1).AddHours(-5);
            //            OTx1Period.Add(OTPeriod);

            //            OTPeriod = new DateTimeIntersec();
            //            OTPeriod.Begin1 = OffBeginDateTime.AddDays(1).AddHours(5);
            //            OTPeriod.End1 = OffEndDateTime.AddDays(1);
            //            OTx1Period.Add(OTPeriod);

            //            OTPeriod = new DateTimeIntersec();
            //            OTPeriod.Begin1 = OffEndDateTime.AddDays(1);
            //            OTPeriod.End1 = CurrentDate.AddDays(2);
            //            OTx3Period.Add(OTPeriod);
            //        }
            //        else
            //        {
            //            // ?-Day-Day
            //            otType = otType == CalculateOTType.OWW ? CalculateOTType.OWW : CalculateOTType.WWW;

            //            OTPeriod = new DateTimeIntersec();
            //            OTPeriod.Begin1 = WorkEndDateTime;
            //            OTPeriod.End1 = CurrentDate.AddDays(1).Add(DWS.WorkBeginTime);
            //            OTx15Period.Add(OTPeriod);

            //            OTPeriod = new DateTimeIntersec();
            //            OTPeriod.Begin1 = CurrentDate.AddDays(1).Add(DWS.WorkEndTime);
            //            OTPeriod.End1 = CurrentDate.AddDays(1);
            //            OTx15Period.Add(OTPeriod);
            //        }

            //        #endregion
            //    }
            //    else
            //    {
            //        #region "Not(Norm Or Flex)"
            //        otType = new CalculateOTType();

            //        DWS = EmpData.GetDailyWorkSchedule(CurrentDate.AddDays(-1), true);
            //        if (DWS.IsDayOff || MWS.GetWSCode(CurrentDate.AddDays(-1).Day, false) == "1")
            //        {
            //            // Off-Day-?
            //            otType = CalculateOTType.OWW;

            //            if (WorkBeginDateTime > DateBeginRequest)
            //            {
            //                OTPeriod = new DateTimeIntersec();
            //                OTPeriod.Begin1 = WorkBeginDateTime.AddHours(-6);
            //                OTPeriod.End1 = WorkBeginDateTime;
            //                OTx15Period.Add(OTPeriod);

            //                DateTime chkTime = WorkBeginDateTime.AddHours(-6);
            //                TimeSpan subchkTime = chkTime.Subtract(DateBeginRequest);

            //                if (DateBeginRequest < chkTime)
            //                {
            //                    if(subchkTime.Hours > 8)
            //                    {
            //                        OTPeriod = new DateTimeIntersec();
            //                        OTPeriod.Begin1 = CurrentDate.Date;
            //                        OTPeriod.End1 = CurrentDate.AddHours(8);
            //                        OTx1Period.Add(OTPeriod);

            //                        OTPeriod = new DateTimeIntersec();
            //                        OTPeriod.Begin1 = CurrentDate.AddHours(8);
            //                        OTPeriod.End1 = WorkBeginDateTime.AddHours(-6);
            //                        OTx3Period.Add(OTPeriod);
            //                    }
            //                    else
            //                    {
            //                        OTPeriod = new DateTimeIntersec();
            //                        OTPeriod.Begin1 = DateBeginRequest;
            //                        OTPeriod.End1 = WorkBeginDateTime.AddHours(-6);
            //                        OTx1Period.Add(OTPeriod);
            //                    }
            //                }
            //            }
            //        }
            //        else
            //        {
            //            // Day-Day-?
            //            otType = CalculateOTType.WWW;

            //            OTPeriod = new DateTimeIntersec();
            //            OTPeriod.Begin1 = WorkBeginDateTime.AddHours(-12);
            //            OTPeriod.End1 = WorkBeginDateTime;
            //            OTx15Period.Add(OTPeriod);
            //        }

            //        DWS = EmpData.GetDailyWorkSchedule(CurrentDate.AddDays(1), true);
            //        if (DWS.IsDayOff || MWS.GetWSCode(CurrentDate.AddDays(1).Day, false) == "1")
            //        {
            //            // ?-Day-Off
            //            otType = otType == CalculateOTType.OWW ? CalculateOTType.OWO : CalculateOTType.WWO;

            //            OTPeriod = new DateTimeIntersec();
            //            OTPeriod.Begin1 = WorkEndDateTime;
            //            OTPeriod.End1 = WorkEndDateTime.AddHours(6);
            //            OTx15Period.Add(OTPeriod);

            //            if (WorkBeginDateTime.AddHours(24) > OffEndDateTime.AddDays(1))
            //            {
            //                OTPeriod = new DateTimeIntersec();
            //                OTPeriod.Begin1 = WorkEndDateTime.AddDays(-1);
            //                OTPeriod.End1 = WorkBeginDateTime.AddHours(-6);
            //                OTx15Period.Add(OTPeriod);
            //            }
            //            else if (WorkBeginDateTime.AddHours(24) > OffBeginDateTime.AddDays(1) && WorkBeginDateTime.AddHours(24) < OffEndDateTime.AddDays(1))
            //            {
            //                OTPeriod = new DateTimeIntersec();
            //                OTPeriod.Begin1 = WorkBeginDateTime.AddHours(24);
            //                OTPeriod.End1 = OffEndDateTime.AddDays(1);
            //                OTx1Period.Add(OTPeriod);

            //                OTPeriod = new DateTimeIntersec();
            //                OTPeriod.Begin1 = OffEndDateTime.AddDays(1);
            //                OTPeriod.End1 = CurrentDate.AddDays(2);
            //                OTx3Period.Add(OTPeriod);
            //            }
            //            else if (WorkBeginDateTime.AddHours(24) <= OffBeginDateTime.AddDays(1))
            //            {
            //                OTPeriod = new DateTimeIntersec();
            //                OTPeriod.Begin1 = WorkEndDateTime.AddHours(6);
            //                EndDayOffOT1x = OTPeriod.Begin1;
            //                OTPeriod.End1 = EndDayOffOT1x.AddHours(8);
            //                OTx1Period.Add(OTPeriod);

            //                TimeSpan subRequest = DateEndRequest.Subtract(DateBeginRequest);
            //                if (subRequest.Hours > 8)
            //                {
            //                    OTPeriod = new DateTimeIntersec();
            //                    OTPeriod.Begin1 = EndDayOffOT1x.AddHours(8);
            //                    OTPeriod.End1 = DateEndRequest;
            //                    OTx3Period.Add(OTPeriod);
            //                }
            //            }
            //        }
            //        else
            //        {
            //            // ?-Day-Day
            //            otType = otType == CalculateOTType.OWW ? CalculateOTType.OWW : CalculateOTType.WWW;

            //            OTPeriod = new DateTimeIntersec();
            //            OTPeriod.Begin1 = WorkEndDateTime;
            //            OTPeriod.End1 = WorkEndDateTime.AddHours(12);
            //            OTx15Period.Add(OTPeriod);
            //        }
            //        #endregion
            //    }
            //}
            //#endregion
            //else
            //#region " DayOff / Holiday "
            //{
            //    WorkBeginDateTime = OffBeginDateTime;
            //    WorkEndDateTime = OffEndDateTime;

            //    if (EmpData.EmpSubAreaType == EmployeeSubAreaType.Norm || EmpData.EmpSubAreaType == EmployeeSubAreaType.Flex)
            //    {
            //        #region "Norm Or Flex"
            //        OTPeriod = new DateTimeIntersec();
            //        OTPeriod.Begin1 = OffBeginDateTime;
            //        OTPeriod.End1 = OffEndDateTime.AddHours(-5);
            //        OTx1Period.Add(OTPeriod);

            //        OTPeriod = new DateTimeIntersec();
            //        OTPeriod.Begin1 = OffBeginDateTime.AddHours(5);
            //        OTPeriod.End1 = OffEndDateTime;
            //        OTx1Period.Add(OTPeriod);

            //        otType = new CalculateOTType();
            //        DWS = EmpData.GetDailyWorkSchedule(CurrentDate.AddDays(-1), true);
            //        if (DWS.IsDayOff || MWS.GetWSCode(CurrentDate.AddDays(-1).Day, false) == "1")
            //        {
            //            // off-off-?
            //            otType = CalculateOTType.OOW;

            //            OTPeriod = new DateTimeIntersec();
            //            OTPeriod.Begin1 = OffEndDateTime.AddDays(-1);
            //            OTPeriod.End1 = OffBeginDateTime;
            //            OTx3Period.Add(OTPeriod);

            //            OTPeriod = new DateTimeIntersec();
            //            OTPeriod.Begin1 = OffBeginDateTime.AddDays(-1);
            //            OTPeriod.End1 = OffEndDateTime.AddDays(-1).AddHours(-5);
            //            OTx1Period.Add(OTPeriod);

            //            OTPeriod = new DateTimeIntersec();
            //            OTPeriod.Begin1 = OffBeginDateTime.AddDays(-1).AddHours(5);
            //            OTPeriod.End1 = OffEndDateTime.AddDays(-1);
            //            OTx1Period.Add(OTPeriod);

            //            OTPeriod = new DateTimeIntersec();
            //            OTPeriod.Begin1 = CurrentDate.AddDays(-1);
            //            OTPeriod.End1 = OffBeginDateTime.AddDays(-1);
            //            OTx3Period.Add(OTPeriod);
            //        }
            //        else
            //        {
            //            // Day-off-?
            //            otType = CalculateOTType.WOW;

            //            OTPeriod = new DateTimeIntersec();
            //            OTPeriod.Begin1 = CurrentDate;
            //            OTPeriod.End1 = OffBeginDateTime;
            //            OTx3Period.Add(OTPeriod);

            //            OTPeriod = new DateTimeIntersec();
            //            OTPeriod.Begin1 = OffEndDateTime.AddDays(-1);
            //            OTPeriod.End1 = CurrentDate;
            //            OTx15Period.Add(OTPeriod);

            //            OTPeriod = new DateTimeIntersec();
            //            OTPeriod.Begin1 = CurrentDate.AddDays(-1);
            //            OTPeriod.End1 = OffBeginDateTime.AddDays(-1);
            //            OTx15Period.Add(OTPeriod);
            //        }

            //        DWS = EmpData.GetDailyWorkSchedule(CurrentDate.AddDays(1), true);
            //        if (DWS.IsDayOff || MWS.GetWSCode(CurrentDate.AddDays(1).Day, false) == "1")
            //        {
            //            // ?-Off-Off
            //            otType = otType == CalculateOTType.OOW ? CalculateOTType.OOO : CalculateOTType.WOO;

            //            OTPeriod = new DateTimeIntersec();
            //            OTPeriod.Begin1 = OffEndDateTime;
            //            OTPeriod.End1 = OffBeginDateTime.AddDays(1);
            //            OTx3Period.Add(OTPeriod);

            //            OTPeriod = new DateTimeIntersec();
            //            OTPeriod.Begin1 = OffBeginDateTime.AddDays(1);
            //            OTPeriod.End1 = OffEndDateTime.AddDays(1).AddHours(-5);
            //            OTx1Period.Add(OTPeriod);

            //            OTPeriod = new DateTimeIntersec();
            //            OTPeriod.Begin1 = OffBeginDateTime.AddDays(1).AddHours(5);
            //            OTPeriod.End1 = OffEndDateTime.AddDays(1);
            //            OTx1Period.Add(OTPeriod);

            //            OTPeriod = new DateTimeIntersec();
            //            OTPeriod.Begin1 = OffEndDateTime.AddDays(1);
            //            OTPeriod.End1 = CurrentDate.AddDays(2);
            //            OTx3Period.Add(OTPeriod);
            //        }
            //        else
            //        {
            //            // ?-Off-Day
            //            otType = otType == CalculateOTType.OOW ? CalculateOTType.OOW : CalculateOTType.WOW;

            //            OTPeriod = new DateTimeIntersec();
            //            OTPeriod.Begin1 = OffEndDateTime;
            //            OTPeriod.End1 = CurrentDate.AddDays(1);
            //            OTx3Period.Add(OTPeriod);

            //            OTPeriod = new DateTimeIntersec();
            //            OTPeriod.Begin1 = CurrentDate.AddDays(1);
            //            OTPeriod.End1 = OffBeginDateTime.AddDays(1);
            //            OTx15Period.Add(OTPeriod);

            //            OTPeriod = new DateTimeIntersec();
            //            OTPeriod.Begin1 = OffEndDateTime.AddDays(1);
            //            OTPeriod.End1 = CurrentDate.AddDays(2);
            //            OTx15Period.Add(OTPeriod);
            //        }
            //        #endregion
            //    }
            //    else
            //    {
            //        #region "Not(Norm Or Flex)"
            //        otType = new CalculateOTType();
            //        DWS = EmpData.GetDailyWorkSchedule(CurrentDate.AddDays(-1), true);
            //        if (DWS.IsDayOff || MWS.GetWSCode(CurrentDate.AddDays(-1).Day, false) == "1")
            //        {
            //            // Off-Off-?
            //            otType = CalculateOTType.OOW;

            //            if (!DWS_N.IsDayOffOrHoliday)
            //            {
            //                OTPeriod = new DateTimeIntersec();
            //                OTPeriod.Begin1 = DateBeginRequest;
            //                OTPeriod.End1 = DateBeginRequest.Add(new TimeSpan(8, 0, 0));
            //                OTx1Period.Add(OTPeriod);

            //                TimeSpan subRequest = DateEndRequest.Subtract(DateBeginRequest);
            //                if (subRequest.Hours > 8)
            //                {
            //                    OTPeriod = new DateTimeIntersec();
            //                    OTPeriod.Begin1 = DateBeginRequest.Add(new TimeSpan(8, 0, 0));
            //                    OTPeriod.End1 = DWS_N.BeginDate.Add(DWS_N.WorkBeginTime.Add(new TimeSpan(-6,0,0)));
            //                    OTx3Period.Add(OTPeriod);
            //                }

            //                OTPeriod = new DateTimeIntersec();
            //                OTPeriod.Begin1 = DWS_N.BeginDate.Add(DWS_N.WorkBeginTime.Add(new TimeSpan(-6, 0, 0)));
            //                OTPeriod.End1 = DWS_N.BeginDate.Add(DWS_N.WorkBeginTime);
            //                OTx15Period.Add(OTPeriod);
            //            }
            //        }
            //        else
            //        {
            //            // Day-Off-?
            //            otType = CalculateOTType.WOW;

            //            OTPeriod = new DateTimeIntersec();
            //            OTPeriod.Begin1 = DWS_P.EndDate.Add(DWS_P.WorkEndTime);
            //            EndDayToOff = OTPeriod.End1 = DWS_P.EndDate.Add(DWS_P.WorkEndTime.Add(new TimeSpan(24,0,0)));
            //            OTx15Period.Add(OTPeriod);

            //            if (CurrentDate.AddDays(-1).Add(DWS.WorkBeginTime).AddHours(24) <= OffBeginDateTime)
            //            {
            //                OTPeriod = new DateTimeIntersec();
            //                OTPeriod.Begin1 = DWS_P.EndDate.Add(DWS_P.WorkEndTime.Add(new TimeSpan(6,0,0)));
            //                OTPeriod.End1 = OTPeriod.Begin1.Add(new TimeSpan(8,0,0));
            //                EndDayOffOT1x = OTPeriod.End1;
            //                OTx1Period.Add(OTPeriod);

            //                OTPeriod = new DateTimeIntersec();
            //                OTPeriod.Begin1 = EndDayOffOT1x;
            //                OTPeriod.End1 = DWS_N.BeginDate.Add(DWS_N.WorkBeginTime.Subtract(new TimeSpan(6, 0, 0)));
            //                OTx3Period.Add(OTPeriod);
            //            }
            //            else if (CurrentDate.AddDays(-1).Add(DWS.WorkBeginTime).AddHours(24) > OffBeginDateTime && CurrentDate.AddDays(-1).Add(DWS.WorkBeginTime).AddHours(24) < OffEndDateTime)
            //            {
            //                OTPeriod = new DateTimeIntersec();
            //                OTPeriod.Begin1 = CurrentDate.AddDays(-1).Add(DWS.WorkBeginTime).AddHours(24);
            //                OTPeriod.End1 = OffEndDateTime;
            //                OTx1Period.Add(OTPeriod);
            //            }
            //        }

            //        DWS = EmpData.GetDailyWorkSchedule(CurrentDate.AddDays(1), true);
            //        if (DWS.IsDayOff || MWS.GetWSCode(CurrentDate.AddDays(1).Day, false) == "1")
            //        {
            //            // ?-Off-Off
            //            otType = otType == CalculateOTType.OOW ? CalculateOTType.OOO : CalculateOTType.WOO;

            //            if (otType == CalculateOTType.WOO)
            //            {
            //                OTPeriod = new DateTimeIntersec();
            //                OTPeriod.Begin1 = DWS_P.EndDate.Add(DWS_P.WorkEndTime);
            //                OTPeriod.End1 = DWS_P.EndDate.Add(DWS_P.WorkEndTime.Add(new TimeSpan(6, 0, 0)));
            //                OTx15Period.Add(OTPeriod);

            //                OTPeriod = new DateTimeIntersec();
            //                OTPeriod.Begin1 = DWS_P.EndDate.Add(DWS_P.WorkEndTime.Add(new TimeSpan(6, 0, 0)));
            //                OTPeriod.End1 = DWS_P.EndDate.Add(DWS_P.WorkEndTime.Add(new TimeSpan(6, 0, 0))).AddHours(8);
            //                OTx1Period.Add(OTPeriod);

            //                OTPeriod = new DateTimeIntersec();
            //                OTPeriod.Begin1 = DWS_P.EndDate.Add(DWS_P.WorkEndTime.Add(new TimeSpan(6, 0, 0))).AddHours(8);
            //                OTPeriod.End1 = DWS_C.EndDate;
            //                OTx3Period.Add(OTPeriod);
            //            }
            //            else
            //            { 
            //                //OOO

            //                OTPeriod = new DateTimeIntersec();
            //                OTPeriod.Begin1 = DateBeginRequest;
            //                OTPeriod.End1 = DateBeginRequest.AddHours(8);
            //                OTx1Period.Add(OTPeriod);

            //                TimeSpan subRequest = DateEndRequest.Subtract(DateBeginRequest);
            //                if (subRequest.Hours > 8)
            //                {
            //                    OTPeriod = new DateTimeIntersec();
            //                    OTPeriod.Begin1 = DateBeginRequest.AddHours(8);
            //                    OTPeriod.End1 = DateEndRequest;
            //                    OTx3Period.Add(OTPeriod);
            //                }
            //            }
            //        }
            //        else
            //        {
            //            // ?-Off-Day
            //            otType = otType == CalculateOTType.OOW ? CalculateOTType.OOW : CalculateOTType.WOW;

            //            OTPeriod = new DateTimeIntersec();
            //            OTPeriod.Begin1 = OffEndDateTime > EndDayToOff ? OffEndDateTime : EndDayToOff;
            //            OTPeriod.End1 = OffBeginDateTime.AddDays(1) < CurrentDate.AddDays(1).Add(DWS.WorkBeginTime) ? OffBeginDateTime.AddDays(1) : CurrentDate.AddDays(1).Add(DWS.WorkBeginTime);
            //            OTx3Period.Add(OTPeriod);

            //            if (OffBeginDateTime.AddDays(1) < CurrentDate.AddDays(1).Add(DWS.WorkBeginTime))
            //            {
            //                OTPeriod = new DateTimeIntersec();
            //                OTPeriod.Begin1 = OffBeginDateTime.AddDays(1);
            //                OTPeriod.End1 = CurrentDate.AddDays(1).Add(DWS.WorkBeginTime);
            //                OTx1Period.Add(OTPeriod);
            //            }
            //        }
            //        #endregion
            //    }

            //}
            //#endregion

            //#region "Match OT"
            //foreach (DateTimeIntersec item in OTx1Period)
            //{
            //    item.Begin2 = DateBeginRequest;
            //    item.End2 = DateEndRequest;
            //    if (item.IsIntersecOT())
            //    {
            //        item.Intersection();
            //        OTx1 += ((TimeSpan)(item.EndBound - item.BeginBound)).TotalMinutes;
            //        AddTimeEval(item.BeginBound, item.EndBound, "2010", ref Result);
            //    }
            //}

            //foreach(DateTimeIntersec item in OTx15Period)
            //{
            //    item.Begin2 = DateBeginRequest;
            //    item.End2 = DateEndRequest;
            //    if (item.IsIntersecOT())
            //    {
            //        item.Intersection();
            //        OTx1_5 += ((TimeSpan)(item.EndBound - item.BeginBound)).TotalMinutes;
            //        AddTimeEval(item.BeginBound, item.EndBound, "2015", ref Result);
            //    }
            //}

            //foreach(DateTimeIntersec item in OTx3Period)
            //{
            //    item.Begin2 = DateBeginRequest;
            //    item.End2 = DateEndRequest;
            //    if (item.IsIntersecOT())
            //    {
            //        item.Intersection();
            //        OTx3 += ((TimeSpan)(item.EndBound - item.BeginBound)).TotalMinutes;
            //        AddTimeEval(item.BeginBound, item.EndBound, "2030", ref Result);
            //    }
            //}
            //#endregion

            //#region "Fill OT"

            //#endregion

            //if (OTType == 0)
            //{
            //    Result.RequestOTHour10 = Convert.ToDecimal(OTx1);
            //    Result.RequestOTHour15 = Convert.ToDecimal(OTx1_5);
            //    Result.RequestOTHour30 = Convert.ToDecimal(OTx3);
            //}
            //else if (OTType == 1)
            //{
            //    Result.EvaOTHour10 = Convert.ToDecimal(OTx1);
            //    Result.EvaOTHour15 = Convert.ToDecimal(OTx1_5);
            //    Result.EvaOTHour30 = Convert.ToDecimal(OTx3);
            //}
            //else if (OTType == 2)
            //{
            //    Result.FinalOTHour10 = Convert.ToDecimal(OTx1);
            //    Result.FinalOTHour15 = Convert.ToDecimal(OTx1_5);
            //    Result.FinalOTHour30 = Convert.ToDecimal(OTx3);
            //}
            //return Result;
            #endregion

            DateTime WorkBeginDateTime, OffBeginDateTime;
            DateTime WorkEndDateTime, OffEndDateTime;
            DateTime CurrentDate = new DateTime(DateBeginRequest.Year, DateBeginRequest.Month, DateBeginRequest.Day);
            DateTime EndDayToOff = new DateTime();
            Double OTx1, OTx1_5, OTx3;
            OTx1 = OTx1_5 = OTx3 = 0;
            DailyOTLog Result = new DailyOTLog();
            EmployeeData EmpData = new EmployeeData(EmpID, DateBeginRequest);
            DailyWS DWS;
            CalculateOTType otType;
            List<DateTimeIntersec> OTx1Period = new List<DateTimeIntersec>();
            List<DateTimeIntersec> OTx15Period = new List<DateTimeIntersec>();
            List<DateTimeIntersec> OTx3Period = new List<DateTimeIntersec>();
            DateTimeIntersec OTPeriod;
            bool IsFirst6HourFromWorkEndTime = true;
            bool IsOffToDay = false;
            OffBeginDateTime = CurrentDate.Add(new TimeSpan(8, 30, 0));
            OffEndDateTime = CurrentDate.Add(new TimeSpan(17, 30, 0));

            Result.EmployeeID = EmpID;
            Result.oTimeEval = new List<TimeEval>();

            if (!_DailyWS.IsDayOff && MWS.GetWSCode(DateBeginRequest.Day, false) != "1")
            #region "WorkDay"
            {
                WorkBeginDateTime = CurrentDate.Add(_DailyWS.WorkBeginTime);
                WorkEndDateTime = _DailyWS.WorkEndTime < _DailyWS.WorkBeginTime ? CurrentDate.Add(_DailyWS.WorkEndTime).AddDays(1) : CurrentDate.Add(_DailyWS.WorkEndTime);

                if (EmpData.EmpSubAreaType == EmployeeSubAreaType.Norm || EmpData.EmpSubAreaType == EmployeeSubAreaType.Flex)
                {
                    #region "Norm Or Flex"

                    otType = new CalculateOTType();
                    DWS = EmpData.GetDailyWorkSchedule(CurrentDate.AddDays(-1), true);
                    if (EmpData.DateSpecific.HiringDate.Date < CurrentDate.AddDays(-1).Date)
                        if (DWS.IsDayOff || MWS.GetWSCode(CurrentDate.AddDays(-1).Day, false) == "1")
                        {
                            // Off-Day-?
                            otType = CalculateOTType.OWW;

                            OTPeriod = new DateTimeIntersec();
                            OTPeriod.Begin1 = CurrentDate;
                            OTPeriod.End1 = WorkBeginDateTime;
                            OTx15Period.Add(OTPeriod);

                            OTPeriod = new DateTimeIntersec();
                            OTPeriod.Begin1 = OffEndDateTime.AddDays(-1);
                            OTPeriod.End1 = CurrentDate;
                            OTx3Period.Add(OTPeriod);
                        }
                        else
                        {
                            // Day-Day-?
                            otType = CalculateOTType.WWW;

                            OTPeriod = new DateTimeIntersec();
                            OTPeriod.Begin1 = CurrentDate.AddDays(-1).Add(DWS.WorkEndTime);
                            OTPeriod.End1 = WorkBeginDateTime;
                            OTx15Period.Add(OTPeriod);

                        }
                    else
                    {
                        otType = CalculateOTType.OWW;
                        OTPeriod = new DateTimeIntersec();
                        OTPeriod.Begin1 = CurrentDate.AddDays(-1);
                        OTPeriod.End1 = WorkBeginDateTime;
                        OTx15Period.Add(OTPeriod);
                    }

                    DWS = EmpData.GetDailyWorkSchedule(CurrentDate.AddDays(1), true);
                    if (DWS.IsDayOff || MWS.GetWSCode(CurrentDate.AddDays(1).Day, false) == "1")
                    {
                        // ?-Day-Off
                        otType = otType == CalculateOTType.OWW ? CalculateOTType.OWO : CalculateOTType.WWO;

                        OTPeriod = new DateTimeIntersec();
                        OTPeriod.Begin1 = WorkEndDateTime;
                        OTPeriod.End1 = CurrentDate.AddDays(1);
                        OTx15Period.Add(OTPeriod);

                        OTPeriod = new DateTimeIntersec();
                        OTPeriod.Begin1 = CurrentDate.AddDays(1);
                        OTPeriod.End1 = OffBeginDateTime.AddDays(1);
                        OTx3Period.Add(OTPeriod);
                    }
                    else
                    {
                        // ?-Day-Day
                        otType = otType == CalculateOTType.OWW ? CalculateOTType.OWW : CalculateOTType.WWW;

                        OTPeriod = new DateTimeIntersec();
                        OTPeriod.Begin1 = WorkEndDateTime;
                        OTPeriod.End1 = CurrentDate.AddDays(1).Add(DWS.WorkBeginTime);
                        OTx15Period.Add(OTPeriod);
                    }

                    #endregion
                }
                else
                {
                    #region "Not(Norm Or Flex)"

                    otType = new CalculateOTType();

                    DWS = EmpData.GetDailyWorkSchedule(CurrentDate.AddDays(-1), true);
                    if (EmpData.DateSpecific.HiringDate.Date < CurrentDate.AddDays(-1).Date)
                    {
                        if (DWS.IsDayOff || MWS.GetWSCode(CurrentDate.AddDays(-1).Day, false) == "1")
                        {
                            // Off-Day-?
                            otType = CalculateOTType.OWW;

                            if (IsOffToDay = DateBeginRequest > CurrentDate.AddDays(-1) && DateEndRequest <= WorkBeginDateTime) //&& WorkBeginDateTime.AddHours(-6) > DateBeginRequest
                            {
                                if (((TimeSpan)(WorkBeginDateTime.AddHours(-6) - DateBeginRequest)).TotalHours > 8)
                                    if (DateEndRequest > WorkBeginDateTime.AddHours(-6))
                                    {
                                        OTx1 = 8 * 60;
                                        OTx3 = ((TimeSpan)(WorkBeginDateTime.AddHours(-6) - DateBeginRequest.AddHours(8))).TotalMinutes;
                                        OTx1_5 = ((TimeSpan)(DateEndRequest - WorkBeginDateTime.AddHours(-6))).TotalMinutes;
                                    }
                                    else
                                    {
                                        OTx1 = 8 * 60;
                                        OTx3 = ((TimeSpan)(DateEndRequest - DateBeginRequest.AddHours(8))).TotalMinutes;
                                    }
                                else
                                {
                                    if (((TimeSpan)(WorkBeginDateTime - DateBeginRequest)).TotalHours <= 6)
                                        OTx1_5 = ((TimeSpan)(DateEndRequest - DateBeginRequest)).TotalMinutes;
                                    else
                                    {
                                        OTx1 = ((TimeSpan)(WorkBeginDateTime.AddHours(-6) - DateBeginRequest)).TotalMinutes;
                                        OTx1_5 = ((TimeSpan)(DateEndRequest - WorkBeginDateTime.AddHours(-6))).TotalMinutes;
                                    }
                                }
                            }
                        }
                        else
                        {
                            // Day-Day-?
                            otType = CalculateOTType.WWW;

                            OTPeriod = new DateTimeIntersec();
                            OTPeriod.Begin1 = CurrentDate.AddDays(-1).Add(DWS.WorkEndTime);
                            OTPeriod.End1 = WorkBeginDateTime;
                            OTx15Period.Add(OTPeriod);
                        }
                    }

                    DWS = EmpData.GetDailyWorkSchedule(CurrentDate.AddDays(1), true);
                    if (DWS.IsDayOff || MWS.GetWSCode(CurrentDate.AddDays(1).Day, false) == "1")
                    {
                        // ?-Day-Off
                        otType = otType == CalculateOTType.OWW ? CalculateOTType.OWO : CalculateOTType.WWO;

                        OTPeriod = new DateTimeIntersec();
                        OTPeriod.Begin1 = WorkEndDateTime;
                        OTPeriod.End1 = WorkEndDateTime.AddHours(6);
                        OTx15Period.Add(OTPeriod);

                        OTPeriod = new DateTimeIntersec();
                        OTPeriod.Begin1 = WorkEndDateTime.AddHours(6); ;
                        OTPeriod.End1 = WorkEndDateTime.AddHours(6).AddHours(8);
                        OTx1Period.Add(OTPeriod);

                        OTPeriod = new DateTimeIntersec();
                        OTPeriod.Begin1 = WorkEndDateTime.AddHours(6).AddHours(8);
                        OTPeriod.End1 = CurrentDate.AddDays(2);
                        OTx3Period.Add(OTPeriod);
                    }
                    else
                    {
                        // ?-Day-Day
                        otType = otType == CalculateOTType.OWW ? CalculateOTType.OWW : CalculateOTType.WWW;

                        OTPeriod = new DateTimeIntersec();
                        OTPeriod.Begin1 = WorkEndDateTime;
                        OTPeriod.End1 = CurrentDate.AddDays(1).Add(DWS.WorkBeginTime);
                        OTx15Period.Add(OTPeriod);
                    }
                    #endregion
                }
            }
            #endregion
            else
            #region " DayOff / Holiday "
            {
                WorkBeginDateTime = OffBeginDateTime;
                WorkEndDateTime = OffEndDateTime;


                if (EmpData.EmpSubAreaType == EmployeeSubAreaType.Norm || EmpData.EmpSubAreaType == EmployeeSubAreaType.Flex)
                {
                    #region "Norm Or Flex"
                    OTPeriod = new DateTimeIntersec();
                    OTPeriod.Begin1 = OffBeginDateTime;
                    OTPeriod.End1 = OffEndDateTime.AddHours(-5.5);
                    OTx1Period.Add(OTPeriod);

                    OTPeriod = new DateTimeIntersec();
                    OTPeriod.Begin1 = OffBeginDateTime.AddHours(4.5);
                    OTPeriod.End1 = OffEndDateTime;
                    OTx1Period.Add(OTPeriod);

                    otType = new CalculateOTType();
                    DWS = EmpData.GetDailyWorkSchedule(CurrentDate.AddDays(-1), true);
                    if (DWS.IsDayOff || MWS.GetWSCode(CurrentDate.AddDays(-1).Day, false) == "1")
                    {
                        // off-off-?
                        otType = CalculateOTType.OOW;

                        OTPeriod = new DateTimeIntersec();
                        OTPeriod.Begin1 = OffEndDateTime.AddDays(-1);
                        OTPeriod.End1 = OffBeginDateTime;
                        OTx3Period.Add(OTPeriod);

                        OTPeriod = new DateTimeIntersec();
                        OTPeriod.Begin1 = OffBeginDateTime.AddDays(-1);
                        OTPeriod.End1 = OffEndDateTime.AddDays(-1).AddHours(-5.5);
                        OTx1Period.Add(OTPeriod);

                        OTPeriod = new DateTimeIntersec();
                        OTPeriod.Begin1 = OffBeginDateTime.AddDays(-1).AddHours(4.5);
                        OTPeriod.End1 = OffEndDateTime.AddDays(-1);
                        OTx1Period.Add(OTPeriod);

                        OTPeriod = new DateTimeIntersec();
                        OTPeriod.Begin1 = CurrentDate.AddDays(-1);
                        OTPeriod.End1 = OffBeginDateTime.AddDays(-1);
                        OTx3Period.Add(OTPeriod);
                    }
                    else
                    {
                        // Day-off-?
                        otType = CalculateOTType.WOW;

                        OTPeriod = new DateTimeIntersec();
                        OTPeriod.Begin1 = CurrentDate;
                        OTPeriod.End1 = OffBeginDateTime;
                        OTx3Period.Add(OTPeriod);

                        OTPeriod = new DateTimeIntersec();
                        OTPeriod.Begin1 = CurrentDate.AddDays(-1).Add(DWS.WorkEndTime);
                        OTPeriod.End1 = CurrentDate;
                        OTx15Period.Add(OTPeriod);

                    }

                    DWS = EmpData.GetDailyWorkSchedule(CurrentDate.AddDays(1), true);
                    if (DWS.IsDayOff || MWS.GetWSCode(CurrentDate.AddDays(1).Day, false) == "1")
                    {
                        // ?-Off-Off
                        otType = otType == CalculateOTType.OOW ? CalculateOTType.OOO : CalculateOTType.WOO;

                        OTPeriod = new DateTimeIntersec();
                        OTPeriod.Begin1 = OffEndDateTime;
                        OTPeriod.End1 = OffBeginDateTime.AddDays(1);
                        OTx3Period.Add(OTPeriod);

                        OTPeriod = new DateTimeIntersec();
                        OTPeriod.Begin1 = OffBeginDateTime.AddDays(1);
                        OTPeriod.End1 = OffEndDateTime.AddDays(1).AddHours(-5.5);
                        OTx1Period.Add(OTPeriod);

                        OTPeriod = new DateTimeIntersec();
                        OTPeriod.Begin1 = OffBeginDateTime.AddDays(1).AddHours(4.5);
                        OTPeriod.End1 = OffEndDateTime.AddDays(1);
                        OTx1Period.Add(OTPeriod);

                        OTPeriod = new DateTimeIntersec();
                        OTPeriod.Begin1 = OffEndDateTime.AddDays(1);
                        OTPeriod.End1 = CurrentDate.AddDays(2);
                        OTx3Period.Add(OTPeriod);
                    }
                    else
                    {
                        // ?-Off-Day
                        otType = otType == CalculateOTType.OOW ? CalculateOTType.OOW : CalculateOTType.WOW;

                        OTPeriod = new DateTimeIntersec();
                        OTPeriod.Begin1 = OffEndDateTime;
                        OTPeriod.End1 = CurrentDate.AddDays(1);
                        OTx3Period.Add(OTPeriod);

                        OTPeriod = new DateTimeIntersec();
                        OTPeriod.Begin1 = CurrentDate.AddDays(1);
                        OTPeriod.End1 = CurrentDate.AddDays(1).Add(DWS.WorkBeginTime);
                        OTx15Period.Add(OTPeriod);
                    }
                    #endregion
                }
                else
                {
                    #region "Not(Norm Or Flex)"
                    otType = new CalculateOTType();
                    DWS = EmpData.GetDailyWorkSchedule(CurrentDate.AddDays(-1), true);
                    if (DWS.IsDayOff || MWS.GetWSCode(CurrentDate.AddDays(-1).Day, false) == "1")
                    {
                        // Off-Off-?

                        otType = CalculateOTType.OOW;
                        IsFirst6HourFromWorkEndTime = false;

                        //OTPeriod = new DateTimeIntersec();
                        //OTPeriod.Begin1 = CurrentDate.AddDays(-1);
                        //OTPeriod.End1 = CurrentDate.AddDays(-1).AddHours(8);
                        //OTx1Period.Add(OTPeriod);

                        //OTPeriod = new DateTimeIntersec();
                        //OTPeriod.Begin1 = CurrentDate.AddDays(-1).AddHours(8);
                        //OTPeriod.End1 = CurrentDate.AddDays(1);
                        //OTx3Period.Add(OTPeriod);
                    }
                    else
                    {
                        // Day-Off-?
                        otType = CalculateOTType.WOW;

                        OTPeriod = new DateTimeIntersec();
                        OTPeriod.Begin1 = DWS.WorkBeginTime > DWS.WorkEndTime ? CurrentDate.Add(DWS.WorkEndTime) : CurrentDate.AddDays(-1).Add(DWS.WorkEndTime);
                        OTPeriod.End1 = OTPeriod.Begin1.AddHours(6);
                        OTx15Period.Add(OTPeriod);

                        IsFirst6HourFromWorkEndTime = DateBeginRequest >= OTPeriod.Begin1 && DateBeginRequest <= OTPeriod.End1;

                        OTPeriod = new DateTimeIntersec();
                        OTPeriod.Begin1 = DWS.WorkBeginTime > DWS.WorkEndTime ? CurrentDate.Add(DWS.WorkEndTime).AddHours(6) : CurrentDate.AddDays(-1).Add(DWS.WorkEndTime).AddHours(6);
                        OTPeriod.End1 = OTPeriod.Begin1.AddHours(8);
                        OTx1Period.Add(OTPeriod);

                        OTPeriod = new DateTimeIntersec();
                        OTPeriod.Begin1 = DWS.WorkBeginTime > DWS.WorkEndTime ? CurrentDate.Add(DWS.WorkEndTime).AddHours(6).AddHours(8) : CurrentDate.AddDays(-1).Add(DWS.WorkEndTime).AddHours(6).AddHours(8);
                        OTPeriod.End1 = CurrentDate.AddDays(1);
                        OTx3Period.Add(OTPeriod);
                    }

                    DWS = EmpData.GetDailyWorkSchedule(CurrentDate.AddDays(1), true);
                    if (DWS.IsDayOff || MWS.GetWSCode(CurrentDate.AddDays(1).Day, false) == "1")
                    {
                        // ?-Off-Off
                        otType = otType == CalculateOTType.OOW ? CalculateOTType.OOO : CalculateOTType.WOO;
                        if (otType == CalculateOTType.WOO)
                        {
                            OTPeriod = new DateTimeIntersec();
                            OTPeriod.Begin1 = OTx3Period[0].End1;
                            OTPeriod.End1 = CurrentDate.AddDays(2).AddHours(-6);
                            OTx3Period.Add(OTPeriod);
                        }
                        //else if (otType == CalculateOTType.OOO)
                        //{
                        //    OTPeriod = new DateTimeIntersec();
                        //    OTPeriod.Begin1 = CurrentDate;
                        //    OTPeriod.End1 = CurrentDate.AddDays(2).AddHours(-6);
                        //    OTx3Period.Add(OTPeriod);
                        //}

                    }
                    else
                    {
                        // ?-Off-Day
                        otType = otType == CalculateOTType.OOW ? CalculateOTType.OOW : CalculateOTType.WOW;
                        if (IsOffToDay = DateBeginRequest > CurrentDate && DateEndRequest <= CurrentDate.AddDays(1).Add(DWS.WorkBeginTime) && !IsFirst6HourFromWorkEndTime)
                        {
                            if (((TimeSpan)(CurrentDate.AddDays(1).Add(DWS.WorkBeginTime).AddHours(-6) - DateBeginRequest)).TotalHours > 8)
                            {
                                if (DateEndRequest > CurrentDate.AddDays(1).Add(DWS.WorkBeginTime).AddHours(-6))
                                {
                                    OTx1 = 8 * 60;
                                    OTx3 = ((TimeSpan)(CurrentDate.AddDays(1).Add(DWS.WorkBeginTime).AddHours(-6) - DateBeginRequest.AddHours(8))).TotalMinutes;
                                    OTx1_5 = ((TimeSpan)(DateEndRequest - CurrentDate.AddDays(1).Add(DWS.WorkBeginTime).AddHours(-6))).TotalMinutes;
                                }
                                else if (((TimeSpan)(DateEndRequest - DateBeginRequest)).TotalHours >= 8)
                                {
                                    OTx1 = 8 * 60;
                                    OTx3 = ((TimeSpan)(DateEndRequest - DateBeginRequest.AddHours(8))).TotalMinutes;
                                }
                                else if (((TimeSpan)(DateEndRequest - DateBeginRequest)).TotalHours < 8)
                                {
                                    OTx1 = ((TimeSpan)(DateEndRequest - DateBeginRequest)).TotalMinutes;
                                }
                            }
                            else if (DateEndRequest > CurrentDate.AddDays(1).Add(DWS.WorkBeginTime).AddHours(-6))
                            {
                                OTx1 = ((TimeSpan)(CurrentDate.AddDays(1).Add(DWS.WorkBeginTime).AddHours(-6) - DateBeginRequest)).TotalMinutes;
                                OTx1_5 = ((TimeSpan)(DateEndRequest - CurrentDate.AddDays(1).Add(DWS.WorkBeginTime).AddHours(-6))).TotalMinutes;
                            }
                            else
                                OTx1 = ((TimeSpan)(DateEndRequest - DateBeginRequest)).TotalMinutes;

                            IsFirst6HourFromWorkEndTime = !IsFirst6HourFromWorkEndTime;
                        }

                        //if (otType == CalculateOTType.WOW)
                        //{
                        //    OTPeriod = new DateTimeIntersec();
                        //    OTPeriod.Begin1 = OTx3Period[0].End1;
                        //    OTPeriod.End1 = CurrentDate.AddDays(1).Add(DWS.WorkBeginTime).AddHours(-6);
                        //    OTx3Period.Add(OTPeriod);

                        //    OTPeriod = new DateTimeIntersec();
                        //    OTPeriod.Begin1 = OTx3Period[0].End1;
                        //    OTPeriod.End1 = CurrentDate.AddDays(1).Add(DWS.WorkBeginTime).AddHours(-6);
                        //    OTx1Period.Add(OTPeriod);
                        //}
                        //else if (otType == CalculateOTType.OOW)
                        //{
                        //    OTPeriod = new DateTimeIntersec();
                        //    OTPeriod.Begin1 = CurrentDate;
                        //    OTPeriod.End1 = CurrentDate.AddDays(1).Add(DWS.WorkBeginTime).AddHours(-6);
                        //    OTx3Period.Add(OTPeriod);
                        //}
                    }
                    #endregion
                }

            }
            #endregion

            #region "Match OT"
            if (((EmpData.EmpSubAreaType == EmployeeSubAreaType.Shift8 || EmpData.EmpSubAreaType == EmployeeSubAreaType.Shift12) && !IsFirst6HourFromWorkEndTime))
            {
                TimeSpan tp = DateEndRequest - DateBeginRequest;
                if (tp.TotalHours >= 8)
                {
                    OTx1 = 8 * 60;
                    if (tp.TotalHours > 8)
                        OTx3 = tp.TotalMinutes - 8 * 60;
                }
                else
                    OTx1 = tp.TotalMinutes;

            }
            else if ((EmpData.EmpSubAreaType == EmployeeSubAreaType.Shift8 || EmpData.EmpSubAreaType == EmployeeSubAreaType.Shift12) && IsOffToDay)
            {
                // Do Nothing
            }
            else
            {
                foreach (DateTimeIntersec item in OTx1Period)
                {
                    item.Begin2 = DateBeginRequest;
                    item.End2 = DateEndRequest;
                    if (item.IsIntersecOT())
                    {
                        item.Intersection();
                        OTx1 += ((TimeSpan)(item.EndBound - item.BeginBound)).TotalMinutes;
                        AddTimeEval(item.BeginBound, item.EndBound, "2010", ref Result);
                    }
                }

                foreach (DateTimeIntersec item in OTx15Period)
                {
                    item.Begin2 = DateBeginRequest;
                    item.End2 = DateEndRequest;
                    if (item.IsIntersecOT())
                    {
                        item.Intersection();
                        OTx1_5 += ((TimeSpan)(item.EndBound - item.BeginBound)).TotalMinutes;
                        AddTimeEval(item.BeginBound, item.EndBound, "2015", ref Result);
                    }
                }

                foreach (DateTimeIntersec item in OTx3Period)
                {
                    item.Begin2 = DateBeginRequest;
                    item.End2 = DateEndRequest;
                    if (item.IsIntersecOT())
                    {
                        item.Intersection();
                        OTx3 += ((TimeSpan)(item.EndBound - item.BeginBound)).TotalMinutes;
                        AddTimeEval(item.BeginBound, item.EndBound, "2030", ref Result);
                    }
                }
            }
            #endregion


            if (OTType == 0)
            {
                Result.RequestOTHour10 = Convert.ToDecimal(OTx1);
                Result.RequestOTHour15 = Convert.ToDecimal(OTx1_5);
                Result.RequestOTHour30 = Convert.ToDecimal(OTx3);
            }
            else if (OTType == 1)
            {
                Result.EvaOTHour10 = Convert.ToDecimal(OTx1);
                Result.EvaOTHour15 = Convert.ToDecimal(OTx1_5);
                Result.EvaOTHour30 = Convert.ToDecimal(OTx3);
            }
            else if (OTType == 2)
            {
                Result.FinalOTHour10 = Convert.ToDecimal(OTx1);
                Result.FinalOTHour15 = Convert.ToDecimal(OTx1_5);
                Result.FinalOTHour30 = Convert.ToDecimal(OTx3);
            }
            return Result;
        }

        //AddBy: Ratchatawan W.
        /// <param name="OTType">
        /// OTType == 0 ? DailyOTRequest : OTType == 1 ? MonthlyOTEval : OTType == 2 ? MonthlyOTFinal : false
        /// </param>
        public DailyOTLog CalculateDailyOTNew(string EmployeeID, DailyWS oDailyWS, MonthlyWS oMWS, DateTime oDateBeginRequest, DateTime oDateEndRequest, Byte oOTType, List<DelegateData> lstDelegate)
        {
            DailyOTLog Result = new DailyOTLog();
            EmployeeData oEmployeeData = new EmployeeData(EmployeeID, oDateBeginRequest);

            DateTime WorkBeginDateTime = new DateTime();
            DateTime WorkEndDateTime = new DateTime();
            DateTime OTDate = oDateBeginRequest.Date;
            INFOTYPE0007 oInfotype0007 = new EmployeeData().GetEmployeeWF(EmployeeID, OTDate);
            Double OTx1 = 0, OTx1_5 = 0, OTx3 = 0;
            List<DateTimeIntersec> OTx1Period = new List<DateTimeIntersec>();
            List<DateTimeIntersec> OTx15Period = new List<DateTimeIntersec>();
            List<DateTimeIntersec> OTx3Period = new List<DateTimeIntersec>();
            DateTimeIntersec OTPeriod;

            EmployeeData oDelegateEmployee = null;
            DailyWS oldWS = null;
            bool isShiftDelegateToNorm = false;
            bool isNewBeginOverEnd = false;
            if (lstDelegate.Count > 0)
            {
                oDelegateEmployee = new EmployeeData(lstDelegate[0].EmployeeID, oDateBeginRequest);

                //if (oEmployeeData.EmpSubAreaType <= EmployeeSubAreaType.Norm && oDelegateEmployee.EmpSubAreaType >= EmployeeSubAreaType.Shift8 && OTManagement.AREA_OT_SHIFT_DELEGATETO_NORM_ONABSENCE.Contains(oEmployeeData.OrgAssignment.Area) && OTManagement.AREA_OT_SHIFT_DELEGATETO_NORM_ONABSENCE.Contains(oDelegateEmployee.OrgAssignment.Area))
                if (oEmployeeData.EmpSubAreaType <= EmployeeSubAreaType.Norm && oDelegateEmployee.EmpSubAreaType >= EmployeeSubAreaType.Shift8 && AREA_OT_SHIFT_DELEGATETO_NORM_ONABSENCE.Contains(oEmployeeData.OrgAssignment.Area) && AREA_OT_SHIFT_DELEGATETO_NORM_ONABSENCE.Contains(oDelegateEmployee.OrgAssignment.Area))
                {
                    isShiftDelegateToNorm = true;
                    oldWS = oEmployeeData.GetDailyWorkSchedule(OTDate, false);
                    isNewBeginOverEnd = oldWS.WorkEndTime < oDailyWS.WorkBeginTime;
                }
            }

            //2013-01-18 �ҡ��� User �繾�ѡ�ҹ�� ��Тͤ���ͷա�͹���ҷӧҹ �������ҷӧҹ�ͧ�ѹ��͹˹���ҤԴ�ͷ���ѧ��ԡ�ҹ
            if (oInfotype0007.WFRule.StartsWith("S") && oDateBeginRequest.TimeOfDay < oDailyWS.WorkBeginTime)
            {
                OTDate = oDateBeginRequest.AddDays(-1).Date;
                EmployeeData oEmp = new EmployeeData(EmployeeID, OTDate);
                oDailyWS = oEmp.GetDailyWorkSchedule(OTDate);
                oInfotype0007 = new EmployeeData().GetEmployeeWF(EmployeeID, OTDate);
            }

            Result.EmployeeID = EmployeeID;
            Result.oTimeEval = new List<TimeEval>();

            #region �觪�ǧ���ҷ����ӹǳ�ͷ� ������͹䢵�ҧ�
            //�������� DayOff/Holiday
            if (!oDailyWS.IsDayOff && oMWS.GetWSCode(OTDate.Day, false) != "1")
            {
                //����繾�ѡ�ҹẺ Flex �е�ͧ�����仵��������ǧ���ҡ�÷ӧҹ�˹
                if (oEmployeeData.EmpSubAreaType == EmployeeSubAreaType.Flex)
                {
                    WorkBeginDateTime = new DateTime();
                    WorkEndDateTime = new DateTime();
                    //HRTMManagement.GetWorkingTimeOfFlex(OTDate, oInfotype0007, ref WorkBeginDateTime, ref WorkEndDateTime);

                    HRTMManagement.CreateInstance(CompanyCode).GetWorkingTimeOfFlex(OTDate, oInfotype0007, ref WorkBeginDateTime, ref WorkEndDateTime);
                }
                //else if (isShiftDelegateToNorm)
                //{
                //    WorkBeginDateTime = OTDate.Add(oDelegateWS.WorkBeginTime);
                //    WorkEndDateTime = oDelegateWS.WorkEndTime < oDelegateWS.WorkBeginTime ? OTDate.Add(oDelegateWS.WorkEndTime).AddDays(1) : OTDate.Add(oDelegateWS.WorkEndTime);
                //}
                //����繾�ѡ�ҹ Norm ��� Shift ��� Set ������ҷӧҹ���Ԣͧ�ѹ���
                else
                {
                    WorkBeginDateTime = OTDate.Add(oDailyWS.WorkBeginTime);
                    WorkEndDateTime = oDailyWS.WorkEndTime < oDailyWS.WorkBeginTime ? OTDate.Add(oDailyWS.WorkEndTime).AddDays(1) : OTDate.Add(oDailyWS.WorkEndTime);
                }

                //����Ѻ��ѡ�ҹ������ Norm/Flex
                //��͹�����ѧ���ҷӧҹ ���� 1.5 ���
                if (!isShiftDelegateToNorm && (oEmployeeData.EmpSubAreaType == EmployeeSubAreaType.Norm || oEmployeeData.EmpSubAreaType == EmployeeSubAreaType.Flex))
                {
                    //��͹���ҷӧҹ
                    OTPeriod = new DateTimeIntersec();
                    OTPeriod.Begin1 = oDateBeginRequest;
                    OTPeriod.End1 = WorkBeginDateTime;
                    OTx15Period.Add(OTPeriod);

                    //��ѧ���ҷӧҹ
                    OTPeriod = new DateTimeIntersec();
                    OTPeriod.Begin1 = WorkEndDateTime;
                    OTPeriod.End1 = oDateEndRequest;
                    OTx15Period.Add(OTPeriod);
                }
                else if (isShiftDelegateToNorm)
                {
                    if (isNewBeginOverEnd)
                    {
                        //��͹���ҷӧҹ 价Ѻ��͹�Ѻ���ҷӧҹ��� ��ͧ�觪�ǧ�ѡ���§�͡
                        TimeSpan Break1200 = new TimeSpan(12, 0, 0);
                        TimeSpan Break1300 = new TimeSpan(13, 0, 0);

                        OTPeriod = new DateTimeIntersec();
                        OTPeriod.Begin1 = oDateBeginRequest;
                        OTPeriod.End1 = oDateBeginRequest.Date.Add(Break1200);
                        OTx15Period.Add(OTPeriod);

                        OTPeriod = new DateTimeIntersec();
                        OTPeriod.Begin1 = oDateBeginRequest.Date.Add(Break1300);
                        OTPeriod.End1 = WorkBeginDateTime;
                        OTx15Period.Add(OTPeriod);
                    }
                    else
                    {
                        //��͹���ҷӧҹ
                        OTPeriod = new DateTimeIntersec();
                        OTPeriod.Begin1 = oDateBeginRequest;
                        OTPeriod.End1 = WorkBeginDateTime;
                        OTx15Period.Add(OTPeriod);
                    }

                    //��ѧ���ҷӧҹ
                    OTPeriod = new DateTimeIntersec();
                    OTPeriod.Begin1 = WorkEndDateTime;
                    OTPeriod.End1 = oDateEndRequest;
                    OTx15Period.Add(OTPeriod);
                }
                //����Ѻ��ѡ�ҹ������ Shift12
                //��ѧ���ҷӧҹ 1.5 ���
                else
                {
                    //��ѧ���ҷӧҹ
                    OTPeriod = new DateTimeIntersec();
                    OTPeriod.Begin1 = WorkEndDateTime;
                    OTPeriod.End1 = oDateEndRequest;
                    OTx15Period.Add(OTPeriod);
                }
            }
            //������ѹ DayOff/Holiday
            else
            {
                //����繾�ѡ�ҹ Norm 
                //��� Set ���ҷӧҹ�� ���ҷӧҹ�ͧ�ѹ WorkingDay ��͹˹�ҷ��������ش
                if (!isShiftDelegateToNorm && oEmployeeData.EmpSubAreaType == EmployeeSubAreaType.Norm)
                {
                    DateTime tmpDate = OTDate;
                    DailyWS tmpDWS = oEmployeeData.GetDailyWorkSchedule(tmpDate, true);
                    MonthlyWS tmpMWS = MonthlyWS.GetCalendar(EmployeeID, tmpDate.Year, tmpDate.Month);

                    while (tmpDWS.IsDayOff || tmpMWS.GetWSCode(tmpDate.Day, false) == "1")
                    {
                        tmpDate = tmpDate.AddDays(-1);
                        if (tmpMWS.WS_Year != tmpDate.Year || tmpMWS.WS_Month != tmpDate.Month)
                            tmpMWS = MonthlyWS.GetCalendar(EmployeeID, tmpDate);
                        EmployeeData tmpEmployee = new EmployeeData(oEmployeeData.EmployeeID, tmpDate);
                        tmpDWS = tmpEmployee.GetDailyWorkSchedule(tmpDate, true);
                        tmpEmployee = null;
                    }

                    if (tmpDate < oInfotype0007.BeginDate)
                    {
                        tmpDate = OTDate.AddDays(1);
                        tmpDWS = oEmployeeData.GetDailyWorkSchedule(tmpDate, true);
                        tmpMWS = MonthlyWS.GetCalendar(EmployeeID, tmpDate.Year, tmpDate.Month);
                        while (tmpDWS.IsDayOff || tmpMWS.GetWSCode(tmpDate.Day, false) == "1")
                        {
                            tmpDate = tmpDate.AddDays(1);
                            if (tmpMWS.WS_Year != tmpDate.Year || tmpMWS.WS_Month != tmpDate.Month)
                                tmpMWS = MonthlyWS.GetCalendar(EmployeeID, tmpDate);
                            EmployeeData tmpEmployee = new EmployeeData(oEmployeeData.EmployeeID, tmpDate);
                            tmpDWS = tmpEmployee.GetDailyWorkSchedule(tmpDate, true);
                            tmpEmployee = null;
                        }
                    }

                    WorkBeginDateTime = OTDate.Add(tmpDWS.WorkBeginTime);
                    WorkEndDateTime = tmpDWS.WorkEndTime < tmpDWS.WorkBeginTime ? OTDate.Add(tmpDWS.WorkEndTime).AddDays(1) : OTDate.Add(tmpDWS.WorkEndTime);
                }
                //����繾�ѡ�ҹ Flex Set ���ҷӧҹ��ѹ ����� 8:30-17:30
                if (oEmployeeData.EmpSubAreaType == EmployeeSubAreaType.Flex)
                {
                    WorkBeginDateTime = OTDate.Add(new TimeSpan(8, 00, 0));
                    WorkEndDateTime = OTDate.Add(new TimeSpan(17, 00, 0));
                }


                //����繾�ѡ�ҹ Norm/Flex
                //��͹���ҷӧҹ�� 3 ���/����ҷӧҹ�� 1 ���/��ѧ���ҷӧҹ�� 3 ���
                if (!isShiftDelegateToNorm && (oEmployeeData.EmpSubAreaType == EmployeeSubAreaType.Norm || oEmployeeData.EmpSubAreaType == EmployeeSubAreaType.Flex))
                {
                    //��͹���ҷӧҹ
                    if (oDateBeginRequest < WorkBeginDateTime)
                    {
                        OTPeriod = new DateTimeIntersec();
                        OTPeriod.Begin1 = oDateBeginRequest;
                        OTPeriod.End1 = WorkBeginDateTime;
                        OTx3Period.Add(OTPeriod);
                    }

                    //����ҷӧҹ
                    if (WorkBeginDateTime <= oDateEndRequest && WorkEndDateTime >= oDateBeginRequest)
                    {
                        TimeSpan Break1200 = new TimeSpan(12, 0, 0);
                        TimeSpan Break1300 = new TimeSpan(13, 0, 0);
                        OTPeriod = new DateTimeIntersec();

                        if (oDateBeginRequest >= WorkBeginDateTime)
                            OTPeriod.Begin1 = oDateBeginRequest;
                        else
                            OTPeriod.Begin1 = WorkBeginDateTime;

                        //������ҷ��͡�͹���§�������ش��ѧ���§ ������͡�� 2 ��ǧ
                        if (OTPeriod.Begin1 <= OTPeriod.Begin1.Date.Add(Break1200) && oDateEndRequest > OTPeriod.Begin1.Date.Add(Break1200))
                        {
                            //������ҷ�����ҡѺ���§�ʹ� ���Ѵ��ǧ��� 1 ���
                            if (OTPeriod.Begin1 < OTPeriod.Begin1.Date.Add(Break1200))
                            {
                                //��ǧ��� 1 Begin   : oDateBeginRequest/WorkBeginDateTime
                                //      End     : Break1200
                                OTPeriod.End1 = oDateBeginRequest.Date.Add(Break1200);
                                OTx1Period.Add(OTPeriod);
                            }

                            //��ǧ��� 2 Begin   : Break1300
                            //      End     : oDateEndRequest/WorkEndDateTime
                            OTPeriod = new DateTimeIntersec();
                            OTPeriod.Begin1 = oDateBeginRequest.Date.Add(Break1300);
                            if (oDateEndRequest <= WorkEndDateTime)
                                OTPeriod.End1 = oDateEndRequest;
                            else
                                OTPeriod.End1 = WorkEndDateTime;
                            OTx1Period.Add(OTPeriod);
                        }
                        //������ҷ��͡�͹���§ ������ش��͹������ҡѺ���§ �����ӹǳ�����ԧ
                        else
                        {
                            if (oDateEndRequest <= WorkEndDateTime)
                                OTPeriod.End1 = oDateEndRequest;
                            else
                                OTPeriod.End1 = WorkEndDateTime;
                            OTx1Period.Add(OTPeriod);
                        }
                    }

                    //��ѧ���ҷӧҹ
                    if (oDateEndRequest > WorkBeginDateTime)
                    {
                        OTPeriod = new DateTimeIntersec();
                        OTPeriod.Begin1 = WorkEndDateTime;
                        OTPeriod.End1 = oDateEndRequest;
                        OTx3Period.Add(OTPeriod);
                    }
                }
                //����繾�ѡ�ҹ Shift12
                //8 ��������á�� 1 ��� ��ѧ�ҡ��� 3 ���
                else
                {

                    //�ҡ�ӧҹ�ҡ���� 8 �������
                    TimeSpan tp = oDateEndRequest - oDateBeginRequest;
                    if (tp.TotalHours > 8)
                    {
                        //8 ��������á �� 1 ���
                        OTPeriod = new DateTimeIntersec();
                        OTPeriod.Begin1 = oDateBeginRequest;
                        OTPeriod.End1 = oDateBeginRequest.Add(new TimeSpan(8, 0, 0));
                        OTx1Period.Add(OTPeriod);

                        //��ѧ�ҡ����� 3 ���
                        OTPeriod = new DateTimeIntersec();
                        OTPeriod.Begin1 = oDateBeginRequest.Add(new TimeSpan(8, 0, 1));
                        OTPeriod.End1 = oDateEndRequest;
                        OTx3Period.Add(OTPeriod);
                    }
                    //�ҡ�ӧҹ���¡���������ҡѺ 8 �������
                    else
                    {
                        OTPeriod = new DateTimeIntersec();
                        OTPeriod.Begin1 = oDateBeginRequest;
                        OTPeriod.End1 = oDateEndRequest;
                        OTx1Period.Add(OTPeriod);
                    }

                }
            }
            #endregion

            #region �ӹǳ�ӹǹ��������ͷ�
            foreach (DateTimeIntersec item in OTx1Period)
            {
                item.Begin2 = oDateBeginRequest;
                item.End2 = oDateEndRequest;
                if (item.IsIntersecOT())
                {
                    item.Intersection();
                    OTx1 += ((TimeSpan)(item.EndBound - item.BeginBound)).TotalMinutes;
                    AddTimeEval(item.BeginBound, item.EndBound, "2010", ref Result);
                }
            }

            foreach (DateTimeIntersec item in OTx15Period)
            {
                item.Begin2 = oDateBeginRequest;
                item.End2 = oDateEndRequest;
                if (item.IsIntersecOT())
                {
                    item.Intersection();
                    OTx1_5 += ((TimeSpan)(item.EndBound - item.BeginBound)).TotalMinutes;
                    AddTimeEval(item.BeginBound, item.EndBound, "2015", ref Result);
                }
            }

            foreach (DateTimeIntersec item in OTx3Period)
            {
                item.Begin2 = oDateBeginRequest;
                item.End2 = oDateEndRequest;
                if (item.IsIntersecOT())
                {
                    item.Intersection();
                    OTx3 += ((TimeSpan)(item.EndBound - item.BeginBound)).TotalMinutes;
                    AddTimeEval(item.BeginBound, item.EndBound, "2030", ref Result);
                }
            }
            #endregion

            //����� DailyOTRequest
            if (oOTType == 0)
            {
                Result.RequestOTHour10 = Convert.ToDecimal(OTx1);
                Result.RequestOTHour15 = Convert.ToDecimal(OTx1_5);
                Result.RequestOTHour30 = Convert.ToDecimal(OTx3);
            }
            //����� MonthlyOTEval
            else if (oOTType == 1)
            {
                Result.EvaOTHour10 = Convert.ToDecimal(OTx1);
                Result.EvaOTHour15 = Convert.ToDecimal(OTx1_5);
                Result.EvaOTHour30 = Convert.ToDecimal(OTx3);
            }
            //����� MonthlyOTFinal
            else if (oOTType == 2)
            {
                Result.FinalOTHour10 = Convert.ToDecimal(OTx1);
                Result.FinalOTHour15 = Convert.ToDecimal(OTx1_5);
                Result.FinalOTHour30 = Convert.ToDecimal(OTx3);
            }

            Result.BeginDate = oDateBeginRequest.Date;
            Result.EndDate = oDateBeginRequest.Date;

            return Result;
        }

        enum CalculateOTType
        {
            WWW,
            WWO,
            WOO,
            WOW,
            OOO,
            OOW,
            OWW,
            OWO
        }

        public void AddTimeEval(DateTime BeginDate, DateTime EndDate, String WageType, ref DailyOTLog _DailyOTLog)
        {
            if (EndDate - BeginDate <= TimeSpan.MinValue
                || EndDate - BeginDate <= new TimeSpan() || BeginDate == EndDate) return;
            TimeEval oTimeEval = new TimeEval();
            oTimeEval.EmployeeID = _DailyOTLog.EmployeeID;
            oTimeEval.BeginDate = BeginDate;
            oTimeEval.EndDate = EndDate;
            oTimeEval.WageHour = Convert.ToDecimal(((TimeSpan)(EndDate - BeginDate)).TotalHours);
            oTimeEval.WageType = WageType;
            _DailyOTLog.oTimeEval.Add(oTimeEval);
        }

        DailyWS PreviousWorkingDay(String EmpID, DailyWS _DailyWS, DateTime CurrentDate, out DateTime DailyWSNormalBeginTime, out DateTime DailyWSNormalEndTime)
        {
            DailyWSNormalBeginTime = DateTime.MinValue;
            DailyWSNormalEndTime = DateTime.MinValue;

            EmployeeData _EmployeeData = new EmployeeData().GetUserSetting(EmpID).Employee;
            for (DateTime iDate = CurrentDate; iDate > DateTime.MinValue; iDate = iDate.AddDays(-1))
            {
                _DailyWS = _EmployeeData.GetDailyWorkSchedule(iDate);
                if (_DailyWS.IsDayOff || MonthlyWS.GetCalendar(EmpID, CurrentDate).GetWSCode(CurrentDate.Day, false) == "1") continue;
                DailyWSNormalBeginTime = _DailyWS.BeginDate.Add(_DailyWS.WorkBeginTime);
                DailyWSNormalEndTime = _DailyWS.BeginDate.Add(_DailyWS.WorkEndTime);
                return _DailyWS;
            }
            return null;
        }

        DailyWS PreviousWorkingDay(String EmpID, DailyWS _DailyWS, DateTime CurrentDate)
        {
            DateTime DailyWSNormalBeginTime;
            DateTime DailyWSNormalEndTime;
            return PreviousWorkingDay(EmpID, _DailyWS, CurrentDate, out DailyWSNormalBeginTime, out DailyWSNormalEndTime);
        }

        void MatchingOT(String EmpID, MonthlyWS MWS, DailyWS _DailyWS, DateTime BeginDate, DateTime EndDate, DateTime EvalBegin, DateTime EvalEnd, DailyOTLog oDailyOTLog)
        {
            OTClock otClock = new OTClock();
            DailyOTLog tmpDailyOTLog = new DailyOTLog();
            DateTimeIntersec _DateTimeIntersec = new DateTimeIntersec();
            _DateTimeIntersec.Begin1 = BeginDate;
            _DateTimeIntersec.End1 = EndDate;
            _DateTimeIntersec.Begin2 = EvalBegin;
            _DateTimeIntersec.End2 = EvalEnd;

            if (_DateTimeIntersec.IsIntersecOT())
            {
                otClock.MachineBegin = EvalBegin;
                otClock.MachineEnd = EvalEnd;

                // use for job6 retrive timeEval from SAP else Plase Comment!!!!! Kiattiwat 20-10-2011
                //tmpDailyOTLog = CalculateDailyOT(EmpID, _DailyWS, MWS, EvalBegin, EvalEnd, 1);
                //oDailyOTLog.EvaOTHour10 += tmpDailyOTLog.EvaOTHour10;
                //oDailyOTLog.EvaOTHour15 += tmpDailyOTLog.EvaOTHour15;
                //oDailyOTLog.EvaOTHour30 += tmpDailyOTLog.EvaOTHour30;

                _DateTimeIntersec.Intersection();
                if (_DateTimeIntersec.BeginBound != _DateTimeIntersec.EndBound)
                {
                    tmpDailyOTLog = CalculateDailyOT(EmpID, _DailyWS, MWS, _DateTimeIntersec.BeginBound, _DateTimeIntersec.EndBound, 2);
                    oDailyOTLog.FinalOTHour10 += tmpDailyOTLog.FinalOTHour10;
                    oDailyOTLog.FinalOTHour15 += tmpDailyOTLog.FinalOTHour15;
                    oDailyOTLog.FinalOTHour30 += tmpDailyOTLog.FinalOTHour30;

                    otClock.IntersecBegin = _DateTimeIntersec.BeginBound;
                    otClock.IntersecEnd = _DateTimeIntersec.EndBound;
                    oDailyOTLog.OTClockINOUT.Add(otClock);
                }
            }

        }

        public void MatchingOT(String EmpID, List<TimePairArchive> oTimePairArchive, DateTime BeginDate, DateTime EndDate, DailyOTLog oDailyOTLog)
        {
            MonthlyWS MWS = MonthlyWS.GetCalendar(EmpID, BeginDate, BeginDate.Year, BeginDate.Month, true);
            EmployeeData EmpData = new EmployeeData().GetUserSetting(EmpID).Employee;

            DailyWS _DailyWS = EmpData.GetDailyWorkSchedule(BeginDate, true);
            DateTime WorkBegin, WorkEnd;
            foreach (TimePairArchive timePairArchive in oTimePairArchive)
            {
                _DailyWS = EmpData.GetDailyWorkSchedule(timePairArchive.Date, true);
                if (_DailyWS.IsDayOff || MWS.GetWSCode(timePairArchive.Date.Day, false) == "1")
                {
                    WorkBegin = timePairArchive.Date.Add(new TimeSpan(8, 0, 0));
                    WorkEnd = timePairArchive.Date.Add(new TimeSpan(17, 0, 0));
                }
                else
                {
                    WorkBegin = timePairArchive.Date.Add(_DailyWS.WorkBeginTime);
                    WorkEnd = _DailyWS.WorkBeginTime > _DailyWS.WorkEndTime ? timePairArchive.Date.AddDays(1).Add(_DailyWS.WorkEndTime) : timePairArchive.Date.Add(_DailyWS.WorkEndTime);
                }

                MatchingOT(EmpID, MWS, _DailyWS, WorkBegin, WorkEnd, timePairArchive.ClockIN, timePairArchive.ClockOUT, oDailyOTLog);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EmpID"></param>
        /// <param name="oTimeEval"></param>
        /// <param name="BeginDate">Request OT BeginDate</param>
        /// <param name="EndDate">Request OT  EndDate</param>
        /// <param name="oDailyOTLog"></param>
        public void MatchingOT(String EmpID, List<TimeEval> oTimeEval, DateTime BeginDate, DateTime EndDate, DailyOTLog oDailyOTLog)
        {
            MonthlyWS MWS = MonthlyWS.GetCalendar(EmpID, BeginDate, BeginDate.Year, BeginDate.Month, true);
            EmployeeData EmpData = new EmployeeData().GetUserSetting(EmpID).Employee;
            DailyWS _DailyWS = EmpData.GetDailyWorkSchedule(BeginDate, true);
            foreach (TimeEval timeEval in oTimeEval)
                MatchingOT(EmpID, MWS, _DailyWS, BeginDate, EndDate, timeEval.BeginDate, timeEval.EndDate, oDailyOTLog);

        }

        public void MatchingOT(List<TimeEval> oTimeEval, DateTime BeginDate, DateTime EndDate, DailyOTLog oDailyOTLog)
        {
            DateTimeIntersec oDateTimeIntersec = new DateTimeIntersec();
            oDateTimeIntersec.Begin1 = BeginDate;
            oDateTimeIntersec.End1 = EndDate;

            foreach (TimeEval timeEval in oTimeEval)
            {
                oDateTimeIntersec.Begin2 = timeEval.BeginDate;
                oDateTimeIntersec.End2 = timeEval.EndDate;
                if (oDateTimeIntersec.IsIntersecOT())
                {
                    oDateTimeIntersec.Intersection();
                    switch (timeEval.WageType)
                    {
                        case "2010":
                            oDailyOTLog.FinalOTHour10 += Convert.ToDecimal(((TimeSpan)(oDateTimeIntersec.EndBound - oDateTimeIntersec.BeginBound)).TotalMinutes);
                            break;
                        case "2015":
                            oDailyOTLog.FinalOTHour15 += Convert.ToDecimal(((TimeSpan)(oDateTimeIntersec.EndBound - oDateTimeIntersec.BeginBound)).TotalMinutes);
                            break;
                        case "2030":
                            oDailyOTLog.FinalOTHour30 += Convert.ToDecimal(((TimeSpan)(oDateTimeIntersec.EndBound - oDateTimeIntersec.BeginBound)).TotalMinutes);
                            break;
                    }
                }
            }
        }

        public void MatchingOT(String EmpID, DateTime BeginDate, DateTime EndDate, DailyOTLog oDailyOTLog)
        {
            if (oDailyOTLog.EvaTimePairs == null) return;
            MonthlyWS MWS = MonthlyWS.GetCalendar(EmpID, BeginDate, BeginDate.Year, BeginDate.Month, true);
            EmployeeData EmpData = new EmployeeData().GetUserSetting(EmpID).Employee;
            DailyWS _DailyWS = EmpData.GetDailyWorkSchedule(BeginDate, true);
            DateTime EvalBegin, EvalEnd;
            foreach (String strEvalPair in oDailyOTLog.EvaTimePairs.Split('|'))
            {
                if (DateTime.TryParseExact(strEvalPair.Split('-')[0], "yyyyMMdd HHmm", new CultureInfo("en-US"), DateTimeStyles.None, out EvalBegin) &&
                    DateTime.TryParseExact(strEvalPair.Split('-')[1], "yyyyMMdd HHmm", new CultureInfo("en-US"), DateTimeStyles.None, out EvalEnd))
                    MatchingOT(EmpID, MWS, _DailyWS, BeginDate, EndDate, EvalBegin, EvalEnd, oDailyOTLog);
            }
        }

        public List<TimeEval> SimulateTimeEval(DateTime beginDate, DateTime endDate)
        {
            List<TimePairArchive> oTimePairArchive = LoadTimePairArchive(beginDate, endDate);

            MonthlyWS MWS;
            EmployeeData EmpData;
            DateTime CurrentDate;
            DailyWS DWS;
            DailyOTLog DOTLog;
            List<TimeEval> TimeEvalList = new List<TimeEval>();
            for (int i = beginDate.Month; i <= endDate.Month; i++)
            {
                CurrentDate = new DateTime(beginDate.AddMonths(-beginDate.Month).AddMonths(i).Year, beginDate.AddMonths(-beginDate.Month).AddMonths(i).Month, 1);
                foreach (TimePairArchive item in oTimePairArchive)
                {
                    if (item.ClockIN == null || item.ClockOUT == null
                        || item.ClockIN == DateTime.MinValue || item.ClockOUT == DateTime.MinValue
                        || item.ClockIN == item.ClockOUT) continue;
                    MWS = MonthlyWS.GetCalendar(item.EmployeeID, CurrentDate, true);
                    EmpData = new EmployeeData(item.EmployeeID);
                    DWS = EmpData.GetDailyWorkSchedule(item.Date, true);
                    DOTLog = CalculateDailyOT(item.EmployeeID, DWS, MWS, item.ClockIN, item.ClockOUT, 9);
                    foreach (TimeEval iTimeEval in DOTLog.oTimeEval)
                        if (!String.IsNullOrEmpty(iTimeEval.WageType))
                            TimeEvalList.Add(iTimeEval);
                }
            }
            return TimeEvalList;
        }

        double CalculateBreakCode(string BreakCode, DateTime DateBeginRequest, DateTime DateEndRequest, DateTime BeginDate)
        {
            if (BreakCode.ToUpper() == "NOON")
            {
                DateTime BreakStart = BeginDate.AddHours(-BeginDate.Hour + 4);
                DateTime BreakFinish = BeginDate.AddHours(-BeginDate.Hour + 5);

                if (DateBeginRequest < BreakStart && BreakFinish < DateEndRequest)
                    return 60;
                else if (DateBeginRequest < BreakStart && DateEndRequest > BreakStart)
                    return 60;
                else if (DateBeginRequest < BreakFinish && DateEndRequest > BreakFinish)
                    return 60;
                else if (DateBeginRequest > BreakStart && DateEndRequest < BreakFinish)
                    return 60;
                else
                    return 0;
            }
            return 0;
        }

        public List<MonthlyOT> CalculateMonthlyOT(List<MonthlyOT> listData, MonthlyWS MWS)
        {
            List<MonthlyOT> list = new List<MonthlyOT>();
            Dictionary<int, Decimal> additionalTNOM = new Dictionary<int, decimal>();
            Dictionary<int, Decimal> otTNOM = new Dictionary<int, decimal>();
            List<int> isLoadTNOM = new List<int>();
            for (int index = 0; index < listData.Count; index++)
            {
                MonthlyOT item = listData[index];
                TimeSpan time1, time2;
                time1 = item.DailyOT.BeginTime;
                time2 = item.DailyOT.EndTime;
                if (time2 <= time1)
                {
                    time2 = time2.Add(new TimeSpan(1, 0, 0, 0));
                }
                bool lEnd = false;
                do
                {
                    item.DailyOT.Calculate(MWS);
                    item.BeginTime = item.DailyOT.BeginTime;
                    item.EndTime = item.DailyOT.EndTime;
                    if (item.DailyOT.IsTNOM)
                    {
                        #region " ONLY TNOM "
                        if (!additionalTNOM.ContainsKey(item.DailyOT.OTDate.Day))
                        {
                            additionalTNOM.Add(item.DailyOT.OTDate.Day, 0);
                        }
                        if (!otTNOM.ContainsKey(item.DailyOT.OTDate.Day))
                        {
                            otTNOM.Add(item.DailyOT.OTDate.Day, 0);
                        }
                        if (!isLoadTNOM.Contains(item.DailyOT.OTDate.Day))
                        {
                            List<DailyOT> otList = LoadDailyOT(item.DailyOT.EmployeeID, MWS, item.DailyOT.OTDate);
                            additionalTNOM[item.DailyOT.OTDate.Day] += Summary(otList, item.DailyOT);
                            isLoadTNOM.Add(item.DailyOT.OTDate.Day);
                        }
                        additionalTNOM[item.DailyOT.OTDate.Day] += item.DailyOT.OTHours;
                        if (item.DailyOT.DWS.IsDayOff || MWS.GetWSCode(item.DailyOT.OTDate.Day, false) != "")
                        {
                            item.DailyOT.OTHours = additionalTNOM[item.DailyOT.OTDate.Day] - otTNOM[item.DailyOT.OTDate.Day];
                        }
                        else if (additionalTNOM[item.DailyOT.OTDate.Day] > 9)
                        {
                            item.DailyOT.OTHours = additionalTNOM[item.DailyOT.OTDate.Day] - 9 - otTNOM[item.DailyOT.OTDate.Day];
                        }
                        else
                        {
                            item.DailyOT.OTHours = 0.0M;
                        }
                        otTNOM[item.DailyOT.OTDate.Day] += item.DailyOT.OTHours;

                        list.Add(item);
                        #endregion
                    }
                    else
                    {
                        //if (item.OTHours > 0)
                        //{
                        //    list.Add(item);
                        //}

                        if (item.DailyOT.BeginTime.Days > 0)
                        {
                            item.DailyOT.OTDate = item.DailyOT.OTDate.AddDays(item.DailyOT.BeginTime.Days);
                            item.DailyOT.BeginTime = new TimeSpan(item.DailyOT.BeginTime.Hours, item.DailyOT.BeginTime.Minutes, item.DailyOT.BeginTime.Seconds);
                            item.DailyOT.EndTime = new TimeSpan(item.DailyOT.EndTime.Hours, item.DailyOT.EndTime.Minutes, item.DailyOT.EndTime.Seconds);
                            time2 = new TimeSpan(time2.Hours, time2.Minutes, time2.Seconds);
                            continue;
                        }
                        list.Add(item);
                    }
                    if (item.DailyOT.EndTime >= time2)
                    {
                        break;
                    }
                    else
                    {
                        MonthlyOT mot = new MonthlyOT();
                        DailyOT buffer = new DailyOT();
                        buffer.Description = item.DailyOT.Description;
                        buffer.EmployeeID = item.DailyOT.EmployeeID;
                        buffer.OTItemTypeID = item.DailyOT.OTItemTypeID;
                        buffer.IONumber = item.DailyOT.IONumber;
                        buffer.ActivityCode = item.DailyOT.ActivityCode;
                        if (item.DailyOT.EndTime.Days > 0)
                        {
                            time2 = new TimeSpan(time2.Hours, time2.Minutes, time2.Seconds);
                            buffer.BeginTime = new TimeSpan(item.DailyOT.EndTime.Hours, item.DailyOT.EndTime.Minutes, item.DailyOT.EndTime.Seconds);
                            buffer.EndTime = time2;
                            buffer.OTDate = item.DailyOT.OTDate.AddDays(item.DailyOT.EndTime.Days);
                            if (buffer.OTDate.Month != MWS.WS_Month || buffer.OTDate.Year != MWS.WS_Year)
                            {
                                MWS = MonthlyWS.GetCalendar(buffer.EmployeeID, buffer.OTDate.Year, buffer.OTDate.Month);
                            }
                        }
                        else
                        {
                            buffer.BeginTime = item.DailyOT.EndTime;
                            buffer.EndTime = time2;
                            buffer.OTDate = item.DailyOT.OTDate;
                        }
                        mot.BeginTime = buffer.BeginTime;
                        mot.DailyAssignFrom = item.DailyAssignFrom;
                        mot.DailyOT = buffer;
                        mot.DailyRequestNo = item.DailyRequestNo;
                        mot.Description = item.Description;
                        mot.EndTime = buffer.EndTime;
                        item = mot;
                    }
                } while (!lEnd);
            }
            return list;
        }

        private decimal Summary(List<DailyOT> otList, DailyOT current)
        {
            Decimal oReturn = 0.0M;
            foreach (DailyOT item in otList)
            {
                if (item.Equals(current))
                {
                    continue;
                }
                if (item.OTDate != current.OTDate)
                {
                    continue;
                }
                oReturn += item.OTHours;
            }
            return oReturn;
        }

        public PlannedOT LoadFirstPlannedOT(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            return LoadFirstPlannedOT(EmployeeID, BeginDate, EndDate);
        }

        public List<PlannedOT> LoadPlannedOT(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            return LoadPlannedOT(EmployeeID, BeginDate, EndDate);
        }

        public List<DailyOT> LoadDailyOT(string EmployeeID, string Period, MonthlyWS MWS)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            List<DailyOT> oReturn = new List<DailyOT>();
            List<SaveOTData> otlist;
            DateTime oPeriod = DateTime.ParseExact(Period, "yyyyMM", oCL);
            //otlist =  HR.TM.ServiceManager.HRTMBuffer.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", true, true);

            otlist = ServiceManager.CreateInstance(CompanyCode).ESSData.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", true, true);
            List<SaveOTData> buffer = new List<SaveOTData>();
            List<string> requestNoList = new List<string>();
            List<DailyOT> temp = new List<DailyOT>();
            foreach (SaveOTData item in otlist)
            {
                if (!requestNoList.Contains(item.RequestNo))
                {
                    requestNoList.Add(item.RequestNo);
                }
                if (item.OTPeriod != Period)
                {
                    continue;
                }
                DailyOT dailyOT = new DailyOT();
                dailyOT.BeginTime = item.BeginTime;
                dailyOT.EmployeeID = EmployeeID;
                dailyOT.EndTime = item.EndTime;
                dailyOT.OTDate = item.OTDate;
                dailyOT.IsPosted = false;
                dailyOT.Description = item.Description;
                dailyOT.OTItemTypeID = item.OTItemTypeID;
                dailyOT.IONumber = item.IONumber;
                dailyOT.ActivityCode = item.ActivityCode;
                temp.Add(dailyOT);
            }
            oReturn = CalculateDailyOT(temp, MWS);
            return oReturn;
        }

        public List<DailyOT> LoadDailyOT1(string EmployeeID, string Period)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            List<DailyOT> oReturn = new List<DailyOT>();
            List<SaveOTData> otlist;
            DateTime oPeriod = DateTime.ParseExact(Period, "yyyyMM", oCL);
            //otlist = HR.TM.ServiceManager.HRTMBuffer.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", true, true);

            otlist = ServiceManager.CreateInstance(CompanyCode).ESSData.LoadOTData(EmployeeID, oPeriod, oPeriod.AddMonths(1), "D", true, true);
            List<SaveOTData> buffer = new List<SaveOTData>();
            List<string> requestNoList = new List<string>();
            foreach (SaveOTData item in otlist)
            {
                if (!requestNoList.Contains(item.RequestNo))
                {
                    requestNoList.Add(item.RequestNo);
                }
                if (item.OTPeriod != Period)
                {
                    continue;
                }
                DailyOT dailyOT = new DailyOT();
                dailyOT.BeginTime = item.BeginTime;
                dailyOT.EmployeeID = item.EmployeeID;
                dailyOT.EndTime = item.EndTime;
                dailyOT.OTDate = item.OTDate;
                dailyOT.OTHours = item.OTHours;
                dailyOT.IsPosted = false;
                dailyOT.Description = item.Description;
                dailyOT.OTItemTypeID = item.OTItemTypeID;
                dailyOT.IONumber = item.IONumber;
                dailyOT.ActivityCode = item.ActivityCode;
                oReturn.Add(dailyOT);
            }
            return oReturn;
        }

        public List<DailyOT> LoadDailyOT(string EmployeeID, MonthlyWS MWS, DateTime LoadDate)
        {
            return LoadDailyOT(EmployeeID, MWS, LoadDate, LoadDate.AddDays(1));
        }
        public List<DailyOT> LoadDailyOT(string EmployeeID, MonthlyWS MWS, DateTime BeginDate, DateTime EndDate)
        {
            List<DailyOT> oReturn = new List<DailyOT>();
            List<SaveOTData> otlist;
            //otlist = HR.TM.ServiceManager.HRTMBuffer.LoadOTData(EmployeeID, BeginDate, EndDate, "D", true);

            otlist = ServiceManager.CreateInstance(CompanyCode).ESSData.LoadOTData(EmployeeID, BeginDate, EndDate, "D", true);
            foreach (SaveOTData item in otlist)
            {
                DailyOT dailyOT = new DailyOT();
                dailyOT.BeginTime = item.BeginTime;
                dailyOT.EmployeeID = EmployeeID;
                dailyOT.EndTime = item.EndTime;
                dailyOT.OTDate = item.OTDate;
                dailyOT.IsPosted = false;
                dailyOT.IsSummary = item.IsSummary;
                dailyOT.Description = item.Description;
                dailyOT.AssignFrom = item.AssignFrom;
                dailyOT.IONumber = item.IONumber;
                dailyOT.ActivityCode = item.ActivityCode;
                dailyOT.Calculate(MWS, true);
                oReturn.Add(dailyOT);
            }

            //otlist = HR.TM.ServiceManager.HRTMService.LoadOTData(EmployeeID, BeginDate, EndDate, "D", true);

            otlist = ServiceManager.CreateInstance(CompanyCode).ERPData.LoadOTData(EmployeeID, BeginDate, EndDate, "D", true);
            foreach (SaveOTData item in otlist)
            {
                DailyOT dailyOT = new DailyOT();
                dailyOT.BeginTime = item.BeginTime;
                dailyOT.EmployeeID = EmployeeID;
                dailyOT.EndTime = item.EndTime;
                dailyOT.OTDate = item.OTDate;
                dailyOT.IsPosted = true;
                dailyOT.IONumber = item.IONumber;
                dailyOT.ActivityCode = item.ActivityCode;
                dailyOT.Calculate(MWS, true);
                oReturn.Add(dailyOT);
            }
            return oReturn;
        }


        public void DeleteDailyOT(DailyOT daily)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.DeleteDailyOT(daily);
        }

        public void SaveDailyOT(List<SaveOTData> data, bool isMark, List<string> requestNoList, bool deleteItemNotInList)
        {
            //HR.TM.ServiceManager.HRTMBuffer.SaveOTList(data, isMark, requestNoList, deleteItemNotInList);
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveOTList(data, isMark, requestNoList, deleteItemNotInList);
        }

        public List<DailyOTLog> LoadDailyOTLog(string RequestNo, string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.LoadDailyOTLog(RequestNo, EmployeeID, BeginDate, EndDate);
        }

        public List<DailyOTLog> LoadDailyOTLog(string EmployeeID, string BeginDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.LoadDailyOTLog(EmployeeID, BeginDate);
        }

        public List<DailyOTLog> LoadDailyOTLog(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.LoadDailyOTLog(EmployeeID, BeginDate, EndDate);
        }

        public List<DailyOTLog> LoadDailyOTLog(string EmployeeID, DateTime BeginDate, DateTime EndDate, DailyOTLogStatus _DailyOTLogStatus)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.LoadDailyOTLog(EmployeeID, BeginDate, EndDate, _DailyOTLogStatus);
        }

        public List<DailyOTLog> LoadDailyOTLog(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.LoadDailyOTLog(EmployeeID);
        }

        public List<DailyOTLog> LoadOTLogByCriteria(string EmployeeList, string StatusList, DateTime BeginDate, DateTime EndDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.LoadOTLogByCriteria(EmployeeList, StatusList, BeginDate, EndDate);
        }

        public DataTable GetSummaryOTInMonth(string EmployeeID, DateTime BeginDate, DateTime EndDate, string RequestNo)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetSummaryOTInMonth(EmployeeID, BeginDate, EndDate, RequestNo);
        }
        /* ====== Add by Han on 15/03/2019 : Create method for transfer data ==================== */
        public DataTable GetOTOverLimitPerWeek(string Employeeid, DateTime BeginDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetOTOverLimitPerWeek(Employeeid, BeginDate);
        }
        /* ====== Add by Han on 10/05/2019 : Create method for transfer data ==================== */
        public DataTable GetOTOverLimitPerMonth(string Employeeid, DateTime BeginDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetOTOverLimitPerMonth(Employeeid, BeginDate);
        }

        public DataTable GetOTOnHoliday(string Employeeid, DateTime BeginDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetOTOnHoliday(Employeeid, BeginDate);
        }

        public AbsenceCreatingRule GetCreatingRule(EmployeeData oEmp, string AbsenceTypeCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAbsenceCreatingRule(oEmp, AbsenceTypeCode);
        }

        public AbsenceCreatingRule GetCreatingRule(EmployeeData oEmp, string AbsenceTypeCode, decimal AbsenceDays)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAbsenceCreatingRule(oEmp, AbsenceTypeCode, AbsenceDays);
        }

        public bool ValidateAbsenceHoliday(EmployeeData oEmp, string AbsenceTypeCode, DateTime BeginDate, DateTime EndDate)
        {
            AbsenceCreatingRule oAb = GetCreatingRule(oEmp, AbsenceTypeCode);
            bool isPassValidate = false;
            if (oAb.TruncateDayOff)
            {
                MonthlyWS oMWS_BeginDate = GetCalendar(oEmp, BeginDate.Year, BeginDate.Month);
                DailyWS _DailyWS_BeginDate = oEmp.GetDailyWorkSchedule(BeginDate);

                MonthlyWS oMWS_EndDate = GetCalendar(oEmp, EndDate.Year, EndDate.Month);
                DailyWS _DailyWS_EndDate = oEmp.GetDailyWorkSchedule(EndDate);

                if (_DailyWS_BeginDate.IsDayOffOrHoliday || _DailyWS_EndDate.IsDayOffOrHoliday)
                {
                    isPassValidate = false;
                }
                else
                {
                    isPassValidate = true;
                }
            }
            else
            {
                isPassValidate = true;
            }
            return isPassValidate;
        }

        public AbsenceCreatingRule GetCreatingRule(string EmpID, string AbsenceTypeCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAbsenceCreatingRule(EmpID, AbsenceTypeCode);
        }

        public AbsenceCreatingRule GetAbsenceCreatingRule(EmployeeData oEmp, string AbsenceTypeCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAbsenceCreatingRule(oEmp, AbsenceTypeCode);
        }

        public AbsenceCreatingRule GetAbsenceCreatingRule(EmployeeData oEmp, string AbsenceTypeCode, decimal AbsenceDays)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAbsenceCreatingRule(oEmp, AbsenceTypeCode, AbsenceDays);
        }

        public AbsenceCreatingRule GetAbsenceCreatingRule(string EmpID, string AbsenceTypeCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAbsenceCreatingRule(EmpID, AbsenceTypeCode);
        }

        public AttendanceCreatingRule GetCreatingRule(EmployeeData oEmp, string AttendanceTypeCode, DateTime BeginDate, DateTime EndDate)
        {
            AttendanceCreatingRule ACR = ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAttendanceCreatinigRule(oEmp, AttendanceTypeCode);
            ACR.EmployeeID = oEmp.EmployeeID;
            ACR.BeginDate = BeginDate;
            ACR.EndDate = EndDate;
            return ACR;
        }

        public AttendanceCreatingRule GetCreatingRule(string EmployeeID, string AttendanceTypeCode, DateTime BeginDate, DateTime EndDate)
        {
            AttendanceCreatingRule ACR = ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAttendanceCreatinigRule(EmployeeID, AttendanceTypeCode);
            ACR.EmployeeID = EmployeeID;
            ACR.BeginDate = BeginDate;
            ACR.EndDate = EndDate;
            return ACR;
        }

        public DateTime LoadArchiveDate(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.LoadArchiveDate(EmployeeID);
        }

        public List<TimeElement> LoadArchiveTimeElementRelative(string Employee, DateTime BeginDate, DateTime EndDate)
        {
            List<TimeElement> list = ArchiveTimeElementRelative(Employee, BeginDate, EndDate);
            list.Sort(new TimeElementSortingTime(true));
            return list;
        }
        public List<TimePair> LoadArchiveTimepair(string Employee, DateTime BeginDate, DateTime EndDate)
        {
            List<TimePair> pair = ArchiveTimepair(Employee, BeginDate, EndDate);
            pair.Sort(new TimePairSortingDate());
            if (pair.Count == 0)
            {
                TimePair item = new TimePair();
                item.EmployeeID = Employee;
                pair.Add(item);
            }
            FillDWS(Employee, pair, true, false, BeginDate, EndDate);
            return pair;
        }


        public List<TimeElement> ArchiveTimeElementRelative(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            List<TimeElement> buffer = Load_ArchiveEventRelative(EmployeeID, BeginDate, EndDate);
            foreach (TimeElement item in buffer)
            {
                item.SetElementType(TimeElementType.ACCEPT);
            }
            return buffer;
        }
        public List<TimePair> ArchiveTimepair(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            return Load_ArchiveTimePair(EmployeeID, BeginDate, EndDate, false);
        }

        public List<TimeElement> Load_ArchiveEventRelative(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            List<TimeElement> oReturn = new List<TimeElement>();
            DateTime date1, date2;
            List<TimePair> nextPair = Load_ArchiveTimePair(EmployeeID, BeginDate.AddDays(1), EndDate.AddMonths(1), true);
            date1 = BeginDate;
            date2 = EndDate;
            if (nextPair.Count > 0)
            {
                if (nextPair[0].ClockIN != null)
                {
                    if (nextPair[0].ClockOUT != null)
                    {
                        date2 = nextPair[0].ClockIN.EventTime.AddMinutes(-1);
                    }
                    else if (nextPair.Count > 1)
                    {
                        if (nextPair[1].ClockIN != null)
                        {
                            date2 = nextPair[1].ClockIN.EventTime.AddMinutes(-1);
                        }
                        else if (nextPair[1].ClockOUT != null)
                        {
                            date2 = nextPair[1].ClockOUT.EventTime.AddMinutes(-1);
                        }
                        else
                        {
                            date2 = EndDate.AddDays(1);
                        }
                    }
                    else
                    {
                        date2 = EndDate.AddDays(1);
                    }
                }
                else if (nextPair[0].ClockOUT != null)
                {
                    date2 = nextPair[0].ClockOUT.EventTime.AddMinutes(-1);
                    nextPair = Load_ArchiveTimePair(EmployeeID, BeginDate.AddDays(1), EndDate.AddMonths(1), false);
                    if (nextPair.Count > 1)
                    {
                        if (nextPair[1].ClockIN != null)
                        {
                            date2 = nextPair[1].ClockIN.EventTime.AddMinutes(-1);
                        }
                        else if (nextPair[1].ClockOUT != null)
                        {
                            date2 = nextPair[1].ClockOUT.EventTime.AddMinutes(-1);
                        }
                    }
                    else
                    {
                        date2 = EndDate;
                    }
                }
                else
                {
                    date2 = EndDate;
                }
            }
            else
            {
                List<TimePair> currentPair = Load_ArchiveTimePair(EmployeeID, BeginDate, EndDate.AddDays(1), true);
                if (currentPair.Count == 0)
                {
                    date2 = EndDate.AddDays(1);
                }
                else if (currentPair[0].ClockOUT != null)
                {
                    date1 = currentPair[0].ClockOUT.EventTime;
                    date2 = EndDate.AddDays(2);
                }
                else
                {
                    date2 = EndDate.AddDays(2);
                }
            }
            oReturn = LoadArchiveEvent(EmployeeID, date1, date2, -1);
            return oReturn;
        }

        public List<TimePair> Load_ArchiveTimePair(string EmployeeID, DateTime BeginDate, DateTime EndDate, bool OnlyFirstRecord)
        {
            DateTime date1, date2;
            date1 = BeginDate.AddDays(-2);
            date2 = EndDate.AddDays(7);
            List<TimePair> oReturn = new List<TimePair>();
            List<TimeElement> buffer = LoadArchiveEvent(EmployeeID, date1, date2, -1);
            buffer.Sort(new TimeElementSortingTime());
            List<TimePair> pairs = MatchingClockNew(EmployeeID, buffer, true, BeginDate, EndDate, new Dictionary<DateTime, string>());
            pairs.Sort(new TimePairSortingDate());

            foreach (TimePair pair in pairs)
            {
                if (pair.Date >= BeginDate && pair.Date <= EndDate)
                {
                    oReturn.Add(pair);
                    if (OnlyFirstRecord && oReturn.Count > 1)
                    {
                        break;
                    }
                }
            }
            return oReturn;
        }

        public List<TimeElement> LoadArchiveEvent(string EmployeeID, DateTime BeginDate, DateTime EndDate, int RecordCount)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.LoadArchiveEvent(EmployeeID, BeginDate, EndDate, RecordCount);
        }

        public void FillDWS(string EmployeeID, List<TimePair> pairs, bool FillBlankPair, bool DeleteFutureDate, DateTime BeginDate, DateTime EndDate)
        {
            FillDWS(EmployeeID, pairs, FillBlankPair, DeleteFutureDate, BeginDate, EndDate, true);
        }

        public void FillDWS(string EmployeeID, List<TimePair> pairs, bool FillBlankPair, bool DeleteFutureDate, DateTime BeginDate, DateTime EndDate, bool DeleteOutSide)
        {
            FillDWS(EmployeeID, pairs, new List<TimeElement>(), FillBlankPair, DeleteFutureDate, BeginDate, EndDate, DeleteOutSide);
        }

        private static void FillDWS(string EmployeeID, List<TimePair> pairs, List<TimeElement> clock, bool FillBlankPair, bool DeleteFutureDate, DateTime BeginDate, DateTime EndDate, bool DeleteOutSide)
        {
            List<DateTime> dateList = new List<DateTime>();
            Dictionary<string, MonthlyWS> listMWS = new Dictionary<string, MonthlyWS>();
            Dictionary<string, Dictionary<string, DailyWS>> listDWS = new Dictionary<string, Dictionary<string, DailyWS>>();
            EmployeeData oEmp = null;
            string cEmpID = EmployeeID;
            string cYYYYMM;
            MonthlyWS oMWS;
            DailyWS oDWS;
            string cDWSCode = "";
            bool isHoliday = false;
            string cDailyGroup;

            if (pairs.Count > 0)
            {
                //cEmpID = pairs[0].EmployeeID;
                if (pairs[0].ClockIN != null)
                {
                    oEmp = new EmployeeData(cEmpID, pairs[0].ClockIN.EventTime);
                }
                else if (pairs[0].ClockOUT != null)
                {
                    oEmp = new EmployeeData(cEmpID, pairs[0].ClockOUT.EventTime);
                }
                else if (BeginDate != DateTime.MinValue)
                {
                    oEmp = new EmployeeData(cEmpID, BeginDate);
                }
                List<TimePair> delList = new List<TimePair>();
                List<TimePair> delList1 = new List<TimePair>();

                #region " find Date, DWS "
                foreach (TimePair pairitem in pairs)
                {
                    if (pairitem.ClockIN != null)
                    {
                        #region " CLOCKIN "
                        cYYYYMM = pairitem.ClockIN.EventTime.ToString("yyyyMM");
                        if (!listMWS.ContainsKey(cYYYYMM))
                        {
                            oMWS = MonthlyWS.GetCalendar(cEmpID, pairitem.ClockIN.EventTime.Year, pairitem.ClockIN.EventTime.Month);
                            listMWS.Add(cYYYYMM, oMWS);
                        }
                        else
                        {
                            oMWS = listMWS[cYYYYMM];
                        }

                        cDWSCode = oMWS.GetWSCode(pairitem.Date.Day, true);
                        if (cDWSCode == null)
                        {
                            cDWSCode = "";
                        }
                        isHoliday = oMWS.GetWSCode(pairitem.ClockIN.EventTime.Day, false) != "";

                        // OrgAssignment not support --- > Load new
                        if (oEmp.OrgAssignment == null || !(oEmp.OrgAssignment.BeginDate <= pairitem.ClockIN.EventTime && oEmp.OrgAssignment.EndDate >= pairitem.ClockIN.EventTime))
                        {
                            oEmp = new EmployeeData(cEmpID, pairitem.ClockIN.EventTime);
                        }

                        if (oEmp.OrgAssignment != null)
                        {
                            cDailyGroup = oEmp.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping;
                        }
                        else
                        {
                            cDailyGroup = "";
                        }

                        Dictionary<string, DailyWS> subList;
                        if (listDWS.ContainsKey(cDailyGroup))
                        {
                            subList = listDWS[cDailyGroup];
                        }
                        else
                        {
                            subList = new Dictionary<string, DailyWS>();
                            listDWS.Add(cDailyGroup, subList);
                        }

                        if (subList.ContainsKey(cDWSCode))
                        {
                            oDWS = subList[cDWSCode];
                        }
                        else
                        {
                            oDWS = DailyWS.GetDailyWorkschedule(cDailyGroup, cDWSCode, oEmp.CheckDate);
                            subList.Add(cDWSCode, oDWS);
                        }
                        if (oDWS == null || oDWS.WorkscheduleClass == null || "0,9,".IndexOf(oDWS.WorkscheduleClass.Trim() + ",") > -1)
                        {
                            // if DWS is off
                            pairitem.DWSCode = cDWSCode;
                            if (pairitem.Date <= DateTime.MinValue)
                                pairitem.Date = pairitem.ClockIN.EventTime.Date;
                            pairitem.IsDayOff = oDWS.IsDayOff;
                            pairitem.IsHoliday = false;
                        }
                        else
                        {
                            // if DWS is work day
                            pairitem.IsDayOff = false;
                            pairitem.IsHoliday = isHoliday;

                            bool lFound = false;
                            if (pairitem.ClockOUT != null)
                            {
                                DateTime dateCal;
                                DateTime dateBegin, dateEnd;
                                dateCal = pairitem.Date.Date;
                                if (oDWS.NormalBeginTime != TimeSpan.MinValue)
                                {
                                    // case FlexTime
                                    dateBegin = dateCal.Add(oDWS.NormalBeginTime);
                                    dateEnd = dateCal.Add(oDWS.NormalEndTime);
                                }
                                else
                                {
                                    // case StandardTime
                                    dateBegin = dateCal.Add(oDWS.WorkBeginTime);
                                    dateEnd = dateCal.Add(oDWS.WorkEndTime);
                                }
                                if (dateBegin >= dateEnd)
                                {
                                    dateEnd = dateEnd.AddDays(1);
                                }
                                if (pairitem.ClockIN.EventTime <= dateEnd && pairitem.ClockOUT.EventTime >= dateBegin)
                                {
                                    lFound = true;
                                    pairitem.Date = dateCal;
                                    pairitem.DWSCode = cDWSCode;
                                }
                            }
                            if (pairitem.ClockOUT == null || !lFound)
                            {
                                #region " case (in,null)"
                                // case (in,null)
                                if ("1,2,".IndexOf(oMWS.ValuationClass.Trim() + ",") > -1)
                                {
                                    // if (norm or tnom)
                                    pairitem.DWSCode = cDWSCode;
                                    pairitem.Date = pairitem.Date.Date;
                                }
                                else
                                {
                                    // if shift cutoff at 4:30
                                    if (pairitem.ClockIN.EventTime.TimeOfDay < new TimeSpan(4, 30, 0) && pairitem.ClockIN.EventTime.Date < DateTime.Now.Date)
                                    {
                                        DateTime oYesterday = pairitem.ClockIN.EventTime.AddDays(-1);

                                        string cYYYYMM_temp = oYesterday.ToString("yyyyMM");
                                        MonthlyWS oMWS_temp;
                                        DailyWS oDWS_temp;
                                        if (!listMWS.ContainsKey(cYYYYMM_temp))
                                        {
                                            oMWS_temp = MonthlyWS.GetCalendar(pairitem.ClockIN.EmployeeID, oYesterday.Year, oYesterday.Month);
                                            listMWS.Add(cYYYYMM_temp, oMWS);
                                        }
                                        else
                                        {
                                            oMWS_temp = listMWS[cYYYYMM_temp];
                                        }
                                        string cDWSCode_temp = oMWS_temp.GetWSCode(oYesterday.Day, true);
                                        bool isHoliday_temp = oMWS_temp.GetWSCode(oYesterday.Day, false) != "";

                                        // OrgAssignment not support --- > Load new
                                        if (!(oEmp.OrgAssignment.BeginDate <= pairitem.ClockIN.EventTime && oEmp.OrgAssignment.EndDate >= pairitem.ClockIN.EventTime))
                                        {
                                            oEmp = new EmployeeData(cEmpID, pairitem.ClockIN.EventTime);
                                        }

                                        cDailyGroup = oEmp.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping;

                                        Dictionary<string, DailyWS> subList_temp;

                                        if (listDWS.ContainsKey(cDailyGroup))
                                        {
                                            subList_temp = listDWS[cDailyGroup];
                                        }
                                        else
                                        {
                                            subList_temp = new Dictionary<string, DailyWS>();
                                            listDWS.Add(cDailyGroup, subList_temp);
                                        }
                                        if (subList_temp.ContainsKey(cDWSCode_temp))
                                        {
                                            oDWS_temp = subList_temp[cDWSCode_temp];
                                        }
                                        else
                                        {
                                            oDWS_temp = DailyWS.GetDailyWorkschedule(cDailyGroup, cDWSCode_temp, oYesterday);
                                            subList_temp.Add(cDWSCode_temp, oDWS_temp);
                                        }
                                        if ("0,9,".IndexOf(oDWS_temp.WorkscheduleClass.Trim() + ",") > -1)
                                        {
                                            // if DWS is off
                                            pairitem.IsDayOff = true;
                                        }
                                        else
                                        {
                                            pairitem.IsDayOff = false;
                                        }
                                        pairitem.IsHoliday = isHoliday_temp;
                                        pairitem.DWSCode = cDWSCode_temp;
                                        pairitem.Date = oYesterday.Date;
                                    }
                                    else
                                    {
                                        pairitem.DWSCode = cDWSCode;
                                        pairitem.Date = pairitem.ClockIN.EventTime.Date;
                                    }
                                }
                                #endregion
                            }
                        }
                        #endregion
                    }
                    else if (pairitem.ClockOUT != null)
                    {
                        #region " CLOCKOUT "
                        cYYYYMM = pairitem.ClockOUT.EventTime.ToString("yyyyMM");
                        if (!listMWS.ContainsKey(cYYYYMM))
                        {
                            oMWS = MonthlyWS.GetCalendar(cEmpID, pairitem.ClockOUT.EventTime.Year, pairitem.ClockOUT.EventTime.Month);
                            listMWS.Add(cYYYYMM, oMWS);
                        }
                        else
                        {
                            oMWS = listMWS[cYYYYMM];
                        }

                        cDWSCode = oMWS.GetWSCode(pairitem.ClockOUT.EventTime.Day, true);
                        isHoliday = oMWS.GetWSCode(pairitem.ClockOUT.EventTime.Day, false) != "";

                        // OrgAssignment not support --- > Load new
                        if (oEmp == null || oEmp.OrgAssignment == null || !(oEmp.OrgAssignment.BeginDate <= pairitem.ClockOUT.EventTime && oEmp.OrgAssignment.EndDate >= pairitem.ClockOUT.EventTime))
                        {
                            oEmp = new EmployeeData(cEmpID, pairitem.ClockOUT.EventTime);
                        }

                        cDailyGroup = oEmp.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping;

                        Dictionary<string, DailyWS> subList;
                        if (listDWS.ContainsKey(cDailyGroup))
                        {
                            subList = listDWS[cDailyGroup];
                        }
                        else
                        {
                            subList = new Dictionary<string, DailyWS>();
                            listDWS.Add(cDailyGroup, subList);
                        }

                        if (subList.ContainsKey(cDWSCode))
                        {
                            oDWS = subList[cDWSCode];
                        }
                        else
                        {
                            oDWS = DailyWS.GetDailyWorkschedule(cDailyGroup, cDWSCode, oEmp.CheckDate);
                            subList.Add(cDWSCode, oDWS);
                        }

                        pairitem.IsHoliday = isHoliday;

                        if ("0,9,".IndexOf(oDWS.WorkscheduleClass.Trim() + ",") > -1)
                        {
                            // if DWS is off
                            pairitem.IsDayOff = true;
                        }
                        else
                        {
                            pairitem.IsDayOff = false;
                        }

                        #region " case (null,out)"
                        if ("1,2,".IndexOf(oMWS.ValuationClass.Trim() + ",") > -1)
                        {
                            // if (norm or tnom)
                            pairitem.DWSCode = cDWSCode;
                            pairitem.Date = pairitem.ClockOUT.EventTime.Date;
                        }
                        else
                        {
                            // if shift cutoff at 7:30
                            if (pairitem.ClockOUT.EventTime.TimeOfDay < new TimeSpan(8, 30, 0))
                            {
                                DateTime oYesterday = pairitem.ClockOUT.EventTime.AddDays(-1);
                                string cYYYYMM_temp = oYesterday.ToString("yyyyMM");
                                MonthlyWS oMWS_temp;
                                DailyWS oDWS_temp;
                                if (!listMWS.ContainsKey(cYYYYMM_temp))
                                {
                                    oMWS_temp = MonthlyWS.GetCalendar(pairitem.ClockOUT.EmployeeID, oYesterday.Year, oYesterday.Month);
                                    listMWS.Add(cYYYYMM_temp, oMWS);
                                }
                                else
                                {
                                    oMWS_temp = listMWS[cYYYYMM_temp];
                                }
                                string cDWSCode_temp = oMWS_temp.GetWSCode(oYesterday.Day, true);
                                bool isHoliday_temp = oMWS_temp.GetWSCode(oYesterday.Day, false) != "";

                                // OrgAssignment not support --- > Load new
                                if (!(oEmp.OrgAssignment.BeginDate <= pairitem.ClockOUT.EventTime && oEmp.OrgAssignment.EndDate >= pairitem.ClockOUT.EventTime))
                                {
                                    oEmp = new EmployeeData(cEmpID, pairitem.ClockOUT.EventTime);
                                }

                                cDailyGroup = oEmp.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping;

                                Dictionary<string, DailyWS> subList_temp;

                                if (listDWS.ContainsKey(cDailyGroup))
                                {
                                    subList_temp = listDWS[cDailyGroup];
                                }
                                else
                                {
                                    subList_temp = new Dictionary<string, DailyWS>();
                                    listDWS.Add(cDailyGroup, subList_temp);
                                }
                                if (subList_temp.ContainsKey(cDWSCode_temp))
                                {
                                    oDWS_temp = subList_temp[cDWSCode_temp];
                                }
                                else
                                {
                                    oDWS_temp = DailyWS.GetDailyWorkschedule(cDailyGroup, cDWSCode_temp, oYesterday);
                                    subList_temp.Add(cDWSCode_temp, oDWS_temp);
                                }
                                if ("0,9,".IndexOf(oDWS_temp.WorkscheduleClass.Trim() + ",") > -1)
                                {
                                    // if DWS is off
                                    pairitem.IsDayOff = true;
                                }
                                else
                                {
                                    pairitem.IsDayOff = false;
                                }
                                pairitem.IsHoliday = isHoliday_temp;

                                pairitem.DWSCode = cDWSCode_temp;
                                pairitem.Date = oYesterday.Date;
                            }
                            else
                            {
                                pairitem.DWSCode = cDWSCode;
                                pairitem.Date = pairitem.Date.Date;
                            }
                        }
                        #endregion
                        #endregion
                    }
                    if (pairitem.Date == DateTime.MinValue || (pairitem.Date > DateTime.Now.Date && DeleteFutureDate))
                    {
                        if (delList.Count > 0)
                        {
                            TimePair oLastPair = delList[delList.Count - 1];
                            if (oLastPair.Date == DateTime.Now.Date.AddDays(-1) && oLastPair.ClockOUT == null)
                            {
                                delList.Remove(oLastPair);
                            }
                        }
                        delList.Add(pairitem);
                    }
                    //else if (pairitem.Date >= DateTime.Now.Date.AddDays(-1) && DeleteFutureDate && pairitem.ClockOUT == null)
                    //{
                    //    delList.Add(pairitem);
                    //}
                    else if (DeleteOutSide && ((BeginDate != DateTime.MinValue && pairitem.Date < BeginDate) || (EndDate != DateTime.MinValue && pairitem.Date > EndDate)))
                    {
                        delList1.Add(pairitem);
                    }
                    else if (!dateList.Contains(pairitem.Date))
                    {
                        dateList.Add(pairitem.Date);
                    }
                }

                TimeElement lastEvent = null;
                foreach (TimePair pairitem in delList)
                {
                    if (lastEvent == null)
                    {
                        if (pairitem.ClockIN != null)
                        {
                            lastEvent = pairitem.ClockIN;
                        }
                        else if (pairitem.ClockOUT != null)
                        {
                            lastEvent = pairitem.ClockOUT;
                        }
                    }
                    pairs.Remove(pairitem);
                }

                foreach (TimePair pairitem in delList1)
                {
                    pairs.Remove(pairitem);
                }


                if (lastEvent != null)
                {
                    for (int index = clock.Count - 1; index >= 0; index--)
                    {
                        if (clock[index] >= lastEvent)
                        {
                            clock.RemoveAt(index);
                        }
                        else
                        {
                            break;
                        }
                    }
                }

                #endregion
            }
            else
            {
                oEmp = new EmployeeData(cEmpID, BeginDate);
            }

            #region " FillBlankPair "
            if (FillBlankPair)
            {
                DateTime dateMin, dateMax;
                if (dateList.Count > 0 || (BeginDate != DateTime.MinValue && EndDate != DateTime.MinValue))
                {
                    if (BeginDate != DateTime.MinValue && EndDate != DateTime.MinValue)
                    {
                        dateMin = BeginDate;
                        dateMax = EndDate;
                    }
                    else
                    {
                        dateMin = dateList[0];
                        dateMax = dateList[dateList.Count - 1];
                    }
                    for (DateTime rundate = dateMin.Date; rundate <= dateMax.Date; rundate = rundate.AddDays(1))
                    {
                        if (!dateList.Contains(rundate))
                        {
                            cYYYYMM = rundate.ToString("yyyyMM");
                            if (!listMWS.ContainsKey(cYYYYMM))
                            {
                                oMWS = MonthlyWS.GetCalendar(cEmpID, rundate.Year, rundate.Month);
                                listMWS.Add(cYYYYMM, oMWS);
                            }
                            else
                            {
                                oMWS = listMWS[cYYYYMM];
                            }

                            cDWSCode = oMWS.GetWSCode(rundate.Day, true);
                            if (cDWSCode == null)
                            {
                                cDWSCode = "";
                            }
                            isHoliday = oMWS.GetWSCode(rundate.Day, false) != "";
                            // OrgAssignment not support --- > Load new
                            if (oEmp.OrgAssignment == null || !(oEmp.OrgAssignment.BeginDate <= rundate && oEmp.OrgAssignment.EndDate >= rundate))
                            {
                                oEmp = new EmployeeData(cEmpID, rundate);
                            }

                            if (oEmp.OrgAssignment != null && oEmp.OrgAssignment.SubAreaSetting != null)
                            {
                                cDailyGroup = oEmp.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping;

                                Dictionary<string, DailyWS> subList;
                                if (listDWS.ContainsKey(cDailyGroup))
                                {
                                    subList = listDWS[cDailyGroup];
                                }
                                else
                                {
                                    subList = new Dictionary<string, DailyWS>();
                                    listDWS.Add(cDailyGroup, subList);
                                }

                                if (subList.ContainsKey(cDWSCode))
                                {
                                    oDWS = subList[cDWSCode];
                                }
                                else
                                {
                                    oDWS = DailyWS.GetDailyWorkschedule(cDailyGroup, cDWSCode, oEmp.CheckDate);
                                    subList.Add(cDWSCode, oDWS);
                                }
                                TimePair oBlankPair = new TimePair();
                                oBlankPair.EmployeeID = cEmpID;
                                oBlankPair.Date = rundate;
                                oBlankPair.DWSCode = cDWSCode;
                                pairs.Add(oBlankPair);
                                oBlankPair.IsHoliday = isHoliday;

                                if (oDWS != null && oDWS.WorkscheduleClass != null)
                                {
                                    if ("0,9,".IndexOf(oDWS.WorkscheduleClass.Trim() + ",") > -1)
                                    {
                                        // if DWS is off
                                        oBlankPair.IsDayOff = true;
                                    }
                                    else
                                    {
                                        oBlankPair.IsDayOff = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            #endregion
            pairs.Sort(new TimePairSortingDate());
            clock.Sort(new TimeElementSortingTime());
        }

        public DailyWS GetDailyWorkschedule(string DailyGroup, string WSCode, DateTime CheckDate)
        {
            if (WSCode == null)
            {
                throw new Exception("WSCODE_CANT_BE_NULL");
            }
            return EmployeeManagement.CreateInstance(CompanyCode).GetDailyWorkschedule(DailyGroup, WSCode, CheckDate);
        }

        public List<AbsenceAssignment> GetAbsenceAssignment(string AbsAttGrouping, string AbsenceType)
        {
            return GetAbsenceAssignment(AbsAttGrouping, AbsenceType, "*");

        }
        public List<AbsenceAssignment> GetAbsenceAssignment(string AbsAttGrouping, string AbsenceType, string SubKey)
        {
            //return HR.TM.ServiceManager.HRTMExtraConfig.GetAbsenceAssignmentList(AbsAttGrouping, AbsenceType, SubKey);
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAbsenceAssignmentList(AbsAttGrouping, AbsenceType, SubKey);
        }

        public AbsenceCancelRule GetCancelRule(string EmployeeID, string AbsenceTypeCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAbsenceCancelRule(EmployeeID, AbsenceTypeCode);
        }

        public AbsenceCancelRule GetCancelRule(EmployeeData oEmp, string AbsenceTypeCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAbsenceCancelRule(oEmp, AbsenceTypeCode);
        }

        public List<AbsenceQuotaType> GetAllAbsenceQuotaType()
        {
            //return ESS.HR.TM.ServiceManager.HRTMConfig.GetAbsenceQuotaTypeList("");
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAbsenceQuotaTypeList("");

        }

        public List<AbsenceType> GetAllAbsenceType()
        {
            //return ESS.HR.TM.ServiceManager.HRTMConfig.GetAllAbsenceTypeList();
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAllAbsenceTypeList();
        }

        public List<AbsenceType> GetAbsenceTypeList(string EmployeeID, string LanguageCode)
        {
            //return ESS.HR.TM.ServiceManager.HRTMConfig.GetAbsenceTypeList(EmployeeID, LanguageCode);
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAbsenceTypeList(EmployeeID, LanguageCode);
        }

        public List<AbsenceType> GetAbsenceTypeList(EmployeeData oEmp, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAbsenceTypeList(oEmp, LanguageCode);
        }

        public AbsenceType GetAbsenceType(string EmployeeID, string AbsenceType, string LanguageCode)
        {
            //return ESS.HR.TM.ServiceManager.HRTMConfig.GetAbsenceType(EmployeeID, AbsenceType, LanguageCode);
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAbsenceType(EmployeeID, AbsenceType, LanguageCode);
        }

        public List<AttendanceType> GetAllAttendanceType()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAllAttendanceTypeList();
        }

        public List<AttendanceType> GetAttendanceTypeList(EmployeeData oEmp, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAttendanceTypeList(oEmp, LanguageCode);
        }

        public List<AttendanceType> GetAttendanceTypeList(string EmployeeID, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAttendanceTypeList(EmployeeID, LanguageCode);
        }

        public bool CheckAbsenceExistingData(EmployeeData Requestor, INFOTYPE2001 oAbsence)
        {
            INFOTYPE2001 oINFOTYPE2001 = new INFOTYPE2001();
            List<INFOTYPE2001> list = oINFOTYPE2001.GetDataOnlyRecorded(Requestor, oAbsence.BeginDate, oAbsence.EndDate.AddDays(1));
            bool lFound = false;
            foreach (INFOTYPE2001 item in list)
            {
                if (item.ToString() == oAbsence.ToString())
                {
                    lFound = true;
                    break;
                }
            }
            return lFound;
        }

        public bool CheckAttendanceExistingData(EmployeeData Requestor, INFOTYPE2002 oAttendance)
        {
            INFOTYPE2002 oINFOTYPE2002 = new INFOTYPE2002();
            List<INFOTYPE2002> list = oINFOTYPE2002.GetData(Requestor, oAttendance.BeginDate, oAttendance.EndDate.AddDays(1), true);
            bool lFound = false;
            foreach (INFOTYPE2002 item in list)
            {
                if (item.ToString() == oAttendance.ToString())
                {
                    lFound = true;
                    break;
                }
            }
            return lFound;
        }



        public void SaveTo(List<AttendanceType> Data, string Mode)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSConfig.SaveAttendanceTypeList(Data);
        }

        public AttendanceType GetAttendanceType(string EmployeeID, string AttendanceType, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAttendanceType(EmployeeID, AttendanceType, LanguageCode);
        }

        public string GetAttendanceFlowSetting(string EmployeeID, string AttendanceTypeCode)
        {
            EmployeeData oEmp = new EmployeeData(EmployeeID);
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAttendanceFlowSetting(oEmp.OrgAssignment.SubAreaSetting.AbsAttGrouping, AttendanceTypeCode);
        }

        public List<CountingRule> GetAllCountingRule()
        {
            //return HR.TM.ServiceManager.HRTMConfig.GetAllCountingRuleList();
            return ServiceManager.CreateInstance(CompanyCode).ERPConfig.GetAllCountingRuleList();
        }

        public AbsenceQuotaType GetAbsenceQuotaType(string EmployeeID, string QuotaTypeCode, string LanguageCode)
        {
            //return ESS.HR.TM.ServiceManager.HRTMConfig.GetAbsenceQuotaType(EmployeeID, QuotaTypeCode, LanguageCode);
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAbsenceQuotaType(EmployeeID, QuotaTypeCode, LanguageCode);
        }

        public DeductionRule GetDeductionRule(EmployeeData employeeData, string AbsenceType)
        {
            //return HR.TM.ServiceManager.HRTMConfig.GetDeductionRule(employeeData.OrgAssignment.SubGroupSetting.TimeQuotaTypeGrouping, employeeData.OrgAssignment.SubAreaSetting.TimeQuotaGrouping, employeeData.OrgAssignment.SubAreaSetting.AbsAttGrouping, AbsenceType);
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetDeductionRule(employeeData.OrgAssignment.SubGroupSetting.TimeQuotaTypeGrouping, employeeData.OrgAssignment.SubAreaSetting.TimeQuotaGrouping, employeeData.OrgAssignment.SubAreaSetting.AbsAttGrouping, AbsenceType);
        }

        public AbsenceQuotaType GetAbsenceQuotaType(string sAbsenceQuotaKey)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAbsenceQuotaType(sAbsenceQuotaKey);
        }

        public List<OTItemType> GetAllOTItemType()
        {
            //return HR.TM.ServiceManager.HRTMExtraConfig.GetAllOTItemType();
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAllOTItemType();
        }

        public bool OverlapDialyOTLog(string EmployeeID, DateTime BeginDate, DateTime EndDate, String RequestNo)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.OverlapDialyOTLog(EmployeeID, BeginDate, EndDate, RequestNo);
        }

        public OTItemType GetOTItemType(int OTItemTypeID)
        {
            //return HR.TM.ServiceManager.HRTMExtraConfig.GetOTItemType(OTItemTypeID);
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetOTItemType(OTItemTypeID);
        }

        public SubstituteCreatingRule GetCreatingRule()
        {
            //return TM.ServiceManager.HRTMExtraConfig.GetSubstituteCreatingRule();
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetSubstituteCreatingRule();
        }

        public void DeleteSubstitution(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            //ESS.HR.TM.ServiceManager.HRTMService.DeleteSubstitution(EmployeeID, BeginDate, EndDate);
            //ESS.HR.TM.ServiceManager.HRTMConfig.DeleteSubstitution(EmployeeID, BeginDate, EndDate);

            ServiceManager.CreateInstance(CompanyCode).ERPData.DeleteSubstitution(EmployeeID, BeginDate, EndDate);
            ServiceManager.CreateInstance(CompanyCode).ESSConfig.DeleteSubstitution(EmployeeID, BeginDate, EndDate);
        }

        public List<EMPLOYEE.CONFIG.TM.Substitution> GetSubstitutionList(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            return EMPLOYEE.CONFIG.TM.Substitution.CreateInstance(CompanyCode).GetSubstitutionList(EmployeeID, BeginDate, EndDate, true);
        }

        public List<EMPLOYEE.CONFIG.TM.Substitution> GetSubstitutionList(string EmployeeID, int Year, int Month, bool OnlyEffective)
        {
            DateTime date1, date2;
            date1 = new DateTime(Year, Month, 1);
            date2 = date1.AddMonths(1).AddDays(-1);
            return GetSubstitutionList(EmployeeID, date1, date2, OnlyEffective);
        }

        public List<EMPLOYEE.CONFIG.TM.Substitution> GetSubstitutionListLog(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            List<EMPLOYEE.CONFIG.TM.Substitution> buffer = new List<EMPLOYEE.CONFIG.TM.Substitution>();
            buffer.AddRange(EmployeeManagement.CreateInstance(CompanyCode).GetInfotype2003_Log(EmployeeID, BeginDate, EndDate));
            return buffer;
        }

        public List<EMPLOYEE.CONFIG.TM.Substitution> GetSubstitutionList(string EmployeeID, DateTime BeginDate, DateTime EndDate, bool OnlyEffective, String RequestNo)
        {
            List<EMPLOYEE.CONFIG.TM.Substitution> buffer = new List<EMPLOYEE.CONFIG.TM.Substitution>();
            if (!OnlyEffective)
            {
                buffer.AddRange(EmployeeManagement.CreateInstance(CompanyCode).GetInfotype2003(EmployeeID, BeginDate, EndDate));
            }
            List<EMPLOYEE.CONFIG.TM.Substitution> oSubstitutionListFromSAP = EmployeeManagement.CreateInstance(CompanyCode).GetInfotype2003_ERP(EmployeeID, BeginDate, EndDate);
            foreach (EMPLOYEE.CONFIG.TM.Substitution item in oSubstitutionListFromSAP)
            {
                item.Status = "COMPLETED";   //Add by Koissares P. 20180705
                buffer.Add(item);
            }
            List<EMPLOYEE.CONFIG.TM.Substitution> oSubstitutionListFromDB = EmployeeManagement.CreateInstance(CompanyCode).GetInfotype2003(EmployeeID, BeginDate, EndDate, RequestNo);

            return oSubstitutionListFromDB;
        }

        public int GetNextYearAbsenceQuota(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).ERPData.GetNextYearAbsenceQuota(EmployeeID);
        }

        public object GetTimeManagementList(EmployeeData oEmp, string CompanyCode)
        {
            List<AbsenceOther> data_list_absence_other = new List<AbsenceOther>();
            List<INFOTYPE2006> oListINFOTYPE2006 = new List<INFOTYPE2006>();
            List<INFOTYPE2001> oListINFOTYPE2001 = new List<INFOTYPE2001>();
            List<INFOTYPE2002> oListINFOTYPE2002 = new List<INFOTYPE2002>();
            List<EMPLOYEE.CONFIG.TM.Substitution> oListSubstitution = new List<EMPLOYEE.CONFIG.TM.Substitution>();
            INFOTYPE2006 oINFOTYPE2006 = new INFOTYPE2006();
            oINFOTYPE2006.CompanyCode = CompanyCode;
            oListINFOTYPE2006 = oINFOTYPE2006.GetData(oEmp);

            int currentYear = DateTime.Now.Year;
            DateTime oCurrentDate = new DateTime(currentYear, 1, 1);
            INFOTYPE2001 oINFOTYPE2001 = new INFOTYPE2001();
            oListINFOTYPE2001 = oINFOTYPE2001.GetData(oEmp, oCurrentDate, oCurrentDate.AddYears(1));

            INFOTYPE2002 oINFOTYPE2002 = new INFOTYPE2002();
            oListINFOTYPE2002 = oINFOTYPE2002.GetData(oEmp, oCurrentDate, oCurrentDate.AddYears(1));

            EMPLOYEE.CONFIG.TM.Substitution oSubstitution = new EMPLOYEE.CONFIG.TM.Substitution();
            oSubstitution.CompanyCode = CompanyCode;
            oListSubstitution = oSubstitution.GetSubstitutionList(oEmp.EmployeeID, oCurrentDate, oCurrentDate.AddYears(1));

            var list_absence_other = oListINFOTYPE2006.Where(s => s.IsShowDashboard == false).ToList();
            if (list_absence_other.Count > 0)
            {
                foreach (var item in list_absence_other)
                {
                    var absence_other = new AbsenceOther()
                    {
                        InProcessAbsence = item.InprocessAmount,
                        QuotaAbsence = item.QuotaAmount,
                        UseAbsence = item.UsedAmount,
                        AbsenceDefalutType = item.DefaultAbsenceType,
                        AbsenceType = item.AbsenceType,
                        PercentageUse = 0
                    };

                    if (item.QuotaAmount > 0)
                    {
                        absence_other.PercentageUse = (item.UsedAmount * 100) / item.QuotaAmount;
                    }
                    data_list_absence_other.Add(absence_other);
                }
            }

            object oResult = new
            {
                INFOTYPE2006Data = oListINFOTYPE2006,
                INFOTYPE2001Data = oListINFOTYPE2001,
                INFOTYPE2002Data = oListINFOTYPE2002,
                SubstitutionData = oListSubstitution,
                INFOTYPE2006DataOhter = data_list_absence_other
            };
            return oResult;
        }

        public ListItem[] DivideDailyWorkSchedule(string EmployeeID, int divisor, bool inculdeAllDay, DateTime Absencedate)
        {
            EmployeeData _EmployeeData = new EmployeeData(EmployeeID, Absencedate);
            ESS.EMPLOYEE.CONFIG.TM.DailyWS _DailyWS = _EmployeeData.GetDailyWorkSchedule(Absencedate);
            decimal _workingHours = _DailyWS.WorkHours / divisor;

            if (!inculdeAllDay)
            {
                //DateTime beginTime = new DateTime(_DailyWS.WorkBeginTime.Ticks);
                //DateTime endTime = new DateTime(_DailyWS.WorkEndTime.Ticks);
                //Edit by Koissares 20170915
                DateTime beginTime = Absencedate.Date.Add(_DailyWS.WorkBeginTime);
                DateTime endTime = Absencedate.Date.Add(_DailyWS.WorkEndTime);

                ListItem[] _ListItem = new ListItem[4];

                if (_DailyWS.BreakCode.Equals("NOON"))
                {
                    _ListItem[0] = new ListItem();
                    _ListItem[0].Text = beginTime.ToString("HH:mm");
                    _ListItem[1] = new ListItem();
                    _ListItem[1].Text = beginTime.Date.AddHours(12).ToString("HH:mm");
                    _ListItem[2] = new ListItem();
                    _ListItem[2].Text = beginTime.Date.AddHours(13).ToString("HH:mm");
                    _ListItem[3] = new ListItem();
                    _ListItem[3].Text = endTime.ToString("HH:mm");
                }
                else
                {
                    _ListItem[0] = new ListItem();
                    _ListItem[0].Text = beginTime.ToString("HH:mm");//_DailyWS.WorkBeginTime.ToString().Substring(0, 5);
                    _ListItem[1] = new ListItem();
                    _ListItem[1].Text = beginTime.AddHours((double)_workingHours).ToString("HH:mm");//_DailyWS.WorkBeginTime.Add(new TimeSpan(Convert.ToInt16(_workingHours), 0, 0)).ToString().Substring(0, 5);
                    _ListItem[2] = new ListItem();
                    _ListItem[2].Text = endTime.AddHours(-(double)_workingHours).ToString("HH:mm");//_DailyWS.WorkEndTime.Add(new TimeSpan(-Convert.ToInt16(_workingHours), 0, 0)).ToString().Substring(0, 5);
                    _ListItem[3] = new ListItem();
                    _ListItem[3].Text = endTime.ToString("HH:mm");//_DailyWS.WorkEndTime.ToString().Substring(0, 5);
                }
                return _ListItem;
            }
            else
                return new ListItem[0];
        }

        public List<string> GetValueDivideDailyWorkSchedules(int divisor, bool inculdeAllDay, DateTime dateValue, EmployeeData empValue, string strFullDayText, bool shiftOffSelectAll)
        {
            List<string> oResult = new List<string>();
            ListItem[] olstItem = DivideDailyWorkSchedule(divisor, inculdeAllDay, dateValue, empValue, strFullDayText, shiftOffSelectAll);
            if (olstItem != null)
            {
                int iCount = olstItem.Length;
                for (int i = 0; i < iCount; i++)
                {
                    oResult.Add(olstItem[i].Value);
                }
            }
            return oResult;
        }
        public List<string> GetTextDivideDailyWorkSchedules(int divisor, bool inculdeAllDay, DateTime dateValue, EmployeeData empValue, string strFullDayText, bool shiftOffSelectAll)
        {
            List<string> oResult = new List<string>();
            ListItem[] olstItem = DivideDailyWorkSchedule(divisor, inculdeAllDay, dateValue, empValue, strFullDayText, shiftOffSelectAll);
            if (olstItem != null)
            {
                int iCount = olstItem.Length;
                for (int i = 0; i < iCount; i++)
                {
                    oResult.Add(olstItem[i].Text);
                }
            }
            return oResult;
        }

        public ListItem[] DivideDailyWorkSchedule(int divisor, bool inculdeAllDay, DateTime dateValue, EmployeeData empValue, string strFullDayText, bool shiftOffSelectAll)
        {
            DateTime tmpDate = dateValue;
            //EmployeeData _EmployeeData = empValue;

            EmployeeData _EmployeeData = new EmployeeData(empValue.EmployeeID, dateValue) { CompanyCode = this.CompanyCode };

            DailyWS _DailyWS = _EmployeeData.GetDailyWorkSchedule(tmpDate ,this.CompanyCode);
            while (_DailyWS.IsDayOff && tmpDate > _EmployeeData.DateSpecific.HiringDate)
            {
                tmpDate = tmpDate.AddDays(-1);
                _DailyWS = _EmployeeData.GetDailyWorkSchedule(tmpDate);
            }

            if (_DailyWS.IsDayOff)
            {
                bool foundWorkingDay = false;
                tmpDate = dateValue;
                for (int i = 0; i < 20; i++)
                {
                    _DailyWS = _EmployeeData.GetDailyWorkSchedule(tmpDate.AddDays(i));
                    if (!_DailyWS.IsDayOff)
                    {
                        foundWorkingDay = true;
                        break;
                    }
                }

                if (!foundWorkingDay)
                {
                    throw new Exception("CANNOT_FIND_WORKING_DAY");
                }
            }

            decimal _workingHours = _DailyWS.WorkHours / divisor;

            DateTime FirstBegin = new DateTime();
            DateTime FirstEnd = new DateTime();
            DateTime SecondBegin = new DateTime();
            DateTime SecondEnd = new DateTime();
            DateTime ThirdBegin = new DateTime();
            DateTime ThirdEnd = new DateTime();
            DateTime FourthBegin = new DateTime();
            DateTime FourthEnd = new DateTime();

            TimeSpan firstBegin = new TimeSpan();
            TimeSpan firstEnd = new TimeSpan();
            TimeSpan secoundBegin = new TimeSpan();
            TimeSpan secoundEnd = new TimeSpan();

            //if (_EmployeeData.EmpSubAreaType == EmployeeSubAreaType.Norm || _EmployeeData.EmpSubAreaType == EmployeeSubAreaType.Flex)
            //{

            //    if (_DailyWS.WorkBeginTime < _DailyWS.WorkEndTime)
            //    {
            //        FirstBegin = dateValue.Add(_DailyWS.WorkBeginTime);
            //        TimeSpan tmNoon = new TimeSpan(12, 0, 0);
            //        FirstEnd = dateValue.Add(_DailyWS.WorkBeginTime.Add(tmNoon - _DailyWS.WorkBeginTime));

            //        TimeSpan tmAfterNoon = new TimeSpan(13, 0, 0);
            //        SecondBegin = dateValue.Add(_DailyWS.WorkBeginTime.Add(tmAfterNoon - _DailyWS.WorkBeginTime));
            //        SecondEnd = dateValue.Add(_DailyWS.WorkEndTime);
            //    }
            //    else
            //    {
            //        FirstBegin = dateValue.Add(_DailyWS.WorkBeginTime);
            //        FirstEnd = dateValue.Add(_DailyWS.WorkBeginTime.Add(new TimeSpan(4, 0, 0)));

            //        SecondBegin = dateValue.Add(_DailyWS.WorkEndTime.Add(new TimeSpan(-4, 0, 0)));
            //        SecondEnd = dateValue.Add(_DailyWS.WorkEndTime);
            //    }

            //    shiftOffSelectAll = false;
            //}

            string FirstText = string.Empty;
            string SecondText = string.Empty;
            BreakPattern oBreakPattern = new BreakPattern();
            oBreakPattern = EmployeeManagement.CreateInstance(CompanyCode).GetBreakPattern(_DailyWS.DailyWorkscheduleGrouping, _DailyWS.BreakCode);
            if (_EmployeeData.EmpSubAreaType == EmployeeSubAreaType.Norm)
            {

                if (_DailyWS.WorkBeginTime < _DailyWS.WorkEndTime)
                {
                    FirstBegin = dateValue.Add(_DailyWS.WorkBeginTime);
                    TimeSpan tmNoon = new TimeSpan(12, 0, 0);
                    FirstEnd = dateValue.Add(_DailyWS.WorkBeginTime.Add(tmNoon - _DailyWS.WorkBeginTime));

                    TimeSpan tmAfterNoon = new TimeSpan(13, 0, 0);
                    SecondBegin = dateValue.Add(_DailyWS.WorkBeginTime.Add(tmAfterNoon - _DailyWS.WorkBeginTime));
                    SecondEnd = dateValue.Add(_DailyWS.WorkEndTime);
                }
                else
                {
                    FirstBegin = dateValue.Add(_DailyWS.WorkBeginTime);
                    FirstEnd = dateValue.Add(_DailyWS.WorkBeginTime.Add(new TimeSpan(4, 0, 0)));

                    SecondBegin = dateValue.Add(_DailyWS.WorkEndTime.Add(new TimeSpan(-4, 0, 0)));
                    SecondEnd = dateValue.Add(_DailyWS.WorkEndTime);
                }

                shiftOffSelectAll = false;
            }
            else if (_EmployeeData.EmpSubAreaType == EmployeeSubAreaType.Flex)
            {
                INFOTYPE0007 oInfotype0007 = _EmployeeData.GetEmployeeWF(_EmployeeData.EmployeeID, dateValue);
                if (!string.IsNullOrEmpty(oInfotype0007.WFRule))
                {
                    TimeSpan oBeginTimeDailyFlexTime;
                    TimeSpan oEndTimeDailyFlexTime;
                    DailyFlexTime oDFX = HRTMManagement.CreateInstance(CompanyCode).GetDailyFlexTimeMinForCalculateByFlexCode(oInfotype0007.WFRule);

                    BreakPattern oBreak = new BreakPattern();
                    oBreak = EmployeeManagement.CreateInstance(CompanyCode).GetBreakPattern(_DailyWS.DailyWorkscheduleGrouping, oDFX.BreakCode);

                    TimeSpan TBegin;
                    if (!TimeSpan.TryParse(oDFX.BeginTime, out TBegin))
                    {
                        // handle validation error
                    }
                    oBeginTimeDailyFlexTime = new TimeSpan(TBegin.Hours, TBegin.Minutes, 0);

                    TimeSpan TEnd;
                    if (!TimeSpan.TryParse(oDFX.EndTime, out TEnd))
                    {
                        // handle validation error
                    }
                    oEndTimeDailyFlexTime = new TimeSpan(TEnd.Hours, TEnd.Minutes, 0);


                    FirstText = GetCommonText("HRTMABSENCE", WorkflowPrinciple.Current.UserSetting.Language, "AMTIME");
                    SecondText = GetCommonText("HRTMABSENCE", WorkflowPrinciple.Current.UserSetting.Language, "PMTIME");
                    FirstBegin = dateValue.Add(oBeginTimeDailyFlexTime);
                    if (oBreakPattern != null)
                    {
                        FirstEnd = dateValue.Add(oBreak.BeginBreak);
                        SecondBegin = dateValue.Add(oBreak.EndBreak);
                    }
                    else
                    {
                        FirstEnd = dateValue.Add(oBeginTimeDailyFlexTime).Add(new TimeSpan(4, 00, 0));
                        SecondBegin = dateValue.Add(oBeginTimeDailyFlexTime).Add(new TimeSpan(5, 00, 0));
                    }
                    SecondEnd = dateValue.Add(oEndTimeDailyFlexTime);

                }



                //if (isAMPM)
                //{
                //    FirstText = GetCommonText("HRTMABSENCE", WorkflowPrinciple.Current.UserSetting.Language, "AMTIME");
                //    SecondText = GetCommonText("HRTMABSENCE", WorkflowPrinciple.Current.UserSetting.Language, "PMTIME");
                //}

                //FirstText = GetCommonText("HRTMABSENCE", WorkflowPrinciple.Current.UserSetting.Language, "AMTIME");
                //SecondText = GetCommonText("HRTMABSENCE", WorkflowPrinciple.Current.UserSetting.Language, "PMTIME");
                //FirstBegin = dateValue.Add(_DailyWS.WorkBeginTime);
                //if (oBreakPattern != null)
                //{
                //    FirstEnd = dateValue.Add(oBreakPattern.BeginBreak);
                //    SecondBegin = dateValue.Add(oBreakPattern.EndBreak);
                //}
                //else
                //{
                //    FirstEnd = dateValue.Add(_DailyWS.WorkBeginTime).Add(new TimeSpan(4, 00, 0));
                //    SecondBegin = dateValue.Add(_DailyWS.WorkBeginTime).Add(new TimeSpan(5, 00, 0));
                //}
                //SecondEnd = dateValue.Add(_DailyWS.WorkEndTime);

            }
            else if (_EmployeeData.EmpSubAreaType == EmployeeSubAreaType.Shift8 || _EmployeeData.EmpSubAreaType == EmployeeSubAreaType.Shift12)
            {
                FirstBegin = dateValue.Add(_DailyWS.WorkBeginTime);
                FirstEnd = dateValue.Add(_DailyWS.WorkBeginTime).Add(new TimeSpan(Convert.ToInt16(_workingHours), 0, 0));
                SecondBegin = FirstEnd;
                SecondEnd = dateValue.Add(_DailyWS.WorkEndTime);
                ThirdBegin = SecondEnd;
                ThirdEnd = ThirdBegin.Add(new TimeSpan(Convert.ToInt16(_workingHours), 0, 0));
                FourthBegin = ThirdEnd;
                FourthEnd = FourthBegin.Add(new TimeSpan(Convert.ToInt16(_workingHours), 0, 0));

            }



            if (!inculdeAllDay)
            {
                ListItem[] _ListItem = new ListItem[2];
                _ListItem[0] = new ListItem();
                //_ListItem[0].Text = String.Format("{0} - {1}", FirstBegin.ToString("HH:mm"), FirstEnd.ToString("HH:mm"));
                _ListItem[0].Text = !string.IsNullOrEmpty(FirstText) ? FirstText : String.Format("{0} - {1}", FirstBegin.ToString("HH:mm"), FirstEnd.ToString("HH:mm"));
                _ListItem[0].Value = String.Format("{0} - {1}", FirstBegin.ToString("HH:mm"), FirstEnd.ToString("HH:mm"));
                _ListItem[1] = new ListItem();
                //_ListItem[1].Text = String.Format("{0} - {1}", SecondBegin.ToString("HH:mm"), SecondEnd.ToString("HH:mm"));
                _ListItem[1].Text = !string.IsNullOrEmpty(SecondText) ? SecondText : String.Format("{0} - {1}", SecondBegin.ToString("HH:mm"), SecondEnd.ToString("HH:mm"));
                _ListItem[1].Value = String.Format("{0} - {1}", SecondBegin.ToString("HH:mm"), SecondEnd.ToString("HH:mm"));

                if (shiftOffSelectAll && !dateValue.Equals(tmpDate))
                {
                    _ListItem[2] = new ListItem();
                    _ListItem[2].Text = String.Format("{0} - {1}", ThirdBegin.ToString("HH:mm"), ThirdEnd.ToString("HH:mm"));
                    _ListItem[2].Value = String.Format("{0} - {1}", ThirdBegin.ToString("HH:mm"), ThirdEnd.ToString("HH:mm"));
                    _ListItem[3] = new ListItem();
                    _ListItem[3].Text = String.Format("{0} - {1}", FourthBegin.ToString("HH:mm"), FourthEnd.ToString("HH:mm"));
                    _ListItem[3].Value = String.Format("{0} - {1}", FourthBegin.ToString("HH:mm"), FourthEnd.ToString("HH:mm"));
                }
                return _ListItem;
            }
            else if (inculdeAllDay)
            {
                ListItem[] _ListItem = new ListItem[3];
                _ListItem[0] = new ListItem();
                //_ListItem[0].Text = String.Format("{0} - {1}", FirstBegin.ToString("HH:mm"), FirstEnd.ToString("HH:mm"));
                _ListItem[0].Text = !string.IsNullOrEmpty(FirstText) ? FirstText : String.Format("{0} - {1}", FirstBegin.ToString("HH:mm"), FirstEnd.ToString("HH:mm"));
                _ListItem[0].Value = String.Format("{0} - {1}", FirstBegin.ToString("HH:mm"), FirstEnd.ToString("HH:mm"));
                _ListItem[1] = new ListItem();
                //_ListItem[1].Text = String.Format("{0} - {1}", SecondBegin.ToString("HH:mm"), SecondEnd.ToString("HH:mm"));
                _ListItem[1].Text = !string.IsNullOrEmpty(SecondText) ? SecondText : String.Format("{0} - {1}", SecondBegin.ToString("HH:mm"), SecondEnd.ToString("HH:mm"));
                _ListItem[1].Value = String.Format("{0} - {1}", SecondBegin.ToString("HH:mm"), SecondEnd.ToString("HH:mm"));

                if (shiftOffSelectAll && !dateValue.Equals(tmpDate))
                {
                    Array.Resize(ref _ListItem, 5);

                    _ListItem[2] = new ListItem();
                    _ListItem[2].Text = String.Format("{0} - {1}", ThirdBegin.ToString("HH:mm"), ThirdEnd.ToString("HH:mm"));
                    _ListItem[2].Value = String.Format("{0} - {1}", ThirdBegin.ToString("HH:mm"), ThirdEnd.ToString("HH:mm"));
                    _ListItem[3] = new ListItem();
                    _ListItem[3].Text = String.Format("{0} - {1}", FourthBegin.ToString("HH:mm"), FourthEnd.ToString("HH:mm"));
                    _ListItem[3].Value = String.Format("{0} - {1}", FourthBegin.ToString("HH:mm"), FourthEnd.ToString("HH:mm"));

                    _ListItem[4] = new ListItem();
                    _ListItem[4].Text = strFullDayText;
                    _ListItem[4].Value = String.Format("{0} - {1}", FirstBegin.ToString("HH:mm"), SecondEnd.ToString("HH:mm"));

                }
                else
                {
                    _ListItem[2] = new ListItem();
                    _ListItem[2].Text = strFullDayText;
                    _ListItem[2].Value = String.Format("{0} - {1}", FirstBegin.ToString("HH:mm"), SecondEnd.ToString("HH:mm"));
                }
                return _ListItem;
            }
            else
                return new ListItem[0];
        }

        public DataTable GetPeriodSummaryOTList(string Language)
        {
            DataTable dt = new DataTable("ListPeriod");
            dt.Columns.Add("PeriodValue", typeof(System.String));
            dt.Columns.Add("PeriodText", typeof(System.String));

            int iMonthlyOTOffsetBefore = Convert.ToInt32(MonthlyOTOffsetBefore);
            int iMonthlyOTOffsetAfter = Convert.ToInt32(MonthlyOTOffsetAfter);

            DateTime start = DateTime.Now.Date;
            DateTime rundate;
            for (int index = -iMonthlyOTOffsetBefore; index <= iMonthlyOTOffsetAfter; index++)
            {
                DataRow dr = dt.NewRow();
                rundate = start.AddMonths(index);
                dr["PeriodValue"] = rundate.ToString("yyyyMM", oCL);
                dr["PeriodText"] = String.Format("{0} {1}", GetCommonText("SYSTEM", Language, "D_M" + rundate.Month.ToString()), rundate.ToString("yyyy", oCL));
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public DataTable GetPeriodOTList(string Language)
        {
            DataTable dt = new DataTable("ListPeriod");
            dt.Columns.Add("PeriodValue", typeof(System.String));
            dt.Columns.Add("PeriodText", typeof(System.String));

            int iPlannedOTOffsetBefore = Convert.ToInt32(PlannedOTOffsetBefore);
            int iPlannedOTOffsetAfter = Convert.ToInt32(PlannedOTOffsetAfter);

            DateTime start = DateTime.Now.Date;
            DateTime rundate;
            for (int index = -iPlannedOTOffsetBefore; index <= iPlannedOTOffsetAfter; index++)
            {
                DataRow dr = dt.NewRow();
                rundate = start.AddMonths(index);
                dr["PeriodValue"] = rundate.ToString("yyyyMM", oCL);
                dr["PeriodText"] = String.Format("{0} {1}", GetCommonText("SYSTEM", Language, "D_M" + rundate.Month.ToString()), rundate.ToString("yyyy", oCL));
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public List<DailyOTLog> GetDailyOTList(EmployeeData oEmp, string selectedPeriod)
        {
            List<DailyOTLog> oListDailyOTLog = new List<DailyOTLog>();
            int currentYear = int.Parse(selectedPeriod.Substring(0, 4));
            int currentMonth = int.Parse(selectedPeriod.Substring(4, 2));
            DateTime oCurrentDate = new DateTime(currentYear, currentMonth, 1);

            DateTime BeginDate = oCurrentDate;
            DateTime EndDate = BeginDate.AddMonths(1).AddMinutes(-1);
            oListDailyOTLog = LoadDailyOTLog(oEmp.EmployeeID, BeginDate, EndDate);

            foreach (DailyOTLog oDL in oListDailyOTLog)
            {
                oDL.IsView = HRTMManagement.CreateInstance(CompanyCode).CheckDailyOTLogIsView(oDL.RequestNo);
            }

            return oListDailyOTLog;
        }

        public bool CheckDailyOTLogIsView(string RequestNo)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.CheckDailyOTLogIsView(RequestNo);
        }

        public DataTable GetPeriodOTCreate(string Language)
        {
            DataTable dt = new DataTable("ListPeriod");
            dt.Columns.Add("PeriodValue", typeof(System.String));
            dt.Columns.Add("PeriodText", typeof(System.String));

            int iDAILYOTOFFSETBEFORE = Convert.ToInt32(DAILYOTOFFSETBEFORE);
            int iDAILYOTOFFSETAFTER = Convert.ToInt32(DAILYOTOFFSETAFTER);

            DateTime start = DateTime.Now.Date;
            DateTime rundate;
            for (int index = -iDAILYOTOFFSETBEFORE - 1; index <= iDAILYOTOFFSETAFTER; index++)
            {
                DataRow dr = dt.NewRow();
                rundate = start.AddMonths(index);
                dr["PeriodValue"] = rundate.ToString("yyyyMM", oCL);
                dr["PeriodText"] = String.Format("{0} {1}", GetCommonText("SYSTEM", Language, "D_M" + rundate.Month.ToString()), rundate.ToString("yyyy", oCL));
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public DataTable GetPeriodDutyPaymentCreate(string Language)
        {
            DataTable dt = new DataTable("ListPeriod");
            dt.Columns.Add("PeriodValue", typeof(System.String));
            dt.Columns.Add("PeriodText", typeof(System.String));

            int iDutyOffsetBefore = Convert.ToInt32(DutyOffsetBefore);
            int iDutyOffsetAfter = Convert.ToInt32(DutyOffsetAfter);

            //iDutyOffsetBefore = 36;

            DateTime start = DateTime.Now.Date;
            DateTime rundate;
            for (int index = -iDutyOffsetBefore; index <= iDutyOffsetAfter; index++)
            {
                DataRow dr = dt.NewRow();
                rundate = start.AddMonths(index);
                dr["PeriodValue"] = rundate.ToString("yyyyMM", oCL);
                dr["PeriodText"] = String.Format("{0} {1}", GetCommonText("SYSTEM", Language, "D_M" + rundate.Month.ToString()), rundate.ToString("yyyy", oCL));
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public DataTable GetPeriodSubstitutionCreate(string Language)
        {
            DataTable dt = new DataTable("ListPeriod");
            dt.Columns.Add("PeriodValue", typeof(System.String));
            dt.Columns.Add("PeriodText", typeof(System.String));

            DateTime dtCurrent = DateTime.ParseExact(DateTime.Now.ToString("01/MM/yyyy", oCL), "dd/MM/yyyy", oCL);

            DataRow dRow = dt.NewRow();
            dRow["PeriodValue"] = dtCurrent.AddMonths(1).ToString("ddMMyyyy", oCL);
            dRow["PeriodText"] = String.Format("{0} {1}", GetCommonText("SYSTEM", Language, "D_M" + dtCurrent.AddMonths(1).Month.ToString()), dtCurrent.AddMonths(1).ToString("yyyy", oCL));
            dt.Rows.Add(dRow);

            for (int i = 1; i <= 11; i++)
            {
                DataRow dr = dt.NewRow();
                dr["PeriodValue"] = dtCurrent.ToString("ddMMyyyy", oCL);
                dr["PeriodText"] = String.Format("{0} {1}", GetCommonText("SYSTEM", Language, "D_M" + dtCurrent.Month.ToString()), dtCurrent.ToString("yyyy", oCL));
                dtCurrent = dtCurrent.AddMonths(-1);
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public bool IsOverLapWorkingDay(DateTime DateBeginRequest, DateTime DateEndRequest, DateTime WorkBeginDateTime, DateTime WorkEndDateTime)
        {
            DateTimeIntersec oDateTimeIntersec = new DateTimeIntersec();
            oDateTimeIntersec.Begin1 = DateBeginRequest.AddMinutes(1);
            oDateTimeIntersec.End1 = DateEndRequest.AddMinutes(-1);
            oDateTimeIntersec.Begin2 = WorkBeginDateTime;
            oDateTimeIntersec.End2 = WorkEndDateTime;
            if (oDateTimeIntersec.IsIntersecOT())
                return true;
            else
                return false;
        }

        public string ProcessError(Exception e)
        {
            string ErrorText = string.Empty;
            if (e is DataServiceException)
            {
                DataServiceException oException = (DataServiceException)e;
                string ExceptionText = string.Empty;
                if (oException.Code.Split(',').Length > 0)
                {
                    foreach (string Code in oException.Code.Split(','))
                        ErrorText += GetCommonText(oException.Category, WorkflowPrinciple.Current.UserSetting.Language, Code) + " ";
                }
                else
                    ErrorText += GetCommonText(oException.Category, WorkflowPrinciple.Current.UserSetting.Language, oException.Code) + " ";

                if (oException.Args != null)
                {
                    ErrorText = string.Format(ErrorText, oException.Args);
                }
            }
            else if (e is SaveExternalDataException)
            {
                ErrorText += "Error in save external function: " + e.Message;
            }
            else
            {
                ErrorText = (e.Message.Equals(string.Empty) ? e.InnerException.Message : e.Message);
            }

            return ErrorText;
        }

        public DataTable GetManagerOTCreate(EmployeeData oEmp, string period, string Language)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ManagerValue", typeof(System.String));
            dt.Columns.Add("ManagerText", typeof(System.String));

            List<EmployeeData> oEmployeeList = EmployeeManagement.CreateInstance(CompanyCode).GetManagerInSameOraganizationAndSameEmpSubGroup(oEmp.EmployeeID, oEmp.ActionOfPosition.ObjectID, DateTime.ParseExact(period, "yyyyMM", oCL));
            foreach (EmployeeData oEmployeeData in oEmployeeList)
            {
                DataRow dr = dt.NewRow();
                dr["ManagerValue"] = oEmployeeData.ActionOfPosition.ObjectID;
                dr["ManagerText"] = string.Format("{0} ({1})", oEmployeeData.Name, oEmployeeData.OrgAssignment.OrgUnitData.Text);
                dt.Rows.Add(dr);
            }

            DataRow dRow = dt.NewRow();
            dRow["ManagerValue"] = string.Empty;
            dRow["ManagerText"] = GetCommonText("HRTMOT", Language, "MANAGER_NONE");
            dt.Rows.Add(dRow);

            return dt;

            //if (ds.Tables["InfoTable"].Rows.Count > 0)
            //{
            //    ddlManager.SelectedValue = ds.Tables["InfoTable"].Rows[0]["REPORTTO"].ToString();
            //}
        }

        public List<OTWorkType> GetOTWorkType(string Language)
        {
            List<OTWorkType> oListOTWorkType = new List<OTWorkType>();
            oListOTWorkType = GetAllOTWorkType(Language).FindAll(delegate (OTWorkType ottype) { return ottype.IsActive; });
            oListOTWorkType.Insert(0, new OTWorkType());
            return oListOTWorkType;
        }

        public DailyOTLog CalculateOT(string EmployeeID, DateTime DateBeginRequest, DateTime DateEndRequest, string Description, string CostCenter, bool isRecalculateFromService, bool chkSubstituteOT, int OTYear, int OTMonth)
        {
            DailyOTLog result = null;
            try
            {
                //MonthlyWS MonthlyCalendar = MonthlyWS.GetCalendar(EmployeeID, OTYear, OTMonth);
                MonthlyWS MonthlyCalendar = GetCalendar(EmployeeID, OTYear, OTMonth);
                DateTime BeginDate = new DateTime(DateBeginRequest.Year, DateBeginRequest.Month, DateBeginRequest.Day, 0, 0, 0);

                //DateTimePicker BeginDate = (DateTimePicker)oTR.Cells[0].Controls[0];
                //DropDownList begindTimeH = (DropDownList)oTR.Cells[1].Controls[0];
                //DropDownList begindTimeM = (DropDownList)oTR.Cells[1].Controls[1];
                //DropDownList endTimeH = (DropDownList)oTR.Cells[2].Controls[0];
                //DropDownList endTimeM = (DropDownList)oTR.Cells[2].Controls[1];
                //if (String.IsNullOrEmpty(begindTimeH.Text) || String.IsNullOrEmpty(begindTimeM.Text)
                //    || String.IsNullOrEmpty(endTimeH.Text) || String.IsNullOrEmpty(endTimeM.Text))
                //    throw new DataServiceException("HRTM_EXCEPTION", "INVALID_DATETIME");


                //TimeTextBox BeginTime = new TimeTextBox();
                //TimeTextBox EndTime = new TimeTextBox();
                //BeginTime.Value = TimeSpan.Parse(String.Format("{0}:{1}", begindTimeH.SelectedValue, begindTimeM.SelectedValue));
                //EndTime.Value = TimeSpan.Parse(String.Format("{0}:{1}", endTimeH.SelectedValue, endTimeM.SelectedValue));


                //DateTimePicker EndDate = BeginDate;

                //if (BeginDate.Value == DateTime.MinValue || EndDate.Value == DateTime.MinValue || BeginTime.Value == TimeSpan.MinValue || EndTime.Value == TimeSpan.MinValue)
                //{
                //    this.ClearTableRow(oTR);
                //    return null;
                //}

                result = new DailyOTLog();
                //DateTime DateBeginRequest = BeginDate.Value.Add(BeginTime.Value);
                //DateTime DateEndRequest = EndTime.Value < BeginTime.Value ? EndDate.Value.AddDays(1).Add(EndTime.Value) : EndDate.Value.Add(EndTime.Value);

                if (DateEndRequest < DateBeginRequest)
                {
                    DateEndRequest = DateEndRequest.AddDays(1);
                }

                EmployeeData _EmployeeData = new EmployeeData(EmployeeID, DateBeginRequest);
                DailyWS _DailyWS = _EmployeeData.GetDailyWorkSchedule(DateBeginRequest, true);


                if (_EmployeeData.EmpSubAreaType == EmployeeSubAreaType.Flex)
                {
                    INFOTYPE0007 oInfotype0007 = _EmployeeData.GetEmployeeWF(_EmployeeData.EmployeeID, DateBeginRequest);
                    if (!string.IsNullOrEmpty(oInfotype0007.WFRule))
                    {
                        TimeSpan oBeginTimeDailyFlexTime;
                        TimeSpan oEndTimeDailyFlexTime;
                        DailyFlexTime oDFX = HRTMManagement.CreateInstance(CompanyCode).GetDailyFlexTimeMinForCalculateByFlexCode(oInfotype0007.WFRule);

                        BreakPattern oBreak = new BreakPattern();
                        oBreak = EmployeeManagement.CreateInstance(CompanyCode).GetBreakPattern(_DailyWS.DailyWorkscheduleGrouping, oDFX.BreakCode);

                        TimeSpan TBegin;
                        if (!TimeSpan.TryParse(oDFX.BeginTime, out TBegin))
                        {
                            // handle validation error
                        }
                        oBeginTimeDailyFlexTime = new TimeSpan(TBegin.Hours, TBegin.Minutes, 0);

                        TimeSpan TEnd;
                        if (!TimeSpan.TryParse(oDFX.EndTime, out TEnd))
                        {
                            // handle validation error
                        }
                        oEndTimeDailyFlexTime = new TimeSpan(TEnd.Hours, TEnd.Minutes, 0);
                        _DailyWS.WorkBeginTime = oBeginTimeDailyFlexTime;
                        _DailyWS.WorkEndTime = oEndTimeDailyFlexTime;
                    }
                }


                List<DelegateData> oDelegate = GetDelegateByAbsence(String.Empty, EmployeeID, DateBeginRequest, DateEndRequest);

                if (!_DailyWS.IsDayOff && MonthlyCalendar.GetWSCode(BeginDate.Day, false) != "1")
                {
                    if (IsOverLapWorkingDay(DateBeginRequest, DateEndRequest, BeginDate.Add(_DailyWS.WorkBeginTime), _DailyWS.WorkEndTime < _DailyWS.WorkBeginTime ? BeginDate.AddDays(1).Add(_DailyWS.WorkEndTime) : BeginDate.Add(_DailyWS.WorkEndTime)))
                        throw new DataServiceException("HRTM_EXCEPTION", "OT_TIME_OVERLAP_NORMALWORKINGTIME"
                            + "|" + BeginDate.ToString("dd/MM/", oCL) + BeginDate.ToString("yyyy ", new CultureInfo("en-US")) + BeginDate.Add(_DailyWS.WorkBeginTime).ToString("H:mm") + " - "
                            + (_DailyWS.WorkEndTime < _DailyWS.WorkBeginTime ? BeginDate.AddDays(1).Add(_DailyWS.WorkEndTime) : BeginDate.Add(_DailyWS.WorkEndTime)).ToString("H:mm"));
                }

                _EmployeeData = new EmployeeData(EmployeeID, DateBeginRequest.AddDays(1));
                _DailyWS = _EmployeeData.GetDailyWorkSchedule(DateBeginRequest.AddDays(1), true);
                if (!_DailyWS.IsDayOff && MonthlyCalendar.GetWSCode(BeginDate.AddDays(1).Day, false) != "1")
                    if (IsOverLapWorkingDay(DateBeginRequest, DateEndRequest, BeginDate.AddDays(1).Add(_DailyWS.WorkBeginTime), _DailyWS.WorkEndTime < _DailyWS.WorkBeginTime ? BeginDate.AddDays(1).AddDays(1).Add(_DailyWS.WorkEndTime) : BeginDate.AddDays(1).Add(_DailyWS.WorkEndTime)))
                        throw new DataServiceException("HRTM_EXCEPTION", "OT_TIME_OVERLAP_NORMALWORKINGTIME"
                        + "|" + BeginDate.AddDays(1).ToString("dd/MM/", oCL) + BeginDate.AddDays(1).ToString("yyyy ", new CultureInfo("en-US")) + BeginDate.AddDays(1).Add(_DailyWS.WorkBeginTime).ToString("H:mm") + " - "
                        + (_DailyWS.WorkEndTime < _DailyWS.WorkBeginTime ? BeginDate.AddDays(1).AddDays(1).Add(_DailyWS.WorkEndTime) : BeginDate.AddDays(1).Add(_DailyWS.WorkEndTime)).ToString("H:mm"));

                _EmployeeData = new EmployeeData(EmployeeID, DateBeginRequest.AddDays(-1));
                if (DateBeginRequest.AddDays(-1) >= _EmployeeData.DateSpecific.HiringDate)
                {
                    _DailyWS = _EmployeeData.GetDailyWorkSchedule(DateBeginRequest.AddDays(-1), true);
                    if (!_DailyWS.IsDayOff && MonthlyCalendar.GetWSCode(BeginDate.AddDays(-1).Day, false) != "1")
                        if (IsOverLapWorkingDay(DateBeginRequest, DateEndRequest, BeginDate.AddDays(-1).Add(_DailyWS.WorkBeginTime), _DailyWS.WorkEndTime < _DailyWS.WorkBeginTime ? BeginDate.AddDays(-1).AddDays(1).Add(_DailyWS.WorkEndTime) : BeginDate.AddDays(-1).Add(_DailyWS.WorkEndTime)))
                            throw new DataServiceException("HRTM_EXCEPTION", "OT_TIME_OVERLAP_NORMALWORKINGTIME"
                            + "|" + BeginDate.AddDays(-1).ToString("dd/MM/", oCL) + BeginDate.AddDays(-1).ToString("yyyy ", new CultureInfo("en-US")) + BeginDate.AddDays(-1).Add(_DailyWS.WorkBeginTime).ToString("H:mm") + " - "
                            + (_DailyWS.WorkEndTime < _DailyWS.WorkBeginTime ? BeginDate.AddDays(-1).AddDays(1).Add(_DailyWS.WorkEndTime) : BeginDate.AddDays(-1).Add(_DailyWS.WorkEndTime)).ToString("H:mm"));
                }
                _EmployeeData = new EmployeeData(EmployeeID, DateBeginRequest);
                _DailyWS = _EmployeeData.GetDailyWorkSchedule(DateBeginRequest, true);

                try
                {

                    result = CalculateDailyOT(EmployeeID, _DailyWS, MonthlyCalendar, DateBeginRequest, DateEndRequest, oDelegate);

                    //AddBy: Ratchatawan W. (2012-11-16)
                    if (chkSubstituteOT)
                    {
                        TimeSpan Break1200 = new TimeSpan(12, 0, 0);
                        TimeSpan Break1300 = new TimeSpan(13, 0, 0);

                        if (Break1200 > DateBeginRequest.TimeOfDay && Break1300 < DateEndRequest.TimeOfDay && result.RequestOTHour10 > 0)
                        {
                            decimal DeductNoonBreak = 60M;
                            result.RequestOTHour10 = result.RequestOTHour10 - DeductNoonBreak;
                        }
                    }

                    //oTR.Cells[5].Text = Convert.ToString(Math.Round(result.RequestOTHour10 / 60, 2, MidpointRounding.ToEven));
                    //oTR.Cells[6].Text = Convert.ToString(Math.Round(result.RequestOTHour15 / 60, 2, MidpointRounding.ToEven));
                    //oTR.Cells[7].Text = Convert.ToString(Math.Round(result.RequestOTHour30 / 60, 2, MidpointRounding.ToEven));
                    result.BeginDate = DateBeginRequest;
                    result.EndDate = DateEndRequest;
                    result.Description = Description;

                }
                catch (Exception e)
                {
                    if (e is DailyWSException)
                        throw new DataServiceException("HRTM_EXCEPTION", e.Message);
                    else
                        throw new DataServiceException("HRTM_EXCEPTION", "CALCULATION_ERROR");
                }

                //DropDownList oWorkTypeSelection = (DropDownList)oTR.Cells[3].Controls[0];
                //if (oWorkTypeSelection.SelectedIndex > 0)
                //{
                //    result.OTWorkTypeID = oWorkTypeSelection.SelectedValue;
                //}
                //else
                //{
                //    throw new DataServiceException("HRTM_EXCEPTION", "REQUIRE_OTWORKTYPE");
                //}
            }
            catch (Exception ex)
            {
                string Err = HRTMManagement.CreateInstance(CompanyCode).ShowError("HRTM_EXCEPTION", ex.Message.ToString(), WorkflowPrinciple.Current.UserSetting.Language);
                throw new Exception(Err);
                //if (isRecalculateFromService)
                //    throw ex;
                //else
                //{
                //    ProcessError(ex);
                //}
            }

            return result;
        }

        public MonthlyWS GetCalendar(string EmployeeID, int Year, int Month)
        {
            return GetCalendar(EmployeeID, DateTime.Now, Year, Month, CALENDAR_USE_SUBSTITUTE);
        }

        public MonthlyWS GetCalendar(EmployeeData oEmp, int Year, int Month)
        {
            return GetCalendar(oEmp, DateTime.Now, Year, Month, CALENDAR_USE_SUBSTITUTE);
        }

        public MonthlyWS GetCalendar(string EmployeeID, DateTime CheckDate, int Year, int Month, bool includeSubstitute)
        {
            EmployeeData oEmp = new EmployeeData(EmployeeID, CheckDate);

            string cSubGroup = "";
            string cSubArea = "";
            string cHoliday = "";

            if (oEmp.OrgAssignment != null)
            {
                cSubGroup = oEmp.OrgAssignment.SubGroupSetting.WorkScheduleGrouping;
                cSubArea = oEmp.OrgAssignment.SubAreaSetting.WorkScheduleGrouping;
                cHoliday = oEmp.OrgAssignment.SubAreaSetting.HolidayCalendar;
            }

            MonthlyWS oReturn = new MonthlyWS();
            oReturn.WS_Year = Year;
            oReturn.WS_Month = Month;
            oReturn.EmpSubGroupForWorkSchedule = cSubGroup;
            oReturn.EmpSubAreaForWorkSchedule = cSubArea;
            oReturn.PublicHolidayCalendar = cHoliday;

            //ModifiedBy: Ratchatawan W. (2013-04-05)
            List<EMPLOYEE.CONFIG.TM.Substitution> substitutionList = new List<EMPLOYEE.CONFIG.TM.Substitution>();
            if (includeSubstitute)
                substitutionList = GetSubstitutionList(EmployeeID, Year, Month, true);
            //Dictionary<int, Substitution> substitutionDict = new Dictionary<int, Substitution>();
            //if (includeSubstitute)
            //{
            //    List<Substitution> substitutionList = Substitution.GetSubstitutionList(EmployeeID, Year, Month, true);

            //    foreach (Substitution item in substitutionList.FindAll(delegate(Substitution oSub) { return oSub.Status == "COMPLETED"; }))
            //    {
            //        for (DateTime rundate = item.BeginDate; rundate <= item.EndDate; rundate = rundate.AddDays(1))
            //        {
            //            if (rundate.Year == Year && rundate.Month == Month)
            //            {
            //                substitutionDict.Add(rundate.Day, item);
            //            }
            //        }
            //    }
            //}
            bool lFirst = true;
            foreach (INFOTYPE0007 item in oEmp.GetWorkSchedule(Year, Month))
            {
                DateTime dTemp = new DateTime(Year, Month, 1);
                dTemp = dTemp.AddMonths(1).AddDays(-1);
                if (dTemp < item.BeginDate)
                    break;
                EmployeeData oEmp1 = new EmployeeData(EmployeeID, item.BeginDate);
                //MonthlyWS temp = EMPLOYEE.ServiceManager.EmployeeConfig.GetMonthlyWorkschedule(oEmp1.OrgAssignment.SubGroupSetting.WorkScheduleGrouping, oEmp1.OrgAssignment.SubAreaSetting.WorkScheduleGrouping, oEmp1.OrgAssignment.SubAreaSetting.HolidayCalendar, item.WFRule, Year, Month);

                MonthlyWS temp = EmployeeManagement.CreateInstance(CompanyCode).GetMonthlyWorkschedule(oEmp1.OrgAssignment.SubGroupSetting.WorkScheduleGrouping, oEmp1.OrgAssignment.SubAreaSetting.WorkScheduleGrouping, oEmp1.OrgAssignment.SubAreaSetting.HolidayCalendar, item.WFRule, Year, Month);
                oReturn.WorkScheduleRule = temp.WorkScheduleRule;
                if (lFirst)
                {
                    oReturn.ValuationClass = temp.ValuationClass;
                }
                lFirst = false;
                int nStart = 1;
                if (item.BeginDate.Year == Year && item.BeginDate.Month == Month)
                {
                    nStart = item.BeginDate.Day;
                }
                int nEnd = dTemp.Day;
                if (item.EndDate.Year == Year && item.EndDate.Month == Month)
                {
                    nEnd = item.EndDate.Day;
                }
                oReturn.SetValuationClassExactly(nEnd, temp.ValuationClass);
                PropertyInfo oProp;
                Type oType = oReturn.GetType();
                for (int index = nStart; index <= nEnd; index++)
                {
                    string cDayNo = index.ToString("00");
                    string cDWSCode;
                    oProp = oType.GetProperty(string.Format("Day{0}", cDayNo));

                    //Modified By: Ratchatawan W. (2013-4-5)
                    if (substitutionList.Exists(delegate (EMPLOYEE.CONFIG.TM.Substitution oSub) { return oSub.Status == "COMPLETED" && oSub.BeginDate.Day == index && oSub.BeginDate.Month == Month && oSub.BeginDate.Year == Year; }))
                    {
                        //Edit by Koissares 20161007
                        EMPLOYEE.CONFIG.TM.Substitution oSubstitution = substitutionList.Find(delegate (EMPLOYEE.CONFIG.TM.Substitution oSub) { return oSub.Status == "COMPLETED" && oSub.BeginDate.Day == index && oSub.BeginDate.Month == Month && oSub.BeginDate.Year == Year; });
                        //Substitution oSubstitution = substitutionList.Find(delegate(Substitution oSub) { return oSub.BeginDate.Day == index; });
                        if (String.IsNullOrEmpty(oSubstitution.EmpDWSCodeNew))
                        {
                            cDWSCode = oSubstitution.SubstituteBeginTime.ToString() + "-" + oSubstitution.SubstituteEndTime.ToString();
                        }
                        else
                        {
                            if (oEmp.EmployeeID.Trim() == oSubstitution.EmployeeID.Trim())
                                cDWSCode = oSubstitution.EmpDWSCodeNew;
                            else
                                cDWSCode = oSubstitution.SubDWSCodeNew;
                        }
                    }
                    else
                    {
                        cDWSCode = (string)oProp.GetValue(temp, null);
                    }

                    //if (substitutionDict.ContainsKey(index))
                    //{
                    //    //CommentBy: Ratchatawan W. (2012-11-16)
                    //    //cDWSCode = substitutionDict[index].DWSCodeView;
                    //    if(oEmp.EmployeeID == substitutionDict[index].EmployeeID)
                    //        cDWSCode = substitutionDict[index].EmpDWSCodeNew;
                    //    else
                    //        cDWSCode = substitutionDict[index].SubDWSCodeNew;
                    //}
                    //else
                    //{
                    //    cDWSCode = (string)oProp.GetValue(temp, null);
                    //}

                    oProp.SetValue(oReturn, cDWSCode, null);

                    oProp = oType.GetProperty(string.Format("Day{0}_H", cDayNo));
                    oProp.SetValue(oReturn, oProp.GetValue(temp, null), null);
                }
            }
            return oReturn;
        }

        public MonthlyWS GetCalendar(EmployeeData oEmp, DateTime CheckDate, int Year, int Month, bool includeSubstitute)
        {
            string cSubGroup = "";
            string cSubArea = "";
            string cHoliday = "";


            EmployeeData zz = new EmployeeData(oEmp.EmployeeID, CheckDate) { CompanyCode = this.CompanyCode };

            if (oEmp.OrgAssignment != null)
            {
                cSubGroup = oEmp.OrgAssignment.SubGroupSetting.WorkScheduleGrouping;
                cSubArea = oEmp.OrgAssignment.SubAreaSetting.WorkScheduleGrouping;
                cHoliday = oEmp.OrgAssignment.SubAreaSetting.HolidayCalendar;
            }

            MonthlyWS oReturn = new MonthlyWS();
            oReturn.WS_Year = Year;
            oReturn.WS_Month = Month;
            oReturn.EmpSubGroupForWorkSchedule = cSubGroup;
            oReturn.EmpSubAreaForWorkSchedule = cSubArea;
            oReturn.PublicHolidayCalendar = cHoliday;

            //ModifiedBy: Ratchatawan W. (2013-04-05)
            List<EMPLOYEE.CONFIG.TM.Substitution> substitutionList = new List<EMPLOYEE.CONFIG.TM.Substitution>();
            if (includeSubstitute)
                substitutionList = GetSubstitutionList(oEmp.EmployeeID, Year, Month, true);
            //Dictionary<int, Substitution> substitutionDict = new Dictionary<int, Substitution>();
            //if (includeSubstitute)
            //{
            //    List<Substitution> substitutionList = Substitution.GetSubstitutionList(EmployeeID, Year, Month, true);

            //    foreach (Substitution item in substitutionList.FindAll(delegate(Substitution oSub) { return oSub.Status == "COMPLETED"; }))
            //    {
            //        for (DateTime rundate = item.BeginDate; rundate <= item.EndDate; rundate = rundate.AddDays(1))
            //        {
            //            if (rundate.Year == Year && rundate.Month == Month)
            //            {
            //                substitutionDict.Add(rundate.Day, item);
            //            }
            //        }
            //    }
            //}
            bool lFirst = true;
            foreach (INFOTYPE0007 item in oEmp.GetWorkSchedule(Year, Month))
            {
                DateTime dTemp = new DateTime(Year, Month, 1);
                dTemp = dTemp.AddMonths(1).AddDays(-1);
                if (dTemp < item.BeginDate)
                    break;
                //EmployeeData oEmp1 = new EmployeeData(oEmp.EmployeeID, item.BeginDate);
                //MonthlyWS temp = EMPLOYEE.ServiceManager.EmployeeConfig.GetMonthlyWorkschedule(oEmp1.OrgAssignment.SubGroupSetting.WorkScheduleGrouping, oEmp1.OrgAssignment.SubAreaSetting.WorkScheduleGrouping, oEmp1.OrgAssignment.SubAreaSetting.HolidayCalendar, item.WFRule, Year, Month);

                EmployeeData oEmp1 = oEmp;
                MonthlyWS temp = EmployeeManagement.CreateInstance(CompanyCode).GetMonthlyWorkschedule(oEmp1.OrgAssignment.SubGroupSetting.WorkScheduleGrouping, oEmp1.OrgAssignment.SubAreaSetting.WorkScheduleGrouping, oEmp1.OrgAssignment.SubAreaSetting.HolidayCalendar, item.WFRule, Year, Month);
                oReturn.WorkScheduleRule = temp.WorkScheduleRule;
                if (lFirst)
                {
                    oReturn.ValuationClass = temp.ValuationClass;
                }
                lFirst = false;
                int nStart = 1;
                if (item.BeginDate.Year == Year && item.BeginDate.Month == Month)
                {
                    nStart = item.BeginDate.Day;
                }
                int nEnd = dTemp.Day;
                if (item.EndDate.Year == Year && item.EndDate.Month == Month)
                {
                    nEnd = item.EndDate.Day;
                }
                oReturn.SetValuationClassExactly(nEnd, temp.ValuationClass);
                PropertyInfo oProp;
                Type oType = oReturn.GetType();
                for (int index = nStart; index <= nEnd; index++)
                {
                    string cDayNo = index.ToString("00");
                    string cDWSCode;
                    oProp = oType.GetProperty(string.Format("Day{0}", cDayNo));

                    //Modified By: Ratchatawan W. (2013-4-5)
                    if (substitutionList.Exists(delegate (EMPLOYEE.CONFIG.TM.Substitution oSub) { return oSub.Status == "COMPLETED" && oSub.BeginDate.Day == index && oSub.BeginDate.Month == Month && oSub.BeginDate.Year == Year; }))
                    {
                        //Edit by Koissares 20161007
                        EMPLOYEE.CONFIG.TM.Substitution oSubstitution = substitutionList.Find(delegate (EMPLOYEE.CONFIG.TM.Substitution oSub) { return oSub.Status == "COMPLETED" && oSub.BeginDate.Day == index && oSub.BeginDate.Month == Month && oSub.BeginDate.Year == Year; });
                        //Substitution oSubstitution = substitutionList.Find(delegate(Substitution oSub) { return oSub.BeginDate.Day == index; });
                        if (String.IsNullOrEmpty(oSubstitution.EmpDWSCodeNew))
                        {
                            cDWSCode = oSubstitution.SubstituteBeginTime.ToString() + "-" + oSubstitution.SubstituteEndTime.ToString();
                        }
                        else
                        {
                            if (oEmp.EmployeeID.Trim() == oSubstitution.EmployeeID.Trim())
                                cDWSCode = oSubstitution.EmpDWSCodeNew;
                            else
                                cDWSCode = oSubstitution.SubDWSCodeNew;
                        }
                    }
                    else
                    {
                        cDWSCode = (string)oProp.GetValue(temp, null);
                    }

                    //if (substitutionDict.ContainsKey(index))
                    //{
                    //    //CommentBy: Ratchatawan W. (2012-11-16)
                    //    //cDWSCode = substitutionDict[index].DWSCodeView;
                    //    if(oEmp.EmployeeID == substitutionDict[index].EmployeeID)
                    //        cDWSCode = substitutionDict[index].EmpDWSCodeNew;
                    //    else
                    //        cDWSCode = substitutionDict[index].SubDWSCodeNew;
                    //}
                    //else
                    //{
                    //    cDWSCode = (string)oProp.GetValue(temp, null);
                    //}

                    oProp.SetValue(oReturn, cDWSCode, null);

                    oProp = oType.GetProperty(string.Format("Day{0}_H", cDayNo));
                    oProp.SetValue(oReturn, oProp.GetValue(temp, null), null);
                }
            }
            return oReturn;
        }


        public DailyWS GetDailyWorkSchedule(EmployeeData oEmp, DateTime date, bool includeSubstitute)
        {
            MonthlyWS oMWS = GetCalendar(oEmp, date, date.Year, date.Month, includeSubstitute);

            DailyWS dws = GetDailyWorkschedule(oEmp.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, oMWS.GetWSCode(date.Day, true), date);
            dws.IsHoliday = oMWS.GetWSCode(date.Day, false) != "";
            return dws;
        }

        public List<EMPLOYEE.CONFIG.TM.Substitution> GetSubstitutionList(string EmployeeID, DateTime BeginDate, DateTime EndDate, bool OnlyEffective)
        {
            List<EMPLOYEE.CONFIG.TM.Substitution> buffer = new List<EMPLOYEE.CONFIG.TM.Substitution>();
            Dictionary<DateTime, EMPLOYEE.CONFIG.TM.Substitution> oDictSubstition = new Dictionary<DateTime, EMPLOYEE.CONFIG.TM.Substitution>();
            if (!OnlyEffective)
            {
                //buffer.AddRange(EMPLOYEE.ServiceManager.EmployeeService.GetInfotype2003(EmployeeID, BeginDate, EndDate));
                buffer.AddRange(EmployeeManagement.CreateInstance(CompanyCode).GetInfotype2003(EmployeeID, BeginDate, EndDate));
            }
            //buffer.AddRange(EMPLOYEE.ServiceManager.ERPEmployeeService.GetInfotype2003(EmployeeID, BeginDate, EndDate));
            buffer.AddRange(EmployeeManagement.CreateInstance(CompanyCode).GetInfotype2003_ERP(EmployeeID, BeginDate, EndDate));

            foreach (EMPLOYEE.CONFIG.TM.Substitution item in buffer)
            {
                if (!oDictSubstition.ContainsKey(item.BeginDate.Date))  //Modify by Morakot.t 2017-12-22
                    oDictSubstition.Add(item.BeginDate.Date, item);
            }

            //List<Substitution> oSubstitutionListFromDB = EMPLOYEE.ServiceManager.EmployeeService.GetInfotype2003(EmployeeID, BeginDate, EndDate);

            List<EMPLOYEE.CONFIG.TM.Substitution> oSubstitutionListFromDB = EmployeeManagement.CreateInstance(CompanyCode).GetInfotype2003(EmployeeID, BeginDate, EndDate);
            foreach (EMPLOYEE.CONFIG.TM.Substitution oSubstitution in oSubstitutionListFromDB)
            {
                if (oDictSubstition.ContainsKey(oSubstitution.BeginDate.Date) && oDictSubstition[oSubstitution.BeginDate.Date].Status.Equals(oSubstitution.Status))
                {
                    buffer.Remove(oDictSubstition[oSubstitution.BeginDate.Date]);
                }
                buffer.Add(oSubstitution);
            }
            oDictSubstition.Clear();
            oDictSubstition = null;
            return buffer;
        }

        public List<INFOTYPE0007> GetWorkSchedule(string EmployeeID)
        {
            List<INFOTYPE0007> oListINF0007 = EmployeeManagement.CreateInstance(CompanyCode).GetWorkSchedule(EmployeeID, -1, 0);
            return oListINF0007;
        }

        public INFOTYPE0007 GetWorkSchedule(string Employee, DateTime Date)
        {
            INFOTYPE0007 oReturn = null;
            List<INFOTYPE0007> list = EmployeeManagement.CreateInstance(CompanyCode).GetWorkSchedule(Employee, Date.Year, Date.Month);
            foreach (INFOTYPE0007 item in list)
            {
                if (item.BeginDate <= Date.Date && item.EndDate >= Date.Date)
                {
                    oReturn = item;
                    break;
                }
            }
            return oReturn;
        }

        public void MarkTimesheet(string Employee, string Application, bool isMark, string Detail)
        {
            MarkTimesheet(Employee, Application, isMark, false, Detail);
        }

        public void MarkTimesheet(string EmployeeID, string ApplicationKey, string SubKey1, string SubKey2, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark, bool isCheck, string Detail)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.MarkTimesheet(EmployeeID, ApplicationKey, SubKey1, SubKey2, isFullDay, BeginDate, EndDate, isMark, isCheck, Detail);
        }

        public void MarkTimesheet(string Employee, string Application, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark, bool isCheck, string Detail)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.MarkTimesheet(Employee, Application, "", "", isFullDay, BeginDate, EndDate, isMark, isCheck, Detail);
        }

        public void MarkTimesheet(string Employee, string Application, bool isMark, bool isCheck, string Detail)
        {
            DateTime dateTime1, dateTime2;
            dateTime1 = new DateTime(1900, 1, 1);
            dateTime2 = new DateTime(9999, 12, 31);
            ServiceManager.CreateInstance(CompanyCode).ESSData.MarkTimesheet(Employee, Application, "", "", false, dateTime1, dateTime2, isMark, isCheck, Detail);
        }

        public void MarkTimesheet(List<TimesheetData> oTimesheetData, bool isMark, bool isCheck)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.MarkTimesheet(oTimesheetData, isMark, isCheck);
        }

        public void ArchiveData(string EmployeeID, DateTime BeginDate, DateTime EndDate, List<TimeElement> Data)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveArchiveEvent(EmployeeID, BeginDate, EndDate, Data);
        }
        public void ArchiveData(string EmployeeID, DateTime BeginDate, DateTime EndDate, List<TimePair> Data)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveArchiveTimePair(EmployeeID, BeginDate, EndDate, Data);
        }

        public DataTable GetPeriodTimesheet(string Language)
        {
            DataTable dt = new DataTable("ListPeriod");
            dt.Columns.Add("PeriodValue", typeof(System.String));
            dt.Columns.Add("PeriodText", typeof(System.String));

            DateTime oRunDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            DateTime myDate = new DateTime();

            int iTimeSheetSetMonth = Convert.ToInt32(TimeSheetSetMonth);
            DateTime iTimeSheetStartDate = Convert.ToDateTime(TimeSheetStartDate);

            myDate = oRunDate.AddMonths(1);
            DataRow dRow = dt.NewRow();
            dRow["PeriodValue"] = myDate.ToString("yyyyMM", oCL);
            dRow["PeriodText"] = string.Format("{0} {1}", GetCommonText("SYSTEM", Language, "D_M" + myDate.Month.ToString()), myDate.ToString("yyyy", oCL));
            dt.Rows.Add(dRow);

            for (int index = 0; index < iTimeSheetSetMonth; index++)
            {
                myDate = oRunDate.AddMonths(-index);
                if (myDate >= iTimeSheetStartDate)
                {
                    DataRow dr = dt.NewRow();
                    dr["PeriodValue"] = myDate.ToString("yyyyMM", oCL);
                    dr["PeriodText"] = string.Format("{0} {1}", GetCommonText("SYSTEM", Language, "D_M" + myDate.Month.ToString()), myDate.ToString("yyyy", oCL));
                    dt.Rows.Add(dr);
                }
            }
            return dt;
        }


        public DataTable GetPeriodTimesheetMatching(string Language)
        {
            DataTable dt = new DataTable("ListPeriod");
            dt.Columns.Add("PeriodValue", typeof(System.String));
            dt.Columns.Add("PeriodText", typeof(System.String));

            int iTimeSheetSetMonth = Convert.ToInt32(TimeSheetSetMonth);

            DateTime oRunDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            for (int index = 0; index < iTimeSheetSetMonth; index++)
            {
                DateTime myDate = oRunDate.AddMonths(-index);
                DataRow dRow = dt.NewRow();
                dRow["PeriodValue"] = myDate.ToString("yyyyMM", oCL);
                dRow["PeriodText"] = string.Format("{0} {1}", GetCommonText("SYSTEM", Language, "D_M" + myDate.Month.ToString()), myDate.ToString("yyyy", oCL));
                dt.Rows.Add(dRow);
            }
            return dt;
        }

        public class ClockinLateClockoutEarlyMatching
        {
            public ClockinLateClockoutEarlyMatching()
            {
                TimePair = new List<TimePair>();
                TimeElement = new List<TimeElement>();
            }
            public List<TimePair> TimePair { get; set; }
            public List<TimeElement> TimeElement { get; set; }
        }
        public ClockinLateClockoutEarlyMatching GetClockinLateClockoutEarlyMatching(string EmployeeID, string Period)
        {
            DateTime oBeginDate = DateTime.ParseExact(Period, "yyyyMM", oCL);
            DateTime oEndDate = oBeginDate.AddMonths(1).AddMinutes(-1);
            List<TimeElement> TimeElementList = LoadTimeElement(EmployeeID, oBeginDate.AddDays(-1), oEndDate.AddDays(2), -1, true, true);
            List<TimePair> TimePairList = MatchingClockNew(EmployeeID, TimeElementList, true, oBeginDate, oEndDate, new Dictionary<DateTime, string>());
            TimeElementList = LoadTimeElement(EmployeeID, oBeginDate.AddDays(-1), oEndDate.AddDays(2), -1, true, true);
            TimeElementList.Sort(new TimeElementSortingTime());

            foreach (TimePair oTP in TimePairList)
            {
                if (oTP.ClockIN != null)
                {
                    if (TimeElementList.Exists(delegate (TimeElement oTE) { return oTE.EventTime == oTP.ClockIN.EventTime; }))
                        TimeElementList.Find(delegate (TimeElement oTE) { return oTE.EventTime == oTP.ClockIN.EventTime; }).IsDelete = true;
                }
                if (oTP.ClockOUT != null)
                {
                    if (TimeElementList.Exists(delegate (TimeElement oTE) { return oTE.EventTime == oTP.ClockOUT.EventTime; }))
                        TimeElementList.Find(delegate (TimeElement oTE) { return oTE.EventTime == oTP.ClockOUT.EventTime; }).IsDelete = true;
                }
            }

            var oResult = new ClockinLateClockoutEarlyMatching()
            {
                TimeElement = TimeElementList.FindAll(delegate (TimeElement oTE) { return oTE.IsDelete == false; }),
                TimePair = TimePairList

            };
            return oResult;
        }

        public void SaveTimePairArchiveMappingByUser(List<TimePair> TimePairList)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveTimePairArchiveMappingByUser(TimePairList);
        }

        public void SaveTimePairArchiveMappingByUser(List<TimePair> TimePairList, string RequestNo)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveTimePairArchiveMappingByUser(TimePairList, RequestNo);
        }

        public void UpdateTimePair(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            List<string> EmployeeIDList = new List<string>();
            EmployeeIDList.Add(EmployeeID);
            UpdateTimePair(EmployeeIDList, BeginDate, EndDate);
        }

        public void UpdateTimePair(List<string> EmployeeIDList, DateTime BeginDate, DateTime EndDate)
        {
            try
            {
                int count = 0;
                Dictionary<string, MonthlyWS> dictMWS = new Dictionary<string, MonthlyWS>();
                foreach (string EmployeeID in EmployeeIDList)
                {
                    count++;
                    Console.WriteLine("{0}. try to accept timesheet for {1} : {2}", count, EmployeeID, DateTime.Now.ToString("dd/MM/yyy hh:mm:ss"));
                    List<TimeElement> oTimeElement = LoadTimeElement(EmployeeID, BeginDate, EndDate, -1, true, false);
                    List<TimePair> pairs = TM_TimePairing(EmployeeID, oTimeElement, true, BeginDate, EndDate, new Dictionary<DateTime, string>(), ref dictMWS);
                    if (pairs == null || pairs.Count == 0)
                    {
                        Console.WriteLine("Not found time pair of {0}", EmployeeID);
                        continue;
                    }

                    SaveTimePair(EmployeeID, BeginDate, EndDate, pairs);
                    Console.WriteLine("ACCEPTED.");
                }
                dictMWS.Clear();
                dictMWS = null;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void SaveTimePair(string EmployeeID, DateTime BeginDate, DateTime EndDate, List<ESS.TIMESHEET.TimePair> list)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveTimePair(EmployeeID, BeginDate, EndDate, list);
        }

        public void MappingTimePairInLateOutEarlyID(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.MappingTimePairInLateOutEarlyID(EmployeeID, BeginDate, EndDate);
        }

        public void SaveInLateOutEarly(List<ClockinLateClockoutEarly> List, string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveInLateOutEarly(List, EmployeeID, BeginDate, EndDate);
        }

        public List<TimePair> TM_TimePairing(string EmployeeID, List<TimeElement> TimeElementList, bool FillBlankPair, DateTime BeginDate, DateTime EndDate)
        {
            Dictionary<DateTime, string> optionAttendance = new Dictionary<DateTime, string>();
            Dictionary<string, MonthlyWS> dictMWS = new Dictionary<string, MonthlyWS>();
            List<TimePair> oReturn = TM_TimePairing(EmployeeID, TimeElementList, FillBlankPair, BeginDate, EndDate, optionAttendance, ref dictMWS);
            optionAttendance.Clear();
            dictMWS.Clear();
            optionAttendance = null;
            dictMWS = null;
            return oReturn;
        }

        public List<TimePair> TM_TimePairing(string EmployeeID, List<TimeElement> TimeElementList, bool FillBlankPair, DateTime BeginDate, DateTime EndDate, Dictionary<DateTime, string> optionAttendance, ref Dictionary<string, MonthlyWS> dictMWS)
        {
            List<TimePair> MatchTimePair = new List<TimePair>();
            //���§�ӴѺ TimeElement ���� 
            TimeElementList.Sort(new TimeElementSortingTime());

            //�ѹ�����������������ش��èѺ�������
            //�ҡ BeginDate �դ����ҡѺ DateTime.MinValue �����ѹ����á� TimeElementList ᷹
            //�ҡ EndDate �դ����ҡѺ DateTime.MinValue �����ѹ����ش����� TimeElementList ᷹
            DateTime oBeginDate = BeginDate != DateTime.MinValue ? BeginDate : TimeElementList[0].EventTime.Date;
            DateTime oEndDate = EndDate != DateTime.MinValue ? EndDate : TimeElementList[TimeElementList.Count - 1].EventTime.Date;

            DateTime oRunningDate = TimeElementList.Count == 0 || oBeginDate < TimeElementList[0].EventTime.Date ? oBeginDate : TimeElementList[0].EventTime.Date, oCheckDateBegin, oCheckDateEnd;
            EmployeeData oEmployeeData = null;
            DailyWS oDWS;
            TimePair oTimePair = new TimePair();
            List<TimeElement> TimeElementForShift = new List<TimeElement>();
            TimeElementForShift = TimeElementList;

            //- TimeElement ���١ Map �� User ���Ǩ����ӡ�Ѻ�����ա
            List<TimePair> oTimePairList = LoadTimePairArchiveMappingByUser(EmployeeID, BeginDate, EndDate);
            if (oTimePairList.Count > 0)
            {
                List<DateTime> oUsedTimeElement = new List<DateTime>();
                foreach (TimePair item in oTimePairList)
                {
                    if (item.ClockIN != null && item.ClockOUT != null)
                    {
                        foreach (TimeElement element in TimeElementForShift.FindAll(delegate (TimeElement TE) { return TE.EventTime >= item.ClockIN.EventTime && TE.EventTime <= item.ClockOUT.EventTime; }))
                        {
                            oUsedTimeElement.Add(element.EventTime);
                        }
                    }
                    else
                    {
                        if (item.ClockIN != null)
                            oUsedTimeElement.Add(item.ClockIN.EventTime);
                        if (item.ClockOUT != null)
                            oUsedTimeElement.Add(item.ClockOUT.EventTime);
                    }
                }

                if (oUsedTimeElement.Count > 0)
                    TimeElementForShift = TimeElementForShift.FindAll(delegate (TimeElement TE) { return !oUsedTimeElement.Contains(TE.EventTime); });

                oUsedTimeElement.Clear();
            }

            List<TimeElement> TimeElementMatchRange = new List<TimeElement>();
            DateTime dtTimeElement = DateTime.MinValue;

            List<DailyWS> listDWS = new List<DailyWS>();
            Dictionary<string, List<EMPLOYEE.CONFIG.TM.Substitution>> dictSubstitution = new Dictionary<string, List<EMPLOYEE.CONFIG.TM.Substitution>>();
            INFOTYPE0007 info0007 = null;
            Boolean IsNextDay = true;

            while (oRunningDate <= oEndDate.Date.AddDays(1) || IsNextDay)
            {
                try
                {
                    IsNextDay = false;
                    DateTime tmpDate = oRunningDate;

                    if (oEmployeeData == null || oEmployeeData.OrgAssignment == null || oRunningDate.Date > oEmployeeData.OrgAssignment.EndDate || oRunningDate.Date < oEmployeeData.OrgAssignment.BeginDate.Date)
                        oEmployeeData = new EmployeeData(EmployeeID, tmpDate);

                    if (info0007 == null || info0007.BeginDate > tmpDate || info0007.EndDate < tmpDate)
                    {
                        info0007 = EmployeeManagement.CreateInstance(CompanyCode).GetWorkSchedule(EmployeeID, tmpDate);
                    }

                    if (oEmployeeData != null && oEmployeeData.DateSpecific.HiringDate != DateTime.MinValue && oEmployeeData.DateSpecific.HiringDate <= tmpDate.Date)
                    {
                        if (oEmployeeData.OrgAssignment == null || info0007 == null || oRunningDate.Date > oEmployeeData.OrgAssignment.EndDate || oRunningDate.Date < oEmployeeData.OrgAssignment.BeginDate.Date)
                        {
                            oTimePair = new TimePair();
                            oTimePair.Date = tmpDate;
                            oTimePair.EmployeeID = EmployeeID;
                            MatchTimePair.Add(oTimePair);
                        }
                        else
                        {
                            #region ��Ҿ�ѡ�ҹ�� Norm/Flex
                            if (oEmployeeData.EmpSubAreaType != EmployeeSubAreaType.Shift12)
                            {
                                oTimePair = new TimePair();
                                oTimePair.Date = tmpDate;
                                oTimePair.EmployeeID = EmployeeID;

                                oDWS = GetDailyWSFromCollectedData(tmpDate, oEmployeeData, info0007, ref dictMWS, ref dictSubstitution, ref listDWS);

                                if (oDWS == null)
                                {
                                    oTimePair = new TimePair();
                                    oTimePair.Date = tmpDate;
                                    oTimePair.EmployeeID = EmployeeID;
                                    MatchTimePair.Add(oTimePair);
                                }
                                else
                                {
                                    oTimePair.DWS = oDWS;
                                    oTimePair.DWSCode = oDWS.DailyWorkscheduleCode;
                                    oTimePair.IsDayOff = oDWS.IsDayOff;
                                    oTimePair.IsHoliday = oDWS.IsHoliday;

                                    //�� TimeElement �á�� Clock-In ��� Record �ش������ Clock-Out
                                    if (!String.IsNullOrEmpty(oDWS.DailyWorkscheduleCode) && !oDWS.DailyWorkscheduleCode.StartsWith("S"))
                                    {
                                        #region " + DailyWS for OFF "
                                        //����ѹ������ѹ OFF ����� DailyWS �ѹ�ӧҹ��͹˹�ҹ�鹷������ش
                                        if (oDWS.DailyWorkscheduleCode.StartsWith("OFF"))
                                        {
                                            oTimePair.IsDayOff = true;
                                            oTimePair.DWS.WorkBeginTime = new TimeSpan(0, 0, 0);
                                            oTimePair.DWS.WorkEndTime = new TimeSpan(0, 0, 0);
                                            DateTime oBackwardDate = tmpDate.AddDays(-1);

                                            while (oDWS.DailyWorkscheduleCode.StartsWith("OFF"))
                                            {
                                                try
                                                {
                                                    oDWS = GetDailyWSFromCollectedData(oBackwardDate, oEmployeeData, info0007, ref dictMWS, ref dictSubstitution, ref listDWS);
                                                }
                                                catch
                                                {
                                                    oDWS.DailyWorkscheduleCode = oEmployeeData.OrgAssignment.SubAreaSetting.Description.ToUpper();
                                                    break;
                                                }
                                                oBackwardDate = oBackwardDate.AddDays(-1);
                                            }

                                        }
                                        #endregion

                                        oCheckDateBegin = tmpDate.Add(new TimeSpan(4, 0, 0));
                                        oCheckDateEnd = oCheckDateBegin.AddDays(1).AddSeconds(-1);
                                        TimeElementMatchRange = TimeElementForShift.FindAll(delegate (TimeElement TE) { return TE.EventTime >= oCheckDateBegin && TE.EventTime <= oCheckDateEnd && !TE.IsSystemDelete; });
                                        if (TimeElementMatchRange.Count > 0)
                                        {
                                            if (TimeElementMatchRange.Count > 0)
                                            {
                                                oTimePair.ClockIN = TimeElementMatchRange[0];
                                                //�Ӥ�� Element ���١�������� Timepair �͡�ҡ TimeElementForShift
                                                TimeElementForShift.Remove(TimeElementMatchRange[0]);
                                            }
                                            if (TimeElementMatchRange.Count > 1)
                                            {
                                                oTimePair.ClockOUT = TimeElementMatchRange[TimeElementMatchRange.Count - 1];
                                                //�Ӥ�� Element ���١�������� Timepair �͡�ҡ TimeElementForShift
                                                TimeElementForShift.Remove(TimeElementMatchRange[TimeElementMatchRange.Count - 1]);
                                            }
                                        }
                                    }
                                    MatchTimePair.Add(oTimePair);
                                }
                            }
                            #endregion
                            #region Shift
                            else
                            {
                                ////��Ҿ�ѡ�ҹ�� Shift
                                oDWS = GetDailyWSFromCollectedData(tmpDate, oEmployeeData, info0007, ref dictMWS, ref dictSubstitution, ref listDWS);

                                //�ó����ѹ�ӧҹ 
                                if (oDWS != null && !oDWS.IsDayOffOrHoliday)
                                {
                                    #region �� DailyWS �ѹ�ӧҹ��͹˹�ҹ�鹷������ش
                                    DailyWS oDWSBackward = null;
                                    DateTime oBackwardDate = tmpDate;
                                    EmployeeData tmpEmployeeData = null;
                                    INFOTYPE0007 tmpInfo0007 = null;
                                    do
                                    {
                                        oBackwardDate = oBackwardDate.AddDays(-1);
                                        if (tmpEmployeeData == null || oBackwardDate.Date > tmpEmployeeData.OrgAssignment.EndDate || oBackwardDate.Date < tmpEmployeeData.OrgAssignment.BeginDate.Date)
                                            tmpEmployeeData = new EmployeeData(EmployeeID, oBackwardDate);

                                        if (tmpInfo0007 == null || tmpInfo0007.BeginDate > oBackwardDate || tmpInfo0007.EndDate < oBackwardDate)
                                        {
                                            tmpInfo0007 = EmployeeManagement.CreateInstance(CompanyCode).GetWorkSchedule(EmployeeID, oBackwardDate);
                                        }

                                        if (tmpInfo0007 == null)
                                            break;

                                        oDWSBackward = GetDailyWSFromCollectedData(oBackwardDate, tmpEmployeeData, tmpInfo0007, ref dictMWS, ref dictSubstitution, ref listDWS);

                                    } while (oDWSBackward != null && oDWSBackward.IsDayOffOrHoliday);

                                    #endregion

                                    //������������ѹ�Ѩ�غѹ
                                    DateTime dtTmpDateTime = tmpDate.Date.Add(oDWS.WorkBeginTime);
                                    //���ҷӧҹ����ش�ѹ��͹˹��
                                    DateTime dtTmpDateTime2 = oBackwardDate.Date;

                                    if (oDWSBackward != null)
                                    {
                                        dtTmpDateTime2 = dtTmpDateTime2.Add(oDWSBackward.WorkEndTime);
                                        if (oDWSBackward.WorkBeginTime >= oDWSBackward.WorkEndTime)
                                            dtTmpDateTime2 = dtTmpDateTime2.AddDays(1);
                                    }
                                    else
                                    {
                                        dtTmpDateTime2 = dtTmpDateTime2.Add(oDWS.WorkEndTime);//by Koissares 20170825 �ó��ѹ��͹˹������բ���������
                                        if (oDWS.WorkBeginTime >= oDWS.WorkEndTime)
                                            dtTmpDateTime2 = dtTmpDateTime2.AddDays(1);
                                    }

                                    TimeSpan oTmpTimeSpan = (dtTmpDateTime - dtTmpDateTime2);

                                    int iDayOff = (tmpDate.Date - oBackwardDate.Date).Days;
                                    iDayOff = iDayOff < 1 ? 1 : iDayOff;
                                    double dbHours = oTmpTimeSpan.TotalMinutes / 60;
                                    double dbTimeDiffOff = (dbHours / (double)iDayOff);
                                    #region ��������� 0.5
                                    double dbTmpNumerator = dbTimeDiffOff - Math.Truncate(dbTimeDiffOff);
                                    dbTimeDiffOff = dbTmpNumerator < 0.5 ? Math.Truncate(dbTimeDiffOff) : dbTmpNumerator == 0.5 ? Math.Truncate(dbTimeDiffOff) + 0.5 : Math.Truncate(dbTimeDiffOff) + 1;
                                    #endregion
                                    double dbTimeDiff = dbTimeDiffOff / 2;

                                    //��ǧ������������ѹ�Ѩ�غѹ
                                    dtTmpDateTime = dtTmpDateTime.AddHours(-dbTimeDiff);

                                    if (dtTimeElement == DateTime.MinValue)//loop �á����ҨѺ������������ѧ����ժ�ǧ��������ش
                                    {
                                        dtTimeElement = dtTmpDateTime;
                                    }
                                    else
                                    {
                                        if ((tmpDate.Date - oBackwardDate.Date).Days == 1) //�ѹ��͹˹�����ѹ�ӧҹ
                                        {
                                            oTimePair = new TimePair();
                                            oTimePair.Date = tmpDate.AddDays(-1);
                                            oTimePair.EmployeeID = EmployeeID;

                                            //DailyWS otmpDWS = oEmployeeData.GetDailyWorkSchedule(oTimePair.Date, true);
                                            DailyWS otmpDWS = GetDailyWSFromCollectedData(oTimePair.Date, oEmployeeData, info0007, ref dictMWS, ref dictSubstitution, ref listDWS);
                                            oTimePair.DWS = otmpDWS;
                                            oTimePair.DWSCode = otmpDWS.DailyWorkscheduleCode;


                                            //�����ٴ�ѵ÷������㹪�ǧ
                                            TimeElementMatchRange = TimeElementForShift.FindAll(delegate (TimeElement TE) { return TE.EventTime >= dtTimeElement && TE.EventTime < dtTmpDateTime && !TE.IsSystemDelete; });

                                            if (TimeElementMatchRange.Count > 0)
                                            {
                                                if (TimeElementMatchRange.Count > 0)
                                                {
                                                    oTimePair.ClockIN = TimeElementMatchRange[0];
                                                    //�Ӥ�� Element ���١�������� Timepair �͡�ҡ TimeElementForShift
                                                    TimeElementForShift.Remove(TimeElementMatchRange[0]);
                                                }
                                                if (TimeElementMatchRange.Count > 1)
                                                {
                                                    oTimePair.ClockOUT = TimeElementMatchRange[TimeElementMatchRange.Count - 1];
                                                    //�Ӥ�� Element ���١�������� Timepair �͡�ҡ TimeElementForShift
                                                    TimeElementForShift.Remove(TimeElementMatchRange[TimeElementMatchRange.Count - 1]);
                                                }
                                            }

                                            if (!MatchTimePair.Exists(delegate (TimePair TP) { return TP.Date == oTimePair.Date; }))
                                                MatchTimePair.Add(oTimePair);

                                            dtTimeElement = dtTmpDateTime;
                                        }
                                        else//�ѹ��͹˹�����ѹ��ش
                                        {
                                            //����������ҹ�ѹ�Ѩ�غѹ - ������ԡ�ҹ�ѹ��͹˹������ش
                                            DateTime dtTmpTimeSpanEnd = dtTmpDateTime;
                                            TimeSpan TimeSpanY = new TimeSpan((int)Math.Truncate(dbTimeDiffOff), (int)((dbTimeDiffOff - Math.Truncate(dbTimeDiffOff)) * 60), 0);

                                            for (DateTime dt = tmpDate.AddDays(-1); dt >= oBackwardDate.Date; dt = dt.AddDays(-1))
                                            {
                                                DateTime dtTmpTimeSpanBegin;
                                                if (dt.Date == oBackwardDate.Date)
                                                    dtTmpTimeSpanBegin = dtTimeElement;
                                                else
                                                    dtTmpTimeSpanBegin = dtTmpTimeSpanEnd.Add(-TimeSpanY);

                                                oTimePair = new TimePair();
                                                oTimePair.Date = dt;
                                                oTimePair.EmployeeID = EmployeeID;

                                                //DailyWS otmpDWS = oEmployeeData.GetDailyWorkSchedule(dt, true);
                                                DailyWS otmpDWS = GetDailyWSFromCollectedData(dt, oEmployeeData, info0007, ref dictMWS, ref dictSubstitution, ref listDWS);
                                                oTimePair.DWS = otmpDWS;
                                                oTimePair.DWSCode = otmpDWS.DailyWorkscheduleCode;
                                                oTimePair.IsDayOff = otmpDWS.IsDayOff;
                                                oTimePair.IsHoliday = otmpDWS.IsHoliday;


                                                //�����ٴ�ѵ÷������㹪�ǧ
                                                TimeElementMatchRange = TimeElementForShift.FindAll(delegate (TimeElement TE) { return TE.EventTime >= dtTmpTimeSpanBegin && TE.EventTime < dtTmpTimeSpanEnd && !TE.IsSystemDelete; });
                                                if (TimeElementMatchRange.Count > 0)
                                                {
                                                    if (TimeElementMatchRange.Count > 0)
                                                    {
                                                        oTimePair.ClockIN = TimeElementMatchRange[0];
                                                        //�Ӥ�� Element ���١�������� Timepair �͡�ҡ TimeElementForShift
                                                        TimeElementForShift.Remove(TimeElementMatchRange[0]);
                                                    }
                                                    if (TimeElementMatchRange.Count > 1)
                                                    {
                                                        oTimePair.ClockOUT = TimeElementMatchRange[TimeElementMatchRange.Count - 1];
                                                        //�Ӥ�� Element ���١�������� Timepair �͡�ҡ TimeElementForShift
                                                        TimeElementForShift.Remove(TimeElementMatchRange[TimeElementMatchRange.Count - 1]);
                                                    }
                                                }
                                                dtTmpTimeSpanEnd = dtTmpTimeSpanBegin;
                                                if (!MatchTimePair.Exists(delegate (TimePair TP) { return TP.Date == oTimePair.Date; }))
                                                    MatchTimePair.Add(oTimePair);
                                            }

                                            dtTimeElement = dtTmpDateTime;
                                        }
                                    }
                                }
                                else //�ѹ��ش
                                {
                                    if (dtTimeElement == DateTime.MinValue)//loop �á����ҨѺ������������ѧ����ժ�ǧ��������ش
                                        dtTimeElement = oRunningDate;
                                    if (oDWS != null)
                                    {
                                        IsNextDay = true;
                                    }
                                }
                            }
                            #endregion
                        }
                    }
                }
                catch (Exception ex)
                {

                }

                oRunningDate = oRunningDate.AddDays(1);

            }

            foreach (TimePair obj in oTimePairList)
            {
                if (oEmployeeData == null || oEmployeeData.OrgAssignment == null || obj.Date > oEmployeeData.OrgAssignment.EndDate || obj.Date < oEmployeeData.OrgAssignment.BeginDate.Date)
                    oEmployeeData = new EmployeeData(EmployeeID, obj.Date);

                if (info0007 == null || info0007.BeginDate > obj.Date || info0007.EndDate < obj.Date)
                {
                    info0007 = EmployeeManagement.CreateInstance(CompanyCode).GetWorkSchedule(EmployeeID, obj.Date);
                }
                oDWS = GetDailyWSFromCollectedData(obj.Date, oEmployeeData, info0007, ref dictMWS, ref dictSubstitution, ref listDWS);
                obj.DWS = oDWS;
                obj.DWSCode = oDWS.DailyWorkscheduleCode;
                obj.IsDayOff = oDWS.IsDayOff;
                obj.IsHoliday = oDWS.IsHoliday;

                if (MatchTimePair.Exists(delegate (TimePair otp) { return otp.Date == obj.Date; }))
                {
                    oTimePair = MatchTimePair.Find(delegate (TimePair otp) { return otp.Date == obj.Date; });

                    if (obj.ClockIN != null)
                        oTimePair.ClockIN = obj.ClockIN;//.EventTime = obj.ClockIN.EventTime;
                    else
                        oTimePair.ClockIN = null;
                    if (obj.ClockOUT != null)
                        oTimePair.ClockOUT = obj.ClockOUT; //.EventTime = obj.ClockOUT.EventTime;
                    else
                        oTimePair.ClockOUT = null;

                }
                else
                {   //case TimePair by TimesheetManagementEditer
                    //if (BeginDate == EndDate) //comment by Koissares 20180103
                    MatchTimePair.Add(obj);
                }
            }

            listDWS.Clear();
            dictSubstitution.Clear();
            listDWS = null;
            dictSubstitution = null;

            MatchTimePair.Sort(new TimePairSortingDate());
            return MatchTimePair.FindAll(delegate (TimePair tp) { return tp.Date >= BeginDate && tp.Date <= EndDate; });
        }


        public string GetDWSCodeFormMWSwithSubstitute(string EmployeeID, DateTime chkDate, MonthlyWS _MWS, List<EMPLOYEE.CONFIG.TM.Substitution> _SubstitutionList)
        {
            string cDWSCode = "";

            EMPLOYEE.CONFIG.TM.Substitution oSubstitution = _SubstitutionList.Find(delegate (EMPLOYEE.CONFIG.TM.Substitution oSub) { return oSub.Status == "COMPLETED" && oSub.BeginDate.Day == chkDate.Day && oSub.BeginDate.Month == chkDate.Month && oSub.BeginDate.Year == chkDate.Year; });
            if (oSubstitution != null)
            {
                if (String.IsNullOrEmpty(oSubstitution.EmpDWSCodeNew))
                {
                    if (String.IsNullOrEmpty(oSubstitution.DWSCode))
                    {
                        cDWSCode = oSubstitution.SubstituteBeginTime.ToString() + "-" + oSubstitution.SubstituteEndTime.ToString();
                    }
                    else
                    {
                        cDWSCode = oSubstitution.DWSCode;
                    }
                }
                else
                {
                    if (EmployeeID.Trim() == oSubstitution.EmployeeID.Trim())
                        cDWSCode = oSubstitution.EmpDWSCodeNew;
                    else
                        cDWSCode = oSubstitution.SubDWSCodeNew;
                }
            }
            else
            {
                cDWSCode = _MWS.GetWSCode(chkDate.Day, true);
            }
            return cDWSCode;
        }

        public DailyWS GetDailyWSFromCollectedData(DateTime chkDate, EmployeeData _EmployeeData, INFOTYPE0007 info0007, ref Dictionary<string, MonthlyWS> dictMWS, ref Dictionary<string, List<EMPLOYEE.CONFIG.TM.Substitution>> dictSubstitution, ref List<DailyWS> listDWS)
        {
            try
            {
                DataTable dtDWS = new DataTable();
                DailyWS oDWS = null;
                string cSubGroup = _EmployeeData.OrgAssignment.SubGroupSetting.WorkScheduleGrouping;
                string cSubArea = _EmployeeData.OrgAssignment.SubAreaSetting.WorkScheduleGrouping;
                string cHoliday = _EmployeeData.OrgAssignment.SubAreaSetting.HolidayCalendar;
                string keyMWS = "";
                string keySubstitution = "";

                //Dictionary MonthlyWS
                keyMWS = string.Format("y{0}m{1}|Group{2}Area{3}Hol{4}WF{5}", chkDate.Year, chkDate.Month, cSubGroup, cSubArea, cHoliday, info0007.WFRule);
                if (!dictMWS.ContainsKey(keyMWS))
                {
                    dictMWS.Add(keyMWS, MonthlyWS.GetCalendar(_EmployeeData.EmployeeID, chkDate.Date, chkDate.Year, chkDate.Month, false));
                }

                //Dictionary Substitution
                keySubstitution = string.Format("{0}y{1}m{2}", _EmployeeData.EmployeeID, chkDate.Year, chkDate.Month);
                if (!dictSubstitution.ContainsKey(keySubstitution))
                {
                    dictSubstitution.Add(keySubstitution, EMPLOYEE.CONFIG.TM.Substitution.CreateInstance(CompanyCode).GetSubstitutionList(_EmployeeData.EmployeeID, chkDate.Year, chkDate.Month, true));
                }

                string _WSCode = GetDWSCodeFormMWSwithSubstitute(_EmployeeData.EmployeeID, chkDate, dictMWS[keyMWS], dictSubstitution[keySubstitution]);

                if (listDWS == null || listDWS.Count == 0 || listDWS[0].DailyWorkscheduleGrouping != _EmployeeData.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping)
                {
                    listDWS = EmployeeManagement.CreateInstance(CompanyCode).GetDailyWSByWorkscheduleGrouping(_EmployeeData.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, chkDate.Date);
                }
                else
                {
                    if (!listDWS.Exists(delegate (DailyWS ws) { return ws.DailyWorkscheduleCode == _WSCode && ws.BeginDate <= chkDate.Date && chkDate.Date <= ws.EndDate; }))
                    {
                        listDWS = EmployeeManagement.CreateInstance(CompanyCode).GetDailyWSByWorkscheduleGrouping(_EmployeeData.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, chkDate.Date);
                    }
                    else
                    {
                        foreach (DailyWS dws in listDWS)
                        {
                            if (chkDate.Date > dws.EndDate || chkDate.Date < dws.BeginDate)
                            {
                                listDWS = EmployeeManagement.CreateInstance(CompanyCode).GetDailyWSByWorkscheduleGrouping(_EmployeeData.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, chkDate.Date);
                            }
                        }
                    }
                }


                if (listDWS.Exists(delegate (DailyWS ws) { return ws.DailyWorkscheduleCode == _WSCode && ws.BeginDate <= chkDate.Date && chkDate.Date <= ws.EndDate; }))
                {
                    dtDWS = listDWS.Find(delegate (DailyWS ws) { return ws.DailyWorkscheduleCode == _WSCode && ws.BeginDate <= chkDate.Date && chkDate.Date <= ws.EndDate; }).ToADODataTable();
                    if (dtDWS.Rows.Count > 0)
                    {
                        oDWS = new DailyWS();
                        oDWS.ParseToObject(dtDWS);
                        oDWS.IsHoliday = dictMWS[keyMWS].GetWSCode(chkDate.Day, false) != "";
                        //oDWS.IsDayOff = oDWS.WorkHours == 0.00m;
                    }
                }

                cSubGroup = null;
                cSubArea = null;
                cHoliday = null;
                keyMWS = null;
                keySubstitution = null;

                return oDWS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string TimeSpanToString(TimeSpan input, CultureInfo oCL)
        {
            if (input != TimeSpan.MinValue)
            {
                DateTime oDate = DateTime.Now.Date.Add(input);
                return oDate.ToString("HH:mm", oCL);
            }
            else
            {
                return "-";
            }
        }

        public List<TimePair> GetTM_TimePairList(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetTM_TimePairList(EmployeeID, BeginDate, EndDate);
        }

        public List<TM_Timesheet> GetTM_Timesheet(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetTM_Timesheet(EmployeeID, BeginDate, EndDate);
        }

        public List<TM_Timesheet> GetTM_Timesheet(string EmployeeID, DateTime BeginDate, DateTime EndDate, List<TimeElement> oTimeElementList)
        {
            EmployeeData empData = null;
            MonthlyWS oMWS = null;
            List<DailyWS> listDWS = null;
            Dictionary<string, MonthlyWS> dictMWS = new Dictionary<string, MonthlyWS>();
            List<TimePair> pairs = null;

            List<TM_Timesheet> TM_TimesheetList = GetTM_Timesheet(EmployeeID, BeginDate, EndDate);
            int lastIndex = oTimeElementList.FindLastIndex(delegate (TimeElement TE) { return TE.ItemType == TimeElementType.REALTIME; });
            if (lastIndex >= 0)
            {
                if (!TM_TimesheetList.Exists(delegate (TM_Timesheet TS) { return TS.ClockIn == oTimeElementList[lastIndex].EventTime || TS.ClockOut == oTimeElementList[lastIndex].EventTime; }))
                {
                    pairs = TM_TimePairing(EmployeeID, oTimeElementList, true, DateTime.Now.Date, oTimeElementList[lastIndex].EventTime.Date, new Dictionary<DateTime, string>(), ref dictMWS);
                }
            }

            foreach (TM_Timesheet oTM_Timesheet in TM_TimesheetList)
            {

                if (string.IsNullOrEmpty(oTM_Timesheet.Type))
                {
                    DailyWS dws = new DailyWS();
                    if (empData == null || oTM_Timesheet.Date > empData.OrgAssignment.EndDate)
                    {
                        empData = new EmployeeData(EmployeeID, oTM_Timesheet.Date);
                        oMWS = MonthlyWS.GetCalendar(EmployeeID, oTM_Timesheet.Date, oTM_Timesheet.Date.Date.Year, oTM_Timesheet.Date.Month, true);
                        listDWS = EmployeeManagement.CreateInstance(CompanyCode).GetDailyWSByWorkscheduleGrouping(empData.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, oTM_Timesheet.Date);
                    }

                    dws = listDWS.Find(delegate (DailyWS ws) { return ws.DailyWorkscheduleCode == oMWS.GetWSCode(oTM_Timesheet.Date.Day, true) && ws.BeginDate <= oTM_Timesheet.Date && oTM_Timesheet.Date <= ws.EndDate; });
                    if (dws != null)
                    {
                        if (dws.DailyWorkscheduleCode.StartsWith("OFF") || oMWS.GetWSCode(oTM_Timesheet.Date.Day, false) != "")
                        {
                            oTM_Timesheet.Type = dws.DailyWorkscheduleCode;
                        }
                        else
                        {
                            oTM_Timesheet.Type = string.Format("{0} ({1}-{2})", dws.DailyWorkscheduleCode, TimeSpanToString(dws.WorkBeginTime, oCL), TimeSpanToString(dws.WorkEndTime, oCL));
                        }
                    }
                }
                if (oTM_Timesheet.Date == DateTime.Now.Date)
                {
                    if (pairs != null)
                    {
                        if (pairs.Exists(delegate (TimePair pair) { return pair.Date == oTM_Timesheet.Date; }))
                        {
                            TimePair pair = pairs.Find(delegate (TimePair p) { return p.Date == oTM_Timesheet.Date; });
                            if (pair.ClockIN != null)
                                oTM_Timesheet.ClockIn = pair.ClockIN.EventTime;
                            if (pair.ClockOUT != null)
                                oTM_Timesheet.ClockOut = pair.ClockOUT.EventTime;
                        }
                    }
                }
            }

            dictMWS.Clear();
            dictMWS = null;
            if (listDWS != null)
                listDWS.Clear();
            listDWS = null;
            empData = null;
            oMWS = null;

            return TM_TimesheetList;
        }



        public List<TimeElement> LoadTimeElementArchive(string EmployeeID, DateTime BeginDate, DateTime EndDate, int RecordCount)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.LoadArchiveEvent(EmployeeID, BeginDate, EndDate, RecordCount);
        }

        public List<TimeElement> LoadTimeElement(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            return LoadTimeElement(EmployeeID, BeginDate, EndDate, -1, true, true);
        }

        public List<TimeElement> LoadTimeElement(string EmployeeID, DateTime BeginDate, DateTime EndDate, int RecordCount, bool LoadRealtime)
        {
            return LoadTimeElement(EmployeeID, BeginDate, EndDate, RecordCount, LoadRealtime, false);
        }

        public List<TimeElement> LoadTimeElement(string EmployeeID, DateTime BeginDate, DateTime EndDate, int RecordCount, bool LoadRealtime, bool LoadArchive)
        {
            List<TimeElement> oReturn = new List<TimeElement>();
            ////Edit by Koissares 20180408 
            ////Always Load Real-Time because No DATA in TimeEventArchive
            //load realtime
            if (LoadRealtime)
            {
                oReturn.AddRange(LoadTimeElementRealtime(EmployeeID, BeginDate, EndDate, RecordCount));
            }
            else
            {
                // load archive;
                List<TimeElement> buffer = LoadTimeElementArchive(EmployeeID, BeginDate, EndDate, RecordCount);
                foreach (TimeElement item in buffer)
                {
                    item.SetElementType(TimeElementType.ACCEPT);
                    oReturn.Add(item);
                }
            }

            #region old code 20180408
            //// load last archive date
            //DateTime lastArchive = LoadArchiveDate(EmployeeID);
            //DateTime date1, date2;
            //if (EndDate < lastArchive)
            //{
            //    date1 = BeginDate;
            //    date2 = EndDate;
            //    // load archive;
            //    if (LoadArchive)
            //    {
            //        List<TimeElement> buffer = LoadTimeElementArchive(EmployeeID, date1, date2, RecordCount);
            //        foreach (TimeElement item in buffer)
            //        {
            //            item.SetElementType(TimeElementType.ACCEPT);
            //            oReturn.Add(item);
            //        }
            //    }
            //}
            //else if (BeginDate < lastArchive)
            //{
            //    date1 = lastArchive;
            //    date2 = EndDate;
            //    //load realtime
            //    if (LoadRealtime)
            //    {
            //        oReturn.AddRange(LoadTimeElementRealtime(EmployeeID, date1, date2, RecordCount));
            //    }
            //    if ((oReturn.Count < RecordCount && RecordCount > 0 && LoadArchive) || RecordCount == -1)
            //    {
            //        date1 = BeginDate;
            //        date2 = lastArchive;
            //        // load archive;
            //        List<TimeElement> buffer = LoadTimeElementArchive(EmployeeID, date1, date2, RecordCount);
            //        foreach (TimeElement item in buffer)
            //        {
            //            item.SetElementType(TimeElementType.ACCEPT);
            //            oReturn.Add(item);
            //        }
            //    }
            //}
            //else
            //{
            //    date1 = BeginDate;
            //    date2 = EndDate;
            //    //load realtime
            //    if (LoadRealtime)
            //    {
            //        oReturn.AddRange(LoadTimeElementRealtime(EmployeeID, date1, date2, RecordCount));
            //    }
            //}
            #endregion

            return oReturn;
        }

        public List<CardSetting> LoadCardSetting(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.LoadCardSetting(EmployeeID, BeginDate, EndDate);
        }

        public List<ClockinLateClockoutEarly> CalculateTM_InLateOutEarly(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            List<string> EmployeeIDList = new List<string>();
            EmployeeIDList.Add(EmployeeID);
            return CalculateTM_InLateOutEarly(EmployeeIDList, BeginDate, EndDate);
        }

        public List<ClockinLateClockoutEarly> CalculateTM_InLateOutEarly(List<string> EmployeeIDList, DateTime BeginDate, DateTime EndDate)
        {
            List<ClockinLateClockoutEarly> oReturn = new List<ClockinLateClockoutEarly>();

            CultureInfo oCL = new CultureInfo("en-US");
            DateTime lastDate = DateTime.MinValue;
            DateTime FlexLate = DFlexLate; //ESS.HR.TM.OTManagement.FlexLate;
            bool isShift = false;
            bool isNorm = false;
            bool isFlex = false;
            bool AbsenceOrAttendanceAllday = false;
            ClockinLateClockoutEarly oClockinLateClockoutEarly;
            Dictionary<string, MonthlyWS> dictMWS = new Dictionary<string, MonthlyWS>();
            MonthlyWS oMWS = null;
            List<DailyWS> listDWS = new List<DailyWS>();
            DataTable dtDWS = new DataTable();

            Dictionary<INFOTYPE0007, List<DailyFlexTime>> dictInfty7FlexTime = new Dictionary<INFOTYPE0007, List<DailyFlexTime>>();
            Dictionary<DateTime, List<INFOTYPE2001>> absenceDict = new Dictionary<DateTime, List<INFOTYPE2001>>();
            Dictionary<DateTime, List<INFOTYPE2002>> attendanceDict = new Dictionary<DateTime, List<INFOTYPE2002>>();
            List<INFOTYPE2001> oAbsenceList = new List<INFOTYPE2001>();
            List<INFOTYPE2002> oAttendanceList = new List<INFOTYPE2002>();
            List<TimePair> oTimePair = new List<TimePair>();
            Dictionary<string, List<EMPLOYEE.CONFIG.TM.Substitution>> dictSubstitution = new Dictionary<string, List<EMPLOYEE.CONFIG.TM.Substitution>>();

            foreach (string EmployeeID in EmployeeIDList)
            {
                INFOTYPE0007 info0007 = null;
                EmployeeData empData = null;
                try
                {
                    //Console.WriteLine(string.Format("+++ EmployeeID: {0} From {1} To {2} +++", EmployeeID, BeginDate.ToString("dd/MM/yyyy", oCL), EndDate.ToString("dd/MM/yyyy", oCL)));

                    INFOTYPE2001 oINFOTYPE2001 = new INFOTYPE2001();
                    oAbsenceList = oINFOTYPE2001.GetDataFromDB(EmployeeID, BeginDate.AddDays(-1), EndDate.AddDays(2), true);
                    INFOTYPE2002 oINFOTYPE2002 = new INFOTYPE2002();
                    oAttendanceList = oINFOTYPE2002.GetDataFromDB(EmployeeID, BeginDate.AddDays(-1), EndDate.AddDays(2), true);
                    oTimePair = GetTM_TimePairList(EmployeeID, BeginDate, EndDate);

                    #region Set AbsenceDict/AttendanceDict

                    foreach (INFOTYPE2001 absenceItem in oAbsenceList)
                    {
                        for (DateTime runDate = absenceItem.BeginDate; runDate <= absenceItem.EndDate; runDate = runDate.AddDays(1))
                        {
                            if (!absenceDict.ContainsKey(runDate))
                            {
                                absenceDict[runDate] = new List<INFOTYPE2001>();
                            }
                            absenceDict[runDate].Add(absenceItem);
                        }
                    }

                    foreach (INFOTYPE2002 attendanceItem in oAttendanceList)
                    {
                        for (DateTime runDate = attendanceItem.BeginDate; runDate <= attendanceItem.EndDate; runDate = runDate.AddDays(1))
                        {
                            if (!attendanceDict.ContainsKey(runDate))
                            {
                                attendanceDict[runDate] = new List<INFOTYPE2002>();
                            }
                            attendanceDict[runDate].Add(attendanceItem);
                        }
                    }


                    #endregion

                    #region Calculate loop
                    foreach (TimePair timePair in oTimePair)
                    {
                        AbsenceOrAttendanceAllday = false;
                        oClockinLateClockoutEarly = new ClockinLateClockoutEarly();
                        oClockinLateClockoutEarly.EmployeeID = EmployeeID;
                        oClockinLateClockoutEarly.Date = timePair.Date;

                        if (empData == null || empData.OrgAssignment == null || timePair.Date > empData.OrgAssignment.EndDate)
                        {
                            empData = new EmployeeData(EmployeeID, timePair.Date);
                            if (empData.OrgAssignment == null)
                                continue;
                        }
                        if (info0007 == null || info0007.BeginDate > timePair.Date || info0007.EndDate < timePair.Date)
                        {
                            info0007 = GetWorkSchedule(timePair.EmployeeID, timePair.Date);
                            if (info0007 == null)
                                continue;
                        }

                        DailyWS oDWS = new DailyWS();
                        oDWS = TimesheetManagement.GetDailyWSFromCollectedData(timePair.Date, empData, info0007, ref dictMWS, ref dictSubstitution, ref listDWS);
                        if (oDWS != null)
                        {
                            timePair.DWS = oDWS;
                            timePair.DWSCode = oDWS.DailyWorkscheduleCode;
                            timePair.IsDayOff = oDWS.DailyWorkscheduleCode.StartsWith("OFF");
                            timePair.IsHoliday = oDWS.IsHoliday;
                        }

                        if (!String.IsNullOrEmpty(timePair.DWSCode))
                        {
                            if (timePair.DWSCode.StartsWith("S"))
                            {
                                isShift = true;
                                isNorm = isFlex = false;
                            }
                            else if (timePair.DWSCode.StartsWith("N"))
                            {
                                isNorm = true;
                                isShift = isFlex = false;
                            }
                            else if (timePair.DWSCode.StartsWith("F"))
                            {
                                isFlex = true;
                                isShift = isNorm = false;
                            }
                            else
                            {
                                isShift = isNorm = isFlex = false;
                            }
                        }
                        else
                        {
                            isShift = isNorm = isFlex = false;
                        }

                        //Add by Koissares 20170529
                        if (isFlex)
                        {
                            if (!dictInfty7FlexTime.ContainsKey(info0007))
                            {
                                List<DailyFlexTime> oDailyFlexTime = GetDailyFlexTimeByFlexCode(info0007.WFRule);
                                dictInfty7FlexTime.Add(info0007, oDailyFlexTime);
                            }
                        }

                        if (lastDate != timePair.Date)
                        {
                            if (timePair.IsDayOff || timePair.IsHoliday)
                            {
                                oClockinLateClockoutEarly.Type = timePair.DWSCode;
                            }
                            else
                            {
                                if (isFlex)
                                {
                                    FlexLate = DFlexLate;
                                    if (timePair.ClockIN == null)
                                    {
                                        if (dictInfty7FlexTime.ContainsKey(info0007) && dictInfty7FlexTime[info0007] != null)
                                        {
                                            DailyFlexTime oSelectedFlexTime = dictInfty7FlexTime[info0007][0];
                                            timePair.DWS.WorkBeginTime = new TimeSpan(int.Parse(oSelectedFlexTime.BeginTime.Substring(0, 2)), int.Parse(oSelectedFlexTime.BeginTime.Substring(3, 2)), 0);
                                            timePair.DWS.WorkEndTime = new TimeSpan(int.Parse(oSelectedFlexTime.EndTime.Substring(0, 2)), int.Parse(oSelectedFlexTime.EndTime.Substring(3, 2)), 0);
                                            oClockinLateClockoutEarly.Type = string.Format("{0} ({1}-{2})", timePair.DWSCode, oSelectedFlexTime.BeginTime.Substring(0, 5), oSelectedFlexTime.EndTime.Substring(0, 5));
                                        }
                                        else
                                            oClockinLateClockoutEarly.Type = string.Format("{0} ({1}-{2})", timePair.DWSCode, TimeSpanToString(timePair.DWS.WorkBeginTime, oCL), TimeSpanToString(timePair.DWS.WorkEndTime, oCL));

                                    }
                                    else
                                    {
                                        DateTime MinClockIN = timePair.ClockIN.EventTime;

                                        List<INFOTYPE2002> attendance = null;
                                        if (attendanceDict.ContainsKey(timePair.Date))
                                        {
                                            attendance = attendanceDict[timePair.Date];

                                            if (attendance != null && attendance.Count > 0)
                                            {
                                                int i = 0;
                                                for (i = 0; i < attendance.Count; i++)
                                                {
                                                    //MinTime
                                                    if (MinClockIN > (attendance[i].BeginDate + attendance[i].BeginTime))
                                                        MinClockIN = (attendance[i].BeginDate + attendance[i].BeginTime);
                                                }
                                            }
                                        }

                                        if (absenceDict.ContainsKey(timePair.Date))
                                        {
                                            foreach (INFOTYPE2001 _INFOTYPE2001 in absenceDict[timePair.Date])
                                            {
                                                if (!_INFOTYPE2001.AllDayFlag)
                                                {
                                                    BreakPattern oBreak = EmployeeManagement.CreateInstance(CompanyCode).GetBreakPattern(timePair.DWS.DailyWorkscheduleGrouping, timePair.DWS.BreakCode);
                                                    if (oBreak != null)
                                                    {
                                                        if (_INFOTYPE2001.BeginDate.Add(_INFOTYPE2001.BeginTime) < timePair.Date.Add(oBreak.EndBreak))
                                                            MinClockIN = timePair.Date.Add(timePair.DWS.WorkBeginTime);
                                                    }
                                                    else
                                                    {
                                                        if (_INFOTYPE2001.BeginDate.Add(_INFOTYPE2001.BeginTime) < timePair.Date.Add(timePair.DWS.WorkEndTime).AddHours(-1 * (double)timePair.DWS.WorkHours / 2))
                                                            MinClockIN = timePair.Date.Add(timePair.DWS.WorkBeginTime);
                                                    }
                                                }
                                                else
                                                {
                                                    MinClockIN = timePair.Date.Add(timePair.DWS.WorkBeginTime);
                                                }
                                            }
                                        }

                                        if (dictInfty7FlexTime.ContainsKey(info0007) && dictInfty7FlexTime[info0007] != null)
                                        {
                                            DailyFlexTime oSelectedFlexTime = dictInfty7FlexTime[info0007][dictInfty7FlexTime[info0007].Count - 1];
                                            foreach (DailyFlexTime oFlexTime in dictInfty7FlexTime[info0007])
                                            {
                                                TimeSpan tsBeginTime = TimeSpan.Parse(oFlexTime.BeginTime);
                                                if (MinClockIN.TimeOfDay < tsBeginTime)
                                                {
                                                    oSelectedFlexTime = oFlexTime;
                                                    break;
                                                }
                                            }

                                            timePair.DWS.WorkBeginTime = new TimeSpan(int.Parse(oSelectedFlexTime.BeginTime.Substring(0, 2)), int.Parse(oSelectedFlexTime.BeginTime.Substring(3, 2)), 0);
                                            timePair.DWS.WorkEndTime = new TimeSpan(int.Parse(oSelectedFlexTime.EndTime.Substring(0, 2)), int.Parse(oSelectedFlexTime.EndTime.Substring(3, 2)), 0);
                                            oClockinLateClockoutEarly.Type = string.Format("{0} ({1}-{2})", timePair.DWSCode, oSelectedFlexTime.BeginTime.Substring(0, 5), oSelectedFlexTime.EndTime.Substring(0, 5));

                                            FlexLate = timePair.Date.Add(new TimeSpan(int.Parse(oSelectedFlexTime.BeginTime.Substring(0, 2)), int.Parse(oSelectedFlexTime.BeginTime.Substring(3, 2)), 0));
                                        }
                                    }
                                }
                                else
                                {
                                    oClockinLateClockoutEarly.Type = string.Format("{0} ({1}-{2})", timePair.DWSCode, TimeSpanToString(timePair.DWS.WorkBeginTime, oCL), TimeSpanToString(timePair.DWS.WorkEndTime, oCL));
                                }

                            }
                        }
                        else
                        {
                            oClockinLateClockoutEarly.Type = "\"";
                        }

                        //Clock IN
                        if (timePair.ClockIN != null)
                        {
                            oClockinLateClockoutEarly.ClockIn = timePair.ClockIN.EventTime;
                            oClockinLateClockoutEarly.IsPlusClockIn = (timePair.ClockIN.EventTime.Date > timePair.Date);
                        }

                        //Clock OUT
                        if (timePair.ClockOUT != null)
                        {
                            oClockinLateClockoutEarly.ClockOut = timePair.ClockOUT.EventTime;
                            oClockinLateClockoutEarly.IsPlusClockOut = (timePair.ClockOUT.EventTime.Date > timePair.Date);
                        }

                        // check isLate  isEarly  isAbsence
                        bool isLate = false;
                        bool isEarly = false;
                        bool isAbsence = false;

                        #region Check isLate? isEarly? isAbsence?

                        if (!timePair.DWSCode.StartsWith("OFF") && !timePair.DWSCode.StartsWith("HOL") && timePair.Date <= DateTime.Now)
                        {
                            List<INFOTYPE2002> attendance = new List<INFOTYPE2002>();
                            if (attendanceDict.ContainsKey(timePair.Date))
                            {
                                DateTime dtWorkBegin = timePair.DWS.WorkBeginTime == TimeSpan.MinValue ? timePair.Date.Date : timePair.Date.Add(timePair.DWS.WorkBeginTime);
                                attendance = attendanceDict[timePair.Date].FindAll(delegate (INFOTYPE2002 all) { return all.AllDayFlag || all.EndDate.Add(all.EndTime) >= dtWorkBegin || (all.EndTime <= all.BeginTime && all.EndDate.AddDays(1).Add(all.EndTime) >= dtWorkBegin); });
                            }
                            if (timePair.DWS.WorkBeginTime >= timePair.DWS.WorkEndTime)
                            {
                                if (attendanceDict.ContainsKey(timePair.Date.AddDays(1)))
                                {
                                    List<INFOTYPE2002> attendanceNextDay = attendanceDict[timePair.Date.AddDays(1)].FindAll(delegate (INFOTYPE2002 tp) { return tp.BeginTime < timePair.DWS.WorkEndTime && !tp.AllDayFlag; });
                                    if (attendanceNextDay != null)
                                        attendance.AddRange(attendanceNextDay);
                                }
                            }
                            if (attendance != null && attendance.Count > 0)
                            {
                                int i = 0;
                                for (i = 0; i < attendance.Count; i++)
                                {
                                    if (!AbsenceOrAttendanceAllday)
                                        AbsenceOrAttendanceAllday = attendance[i].AllDayFlag;

                                    //Add by Koissares 20160310  ���Ѻ�ͧ���ҡТ����ѹ
                                    DateTime dtBeginAttendance = new DateTime();
                                    DateTime dtEndAttendance = new DateTime();
                                    dtBeginAttendance = attendance[i].BeginDate + attendance[i].BeginTime;
                                    if (attendance[i].BeginDate == attendance[i].EndDate && attendance[i].BeginTime > attendance[i].EndTime)
                                    {
                                        dtEndAttendance = attendance[i].EndDate.AddDays(1) + attendance[i].EndTime;
                                    }
                                    else
                                    {
                                        dtEndAttendance = attendance[i].EndDate + attendance[i].EndTime;
                                    }

                                    //normalize clockin, clockout
                                    if (timePair.ClockIN == null)
                                    {
                                        timePair.ClockIN = new TimeElement();
                                        timePair.ClockIN.EventTime = dtBeginAttendance;
                                    }
                                    if (timePair.ClockOUT == null)
                                    {
                                        timePair.ClockOUT = new TimeElement();
                                        timePair.ClockOUT.EventTime = timePair.ClockIN.EventTime; //�к��ШѺ�������Clockin��͹ �ҧ��EventTime������Ҩ���Դ�ҡ�ʡ��͡ �������º���ҡѺattendance.EndTime
                                    }

                                    //MinBeginLate
                                    if (timePair.ClockIN.EventTime > dtBeginAttendance)
                                        timePair.ClockIN.EventTime = dtBeginAttendance;
                                    //MaxEnd
                                    if (timePair.ClockOUT.EventTime < dtEndAttendance)
                                        timePair.ClockOUT.EventTime = dtEndAttendance;
                                }
                            }
                            else
                            {
                                //normalize clockin, clockout
                                if (timePair.ClockIN == null)
                                    timePair.ClockIN = timePair.ClockOUT;
                                if (timePair.ClockOUT == null)
                                    timePair.ClockOUT = timePair.ClockIN;
                            }

                            DateTime workBeginTime = timePair.Date.Add(timePair.DWS.WorkBeginTime);

                            if (info0007.TimeEvaluateClass == "1") //Add by Koissares 20161219
                            {
                                DateTime dtWorkBegin = timePair.DWS.WorkBeginTime == TimeSpan.MinValue ? timePair.Date.Date : timePair.Date.Add(timePair.DWS.WorkBeginTime);
                                DateTime dtWorkEnd = timePair.DWS.WorkEndTime == TimeSpan.MinValue ? timePair.Date.Date : timePair.Date.Add(timePair.DWS.WorkEndTime);
                                if (dtWorkEnd < dtWorkBegin)
                                    dtWorkEnd = dtWorkEnd.AddDays(1);

                                INFOTYPE2001 absence = null;

                                #region New Method use List by Koissares 20160226
                                if (oAbsenceList != null)
                                {
                                    List<INFOTYPE2001> overlapAbsences = new List<INFOTYPE2001>();  //Edit by Koissares 20170731
                                    overlapAbsences = oAbsenceList.FindAll(delegate (INFOTYPE2001 abs)
                                    {
                                        return (!abs.AllDayFlag && abs.BeginDate.Add(abs.BeginTime) <= dtWorkEnd && abs.EndDate.Add(abs.EndTime) >= dtWorkBegin)
                                              || (abs.AllDayFlag && abs.BeginDate <= timePair.Date && abs.EndDate >= timePair.Date);
                                    });
                                    foreach (INFOTYPE2001 absenceItem in overlapAbsences)
                                    {
                                        absence = absenceItem;
                                        DateTime BeginAbsence = absence.BeginDate.Add(absence.BeginTime);

                                        ListItem[] WorkTime = DivideDailyWorkSchedule(EmployeeID, 2, false, timePair.Date);

                                        DateTime MorningTimeBegin = Convert.ToDateTime(WorkTime[0].Text);
                                        DateTime MorningTimeEnd = Convert.ToDateTime(WorkTime[1].Text);
                                        DateTime NoonTimeBegin = Convert.ToDateTime(WorkTime[2].Text);
                                        DateTime NoonTimeEnd = Convert.ToDateTime(WorkTime[3].Text);
                                        if (isNorm)
                                        {
                                            BreakPattern oBreak = EmployeeManagement.CreateInstance(CompanyCode).GetBreakPattern(timePair.DWS.DailyWorkscheduleGrouping, timePair.DWS.BreakCode);
                                            if (oBreak != null)
                                            {
                                                MorningTimeEnd = timePair.Date.Add(oBreak.BeginBreak);
                                                NoonTimeBegin = timePair.Date.Add(oBreak.EndBreak);
                                            }
                                        }
                                        else if (isFlex)
                                        {
                                            DailyFlexTime oSelectedFlexTime = dictInfty7FlexTime[info0007][dictInfty7FlexTime[info0007].Count - 1];
                                            foreach (DailyFlexTime oFlexTime in dictInfty7FlexTime[info0007])
                                            {
                                                TimeSpan tsBeginTime = new TimeSpan(int.Parse(oFlexTime.BeginTime.Substring(0, 2)), int.Parse(oFlexTime.BeginTime.Substring(3, 2)), 0);
                                                TimeSpan tsEndTime = new TimeSpan(int.Parse(oFlexTime.EndTime.Substring(0, 2)), int.Parse(oFlexTime.EndTime.Substring(3, 2)), 0);
                                                if (absence.BeginTime == tsBeginTime || absence.EndTime == tsEndTime)
                                                {
                                                    oSelectedFlexTime = oFlexTime;
                                                    MorningTimeBegin = MorningTimeBegin.Date.Add(tsBeginTime);
                                                    break;
                                                }
                                            }
                                            if (oSelectedFlexTime.BreakCode != null)
                                            {
                                                BreakPattern oBreak = EmployeeManagement.CreateInstance(CompanyCode).GetBreakPattern(timePair.DWS.DailyWorkscheduleGrouping, oSelectedFlexTime.BreakCode);
                                                if (oBreak != null)
                                                {
                                                    MorningTimeEnd = timePair.Date.Add(oBreak.BeginBreak);
                                                    NoonTimeBegin = timePair.Date.Add(oBreak.EndBreak);
                                                }
                                            }
                                        }

                                        if (!absence.AllDayFlag)
                                        {
                                            //Edit by Koissares 20170717
                                            if (NoonTimeBegin.TimeOfDay > MorningTimeBegin.TimeOfDay)
                                            {
                                                if (BeginAbsence < timePair.Date.Add(NoonTimeBegin.TimeOfDay))
                                                {
                                                    timePair.DWS.WorkBeginTime = NoonTimeBegin.TimeOfDay;
                                                    workBeginTime = timePair.Date.Add(timePair.DWS.WorkBeginTime);

                                                    FlexLate = workBeginTime;
                                                }
                                                else
                                                {
                                                    timePair.DWS.WorkEndTime = MorningTimeEnd.TimeOfDay;
                                                }
                                            }
                                            else
                                            {
                                                if (BeginAbsence < timePair.Date.Add(NoonTimeBegin.TimeOfDay).AddDays(1))
                                                {
                                                    timePair.DWS.WorkBeginTime = NoonTimeBegin.TimeOfDay;
                                                    workBeginTime = timePair.Date.Add(timePair.DWS.WorkBeginTime).AddDays(1);

                                                    FlexLate = workBeginTime;
                                                }
                                                else
                                                {
                                                    timePair.DWS.WorkEndTime = MorningTimeEnd.TimeOfDay;
                                                }
                                            }

                                            if (timePair.DWS.WorkBeginTime == timePair.DWS.WorkEndTime ||
                                                (timePair.DWS.WorkBeginTime == NoonTimeBegin.TimeOfDay && timePair.DWS.WorkEndTime == MorningTimeEnd.TimeOfDay))
                                                AbsenceOrAttendanceAllday = true;
                                        }
                                        else
                                        {
                                            if (absenceItem.BeginDate <= timePair.Date && absenceItem.EndDate >= timePair.Date)//Fix by Koissares 20160831
                                            {
                                                if (!AbsenceOrAttendanceAllday)
                                                    AbsenceOrAttendanceAllday = absence.AllDayFlag;
                                                timePair.DWS.WorkBeginTime = timePair.DWS.WorkEndTime;
                                            }
                                        }
                                    }
                                }

                                DateTime workEndTime = timePair.Date.Add(timePair.DWS.WorkEndTime);

                                if (workBeginTime >= workEndTime)
                                    workEndTime = workEndTime.AddDays(1);

                                #endregion


                                Boolean AbsenceAllDayFlag = false;
                                if (absence != null)
                                    AbsenceAllDayFlag = absence.AllDayFlag;

                                #region " Calculate Flag isLate isEarly isAbsence invalidClockInOut"
                                if (timePair.ClockIN != timePair.ClockOUT) //if ClockIn or ClockOut is null, ClockIn==ClockOut
                                {
                                    //check Early
                                    if (!timePair.DWS.IsDayOffOrHoliday && (workEndTime > timePair.ClockOUT.EventTime)) //&& !AbsenceAllDayFlag
                                        isEarly = true;

                                    //Employee Type just like Norm,Shift,Flex
                                    string Emp_Type = timePair.DWSCode;

                                    if (isNorm) //Norm
                                    {
                                        //check late
                                        if (workBeginTime < timePair.ClockIN.EventTime)// && timePair.ClockIN.EventTime <= workEndTime)
                                        {
                                            isLate = true;
                                        }
                                        else
                                        {
                                            //is attendance fix late
                                            if (attendanceDict.ContainsKey(timePair.Date))
                                            {
                                                DateTime beginLate = timePair.ClockIN.EventTime;

                                                foreach (INFOTYPE2002 item1 in attendanceDict[timePair.Date])
                                                {
                                                    //normalize attendance time
                                                    DateTime beginAttend = timePair.Date.Add(item1.BeginTime);
                                                    if (timePair.DWS.WorkBeginTime > timePair.DWS.WorkEndTime)
                                                    {
                                                        beginAttend = beginAttend.AddDays(1);
                                                    }
                                                    if (!item1.AllDayFlag && beginLate > beginAttend)
                                                    {
                                                        isLate = true;
                                                        //break;
                                                    }
                                                    else
                                                    {
                                                        isLate = false;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else if (isShift) //Shift
                                    {
                                        //check late
                                        if (workBeginTime < timePair.ClockIN.EventTime)//Add by Koissares 20160218
                                        {
                                            isLate = true;
                                        }
                                        else
                                        {
                                            //is attendance fix late
                                            if (attendanceDict.ContainsKey(timePair.Date))
                                            {
                                                DateTime beginLate = timePair.ClockIN.EventTime;

                                                foreach (INFOTYPE2002 item1 in attendanceDict[timePair.Date])
                                                {
                                                    //normalize attendance time
                                                    DateTime beginAttend = timePair.Date.Add(item1.BeginTime);
                                                    if (timePair.DWS.WorkBeginTime > timePair.DWS.WorkEndTime)
                                                    {
                                                        beginAttend = beginAttend.AddDays(1);
                                                    }
                                                    if (!item1.AllDayFlag && beginLate > beginAttend)
                                                    {
                                                        isLate = true;
                                                        //break;
                                                    }
                                                    else
                                                    {
                                                        isLate = false;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else if (isFlex) //Flex
                                    {
                                        //check late
                                        if (FlexLate < timePair.ClockIN.EventTime)// && timePair.ClockIN.EventTime <= workEndTime)
                                        {
                                            isLate = true;
                                        }
                                        else
                                        {
                                            //is attendance fix late
                                            if (attendanceDict.ContainsKey(timePair.Date))
                                            {
                                                DateTime beginLate = timePair.ClockIN.EventTime;

                                                foreach (INFOTYPE2002 item1 in attendanceDict[timePair.Date])
                                                {
                                                    //normalize attendance time
                                                    DateTime beginAttend = timePair.Date.Add(item1.BeginTime);
                                                    if (timePair.DWS.WorkBeginTime > timePair.DWS.WorkEndTime)
                                                    {
                                                        beginAttend = beginAttend.AddDays(1);
                                                    }
                                                    if (!item1.AllDayFlag && beginLate > beginAttend)
                                                    {
                                                        isLate = true;
                                                        //break;
                                                    }
                                                    else
                                                    {
                                                        isLate = false;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }//end if timePair.ClockIN != null && timePair.ClockOUT != null

                                ////Check Absence
                                //if ((!AbsenceOrAttendanceAllday) && (timePair.ClockIN == null || timePair.ClockOUT == null || timePair.ClockIN == timePair.ClockOUT))
                                //    isAbsence = true;

                                //Check Absence LNG ClockNoneWork LNG Version 20171212 Koissares
                                if ((!AbsenceOrAttendanceAllday) && timePair.ClockIN == null && timePair.ClockOUT == null)
                                {
                                    isAbsence = true;
                                }
                                else
                                {
                                    isAbsence = false;
                                }
                                #endregion Calculate Flag TimeEvaluateClass == "1"
                            }
                            else
                            {
                                isAbsence = false;
                            }
                        }

                        ////SET remark ClockinLateClockoutEarly
                        #region SET remark ABSENCETYPE/ATTENDANCETYPE
                        if (absenceDict.ContainsKey(timePair.Date))
                        {
                            oClockinLateClockoutEarly.IsRemark = true;
                            List<INFOTYPE2001> absenceAll = absenceDict[timePair.Date];
                            INFOTYPE2001 absenceMatchAllDay; List<INFOTYPE2001> absenceMatchHalfDay;
                            absenceMatchAllDay = absenceAll.Find(delegate (INFOTYPE2001 e) { return e.AbsenceDays > Convert.ToDecimal(0.5); });
                            if (absenceMatchAllDay != null)
                            {
                                oClockinLateClockoutEarly.IsRemark = true;
                                oClockinLateClockoutEarly.CategoryCode = "ABSENCETYPE";
                                oClockinLateClockoutEarly.TextCode = absenceMatchAllDay.AbsenceType;
                                oClockinLateClockoutEarly.BeginDate = absenceMatchAllDay.BeginDate.Add(absenceMatchAllDay.BeginTime).ToString("ddMMyyyy HH:mm:ss", oCL);
                                oClockinLateClockoutEarly.EndDate = absenceMatchAllDay.EndDate.Add(absenceMatchAllDay.EndTime).ToString("ddMMyyyy HH:mm:ss", oCL);
                                oClockinLateClockoutEarly.PayrollDays = absenceMatchAllDay.PayrollDays;//Add .ToString() by Koissares 20171106
                                //oClockinLateClockoutEarly.PayrollDays = absenceMatchAllDay.PayrollDays.ToString();//Add .ToString() by Koissares 20171106
                            }
                            absenceMatchHalfDay = absenceAll.FindAll(delegate (INFOTYPE2001 e) { return e.AbsenceDays <= Convert.ToDecimal(0.5); });
                            foreach (INFOTYPE2001 inf2001 in absenceMatchHalfDay)
                            {
                                if (!string.IsNullOrEmpty(oClockinLateClockoutEarly.CategoryCode)) oClockinLateClockoutEarly.CategoryCode += ",";
                                if (!string.IsNullOrEmpty(oClockinLateClockoutEarly.TextCode)) oClockinLateClockoutEarly.TextCode += ",";
                                if (!string.IsNullOrEmpty(oClockinLateClockoutEarly.BeginDate)) oClockinLateClockoutEarly.BeginDate += ",";
                                if (!string.IsNullOrEmpty(oClockinLateClockoutEarly.EndDate)) oClockinLateClockoutEarly.EndDate += ",";
                                oClockinLateClockoutEarly.CategoryCode += "ABSENCETYPE";
                                oClockinLateClockoutEarly.TextCode += inf2001.AbsenceType;
                                oClockinLateClockoutEarly.BeginDate += inf2001.BeginDate.Add(inf2001.BeginTime).ToString("ddMMyyyy HH:mm:ss", oCL);
                                if (inf2001.EndDate.Add(inf2001.EndTime).Date > inf2001.BeginDate.Date)
                                    oClockinLateClockoutEarly.EndDate += inf2001.BeginDate.Add(inf2001.EndTime).AddDays(-1).ToString("ddMMyyyy HH:mm:ss", oCL);
                                else
                                    oClockinLateClockoutEarly.EndDate += inf2001.EndDate.Add(inf2001.EndTime).ToString("ddMMyyyy HH:mm:ss", oCL);
                                oClockinLateClockoutEarly.PayrollDays += inf2001.PayrollDays;
                            }
                        }

                        if (attendanceDict.ContainsKey(timePair.Date))
                        {
                            oClockinLateClockoutEarly.IsRemark = true;
                            List<INFOTYPE2002> attendanceAll = attendanceDict[timePair.Date];
                            INFOTYPE2002 attendanceMatchAllDay; List<INFOTYPE2002> attendanceMatchHalfDay;
                            attendanceMatchAllDay = attendanceAll.Find(delegate (INFOTYPE2002 e) { return (e.AttendanceDays > Convert.ToDecimal(0.5)) && e.AttendanceType != "2040" && e.AttendanceType != "2050"; });
                            if (attendanceMatchAllDay != null)
                            {
                                oClockinLateClockoutEarly.CategoryCode = "ATTENDANCETYPE";
                                oClockinLateClockoutEarly.TextCode = attendanceMatchAllDay.AttendanceType;
                                oClockinLateClockoutEarly.BeginDate = attendanceMatchAllDay.BeginDate.Add(attendanceMatchAllDay.BeginTime).ToString("ddMMyyyy HH:mm:ss", oCL);
                                oClockinLateClockoutEarly.EndDate = attendanceMatchAllDay.EndDate.Add(attendanceMatchAllDay.EndTime).ToString("ddMMyyyy HH:mm:ss", oCL);
                                oClockinLateClockoutEarly.PayrollDays = attendanceMatchAllDay.PayrollDays;//Add .ToString() by Koissares 20171106
                                //oClockinLateClockoutEarly.PayrollDays = attendanceMatchAllDay.PayrollDays.ToString();//Add .ToString() by Koissares 20171106
                            }
                            attendanceMatchHalfDay = attendanceAll.FindAll(delegate (INFOTYPE2002 e) { return (e.AttendanceDays <= Convert.ToDecimal(0.5)) || e.AttendanceType == "2040" || e.AttendanceType == "2050"; });
                            foreach (INFOTYPE2002 inf2002 in attendanceMatchHalfDay)
                            {
                                oClockinLateClockoutEarly.IsRemark = true;
                                if (!string.IsNullOrEmpty(oClockinLateClockoutEarly.CategoryCode)) oClockinLateClockoutEarly.CategoryCode += ",";
                                if (!string.IsNullOrEmpty(oClockinLateClockoutEarly.TextCode)) oClockinLateClockoutEarly.TextCode += ",";
                                if (!string.IsNullOrEmpty(oClockinLateClockoutEarly.BeginDate)) oClockinLateClockoutEarly.BeginDate += ",";
                                if (!string.IsNullOrEmpty(oClockinLateClockoutEarly.EndDate)) oClockinLateClockoutEarly.EndDate += ",";
                                oClockinLateClockoutEarly.CategoryCode += "ATTENDANCETYPE";
                                oClockinLateClockoutEarly.TextCode += inf2002.AttendanceType;
                                oClockinLateClockoutEarly.BeginDate += inf2002.BeginDate.Add(inf2002.BeginTime).ToString("ddMMyyyy HH:mm:ss", oCL);
                                if (inf2002.EndDate.Add(inf2002.EndTime).Date > inf2002.BeginDate.Date)
                                    oClockinLateClockoutEarly.EndDate += inf2002.BeginDate.Add(inf2002.EndTime).AddDays(-1).ToString("ddMMyyyy HH:mm:ss", oCL);
                                else
                                    oClockinLateClockoutEarly.EndDate += inf2002.EndDate.Add(inf2002.EndTime).ToString("ddMMyyyy HH:mm:ss", oCL);
                                oClockinLateClockoutEarly.PayrollDays += inf2002.PayrollDays;
                            }
                        }
                        #endregion

                        if (AbsenceOrAttendanceAllday)
                        {
                            isLate = false;
                            isEarly = false;
                        }

                        //Add by Koissares 20170605
                        //����ʴ� flag �ͧ�ѹ�Ѩ�غѹ
                        if (timePair.Date >= DateTime.Now.Date)
                        {
                            isLate = false;
                            isEarly = false;
                            isAbsence = false;
                        }
                        else if (info0007.TimeEvaluateClass == "1" && !AbsenceOrAttendanceAllday && !timePair.DWS.IsDayOffOrHoliday)
                        {
                            //ClockAbnormal LNG Version 20171212 Koissares
                            //Check ClockAbnormal
                            if (!isAbsence && (timePair.ClockIN == null || timePair.ClockOUT == null || timePair.ClockIN == timePair.ClockOUT))
                            {
                                oClockinLateClockoutEarly.ClockAbnormal = true;
                            }
                        }
                        #endregion

                        ////SET flag ClockinLateClockoutEarly
                        oClockinLateClockoutEarly.ClockInLate = isLate;
                        oClockinLateClockoutEarly.ClockoutEarly = isEarly;
                        oClockinLateClockoutEarly.ClockNoneWork = isAbsence;

                        oReturn.Add(oClockinLateClockoutEarly);
                        lastDate = timePair.Date;
                    }
                    #endregion calculate loop

                    absenceDict.Clear();
                    attendanceDict.Clear();
                    dictSubstitution.Clear();
                }
                catch (Exception e)
                {
                    Console.WriteLine("FAIL >>> EmployeeID: " + EmployeeID + "\r\nError: " + e.Message + "\r\nInnerException: " + (e.InnerException == null ? "" : e.InnerException.Message));
                }
            }

            #region Clear Data
            oAbsenceList = null;
            oAttendanceList = null;
            dictMWS.Clear();
            dictMWS = null;

            dictInfty7FlexTime.Clear();
            dictInfty7FlexTime = null;
            absenceDict = null;
            attendanceDict = null;
            dictSubstitution = null;
            #endregion

            return oReturn;
        }

        public List<TimeElement> GetTimeElement(string CardNo, DateTime BeginDate, DateTime EndDate, int RecordCount)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetTimeElement(CardNo, BeginDate, EndDate, RecordCount);
        }


        public ITimeEventService GetTimeEventService(string ServiceCode)
        {
            ITimeEventService oService = null;
            Type oReturn = null;
            Assembly oAssembly;
            string assemblyName = string.Format("ESS.TIMESHEET.{0}", ServiceCode.Trim().ToUpper());
            string typeName = string.Format("ESS.TIMESHEET.TIMEEVENT.{0}.TIMEEVENTSERVICE", ServiceCode.Trim().ToUpper());
            oAssembly = Assembly.Load(assemblyName);
            oReturn = oAssembly.GetType(typeName);
            if (oReturn != null)
            {
                oService = (ITimeEventService)Activator.CreateInstance(oReturn);
            }
            return oService;
        }
        public List<string> GetYearAttendance()
        {
            List<string> lstYear = new List<string>();
            int start = DateTime.Now.Month < 10 ? 0 : -1;
            for (int index = 0; index < 1; index++)
            {
                int year = (DateTime.Now.Year - index);
                lstYear.Add(year.ToString("0000"));
            }
            return lstYear;
        }
        public List<TimeElement> LoadTimeElementRealtime(string EmployeeID, DateTime BeginDate, DateTime EndDate, int RecordCount)
        {
            List<TimeElement> oReturn = new List<TimeElement>();
            foreach (CardSetting CS in LoadCardSetting(EmployeeID, BeginDate, EndDate))
            {
                DateTime date1, date2;
                date1 = BeginDate < CS.BeginDate ? CS.BeginDate : BeginDate;
                date2 = EndDate < CS.EndDate ? EndDate : CS.EndDate;
                ITimeEventService oService = ServiceManager.GetTimeEventService(CS.Location);
                //oReturn.AddRange(oService.GetTimeElement(CS.CardNo, date1, date2, RecordCount));
                oReturn.AddRange(GetTimeElement(CS.CardNo, date1, date2, RecordCount));
            }
            foreach (TimeElement item in oReturn)
            {
                item.EmployeeID = EmployeeID;
            }
            return oReturn;
        }

        public string ConvertFlexTimeText(string EmpID, DailyWS DWS, DateTime dDate, TimeSpan BeginTime)
        {
            string strFlex = string.Empty;
            EmployeeData _EmployeeData = new EmployeeData(EmpID, dDate);
            if (_EmployeeData.EmpSubAreaType == EmployeeSubAreaType.Flex)
            {
                INFOTYPE0007 oInfotype0007 = _EmployeeData.GetEmployeeWF(_EmployeeData.EmployeeID, dDate);
                if (!string.IsNullOrEmpty(oInfotype0007.WFRule))
                {
                    DailyFlexTime oDFX = HRTMManagement.CreateInstance(CompanyCode).GetDailyFlexTimeMinForCalculateByFlexCode(oInfotype0007.WFRule);

                    BreakPattern oBreak = new BreakPattern();
                    oBreak = EmployeeManagement.CreateInstance(CompanyCode).GetBreakPattern(DWS.DailyWorkscheduleGrouping, oDFX.BreakCode);
                    if (oBreak.BeginBreak > BeginTime)
                    {
                        strFlex = GetCommonText("HRTMABSENCE", WorkflowPrinciple.Current.UserSetting.Language, "AMTIME");
                    }
                    else
                    {
                        strFlex = GetCommonText("HRTMABSENCE", WorkflowPrinciple.Current.UserSetting.Language, "PMTIME");
                    }
                }
            }
            return strFlex;
        }


        public class ListTimesheetModel
        {
            public ListTimesheetModel()
            {
                TimeElements = new List<TimeElement>();
                Timesheets = new List<TM_Timesheet>();
                TimePairs = new List<TimePair>();
            }
            public List<TimeElement> TimeElements { get; set; }
            public List<TM_Timesheet> Timesheets { get; set; }
            public List<TimePair> TimePairs { get; set; }
        }
        public ListTimesheetModel GetListTimesheet(string PeriodValue, EmployeeData oEmp, out List<INFOTYPE2001> oAbs, out List<INFOTYPE2002> oAtt)
        //public ListTimesheetModel GetListTimesheet(string PeriodValue, EmployeeData oEmp)
        {
            DateTime BeginDate = DateTime.ParseExact(PeriodValue, "yyyyMM", oCL);
            DateTime EndDate = BeginDate.AddMonths(1).AddMinutes(-1);

            List<TimeElement> oTimeElement = LoadTimeElement(oEmp.EmployeeID, BeginDate.AddDays(-1), EndDate.AddDays(2), -1, true, true);
            List<TimeElement> oTimeElementList = LoadTimeElement(oEmp.EmployeeID, BeginDate.AddDays(-1), EndDate.AddDays(2), -1, true, true);
            // List<TM_Timesheet> TM_TimesheetList = GetTM_Timesheet(oEmp.EmployeeID, BeginDate, EndDate, oTimeElementList);  // �ͧ��������


            List<TM_Timesheet> oTM_Timesheet = GetTM_Timesheet(oEmp.EmployeeID, BeginDate, EndDate);


            INFOTYPE2001 oINFOTYPE2001 = new INFOTYPE2001();
            INFOTYPE2002 oINFOTYPE2002 = new INFOTYPE2002();
            List<INFOTYPE2001> oAbsenceList = oINFOTYPE2001.GetData(oEmp, BeginDate.AddDays(-1), EndDate.AddDays(2));
            List<INFOTYPE2002> oAttendanceList = oINFOTYPE2002.GetData(oEmp, BeginDate.AddDays(-1), EndDate.AddDays(2));

            oAbs = oAbsenceList;
            oAtt = oAttendanceList;

            Dictionary<DateTime, string> DictAttendanceType0192 = new Dictionary<DateTime, string>();
            foreach (INFOTYPE2002 item in oAttendanceList)
                if (item.AttendanceType == "0192")
                    if (!DictAttendanceType0192.ContainsKey(item.BeginDate.Date))
                        DictAttendanceType0192.Add(item.BeginDate.Date, "0");

            List<TimePair> pairs = MatchingClockNew(oEmp.EmployeeID, oTimeElement, true, BeginDate, EndDate, DictAttendanceType0192);

            Dictionary<DateTime, string> tmpATT = new Dictionary<DateTime, string>();
            List<DateTime> tmpDate = new List<DateTime>();
            if (pairs.Count > 0)
            {
                foreach (var item in pairs)
                {
                    var absence = oAbsenceList.Where(a => a.BeginDate.Date == item.Date).OrderBy(s => s.BeginTime).ToList();
                    if (absence.Count == 0)
                    {
                        var list_attendance = oAttendanceList.Where(a => a.BeginDate.Date <= item.Date && a.EndDate.Date >= item.Date).ToList();
                        if (list_attendance.Count > 0)
                        {
                            int count_attendance = 0;
                            int count_comma_attendance = 0;
                            foreach (var data_attendance in list_attendance)
                            {
                                if (data_attendance.Status == "RECORDED")
                                {
                                    string concat_remark_attendance = string.Empty;
                                    //string concat_remark_attendance = GetAttendanceTypeText(data_attendance.AttendanceType);
                                    //if (!data_attendance.AllDayFlag)
                                    if (!(data_attendance.PayrollDays >= 1))
                                    {
                                        string stringFlex = ConvertFlexTimeText(item.EmployeeID, item.DWS, data_attendance.BeginDate, data_attendance.BeginTime);
                                        if (string.IsNullOrEmpty(stringFlex))
                                        {


                                            List<TimePair> tmpTimePairList = pairs.FindAll(delegate (TimePair tmp) { return tmp.Date >= data_attendance.BeginDate.AddDays(-1) && tmp.Date <= data_attendance.BeginDate; });
                                            List<DateTimeIntersec> oPeriod = new List<DateTimeIntersec>();
                                            foreach (TimePair itemTP in tmpTimePairList)
                                            {

                                                DateTimeIntersec tmpPeriod = new DateTimeIntersec();
                                                tmpPeriod.Begin1 = itemTP.Date.Add(itemTP.DWS.WorkBeginTime);
                                                tmpPeriod.End1 = itemTP.Date.Add(itemTP.DWS.WorkEndTime);
                                                if (tmpPeriod.Begin1 > tmpPeriod.End1)
                                                {
                                                    tmpPeriod.End1 = tmpPeriod.End1.AddDays(1);
                                                }
                                                oPeriod.Add(tmpPeriod);
                                            }
                                            foreach (DateTimeIntersec itemDTI in oPeriod)
                                            {
                                                itemDTI.Begin2 = data_attendance.BeginDate.Add(data_attendance.BeginTime);
                                                itemDTI.End2 = data_attendance.BeginDate.Add(data_attendance.EndTime);
                                                if (itemDTI.IsIntersecOT())
                                                {
                                                    data_attendance.BeginDate = itemDTI.Begin1.Date;
                                                    data_attendance.EndDate = itemDTI.Begin1.Date;
                                                }
                                            }

                                            if (data_attendance.BeginDate != item.Date)
                                            {
                                                string sAttDesc = GetAttendanceTypeText(data_attendance.AttendanceType, oEmp.OrgAssignment.SubAreaSetting.AbsAttGrouping) + " " + data_attendance.BeginTime.ToString().Substring(0, 5) + "-" + data_attendance.EndTime.ToString().Substring(0, 5);
                                                if (!tmpATT.ContainsKey(data_attendance.BeginDate))
                                                    tmpATT.Add(data_attendance.BeginDate, sAttDesc);

                                                tmpDate.Add(item.Date);
                                            }
                                            else
                                            {
                                                // �Ѻ�ͧ���Ҥ����ѹ
                                                //item.Remark += " " + data_attendance.BeginTime.ToString().Substring(0, 5) + "-" + data_attendance.EndTime.ToString().Substring(0, 5);
                                                concat_remark_attendance += GetAttendanceTypeText(data_attendance.AttendanceType, oEmp.OrgAssignment.SubAreaSetting.AbsAttGrouping) + " " + data_attendance.BeginTime.ToString().Substring(0, 5) + "-" + data_attendance.EndTime.ToString().Substring(0, 5);
                                            }
                                        }
                                        else
                                        {
                                            //item.Remark += " " + stringFlex;
                                            concat_remark_attendance += GetAttendanceTypeText(data_attendance.AttendanceType, oEmp.OrgAssignment.SubAreaSetting.AbsAttGrouping) + " " + stringFlex;
                                        }

                                        // Check Clok In - Out �ҡ���˵�
                                        if (item.ClockIN != null || item.ClockOUT != null)
                                            item.IsCheckRemark = true;

                                        // �Ѻ�ͧ�����ҡ���� 1 ����� 1 �ѹ�� �Ѻ�ͧ������� - ����
                                        if (count_attendance > 0)
                                            item.IsCheckRemark = true;

                                        count_attendance++;
                                    }
                                    else
                                    {
                                        // �Ѻ�ͧ��������ѹ 
                                        item.IsCheckRemark = true;
                                        concat_remark_attendance += GetAttendanceTypeText(data_attendance.AttendanceType, oEmp.OrgAssignment.SubAreaSetting.AbsAttGrouping) + " " + GetCommonText("HRTMABSENCE", WorkflowPrinciple.Current.UserSetting.Language, "FULLDAY");
                                        item.Remark += GetAttendanceTypeText(data_attendance.AttendanceType, oEmp.OrgAssignment.SubAreaSetting.AbsAttGrouping) + " " + GetCommonText("HRTMABSENCE", WorkflowPrinciple.Current.UserSetting.Language, "FULLDAY");
                                        //item.Remark += " " + GetCommonText("HRTMABSENCE", WorkflowPrinciple.Current.UserSetting.Language, "FULLDAY");
                                    }

                                    // �ͧ�Ѻ comma �Ѻ�ͧ���ҵ���� 2 ��¡��
                                    if (count_comma_attendance > 0)
                                        item.Remark = item.Remark + " , " + concat_remark_attendance;
                                    else
                                        item.Remark = concat_remark_attendance;

                                    count_comma_attendance++;
                                }
                            }
                        }

                        #region �ͧ���
                        //var attendance = oAttendanceList.FirstOrDefault(a => a.BeginDate.Date <= item.Date && a.EndDate.Date >= item.Date);
                        //if (attendance != null)
                        //{
                        //    if (attendance.Status == "RECORDED")
                        //    {
                        //        item.Remark = GetAttendanceTypeText(attendance.AttendanceType);
                        //        if (!attendance.AllDayFlag)
                        //        {
                        //            string stringFlex = ConvertFlexTimeText(item.EmployeeID, item.DWS, attendance.BeginDate, attendance.BeginTime);
                        //            if(string.IsNullOrEmpty(stringFlex))
                        //            {
                        //                // �Ѻ�ͧ���Ҥ����ѹ
                        //                item.Remark += " " + attendance.BeginTime.ToString().Substring(0, 5) + "-" + attendance.EndTime.ToString().Substring(0, 5);
                        //            }
                        //            else
                        //            {
                        //                item.Remark += " " + stringFlex;
                        //            }

                        //            // Check Clok In - Out �ҡ���˵�
                        //            if(item.ClockIN != null || item.ClockOUT != null)
                        //            {
                        //                item.IsCheckRemark = true;
                        //            }
                        //        }
                        //        else
                        //        {
                        //            // �Ѻ�ͧ��������ѹ 
                        //            item.IsCheckRemark = true;
                        //            item.Remark += " " + GetCommonText("HRTMABSENCE", WorkflowPrinciple.Current.UserSetting.Language, "FULLDAY");
                        //        }
                        //    }
                        //}
                        #endregion
                    }
                    else
                    {
                        // Absence
                        int countMixAbsenceAttendance = 0;
                        int ischeck_comma = 0;
                        foreach (var item_absence in absence)
                        {
                            if (item_absence.Status == "RECORDED")
                            {
                                string concat_text_absence = GetAbsenceTypeText(item_absence.AbsenceType, oEmp.OrgAssignment.SubAreaSetting.AbsAttGrouping);
                                //item.Remark = GetAbsenceTypeText(item_absence.AbsenceType);

                                //if (!item_absence.AllDayFlag)
                                if (!(item_absence.PayrollDays >= 1))
                                {
                                    string stringFlex = ConvertFlexTimeText(item.EmployeeID, item.DWS, item_absence.BeginDate, item_absence.BeginTime);
                                    if (string.IsNullOrEmpty(stringFlex))
                                    {
                                        //item.Remark += " " + item_absence.BeginTime.ToString().Substring(0, 5) + "-" + item_absence.EndTime.ToString().Substring(0, 5);
                                        concat_text_absence += " " + item_absence.BeginTime.ToString().Substring(0, 5) + "-" + item_absence.EndTime.ToString().Substring(0, 5);
                                    }
                                    else
                                    {
                                        concat_text_absence += " " + stringFlex;
                                    }

                                    if (item.ClockIN != null || item.ClockOUT != null)
                                    {
                                        item.IsCheckRemark = true;
                                    }

                                    // �ա���� 2 ��ǧ���ҵԴ
                                    if (countMixAbsenceAttendance > 0)
                                        item.IsCheckRemark = true;
                                }
                                else
                                {
                                    // ������ѹ 
                                    item.IsCheckRemark = true;
                                    concat_text_absence += " " + GetCommonText("HRTMABSENCE", WorkflowPrinciple.Current.UserSetting.Language, "FULLDAY");
                                }

                                if (ischeck_comma > 0)
                                    item.Remark = item.Remark + " , " + concat_text_absence;
                                else
                                    item.Remark = concat_text_absence;

                                ischeck_comma++;
                                countMixAbsenceAttendance++;
                            }
                        }

                        // Attendance
                        var list_attendance = oAttendanceList.Where(a => a.BeginDate.Date <= item.Date && a.EndDate.Date >= item.Date).ToList();
                        if (list_attendance.Count > 0)
                        {
                            foreach (var data_attendance in list_attendance)
                            {
                                if (data_attendance.Status == "RECORDED")
                                {
                                    string concat_remark_attendance = GetAttendanceTypeText(data_attendance.AttendanceType, oEmp.OrgAssignment.SubAreaSetting.AbsAttGrouping);

                                    //if (!data_attendance.AllDayFlag)
                                    if (!(data_attendance.PayrollDays >= 1))
                                    {
                                        string stringFlex = ConvertFlexTimeText(item.EmployeeID, item.DWS, data_attendance.BeginDate, data_attendance.BeginTime);
                                        if (string.IsNullOrEmpty(stringFlex))
                                        {
                                            // �Ѻ�ͧ���Ҥ����ѹ
                                            //item.Remark += " " + data_attendance.BeginTime.ToString().Substring(0, 5) + "-" + data_attendance.EndTime.ToString().Substring(0, 5);
                                            concat_remark_attendance += " " + data_attendance.BeginTime.ToString().Substring(0, 5) + "-" + data_attendance.EndTime.ToString().Substring(0, 5);
                                        }
                                        else
                                        {
                                            //item.Remark += " " + stringFlex;
                                            concat_remark_attendance += " " + stringFlex;
                                        }

                                        // Check Clok In - Out �ҡ���˵�
                                        if (item.ClockIN != null || item.ClockOUT != null)
                                            item.IsCheckRemark = true;
                                    }
                                    else
                                    {
                                        // �Ѻ�ͧ��������ѹ 
                                        item.IsCheckRemark = true;
                                        item.Remark += " " + GetCommonText("HRTMABSENCE", WorkflowPrinciple.Current.UserSetting.Language, "FULLDAY");
                                    }

                                    // �ͧ�Ѻ comma �Ѻ�ͧ���ҵ���� 2 ��¡��
                                    if (ischeck_comma > 0)
                                        item.Remark = item.Remark + " , " + concat_remark_attendance;
                                    else
                                        item.Remark = concat_remark_attendance;

                                    // �ա���Ҥ����ѹ���� ������Ѻ�ͺ�ͧ���ҵ���ա�����ѹ ���ʶҹ� Policy �� true
                                    if (countMixAbsenceAttendance > 0)
                                        item.IsCheckRemark = true;

                                    ischeck_comma++;
                                }
                            }
                        }

                        #region Attendance �ͧ���
                        // Check �Ѻ�ͧ�����ա�������ͧ�ҡ � 1 �ѹ�Ҩ���ա���� ����Ѻ�ͧ���Ҵ���
                        //var attendance = oAttendanceList.FirstOrDefault(a => a.BeginDate.Date <= item.Date && a.EndDate.Date >= item.Date);
                        //if (attendance != null)
                        //{
                        //    if (attendance.Status == "RECORDED")
                        //    {
                        //        //item.Remark = GetAttendanceTypeText(attendance.AttendanceType);
                        //        string concat_text_attendance = GetAttendanceTypeText(attendance.AttendanceType);
                        //        if (!attendance.AllDayFlag)
                        //        {
                        //            string stringFlex = ConvertFlexTimeText(item.EmployeeID, item.DWS, attendance.BeginDate, attendance.BeginTime);
                        //            if (string.IsNullOrEmpty(stringFlex))
                        //            {
                        //                //item.Remark += " " + attendance.BeginTime.ToString().Substring(0, 5) + "-" + attendance.EndTime.ToString().Substring(0, 5);
                        //                concat_text_attendance += " " + attendance.BeginTime.ToString().Substring(0, 5) + "-" + attendance.EndTime.ToString().Substring(0, 5);
                        //            }
                        //            else
                        //            {
                        //                concat_text_attendance += " " + stringFlex;
                        //            }

                        //            if (ischeck_comma > 0)
                        //                item.Remark = item.Remark + " , " + concat_text_attendance;
                        //            else
                        //                item.Remark = concat_text_attendance;

                        //            // �ա���Ҥ����ѹ���� ������Ѻ�ͺ�ͧ���ҵ���ա�����ѹ ���ʶҹ� Policy �� true
                        //            if (countMixAbsenceAttendance > 0)
                        //                item.IsCheckRemark = true;

                        //            ischeck_comma++;
                        //        }
                        //    }
                        //}
                        #endregion
                    }
                }
            }

            foreach (var item in tmpATT)
            {
                foreach (var itemPairs in pairs.Where(w => w.Date == item.Key))
                {
                    if (string.IsNullOrEmpty(itemPairs.Remark))
                    {
                        itemPairs.Remark += item.Value;
                    }
                    else
                    {
                        itemPairs.Remark += " , " + item.Value;
                    }

                    if (itemPairs.ClockIN != null || itemPairs.ClockOUT != null)
                    {
                        itemPairs.IsCheckRemark = true;
                    }
                    else
                    {
                        itemPairs.IsCheckRemark = false;
                    }
                }
            }

            if (tmpDate.Count > 0)
            {
                foreach (DateTime dt in tmpDate)
                {
                    foreach (var iPairs in pairs.Where(z => z.Date == dt))
                    {
                        if (string.IsNullOrEmpty(iPairs.Remark) && (iPairs.ClockIN == null || iPairs.ClockOUT == null))
                        {
                            iPairs.IsCheckRemark = false;
                        }
                    }
                }
            }

            var oResult = new ListTimesheetModel()
            {
                TimeElements = oTimeElementList,
                //Timesheets = TM_TimesheetList,
                TimePairs = pairs,
                Timesheets = oTM_Timesheet
            };
            return oResult;
        }

        public ListTimesheetModel GetListTimesheet(DateTime BeginDate, DateTime EndDate, EmployeeData oEmp)
        {
            List<TimeElement> oTimeElementList = LoadTimeElement(oEmp.EmployeeID, BeginDate.AddDays(-1), EndDate.AddDays(2), -1, true, true);
            List<TM_Timesheet> TM_TimesheetList = GetTM_Timesheet(oEmp.EmployeeID, BeginDate, EndDate, oTimeElementList);

            var oResult = new ListTimesheetModel()
            {
                TimeElements = oTimeElementList,
                Timesheets = TM_TimesheetList
            };
            return oResult;
        }

        public List<INFOTYPE2001> GetListAbsence(EmployeeData oEmp, string CompanyCode, int YearSelect)
        {
            List<INFOTYPE2001> oListINFOTYPE2001 = new List<INFOTYPE2001>();
            int currentYear = DateTime.Now.Year;
            DateTime oCurrentDate = new DateTime(YearSelect, 1, 1);
            INFOTYPE2001 oINFOTYPE2001 = new INFOTYPE2001();
            oListINFOTYPE2001 = oINFOTYPE2001.GetData(oEmp, oCurrentDate, oCurrentDate.AddYears(1));

            return oListINFOTYPE2001;
        }

        public List<INFOTYPE2002> GetListAttendance(EmployeeData oEmp, string CompanyCode)
        {
            List<INFOTYPE2002> oListINFOTYPE2002 = new List<INFOTYPE2002>();
            int currentYear = DateTime.Now.Year;
            DateTime oCurrentDate = new DateTime(currentYear, 1, 1);
            INFOTYPE2002 oINFOTYPE2002 = new INFOTYPE2002();
            oListINFOTYPE2002 = oINFOTYPE2002.GetData(oEmp, oCurrentDate, oCurrentDate.AddYears(1));

            return oListINFOTYPE2002;
        }

        public bool ValidateListAttendance(List<INFOTYPE2002> tmpListAttendance, INFOTYPE2002 newAttendance, EmployeeData oEmp, int rowId, string DocumentState, string Language, out string msg) //���ǹ DocumentState ����觨ҡ State �ͧ�͡��ù�� ���Һѹ�֡�����á����� String.Empty 
        {
            msg = string.Empty;
            if (newAttendance.BeginDate == DateTime.MinValue)
            {
                msg = GetCommonText("HRTM_EXCEPTION", Language, "ATTENDANCE_DATE__NULL");
                return false;
            }
            if (newAttendance.Remark == "")
            {
                msg = GetCommonText("HRTM_EXCEPTION", Language, "ATTENDANCE_REASON_NULL");
                return false;
            }

            foreach (INFOTYPE2002 oAtt in tmpListAttendance)
            {
                if (tmpListAttendance.IndexOf(oAtt) == rowId)
                    continue;

                DateTime dtExBeginTime = oAtt.BeginDate.Add(oAtt.BeginTime);
                DateTime dtExEndTime = oAtt.EndDate.Add(oAtt.EndTime);
                //if (dtExEndTime <= dtExBeginTime)
                //    dtExEndTime = dtExEndTime.AddDays(1);
                if (dtExEndTime < dtExBeginTime)
                {
                    dtExEndTime = dtExEndTime.AddDays(1);
                }
                else
                {
                    if(dtExEndTime == dtExBeginTime)
                    {
                        dtExEndTime = dtExEndTime.AddDays(1).AddSeconds(-1);
                    }
                    else
                    {
                        if(dtExEndTime.Date != dtExBeginTime.Date)
                        {
                            dtExEndTime = dtExEndTime.AddDays(1).AddSeconds(-1);
                        }
                    }
                }

                DateTime dtBeginTime = newAttendance.BeginDate.Add(newAttendance.BeginTime);
                DateTime dtEndTime = newAttendance.EndDate.Add(newAttendance.EndTime);
                //if (dtEndTime <= dtBeginTime)
                //    dtEndTime = dtEndTime.AddDays(1);

                //if (dtEndTime <= dtBeginTime)
                //    dtEndTime = dtEndTime.AddDays(1).AddSeconds(-1);


                if (dtEndTime < dtBeginTime)
                {
                    dtEndTime = dtEndTime.AddDays(1);
                }
                else
                {
                    if (dtEndTime == dtBeginTime)
                    {
                        dtEndTime = dtEndTime.AddDays(1).AddSeconds(-1);
                    }
                    else
                    {
                        if (dtEndTime.Date != dtBeginTime.Date)
                        {
                            dtEndTime = dtEndTime.AddDays(1).AddSeconds(-1);
                        }
                    }
                }


                if (dtExBeginTime < dtEndTime && dtExEndTime > dtBeginTime) //Fix for continuous time ex 08:00-12:00 + 12:00-17:30  by Koissares 2016/06/20
                {
                    msg = GetCommonText("HRTM_EXCEPTION", Language, "DUPLICATE_ATTENDANCE");
                    return false;
                }

            }

            if (DocumentState != "WAITFOREDIT")
            {
                try
                {
                    AttendanceCreatingRule oACR = GetCreatingRule(oEmp, newAttendance.AttendanceType, newAttendance.BeginDate, newAttendance.EndDate);
                    oACR.IsCanCreate();
                }

                catch (AttendanceDayOffException e)
                {
                    throw new DataServiceException("HRTM_EXCEPTION", "VALIDATE_DAYOFF", "", e);
                }
                catch (AttendanceCreatingException ex)
                {
                    string Err = ShowErrorAttendanceCreating(oEmp.EmployeeID, Language, ex);
                    throw new Exception(Err);
                }
                catch (Exception e)
                {
                    msg = GetCommonText("HRTM_EXCEPTION", Language, e.Message);
                    return false;
                }
            }
            return true;
        }

        //public List<string> GetYearAttendance(RequestParameter oRequestParameter)
        //{
        //    EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
        //    SetAuthenticate(oCurrentEmployee);

        //    return new List<string>();
        //}

        public string ShowErrorAttendanceCreating(string EmployeeID, string LanguageCode, AttendanceCreatingException ex)
        {
            string oError = string.Empty;
            string strError = GetCommonText("HRTM_EXCEPTION", LanguageCode, ex.ErrMessage);
            //string strAttendanceType = GetCommonText("ATTENDANCETYPE", LanguageCode, "92#" + ex.AttendanceType);
            string strAttendanceType = GetCommonText("ATTENDANCETYPE", LanguageCode, ex.AbsAttGrouping + "#" + ex.AttendanceType);
            if (ex.IsPast)
            {
                if (strError.Contains("{1}"))
                    oError = string.Format(strError, strAttendanceType, ex.OffsetValue.ToString());
                else
                    oError = string.Format(strError, strAttendanceType);
            }
            else
            {
                if (strError.Contains("{1}"))
                    oError = string.Format(strError, strAttendanceType, ex.AdvanceValue.ToString());
                else
                    oError = string.Format(strError, strAttendanceType);
            }
            return oError;
        }

        public AbsAttCalculateResult ValidateData(DateTime DocumentDate, INFOTYPE2002 oAttendance, EmployeeData oEmp)
        {
            // validate begin - end
            DateTime date1, date2;
            if (oAttendance.AllDayFlag)
            {
                date1 = oAttendance.BeginDate;
                date2 = oAttendance.EndDate;
            }
            else
            {
                date1 = oAttendance.BeginDate.Add(oAttendance.BeginTime);
                date2 = oAttendance.EndDate.Add(oAttendance.EndTime);
                if (date2 < date1)
                {
                    date2 = date2.AddDays(1);
                }
            }
            if (date1 > date2)
            {
                throw new Exception("BEGINDATE_GREATER_THAN_ENDDATE");
            }



            // validate AbsenceType
            //AttendanceType oAttendanceType = HRTMManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetAttendanceType(this.EmployeeID, this.AttendanceType, "");
            AttendanceType oAttendanceType = HRTMManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetAttendanceType(oAttendance.EmployeeID, oAttendance.AttendanceType, "TH");
            if (oAttendanceType.Key == null)
            {
                throw new Exception(string.Format("AttendanceType '{0}' for EmpID '{1}' not found", oAttendance.AttendanceType, oAttendance.EmployeeID));
            }




            AttendanceCreatingRule oACR = HRTMManagement.CreateInstance(CompanyCode).GetCreatingRule(oEmp, oAttendance.AttendanceType, oAttendance.BeginDate, oAttendance.EndDate);

            #region Check requested absence or attendance is in process?
            DateTime tmpBeginDate = oAttendance.BeginDate.Add(oAttendance.BeginTime);
            DateTime tmpEndDate = oAttendance.EndDate.Add(oAttendance.EndTime);
            if (tmpEndDate < tmpBeginDate)
                tmpEndDate = tmpEndDate.AddDays(1);
            else if (oAttendance.AllDayFlag)
                tmpEndDate = tmpEndDate.AddDays(1).AddSeconds(-1);

            string ExistingDate = string.Empty;
            List<INFOTYPE2001> listInfotype2001 = new INFOTYPE2001().GetData(oEmp, tmpBeginDate, tmpEndDate);
            if (listInfotype2001.Exists(delegate (INFOTYPE2001 item) { return item.RequestNo != oAttendance.RequestNo; }))
            {
                INFOTYPE2001 ExistingItem = listInfotype2001.Find(delegate (INFOTYPE2001 item) { return item.RequestNo != oAttendance.RequestNo; });
                if (ExistingItem.AllDayFlag)
                {
                    if (ExistingItem.BeginDate <= oAttendance.EndDate && ExistingItem.EndDate >= oAttendance.BeginDate)
                    {
                        //����¡�ë�ӡѺ
                        if (ExistingItem.BeginDate.Date == ExistingItem.EndDate.Date)//���ѹ����
                        {
                            ExistingDate = ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US"));
                        }
                        else//�������ѹ
                        {
                            ExistingDate = string.Format("{0} - {1}", ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")), ExistingItem.EndDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")));
                        }
                        throw new Exception("ABSENCE_IN_PROCESS|" + ExistingDate);
                    }
                }
                else
                {
                    DateTime ExistingBegin = ExistingItem.BeginDate.Add(ExistingItem.BeginTime);
                    DateTime ExistingEnd = ExistingItem.EndDate.Add(ExistingItem.EndTime);
                    if (ExistingEnd < ExistingBegin)
                        ExistingEnd = ExistingEnd.AddDays(1);

                    if (tmpBeginDate <= ExistingEnd && tmpEndDate >= ExistingBegin)   //Check Overlap 20171214 Koissares
                    {
                        //����¡�ë�ӡѺ
                        if (ExistingItem.BeginDate.Date == ExistingItem.EndDate.Date)//���ѹ����
                        {
                            ExistingDate = ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")) + " " + ExistingItem.BeginTime.ToString() + "-" + ExistingItem.EndTime.ToString(); ;
                        }
                        else//�������ѹ
                        {
                            ExistingDate = string.Format("{0} - {1}", ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")), ExistingItem.EndDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")));
                        }
                        throw new Exception("ABSENCE_IN_PROCESS|" + ExistingDate);
                    }
                }

            }

            INFOTYPE2002 oINFOTYPE2002 = new INFOTYPE2002();
            List<INFOTYPE2002> listInfotype2002 = oINFOTYPE2002.GetData(oEmp, tmpBeginDate, tmpEndDate);
            if (listInfotype2002.Exists(delegate (INFOTYPE2002 item) { return item.RequestNo != oAttendance.RequestNo; }))
            {
                INFOTYPE2002 ExistingItem = listInfotype2002.Find(delegate (INFOTYPE2002 item) { return item.RequestNo != oAttendance.RequestNo; });
                if (ExistingItem.AllDayFlag)
                {
                    if (ExistingItem.BeginDate <= oAttendance.EndDate && ExistingItem.EndDate >= oAttendance.BeginDate)
                    {
                        //����¡�ë�ӡѺ
                        if (ExistingItem.BeginDate.Date == ExistingItem.EndDate.Date)//�Ѻ�ͧ�����ѹ����
                        {
                            ExistingDate = ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US"));
                        }
                        else//�Ѻ�ͧ���������ѹ
                        {
                            ExistingDate = string.Format("{0} - {1}", ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")), ExistingItem.EndDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")));
                        }
                        throw new Exception("ATTENDANCE_IN_PROCESS|" + ExistingDate);
                    }
                }
                else
                {
                    DateTime ExistingBegin = ExistingItem.BeginDate.Add(ExistingItem.BeginTime);
                    DateTime ExistingEnd = ExistingItem.EndDate.Add(ExistingItem.EndTime);
                    if (ExistingEnd < ExistingBegin)
                        ExistingEnd = ExistingEnd.AddDays(1);

                    if (tmpBeginDate <= ExistingEnd && tmpEndDate >= ExistingBegin)   //Check Overlap 20171214 Koissares
                    {
                        //����¡�ë�ӡѺ
                        if (ExistingItem.BeginDate.Date == ExistingItem.EndDate.Date)//�Ѻ�ͧ�����ѹ����
                        {
                            ExistingDate = ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")) + " " + ExistingItem.BeginTime.ToString() + "-" + ExistingItem.EndTime.ToString();
                        }
                        else//�Ѻ�ͧ���������ѹ
                        {
                            ExistingDate = string.Format("{0} - {1}", ExistingItem.BeginDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")), ExistingItem.EndDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")));
                        }
                        throw new Exception("ATTENDANCE_IN_PROCESS|" + ExistingDate);
                    }
                }
            }
            #endregion

            //ModifiedBy: Ratchatawan W. (2012-03-21)
            if (!(WorkflowPrinciple.Current.IsInRole("TIMEADMIN") || WorkflowPrinciple.Current.IsInRole("#MANAGER")))
                oACR.IsCanCreate();
            else if (oAttendance.EmployeeID == WorkflowPrinciple.CurrentIdentity.EmployeeID)
                oACR.IsCanCreate();

            //if (!WorkflowPrinciple.Current.IsInRole("TIMEADMIN") && !WorkflowPrinciple.Current.IsInRole("#MANAGER"))
            //{
            //    oACR.IsCanCreate();
            //}

            #region " check Time "
            if (oACR.IsLoadClockIN)
            {
                if (oAttendance.AllDayFlag)
                {
                    throw new Exception("THIS_TYPE_CAN_NOT_USE_FULL_DAY");
                }
                else
                {
                    if (!oACR.CheckClock(DoorType.IN, oAttendance.BeginDate.Add(oAttendance.BeginTime)) && !oACR.CheckClock(DoorType.OUT, oAttendance.BeginDate.Add(oAttendance.BeginTime)))
                    {
                        throw new Exception("NOT_FOUND_EVIDANCE");
                    }
                }
            }
            if (oACR.IsLoadClockOUT)
            {
                if (oAttendance.AllDayFlag)
                {
                    throw new Exception("THIS_TYPE_CAN_NOT_USE_FULL_DAY");
                }
                else
                {
                    if (!oACR.CheckClock(DoorType.IN, oAttendance.EndDate.Add(oAttendance.EndTime)) && !oACR.CheckClock(DoorType.OUT, oAttendance.EndDate.Add(oAttendance.EndTime)))
                    {
                        throw new Exception("NOT_FOUND_EVIDANCE");
                    }
                }
            }
            if (oACR.IsSwapClock)
            {
                if (oAttendance.AllDayFlag)
                {
                    throw new Exception("THIS_TYPE_CAN_NOT_USE_FULL_DAY");
                }
                else
                {
                    if (oACR.CheckClock(DoorType.IN, oAttendance.BeginDate.Add(oAttendance.BeginTime)))
                    {
                        if (!oACR.CheckClock(DoorType.IN, oAttendance.EndDate.Add(oAttendance.EndTime)))
                        {
                            throw new Exception("NOT_FOUND_EVIDANCE");
                        }
                    }
                    else if (oACR.CheckClock(DoorType.OUT, oAttendance.BeginDate.Add(oAttendance.BeginTime)))
                    {
                        if (!oACR.CheckClock(DoorType.IN, oAttendance.EndDate.Add(oAttendance.EndTime)))
                        {
                            if (!oACR.CheckClock(DoorType.OUT, oAttendance.EndDate.Add(oAttendance.EndTime)))
                            {
                                throw new Exception("NOT_FOUND_EVIDANCE");
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("NOT_FOUND_EVIDANCE");
                    }
                }
            }
            #endregion


            AbsAttCalculateResult oCal;
            AbsAttCalculateResult oResult = new AbsAttCalculateResult();
            Decimal AttendanceDays = 0.0M;
            Decimal AttendanceHours = 0.0M;
            Decimal PayrollDays = 0.0M;
            TimeSpan AttBeginTime;
            TimeSpan AttEndTime;
            if (oAttendance.BeginDate != oAttendance.EndDate && !oAttendance.AllDayFlag)
            {

                for (DateTime rundate = oAttendance.BeginDate; rundate <= oAttendance.EndDate; rundate = rundate.AddDays(1))
                {
                    if (rundate == oAttendance.EndDate)
                    {
                        oCal = CalculateAbsAttEntry(oAttendance.EmployeeID, oAttendance.SubType, rundate, rundate, oAttendance.BeginTime, oAttendance.EndTime, false, false, false);
                        AttendanceDays += oCal.AbsenceDays;
                        AttendanceHours += oCal.AbsenceHours;
                        PayrollDays += oCal.PayrollDays;
                        AttBeginTime = oCal.BeginTime;
                        AttEndTime = oCal.EndTime;
                    }
                    else if (rundate == oAttendance.EndDate.AddDays(-1) && oAttendance.BeginTime >= oAttendance.EndTime)
                    {
                        oCal = CalculateAbsAttEntry(oAttendance.EmployeeID, oAttendance.SubType, rundate, rundate, oAttendance.BeginTime, oAttendance.EndTime, false, false, false);
                        AttendanceDays += oCal.AbsenceDays;
                        AttendanceHours += oCal.AbsenceHours;
                        PayrollDays += oCal.PayrollDays;
                        AttBeginTime = oCal.BeginTime;
                        AttEndTime = oCal.EndTime;
                        break;
                    }
                    else
                    {
                        oCal = CalculateAbsAttEntry(oAttendance.EmployeeID, oAttendance.SubType, rundate, rundate.AddDays(1), oAttendance.BeginTime, oAttendance.BeginTime, true, false, false);
                        AttendanceDays += 1;
                        AttendanceHours += 24;
                        PayrollDays += 1;
                        AttBeginTime = oCal.BeginTime;
                        AttEndTime = oCal.EndTime;
                    }
                }
            }
            else
            {
                oCal = CalculateAbsAttEntry(oAttendance.EmployeeID, oAttendance.SubType, oAttendance.BeginDate, oAttendance.EndDate, oAttendance.BeginTime, oAttendance.EndTime, oAttendance.AllDayFlag, false, false);
                AttendanceDays = oCal.AbsenceDays;
                AttendanceHours = oCal.AbsenceHours;
                PayrollDays = oCal.PayrollDays;
                AttBeginTime = oCal.BeginTime;
                AttEndTime = oCal.EndTime;
            }

            oResult.AbsenceDays = AttendanceDays;
            oResult.AbsenceHours = AttendanceHours;
            oResult.PayrollDays = PayrollDays;
            oResult.BeginDate = oAttendance.BeginDate;
            oResult.EndDate = oAttendance.EndDate;
            oResult.BeginTime = oAttendance.BeginTime;
            oResult.EndTime = oAttendance.EndTime;
            oResult.FullDay = oAttendance.AllDayFlag;

            return oResult;
        }

        public INFOTYPE2002 ParseINFOTYPE2002(string value)
        {
            INFOTYPE2002_Convertor oConverter = new INFOTYPE2002_Convertor();
            return (INFOTYPE2002)oConverter.ConvertFrom(value);
        }

        public INFOTYPE2001 GetINFOTYPE2001ByRequestNo(string RequestNo)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetINFOTYPE2001ByRequestNo(RequestNo);
        }

        public AbsAttCalculateResult CalAbsence(string EmployeeID, string AbsenceType, DateTime BeginDate, DateTime EndDate, TimeSpan BeginTime, TimeSpan EndTime, bool AllDayFlag, string RequestNo)
        {
            AbsAttCalculateResult oResult = new AbsAttCalculateResult();
            INFOTYPE2001 oINF2001;
            oINF2001 = HRTMManagement.CreateInstance(CompanyCode).GetINFOTYPE2001ByRequestNo(RequestNo);
            if (oINF2001.PayrollDays != 0)
            {
                oResult.AbsenceDays = oINF2001.AbsenceDays;
                oResult.AbsenceHours = oINF2001.AbsenceHours;
                oResult.PayrollDays = oINF2001.PayrollDays;
            }
            else
            {

                if (AllDayFlag)
                {
                    oResult = HRTMManagement.CreateInstance(CompanyCode).CalculateAbsAttEntry(EmployeeID, AbsenceType, BeginDate, EndDate, TimeSpan.MinValue, TimeSpan.MinValue, true, false, false);
                }
                else
                {
                    oResult = HRTMManagement.CreateInstance(CompanyCode).CalculateAbsAttEntry(EmployeeID, AbsenceType, BeginDate, EndDate, BeginTime, EndTime, false, false, false);
                }
            }
            return oResult;
        }

        public AbsAttCalculateResult CalAbsenceIsAllDay(string EmployeeID, string AbsenceType, DateTime BeginDate, DateTime EndDate)
        {
            AbsAttCalculateResult oResult = new AbsAttCalculateResult();
            oResult = HRTMManagement.CreateInstance(CompanyCode).CalculateAbsAttEntry(EmployeeID, AbsenceType, BeginDate, EndDate, TimeSpan.MinValue, TimeSpan.MinValue, true, false, false);
            return oResult;
        }

        public class SummaryOTModel
        {
            public List<DailyOTLog> DailyOTLog { get; set; }
            public List<DailyOTLog> AllDailyOTLog { get; set; }
        }
        public SummaryOTModel GetListSummaryOT(string employeeID, DataSet ds, string sPeriod, string state)
        {
            List<DailyOTLog> oDailyOTLog = new List<DailyOTLog>();
            List<DailyOTLog> AllDailyOTLog = new List<DailyOTLog>();
            List<DailyOTLog> CancellationDialyOTLog = new List<DailyOTLog>();
            DateTime beginDate;
            if (String.IsNullOrEmpty(state))
            {
                DateTime BeginDate;
                DateTime EndDate;
                beginDate = BeginDate = DateTime.ParseExact(sPeriod.Substring(0, 6) + "01", "yyyyMMdd", oCL);
                EndDate = BeginDate.AddMonths(1).AddSeconds(-1);
                oDailyOTLog = LoadDailyOTLog(employeeID, BeginDate, EndDate, DailyOTLogStatus.WAITFORMAKESUMMARY);
                AllDailyOTLog = LoadDailyOTLog(employeeID, beginDate.AddDays(-beginDate.Day).Date, beginDate.AddDays(-beginDate.Day).Date.AddMonths(1).AddMinutes(-1));

                CancellationDialyOTLog = new List<DailyOTLog>();
                foreach (DailyOTLog item in LoadDailyOTLog(employeeID))
                    if ((DailyOTLogStatus)item.Status == DailyOTLogStatus.CANCELLED ||
                                   (DailyOTLogStatus)item.Status == DailyOTLogStatus.WAITFORCANCEL)
                        CancellationDialyOTLog.Add(item);


                foreach (DailyOTLog item in CancellationDialyOTLog)
                    for (int i = oDailyOTLog.Count - 1; i >= 0; i--)
                        if (item.RefRequestNo == oDailyOTLog[i].RequestNo || item.RequestNo == oDailyOTLog[i].RequestNo)
                            oDailyOTLog.RemoveAt(i);


                foreach (DailyOTLog item in CancellationDialyOTLog)
                    for (int i = AllDailyOTLog.Count - 1; i >= 0; i--)
                        if (item.RefRequestNo == AllDailyOTLog[i].RequestNo || item.RequestNo == AllDailyOTLog[i].RequestNo)
                            AllDailyOTLog.RemoveAt(i);
            }
            else
            {
                DailyOTLog item;
                oDailyOTLog = new List<DailyOTLog>();
                foreach (DataRow row in ds.Tables["DailyOTLog"].Rows)
                {
                    item = new DailyOTLog();
                    item.ParseToObject(row);
                    oDailyOTLog.Add(item);
                    beginDate = DateTime.ParseExact(item.BeginDate.ToString("yyyyMM", oCL) + "01", "yyyyMMdd", oCL);
                }

                CancellationDialyOTLog = new List<DailyOTLog>();
                foreach (DataRow row in ds.Tables["CancellationDialyOTLog"].Rows)
                {
                    item = new DailyOTLog();
                    item.ParseToObject(row);
                    CancellationDialyOTLog.Add(item);
                }

                AllDailyOTLog = new List<DailyOTLog>();
                foreach (DataRow row in ds.Tables["AllDailyOTLog"].Rows)
                {
                    item = new DailyOTLog();
                    item.ParseToObject(row);
                    AllDailyOTLog.Add(item);
                }
            }

            return new SummaryOTModel
            {
                DailyOTLog = oDailyOTLog,
                AllDailyOTLog = AllDailyOTLog
            };
        }

        public List<EmployeeData> GetListEmpResponsible(EmployeeData oEmp, int SubjectID)
        {
            List<EmployeeData> oEmpResponsible = new List<EmployeeData>();
            bool isVisibleItem1, isVisibleItem2 = false;
            bool isVisibleTab1, isVisibleTab2 = false;
            if (oEmp.EmpSubGroup <= OTManagement.ENDEMPSUBGROUPDAILYOT && oEmp.EmpSubGroup >= OTManagement.BEGINEMPSUBGROUPDAILYOT)
            {
                isVisibleItem1 = true;
                isVisibleTab1 = true;
            }

            if (EmployeeManagement.CreateInstance(CompanyCode).IsHaveOTSummaryPermissionByEmployeeID(oEmp.EmployeeID, DateTime.Now) || (oEmp.EmpSubGroup >= OTManagement.BEGINEMPSUBGROUPSUMMARYOT && oEmp.EmpSubGroup <= OTManagement.ENDEMPSUBGROUPSUMMARYOT))
            {
                isVisibleItem2 = true;
                isVisibleTab2 = true;
            }

            //if (oEmp.EmpSubGroup >= BEGINEMPSUBGROUPSUMMARYOT && oEmp.EmpSubGroup <= ENDEMPSUBGROUPSUMMARYOT)
            //{
            //    List<UserRoleResponseSetting> authorizeRoles = ApplicationSubject.CreateInstance(CompanyCode).LoadUserResponse(SubjectID);
            //    oEmpResponsible = oEmp.LoadUserResponsible(authorizeRoles, oEmp.EmployeeID);
            //}

            if (WorkflowPrinciple.Current.IsInRole("TIMEADMIN"))
            {
                List<UserRoleResponseSetting> authorizeRoles = ApplicationSubject.CreateInstance(CompanyCode).LoadUserResponse(SubjectID);
                oEmpResponsible = oEmp.LoadUserResponsible(authorizeRoles, oEmp.EmployeeID);
            }

            foreach (EmployeeData oEmployeeData in EmployeeManagement.CreateInstance(CompanyCode).GetOTSummaryPermissionByEmployeeID(oEmp.EmployeeID, DateTime.Now))
            {
                if (!oEmpResponsible.Exists(delegate (EmployeeData emp) { return emp.EmployeeID == oEmployeeData.EmployeeID; }))
                    oEmpResponsible.Add(oEmployeeData);
            }

            return oEmpResponsible;
        }

        public List<DailyOTLog> GetListSummaryOTEmpResponsible(List<EmployeeData> oListEmp, string sPeriod)
        {
            DateTime BeginDate, EndDate;
            List<DailyOTLog> oDailyOTLog = new List<DailyOTLog>();
            List<DailyOTLog> oTmpDailyOTLog = new List<DailyOTLog>();
            BeginDate = new DateTime(Convert.ToInt16(sPeriod.Substring(0, 4)), Convert.ToInt16(sPeriod.Substring(4, 2)), 1);
            EndDate = BeginDate.AddMonths(1).AddSeconds(-1);
            List<string> RefRequestNo = new List<string>();

            foreach (EmployeeData oEmp in oListEmp)
            {
                oTmpDailyOTLog = LoadDailyOTLog(oEmp.EmployeeID, BeginDate, EndDate);
                foreach (DailyOTLog oItem in oTmpDailyOTLog)
                {
                    //oItem.Concat = oEmp.Name;
                    oItem.Concat = oEmp.MultiLanguageName;
                    oDailyOTLog.Add(oItem);
                }

            }
            if (oDailyOTLog.Count > 0)
            {
                foreach (DailyOTLog item in oDailyOTLog)
                    if ((DailyOTLogStatus)item.Status == DailyOTLogStatus.CANCELLED
                        || (DailyOTLogStatus)item.Status == DailyOTLogStatus.WAITFORCANCEL)
                        RefRequestNo.Add(item.RefRequestNo);

                foreach (DailyOTLog item in oDailyOTLog)
                {
                    if (((DailyOTLogStatus)item.Status == DailyOTLogStatus.WAITFORMAKESUMMARY
                        || (DailyOTLogStatus)item.Status == DailyOTLogStatus.SENTSUMMARY
                        || (DailyOTLogStatus)item.Status == DailyOTLogStatus.MAKESUMMARY
                        || (DailyOTLogStatus)item.Status == DailyOTLogStatus.SUMMARYDRAFT)
                        && !RefRequestNo.Contains(item.RequestNo))
                    {
                        item.ApproveRecord++;
                        if ((DailyOTLogStatus)item.Status == DailyOTLogStatus.WAITFORMAKESUMMARY)
                            item.PayrollRecord++;
                    }
                }
            }
            return oDailyOTLog;
        }

        public bool IsCompleteCancleDailyOT(String EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.IsCompleteCancleDailyOT(EmployeeID, BeginDate, EndDate);
        }

        public void SaveOTLogStatus(DataTable data)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveOTLogStatus(data);
        }

        // List ��¡������¹���ҧ��
        public List<EMPLOYEE.CONFIG.TM.Substitution> GetSubstitutionList(EmployeeData oEmp, string sPeriod)
        {
            DateTime oCurrentDate = DateTime.ParseExact(sPeriod, "ddMMyyyy", oCL);
            List<EMPLOYEE.CONFIG.TM.Substitution> oSubstitutionList = GetSubstitutionList(oEmp.EmployeeID, oCurrentDate, oCurrentDate.AddMonths(1));

            return oSubstitutionList;
        }

        public string DataServiceExceptionProcessError(EmployeeData oEmp, string LanguageCode, Exception e)
        {
            string ErrorText = string.Empty;
            if (e is DataServiceException)
            {
                if (e.InnerException is AbsenceCreatingException)
                {
                    #region " AbsenceCreatingException "
                    AbsenceCreatingException e1 = (AbsenceCreatingException)e.InnerException;
                    object[] arguments = new object[10];
                    AbsenceType oAT = GetAbsenceType(oEmp.EmployeeID, e1.AbsenceType, LanguageCode);
                    arguments[0] = oAT.Description;
                    arguments[1] = Math.Abs(e1.OffsetValue);

                    if (e1.OffsetValue == -1 && e1.CountOnDayOff && e1.OffsetFlag == "D")
                    {
                        ErrorText = string.Format("{0}{1}", GetCommonText("HRTM_EXCEPTION", LanguageCode, "CONFLICT_RULE"), " : " + string.Format(GetCommonText("HRTM_EXCEPTION", LanguageCode, "CONFLICT_RULE_1"), arguments));
                    }
                    else if (e1.OffsetValue < 0 && e1.OffsetFlag == "D")
                    {
                        ErrorText = string.Format("{0}{1}", GetCommonText("HRTM_EXCEPTION", LanguageCode, "CONFLICT_RULE"), " : " + string.Format(GetCommonText("HRTM_EXCEPTION", LanguageCode, "CONFLICT_RULE_2"), arguments));
                    }
                    else if (e1.OffsetValue > 0 && e1.OffsetFlag == "D")
                    {
                        ErrorText = string.Format("{0}{1}", GetCommonText("HRTM_EXCEPTION", LanguageCode, "CONFLICT_RULE"), " : " + string.Format(GetCommonText("HRTM_EXCEPTION", LanguageCode, "CONFLICT_RULE_3"), arguments));
                    }
                    else if (e1.OffsetValue == 0 && e1.OffsetFlag == "D")
                    {
                        ErrorText = string.Format("{0}{1}", GetCommonText("HRTM_EXCEPTION", LanguageCode, "CONFLICT_RULE"), " : " + string.Format(GetCommonText("HRTM_EXCEPTION", LanguageCode, "CONFLICT_RULE_4"), arguments));
                    }
                    else if (e1.OffsetValue < 0 && e1.OffsetFlag == "M")
                    {
                        ErrorText = string.Format("{0}{1}", GetCommonText("HRTM_EXCEPTION", LanguageCode, "CONFLICT_RULE"), " : " + string.Format(GetCommonText("HRTM_EXCEPTION", LanguageCode, "CONFLICT_RULE_5"), arguments));
                    }
                    else if (e1.OffsetValue > 0 && e1.OffsetFlag == "M")
                    {
                        ErrorText = string.Format("{0}{1}", GetCommonText("HRTM_EXCEPTION", LanguageCode, "CONFLICT_RULE"), " : " + string.Format(GetCommonText("HRTM_EXCEPTION", LanguageCode, "CONFLICT_RULE_6"), arguments));
                    }
                    else
                    {
                        ErrorText = string.Format("{0}{1}", GetCommonText("HRTM_EXCEPTION", LanguageCode, "CONFLICT_RULE"), e1.Message == "" ? "" : " : " + e1.Message);
                    }
                    #endregion
                }
                else if (e.InnerException is TimesheetCollisionException)
                {
                    #region " TimesheetCollisionException "
                    TimesheetCollisionException e1 = (TimesheetCollisionException)e.InnerException;
                    TimesheetData data = e1.CollissionData[0];
                    string cKeyData = "";
                    switch (data.ApplicationKey.ToUpper())
                    {
                        case "LEAVE":
                            AbsenceType absenceType = GetAbsenceType(oEmp.EmployeeID, data.SubKey1, LanguageCode);
                            cKeyData = string.Format("{0} : {1}", GetCommonText("REQUESTTYPE", LanguageCode, "LEAVE"), absenceType.Description);
                            break;
                        case "ATTENDANCE":
                            AttendanceType attendanceType = GetAttendanceType(oEmp.EmployeeID, data.SubKey1, LanguageCode);
                            cKeyData = string.Format("{0} : {1}", GetCommonText("REQUESTTYPE", LanguageCode, "ATTENDANCE"), attendanceType.Description);
                            break;
                        default:
                            cKeyData = data.ApplicationKey + ":" + data.SubKey1 + ":" + data.SubKey2;
                            break;
                    }
                    string cDateFormat = "dd/MM/yyyy HH:mm";
                    if (data.FullDay)
                    {
                        cDateFormat = "dd/MM/yyyy";
                    }
                    if (string.IsNullOrEmpty(data.Detail))
                    {
                        ErrorText = string.Format(GetCommonText("HRTM_EXCEPTION", LanguageCode, "COLLISION_OCCUR_EXTERNAL_1"), cKeyData, data.BeginDate.ToString(cDateFormat, oCL), data.EndDate.ToString(cDateFormat, oCL));
                    }
                    else
                    {
                        ErrorText = string.Format(GetCommonText("HRTM_EXCEPTION", LanguageCode, "COLLISION_OCCUR_EXTERNAL_2"), cKeyData, data.BeginDate.ToString(cDateFormat, oCL), data.EndDate.ToString(cDateFormat, oCL), data.Detail);
                    }
                    #endregion
                }
                else if (e.InnerException is RequiredDocumentException)
                {
                    ErrorText = GetCommonText("HRTM_EXCEPTION", LanguageCode, "REQUIRED_DOCUMENT");
                }
                else if (e.InnerException is ValidatingException)
                {
                    ErrorText = GetCommonText("HRTM_EXCEPTION", LanguageCode, e.InnerException.Message);
                }
                else
                {
                    //DataServiceException e1 = (DataServiceException)e;
                    //string[] strErrParam = ex.Split('|');
                    //if (strErrParam.Length == 1)
                    //    ErrorText = GetCommonText("HRTM_EXCEPTION", LanguageCode, strErrParam[0]);
                    //else
                    //    ErrorText = string.Format(GetCommonText("HRTM_EXCEPTION", LanguageCode, strErrParam[0]), strErrParam[1]);

                    string[] strErrParam = e.Message.Split('|');
                    if (strErrParam.Length == 1)
                        ErrorText = GetCommonText("HRTM_EXCEPTION", LanguageCode, strErrParam[0]);
                    else
                        ErrorText = string.Format(GetCommonText("HRTM_EXCEPTION", LanguageCode, strErrParam[0]), strErrParam[1]);

                }
            }
            else
            {
                ErrorText = e.Message;
            }
            return ErrorText;
        }
        //public bool ProcessError(Exception e)
        //{
        //    if (e.InnerException is TimesheetCollisionException)
        //    {
        //        ShowError((TimesheetCollisionException)e.InnerException);
        //        return true;
        //    }
        //    if (e.InnerException is AttendanceDayOffException)
        //    {
        //        ShowError((AttendanceDayOffException)e.InnerException);
        //        return true;
        //    }

        //    //AddBy: Ratchatawan W. (2012-03-21)
        //    if (e is AttendanceCreatingException)
        //    {
        //        ShowError((AttendanceCreatingException)e);
        //        return true;
        //    }

        //    else if (e is DataServiceException)
        //    {
        //        ShowError((DataServiceException)e);
        //        return true;
        //    }
        //    else if (e.InnerException is DataServiceException)
        //    {
        //        ShowError((DataServiceException)e.InnerException);
        //        return true;
        //    }
        //    else if (e.InnerException is SqlException)
        //    {
        //        ShowError((SqlException)e.InnerException);
        //        return true;
        //    }
        //    else
        //    {
        //        ShowError(e);
        //        return true;
        //    }
        //}

        public string ShowError(string CategoryCode, string ex, string LanguageCode)
        {
            string sErr = string.Empty;
            string[] strErrParam = ex.Split('|');
            if (strErrParam.Length == 1)
                sErr = GetCommonText(CategoryCode, LanguageCode, strErrParam[0]);
            else
                sErr = string.Format(GetCommonText(CategoryCode, LanguageCode, strErrParam[0]), strErrParam[1]);

            return sErr;
        }

        public DailyWS GetListDetailDailyWs(EmployeeData oEmp, DateTime dtRunning, DateTime dtBeginDate, DateTime dtEndDate)
        {
            //DateTime dtBeginDate = DateTime.ParseExact(Period, "yyyyMM", oCL);
            //DateTime dtEndDate = dtBeginDate.AddMonths(1).AddDays(-1);

            EmployeeData emp = new EmployeeData(oEmp.EmployeeID, dtBeginDate);
            MonthlyWS oMWS = GetCalendar(emp, dtBeginDate, dtBeginDate.Year, dtBeginDate.Month, true);

            DailyWS oDWS = DailyWS.GetDailyWorkschedule(emp.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, oMWS.GetWSCode(dtRunning.Day, true), dtRunning);
            if (oMWS.GetWSCode(dtRunning.Day, false) == "1")
            {
                oDWS.DailyWorkscheduleCode = "HOL";
            }
            return oDWS;
        }

        public object RefreshSubstitutionList(EmployeeData oEmpRequest, string sEmpIDChange, DateTime RequestDate, DateTime ChangeDate, bool IsTypeSelectDay)
        {
            bool IsVisibleTypeSelectDay = false;
            bool IsVisibleTypePeriod = false;
            bool IsEnabledChangeDate = false;

            if (RequestDate.Month != ChangeDate.Month)
            {
                //ModifiedBy: Ratchatawan W. (2012-10-15)
                //����ö�Դ�Է�������š����¹�Т�����͹�� ���բ�������Ҩе�ͧ�š����¹������Թ�ѹ��� 15 �ͧ�ա��͹  ���ִ�ҡ CurrentDate
                //subDate.Value = empDate.Value.AddDays(-empDate.Value.Day).AddMonths(1);
                DateTime FirstDateOfCurrentMonth = DateTime.ParseExact(DateTime.Now.ToString("01/MM/yyyy", oCL), "dd/MM/yyyy", oCL);
                DateTime LimitDateOfSubstituteOverMonth = DateTime.ParseExact(FirstDateOfCurrentMonth.AddMonths(1).ToString("15/MM/yyyy", oCL), "dd/MM/yyyy", oCL);
                if (ChangeDate > LimitDateOfSubstituteOverMonth)
                    ChangeDate = LimitDateOfSubstituteOverMonth;
            }

            if (oEmpRequest.EmployeeID == sEmpIDChange)
            {
                sEmpIDChange = "";
            }

            List<INFOTYPE2003> oINFOTYPE2003 = new List<INFOTYPE2003>();
            if (!IsTypeSelectDay)
            {
                DateTime BeginDate = DateTime.MinValue;
                DateTime EndDate = DateTime.MinValue;
                if (RequestDate <= ChangeDate)
                {
                    BeginDate = RequestDate;
                    EndDate = ChangeDate;
                }
                else
                {
                    BeginDate = ChangeDate;
                    EndDate = RequestDate;
                }
                for (DateTime dt = BeginDate; dt <= EndDate; dt = dt.AddDays(1))
                {
                    INFOTYPE2003 item = new INFOTYPE2003();
                    item.EmployeeID = oEmpRequest.EmployeeID;
                    item.SubstituteBeginDate = item.SubstituteEndDate = item.BeginDate = item.EndDate = dt;
                    item.Substitute = sEmpIDChange;
                    item.IsOverride = false; //UT not have Override
                    oINFOTYPE2003.Add(item);
                }
            }
            else
            {
                INFOTYPE2003 item = new INFOTYPE2003();
                item.EmployeeID = oEmpRequest.EmployeeID;
                item.SubstituteBeginDate = item.SubstituteEndDate = item.BeginDate = item.EndDate = RequestDate;
                item.Substitute = sEmpIDChange;
                item.IsOverride = false; //UT not have Override
                oINFOTYPE2003.Add(item);

                if (RequestDate != ChangeDate)
                {
                    item = new INFOTYPE2003();
                    item.EmployeeID = oEmpRequest.EmployeeID;
                    item.SubstituteBeginDate = item.SubstituteEndDate = item.BeginDate = item.EndDate = ChangeDate;
                    item.Substitute = sEmpIDChange;
                    item.IsOverride = false; //UT not have Override
                    oINFOTYPE2003.Add(item);
                }
            }

            IsEnabledChangeDate = !string.IsNullOrEmpty(sEmpIDChange);

            //this.subDate.Enabled = !string.IsNullOrEmpty(sEmpIDChange);
            //this.subDate.ButtonEnabled = this.subDate.Enabled;
            //this.subDate.ShowClear = this.subDate.Enabled;
            //this.subDate.CustomQueryString = "&EmployeeID=" + sEmpIDChange;

            if (IsTypeSelectDay)
            {
                IsVisibleTypeSelectDay = true;
                IsVisibleTypePeriod = false;
            }
            else
            {
                IsVisibleTypeSelectDay = false;
                IsVisibleTypePeriod = true;
            }

            if (string.IsNullOrEmpty(sEmpIDChange)
                || string.IsNullOrEmpty(oEmpRequest.EmployeeID)
                || RequestDate == DateTime.MinValue
                || ChangeDate == DateTime.MinValue
                || !IsEnabledChangeDate)
            {
                oINFOTYPE2003 = null;
            }


            object oResult = new
            {
                visibleTypeSelectDay = IsVisibleTypeSelectDay,
                visibleTypePeriod = IsVisibleTypePeriod,
                enabledChangeDate = IsEnabledChangeDate,
                objListINFOTYPE2003 = oINFOTYPE2003

            };
            return oResult;
        }


        public Decimal GetSummaryOTInMonth(string EmployeeID, string sPeriod, string RequestNo)
        {
            DateTime Period = DateTime.ParseExact(sPeriod, "yyyyMM", oCL);
            DateTime BeginDate = Period;
            DateTime EndDate = BeginDate.AddMonths(1).AddMinutes(-1);
            DataTable oDT = GetSummaryOTInMonth(EmployeeID, BeginDate, EndDate, RequestNo);
            Decimal oDc = Convert.ToDecimal(oDT.Rows[0]["Summary"]);
            return oDc;
        }

        public string GetAbsenceTypeText(string AbsenceType, string SubAreaSetting)
        {
            //return GetCommonText("ABSENCETYPE", WorkflowPrinciple.Current.UserSetting.Language, "92#" + AbsenceType);
            return GetCommonText("ABSENCETYPE", WorkflowPrinciple.Current.UserSetting.Language, SubAreaSetting + "#" + AbsenceType);
        }

        public string GetAttendanceTypeText(string AttendanceType, string SubAreaSetting)
        {
            //return GetCommonText("ATTENDANCETYPE", WorkflowPrinciple.Current.UserSetting.Language, "92#" + AttendanceType);
            return GetCommonText("ATTENDANCETYPE", WorkflowPrinciple.Current.UserSetting.Language, SubAreaSetting + "#" + AttendanceType);
        }

        public int GetTimeManagementSubjectID()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetTimeManagementSubjectID();
        }

        public List<Holiday> GetHoliday(EmployeeData oEmp, DateTime oFromDate)
        {
            #region �� Holiday ���˹�� 1 �� �����ѧ 1 ��
            //DateTime dMinDate = oEmp.DateSpecific.HiringDate;
            //DateTime dMaxDate = EmployeeManagement.CreateInstance(CompanyCode).GetCalendarEndDate(oEmp.EmployeeID);

            //int CalendarYearBegin = dMinDate.Date.Year;
            //int CalendarYearEnd = dMaxDate.Date.Year;

            //if (DateTime.Now.Year > dMinDate.Date.Year)
            //{
            //    CalendarYearBegin = DateTime.Now.Year - 1;
            //}

            //if (DateTime.Now.Year < dMaxDate.Date.Year)
            //{
            //    CalendarYearEnd = DateTime.Now.Year + 1;
            //}

            //List<Holiday> oMonthlyWorkSchedulers = new List<Holiday>();
            //List<MonthlyWS> oMonthlyWS;

            //if (dMinDate != dMaxDate)
            //{
            //    for (int iYear = CalendarYearBegin; iYear <= CalendarYearEnd; iYear++)
            //    {
            //        oMonthlyWS = EmployeeManagement.CreateInstance(CompanyCode).GetMonthlyWS(oEmp, iYear);
            //        if (oMonthlyWS != null && oMonthlyWS.Count > 0)
            //        {
            //            int iCount = oMonthlyWS.Count;
            //            Holiday oCurrentMonthlyWorkScheduler = null;
            //            MonthlyWS oCurrentMonlyWS = null;
            //            List<string> oDayList = null;
            //            for (int i = 0; i < iCount; i++)
            //            {
            //                oCurrentMonlyWS = oMonthlyWS[i];
            //                if (!string.IsNullOrEmpty(oCurrentMonlyWS.EmpSubAreaForWorkSchedule) && !string.IsNullOrEmpty(oCurrentMonlyWS.EmpSubGroupForWorkSchedule))
            //                {
            //                    oDayList = GetPropertyFromValueDecision<MonthlyWS>(oCurrentMonlyWS, "_H", "Day,_H", "", "1");
            //                    oDayList.AddRange(GetPropertyFromValueDecision<MonthlyWS>(oCurrentMonlyWS, "Day", "Day,_H", "", "OFF"));
            //                    for (int j = 0; j < oDayList.Count; j++)
            //                    {
            //                        oCurrentMonthlyWorkScheduler = new Holiday();
            //                        oCurrentMonthlyWorkScheduler.Day = oDayList[j];
            //                        oCurrentMonthlyWorkScheduler.Month = oCurrentMonlyWS.WS_Month.ToString();
            //                        oCurrentMonthlyWorkScheduler.Year = oCurrentMonlyWS.WS_Year.ToString();

            //                        string IsHoliday = string.Format("{0}{1}{2}", "Day", oCurrentMonthlyWorkScheduler.Day, "_H");
            //                        if (oCurrentMonlyWS.GetType().GetProperty(IsHoliday) != null)
            //                        {
            //                            if ((string)oCurrentMonlyWS.GetType().GetProperty(IsHoliday).GetValue(oCurrentMonlyWS, null) == "1")
            //                            {
            //                                oCurrentMonthlyWorkScheduler.Description = "HOLIDAY";
            //                            }
            //                            else
            //                            {
            //                                oCurrentMonthlyWorkScheduler.Description = "WEEKEND";
            //                            }
            //                        }

            //                        oMonthlyWorkSchedulers.Add(oCurrentMonthlyWorkScheduler);
            //                    }
            //                }
            //            }
            //        }
            //    }
            //}
            //return oMonthlyWorkSchedulers;
            #endregion

            #region �� Holiday ��� min max
            //DateTime dMinDate = oEmp.DateSpecific.HiringDate;
            //DateTime dMaxDate = EmployeeManagement.CreateInstance(CompanyCode).GetCalendarEndDate(oEmp.EmployeeID);

            //List<Holiday> oMonthlyWorkSchedulers = new List<Holiday>();
            //List<MonthlyWS> oMonthlyWS;

            //if (dMinDate != dMaxDate)
            //{
            //    for (int iYear = dMinDate.Date.Year; iYear <= dMaxDate.Date.Year; iYear++)
            //    {
            //        oMonthlyWS = EmployeeManagement.CreateInstance(CompanyCode).GetMonthlyWS(oEmp, iYear);
            //        if (oMonthlyWS != null && oMonthlyWS.Count > 0)
            //        {
            //            int iCount = oMonthlyWS.Count;
            //            Holiday oCurrentMonthlyWorkScheduler = null;
            //            MonthlyWS oCurrentMonlyWS = null;
            //            List<string> oDayList = null;
            //            for (int i = 0; i < iCount; i++)
            //            {
            //                oCurrentMonlyWS = oMonthlyWS[i];
            //                if (!string.IsNullOrEmpty(oCurrentMonlyWS.EmpSubAreaForWorkSchedule) && !string.IsNullOrEmpty(oCurrentMonlyWS.EmpSubGroupForWorkSchedule))
            //                {
            //                    oDayList = GetPropertyFromValueDecision<MonthlyWS>(oCurrentMonlyWS, "_H", "Day,_H", "", "1");
            //                    oDayList.AddRange(GetPropertyFromValueDecision<MonthlyWS>(oCurrentMonlyWS, "Day", "Day,_H", "", "OFF"));
            //                    for (int j = 0; j < oDayList.Count; j++)
            //                    {
            //                        oCurrentMonthlyWorkScheduler = new Holiday();
            //                        oCurrentMonthlyWorkScheduler.Day = oDayList[j];
            //                        oCurrentMonthlyWorkScheduler.Month = oCurrentMonlyWS.WS_Month.ToString();
            //                        oCurrentMonthlyWorkScheduler.Year = oCurrentMonlyWS.WS_Year.ToString();

            //                        string IsHoliday = string.Format("{0}{1}{2}", "Day", oCurrentMonthlyWorkScheduler.Day, "_H");
            //                        if (oCurrentMonlyWS.GetType().GetProperty(IsHoliday) != null)
            //                        {
            //                            if ((string)oCurrentMonlyWS.GetType().GetProperty(IsHoliday).GetValue(oCurrentMonlyWS, null) == "1")
            //                            {
            //                                oCurrentMonthlyWorkScheduler.Description = "HOLIDAY";
            //                            }
            //                            else
            //                            {
            //                                oCurrentMonthlyWorkScheduler.Description = "WEEKEND";
            //                            }
            //                        }

            //                        oMonthlyWorkSchedulers.Add(oCurrentMonthlyWorkScheduler);
            //                    }
            //                }
            //            }
            //        }
            //    }
            //}
            //return oMonthlyWorkSchedulers;
            #endregion

            #region �� Holiday ������
            EmployeeData oEmployeeData = new EmployeeData(oEmp.EmployeeID, oEmp.PositionID, oFromDate);
            List<Holiday> oMonthlyWorkSchedulers = new List<Holiday>();
            List<MonthlyWS> oMonthlyWS = EmployeeManagement.CreateInstance(CompanyCode).GetMonthlyWS(oEmp, oFromDate.Year);
            if (oMonthlyWS != null && oMonthlyWS.Count > 0)
            {
                int iCount = oMonthlyWS.Count;
                Holiday oCurrentMonthlyWorkScheduler = null;
                MonthlyWS oCurrentMonlyWS = null;
                List<string> oDayList = null;
                for (int i = 0; i < iCount; i++)
                {
                    oCurrentMonlyWS = oMonthlyWS[i];
                    if (!string.IsNullOrEmpty(oCurrentMonlyWS.EmpSubAreaForWorkSchedule) && !string.IsNullOrEmpty(oCurrentMonlyWS.EmpSubGroupForWorkSchedule))
                    {
                        oDayList = GetPropertyFromValueDecision<MonthlyWS>(oCurrentMonlyWS, "_H", "Day,_H", "", "1");
                        oDayList.AddRange(GetPropertyFromValueDecision<MonthlyWS>(oCurrentMonlyWS, "Day", "Day,_H", "", "OFF"));
                        for (int j = 0; j < oDayList.Count; j++)
                        {
                            oCurrentMonthlyWorkScheduler = new Holiday();
                            oCurrentMonthlyWorkScheduler.Day = oDayList[j];
                            oCurrentMonthlyWorkScheduler.Month = oCurrentMonlyWS.WS_Month.ToString();
                            oCurrentMonthlyWorkScheduler.Year = oCurrentMonlyWS.WS_Year.ToString();

                            string IsHoliday = string.Format("{0}{1}{2}", "Day", oCurrentMonthlyWorkScheduler.Day, "_H");
                            if (oCurrentMonlyWS.GetType().GetProperty(IsHoliday) != null)
                            {
                                if ((string)oCurrentMonlyWS.GetType().GetProperty(IsHoliday).GetValue(oCurrentMonlyWS, null) == "1")
                                {
                                    oCurrentMonthlyWorkScheduler.Description = "HOLIDAY";
                                }
                                else
                                {
                                    oCurrentMonthlyWorkScheduler.Description = "WEEKEND";
                                }
                            }

                            oMonthlyWorkSchedulers.Add(oCurrentMonthlyWorkScheduler);
                        }
                    }
                }
            }
            return oMonthlyWorkSchedulers;
            #endregion
        }


        public List<Holiday> GetHoliday(string EmpID, string EmpPosition, DateTime oFromDate)
        {
            //Calendar
            EmployeeData oEmployeeData = new EmployeeData(EmpID, EmpPosition, oFromDate);
            List<Holiday> oMonthlyWorkSchedulers = new List<Holiday>();
            List<MonthlyWS> oMonthlyWS = EmployeeManagement.CreateInstance(CompanyCode).GetMonthlyWS(oEmployeeData, oFromDate.Year);
            if (oMonthlyWS != null && oMonthlyWS.Count > 0)
            {
                //oMonthlyWS = oMonthlyWS.FindAll(delegate (MonthlyWS all) { return (all.WS_Month == m); });
                int iCount = oMonthlyWS.Count;
                Holiday oCurrentMonthlyWorkScheduler = null;
                MonthlyWS oCurrentMonlyWS = null;
                List<string> oDayList = null;
                for (int i = 0; i < iCount; i++)
                {
                    oCurrentMonlyWS = oMonthlyWS[i];
                    if (!string.IsNullOrEmpty(oCurrentMonlyWS.EmpSubAreaForWorkSchedule) && !string.IsNullOrEmpty(oCurrentMonlyWS.EmpSubGroupForWorkSchedule))
                    {
                        oDayList = GetPropertyFromValueDecision<MonthlyWS>(oCurrentMonlyWS, "_H", "Day,_H", "", "1");
                        oDayList.AddRange(GetPropertyFromValueDecision<MonthlyWS>(oCurrentMonlyWS, "Day", "Day,_H", "", "OFF"));
                        for (int j = 0; j < oDayList.Count; j++)
                        {
                            oCurrentMonthlyWorkScheduler = new Holiday();
                            oCurrentMonthlyWorkScheduler.Day = oDayList[j];
                            oCurrentMonthlyWorkScheduler.Month = oCurrentMonlyWS.WS_Month.ToString();
                            oCurrentMonthlyWorkScheduler.Year = oCurrentMonlyWS.WS_Year.ToString();

                            string IsHoliday = string.Format("{0}{1}{2}", "Day", oCurrentMonthlyWorkScheduler.Day, "_H");
                            if (oCurrentMonlyWS.GetType().GetProperty(IsHoliday) != null)
                            {
                                if ((string)oCurrentMonlyWS.GetType().GetProperty(IsHoliday).GetValue(oCurrentMonlyWS, null) == "1")
                                {
                                    oCurrentMonthlyWorkScheduler.Description = "HOLIDAY";
                                }
                                else
                                {
                                    oCurrentMonthlyWorkScheduler.Description = "WEEKEND";
                                }
                            }

                            oMonthlyWorkSchedulers.Add(oCurrentMonthlyWorkScheduler);
                        }
                    }
                }
            }
            return oMonthlyWorkSchedulers;
        }

        public List<string> GetPropertyFromValueDecision<T>(T obj, string oLikePropertyName, string oReplaceProperty, string oFormat, string oValueDecision)
        {
            List<string> oResults = new List<string>();
            string oCurrentResult = string.Empty;
            Type oPropertyType = null;
            foreach (PropertyDescriptor oCurrentProperty in TypeDescriptor.GetProperties(obj))
            {
                oPropertyType = oCurrentProperty.PropertyType;
                if (oCurrentProperty.Name.Contains(oLikePropertyName))
                {
                    oPropertyType = oCurrentProperty.PropertyType;
                    if (oPropertyType.Name.ToLower() == "string")
                    {
                        oCurrentResult = oCurrentProperty.GetValue(obj).ToString();
                    }
                    else if (oPropertyType.Name.ToLower() == "boolean")
                    {
                        oCurrentResult = Convert.ToBoolean(oCurrentProperty.GetValue(obj).ToString()).ToString();
                    }
                    else if (oPropertyType.Name.IndexOf("int", StringComparison.CurrentCultureIgnoreCase) >= 0)
                    {
                        oCurrentResult = Convert.ToInt32(oCurrentProperty.GetValue(obj).ToString()).ToString(oFormat);
                    }
                    else if (oPropertyType.Name.ToLower() == "decimal")
                    {
                        oCurrentResult = Convert.ToDecimal(oCurrentProperty.GetValue(obj).ToString()).ToString(oFormat);
                    }
                    else if (oPropertyType.Name.ToLower() == "double")
                    {
                        oCurrentResult = Convert.ToDouble(oCurrentProperty.GetValue(obj).ToString()).ToString(oFormat);
                    }
                    else if (oPropertyType.Name.ToLower() == "datetime")
                    {
                        oCurrentResult = Convert.ToDateTime(oCurrentProperty.GetValue(obj).ToString()).ToString(oFormat);
                    }
                    if (oCurrentResult.Equals(oValueDecision, StringComparison.CurrentCultureIgnoreCase))
                    {
                        string[] tmpSplit = oReplaceProperty.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        oCurrentResult = oCurrentProperty.Name;
                        for (int i = 0; i < tmpSplit.Length; i++)
                        {
                            oCurrentResult = oCurrentResult.Replace(tmpSplit[i], "");
                        }
                        oResults.Add(oCurrentResult);
                    }
                }
            }
            return oResults;
        }
        public List<INFOTYPE2003_LOG> LoadINFOTYPE2003LOG(string RequestNo)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.LoadINFOTYPE2003LOG(RequestNo);
        }


        public ListTimesheetModel GetListTimesheetAbsence(EmployeeData oEmp, DateTime AbDate, DateTime AbDateTo, List<TimeElement> oTimeElement)
        {
            INFOTYPE2001 oINFOTYPE2001 = new INFOTYPE2001();
            INFOTYPE2002 oINFOTYPE2002 = new INFOTYPE2002();
            List<INFOTYPE2001> oAbsenceList = oINFOTYPE2001.GetData(oEmp, AbDate.AddDays(-1), AbDateTo.AddDays(2));
            List<INFOTYPE2002> oAttendanceList = oINFOTYPE2002.GetData(oEmp, AbDate.AddDays(-1), AbDateTo.AddDays(2));

            Dictionary<DateTime, string> DictAttendanceType0192 = new Dictionary<DateTime, string>();
            foreach (INFOTYPE2002 item in oAttendanceList)
                if (item.AttendanceType == "0192")
                    if (!DictAttendanceType0192.ContainsKey(item.BeginDate.Date))
                        DictAttendanceType0192.Add(item.BeginDate.Date, "0");

            List<TimePair> pairs = MatchingClockNew(oEmp.EmployeeID, oTimeElement, true, AbDate, AbDateTo, DictAttendanceType0192);

            #region �ͧ���
            //if (pairs.Count > 0)
            //{
            //    foreach (var item in pairs)
            //    {
            //        var absence = oAbsenceList.Where(a => a.BeginDate.Date == item.Date).OrderBy(s => s.BeginTime).ToList();
            //        if (absence.Count == 0)
            //        {
            //            var attendance = oAttendanceList.Where(a => a.BeginDate.Date <= item.Date && a.EndDate.Date >= item.Date).ToList();
            //            if (attendance.Count >  0)
            //            {
            //                int count_comma_attendance = 0;
            //                foreach(var data_attendance in attendance)
            //                {
            //                    if (data_attendance.Status == "RECORDED")
            //                    {
            //                        //item.Remark = GetAttendanceTypeText(data_attendance.AttendanceType);
            //                        string concat_remark_attendance = GetAttendanceTypeText(data_attendance.AttendanceType);
            //                        if (!data_attendance.AllDayFlag)
            //                        {
            //                            string stringFlex = ConvertFlexTimeText(item.EmployeeID, item.DWS, data_attendance.BeginDate, data_attendance.BeginTime);
            //                            if (string.IsNullOrEmpty(stringFlex))
            //                                concat_remark_attendance += " " + data_attendance.BeginTime.ToString().Substring(0, 5) + "-" + data_attendance.EndTime.ToString().Substring(0, 5);
            //                            else
            //                                concat_remark_attendance += " " + stringFlex;

            //                            // Check Clok In - Out �ҡ���˵��ҡ�ա���Ѻ�ͧ������� Remark = true
            //                            if (item.ClockIN != null || item.ClockOUT != null)
            //                            {
            //                                item.IsCheckRemark = true;
            //                            }
            //                        }
            //                        else
            //                        {
            //                            item.IsCheckRemark = true;
            //                            item.Remark += " " + GetCommonText("HRTMABSENCE", WorkflowPrinciple.Current.UserSetting.Language, "FULLDAY");
            //                        }

            //                        if (count_comma_attendance > 0)
            //                            item.Remark += " , " + concat_remark_attendance;
            //                        else
            //                            item.Remark = concat_remark_attendance;


            //                        count_comma_attendance++;
            //                    }
            //                }
            //            }
            //        }
            //        else
            //        {
            //            int countMixAbsenceAttendance = 0;
            //            int ischeck_comma = 0;
            //            foreach (var item_absence in absence)
            //            {
            //                if (item_absence.Status == "RECORDED")
            //                {
            //                    string concat_text_absence = GetAbsenceTypeText(item_absence.AbsenceType);
            //                    //item.Remark = GetAbsenceTypeText(item_absence.AbsenceType);
            //                    if (!item_absence.AllDayFlag)
            //                    {
            //                        string stringFlex = ConvertFlexTimeText(item.EmployeeID, item.DWS, item_absence.BeginDate, item_absence.BeginTime);
            //                        if (string.IsNullOrEmpty(stringFlex))
            //                        {
            //                            concat_text_absence += " " + item_absence.BeginTime.ToString().Substring(0, 5) + "-" + item_absence.EndTime.ToString().Substring(0, 5);
            //                        }
            //                        else
            //                        {
            //                            concat_text_absence += " " + stringFlex;
            //                        }

            //                        // Check Clok In - Out �ҡ���˵��ҡ�ա���Ѻ�ͧ������� Remark = true
            //                        if (item.ClockIN != null || item.ClockOUT != null)
            //                        {
            //                            item.IsCheckRemark = true;
            //                        }
            //                    }
            //                    else
            //                    {
            //                        item.IsCheckRemark = true;
            //                        concat_text_absence += " " + GetCommonText("HRTMABSENCE", WorkflowPrinciple.Current.UserSetting.Language, "FULLDAY");
            //                    }

            //                    if (ischeck_comma > 0)
            //                        item.Remark = item.Remark + " , " + concat_text_absence;
            //                    else
            //                        item.Remark = concat_text_absence;

            //                    ischeck_comma++;
            //                    countMixAbsenceAttendance++;
            //                }
            //            }

            //            // Attendance
            //            var list_attendance = oAttendanceList.Where(a => a.BeginDate.Date <= item.Date && a.EndDate.Date >= item.Date).ToList();
            //            if (list_attendance.Count > 0)
            //            {
            //                foreach (var data_attendance in list_attendance)
            //                {
            //                    if (data_attendance.Status == "RECORDED")
            //                    {
            //                        string concat_remark_attendance = GetAttendanceTypeText(data_attendance.AttendanceType);
            //                        if (!data_attendance.AllDayFlag)
            //                        {
            //                            string stringFlex = ConvertFlexTimeText(item.EmployeeID, item.DWS, data_attendance.BeginDate, data_attendance.BeginTime);
            //                            if (string.IsNullOrEmpty(stringFlex))
            //                            {
            //                                // �Ѻ�ͧ���Ҥ����ѹ
            //                                //item.Remark += " " + data_attendance.BeginTime.ToString().Substring(0, 5) + "-" + data_attendance.EndTime.ToString().Substring(0, 5);
            //                                concat_remark_attendance += " " + data_attendance.BeginTime.ToString().Substring(0, 5) + "-" + data_attendance.EndTime.ToString().Substring(0, 5);
            //                            }
            //                            else
            //                            {
            //                                //item.Remark += " " + stringFlex;
            //                                concat_remark_attendance += " " + stringFlex;
            //                            }

            //                            // Check Clok In - Out 
            //                            if (item.ClockIN != null || item.ClockOUT != null)
            //                                item.IsCheckRemark = true;
            //                        }
            //                        else
            //                        {
            //                            // �Ѻ�ͧ��������ѹ 
            //                            item.IsCheckRemark = true;
            //                            item.Remark += " " + GetCommonText("HRTMABSENCE", WorkflowPrinciple.Current.UserSetting.Language, "FULLDAY");
            //                        }

            //                        // �ͧ�Ѻ comma �Ѻ�ͧ���ҵ���� 2 ��¡��
            //                        if (ischeck_comma > 0)
            //                            item.Remark = item.Remark + " , " + concat_remark_attendance;
            //                        else
            //                            item.Remark = concat_remark_attendance;

            //                        ischeck_comma++;
            //                    }
            //                }
            //            }

            //            #region �ͧ���
            //            // Check �Ѻ�ͧ�����ա�������ͧ�ҡ � 1 �ѹ�Ҩ���ա���� ����Ѻ�ͧ���Ҵ���
            //            //var attendance = oAttendanceList.FirstOrDefault(a => a.BeginDate.Date <= item.Date && a.EndDate.Date >= item.Date);
            //            //if (attendance != null)
            //            //{
            //            //    if (attendance.Status == "RECORDED")
            //            //    {
            //            //        //item.Remark = GetAttendanceTypeText(attendance.AttendanceType);
            //            //        string concat_text_attendance = GetAttendanceTypeText(attendance.AttendanceType);
            //            //        if (!attendance.AllDayFlag)
            //            //        {
            //            //            string stringFlex = ConvertFlexTimeText(item.EmployeeID, item.DWS, attendance.BeginDate, attendance.BeginTime);
            //            //            if (string.IsNullOrEmpty(stringFlex))
            //            //            {
            //            //                item.Remark += " " + attendance.BeginTime.ToString().Substring(0, 5) + "-" + attendance.EndTime.ToString().Substring(0, 5);
            //            //            }
            //            //            else
            //            //            {
            //            //                item.Remark += " " + stringFlex;
            //            //            }

            //            //            if (ischeck_comma > 0)
            //            //                item.Remark = item.Remark + " , " + concat_text_attendance;
            //            //            else
            //            //                item.Remark = concat_text_attendance;

            //            //            // �ա���Ҥ����ѹ���� ������Ѻ�ͺ�ͧ���ҵ���ա�����ѹ ���ʶҹ� Policy �� true
            //            //            if (countMixAbsenceAttendance > 0)
            //            //            {
            //            //                item.IsCheckRemark = true;
            //            //            }

            //            //            ischeck_comma++;
            //            //        }
            //            //    }
            //            //}
            //            #endregion �ͧ���
            //        }
            //    }
            //}
            #endregion

            if (pairs.Count > 0)
            {
                foreach (var item in pairs)
                {
                    var absence = oAbsenceList.Where(a => a.BeginDate.Date == item.Date).OrderBy(s => s.BeginTime).ToList();
                    if (absence.Count == 0)
                    {
                        var list_attendance = oAttendanceList.Where(a => a.BeginDate.Date <= item.Date && a.EndDate.Date >= item.Date).ToList();
                        if (list_attendance.Count > 0)
                        {
                            int count_attendance = 0;
                            int count_comma_attendance = 0;
                            foreach (var data_attendance in list_attendance)
                            {
                                if (data_attendance.Status == "RECORDED")
                                {
                                    // item.Remark = GetAttendanceTypeText(data_attendance.AttendanceType);
                                    string concat_remark_attendance = GetAttendanceTypeText(data_attendance.AttendanceType, oEmp.OrgAssignment.SubAreaSetting.AbsAttGrouping);
                                    if (!data_attendance.AllDayFlag)
                                    {
                                        string stringFlex = ConvertFlexTimeText(item.EmployeeID, item.DWS, data_attendance.BeginDate, data_attendance.BeginTime);
                                        if (string.IsNullOrEmpty(stringFlex))
                                        {
                                            // �Ѻ�ͧ���Ҥ����ѹ
                                            //item.Remark += " " + data_attendance.BeginTime.ToString().Substring(0, 5) + "-" + data_attendance.EndTime.ToString().Substring(0, 5);
                                            concat_remark_attendance += " " + data_attendance.BeginTime.ToString().Substring(0, 5) + "-" + data_attendance.EndTime.ToString().Substring(0, 5);
                                        }
                                        else
                                        {
                                            //item.Remark += " " + stringFlex;
                                            concat_remark_attendance += " " + stringFlex;
                                        }

                                        // Check Clok In - Out �ҡ���˵�
                                        if (item.ClockIN != null || item.ClockOUT != null)
                                            item.IsCheckRemark = true;

                                        // �Ѻ�ͧ�����ҡ���� 1 ����� 1 �ѹ�� �Ѻ�ͧ������� - ����
                                        if (count_attendance > 0)
                                            item.IsCheckRemark = true;

                                        count_attendance++;
                                    }
                                    else
                                    {
                                        // �Ѻ�ͧ��������ѹ 
                                        item.IsCheckRemark = true;
                                        item.Remark += " " + GetCommonText("HRTMABSENCE", WorkflowPrinciple.Current.UserSetting.Language, "FULLDAY");
                                    }

                                    // �ͧ�Ѻ comma �Ѻ�ͧ���ҵ���� 2 ��¡��
                                    if (count_comma_attendance > 0)
                                        item.Remark = item.Remark + " , " + concat_remark_attendance;
                                    else
                                        item.Remark = concat_remark_attendance;

                                    count_comma_attendance++;
                                }
                            }
                        }
                    }
                    else
                    {
                        // Absence
                        int countMixAbsenceAttendance = 0;
                        int ischeck_comma = 0;
                        foreach (var item_absence in absence)
                        {
                            if (item_absence.Status == "RECORDED")
                            {
                                string concat_text_absence = GetAbsenceTypeText(item_absence.AbsenceType, oEmp.OrgAssignment.SubAreaSetting.AbsAttGrouping);
                                //item.Remark = GetAbsenceTypeText(item_absence.AbsenceType);
                                if (!item_absence.AllDayFlag)
                                {
                                    string stringFlex = ConvertFlexTimeText(item.EmployeeID, item.DWS, item_absence.BeginDate, item_absence.BeginTime);
                                    if (string.IsNullOrEmpty(stringFlex))
                                    {
                                        //item.Remark += " " + item_absence.BeginTime.ToString().Substring(0, 5) + "-" + item_absence.EndTime.ToString().Substring(0, 5);
                                        concat_text_absence += " " + item_absence.BeginTime.ToString().Substring(0, 5) + "-" + item_absence.EndTime.ToString().Substring(0, 5);
                                    }
                                    else
                                    {
                                        concat_text_absence += " " + stringFlex;
                                    }

                                    if (item.ClockIN != null || item.ClockOUT != null)
                                    {
                                        item.IsCheckRemark = true;
                                    }
                                }
                                else
                                {
                                    // ������ѹ 
                                    item.IsCheckRemark = true;
                                    concat_text_absence += " " + GetCommonText("HRTMABSENCE", WorkflowPrinciple.Current.UserSetting.Language, "FULLDAY");
                                }

                                if (ischeck_comma > 0)
                                    item.Remark = item.Remark + " , " + concat_text_absence;
                                else
                                    item.Remark = concat_text_absence;

                                ischeck_comma++;
                                countMixAbsenceAttendance++;
                            }
                        }

                        // Attendance
                        var list_attendance = oAttendanceList.Where(a => a.BeginDate.Date <= item.Date && a.EndDate.Date >= item.Date).ToList();
                        if (list_attendance.Count > 0)
                        {
                            foreach (var data_attendance in list_attendance)
                            {
                                if (data_attendance.Status == "RECORDED")
                                {
                                    string concat_remark_attendance = GetAttendanceTypeText(data_attendance.AttendanceType, oEmp.OrgAssignment.SubAreaSetting.AbsAttGrouping);
                                    if (!data_attendance.AllDayFlag)
                                    {
                                        string stringFlex = ConvertFlexTimeText(item.EmployeeID, item.DWS, data_attendance.BeginDate, data_attendance.BeginTime);
                                        if (string.IsNullOrEmpty(stringFlex))
                                        {
                                            // �Ѻ�ͧ���Ҥ����ѹ
                                            //item.Remark += " " + data_attendance.BeginTime.ToString().Substring(0, 5) + "-" + data_attendance.EndTime.ToString().Substring(0, 5);
                                            concat_remark_attendance += " " + data_attendance.BeginTime.ToString().Substring(0, 5) + "-" + data_attendance.EndTime.ToString().Substring(0, 5);
                                        }
                                        else
                                        {
                                            //item.Remark += " " + stringFlex;
                                            concat_remark_attendance += " " + stringFlex;
                                        }

                                        // Check Clok In - Out �ҡ���˵�
                                        if (item.ClockIN != null || item.ClockOUT != null)
                                            item.IsCheckRemark = true;
                                    }
                                    else
                                    {
                                        // �Ѻ�ͧ��������ѹ 
                                        item.IsCheckRemark = true;
                                        item.Remark += " " + GetCommonText("HRTMABSENCE", WorkflowPrinciple.Current.UserSetting.Language, "FULLDAY");
                                    }

                                    // �ͧ�Ѻ comma �Ѻ�ͧ���ҵ���� 2 ��¡��
                                    if (ischeck_comma > 0)
                                        item.Remark = item.Remark + " , " + concat_remark_attendance;
                                    else
                                        item.Remark = concat_remark_attendance;

                                    // �ա���Ҥ����ѹ���� ������Ѻ�ͺ�ͧ���ҵ���ա�����ѹ ���ʶҹ� Policy �� true
                                    if (countMixAbsenceAttendance > 0)
                                        item.IsCheckRemark = true;

                                    ischeck_comma++;
                                }
                            }
                        }
                    }
                }
            }

            var oResult = new ListTimesheetModel()
            {
                TimePairs = pairs
            };
            return oResult;
        }

        public ListTimesheetModel GetListTimesheetAttendance(EmployeeData oEmp, DateTime BeginDate, DateTime EndDate, List<TimeElement> oTimeElement)
        {
            INFOTYPE2001 oINFOTYPE2001 = new INFOTYPE2001();
            INFOTYPE2002 oINFOTYPE2002 = new INFOTYPE2002();
            List<INFOTYPE2001> oAbsenceList = oINFOTYPE2001.GetData(oEmp, BeginDate.AddDays(-1), EndDate.AddDays(2));
            List<INFOTYPE2002> oAttendanceList = oINFOTYPE2002.GetData(oEmp, BeginDate.AddDays(-1), EndDate.AddDays(2));

            Dictionary<DateTime, string> DictAttendanceType0192 = new Dictionary<DateTime, string>();
            foreach (INFOTYPE2002 item in oAttendanceList)
                if (item.AttendanceType == "0192")
                    if (!DictAttendanceType0192.ContainsKey(item.BeginDate.Date))
                        DictAttendanceType0192.Add(item.BeginDate.Date, "0");

            List<TimePair> pairs = MatchingClockNew(oEmp.EmployeeID, oTimeElement, true, BeginDate, EndDate, DictAttendanceType0192);

            if (pairs.Count > 0)
            {
                foreach (var item in pairs)
                {
                    var absence = oAbsenceList.Where(a => a.BeginDate.Date == item.Date).OrderBy(s => s.BeginTime).ToList();
                    if (absence.Count == 0)
                    {
                        var list_attendance = oAttendanceList.Where(a => a.BeginDate.Date <= item.Date && a.EndDate.Date >= item.Date).ToList();
                        if (list_attendance.Count > 0)
                        {
                            int count_attendance = 0;
                            int count_comma_attendance = 0;
                            foreach (var data_attendance in list_attendance)
                            {
                                if (data_attendance.Status == "RECORDED")
                                {
                                    // item.Remark = GetAttendanceTypeText(data_attendance.AttendanceType);
                                    string concat_remark_attendance = GetAttendanceTypeText(data_attendance.AttendanceType, oEmp.OrgAssignment.SubAreaSetting.AbsAttGrouping);
                                    if (!data_attendance.AllDayFlag)
                                    {
                                        string stringFlex = ConvertFlexTimeText(item.EmployeeID, item.DWS, data_attendance.BeginDate, data_attendance.BeginTime);
                                        if (string.IsNullOrEmpty(stringFlex))
                                        {
                                            // �Ѻ�ͧ���Ҥ����ѹ
                                            //item.Remark += " " + data_attendance.BeginTime.ToString().Substring(0, 5) + "-" + data_attendance.EndTime.ToString().Substring(0, 5);
                                            concat_remark_attendance += " " + data_attendance.BeginTime.ToString().Substring(0, 5) + "-" + data_attendance.EndTime.ToString().Substring(0, 5);
                                        }
                                        else
                                        {
                                            //item.Remark += " " + stringFlex;
                                            concat_remark_attendance += " " + stringFlex;
                                        }

                                        // Check Clok In - Out �ҡ���˵�
                                        if (item.ClockIN != null || item.ClockOUT != null)
                                            item.IsCheckRemark = true;

                                        // �Ѻ�ͧ�����ҡ���� 1 ����� 1 �ѹ�� �Ѻ�ͧ������� - ����
                                        if (count_attendance > 0)
                                            item.IsCheckRemark = true;

                                        count_attendance++;
                                    }
                                    else
                                    {
                                        // �Ѻ�ͧ��������ѹ 
                                        item.IsCheckRemark = true;
                                        item.Remark += " " + GetCommonText("HRTMABSENCE", WorkflowPrinciple.Current.UserSetting.Language, "FULLDAY");
                                    }

                                    // �ͧ�Ѻ comma �Ѻ�ͧ���ҵ���� 2 ��¡��
                                    if (count_comma_attendance > 0)
                                        item.Remark = item.Remark + " , " + concat_remark_attendance;
                                    else
                                        item.Remark = concat_remark_attendance;

                                    count_comma_attendance++;
                                }
                            }
                        }
                    }
                    else
                    {
                        // Absence
                        int countMixAbsenceAttendance = 0;
                        int ischeck_comma = 0;
                        foreach (var item_absence in absence)
                        {
                            if (item_absence.Status == "RECORDED")
                            {
                                string concat_text_absence = GetAbsenceTypeText(item_absence.AbsenceType, oEmp.OrgAssignment.SubAreaSetting.AbsAttGrouping);
                                //item.Remark = GetAbsenceTypeText(item_absence.AbsenceType);
                                if (!item_absence.AllDayFlag)
                                {
                                    string stringFlex = ConvertFlexTimeText(item.EmployeeID, item.DWS, item_absence.BeginDate, item_absence.BeginTime);
                                    if (string.IsNullOrEmpty(stringFlex))
                                    {
                                        //item.Remark += " " + item_absence.BeginTime.ToString().Substring(0, 5) + "-" + item_absence.EndTime.ToString().Substring(0, 5);
                                        concat_text_absence += " " + item_absence.BeginTime.ToString().Substring(0, 5) + "-" + item_absence.EndTime.ToString().Substring(0, 5);
                                    }
                                    else
                                    {
                                        concat_text_absence += " " + stringFlex;
                                    }

                                    if (item.ClockIN != null || item.ClockOUT != null)
                                    {
                                        item.IsCheckRemark = true;
                                    }
                                }
                                else
                                {
                                    // ������ѹ 
                                    item.IsCheckRemark = true;
                                    concat_text_absence += " " + GetCommonText("HRTMABSENCE", WorkflowPrinciple.Current.UserSetting.Language, "FULLDAY");
                                }

                                if (ischeck_comma > 0)
                                    item.Remark = item.Remark + " , " + concat_text_absence;
                                else
                                    item.Remark = concat_text_absence;

                                ischeck_comma++;
                                countMixAbsenceAttendance++;
                            }
                        }

                        // Attendance
                        var list_attendance = oAttendanceList.Where(a => a.BeginDate.Date <= item.Date && a.EndDate.Date >= item.Date).ToList();
                        if (list_attendance.Count > 0)
                        {
                            foreach (var data_attendance in list_attendance)
                            {
                                if (data_attendance.Status == "RECORDED")
                                {
                                    string concat_remark_attendance = GetAttendanceTypeText(data_attendance.AttendanceType, oEmp.OrgAssignment.SubAreaSetting.AbsAttGrouping);
                                    if (!data_attendance.AllDayFlag)
                                    {
                                        string stringFlex = ConvertFlexTimeText(item.EmployeeID, item.DWS, data_attendance.BeginDate, data_attendance.BeginTime);
                                        if (string.IsNullOrEmpty(stringFlex))
                                        {
                                            // �Ѻ�ͧ���Ҥ����ѹ
                                            //item.Remark += " " + data_attendance.BeginTime.ToString().Substring(0, 5) + "-" + data_attendance.EndTime.ToString().Substring(0, 5);
                                            concat_remark_attendance += " " + data_attendance.BeginTime.ToString().Substring(0, 5) + "-" + data_attendance.EndTime.ToString().Substring(0, 5);
                                        }
                                        else
                                        {
                                            //item.Remark += " " + stringFlex;
                                            concat_remark_attendance += " " + stringFlex;
                                        }

                                        // Check Clok In - Out �ҡ���˵�
                                        if (item.ClockIN != null || item.ClockOUT != null)
                                            item.IsCheckRemark = true;
                                    }
                                    else
                                    {
                                        // �Ѻ�ͧ��������ѹ 
                                        item.IsCheckRemark = true;
                                        item.Remark += " " + GetCommonText("HRTMABSENCE", WorkflowPrinciple.Current.UserSetting.Language, "FULLDAY");
                                    }

                                    // �ͧ�Ѻ comma �Ѻ�ͧ���ҵ���� 2 ��¡��
                                    if (ischeck_comma > 0)
                                        item.Remark = item.Remark + " , " + concat_remark_attendance;
                                    else
                                        item.Remark = concat_remark_attendance;

                                    // �ա���Ҥ����ѹ���� ������Ѻ�ͺ�ͧ���ҵ���ա�����ѹ ���ʶҹ� Policy �� true
                                    if (countMixAbsenceAttendance > 0)
                                        item.IsCheckRemark = true;

                                    ischeck_comma++;
                                }
                            }
                        }
                    }
                }
            }

            #region �ͧ���
            //if (pairs.Count > 0)
            //{
            //    foreach (var item in pairs)
            //    {
            //        var absence = oAbsenceList.Where(a => a.BeginDate.Date == item.Date).OrderBy(s => s.BeginTime).ToList();
            //        if (absence.Count == 0)
            //        {
            //            var attendance = oAttendanceList.Where(a => a.BeginDate.Date <= item.Date && a.EndDate.Date >= item.Date).ToList();
            //            if (attendance.Count > 0)
            //            {
            //                int count_comma_attendance = 0;
            //                foreach (var data_attendance in attendance)
            //                {
            //                    if (data_attendance.Status == "RECORDED")
            //                    {
            //                        //item.Remark = GetAttendanceTypeText(data_attendance.AttendanceType);
            //                        string concat_remark_attendance = GetAttendanceTypeText(data_attendance.AttendanceType);
            //                        if (!data_attendance.AllDayFlag)
            //                        {
            //                            string stringFlex = ConvertFlexTimeText(item.EmployeeID, item.DWS, data_attendance.BeginDate, data_attendance.BeginTime);
            //                            if (string.IsNullOrEmpty(stringFlex))
            //                                concat_remark_attendance += " " + data_attendance.BeginTime.ToString().Substring(0, 5) + "-" + data_attendance.EndTime.ToString().Substring(0, 5);
            //                            else
            //                                concat_remark_attendance += " " + stringFlex;

            //                            // Check Clok In - Out �ҡ���˵��ҡ�ա���Ѻ�ͧ������� Remark = true
            //                            if (item.ClockIN != null || item.ClockOUT != null)
            //                            {
            //                                item.IsCheckRemark = true;
            //                            }
            //                        }
            //                        else
            //                        {
            //                            item.IsCheckRemark = true;
            //                            item.Remark += " " + GetCommonText("HRTMABSENCE", WorkflowPrinciple.Current.UserSetting.Language, "FULLDAY");
            //                        }

            //                        if (count_comma_attendance > 0)
            //                            item.Remark += " , " + concat_remark_attendance;
            //                        else
            //                            item.Remark = concat_remark_attendance;


            //                        count_comma_attendance++;
            //                    }
            //                }
            //            }

            //            #region �ͧ���
            //            //var attendance = oAttendanceList.FirstOrDefault(a => a.BeginDate.Date <= item.Date && a.EndDate.Date >= item.Date);
            //            //if (attendance != null)
            //            //{
            //            //    if (attendance.Status == "RECORDED")
            //            //    {
            //            //        item.Remark = GetAttendanceTypeText(attendance.AttendanceType);
            //            //        if (!attendance.AllDayFlag)
            //            //        {
            //            //            string stringFlex = ConvertFlexTimeText(item.EmployeeID, item.DWS, attendance.BeginDate, attendance.BeginTime);
            //            //            if (string.IsNullOrEmpty(stringFlex))
            //            //                item.Remark += " " + attendance.BeginTime.ToString().Substring(0, 5) + "-" + attendance.EndTime.ToString().Substring(0, 5);
            //            //            else
            //            //                item.Remark += " " + stringFlex;

            //            //            // Check Clok In - Out �ҡ���˵��ҡ�ա���Ѻ�ͧ������� Remark = true
            //            //            if (item.ClockIN != null || item.ClockOUT != null)
            //            //            {
            //            //                item.IsCheckRemark = true;
            //            //            }
            //            //        }
            //            //        else
            //            //        {
            //            //            item.IsCheckRemark = true;
            //            //            item.Remark += " " + GetCommonText("HRTMABSENCE", WorkflowPrinciple.Current.UserSetting.Language, "FULLDAY");
            //            //        }
            //            //    }
            //            //}
            //            #endregion
            //        }
            //        else
            //        {
            //            int countMixAbsenceAttendance = 0;
            //            int ischeck_comma = 0;
            //            foreach (var item_absence in absence)
            //            {
            //                if (item_absence.Status == "RECORDED")
            //                {
            //                    string concat_text_absence = GetAbsenceTypeText(item_absence.AbsenceType);
            //                    //item.Remark = GetAbsenceTypeText(item_absence.AbsenceType);
            //                    if (!item_absence.AllDayFlag)
            //                    {
            //                        string stringFlex = ConvertFlexTimeText(item.EmployeeID, item.DWS, item_absence.BeginDate, item_absence.BeginTime);
            //                        if (string.IsNullOrEmpty(stringFlex))
            //                        {
            //                            concat_text_absence += " " + item_absence.BeginTime.ToString().Substring(0, 5) + "-" + item_absence.EndTime.ToString().Substring(0, 5);
            //                        }
            //                        else
            //                        {
            //                            concat_text_absence += " " + stringFlex;
            //                        }

            //                        // Check Clok In - Out �ҡ���˵��ҡ�ա���Ѻ�ͧ������� Remark = true
            //                        if (item.ClockIN != null || item.ClockOUT != null)
            //                        {
            //                            item.IsCheckRemark = true;
            //                        }
            //                    }
            //                    else
            //                    {
            //                        item.IsCheckRemark = true;
            //                        concat_text_absence += " " + GetCommonText("HRTMABSENCE", WorkflowPrinciple.Current.UserSetting.Language, "FULLDAY");
            //                    }

            //                    if (ischeck_comma > 0)
            //                        item.Remark = item.Remark + " , " + concat_text_absence;
            //                    else
            //                        item.Remark = concat_text_absence;

            //                    ischeck_comma++;
            //                    countMixAbsenceAttendance++;
            //                }
            //            }

            //            // Check �Ѻ�ͧ�����ա�������ͧ�ҡ � 1 �ѹ�Ҩ���ա���� ����Ѻ�ͧ���Ҵ���
            //            // Attendance
            //            var list_attendance = oAttendanceList.Where(a => a.BeginDate.Date <= item.Date && a.EndDate.Date >= item.Date).ToList();
            //            if (list_attendance.Count > 0)
            //            {
            //                foreach (var data_attendance in list_attendance)
            //                {
            //                    if (data_attendance.Status == "RECORDED")
            //                    {
            //                        string concat_remark_attendance = GetAttendanceTypeText(data_attendance.AttendanceType);
            //                        if (!data_attendance.AllDayFlag)
            //                        {
            //                            string stringFlex = ConvertFlexTimeText(item.EmployeeID, item.DWS, data_attendance.BeginDate, data_attendance.BeginTime);
            //                            if (string.IsNullOrEmpty(stringFlex))
            //                            {
            //                                // �Ѻ�ͧ���Ҥ����ѹ
            //                                //item.Remark += " " + data_attendance.BeginTime.ToString().Substring(0, 5) + "-" + data_attendance.EndTime.ToString().Substring(0, 5);
            //                                concat_remark_attendance += " " + data_attendance.BeginTime.ToString().Substring(0, 5) + "-" + data_attendance.EndTime.ToString().Substring(0, 5);
            //                            }
            //                            else
            //                            {
            //                                //item.Remark += " " + stringFlex;
            //                                concat_remark_attendance += " " + stringFlex;
            //                            }

            //                            // Check Clok In - Out 
            //                            if (item.ClockIN != null || item.ClockOUT != null)
            //                                item.IsCheckRemark = true;
            //                        }
            //                        else
            //                        {
            //                            // �Ѻ�ͧ��������ѹ 
            //                            item.IsCheckRemark = true;
            //                            item.Remark += " " + GetCommonText("HRTMABSENCE", WorkflowPrinciple.Current.UserSetting.Language, "FULLDAY");
            //                        }

            //                        // �ͧ�Ѻ comma �Ѻ�ͧ���ҵ���� 2 ��¡��
            //                        if (ischeck_comma > 0)
            //                            item.Remark = item.Remark + " , " + concat_remark_attendance;
            //                        else
            //                            item.Remark = concat_remark_attendance;

            //                        ischeck_comma++;
            //                    }
            //                }
            //            }

            //            #region �ͧ���
            //            //var attendance = oAttendanceList.FirstOrDefault(a => a.BeginDate.Date <= item.Date && a.EndDate.Date >= item.Date);
            //            //if (attendance != null)
            //            //{
            //            //    if (attendance.Status == "RECORDED")
            //            //    {
            //            //        string concat_text_attendance = GetAttendanceTypeText(attendance.AttendanceType);
            //            //        if (!attendance.AllDayFlag)
            //            //        {
            //            //            string stringFlex = ConvertFlexTimeText(item.EmployeeID, item.DWS, attendance.BeginDate, attendance.BeginTime);
            //            //            if (string.IsNullOrEmpty(stringFlex))
            //            //            {
            //            //                item.Remark += " " + attendance.BeginTime.ToString().Substring(0, 5) + "-" + attendance.EndTime.ToString().Substring(0, 5);
            //            //            }
            //            //            else
            //            //            {
            //            //                item.Remark += " " + stringFlex;
            //            //            }


            //            //            if (ischeck_comma > 0)
            //            //                item.Remark = item.Remark + " , " + concat_text_attendance;
            //            //            else
            //            //                item.Remark = concat_text_attendance;


            //            //            if (countMixAbsenceAttendance > 0)
            //            //            {
            //            //                item.IsCheckRemark = true;
            //            //            }

            //            //            ischeck_comma++;
            //            //        }
            //            //    }
            //            //}
            //            #endregion
            //        }
            //    }
            //}
            #endregion
            var oResult = new ListTimesheetModel()
            {
                TimePairs = pairs
            };
            return oResult;
        }

        public class ListClockInClockOutOTSummaryModel
        {
            public ListClockInClockOutOTSummaryModel()
            {
                TimeElements = new List<TimeElement>();
                Timesheets = new List<TM_Timesheet>();
                TimePairs = new List<TimePair>();
            }
            public List<TimeElement> TimeElements { get; set; }
            public List<TM_Timesheet> Timesheets { get; set; }
            public List<TimePair> TimePairs { get; set; }
        }
        public class TimePairOTSummary
        {
            public DateTime Date { get; set; }
            public DateTime? ClockIn { get; set; }
            public DateTime? ClockOut { get; set; }
            public string Type { get; set; }
            public DateTime? WorkBegin { get; set; }
            public DateTime? WorkEnd { get; set; }
            public DateTime? AbsAttBegin { get; set; }
            public DateTime? AbsAttEnd { get; set; }
            public TimeSpan AbsAttBeginTime { get; set; }
            public TimeSpan AbsAttEndTime { get; set; }
            public bool IsAllDayFlag { get; set; }
            public int CountAbsAtt { get; set; }

        }
        public List<TimePairOTSummary> GetTimePairOTSummary(DateTime BeginDate, DateTime EndDate, EmployeeData oEmp)
        {
            //var watch = new System.Diagnostics.Stopwatch();
            //watch.Start();

            //DateTime BeginDate = DateTime.ParseExact(PeriodValue, "yyyyMM", oCL);
            //DateTime EndDate = BeginDate.AddMonths(1).AddMinutes(-1);

            List<TimeElement> oTimeElement = LoadTimeElement(oEmp.EmployeeID, BeginDate.AddDays(-1), EndDate.AddDays(2), -1, true, true);
            List<TimeElement> oTimeElementList = LoadTimeElement(oEmp.EmployeeID, BeginDate.AddDays(-1), EndDate.AddDays(2), -1, true, true);

            INFOTYPE2001 oINFOTYPE2001 = new INFOTYPE2001();
            INFOTYPE2002 oINFOTYPE2002 = new INFOTYPE2002();
            List<INFOTYPE2001> oAbsenceList = oINFOTYPE2001.GetData(oEmp, BeginDate.AddDays(-1), EndDate.AddDays(2));
            List<INFOTYPE2002> oAttendanceList = oINFOTYPE2002.GetData(oEmp, BeginDate.AddDays(-1), EndDate.AddDays(2));

            Dictionary<DateTime, string> DictAttendanceType0192 = new Dictionary<DateTime, string>();
            foreach (INFOTYPE2002 item in oAttendanceList)
                if (item.AttendanceType == "0192")
                    if (!DictAttendanceType0192.ContainsKey(item.BeginDate.Date))
                        DictAttendanceType0192.Add(item.BeginDate.Date, "0");

            List<TimePair> pairs = MatchingClockNew(oEmp.EmployeeID, oTimeElement, true, BeginDate, EndDate, DictAttendanceType0192);

            //watch.Stop();
            //Console.WriteLine($"Execution Time: {watch.ElapsedMilliseconds} ms");

            List<TimePairOTSummary> oTimePairOTSummary = new List<TimePairOTSummary>();
            foreach (var _timePairs in pairs)
            {
                DateTime dt = _timePairs.Date;
                var timesheet = new TimePairOTSummary()
                {
                    Date = _timePairs.Date,
                    ClockIn = _timePairs.ClockIN == null ? (DateTime?)null : _timePairs.ClockIN.EventTime,
                    ClockOut = _timePairs.ClockOUT == null ? (DateTime?)null : _timePairs.ClockOUT.EventTime,
                    AbsAttBeginTime = TimeSpan.Zero,
                    AbsAttEndTime = TimeSpan.Zero
                };


                var listAbsence = oAbsenceList.Where(a => a.BeginDate.Date == _timePairs.Date)
                                              .Where(a => a.Status == "RECORDED").OrderBy(s => s.BeginTime).ToList();
                var listAttendance = oAttendanceList.Where(a => a.BeginDate.Date == _timePairs.Date)
                                                .Where(a => a.Status == "RECORDED").OrderBy(s => s.BeginTime).ToList();

                DateTime dtAbsAtt = _timePairs.Date;
                if (listAbsence.Count + listAttendance.Count == 0)
                {
                    timesheet.CountAbsAtt = 0;
                }
                else if (listAbsence.Count + listAttendance.Count == 1)
                {
                    bool chkAllDayFlag = true;
                    foreach (var dataAbs in listAbsence)
                    {
                        if (timesheet.AbsAttBeginTime == TimeSpan.Zero && timesheet.AbsAttEndTime == TimeSpan.Zero)
                        {
                            timesheet.AbsAttBeginTime = dataAbs.BeginTime;
                            timesheet.AbsAttEndTime = dataAbs.EndTime;
                        }
                        else
                        {
                            if (timesheet.AbsAttBeginTime > dataAbs.BeginTime)
                                timesheet.AbsAttBeginTime = dataAbs.BeginTime;
                            if (timesheet.AbsAttEndTime < dataAbs.EndTime)
                                timesheet.AbsAttEndTime = dataAbs.EndTime;
                        }
                        chkAllDayFlag = dataAbs.AllDayFlag;
                    }
                    foreach (var dataAtt in listAttendance)
                    {
                        if (timesheet.AbsAttBeginTime == TimeSpan.Zero && timesheet.AbsAttEndTime == TimeSpan.Zero)
                        {
                            timesheet.AbsAttBeginTime = dataAtt.BeginTime;
                            timesheet.AbsAttEndTime = dataAtt.EndTime;
                        }
                        else
                        {
                            if (timesheet.AbsAttBeginTime > dataAtt.BeginTime)
                                timesheet.AbsAttBeginTime = dataAtt.BeginTime;
                            if (timesheet.AbsAttEndTime < dataAtt.EndTime)
                                timesheet.AbsAttEndTime = dataAtt.EndTime;
                        }
                        chkAllDayFlag = dataAtt.AllDayFlag;
                    }
                    timesheet.IsAllDayFlag = chkAllDayFlag;
                    timesheet.CountAbsAtt = listAbsence.Count + listAttendance.Count;
                }
                else if (listAbsence.Count + listAttendance.Count > 1)
                {
                    foreach (var dataAbs in listAbsence)
                    {
                        if (timesheet.AbsAttBeginTime == TimeSpan.Zero && timesheet.AbsAttEndTime == TimeSpan.Zero)
                        {
                            timesheet.AbsAttBeginTime = dataAbs.BeginTime;
                            timesheet.AbsAttEndTime = dataAbs.EndTime;
                        }
                        else
                        {
                            if (timesheet.AbsAttBeginTime > dataAbs.BeginTime)
                                timesheet.AbsAttBeginTime = dataAbs.BeginTime;
                            if (timesheet.AbsAttEndTime < dataAbs.EndTime)
                                timesheet.AbsAttEndTime = dataAbs.EndTime;
                        }
                    }
                    foreach (var dataAtt in listAttendance)
                    {
                        if (timesheet.AbsAttBeginTime == TimeSpan.Zero && timesheet.AbsAttEndTime == TimeSpan.Zero)
                        {
                            timesheet.AbsAttBeginTime = dataAtt.BeginTime;
                            timesheet.AbsAttEndTime = dataAtt.EndTime;
                        }
                        else
                        {
                            if (timesheet.AbsAttBeginTime > dataAtt.BeginTime)
                                timesheet.AbsAttBeginTime = dataAtt.BeginTime;
                            if (timesheet.AbsAttEndTime < dataAtt.EndTime)
                                timesheet.AbsAttEndTime = dataAtt.EndTime;
                        }
                    }
                    timesheet.IsAllDayFlag = true;
                    timesheet.CountAbsAtt = listAbsence.Count + listAttendance.Count;
                }

                EmployeeData _EmployeeData = new EmployeeData(_timePairs.EmployeeID, _timePairs.Date);
                if (_EmployeeData.EmpSubAreaType == EmployeeSubAreaType.Flex)
                {
                    INFOTYPE0007 oInfotype0007 = _EmployeeData.GetEmployeeWF(_EmployeeData.EmployeeID, _timePairs.Date);
                    if (!string.IsNullOrEmpty(oInfotype0007.WFRule))
                    {
                        TimeSpan oBeginTimeDailyFlexTime;
                        TimeSpan oEndTimeDailyFlexTime;
                        DailyFlexTime oDFX = HRTMManagement.CreateInstance(CompanyCode).GetDailyFlexTimeMinForCalculateByFlexCode(oInfotype0007.WFRule);

                        BreakPattern oBreak = new BreakPattern();
                        oBreak = EmployeeManagement.CreateInstance(CompanyCode).GetBreakPattern(_timePairs.DWS.DailyWorkscheduleGrouping, oDFX.BreakCode);

                        TimeSpan TBegin;
                        if (!TimeSpan.TryParse(oDFX.BeginTime, out TBegin))
                        {
                            // handle validation error
                        }
                        oBeginTimeDailyFlexTime = new TimeSpan(TBegin.Hours, TBegin.Minutes, 0);

                        TimeSpan TEnd;
                        if (!TimeSpan.TryParse(oDFX.EndTime, out TEnd))
                        {
                            // handle validation error
                        }
                        oEndTimeDailyFlexTime = new TimeSpan(TEnd.Hours, TEnd.Minutes, 0);
                        _timePairs.DWS.WorkBeginTime = oBeginTimeDailyFlexTime;
                        _timePairs.DWS.WorkEndTime = oEndTimeDailyFlexTime;
                    }
                }


                if (_timePairs.DWSCode.StartsWith("OFF") || _timePairs.DWSCode.StartsWith("HOL"))
                {
                    timesheet.Type = _timePairs.DWSCode;
                }
                else
                {
                    timesheet.Type = _timePairs.DWSCode;
                    //timesheet.Type = string.Format("{0} ({1}-{2})", _timePairs.DWSCode, TimeSpanToString(_timePairs.DWS.WorkBeginTime, oCL), TimeSpanToString(_timePairs.DWS.WorkEndTime, oCL));
                    timesheet.WorkBegin = dt.Add(_timePairs.DWS.WorkBeginTime);
                    timesheet.WorkEnd = dt.Add(_timePairs.DWS.WorkEndTime);

                    if (timesheet.WorkBegin > timesheet.WorkEnd)
                        timesheet.WorkEnd = timesheet.WorkEnd.Value.AddDays(1);
                }

                if (timesheet.AbsAttBeginTime != TimeSpan.Zero && timesheet.AbsAttEndTime != TimeSpan.Zero)
                {
                    timesheet.AbsAttBegin = dtAbsAtt.Add(timesheet.AbsAttBeginTime);
                    timesheet.AbsAttEnd = dtAbsAtt.Add(timesheet.AbsAttEndTime);
                    if (timesheet.AbsAttBegin > timesheet.AbsAttEnd)
                        timesheet.AbsAttEnd = timesheet.AbsAttEnd.Value.AddDays(1);
                }

                //result.TimeSheets.Add(timesheet);
                oTimePairOTSummary.Add(timesheet);
            }
            return oTimePairOTSummary;
        }

        public List<AreaWorkschedule> GetAreaWorkscheduleAllByArea(string Area, string SubArea)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAreaWorkscheduleAllByArea(Area, SubArea);
        }

        public List<AreaWorkschedule> GetAreaWorkscheduleAreaCode(string Area, string SubArea)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAreaWorkscheduleAreaCode(Area, SubArea);
        }

        public List<AreaWorkschedule> GetAreaWorkscheduleAllByWFRule(string Area, string SubArea, string WFRule)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAreaWorkscheduleAllByWFRule(Area, SubArea, WFRule);
        }

        public string GetTextNameAreaWorkscheduleByWFRule(string EmployeeID, DateTime EffectiveDate)
        {
            string oResult = string.Empty;
            EmployeeData oEmp = new EmployeeData(EmployeeID, EffectiveDate);
            if (oEmp.OrgAssignment != null)
            {
                string sArea = oEmp.OrgAssignment.Area;
                string sSubArea = oEmp.OrgAssignment.SubArea;

                INFOTYPE0007 oINFOTYPE0007 = EmployeeManagement.CreateInstance(CompanyCode).GetWorkSchedule(EmployeeID, EffectiveDate);
                if (oINFOTYPE0007 != null)
                {
                    List<AreaWorkschedule> oAreaWS = ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAreaWorkscheduleAllByWFRule(sArea, sSubArea, oINFOTYPE0007.WFRule);
                    if (oAreaWS != null && oAreaWS.Count > 0)
                    {
                        oResult = oAreaWS[0].TextName;
                    }
                }
            }
            return oResult;
        }

        public List<AreaWorkscheduleLog> GetAreaWorkscheduleLogByRequestNo(string RequestNo)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetAreaWorkscheduleLogByRequestNo(RequestNo);
        }

        public List<AreaWorkscheduleLog> GetAreaWorkscheduleLogByPeriod(DateTime BeginDate, DateTime EndDate, string CreatorID)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetAreaWorkscheduleLogByPeriod(BeginDate, EndDate, CreatorID);
        }

        public class ListOTOverMsg
        {
            public bool IsOverMonth { get; set; }
            public string Monthly { get; set; }
            public int WeekOfMonth { get; set; }
            public decimal SumOT { get; set; }
            public string OTStatus { get; set; }
            public bool IsSendToMD { get; set; }
        }
        public ListOTOverMsg CheckOTOverMsg(EmployeeData oEmp, List<DateTime> oListDate)
        {
            ListOTOverMsg oReturn = new ListOTOverMsg();

            DateTime oFirstDate = oListDate[0];
            DataTable dtOLPM = HRTMManagement.CreateInstance(CompanyCode).GetOTOverLimitPerMonth(oEmp.EmployeeID, oFirstDate);
            if (dtOLPM != null && dtOLPM.Rows.Count > 0)
            {
                oReturn.IsOverMonth = true;
                oReturn.Monthly = Convert.ToString(dtOLPM.Rows[0]["Monthly"]);
                oReturn.SumOT = Convert.ToDecimal(dtOLPM.Rows[0]["SumOT"]);
                oReturn.OTStatus = Convert.ToString(dtOLPM.Rows[0]["OTStatus"]);
                oReturn.IsSendToMD = Convert.ToBoolean(dtOLPM.Rows[0]["IsSendToMD"]);
            }
            else
            {
                foreach (DateTime oDate in oListDate)
                {
                    DataTable dtOT = HRTMManagement.CreateInstance(CompanyCode).GetOTOverLimitPerWeek(oEmp.EmployeeID, oDate);
                    if (dtOT != null && dtOT.Rows.Count > 0)
                    {
                        oReturn.IsOverMonth = false;
                        oReturn.Monthly = Convert.ToString(dtOT.Rows[0]["Monthly"]);
                        oReturn.WeekOfMonth = Convert.ToInt32(dtOT.Rows[0]["WeekOfMonth"]);
                        oReturn.SumOT = Convert.ToDecimal(dtOT.Rows[0]["SumOT"]);
                        oReturn.OTStatus = Convert.ToString(dtOT.Rows[0]["OTStatus"]);
                        oReturn.IsSendToMD = Convert.ToBoolean(dtOT.Rows[0]["IsSendToMD"]);
                        break;
                    }
                }
            }

            if (string.IsNullOrEmpty(oReturn.OTStatus))
            {
                DataTable dtOTOnHoliday = HRTMManagement.CreateInstance(CompanyCode).GetOTOnHoliday(oEmp.EmployeeID, oFirstDate);
                if (dtOTOnHoliday != null && dtOTOnHoliday.Rows.Count > 0)
                {
                    oReturn.IsOverMonth = false;
                    oReturn.Monthly = Convert.ToString(dtOTOnHoliday.Rows[0]["Monthly"]);
                    oReturn.SumOT = Convert.ToDecimal(dtOTOnHoliday.Rows[0]["SumOT"]);
                    oReturn.OTStatus = Convert.ToString(dtOTOnHoliday.Rows[0]["OTStatus"]);
                    oReturn.IsSendToMD = Convert.ToBoolean(dtOTOnHoliday.Rows[0]["IsSendToMD"]);
                }
            }
            return oReturn;
        }

        public void SaveAreaWorkscheduleLog(List<AreaWorkscheduleLog> data, bool IsMark)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveAreaWorkscheduleLog(data, IsMark);
        }

        public bool ShowTimeAwareLink(int LinkID)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.ShowTimeAwareLink(LinkID);
        }

        public DataTable GetClockinLateClockoutEarly(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetClockinLateClockoutEarly(EmployeeID, BeginDate, EndDate);
        }

        public List<ReportGroupAdmin> LoadReportAdminGroup(int reportGroupId)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetGroupReportAdmin(reportGroupId);
        }

        //AddBy: Ratchatawan W. (2013-01-15)
        public DataSet GenerateAbsenceReportDataSet(string lit_org, string list_absence, DateTime BeginDate, DateTime EndDate, EmployeeData oEmp)
        {
            DataSet DS = new DataSet("DSAbsenceAttendance");

            string OrganizationXML = "<ORGANIZATIONID>";
            foreach (string orgID in lit_org.Split('|'))
                OrganizationXML += "<value>" + orgID + "</value>";
            OrganizationXML += "</ORGANIZATIONID>";

            string AType = "<ATYPE>";
            foreach (string dt in list_absence.Split('|'))
                AType += "<value>" + dt + "</value>";
            AType += "</ATYPE>";

            DataSet dsEmployee = new DataSet();
            DataTable oEmployeeListTable = EmployeeManagement.CreateInstance(CompanyCode).GetEmployeeListByOrganizationXML(OrganizationXML, DateTime.Now);
            dsEmployee.Tables.Add(oEmployeeListTable);
            string xmlEmployee = dsEmployee.GetXml();

            DataTable tb_covert_xml = ServiceManager.CreateInstance(CompanyCode).ESSData.GetAbsenceReportByEmployeeXML(xmlEmployee, AType, BeginDate, EndDate, oEmp);
            DS.Tables.Add(tb_covert_xml);

            return DS;
        }

        #region ��§ҹ��ػ�ӹǹ��ѡ�ҹ����ԡ�����ǧ�����觵���ӹǹ�������

        public List<string> GetListYearConfigOTLogByHour()
        {
            DateTime date_now = DateTime.Now;
            List<string> list_year = new List<string>();
            for (int i = 0; i < YEAR_OTLOG_HOUR; i++)
            {
                if (i == 0)
                {
                    string year = date_now.Date.Year.ToString();
                    list_year.Add(year);
                }
                else
                {
                    string year = (date_now.Date.Year - i).ToString();
                    list_year.Add(year);
                }

            }
            return list_year;
        }

        public XLWorkbook CalculateOTLogByHour(string year)
        {
            XLWorkbook workBook = new XLWorkbook();
            var sheet1 = workBook.Worksheets.Add("��ѡ�ҹ�Դ�ͷ��觵���������");

            #region Header Excel

            int header_row = 1;
            int header_column = 1;
            string[] list_month_header = { "Jun", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

            // �Ƿ�� 1 Merg Column 1-2
            sheet1.Row(header_row).Cell(header_column).Value = "�ӹǹ�����������ԡ / �ѻ����";
            sheet1.Column(header_column).Width = 50;
            var range3 = sheet1.Range("A1:A2");
            var columnInRange = range3.Column(1);
            columnInRange.Merge();
            columnInRange.Style.Alignment.WrapText = true;
            columnInRange.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            columnInRange.Style.Fill.BackgroundColor = XLColor.Gray;

            header_column++; // column 2
            // Merg Row 
            sheet1.Row(header_row).Cell(header_column).Value = "�ӹǹ��ѡ�ҹ����ԡ��Шӻ� " + year;
            var range2 = sheet1.Range("B1:M1");
            var rowInRange = range2.Row(1);
            rowInRange.Merge();
            rowInRange.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            rowInRange.Style.Fill.BackgroundColor = XLColor.Gray;

            //�Ƿ�� 2
            header_row++;
            header_column = 2;
            foreach (string month in list_month_header)
            {
                sheet1.Row(header_row).Cell(header_column).Value = month;
                sheet1.Row(header_row).Cell(header_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                sheet1.Column(header_column).Width = 10;
                header_column++;
            }

            sheet1.Range(1, 1, 2, 13).Style.Border
                .SetOutsideBorder(XLBorderStyleValues.Thick)
                .Border.SetInsideBorder(XLBorderStyleValues.Thick);

            sheet1.Range(1, 1, 1, 13).Style.Font.FontName = "Angsana New";
            sheet1.Range(1, 1, 1, 13).Style.Font.FontSize = 14;
            sheet1.Range(1, 1, 1, header_column).Style.Font.Bold = true;

            #endregion

            #region Process Data

            // ��������ػ����ԡ OT
            List<OTLogMonthByHour> list_summary_report = new List<OTLogMonthByHour>();
            for (int i = 1; i <= 4; i++)
            {
                OTLogMonthByHour summary_ot = new OTLogMonthByHour();
                switch (i)
                {
                    case 1:
                        summary_ot.Name = "";
                        summary_ot.TypePeriod = TypePeriodReportOTLogByMonth.SummaryOTByMonth;
                        break;
                    case 2:
                        summary_ot.Name = "�ӹǹ����Թ 36 �������/�ѻ���� (�繨ӹǹ��)";
                        summary_ot.TypePeriod = TypePeriodReportOTLogByMonth.SummaryOTByMonthOver36_PerWeek;
                        break;
                    case 3:
                        summary_ot.Name = "�ӹǹ��ѡ�ҹ�дѺ 4 -10 ����ԡ�Թ 120 �������/��͹";
                        summary_ot.TypePeriod = TypePeriodReportOTLogByMonth.SummaryOTEmployee_Level4_And_Level10_Over120_PerMonth;
                        break;
                    case 4:
                        summary_ot.Name = "�ӹǹ��ѡ�ҹ�дѺ 9 � 10 ����ԡ�Թ 48 �������/��͹";
                        summary_ot.TypePeriod = TypePeriodReportOTLogByMonth.SummaryOTEmployee_Level9_And_Level10_Over48_PerMonth;
                        break;

                }
                list_summary_report.Add(summary_ot);

            }


            string[] list_month_data = { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" };
            // string[] list_month_data = { "03" };

            List<PeriodOTSumary> period_report_ot_sumary = ServiceManager.CreateInstance(CompanyCode).ESSData.GetPeiodReportOTSummary("OTSummaryReport", DateTime.Now);

            // �������͹ 1 - 12
            foreach (var month in list_month_data)
            {
                List<OTLogAll> list_ot = ServiceManager.CreateInstance(CompanyCode).ESSData.GetOTLogAll(year, month);
                if (list_ot.Count > 0)
                {
                    var group_emp = list_ot.GroupBy(s => s.EmployeeID).ToList();
                    if (group_emp.Count > 0)
                    {
                        foreach (var employee in group_emp)
                        {
                            decimal summary_week_1 = 0;
                            decimal summary_week_2 = 0;
                            decimal summary_week_3 = 0;
                            decimal summary_week_4 = 0;
                            decimal summary_week_5 = 0;
                            List<int> list_level = new List<int>();
                            foreach (var data in employee)
                            {
                                if (data.BeginDate.Date.Day >= 1 && data.BeginDate.Date.Day <= 7)
                                    summary_week_1 += data.FinalOTHour10 + data.FinalOTHour15 + data.FinalOTHour30; // Sum Total Ot Week 1
                                else if (data.BeginDate.Date.Day > 7 && data.BeginDate.Date.Day <= 14)
                                    summary_week_2 += data.FinalOTHour10 + data.FinalOTHour15 + data.FinalOTHour30; // Sum Total Ot Week 2
                                else if (data.BeginDate.Date.Day > 14 && data.BeginDate.Date.Day <= 21)
                                    summary_week_3 += data.FinalOTHour10 + data.FinalOTHour15 + data.FinalOTHour30; // Sum Total Ot Week 3
                                else if (data.BeginDate.Date.Day > 21 && data.BeginDate.Date.Day <= 28)
                                    summary_week_4 += data.FinalOTHour10 + data.FinalOTHour15 + data.FinalOTHour30; // Sum Total Ot Week 4
                                else
                                    summary_week_5 += data.FinalOTHour10 + data.FinalOTHour15 + data.FinalOTHour30; // Sum Total Ot Week 5

                                // ���дѺ��ѡ�ҹ����ԡ������͹����������дѺ㴺�ҧ
                                if (Convert.ToInt16(data.LevelEmployee) >= 4)
                                    list_level.Add(Convert.ToInt16(data.LevelEmployee));
                            }

                            decimal weekMaximum = 0;
                            if (summary_week_1 > weekMaximum)
                                weekMaximum = summary_week_1;

                            if (summary_week_2 > weekMaximum)
                                weekMaximum = summary_week_2;

                            if (summary_week_3 > weekMaximum)
                                weekMaximum = summary_week_3;

                            if (summary_week_4 > weekMaximum)
                                weekMaximum = summary_week_4;

                            if (summary_week_5 > weekMaximum)
                                weekMaximum = summary_week_5;

                            if (weekMaximum > 0)
                                CalculatePeriodHour(weekMaximum, period_report_ot_sumary, month);

                            // ��ѡ�ҹ����ԡ OT �Թ 36 �.� �����Թ 2160 �ҷ� ����ѻ����
                            decimal overOt36Hour = 36 * 60;
                            if (summary_week_1 > overOt36Hour || summary_week_2 > overOt36Hour || summary_week_3 > overOt36Hour
                                || summary_week_4 > overOt36Hour || summary_week_5 > overOt36Hour)
                            {
                                var summary_report_over36Hour = list_summary_report.Where(s => s.TypePeriod == TypePeriodReportOTLogByMonth.SummaryOTByMonthOver36_PerWeek).FirstOrDefault();
                                if (summary_report_over36Hour != null)
                                    CalculateOtSumary(month, summary_report_over36Hour);
                            }

                            // �ӹǹ ot ��������͹  (��� 5 week)
                            decimal totalOT_month = summary_week_1 + summary_week_2 + summary_week_3 + summary_week_4 + summary_week_5;

                            // Level ��ѡ�ҹ
                            decimal? level_data = list_level.OrderByDescending(s => s).FirstOrDefault();

                            if (level_data.HasValue && level_data.Value >= 4)
                            {
                                // ��ѡ�ҹ����ԡ OT �Թ 120 �.� �����Թ 7200 �ҷ� �����͹
                                decimal overOt120Hour_Level4To12 = 120 * 60;
                                if (totalOT_month > overOt120Hour_Level4To12)
                                {
                                    var summary_report_over120Hour_level4To10 = list_summary_report.Where(s => s.TypePeriod == TypePeriodReportOTLogByMonth.SummaryOTEmployee_Level4_And_Level10_Over120_PerMonth).FirstOrDefault();
                                    if (summary_report_over120Hour_level4To10 != null)
                                        CalculateOtSumary(month, summary_report_over120Hour_level4To10);
                                }
                            }

                            if (level_data.HasValue && level_data.Value >= 9 && level_data.Value <= 10)
                            {
                                decimal overOT48Hour_Level10To12 = 48 * 60;
                                if (totalOT_month > overOT48Hour_Level10To12)
                                {
                                    var summary_report_over48Hour_level9to10 = list_summary_report.Where(s => s.TypePeriod == TypePeriodReportOTLogByMonth.SummaryOTEmployee_Level9_And_Level10_Over48_PerMonth).FirstOrDefault();
                                    if (summary_report_over48Hour_level9to10 != null)
                                        CalculateOtSumary(month, summary_report_over48Hour_level9to10);
                                }
                            }
                        }
                    }
                }
            }

            #endregion

            #region Excel Data
            if (period_report_ot_sumary.Count > 0)
            {
                int data_row = 3;
                int data_column;
                int row_last = 0;
                int column_last = 0;
                foreach (var item in period_report_ot_sumary)
                {
                    data_column = 1;

                    // Name
                    sheet1.Row(data_row).Cell(data_column).Value = item.RptSeqDesc;
                    sheet1.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    // ���Ҥ�
                    sheet1.Row(data_row).Cell(data_column).Value = item.Count_Jan;
                    sheet1.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    // ����Ҿѹ��
                    sheet1.Row(data_row).Cell(data_column).Value = item.Count_Feb;
                    sheet1.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    // �չҤ�
                    sheet1.Row(data_row).Cell(data_column).Value = item.Count_Mar;
                    sheet1.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    // ����¹
                    sheet1.Row(data_row).Cell(data_column).Value = item.Count_Apr;
                    sheet1.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    // ����Ҥ�
                    sheet1.Row(data_row).Cell(data_column).Value = item.Count_May;
                    sheet1.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    // �Զع�¹
                    sheet1.Row(data_row).Cell(data_column).Value = item.Count_Jun;
                    sheet1.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    // �á�Ҥ�
                    sheet1.Row(data_row).Cell(data_column).Value = item.Count_Jul;
                    sheet1.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    // �ԧ�Ҥ�
                    sheet1.Row(data_row).Cell(data_column).Value = item.Count_Aug;
                    sheet1.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    // �ѹ��¹
                    sheet1.Row(data_row).Cell(data_column).Value = item.Count_Sep;
                    sheet1.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    // ���Ҥ�
                    sheet1.Row(data_row).Cell(data_column).Value = item.Count_Oct;
                    sheet1.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    // ��Ȩԡ�¹
                    sheet1.Row(data_row).Cell(data_column).Value = item.Count_Nov;
                    sheet1.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    // �ѹ�Ҥ�
                    sheet1.Row(data_row).Cell(data_column).Value = item.Count_Dec;
                    sheet1.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    data_row++;
                    row_last = data_row;  // last row
                    column_last = data_column;
                }

                row_last = row_last - 1;
                column_last = column_last - 1;

                sheet1.Range(3, 1, row_last, column_last).Style
                    .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                    .Border.SetInsideBorder(XLBorderStyleValues.Thin);

                sheet1.Range(3, 1, row_last, column_last).Style.Font.FontName = "Angsana New";
                sheet1.Range(3, 1, row_last, column_last).Style.Font.FontSize = 14;

                // Sum �ӹǹ���ԡ OT �������͹
                var summaryOtByMonth = list_summary_report.Where(s => s.TypePeriod == TypePeriodReportOTLogByMonth.SummaryOTByMonth).FirstOrDefault();
                if (summaryOtByMonth != null)
                    CountEmployeeOvertimePay(period_report_ot_sumary, summaryOtByMonth);

                // ��ػ���
                data_row = row_last + 1;
                int first_row_summary = data_row;
                int last_row_summary = 0;
                foreach (var summary_ot in list_summary_report)
                {
                    data_column = 1;

                    // Name
                    sheet1.Row(data_row).Cell(data_column).Value = summary_ot.Name;
                    sheet1.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    sheet1.Row(data_row).Cell(data_column).Style.Fill.SetBackgroundColor(XLColor.Yellow);
                    data_column++;

                    // ���Ҥ�
                    sheet1.Row(data_row).Cell(data_column).Value = summary_ot.Count_Jan.HasValue && summary_ot.Count_Jan.Value != 0 ? summary_ot.Count_Jan.Value.ToString() : "";
                    sheet1.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    // ����Ҿѹ��
                    sheet1.Row(data_row).Cell(data_column).Value = summary_ot.Count_Feb.HasValue && summary_ot.Count_Feb.Value != 0 ? summary_ot.Count_Feb.Value.ToString() : "";
                    sheet1.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    // �չҤ�
                    sheet1.Row(data_row).Cell(data_column).Value = summary_ot.Count_Mar.HasValue && summary_ot.Count_Mar.Value != 0 ? summary_ot.Count_Mar.Value.ToString() : "";
                    sheet1.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    // ����¹
                    sheet1.Row(data_row).Cell(data_column).Value = summary_ot.Count_Apr.HasValue && summary_ot.Count_Apr.Value != 0 ? summary_ot.Count_Apr.Value.ToString() : "";
                    sheet1.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    // ����Ҥ�
                    sheet1.Row(data_row).Cell(data_column).Value = summary_ot.Count_May.HasValue && summary_ot.Count_May.Value != 0 ? summary_ot.Count_May.Value.ToString() : "";
                    sheet1.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    // �Զع�¹
                    sheet1.Row(data_row).Cell(data_column).Value = summary_ot.Count_Jun.HasValue && summary_ot.Count_Jun.Value != 0 ? summary_ot.Count_Jun.Value.ToString() : "";
                    sheet1.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    // �á�Ҥ�
                    sheet1.Row(data_row).Cell(data_column).Value = summary_ot.Count_Jul.HasValue && summary_ot.Count_Jul.Value != 0 ? summary_ot.Count_Jul.Value.ToString() : "";
                    sheet1.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    // �ԧ�Ҥ�
                    sheet1.Row(data_row).Cell(data_column).Value = summary_ot.Count_Aug.HasValue && summary_ot.Count_Aug.Value != 0 ? summary_ot.Count_Aug.Value.ToString() : "";
                    sheet1.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    // �ѹ��¹
                    sheet1.Row(data_row).Cell(data_column).Value = summary_ot.Count_Sep.HasValue && summary_ot.Count_Sep.Value != 0 ? summary_ot.Count_Sep.Value.ToString() : "";
                    sheet1.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    // ���Ҥ�
                    sheet1.Row(data_row).Cell(data_column).Value = summary_ot.Count_Oct.HasValue && summary_ot.Count_Oct.Value != 0 ? summary_ot.Count_Oct.Value.ToString() : "";
                    sheet1.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    // ��Ȩԡ�¹
                    sheet1.Row(data_row).Cell(data_column).Value = summary_ot.Count_Nov.HasValue && summary_ot.Count_Nov.Value != 0 ? summary_ot.Count_Nov.Value.ToString() : "";
                    sheet1.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    // �ѹ�Ҥ�
                    sheet1.Row(data_row).Cell(data_column).Value = summary_ot.Count_Dec.HasValue && summary_ot.Count_Dec.Value != 0 ? summary_ot.Count_Dec.Value.ToString() : "";
                    sheet1.Row(data_row).Cell(data_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    data_column++;

                    data_row++;

                    last_row_summary = data_row;
                }

                sheet1.Range(first_row_summary, 1, last_row_summary - 1, column_last).Style.Font.FontName = "Angsana New";
                sheet1.Range(first_row_summary, 1, last_row_summary - 1, column_last).Style.Font.FontSize = 14;
            }

            #endregion

            return workBook;

        }

        public void CalculatePeriodHour(decimal maxOtPerHour, List<PeriodOTSumary> list_period_ot, string month)
        {
            var period_ot = list_period_ot.Where(s => maxOtPerHour >= s.MinMinute && maxOtPerHour <= s.MaxMinute).FirstOrDefault();

            if (period_ot != null)
            {
                SearchMonthOtByHour(month, period_ot);
            }
        }

        public void SearchMonthOtByHour(string month, PeriodOTSumary report)
        {
            switch (month)
            {
                case "01": report.Count_Jan = report.Count_Jan.HasValue ? report.Count_Jan + 1 : 1; break;
                case "02": report.Count_Feb = report.Count_Feb.HasValue ? report.Count_Feb + 1 : 1; break;
                case "03": report.Count_Mar = report.Count_Mar.HasValue ? report.Count_Mar + 1 : 1; break;
                case "04": report.Count_Apr = report.Count_Apr.HasValue ? report.Count_Apr + 1 : 1; break;
                case "05": report.Count_May = report.Count_May.HasValue ? report.Count_May + 1 : 1; break;
                case "06": report.Count_Jun = report.Count_Jun.HasValue ? report.Count_Jun + 1 : 1; break;
                case "07": report.Count_Jul = report.Count_Jul.HasValue ? report.Count_Jul + 1 : 1; break;
                case "08": report.Count_Aug = report.Count_Aug.HasValue ? report.Count_Aug + 1 : 1; break;
                case "09": report.Count_Sep = report.Count_Sep.HasValue ? report.Count_Sep + 1 : 1; break;
                case "10": report.Count_Oct = report.Count_Oct.HasValue ? report.Count_Oct + 1 : 1; break;
                case "11": report.Count_Nov = report.Count_Nov.HasValue ? report.Count_Nov + 1 : 1; break;
                case "12": report.Count_Dec = report.Count_Dec.HasValue ? report.Count_Dec + 1 : 1; break;
            };
        }

        public void CalculateOtSumary(string month, OTLogMonthByHour report)
        {
            switch (month)
            {
                case "01": report.Count_Jan = report.Count_Jan.HasValue ? report.Count_Jan + 1 : 1; break;
                case "02": report.Count_Feb = report.Count_Feb.HasValue ? report.Count_Feb + 1 : 1; break;
                case "03": report.Count_Mar = report.Count_Mar.HasValue ? report.Count_Mar + 1 : 1; break;
                case "04": report.Count_Apr = report.Count_Apr.HasValue ? report.Count_Apr + 1 : 1; break;
                case "05": report.Count_May = report.Count_May.HasValue ? report.Count_May + 1 : 1; break;
                case "06": report.Count_Jun = report.Count_Jun.HasValue ? report.Count_Jun + 1 : 1; break;
                case "07": report.Count_Jul = report.Count_Jul.HasValue ? report.Count_Jul + 1 : 1; break;
                case "08": report.Count_Aug = report.Count_Aug.HasValue ? report.Count_Aug + 1 : 1; break;
                case "09": report.Count_Sep = report.Count_Sep.HasValue ? report.Count_Sep + 1 : 1; break;
                case "10": report.Count_Oct = report.Count_Oct.HasValue ? report.Count_Oct + 1 : 1; break;
                case "11": report.Count_Nov = report.Count_Nov.HasValue ? report.Count_Nov + 1 : 1; break;
                case "12": report.Count_Dec = report.Count_Dec.HasValue ? report.Count_Dec + 1 : 1; break;
            };
        }

        public void CountEmployeeOvertimePay(List<PeriodOTSumary> list_period_ot, OTLogMonthByHour summary)
        {
            summary.Count_Jan = list_period_ot.Where(s => s.Count_Jan.HasValue).Sum(s => s.Count_Jan);
            summary.Count_Feb = list_period_ot.Where(s => s.Count_Feb.HasValue).Sum(s => s.Count_Feb);
            summary.Count_Mar = list_period_ot.Where(s => s.Count_Mar.HasValue).Sum(s => s.Count_Mar);
            summary.Count_Apr = list_period_ot.Where(s => s.Count_Apr.HasValue).Sum(s => s.Count_Apr);
            summary.Count_May = list_period_ot.Where(s => s.Count_May.HasValue).Sum(s => s.Count_May);
            summary.Count_Jun = list_period_ot.Where(s => s.Count_Jun.HasValue).Sum(s => s.Count_Jun);
            summary.Count_Jul = list_period_ot.Where(s => s.Count_Jul.HasValue).Sum(s => s.Count_Jul);
            summary.Count_Aug = list_period_ot.Where(s => s.Count_Aug.HasValue).Sum(s => s.Count_Aug);
            summary.Count_Sep = list_period_ot.Where(s => s.Count_Sep.HasValue).Sum(s => s.Count_Sep);
            summary.Count_Oct = list_period_ot.Where(s => s.Count_Oct.HasValue).Sum(s => s.Count_Oct);
            summary.Count_Nov = list_period_ot.Where(s => s.Count_Nov.HasValue).Sum(s => s.Count_Nov);
            summary.Count_Dec = list_period_ot.Where(s => s.Count_Dec.HasValue).Sum(s => s.Count_Dec);
        }

        #endregion

        #region ��§ҹ��ػ�ѹ�һ�����Шӹǹ��ѡ�ҹ����һ���

        public List<string> GetListYearConfigSummaryAbsenceWorkLocation()
        {
            DateTime date_now = DateTime.Now;
            List<string> list_year = new List<string>();
            for (int i = 0; i < YEAR_SICK_LEAVE; i++)
            {
                if (i == 0)
                {
                    string year = date_now.Date.Year.ToString();
                    list_year.Add(year);
                }
                else
                {
                    string year = (date_now.Date.Year - i).ToString();
                    list_year.Add(year);
                }

            }
            return list_year;
        }

        public XLWorkbook ExportReportSummaryAbsenceInLocationByAdmin(string year, EmployeeData employee)
        {
            var workbook = new XLWorkbook();
            //var sheet = workbook.Worksheets.Add("��§ҹ��ػ��ѡ�ҹ�һ���");

            List<MasterWorkLocation> list_master_work_location = ServiceManager.CreateInstance(CompanyCode).ESSData.GetMasterWorklocation(Convert.ToInt16(year));

            List<DbAbsenceSickLeave> list_AbsenceSickLeave = ServiceManager.CreateInstance(CompanyCode).ESSData.GetAbsenceSickLeaveSummary(Convert.ToInt16(year), employee.OrgAssignment.SubAreaSetting.AbsAttGrouping);

            if (list_master_work_location.Count > 0)
            {
                foreach (var work_location in list_master_work_location)
                {
                    int charLength = work_location.Name.ToCharArray().Count();
                    int length_name = 0;
                    if (charLength < 30)
                        length_name = charLength;
                    else
                        length_name = 30;

                    string name_sheet = work_location.Name.Substring(0, length_name);

                    // ������� Validate ��õ�駪��� sheet �ͧ Excel
                    Regex reg = new Regex(@"[\[/\?\]\:*]");
                    name_sheet = reg.Replace(name_sheet, string.Empty);

                    // ���� sheet ��ͧ���¡�����ҡѺ 30 ����ѡ��
                    var sheet = workbook.Worksheets.Add(name_sheet);

                    #region Header Excel

                    int header_row = 1;
                    int header_column = 1;
                    string[] list_month_header = { "Topic", "Unit", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December", "Total" };

                    foreach (string txt_header in list_month_header)
                    {
                        sheet.Row(header_row).Cell(header_column).Value = txt_header;
                        sheet.Row(header_row).Cell(header_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        sheet.Column(header_column).Width = 10;

                        if (txt_header == "Topic")
                            sheet.Column(header_column).Width = 25;

                        if (txt_header == "Unit")
                            sheet.Column(header_column).Width = 10;
                        header_column++;
                    }

                    var rang_header = sheet.Range("A1:O1");
                    rang_header.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    rang_header.Style.Fill.BackgroundColor = XLColor.Gray;
                    rang_header.Style.Font.FontColor = XLColor.White;
                    rang_header.Style.Font.Bold = true;
                    rang_header.Style.Alignment.WrapText = true;
                    rang_header.Style.Font.FontName = "Angsana New";
                    rang_header.Style.Font.FontSize = 14;
                    rang_header.Style.Border
                        .SetOutsideBorder(XLBorderStyleValues.Thick)
                        .Border.SetInsideBorder(XLBorderStyleValues.Thick);

                    header_row = 2;
                    header_column = 1;

                    sheet.Row(header_row).Cell(header_column).Value = "Absentee Rate �һ��� (AR)";
                    rang_header = sheet.Range("A2:O2");
                    rang_header.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    rang_header.Style.Fill.BackgroundColor = XLColor.SkyBlue;
                    rang_header.Style.Font.Bold = true;
                    rang_header.Style.Alignment.WrapText = true;
                    rang_header.Style.Font.FontName = "Angsana New";
                    rang_header.Style.Font.FontSize = 14;
                    rang_header.Style.Border
                        .SetOutsideBorder(XLBorderStyleValues.Thick)
                        .Border.SetInsideBorder(XLBorderStyleValues.Thick);
                    #endregion

                    #region  Data Excel

                    var result_report = SummarySickLeaveEmployeeByLocation(list_AbsenceSickLeave, work_location.WorkLocationId);

                    int data_row;
                    int data_column;

                    #region  �ѹ��边ѡ�ҹ�һ���
                    data_row = 3;
                    data_column = 1;

                    sheet.Row(data_row).Cell(data_column).Value = "�ѹ��边ѡ�ҹ�һ���";
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = "�ѹ";
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = result_report.CountSickLeave.Jan;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = result_report.CountSickLeave.Feb;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = result_report.CountSickLeave.Mar;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = result_report.CountSickLeave.Apr;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = result_report.CountSickLeave.May;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = result_report.CountSickLeave.Jun;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = result_report.CountSickLeave.Jul;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = result_report.CountSickLeave.Aug;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = result_report.CountSickLeave.Sep;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = result_report.CountSickLeave.Oct;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = result_report.CountSickLeave.Nov;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = result_report.CountSickLeave.Dec;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = result_report.CountSickLeave.SickLeaveTotal;

                    var rang_data = sheet.Range("A3:O3");
                    rang_data.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    rang_data.Style.Font.FontName = "Angsana New";
                    rang_data.Style.Font.FontSize = 14;
                    rang_data.Style.Border
                        .SetOutsideBorder(XLBorderStyleValues.Thick)
                        .Border.SetInsideBorder(XLBorderStyleValues.Thick);

                    #endregion

                    #region �ӹǹ��ѡ�ҹ
                    data_row = 4;
                    data_column = 1;

                    sheet.Row(data_row).Cell(data_column).Value = "�ӹǹ��ѡ�ҹ";
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = "��";
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = result_report.CountEmployeeSickLeave.CountEmp_Jan;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = result_report.CountEmployeeSickLeave.CountEmp_Feb;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = result_report.CountEmployeeSickLeave.CountEmp_Mar;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = result_report.CountEmployeeSickLeave.CountEmp_Apr;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = result_report.CountEmployeeSickLeave.CountEmp_May;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = result_report.CountEmployeeSickLeave.CountEmp_Jun;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = result_report.CountEmployeeSickLeave.CountEmp_Jul;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = result_report.CountEmployeeSickLeave.CountEmp_Aug;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = result_report.CountEmployeeSickLeave.CountEmp_Sep;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = result_report.CountEmployeeSickLeave.CountEmp_Oct;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = result_report.CountEmployeeSickLeave.CountEmp_Nov;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = result_report.CountEmployeeSickLeave.CountEmp_Dec;
                    data_column++;

                    sheet.Row(data_row).Cell(data_column).Value = result_report.CountEmployeeSickLeave.EmpTotal;

                    rang_data = sheet.Range("A4:O4");
                    rang_data.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    rang_data.Style.Font.FontName = "Angsana New";
                    rang_data.Style.Font.FontSize = 14;
                    rang_data.Style.Border
                        .SetOutsideBorder(XLBorderStyleValues.Thick)
                        .Border.SetInsideBorder(XLBorderStyleValues.Thick);

                    #endregion

                    #endregion
                }
            }
            else
            {
                // ���� sheet ��ͧ���¡�����ҡѺ 30 ����ѡ��
                var sheet = workbook.Worksheets.Add("Sheet1");
                int header_row = 1;
                int header_column = 1;
                string[] list_month_header = { "Topic", "Unit", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December", "Total" };

                foreach (string txt_header in list_month_header)
                {
                    sheet.Row(header_row).Cell(header_column).Value = txt_header;
                    sheet.Row(header_row).Cell(header_column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    sheet.Column(header_column).Width = 10;

                    if (txt_header == "Topic")
                        sheet.Column(header_column).Width = 25;

                    if (txt_header == "Unit")
                        sheet.Column(header_column).Width = 10;
                    header_column++;
                }

                var rang_header = sheet.Range("A1:O1");
                rang_header.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                rang_header.Style.Fill.BackgroundColor = XLColor.Gray;
                rang_header.Style.Font.FontColor = XLColor.White;
                rang_header.Style.Font.Bold = true;
                rang_header.Style.Alignment.WrapText = true;
                rang_header.Style.Font.FontName = "Angsana New";
                rang_header.Style.Font.FontSize = 14;
                rang_header.Style.Border
                    .SetOutsideBorder(XLBorderStyleValues.Thick)
                    .Border.SetInsideBorder(XLBorderStyleValues.Thick);
            }

            return workbook;
        }

        public ReportSummarySickLeave SummarySickLeaveEmployeeByLocation(List<DbAbsenceSickLeave> list_AbsenceSickLeave, string locationId)
        {
            ReportSummarySickLeave result = new ReportSummarySickLeave();
            MonthAbsenceSickLeave sick_leave = new MonthAbsenceSickLeave();
            MonthAbsenceSickLeaveEmployee emp_sick_leave = new MonthAbsenceSickLeaveEmployee();

            if (list_AbsenceSickLeave.Count > 0)
            {
                // 1. group by ��ѡ�ҹ
                var group_employee = list_AbsenceSickLeave
                    .GroupBy(s => s.EmployeeID)
                    .ToList();
                foreach (var emp_data in group_employee)
                {
                    // 2 group by ੾�� Location 
                    var emp_group_location = emp_data.Where(s => s.WorkLocationCode == locationId).ToList();
                    if (emp_group_location.Count > 0)
                    {
                        decimal total_sick_leave = 0;
                        int total_emp_absence = 0;
                        bool is_count_month1 = false;
                        bool is_count_month2 = false;
                        bool is_count_month3 = false;
                        bool is_count_month4 = false;
                        bool is_count_month5 = false;
                        bool is_count_month6 = false;
                        bool is_count_month7 = false;
                        bool is_count_month8 = false;
                        bool is_count_month9 = false;
                        bool is_count_month10 = false;
                        bool is_count_month11 = false;
                        bool is_count_month12 = false;

                        foreach (var item in emp_group_location)
                        {
                            switch (item.ActionDate.Date.Month)
                            {
                                case 1:
                                    sick_leave.Jan += item.PayrollDays;
                                    if (is_count_month1 == false)
                                    {
                                        emp_sick_leave.CountEmp_Jan++;
                                        total_emp_absence++;
                                        is_count_month1 = true;
                                    }
                                    break;
                                case 2:
                                    sick_leave.Feb += item.PayrollDays;
                                    if (is_count_month2 == false)
                                    {
                                        emp_sick_leave.CountEmp_Feb++;
                                        total_emp_absence++;
                                        is_count_month2 = true;
                                    }
                                    break;
                                case 3:
                                    sick_leave.Mar += item.PayrollDays;
                                    if (is_count_month3 == false)
                                    {
                                        emp_sick_leave.CountEmp_Mar++;
                                        total_emp_absence++;
                                        is_count_month3 = true;
                                    }
                                    break;
                                case 4:
                                    sick_leave.Apr += item.PayrollDays;
                                    if (is_count_month4 == false)
                                    {
                                        emp_sick_leave.CountEmp_Apr++;
                                        total_emp_absence++;
                                        is_count_month4 = true;
                                    }
                                    break;
                                case 5:
                                    sick_leave.May += item.PayrollDays;
                                    if (is_count_month5 == false)
                                    {
                                        emp_sick_leave.CountEmp_May++;
                                        total_emp_absence++;
                                        is_count_month5 = true;
                                    }
                                    break;
                                case 6:
                                    sick_leave.Jun += item.PayrollDays;
                                    if (is_count_month6 == false)
                                    {
                                        emp_sick_leave.CountEmp_Jun++;
                                        total_emp_absence++;
                                        is_count_month6 = true;
                                    }
                                    break;
                                case 7:
                                    sick_leave.Jul += item.PayrollDays;
                                    if (is_count_month7 == false)
                                    {
                                        emp_sick_leave.CountEmp_Jul++;
                                        total_emp_absence++;
                                        is_count_month7 = true;
                                    }
                                    break;
                                case 8:
                                    sick_leave.Aug += item.PayrollDays;
                                    if (is_count_month8 == false)
                                    {
                                        emp_sick_leave.CountEmp_Aug++;
                                        total_emp_absence++;
                                        is_count_month8 = true;
                                    }
                                    break;
                                case 9:
                                    sick_leave.Sep += item.PayrollDays;
                                    if (is_count_month9 == false)
                                    {
                                        emp_sick_leave.CountEmp_Sep++;
                                        total_emp_absence++;
                                        is_count_month9 = true;
                                    }
                                    break;
                                case 10:
                                    sick_leave.Oct += item.PayrollDays;
                                    if (is_count_month10 == false)
                                    {
                                        emp_sick_leave.CountEmp_Oct++;
                                        total_emp_absence++;
                                        is_count_month10 = true;
                                    }
                                    break;
                                case 11:
                                    sick_leave.Nov += item.PayrollDays;
                                    if (is_count_month11 == false)
                                    {
                                        emp_sick_leave.CountEmp_Nov++;
                                        total_emp_absence++;
                                        is_count_month11 = true;
                                    }
                                    break;
                                case 12:
                                    sick_leave.Dec += item.PayrollDays;
                                    if (is_count_month12 == false)
                                    {
                                        emp_sick_leave.CountEmp_Dec++;
                                        total_emp_absence++;
                                        is_count_month12 = true;
                                    }
                                    break;
                            }
                            total_sick_leave += item.PayrollDays;
                        }
                        sick_leave.SickLeaveTotal += total_sick_leave; // �ӹǹ�ѹ��������边ѡ�ҹ����
                        emp_sick_leave.EmpTotal += total_emp_absence;
                    }
                }
            }

            result.CountSickLeave = sick_leave;
            result.CountEmployeeSickLeave = emp_sick_leave;

            return result;
        }

        #endregion

        public decimal GetOTSummaryInMonth(string EmployeeID, DateTime BeginDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetOTSummaryInMonth(EmployeeID, BeginDate);
        }

        #region 

        // �ͧ��� (�Ҩ���������)
        public List<OrgUnitEmployeeByRole> GetListOrgUnitByUserRole(string employeeId, string userRole, string companyCode)
        {
            List<OrgUnitEmployeeByRole> result_org = new List<OrgUnitEmployeeByRole>();

            var list_org_unit = ServiceManager.CreateInstance(CompanyCode).ESSData.GetListOrgUnitByUserRole(employeeId, userRole, companyCode);
            if (list_org_unit.Count > 0)
            {
                var arr_org_distinct = list_org_unit.Select(s => s.OrgUnit).Distinct().ToList();  // Org unit �����

                var list_new_org_distinct = new List<OrgUnitEmployeeByRole>();
                foreach (string org_id in arr_org_distinct)
                {
                    var org = list_org_unit.Where(s => s.OrgUnit == org_id).OrderByDescending(s => s.BeginDate).FirstOrDefault();
                    if (org != null)
                        list_new_org_distinct.Add(org);
                }

                if (list_new_org_distinct.Count > 0)
                {
                    result_org = ServiceManager.CreateInstance(CompanyCode).ESSData.GetListOrgUnitNameByUserRole(list_new_org_distinct);
                }
            }

            return result_org;
        }

        public List<OrgUnitTree> GetListOTOrgTree(string employeeId, int begin_year, int end_year)
        {
            var list_tree_org = new List<OrgUnitTree>();

            DateTime date_now = DateTime.Now;

            DateTime data_begin_year = new DateTime(begin_year, 1, 1);
            DateTime data_end_year = DateTime.Now;

            if (end_year == date_now.Year)
                data_end_year = date_now;
            else
                data_end_year = new DateTime(end_year, 12, 31);

            list_tree_org = ServiceManager.CreateInstance(CompanyCode).ESSData.GetOrgOTUnitTree(employeeId, data_begin_year, data_end_year);

            return list_tree_org;
        }


        public DashbaordYTDOvertimeandSalary GetDashbaordYTDOvertimeandSalary(string[] arr_org)
        {
            var dashbaord_tab1 = new DashbaordYTDOvertimeandSalary();

            // 1. Org Unit �Ҿ�ѡ�ҹ�ء��
            List<string> list_emp = ServiceManager.CreateInstance(CompanyCode).ESSData.GetEmployeeInOrganization(arr_org);
            var list_emp_distinct = list_emp.Distinct().ToList();

            // 2. �Ӿ�ѡ�ҹ����������������� �չ��
            List<PaySlipEmployeeInOrganization> list_emp_payslip = ServiceManager.CreateInstance(CompanyCode).ESSData.GetPaySlipEmployeeInOrganization(list_emp_distinct);

            // 3. Summary ����Թ��͹��� OT ��������������� Wage Type
            if (list_emp_payslip.Count > 0)
            {
                // 3.1  Summary �Թ��͹
                dashbaord_tab1.Salary = list_emp_payslip.Where(s => s.WageTypeCode == "1000").Sum(s => s.Amount);

                // 3.2 Summary OT
                dashbaord_tab1.Overtime = list_emp_payslip.Where(s => s.WageTypeCode == "2010"
                    || s.WageTypeCode == "2015" || s.WageTypeCode == "2030").Sum(s => s.Amount);
            }
            return dashbaord_tab1;
        }

        public List<string> GetListYearConfigDashbaordTab2()
        {
            DateTime date_now = DateTime.Now;
            List<string> list_year = new List<string>();
            for (int i = 0; i < YEAR_DASHBOARD_OT_Tab2; i++)
            {
                if (i == 0)
                {
                    string year = date_now.Date.Year.ToString();
                    list_year.Add(year);
                }
                else
                {
                    string year = (date_now.Date.Year - i).ToString();
                    list_year.Add(year);
                }

            }
            return list_year;
        }

        public DashboardOTReason GetDashboardOTReason(string[] arr_org, int year, int month)
        {
            var dashboard_ot_reason = new DashboardOTReason();

            // 1. Org Unit �Ҿ�ѡ�ҹ�ء��
            List<string> list_emp = ServiceManager.CreateInstance(CompanyCode).ESSData.GetEmployeeInOrganization(arr_org);
            var list_emp_distinct = list_emp.Distinct().ToList();

            // 2. Get OTWorkType
            List<DbOTWorkType> list_work_type = ServiceManager.CreateInstance(CompanyCode).ESSData.GetListOTWorkType();

            // 3. �Ӿ�ѡ�ҹ� Org ��� OT Reason
            List<DbOTReason> list_ot_reason = ServiceManager.CreateInstance(CompanyCode).ESSData.GetDashbaordOTReason(list_emp_distinct, year, month);

            // 4. ���Ҥӹǳ�����ա���� ������ OT Type ����
            if (list_ot_reason.Count > 0)
            {
                var new_list_ot_reason = new List<DbOTReason>();
                var group_ot_reason = list_ot_reason.OrderBy(s => s.OTWorkTypeID).GroupBy(s => s.OTWorkTypeID).ToList();
                foreach (var ot_work_type in group_ot_reason)
                {
                    DbOTReason n_ot = new DbOTReason()
                    {
                        OTWorkTypeID = ot_work_type.Key,
                        OTWorkTypeDesc = ot_work_type.First().OTWorkTypeDesc,
                        Count_OT = ot_work_type.Sum(s => s.Count_OT)
                    };
                    new_list_ot_reason.Add(n_ot);
                }

                // 5. �Ң����ŷ�� Max ����ش
                decimal max_100 = new_list_ot_reason.Sum(s => s.Count_OT);

                // 6 �Ѵ��������� Array
                if (list_work_type.Count > 0)
                {
                    foreach (var item in list_work_type)
                    {
                        dashboard_ot_reason.ListTextOT.Add(item.OTWorkTypeDesc);
                        var check_dup = new_list_ot_reason.FirstOrDefault(s => s.OTWorkTypeID == item.OTWorkTypeID);
                        if (check_dup != null)
                        {
                            decimal percentage_txt = Math.Round((check_dup.Count_OT * 100) / max_100, 2);
                            dashboard_ot_reason.ListPercentageOT.Add(percentage_txt);
                        }
                        else
                        {
                            dashboard_ot_reason.ListPercentageOT.Add(0);
                        }
                    }

                    decimal percentage_total = dashboard_ot_reason.ListPercentageOT.Sum(s => s);
                    if (percentage_total > 0 && percentage_total != 100)
                    {
                        if (percentage_total > 100)
                        {
                            decimal diff = percentage_total - 100;
                            decimal percentage_max = dashboard_ot_reason.ListPercentageOT.Max(s => s);
                            bool is_diff = false;
                            for (int i = 0; i < dashboard_ot_reason.ListPercentageOT.Count; i++)
                            {
                                if (percentage_max == dashboard_ot_reason.ListPercentageOT[i])
                                {
                                    if (!is_diff)
                                    {
                                        dashboard_ot_reason.ListPercentageOT[i] = dashboard_ot_reason.ListPercentageOT[i] - diff;
                                        is_diff = true;
                                    }
                                }
                            }
                        }
                        else
                        {
                            decimal diff = 100 - percentage_total;
                            decimal percentage_max = dashboard_ot_reason.ListPercentageOT.Max(s => s);
                            bool is_diff = false;
                            for (int i = 0; i < dashboard_ot_reason.ListPercentageOT.Count; i++)
                            {
                                if (percentage_max == dashboard_ot_reason.ListPercentageOT[i])
                                {
                                    if (!is_diff)
                                    {
                                        dashboard_ot_reason.ListPercentageOT[i] = dashboard_ot_reason.ListPercentageOT[i] + diff;
                                        is_diff = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                // ����բ�����
                foreach (var item in list_work_type)
                {
                    dashboard_ot_reason.ListTextOT.Add(item.OTWorkTypeDesc);
                    dashboard_ot_reason.ListPercentageOT.Add(0);
                }
            }

            return dashboard_ot_reason;
        }

        public DashboardOvertimebyBusinessUnitinmonth GetDashboardOvertimebyBusinessUnitinmonth(string[] arr_org, int year, int month, string selectOrgId, string employeeId)
        {
            var dashboard = new DashboardOvertimebyBusinessUnitinmonth();
            var data_pay_overtime = new List<PayOvertimeInOrganization>();

            #region Logic ���
            //var data_pay_overtime = new List<PayOvertimeInOrganization>();
            //foreach (string org_id in arr_org)
            //{
            //    var data = new PayOvertimeInOrganization();

            //    // 1. �Ҫ��� Organization
            //    var data_organization = ServiceManager.CreateInstance(CompanyCode).ESSData.GetNameOrganizationInfotype1000(org_id);

            //    // 2. �Ҿ�ѡ�ҹ����ѧ�Ѵ����� Org
            //    List<string> list_employee = ServiceManager.CreateInstance(CompanyCode).ESSData.GetEmployeeInOrganization(org_id);
            //    var list_employee_distinct = list_employee.Distinct().ToList();

            //    // 3. �Ҿ�ѡ�ҹ���˹����ԡ OT  (�͡�������������Ǻ�ҧ ���ԧ�ҡ �� �����͹)
            //    List<DbOvertimeInMonth> list_ot_emp = ServiceManager.CreateInstance(CompanyCode).ESSData
            //        .GetTransactionPayOvertimeByEmployeeInOrganization(list_employee_distinct, year, month);


            //    if (!string.IsNullOrEmpty(data_organization.ShortText))
            //    {
            //        data.OrganizationId = org_id;
            //        data.OrganizationName = data_organization.ShortText;

            //        if (list_ot_emp.Count > 0)
            //            data.AmountOvertimeByOrg = list_ot_emp.Sum(s => s.Amount);
            //        else
            //            data.AmountOvertimeByOrg = 0;

            //        data_pay_overtime.Add(data);
            //    }
            //}

            //if (data_pay_overtime.Count > 0)
            //{
            //    decimal max100 = data_pay_overtime.Sum(s => s.AmountOvertimeByOrg);
            //    foreach (var item in data_pay_overtime)
            //    {
            //        if (max100 > 0)
            //        {
            //            string unit_name = item.OrganizationName;
            //            decimal percentage = (item.AmountOvertimeByOrg * 100) / max100;
            //            percentage = Math.Round(percentage, 2);

            //            dashboard.UnitName.Add(unit_name);
            //            dashboard.PercentageOverTime.Add(percentage);
            //            dashboard.CodeColor.Add("#00ADEE");
            //        }
            //        else
            //        {
            //            string unit_name = item.OrganizationName;
            //            dashboard.UnitName.Add(unit_name);
            //            dashboard.PercentageOverTime.Add(0);
            //            dashboard.CodeColor.Add("#00ADEE");
            //        }
            //    }
            //}
            #endregion

            var list_tree_org = GetListOTOrgTree(employeeId, year, year);

            var org_select = list_tree_org.FirstOrDefault(s => s.OrgUnit == selectOrgId);

            // ˹��§ҹ������͡
            List<string> list_employee = ServiceManager.CreateInstance(CompanyCode).ESSData.GetEmployeeInOrganization(selectOrgId);
            var list_employee_distinct = list_employee.Distinct().ToList();

            List<DbOvertimeInMonth> list_ot_emp = ServiceManager.CreateInstance(CompanyCode).ESSData
                .GetTransactionPayOvertimeByEmployeeInOrganization(list_employee_distinct, year, month);

            if (org_select != null)
            {
                var data = new PayOvertimeInOrganization();
                data.OrganizationId = selectOrgId;
                data.OrganizationName = org_select.OrgName;
                if (list_ot_emp.Count > 0)
                    data.AmountOvertimeByOrg = list_ot_emp.Sum(s => s.Amount);
                else
                    data.AmountOvertimeByOrg = 0;

                data_pay_overtime.Add(data);
            }

            // ˹��§ҹ������������ 1 Level
            var list_child_org = list_tree_org.Where(s => s.ParentOrgUnit == selectOrgId).ToList();
            if (list_child_org.Count > 0)
            {
                foreach (var child_org in list_child_org)
                {
                    var list_root_child = list_tree_org.Where(s => s.OrgUnitPath.Contains(child_org.OrgUnit)).ToList();
                    decimal total_amount_root_child = 0;
                    if (list_root_child.Count > 0)
                    {
                        foreach (var root_child in list_root_child)
                        {
                            list_employee = ServiceManager.CreateInstance(CompanyCode).ESSData.GetEmployeeInOrganization(root_child.OrgUnit);
                            list_employee_distinct = list_employee.Distinct().ToList();

                            list_ot_emp = ServiceManager.CreateInstance(CompanyCode).ESSData.GetTransactionPayOvertimeByEmployeeInOrganization(list_employee_distinct, year, month);

                            if (list_ot_emp.Count > 0)
                            {
                                total_amount_root_child = list_ot_emp.Sum(s => s.Amount);
                            }
                        }
                    }
                    var data = new PayOvertimeInOrganization()
                    {
                        OrganizationId = child_org.OrgUnit,
                        OrganizationName = child_org.OrgName,
                        AmountOvertimeByOrg = total_amount_root_child
                    };
                    data_pay_overtime.Add(data);
                }
            }

            if (data_pay_overtime.Count > 0)
            {
                decimal max100 = data_pay_overtime.Sum(s => s.AmountOvertimeByOrg);
                foreach (var item in data_pay_overtime)
                {
                    if (max100 > 0)
                    {
                        string unit_name = item.OrganizationName;
                        decimal percentage = (item.AmountOvertimeByOrg * 100) / max100;
                        percentage = Math.Round(percentage, 2);

                        dashboard.UnitName.Add(unit_name);
                        dashboard.PercentageOverTime.Add(percentage);
                        dashboard.CodeColor.Add("#00ADEE");
                    }
                    else
                    {
                        string unit_name = item.OrganizationName;
                        dashboard.UnitName.Add(unit_name);
                        dashboard.PercentageOverTime.Add(0);
                        dashboard.CodeColor.Add("#00ADEE");
                    }
                    dashboard.Amount.Add(item.AmountOvertimeByOrg);
                }
            }

            return dashboard;
        }

        public DashbaordOvertimeAndSalaryByMonth GetDashbaordOvertimeAndSalaryByMonth(string[] arr_org, int year)
        {
            DateTime date_data = new DateTime(year, 12, 31);

            // ��͹�ʴ���
            List<string> month_txt = new List<string>();
            for (int i = 1; i <= 12; i++)
            {
                DateTime new_date = new DateTime(year, i, 1);
                string txt_mon = new_date.ToString("MMMM yy", CultureInfo.CreateSpecificCulture("en-US"));
                month_txt.Add(txt_mon);
            }

            // 1. Org Unit �Ҿ�ѡ�ҹ�ء��
            List<string> list_emp = ServiceManager.CreateInstance(CompanyCode).ESSData.GetEmployeeInOrganization(arr_org);
            var list_emp_distinct = list_emp.Distinct().ToList();

            // 2. �ӹǳ�Թ��͹ , OT , �Ѵ��ǹ OT �����ҧ�Թ��͹
            var dashboard = ServiceManager.CreateInstance(CompanyCode).ESSData.GetSalaryOrOvertimeInYear(list_emp_distinct, year);

            dashboard.Month = month_txt;

            return dashboard;
        }

        public DashbaordOTSalary6Months GetDashbaordOTSalary6Months(string[] arr_org, int year, int month)
        {
            var dashboard = new DashbaordOTSalary6Months();

            // 1. ����͹��͹��ѧ 6 ��͹
            List<DateTime> list_month_six = new List<DateTime>();
            for (int i = 0; i < 6; i++)
            {
                DateTime date_select = new DateTime(year, month, 1);

                if (i > 0)
                    date_select = date_select.AddMonths(-i);

                list_month_six.Add(date_select);
            }

            // 2. Org Unit �Ҿ�ѡ�ҹ�ء��
            List<string> list_emp = ServiceManager.CreateInstance(CompanyCode).ESSData.GetEmployeeInOrganization(arr_org);
            var list_emp_distinct = list_emp.Distinct().ToList();

            // 3. Sort Datetime ������§ ���� => �ҡ
            var sort_datetime = list_month_six.OrderBy(s => s).ToList();

            dashboard = ServiceManager.CreateInstance(CompanyCode)
                .ESSData.GetDashbaordOTSalary6Months(sort_datetime, list_emp_distinct);

            return dashboard;
        }

        public DashbaordOTHours6Months GetDashbaordOTHours6Months(string[] arr_org, int year, int month)
        {
            var dashboard = new DashbaordOTHours6Months();

            // 1. ����͹��͹��ѧ 6 ��͹
            List<DateTime> list_month_six = new List<DateTime>();
            for (int i = 0; i < 6; i++)
            {
                DateTime date_select = new DateTime(year, month, 1);

                if (i > 0)
                    date_select = date_select.AddMonths(-i);

                list_month_six.Add(date_select);
            }

            // 2. Org Unit �Ҿ�ѡ�ҹ�ء��
            List<string> list_emp = ServiceManager.CreateInstance(CompanyCode).ESSData.GetEmployeeInOrganization(arr_org);
            var list_emp_distinct = list_emp.Distinct().ToList();

            // 3. Sort Datetime ������§ ���� => �ҡ
            var sort_datetime = list_month_six.OrderBy(s => s).ToList();

            // 4. Summary Salary , OvertimePay , OT Hour
            dashboard = ServiceManager.CreateInstance(CompanyCode)
                .ESSData.GetDashbaordOTHours6Months(sort_datetime, list_emp_distinct);

            return dashboard;
        }

        public List<string> GetListYearConfigDashbaordTab3()
        {
            DateTime date_now = DateTime.Now;
            List<string> list_year = new List<string>();
            for (int i = 0; i < YEAR_DASHBOARD_OT_Tab3; i++)
            {
                if (i == 0)
                {
                    string year = date_now.Date.Year.ToString();
                    list_year.Add(year);
                }
                else
                {
                    string year = (date_now.Date.Year - i).ToString();
                    list_year.Add(year);
                }

            }
            return list_year;
        }

        public DashboardSummarySalaryOvertimePayByYear GetDashboardSummarySalaryOvertimePayByYear(string[] arr_org, int begin_year, int end_year)
        {
            var dashboard = new DashboardSummarySalaryOvertimePayByYear();

            // 1. ��ѡ�ҹ� ORG
            List<string> list_emp = ServiceManager.CreateInstance(CompanyCode).ESSData.GetEmployeeInOrganization(arr_org);
            var list_emp_distinct = list_emp.Distinct().ToList();

            dashboard = ServiceManager.CreateInstance(CompanyCode).ESSData.GetDashboardSummarySalaryOvertimePayByYear(list_emp_distinct, begin_year, end_year);

            return dashboard;
        }

        public DashboardSummarySalaryOvertimeHourByYear GetDashboardSummarySalaryOvertimeHourByYear(string[] arr_org, int begin_year, int end_year)
        {
            var dashboard = new DashboardSummarySalaryOvertimeHourByYear();

            // 1. ��ѡ�ҹ� ORG
            List<string> list_emp = ServiceManager.CreateInstance(CompanyCode).ESSData.GetEmployeeInOrganization(arr_org);
            var list_emp_distinct = list_emp.Distinct().ToList();

            // 2. �Ҽ������»�
            dashboard = ServiceManager.CreateInstance(CompanyCode).ESSData.GetDashboardSummarySalaryOvertimeHourByYear(list_emp_distinct, begin_year, end_year);

            return dashboard;
        }

        public OTEmployeeByLevel GetDashboardOTEmployeeByLevel(string[] arr_org, int begin_year, int end_year)
        {
            var dashboard = new OTEmployeeByLevel();

            // 1. ��ѡ�ҹ� ORG
            List<string> list_emp = ServiceManager.CreateInstance(CompanyCode).ESSData.GetEmployeeInOrganization(arr_org);
            var list_emp_distinct = list_emp.Distinct().ToList();

            // 2.�ӹǹ��ѡ�ҹ���� Level ����ԡ OT
            dashboard = ServiceManager.CreateInstance(CompanyCode).ESSData.GetDashboardOTEmployeeByLevel(list_emp_distinct, begin_year, end_year);

            return dashboard;
        }

        public OTEmployeeByBath GetDashboardOTEmployeeByBath(string[] arr_org, int begin_year, int end_year)
        {
            var dashboard = new OTEmployeeByBath();

            // 1. ��ѡ�ҹ� ORG
            List<string> list_emp = ServiceManager.CreateInstance(CompanyCode).ESSData.GetEmployeeInOrganization(arr_org);
            var list_emp_distinct = list_emp.Distinct().ToList();

            // 2.�ӹǹ��ѡ�ҹ���� Level ����ԡ OT
            dashboard = ServiceManager.CreateInstance(CompanyCode).ESSData.GetDashboardOTEmployeeByBath(list_emp_distinct, begin_year, end_year);

            return dashboard;
        }

        #endregion

        public HolidayMaxMin GetHolidayByMonthYear(EmployeeData oEmp, DateTime CheckDate, int iYear, int iMonth)
        {

            HolidayMaxMin holiday_data = new HolidayMaxMin();

            DateTime dMinDate = oEmp.DateSpecific.HiringDate;
            DateTime dMaxDate = EmployeeManagement.CreateInstance(CompanyCode).GetCalendarEndDate(oEmp.EmployeeID);

            holiday_data.HireDate = dMinDate.AddDays(-1).ToString("yyyy-MM-dd");
            holiday_data.MaxDate = dMaxDate.ToString("yyyy-MM-dd");

            List<Holiday> oMonthlyWorkSchedulers = new List<Holiday>();

            MonthlyWS oMonthlyWS = EmployeeManagement.CreateInstance(CompanyCode).GetCalendar(oEmp.EmployeeID, CheckDate, iYear, iMonth, true);
            if (oMonthlyWS != null)
            {
                if (oMonthlyWS.Day29 == null) { oMonthlyWS.Day29 = ""; }
                if (oMonthlyWS.Day29_H == null) { oMonthlyWS.Day29_H = ""; }
                if (oMonthlyWS.Day30 == null) { oMonthlyWS.Day30 = ""; }
                if (oMonthlyWS.Day30_H == null) { oMonthlyWS.Day30_H = ""; }
                if (oMonthlyWS.Day31 == null) { oMonthlyWS.Day31 = ""; }
                if (oMonthlyWS.Day31_H == null) { oMonthlyWS.Day31_H = ""; }


                Holiday oCurrentMonthlyWorkScheduler = null;
                List<string> oDayList = null;

                if (!string.IsNullOrEmpty(oMonthlyWS.EmpSubAreaForWorkSchedule) && !string.IsNullOrEmpty(oMonthlyWS.EmpSubGroupForWorkSchedule))
                {
                    oDayList = GetPropertyFromValueDecision<MonthlyWS>(oMonthlyWS, "_H", "Day,_H", "", "1");
                    oDayList.AddRange(GetPropertyFromValueDecision<MonthlyWS>(oMonthlyWS, "Day", "Day,_H", "", "OFF"));
                    for (int j = 0; j < oDayList.Count; j++)
                    {
                        oCurrentMonthlyWorkScheduler = new Holiday();
                        oCurrentMonthlyWorkScheduler.Day = oDayList[j];
                        oCurrentMonthlyWorkScheduler.Month = oMonthlyWS.WS_Month.ToString();
                        oCurrentMonthlyWorkScheduler.Year = oMonthlyWS.WS_Year.ToString();

                        string IsHoliday = string.Format("{0}{1}{2}", "Day", oCurrentMonthlyWorkScheduler.Day, "_H");
                        if (oMonthlyWS.GetType().GetProperty(IsHoliday) != null)
                        {
                            if ((string)oMonthlyWS.GetType().GetProperty(IsHoliday).GetValue(oMonthlyWS, null) == "1")
                            {
                                oCurrentMonthlyWorkScheduler.Description = "HOLIDAY";
                            }
                            else
                            {
                                oCurrentMonthlyWorkScheduler.Description = "WEEKEND";
                            }
                        }

                        oMonthlyWorkSchedulers.Add(oCurrentMonthlyWorkScheduler);
                    }
                }

            }

            holiday_data.Holiday = oMonthlyWorkSchedulers;

            return holiday_data;
        }

        public DataTable GetOTTrackingStatusReport(DateTime BeginDate, DateTime EndDate, string EmpID, string OrgUnitList, string CompanyCode, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetOTTrackingStatusReport(BeginDate, EndDate, EmpID, OrgUnitList, CompanyCode, LanguageCode);
        }

        public XLWorkbook ExportExcelOTTrackingStatusReport(DataTable oDT, string sLanguageCode)
        {
            var workbook = new XLWorkbook();
            var sheet1 = workbook.Worksheets.Add("Sheet1");


            int header_row = 1;
            int header_column = 1;

            #region Header Excel

            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("OTTrackingStatusReport", sLanguageCode, "COL_OTSender"); // 1
            //sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("OTTrackingStatusReport", sLanguageCode, "COL_OTRequestor"); // 2
            //sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("OTTrackingStatusReport", sLanguageCode, "COL_OrgUnitFullName"); // 3
            //sheet1.Column(header_column).Width = 50;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("OTTrackingStatusReport", sLanguageCode, "COL_OrgUnitShortName"); // 4
            //sheet1.Column(header_column).Width = 15;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("OTTrackingStatusReport", sLanguageCode, "COL_WorkLocation"); // 5
            //sheet1.Column(header_column).Width = 15;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("OTTrackingStatusReport", sLanguageCode, "COL_Province"); // 6
            //sheet1.Column(header_column).Width = 15;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("OTTrackingStatusReport", sLanguageCode, "COL_OTPeriod");  // 7
            //sheet1.Column(header_column).Width = 15;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("OTTrackingStatusReport", sLanguageCode, "COL_OTBeginDate");  // 8
            //sheet1.Column(header_column).Width = 20;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("OTTrackingStatusReport", sLanguageCode, "COL_OTEndDate");  // 9
            //sheet1.Column(header_column).Width = 20;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("OTTrackingStatusReport", sLanguageCode, "COL_RequestDocStatus"); // 10
            //sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("OTTrackingStatusReport", sLanguageCode, "COL_RequestNo"); // 11
            //sheet1.Column(header_column).Width = 20;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("OTTrackingStatusReport", sLanguageCode, "COL_ApprovedDate");  // 12
            //sheet1.Column(header_column).Width = 20;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("OTTrackingStatusReport", sLanguageCode, "COL_OTRequestHours_10");  // 13
            //sheet1.Column(header_column).Width = 15;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("OTTrackingStatusReport", sLanguageCode, "COL_OTRequestHours_15");  // 14
            //sheet1.Column(header_column).Width = 15;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("OTTrackingStatusReport", sLanguageCode, "COL_OTRequestHours_30"); // 15
            //sheet1.Column(header_column).Width = 15;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("OTTrackingStatusReport", sLanguageCode, "COL_OTApproveHours_10");  // 16
            //sheet1.Column(header_column).Width = 15;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("OTTrackingStatusReport", sLanguageCode, "COL_OTApproveHours_15");  // 17
            //sheet1.Column(header_column).Width = 15;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("OTTrackingStatusReport", sLanguageCode, "COL_OTApproveHours_30");  // 18
            //sheet1.Column(header_column).Width = 15;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("OTTrackingStatusReport", sLanguageCode, "COL_OTWorkType");  // 19
            //sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("OTTrackingStatusReport", sLanguageCode, "COL_OTWorkDescription");  // 20
            //sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("OTTrackingStatusReport", sLanguageCode, "COL_MonthlyOTRequestNo");  // 21
            //sheet1.Column(header_column).Width = 15;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("OTTrackingStatusReport", sLanguageCode, "COL_MonthlyOTApprover");  // 22
            //sheet1.Column(header_column).Width = 15;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("OTTrackingStatusReport", sLanguageCode, "COL_MonthlyOTRequestDate");  // 23
            //sheet1.Column(header_column).Width = 20;
            header_column++;
            #endregion

            var range = sheet1.Range("A1:W1");

            range.Style.Fill.BackgroundColor = XLColor.Yellow;
            range.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            range.Style.Font.FontName = "Angsana New";
            range.Style.Font.FontSize = 14;
            range.Style.Border.SetOutsideBorder(XLBorderStyleValues.Thick).Border.SetInsideBorder(XLBorderStyleValues.Thick);

            #region Data Excel


            if (oDT.Rows.Count > 0)
            {
                int column;
                int row = 2;
                int last_column = 0;
                int last_row = 0;
                foreach (DataRow dRow in oDT.Rows)
                {
                    column = 1;

                    sheet1.Row(row).Cell(column).Value = dRow["OTSender"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["OTRequestor"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["OrgUnitFullName"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["OrgUnitShortName"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["WorkLocation"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["Province"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = Convert.ToString(dRow["OTPeriod"]);
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    column++;

                    //sheet1.Row(row).Cell(column).Value = Convert.ToDateTime(dRow["OTBeginDate"]).ToString("dd/MM/yyyy HH:mm", oCL);
                    sheet1.Row(row).Cell(column).Value = dRow["OTBeginDate"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    column++;

                    //sheet1.Row(row).Cell(column).Value = Convert.ToDateTime(dRow["OTEndDate"]).ToString("dd/MM/yyyy HH:mm", oCL);
                    sheet1.Row(row).Cell(column).Value = dRow["OTEndDate"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["RequestDocStatus"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["RequestNo"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    //sheet1.Row(row).Cell(column).Value = Convert.ToDateTime(dRow["ApprovedDate"]).ToString("dd/MM/yyyy HH:mm", oCL);
                    sheet1.Row(row).Cell(column).Value = dRow["ApprovedDate"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["OTRequestHours_10"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["OTRequestHours_15"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["OTRequestHours_30"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["OTApproveHours_10"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["OTApproveHours_15"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["OTApproveHours_30"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["OTWorkType"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["OTWorkDescription"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["MonthlyOTRequestNo"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["MonthlyOTApprover"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["MonthlyOTRequestDate"];
                    //sheet1.Row(row).Cell(column).Value = dRow["MonthlyOTRequestDate"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    if (last_column == 0)
                        last_column = column;

                    row++;
                }

                last_row = row - 1;
                var rang_detail = sheet1.Range(2, 1, last_row, last_column);
                rang_detail.Style.Font.FontName = "Angsana New";
                rang_detail.Style.Font.FontSize = 14;
            }

            #endregion

            range.SetAutoFilter();
            range.Style.Alignment.SetShrinkToFit(true);
            sheet1.Columns().AdjustToContents();
            sheet1.SheetView.Freeze(1, 1);

            return workbook;
        }


        public DataTable GetDutyTrackingStatusReport(DateTime BeginDate, DateTime EndDate, string EmpID, string OrgUnitList, string CompanyCode, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetDutyTrackingStatusReport(BeginDate, EndDate, EmpID, OrgUnitList, CompanyCode, LanguageCode);
        }

        public XLWorkbook ExportExcelDutyTrackingStatusReport(DataTable oDT, string sLanguageCode)
        {

            var workbook = new XLWorkbook();
            var sheet1 = workbook.Worksheets.Add("Sheet1");

            int header_row = 1;
            int header_column = 1;

            #region Header Excel
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("DutyTrackingStatusReport", sLanguageCode, "COL_EmployeeID");  // 1
            //sheet1.Column(header_column).Width = 10;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("DutyTrackingStatusReport", sLanguageCode, "COL_EmployeeName");  // 2
            //sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("DutyTrackingStatusReport", sLanguageCode, "COL_OrgUnitFullName");  // 3
            //sheet1.Column(header_column).Width = 50;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("DutyTrackingStatusReport", sLanguageCode, "COL_OrgUnitShortName");  // 4
            //sheet1.Column(header_column).Width = 10;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("DutyTrackingStatusReport", sLanguageCode, "COL_WorkLocation");  // 5
            //sheet1.Column(header_column).Width = 20;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("DutyTrackingStatusReport", sLanguageCode, "COL_Province");  // 6
            //sheet1.Column(header_column).Width = 20;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("DutyTrackingStatusReport", sLanguageCode, "COL_RequestNo");  // 7
            //sheet1.Column(header_column).Width = 20;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("DutyTrackingStatusReport", sLanguageCode, "COL_DutyDate");  // 8
            //sheet1.Column(header_column).Width = 15;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("DutyTrackingStatusReport", sLanguageCode, "COL_BeginDate");  // 9
            //sheet1.Column(header_column).Width = 15;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("DutyTrackingStatusReport", sLanguageCode, "COL_EndDate");  // 10
            //sheet1.Column(header_column).Width = 15;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("DutyTrackingStatusReport", sLanguageCode, "COL_DocumentStatus");  // 11
            //sheet1.Column(header_column).Width = 30;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("DutyTrackingStatusReport", sLanguageCode, "COL_ApproverName");  // 12
            //sheet1.Column(header_column).Width = 50;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("DutyTrackingStatusReport", sLanguageCode, "COL_ApprovedDate");  // 13
            //sheet1.Column(header_column).Width = 15;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("DutyTrackingStatusReport", sLanguageCode, "COL_DutyWageType");  // 14
            //sheet1.Column(header_column).Width = 10;
            header_column++;


            #endregion

            var range = sheet1.Range("A1:N1");
            range.Style.Fill.BackgroundColor = XLColor.Yellow;
            range.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            range.Style.Font.FontName = "Angsana New";
            range.Style.Font.FontSize = 14;
            range.Style.Border.SetOutsideBorder(XLBorderStyleValues.Thick).Border.SetInsideBorder(XLBorderStyleValues.Thick);

            #region Data Excel


            if (oDT.Rows.Count > 0)
            {
                int column;
                int row = 2;
                int last_column = 0;
                int last_row = 0;
                foreach (DataRow dRow in oDT.Rows)
                {
                    column = 1;

                    sheet1.Row(row).Cell(column).Value = dRow["EmployeeID"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["EmployeeName"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["OrgUnitFullName"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["OrgUnitShortName"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["WorkLocation"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["Province"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["RequestNo"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["DutyDate"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["BeginDate"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["EndDate"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["DocumentStatus"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["ApproverName"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["ApprovedDate"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["Duty WageType"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    if (last_column == 0)
                        last_column = column;

                    //.AutoFitColumnWidth(0, false);
                    row++;
                }

                last_row = row - 1;
                var rang_detail = sheet1.Range(2, 1, last_row, last_column);
                rang_detail.Style.Font.FontName = "Angsana New";
                rang_detail.Style.Font.FontSize = 14;
            }

            #endregion

            range.SetAutoFilter();
            range.Style.Alignment.SetShrinkToFit(true);
            sheet1.Columns().AdjustToContents();
            sheet1.SheetView.Freeze(1, 1);

            return workbook;
        }

        public DataTable GetAttendanceMonthlyReport(DateTime BeginDate, DateTime EndDate, string EmpID, string OrgUnitList, string CompanyCode, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetAttendanceMonthlyReport(BeginDate, EndDate, EmpID, OrgUnitList, CompanyCode, LanguageCode);
        }

        public XLWorkbook ExportExcelAttendanceMonthlyReport(DataTable oDT, string sLanguageCode)
        {
            var workbook = new XLWorkbook();
            var sheet1 = workbook.Worksheets.Add("Sheet1");

            int header_row = 1;
            int header_column = 1;

            #region Header Excel
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("AttendanceMonthlyReport", sLanguageCode, "COL_EmployeeID");  // 1
            //sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("AttendanceMonthlyReport", sLanguageCode, "COL_EmployeeName");  // 2
            //sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("AttendanceMonthlyReport", sLanguageCode, "COL_OrgUnitFullName");  // 3
                                                                                                                                                //            sheet1.Column(header_column).Width = 50;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("AttendanceMonthlyReport", sLanguageCode, "COL_OrgUnitShortName");  // 4
            //sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("AttendanceMonthlyReport", sLanguageCode, "COL_WorkLocation");  // 5
            //sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("AttendanceMonthlyReport", sLanguageCode, "COL_Province");  // 6
                                                                                                                                         //            sheet1.Column(header_column).Width = 50;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("AttendanceMonthlyReport", sLanguageCode, "COL_AttendanceType");  // 7
            //sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("AttendanceMonthlyReport", sLanguageCode, "COL_AttendanceName");  // 8
            //sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("AttendanceMonthlyReport", sLanguageCode, "COL_BeginDate");  // 9
                                                                                                                                          //            sheet1.Column(header_column).Width = 50;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("AttendanceMonthlyReport", sLanguageCode, "COL_EndDate");  // 10
            //sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("AttendanceMonthlyReport", sLanguageCode, "COL_BeginTime");  // 11
            //sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("AttendanceMonthlyReport", sLanguageCode, "COL_EndTime");  // 12
                                                                                                                                        //            sheet1.Column(header_column).Width = 50;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("AttendanceMonthlyReport", sLanguageCode, "COL_PayrollDays");  // 13
            //sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("AttendanceMonthlyReport", sLanguageCode, "COL_Remark");  // 14
                                                                                                                                       //            sheet1.Column(header_column).Width = 50;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("AttendanceMonthlyReport", sLanguageCode, "COL_RequestNo");  // 15
            //sheet1.Column(header_column).Width = 35;
            header_column++;


            #endregion

            var range = sheet1.Range("A1:O1");
            range.Style.Fill.BackgroundColor = XLColor.Yellow;
            range.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            range.Style.Font.FontName = "Angsana New";
            range.Style.Font.FontSize = 14;
            range.Style.Border.SetOutsideBorder(XLBorderStyleValues.Thick).Border.SetInsideBorder(XLBorderStyleValues.Thick);

            #region Data Excel

            if (oDT.Rows.Count > 0)
            {
                int column;
                int row = 2;
                int last_column = 0;
                int last_row = 0;
                foreach (DataRow dRow in oDT.Rows)
                {
                    column = 1;

                    sheet1.Row(row).Cell(column).Value = dRow["EmployeeID"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["EmployeeName"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["OrgUnitFullName"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["OrgUnitShortName"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["WorkLocation"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["Province"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["AttendanceType"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["AttendanceName"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["BeginDate"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["EndDate"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["BeginTime"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["EndTime"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["PayrollDays"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["Remark"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["RequestNo"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    if (last_column == 0)
                        last_column = column;

                    row++;
                }

                last_row = row - 1;
                var rang_detail = sheet1.Range(2, 1, last_row, last_column);
                rang_detail.Style.Font.FontName = "Angsana New";
                rang_detail.Style.Font.FontSize = 14;
            }

            #endregion

            range.SetAutoFilter();
            range.Style.Alignment.SetShrinkToFit(true);
            sheet1.Columns().AdjustToContents();
            sheet1.SheetView.Freeze(1, 1);

            return workbook;
        }

        public DataTable GetTimeScanReport(DateTime BeginDate, DateTime EndDate, string EmpID, string OrgUnitList, string CompanyCode, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetTimeScanReport(BeginDate, EndDate, EmpID, OrgUnitList, CompanyCode, LanguageCode);
        }

        public XLWorkbook ExportExcelTimeScanReport(DataTable oDT, string sLanguageCode)
        {
            var workbook = new XLWorkbook();
            var sheet1 = workbook.Worksheets.Add("Sheet1");

            int header_row = 1;
            int header_column = 1;


            #region Header Excel
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("TimeScanReport", sLanguageCode, "COL_EmployeeID");  // 1
            //sheet1.Column(header_column).Width = 10;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("TimeScanReport", sLanguageCode, "COL_EmployeeName");  // 2
            //sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("TimeScanReport", sLanguageCode, "COL_OrgUnitFullName");  // 3
            //sheet1.Column(header_column).Width = 50;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("TimeScanReport", sLanguageCode, "COL_OrgUnitShortName");  // 4
            //sheet1.Column(header_column).Width = 50;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("TimeScanReport", sLanguageCode, "COL_WorkLocation");  // 5
            //sheet1.Column(header_column).Width = 50;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("TimeScanReport", sLanguageCode, "COL_Province");  // 6
            //sheet1.Column(header_column).Width = 50;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("TimeScanReport", sLanguageCode, "COL_WorkDate");  // 7
            //sheet1.Column(header_column).Width = 50;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("TimeScanReport", sLanguageCode, "COL_ClockInDate");  // 8
            //sheet1.Column(header_column).Width = 50;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("TimeScanReport", sLanguageCode, "COL_ClockInLocation");  // 9
            //sheet1.Column(header_column).Width = 50;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("TimeScanReport", sLanguageCode, "COL_ClockOutDate");  // 10
            //sheet1.Column(header_column).Width = 50;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("TimeScanReport", sLanguageCode, "COL_ClockOutLocation");  // 11
            //sheet1.Column(header_column).Width = 50;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("TimeScanReport", sLanguageCode, "COL_AbsenseAttendanceCode");  // 12
            //sheet1.Column(header_column).Width = 50;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = GetCommonText("TimeScanReport", sLanguageCode, "COL_Remarks");  // 13
            //sheet1.Column(header_column).Width = 50;
            header_column++;


            #endregion

            var range = sheet1.Range("A1:M1");
            range.Style.Fill.BackgroundColor = XLColor.Yellow;
            range.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            range.Style.Font.FontName = "Angsana New";
            range.Style.Font.FontSize = 14;
            range.Style.Border.SetOutsideBorder(XLBorderStyleValues.Thick).Border.SetInsideBorder(XLBorderStyleValues.Thick);

            #region Data Excel


            if (oDT.Rows.Count > 0)
            {
                int column;
                int row = 2;
                int last_column = 0;
                int last_row = 0;
                foreach (DataRow dRow in oDT.Rows)
                {
                    column = 1;

                    sheet1.Row(row).Cell(column).Value = dRow["EmployeeID"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["EmployeeName"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["OrgUnitFullName"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["OrgUnitShortName"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["WorkLocation"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["Province"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["WorkDate"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["ClockInDate"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["ClockInLocation"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["ClockOutDate"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["ClockOutLocation"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["AbsenseAttendanceCode"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    sheet1.Row(row).Cell(column).Value = dRow["Remarks"];
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    column++;

                    if (last_column == 0)
                        last_column = column;

                    row++;
                }

                last_row = row - 1;
                var rang_detail = sheet1.Range(2, 1, last_row, last_column);
                rang_detail.Style.Font.FontName = "Angsana New";
                rang_detail.Style.Font.FontSize = 14;
            }

            #endregion

            range.SetAutoFilter();
            range.Style.Alignment.SetShrinkToFit(true);
            sheet1.Columns().AdjustToContents();
            sheet1.SheetView.Freeze(1, 1);

            return workbook;
        }

        public DataTable GetWorkingSummaryReport(DateTime BeginDate, DateTime EndDate, string EmpID, string OrgUnitList, string CompanyCode, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetWorkingSummaryReport(BeginDate, EndDate, EmpID, OrgUnitList, CompanyCode, LanguageCode);
        }

        public XLWorkbook ExportExcelWorkingSummaryReport(DataTable oDT)
        {
            var workbook = new XLWorkbook();
            var sheet1 = workbook.Worksheets.Add("Sheet1");

            int header_row = 1;
            int header_column = 1;

            #region Header Excel
            //sheet1.Row(header_row).Cell(header_column).Value = "OTSender"; // 1
            //sheet1.Column(header_column).Width = 35;
            //header_column++;
            //sheet1.Row(header_row).Cell(header_column).Value = "OTRequestor"; // 2
            //sheet1.Column(header_column).Width = 35;
            //header_column++;
            //sheet1.Row(header_row).Cell(header_column).Value = "OrgUnitFullName"; // 3
            //sheet1.Column(header_column).Width = 50;
            //header_column++;


            #endregion

            var range = sheet1.Range("A1:C1");
            range.Style.Fill.BackgroundColor = XLColor.Yellow;
            range.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            range.Style.Font.FontName = "Angsana New";
            range.Style.Font.FontSize = 14;
            range.Style.Border.SetOutsideBorder(XLBorderStyleValues.Thick).Border.SetInsideBorder(XLBorderStyleValues.Thick);

            #region Data Excel


            if (oDT.Rows.Count > 0)
            {
                int column;
                int row = 2;
                int last_column = 0;
                int last_row = 0;
                foreach (DataRow dRow in oDT.Rows)
                {
                    column = 1;

                    //sheet1.Row(row).Cell(column).Value = dRow["OTSender"];
                    //sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    //column++;

                    //sheet1.Row(row).Cell(column).Value = dRow["OTRequestor"];
                    //sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    //column++;

                    //sheet1.Row(row).Cell(column).Value = dRow["OrgUnitFullName"];
                    //sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                    //column++;

                    if (last_column == 0)
                        last_column = column;

                    row++;
                }

                last_row = row - 1;
                var rang_detail = sheet1.Range(2, 1, last_row, last_column);
                rang_detail.Style.Font.FontName = "Angsana New";
                rang_detail.Style.Font.FontSize = 14;
            }

            #endregion

            return workbook;
        }

        public decimal GetMaxLevelMD()
        {
            return Convert.ToDecimal(MAXLEVELMD);
        }
    }
}