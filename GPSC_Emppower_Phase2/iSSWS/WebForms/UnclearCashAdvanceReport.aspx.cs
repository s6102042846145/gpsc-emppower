﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data.SqlClient;
using System.Data;
using ESS.HR.TR;
using ESS.HR.TR.DATACLASS;
using System.IO;

namespace iSSWS.WebForms
{
    /// <summary>
    /// รายงานลูกหนี้เงินยืมคงค้าง
    /// </summary>
    public partial class UnclearCashAdvanceReport : ReportPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ExportData();
                ClientScript.RegisterStartupScript(typeof(Page), "closePage", "window.close();", true);
            }
        }

        private DataTable GetData()
        {
            DataSet ds = new DataSet("HRTRDS");
            foreach (string companyCode in this.TravelReportParams.CompanyCode.Split(','))
            {
                if (ds.Tables.Count <= 0)
                    ds = HRTRManagement.CreateInstance(companyCode).GetUnclearCashAdvanceReport(this.TravelReportParams);
                else
                    ds.Tables[0].Rows.Add(HRTRManagement.CreateInstance(companyCode).GetUnclearCashAdvanceReport(this.TravelReportParams).Tables[0].Rows);
            }
            return ds.Tables[0];
        }
        public void ExportExcel()
        {
            try
            {
                DataTable dt = GetData();

                string attachment = "attachment; filename=UnclearCashAdvanceReport.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentEncoding = System.Text.Encoding.Unicode;
                Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());
                Response.ContentType = "application/vnd.ms-excel";
                string tab = "\t";

                Response.Write("\t\t\t\t\t\t\t\tรายงานลูกหนี้เงินยืมคงค้าง\n");
                Response.Write("\t\t\t\t\t\t\t\t" + dt.Rows[0]["CompanyName"] + "\n");
                Response.Write("\t\t\t\t\t\t\t\tรายงาน ณ วันที่ " + DateTime.Now.ToString("dd/MM/yyyy") + "\n");

                if (this.TravelReportParams.ClientCompanyID <= 0)
                    Response.Write("\t\tClient Company\t\t-\n");
                else
                    Response.Write("\t\tClient Company\t\t" + this.TravelReportParams.ClientCompanyName + "\n");

                if (string.IsNullOrEmpty(this.TravelReportParams.OrgUnitID))
                    Response.Write("\t\tรหัสหน่วยงาน \t\t-\n");
                else
                    Response.Write("\t\tรหัสหน่วยงาน \t\t" + this.TravelReportParams.OrgUnitID + "\n");

                if (string.IsNullOrEmpty(this.TravelReportParams.RequestorName))
                    Response.Write("\t\tรหัสพนักงาน \t\t-\t");
                else
                    Response.Write("\t\tรหัสพนักงาน \t\t" + this.TravelReportParams.RequestorName + "\t");

                if (string.IsNullOrEmpty(this.TravelReportParams.PositionID))
                    Response.Write("\t\t\t\tตำแหน่ง \t-\n");
                else
                    Response.Write("\t\t\t\tตำแหน่ง \t" + this.TravelReportParams.Position + "\n");
                
                if (string.IsNullOrEmpty(this.TravelReportParams.RequestNo))
                    Response.Write("\t\tเลขที่เอกสาร\t\t-\n");
                else
                    Response.Write("\t\tเลขที่เอกสาร\t\t" + this.TravelReportParams.RequestNo + "\n");

                if (DateTime.MinValue == this.TravelReportParams.ApproveBeginDate)
                    Response.Write("\t\tวันที่ยืม\t\t-\t\t\t\t\tถึง\t-\n");
                else
                    Response.Write("\t\tวันที่ยืม\t\t" + this.TravelReportParams.ApproveBeginDate + "\t\t\t\t\tถึง\t" + this.TravelReportParams.ApproveEndDate + "\n");

                if (this.TravelReportParams.OverDueFrom < 0)
                    Response.Write("\t\tจำนวนวันที่คงค้าง\t\t-\tถึง\t-\n");
                else
                    Response.Write("\t\tจำนวนวันที่คงค้าง\t\t" + this.TravelReportParams.OverDueFrom + " - " + this.TravelReportParams.OverDueTo + "\n");

                Response.Write("\n\n");
                //Gend Columns

                //Response.Write("No." + tab);
                Response.Write("ClientCompany" + tab + tab);
                Response.Write("เลขที่สัญญา" + tab);
                Response.Write("ชื่อพนักงาน" + tab + tab);
                Response.Write("เลขที่เอกสาร" + tab);
                Response.Write("วัตถุประสงฆ์" + tab);
                Response.Write(tab + "จำนวนเงินยืม" + tab + tab);
                Response.Write("วันที่ยืม" + tab);
                Response.Write("วันที่ครบกำหนดเงินยืม" + tab);
                Response.Write("จำนวนวันที่ครบกำหนด" + tab);
                Response.Write("สถานะ" + tab);
                Response.Write("\n");
                Response.Write(tab + tab + tab + tab + tab);
                Response.Write(tab + tab + "สกุลเงิน" + tab + "อัตราแลกเปลี่ยน" + tab + "บาท");
                Response.Write("\n");

                int i = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    i++;
                    Response.Write(dr["ContractClientCompanyAbbrv"] + tab);
                    Response.Write(dr["ContractClientCompanyName"] + tab);
                    if (dr["ContractNo"].ToString() != "")
                        Response.Write("'" + dr["ContractNo"] + tab);
                    else
                        Response.Write("" + tab);
                    Response.Write("'" + dr["EmployeeID"] + tab);
                    Response.Write(dr["EmployeeName"] + tab);
                    Response.Write(dr["RequestNo"] + tab);
                    Response.Write(dr["Remark"].ToString().Replace("\n", " ") + tab);
                    Response.Write(dr["CashAdvanceCurrency"] + tab);
                    Response.Write(dr["CashAdvanceRate"] + tab);
                    Response.Write(Convert.ToDecimal(dr["CashAdvanceAmount"]).ToString("#,###,##0.00") + tab);
                    Response.Write(dr["PostingDate"] + tab);
                    Response.Write(dr["Clearing_DueDate"] + tab);
                    Response.Write(Convert.ToDecimal(dr["OverdueDay"]).ToString("#,###,##0.00") + tab);
                    Response.Write(dr["Status"] + tab);
                    Response.Write("\n");
                }
                Response.End();
            }
            catch (Exception ex)
            {

            }
        }
        public void ExportData()
        {
            string fileRdlc = "";
            string tableName = "UnclearCashAdvanceReport";
            string filename = "UnclearCashAdvanceReport";

            if (this.TravelReportParams.RequestorCompanyCode == "0030")
                fileRdlc = "UnclearCashAdvanceReportForBSA";
            else
                fileRdlc = "UnclearCashAdvancePTTReport";

            HRTRManagement oHRTR = HRTRManagement.CreateInstance(this.TravelReportParams.RequestorCompanyCode);
            string rootPath = oHRTR.FILE_ROOTPATH;
            byte[] ar;
            ar = HRTRManagement.CreateInstance(this.TravelReportParams.RequestorCompanyCode).saveFileReport(fileRdlc, tableName, rootPath, this.TravelReportParams);

            MemoryStream stream = new MemoryStream();
            stream.Write(ar, 0, ar.Length);

            if(this.TravelReportParams.ExportType == "EXCEL")
                Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".xls");
            else if (this.TravelReportParams.ExportType == "PDF")
                Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".pdf");
            Response.ContentType = "application/octectstream";
            Response.BinaryWrite(ar);
            Response.End();
        }
    }
}