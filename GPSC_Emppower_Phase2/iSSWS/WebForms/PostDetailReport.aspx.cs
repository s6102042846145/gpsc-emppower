﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data.SqlClient;
using System.Data;
using ESS.HR.TR;
using ESS.HR.TR.DATACLASS;
using System.IO;
using System.Drawing;
using System.Text;

namespace iSSWS.WebForms
{
    /// <summary>
    /// รายงานรายละเอียดการบันทึกบัญชี
    /// </summary>
    public partial class PostDetailReport : ReportPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {                
                ExportData();
                ClientScript.RegisterStartupScript(typeof(Page), "closePage", "window.close();", true);
            }
        }

        private DataTable GetData()
        {
            DataSet ds = new DataSet("HRTRDS");
            foreach (string companyCode in this.TravelReportParams.CompanyCode.Split(','))
            {
                if (ds.Tables.Count <= 0)
                    ds = HRTRManagement.CreateInstance(companyCode).GetPostDetailReport(this.TravelReportParams);
                else
                    ds.Tables[0].Rows.Add(HRTRManagement.CreateInstance(companyCode).GetPostDetailReport(this.TravelReportParams).Tables[0].Rows);
            }
            return ds.Tables[0];
        }
        public void ExportExcel()
        {
            try
            {
                DataTable dt = GetData();
                                
                string attachment = "attachment; filename=PostDetailReport.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentEncoding = System.Text.Encoding.Unicode;
                Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());
                Response.ContentType = "application/vnd.ms-excel";
                string tab = "\t";
                
                Response.Write("\t\t\t\t\t\t\t\tรายงานรายละเอียดการบันทึกบัญชี\n");
                Response.Write("\t\t\t\t\t\t\t\t" + dt.Rows[0]["CompanyName"]+ "\n");
                Response.Write("\t\t\t\t\t\t\t\tรายงาน ณ วันที่ " + DateTime.Now.ToString("dd/MM/yyyy") + "\n");

                if (this.TravelReportParams.RequestorCompanyCode == "0030")
                {
                    if (this.TravelReportParams.ClientCompanyID <= 0)
                        Response.Write("\t\tClient Company\t\t-\n");
                    else
                        Response.Write("\t\tClient Company\t\t" + this.TravelReportParams.ClientCompanyName + "\n");
                }
                if (string.IsNullOrEmpty(this.TravelReportParams.OrgUnitID))
                    Response.Write("\t\tรหัสหน่วยงาน \t\t-\t");
                else
                    Response.Write("\t\tรหัสหน่วยงาน \t\t" + this.TravelReportParams.OrgUnitID + "\t");

                if (DateTime.MinValue == this.TravelReportParams.CreatedBeginDate)
                    Response.Write("\t\t\t\t\t\t\t\tวันที่เดินทาง\t\t-\t\tถึง\t-\n");
                else
                    Response.Write("\t\t\t\t\t\t\t\tวันที่เดินทาง\t\t" + this.TravelReportParams.CreatedBeginDate + "\t\tถึง\t" + this.TravelReportParams.CreatedEndDate + "\n");

                if (string.IsNullOrEmpty(this.TravelReportParams.RequestorName))
                    Response.Write("\t\tรหัสพนักงาน \t\t-\t");
                else
                    Response.Write("\t\tรหัสพนักงาน \t\t'" + this.TravelReportParams.RequestorName + "\t");

                if (DateTime.MinValue == this.TravelReportParams.ReportBeginDate)
                    Response.Write("\t\t\t\t\t\t\t\tวันที่รายงาน\t\t-\t\tถึง\t-\n");
                else
                    Response.Write("\t\t\t\t\t\t\t\tวันที่รายงาน\t\t" + this.TravelReportParams.ReportBeginDate + "\t\tถึง\t" + this.TravelReportParams.ReportEndDate + "\n");

                if (string.IsNullOrEmpty(this.TravelReportParams.PositionID))
                    Response.Write("\t\tตำแหน่ง \t\t-\t");
                else
                    Response.Write("\t\tตำแหน่ง \t\t'" + this.TravelReportParams.Position + "\t");

                if (DateTime.MinValue == this.TravelReportParams.PostingBeginDate)
                    Response.Write("\t\t\t\t\t\t\t\tวันที่เอกสาร (Posting Date)\t\t-\t\tถึง\t-\n");
                else
                    Response.Write("\t\t\t\t\t\t\t\tวันที่เอกสาร (Posting Date)\t\t" + this.TravelReportParams.PostingBeginDate + "\t\tถึง\t" + this.TravelReportParams.PostingEndDate + "\n");

                if(string.IsNullOrEmpty(this.TravelReportParams.CostCenterCodeBegin))
                    Response.Write("\t\tรหัสศูนย์ต้นทุน \t\t-\t");
                else
                    Response.Write("\t\tรหัสศูนย์ต้นทุน \t\t" + this.TravelReportParams.CostCenterCodeBegin + "\t");

                if (string.IsNullOrEmpty(this.TravelReportParams.IOID))
                    Response.Write("\t\tรหัสงบประมาณ\t\t-\t\t\t");
                else
                    Response.Write("\t\tรหัสงบประมาณ\t\t" + this.TravelReportParams.IOID + "\t\t\t");

                if (string.IsNullOrEmpty(this.TravelReportParams.RequestNo))
                    Response.Write("\tเลขที่เอกสาร\t\t-\t\tถึง\t-\n");
                else
                    Response.Write("\tเลขที่เอกสาร\t\t" + this.TravelReportParams.RequestNo+ "\n");
                
                if (string.IsNullOrEmpty(this.TravelReportParams.GLAccount))
                    Response.Write("\t\tGL Account\t\t-");
                else
                    Response.Write("\t\tGL Account\t\t'" + this.TravelReportParams.GLAccount);

                if (string.IsNullOrEmpty(this.TravelReportParams.VendorCode))
                    Response.Write("\t\t\t\t\t\t\t\t\tรหัสร้านค้า/บริษัท\t\t-" + "\n");
                else
                    Response.Write("\t\t\t\t\t\t\t\t\tรหัสร้านค้า/บริษัท\t\t" + this.TravelReportParams.VendorName + "\n");

                Response.Write("\n\n");
                //Gend Columns

                //Response.Write("No." + tab);
                if (this.TravelReportParams.RequestorCompanyCode == "0030")
                {
                    //BSA
                    Response.Write("GL" + tab);
                    Response.Write("ClientCompany" + tab + tab);
                    Response.Write("Contract No" + tab);
                    Response.Write("Employee" + tab + tab);
                    Response.Write("Vendor" + tab + tab);
                    Response.Write("CCTR" + tab);
                    Response.Write("IO" + tab);
                    Response.Write("เลขที่เอกสาร (WEB)" + tab);
                    Response.Write("วัตถุประสงค์" + tab);
                    Response.Write("สกุลเงิน" + tab);
                    Response.Write("อัตราแลกเปลี่ยน" + tab);
                    Response.Write("Vat Code" + tab);
                    Response.Write("Vat Amount" + tab);
                    Response.Write("จำนวนเงินรวม" + tab);
                    Response.Write("วันที่ Posting Date" + tab);
                    Response.Write("วันที่รายงาน" + tab);
                    Response.Write("สถานะ" + tab);
                }
                else
                {
                    Response.Write("GL" + tab);
                    Response.Write("ชื่อพนักงาน" + tab + tab);
                    Response.Write("เลขที่เอกสารรายงาน" + tab);
                    Response.Write("CCTR" + tab);
                    Response.Write("IO" + tab);
                    Response.Write("วัตถุประสงค์" + tab);
                    Response.Write("สกุลเงิน" + tab);
                    Response.Write("อัตราแลกเปลี่ยน" + tab);
                    Response.Write("Vat Code" + tab);
                    Response.Write("จำนวนเงินรวม" + tab);
                    Response.Write("วันที่ Posting Date" + tab);
                    Response.Write("วันที่รายงาน" + tab);
                    Response.Write("สถานะ" + tab);
                }
                Response.Write("\n");

                int i = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    i++;
                    if (this.TravelReportParams.RequestorCompanyCode == "0030")
                    {
                        //BSA
                        if (dr["GL"].ToString() != "")
                            Response.Write("'" + dr["GL"].ToString().Replace("<br/>", ",") + tab);
                        else
                            Response.Write("" + tab);
                        Response.Write(dr["ContractClientCompanyAbbrv"] + tab);
                        Response.Write(dr["ContractClientCompanyName"] + tab);
                        if (dr["ContractNo"].ToString() != "")
                            Response.Write("'" + dr["ContractNo"] + tab);
                        else
                            Response.Write("" + tab);
                        Response.Write("'" + dr["EmployeeID"] + tab);
                        Response.Write(dr["EmployeeName"] + tab);
                        Response.Write("'" + dr["VendorCode"] + tab);
                        Response.Write(dr["VenderName"] + tab);
                        Response.Write("'" + dr["CCTR"] + tab);
                        Response.Write("'" + dr["IO"] + tab);
                        Response.Write(dr["RequestNo"] + tab);
                        Response.Write(dr["TravelType"].ToString().Replace("\n", " ") + tab);
                        Response.Write(dr["Currency"] + tab);
                        Response.Write(dr["ExchangeRate"] + tab);
                        Response.Write(dr["VatCode"] + tab);
                        Response.Write(dr["VatAmount"] + tab);
                        Response.Write(String.Format("{0:#,###,##0.00}", Convert.ToDecimal(dr["TransactionAmount"])) + tab);
                        Response.Write(dr["PostingDate"] + tab);
                        Response.Write(dr["DocumentDate"] + tab);
                        Response.Write(dr["Status"] + tab);
                    }
                    else
                    {
                        //Other PTT, ENCO, TANK, ES, LNG
                        if (dr["GL"].ToString() != "")
                            Response.Write("'" + dr["GL"].ToString().Replace("<br/>", ",") + tab);
                        else
                            Response.Write("" + tab);

                        Response.Write("'" + dr["EmployeeID"] + tab);
                        Response.Write(dr["EmployeeName"] + tab);
                        Response.Write(dr["RequestNo"] + tab);
                        Response.Write("'" + dr["CCTR"] + tab);
                        Response.Write("'" + dr["IO"] + tab);
                        Response.Write(dr["TravelType"].ToString().Replace("\n", " ") + tab);
                        Response.Write(dr["Currency"] + tab);
                        Response.Write(dr["ExchangeRate"] + tab);
                        Response.Write(dr["VatCode"] + tab);
                        Response.Write(String.Format("{0:#,###,##0.00}", Convert.ToDecimal(dr["TransactionAmount"])) + tab);
                        Response.Write(dr["PostingDate"] + tab);
                        Response.Write(dr["DocumentDate"] + tab);
                        Response.Write(dr["Status"] + tab);
                    }
                    Response.Write("\n");
                }
                Response.End();
            }
            catch(Exception ex)
            {

            }
        }
        public void ExportData()
        {
            string fileRdlc = "";
            string tableName = "PostDetailReport";
            string filename = "PostDetailReport";

            if (this.TravelReportParams.RequestorCompanyCode == "0030")
                fileRdlc = "PostDetailReport";
            else
                fileRdlc = "PostDetailPTTReport";

            HRTRManagement oHRTR = HRTRManagement.CreateInstance(this.TravelReportParams.RequestorCompanyCode);
            string rootPath = oHRTR.FILE_ROOTPATH;
            byte[] ar;
            ar = HRTRManagement.CreateInstance(this.TravelReportParams.RequestorCompanyCode).saveFileReport(fileRdlc, tableName, rootPath, this.TravelReportParams);

            MemoryStream stream = new MemoryStream();
            stream.Write(ar, 0, ar.Length);

            if(this.TravelReportParams.ExportType == "EXCEL")
                Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".xls");
            else if(this.TravelReportParams.ExportType == "PDF")
                Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".pdf");
            Response.ContentType = "application/octectstream";
            Response.BinaryWrite(ar);
            Response.End();

            #region RDLC
            //ReportViewer1.ProcessingMode = ProcessingMode.Local;
            //ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Views/Report/HR/TR/PostDetailReport.rdlc");
            //ReportViewer1.LocalReport.SetParameters(new ReportParameter("p_RequestorName", this.TravelReportParams.RequestorName));
            //ReportViewer1.LocalReport.SetParameters(new ReportParameter("p_EmployeeID", this.TravelReportParams.RequestorID));
            //ReportViewer1.LocalReport.SetParameters(new ReportParameter("p_OrgUnit", this.TravelReportParams.OrgUnitID));
            //ReportViewer1.LocalReport.SetParameters(new ReportParameter("p_CCCode", this.TravelReportParams.CostCenterCodeBegin));
            //ReportViewer1.LocalReport.SetParameters(new ReportParameter("p_IOCode", this.TravelReportParams.IOID));
            //ReportViewer1.LocalReport.SetParameters(new ReportParameter("p_ProjectCode", this.TravelReportParams.ProjectCode));
            //ReportViewer1.LocalReport.SetParameters(new ReportParameter("p_VendorCode", this.TravelReportParams.VendorCode));
            //ReportViewer1.LocalReport.SetParameters(new ReportParameter("p_RequestNo", this.TravelReportParams.RequestNo));
            //ReportViewer1.LocalReport.SetParameters(new ReportParameter("p_GLAccountFrom", this.TravelReportParams.GLAccountFrom));
            //ReportViewer1.LocalReport.SetParameters(new ReportParameter("p_GLAccountTo", this.TravelReportParams.GLAccountTo));

            //if (this.TravelReportParams.CreatedBeginDate != default(DateTime))
            //{
            //    ReportViewer1.LocalReport.SetParameters(new ReportParameter("p_CreatedBeginDate", this.TravelReportParams.CreatedBeginDate.ToString("dd/MM/yyyy")));
            //}

            //if (this.TravelReportParams.CreatedEndDate != default(DateTime))
            //{
            //    ReportViewer1.LocalReport.SetParameters(new ReportParameter("p_CreatedEndDate", this.TravelReportParams.CreatedEndDate.ToString("dd/MM/yyyy")));
            //}

            //if (this.TravelReportParams.PostingBeginDate != default(DateTime))
            //{
            //    ReportViewer1.LocalReport.SetParameters(new ReportParameter("p_PostingBeginDate", this.TravelReportParams.PostingBeginDate.ToString("dd/MM/yyyy")));
            //}

            //if (this.TravelReportParams.PostingEndDate != default(DateTime))
            //{
            //    ReportViewer1.LocalReport.SetParameters(new ReportParameter("p_PostingEndDate", this.TravelReportParams.PostingEndDate.ToString("dd/MM/yyyy")));
            //}

            //ReportViewer1.LocalReport.SetParameters(new ReportParameter("p_BackgroundTableHeader", "Lavender"));
            //ReportViewer1.LocalReport.SetParameters(new ReportParameter("p_BorderTable", "Silver"));

            //DataTable dt = GetData();
            //dt.TableName = "PostDetailReport";
            //ReportDataSource datasource = new ReportDataSource("PostDetailReport", dt);
            //ReportViewer1.LocalReport.DataSources.Clear();
            //ReportViewer1.LocalReport.DataSources.Add(datasource);
            //ReportViewer1.DataBind();          
            #endregion RDLC
        }
    }
}