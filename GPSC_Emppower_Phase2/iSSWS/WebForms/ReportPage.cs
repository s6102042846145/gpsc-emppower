﻿using ESS.HR.TR.DATACLASS;

namespace iSSWS.WebForms
{
    public class ReportPage : System.Web.UI.Page
    {
        protected TravelReportTransfer TravelReportParams
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["TravelReportParams"] == null)
                {
                    return new TravelReportTransfer(); // for report test
                } 

                return System.Web.HttpContext.Current.Session["TravelReportParams"] as TravelReportTransfer;
            }
        }

        protected ESS.HR.TR.HRTRManagement HRTRService
        {
            get
            {
                if (string.IsNullOrEmpty(this.TravelReportParams.RequestorCompanyCode)) {
                    throw new System.Exception("Invalid requestor company code");
                }
            
                return ESS.HR.TR.HRTRManagement.CreateInstance(this.TravelReportParams.RequestorCompanyCode);               
            }
        }
    }
}