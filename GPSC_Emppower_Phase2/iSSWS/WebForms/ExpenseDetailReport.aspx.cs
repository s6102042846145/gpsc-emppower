﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data.SqlClient;
using System.Data;
using ESS.HR.TR;
using ESS.HR.TR.DATACLASS;
using System.Text.RegularExpressions;
using System.IO;

namespace iSSWS.WebForms
{
    public partial class ExpenseDetailReport : ReportPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                DataTable dt = GetData();
                if(dt.Rows.Count > 0)                
                    ExportData();
                else
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Data not found.')", true);
                ClientScript.RegisterStartupScript(typeof(Page), "closePage", "window.close();", true);
            }
        }
        private DataTable GetData()
        {
            DataSet ds = new DataSet("HRTRDS");
            foreach (string companyCode in this.TravelReportParams.CompanyCode.Split(','))
            {
                if (ds.Tables.Count <= 0)
                    ds = HRTRManagement.CreateInstance(companyCode).GetExpenseDetailReport(this.TravelReportParams);
                else
                    ds.Tables[0].Rows.Add(HRTRManagement.CreateInstance(companyCode).GetExpenseDetailReport(this.TravelReportParams).Tables[0].Rows);
            }
            return ds.Tables[0];
        }
        public void ExportData()
        {
            string fileRdlc = "";
            string tableName = "ExpenseDetailReport";
            string filename = "ExpenseDetailReport";

            if (this.TravelReportParams.RequestorCompanyCode == "0030")
                fileRdlc = "ExpenseDetailReport";
            else
                fileRdlc = "ExpenseDetailPTTReport";

            HRTRManagement oHRTR = HRTRManagement.CreateInstance(this.TravelReportParams.RequestorCompanyCode);
            string rootPath = oHRTR.FILE_ROOTPATH;
            byte[] ar;
            ar = HRTRManagement.CreateInstance(this.TravelReportParams.RequestorCompanyCode).saveFileReport(fileRdlc, tableName, rootPath, this.TravelReportParams);

            MemoryStream stream = new MemoryStream();
            stream.Write(ar, 0, ar.Length);

            if(this.TravelReportParams.ExportType == "EXCEL")
                Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".xls");
            else if (this.TravelReportParams.ExportType == "PDF")
                Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".pdf");
            Response.ContentType = "application/octectstream";
            Response.BinaryWrite(ar);
            Response.End();
        }
    }
}