﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iSSWS.Models
{
    public class GeneralExpenseKeyword
    {
        EmployeeDataForMobile EmployeeData { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Status { get; set; }
        public string CostCenter { get; set; }
        public string BudgetCenter { get; set; }
    }
}