using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Reflection;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using ESS.EMPLOYEE;
using ESS.DATA;
using ESS.WORKFLOW.ABSTRACT;
using ESS.FILE;
using ESS.MAIL;
using ESS.WORKFLOW;
using ESS.DATA.FILE;
using ESS.DATA.INTERFACE;
using Newtonsoft.Json;


namespace iSSWS.Models
{

    public class RequestDocumentForMobile
    {
     

        public RequestDocumentForMobile()
        {
        }

        #region " General Detail "
        public string Viewer { get; set; }
        public string Editor { get; set; }

        public int RequestTypeID { get; set; }

        public string RequestTypeSummary { get; set; }

        public bool IsFinish { get; set; }

        
        public string Status { get; set; }
        public string LanguageCode { get; set; }

        public int RequestID { get; set; }
        public string RequestNo { get; set; }
        public string KeyMaster { get; set; }

        public string RequestorNo { get; set; }
        public string RequestorPosition { get; set; }
        public string RequestorCompanyCode { get; set; }
        public string RequestorName { get; set; }
        public string RequestorPositionName { get; set; }

        public string CreatorNo { get; set; }
        public string CreatorPosition { get; set; }
        public string CreatorCompanyCode { get; set; }
        public string CreatorName { get; set; }
        public string CreatorPositionName { get; set; }


        public bool IsOwnerView { get; set; }
        public bool IsDataOwnerView { get; set; }

        public DateTime CreatedDate { get; set; }
        public DateTime SubmittedDate { get; set; }
        public DateTime DocumentDate { get; set; }

        public int FileSetID { get; set; }
        public FileAttachmentSetForMobile FileSet { get; set; }
        public bool HasFileAttached { get; set; }

        public int FlowID { get; set; }
        public int CurrentFlowItemID { get; set; }

        public string CurrentFlowItemCode { get; set; }
        public int CurrentItemID { get; set; }

        public int AuthorizeStep { get; set; }

        public DateTime CurrentActionByDate { get; set; }
        public string CurrentActionByName { get; set; }
        public string CurrentActionByPosition { get; set; }
        #endregion

        #region " Data "


        public bool RequireComment { get; set; }
        public DataTable Data { get; set; }
        public Object Additional { get; set; }

        public string AdditionalJson { get; set; }

        public string Comment { get; set; }

        public string Comment2 { get; set; }
        #endregion

        public List<RequestActionForMobile> RequestActionList { get; set; }

        public List<RequestCommentForMobile> RequestCommentList { get; set; }

        public void ParseToObject(RequestDocument oDoc, EmployeeDataForMobile oInfotype0001)
        {
            this.RequestNo = oDoc.RequestNo;
            this.RequestID = oDoc.RequestID;
            this.FlowID = oDoc.FlowID;
            this.IsFinish = oDoc.IsCompleted || oDoc.IsCancelled;
            this.IsCompleted = oDoc.IsCompleted;
            this.IsCancelled = oDoc.IsCancelled;
            this.CurrentFlowItemID = oDoc.CurrentFlowItemID;
            this.CurrentItemID = oDoc.CurrentItemID;
            this.LanguageCode = oInfotype0001.Language;
            this.CreatedDate = oDoc.CreatedDate;
            this.SubmittedDate = oDoc.SubmittedDate;
            this.Viewer = oDoc.Viewer;
            this.Editor = oDoc.Editor;
            this.KeyMaster = oDoc.KeyMaster;
            this.IsOwnerView = oDoc.IsOwnerView;
            this.IsDataOwnerView = oDoc.IsDataOwnerView;

            if (oDoc.CurrentFlowItem != null)
                this.CurrentFlowItemCode = oDoc.CurrentFlowItem.Code;
            //Creator
            this.CreatorNo = oDoc.CreatorNo;
            this.CreatorPosition = oDoc.CreatorPosition;
            this.CreatorCompanyCode = oDoc.CreatorCompanyCode;
            this.CreatorName = oDoc.Creator.AlternativeName(oInfotype0001.Language);
            this.CreatorPositionName = oDoc.Creator.ActionOfPosition.AlternativeName(oInfotype0001.Language);
            //Requestor
            this.RequestorNo = oDoc.RequestorNo;
            this.RequestorPosition = oDoc.RequestorPosition;
            this.RequestorCompanyCode = oDoc.RequestorCompanyCode;
            this.RequestorName = oDoc.Requestor.AlternativeName(oInfotype0001.Language);
            this.RequestorPositionName = oDoc.Requestor.ActionOfPosition.AlternativeName(oInfotype0001.Language);
            this.RequestTypeID = oDoc.RequestTypeID;
            this.ReferRequestNo = oDoc.ReferRequestNo;

            //File Attachment
            this.FileSetID = oDoc.FileSetID;
            if (oDoc.FileSetID > -1)
            {
                FileAttachmentSet oFileSet = FileAttachmentSystemManagement.CreateInstance(RequestorCompanyCode).LoadFileSet(this.FileSetID);
                this.FileSet = new FileAttachmentSetForMobile();
                this.FileSet.FileSetID = this.FileSetID;
                this.FileSet.RequestID = this.RequestID;
                this.FileSet.Attachments = new List<FileAttachmentForMobile>();
                for (int i = 0; i < oFileSet.Count;i++ )
                {
                    FileAttachment att = oFileSet[i];
                        FileAttachmentForMobile file = new FileAttachmentForMobile();
                        file.FileID = att.FileID;
                        file.FileName = att.FileName;
                        file.FileSetID = this.FileSetID;
                        file.FilePath = att.FilePath;
                        file.FileType = att.FileType;
                        this.FileSet.Attachments.Add(file);
                    }
            }
            else
            {
                this.FileSet = new FileAttachmentSetForMobile();

            }

            if (FileSet.Attachments == null)
                FileSet.Attachments = new List<FileAttachmentForMobile>();
            HasFileAttached = FileSet.Attachments.Count > 0;

            this.Data = oDoc.Data;
            this.Additional = oDoc.Additional;
            this.AdditionalJson = JsonConvert.SerializeObject(oDoc.Additional);
          
            this.HistoryList = oDoc.HistoryList;

            //Get flow in pass
            this.RequestCommentList = new List<RequestCommentForMobile>();
            List<RequestFlow> list = oDoc.GetPassedFlow();
            RequestCommentForMobile comment;
            foreach (RequestFlow RF in list.FindAll(obj => !string.IsNullOrEmpty(obj.Comment.Trim())))
            {
                comment = new RequestCommentForMobile();
                comment.CommentbyEmployeeID = RF.ActionByCode;
                comment.CommentbyEmployeeName = RF.ActionBy;
                comment.CommentDate = RF.ActionDate;
                comment.CommentText = RF.Comment;
                this.RequestCommentList.Add(comment);
            }

            this.RequestActionList = new List<RequestActionForMobile>();
            RequestActionForMobile action;
            DataTable FlowItemActionPreference = WorkflowManagement.CreateInstance(this.RequestorCompanyCode).GetFlowItemActionPreference();
            if (oDoc.CurrentFlowItem != null)
            {
                //foreach (IActionData iact in oDoc.GetPossibleActionsForMobile())
                foreach (FlowItemAction iact in oDoc.GetPossibleActionsForMobile())
                {
                    action = new RequestActionForMobile();
                    action.IsVisible = iact.IsVisible;
                    action.ActionText = iact.Code;
                    action.ColorClass = "";
                    action.IconClass = "icon-man_tie";
                    foreach (DataRow Preference in FlowItemActionPreference.Select("FlowItemActionCode = '" + iact.Code + "'"))
                    {
                        action.ColorClass = Preference["ColorClass"].ToString();
                        action.IconClass = Preference["IconClass"].ToString();
                    }
                    action.Code = iact.Code;

                    //if (iact.Code == "EDIT")
                    //{
                    //    action.isInternal = true;
                    //    action.URL = string.Format("frmEditRequest/{0}/{1}/{2}/{3}/", oDoc.RequestNo, oDoc.KeyMaster, oDoc.IsOwnerView, oDoc.IsDataOwnerView);
                    //}
                    //else
                    action.ActionText = RequestText.CreateInstance(this.RequestorCompanyCode).LoadText("ACTION", oInfotype0001.Language, iact.Code);
                    action.URL = "Workflow/TakeActionFromViewRequest/" + iact.Code;
                    this.RequestActionList.Add(action);
                }
            }
        }

        public EmployeeDataForMobile Requestor { get; set; }
        public EmployeeDataForMobile ActionBy { get; set; }


        /// <summary>
        /// Redirect to specific internal page after take action success
        /// </summary>
        public string RedirectURL { get; set; }

        /// <summary>
        /// HasError will be set to 'true', If have something went wrong in action process with exception text in 'ErrorText'
        /// </summary>
        public bool HasError { get; set; }
        public string ErrorText { get; set; }

        /// <summary>
        /// Show flow for mobile version
        /// </summary>
        public List<RequestFlow> RequestFlowList { get; set; }

        public bool NewRequest { get; set; }

        public string BoxDescription { get; set; }

        public string ReferRequestNo { get; set; }

        public string PopupTitle { get; set; }

        public string PopupMessage { get; set; }

        public bool IsCompleted { get; set; }

        public bool IsCancelled { get; set; }

        public DataTable HistoryList { get; set; }
    }

}
