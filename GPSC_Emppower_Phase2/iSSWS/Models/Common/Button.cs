﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iSSWS.Models.Common
{
    public class Button
    {
        public Button()
        {

        }
        public string Title { get; set; }
        public string Icon { get; set; }
        public string Show { get; set; }
        public string Link { get; set; }
    }
}