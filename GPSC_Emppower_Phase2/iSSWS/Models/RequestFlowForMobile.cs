using System;
using System.Collections.Generic;
using System.Text;

namespace iSSWS.Models
{
    public class RequestFlowForMobile
    {
        public string ActionBy { get; set; }
        public string ActionCode { get; set; }
        public string Comment { get; set; }
        public bool IsCurrentItem { get; set; }
        public bool IsEndPoint { get; set; }
        public string Detail { get; set; }
        public string GroupName { get; set; }
        public string ActionByPosition { get; set; }

    }
}
