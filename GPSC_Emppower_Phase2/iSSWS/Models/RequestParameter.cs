﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iSSWS.Models
{
    public class RequestParameter
    {
        public RequestParameter()
        { 
        }
        public Dictionary<string, object> InputParameter { get; set; }
        public EmployeeDataForMobile CurrentEmployee { get; set; }
        public EmployeeDataForMobile Requestor { get; set; }
        public EmployeeDataForMobile Creator { get; set; }
        public RequestDocumentForMobile RequestDocument { get; set; }
    }
}