﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iSSWS.Models
{
    public class DayTemplate
    {
        public DateTime Day_Date { get; set; }
        public string Day_Code { get; set; }

        public bool IsDayOff { get; set; }

        public bool IsHoliday { get; set; }   
    }
}
