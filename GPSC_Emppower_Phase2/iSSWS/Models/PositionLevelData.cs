﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iSSWS.Models
{
    public class PositionLevelData
    {
        public PositionLevelData()
        {

        }
        public string ObjectID { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public string EmpGroup { get; set; }
        public string EmpSubGroup { get; set; }
    }
}