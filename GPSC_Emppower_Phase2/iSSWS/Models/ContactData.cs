﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iSSWS.Models
{
    public class ContactData
    {
        public string EmployeeID { get; set; }
        public string Name { get; set; }
        public string CategoryCode { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public string DataText { get; set; }
    }
}