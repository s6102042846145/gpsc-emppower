using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ESS.WORKFLOW;

/// <summary>
/// Summary description for RequestSelectedEventArgs
/// </summary>
public class RequestSelectedEventArgs:EventArgs
{
    private RequestDocument __document;
    public RequestSelectedEventArgs()
    {
    }
    public RequestSelectedEventArgs(RequestDocument Document)
    {
        __document = Document;
    }
    public RequestDocument Request
    {
        get
        {
            return __document;
        }
    }
}
public delegate void RequestSelectedEventHandler(object sender, RequestSelectedEventArgs e);

