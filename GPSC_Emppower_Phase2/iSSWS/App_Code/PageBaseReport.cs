using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ESS.EMPLOYEE;
using System.Collections.Generic;
using ESS.WORKFLOW;
using ESS.EMPLOYEE.CONFIG;


/// <summary>
/// Summary description for PageBaseReport
/// </summary>
public class PageBaseReport : System.Web.UI.Page
{
    public PageBaseReport()
    {
       
    }
    
    private EmployeeData __requestor;
    private EmployeeData __creator;

    public virtual EmployeeData Requestor
    {
        get
        {
            return __requestor;
        }
    }

    protected EmployeeData Creator
    {
        get
        {
            return __creator;
        }
    }

    public void SetUserData(EmployeeData RequestorData, EmployeeData CreatorData)
    {
        this.__requestor = RequestorData;
        this.__creator = CreatorData;
    }

    protected virtual string TextCategory
    {
        get
        {
            return "";
        }
    }

}
