﻿using System;
using System.Collections.Generic;
using System.Linq;

public static class Convert<T>
{

    /// <summary>
    /// แปลงข้อมูล DataRow ให้เป็น Object ใดๆ
    /// </summary>
    /// <param name="row">DataRow ที่ต้องการแปลงเป็น Object</param>
    /// <returns></returns>
    private static T ConvertFrom(System.Data.DataRow row)
    {
        string strMethod = System.Reflection.MethodInfo.GetCurrentMethod().Name;
        T obj = default(T);
        if (row == null)
        {
            return obj;
        }
        obj = Activator.CreateInstance<T>();
        System.Reflection.PropertyInfo[] props = obj.GetType().GetProperties();

        foreach (System.Data.DataColumn column in row.Table.Columns)
        {
            System.Reflection.PropertyInfo prop = getProperty(props, column.ColumnName);
            try
            {
                if (prop != null && prop.CanWrite)
                {
                    object value = row[column.ColumnName];
                    prop.SetValue(obj, ((value.Equals(System.DBNull.Value)) ? null : value), null);
                    //logger.Debug(strMethod + "-SetValue_To[" + prop.Name + "]From[" + column.ColumnName + "]");
                }
            }
            catch
            {
                throw;
            }
        }
        return obj;
    }

    /// <summary>
    /// แปลงกลุ่มข้อมูล DataRow[] ให้เป็น List ของ Object ใดๆ
    /// </summary>
    /// <param name="rows">กลุ่มข้อมูล DataRow[] ที่ต้องการแปลงเป็น List ของ Object</param>
    /// <returns></returns>
    private static IList<T> ConvertFrom(System.Data.DataRow[] rows)
    {
        string strMethod = System.Reflection.MethodInfo.GetCurrentMethod().Name;
        if (rows == null)
        {
            return null;
        }
        IList<T> list = new List<T>();
        foreach (System.Data.DataRow row in rows) list.Add(ConvertFrom(row));
        return list;
    }

    /// <summary>
    /// แปลงข้อมูล DataTable ให้เป็น List ของ Object ใดๆ
    /// </summary>
    /// <param name="dataTable">DataTable ที่ต้องการแปลงเป็น Object</param>
    /// <returns></returns>
    public static IList<T> ConvertFrom(System.Data.DataTable dataTable)
    {
        if (dataTable == null)
        {
            return null;
        }
        return ConvertFrom(dataTable.Select());
    }

    /// <summary>
    /// แปลง List ของ Object ใดๆ ให้เป็น List ของ Object ใดๆ 
    /// </summary>
    /// <param name="lsObj">List ของ Object ที่ต้องการแปลงเป็น List ของ Object T</param>
    /// <returns></returns>
    public static IList<T> ObjectFrom<TT>(IList<TT> lsObj)
    {
        string strMethod = System.Reflection.MethodInfo.GetCurrentMethod().Name;
        if (lsObj == null)
        {
            return null;
        }
        return ObjectFrom(lsObj.ToArray());
    }

    /// <summary>
    /// แปลงกลุ่มข้อมูล Object[]ใดๆ ให้เป็น List ของ Object ใดๆ 
    /// โดย Object ทั้งต้นทางและปลายทาง ต้องมี Property ที่เหมือนกัน(บางส่วน)
    /// </summary>
    /// <param name="objs">กลุ่มข้อมูล Object[] ที่ต้องการแปลงเป็น List ของ Object</param>
    /// <returns></returns>
    public static IList<T> ObjectFrom<TT>(TT[] objs)
    {
        string strMethod = System.Reflection.MethodInfo.GetCurrentMethod().Name;
        if (objs == null)
        {
            return null;
        }
        IList<T> lsT = new List<T>();
        foreach (TT obj in objs) lsT.Add(ObjectFrom(obj));
        return lsT;
    }

    /// <summary>
    /// แปลง Object ให้เป็นอีก Object หนึ่ง
    /// โดย Object ทั้งต้นทางและปลายทาง ต้องมี Property ที่เหมือนกัน(บางส่วน)
    /// </summary>
    /// <param name="obj">Object ที่ต้องการแปลง</param>
    /// <returns></returns>
    public static T ObjectFrom<TT>(TT obj)
    {
        string strMethod = System.Reflection.MethodInfo.GetCurrentMethod().Name;
        T objT = default(T);
        if (obj == null)
        {
            return objT;
        }
        objT = Activator.CreateInstance<T>();
        System.Reflection.PropertyInfo[] propsT = objT.GetType().GetProperties();

        Dictionary<string, System.Reflection.PropertyInfo> dicPropT;
        string propName = string.Empty;
        Dictionary<string, System.Reflection.PropertyInfo> dicPropTT = getProperty(obj.GetType().GetProperties());
        foreach (System.Reflection.PropertyInfo propT in propsT)
        {
            dicPropT = getProperty(propT);
            foreach (string propNameT in dicPropT.Keys)
            {
                if (dicPropTT.ContainsKey(propNameT))
                {
                    propName = propNameT;
                    break;
                }
            }
            if (propName != string.Empty)
            {
                try
                {
                    Object val = dicPropTT[propName].GetValue(obj, null);
                    propT.SetValue(objT, Convert.ChangeType(val, propT.PropertyType), null);
                    //logger.Debug(strMethod + "-SetValue_To[" + propT.Name + "]From[" + propName + "]");
                }
                catch
                {
                    //throw;
                }
                propName = string.Empty;
            }
        }
        return objT;
    }

    /// <summary>
    /// แปลง List ของ Object ใดๆ ให้เป็น List ของ Object ใดๆ 
    /// </summary>
    /// <param name="lsObj">List ของ Object ที่ต้องการแปลงเป็น List ของ Object T</param>
    /// <returns></returns>
    public static IList<T> ObjectExtraFrom<TT>(IList<TT> lsObj)
    {
        string strMethod = System.Reflection.MethodInfo.GetCurrentMethod().Name;
        if (lsObj == null)
        {
            return null;
        }
        return ObjectExtraFrom(lsObj.ToArray());
    }

    /// <summary>
    /// แปลงกลุ่มข้อมูล Object[]ใดๆ ที่มี class อื่นๆ อยู่ภายใน ให้เป็น List ของ Object ใดๆ 
    /// โดย Object ทั้งต้นทางและปลายทาง ต้องมี Property ที่เหมือนกัน(บางส่วน)
    /// </summary>
    /// <param name="objs">กลุ่มข้อมูล Object[] ที่ต้องการแปลงเป็น List ของ Object</param>
    /// <returns></returns>
    public static IList<T> ObjectExtraFrom<TT>(TT[] objs)
    {
        string strMethod = System.Reflection.MethodInfo.GetCurrentMethod().Name;
        if (objs == null)
        {
            return null;
        }
        System.Xml.XmlDocument xmldoc = XmlHelper.ConvertToXmlDocument(objs);
        IList<T> ret = XmlHelper.ConvertXmlDocument<List<T>>(xmldoc);
        return ret;
    }

    /// <summary>
    /// แปลง Object ที่มี class อื่นๆ อยู่ภายใน ให้เป็นอีก Object หนึ่ง
    /// โดย Object ทั้งต้นทางและปลายทาง ต้องมี Property ที่เหมือนกัน(บางส่วน)
    /// </summary>
    /// <typeparam name="TT"></typeparam>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static T ObjectExtraFrom<TT>(TT obj)
    {
        string strMethod = System.Reflection.MethodInfo.GetCurrentMethod().Name;
        System.Xml.XmlDocument xmldoc = XmlHelper.ConvertToXmlDocument(obj);
        T ret = XmlHelper.ConvertXmlDocument<T>(xmldoc);
        return ret;
    }

    #region "Private method"
    private static System.Reflection.PropertyInfo getProperty(System.Reflection.PropertyInfo[] PropertyInfos, string propertyName)
    {
        foreach (System.Reflection.PropertyInfo item in PropertyInfos)
        {
            foreach (var attribute in item.GetCustomAttributes(true))
            {
                if (attribute is System.Xml.Serialization.XmlElementAttribute)
                {
                    string elementName = ((System.Xml.Serialization.XmlElementAttribute)attribute).ElementName;
                    if (elementName.ToUpper().Equals(propertyName.ToUpper()))
                    {
                        return item;
                    }
                }
            }
            if (item.Name.ToUpper().Equals(propertyName.ToUpper()))
            {
                return item;
            }
        }

        return null;
    }
    private static Dictionary<string, System.Reflection.PropertyInfo> getProperty(System.Reflection.PropertyInfo[] propertyInfos)
    {
        Dictionary<string, System.Reflection.PropertyInfo> ret = new Dictionary<string, System.Reflection.PropertyInfo>();
        foreach (System.Reflection.PropertyInfo item in propertyInfos)
        {
            if (!ret.ContainsKey(item.Name)) ret.Add(item.Name, item);
            foreach (var attribute in item.GetCustomAttributes(true))
            {
                if (attribute is System.Xml.Serialization.XmlElementAttribute)
                {
                    string elementName = ((System.Xml.Serialization.XmlElementAttribute)attribute).ElementName;
                    if (!ret.ContainsKey(elementName)) ret.Add(elementName, item);
                }
            }
        }
        return ret;
    }
    private static Dictionary<string, System.Reflection.PropertyInfo> getProperty(System.Reflection.PropertyInfo propertyInfo)
    {
        Dictionary<string, System.Reflection.PropertyInfo> ret = new Dictionary<string, System.Reflection.PropertyInfo>();
        if (!ret.ContainsKey(propertyInfo.Name)) ret.Add(propertyInfo.Name, propertyInfo);
        foreach (var attribute in propertyInfo.GetCustomAttributes(true))
        {
            if (attribute is System.Xml.Serialization.XmlElementAttribute)
            {
                string elementName = ((System.Xml.Serialization.XmlElementAttribute)attribute).ElementName;
                if (!ret.ContainsKey(elementName)) ret.Add(elementName, propertyInfo);
            }
        }
        return ret;
    }
    #endregion

}
