using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


/// <summary>
/// Summary description for Utility
/// </summary>
public class Utility
{
    public Utility()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static bool IsViewPage()
    {
        bool ret = false;

        //string url = HttpRequest.ServerVariables["SCRIPT_NAME"];
        string url = HttpContext.Current.Request.ServerVariables["SCRIPT_NAME"];
        string pageName = "";
        string[] arr = url.Split('/');
        if (arr.Length != 0)
        {
            pageName = arr[arr.Length - 1];
        }

        if (pageName == "frmViewRequest.aspx")  //page View
        {
            ret = true;
        }

        return ret;
    }

}
