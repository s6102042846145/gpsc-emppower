using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public delegate void ListSelectChangeEventHandler(object sender, ListSelectChangeEventArgs e);
public class ListSelectChangeEventArgs : EventArgs
{
    private string __selectedItem = "";
    private bool __isHaveAuthorize = true;
    public ListSelectChangeEventArgs(string selectedItem)
    {
        __selectedItem = selectedItem;
    }

    public string SelectedItem
    {
        get
        {
            return __selectedItem;
        }
    }

    public bool IsHaveAuthorize
    {
        get
        {
            return __isHaveAuthorize;
        }
        set
        {
            __isHaveAuthorize = value;
        }
    }
}