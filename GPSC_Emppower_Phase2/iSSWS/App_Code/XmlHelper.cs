﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

    public static class XmlHelper
    {

        ///// <summary>
        ///// แปลงจาก DataSet เป็น XmlDocument
        ///// </summary>
        ///// <param name="dataset">DataSet ที่ต้องการแปลง</param>
        ///// <returns></returns>
        //public static XmlDocument ConvertToXmlDocument(DataSet dataset)
        //{
        //    XmlDocument xmlDoc = new XmlDocument();
        //    try
        //    {
        //        StringWriter stringWriter = new StringWriter();
        //        XmlTextWriter xmlTextWriter = new XmlTextWriter(stringWriter);
        //        dataset.WriteXml(xmlTextWriter, XmlWriteMode.IgnoreSchema);
        //        string contentAsXmlString = stringWriter.ToString();
        //        xmlDoc.LoadXml(contentAsXmlString);
        //        xmlTextWriter.Close();
        //        stringWriter.Close();
        //    }
        //    catch { }
        //    return xmlDoc;
        //}

        ///// <summary>
        ///// แปลงจาก DataTable เป็น XmlDocument
        ///// </summary>
        ///// <param name="datatable">DataTable ที่ต้องการแปลง</param>
        ///// <returns></returns>
        //public static XmlDocument ConvertToXmlDocument(DataTable datatable)
        //{
        //    DataSet ds = new DataSet();
        //    ds.Tables.Add(datatable);
        //    return ConvertToXmlDocument(ds);
        //}

        /// <summary>
        /// เปลี่ยนจาก Object ให้เป็น XmlDocument
        /// </summary>
        /// <param name="obj">Object ที่ต้องการเปลี่ยนเป็น XmlDocument</param>
        /// <returns></returns>
        public static XmlDocument ConvertToXmlDocument<T>(T obj)
        {
            XmlDocument xmlOut = new XmlDocument();
            try
            {
                StringWriter writer = new StringWriter();
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                ns.Add("", "");
                XmlSerializer xsr = new XmlSerializer(typeof(T));
                xsr.Serialize(writer, obj, ns);
                xmlOut.LoadXml(writer.ToString());
                writer.Close();
            }
            catch { }
            return xmlOut;
        }

        ///// <summary>
        ///// แปลงจาก XmlDocument เป็น DataSet
        ///// </summary>
        ///// <param name="xmlDoc">XmlDocument ที่ต้องการแปลง</param>
        ///// <returns></returns>
        //public static DataSet ConvertXmlDocument(XmlDocument xmlDoc)
        //{
        //    DataSet ds = new DataSet();
        //    try
        //    {
        //        StringReader sr = new StringReader(xmlDoc.OuterXml.ToString());
        //        XmlReader reader = new XmlTextReader(sr);
        //        ds.ReadXml(reader);
        //        sr.Close();
        //        reader.Close();
        //    }
        //    catch { }
        //    return ds;
        //}

        /// <summary>
        /// เปลี่ยนจากไฟล์ XmlDocument ให้เป็น Object
        /// </summary>
        /// <param name="xmlDoc">Xml Document</param>
        /// <returns></returns>
        public static T ConvertXmlDocument<T>(XmlDocument xmlDoc)
        {
            Object obj = null;
            try
            {
                // reader xml text to string reader object
                StringReader strReader = new StringReader(xmlDoc.OuterXml.ToString());
                Assembly assembly = Assembly.Load(typeof(T).Assembly.FullName);
                Object objType = assembly.CreateInstance(typeof(T).FullName);

                // define type to generic object
                XmlSerializer xsr = new XmlSerializer(objType.GetType());
                obj = xsr.Deserialize(strReader);
                strReader.Close();
            }
            catch { }
            return (T)obj;
        }

        ///// <summary>
        ///// เปลี่ยนจากไฟล์ XmlDocument ตาม xmlPath โดยระบุ Assembly String และ Class Name ให้เป็น Object
        ///// </summary>
        ///// <param name="xmlPath">file name ของ xml</param>
        ///// <param name="assemblyString">The long from of the assembly name</param>
        ///// <param name="className">ClassName</param>
        ///// <returns>Object</returns>
        //public static T ConvertXmlDocument<T>(String xmlPath)
        //{
        //    XmlDocument xDoc = new XmlDocument();
        //    xDoc.Load(xmlPath);
        //    return ConvertXmlDocument<T>(xDoc);
        //}

        ///// <summary>
        ///// เปลี่ยนจากไฟล์ XmlDocument ตาม xmlPath โดยระบุ Type ของ Object ที่ต้องการแปลง
        ///// </summary>
        ///// <param name="xmlPath">file name ของ xml</param>
        ///// <param name="typeObject">Type ของ Object ที่ต้องการแปลง</param>
        ///// <returns></returns>
        //public static Object ConvertXmlDocument(String xmlPath, Type typeObject)
        //{
        //    return ConvertXmlDocument(xmlPath, typeObject.Assembly.FullName, typeObject.FullName);
        //}

        //public static bool WriteXmlDocument<T>(string filepath, T obj)
        //{
        //    try
        //    {
        //        System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(typeof(T));
        //        System.IO.StreamWriter file;
        //        using (file = new System.IO.StreamWriter(filepath))
        //        {
        //            writer.Serialize(file, obj);
        //            file.Close();
        //            return true;
        //        }
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}

        //public static void WriteToBinaryFile<T>(string filepath, T obj)
        //{
        //    using (System.IO.Stream stream = System.IO.File.Open(filepath, System.IO.FileMode.Create))
        //    {
        //        var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
        //        binaryFormatter.Serialize(stream, obj);
        //    }
        //}

        //public static T[] ReadFromBinaryFile<T>(string filepath)
        //{
        //    using (System.IO.Stream stream = System.IO.File.Open(filepath, System.IO.FileMode.Open))
        //    {
        //        var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
        //        return (T[])binaryFormatter.Deserialize(stream);
        //    }
        //}
    }
