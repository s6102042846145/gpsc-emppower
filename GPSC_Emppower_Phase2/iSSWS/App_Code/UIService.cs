using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Reflection;
using ESS.DATA;
using ESS.DATA.INTERFACE;

/// <summary>
/// Summary description for UIService
/// </summary>
public class UIService
{
    public static Type GetControlType(string ClassName)
    {
        Type oType = Type.GetType(string.Format("Editor.Control.{0}", ClassName));
        return oType;
    }
    public static IDataService GetDataService(string AssemblyName, string ClassName)
    {
        try
        {
            Assembly oAssembly = Assembly.Load(AssemblyName);
            Type oType = oAssembly.GetType(ClassName);
            IDataService oService = (IDataService)Activator.CreateInstance(oType);
            return oService;
        }
        catch (Exception ex)
        {
            throw new Exception("Can't get data service",ex);
        }
    }
}