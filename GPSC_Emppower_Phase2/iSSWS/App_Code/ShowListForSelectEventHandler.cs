using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ESS.EMPLOYEE;

public delegate void ShowListForSelectEventHandler(object sender, ShowListForSelectEventArgs e);
public class ShowListForSelectEventArgs : EventArgs
{
    List<EmployeeData> __listForShow = new List<EmployeeData>();
    public ShowListForSelectEventArgs()
    {
    }

    public List<EmployeeData> ListForShow
    {
        get
        {
            return __listForShow;
        }
    }
}