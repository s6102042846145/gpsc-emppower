﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

namespace iSSWS
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            config.EnableCors();
          
            // Web API routes
            config.MapHttpAttributeRoutes();


            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "{controller}/{action}/{Param1}/{Param2}/{Param3}/{Param4}/{Param5}",
                defaults: new { Param1 = RouteParameter.Optional, Param2 = RouteParameter.Optional, Param3 = RouteParameter.Optional, Param4 = RouteParameter.Optional, Param5 = RouteParameter.Optional }
            );

        }
    }
}
