﻿(function () {
    angular.module('ESSMobile')
        .controller('MainController', ['$rootScope', '$scope', '$http', '$document', '$window', '$route', '$location', 'CONFIG', '$timeout', '$mdDialog', '$routeParams', function ($rootScope, $scope, $http, $document, $window, $route, $location, CONFIG, $timeout, $mdDialog, $routeParams) {

            $scope.key_lightbox = false;
            $scope.ShowTextMenu = false;
            var qa_type = $location.search().type;

            if (qa_type == 1) {
                $scope.key_lightbox = true;
            } else {
                $scope.key_lightbox = false;
            }

            $scope.rootNewWindowTab = function (url, title, w, h) {
                // Fixes dual-screen position                             Most browsers      Firefox
                var dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : window.screenX;
                var dualScreenTop = window.screenTop !== undefined ? window.screenTop : window.screenY;

                var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
                var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

                var systemZoom = width / window.screen.availWidth;
                var left = (width - w) / 2 / systemZoom + dualScreenLeft;
                var top = (height - h) / 2 / systemZoom + dualScreenTop;
                var newWindow = window.open(url, title, `scrollbars=yes, width=${w / systemZoom},  height=${h / systemZoom}, top=${top}, left=${left}`);

                // ลบ Element Menu ออกจาก Window open
                $(newWindow.document).ready(function () {
                    var checkJob = setInterval(function () {
                        if ($('#menuSection', newWindow.document)) {
                            $('#menuSection', newWindow.document).remove();
                            clearInterval(checkJob);
                        }
                    }, 100);
                });
                
                if (window.focus) newWindow.focus();
            };


            $scope.runtime = Date.now();
            $scope.loader = {
                enable: false,
                childRequestEnable: false
            };
            $scope.employeeData = getToken(CONFIG.USER);
            $scope.PassingDocument = { document: null };
            console.log('MainController clear Passing Document.');
            $scope.MODULE_SETTING = CONFIG.MODULE_SETTING;
            $scope.PAGE_SETTING = CONFIG.PAGE_SETTING;
            $scope.PROFILE_SETTING = { POSITIONID: '' };
            $scope.CONFIG_SERVER = CONFIG.SERVER;

            // Please search all text int project 'RECEIPT_LIMIT_DATE_09182017' to find the assignment line
            $scope.RECEIPT_LIMIT_DATE = {
                RP_LIMIT_MINDATE: 0,
                RP_LIMIT_MAXDATE: 0,
                GE_LIMIT_MINDATE: 0,
                GE_LIMIT_MAXDATE: 0,
                GE_ALLOW_SELECTDATE_OUTOFPERIOD: false,
                RP_ALLOW_SELECTDATE_OUTOFPERIOD: false,
                GE_VALIDATE_EXPENSETYPE_TIMECONSTRAIN: false,
                RP_VALIDATE_EXPENSETYPE_TIMECONSTRAIN: false
            };
            $scope.documentState = {
                isDirty: false
            };
            $scope.start_date_for_apply_perdiem_distribution = '2016-12-01T00:00:00.000';
            $scope.blockAction = {
                isBLock: false,
                blockMessage: ''
            };

            $scope.class = "red";
            $scope.changeClass = function () {
                if ($scope.class === "red")
                    $scope.class = "blue";
                else
                    $scope.class = "red";
            };
            $scope.menuCtrl = {
                isHideMenu: true
            };
            $scope.toggleMenu = function () {
                $scope.menuCtrl.isHideMenu = !$scope.menuCtrl.isHideMenu;

            };
            var focus_guide = $("body").guide();
            $scope.foucus_element = function (tagId, text) {
                focus_guide = $("body").guide();
                focus_guide.addStep(tagId, text);
                focus_guide.start();
                $(tagId).click(function () {
                    focus_guide.clear();
                });
            };

            $scope.getCurrentMonth = function () {
                var date = new Date();
                var y = date.getFullYear();
                var m = date.getMonth() + 1;
                if (m.toString().length == 1) {
                    m = '0' + m.toString();
                }
                return y.toString() + m.toString();
            };


            $scope.flowCurrent = function (flowItems) {
                var status = 0;
                for (i = 0; i < flowItems.length; i++) {
                    if (flowItems[i].IsCurrentItem) {
                        flowItems[i].STATUS_FLOW_ITEM = 1;
                        status = 2;
                        continue;
                    }
                    else if (i == flowItems.length - 1) {
                        status = 3;
                    }
                    flowItems[i].STATUS_FLOW_ITEM = angular.copy(status);

                }
                return flowItems;
            };
            /* Text Description */

            $scope.Text = null;
            $scope.RequestorText = null;
            $scope.getAllTextDescription = function () {
                $scope.Text = null;
                var oRequestParameter = { InputParameter: { SYSTEM: 'TE&E' }, CurrentEmployee: getToken(CONFIG.USER) };
                var URL = CONFIG.SERVER + 'workflow/GetTextDescriptionBySystem/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    if (response.data != null) {
                        $scope.Text = response.data;
                        $scope.Text['SYSTEM']['FILE_LIMIT'] = $scope.FILELIMIT_DESCRIPTION();
                        $scope.ShowTextMenu = true;
                    }
                    console.log('----------- TextDescription. -----------', angular.copy($scope.Text));
                }, function errorCallback(response) {
                    // Error
                    $scope.Text = null;
                    console.log('error MainController TextDescription.', response);
                });
            };
            $scope.getAllTextDescription();

            $scope.getTextByCompany = function (oRequestor) {
                //$scope.RequestorText = angular.copy($scope.Text);
                var oRequestParameter = { InputParameter: {}, Requestor: oRequestor };
                var URL = CONFIG.SERVER + 'workflow/GetTextDescriptionBySystem/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    if (response.data != null) {
                        $scope.RequestorText = response.data;
                    }
                    console.log('----------- TextDescription. -----------', angular.copy($scope.Text));
                }, function errorCallback(response) {
                    // Error
                    $scope.RequestorText = angular.copy($scope.Text);
                    console.log('error MainController TextDescription.', response);
                });
            };

            /* !Text Description */

            /* Expense Receipt */

            $scope.SystemInitialReady = false;
            $scope.systemMasterData = {};
            $scope.initialSystem = function () {
                $scope.systemMasterData = {};
                $scope.SystemInitialReady = true;
                var oRequestParameter = { InputParameter: { Module: 'TE&E' }, CurrentEmployee: getToken(CONFIG.USER) }
                var URL = CONFIG.SERVER + 'workflow/InitialSystemByModule/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    if (response.data != null) {
                        $scope.systemMasterData.IsAccountUser = response.data.IsAccountUser;
                        $scope.systemMasterData.ExpenseModel = response.data.ExpenseModel;
                    }
                    $scope.SystemInitialReady = true;
                }, function errorCallback(response) {
                    // Error
                    $scope.SystemInitialReady = false;
                    alert('Please refresh page again.');
                });
            };
            $scope.initialSystem();

            /* Expense Receipt */


            //$scope.getTextDescriptionByTextCode = function (Textcategory, TextCode) {
            //    var URL = CONFIG.SERVER + 'workflow/GetTextDescription/' + Textcategory + '/' + TextCode;
            //    $http({
            //        method: 'POST',
            //        url: URL,
            //        data: getToken(CONFIG.USER)
            //    }).then(function successCallback(response) {
            //        // Success
            //        //console.log('content.', response.data);
            //        return response.data;

            //    }, function errorCallback(response) {
            //        // Error
            //        console.log('error ContentController.', response);
            //        return null;

            //    });
            //};

            $scope.changeLanguage = function (language) {
                //var employeeData = getToken(CONFIG.USER);
                //var URL = CONFIG.SERVER + 'workflow/GetEmployeeData/' + language;

                var URL = CONFIG.SERVER + 'workflow/GetEmployeeData';
                var oRequestParameter = { InputParameter: { "Language": language }, CurrentEmployee: getToken(CONFIG.USER) };

                // change EmployeeData
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    window.localStorage.removeItem(CONFIG.USER);
                    window.localStorage.setItem(CONFIG.USER, JSON.stringify(response.data.EmployeeData));
                    console.log('success changeLanguage.', getToken(CONFIG.USER));

                    // Reload Menu
                    var args = {};

                    // $timeout
                    $scope.getAllTextDescription();
                    $scope.initialSystem();

                    $rootScope.$broadcast('onReloadMenu', args);


                }, function errorCallback(response) {
                    // Error
                    console.log('error changeLanguage.', response);
                });
            };

            $scope.goBack = function () {
                $window.history.back();
            };


            $scope.Orgsettings = {
                OrgUnit: '',
                OrgUnitName: '',
                OrgUnitShortText: ''

            };


            $scope.getOrgUnitShortText = function () {
                var URL = CONFIG.SERVER + 'Employee/INFOTYPE1000GetOrgUnit/';
                var oRequestParameter = {
                    InputParameter: { "ObjectID": $scope.employeeData.OrgUnit }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success

                    $scope.objOrgUnit = response.data;
                    if (angular.isDefined($scope.objOrgUnit) && $scope.objOrgUnit != null) {
                        if ($scope.employeeData.Language == 'EN') {
                            if ($scope.objOrgUnit.ShortTextEn !== null && $scope.objOrgUnit.ShortTextEn !== '') {
                                $scope.Orgsettings.OrgUnitShortText = '( ' + $scope.objOrgUnit.ShortTextEn + ' )';
                            }
                        }
                        else {
                            if ($scope.objOrgUnit.ShortText !== null && $scope.objOrgUnit.ShortText !== '') {
                                $scope.Orgsettings.OrgUnitShortText = '( ' + $scope.objOrgUnit.ShortText + ' )';
                            }
                        }
                    }
                }, function errorCallback(response) {
                });
            };
            $scope.getOrgUnitShortText();


            $scope.getImageProxy = function (urlImage) {
                var urlImage = window.encodeURIComponent(urlImage);
                var URL = CONFIG.SERVER + 'workflow/GetProfileImageProxy/?Param1=' + urlImage;
                return URL;
            };

            /* helper method */

            $scope.parseDateStringToObj = function (dateString) {
                if (typeof (dateString) != 'undefined' && dateString != '') {
                    var from = dateString.split("/");
                    var dateObj = new Date(from[2], from[1] - 1, from[0]); // (year,month,date)
                    return dateObj;
                }
                return new Date();
            };

            $scope.getLocalDateString = function (dateString) {
                if (typeof (dateString) != 'undefined' && typeof (dateString) == 'string' && dateString != '') {
                    return dateString + '+07:00';
                }
                return '';
            };

            $scope.showText = function (sourceText) {
                if (angular.isDefined(sourceText) || sourceText == null || sourceText == '') {
                    return '-';
                }
                return sourceText;
            };

            $scope.zeroPaddingText = function (sourceText, digit) {
                var str = '';
                for (var i = 0; i < digit; i++) {
                    str += '0';
                }
                sourceText = str + sourceText.toString();
                return sourceText.substr((-1 * digit), digit);
            };

            $scope.roundDecimal = function (number) {
                var checkNumber = Number(number);
                if (isNaN(checkNumber)) {
                    return 0;
                } else {
                    return Math.round(checkNumber * 100) / 100;
                }
            };

            Array.prototype.sum = function (prop) {
                var total = 0
                for (var i = 0, _len = this.length; i < _len; i++) {
                    if (angular.isString(this[i][prop])) {
                        total += $scope.roundDecimal(parseFloat(this[i][prop].replace(',', '')));
                    }
                    else {
                        total += $scope.roundDecimal(parseFloat(this[i][prop]));
                    }
                }
                return total;
            };

            Array.prototype.isZero = function (prop) {
                var ret = false;
                for (var i = 0, _len = this.length; i < _len; i++) {
                    if (angular.isString(this[i][prop])) {
                        if (this[i][prop].replace(',', '') <= 0) return true;
                    }
                    else {
                        if (this[i][prop] <= 0) return true;
                    }
                }
                return ret;
            };

            Array.prototype.nullProp = function (prop) {
                var ret = false;
                for (var i = 0, _len = this.length; i < _len; i++) {
                    if (!this[i][prop]) return true;
                }
                return ret;
            };

            Array.prototype.PropHasValue = function (prop) {
                var ret = false;
                for (var i = 0, _len = this.length; i < _len; i++) {
                    if (this[i][prop]) return true;
                }
                return ret;
            };

            Array.prototype.hasNull = function () {
                var ret = false;
                for (var i = 0, _len = this.length; i < _len; i++) {
                    if (!this[i]) return true;
                }
                return ret;
            };

            Array.prototype.findIndexWithAttr = function (prop, value) {
                var ret = -1;
                for (var i = 0, _len = this.length; i < _len; i++) {
                    if (this[i])
                        if (this[i][prop] == value)
                            return i;
                }
                return ret;
            };

            Array.prototype.duplicate = function (value) {
                for (var i = 0, _len = this.length; i < _len; i++) {
                    if (this[i] == value) return true;
                }
                return false;
            };

            Array.prototype.duplicateProp = function (prop, value) {
                for (var i = 0, _len = this.length; i < _len; i++) {
                    if (this[i])
                        if (this[i][prop] == value)
                            return true;
                }
                return false;
            };

            Array.prototype.duplicatePropOfProp = function (prop, propOfProp, value) {
                for (var i = 0, _len = this.length; i < _len; i++) {
                    if (this[i])
                        if (this[i][prop])
                            if (this[i][prop][propOfProp] == value)
                                return true;
                }
                return false;
            };

            Array.prototype.propIndexOf = function (prop, val) {
                for (var i = 0, _len = this.length; i < _len; i++) {
                    if (this[i])
                        if (this[i][prop].toLowerCase().indexOf(val) > -1)
                            return i;
                }
                return -1;
            };

            Array.prototype.likeIndexOf = function (val) {
                for (var i = 0, _len = this.length; i < _len; i++) {
                    if (this[i].toLowerCase().indexOf(val) > -1)
                        return i;
                }
                return -1;
            };

            Array.prototype.likeWithMappingIndexOf = function (func, val) {
                for (var i = 0, _len = this.length; i < _len; i++) {
                    if (func[this[i]].toLowerCase().indexOf(val) > -1)
                        return i;
                }
                return -1;
            };

            $scope.MathRounding = function (amount) {
                return Math.round(amount * 100) / 100;
            };

            $scope.MathRoundingToString = function (amount) {
                return (Math.round(amount * 100) / 100).toFixed(2);
            };

            /* !helper method */

            // document ready
            angular.element(document).ready(function () {
                $('body').on('keypress', 'input[type="number"]', function (evt) {
                    //alert(this.value);
                    //alert($(this).val());
                    //alert($(evt.target).val());
                    var key = evt.which;
                    if (!evt.ctrlKey) {
                        if (!((key >= 48 && key <= 57) || key == 0 || key == 8 || key == 46)) {
                            evt.preventDefault();
                        }
                    }
                });

                //$('body').on('change', 'input[type="number"]', function (evt) {
                //    $(this).val(parseFloat($(this).val()).toFixed(2));
                //});

                $('body').on('keypress', 'input[type="text"].only-number', function (evt) {
                    var key = evt.which;
                    if (!evt.ctrlKey) {
                        if (!((key >= 48 && key <= 57) || key == 0 || key == 8)) {
                            evt.preventDefault();
                        }
                    }
                });

                $('body').on('keypress', 'input[type="text"].only-decimal', function (evt) {
                    var key = evt.which;
                    if (!evt.ctrlKey) {
                        if (!((key >= 48 && key <= 57) || key == 0 || key == 8 || key == 46)) {
                            evt.preventDefault();
                        }
                    }
                });

                $('body').on('click', 'div[data-toggle="collapse2"]', function (evt) {
                    $(this).next('div.collapse').slideToggle();
                });

            });

            $scope.$on('$viewContentLoaded', function () {
                $window.scrollTo(0, 0);
            });

            $scope.$on("$locationChangeStart", function (event, next, current) {
                console.log('$locationChangeStart');
                // prevent user accidentally leave edit document page
                //console.debug('here');
                if ($scope.documentState.isDirty) {
                    event.preventDefault();
                    var confirm = $mdDialog.confirm()
                     .title($scope.Text['SYSTEM']['CONFIRM_REDIRECT_TITLE'])
                     .textContent($scope.Text['SYSTEM']['CONFIRM_REDIRECT_DETAIL'])
                     .ok('OK')
                     .cancel('Cancel');
                    $mdDialog.show(confirm).then(function (result) {
                        $scope.documentState.isDirty = false;
                        //console.debug(next);
                        //console.debug(next.replace(CONFIG.SERVER + 'client/index.html#', ''));
                        //console.debug(CONFIG.SERVER);
                        //console.debug(next);
                        var cutoff = CONFIG.SERVER + 'Client/index.html#!';
                        //console.debug(cutoff);
                        var newNext = next.replace(cutoff, '');
                        //console.debug(newNext);
                        $location.path(newNext);
                    }, function () {
                        event.preventDefault();
                    });
                }

            });

            $scope.scrollToTop = function () {
                $('#main_scroll').animate({ scrollTop: 0 }, 'fast');
            };

            $scope.setBlockAction = function (isBLock, blockMessage) {
                $scope.blockAction.isBLock = isBLock;
                $scope.blockAction.blockMessage = blockMessage;
            };
          

            //GUID gen
            $scope.guid = function () {
                function s4() {
                    return Math.floor((1 + Math.random()) * 0x10000)
                        .toString(16)
                        .substring(1);
                }
                return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                    s4() + '-' + s4() + s4() + s4();
            };
            
            $scope.limMaxArr = function (arr, max) {
                var tempArr = []; var tempArrIn = angular.copy(arr);
                if (tempArrIn.length > max) { tempArr = tempArrIn.splice(0, max); } else { return arr; }
                return tempArr;
            };

            $scope.FILELIMIT_DESCRIPTION = function () {
                var oResult = String.format($scope.Text['SYSTEM']['FILE_LIMIT']
                    , (CONFIG.FILE_SETTING.ALLOW_FILESIZE / CONFIG.FILE_SETTING.DIVIDE_FILESIZE)
                    , CONFIG.FILE_SETTING.ALLOW_FILENAME_LENGTH);
                return oResult;
            };
            $scope.AllowUploadFile = function (fileName) {
                return CONFIG.FILE_SETTING.ALLOW_FILE.test(fileName);
            };
            $scope.AllowUploadFileType = function (fileName) {
                return CONFIG.FILE_SETTING.ALLOW_FILETYPE.indexOf(fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase()) >= 0;
            };
            $scope.AllowFileNameLength = function (fileNameLength) {
                return CONFIG.FILE_SETTING.ALLOW_FILENAME_LENGTH >= fileNameLength;
            };
            $scope.AllowFileSize = function (fileSize) {
                return CONFIG.FILE_SETTING.ALLOW_FILESIZE >= fileSize;
            };
            $scope.AllowFileZeroSize = function (fileSize) {
                return fileSize > 0;
            };
        }]);

})();