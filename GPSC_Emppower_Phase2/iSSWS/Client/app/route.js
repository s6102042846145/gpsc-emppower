﻿(function () {
    angular.module('ESSMobile')
        .config(function ($routeProvider) {
            $routeProvider
                .when('/frmViewContent/:id', {
                    templateUrl: 'views/frmViewContent.html'
                })
                .when('/frmViewContent/:id/:itemKey', {
                    templateUrl: 'views/frmViewContent.html'
                })
                .when('/frmViewContent/:id/:itemKey/:companycode', {
                    templateUrl: 'views/frmViewContent.html'
                })
                .when('/frmViewContent/:id/:requesterEmployeeId/:requesterPositionId/:requesterName/:requesterCompanyCode', {
                    /* ดูในฐานะหัวหน้า */
                    templateUrl: 'views/frmViewContent.html'
                })
                .when('/frmViewContent/:id/:requesterEmployeeId/:requesterPositionId/:requesterName/:requesterCompanyCode/:itemKey', {
                    /* ดูในฐานะหัวหน้า */
                    templateUrl: 'views/frmViewContent.html'
                })
                .when('/frmViewContent/:id/NEWPIN/:TicketID', {
                    /* ดูในฐานะหัวหน้า */
                    templateUrl: 'views/frmViewContent.html'
                })
                .when('/ByPassTicket/:CompanyCode/:TicketID/:ExternaluserID', {
                    templateUrl: 'views/ByPassTicket.html'
                })
                .when('/ByPassTicket/:CompanyCode/:TicketID', {
                    templateUrl: 'views/ByPassTicket.html'
                })

                .when('/actionInsteadOf/:id', {
                    templateUrl: 'views/actionInsteadOf.html'
                })
                .when('/frmViewBox/:id', {
                    templateUrl: 'views/frmViewBox.html'
                })
                .when('/frmCreateRequest/:id/:itemKey', {
                    templateUrl: 'views/frmCreateRequest.html'
                })
                .when('/frmCreateRequest/:id/:itemKey/:otherParam', {
                    /* กรณีมีการส่ง Text Header */
                    templateUrl: 'views/frmCreateRequest.html'
                })
                .when('/frmCreateRequest/:id/:itemKey/:requesterEmployeeId/:requesterPositionId/:requesterName/:requesterCompanyCode', {
                    /* สร้างในฐานะหัวหน้า */
                    templateUrl: 'views/frmCreateRequest.html'
                })
                .when('/frmCreateRequest/:id/:itemKey/:requesterEmployeeId/:requesterPositionId/:requesterName/:requesterCompanyCode/:otherParam', {
                    /* สร้างในฐานะหัวหน้า และมี otherParam*/
                    templateUrl: 'views/frmCreateRequest.html'
                })
                //comment by Koissares P. 20190122: ESS  not use referRequestNo
                //.when('/frmCreateRequest/:id/:itemKey/:referRequestNo', {
                //    /* กรณีมีการส่ง Text Header */
                //    /* referRequestNo อ้างเอกสารเดิม */
                //    templateUrl: 'views/frmCreateRequest.html'
                //})
                .when('/frmCreateRequest/:id/:itemKey/:referRequestNo/:otherParam', {
                    /* กรณีมีการส่ง Text Header */
                    /* referRequestNo อ้างเอกสารเดิม */
                    templateUrl: 'views/frmCreateRequest.html'
                })
                .when('/frmCreateRequest/:id/:itemKey/:referRequestNo/:requesterEmployeeId/:requesterPositionId/:requesterName/:requesterCompanyCode/:otherParam', {
                    /* กรณีมีการส่ง Text Header */
                    /* referRequestNo อ้างเอกสารเดิม */
                    templateUrl: 'views/frmCreateRequest.html'
                })
                .when('/frmViewRequest/:id/:requestCompanyCode/:keyMaster/:isOwner/:isDataOwner', {
                    /* ถ้าดูในโหมด view only หรือดูในฐานะหัวหน้า keyMaster = null, isOwner = true, isDataOwner = true */
                    templateUrl: 'views/frmViewRequest.html'
                })
                .when('/frmViewRequest/:id/:requestCompanyCode/:keyMaster/:isOwner/:isDataOwner/:textHeader', {
                    /* กรณีมีการส่ง Text Header */
                    /* ถ้าดูในโหมด view only หรือดูในฐานะหัวหน้า keyMaster = null, isOwner = true, isDataOwner = true */
                    templateUrl: 'views/frmViewRequest.html'
                })
                .when('/frmViewRequest/:id/:requestCompanyCode/:keyMaster/:isOwner/:isDataOwner/:textHeader/:requesterEmployeeId/:requesterPositionId/:requesterName/:requesterCompanyCode', {
                    /* ถ้าดูในโหมด view only หรือดูในฐานะหัวหน้า keyMaster = null, isOwner = true, isDataOwner = true */
                    /* สร้างในฐานะหัวหน้า */
                    templateUrl: 'views/frmViewRequest.html'
                })
                .when('/frmViewRequest/:id/:requestCompanyCode/:keyMaster/:isOwner/:isDataOwner/:textHeader/:requesterEmployeeId/:requesterPositionId/:requesterName', {
                    /* ถ้าดูในโหมด view only หรือดูในฐานะหัวหน้า keyMaster = null, isOwner = true, isDataOwner = true */
                    /* สร้างในฐานะหัวหน้า */
                    templateUrl: 'views/frmViewRequest.html'
                })
                .when('/frmViewRequestSupport/:id/:requestCompanyCode/:keyMaster/:isOwner/:isDataOwner/:itemID', {
                    /* ถ้าดูในโหมด view only หรือดูในฐานะหัวหน้า keyMaster = null, isOwner = true, isDataOwner = true */
                    templateUrl: 'views/frmViewRequestSupport.html'
                })
                .when('/frmViewRequestWithReSimulate/:id/:requestCompanyCode/:keyMaster/:isOwner/:isDataOwner', {
                    /* ถ้าดูในโหมด view only หรือดูในฐานะหัวหน้า keyMaster = null, isOwner = true, isDataOwner = true */
                    templateUrl: 'views/frmViewRequestWithReSimulate.html'
                })
                //.when('/frmViewRequest/:id/:keyMaster/:isOwner/:isDataOwner/:textHeader', {
                //    /* กรณีมีการส่ง Text Header */
                //    /* ถ้าดูในโหมด view only หรือดูในฐานะหัวหน้า keyMaster = null, isOwner = true, isDataOwner = true */
                //    templateUrl: 'views/frmViewRequest.html'
                //})
                //.when('/frmViewRequest/:id/:keyMaster/:isOwner/:isDataOwner/:textHeader/:requesterEmployeeId/:requesterPositionId/:requesterName', {
                //    /* กรณีมีการส่ง Text Header */
                //    /* ถ้าดูในโหมด view only หรือดูในฐานะหัวหน้า keyMaster = null, isOwner = true, isDataOwner = true */
                //    /* สร้างในฐานะหัวหน้า */
                //    templateUrl: 'views/frmViewRequest.html'
                //})
                .when('/frmEditRequest/:id/:keyMaster/:isOwner/:isDataOwner/:itemKey', {
                    /* ถ้าไม่ใช้ ItemKey (กรณี Edit) ให้ใส่ 0 - กรณีขอยกเลิกเอกสารจะใช้ ItemKey  */
                    templateUrl: 'views/frmEditRequest.html'
                })
                .when('/frmEditRequest/:id/:keyMaster/:isOwner/:isDataOwner/:itemKey/:referRequestNo', {
                    /* กรณีมีการส่ง Text Header */
                    /* ถ้าไม่ใช้ ItemKey (กรณี Edit) ให้ใส่ 0 - กรณีขอยกเลิกเอกสารจะใช้ ItemKey  */
                    /* referRequestNo อ้างเอกสารเดิม */
                    templateUrl: 'views/frmEditRequest.html'
                })
                .when('/frmEditRequest/:id/:keyMaster/:isOwner/:isDataOwner/:itemKey/:referRequestNo/:otherParam', {
                    /* กรณีมีการส่ง Text Header */
                    /* ถ้าไม่ใช้ ItemKey (กรณี Edit) ให้ใส่ 0 - กรณีขอยกเลิกเอกสารจะใช้ ItemKey  */
                    /* referRequestNo อ้างเอกสารเดิม */
                    templateUrl: 'views/frmEditRequest.html'
                })
                .when('/frmEditRequest/:id/:keyMaster/:isOwner/:isDataOwner/:itemKey/:referRequestNo/:requesterEmployeeId/:requesterPositionId/:requesterName/:requesterCompanyCode/:otherParam', {
                    /* กรณีมีการส่ง Text Header */
                    /* ถ้าไม่ใช้ ItemKey (กรณี Edit) ให้ใส่ 0 - กรณีขอยกเลิกเอกสารจะใช้ ItemKey  */
                    /* สร้างในฐานะหัวหน้า */
                    templateUrl: 'views/frmEditRequest.html'
                })
                .when('/frmViewResult', {
                    templateUrl: 'views/frmViewResult.html'
                })
                .when('/frmViewResult/:requesterEmployeeId/:requesterPositionId/:requesterName/:requesterCompanyCode', {
                    /* สร้างในฐานะหัวหน้า */
                    templateUrl: 'views/frmViewResult.html'
                })
                .when('/Setting', {
                    templateUrl: 'views/common/SettingPanel.html'
                })
                .when('/getFile/:module/:RequestNo/:ReceiptID/:FileID', {
                    templateUrl: 'views/common/file_download.html'
                })
                //.when('/data', {
                //    templateUrl: 'views/tr/**/'
                //})
                .when('/', {
                    redirectTo: '/frmViewContent/0'
                })
                .otherwise({ redirectTo: '/frmViewContent/0' });
        });
})();