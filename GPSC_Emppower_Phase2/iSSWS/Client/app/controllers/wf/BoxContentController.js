﻿(function () {
    angular.module('ESSMobile')
        .controller('BoxContentController', ['$rootScope', '$route', '$scope', '$http', '$routeParams', '$location', '$filter', '$q', 'CONFIG', '$window', '$mdDialog', '$interval', function ($rootScope, $route, $scope, $http, $routeParams, $location, $filter, $q, CONFIG, $window, $mdDialog, $interval) {
            //#### FRAMEWORK FUNCTION ### START
            //Set text category of this content
            $scope.Textcategory = 'BOXCONTENT';
            $scope.content.Header = $scope.Text[$scope.Textcategory]['BOX' + $routeParams.id];


            $scope.data = {
                nPageSize: 10,
                arrFilterd: [],
                arrSplited: [],
                selectedColumn: 0,
                tFilter: '',
                pages: []
            };
            //#### FRAMEWORK FUNCTION ### END

            //#### OTHERS FUNCTION ### START
            $scope.content.isShowHeader = false;
            $scope.formData = { searchKeyword: '' };
            $scope.showWarning = false;
            $scope.warningActionText = "";

            var search = function (item, keyword) {
                // Search Criteria
                if (!keyword
                    || (item.RequestNo != null && item.RequestNo.toLowerCase().indexOf(keyword) != -1)
                    || (item.RequestorNo != null && item.RequestorNo.toLowerCase().indexOf(keyword) != -1)
                    || (item.RequestorName != null && item.RequestorName.toLowerCase().indexOf(keyword) != -1)
                    || (item.Summary != null && item.Summary.toLowerCase().indexOf(keyword) != -1)
                    || (item.CompanyShortName != null && item.CompanyShortName.toLowerCase().indexOf(keyword) != -1)
                    || (item.OrgName != null && item.OrgName.toLowerCase().indexOf(keyword) != -1)
                ) {
                    return true;
                }
                return false;
            };
            $scope.searchInBox = function () {
                console.log('searchInBox.', $scope.formData.searchKeyword);
                if (!$scope.formData.searchKeyword) {
                    $scope.data = $scope.masterDocuments;
                    temp_documents = angular.copy($scope.data);
                }
                else {
                    var count = $scope.masterDocuments.length;
                    var docs = [];
                    for (var i = 0; i < count; i++) {
                        if (search($scope.masterDocuments[i], $scope.formData.searchKeyword.toLowerCase())) {
                            docs.push($scope.masterDocuments[i]);
                        }
                    }
                    $scope.data = docs;
                    temp_documents = angular.copy($scope.data);
                }
                $scope.Action = $scope.Actions[0];
                //filterArry('');
                $scope.filterDatas('');
            };
            $scope.CheckRequestType = function () {

                return $scope.CanMassApprove;
            };
            $scope.getWarningText = function (text) {
                if (!angular.isUndefined(text)) {
                    return text.replace("{0}", $scope.warningActionText);
                }
                return '';
            };
            $scope.ActionFromDocument = function () {
                //requestno,acctioncode,current user
                //requestno,keymaster|requestno,keymaster, ActionCode , currentuser

                $scope.loader.enable = true;
                var actioncode = $scope.Action.ActionCode;
                var objRequestDocIsSel = angular.element(document.querySelectorAll("[requestdoc-isselect]:checked"));
                var oRequestNoSelected = "";
                for (var i = 0; i < objRequestDocIsSel.length; i++) {
                    if (oRequestNoSelected == "") {
                        oRequestNoSelected = oRequestNoSelected.concat(objRequestDocIsSel[i].value);
                    }
                    else {
                        oRequestNoSelected = oRequestNoSelected.concat('|', objRequestDocIsSel[i].value);

                    }
                }
                //alert(oRequestNoSelected);
                if (oRequestNoSelected != '' && actioncode != '') {
                    var oRequestParameter = { InputParameter: { "RequestNoSelected": oRequestNoSelected, "ActionCode": actioncode }, CurrentEmployee: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER) };
                    var URL = CONFIG.SERVER + 'workflow/MassApprove/' //+ oRequestNoSelected + '/' + actioncode;
                    // alert(URL);
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.loader.enable = false;

                        // Reload Menu
                        var args = {};
                        $rootScope.$broadcast('onReloadMenu', args);

                        $route.reload();
                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                        console.log('error boxcontroller.', response);
                    });
                }
                else {
                    $scope.loader.enable = false;
                    $scope.warningActionText = $scope.Action.ActionText;
                    $scope.showWarning = true;
                }
            };

            $scope.CanMassApprove = false;
            $scope.CheckMassApprove = function () {
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER) };
                var URL = CONFIG.SERVER + 'workflow/GetAuthorizeMassApprove/' + $scope.ContentParam;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.CanMassApprove = response.data;
                }, function errorCallback(response) {
                    console.log('error boxcontroller.', response);
                });
            };

            $scope.lstPositionApprove = [];
            $scope.IsPositionApprove = false;
            $scope.GetPositionApprove = function () {
                var oRequestParameter = { InputParameter: { "BoxID": $scope.ContentParam }, CurrentEmployee: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER) };
                var URL = CONFIG.SERVER + 'workflow/GetPositionApprove';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    if (response.data.Table4.length > 0) {
                        $scope.User = getToken(CONFIG.USER);
                        $scope.UserPosition = $scope.User.PositionID;

                        //$scope.lstPositionApprove = response.data.Table4;
                        $scope.lstPositionApprove.push({ isCurrentPostion: 'false', PositionID: '99999999', PositionName: 'ทั้งหมด' });

                        for (i = 0; i < response.data.Table4.length > 0;) {
                            if (response.data.Table4[i].IsCuurentPosition)
                                $scope.UserCurrentPosition = response.data.Table4[i].PositionID;
                            $scope.lstPositionApprove.push({ isCurrentPostion: response.data.Table4[i].IsCuurentPosition, PositionID: response.data.Table4[i].PositionID, PositionName: response.data.Table4[i].PositionName });
                            i++;
                        }
                        $scope.PositionID = "99999999";
                        $scope.PositionName = "ทั้งหมด";
                        $scope.auto_complete.ap_pdr_Position = $scope.PositionName;
                        $scope.temp_PositionName = $scope.PositionName;

                        if (($scope.ContentParam == 101 || $scope.ContentParam == 102 || $scope.ContentParam == 103 || $scope.ContentParam == 104) && $scope.lstPositionApprove.length > 1) {
                            $scope.IsPositionApprove = true;
                        }
                    }
                }, function errorCallback(response) {
                    console.log('error PositionApprove.', response);
                });
            };
            $scope.iPosition = 0;
            $scope.CountPostion = function (oPosition) {
                $scope.iPosition = 0;
                for (var i = 0; i < $scope.masterDocuments.length; i++) {
                    if ($scope.masterDocuments[i].ReceipientCode == oPosition)
                        $scope.iPosition++;
                }
                return $scope.iPosition;
            };

            $scope.SumCountDocument = function () {
                return ID;
            };

            var URL = CONFIG.SERVER + 'workflow/GetRequestDocumentListInBox';
            var oRequestParameter = { InputParameter: { "BoxID": $scope.ContentParam }, CurrentEmployee: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER) };

            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {


                /**Argif test filter start **/
                filterOption = '';
                $scope.searchOption = response.data.FLAGMASTER;
                $scope.masterDocuments = response.data.REQUESTDOCUMENT;

                $scope.documents = $filter('orderBy')(response.data.REQUESTDOCUMENT, 'LastActionDate', true);
                //console.log('D', $scope.documents);
                temp_documents = angular.copy($scope.documents);
                $scope.data = angular.copy($scope.documents);
                //filterArry('');
                $scope.filterDatas('');
                $scope.Actions = response.data.ACTION;
                if ($scope.Actions != null) {
                    if ($scope.Actions.length == 2) {
                        $scope.Actions.shift();
                        $scope.Action = $scope.Actions[0];
                    }
                    else if ($scope.Actions.length > 0) {
                        $scope.Action = $scope.Actions[0];
                    }
                }
                $scope.loader.enable = false;

                //console.log('documents.', $scope.documents);
            }, function errorCallback(response) {
                //    ActionCode;
                //console.log('error boxcontroller.', response);
            });
            var temp_documents;
            $scope.filterDocumentByAction = function () {
                if ($scope.Action.ActionCode == 'ALL') {
                    $scope.documents = angular.copy(temp_documents);
                } else {
                    var temp_documents_filtered = [];
                    for (var i = 0; i < temp_documents.length; i++) {
                        if (temp_documents[i].ActionCode == $scope.Action.ActionCode) {
                            temp_documents_filtered.push(temp_documents[i]);
                        }
                    }
                    $scope.documents = angular.copy(temp_documents_filtered);
                }
            };

            $scope.windowCenter = function (url, title, w, h) {
                // Fixes dual-screen position                             Most browsers      Firefox
                var dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : window.screenX;
                var dualScreenTop = window.screenTop !== undefined ? window.screenTop : window.screenY;

                var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
                var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

                var systemZoom = width / window.screen.availWidth;
                var left = (width - w) / 2 / systemZoom + dualScreenLeft;
                var top = (height - h) / 2 / systemZoom + dualScreenTop;
                var newWindow = window.open(url, title, `scrollbars=yes, width=${w / systemZoom},  height=${h / systemZoom}, top=${top}, left=${left}`);

                // ลบ Element Menu ออกจาก Window open
                $(newWindow.document).ready(function () {

                    var checkJob = setInterval(function () {
                        if ($('#menuSection', newWindow.document).length > 0) {
                            $('#menuSection', newWindow.document).remove();
                            clearInterval(checkJob);
                        }
                    }, 1000);

                    var checkJob2 = setInterval(function () {
                        if ($("#PopupDelete", newWindow.document).length > 0) {
                            $("#PopupDelete", newWindow.document).remove();
                            clearInterval(checkJob2);
                        }
                    }, 1000);

                    var checkJob3 = setInterval(function () {

                        if ($("#navBarRight", newWindow.document).length > 0) {
                            $("#navBarRight", newWindow.document).remove();
                            clearInterval(checkJob3);
                        }
                    }, 1000);
                });

                if (window.focus) {
                    newWindow.focus();
                }

            };


            $scope.openRequest = function (id, companyCode, keyMaster, isOwner, isDataOwner, requestType) {

                $scope.windowCenter(CONFIG.SERVER + 'Client/index.html#!/frmViewRequest/' + id + '/' + companyCode + '/' + keyMaster + '/' + isOwner + '/' + isDataOwner, '', 1200, 700);
                // $location.path('/frmViewRequest/' + id + '/' + companyCode + '/' + keyMaster + '/' + isOwner + '/' + isDataOwner);
            };

            //$scope.openRequest = function (id, companyCode, keyMaster, isOwner, isDataOwner, requestType) {
            //    //$location.path('/frmViewRequest/' + id + '/' + companyCode+'/' + keyMaster + '/' + isOwner + '/' + isDataOwner );
            //    var x = 'index.html#!' + '/frmViewRequest/' + id + '/' + companyCode + '/' + keyMaster + '/' + isOwner + '/' + isDataOwner + '?type=1';
            //    //alert('id=' + id + 'companyCode=' + companyCode + 'keyMaster=' + keyMaster + 'isDataOwner=' + isDataOwner + 'isOwner=' + isOwner + 'requestType=' + requestType);
            //    //document.write('<div><iframe src="'+x+'" width="500px" heigth="500px"></iframe></div>');
            //    // $window.open(x);
            //    //x = '<iframe src="' + x + '" width="500px" heigth="500px"></iframe>';
            //    //$scope.showAlert(x);

            //    $scope.showDialog(x);
            //};

            $scope.showAlert = function (ev) {
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title('Title')
                        .textContent(ev)
                        .ok('ตกลง')
                );
            };


            $scope.showDialog = function (url) {
                var parentEl = angular.element(document.body);
                $mdDialog.show({
                    parent: parentEl,
                    //targetEvent: $event,
                    template:
                        '<md-dialog aria-label="List dialog">' +
                        '  <md-dialog-content>' +
                        //'    <md-list>' +
                        //'      <md-list-item ng-repeat="item in items">' +
                        //'       <p>Number {{item}}</p>' +
                        // '      ' +
                        '<iframe src="' + url + '" width="100%" height="700px"></iframe>' +
                        // '    </md-list-item></md-list>' +
                        '  </md-dialog-content>' +
                        '  <md-dialog-actions>' +
                        '    <md-button ng-click="closeDialog()" class="md-primary">' +
                        '      Close Dialog' +
                        '    </md-button>' +
                        '  </md-dialog-actions>' +
                        '</md-dialog>',
                    locals: {
                        items: $scope.items
                    },
                    controller: $scope.DialogController
                });
            };

            $scope.DialogController = function ($scope, $mdDialog, items) {
                $scope.items = items;
                $scope.closeDialog = function () {
                    $mdDialog.hide();
                };
            };








            $scope.getBoxText = function () {
                var text = '';
                var currentPath = $location.path();
                var menuItems = getToken('com.pttdigital.ess.menu')
                for (var i = 0; i < menuItems.length; i++) {
                    if (('/' + menuItems[i].url) == currentPath) {
                        text = menuItems[i].text;
                        break;
                    }
                }
                return text;
            };
            $scope.CheckMassApprove();
            $scope.GetPositionApprove();

            //Reload all request and no in box list
            $scope.onReload = function () {


                console.warn('reload');
                var deferred = $q.defer();
                setTimeout(function () {
                    deferred.resolve(true);
                }, 1000);
                return deferred.promise;
            };

            $scope.Reload = function () {

                // Reload Menu
                var args = {};
                $rootScope.$broadcast('onReloadMenu', args);
                $route.reload();
            };


            //dropdownlist position
            $scope.PositionID = '';
            $scope.PositionName = '';
            function createFilterForPosition(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    if (!x) return false;
                    var source = angular.lowercase(x.PositionID + " : " + x.PositionName);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }
            var tempResult_Position;
            $scope.init_Position = function () { }
            $scope.querySearchPosition = function (query) {
                if (!$scope.lstPositionApprove) return;
                var results = angular.copy(query ? $scope.lstPositionApprove.filter(createFilterForPosition(query)) : $scope.lstPositionApprove), deferred;
                results = results.splice(0, 30);
                return results;
            };
            $scope.selectedItemPositionChange = function (item) {
                if (angular.isDefined(item) && item != null) {
                    $scope.auto_complete.ap_pdr_Position = item.PositionName;
                    $scope.PositionID = item.PositionID;
                    $scope.PositionName = item.PositionName;

                    $scope.temp_PositionName = item.PositionName;
                    //$scope.searchPositionID = item.PositionID;
                    //$scope.searchPositionName = item.PositionName;

                    tempResult_Position = angular.copy(item);
                    $scope.filterPositionSelected(item);
                }
                else {
                    $scope.auto_complete.ap_pdr_Position = '';
                }
            };
            $scope.auto_complete = {
                ap_pdr_Position: ''
            };
            $scope.selected_position = '';
            $scope.tryToSelect_Position = function (text) {
                $scope.auto_complete.ap_pdr_Position = '';
                $scope.selected_position = null;
                $scope.PositionID = '';
                $scope.PositionName = '';
            };
            $scope.checkText_Position = function (text) {
                //if (text == '')
                $scope.auto_complete.ap_pdr_Position = $scope.temp_PositionName;
                //$scope.PositionID = $scope.searchPositionID;
                //$scope.PositionName = $scope.searchPositionName;
                //else
                //$scope.ap_pdr_Position = text;
            };

            function createFilterForRequestDocument(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    if (!x) return false;
                    var source = angular.lowercase(x.ReceipientCode);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }
            $scope.filterPositionSelected = function (item) {
                if ($scope.PositionID == "99999999") {
                    if (!$scope.documents) return;
                    $scope.data = angular.copy($scope.documents);
                    //filterArry('');
                    $scope.filterDatas('');
                }
                else {
                    if (!$scope.documents) return;
                    var myObjects = angular.copy($filter('filter')($scope.documents, { ReceipientCode: $scope.PositionID }));
                    $scope.data = angular.copy(myObjects);
                    //filterArry('');
                    $scope.filterDatas('');
                }
            };
            $scope.FilterPositionName = function (item) {
                var myObjects = angular.copy($filter('filter')($scope.lstPositionApprove, { PositionID: item }));
                $scope.fillPositionName = myObjects[0].PositionName;
            };
            //dropdownlist position

            //** Argif Smart Search filter **//
            var filterOption = "";
            $scope.searchOption = [

            ];
            $scope.setOptionFilter = function () {
                if ($scope.searchOption.length == 0) return;
                filterOption = "";
                for (var i = 0; i < $scope.searchOption.length; i++) {
                    if ($scope.searchOption[i].IsCheck == true) {
                        filterOption += $scope.searchOption[i].FlagID + "/";
                    }

                }

                //filterArry('');
                $scope.filterDatas('');

            };

            $scope.advacneOptionFilter = function (item) {
                if (filterOption == '') return true;
                var arr = item.FlagList.split('/');
                for (var i = 0; i < arr.length; i++) {
                    if (arr[i] === '') break;
                    var flag = (filterOption.search(arr[i]) !== -1);
                    if (flag) {
                        return true;
                    }
                }
                return false;
            };

            $scope.checkContainFag = function (doc, flag) {
                var arr = doc.FlagList.split('/');
                for (var i = 0; i < arr.length; i++) {
                    if (arr[i] === '') break;
                    if (arr[i] == flag.FlagID) {
                        return true;
                    }
                }
                return false;
            };

            //Pagination of nested objects in angularjs
            $scope.pagin = {
                currentPage: 1, itemPerPage: '10', numPage: 1
            };
            $scope.filterDatas = function (searchTxt) {
                if (!$scope.data || !$scope.data.length) return [];
                var datas = $filter('filter')($scope.data, searchTxt);
                datas = $filter('filter')(datas, $scope.advacneOptionFilter);

                return datas;
            };
            $scope.genDatas = function (searchTxt) {
                var datas = $scope.filterDatas(searchTxt);
                setNumOfPage(datas);
                if (datas.length > 0)
                    $scope.iDocLength = datas.length;
                else
                    $scope.iDocLength = 0;

                var res = [];

                if ($scope.pagin.numPage > 1) {
                    datas.forEach(function (value, i) {
                        var from = (parseInt($scope.pagin.currentPage) - 1) * parseInt($scope.pagin.itemPerPage);
                        var to = from + parseInt($scope.pagin.itemPerPage);

                        if (from <= i && to > i)
                            res.push(value);
                    });
                } else {
                    res = datas.slice();
                }

                return res;
            };
            var setNumOfPage = function (datas) {
                var totalItems = 0;
                if (datas) totalItems = datas.length;
                $scope.pagin.numPage = Math.ceil(totalItems / $scope.pagin.itemPerPage);
                if ($scope.pagin.numPage <= 0) $scope.pagin.numPage = 1;

                if ($scope.pagin.currentPage > $scope.pagin.numPage) $scope.pagin.currentPage = 1;
                $scope.getPages(); 

            };
            $scope.changePage = function (targetPage) {
                $scope.pagin.currentPage = angular.copy(targetPage);
            };
            
            $scope.changePageBack = function (targetPage) {
              var _i = (($scope.view_pages_setting*$scope.view_pages_set) - $scope.view_pages_setting + 1); 
                if(targetPage < _i){
                 $scope.view_pages_set = $scope.view_pages_set  - 1;
                 $scope.pagin.currentPage =  angular.copy(targetPage);
                 $scope.getPages();
                 }else{
                     $scope.pagin.currentPage = angular.copy(targetPage);
                }
            };
            $scope.changePageNext = function (targetPage) {
                var _i  =(($scope.view_pages_setting* $scope.view_pages_set)); 
                if(targetPage > _i){
                 $scope.nextStep();
                 }else{
                     $scope.pagin.currentPage = angular.copy(targetPage);
                }
            };
            $scope.pages = [];
            $scope.view_pages_setting = 10;
            $scope.view_pages_set = 1;
            $scope.getPages = function () {
                $scope.pages = [];
                var i_set = (($scope.view_pages_setting*$scope.view_pages_set) - $scope.view_pages_setting + 1); 
                if(i_set > $scope.pagin.numPage) i_set = $scope.pagin.numPage;
                for (var i = i_set; i <= $scope.pagin.numPage; i++) {
                    $scope.pages.push((i));
                    if(i == $scope.view_pages_setting*$scope.view_pages_set) { i =  $scope.pagin.numPage;}
                }
            };

            $scope.nextStep = function () {
               $scope.view_pages_set = $scope.view_pages_set + 1;
                $scope.pagin.currentPage =(($scope.view_pages_setting* $scope.view_pages_set) - $scope.view_pages_setting + 1); 
               $scope.getPages();
            }
            $scope.backStep = function () {
              $scope.view_pages_set = $scope.view_pages_set  - 1;
               $scope.pagin.currentPage =(($scope.view_pages_setting* $scope.view_pages_set) - $scope.view_pages_setting + 1); 
               $scope.getPages();
            }
        }]);

    //#### OTHERS FUNCTION ### END

})();
