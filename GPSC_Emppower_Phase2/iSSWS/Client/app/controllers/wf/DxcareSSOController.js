﻿(function () {
    angular.module('ESSMobile')
        .controller('DxCareSSOController', ['$rootScope', '$route', '$scope', '$http', '$routeParams', '$location', '$filter', '$q', 'CONFIG', '$window', '$mdDialog', '$interval', function ($rootScope, $route, $scope, $http, $routeParams, $location, $filter, $q, CONFIG, $window, $mdDialog, $interval) {

         
        
            $scope.init = function () {
        
                nevigateToDxcare();

            }

            function nevigateToDxcare(){
                var URL = CONFIG.SERVER + 'workflow/GenerateAuthenticationForDxCareSSO';
                var oRequestParameter = { InputParameter: { }, CurrentEmployee: getToken(CONFIG.USER) };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    window.open(response.data, '_blank');
                }, function errorCallback(response) {
                    console.log('error IsAccountingRole.', response);
                });
            }
          

    }]);

    //#### OTHERS FUNCTION ### END

})();
