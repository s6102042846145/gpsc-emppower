﻿(function () {
    angular.module('ESSMobile')
        .controller('UserRoleSettingEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$timeout', '$q', '$log', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $timeout, $q, $log, $mdDialog) {

            $scope.content.isShowHeader = true;
            $scope.content.isShowBackIcon = true;
            $scope.Textcategory = 'ACCOUNT_SETTING';
            $scope.content.Header = $scope.Text['APPLICATION'].USER_ROLE_SETTNG;
            $scope.Textcategory2 = 'SYSTEM';

            $scope.requestType = $routeParams.id;
            $scope.formData = { searchKeyword: '' };
            $scope.data = {};
            $scope.oUserRole = {
                EmployeeID: '',
                CompanyCode: '',
                EmployeeName: '',
                UserRoleResponse: []
            };
            $scope.UserRoleResponse = {
                EmployeeID: '',
                CompanyCode: '',
                UserRole: '',
                ResponseType: '',
                ResponseCode: '',
                ResponseCompanyCode: '',
                searchResponseCompanyText: ''
            };

            $scope.init = function () {
                $scope.GetCompany();
            }

            $scope.GetCompany = function () {
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };
                var URL = CONFIG.SERVER + 'Share/GetCompanyList/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.CompanyList = response.data;
                    if ($scope.itemKey && $scope.itemKey != 'null') {
                        $scope.oUserRole.EmployeeID = $scope.itemKey.split("&")[0];
                        $scope.oUserRole.CompanyCode = $scope.itemKey.split("&")[1];
                        $scope.checkTextCompany($scope.oUserRole.CompanyCode);
                        $scope.GetEmployeeName($scope.oUserRole.EmployeeID);
                    }
                    else {
                        $scope.GetRole();
                    }
                }, function errorCallback(response) {
                    // Error
                    console.log('error ProjectSettingEditorController.', response);
                });
            }

            //'Employee/INFOTYPE0001Get'
            $scope.GetEmployeeName = function (EmployeeID) {
                $scope.employeeDataList = null;
                var URL = CONFIG.SERVER + 'Employee/INFOTYPE0001Get';
                var oRequestParameter = { InputParameter: { "EmployeeID": EmployeeID }, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    if ($scope.oUserRole.EmployeeID) {
                        $scope.GetRole();
                        $scope.oUserRole.EmployeeName = response.data.Name;
                        $scope.oUserRole.searchEmployeeText = $scope.oUserRole.EmployeeID + ' : ' + response.data.Name;
                    }
                }, function errorCallback(response) {
                    console.log('error GetEmployees list', response);
                });
            }

            $scope.GetEmployees = function () {
                $scope.employeeDataList = null;
                var URL = CONFIG.SERVER + 'HRTR/GetAllEmployeeNameForUserRole';
                var oRequestParameter = { InputParameter: { "CompanyCode": $scope.oUserRole.CompanyCode }, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.employeeDataList = response.data;
                }, function errorCallback(response) {
                    console.log('error GetEmployees list', response);
                });
            }

            $scope.GetRole = function () {
                var URL = CONFIG.SERVER + 'workflow/GetAllRole';
                var oRequestParameter = { InputParameter: { "CompanyCode": $scope.oUserRole.CompanyCode }, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.roleList = response.data;
                    $scope.GetResponseType();
                }, function errorCallback(response) {
                    console.log('error GetRole list', response);
                });
            }

            $scope.GetResponseType = function () {
                var URL = CONFIG.SERVER + 'workflow/GetAllUserRoleResponseType';
                var oRequestParameter = { InputParameter: { "CompanyCode": $scope.oUserRole.CompanyCode }, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.userRoleResponseTypeList = response.data;
                    if ($scope.oUserRole.EmployeeID) {
                        $scope.GetUserRoleResponse($scope.oUserRole.EmployeeID, $scope.oUserRole.CompanyCode);
                    }
                    else{
                        $scope.ready = true;
                    }
                }, function errorCallback(response) {
                    console.log('error GetResponseType list', response);
                });
            }

            $scope.GetUserRoleResponse = function (EmployeeID, CompanyCode) {
                var URL = CONFIG.SERVER + 'workflow/GetUserRoleResponse';
                var oRequestParameter = { InputParameter: { "EmployeeID": EmployeeID, "CompanyCode": CompanyCode }, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    for (var i = 0; i < response.data.length; i++) {
                        $scope.oUserRole.UserRoleResponse.push(angular.copy($scope.UserRoleResponse));
                        $scope.checkTextResponseCompany(response.data[i].ResponseCompanyCode, i);
                        $scope.checkTextRole(response.data[i].UserRole, i);
                        $scope.checkTextResponseType(response.data[i].ResponseType, i);
                        $scope.oUserRole.UserRoleResponse[i].ResponseCode = response.data[i].ResponseCode;
                    }
                    $scope.ready = true;
                }, function errorCallback(response) {
                    console.log('error GetUserRoleResponse list', response);
                });
            }

            $scope.addRow = function (ev) {
                $scope.oUserRole.UserRoleResponse.push(angular.copy($scope.UserRoleResponse));
            };

            $scope.removeRow = function (index) {
                $scope.oUserRole.UserRoleResponse.splice(index, 1);
            };

            //onsave
            $scope.SaveData = function (ev) {
                var valid = true;
                if (!$scope.oUserRole.EmployeeID) valid = false;
                else if (!$scope.oUserRole.CompanyCode) valid = false;
                else {
                    for (var i = 0; i < $scope.oUserRole.UserRoleResponse.length; i++) {
                        if (!$scope.oUserRole.UserRoleResponse[i]
                            || !$scope.oUserRole.UserRoleResponse[i].UserRole
                            || !$scope.oUserRole.UserRoleResponse[i].ResponseType
                            || !$scope.oUserRole.UserRoleResponse[i].ResponseCode
                            || !$scope.oUserRole.UserRoleResponse[i].ResponseCompanyCode
                            ) {
                            valid = false;
                        }
                        $scope.oUserRole.UserRoleResponse[i].EmployeeID = $scope.oUserRole.EmployeeID;
                        $scope.oUserRole.UserRoleResponse[i].CompanyCode = $scope.oUserRole.CompanyCode;
                    }
                }

                if (!valid) {
                    $scope.HasError = true;
                    $scope.ErrorText = $scope.Text['ACCOUNT_SETTING'].PLEASE_INSERT_ALL_FIELDS;
                }
                else {
                    var oRequestParameter = { InputParameter: { "UserRoleResponse": $scope.oUserRole.UserRoleResponse }, CurrentEmployee: $scope.employeeData, Requestor: getToken(CONFIG.USER) }

                    var URL = CONFIG.SERVER + 'workflow/SaveUserRoleResponse/';
                    console.log(URL);
                    $scope.HasError = false;
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                .textContent($scope.Text['ACCOUNT_SETTING'].GO_BACK)
                                .ariaLabel($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                .targetEvent(ev));
                        $location.path('/frmViewContent/405');

                    }, function errorCallback(response) {
                        // Error
                        console.log('error ProjectSettingEditorController Save.', response);
                    });
                }
            };

            //auto complete Company 
            $scope.querySearchCompany = function (query) {
                var results = angular.copy(query ? $scope.CompanyList.filter(createFilterForCompany(query)) : $scope.CompanyList), deferred;
                results = results.splice(0, 30);
                if ($scope.simulateQuery) {
                    deferred = $q.defer();
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 1, false);
                    return deferred.promise;
                } else {
                    return results;
                }
            };

            $scope.selectedItemCompanyChange = function (item) {
                if (angular.isDefined(item)) {
                    $scope.oUserRole.CompanyCode = item.CompanyCode;
                    $scope.oUserRole.searchCompanyText = item.Name + ' : ' + item.FullNameEN;
                    if (!tempResultCompany || item.CompanyCode != tempResultCompany.CompanyCode) $scope.GetEmployees();
                    tempResultCompany = item;

                }
            };

            function createFilterForCompany(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    var source = angular.lowercase(x.Name + ' : ' + x.FullNameEN);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }

            var tempResultCompany;
            $scope.tryToSelectCompany = function (item) {
                item.searchCompanyText = "";
            }

            $scope.checkTextCompany = function (text) {
                var result = null;
                if (text) {
                    for (var i = 0; i < $scope.CompanyList.length; i++) {
                        if ($scope.CompanyList[i].CompanyCode == text || ($scope.CompanyList[i].Name + ' : ' + $scope.CompanyList[i].FullNameEN) == text) {
                            result = $scope.CompanyList[i];
                            break;
                        }
                    }
                }
                if (result) {
                    $scope.selectedItemCompanyChange(result);
                } else if (tempResultCompany) {
                    $scope.selectedItemCompanyChange(tempResultCompany);
                }
            }

            //auto complete Response Company 
            $scope.querySearchResponseCompany = function (query) {
                var results = angular.copy(query ? $scope.CompanyList.filter(createFilterForCompany(query)) : $scope.CompanyList), deferred;
                results = results.splice(0, 30);
                if ($scope.simulateQuery) {
                    deferred = $q.defer();
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 1, false);
                    return deferred.promise;
                } else {
                    return results;
                }
            };

            $scope.selectedItemResponseCompanyChange = function (item, index) {
                if (angular.isDefined(item)) {
                    $scope.oUserRole.UserRoleResponse[index].ResponseCompanyCode = item.CompanyCode;
                    $scope.oUserRole.UserRoleResponse[index].searchResponseCompanyText = item.Name + ' : ' + item.FullNameEN;
                    tempResultResponseCompany = item;

                }
            };

            var tempResultResponseCompany;
            $scope.tryToSelectResponseCompany = function (item) {
                item.searchResponseCompanyText = "";
            }

            $scope.checkTextResponseCompany = function (text, index) {
                var result = null;
                if (text) {
                    for (var i = 0; i < $scope.CompanyList.length; i++) {
                        if ($scope.CompanyList[i].CompanyCode == text || ($scope.CompanyList[i].Name + ' : ' + $scope.CompanyList[i].FullNameEN) == text) {
                            result = $scope.CompanyList[i];
                            break;
                        }
                    }
                }
                if (result) {
                    $scope.selectedItemResponseCompanyChange(result, index);
                } else if (tempResultResponseCompany) {
                    $scope.selectedItemResponseCompanyChange(tempResultResponseCompany, index);
                }
            }

            //auto complete Employee 
            $scope.querySearchEmployee = function (query) {
                var results = angular.copy(query ? $scope.employeeDataList.filter(createFilterForEmployee(query)) : $scope.employeeDataList), deferred;
                results = results.splice(0, 30);
                if ($scope.simulateQuery) {
                    deferred = $q.defer();
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 1, false);
                    return deferred.promise;
                } else {
                    return results;
                }
            };

            $scope.selectedItemEmployeeChange = function (item) {
                if (angular.isDefined(item)) {
                    $scope.oUserRole.EmployeeID = item.EmployeeID;
                    $scope.oUserRole.EmployeeName = item.Name;
                    $scope.oUserRole.searchEmployeeText = item.EmployeeID + ' : ' + item.Name;
                    tempResultEmployee = item;
                }
            };

            function createFilterForEmployee(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    var source = angular.lowercase(x.EmployeeID + ' : ' + x.Name);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }

            var tempResultEmployee;
            $scope.tryToSelectEmployee = function (item) {
                item.searchEmployeeText = "";
            }

            $scope.checkTextEmployee = function (text) {
                var result = null;
                if (text) {
                    for (var i = 0; i < $scope.employeeDataList.length; i++) {
                        if ($scope.employeeDataList[i].EmployeeID == text || ($scope.employeeDataList[i].EmployeeID + ' : ' + $scope.employeeDataList[i].Name) == text) {
                            result = $scope.employeeDataList[i];
                            break;
                        }
                    }
                }
                if (result) {
                    $scope.selectedItemEmployeeChange(result);
                } else if (tempResultEmployee) {
                    $scope.selectedItemEmployeeChange(tempResultEmployee);
                }
            }

            //auto complete Role 
            $scope.querySearchRole = function (query) {
                var results = angular.copy(query ? $scope.roleList.filter(createFilterForRole(query)) : $scope.roleList), deferred;
                results = results.splice(0, 30);
                if ($scope.simulateQuery) {
                    deferred = $q.defer();
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 1, false);
                    return deferred.promise;
                } else {
                    return results;
                }
            };

            $scope.selectedItemRoleChange = function (item, index) {
                if (angular.isDefined(item)) {
                    $scope.oUserRole.UserRoleResponse[index].UserRole = item.Role;
                    $scope.oUserRole.UserRoleResponse[index].searchRoleText = item.Description;
                    tempResultRole = item;

                }
            };

            function createFilterForRole(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    var source = angular.lowercase(x.Description);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }

            var tempResultRole;
            $scope.tryToSelectRole = function (item) {
                item.searchRoleText = "";
            }

            $scope.checkTextRole = function (text, index) {
                var result = null;
                if (text) {
                    for (var i = 0; i < $scope.roleList.length; i++) {
                        if ($scope.roleList[i].Role == text || $scope.roleList[i].Description == text) {
                            result = $scope.roleList[i];
                            break;
                        }
                    }
                }
                if (result) {
                    $scope.selectedItemRoleChange(result, index);
                } else if (tempResultRole) {
                    $scope.selectedItemRoleChange(tempResultRole, index);
                }
            }

            //auto complete Response Type 
            $scope.querySearchResponseType = function (query) {
                var results = angular.copy(query ? $scope.userRoleResponseTypeList.filter(createFilterForResponseType(query)) : $scope.userRoleResponseTypeList), deferred;
                results = results.splice(0, 30);
                if ($scope.simulateQuery) {
                    deferred = $q.defer();
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 1, false);
                    return deferred.promise;
                } else {
                    return results;
                }
            };

            $scope.selectedItemResponseTypeChange = function (item, index) {
                if (angular.isDefined(item)) {
                    $scope.oUserRole.UserRoleResponse[index].ResponseType = item.ResponseType;
                    $scope.oUserRole.UserRoleResponse[index].searchResponseTypeText = item.Description;
                    tempResultResponseType = item;

                }
            };

            function createFilterForResponseType(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    var source = angular.lowercase(x.Description);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }

            var tempResultResponseType;
            $scope.tryToSelectResponseType = function (item) {
                item.searchResponseTypeText = "";
            }

            $scope.checkTextResponseType = function (text, index) {
                var result = null;
                if (text) {
                    for (var i = 0; i < $scope.userRoleResponseTypeList.length; i++) {
                        if ($scope.userRoleResponseTypeList[i].ResponseType == text || $scope.userRoleResponseTypeList[i].Description == text) {
                            result = $scope.userRoleResponseTypeList[i];
                            break;
                        }
                    }
                }
                if (result) {
                    $scope.selectedItemResponseTypeChange(result, index);
                } else if (tempResultResponseType) {
                    $scope.selectedItemResponseTypeChange(tempResultResponseType, index);
                }
            }
        }]);
})();