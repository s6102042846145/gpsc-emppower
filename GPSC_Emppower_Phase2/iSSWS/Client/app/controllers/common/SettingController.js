﻿(function () {
angular.module('ESSMobile')
    .controller('SettingController', ['$rootScope', '$route','$scope', '$http', '$routeParams', '$location', 'CONFIG','$timeout', function ($rootScope,$route,$scope, $http, $routeParams, $location, CONFIG, $timeout) {


        var oEmployeeData = getToken(CONFIG.USER);
        $scope.isReceive;
        $scope.status = {
            settingL: false
        };

        $scope.Textcategory = 'SETTING';

        $scope.init = function () {
            init();
        }
        $scope.languageChange = function () {

            SaveUserSetting("lgSeting");
        }
        $scope.emailStatusChange = function () {
            if ($scope.status.settingL == true) return;
            $scope.status.settingL = true;
            SaveUserSetting("");
            
          
        }
        $scope.changeAll = function () {
            SaveUserSetting("lgSeting");

        }


        function init() {
            $scope.oLanguageSelected = oEmployeeData.Language;
            $scope.isReceive = oEmployeeData.ReceiveMail;
        }
        function SaveUserSetting(settingType)
        {
           
            if ($scope.oLanguageSelected != "") {

                var URL = CONFIG.SERVER + 'workflow/SaveUserSetting';
                var oRequestParameter = { InputParameter: { "Language": $scope.oLanguageSelected, "ReceiveMail": $scope.isReceive }, CurrentEmployee: getToken(CONFIG.USER) };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                    //data: param
                   
                }).then(function successCallback(response) {
                    // Success
                    if (settingType == "lgSeting") {
                        $scope.changeLanguage($scope.oLanguageSelected);
                    }
                    $timeout(function () {
                        $scope.status.settingL = false;
                    }, 1000);
                    
                }, function errorCallback(response) {
                    $timeout(function () {
                        $scope.status.settingL = false;
                    }, 1000);
                 
                    // Error
                    console.log('error SettingController.', response);
                });
            }
        }



      
    }]);
})();