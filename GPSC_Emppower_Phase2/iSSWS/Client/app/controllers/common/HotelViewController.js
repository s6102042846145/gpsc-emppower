﻿(function () {
    angular.module('ESSMobile')
        .controller('HotelViewController', ['$scope', '$http', '$routeParams', '$location', 'CONFIG', function ($scope, $http, $routeParams, $location, CONFIG) {

            $scope.hotelList = [
                //{
                //    EmployeeID: '245801115',
                //    Name: 'นางจิราวรรณ สงวนสิน',
                //    BeginDate: '15/12/2016',
                //    EndDate: '20/12/2016',
                //    BookingDesc: 'โรงแรมแกรนด์ไฮแอท เอราวัณ'
                //},
                //{
                //    EmployeeID: '245801115',
                //    Name: 'นางจิราวรรณ สงวนสิน',
                //    BeginDate: '21/12/2016',
                //    EndDate: '30/12/2016',
                //    BookingDesc: 'โรงแรมแกรนด์ไฮแอท เอราวัณ'
                //}

            ];
            $scope.column_header = [
                $scope.Text['ACCOUNT_SETTING']['EMPLOYEE_ID'], $scope.Text['EXPENSE']['FULLNAME'], $scope.Text['EXPENSE']['DATEFROM'], $scope.Text['EXPENSE']['DATETO'], $scope.Text['ACCOUNT_SETTING']['DETAIL'],
            ];

            $scope.init = function () {

                getGetHotelViewList();
            }

            function getGetHotelViewList() {
                var URL = CONFIG.SERVER + 'HRTR/GetHotelView';
                var oRequestParameter = { InputParameter: { REQUESTNO: $scope.referRequestNo, TYPE: $scope.TRAVELMODE }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor }
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    if(response.data){
                        $scope.hotelList = response.data;
                    }
                    


                }, function errorCallback(response) {
                    // Error
                    console.log('error AirFareViewController InitialConfig.', response);
                    $scope.setBlockAction(false, "");
                });

            }


        }]);
})();