﻿(function () {
    angular.module('ESSMobile')
        .controller('MenuController', ['$rootScope', '$scope', '$http', '$location', 'CONFIG', function ($rootScope, $scope, $http, $location, CONFIG) {


            var lstExpandedMenu = [];
            $scope.isActiveFastButton = false;

            $scope.toggleFastButton = function () {
                $scope.isActiveFastButton = !$scope.isActiveFastButton;
            };


            $scope.loadMenu = function () {
                var menuList = (getToken('com.pttdigital.ess.menu'));
                if (menuList == null) {
                    menuList = [];
                }
                // set group class
                var groupClass = 0;
                for (var i = 0; i < menuList.length; i++) {
                    //if (menuList[i].isHeader) {
                    //    groupClass++;
                    //}
                    //menuList[i].menuGroupClass = 'mm' + groupClass;
                    if (lstExpandedMenu.indexOf(menuList[i].groupID) <= -1) {
                        menuList[i].isShow = false;
                    }
                    else {
                        menuList[i].isShow = true;
                    }
                }
                // set initial active
                var newLocation = $location.path();
                var selectedGroupID = '-1';
                var selectedIndex = -1;
                for (var i = 0; i < menuList.length; i++) {
                    if ('/' + menuList[i].url == newLocation || (newLocation == '/frmViewContent/0' && i == 0)) {
                        selectedGroupID = menuList[i].groupID;
                        selectedIndex = i;
                        break;
                    }
                }
                if (selectedGroupID != '-1') {
                    for (var i = 0; i < menuList.length; i++) {
                        menuList[i].isActive = (menuList[i].groupID == selectedGroupID);
                        menuList[i].isSelected = false;
                    }
                    menuList[selectedIndex].isSelected = true;
                }
                $rootScope.menus = menuList;
           
                window.localStorage.removeItem('com.pttdigital.ess.menu');
                window.localStorage.setItem('com.pttdigital.ess.menu', JSON.stringify($rootScope.menus));
            };

            $scope.loadMenu();



            var onReloadMenuFunction = function (isReloadAfterFinish) {
              
                var URL = CONFIG.SERVER + 'workflow/GetMenuItem';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    // Success
                    var menuList = response.data;
             
                    // set group class
                    var groupClass = 0;
                    for (var i = 0; i < menuList.length; i++) {
                        //if (menuList[i].isHeader) {
                        //    groupClass++;
                        //}
                        //menuList[i].menuGroupClass = 'mm' + groupClass;
                        if (lstExpandedMenu.indexOf(menuList[i].groupID) <= -1)
                        {
                            menuList[i].isShow = false;
                        }
                        else
                        {
                            menuList[i].isShow = true;
                        }
                    }
                    // set initial active
                    var newLocation = $location.path();
                    var selectedGroupID = '-1';
                    var selectedIndex = -1;
                    for (var i = 0; i < menuList.length; i++) {
                        if ('/' + menuList[i].url == newLocation || (newLocation == '/frmViewContent/0' && i == 0)) {
                            selectedGroupID = menuList[i].groupID;
                            selectedIndex = i;
                            break;
                        }
                    }
                    // find old selected menu
                    if (selectedGroupID == '-1') {
                        for (var i = 0; i < $rootScope.menus.length; i++) {
                            if ($rootScope.menus[i].isSelected) {
                                selectedGroupID = $rootScope.menus[i].groupID;
                                selectedIndex = i;
                                break;
                            }
                        }
                    }
                    if (selectedGroupID != '-1') {
                        for (var i = 0; i < menuList.length; i++) {
                            menuList[i].isActive = (menuList[i].groupID == selectedGroupID);
                            menuList[i].isSelected = false;
                        }
                        if (selectedIndex >= 0 && selectedIndex < menuList.length) {
                            menuList[selectedIndex].isSelected = true;
                        }
                    }

                    // onResetBadge();


                    $rootScope.menus = menuList;


                    window.localStorage.removeItem(CONFIG.MENU);
                    window.localStorage.setItem(CONFIG.MENU, JSON.stringify($rootScope.menus));

                    if (typeof (isReloadAfterFinish) != 'undefined' && isReloadAfterFinish) {
                        
                        $route.reload();
                    }

                }, function errorCallback(response) {
                    // Error
                    console.log('error reload menu.', response);

                });
            };
            // send function to global
            reloadFunction = onReloadMenuFunction;
            $scope.$on('onReloadMenu', function (event, args) {
                onReloadMenuFunction(false);
            });

            /* route change event */
            $scope.$on('$locationChangeStart', function (event) {

            });
            $scope.$on('$locationChangeSuccess', function (event) {
                var newLocation = $location.path();
                var menuList = $rootScope.menus;
                var selectedGroupID = '-1';
                var selectedIndex = -1;
                for (var i = 0; i < menuList.length; i++) {
                    if ('/' + menuList[i].url == newLocation) {
                        selectedGroupID = menuList[i].groupID;
                        selectedIndex = i;
                        break;
                    }
                }
                //console.log('SELECTED GROUP CLASS', selectedGroupID);
                if (selectedGroupID != '-1') {
                    for (var i = 0; i < menuList.length; i++) {
                        menuList[i].isActive = (menuList[i].groupID == selectedGroupID);
                        menuList[i].isSelected = false;
                    }
                    menuList[selectedIndex].isSelected = true;
                    $rootScope.menus = menuList;
                    //console.log('set new active menu');
                }
            });

            $scope.clickMenu = function (sender, url) {
                //console.log("Click Menu");
                console.log(url);
                closeMenu(sender);
                // Reload Menu
                var args = {};
                $rootScope.$broadcast('onReloadMenu', args);
                // Redirect
                $location.path(url);
            };

            $scope.clickFastButton = function (url) {
                // Redirect
                $location.path('frmViewContent/' + url);
                $scope.isActiveFastButton = false;
            };

            var TempMenu_select = 0;

            $scope.clickHeadMenu = function (sender) {
                //console.log("Click Menu");
                //closeMenu(sender);
                //toggleMenu(sender);
                // Reload Menu
                //var args = {};
                //$rootScope.$broadcast('onReloadMenu', args);

                var selectedGroupID = sender.menu.groupID;
                var bIsShow = false;

                for (var i = 0; i < $rootScope.menus.length; i++) {

                    //console.log($rootScope.menus.length, "$rootScope.menus.length");

     
                    //console.log("TempMenu_select", TempMenu_select);
                    $rootScope.menus[i].isShow = false;
                    bIsShow = $rootScope.menus[i].isShow;
                    

                    if ( (selectedGroupID == $rootScope.menus[i].groupID) ) {
                        $rootScope.menus[i].isShow = !$rootScope.menus[i].isShow;
                        //console.log($rootScope.menus[i].isShow,!$rootScope.menus[i].isShow);
                        bIsShow = $rootScope.menus[i].isShow;

                        //console.log("Select set TempMenu_select", TempMenu_select);
                        TempMenu_select = selectedGroupID;
                    }
                    
                }


                if (bIsShow) {
                    if (lstExpandedMenu.indexOf(selectedGroupID) <= -1) {
                        lstExpandedMenu.push(selectedGroupID);
                    }
                }
                else {
                    var index = lstExpandedMenu.indexOf(selectedGroupID);
                    lstExpandedMenu.splice(index, 1);
                }

            };


        }]);
})();

function closeMenu(obj) {
    $("#menuSection").removeClass("navActive");
    $("#MenuButtonIcon").removeClass("faa-horizontal animated");
    $("#MenuButtonIcon").removeClass("eat");

};


