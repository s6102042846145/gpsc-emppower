﻿(function () {
angular.module('ESSMobile')
    .controller('MainController', ['$scope', '$http', '$routeParams', '$location', '$sce', 'CONFIG', function ($scope, $http, $routeParams, $location, $sce, CONFIG) {
        $scope.content = {};
        $scope.contentId = $routeParams.id;
        $scope.content.isShowBackIcon = false;

        /***** System Text Description *****/
        $scope.setSystemText = function () {
            // Set system text here ...
            $scope.content.Header = $scope.Text['SYSTEM'].ANNOUNCEMENT;
        };
        $scope.$on("systemTextReady", function (event, args) {
            $scope.setSystemText();
        });
        if (!angular.isUndefined($scope.Text['SYSTEM'])) {
            $scope.setSystemText();
        }
        /***** !System Text Description *****/


        $scope.renderHtml = function (html_code) {
            return $sce.trustAsHtml(html_code);
        };



    }]);
})();