﻿(function () {
    angular.module('ESSMobile')
        .controller('smartCreateTravelCtrl', ['$scope', '$http', 'CONFIG', '$routeParams', '$location', '$filter', '$mdDialog', '$mdMenu', '$q', '$timeout',
            function ($scope, $http,CONFIG, $routeParams, $location, $filter, $mdDialog, $mdMenu, $q, $timeout) {
                $scope.DateNow = new Date();
                $scope.getFormatDate = function () {
                    return moment();
                }
                $scope.data = {
                    TravelPlaceList: [],
                    TravelType: [],
                }
                $scope.InputFormTravel = {
                    LocationName: '',
                    LocationCode: '',
                    BeginDateEndate: { startDate: new Date(), endDate: new Date() },
                    BeginDate: null,
                    EndDate: null,
                    TravelTypeID: null
                }
                $scope.changeDate = function () {
                    //var BeginDateEndate_a = $scope.InputFormTravel.BeginDateEndate.split(" - ");
                    $scope.InputFormTravel.BeginDate = $filter('date')(moment(BeginDateEndate_a[0], "DD/MM/YYYY").toDate(), 'yyyy-MM-ddT00:00:00');
                    //$scope.InputFormTravel.EndDate = $filter('date')(moment(BeginDateEndate_a[1], "DD/MM/YYYY").toDate(), 'yyyy-MM-ddT00:00:00');

                    $scope.InputFormTravel.BeginDate = $filter('date')($scope.InputFormTravel.BeginDateEndate.startDate, 'yyyy-MM-ddT00:00:00'); 
                    $scope.InputFormTravel.EndDate = $filter('date')($scope.InputFormTravel.BeginDateEndate.endDate, 'yyyy-MM-ddT00:00:00'); 
                }
                /*Date control*/
                //function initDate() {
                //    $('input[name="datefilter"]').daterangepicker({
                //        autoUpdateInput: true,
                //        "startDate": new Date(),
                //        "endDate": new Date(),
                //        locale: {
                //            cancelLabel: 'Clear',
                //            format: 'DD/MM/YYYY'
                //        }
                //    });

                //    var beginDate = moment().format('MM/DD/YYYY');
                //    var endDate = moment().format('MM/DD/YYYY');
                //    $scope.InputFormTravel.BeginDateEndate = beginDate + ' - ' + endDate;
                //    $scope.changeDate();
                //}
                //initDate();

                //$('input[name="datefilter"]').on('apply.daterangepicker', function (ev, picker) {
                //    $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
                //});

                //$('input[name="datefilter"]').on('cancel.daterangepicker', function (ev, picker) {
                //    $(this).val('');
                //});

                var currEmployee = getToken(CONFIG.USER);
                $scope.init = function () {
                    getAllTravelPlace();
                    GetTravelType();

                }
                $scope.init();
                function getAllTravelPlace() {
                    var URL = CONFIG.SERVER + 'HRTR/TravelSchedulePlaceSelection';
                    var oRequestParameter = { InputParameter: {}, CurrentEmployee: currEmployee, Creator: currEmployee.EmployeeID, Requestor: currEmployee.EmployeeID };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.data.TravelPlaceList = response.data;
                    },
                    function errorCallback(response) {
                        console.log('error InitialConfig.', response);
                    });
                }
                function GetTravelType() {
                    var URL = CONFIG.SERVER + 'HRTR/GetTravelType';
                    var oRequestParameter = { InputParameter: {}, CurrentEmployee: currEmployee, Creator: currEmployee, Requestor: currEmployee };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.data.TravelType = response.data;
                    },
                    function errorCallback(response) {
                        console.log('error InitialConfig.', response);
                    });
                }


                /********************  auto complete country end ******************* */
                $scope.simulateQuery = true;
                $scope.searchText = '';
                $scope.autocomplete = {
                    selectedItem: null
                }
                $scope.querySearchItem = function (query) {
                    //if ($scope.IsBudgets()) {
                    //    query = $scope.InputFormTravel.LocationName;
                    //}
                    if (!$scope.data.TravelPlaceList) return;
                    var results = query ? $scope.data.TravelPlaceList.filter(createFilterFor(query)) : $scope.data.TravelPlaceList, deferred;
                    if (results.length == 0) {
                        $scope.autocomplete.selectedItem = null;
                        $scope.InputFormTravel.LocationName = '';
                        $scope.InputFormTravel.LocationCode = '';
                        $scope.InputFormTravel.PlaceName = "";
                        $scope.InputFormTravel.DistrictCode = "";
                        $scope.InputFormTravel.DistrictName = "";
                        $scope.InputFormTravel.RegionCode = "";
                        $scope.InputFormTravel.RegionName = "";
                        $scope.InputFormTravel.ProvinceCode = "";
                        $scope.InputFormTravel.ProvinceName = "";
                        $scope.InputFormTravel.CountryCode = "";
                        $scope.InputFormTravel.CountryName = "";
                        $scope.searchText = '';
                    }
                    if ($scope.simulateQuery) {
                        deferred = $q.defer();
                        $timeout(function () { deferred.resolve(results); }, Math.random() * 50, false);
                        return deferred.promise;
                    } else {
                        return results;
                    }
                }
                $scope.selectedItemChange = function (item) {
                    if (angular.isDefined(item)) {
                        if ($scope.InputFormTravel.LocationCode != item.LocationCode) {
                            $scope.autocomplete.selectedItem = item;
                            $scope.InputFormTravel.LocationName = item.LocationName;
                            $scope.InputFormTravel.LocationCode = item.LocationCode;
                            var LocationName_a = item.LocationName.split("|");
                            var LocationCode_a = item.LocationCode.split("|");
                            $scope.InputFormTravel.CountryCode = LocationCode_a[0];
                            $scope.InputFormTravel.CountryName = LocationName_a[0];
                            $scope.InputFormTravel.RegionCode = LocationCode_a[1];
                            $scope.InputFormTravel.RegionName = LocationName_a[1];
                            $scope.InputFormTravel.ProvinceCode = LocationCode_a[2];
                            $scope.InputFormTravel.ProvinceName = LocationName_a[2];
                            $scope.InputFormTravel.DistrictCode = LocationCode_a[3];
                            $scope.InputFormTravel.DistrictName = LocationName_a[3];
                        }

                    }
                }
                function createFilterFor(query) {
                    if (!angular.isDefined(query)) return false;
                    var lowercaseQuery = angular.lowercase(query);
                    return function filterFn(item) {
                        //console.debug(item);
                        var textquery = angular.lowercase(item.LocationNameToDisplay);
                        return (textquery.indexOf(lowercaseQuery) >= 0);
                    };
                }
                var tempResult;
                $scope.tryToSelect = function (text) {
                    for (var i = 0; i < $scope.data.TravelPlaceList.length; i++) {
                        if ($scope.data.TravelPlaceList[i].LocationNameToDisplay == text) {
                            tempResult = $scope.data.TravelPlaceList[i];
                            break;
                        }
                    }
                    $scope.searchText = '';
                }
                $scope.checkText = function (text) {
                    var result = null;
                    for (var i = 0; i < $scope.data.TravelPlaceList.length; i++) {
                        if ($scope.data.TravelPlaceList[i].LocationNameToDisplay == text) {
                            result = $scope.data.TravelPlaceList[i];
                            break;
                        }
                    }
                    if (result) {
                        $scope.searchText = result.LocationNameToDisplay;
                        $scope.selectedItemChange(result);
                        $scope.autocomplete.selectedItem = result;
                    } else if (tempResult) {
                        $scope.searchText = tempResult.LocationNameToDisplay;
                        $scope.selectedItemChange(tempResult);
                        $scope.autocomplete.selectedItem = tempResult;
                    }
                }
                /********************  auto complete country end ******************* */
                $scope.CreateTravelRequest = function () {


                    $scope.InputFormTravel.BeginDate = $filter('date')(convertToDate($scope.InputFormTravel.BeginDateEndate.startDate), 'yyyy-MM-ddT00:00:00');
                    $scope.InputFormTravel.EndDate = $filter('date')(convertToDate($scope.InputFormTravel.BeginDateEndate.endDate), 'yyyy-MM-ddT00:00:00'); 

                    if ($scope.InputFormTravel.LocationCode == "") {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(false)
                                .title($scope.Text['SYSTEM']['WARNING'])
                                .textContent($scope.Text['TRAVELREQUEST']['REQUIRED_TRAVELSCHEDULEPLACE'])
                                .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                        );
                    }
                    else {
                        var Param = $scope.InputFormTravel.LocationCode + ';' + $scope.InputFormTravel.TravelTypeID + ';' + $scope.InputFormTravel.BeginDate + ';' + $scope.InputFormTravel.EndDate;
                        console.log('param', Param)
                        $scope.CreateNewWithReference('103', '', '', Param);
                    }
                }




            }])
})();