﻿(function () {
    angular.module('ESSMobile')
        .controller('actionFlow', ['$scope', '$http', 'CONFIG', '$routeParams', '$location', '$filter', '$mdDialog', '$mdMenu', '$q', '$timeout','$interval',
            function ($scope, $http, CONFIG, $routeParams, $location, $filter, $mdDialog, $mdMenu, $q, $timeout, $interval) {
              
        $scope.init = function () {
        }
        var currEmployee = getToken(CONFIG.USER);

        $scope.FlowData = {};

        var stop = $interval(function () {
            if ($scope.document) {
                GetFlowMainItemTemplate()
            } 
        }, 100);

        function GetFlowMainItemTemplate() {
            $interval.cancel(stop);
         
           
            var URL = CONFIG.SERVER + 'HRTR/GetFlowMainItemTemplate';
            var oRequestParameter = { InputParameter: { "RequestNo": $scope.document.RequestNo}, CurrentEmployee: currEmployee, Creator: currEmployee, Requestor: currEmployee };
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                $scope.FlowData = response.data;
            },
                function errorCallback(response) {
                console.log('error InitialConfig.', response);
            });
        }


    }])
})();