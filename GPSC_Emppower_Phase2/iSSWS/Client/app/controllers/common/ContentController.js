﻿(function () {
    angular.module('ESSMobile')
        .controller('FormViewContentController', ['$scope', '$http', '$routeParams', '$location', 'CONFIG', '$window', function ($scope, $http, $routeParams, $location, CONFIG, $window) {
            $scope.contentId = $routeParams.id;
            $scope.contentParam = $routeParams.itemKey;  //Add: 2016-Oct-20
            $scope.employeeData = getToken(CONFIG.USER);
            $scope.requesterData = getToken(CONFIG.USER); //AddBy: Ratchatawan W. (9 jan 2016) - Work across company
            $scope.content = {};
            $scope.content.isShowHeader = true;
            $scope.content.isShowBackIcon = true;
            $scope.content.Header = '';
            $scope.content.Param = '';
            $scope.itemKey = angular.isUndefined($routeParams.itemKey) && $routeParams.itemKey != '0' ? 'null' : $routeParams.itemKey;

            $scope.isActionInsteadOfMode = false;
            $scope.positionObj;
            $scope.positionShortText = '';
            $scope.urlImage = '';

            //$scope.actionInsteadOf = {};
            if (!(angular.isUndefined($routeParams.requesterEmployeeId) || angular.isUndefined($routeParams.requesterPositionId))) {
                // using action instead of
                $scope.employeeData.RequesterEmployeeID = $routeParams.requesterEmployeeId;
                $scope.employeeData.RequesterPositionID = $routeParams.requesterPositionId;
                $scope.requesterName = decodeURIComponent($routeParams.requesterName);
                $scope.isActionInsteadOfMode = true;

                //AddBy: Ratchatawan W. (9 jan 2016) - Work across company
                $scope.requesterData.EmployeeID = $routeParams.requesterEmployeeId;
                $scope.requesterData.PositionID = $routeParams.requesterPositionId;
                $scope.requesterData.CompanyCode = $routeParams.requesterCompanyCode;


                var URL = CONFIG.SERVER + 'Employee/INFOTYPE1000GetPosition';
                var oRequestParameter = {
                    InputParameter: { "ObjectID": $scope.requesterData.PositionID }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.positionObj = response.data;
                    if ($scope.employeeData.Language == 'TH') {
                        $scope.positionShortText = $scope.positionObj.ShortText;
                    }
                    else {
                        $scope.positionShortText = $scope.positionObj.ShortTextEn;
                    }
                }, function errorCallback(response) {
                });


                var URL = CONFIG.SERVER + 'Employee/GetImageUrl';
                var oRequestParameter = {
                    InputParameter: { "EmployeeID": $scope.requesterData.EmployeeID }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.urlImage = response.data;
                }, function errorCallback(response) {
                });

            }
            console.log('employeeData.', $scope.employeeData);
            $scope.runtime = Date.now();

            if ($scope.contentId == 0) {
                // Announcement
                $scope.contentInfo = {
                    ContentTemplate: 'common/main',
                    HaveActionInsteadOf: false
                };
            } else if ($scope.contentId == 1) {
                // Profile
                $scope.contentInfo = {
                    ContentTemplate: 'common/profile',
                    HaveActionInsteadOf: false
                };
            } else {
                var oRequestParameter = { InputParameter: { "CONTENTID": $scope.contentId, "CONTENTPARAM": $scope.contentParam }, CurrentEmployee: getToken(CONFIG.USER) }

                var URL = CONFIG.SERVER + 'workflow/GetContentSetting';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.contentInfo = response.data;
                    if ($scope.contentInfo.IsContent == true) {
                        $scope.content.Header = $scope.contentInfo.Header
                        $scope.content.ContentParam = $scope.contentParam //Add: 2016-Oct-20
                        //if (!(angular.isUndefined($routeParams.requesterEmployeeId) || angular.isUndefined($routeParams.requesterPositionId))) {
                        //    // action instead of
                        //    $scope.actionInsteadOf = $scope.ManualEmployeeList
                        //}
                    }
                    else {
                        window.open($scope.contentInfo.ContentTemplate, "_parent ");
                    }

                }, function errorCallback(response) {
                    // Error
                    console.log('error ContentController.', response);
                    $scope.contentInfo = {
                        ContentTemplate: 'common/main',
                        HaveActionInsteadOf: false
                    };

                });
            }
            $scope.ContentParam = "";
            $scope.getTemplate = function () {
                $scope.ContentParam = $scope.contentInfo.ContentParam;
                return 'views/' + $scope.contentInfo.ContentTemplate + '.html?t=' + $scope.runtime;
            };
            $scope.showListActionInsteadOf = function () {

                $location.path('/actionInsteadOf/' + $scope.contentId);
            };

            $scope.windowCenter = function (url, title, w, h) {
                console.log('window open');
                // Fixes dual-screen position                             Most browsers      Firefox
                var dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : window.screenX;
                var dualScreenTop = window.screenTop !== undefined ? window.screenTop : window.screenY;

                var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
                var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

                var systemZoom = width / window.screen.availWidth;
                var left = (width - w) / 2 / systemZoom + dualScreenLeft;
                var top = (height - h) / 2 / systemZoom + dualScreenTop;
                var newWindow = window.open(url, title, `scrollbars=yes, width=${w / systemZoom},  height=${h / systemZoom}, top=${top}, left=${left}`);

                // ลบ Element Menu ออกจาก Window open
                $(newWindow.document).ready(function () {

                    var checkJob = setInterval(function () {
                        if ($('#menuSection', newWindow.document).length > 0) {
                            $('#menuSection', newWindow.document).remove();
                            clearInterval(checkJob);
                        }
                    }, 1000);

                    var checkJob2 = setInterval(function () {
                        if ($("#PopupDelete", newWindow.document).length > 0) {
                            $("#PopupDelete", newWindow.document).remove();
                            clearInterval(checkJob2);
                        }
                    }, 1000);

                    var checkJob3 = setInterval(function () {

                        if ($("#navBarRight", newWindow.document).length > 0) {
                            $("#navBarRight", newWindow.document).remove();
                            clearInterval(checkJob3);
                        }
                    }, 1000);
                });

                if (window.focus) {
                    newWindow.focus();
                }
            };

            /* Create Document from Content */
            $scope.CreateNew = function (requestTypeId, paramValue) {
                if (angular.isUndefined($scope.content.Header) || $scope.content.Header == '') {
                    $scope.content.Header = '-';
                }
                //$routeParams = { Key1: paramValue };
                if ($scope.isActionInsteadOfMode) {
                    // has actionInsteadOf

                    //$location.path('/frmCreateRequest/' + requestTypeId + '/0/' + $routeParams.requesterEmployeeId + '/' + $routeParams.requesterPositionId + '/' + $routeParams.requesterName + '/' + +$routeParams.requesterCompanyCode + '/' + paramValue);
                    $scope.windowCenter(CONFIG.SERVER + 'Client/index.html#!/frmCreateRequest/' + requestTypeId + '/0/' + $routeParams.requesterEmployeeId + '/' + $routeParams.requesterPositionId + '/' + $routeParams.requesterName + '/' + +$routeParams.requesterCompanyCode + '/' + paramValue, '', 1200, 700);
                } else {
                    //$location.path('/frmCreateRequest/' + requestTypeId + '/0/' + paramValue);
                    $scope.windowCenter(CONFIG.SERVER + 'Client/index.html#!/frmCreateRequest/' + requestTypeId + '/0/' + paramValue, '', 1200, 700);
                }
            };

            $scope.CreateNewWithReference = function (requestTypeId, referRequestNo, itemKey, paramValue) {
                if (angular.isUndefined($scope.content.Header) || $scope.content.Header == '') {
                    $scope.content.Header = '-';
                }
                if ($scope.isActionInsteadOfMode) {
                    if (angular.isUndefined(itemKey) || itemKey == null || itemKey == '') {
                        $location.path('/frmCreateRequest/' + requestTypeId + '/0/' + referRequestNo + '/' + $routeParams.requesterEmployeeId + '/' + $routeParams.requesterPositionId + '/' + $routeParams.requesterName + '/' + $routeParams.requesterCompanyCode);
                    } else {
                        $location.path('/frmCreateRequest/' + requestTypeId + '/' + itemKey + '/' + referRequestNo + '/' + $routeParams.requesterEmployeeId + '/' + $routeParams.requesterPositionId + '/' + $routeParams.requesterName + '/' + $routeParams.requesterCompanyCode);
                    }
                }
                else {
                    if (angular.isUndefined(itemKey) || itemKey == null || itemKey == '') {
                        if (angular.isUndefined(paramValue) || paramValue == null || paramValue == '') {
                            $location.path('/frmCreateRequest/' + requestTypeId + '/0/' + referRequestNo);
                        }
                        else {
                            $location.path('/frmCreateRequest/' + requestTypeId + '/0/null/' + paramValue);
                        }
                    } else {
                        if (angular.isUndefined(paramValue) || paramValue == null || paramValue == '') {
                            $location.path('/frmCreateRequest/' + requestTypeId + '/' + itemKey + '/' + referRequestNo);
                        }
                        else {
                            $location.path('/frmCreateRequest/' + requestTypeId + '/' + itemKey + '/' + referRequestNo + '/' + paramValue);
                        }
                    }
                }
            };

            /* View Document from Content */
            $scope.ViewObj = function (RequestNo, CompanyCode) {
                console.log('RequestNo.', RequestNo);
                if ($scope.isActionInsteadOfMode) {
                    // has actionInsteadOf
                    $location.path('/frmViewRequest/' + RequestNo + '/' + CompanyCode + '/null/true/false' + '/' + $routeParams.requesterEmployeeId + '/' + $routeParams.requesterPositionId + '/' + $routeParams.requesterName);
                } else {
                    $location.path('/frmViewRequest/' + RequestNo + '/' + CompanyCode + '/null/true/false');
                }
                console.log('REDIRECT', encodeURIComponent($scope.content.Header));
            };

            /* Create Cancel Document from Content */
            $scope.DeleteObj = function (requestTypeId, Itemkey) {
                console.log('key.', Itemkey);
                if ($scope.isActionInsteadOfMode) {
                    // has actionInsteadOf
                    $location.path('/frmCreateRequest/' + requestTypeId + '/' + Itemkey + '/' + $routeParams.requesterEmployeeId + '/' + $routeParams.requesterPositionId + '/' + $routeParams.requesterName);
                } else {
                    $location.path('/frmCreateRequest/' + requestTypeId + '/' + Itemkey);
                }
                console.log('REDIRECT', encodeURIComponent($scope.content.Header));
            };

            $scope.getFileAttach = function (setId, id) {
                var URL = CONFIG.SERVER + 'workflow/GetFile/' + setId + '/' + id + '/' + $scope.employeeData.CompanyCode + '/' + $scope.employeeData.EmployeeID;
                if (typeof cordova != 'undefined') {
                    cordova.InAppBrowser.open(URL, '_system', 'location=no');
                } else {
                    window.open(URL, '_blank');
                }
            };
            $scope.getFileFromPath = function (attachment) {
                /* direct */
                var path;
                if (angular.isDefined(attachment)) {
                    if (typeof cordova != 'undefined') {
                        console.log("undefined");
                    } else if (attachment.FilePath) {

                        path = CONFIG.SERVER + 'Client/files/' + attachment.FilePath + '/' + attachment.FileName;
                        console.debug(path);
                        $window.open(path);

                    } else {
                        path = CONFIG.SERVER + 'Client/files/' + attachment;
                        console.debug(path);
                        $window.open(path);

                    }
                }
                /* !direct */
            };

        }])
        .controller('ProfileController', ['$scope', '$http', '$routeParams', '$location', 'CONFIG', function ($scope, $http, $routeParams, $location, CONFIG) {
            $scope.contentId = $routeParams.id;

            $scope.Textcategory = 'HRPAPERSONALDATA';
            $scope.content.Header = $scope.Text['HRPAPERSONALDATA']['PERSONALDATA'];

            $scope.profile = getToken(CONFIG.USER);

            $scope.setPosition = function (NewPositionID) {
                $scope.PROFILE_SETTING.POSITIONID = NewPositionID;
                window.localStorage.setItem(CONFIG.USER, JSON.stringify($scope.profile));
            };

        }])
        .controller('AnnouncementController', ['$scope', '$http', '$routeParams', '$location', '$sce', 'CONFIG', '$timeout', '$uibModal', '$window', function ($scope, $http, $routeParams, $location, $sce, CONFIG, $timeout, $uibModal, $window) {
            $scope.contentId = $routeParams.id;
            $scope.content.isShowBackIcon = false;

            /***** System Text Description *****/
            $scope.setSystemText = function () {
                // Set system text here ...
                $scope.content.Header = $scope.Text['SYSTEM'].ANNOUNCEMENT;
            };
            $scope.$on("systemTextReady", function (event, args) {
                $scope.setSystemText();
            });
            if (!angular.isUndefined($scope.Text['SYSTEM'])) {
                $scope.setSystemText();
            }
            /***** !System Text Description *****/

            $scope.announcements = getAnnouncement();
            console.log('announcement.', $scope.announcements);


            function getAnnouncement() {
                var URL = CONFIG.SERVER + 'Workflow/getAnnounement';
                var oRequestParameter = {
                    InputParameter: {},
                    CurrentEmployee: getToken(CONFIG.USER)
                };
                return $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.announcements = response.data;
                    return response;
                }, function errorCallback(response) {
                        console.error(response);
                        return response;
                    });
            }

            $scope.renderHtml = function (html_code) {
                return $sce.trustAsHtml(html_code);
            };

            $scope.profile = getToken(CONFIG.USER);
            $scope.list_slides = [];
            $scope.slides = [];
            $scope.LoadAnnouncementEmployeeAll = function () {

                var URL = CONFIG.SERVER + 'Announcement/GetAnnouncementEmployeeHome';
                $scope.employeeData = getToken(CONFIG.USER);
                $scope.requesterData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {

                    console.log(response.data);
                    //ประกาศประชาสัมพันธ์
                    if (response.data.ListPublicRelations.length > 0) {
                        var obj, i;
                        if ($scope.profile.Language == "TH") {

                            for (i = 0; i < response.data.ListPublicRelations.length; i++) {
                                if (response.data.ListPublicRelations[i].TextFile_TH) {
                                    obj = {
                                        title: response.data.ListPublicRelations[i].Title_TH,
                                        image: '/Uploads/AnnouncementPublicRelations/' + response.data.ListPublicRelations[i].TextFile_TH,
                                        link: response.data.ListPublicRelations[i].TextLink
                                    };
                                    $scope.slides.push(obj);
                                }
                            }
                        }
                        if ($scope.profile.Language == "EN") {
                            for (i = 0; i < response.data.ListPublicRelations.length; i++) {
                                if (response.data.ListPublicRelations[i].Title_EN) {
                                    obj = {
                                        title: response.data.ListPublicRelations[i].Title_EN,
                                        image: '/Uploads/AnnouncementPublicRelations/' + response.data.ListPublicRelations[i].TextFile_EN,
                                        link: response.data.ListPublicRelations[i].TextLink
                                    };
                                    $scope.slides.push(obj);
                                }
                            }
                        }
                    }

                    $scope.callAutoPicture();

                    // ประกาศข้อความ
                    $scope.list_text = response.data.ListAnnouncementText;

                    $scope.loader.enable = false;
                }, function (response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.LoadAnnouncementEmployeeAll();

            $scope.callAutoPicture = function () {

                if ($scope.slides.length > 0) {
                    $timeout(function () {

                        $(".ayodia-slider").not('.slick-initialized').slick({
                            dots: true,
                            touchMove: false,
                            autoplay: true,
                            autoplaySpeed: 3000,
                            prevArrow: `<div class='carousel-prev'><svg xmlns="http://www.w3.org/2000/svg" width="43.725" height="43.725" viewBox="0 0 43.725 43.725">
                        <path style="fill:#00ADEE;" d="M21.862,0A21.862,21.862,0,1,0,43.725,21.862,21.862,21.862,0,0,0,21.862,0Zm0,40.992a19.13,19.13,0,1,1,19.13-19.13A19.129,19.129,0,0,1,21.862,40.992Z"/>
                        <path style="fill:#00ADEE;" d="M171.316,116.8l-10.931,10.931a1.366,1.366,0,0,0,0,1.927l10.931,10.931,1.927-1.94-9.961-9.961,9.961-9.961Z" transform="translate(-146.324 -106.825)"/>
                        </svg></div>`,
                            nextArrow: `<div class='carousel-next'><svg xmlns="http://www.w3.org/2000/svg" width="43.725" height="43.725" viewBox="0 0 43.725 43.725">
                        <path style="fill:#00ADEE;" d="M21.862,0A21.862,21.862,0,1,1,0,21.862,21.862,21.862,0,0,1,21.862,0Zm0,40.992a19.13,19.13,0,1,0-19.13-19.13A19.129,19.129,0,0,0,21.862,40.992Z"/>
                        <path style="fill:#00ADEE;" d="M161.914,116.8l10.931,10.931a1.366,1.366,0,0,1,0,1.927l-10.931,10.931-1.927-1.94,9.961-9.961-9.961-9.961Z" transform="translate(-143.18 -106.825)"/>
                         </svg></div>`,
                            dotsClass: "carousel-dots"
                        });
                        $('.image-popup-no-margins').magnificPopup({
                            type: 'image',
                            closeOnContentClick: true,
                            closeBtnInside: false,
                            fixedContentPos: true,
                            mainClass: 'mfp-no-margins mfp-with-zoom',
                            image: {
                                verticalFit: true
                            },
                            zoom: {
                                enabled: true,
                                duration: 300
                            }
                        });
                        $(".hide-menu-btn").click(function () {
                            $(".ayodia-slider").slick('slickGoTo', 0);
                        });
                        $scope.$apply();



                    }, 0);
                }
            };

            $scope.count_document = 0;
            $scope.count_document_wait = 0;
            $scope.loadRequestByBoxId = function (boxid) {

                var URL = CONFIG.SERVER + 'HRPA/GetRequestByBoxId';
                $scope.employeeData = getToken(CONFIG.USER);
                $scope.requesterData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {
                        BoxId: boxid
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {

                    //console.log(response);
                    if (boxid === 101)
                        $scope.count_document = response.data;

                    if (boxid === 301)
                        $scope.count_document_wait = response.data;

                    //$scope.count_document = response.data;

                    $scope.loader.enable = false;
                }, function (response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.loadRequestByBoxId(101);
            $scope.loadRequestByBoxId(301);

            $scope.next_page = function (contentId) {

                var url = '/frmViewContent/' + contentId;
                $location.path(url);
            };

            $scope.slideText = function () {
                var offset = $("#home-notice").offset();
                if (offset) {
                    $('html, body').animate({
                        scrollTop: offset.top
                    }, 500);
                }
            };


            $scope.birthdayCardLoginPersonal = function () {

                var URL = CONFIG.SERVER + 'Employee/BirthdayCardByEmployee';
                $scope.employeeData = getToken(CONFIG.USER);
                $scope.requesterData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {

                    if (response.data.FlagBirthDay) {
                        //เพิ่ม Birthday Card 
                        var modalInstance = $uibModal.open({
                            animation: $scope.animationsEnabled,
                            templateUrl: 'openBirthDayCard.ng-popup.html',
                            controller: 'openBirthDayCardController',
                            backdrop: 'static',
                            keyboard: false,
                            size: 'lg',
                            resolve: {
                                items: function () {
                                    var data = {
                                        text: $scope.Text,
                                        dataDof: response.data,
                                        fnClose: function (model) {
                                            $scope.closeBirthdayCard(model);
                                        }
                                    };
                                    return data;
                                }
                            }
                        });
                    }
                    $scope.loader.enable = false;
                }, function (response) {
                    $scope.loader.enable = false;
                });


            };
            $scope.birthdayCardLoginPersonal();

            $scope.closeBirthdayCard = function (model) {

                var URL = CONFIG.SERVER + 'Employee/UpdateNotifyBirthdayCard';
                $scope.employeeData = getToken(CONFIG.USER);
                $scope.requesterData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {
                        Year: model.Year,
                        Month: model.Month,
                        Day: model.Day,
                        EmployeeId: model.EmployeeId
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {

                    if (response.data.FlagBirthDay) {
                        //เพิ่ม Birthday Card 
                        var modalInstance = $uibModal.open({
                            animation: $scope.animationsEnabled,
                            templateUrl: 'openBirthDayCard.ng-popup.html',
                            controller: 'openBirthDayCardController',
                            backdrop: 'static',
                            keyboard: false,
                            size: 'lg',
                            resolve: {
                                items: function () {
                                    var data = {
                                        text: $scope.Text,
                                        dataDof: response.data
                                    };
                                    return data;
                                }
                            }
                        });
                    }
                    $scope.loader.enable = false;
                }, function (response) {
                    $scope.loader.enable = false;
                });

            };

            $scope.openlink = function (link) {
                console.log(link);
                $window.open('//'+ link, '_blank');
            };

            //$scope.test_send_email = function () {

            //    var URL = CONFIG.SERVER + 'Employee/TestSendEmail';
            //    $scope.employeeData = getToken(CONFIG.USER);
            //    $scope.requesterData = getToken(CONFIG.USER);

            //    var oRequestParameter = {
            //        InputParameter: { }
            //        , CurrentEmployee: $scope.employeeData
            //        , Requestor: $scope.requesterData
            //        , Creator: getToken(CONFIG.USER)
            //    };

            //    $scope.loader.enable = true;
            //    $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {
            //        $scope.loader.enable = false;
            //    }, function (response) {
            //        $scope.loader.enable = false;
            //    });

            //};
            //$scope.test_send_email();

        }])
        .controller('openBirthDayCardController', ['items', '$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', '$uibModalInstance', function (items, $scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal, $uibModalInstance) {

            //console.log(items);

            $scope.model = items.dataDof;
            $scope.Text = items.text;
            $scope.name_birthday = items.dataDof.FullName;
            $scope.closeModal = function () {
                items.fnClose($scope.model);
                $uibModalInstance.close();
            };
        }]);
})();