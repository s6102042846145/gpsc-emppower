﻿(function () {
    angular.module('ESSMobile')
        .controller('BarProfileController', ['$scope', '$http', '$routeParams', '$location', 'CONFIG', '$interval', function ($scope, $http, $routeParams, $location, CONFIG, $interval) {

            $scope.profile = getToken(CONFIG.USER);
            $scope.navigateTo = function (url) {
                $location.path(url);
            };

            $scope.settings = {
                PositionTH: '',
                PositionEN: ''
            };


            $scope.GetOrganizationByPositionID = function (oPosition) {
                var oRequestParameter = {
                    InputParameter: {
                        "PositionID": oPosition
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'Employee/GetOrganizationByPositionID',
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.ObjINF1000 = response.data;

                    if ($scope.ObjINF1000 != null) {
                        $scope.Orgsettings.OrgUnit = $scope.ObjINF1000.ObjectID;
                        if ($scope.profile.Language == 'EN') {
                            $scope.Orgsettings.OrgUnitName = $scope.ObjINF1000.TextEn;
                            $scope.Orgsettings.OrgUnitShortText = '( ' + $scope.ObjINF1000.ShortTextEn + ' )';
                        }
                        else {
                            $scope.Orgsettings.OrgUnitName = $scope.ObjINF1000.Text;
                            $scope.Orgsettings.OrgUnitShortText = '( ' + $scope.ObjINF1000.ShortText + ' )';
                        }

                    }


                }, function errorCallback(response) {
                });
            };


            $scope.NextAnnouncementAll = function () {
                var url_next = '/frmViewContent/7013/';
                $location.path(url_next);
            };

            $scope.NextAnnouncementById = function (announcementId) {
                var url_next = '/frmViewContent/7014/' + announcementId;
                $location.path(url_next);
            };

            $scope.data_alert = 0;
            $scope.LoadDigitNotify = function () {

                var URL = CONFIG.SERVER + 'Announcement/CalculateNotifyAnnouncement';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {

                    $scope.data_alert = response.data.Notify;

                }, function (response) {
                    console.log(response);
                    $scope.loader.enable = false;
                });
            };

            $scope.list_notify = [];
            $scope.LoadNotifyAnnouncement = function () {

                var URL = CONFIG.SERVER + 'Announcement/GetAllAnnouncementMainNotifyToBell';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {

                    $scope.list_notify = response.data;

                }, function (response) {
                    console.log(response);
                    $scope.loader.enable = false;
                });
            };


            var interval_calculate_notify = $interval(function ()
            {
                $scope.LoadDigitNotify();
                $scope.LoadNotifyAnnouncement();
            }, 5000);
            //$scope.LoadDigitNotify();
            //$scope.LoadNotifyAnnouncement();

            $scope.$on('$destroy', function () {
                $interval.cancel(interval_calculate_notify);
            });


            $scope.$watch('PROFILE_SETTING.POSITIONID', function (newObj, oldObj) {
                var oSetPosition;
                if ($scope.PROFILE_SETTING.POSITIONID != '') {
                    oSetPosition = $scope.PROFILE_SETTING.POSITIONID;
                }
                else {
                    if ($scope.profile != null) {
                        oSetPosition = $scope.profile.PositionID;
                    }
                }
                if (angular.isDefined(newObj) && newObj != null) {
                    if ($scope.profile != null && $scope.profile.AllPosition) {
                        for (var i = 0; i < $scope.profile.AllPosition.length; i++) {
                            if (oSetPosition == $scope.profile.AllPosition[i].ObjectID) {
                                $scope.settings.PositionTH = $scope.profile.AllPosition[i].ShortText;
                                $scope.settings.PositionEN = $scope.profile.AllPosition[i].ShortTextEn;
                                break;
                            }
                        }
                    }
                    if (newObj != '') {
                        $scope.GetOrganizationByPositionID(newObj);
                    }
                    else {
                        $scope.Orgsettings.OrgUnit = $scope.profile.OrgUnit;
                        $scope.Orgsettings.OrgUnitName = $scope.profile.OrgUnitName;
                        $scope.Orgsettings.OrgUnitShortText = $scope.Orgsettings.OrgUnitShortText;
                    }
                }
            });


        }]);
})();