﻿(function () {
    angular.module('ESSMobile')
        .controller('RelationSettingEditorController', ['$scope', '$http', '$routeParams', '$location', 'DTOptionsBuilder', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, DTOptionsBuilder, $filter, CONFIG) {
            $scope.OSTextcategory = 'OBJECTSETTING';
            $scope.content.Header = $scope.Text[$scope.OSTextcategory].CONSOLE_HEADSUBJECT;
            $scope.ObjectType = angular.isUndefined($routeParams.itemKey) && $routeParams.itemKey != '0' ? null : $routeParams.itemKey.split('|')[0];
            $scope.CanEditRelation = $scope.ObjectType == null ? false : true;
            $scope.CanEditPeriod = false;
            $scope.ObjectTypeCanSelect = {
                val: false
            };
            $scope.ObjectIDCanSelect = {
                val: $scope.ObjectType == null ? false : true
            };
            $scope.NextObjectTypeCanSelect = {
                val: false
            };
            $scope.NextObjectIDCanSelect = {
                val: $scope.ObjectType == null ? false : true
            };
            $scope.RelationData = [];
            $scope.RelationSelectList = [];
            $scope.ObjectTypeSelectList = [];
            $scope.ObjectIDSelectList = [];
            $scope.NextObjectTypeSelectList = [];
            $scope.NextObjectIDSelectList = [];
            $scope.AllObject;

            $scope.NoSpecify = {
                val: false
            };
            $scope.OnRelationChange = function () {
                getRelationValidation($scope.RelationData.Relation);
                //$scope.ObjectTypeSelectList = $scope.AllObject['RelationValidation'][$scope.RelationData.Relation]['Object'];
                //$scope.NextObjectTypeSelectList = $scope.AllObject['RelationValidation'][$scope.RelationData.Relation]['NextObject'];
                $('[name="ObjectType"]').attr("disabled", false);
                $('[name="NextObjectType"]').attr("disabled", false); 
                //$scope.ObjectTypeCanSelect.val = true;
                //$scope.NextObjectTypeCanSelect.val = true;
            };
            $scope.OnObjectTypeChange = function () {
                //$scope.ObjectTypeSelectList = response.data["ObjectType"];
                //$scope.NextObjectTypeSelectList = response.data["NextObjectType"];
                $scope.RelationData.NextObjectType = $scope.NextObjectTypeSelectList[$scope.ObjectTypeSelectList.indexOf($scope.RelationData.ObjectType)];
                if ($scope.RelationData.Relation == 'A008') $scope.ObjectIDSelectList = $scope.ObjectDic[$scope.RelationData.ObjectType + 'A008'];
                else $scope.ObjectIDSelectList = $scope.ObjectDic[$scope.RelationData.ObjectType];
                $scope.NextObjectIDSelectList = $scope.ObjectDic[$scope.RelationData.NextObjectType];
                $scope.RelationData.ObjectID = '';
                $scope.RelationData.NextObjectID = '';
                //$scope.ObjectIDSelectList = $scope.AllObject['AllObject'][$scope.RelationData.ObjectType];
                $('[id="ObjectID"]').removeClass('hide');
                $('[id="NextObjectID"]').removeClass('hide');
            };
            $scope.OnNextObjectTypeChange = function () {
                $scope.RelationData.ObjectType = $scope.ObjectTypeSelectList[$scope.NextObjectTypeSelectList.indexOf($scope.RelationData.NextObjectType)];
                if ($scope.RelationData.Relation == 'A008') $scope.ObjectIDSelectList = $scope.ObjectDic[$scope.RelationData.ObjectType + 'A008'];
                else $scope.ObjectIDSelectList = $scope.ObjectDic[$scope.RelationData.ObjectType];
                $scope.NextObjectIDSelectList = $scope.ObjectDic[$scope.RelationData.NextObjectType];
                //$scope.NextObjectIDSelectList = $scope.AllObject['AllObject'][$scope.RelationData.NextObjectType];
                $scope.RelationData.ObjectID = '';
                $scope.RelationData.NextObjectID = '';
                $('[id="ObjectID"]').removeClass('hide');
                $('[id="NextObjectID"]').removeClass('hide');
            };
            $scope.setSelectedBeginDate = function (selectedDate) {
                $scope.RelationData.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
                $scope.getAllObjectNew();
            };
            $scope.setSelectedEndDate = function (selectedDate) {
                $scope.RelationData.EndDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
                console.log(($scope.RelationData.EndDate == '9999-12-31'));
                $scope.NoSpecify.val = ($scope.RelationData.EndDate == '9999-12-31');
            };
            $scope.onCheckBoxChange = function () {
                if ($scope.NoSpecify.val == true) {
                    $scope.RelationData.EndDate = '9999-12-31';
                } else {
                    $scope.RelationData.EndDate = $scope.RelationData.BeginDate;
                }
            }
            $scope.onClickEditSave = function () {
                var oReturn = true;
                if (angular.isDefined($scope.RelationData.ObjectID.ObjectID)) {
                    $scope.RelationData.ObjectID = $scope.RelationData.ObjectID.ObjectID;
                }
                if (angular.isDefined($scope.RelationData.NextObjectID.ObjectID)) {
                    $scope.RelationData.NextObjectID = $scope.RelationData.NextObjectID.ObjectID;
                }
                if ($scope.RelationData.Relation.trim().length <= 0) oReturn = false;
                if ($scope.RelationData.ObjectType.trim().length <= 0) oReturn = false;
                if ($scope.RelationData.ObjectID.trim().length <= 0) oReturn = false;
                if ($scope.RelationData.NextObjectType.trim().length <= 0) oReturn = false;
                if ($scope.RelationData.NextObjectID.trim().length <= 0) oReturn = false;
                if (oReturn == true) {
                    console.log($scope.RelationData);
                    // ------ update data to database -------//
                    var oRequestParameter = {
                        InputParameter: { "RelationData": $scope.RelationData }
                                                , CurrentEmployee: getToken(CONFIG.USER)
                    };
                    var URL = CONFIG.SERVER + 'Employee/UpdateINFOTYPE1001';
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        alert('COMPLETE');
                        $location.path('/frmViewContent/403/');
                    }, function errorCallback(response) {
                        // Error
                        console.log('error RequestController.', response);
                    });
                    // ------ ------------------------------------ -------//
                }
                else {
                    alert($scope.Text['OBJECTSETTING'].PLEASEINPUTALL);
                }
            };
            $scope.onClickAddSave = function () {
                var oReturn = true;
                if (angular.isDefined($scope.RelationData.ObjectID.ObjectID)) {
                    $scope.RelationData.ObjectID = $scope.RelationData.ObjectID.ObjectID;
                }
                if (angular.isDefined($scope.RelationData.NextObjectID.ObjectID)) {
                    $scope.RelationData.NextObjectID = $scope.RelationData.NextObjectID.ObjectID;
                }
                if ($scope.RelationData.Relation.trim().length <= 0) oReturn = false;
                if ($scope.RelationData.ObjectType.trim().length <= 0) oReturn = false;
                if ($scope.RelationData.ObjectID.trim().length <= 0) oReturn = false;
                if ($scope.RelationData.NextObjectType.trim().length <= 0) oReturn = false;
                if ($scope.RelationData.NextObjectID.trim().length <= 0) oReturn = false;
                if (oReturn == true) {
                    var oRequestParameterr = {
                        InputParameter: {
                            "ObjectType": $scope.RelationData.ObjectType, "ObjectID": $scope.RelationData.ObjectID, "DataObjectType": "Relation"
                            , "BeginDate": $scope.RelationData.BeginDate.toString('yyyy-MM-dd'), "EndDate": $scope.RelationData.EndDate.toString('yyyy-MM-dd')
                            , "RelationData": $scope.RelationData
                        }
                        , CurrentEmployee: getToken(CONFIG.USER)
                    };
                    var urlll = CONFIG.SERVER + 'Employee/CheckStatus';
                    $http({
                        method: 'POST',
                        url: urlll,
                        data: oRequestParameterr
                    }).then(function successCallback(response) {
                        // Success
                        console.log('eiei', response);

                        var oRequestParameter;
                        if ($scope.IsEdit == true) {
                            oRequestParameter = {
                                InputParameter: { "RelationData": $scope.RelationData, "OldRelationData": $scope.OldRelationData }
                                                , CurrentEmployee: getToken(CONFIG.USER)
                            };
                        }
                        else {
                            oRequestParameter = {
                                InputParameter: { "RelationData": $scope.RelationData }
                                                , CurrentEmployee: getToken(CONFIG.USER)
                            };
                        }
                        
                        var insertURL = CONFIG.SERVER + 'Employee/InsertINFOTYPE1001';
                        if (response.data.CanInsert == true && response.data.Duplicate == true) {
                            if (confirm($scope.Text['PERSONALADMINISTRATION'].DuplicatePeriod) == true) {
                                InsertINFOTYPE1001(oRequestParameter, insertURL);
                            }
                        }
                        else if (response.data.CanInsert == true) {
                            InsertINFOTYPE1001(oRequestParameter, insertURL);
                        }

                        else if (response.data.CanInsert == false && response.data.Duplicate == true) {
                            alert($scope.Text['PERSONALADMINISTRATION'].DuplicateRegularPosition);
                        }
                        else {
                            //$scope.RelationData.ObjectID = $scope.ObjectIDSelectList[findWithAttr($scope.ObjectIDSelectList, 'ObjectID', $scope.RelationData.ObjectID)];
                            //$scope.RelationData.NextObjectID = $scope.NextObjectIDSelectList[findWithAttr($scope.NextObjectIDSelectList, 'ObjectID', $scope.RelationData.NextObjectID)];
                            alert($scope.Text['PERSONALADMINISTRATION'].StaccatoPeriod);
                        }
                    }, function errorCallback(response) {
                        // Error
                        console.log('error RequestController.', response);
                    });
                }
                else {
                    alert($scope.Text['OBJECTSETTING'].PLEASEINPUTALL);
                }
            };
            

            $scope.getRelation =  function () {
                var oRequestParameter = {
                    InputParameter: {}
                        , CurrentEmployee: getToken(CONFIG.USER)
                };
                var url = CONFIG.SERVER + 'Employee/GetRelation';
                $http({
                    method: 'POST',
                    data: oRequestParameter,
                    url: url
                }).then(function successCallback(response) {
                    // Success
                    console.log('GetRelationSelectData.', response.data);
                    $scope.RelationSelectList = response.data;
                    $scope.getAllObjectNew();
                    if ($scope.ObjectType != null) {
                        if ($routeParams.itemKey.split('|')[8] == '000') { $scope.IsEdit = true; }
                        $scope.CanEditPeriod = true;

                        $scope.RelationData = {
                            ObjectType: $routeParams.itemKey.split('|')[0],
                            ObjectID: $routeParams.itemKey.split('|')[1],
                            Relation:  $scope.RelationSelectList[findWithAttr($scope.RelationSelectList, 'Text', $routeParams.itemKey.split('|')[2])].ObjectID,
                            NextObjectType: $routeParams.itemKey.split('|')[3],
                            NextObjectID: $routeParams.itemKey.split('|')[4],
                            BeginDate: $routeParams.itemKey.split('|')[5],
                            EndDate: $routeParams.itemKey.split('|')[6],
                            PercentValue: $routeParams.itemKey.split('|')[7]
                        };

                        $scope.OldRelationData = angular.copy($scope.RelationData);

                        $scope.NoSpecify.val = ($scope.RelationData.EndDate == '9999-12-31');
                        getRelationValidation($scope.RelationData.Relation);
                        $('[id="ObjectID"]').removeClass('hide');
                        $('[id="NextObjectID"]').removeClass('hide');
                    }
                    else {
                        var date = new Date();
                        var dateStr = date.getFullYear() + '-' + ("0" + (date.getMonth() + 1)).slice(-2) + '-' + ("0" + date.getDate()).slice(-2);
                        $scope.RelationData = {
                            ObjectType: '',
                            ObjectID: '',
                            Relation: '',
                            NextObjectType: '',
                            NextObjectID: '',
                            BeginDate: dateStr,
                            EndDate: '9999-12-31',
                            PercentValue: 100
                        };
                        $scope.NoSpecify.val = ($scope.RelationData.EndDate == '9999-12-31');
                        $scope.CanEditPeriod = true;
                    }
                    
                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                });
            }

            function getRelationValidation(relation) {
                var oRequestParameter = {
                    InputParameter: { "Relation": relation }
                        , CurrentEmployee: getToken(CONFIG.USER)
                };
                var url = CONFIG.SERVER + 'Employee/GetRelationValidation';
                $http({
                    method: 'POST',
                    data: oRequestParameter,
                    url: url
                }).then(function successCallback(response) {
                    // Success
                    console.log('GetRelationSelectData.', response.data);
                    $scope.ObjectTypeSelectList = response.data["ObjectType"];
                    $scope.NextObjectTypeSelectList = response.data["NextObjectType"];
                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                });
            }

            $scope.getAllObjectNew = function () {
                var oRequestParameter;
                if ($scope.RelationData && $scope.RelationData.BeginDate) {
                    oRequestParameter = {
                        InputParameter: { "BeginDate": $scope.RelationData.BeginDate.toString('yyyy-MM-dd') }
                            , CurrentEmployee: getToken(CONFIG.USER)
                    };
                }
                else {
                    oRequestParameter = {
                        InputParameter: { }
                            , CurrentEmployee: getToken(CONFIG.USER)
                    };
                }
                var url = CONFIG.SERVER + 'Employee/GetAllObject';
                $http({
                    method: 'POST',
                    data: oRequestParameter,
                    url: url
                }).then(function successCallback(response) {
                    // Success
                    console.log('GetRelationSelectData.', response.data);
                    $scope.ObjectDic = {};
                    $scope.ObjectDic["EMPLOYEE"] = response.data["EMPLOYEE"];
                    $scope.ObjectDic["POSITION"] = response.data["POSITION"];
                    $scope.ObjectDic["ORGANIZE"] = response.data["ORGANIZE"];
                    $scope.ObjectDic["COSTCENTER"] = response.data["COSTCENTER"];
                    url = CONFIG.SERVER + 'Employee/GetAllPositionForHRMaster';
                    $http({
                        method: 'POST',
                        data: oRequestParameter,
                        url: url
                    }).then(function successCallback(response) {
                        // Success
                        console.log('GetRelationSelectData.', response.data);
                        $scope.ObjectDic["POSITIONA008"] = response.data;
                        if ($scope.RelationData.ObjectType != '') {
                            $scope.ObjectIDSelectList = $scope.ObjectDic[$scope.RelationData.ObjectType];
                            //$scope.RelationData.ObjectID = $scope.ObjectIDSelectList[findWithAttr($scope.ObjectIDSelectList, 'ObjectID', $scope.RelationData.ObjectID)];
                            $scope.NextObjectIDSelectList = $scope.ObjectDic[$scope.RelationData.NextObjectType];
                            //$scope.RelationData.NextObjectID = $scope.NextObjectIDSelectList[findWithAttr($scope.NextObjectIDSelectList, 'ObjectID', $scope.RelationData.NextObjectID)];
                        }
                    }, function errorCallback(response) {
                        // Error
                        console.log('error RequestController.', response);
                    });
                    
                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                });
            }

            function findWithAttr(array, attr, value) {
                for (var i = 0; i < array.length; i += 1) {
                    if (array[i][attr] === value) {
                        return i;
                    }
                }
            }

            function InsertINFOTYPE1001(oRequestParameter, URL) {
                // ------ insert data to database -------//
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    alert('COMPLETE');
                    $location.path('/frmViewContent/403');
                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                });
                // ------ ------------------------------------ -------//
            }
            console.log('ObjectSettingEditorController');

        }]);
})();

