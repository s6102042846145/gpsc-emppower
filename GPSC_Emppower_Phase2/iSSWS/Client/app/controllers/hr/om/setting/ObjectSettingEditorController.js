﻿(function () {
    angular.module('ESSMobile')
        .controller('ObjectSettingEditorController', ['$scope', '$http', '$routeParams', '$location', 'DTOptionsBuilder', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, DTOptionsBuilder, $filter, CONFIG) {
            $scope.OSTextcategory = 'OBJECTSETTING';
            $scope.content.Header = $scope.Text['APPLICATION'].OM_OBJECT_SETTING;
            $scope.ObjectType = angular.isUndefined($routeParams.itemKey) && $routeParams.itemKey != '0' ? null : $routeParams.itemKey.split('|')[0];
            $scope.ObjectID = angular.isUndefined($routeParams.itemKey) && $routeParams.itemKey != '0' ? null : $routeParams.itemKey.split('|')[1];
            $scope.BeginDate = angular.isUndefined($routeParams.itemKey) && $routeParams.itemKey != '0' ? null : $scope.DOB = $filter('date')($routeParams.itemKey.split('|')[2], 'yyyy-MM-dd');
            $scope.EndDate = angular.isUndefined($routeParams.itemKey) && $routeParams.itemKey != '0' ? null : $scope.DOB = $filter('date')($routeParams.itemKey.split('|')[3], 'yyyy-MM-dd');
            $scope.ShowObjectID = true;
            $scope.CanEditPeriod = true;
            $scope.ObjectData = [];
            $scope.NoSpecify = {
                val: false
            };
            $scope.setSelectedBeginDate = function (selectedDate) {
                $scope.ObjectData.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };
            $scope.setSelectedEndDate = function (selectedDate) {
                $scope.ObjectData.EndDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
                console.log(($scope.ObjectData.EndDate == '9999-12-31'));
                $scope.NoSpecify.val = ($scope.ObjectData.EndDate == '9999-12-31');
            };
            $scope.onCheckBoxChange = function () {

                if ($scope.NoSpecify.val == true) {
                    $scope.ObjectData.EndDate = '9999-12-31';
                } else {
                    $scope.ObjectData.EndDate = $scope.ObjectData.BeginDate;
                }
            }


            $scope.onClickEditSave = function () {
                var oReturn = true;
                if ($scope.ObjectData.ObjectType.trim().length <= 0) oReturn = false;
                if ($scope.ObjectData.ObjectID.trim().length <= 0) oReturn = false;
                if ($scope.ObjectData.ShortText.trim().length <= 0) oReturn = false;
                if ($scope.ObjectData.Text.trim().length <= 0) oReturn = false;
                if ($scope.ObjectData.ShortTextEn.trim().length <= 0) oReturn = false;
                if ($scope.ObjectData.TextEn.trim().length <= 0) oReturn = false;
                if (oReturn == true) {
                    console.log($scope.ObjectData);
                    // ------ update data to database -------//
                    var oRequestParameter = {
                        InputParameter: { "ObjectData": $scope.ObjectData }
                                                , CurrentEmployee: getToken(CONFIG.USER)
                    };
                    var URL = CONFIG.SERVER + 'Employee/UpdateINFOTYPE1000';
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        alert('COMPLETE');
                        $location.path('/frmViewContent/402/');
                    }, function errorCallback(response) {
                        // Error
                        console.log('error RequestController.', response);
                    });
                    // ------ ------------------------------------ -------//
                }
                else {
                    alert($scope.Text['OBJECTSETTING'].PLEASEINPUTALL);
                }
            };
            $scope.onClickAddSave = function () {
                var oReturn = true;
                if ($scope.ObjectData.ObjectType.trim().length <= 0) oReturn = false;
                if ($scope.ObjectData.ShortText.trim().length <= 0) oReturn = false;
                if ($scope.ObjectData.Text.trim().length <= 0) oReturn = false;
                if ($scope.ObjectData.ShortTextEn.trim().length <= 0) oReturn = false;
                if ($scope.ObjectData.TextEn.trim().length <= 0) oReturn = false;
                if (oReturn == true) {
                    var oRequestParameterr = {
                        InputParameter: { "ObjectType": $scope.ObjectData.ObjectType, "ObjectID": $scope.ObjectData.ObjectID, "DataObjectType": "Object", "BeginDate": $scope.ObjectData.BeginDate.toString('yyyy-MM-dd'), "EndDate": $scope.ObjectData.EndDate.toString('yyyy-MM-dd') }
                                                , CurrentEmployee: getToken(CONFIG.USER)
                    };
                    var urlll = CONFIG.SERVER + 'Employee/CheckStatus';
                    $http({
                        method: 'POST',
                        url: urlll,
                        data: oRequestParameterr
                    }).then(function successCallback(response) {
                        // Success
                        console.log('eiei', response);
                        var oRequestParameter = {
                            InputParameter: { "ObjectData": $scope.ObjectData }
                                                , CurrentEmployee: getToken(CONFIG.USER)
                        };
                        var insertURL = CONFIG.SERVER + 'Employee/InsertINFOTYPE1000';
                        if (response.data.CanInsert == true && response.data.Duplicate == true && $scope.ObjectData.ObjectID != '') {
                            if (confirm($scope.Text['PERSONALADMINISTRATION'].DuplicatePeriod) == true) {
                                InsertINFOTYPE1000(oRequestParameter, insertURL);
                                //$location.path('/frmViewContent/402/' + $scope.itemKey);
                            }
                        }
                        else if (response.data.CanInsert == true) {
                            InsertINFOTYPE1000(oRequestParameter, insertURL);
                            //$location.path('/frmViewContent/402/' + $scope.itemKey);
                        }
                        else {
                            alert($scope.Text['PERSONALADMINISTRATION'].StaccatoPeriod);
                        }
                    }, function errorCallback(response) {
                        // Error
                        console.log('error RequestController.', response);
                    });
                }
                else {
                    alert($scope.Text['OBJECTSETTING'].PLEASEINPUTALL);
                }
            };

            if ($scope.BeginDate != null) {
                if ($routeParams.itemKey.split('|')[4] != null) { $scope.ShowObjectID = true; }
                else { $scope.ShowObjectID = false; }
                var oRequestParameter = {
                    InputParameter: { 'ObjectType': $scope.ObjectType, 'ObjectID': $scope.ObjectID, 'BeginDate': $scope.BeginDate, 'EndDate': $scope.EndDate }
                    , CurrentEmployee: getToken(CONFIG.USER)
                };
                var URL = CONFIG.SERVER + 'Employee/INFOTYPE1000Get';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    console.log('document.', response.data);
                    $scope.ObjectData = response.data;
                    if ($scope.ShowObjectID == false) { $scope.ObjectData.ObjectID = ''; }
                    $scope.ObjectData.EndDate = $scope.ObjectData.EndDate == '9999-12-31T23:59:59' ? '9999-12-31' : $scope.ObjectData.EndDate;
                    $scope.ObjectData.BeginDate = $filter('date')(response.data.BeginDate, 'yyyy-MM-dd');
                    $scope.ObjectData.EndDate = $filter('date')($scope.ObjectData.EndDate, 'yyyy-MM-dd');
                    $scope.NoSpecify.val = ($scope.ObjectData.EndDate == '9999-12-31');
                    console.log('data.', $scope.ObjectData)
                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                });
            }
            else {
                var date = new Date();
                var dateStr = date.getFullYear() + '-' + ("0" + (date.getMonth() + 1)).slice(-2) + '-' + ("0" + date.getDate()).slice(-2);
                $scope.ObjectData = {
                    ObjectType: '',
                    ObjectID: '',
                    BeginDate: dateStr,
                    EndDate: '9999-12-31',
                    ShortText: '',
                    Text: '',
                    ShortTextEn: '',
                    TextEn: ''
                };
                $scope.NoSpecify.val = ($scope.ObjectData.EndDate == '9999-12-31');
                $scope.ShowObjectID = false;
                $scope.CanEditPeriod = true;
            }

            function InsertINFOTYPE1000(oRequestParameter, URL) {
                // ------ insert data to database -------//
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    alert('COMPLETE');
                    $location.path('/frmViewContent/402');
                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                });
                // ------ ------------------------------------ -------//
            }
            console.log('ObjectSettingEditorController');

        }]);
})();

