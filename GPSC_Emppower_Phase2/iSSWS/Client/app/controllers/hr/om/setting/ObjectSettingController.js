﻿(function () {
    angular.module('ESSMobile')
        .controller('ObjectSettingController', ['$scope', '$http', '$routeParams', '$location', 'DTOptionsBuilder', '$route', 'CONFIG','$filter', function ($scope, $http, $routeParams, $location, DTOptionsBuilder, $route, CONFIG,$filter) {
            $scope.OSTextcategory = 'OBJECTSETTING';
            $scope.content.isShowHeader = false;
            $scope.ObjectData = [];
            $scope.ObjectDataS = [];
            $scope.ObjectDataO = [];
            $scope.ObjectDataA = [];
            $scope.ObjectDataC = [];
            $scope.pageSize = 10;
            $scope.OnChange = function () {
                //alert('dadawd');
                $scope.ObjectData = [];
                var val = $('#ObjectType').val();
                if(val == 'Organize'){
                    $scope.ObjectData = angular.copy($scope.ObjectDataO);
                    //console.log('ooo', $scope.ObjectDataO);
                }
                else if (val == 'Position') {
                    $scope.ObjectData = angular.copy($scope.ObjectDataS);
                    //console.log('sss', $scope.ObjectDataS);
                }
                else if (val == 'CostCenter') {
                    $scope.ObjectData = angular.copy($scope.ObjectDataC);
                    //console.log('sss', $scope.ObjectDataC);
                }
                else {
                    $scope.ObjectData = angular.copy($scope.ObjectDataA);
                    //console.log('aaa', $scope.ObjectDataA);
                }
                // display list
                convertDateArr($scope.ObjectData);
                filterArry('');
            }
            $scope.OnClickAdd = function () {
                //alert(id);
                $location.path('/frmViewContent/414/');
            }
            $scope.OnClickCopy = function (ObjectType, ObjectID, BeginDate, EndDate) {
                $location.path('/frmViewContent/414/' + ObjectType + '|' + ObjectID + '|' + BeginDate.split('T')[0] + '|' + EndDate.split('T')[0]);
            }
            $scope.OnClickEdit = function (ObjectType, ObjectID, BeginDate, EndDate) {
                $location.path('/frmViewContent/414/' + ObjectType + '|' + ObjectID + '|' + BeginDate.split('T')[0] + '|' + EndDate.split('T')[0] + '|000');
            }
            $scope.OnClickDelete = function (ObjectType, ObjectID, BeginDate, EndDate) {
                if (confirm("ต้องการจะลบข้อมูลใช่หรือไม่") == true) {
                    var oRequestParameter = {
                        InputParameter: { "ObjectType": ObjectType, "ObjectID": ObjectID, "BeginDate": BeginDate.split('T')[0], "EndDate": EndDate.split('T')[0] }
                        , CurrentEmployee: getToken(CONFIG.USER)
                    };
                    var url = CONFIG.SERVER + 'Employee/DeleteINFOTYPE1000';
                    $http({
                        method: 'POST',
                        data: oRequestParameter,
                        url: url
                    }).then(function successCallback(response) {
                        // Success
                        alert('ลบข้อมูลเรียบร้อย');
                        $route.reload();
                    }, function errorCallback(response) {
                        // Error
                        console.log('error RequestController.', response);
                    });
                }
            }
            $scope.dtOptions = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength($scope.pageSize).withOption('lengthChange', false).withOption('ordering', false).withDataProp('data');

            var oRequestParameter = {
                InputParameter: {}
                , CurrentEmployee: getToken(CONFIG.USER)
            };
            var URL = CONFIG.SERVER + 'Employee/INFOTYPE1000GetAllHistory';
            $scope.loader.conetent = true;
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                console.log('document.', response.data);
                //$scope.ObjectData = response.data;
                for (var i = 0; i < response.data.length; i++) {
                    if (response.data[i].ObjectType == 'ORGANIZE') {
                        $scope.ObjectDataO.push(response.data[i]);
                    }
                    else if(response.data[i].ObjectType == 'POSITION'){
                        $scope.ObjectDataS.push(response.data[i]);
                    }
                    else {
                        $scope.ObjectDataC.push(response.data[i]);
                    }
                    $scope.ObjectDataA.push(response.data[i]);
                }
                $scope.ObjectData = $scope.ObjectDataA;
                
                console.log('data.', $scope.ObjectData)
                // display list
                convertDateArr($scope.ObjectData);
                filterArry('');
                $scope.loader.conetent = false;
            }, function errorCallback(response) {
                // Error
                console.log('error RequestController.', response);
                $scope.EmployeeData = null;
                $scope.loader.conetent = false;
            });
            console.log('ObjectSettingController');




            // test list script start

            $scope.data= {
                nPageSize:10,
                arrFilterd:[],
                arrSplited:[],
                selectedColumn:0,
                tFilter:'',
                pages:[]
            };
            $scope.filteredItem = [];
            $scope.$watch('search', function(filter){ 
              filterArry(filter)
            });
            function filterArry(filter){
                $scope.filteredItem = $filter('filter')($scope.ObjectData, filter);
                var expectedPage =0;
                var i,j,temparray,chunk = 10;
                $scope.data.arrSplited=[];
                for (i=0,j=$scope.filteredItem.length; i<j; i+=chunk) {
                    temparray = $scope.filteredItem.slice(i,i+chunk);
                    $scope.data.arrSplited.push(temparray);
                    expectedPage++
                }
                $scope.data.selectedColumn = 0;
            }
            $scope.pageLeft = function () {if($scope.data.selectedColumn>0){ $scope.data.selectedColumn--;}}
            $scope.pageRight = function () {if($scope.data.selectedColumn < $scope.data.arrSplited.length-1){$scope.data.selectedColumn++;}}
            function convertDateArr(arr){
                for(var i =0;i <arr.length; i++){
                    var startDate = new Date(arr[i].BeginDate);
                    var endDate = new Date(arr[i].EndDate);
                    var temp1 = $filter('date')(arr[i].BeginDate, "dd/MM/yyyy") +'-'+$filter('date')(arr[i].EndDate, "dd/MM/yyyy"); 
                    arr[i].temp1 = temp1;
                }
            }
            // test list script end

        }]);
})();

