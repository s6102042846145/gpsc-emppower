﻿(function () {
    angular.module('ESSMobile')
        .controller('PositionLevelEditorController', ['$scope', '$http', '$routeParams', '$location', 'DTOptionsBuilder', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, DTOptionsBuilder, $filter, CONFIG) {
            $scope.OSTextcategory = 'OBJECTSETTING';
            $scope.content.Header = $scope.Text['APPLICATION'].POSITION_LEVEL_SETTING;
            $scope.PATextcategory = 'PERSONALADMINISTRATION';
            $scope.ObjectID = angular.isUndefined($routeParams.itemKey) && $routeParams.itemKey != '0' ? null : $routeParams.itemKey.split('|')[0];
            $scope.BeginDate = angular.isUndefined($routeParams.itemKey) && $routeParams.itemKey != '0' ? null : $scope.DOB = $filter('date')($routeParams.itemKey.split('|')[1], 'yyyy-MM-dd');
            $scope.EndDate = angular.isUndefined($routeParams.itemKey) && $routeParams.itemKey != '0' ? null : $scope.DOB = $filter('date')($routeParams.itemKey.split('|')[2], 'yyyy-MM-dd');
            $scope.ShowObjectID = true;
            $scope.CanEditPeriod = true;
            $scope.IsCopy = false;
            $scope.PositionData = [];
            $scope.PositionDataSelector = [];
            $scope.EmpGroupSelector = [];
            $scope.NoSpecify = {
                val: false
            };
            $scope.setSelectedBeginDate = function (selectedDate) {
                $scope.PositionData.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };
            $scope.setSelectedEndDate = function (selectedDate) {
                $scope.PositionData.EndDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
                console.log(($scope.PositionData.EndDate == '9999-12-31'));
                $scope.NoSpecify.val = ($scope.PositionData.EndDate == '9999-12-31');
            };
            $scope.onCheckBoxChange = function () {

                if ($scope.NoSpecify.val == true) {
                    $scope.PositionData.EndDate = '9999-12-31';
                } else {
                    $scope.PositionData.EndDate = $scope.PositionData.BeginDate;
                }
            }
            $scope.onClickEditSave = function () {
                var oReturn = true;
                if ($scope.PositionData.ObjectID.trim().length <= 0 || $scope.PositionData.EmpSubGroup.trim().length <= 0) oReturn = false;
                if (oReturn == true) {
                    // ------ update data to database -------//
                    var oRequestParameter = {
                        InputParameter: { "PositionData": $scope.PositionData }
                                                , CurrentEmployee: getToken(CONFIG.USER)
                    };
                    var URL = CONFIG.SERVER + 'Employee/UpdateINFOTYPE1013';
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        alert('COMPLETE');
                        $location.path('/frmViewContent/404/');
                    }, function errorCallback(response) {
                        // Error
                        console.log('error RequestController.', response);
                    });
                    // ------ ------------------------------------ -------//
                }
            };
            $scope.onClickAddSave = function () {
                var oReturn = true;
                if (oReturn == true) {
                    var oRequestParameterr = {
                        InputParameter: { "ObjectID": $scope.PositionData.ObjectID, "DataObjectType": "Position", "BeginDate": $scope.PositionData.BeginDate.toString('yyyy-MM-dd'), "EndDate": $scope.PositionData.EndDate.toString('yyyy-MM-dd') }
                                                , CurrentEmployee: getToken(CONFIG.USER)
                    };
                    var urlll = CONFIG.SERVER + 'Employee/CheckStatus';
                    $http({
                        method: 'POST',
                        url: urlll,
                        data: oRequestParameterr
                    }).then(function successCallback(response) {
                        // Success
                        console.log('eiei', response);
                        //if (angular.isUndefined($scope.PositionData.EmpGroup)) $scope.PositionData.EmpGroup = 'A';
                        var oRequestParameter = {
                            InputParameter: { "PositionData": $scope.PositionData }
                                                , CurrentEmployee: getToken(CONFIG.USER)
                        };
                        var insertURL = CONFIG.SERVER + 'Employee/InsertINFOTYPE1013';
                        if (response.data.CanInsert == true && response.data.Duplicate == true) {
                            if (confirm($scope.Text['PERSONALADMINISTRATION'].DuplicatePeriod) == true) {
                                InsertINFOTYPE1013(oRequestParameter, insertURL);
                                //$location.path('/frmViewContent/402/' + $scope.itemKey);
                            }
                        }
                        else if (response.data.CanInsert == true) {
                            InsertINFOTYPE1013(oRequestParameter, insertURL);
                            //$location.path('/frmViewContent/402/' + $scope.itemKey);
                        }
                        else {
                            alert($scope.Text['PERSONALADMINISTRATION'].StaccatoPeriod);
                        }
                    }, function errorCallback(response) {
                        // Error
                        console.log('error RequestController.', response);
                    });
                }
            };

            if ($scope.BeginDate != null) {
                if ($routeParams.itemKey.split('|')[3] != null) { $scope.IsEdit = true; }
                var oRequestParameter = {
                    InputParameter: { 'ObjectID': $scope.ObjectID, 'BeginDate': $scope.BeginDate, 'EndDate': $scope.EndDate }
                    , CurrentEmployee: getToken(CONFIG.USER)
                };
                var URL = CONFIG.SERVER + 'Employee/GetPositionDataForEdit';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    console.log('document.', response.data);
                    $scope.PositionData = response.data.PositionData;
                    $scope.PositionData.EndDate = $scope.PositionData.EndDate == '9999-12-31T23:59:59' ? '9999-12-31' : $scope.PositionData.EndDate;
                    $scope.PositionData.BeginDate = $filter('date')(response.data.PositionData.BeginDate, 'yyyy-MM-dd');
                    $scope.PositionData.EndDate = $filter('date')($scope.PositionData.EndDate, 'yyyy-MM-dd');
                    $scope.PositionDataSelector = response.data.PositionSelector;
                    $scope.EmpGroupSelector = response.data.EmpGroupSelector;
                    $scope.PositionData.ObjectID = $scope.PositionData.ObjectID.split(':')[0];
                    $scope.NoSpecify.val = ($scope.PositionData.EndDate == '9999-12-31');
                    console.log('CAnEdit.', $scope.CanEditPeriod, ' IsCopy', $scope.IsCopy);
                    console.log('data.', $scope.PositionData)
                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                });
            }
            else {
                var oRequestParameter = {
                    InputParameter: {  }
                    , CurrentEmployee: getToken(CONFIG.USER)
                };
                var URL = CONFIG.SERVER + 'Employee/GetPositionLevelObject';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    console.log('document.', response.data);
                    $scope.PositionDataSelector = response.data.PositionSelector;
                    $scope.EmpGroupSelector = response.data.EmpGroupSelector;
                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                });
                var date = new Date();
                var dateStr = date.getFullYear() + '-' + ("0" + (date.getMonth() + 1)).slice(-2) + '-' + ("0" + date.getDate()).slice(-2);
                $scope.PositionData = {
                    ObjectID: '',
                    EmpGroup: '',
                    EmpSubGroup: '',
                    BeginDate: dateStr,
                    EndDate: '9999-12-31'
                };
                $scope.NoSpecify.val = ($scope.PositionData.EndDate == '9999-12-31');
                $scope.CanEditPeriod = true;
            }

            function InsertINFOTYPE1013(oRequestParameter, URL) {
                // ------ insert data to database -------//
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    alert('COMPLETE');
                    $location.path('/frmViewContent/404');
                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                });
                // ------ ------------------------------------ -------//
            }
            console.log('PositionLevelEditorController');

        }]);
})();

