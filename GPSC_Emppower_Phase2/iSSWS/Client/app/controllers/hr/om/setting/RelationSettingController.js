﻿(function () {
    angular.module('ESSMobile')
        .controller('RelationSettingController', ['$scope', '$http', '$routeParams', '$location', 'DTOptionsBuilder', '$route', 'CONFIG','$filter', function ($scope, $http, $routeParams, $location, DTOptionsBuilder, $route, CONFIG,$filter) {
            $scope.OSTextcategory = 'OBJECTSETTING';
            $scope.content.Header = $scope.Text[$scope.OSTextcategory].CONSOLE_HEADSUBJECT;
            $scope.content.isShowHeader = false;
            $scope.RelationTextcategory = 'RELATION';
            $scope.RelationData = [];
            $scope.RelationDataArray = new Array();
            $scope.Relation;
            $scope.RelationSelectList = [];
            $scope.RelationValidation;
            $scope.RelationValidationSelectList = [];
            $scope.RelationValidationSelectListAll = [];
            $scope.pageSize = 10;
            $scope.OnRelationChange = function () {
                $scope.RelationValidationSelectList = [];
                if ($scope.Relation != 'ALL') {
                    for (var key in $scope.RelationDataArray[$scope.Relation]) {
                        if ($scope.RelationDataArray[$scope.Relation].hasOwnProperty(key)) {
                            $scope.RelationValidationSelectList.push(key);
                        }
                    }
                    $scope.RelationValidation = 'ALL';
                    $scope.RelationData = $scope.RelationDataArray[$scope.Relation][$scope.RelationValidation];
                }
                else {
                    $scope.RelationValidationSelectList = $scope.RelationValidationSelectListAll;
                    $scope.RelationData = $scope.RelationDataArray[$scope.Relation][$scope.RelationValidation];
                }
                // display list
                convertDateArr($scope.RelationData);
                filterArry('');

            }
            $scope.OnRelationValidationChange = function () {
                $scope.RelationData = $scope.RelationDataArray[$scope.Relation][$scope.RelationValidation];
                // display list
                convertDateArr($scope.RelationData);
                filterArry('');
            }
            $scope.OnClickAdd = function () {
                //alert(id);
                $location.path('/frmViewContent/415/');
            }
            $scope.OnClickCopy = function (ObjectType, ObjectID, Relation, NextObjectType, NextObjectID, BeginDate, EndDate, PercentValue) {
                $location.path('/frmViewContent/415/' + ObjectType + '|' + ObjectID.split(':')[0] + '|' + Relation + '|' + NextObjectType + '|' + NextObjectID.split(':')[0] + '|' + BeginDate.split('T')[0] + '|' + EndDate.split('T')[0] + '|' + PercentValue);
            }
            $scope.OnClickEdit = function (ObjectType, ObjectID, Relation, NextObjectType, NextObjectID, BeginDate, EndDate, PercentValue) {
                $location.path('/frmViewContent/415/' + ObjectType + '|' + ObjectID.split(':')[0] + '|' + Relation + '|' + NextObjectType + '|' + NextObjectID.split(':')[0] + '|' + BeginDate.split('T')[0] + '|' + EndDate.split('T')[0] + '|' + PercentValue + '|000');
            }
            $scope.OnClickDelete = function (obj/*ObjectType, ObjectID, Relation, NextObjectType, NextObjectID, BeginDate, EndDate, PercentValue*/) {
                if (confirm("ต้องการจะลบข้อมูลใช่หรือไม่") == true) {
                    /*var oRequestParameter = {
                        InputParameter: {
                            "RelationData": {
                                "ObjectType": ObjectType, "ObjectID": ObjectID, "Relation": Relation
                            , "NextObjectType": NextObjectType, "NextObjectID": NextObjectID
                            , "BeginDate": BeginDate, "EndDate": EndDate, "PercentValue": PercentValue
                            }
                        }
                        , CurrentEmployee: getToken(CONFIG.USER)
                    };*/
                    var oRequestParameter = {
                        InputParameter: {
                            "RelationData": obj
                        }
                        , CurrentEmployee: getToken(CONFIG.USER)
                    };
                    var url = CONFIG.SERVER + 'Employee/DeleteINFOTYPE1001';
                    $http({
                        method: 'POST',
                        data: oRequestParameter,
                        url: url
                    }).then(function successCallback(response) {
                        // Success
                        alert('ลบข้อมูลเรียบร้อย');
                        $route.reload();
                    }, function errorCallback(response) {
                        // Error
                        console.log('error RequestController.', response);
                    });
                }
            }
            $scope.dtOptions = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength($scope.pageSize).withOption('lengthChange', false).withOption('ordering', false).withDataProp('data');

            var oRequestParameter = {
                InputParameter: {}
                , CurrentEmployee: getToken(CONFIG.USER)
            };
            var URL = CONFIG.SERVER + 'Employee/INFOTYPE1001GetAllHistory';
            $scope.loader.conetent = true;
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                console.log('document.', response.data);
                //$scope.RelationData = response.data;
                var temp = [];
                var tempRelation = '';
                $scope.RelationDataArray['ALL'] = new Array();
                $scope.RelationValidationSelectList.push('ALL');
                for (var i = 0; i < response.data.length; i++) {
                    if ($scope.RelationDataArray[response.data[i].Relation] == null) {
                        $scope.RelationDataArray[response.data[i].Relation] = new Array();
                        $scope.RelationDataArray[response.data[i].Relation]['ALL'] = [];
                        $scope.RelationDataArray[response.data[i].Relation]['ALL'].push(response.data[i]);
                        $scope.RelationDataArray[response.data[i].Relation][response.data[i].ObjectType + ' ' + response.data[i].Relation + ' ' + response.data[i].NextObjectType] = [];
                        $scope.RelationDataArray[response.data[i].Relation][response.data[i].ObjectType + ' ' + response.data[i].Relation + ' ' + response.data[i].NextObjectType].push(response.data[i]);
                        $scope.RelationDataArray['ALL'][response.data[i].ObjectType + ' ' + response.data[i].Relation + ' ' + response.data[i].NextObjectType] = [];
                        $scope.RelationDataArray['ALL'][response.data[i].ObjectType + ' ' + response.data[i].Relation + ' ' + response.data[i].NextObjectType].push(response.data[i]);
                    }
                    else {
                        if ($scope.RelationDataArray[response.data[i].Relation][response.data[i].ObjectType + ' ' + response.data[i].Relation + ' ' + response.data[i].NextObjectType] == null) {
                            $scope.RelationDataArray[response.data[i].Relation][response.data[i].ObjectType + ' ' + response.data[i].Relation + ' ' + response.data[i].NextObjectType] = [];
                            $scope.RelationDataArray[response.data[i].Relation][response.data[i].ObjectType + ' ' + response.data[i].Relation + ' ' + response.data[i].NextObjectType].push(response.data[i]);
                            $scope.RelationDataArray['ALL'][response.data[i].ObjectType + ' ' + response.data[i].Relation + ' ' + response.data[i].NextObjectType] = [];
                            $scope.RelationDataArray['ALL'][response.data[i].ObjectType + ' ' + response.data[i].Relation + ' ' + response.data[i].NextObjectType].push(response.data[i]);
                        }
                        else {
                            $scope.RelationDataArray[response.data[i].Relation][response.data[i].ObjectType + ' ' + response.data[i].Relation + ' ' + response.data[i].NextObjectType].push(response.data[i]);
                            $scope.RelationDataArray['ALL'][response.data[i].ObjectType + ' ' + response.data[i].Relation + ' ' + response.data[i].NextObjectType].push(response.data[i]);
                        }
                        $scope.RelationDataArray[response.data[i].Relation]['ALL'].push(response.data[i]);
                    }
                    if ($scope.RelationValidationSelectList.indexOf(response.data[i].ObjectType + ' ' + response.data[i].Relation + ' ' + response.data[i].NextObjectType) == -1) {
                        $scope.RelationValidationSelectList.push(response.data[i].ObjectType + ' ' + response.data[i].Relation + ' ' + response.data[i].NextObjectType);
                    }
                    //$scope.RelationDataArray['ALL'].push(response.data[i]);
                }
                //$scope.RelationDataArray = response.data;
                $scope.RelationDataArray['ALL']['ALL'] = response.data;
                $scope.RelationData = $scope.RelationDataArray['ALL']['ALL'];
                console.log('data.', $scope.RelationData);
                console.log('dataArray.', $scope.RelationDataArray);
                for (var key in $scope.RelationDataArray) {
                    if ($scope.RelationDataArray.hasOwnProperty(key)) {
                        $scope.RelationSelectList.push(key);
                    }
                }
                console.log('RelationSelectList.', $scope.RelationSelectList);
                console.log('RelationValidationSelectList.', $scope.RelationValidationSelectList);
                $scope.Relation = $scope.RelationSelectList[0];
                $scope.RelationValidation = $scope.RelationValidationSelectList[0];
                $scope.RelationValidationSelectListAll = $scope.RelationValidationSelectList;
                // display list
                convertDateArr($scope.RelationData);
                filterArry('');
                $scope.loader.conetent = false;
            }, function errorCallback(response) {
                // Error
                console.log('error RequestController.', response);
                $scope.EmployeeData = null;
                $scope.loader.conetent = false;
            });
            console.log('RelationSettingController');


            // test list script start

            $scope.data= {
                nPageSize:10,
                arrFilterd:[],
                arrSplited:[],
                selectedColumn:0,
                tFilter:'',
                pages:[]
            };
            $scope.filteredItem = [];
            $scope.$watch('search', function(filter){ 
              filterArry(filter)
            });
            function filterArry(filter){
                $scope.filteredItem = $filter('filter')($scope.RelationData, filter);
                var expectedPage =0;
                var i,j,temparray,chunk = 10;
                $scope.data.arrSplited=[];
                for (i=0,j=$scope.filteredItem.length; i<j; i+=chunk) {
                    temparray = $scope.filteredItem.slice(i,i+chunk);
                    $scope.data.arrSplited.push(temparray);
                    expectedPage++
                }
                $scope.data.selectedColumn = 0;
            }
            $scope.pageLeft = function () {if($scope.data.selectedColumn>0){ $scope.data.selectedColumn--;}}
            $scope.pageRight = function () {if($scope.data.selectedColumn < $scope.data.arrSplited.length-1){$scope.data.selectedColumn++;}}
            function convertDateArr(arr){
                for(var i =0;i <arr.length; i++){
                    var startDate = new Date(arr[i].BeginDate);
                    var endDate = new Date(arr[i].EndDate);
                    var temp1 = $filter('date')(arr[i].BeginDate, "dd/MM/yyyy") +'-'+$filter('date')(arr[i].EndDate, "dd/MM/yyyy"); 
                    arr[i].temp1 = temp1;
                }
            }
            // test list script end


        }]);
})();

