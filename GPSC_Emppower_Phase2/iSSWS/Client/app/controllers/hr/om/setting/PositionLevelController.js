﻿(function () {
    angular.module('ESSMobile')
        .controller('PositionLevelController', ['$scope', '$http', '$routeParams', '$location', 'DTOptionsBuilder', '$route', 'CONFIG','$filter','$sanitize', function ($scope, $http, $routeParams, $location, DTOptionsBuilder, $route, CONFIG,$filter,$sanitize) {
            $scope.OSTextcategory = 'OBJECTSETTING';
                $scope.content.Header = $scope.Text['APPLICATION'].POSITION_LEVEL_SETTING;
            $scope.content.isShowHeader = false;
            $scope.PositionData = [];
            $scope.PositionDeleteData;
            $scope.pageSize = 10;
            $scope.OnClickAdd = function () {
                $location.path('/frmViewContent/416/');
            }
            $scope.OnClickCopy = function (ObjectID, BeginDate, EndDate) {
                $location.path('/frmViewContent/416/' + ObjectID.split(':')[0] + '|' + BeginDate.split('T')[0] + '|' + EndDate.split('T')[0]);
            }
            $scope.OnClickEdit = function (ObjectID, BeginDate, EndDate) {
                $location.path('/frmViewContent/416/' + ObjectID.split(':')[0] + '|' + BeginDate.split('T')[0] + '|' + EndDate.split('T')[0] + '|000');
            }
            $scope.OnClickDelete = function (ObjectID, BeginDate, EndDate) {
                if (confirm("ต้องการจะลบข้อมูลใช่หรือไม่") == true) {
                    $scope.PositionDeleteData = {
                        ObjectID: ObjectID.split(':')[0],
                        EmpGroup:'',
                        EmpSubGroup:'',
                        BeginDate: BeginDate,
                        EndDate: EndDate
                    };
                    var oRequestParameter = {
                        InputParameter: { "PositionData": $scope.PositionDeleteData }
                        , CurrentEmployee: getToken(CONFIG.USER)
                    };
                    var url = CONFIG.SERVER + 'Employee/DeleteINFOTYPE1013';
                    $http({
                        method: 'POST',
                        data: oRequestParameter,
                        url: url
                    }).then(function successCallback(response) {
                        // Success
                        alert('ลบข้อมูลเรียบร้อย');
                        $route.reload();
                    }, function errorCallback(response) {
                        // Error
                        console.log('error RequestController.', response);
                    });
                }
            }
            $scope.dtOptions = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength($scope.pageSize).withOption('lengthChange', false).withOption('ordering', false).withDataProp('data');

            var oRequestParameter = {
                InputParameter: {}
                , CurrentEmployee: getToken(CONFIG.USER)
            };
            var URL = CONFIG.SERVER + 'Employee/INFOTYPE1013GetAllHistory';
            $scope.loader.conetent = true;
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                console.log('document.', response.data);
                $scope.PositionData = response.data;
                console.log('data.', $scope.PositionData)
                convertDateArr($scope.PositionData);
                filterArry('');
                $scope.loader.conetent = false;
            }, function errorCallback(response) {
                // Error
                console.log('error RequestController.', response);
                $scope.loader.conetent = false;
            });
            console.log('PositionLevelController');


            // test list script start
            $scope.data= {
                nPageSize:10,
                arrFilterd:[],
                arrSplited:[],
                selectedColumn:0,
                tFilter:'',
                pages:[]
            };
            $scope.filteredItem = [];
            $scope.$watch('search', function(filter){ 
              filterArry(filter)
            });
            function filterArry(filter){
                $scope.filteredItem = $filter('filter')($scope.PositionData, filter);

                var expectedPage =0;
                var i,j,temparray,chunk = 10;
                $scope.data.arrSplited=[];
                for (i=0,j=$scope.filteredItem.length; i<j; i+=chunk) {
                    temparray = $scope.filteredItem.slice(i,i+chunk);
                    $scope.data.arrSplited.push(temparray);
                    expectedPage++
                }
                $scope.data.selectedColumn = 0;
            }
            
            function convertDateArr(arr){
                for(var i =0;i <arr.length; i++){
                    var startDate = new Date(arr[i].BeginDate);
                    var endDate = new Date(arr[i].EndDate);
                    var temp1 = $filter('date')(arr[i].BeginDate, "dd/MM/yyyy") +'-'+$filter('date')(arr[i].EndDate, "dd/MM/yyyy"); 
                    arr[i].temp1 = temp1;
                }
            }


            $scope.pageLeft = function () {if($scope.data.selectedColumn>0){ $scope.data.selectedColumn--;}}
            $scope.pageRight = function () {if($scope.data.selectedColumn < $scope.data.arrSplited.length-1){$scope.data.selectedColumn++;}}
            // test list script end

          

        }]);
})();

