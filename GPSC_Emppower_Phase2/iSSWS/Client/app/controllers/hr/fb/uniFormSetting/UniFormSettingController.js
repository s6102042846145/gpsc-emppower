﻿(function () {
    angular.module("ESSMobile").controller("UniFormSettingController", [
        "$scope",
        "$http",
        "$routeParams",
        "$location",
        "$filter",
        "CONFIG",
        "$q",
        "$mdDialog",
        function (
            $scope,
            $http,
            $routeParams,
            $location,
            $filter,
            CONFIG,
            $q,
            $mdDialog
        ) {
            console.log("UniFormSettingController", CONFIG.SERVER);
            $scope.isHideMultiCompany = true;
            $scope.GetConfingMultiCompany = function () {
                var URL = CONFIG.SERVER + "Employee/GetConfigMultiCompany";
                var oRequestParameter = {
                    InputParameter: {},
                    CurrentEmployee: $scope.employeeData,
                    Requestor: $scope.requesterData,
                    Creator: getToken(CONFIG.USER),
                };
                $http({
                    method: "POST",
                    url: URL,
                    data: oRequestParameter,
                }).then(
                    function successCallback(response) {
                        if (response.data === 1) {
                            $scope.isHideMultiCompany = false;
                        }

                        $scope.loader.enable = false;
                    },
                    function errorCallback(response) {
                        $scope.loader.enable = false;
                    }
                );
            };
            $scope.GetConfingMultiCompany();

            $scope.data = [];
            $scope.dataCheck = [];
            $scope.dataHistory = [];
            $scope.loadingData = false;
            $scope.UniFormType = [];
            $scope.UniFormSize = [];
            $scope.UniFormData = [];
            $scope.UniFormNew = [];
            $scope.Remove = "N";
            $scope.UniFormName = "";
            $scope.uniFormTypeName = "";
            $scope.loader.enable = true;
            $scope.employeeData = getToken(CONFIG.USER);
            $scope.ImagesChart = null;

            $scope.SelectCompanyCode = $scope.employeeData.CompanyCode;

            $scope.GetAuthorizationCompany = function () {
                var URL = CONFIG.SERVER + "workflow/GetAuthorizationCompany";
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {},
                    CurrentEmployee: $scope.employeeData,
                    Requestor: $scope.requesterData,
                    Creator: getToken(CONFIG.USER),
                };
                $http({
                    method: "POST",
                    url: URL,
                    data: oRequestParameter,
                }).then(
                    function successCallback(response) {
                        $scope.CompanyList = response.data;

                        if ($scope.CompanyList.length > 0) {
                            $scope.ddlCompany = $scope.CompanyList[0].CompanyCode;
                        }

                        $scope.loader.enable = false;
                    },
                    function errorCallback(response) {
                        $scope.loader.enable = false;
                    }
                );
            };
            $scope.GetAuthorizationCompany();

            $scope.GetUniFormType = function () {
                var URL = CONFIG.SERVER + "HRFB/GetUniFormType";
                var oRequestParameter = {
                    InputParameter: { SelectCompany: $scope.SelectCompanyCode },
                };
                $http({
                    method: "POST",
                    url: URL,
                    data: oRequestParameter,
                }).then(
                    function successCallback(response) {
                        const data = [];
                        if (response.data.length > 0) {
                            for (let index = 0; index < response.data.length; index++) {
                                const element = response.data[index];
                                if (element.Status !== "N") {
                                    element.UniFormTypeID = element.UniFormTypeID.toString();
                                    data.push(element);
                                }
                            }

                            $scope.UniFormType = data;
                        }
                        console.log("GetUniFormType", $scope.UniFormType);
                        // $scope.UniFormType = response.data;
                        if ($scope.UniFormType.length > 0) {
                            $scope.uniFormTypeName = $scope.UniFormType[0].UniFormTypeID;
                        }

                        $scope.loader.enable = false;
                    },
                    function errorCallback(response) {
                        $scope.loader.enable = false;
                    }
                );
            };
            $scope.GetUniFormType();

            $scope.GetUniForm = function () {
                var URL = CONFIG.SERVER + "HRFB/GetUniForm";
                var oRequestParameter = {
                    InputParameter: { SelectCompany: $scope.SelectCompanyCode },
                };
                $http({
                    method: "POST",
                    url: URL,
                    data: oRequestParameter,
                }).then(
                    function successCallback(response) {
                        const data = [];
                        if (response.data.length > 0) {
                            for (let index = 0; index < response.data.length; index++) {
                                const element = response.data[index];
                                if (element.Status !== "N") {
                                    element.UniFormTypeID = element.UniFormTypeID.toString();
                                    element.UniFormID = element.UniFormID.toString();
                                    data.push(element);
                                }
                            }
                            $scope.UniFormData = data;
                        }
                        console.log("GetUniFormData", $scope.UniFormData);
                        //$scope.UniFormData = response.data;
                        //console.log($scope.UniFormData);
                        $scope.loader.enable = false;
                    },
                    function errorCallback(response) {
                        $scope.loader.enable = false;
                    }
                );
            };
            $scope.GetUniForm();

            $scope.GetUniFormSize = function () {
                var URL = CONFIG.SERVER + "HRFB/GetUniFormSize";
                var oRequestParameter = {
                    InputParameter: { SelectCompany: $scope.SelectCompanyCode },
                };
                $http({
                    method: "POST",
                    url: URL,
                    data: oRequestParameter,
                }).then(
                    function successCallback(response) {
                        const data = [];
                        if (response.data.length > 0) {
                            for (let index = 0; index < response.data.length; index++) {
                                const element = response.data[index];
                                if (element.Status !== "N") {
                                    element.UniFormTypeID = element.UniFormTypeID.toString();
                                    element.UniFormID = element.UniFormID.toString();
                                    data.push(element);
                                }
                            }
                            $scope.UniFormSize = data;
                        }
                        console.log("$scope.UniFormSize", $scope.UniFormSize);

                        $scope.loader.enable = false;
                    },
                    function errorCallback(response) {
                        $scope.loader.enable = false;
                    }
                );
            };
            $scope.GetUniFormSize();

            $scope.UniFormTypeSave = function () {
                console.log($scope.UniFormNew);

                var URL = CONFIG.SERVER + "HRFB/UniFormTypeSave";
                for (let index = 0; index < $scope.UniFormNew.length; index++) {
                    const element = $scope.UniFormNew[index];
                    console.log(element, index);
                    var oRequestParameter = {
                        InputParameter: {
                            SelectCompany: $scope.SelectCompanyCode,
                            UniFormTypeID: element.UniFormTypeID,
                            UniFormTypeName: element.UniFormTypeName,
                            Status: element.Status !== "Y" ? element.Status : "Y",
                            CreateDate: element.CreateDate,
                            UpdateDate: element.UpdateDate,
                            Description: element.Description,
                            CreateBy: element.CreateBy,
                            UpdateBy: element.UpdateBy,
                            StartDate: element.StartDate,
                            EndDate: element.EndDate,
                        },
                    };
                    $http({
                        method: "POST",
                        url: URL,
                        data: oRequestParameter,
                    }).then(
                        function successCallback(response) {
                            console.log(response.data);

                            $scope.loader.enable = false;
                        },
                        function errorCallback(response) {
                            $scope.loader.enable = false;
                        }
                    );
                }

                $scope.UniFormNew = [];
            };

            $scope.UniFormSave = function () {
                console.log($scope.UniFormNew);

                var URL = CONFIG.SERVER + "HRFB/UniFormSave";
                for (let index = 0; index < $scope.UniFormNew.length; index++) {
                    const element = $scope.UniFormNew[index];
                    console.log(element, index);
                    var oRequestParameter = {
                        InputParameter: {
                            SelectCompany: $scope.SelectCompanyCode,
                            UniFormID: element.UniFormID,
                            UniFormTypeID: element.UniFormTypeID,
                            UniFormName: element.UniFormName,
                            Status: element.Status !== "Y" ? element.Status : "Y",
                            CreateDate: element.CreateDate,
                            UpdateDate: element.UpdateDate,
                            Description: element.Description,
                            CreateBy: element.CreateBy,
                            UpdateBy: element.UpdateBy,
                            StartDate: element.StartDate,
                            EndDate: element.EndDate,
                            Images: element.Images !== null ? element.Images : "",
                        },
                    };
                    $http({
                        method: "POST",
                        url: URL,
                        data: oRequestParameter,
                    }).then(
                        function successCallback(response) {
                            console.log(response.data);

                            $scope.loader.enable = false;
                        },
                        function errorCallback(response) {
                            console.log(response.data);
                            $scope.loader.enable = false;
                        }
                    );
                }

                $scope.UniFormNew = [];
            };

            $scope.UniFormSizeSave = function () {
                console.log($scope.UniFormNew);

                var URL = CONFIG.SERVER + "HRFB/UniFormSizeSave";
                for (let index = 0; index < $scope.UniFormNew.length; index++) {
                    const element = $scope.UniFormNew[index];
                    console.log(element, index);
                    var oRequestParameter = {
                        InputParameter: {
                            SelectCompany: $scope.SelectCompanyCode,
                            SizeID: element.SizeID,
                            UniFormID: element.UniFormID,
                            UniFormTypeID: element.UniFormTypeID,
                            Size: element.Size,
                            Status: element.Status !== "Y" ? element.Status : "Y",
                            CWLength: element.CWLength,
                            Length: element.Length,
                            CreateDate: element.CreateDate,
                            UpdateDate: element.UpdateDate,
                            CreateBy: element.CreateBy,
                            UpdateBy: element.UpdateBy,
                            StartDate: element.StartDate,
                            EndDate: element.EndDate,
                        },
                    };
                    $http({
                        method: "POST",
                        url: URL,
                        data: oRequestParameter,
                    }).then(
                        function successCallback(response) {
                            console.log(response.data);

                            $scope.loader.enable = false;
                        },
                        function errorCallback(response) {
                            $scope.loader.enable = false;
                        }
                    );
                }

                $scope.UniFormNew = [];
            };

            $scope.UploadSizeChart = function () {
                var URL = CONFIG.SERVER + "HRFB/UploadSizeChart";

                var oRequestParameter = {
                    InputParameter: {
                        SelectCompany: $scope.SelectCompanyCode,
                        ImagesChart: $scope.ImagesChart,
                    },
                };
                $http({
                    method: "POST",
                    url: URL,
                    data: oRequestParameter,
                }).then(
                    function successCallback(response) {
                        $scope.loader.enable = false;
                    },
                    function errorCallback(response) {
                        $scope.loader.enable = false;
                    }
                );
            };

            function getBase64(file) {
                return new Promise((resolve, reject) => {
                    const reader = new FileReader();
                    reader.readAsDataURL(file);
                    reader.onload = () => resolve(reader.result);
                    reader.onerror = (error) => reject(error);
                });
            }

            $scope.uploadFileSizeChart = function (files) {
                $scope.ImagesChart = null;
                getBase64(files).then((r) => {
                    $scope.ImagesChart = r;
                });
                setTimeout(() => {
                    console.log($scope.ImagesChart);
                    if ($scope.ImagesChart) {
                        $scope.UploadSizeChart();
                    }
                }, 1000);
            };

            $scope.uploadFile = function (files, item) {
                console.log(files, item);
                getBase64(files).then((r) => {
                    item.Images = r;
                });

                if (
                    $scope.UniFormNew.filter((x) => x.UniFormID === item.UniFormID)
                        .length === 0
                ) {
                    $scope.UniFormNew.push(item);
                }

                console.log($scope.UniFormNew);
            };

            $scope.change = function (type, item) {
                console.log(item.UniFormTypeID, $scope.UniFormNew);

                if (type === "UniFormType") {
                    if (
                        $scope.UniFormNew.filter(
                            (x) => x.UniFormTypeID === item.UniFormTypeID
                        ).length === 0
                    ) {
                        $scope.UniFormNew.push(item);
                    }
                } else if (type === "UniFormData") {
                    if (
                        $scope.UniFormNew.filter((x) => x.UniFormID === item.UniFormID)
                            .length === 0
                    ) {
                        $scope.UniFormNew.push(item);
                    }
                } else if (type === "UniFormSize") {
                    if (
                        $scope.UniFormNew.filter((x) => x.SizeID === item.SizeID).length ===
                        0
                    ) {
                        $scope.UniFormNew.push(item);
                    }
                }

                console.log(item.UniFormTypeID, $scope.UniFormNew);

                //$scope. UniFormNew.push(item)
            };

            $scope.addNew = function (type) {
                if (type === "UniFormType") {
                    $scope.UniFormType.push({
                        UniFormTypeID: "0",
                        UniFormTypeName: null,
                        Status: "Y",
                        CreateDate: null,
                        UpdateDate: null,
                        Description: null,
                        CreateBy: null,
                        UpdateBy: null,
                        StartDate: null,
                        EndDate: null,
                    });
                } else if (type === "UniFormData") {
                    $scope.UniFormData.push({
                        UniFormID: "0",
                        UniFormName: null,
                        UniFormTypeID: "",
                        Description: null,
                        Status: "Y",
                        CreateDate: null,
                        UpdateDate: null,
                        Images: null,
                        CreateBy: null,
                        UpdateBy: null,
                        StartDate: null,
                        EndDate: null,
                    });
                } else if (type === "UniFormSize") {
                    $scope.UniFormSize.push({
                        SizeID: "0",
                        UniFormID: "",
                        CWLength: null,
                        Length: null,
                        Size: "",
                        Status: "Y",
                        CreateDate: null,
                        UpdateDate: null,
                        Images: null,
                        CreateBy: null,
                        UpdateBy: null,
                        StartDate: null,
                        EndDate: null,
                    });
                }
            };

            $scope.init = function () {
                console.log("init ....");
                //TAP 1
                // GetUniFormType();

                //TAP 2
                //GetUniForm();

                //TAP 3
                //GetUniFormSize();
            };

            $scope.changeOptionUniForm = function (type, item) {
                console.log("changeOptionUniForm", type, item);

                if (type === "UniFormType") {
                    if (
                        $scope.UniFormNew.filter(
                            (x) => x.UniFormTypeID === item.UniFormTypeID
                        ).length === 0
                    ) {
                        $scope.UniFormNew.push(item);
                    }
                } else if (type === "UniFormData") {
                    if (
                        $scope.UniFormNew.filter((x) => x.UniFormID === item.UniFormID)
                            .length === 0
                    ) {
                        $scope.UniFormNew.push(item);
                    }
                } else {
                    if (
                        $scope.UniFormNew.filter((x) => x.SizeID === item.SizeID).length ===
                        0
                    ) {
                        $scope.UniFormNew.push(item);
                    }
                }
            };

            $scope.remove = function (type, item) {
                console.log(item, type);
                item.Status = "N";
                if (type === "UniFormType") {
                    if (
                        $scope.UniFormNew.filter(
                            (x) => x.UniFormTypeID === item.UniFormTypeID
                        ).length === 0
                    ) {
                        $scope.UniFormNew.push(item);
                    }
                } else if (type === "UniFormData") {
                    if (
                        $scope.UniFormNew.filter((x) => x.UniFormID === item.UniFormID)
                            .length === 0
                    ) {
                        $scope.UniFormNew.push(item);
                    }
                } else {
                    if (
                        $scope.UniFormNew.filter((x) => x.SizeID === item.SizeID).length ===
                        0
                    ) {
                        $scope.UniFormNew.push(item);
                    }
                }
            };

            $scope.back = function () {
                var url = "/frmViewContent/5001";
                $location.path(url);
            };

            function formatDate(date) {
                var d = new Date(date),
                    month = "" + (d.getMonth() + 1),
                    day = "" + d.getDate(),
                    year = d.getFullYear();

                if (month.length < 2) month = "0" + month;
                if (day.length < 2) day = "0" + day;

                return [year, month, day].join("-");
            }

            $scope.onClickTap = function (type) {
                $scope.UniFormNew = [];
                console.log("onClickTap");
                console.log(type);
                if (type === "UniFormType") {
                    $scope.GetUniFormType();
                } else if (type === "UniFormData") {
                    $scope.GetUniFormType();
                    $scope.GetUniForm();
                } else if (type === "UniFormSize") {
                    $scope.GetUniFormType();
                    $scope.GetUniForm();
                }
            };

            $scope.changeCompany = function (CompanyCode) {
                $scope.SelectCompanyCode = CompanyCode;

                console.log(CompanyCode, $scope.SelectCompanyCode);
            };

            $scope.setSelected = function (type, selectedDate, name, item) {
                let date = formatDate(selectedDate);

                if (name === "StartDate") {
                    item.StartDate = date;
                } else {
                    item.EndDate = date;
                }

                if (type === "UniFormType") {
                    if (
                        $scope.UniFormNew.filter(
                            (x) => x.UniFormTypeID === item.UniFormTypeID
                        ).length === 0
                    ) {
                        $scope.UniFormNew.push(item);
                    }
                } else if (type === "UniFormData") {
                    if (
                        $scope.UniFormNew.filter((x) => x.UniFormID === item.UniFormID)
                            .length === 0
                    ) {
                        $scope.UniFormNew.push(item);
                    }
                } else {
                    if (
                        $scope.UniFormNew.filter((x) => x.SizeID === item.SizeID).length ===
                        0
                    ) {
                        $scope.UniFormNew.push(item);
                    }
                }
            };

            $scope.onSubmit = function (type) {
                console.log("onSubmit", type, $scope.UniFormNew);
                if (type === "UniFormType") {
                    $scope.UniFormTypeSave();
                } else if (type === "UniFormData") {
                    $scope.UniFormSave();
                } else if (type === "UniFormSize") {
                    $scope.UniFormSizeSave();
                }
            };
        },
    ]);
})();
