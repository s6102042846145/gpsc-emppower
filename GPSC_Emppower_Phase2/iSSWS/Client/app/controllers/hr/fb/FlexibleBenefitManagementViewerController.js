﻿(function () {
    angular.module('ESSMobile')
        .controller('FlexibleBenefitManagementViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$sce', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $sce) {
            $scope.data = {
                nPageSize: 30,
                arrFilterd: [],
                arrSplited: [],
                selectedColumn: 0,
                tFilter: '',
                pages: []
            };
            $scope.EnrollmentData = [];
            $scope.ReimburseQuota = [];
            $scope.YearsList = [];
            $scope.TEXTDetail = [];
            $scope.CheckPeriod = [];
            $scope.years = '';
            $scope.keyYear_select = '';
            $scope.defaultYear = '';
            $scope.check_quota_balance = 0;
            $scope.IsShowTimeAwareLink = false;
            $scope.dateVariable = new Date();
            $scope.loader.enable = true;
            $scope.years = $scope.dateVariable.getFullYear();
            $scope.yearsnext = $scope.dateVariable.getFullYear() + 1;
            $scope.prop = {
                "type": "select",
                "name": $scope.years,
                "value": $scope.years,
                "values": $scope.YearsList
            };

            $scope.init = function () {
                GetYears();
                getTimeAwareLink();
                DefaultYear($scope.dateVariable.getFullYear());
                //$scope.years = $scope.dateVariable.getFullYear() + 1;
                //$scope.keyYear_select = $scope.dateVariable.getFullYear() + 1;

                //$scope.years = 2020;
                //$scope.keyYear_select = 2020;
                //GetEnrollmentData($scope.years);
                //GetReimburseData($scope.years);
                //GetReimburseQuota($scope.years);
                //GetTextDetail();
                //CheckPeriodSetting($scope.years);
                //CheckAdmin();
            }

            function DefaultYear(key_year) {

                var URL = CONFIG.SERVER + 'HRFB/CheckPeriodSetting';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "KeyYear": key_year }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    if (response.data != null && response.data.length > 0) {
                        //$scope.years = parseInt(response.data[0].EffectiveYear);
                        //$scope.keyYear_select = parseInt(response.data[0].EffectiveYear);
                        $scope.years = 2020;
                        $scope.keyYear_select = 2020;
                    }
                    else {
                        $scope.years = $scope.dateVariable.getFullYear();
                        $scope.keyYear_select = $scope.dateVariable.getFullYear();
                    }

                    //$scope.keyYear_select = $scope.defaultYear;
                    GetEnrollmentData($scope.years);
                    GetReimburseData($scope.years);
                    GetReimburseQuota($scope.years);
                    GetTextDetail();
                    CheckPeriodSetting($scope.years);
                    CheckAdmin();

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }

            function GetYears() {

                //var URL = CONFIG.SERVER + 'HRFB/GetNextYears';

                var URL = CONFIG.SERVER + 'HRFB/GetNextYearsBenefitManagement';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    angular.forEach(response.data, function (value, key) {
                        $scope.YearsList.push(value.YearName);
                    });
                    /* $scope.prop.value = $scope.keyYear_select; */
                    $scope.prop.value = $scope.dateVariable.getFullYear();

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }

            function getTimeAwareLink() {
                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPA/ShowTimeAwareLink';
                var oRequestParameter = {
                    InputParameter: { "LinkID": 4 }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.IsShowTimeAwareLink = response.data;
                }, function errorCallback(response) {
                });
            }

            function CheckPeriodSetting(key_year) {

                var URL = CONFIG.SERVER + 'HRFB/CheckPeriodSetting';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "KeyYear": key_year}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.CheckPeriod = response.data;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }


            function CheckAdmin() {

                var URL = CONFIG.SERVER + 'HRFB/CheckAdmin';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.loader.enable = false;


                    //เช็ค admin 
                    $scope.RoleAdmin = 0;
                    angular.forEach($scope.employeeData.UserRoles, function (value, key) {
                        var newvalue = value.replace("#", "");

                        if (response.data.indexOf(newvalue) !== -1) {
                            $scope.RoleAdmin = 1;
                        } 
                    });

                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }

            function GetEnrollmentData(key_year) {
                var URL = CONFIG.SERVER + 'HRFB/GetEnrollmentData';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "EmpSubGroup": $scope.requesterData.EmpSubGroup, "RequestNo": 'ALL', "KeyYear": key_year }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.EnrollmentData = response.data;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }

            $scope.getTotal = function () {
                var total = 0;
                for (var i = 0; i < $scope.EnrollmentData.length; i++) {
                    var item = $scope.EnrollmentData[i];
                    total += item.MainBenefitSelected;
                }
                return total;
            }

            function GetReimburseData(key_year) {
                var URL = CONFIG.SERVER + 'HRFB/GetReimburseSelected';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "EmpSubGroup": $scope.requesterData.EmpSubGroup, "RequestNo": 'ALL', "KeyYear": key_year }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.data = response.data;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }

            function GetReimburseQuota(key_year) {
                var URL = CONFIG.SERVER + 'HRFB/GetReimburseQuota';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "EmpSubGroup": $scope.requesterData.EmpSubGroup, "KeyYear": key_year }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.ReimburseQuota = response.data;
                    $scope.check_quota_balance = 0;
                    angular.forEach(response.data, function (value, key) {
                        if (value.AmountReimburse > 0) {
                            $scope.check_quota_balance += value.AmountReimburse;
                        }
                    });

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }

            function GetTextDetail() {
                var URL = CONFIG.SERVER + 'HRFB/GetTextDetail';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {"KeyCode": "FREETEXT" }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.TEXTDetail = response.data;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }

            $scope.openRequest = function (id, companyCode, keyMaster, isOwner, isDataOwner, requestType) {
                $location.path('/frmViewRequest/' + id + '/' + companyCode + '/null/false/true');
            };

            $scope.editRequest = function (id, companyCode, keyMaster, isOwner, isDataOwner, requestType) {
                $location.path('/frmViewRequest/' + id + '/' + companyCode + '/' + keyMaster + '/' + isOwner + '/' + isDataOwner);
            };

            $scope.filterYear = function (key_year) {
                $scope.keyYear_select = key_year;
                GetEnrollmentData(key_year);
                GetReimburseData(key_year);
                GetReimburseQuota(key_year);
                CheckPeriodSetting(key_year);
            };

            $scope.FB_TEXT = $scope.Text["FLEXIBLEBENEFIT"];
            $scope.showAlertReimburse = function () {
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title($scope.FB_TEXT.YEAR_DRAWMONEY + ' ' + $scope.keyYear_select + ' ' + $scope.FB_TEXT.YEAR_DRAWMONEYT)
                        .ok('ตกลง')
                );

                //สิทธิการเบิกปี 2019 ไม่เพียงพอ
            };

            $scope.pagin = {
                currentPage: 1, itemPerPage: '10', numPage: 1
            }
            $scope.filterDatas = function (searchTxt) {
                if (!$scope.data || !$scope.data.length) return [];
                var datas = $filter('filter')($scope.data, searchTxt);
                datas = $filter('filter')(datas, $scope.advacneOptionFilter);

                return datas;
            }
            $scope.genDatas = function (searchTxt) {
                var datas = $scope.filterDatas(searchTxt);
                setNumOfPage(datas);


                var res = [];

                if ($scope.pagin.numPage > 1) {
                    datas.forEach(function (value, i) {
                        var from = (parseInt($scope.pagin.currentPage) - 1) * parseInt($scope.pagin.itemPerPage);
                        var to = from + parseInt($scope.pagin.itemPerPage);

                        if (from <= i && to > i)
                            res.push(value);
                    });
                } else {
                    res = datas.slice();
                }

                return res;
            }
            var setNumOfPage = function (datas) {
                var totalItems = 0;
                if (datas) totalItems = datas.length;
                $scope.pagin.numPage = Math.ceil(totalItems / $scope.pagin.itemPerPage);
                if ($scope.pagin.numPage <= 0) $scope.pagin.numPage = 1;

                if ($scope.pagin.currentPage > $scope.pagin.numPage) $scope.pagin.currentPage = 1;
            }
            $scope.changePage = function (targetPage) {
                $scope.pagin.currentPage = angular.copy(targetPage);
            }
            $scope.getPages = function () {
                var pages = [];

                for (var i = 0; i < $scope.pagin.numPage; i++) {
                    pages.push((i + 1));
                }

                return pages;
            }

            $scope.renderHtml = function (html_code) {
                return $sce.trustAsHtml(html_code);
            };
        }]);
})();