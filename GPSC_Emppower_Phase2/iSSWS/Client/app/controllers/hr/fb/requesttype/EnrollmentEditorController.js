﻿(function () {
    angular.module('ESSMobile')
        .controller('EnrollmentEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter,CONFIG,$mdDialog) {
            //#### FRAMEWORK FUNCTION ### START
            $scope.SelectionItemID = $scope.Text["SelectionItemID"];

            $scope.document.Additional.AgreeStatus = 'false'

            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
            $scope.ChildAction.SetData = function () {
              
            };

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                $scope.document.ErrorText = '';
                $scope.document.Additional.ErrorText = '';
               
   
            };

            var employeeDate = getToken(CONFIG.USER);
            $scope.ChildAction.SetData();
            //#### FRAMEWORK FUNCTION ### END

            //#### OTHERS FUNCTION ### START 

            //เช็ค admin 
            $scope.document.Additional.FBEnrollmentData.RoleAdmin = 0;
            angular.forEach($scope.document.ActionBy.UserRoles, function (value, key) {
                var newvalue = value.replace("#", "");

                if ($scope.document.Additional.FBEnrollmentData.RoleAdminName.indexOf(newvalue) !== -1) {
                    $scope.document.Additional.FBEnrollmentData.RoleAdmin = 1;
                }
            });

            /// pop up
            $scope.showAlert = function (ev) {
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title(ev)
                        .ok('ตกลง')
                );
            };

            /// เช็คค่า ErrorText
            $scope.$watch('document.Additional.ErrorText', function (newValue, oldValue) {

                if (newValue != null) {
                    if (newValue != '') {
                      
                        setTimeout(function () {
                            $scope.showAlert(newValue);
                        }, 300);

                    }
                }

            });


            $scope.CalRate = function () {
              
                var TotalAmount = 0;
              
                for (var i = 0; i < $scope.document.Additional.FBYearlySelection.length; i++) {
                  
                    //if ($scope.document.Additional.FBEnrollmentSelected[i].isMainBenefit) {
                       
                        if ($scope.document.Additional.FBEnrollmentSelected[i].isMainBenefit == false) {
                            //get SelectionItem Rate
                            var SelectionItemID = $scope.document.Additional.FBEnrollmentSelected[i].SelectionItemID;
                            var EmpSubGroup = $scope.document.Additional.EmpSubGroup;
                            var Rate = $scope.document.Additional.FBSelectionItemRate.find(x => x.SelectionItemID == SelectionItemID && x.EmpSubGroup == EmpSubGroup).Rate;
                          
                            TotalAmount += parseFloat(Rate);
                        }
                    //}
                }
                $scope.document.Additional.FBEnrollmentData.FlexPoints = TotalAmount;
            };

            //#### OTHERS FUNCTION ### END
        }]);
})();