﻿(function () {
    angular.module('ESSMobile')
        .controller('EnrollmentViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {

            $scope.EnrollmentData = [];

            $scope.ChildAction.SetData = function () {
                //Do something ...
            };

            $scope.ChildAction.LoadData = function () {
                //Do something ...
            };

            var employeeDate = getToken(CONFIG.USER);
            $scope.ChildAction.SetData();

            $scope.init = function () {
                GetEnrollmentData();
            }

            function GetEnrollmentData(key_year) {
                var URL = CONFIG.SERVER + 'HRFB/GetEnrollmentData';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "EmpSubGroup": $scope.requesterData.EmpSubGroup, "RequestNo": $scope.document.RequestNo, "KeyYear": "" }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.EnrollmentData = response.data;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }

            $scope.getTotal = function () {
                var total = 0;
                for (var i = 0; i < $scope.EnrollmentData.length; i++) {
                    var item = $scope.EnrollmentData[i];
                    total += item.MainBenefitSelected;
                }
                return total;
            }
        }]);
})();