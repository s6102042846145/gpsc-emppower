﻿(function () {
    angular.module('ESSMobile')
        .controller('ReimburseEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {
            //#### FRAMEWORK FUNCTION ### START
            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
            $scope.ChildAction.SetData = function () {
                //Do something ...
                if ($scope.document.Additional.FBReimburseData.Amount == 0) {
                    $scope.document.Additional.FBReimburseData.Amount = '';
                }

                // default when in current year 
                var paths = $location.path().split('/');
                var selYear = paths[paths.length - 1];
                if (selYear == new Date().getFullYear())
                    $scope.document.Additional.FBReimburseData.ReceiptDate = $filter('date')($scope.document.Additional.FBReimburseData.ReceiptDate, 'yyyy-MM-dd');
                else {
                    $scope.document.Additional.FBReimburseData.ReceiptDate = null;
                }
            };

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                $scope.document.ErrorText = '';
                $scope.document.Additional.ErrorText = '';
                $scope.document.Additional.FBReimburseData.ReceiptDate = $filter('date')($scope.document.Additional.FBReimburseData.ReceiptDate, 'yyyy-MM-dd');

                if ($scope.document.Additional.FBReimburseData.Amount == '') {
                    $scope.document.Additional.FBReimburseData.Amount = '0'
                }

                if(!$scope.document.Additional.FBReimburseData.AmountReimburse){
                    $scope.document.Additional.FBReimburseData.AmountReimburse = 0;
                }

            };

            var employeeDate = getToken(CONFIG.USER);
            $scope.ChildAction.SetData();
            //#### FRAMEWORK FUNCTION ### END

            //#### OTHERS FUNCTION ### START 


            //เช็ค admin 
            $scope.document.Additional.RoleAdmin = 0;
            angular.forEach($scope.document.ActionBy.UserRoles, function (value, key) {
                var newvalue = value.replace("#", "");
                if ($scope.document.Additional.RoleAdminName.indexOf(newvalue) !== -1) {
                    $scope.document.Additional.RoleAdmin = 1;
                }
            });


            /// pop up
            $scope.showAlert = function (ev) {
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title(ev)
                        .ok('ตกลง')
                );
            };

            /// เช็คค่า ErrorText
            $scope.$watch('document.Additional.ErrorText', function (newValue, oldValue) {

                if (newValue != null) {
                    if (newValue != '') {

                        setTimeout(function () {
                            $scope.showAlert(newValue);
                        }, 300);

                    }
                }

            });


            $scope.$watch('document.Additional.FBReimburseData.ReceiptDate', function (value) {
                    GetQuotaUse();
            });

            $scope.$watch('document.Additional.FBReimburseData.Amount', function (value) {
                GetQuotaUse();
            });

            function GetQuotaUse()  {

                Year = $filter('date')($scope.document.Additional.FBReimburseData.ReceiptDate, 'yyyy');
                var URL = CONFIG.SERVER + 'HRFB/GetQuotaUse';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "RequestNo": $scope.document.RequestNo, "Year": Year }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.document.Requestor
                    , Creator: getToken(CONFIG.USER)

                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    var QuotaUsed = response.data.split(',')[1];
                    var Quota = response.data.split(',')[0];
                    // เช็คจำนวนเงินที่เบิกเกินกำหนด
                    var QuotaNew = Quota - QuotaUsed;

                    //alert(QuotaUsed + ' ' + Quota + ' ' + QuotaNew);
                    if (QuotaNew <= 0) {
                        $scope.document.Additional.FBReimburseData.AmountReimburse = 0;
                    }
                    else {
                        //ยอดเงินตามที่สามารถเบิกได้จริงตามโควต้า
                        Quota = Quota - QuotaUsed;
                        var AmountReimburse = Quota - $scope.document.Additional.FBReimburseData.Amount;
                        if (AmountReimburse < 0) {
                            $scope.document.Additional.FBReimburseData.AmountReimburse = Quota;
                        }
                        else {
                            $scope.document.Additional.FBReimburseData.AmountReimburse = $scope.document.Additional.FBReimburseData.Amount;
                        }
                    }

                }, function errorCallback(response) {
                    // Error
                    $scope.document.Additional.FBReimburseData.AmountReimburse = 0;
                    console.log('Error GetQuotaUse', response);
                });
             
            }

             $scope.setSelectedDOB = function (selectedDate) {

                 $scope.document.Additional.FBReimburseData.ReceiptDate = $filter('date') (selectedDate, 'yyyy-MM-dd');
            };


            //#### OTHERS FUNCTION ### END
        }]);
})();