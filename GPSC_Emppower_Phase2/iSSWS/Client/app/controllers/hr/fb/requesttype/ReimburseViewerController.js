﻿(function () {
    angular.module('ESSMobile')
        .controller('ReimburseViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {

            $scope.ReimburseData = [];

            $scope.ChildAction.SetData = function () {
                //Do something ...
            };

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                //Do something ...
            };

            var employeeDate = getToken(CONFIG.USER);
            $scope.ChildAction.SetData();

            $scope.init = function () {
                GetReimburseData();
            }

            function GetReimburseData() {
                var URL = CONFIG.SERVER + 'HRFB/GetReimburseSelected';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "EmpSubGroup": $scope.requesterData.EmpSubGroup, "RequestNo": $scope.document.RequestNo, "KeyYear": '' }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.ReimburseData = response.data;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }

        }]);
})();