﻿(function () {
angular.module('ESSMobile')
    .controller('AnnouncementSettingController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {

        $scope.back = function () {
            var url = '/frmViewContent/5001';
            $location.path(url);
        };

        // Get Config Multi Company
        $scope.isHideMultiCompany = true;
        $scope.GetConfingMultiCompany = function () {
            var URL = CONFIG.SERVER + 'Employee/GetConfigMultiCompany';
            var oRequestParameter = {
                InputParameter: {}
                , CurrentEmployee: $scope.employeeData
                , Requestor: $scope.requesterData
                , Creator: getToken(CONFIG.USER)
            };
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {

                if (response.data === 1) {
                    $scope.isHideMultiCompany = false;
                }

                $scope.loader.enable = false;
            }, function errorCallback(response) {
                $scope.loader.enable = false;
            });
        };
        $scope.GetConfingMultiCompany();

        $scope.employeeData = getToken(CONFIG.USER);
        $scope.YearsList = [];
        $scope.years = '';
        $scope.dateVariable = new Date();
        $scope.loader.enable = true;
        $scope.years = $scope.dateVariable.getFullYear() + 1;
        $scope.prop = {
            "type": "select",
            "name": $scope.years,
            "value": $scope.years,
            "values": $scope.YearsList
        };
        $scope.HtmlEditor = '';
        $scope.FlexibleBenefitSettingData = [];
        $scope.SettingID_Delete = '';
        $scope.num = 0;
        $scope.SelectCompanyCode = $scope.employeeData.CompanyCode;


        $scope.GetAuthorizationCompany = function () {
            var URL = CONFIG.SERVER + 'workflow/GetAuthorizationCompany';
            $scope.employeeData = getToken(CONFIG.USER);
            var oRequestParameter = {
                InputParameter: {}
                , CurrentEmployee: $scope.employeeData
                , Requestor: $scope.requesterData
                , Creator: getToken(CONFIG.USER)
            };
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                $scope.CompanyList = response.data;

                if ($scope.CompanyList.length > 0) {
                    $scope.ddlCompany = $scope.CompanyList[0].CompanyCode;
                }

                $scope.loader.enable = false;
            }, function errorCallback(response) {
                $scope.loader.enable = false;
            });
        }
        $scope.GetAuthorizationCompany();


        $scope.changeCompany = function (CompanyCode) {
            $scope.SelectCompanyCode = CompanyCode;
            GetYears();
            GetFlexibleBenefitSettingData($scope.years);
            GETFreeText();
        };


        $scope.init = function () {
            GetYears();
             //TAB1
            GetFlexibleBenefitSettingData($scope.years);
            //TAB2
            GETFreeText();
        }


        //GetYears
        function GetYears() {

            var URL = CONFIG.SERVER + 'HRFB/GetNextYears';
            $scope.employeeData = getToken(CONFIG.USER);
            var oRequestParameter = {
                InputParameter: { "SelectCompany": $scope.SelectCompanyCode }
                , CurrentEmployee: $scope.employeeData
                , Requestor: $scope.requesterData
                , Creator: getToken(CONFIG.USER)
            };
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                $scope.YearsList.splice(0, $scope.YearsList.length);
                angular.forEach(response.data, function (value, key) {
                    $scope.YearsList.push(value.YearName);
                });
                $scope.prop.value = $scope.years;

                $scope.loader.enable = false;
            }, function errorCallback(response) {
                $scope.loader.enable = false;
            });
        }

        $scope.filterYear = function (key_year) {
            $scope.years = key_year;
            GetFlexibleBenefitSettingData(key_year);
        };


         //TAB1
        function GetFlexibleBenefitSettingData(key_year) {
            var URL = CONFIG.SERVER + 'HRFB/GetFlexibleBenefitSetting';
            $scope.employeeData = getToken(CONFIG.USER);
            var oRequestParameter = {
                InputParameter: { "KeyType": "GETDATA", "KeyYear": key_year, "KeyValue": "", "SelectCompany": $scope.SelectCompanyCode }
                , CurrentEmployee: $scope.employeeData
                , Requestor: $scope.requesterData
                , Creator: getToken(CONFIG.USER)
            };

            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                $scope.FlexibleBenefitSettingData = response.data;
                $scope.loader.enable = false;
            }, function errorCallback(response) {
                $scope.loader.enable = false;
            });
        }

        function SaveFlexibleBenefitSettingData(key_year, key_value) {

            var URL = CONFIG.SERVER + 'HRFB/GetFlexibleBenefitSetting';
            $scope.employeeData = getToken(CONFIG.USER);
            var oRequestParameter = {
                InputParameter: { "KeyType": "SAVEDATA", "KeyYear": key_year, "KeyValue": key_value, "SelectCompany": $scope.SelectCompanyCode }
                , CurrentEmployee: $scope.employeeData
                , Requestor: $scope.requesterData
                , Creator: getToken(CONFIG.USER)
            };
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                GetFlexibleBenefitSettingData($scope.years);
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title('บันทึกข้อมูลสำเร็จ!')
                        .ok('ตกลง')
                );

                $scope.loader.enable = false;
            }, function errorCallback(response) {
                GetFlexibleBenefitSettingData($scope.years);
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title('บันทึกข้อมูลไม่สำเร็จ กรุณาลองใหม่อีกครั้ง!')
                        .ok('ตกลง')
                );

                $scope.loader.enable = false;
            });
        }

        $scope.addNew = function () {
            var gen_id = "INSERT_" + $scope.num++;

            $scope.FlexibleBenefitSettingData.push({
                'SettingID': gen_id,
                'BeginDate': "",
                'EndDate': ""
            });
        };

        $scope.remove = function (select_delete) {
            var newDataList = [];

            angular.forEach($scope.FlexibleBenefitSettingData, function (value) {
                if (value.SettingID != select_delete) {
                    newDataList.push(value);
                }

                if (value.SettingID == select_delete) {
                    $scope.SettingID_Delete += select_delete + ",";
                }
            });

            $scope.FlexibleBenefitSettingData = newDataList;
        };

        $scope.onSubmitFlexibleBenefitSetting = function () {

            var save_val = '';
            var Checkdate = 0;

            //UPDATE & ADD
            angular.forEach($scope.FlexibleBenefitSettingData, function (value, key) {
                if (value.SettingID) {
                    save_val += value.SettingID + "|";
                }

                if (value.BeginDate) {
                    save_val += $filter('date')(value.BeginDate, 'yyyy-MM-dd') + "|";
                }

                if (value.EndDate) {
                    save_val += $filter('date')(value.EndDate, 'yyyy-MM-dd') + "|";
                }

                save_val += $scope.years + "|,";

                if (new Date(value.BeginDate) > new Date(value.EndDate)) {
                    Checkdate = 1;
                }

            });

            if (Checkdate == 1) {

                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title('วันที่เริ่มต้นต้องน้อยกว่าวันที่สิ้นสุด!')
                        .ok('ตกลง')
                );

                return false;
            } else {
                SaveFlexibleBenefitSettingData($scope.years, save_val)
            }

        }



        var date = new Date();
        $scope.GetDate = ('0' + date.getDate()).slice(-2) + '/' + ('0' + (date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear();
        $scope.setSelected = function (selectedDate, type, id) {
            $scope.FlexibleBenefitSettingData.forEach(function (v) {

                if (type == 'BeginDate') {
                    if (v.SettingID == id) v.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
                }

                if (type == 'EndDate') {
                    if (v.SettingID == id) v.EndDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
                }
            });
        };


        //TAB2
        $scope.options = {
            language: 'en',
            allowedContent: true,
            entities: false
        };

        function GETFreeText() {
            var URL = CONFIG.SERVER + 'HRFB/GETFreeText';
            $scope.employeeData = getToken(CONFIG.USER);
            var oRequestParameter = {
                InputParameter: { "KeyType": "GETDATA", "KeyCode": "FREETEXT", "KeyValue": "", "SelectCompany": $scope.SelectCompanyCode }
                , CurrentEmployee: $scope.employeeData
                , Requestor: $scope.requesterData
                , Creator: getToken(CONFIG.USER)
            };

            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                $scope.HtmlEditor = '';
                angular.forEach(response.data, function (value, key) {
                    if (value.Value) {
                        $scope.HtmlEditor = value.Value;
                    }
                });

                $scope.loader.enable = false;
            }, function errorCallback(response) {
                $scope.loader.enable = false;
            });
        }

        $scope.onSubmitTextEditor = function () {
            var URL = CONFIG.SERVER + 'HRFB/GETFreeText';
            $scope.employeeData = getToken(CONFIG.USER);
            var oRequestParameter = {
                InputParameter: { "KeyType": "SAVEDATA", "KeyCode": "FREETEXT", "KeyValue": $scope.HtmlEditor, "SelectCompany": $scope.SelectCompanyCode }
                , CurrentEmployee: $scope.employeeData
                , Requestor: $scope.requesterData
                , Creator: getToken(CONFIG.USER)
            };
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {

                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title('บันทึกข้อมูลสำเร็จ!')
                        .ok('ตกลง')
                );

                $scope.loader.enable = false;
            }, function errorCallback(response) {
                $scope.loader.enable = false;
            });
        }

    }]);
})();