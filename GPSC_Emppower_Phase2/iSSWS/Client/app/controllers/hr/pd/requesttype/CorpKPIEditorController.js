﻿(function () {
    angular.module('ESSMobile')
        .controller('CorpKPIEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$q', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $q, $mdDialog) {
            $scope.Textcategory = 'KPICORP';
            $scope.content.HeaderText = $scope.Text[$scope.Textcategory].TITLE;
            $scope.content.Header = $scope.Text[$scope.Textcategory].TITLE;

            $scope.set_KPITypeID = "";
            $scope.KPItypeName = "";
            $scope.ActivityGroupID = '';
            $scope.ActivityTypeID = '';
            $scope.ActivityID = '';
            $scope.KPITypeName = "";
            $scope.IndicatorName = "";
            var kpitype_value = "";

     

            $scope.column_header = [
              { text: 'No', width: '50px' },
              { text: $scope.Text[$scope.Textcategory].TYPE, width: '200px' },
              { text: $scope.Text[$scope.Textcategory].OBJECTTIVENAME, width: '400px' },
              { text: $scope.Text[$scope.Textcategory].LINKMESURE, width: '400px' },
              { text: $scope.Text[$scope.Textcategory].MEASURENAME, width: '400px' },
              { text: $scope.Text[$scope.Textcategory].WEIGHT, width: '120px' },
              { text: 'Rating', width: '120px' },
            ];

            var url = $location.path();
            var newString = url.replace("/", "");

            //select KPIType
            $scope.KPIChage = function (obj) {
                $scope.KPItypeID = obj.id;
                $scope.KPItypeName = obj.text;
            }
            // insert KPIEditor start
            $scope.data = {
                selectedKPI: [],
                selectedList: []
            }
            $scope.ChildAction.SetData = function () {
                //$scope.Page = CONFIG.PAGE_SETTING.TRAVEL_REPORT;
                //$scope.Textcategory = 'EXPENSE';
                //$scope.content.Header = $scope.Text['EXPENSE']['TRAVEL_REPORT'];
                //$scope.Textcategory2 = 'TRAVELREQUEST';
                //$scope.ActivityGroupID = $routeParams.otherParam;
            }

            $scope.ChildAction.LoadData = function () {
                $scope.document.Additional.Contents = [];
                if ($scope.document.RequestNo == 'DUMMY') {
                    //Save
                    $scope.ActivityGroupID = $routeParams.otherParam.substr(0, 1);
                    $scope.ActivityTypeID = $routeParams.otherParam.substr(2, 1);
                    $scope.ActivityID = $routeParams.otherParam.substr(4, 1);
                    $scope.CurrentEmployee = getToken(CONFIG.USER);
                    $scope.RequestorNo = $scope.CurrentEmployee.EmployeeID;

                    $scope.document.Additional.ActivityGroupID = $scope.ActivityGroupID;
                    $scope.document.Additional.ActivityTypeID = $scope.ActivityTypeID;
                    $scope.document.Additional.ActivityID = $scope.ActivityID;
                } else
                {
                    //Load Page
                    if ($scope.document.Additional.CorpKPISetting.length > 0) {
                        $scope.ActivityGroupID = $scope.document.Additional.ActivityGroupID;
                        $scope.ActivityTypeID = $scope.document.Additional.ActivityTypeID;
                        $scope.ActivityID = $scope.document.Additional.ActivityID;
                        $scope.RequestorNo = $scope.document.Additional.RequestorNo;
                    }
                }

                if ($scope.data.selectedKPI.length > 0) {
                    for (i = 0; i <= $scope.data.selectedKPI.length - 1; i++) {
                        var Contents = {
                            ActivityGroupID: $scope.ActivityGroupID,
                            ActivityTypeID: $scope.ActivityTypeID,
                            ActivityID: $scope.ActivityID,
                            ContentID: 0,
                            KPITypeID: $scope.data.selectedKPI[i].Content.KPITypeID,
                            KPITypeName: $scope.GetKPITypeName($scope.data.selectedKPI[i].Content.KPITypeID),
                            ObjectiveID: $scope.data.selectedKPI[i].ObjectiveID,
                            ObjectiveName: $scope.data.selectedKPI[i].ObjectiveName,
                            IndicatorID: $scope.data.selectedKPI[i].IndicatorID,
                            IndicatorName: $scope.data.selectedKPI[i].Content.IndicatorName,
                            LinkIndicatorName: $scope.GetLinkIndicatorName($scope.data.selectedKPI[i].Content.LinkIndicatorID),
                            Weight: $scope.data.selectedKPI[i].Content.Weight,
                            Rating: $scope.data.selectedKPI[i].Content.Rating,
                            Description: $scope.data.selectedKPI[i].Content.Description,

                            AttValue01: $scope.data.selectedKPI[i].Content.AttValue01,
                            AttValue02: $scope.data.selectedKPI[i].Content.AttValue02,
                            AttValue03: $scope.data.selectedKPI[i].Content.AttValue03,
                            AttValue04: $scope.data.selectedKPI[i].Content.AttValue04,
                            AttValue05: $scope.data.selectedKPI[i].Content.AttValue05,
                            AttValue06: $scope.data.selectedKPI[i].Content.AttValue06,
                            AttValue07: $scope.data.selectedKPI[i].Content.AttValue07,
                            AttValue08: $scope.data.selectedKPI[i].Content.AttValue08,
                            AttValue09: $scope.data.selectedKPI[i].Content.AttValue09,
                            AttValue10: $scope.data.selectedKPI[i].Content.AttValue10,
                            AttValue11: $scope.data.selectedKPI[i].Content.AttValue11,
                            AttValue12: $scope.data.selectedKPI[i].Content.AttValue12,
                            AttValue13: $scope.data.selectedKPI[i].Content.AttValue13,
                            AttValue14: $scope.data.selectedKPI[i].Content.AttValue14,
                            AttValue15: $scope.data.selectedKPI[i].Content.AttValue15,

                            AttText01: $scope.GetAttributeText($scope.data.selectedKPI[i].Content, "AttValue01"),
                            AttText02: $scope.GetAttributeText($scope.data.selectedKPI[i].Content, "AttValue02"),
                            AttText03: $scope.GetAttributeText($scope.data.selectedKPI[i].Content, "AttValue03"),
                            AttText04: $scope.GetAttributeText($scope.data.selectedKPI[i].Content, "AttValue04"),
                            AttText05: $scope.GetAttributeText($scope.data.selectedKPI[i].Content, "AttValue05"),
                            AttText06: $scope.GetAttributeText($scope.data.selectedKPI[i].Content, "AttValue06"),
                            AttText07: $scope.GetAttributeText($scope.data.selectedKPI[i].Content, "AttValue07"),
                            AttText08: $scope.GetAttributeText($scope.data.selectedKPI[i].Content, "AttValue08"),
                            AttText09: $scope.GetAttributeText($scope.data.selectedKPI[i].Content, "AttValue09"),
                            AttText10: $scope.GetAttributeText($scope.data.selectedKPI[i].Content, "AttValue10"),
                            AttText11: $scope.GetAttributeText($scope.data.selectedKPI[i].Content, "AttValue11"),
                            AttText12: $scope.GetAttributeText($scope.data.selectedKPI[i].Content, "AttValue12"),
                            AttText13: $scope.GetAttributeText($scope.data.selectedKPI[i].Content, "AttValue13"),
                            AttText14: $scope.GetAttributeText($scope.data.selectedKPI[i].Content, "AttValue14"),
                            AttText15: $scope.GetAttributeText($scope.data.selectedKPI[i].Content, "AttValue15"),
                        }
                        $scope.document.Additional.Contents.push(Contents);
                        $scope.document.Additional.RequestorNo = $scope.document.CreatorNo;
                        $scope.document.Additional.RequestNo = $scope.document.RequestNo;
                    }
                }
            }


            $scope.ChildAction.LoadData();


            /* ====== wizard form control ====== */
            $scope.wizardFormControl = {
                receiptIsActive: false,
                withoutReceiptIsActive: false
            };
            /* ====== !wizard form control ====== */


            //init
            $scope.InitialConfig = function () {
                $scope.loader.enable = true;
                //var promise_service0 = $scope.getTextByCompany($scope.document.Requestor);
                //var promise_service1 = InitialConfig();
                //var promise_service2 = $scope.getMaster();
                //var promise_service3 = $scope.GetMasterdata();
                //$q.all([promise_service0, promise_service1, promise_service2, promise_service3]).then(function (response) {

                //    $scope.data.model.ready = true;
                //}, function (response) {

                //    console.error('init error');

                //})

            }

            $scope.GetMasterUI = function () {
                var URL = CONFIG.SERVER + 'HRPD/GetCorpKPISettingUICreator';
                var oRequestParameter = { InputParameter: { "ActivityGroupID": $scope.ActivityGroupID, "ActivityTypeID": $scope.ActivityTypeID, "ActivityID": $scope.ActivityID,"RequestorNo":$scope.RequestorNo }, CurrentEmployee: getToken(CONFIG.USER) }
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.masterKPISettingUI = [];
                    $scope.masterKPIType = [];
                    $scope.masterIndicator = [];
                    $scope.masterKPISettingUI = response.data.KPISettingUI;
                    $scope.masterKPIAttributeSetting = response.data.KPIAttributeManagement;
                    $scope.masterKPIType = response.data.KPIType;
                    $scope.masterIndicator = response.data.KPILinkIndicatorUI;
                    $scope.column_header_att = $scope.masterKPIAttributeSetting;
                    $scope.LinkIndicatorName = $scope.masterIndicator[0].Name;
                    $scope.data.Activities = response.data.Activities;
                    if ($scope.masterKPISettingUI.KPIDataCheckList.length > 0) {
                        for (i = 0; i <= $scope.masterKPISettingUI.KPIDataCheckList[0].Objective.length - 1; i++) {
                            for (j = 0; j <= $scope.masterKPISettingUI.KPIDataCheckList[0].Objective[i].Indicator.length - 1; j++) {
                                if ($scope.masterKPISettingUI.KPIDataCheckList[0].Objective[i].Indicator[j].Selected == true) {
                                    $scope.data.selectedKPI.push($scope.masterKPISettingUI.KPIDataCheckList[0].Objective[i].Indicator[j]);
                                    $scope.data.selectedList.push($scope.masterKPISettingUI.KPIDataCheckList[0].Objective[i].Indicator[j]);
                                }
                            }
                        }
                    }

                    $scope.GetDefaultKPIType();
                }, function errorCallback(response) {
                    console.log('error Corp KPISetting InitialConfig.', response);
                });
            }
            $scope.GetMasterUI();

            $scope.GetKPITypeName = function (obj) {
                for (j = 0; j <= $scope.masterKPIType.length - 1; j++) {
                    if ($scope.masterKPIType[j].KPITypeID == obj) {
                        $scope.KPITypeName = $scope.masterKPIType[j].Name;
                    }
                }
                return $scope.KPITypeName;
            }
            $scope.GetLinkIndicatorName = function (obj) {
                $scope.LinkIndicatorName = "";
                for (j = 0; j <= $scope.masterIndicator.length - 1; j++) {
                    if ($scope.masterIndicator[j].IndicatorID == obj) {
                        $scope.LinkIndicatorName = $scope.masterIndicator[j].Name;
                    }
                }
                return $scope.LinkIndicatorName;
            }
            $scope.GetAttributeText = function (obj, AttributeValue) {
                $scope.AttributeValueText = "";
                for (k = 0; k <= $scope.masterKPIAttributeSetting.length - 1; k++) {
                    $scope.iCheck = false;
                    if ($scope.masterKPIAttributeSetting[k].Attribute.FieldValue == AttributeValue) {
                        if ($scope.masterKPIAttributeSetting[k].AttributeController.ControllerType == "LIST") {
                            for (g = 0; g <= $scope.masterKPIAttributeSetting[k].AttributeController.ContollerValues.length - 1; g++) {
                                if ($scope.masterKPIAttributeSetting[k].AttributeController.ContollerValues[g].ValueID == obj[AttributeValue]) {
                                    $scope.AttributeValueText = $scope.masterKPIAttributeSetting[k].AttributeController.ContollerValues[g].Name;
                                    $scope.iCheck = true;
                                    break;
                                }
                            }
                        }
                        else if ($scope.masterKPIAttributeSetting[k].AttributeController.ControllerType == "TEXT") {
                            $scope.AttributeValueText = obj[AttributeValue];
                            $scope.iCheck = true;
                            break;
                        }
                    }
                    if ($scope.iCheck == true)
                        break;
                }
                return $scope.AttributeValueText;
            }

            function InitialConfig() {
                $scope.setBlockAction(true, $scope.Text["SYSTEM"]["PROCESSING"]);
                $scope.Page = "TravelExpense";
                console.log('InitialConfig');

            }
            $scope.getIsConfigKPI = function (oKPIId, iNum) {
                for (i = 0; i <= $scope.masterKPIType.length - 1 ; i++) {
                    if ($scope.masterKPIType[i].KPITypeID == oKPIId) {
                        $scope.data.selectedKPI[iNum].Content.IsDefaultSelected = $scope.masterKPIType[i].IsDefaultSelected;
                        $scope.data.selectedKPI[iNum].Content.IsWeightReadonly = $scope.masterKPIType[i].IsWeightReadonly;
                        $scope.data.selectedKPI[iNum].Content.IsRatingReadonly = $scope.masterKPIType[i].IsRatingReadonly;
                        $scope.data.selectedKPI[iNum].Content.IsAttributeReadonly = $scope.masterKPIType[i].IsAttributeReadonly;
                        break;
                    }
                }
            }
            /* ====== Inital data before load control (Finish)====== */

            // promtDialog text
            $scope.promtDialog = function (ev, ObjectiveID,IndicatorID, columnName) {
                var element = $.grep($scope.data.selectedKPI, function (element, index) { return (element.ObjectiveID == ObjectiveID && element.IndicatorID == IndicatorID); })[0];
                $mdDialog.show({
                    controller: promtDialogCtrl,
                    templateUrl: 'views/common/dialog/textInput.tmpl.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    locals: {
                        params: {
                            name: columnName,
                            placeholder: 'Enter ' + columnName,
                            defaultValue: element,
                            maxlength: 800
                        }
                    },
                })
               .then(function (answer) {
                   if (columnName == "IndicatorName") {
                       element.Content.IndicatorName = answer;
                   }
                   else if (columnName == "Description") {
                       element.Content.Description = answer;
                   }
               }, function () {

               });
            };

            function promtDialogCtrl($scope, $mdDialog, params) {
                $scope.params = params;
                if (params.name == "IndicatorName") {
                    $scope.inputValue = params.defaultValue.Content.IndicatorName;
                }
                else if (params.name == "Description") {
                    $scope.inputValue = params.defaultValue.Content.Description;
                }
                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.save = function (answer) {
                    if (answer.length < params.maxlength) {
                        $mdDialog.hide(answer);
                    }
                };
            }


            // dialog KPI Setting
            $scope.selectKPI = function (ev) {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: 'views/common/dialog/dialog1.tmpl.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    locals: {
                        params: {
                            title: 'KPI content',
                            KPIDataCheckList: $scope.masterKPISettingUI.KPIDataCheckList,
                            selectedKPI: $scope.data.selectedList,
                        }
                    },
                })
                .then(function (selectedKPI) {
                    $scope.data.selectedKPI = angular.copy(selectedKPI);
                    $scope.SetContextKPIDefault($scope.data.selectedKPI);
                }, function () {

                });

            }
            $scope.SetAttributeDefault = function (AttributeID, GetType) {
                var result = "";
                var attr = $.grep($scope.masterKPIAttributeSetting, function (element, index) { return (element.AttributeID == AttributeID); });
                if (attr) {
                    if (attr.length > 0 && attr[0].AttributeController  && attr[0].AttributeController.ContollerValues.length > 0) {

                            if (GetType == "value") {
                               
                                result = attr[0].AttributeController.ContollerValues[0].ValueID;
                            }
                            else {
                                result = attr[0].AttributeController.ContollerValues[0].Name;
                            }         
                   }               
                }
                return result
            }
            $scope.GetDefaultKPIType = function()
            {
                if($scope.masterKPIType.length > 0)
                {
                    for(i=0;i<= $scope.masterKPIType.length-1;i++)
                    {
                        if($scope.masterKPIType[i].IsDefaultSelected==true)
                        {
                            $scope.DefaultKPITYpeID = $scope.masterKPIType[i].KPITypeID;
                            $scope.DefaultKPITYpeName = $scope.masterKPIType[i].Name;
                            break;
                        }
                    }
                }
            }

            $scope.SetContextKPIDefault = function (objContent) {
                for (i = 0; i <= $scope.data.selectedKPI.length - 1; i++) {
                    if ($scope.data.selectedKPI[i].Content == null) {
                        $scope.data.selectedKPI[i].Content = [];
                        var Content = {
                            ActivityGroupID: objContent[i].ActivityGroupID,
                            ActivityTypeID: objContent[i].ActivityTypeID,
                            ActivityID: objContent[i].ActivityID,
                            RequestNo: '',
                            ContentID: 0,
                            KPITypeID: $scope.DefaultKPITYpeID,
                            KPITypeName: $scope.DefaultKPITYpeName,
                            ObjectiveID: objContent[i].ObjectiveID,
                            ObjectiveName: objContent[i].ObjectiveName,
                            IndicatorID: objContent[i].IndicatorID,
                            IndicatorName: objContent[i].IndicatorName,
                            LinkIndicatorName: "",
                            Weight: 0.0,
                            Rating: 0.0,
                            Description: '',
                            IsDefaultSelected: false,
                            IsWeightReadonly: false,
                            IsRatingReadonly: false,
                            IsAttributeReadonly: false,

                            AttValue01: $scope.SetAttributeDefault(1, 'value'),
                            AttValue02: $scope.SetAttributeDefault(2, 'value'),
                            AttValue03: $scope.SetAttributeDefault(3, 'value'),
                            AttValue04: $scope.SetAttributeDefault(4, 'value'),
                            AttValue05: $scope.SetAttributeDefault(5, 'value'),
                            AttValue06: $scope.SetAttributeDefault(6, 'value'),
                            AttValue07: $scope.SetAttributeDefault(7, 'value'),
                            AttValue08: $scope.SetAttributeDefault(8, 'value'),
                            AttValue09: $scope.SetAttributeDefault(9, 'value'),
                            AttValue10: $scope.SetAttributeDefault(10, 'value'),
                            AttValue11: $scope.SetAttributeDefault(11, 'value'),
                            AttValue12: $scope.SetAttributeDefault(12, 'value'),
                            AttValue13: $scope.SetAttributeDefault(13, 'value'),
                            AttValue14: $scope.SetAttributeDefault(14, 'value'),
                            AttValue15: $scope.SetAttributeDefault(15, 'value'),

                            AttText01: $scope.SetAttributeDefault(1, 'text'),
                            AttText02: $scope.SetAttributeDefault(2, 'text'),
                            AttText03: $scope.SetAttributeDefault(3, 'text'),
                            AttText04: $scope.SetAttributeDefault(4, 'text'),
                            AttText05: $scope.SetAttributeDefault(5, 'text'),
                            AttText06: $scope.SetAttributeDefault(6, 'text'),
                            AttText07: $scope.SetAttributeDefault(7, 'text'),
                            AttText08: $scope.SetAttributeDefault(8, 'text'),
                            AttText09: $scope.SetAttributeDefault(9, 'text'),
                            AttText10: $scope.SetAttributeDefault(10, 'text'),
                            AttText11: $scope.SetAttributeDefault(11, 'text'),
                            AttText12: $scope.SetAttributeDefault(12, 'text'),
                            AttText13: $scope.SetAttributeDefault(13, 'text'),
                            AttText14: $scope.SetAttributeDefault(14, 'text'),
                            AttText15: $scope.SetAttributeDefault(15, 'text'),
                        }
                        $scope.data.selectedKPI[i].Content = Content;
                    }
                }
            }

            function DialogController($scope, $mdDialog, params) { // start controller
                $scope.params = params;
                $scope.selectedList = params.selectedKPI;

                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.save = function (answer) {
                    $mdDialog.hide(answer);
                };

                $scope.selectGroupKPI = function (items) {
                    if (!items) return;
                    var foundInList = false;
                    var count = 0;
                    for (var i = 0; i < items.length; i++) {
                        if (isSelected(items[i])) {
                            foundInList = true;
                            count++;
                        }
                    }
                    if (count < items.length) {
                        for (var i = 0; i < items.length; i++) {
                            if (!isSelected(items[i]) && !items[i].disable) {
                                $scope.selectedList.push(items[i]);
                            }
                        }
                    } else {
                        for (var i = 0; i < items.length; i++) {
                            var idx = $scope.selectedList.indexOf(items[i]);
                            if (idx > -1 && !items[i].disable) {
                                $scope.selectedList.splice(idx, 1);
                            }
                        }
                    }
                }
                $scope.selectGroupsKPI = function (items) {
                    if (!items) return;
                    var count = 0;
                    var length = 0;
                    for (var i = 0; i < items.length; i++) {
                        for (var a = 0; a < items[i].Indicator.length; a++) {
                            if (isSelected(items[i].Indicator[a])) {
                                count++;
                            }
                            length++;
                        }
                    }

                    if (count < length) {
                        for (var i = 0; i < items.length; i++) {
                            for (var a = 0; a < items[i].Indicator.length; a++) {
                                if (!isSelected(items[i].Indicator[a]) && !items[i].Indicator[a].disable) {
                                    $scope.selectedList.push(items[i].Indicator[a]);
                                }
                            }
                        }
                    } else {
                        for (var i = 0; i < items.length; i++) {
                            for (var a = 0; a < items[i].Indicator.length; a++) {
                                var idx = $scope.selectedList.indexOf(items[i].Indicator[a]);
                                if (idx > -1 && !items[i].Indicator[a].disable) {
                                    $scope.selectedList.splice(idx, 1);
                                }
                            }

                        }
                    }
                }
                $scope.selectKPI = function (item) {
                    if (!item) return;
                    var idx = $scope.selectedList.indexOf(item);
                    if (idx > -1) {
                        item.selected = true;
                        $scope.selectedList.splice(idx, 1);
                    }
                    else {
                        item.selected = true;
                        $scope.selectedList.push(item);
                    }
                }
                $scope.exists = function (item) {
                    if (!item) return;
                    return $scope.selectedList.indexOf(item) > -1;
                };
                $scope.exists_group = function (items) {
                    if (!items) return;
                    var count = 0;
                    for (var i = 0; i < items.length; i++) {
                        if (isSelected(items[i])) {
                            count++;
                        }
                    }
                    return count == items.length;
                }
                $scope.exists_groups = function (items) {
                    if (!items) return;
                    var count = 0;
                    var length = 0;
                    for (var i = 0; i < items.length; i++) {
                        for (var a = 0; a < items[i].Indicator.length; a++) {
                            if (isSelected(items[i].Indicator[a])) {
                                count++;
                            }
                            length++;
                        }
                    }
                    return count == length;
                }
                $scope.isIndeterminate = function (items) {
                    if (!items) return;
                    var count = 0;
                    for (var i = 0; i < items.length; i++) {
                        if (isSelected(items[i])) {
                            count++;
                        }
                    }
                    return count < items.length && count != 0;
                }
                $scope.isIndeterminateGroupKPI = function (items) {
                    if (!items) return;
                    var count = 0;
                    var length = 0;
                    for (var i = 0; i < items.length; i++) {
                        for (var a = 0; a < items[i].Indicator.length; a++) {
                            if (isSelected(items[i].Indicator[a])) {
                                count++;
                            }
                            length++;
                        }
                    }
                    return count < length && count != 0;
                }

                function isSelected(item) {
                    if (!item) return;

                    for (var i = 0; i < $scope.selectedList.length; i++) {
                        if (item.ObjectiveID === $scope.selectedList[i].ObjectiveID && item.IndicatorID == $scope.selectedList[i].IndicatorID) {
                            return true;
                        }
                    }
                    return false;
                }
            } // end controller

            // insert KPIEditor end

        }])
        .directive('expand', function () {
            return {
                restrict: 'A',
                controller: ['$scope', function ($scope) {
                    $scope.$on('onExpandAll', function (event, args) {
                        $scope.expanded = args.expanded;
                    });
                }]
            }; 
        });
})();
