﻿(function () {
    angular.module('ESSMobile')
        .controller('Part_2KPIEvaluationController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$q', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $q, $mdDialog) {
            var url = $location.path();

            // insert KPIEditor start
            $scope.ChildAction.SetData = function () {
                
            }
            $scope.ChildAction.SetData();

            $scope.ChildAction.LoadData = function () {
                
            }
            $scope.ChildAction.LoadData();

            $scope.CalculateWeight = function(obj,No,mode)
            {
                $scope.SumPerformanceScore = 0;
                if (mode == "Approver")
                {
                    if (obj.Performance_Approver.Rating > $scope.data.MaxScaleKPI) {
                        obj.Performance_Approver.Rating = $scope.data.MaxScaleKPI;
                    }
                    
                    for (i = 0; i <= $scope.document.Additional.KPIPerformance.length - 1; i++) {
                        if ($scope.document.Additional.KPIPerformance[i].No == No) {
                            $scope.document.Additional.KPIPerformance[i].Performance_Approver.WeightScore = ($scope.document.Additional.KPIPerformance[i].Weight * obj.Performance_Approver.Rating) / $scope.data.MaxWeigthKPI;
                        }
                        $scope.SumPerformanceScore = $scope.SumPerformanceScore + $scope.document.Additional.KPIPerformance[i].Performance_Approver.WeightScore;
                    }
                }
                else if (mode == "Requestor")
                {
                    if (obj.Performance_Request.Rating > $scope.data.MaxScaleKPI) {
                        obj.Performance_Request.Rating = $scope.data.MaxScaleKPI;
                    }
                    for (i = 0; i <= $scope.document.Additional.KPIPerformance.length - 1; i++) {
                        if ($scope.document.Additional.KPIPerformance[i].No == No) {
                            $scope.document.Additional.KPIPerformance[i].Performance_Request.WeightScore = ($scope.document.Additional.KPIPerformance[i].Weight * obj.Performance_Request.Rating) / $scope.data.MaxWeigthKPI;
                        }
                        $scope.SumPerformanceScore = $scope.SumPerformanceScore + $scope.document.Additional.KPIPerformance[i].Performance_Request.WeightScore;
                    }
                }

                $scope.data.PerformanceScore = ($scope.SumPerformanceScore * $scope.data.BaseScale) / $scope.data.MaxScaleKPI;
                $scope.document.Additional.Score.PerformanceScore = $scope.data.PerformanceScore;
                $scope.data.SumTotalWeight = parseFloat($scope.data.TotalWeightKPI) + parseFloat($scope.data.TotalWeightBehavior);
                
                $scope.data.PScore = ($scope.document.Additional.Score.PerformanceScore / $scope.data.BaseScale) * $scope.data.TotalWeightBehavior;
                $scope.data.BScore = ($scope.document.Additional.Score.BehaviorScore / $scope.data.BaseScale) * $scope.data.TotalWeightBehavior;
                $scope.document.Additional.Score.TotalScore = (($scope.data.PScore + $scope.data.BScore) / $scope.data.SumTotalWeight) * $scope.data.BaseScale;
            }

         
            $scope.promtDialog = function (ev, index, columnName,paramName) {
                $mdDialog.show({
                    controller: promtDialogCtrl,
                    templateUrl: 'views/common/dialog/textInput.tmpl.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    locals: {
                        params: {
                            name: columnName,
                            placeholder: 'Enter ' + columnName,
                            defaultValue: $scope.document.Additional.KPIPerformance[index][columnName],
                            maxlength: 800
                        }
                    },
                })
               .then(function (answer) {
                   $scope.document.Additional.KPIPerformance[index][columnName] = answer;
                 
               }, function () {

               });
            };
            function promtDialogCtrl($scope, $mdDialog, params) {
                $scope.params = params;
       
                $scope.inputValue = params.defaultValue;
                $scope.hide = function () {
                    $mdDialog.hide();
                };
                $scope.cancel = function () {
                    $mdDialog.cancel();
                };
                $scope.save = function (answer) {
                    if (answer.length < params.maxlength) {
                        $mdDialog.hide(answer);
                    }
                };
            }
          
            $scope.promtDialogArchiement = function (ev, index, columnName, paramName, table_index, obj) {
                $mdDialog.show({
                    controller: DialogControllerArchiement,
                    templateUrl: 'views/common/dialog/achievementSelect.tmpl.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    locals: {
                        params: {
                            title: 'AcievementEditorDialog',
                            data: $scope.data.AchievementAllActivity,
                            columns: $scope.data.behavior_col_acheiment,
                        }
                    },
                })
                .then(function (item) {
                    obj.Achievement = {
                        AchievementType: item.AchievementType,
                        ArchievementID: item.AchievementID,
                        ArchievementRequestNo: item.RequestNo,
                        Action: item.Action,
                        Result: item.Result,
                        Situation: item.Situation,
                        Spirit: item.Spirit};
                }, function () {

                });
            };
            function DialogControllerArchiement($scope, $mdDialog, params) {
                $scope.data = angular.copy(params.data);
                $scope.columns = angular.copy(params.columns);
                $scope.selectedIndex;

                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.save = function (answer) {
                    $mdDialog.hide(answer);
                };
            }


        }])
        .directive('expand', function () {
            return {
                restrict: 'A',
                controller: ['$scope', function ($scope) {
                    $scope.$on('onExpandAll', function (event, args) {
                        $scope.expanded = args.expanded;
                    });
                }]
            }; z
        });
})();
