﻿
(function () {
    angular.module('ESSMobile')
        .controller('Part_3BehaviorSPIRITController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$q', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $q, $mdDialog) {
            $scope.TextCategory = 'EVALBEHAVIOR'
            var url = $location.path();
            $scope.SumBehavior = 0;

            $scope.CalculateWeight = function (obj,mode)
            {
                $scope.SumBehavior = 0;
                for (i = 0; i <= obj.LevelScore.length - 1; i++)
                {
                    if (mode == "Requestor") {
                        if (obj.LevelScore[i].LevelID == obj.Score_Requester.LevelID) {
                            obj.Score_Requester.LevelName = obj.LevelScore[i].LevelName;
                            obj.Score_Requester.Score = obj.LevelScore[i].Score;
                        }
                    }
                    else
                    {
                        if (obj.LevelScore[i].LevelID == obj.Score_Approver.LevelID) {
                            obj.Score_Approver.LevelName = obj.LevelScore[i].LevelName;
                            obj.Score_Approver.Score = obj.LevelScore[i].Score;
                        }
                    }
                }

                for (i = 0; i <= $scope.document.Additional.BehaviorGroup.length - 1; i++)
                {
                    for (j = 0; j <= $scope.document.Additional.BehaviorGroup[i].Behavior.length - 1;j++)
                    {
                        if (mode == "Requestor")
                            $scope.SumBehavior = $scope.SumBehavior + $scope.document.Additional.BehaviorGroup[i].Behavior[j].Score_Requester.Score;
                        else
                            $scope.SumBehavior = $scope.SumBehavior + $scope.document.Additional.BehaviorGroup[i].Behavior[j].Score_Approver.Score;
                    }
                }
                
                $scope.data.BehaviorScore = ($scope.SumBehavior * $scope.data.BaseScale) / $scope.data.TotalWeightBehavior;
                $scope.document.Additional.Score.BehaviorScore = $scope.data.BehaviorScore;
                $scope.data.SumTotalWeight = parseInt($scope.data.TotalWeightKPI) + parseInt($scope.data.TotalWeightBehavior);

                $scope.data.PScore = ($scope.document.Additional.Score.PerformanceScore / $scope.data.BaseScale) * $scope.data.TotalWeightBehavior;
                $scope.data.BScore = ($scope.document.Additional.Score.BehaviorScore / $scope.data.BaseScale) * $scope.data.TotalWeightBehavior;

                $scope.document.Additional.Score.TotalScore = (($scope.data.PScore + $scope.data.BScore) / $scope.data.SumTotalWeight) * $scope.data.BaseScale;
            }

            $scope.promtDialog = function (ev, index, columnName, paramName,table_index,obj) {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: 'views/common/dialog/achievementSelect.tmpl.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    locals: {
                        params: {
                            title: 'AcievementEditorDialog',
                            data: $scope.data.AchievementAllActivity,
                            columns: $scope.data.behavior_col_acheiment,
                           // achivementTypes: $scope.achivementTypes
                        }
                    },
                })
                .then(function (item) {
                    obj.Achievement = {
                        AchievementType: item.AchievementType,
                        ArchievementID: item.AchievementID,
                        ArchievementRequestNo: item.RequestNo,
                        Action: item.Action,
                        Result: item.Result,
                        Situation: item.Situation,
                        Spirit: item.Spirit
                    };
                }, function () {

                });
            };

            function DialogController($scope, $mdDialog, params) { // start controller
                $scope.data = angular.copy(params.data);
                $scope.columns = angular.copy(params.columns);
                $scope.selectedIndex;

                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.save = function (answer) {
                    // attribute to blind in the table
                    //answer.achievementTypeItem = getAchievementByID(answer.Type);
                    $mdDialog.hide(answer);
                };
            }
        }])
        .directive('expand', function () {
            return {
                restrict: 'A',
                controller: ['$scope', function ($scope) {
                    $scope.$on('onExpandAll', function (event, args) {
                        $scope.expanded = args.expanded;
                    });
                }]
            }; z
        });
})();
