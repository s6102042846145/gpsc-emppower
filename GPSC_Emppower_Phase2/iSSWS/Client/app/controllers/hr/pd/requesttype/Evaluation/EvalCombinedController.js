﻿(function () {
    angular.module('ESSMobile')
        .controller('EvalCombinedController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$q', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $q, $mdDialog) {
            $scope.Textcategory = 'EVALKPI';
            $scope.content.HeaderText = $scope.Text[$scope.Textcategory].TITLE;
            $scope.Header = $scope.Text[$scope.Textcategory].TITLE;
                        
            var url = $location.path();

            $scope.data = {
                isDataReadOnly:false,
                EnableEvalKPI: [],
                TotalWeightKPI: 0,
                PerformanceScore: 0,
                MaxScaleKPI: 0,
                BaseScale: 0,
                MaxWeigthKPI: 0,
                MaxWeigthBehavior: 0,
                BehaviorScore: 0,
                MaxScaleBehavior: 0,
                TotalWeightBehavior: 0,
                PScore: 0,
                BScore: 0,
                FinalScore: 0,
                SumTotalWeight: 0,
                IntegrateArchievementToKPI : '',
                IntegrateArchievementToBehavior:'',
                IntegrateArchievementToCompetency:'',
                //Data Tab
                master: {AchievementAllActivity: [],},
                KPIEvaluation: {},
                BehaviorSPIRIT: {},
                AchievementAllActivity: {},
                behavior_col_acheiment: {},
                columns_perfor: [],
                columns_behavior: [],
                columns_comp: [],
                BehaviorLevelData: [],
                CompetencyScaleData: [],
            };

            $scope.ChildAction.LoadData = function () {
            }
            $scope.ChildAction.SetData = function () {
            }
            
            $scope.GetMasterUI = function () {
                //$scope.isEditor;
                if ($scope.document.RequestNo == "DUMMY") {
                    
                    $scope.ActivityGroupID = $routeParams.otherParam.substr(0, 1);
                    $scope.ActivityTypeID = $routeParams.otherParam.substr(2, 1);
                    $scope.ActivityID = $routeParams.otherParam.substr(4, 1);
                }
                else
                {
                    $scope.ActivityGroupID = $scope.document.Additional.ActivityGroupID;
                    $scope.ActivityTypeID = $scope.document.Additional.ActivityTypeID;
                    $scope.ActivityID = $scope.document.Additional.ActivityID;
                    $scope.RequestorNo = $scope.document.Additional.RequestorNo;
                }

                var URL = CONFIG.SERVER + 'HRPD/GetEvalEditor';
                var oRequestParameter = { InputParameter: { "ActivityGroupID": $scope.ActivityGroupID, "ActivityTypeID": $scope.ActivityTypeID, "ActivityID": $scope.ActivityID, "RequestorNo": $scope.document.RequestorNo }, CurrentEmployee: getToken(CONFIG.USER) }
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.master = response.data;
                    $scope.Assignment = response.data.Assignment;
                    $scope.Score = response.data.Score;
                    $scope.data.BehaviorScore = 0;
                    $scope.data.PerformanceScore = 0;
                    $scope.data.PScore = 0;
                    $scope.data.BScore = 0;

                    $scope.document.Additional.ActivitiesParam = response.data.ActivitiesParam;
                    $scope.document.Additional.Assignment = response.data.Assignment;
                    $scope.document.Additional.KPIPerformance = response.data.KPIPerformance;
                    $scope.document.Additional.BehaviorGroup = response.data.BehaviorGroup;
                    $scope.document.Additional.Competency = response.data.Competency;
                    $scope.document.Additional.Score = response.data.Score;
                    $scope.data.columns_perfor = response.data.KPIPerTableControllor;
                    $scope.data.columns_behavior = response.data.BehaviorTableControllor;
                    $scope.data.columns_comp = response.data.CompetencyTableControllor;

                    //$scope.document.Additional.Competency.Q[0].Child[0].Child[0].ScoreControl
                    //IsDisable_data_Approver: false
                    //IsDisable_data_Request: true
                    //IsVisible_data_Approver: true
                    //IsVisible_data_Request: false

                    //$scope.document.Additional.Competency.Q[0].Child[0].Child[0].ItemControl
                    //IsDefinedGap_Checked: true
                    //IsDefinedGap_Enable: true
                    //IsDefinedGap_Visible: true
                    //IsPersonalType: false
                    //IsSelect_Checked: true
                    //IsSelect_Enable: true

                    $scope.data.BehaviorLevelData = response.data.BehaviorLevelData;
                    $scope.data.CompetencyScaleData = response.data.CompetencyScaleData;

                    $scope.document.Additional.RequestorNo = $scope.document.RequestorNo;
                    $scope.document.Additional.OwnerID = response.data.OwnerID;
                    $scope.document.Additional.OwnerLevel = response.data.OwnerLevel;

                    $scope.EnableEvalAssignment = response.data.EnableEvalAssignment;
                    $scope.EnableEvalBehavior = response.data.EnableEvalBehavior;
                    $scope.EnableEvalCompetency = response.data.EnableEvalCompetency;
                    $scope.EnableEvalKPI = response.data.EnableEvalKPI;

                    $scope.document.Additional.EnableEvalAssignment = response.data.EnableEvalAssignment;
                    $scope.document.Additional.EnableEvalBehavior = response.data.EnableEvalBehavior;
                    $scope.document.Additional.EnableEvalCompetency = response.data.EnableEvalCompetency;
                    $scope.document.Additional.EnableEvalKPI = response.data.EnableEvalKPI;

                    $scope.document.Additional.ActivityGroupID = $scope.ActivityGroupID;
                    $scope.document.Additional.ActivityTypeID = $scope.ActivityTypeID;
                    $scope.document.Additional.ActivityID = $scope.ActivityID;
                    //Config Perfomance KPI
                    for (i = 0; i <= $scope.document.Additional.ActivitiesParam.length - 1; i++) {
                        if ($scope.document.Additional.ActivitiesParam[i].Name == "IntegrateArchievementToKPI") {
                            if ($scope.document.Additional.ActivitiesParam[i].Value == 1)
                                $scope.document.Additional.IntegrateArchievementToKPI = true;
                            else
                                $scope.document.Additional.IntegrateArchievementToKPI = false;
                        }
                        if ($scope.document.Additional.ActivitiesParam[i].Name == "IntegrateArchievementToBehavior") {
                            if($scope.document.Additional.ActivitiesParam[i].Value == 1)
                                $scope.document.Additional.IntegrateArchievementToBehavior = true;
                            else
                                $scope.document.Additional.IntegrateArchievementToBehavior = false;
                        }
                        if ($scope.document.Additional.ActivitiesParam[i].Name == "IntegrateArchievementToCompetency") {
                            if ($scope.document.Additional.ActivitiesParam[i].Value == 1)
                                $scope.document.Additional.IntegrateArchievementToCompetency = true;
                            else
                                $scope.document.Additional.IntegrateArchievementToCompetency = false;
                        }

                        if ($scope.document.Additional.ActivitiesParam[i].Name == "TotalWeigthKPI") {
                            $scope.data.TotalWeightKPI = parseInt($scope.document.Additional.ActivitiesParam[i].Value);
                        }
                        //MaxScaleKPI
                        if ($scope.document.Additional.ActivitiesParam[i].Name == "MaxScaleKPI") {
                            $scope.data.MaxScaleKPI = parseInt($scope.document.Additional.ActivitiesParam[i].Value);
                        }
                        //BaseScaleKPI
                        if ($scope.document.Additional.ActivitiesParam[i].Name == "BaseScale") {
                            $scope.data.BaseScale = parseInt($scope.document.Additional.ActivitiesParam[i].Value);
                        }
                        //MaxWeigthKPI
                        if ($scope.document.Additional.ActivitiesParam[i].Name == "MaxWeigthKPI") {
                            $scope.data.MaxWeigthKPI = parseInt($scope.document.Additional.ActivitiesParam[i].Value);
                        }
                        //TotalWeightBehavior
                        if ($scope.document.Additional.ActivitiesParam[i].Name == "TotalWeigthBehavior") {
                            $scope.data.TotalWeightBehavior = parseInt($scope.document.Additional.ActivitiesParam[i].Value);
                        }
                        //MaxScaleBehavior
                        if ($scope.document.Additional.ActivitiesParam[i].Name == "MaxScaleBehavior") {
                            $scope.data.MaxScaleBehavior = parseInt($scope.document.Additional.ActivitiesParam[i].Value);
                        }
                    }

                    //summary Perfomance Score
                    if ($scope.document.Additional.KPIPerformance[0].ScoreControl.IsVisible_data_Request == true)
                    {
                        for(i=0;i<= $scope.document.Additional.KPIPerformance.length-1;i++)
                        {
                            $scope.data.PerformanceScore = $scope.data.PerformanceScore + $scope.document.Additional.KPIPerformance[i].Performance_Request.WeightScore;
                        }
                    }
                    else if ($scope.document.Additional.KPIPerformance[0].ScoreControl.IsVisible_data_Approver == true)
                    {
                        for (i = 0; i <= $scope.document.Additional.KPIPerformance.length-1; i++) {
                            $scope.data.PerformanceScore = $scope.data.PerformanceScore + $scope.document.Additional.KPIPerformance[i].Performance_Approver.WeightScore;

                        }
                    }

                    //Set Default Behaviro Score
                    for (i = 0; i <= $scope.document.Additional.BehaviorGroup.length - 1; i++) {
                        for (j = 0; j <= $scope.document.Additional.BehaviorGroup[i].Behavior.length - 1; j++) {
                            if ($scope.document.Additional.BehaviorGroup[i].Behavior[j].ScoreControl.IsVisible_data_Request == true) {
                                if ($scope.document.Additional.BehaviorGroup[i].Behavior[j].Score_Requester.LevelID == 0)
                                {
                                    $scope.document.Additional.BehaviorGroup[i].Behavior[j].Score_Requester.LevelID = $scope.document.Additional.BehaviorGroup[i].Behavior[j].LevelScore[0].LevelID;
                                    $scope.document.Additional.BehaviorGroup[i].Behavior[j].Score_Requester.LevelName = $scope.document.Additional.BehaviorGroup[i].Behavior[j].LevelScore[0].LevelName;
                                    $scope.document.Additional.BehaviorGroup[i].Behavior[j].Score_Requester.Score = $scope.document.Additional.BehaviorGroup[i].Behavior[j].LevelScore[0].Score;
                                }
                                $scope.data.BehaviorScore = $scope.data.BehaviorScore + $scope.document.Additional.BehaviorGroup[i].Behavior[j].Score_Requester.Score;
                            }
                            else {
                                if ($scope.document.Additional.BehaviorGroup[i].Behavior[j].Score_Approver.LevelID == 0) {
                                    $scope.document.Additional.BehaviorGroup[i].Behavior[j].Score_Approver.LevelID = $scope.document.Additional.BehaviorGroup[i].Behavior[j].LevelScore[0].LevelID;
                                    $scope.document.Additional.BehaviorGroup[i].Behavior[j].Score_Approver.LevelName = $scope.document.Additional.BehaviorGroup[i].Behavior[j].LevelScore[0].LevelName;
                                    $scope.document.Additional.BehaviorGroup[i].Behavior[j].Score_Approver.Score = $scope.document.Additional.BehaviorGroup[i].Behavior[j].LevelScore[0].Score;
                                }
                                $scope.data.BehaviorScore = $scope.data.BehaviorScore + $scope.document.Additional.BehaviorGroup[i].Behavior[j].Score_Approver.Score;
                            }
                        }
                    }

                    //Set Scale default Competency
                    if ($scope.document.Additional.Competency.Q.length > 0) {
                        for (i = 0; i <= $scope.document.Additional.Competency.Q.length - 1; i++) {

                        }
                    }
                    
                    $scope.data.BehaviorScore = (parseFloat($scope.data.BehaviorScore) * parseFloat($scope.data.BaseScale)) / $scope.data.TotalWeightBehavior;

                    $scope.document.Additional.Score.PerformanceScore = $scope.data.PerformanceScore;
                    $scope.document.Additional.Score.BehaviorScore = $scope.data.BehaviorScore;
                    $scope.data.SumTotalWeight = parseInt($scope.data.TotalWeightKPI) + parseInt($scope.data.TotalWeightBehavior);

                    $scope.data.PScore = ($scope.document.Additional.Score.PerformanceScore / $scope.data.BaseScale) * $scope.data.TotalWeightBehavior;
                    $scope.data.BScore = ($scope.document.Additional.Score.BehaviorScore / $scope.data.BaseScale) * $scope.data.TotalWeightBehavior;

                    $scope.document.Additional.Score.TotalScore = (($scope.data.PScore + $scope.data.BScore) / $scope.data.SumTotalWeight) * $scope.data.BaseScale;

                }, function errorCallback(response) {
                    console.log('error TravelExpenseContentController InitialConfig.', response);
                });

                var URL = CONFIG.SERVER + 'HRPD/GetAchievementAllActivity';
                var oRequestParameter = { InputParameter: {"RequestorNo": $scope.document.RequestorNo }, CurrentEmployee: getToken(CONFIG.USER) }
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.data.AchievementAllActivity = response.data;
                    getSpiritList();
                    if ($scope.data.AchievementAllActivity.length > 0)
                    {
                        for(i=0;i<=$scope.data.AchievementAllActivity[i].Achievement.length-1;i++)
                        {                            
                            for (l = 0; l <= $scope.data.AchievementAllActivity[i].Achievement.length - 1; l++) {
                                $scope.Spirit = $scope.data.AchievementAllActivity[i].Achievement[l].Spirit.split(",");
                                $scope.data.AchievementAllActivity[i].Achievement[l].selectedTag = [];
                                $scope.data.AchievementAllActivity[i].Achievement[l].selectedTag = angular.copy($scope.spirit_s);

                                for (j = 0; j <= $scope.Spirit.length - 1; j++) {
                                    for (k = 0; k <= $scope.data.AchievementAllActivity[i].Achievement[l].selectedTag.length - 1; k++) {
                                        if ($scope.Spirit[j] == $scope.data.AchievementAllActivity[i].Achievement[l].selectedTag[k].code) {
                                            $scope.data.AchievementAllActivity[i].Achievement[l].selectedTag[k].isSelected = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $scope.data.behavior_col_acheiment = [
                        { Text: '', width: '6%' },
                        { Text: 'Spirit', width: '22%' },
                        { Text: 'Situaltion', width: '22%' },
                        { Text: 'Action', width: '22%' },
                        { Text: 'Result', width: '22%' },
                        { Text: '', width: '6%' },
                    ];
                }, function errorCallback(response) {
                    console.log('error GetAchievementAllActivity InitialConfig.', response);
                });
            }
            $scope.GetMasterUI();

            function getSpiritList() {
                $scope.spirit_s = [
                    {
                        id: 0,
                        code: 'S',
                        cssClass: 'spirit__s',
                        iconPath: '/images/spirit/spirit_s.PNG',
                        full: 'Synergy',
                        Description: 'สร้างพลังร่วมอันยิ่งใหญ่',
                        isSelected: false,
                    },
                    {
                        id: 1,
                        code: 'P',
                        cssClass: 'spirit__p',
                        iconPath: '/images/spirit/spirit_p.PNG',
                        full: 'Performance Excellence',
                        Description: 'ร่วมมุ่งสู่ความเป็นเลิศ',
                        isSelected: false,
                    },
                    {
                        id: 2,
                        code: 'In',
                        cssClass: 'spirit__i',
                        iconPath: '/images/spirit/spirit_i.PNG',
                        full: 'Innovation',
                        Description: 'ร่วมสร้างวัฒนธรรม',
                        isSelected: false,
                    },
                    {
                        id: 3,
                        code: 'R',
                        cssClass: 'spirit__r',
                        iconPath: '/images/spirit/spirit_r.PNG',
                        full: 'Responsibility for Society',
                        Description: 'ร่วมรับผิดชอบต่อสังคม',
                        isSelected: false,
                    },
                    {
                        id: 4,
                        code: 'I',
                        cssClass: 'spirit__ie',
                        iconPath: '/images/spirit/spirit_ie.PNG',
                        full: 'Inegretry & Ethics',
                        Description: 'ร่วมสร้างพลังความดี',
                        isSelected: false,
                    },
                    {
                        id: 5,
                        code: 'T',
                        cssClass: 'spirit__t',
                        iconPath: '/images/spirit/spirit_t.PNG',
                        full: 'Trust & Respect',
                        Description: 'ร่วมใจสร้างความเชื่อมั่น',
                        isSelected: false,
                    },
                ];
            }
            // insert KPIEditor start
            $scope.CalculateWeightPerformance = function () {
                $scope.SumPerformanceScore = 0;
                if (mode == "Approver") {
                    if (obj.Performance_Approver.Rating > $scope.data.MaxScaleKPI) {
                        obj.Performance_Approver.Rating = $scope.data.MaxScaleKPI;
                    }

                    for (i = 0; i <= $scope.document.Additional.KPIPerformance.length - 1; i++) {
                        if ($scope.document.Additional.KPIPerformance[i].No == No) {
                            $scope.document.Additional.KPIPerformance[i].Performance_Approver.WeightScore = ($scope.document.Additional.KPIPerformance[i].Weight * obj.Performance_Approver.Rating) / $scope.data.MaxWeigthKPI;
                        }
                        $scope.SumPerformanceScore = $scope.SumPerformanceScore + $scope.document.Additional.KPIPerformance[i].Performance_Approver.WeightScore;
                    }
                }
                else if (mode == "Requestor") {
                    if (obj.Performance_Request.Rating > $scope.data.MaxScaleKPI) {
                        obj.Performance_Request.Rating = $scope.data.MaxScaleKPI;
                    }
                    for (i = 0; i <= $scope.document.Additional.KPIPerformance.length - 1; i++) {
                        if ($scope.document.Additional.KPIPerformance[i].No == No) {
                            $scope.document.Additional.KPIPerformance[i].Performance_Request.WeightScore = ($scope.document.Additional.KPIPerformance[i].Weight * obj.Performance_Request.Rating) / $scope.data.MaxWeigthKPI;
                        }
                        $scope.SumPerformanceScore = $scope.SumPerformanceScore + $scope.document.Additional.KPIPerformance[i].Performance_Request.WeightScore;
                    }
                }

                $scope.data.PerformanceScore = ($scope.SumPerformanceScore * $scope.data.BaseScale) / $scope.data.MaxScaleKPI;
                $scope.document.Additional.Score.PerformanceScore = $scope.data.PerformanceScore;

            }

            $scope.selectTabN = 0;
            $scope.selectTab = function (tabN) {
                if (tabN == 1)
                    $scope.data.IntegrateArchievementToKPI = $scope.IntegrateArchievementToKPI;
                else if(tabN == 2)
                    $scope.data.IntegrateArchievementToBehavior = $scope.IntegrateArchievementToBehavior;
                else if (tabN == 3)
                    $scope.data.IntegrateArchievementToCompetency = $scope.IntegrateArchievementToCompetency;

                $scope.selectTabN = tabN;
            }
        
 

        }])
        .directive('expand', function () {
            return {
                restrict: 'A',
                controller: ['$scope', function ($scope) {
                    $scope.$on('onExpandAll', function (event, args) {
                        $scope.expanded = args.expanded;
                    });
                }]
            }; z
        });
})();
