﻿(function () {
    angular.module('ESSMobile')
        .controller('IDPActualEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$q', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $q, $mdDialog) {
            $scope.Textcategory = 'IDPACTUAL_REQ';
            $scope.HeaderText = $scope.Text[$scope.Textcategory].TITLE;
            $scope.content.Header = $scope.Text[$scope.Textcategory].TITLE;
            $scope.userData = getToken(CONFIG.USER);

            $scope.column_header = [
              { text: $scope.Text[$scope.Textcategory].NO, width: '50px' },
              { text: $scope.Text[$scope.Textcategory].GAP_COMPETENCY, width: '400px' },
              { text: $scope.Text[$scope.Textcategory].DEV_METHOD, width: '200px' },
              { text: $scope.Text[$scope.Textcategory].PLAN_DETAIL, width: '400px' },
              { text: $scope.Text[$scope.Textcategory].PLAN_DETAIL, width: '400px' },
              { text: $scope.Text[$scope.Textcategory].PLAN_START, width: '200px' },
              { text: $scope.Text[$scope.Textcategory].PLAN_END, width: '200px' },
              { text: $scope.Text[$scope.Textcategory].PLAN_START, width: '200px' },
              { text: $scope.Text[$scope.Textcategory].PLAN_END, width: '200px' },
              { text: "", width: '120px' },
            ];


            var url = $location.path();
            var newString = url.replace("/", "");

            
            // insert KPIEditor start
            $scope.data = {
                DevMethodList:[],
                PlanList:null,
            }
            $scope.ChildAction.SetData = function () {
            }
            $scope.ChildAction.SetData();

            $scope.ChildAction.LoadData = function () {
                
            }
            //$scope.ChildAction.LoadData();


            /* ====== wizard form control ====== */
            $scope.wizardFormControl = {
                receiptIsActive: false,
                withoutReceiptIsActive: false
            };
            /* ====== !wizard form control ====== */


            //init
            $scope.InitialConfig = function () {
                $scope.loader.enable = true;
            }
            $scope.GetMasterUI = function () {
                var URL = CONFIG.SERVER + 'HRPD/GetIDPDevelopmentMethod';
                var oRequestParameter = { InputParameter: { }, CurrentEmployee:  $scope.userData }
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.data.DevMethodList = response.data;
                }, function errorCallback(response) {
                    console.log('error GetIDPPlanOpen Editor InitialConfig.', response);
                });
                var URL = CONFIG.SERVER + 'HRPD/GetIDPPlanOpen';
                var oRequestParameter = { InputParameter: { RequestorNo: $scope.userData.EmployeeID }, CurrentEmployee: $scope.userData }
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.data.PlanList = response.data;
                }, function errorCallback(response) {
                    console.log('error GetIDPPlanOpen Editor InitialConfig.', response);
                });
            }
            $scope.GetMasterUI();



            function InitialConfig() {
                $scope.setBlockAction(true, $scope.Text["SYSTEM"]["PROCESSING"]);

            }

            /* ====== Inital data before load control (Finish)====== */

            //date format
            $scope.getDateFormate = function (date) {
                return getDateFormate(date);
            }
            function getDateFormate(date) { return $filter('date')(date, 'd/M/yyyy'); }
            $scope.checkMaxDate = function (index) {
                checkMaxDate(index);
            }
            function checkMaxDate(index) {
                $scope.document.Additional.ActualContent[index].ActualBeginDate = new Date($scope.document.Additional.ActualContent[index].ActualBeginDate);
                $scope.document.Additional.ActualContent[index].ActualEndDate = new Date($scope.document.Additional.ActualContent[index].ActualEndDate);
                if ($scope.document.Additional.ActualContent[index].ActualBeginDate > $scope.document.Additional.ActualContent[index].ActualEndDate) {
                    $scope.document.Additional.ActualContent[index].ActualEndDate = $scope.document.Additional.ActualContent[index].ActualBeginDate;
                }
            }


            // promtDialog Dialog
            $scope.promtDialog = function (ev, index, columnName) {
                $mdDialog.show({
                    controller: promtDialogCtrl,
                    templateUrl: 'views/common/dialog/textInput.tmpl.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    locals: {
                        params: {
                            name: columnName,
                            placeholder: 'Enter ' + columnName,
                            defaultValue: $scope.document.Additional.ActualContent[index].ActualDescription,
                            maxlength: 800
                        }
                    },
                })
               .then(function (answer) {
                   $scope.document.Additional.ActualContent[index].ActualDescription = answer;
               }, function () {

               });
            };
            function promtDialogCtrl($scope, $mdDialog, params) {
                $scope.params = params;
        
                $scope.inputValue = params.defaultValue;
                
                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.save = function (answer) {
                    if (answer.length < params.maxlength) {
                        $mdDialog.hide(answer);
                    }
                };
            }

            // promDialog Plan
            $scope.selectPlan = function (ev,index) {
                var column_header_dialog = [
                    { text: 'No', width: '50px' },
                    { text: $scope.Text["IDPPLAN_REQ"].GAP_COMPETENCY, width: '400px' },
                ];
                var selectedPlanList = [];
                for (var i = 0; i <  $scope.document.Additional.ActualContent.length; i++) {
                    var newItem = {
                        PlanRequestNo: $scope.document.Additional.ActualContent[i].PlanRequestNo,
                        GapRequestNo: $scope.document.Additional.ActualContent[i].GapRequestNo,
                        GapObjectID: $scope.document.Additional.ActualContent[i].GapObjectID
                    };
                    selectedPlanList.push(newItem);
                }
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: 'views/common/dialog/idpactual.tmpl.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    locals: {
                        params: {
                            PlanList: $scope.data.PlanList,
                            Selected: selectedPlanList,
                            column_header: column_header_dialog
                        }
                    },
                })
                .then(function (selectedItem) {
                   
                    if (index) {
                      
                    } else {
                        var newActualContentObject = {

                            ActionBy: null,
                            ActualBeginDate: new Date(),
                            ActualDescription: "",
                            ActualEndDate: new Date(),
                            ActualRequestNo: null,
                            ActualYear: 0,
                            GapObjectID: selectedItem.GapObjectID,
                            GapObjectType: selectedItem.GapObjectType,
                            GapRequestNo: selectedItem.GapRequestNo,
                            PlanContent: selectedItem,
                            PlanRequestNo: selectedItem.PlanRequestNo,
                            RequestorNo: null,
                        }
                        
                        $scope.document.Additional.ActualContent.push(newActualContentObject);
                    }
                    
                }, function () {

                });

            }
            function DialogController($scope, $mdDialog, params) { // start controller
                $scope.params = params;
                $scope.selectedList = params.Selected;
                $scope.selectedActivityIndex = 0;
                $scope.column_header = params.column_header;

                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.save = function (item) {
                    $mdDialog.hide(item);
                };

                $scope.removeSelected = function (item, b, c) {
                    for (var a = 0 ; a < $scope.selectedList.length; a++) {
                        if ($scope.selectedList[a].PlanRequestNo == item.PlanRequestNo &&
                            $scope.selectedList[a].GapRequestNo == item.GapRequestNo &&
                            $scope.selectedList[a].GapObjectID == item.GapObjectID
                        ) {
                            return false;
                        }
                    }    
                    return true;
                }
                $scope.getDateFormate = function (date) {
                    return getDateFormate(date);
                }
                function getDateFormate(date) { return $filter('date')(date, 'd/M/yyyy'); }
            } // end controller


            // event
            $scope.removeitem = function (index) {
                if (index > -1) {
                    $scope.document.Additional.ActualContent.splice(index, 1);
                }
            }




         
        }])
        .directive('expand', function () {
            return {
                restrict: 'A',
                controller: ['$scope', function ($scope) {
                    $scope.$on('onExpandAll', function (event, args) {
                        $scope.expanded = args.expanded;
                    });
                }]
            }; 
        });
})();
