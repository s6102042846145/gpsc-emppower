﻿(function () {
    angular.module('ESSMobile')
        .controller('AchievementEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$q', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $q, $mdDialog) {
            $scope.Textcategory = 'ARCHIVEMENT';

            $scope.column_header = [
                {Text :'Spirit', width: '13%'},
                { Text: 'Situation', width: '26%' },
                { Text: 'Action', width: '26%' },
                { Text: 'Result', width: '26%' },
                { Text: '', width: '9%' }
            ];


            $scope.selectedList = [];
            $scope.spirit_s = [];
            $scope.achivementTypes = [];

            //$scope.ChildAction.SetData = function () {
            //    $scope.document;
            //}

            $scope.ChildAction.SetData = function () {
                if ($scope.document.RequestNo == 'DUMMY') {
                    $scope.ActivityGroupID = $routeParams.otherParam.substr(0, 1);
                    $scope.ActivityTypeID = $routeParams.otherParam.substr(2, 1);
                    $scope.ActivityID = $routeParams.otherParam.substr(4, 1);
                }

                else {
                    $scope.ActivityGroupID = $scope.document.Additional.ActivityGroupID;
                    $scope.ActivityTypeID = $scope.document.Additional.ActivityTypeID;
                    $scope.ActivityID = $scope.document.Additional.ActivityID;
                }


                GetMasterUI();
            }

            $scope.ChildAction.LoadData = function () {
                $scope.selectedList;
                if ($scope.document.RequestNo == 'DUMMY') {
                    $scope.ActivityGroupID = $routeParams.otherParam.substr(0, 1);
                    $scope.ActivityTypeID = $routeParams.otherParam.substr(2, 1);
                    $scope.ActivityID = $routeParams.otherParam.substr(4, 1);
                }
                else
                {

                        $scope.ActivityGroupID = $scope.document.Additional.ActivityGroupID;
                        $scope.ActivityTypeID = $scope.document.Additional.ActivityTypeID;
                        $scope.ActivityID = $scope.document.Additional.ActivityID;

                }
                $scope.document.Additional.ActivityGroupID = $scope.ActivityGroupID;
                $scope.document.Additional.ActivityTypeID = $scope.ActivityTypeID;
                $scope.document.Additional.ActivityID = $scope.ActivityID;
                if($scope.selectedList.length > 0)
                {
                    $scope.document.Additional.Achievement = [];
                    for(i=0;i<=$scope.selectedList.length -1;i++)
                    {
                        $scope.spirit_data = "";
                        for (j = 0; j <= $scope.selectedList[i].selectedTag.length - 1; j++)
                        {
                            
                            if($scope.selectedList[i].selectedTag[j].isSelected == true)
                            {
                                $scope.spirit_data = $scope.spirit_data + $scope.selectedList[i].selectedTag[j].code + ',';
                            }
                            
                        }
                        var Achievement = {
                            "ActivityGroupID": $scope.ActivityGroupID,
                            "ActivityTypeID": $scope.ActivityTypeID,
                            "ActivityID": $scope.ActivityID,
                            "AchievementID": $scope.selectedList[i].achievementTypeItem.id,
                            "AchievementType": $scope.selectedList[i].achievementTypeItem.text,
                            "Spirit": $scope.spirit_data,
                            "Situation": $scope.selectedList[i].Situation,
                            "Action": $scope.selectedList[i].Action,
                            "Result": $scope.selectedList[i].Result
                        };
                        $scope.document.Additional.Achievement.push(Achievement);
                    }
                }
                else
                {
                    $scope.document.Additional.Achievement = [];
                }
                //console.log("LoadData");
            }

            $scope.init = function () {
                getMaster();
            }
            function getMaster()
            {
                getSpiritList();
                getAchievementType();
                $scope.ChildAction.SetData();
            }

            function GetMasterUI() {
                var URL = CONFIG.SERVER + 'HRPD/GetAchievementViewer';
                var oRequestParameter = { InputParameter: { "ActivityGroupID": $scope.ActivityGroupID, "ActivityTypeID": $scope.ActivityTypeID, "ActivityID": $scope.ActivityID, "RequestorNo": $scope.document.RequestorNo }, CurrentEmployee: getToken(CONFIG.USER) }
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.selectedList = response.data.Achievement;
                    for (i = 0; i <= $scope.selectedList.length - 1; i++) {
                        getSpiritList();
                        var spirit = $scope.selectedList[i].Spirit.split(',');
                        for (k = 0; k <= spirit.length - 1; k++) {
                            for (j = 0; j <= $scope.spirit_s.length - 1; j++) {
                                if (spirit[k] != "" && spirit[k] == $scope.spirit_s[j].code) {
                                    $scope.spirit_s[j].isSelected = true;
                                }
                            }
                        }

                        $scope.selectedList[i].selectedTag = angular.copy($scope.spirit_s);
                        for (l = 0; l <= $scope.achivementTypes.length - 1; l++)
                        {
                            if($scope.selectedList[i].AchievementType == $scope.achivementTypes[l].text)
                                $scope.selectedList[i].achievementTypeItem = angular.copy($scope.achivementTypes[l]);
                        }
                    }
                }, function errorCallback(response) {
                    console.log('error AchievementViewer InitialConfig.', response);
                });
            }

            function getSpiritList() {
                $scope.spirit_s = [
                    {
                        id: 0,
                        code: 'S',
                        cssClass: 'spirit__s',
                        iconPath:'/images/spirit/spirit_s.PNG',
                        full:'Synergy',
                        Description: 'สร้างพลังร่วมอันยิ่งใหญ่',
                        isSelected:false,
                    },
                    {
                        id: 1,
                        code: 'P',
                        cssClass: 'spirit__p',
                        iconPath: '/images/spirit/spirit_p.PNG',
                        full: 'Performance Excellence',
                        Description: 'ร่วมมุ่งสู่ความเป็นเลิศ',
                        isSelected: false,
                    },
                    {
                        id: 2,
                        code: 'I',
                        cssClass: 'spirit__i',
                        iconPath: '/images/spirit/spirit_i.PNG',
                        full: 'Innovation',
                        Description: 'ร่วมสร้างวัฒนธรรม',
                        isSelected: false,
                    },
                    {
                        id: 3,
                        code: 'R',
                        cssClass: 'spirit__r',
                        iconPath: '/images/spirit/spirit_r.PNG',
                        full: 'Responsibility for Society',
                        Description: 'ร่วมรับผิดชอบต่อสังคม',
                        isSelected: false,
                    },
                    {
                        id: 4,
                        code: 'I',
                        cssClass: 'spirit__ie',
                        iconPath: '/images/spirit/spirit_ie.PNG',
                        full: 'Inegretry & Ethics',
                        Description: 'ร่วมสร้างพลังความดี',
                        isSelected: false,
                    },
                    {
                        id: 5,
                        code: 'T',
                        cssClass: 'spirit__t',
                        iconPath: '/images/spirit/spirit_t.PNG',
                        full: 'Trust & Respect',
                        Description: 'ร่วมใจสร้างความเชื่อมั่น',
                        isSelected: false,
                    },
                ];
            }

            function getAchievementType() {
                $scope.achivementTypes = [
                    {
                        id: '0',
                        text: 'Achievement',
                        iconClass: 'fa fa-bullseye spirit__t',
                    },
                    {
                        id: '1',
                        text: 'Star Achievement',
                        iconClass: 'fa fa-star spirit__p',
                    }
                ]
            }
            $scope.GetEditSelected = function(obj)
            {
                if(obj.selectedTag.length > 0)
                {
                    for (i = 0; i <= obj.selectedTag.length - 1; i++)
                    {
                        if (obj.selectedTag[i].code == $scope.spirit_s[i].code && obj.selectedTag[i].isSelected == true)
                        {
                            $scope.spirit_s[i].isSelected = true;
                        }
                    }
                }
                $scope.displayAchievement(event, 'edit');
            }
            function InitialConfig() {
            }

            // promtDialog text
            $scope.promtDialog = function (ev, index, columnName) {
                $mdDialog.show({
                    controller: promtDialogCtrl,
                    templateUrl: 'views/common/dialog/textInput.tmpl.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    locals: {
                        params: {
                            name: columnName,
                            placeholder: 'Enter ' + columnName,
                            defaultValue: $scope.data.selectedKPI[index][columnName],
                            maxlength:800
                        }
                    },
                })
               .then(function (answer) {
                   $scope.data.selectedKPI[index][columnName] = answer;
               }, function () {

               });
            };
            function promtDialogCtrl($scope, $mdDialog, params) {
                $scope.params = params;
                $scope.inputValue = params.defaultValue;
                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.save = function (answer) {
                    if (answer.length < params.maxlength) {
                        $mdDialog.hide(answer);
                    }
                   
                };
            }
            
            function addItem(item) {
                $scope.selectedList.push(item);
            }
            function editItem(item,index)
            {
                $scope.selectedList[index] = angular.copy(item);
            }

            // dialog Arhievement
            $scope.displayAchievement = function (ev, action,index,obj) {
                var o_data;
                if (action === 'add') {
                    if (obj != "") {
                        for (i = 0; i <= obj.selectedTag.length - 1; i++) {
                            if (obj.selectedTag[i].code == $scope.spirit_s[i].code && obj.selectedTag[i].isSelected == true) {
                                $scope.spirit_s[i].isSelected = true;
                            }
                        }
                    }
                    else {
                        getSpiritList();
                    }

                    o_data = {
                        selectedTag: angular.copy($scope.spirit_s),
                        Situation: '',
                        Action: '',
                        Result: '',
                        Type: '0' // default data
                    }
                } else if(action == 'edit') {
                    getSpiritList();
                    for (i = 0; i <= obj.selectedTag.length - 1; i++) {
                        for (k = 0; k <= $scope.spirit_s.length - 1; k++)
                        {
                            if (obj.selectedTag[i].code == $scope.spirit_s[k].code && obj.selectedTag[i].isSelected == true) {
                                $scope.spirit_s[k].isSelected = true;
                            }
                        }
                    }
                    o_data = {
                        selectedTag: angular.copy($scope.spirit_s),
                        Situation: $scope.selectedList[index].Situation,
                        Action: $scope.selectedList[index].Action,
                        Result: $scope.selectedList[index].Result,
                        Type: '0' // default data
                    }                    
                }

                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: 'views/common/dialog/achievement.tmpl.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    locals: {
                        params: {
                            title: 'AcievementEditorDialog',
                            data: o_data,
                            achivementTypes: $scope.achivementTypes,
                            MaxLength:1000,
                        }
                    },
                })
                .then(function (item) {
                    if (item.Situation.length > 150)
                        item.Situation = item.Situation.substring(0, 150);
                    if (item.Action.length > 150)
                        item.Action = item.Action.substring(0, 150);
                    if (item.Result.length > 150)
                        item.Result = item.Result.substring(0, 150);

                    if (action === 'add') {
                        addItem(item);
                    } else {
                        editItem(item, index);
                    }
                    console.log(item);

                }, function () {

                });

            }
            $scope.deleteAchievement = function(index)
            {
                if ($scope.selectedList.length > 0)
                {
                    $scope.selectedList.splice(index, 1);
                }
            }
            function getAchievementByID(id) {
                for (var i = 0; i < $scope.achivementTypes.length; i++) {
                    if ($scope.achivementTypes[i].id === id) {
                        return angular.copy($scope.achivementTypes[i]);
                    }
                }
            }

            function DialogController($scope, $mdDialog, params) { // start controller
                $scope.achivementTypes = angular.copy(params.achivementTypes);
                $scope.data = angular.copy(params.data);
                $scope.MaxLength = params.MaxLength;
                $scope.iSituation = 0;
                $scope.iAction = 0;
                $scope.iResult = 0;
                if ($scope.data.Situation.length > 0)
                    $scope.iSituation = $scope.data.Situation.length;
                if ($scope.data.Action.length > 0)
                    $scope.iAction = $scope.data.Action.length;
                if ($scope.data.Result.length > 0)
                    $scope.iResult = $scope.data.Result.length;

                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.save = function (answer) {
                    // attribute to blind in the table
                    answer.achievementTypeItem = getAchievementByID(answer.Type);
                    $mdDialog.hide(answer);
                };

                $scope.CountText = function(data,mode)
                {
                    if(mode == "Situation")
                        $scope.iSituation = data.length;
                    if (mode == "Action")
                        $scope.iAction = data.length;
                    if (mode == "Result")
                        $scope.iResult = data.length;
                }

                $scope.toggleState = function (index) {
                    $scope.data.selectedTag[index].isSelected = !$scope.data.selectedTag[index].isSelected;
                }
               
            } // end controller
            
        }])// end controller
})();
