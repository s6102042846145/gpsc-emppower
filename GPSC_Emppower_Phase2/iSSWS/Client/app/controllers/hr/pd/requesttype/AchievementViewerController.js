﻿(function () {
    angular.module('ESSMobile')
        .controller('AchievementViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$q', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $q, $mdDialog) {
            $scope.Textcategory = 'ARCHIVEMENT';
            $scope.column_header = [
                { Text: 'Spirit', width: '13%' },
                { Text: 'Situation', width: '29%' },
                { Text: 'Action', width: '29%' },
                { Text: 'Result', width: '29%' },
            ];

            $scope.selectedList = [];
            $scope.spirit_s = [];
            $scope.achivementTypes = [];

            $scope.ChildAction.SetData = function () {
            }
            $scope.ChildAction.LoadData = function () {
                $scope.ActivityGroupID = $scope.document.Additional.ActivityGroupID;
                $scope.ActivityTypeID = $scope.document.Additional.ActivityGroupID;
                $scope.ActivityID = $scope.document.Additional.ActivityGroupID;
                $scope.RequestNo = $scope.document.Additional.RequestNo;

            }

            $scope.init = function () {
                getMaster();
            }
            function getMaster()
            {
                getAchievementType();
                //getSpiritList();
                GetMasterUI();
            }
            function InitialConfig() {

            }

            function GetMasterUI() {
                $scope.ActivityGroupID = $scope.document.Additional.ActivityGroupID;
                $scope.ActivityTypeID = $scope.document.Additional.ActivityTypeID;
                $scope.ActivityID = $scope.document.Additional.ActivityID;

                var URL = CONFIG.SERVER + 'HRPD/GetAchievementViewer';
                var oRequestParameter = { InputParameter: { "ActivityGroupID": $scope.ActivityGroupID, "ActivityTypeID": $scope.ActivityTypeID, "ActivityID": $scope.ActivityID, "RequestorNo": $scope.document.RequestorNo }, CurrentEmployee: getToken(CONFIG.USER) }
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.AchievementViewer = response.data.Achievement;
                    for(i=0;i<=$scope.AchievementViewer.length -1;i++)
                    {
                        getSpiritList();
                        var spirit = $scope.AchievementViewer[i].Spirit.split(',');
                        for (k = 0; k <= spirit.length - 1; k++) {
                            for (j = 0; j <= $scope.spirit_s.length - 1; j++) {
                                if(spirit[k] != "" && spirit[k] == $scope.spirit_s[j].code)
                                {
                                    $scope.spirit_s[j].isSelected = true;
                                }
                            }
                        }

                        $scope.AchievementViewer[i].selectedTag = angular.copy($scope.spirit_s);
                    }
                }, function errorCallback(response) {
                    console.log('error AchievementViewer InitialConfig.', response);
                });
            }

            function getAchievementType() {
                $scope.achivementTypes = [
                    {
                        id: '0',
                        text: 'Achievement',
                        iconClass: 'fa fa-bullseye spirit__t',
                    },
                    {
                        id: '1',
                        text: 'Star Achievement',
                        iconClass: 'fa fa-star spirit__p',
                    }
                ]
            }
            function getSpiritList() {
                $scope.spirit_s = [
                    {
                        id: 0,
                        code: 'S',
                        cssClass: 'spirit__s',
                        iconPath: '/images/spirit/spirit_s.PNG',
                        full: 'Synergy',
                        Description: 'สร้างพลังร่วมอันยิ่งใหญ่',
                        isSelected: false,
                    },
                    {
                        id: 1,
                        code: 'P',
                        cssClass: 'spirit__p',
                        iconPath: '/images/spirit/spirit_p.PNG',
                        full: 'Performance Excellence',
                        Description: 'ร่วมมุ่งสู่ความเป็นเลิศ',
                        isSelected: false,
                    },
                    {
                        id: 2,
                        code: 'I',
                        cssClass: 'spirit__i',
                        iconPath: '/images/spirit/spirit_i.PNG',
                        full: 'Innovation',
                        Description: 'ร่วมสร้างวัฒนธรรม',
                        isSelected: false,
                    },
                    {
                        id: 3,
                        code: 'R',
                        cssClass: 'spirit__r',
                        iconPath: '/images/spirit/spirit_r.PNG',
                        full: 'Responsibility for Society',
                        Description: 'ร่วมรับผิดชอบต่อสังคม',
                        isSelected: false,
                    },
                    {
                        id: 4,
                        code: 'I',
                        cssClass: 'spirit__ie',
                        iconPath: '/images/spirit/spirit_ie.PNG',
                        full: 'Inegretry & Ethics',
                        Description: 'ร่วมสร้างพลังความดี',
                        isSelected: false,
                    },
                    {
                        id: 5,
                        code: 'T',
                        cssClass: 'spirit__t',
                        iconPath: '/images/spirit/spirit_t.PNG',
                        full: 'Trust & Respect',
                        Description: 'ร่วมใจสร้างความเชื่อมั่น',
                        isSelected: false,
                    },
                ];
            }
            
            /* ====== Inital data before load control (Finish)====== */

            // insert

        }])// end controller
})();
