﻿(function () {
    angular.module('ESSMobile')
        .controller('IDPPlanEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$q', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $q, $mdDialog) {
            $scope.Textcategory = 'IDPPLAN_REQ';
            $scope.HeaderText = $scope.Text[$scope.Textcategory].TITLE;
            $scope.content.Header = $scope.Text[$scope.Textcategory].TITLE;
            $scope.userData = getToken(CONFIG.USER);

            $scope.column_header = [
              { text: $scope.Text[$scope.Textcategory].NO, width: '50px' },
              { text: $scope.Text[$scope.Textcategory].GAP_COMPETENCY, width: '400px' },
              { text: $scope.Text[$scope.Textcategory].DEV_METHOD, width: '200px' },
              { text: $scope.Text[$scope.Textcategory].PLAN_DETAIL, width: '400px' },
              { text: $scope.Text[$scope.Textcategory].PLAN_START, width: '200px' },
              { text: $scope.Text[$scope.Textcategory].PLAN_END, width: '200px' },
              { text: "", width: '120px' },
            ];


            var url = $location.path();
            var newString = url.replace("/", "");

            
            // insert KPIEditor start
            $scope.data = {
                DevMethodList:[],
                GapList:null,
            }
            $scope.ChildAction.SetData = function () {
            }
            $scope.ChildAction.SetData();

            $scope.ChildAction.LoadData = function () {
                
            }
            //$scope.ChildAction.LoadData();


            /* ====== wizard form control ====== */
            $scope.wizardFormControl = {
                receiptIsActive: false,
                withoutReceiptIsActive: false
            };
            /* ====== !wizard form control ====== */


            //init
            $scope.InitialConfig = function () {
                $scope.loader.enable = true;
            }
            $scope.GetMasterUI = function () {
                var URL = CONFIG.SERVER + 'HRPD/GetIDPDevelopmentMethod';
                var oRequestParameter = { InputParameter: { }, CurrentEmployee:  $scope.userData }
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.data.DevMethodList = response.data;
                }, function errorCallback(response) {
                    console.log('error IDPPlan Editor InitialConfig.', response);
                });
                var URL = CONFIG.SERVER + 'HRPD/GetGapCompetency';
                var oRequestParameter = { InputParameter: { RequestorNo: $scope.userData.EmployeeID }, CurrentEmployee: $scope.userData }
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.data.GapList = response.data;
                }, function errorCallback(response) {
                    console.log('error IDPPlan Editor InitialConfig.', response);
                });
            }
            $scope.GetMasterUI();



            function InitialConfig() {
                $scope.setBlockAction(true, $scope.Text["SYSTEM"]["PROCESSING"]);

            }

            /* ====== Inital data before load control (Finish)====== */

            //date format
            $scope.getDateFormate = function (date) {
                return getDateFormate(date);
            }
            function getDateFormate(date) { return $filter('date')(date, 'd/M/yyyy'); }
            $scope.checkMaxDate = function (index) {
                checkMaxDate(index);
            }
            function checkMaxDate(index) {
                $scope.document.Additional.PlanContent[index].PlanBeginDate = new Date($scope.document.Additional.PlanContent[index].PlanBeginDate);
                $scope.document.Additional.PlanContent[index].PlanEndDate = new Date($scope.document.Additional.PlanContent[index].PlanEndDate);
                if ($scope.document.Additional.PlanContent[index].PlanBeginDate > $scope.document.Additional.PlanContent[index].PlanEndDate) {
                    $scope.document.Additional.PlanContent[index].PlanEndDate = $scope.document.Additional.PlanContent[index].PlanBeginDate;
                }
            }


            // promtDialog Dialog
            $scope.promtDialog = function (ev, index, columnName) {
                $mdDialog.show({
                    controller: promtDialogCtrl,
                    templateUrl: 'views/common/dialog/textInput.tmpl.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    locals: {
                        params: {
                            name: columnName,
                            placeholder: 'Enter ' + columnName,
                            defaultValue: $scope.document.Additional.PlanContent[index].PlanDescription,
                            maxlength: 800
                        }
                    },
                })
               .then(function (answer) {
                   $scope.document.Additional.PlanContent[index].PlanDescription = answer;
               }, function () {

               });
            };
            function promtDialogCtrl($scope, $mdDialog, params) {
                $scope.params = params;
        
                $scope.inputValue = params.defaultValue;
                
                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.save = function (answer) {
                    if (answer.length < params.maxlength) {
                        $mdDialog.hide(answer);
                    }
                };
            }

            // promDialog GAP
            $scope.selectGAP = function (ev,index) {
                var column_header_dialog = [
                    { text: 'No', width: '50px' },
                    { text: $scope.Text["IDPPLAN_REQ"].GAP_COMPETENCY, width: '400px' },
                ];
                var selectedGapList = [];
                for (var i = 0; i <  $scope.document.Additional.PlanContent.length; i++) {
                    var newItem = {
                        GapRequestNo: $scope.document.Additional.PlanContent[i].GapRequestNo,
                        GapObjectID: $scope.document.Additional.PlanContent[i].GapObjectID
                    };
                    selectedGapList.push(newItem);
                }
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: 'views/common/dialog/idpplan.tmpl.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    locals: {
                        params: {
                            GapList: $scope.data.GapList,
                            Selected: selectedGapList,
                            column_header: column_header_dialog
                        }
                    },
                })
                .then(function (selectedItem) {
                   
                    if (index) {
                      
                    } else {
                        var newPlanContentObject = {
                            ActionBy:null,
                            DevMethodID:"DEV01", // defualt value
                            DevMethodName:"",
                            GapName: selectedItem.GapName,
                            GapObjectID: selectedItem.GapObjectID,
                            GapObjectType: selectedItem.GapObjectType,
                            GapRequestNo: selectedItem.GapRequestNo,
                            PlanBeginDate: new Date(),
                            PlanDescription:"",
                            PlanEndDate: new Date(),
                            PlanRequestNo:null,
                            PlanYear:0,
                            RequestorNo:null,
                           StatusName:null
                        }
                        $scope.document.Additional.PlanContent.push(newPlanContentObject);
                    }
                    
                }, function () {

                });

            }
            function DialogController($scope, $mdDialog, params) { // start controller
                $scope.params = params;
                $scope.selectedList = params.Selected;
                $scope.selectedActivityIndex =0;
                $scope.column_header = params.column_header;

                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.save = function (item) {
                    $mdDialog.hide(item);
                };

                $scope.removeSelected = function (item, b, c) {
                 
                    for (var a = 0 ; a < $scope.selectedList.length; a++) {
                        if ($scope.selectedList[a].GapRequestNo == item.GapRequestNo &&
                            $scope.selectedList[a].GapObjectID == item.GapObjectID
                        ) {

                            return false;
                        }
                    }    
                    return true;
                }
            } // end controller


            // event
            $scope.removeitem = function (index) {
                if (index > -1) {
                    $scope.document.Additional.PlanContent.splice(index, 1);
                }
            }



            $scope.setMethodName = function(item){
                for (var i = 0 ; i < $scope.data.DevMethodList.length; i++) {
                    if ($scope.data.DevMethodList[i].DevMethodID === item.DevMethodID) {
                        item.DevMethodName = $scope.data.DevMethodList[i].DevMethodName;
                    }
                }
            }








         
        }])
        .directive('expand', function () {
            return {
                restrict: 'A',
                controller: ['$scope', function ($scope) {
                    $scope.$on('onExpandAll', function (event, args) {
                        $scope.expanded = args.expanded;
                    });
                }]
            }; 
        });
})();
