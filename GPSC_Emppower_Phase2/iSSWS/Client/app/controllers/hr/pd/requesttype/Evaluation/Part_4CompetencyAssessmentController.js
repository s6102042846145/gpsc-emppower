﻿(function () {
    angular.module('ESSMobile')
        .controller('Part_4CompetencyAssessmentController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$q', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $q, $mdDialog) {
            $scope.TextCategory = 'EVALCOMP';
            var url = $location.path();
            
            $scope.CheckClick = function(obj,category)
            {
                if(obj.ItemControl.IsSelect_Checked == true)
                {
                    obj.ItemControl.IsSelect_Checked = false;
                    obj.ItemControl.IsDefinedGap_Visible = false;
                }
                else if(obj.ItemControl.IsSelect_Checked == false)
                {
                    obj.ItemControl.IsSelect_Checked = true;
                    obj.ItemControl.IsDefinedGap_Visible = true;
                }
                $scope.CheckMinMaxConfig(obj,category);
            }
            $scope.CheckMinMaxConfig = function (obj,category)
            {
                for (i = 0; i <= $scope.document.Additional.Competency.CompSelectConfig.length - 1; i++)
                {
                    if ($scope.document.Additional.Competency.CompSelectConfig[i].ObjectID == category.ObjectID)
                    {
                        if (obj.ItemControl.IsSelect_Checked == true)
                            $scope.document.Additional.Competency.CompSelectConfig[i].Selected += 1;
                        else
                            $scope.document.Additional.Competency.CompSelectConfig[i].Selected -= 1; 
                    }

                    if ($scope.document.Additional.Competency.CompSelectConfig[i].Selected > $scope.document.Additional.Competency.CompSelectConfig[i].MaximumSelect) {
                        obj.ItemControl.IsSelect_Checked = false;
                        obj.ItemControl.IsDefinedGap_Visible = false;
                        $scope.document.Additional.Competency.CompSelectConfig[i].Selected -= 1;

                        $mdDialog.show(
                                 $mdDialog.alert()
                                   .clickOutsideToClose(false)
                                   .title($scope.Text[$scope.TextCategory].ERROR_MAXIMUM_HEADER)
                                   .textContent($scope.Text[$scope.TextCategory].ERROR_MAXIMUM)
                                   .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                                );
                    }
                }
                //$mdDialog.show(
                //         $mdDialog.alert()
                //           .clickOutsideToClose(false)
                //           .title($scope.Text['SYSTEM']['INFORMATION'])
                //           .textContent($scope.Text['EXPENSE']['ROOMMATEISEMPTY'])
                //           .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                //        );
            }
            $scope.SetScaleName = function(obj,mode)
            {
                if (mode == "Requestor") {
                    for (i = 0; i <= obj.Scales.length - 1; i++) {
                        if (obj.Scales[i].ScaleID == obj.Score_Requester.ScaleID) {
                            obj.Score_Requester.ScaleName = obj.Scales[i].ScaleName;
                            break;
                        }
                    }
                }
                else if (mode == "Approver")
                {
                    for (i = 0; i <= obj.Scales.length - 1; i++) {
                        if (obj.Scales[i].ScaleID == obj.Score_Approver.ScaleID) {
                            obj.Score_Approver.ScaleName = obj.Scales[i].ScaleName;
                            break;
                        }
                    }
                }
            }

            $scope.promtDialog = function (ev, index, columnName, paramName, table_index, obj) {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: 'views/common/dialog/achievementSelect.tmpl.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    locals: {
                        params: {
                            title: 'AcievementEditorDialog',
                            data: $scope.data.AchievementAllActivity,
                            columns: $scope.data.behavior_col_acheiment,
                        }
                    },
                })
                .then(function (item) {
                    obj.Achievement = {
                        AchievementType: item.AchievementType,
                        ArchievementID: item.AchievementID,
                        ArchievementRequestNo: item.RequestNo,
                        Action: item.Action,
                        Result: item.Result,
                        Situation: item.Situation,
                        Spirit: item.Spirit
                    };
                }, function () {

                });
            };

            function DialogController($scope, $mdDialog, params) { 
                $scope.data = angular.copy(params.data);
                $scope.columns = angular.copy(params.columns);
                $scope.selectedIndex;

                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.save = function (answer) {
                    $mdDialog.hide(answer);
                };
            }
        }])
        .directive('expand', function () {
            return {
                restrict: 'A',
                controller: ['$scope', function ($scope) {
                    $scope.$on('onExpandAll', function (event, args) {
                        $scope.expanded = args.expanded;
                    });
                }]
            }; z
        });
})();
