﻿(function () {
    angular.module('ESSMobile')
        .controller('KPISettingViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', 'DTOptionsBuilder', 'DTColumnDefBuilder', '$mdDialog', '$mdMenu', function ($scope, $http, $routeParams, $location, $filter, CONFIG, DTOptionsBuilder, DTColumnDefBuilder, $mdDialog, $mdMenu) {
            $scope.DateNow = new Date();
            $scope.content.isShowHeader = true;
            $scope.KPIWeight = 0;

            $scope.formData = {
                IsAdvancedSearch: '', SelYear: '', SelMonth: '', SelRequestType: '', SelDocumentNo: '', SelBeginDate: $filter('date')($scope.DateNow, 'yyyy-MM-dd'), SelEndDate: $filter('date')($scope.DateNow, 'yyyy-MM-dd'), SelTravelType: '', SelCountry: ''
                , EnablePettyCash: ''
            };
            $scope.Textcategory = 'KPISETTING';
            $scope.TextcategoryKPICorp = 'KPICORP';
            $scope.HeaderText = $scope.Text[$scope.Textcategory].TITLE;
            $scope.content.Header = $scope.Text[$scope.Textcategory].TITLE;

            $scope.data = {
                selectedKPI: [],
                selectedList: []
            }

            $scope.column_header = [
              { text: 'No', width: '50px' },
              { text: $scope.Text[$scope.TextcategoryKPICorp].TYPE, width: '200px' },
              { text: $scope.Text[$scope.TextcategoryKPICorp].OBJECTTIVENAME, width: '400px' },
              { text: $scope.Text[$scope.TextcategoryKPICorp].LINKMESURE, width: '400px' },
              { text: $scope.Text[$scope.TextcategoryKPICorp].MEASURENAME, width: '400px' },
              { text: $scope.Text[$scope.TextcategoryKPICorp].WEIGHT, width: '120px' },
            ];


            $scope.ChildAction.SetData = function () {
                
            };
            $scope.ChildAction.SetData();

            $scope.ChildAction.LoadData = function () {

            };

            $scope.GetMasterUI = function () {
                var URL = CONFIG.SERVER + 'HRPD/GetKPISettingUIViewer';
                var oRequestParameter = { InputParameter: { "ActivityGroupID": $scope.document.Additional.ActivityGroupID, "ActivityTypeID": $scope.document.Additional.ActivityTypeID, "ActivityID": $scope.document.Additional.ActivityID, "RequestNo": $scope.document.RequestNo, "RequestorNo": $scope.document.RequestorNo }, CurrentEmployee: getToken(CONFIG.USER) }
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.data.selectedKPI = [];
                    $scope.masterKPISettingUI = [];
                    $scope.masterKPIType = [];
                    $scope.masterIndicator = [];
                    $scope.masterKPISettingUI = response.data.KPISettingUI;
                    $scope.masterKPIAttributeSetting = response.data.KPIAttributeManagement;

                    $scope.masterKPISettingUI.KPIDataCheckList = response.data.KPISettingUI.KPIDataCheckList;
                    $scope.data.Activities = response.data.Activities;
                    if ($scope.masterKPISettingUI.KPIDataCheckList.length > 0)
                    {
                        for (i = 0; i <= $scope.masterKPISettingUI.KPIDataCheckList[0].Objective.length - 1; i++) {
                            for (j = 0; j <= $scope.masterKPISettingUI.KPIDataCheckList[0].Objective[i].Indicator.length - 1; j++) {
                                if ($scope.masterKPISettingUI.KPIDataCheckList[0].Objective[i].Indicator[j].Selected == true) {
                                    $scope.data.selectedKPI.push($scope.masterKPISettingUI.KPIDataCheckList[0].Objective[i].Indicator[j]);
                                }
                            }
                        }
                    }
                    $scope.SummaryKPIWeight($scope.data.selectedKPI);
                    $scope.column_header_att = $scope.masterKPIAttributeSetting;

                }, function errorCallback(response) {
                    console.log('error TravelExpenseContentController InitialConfig.', response);
                });
            }
            $scope.GetMasterUI();

            $scope.InitialConfig = function () {
                $scope.selectedKPI = [];
                $scope.selectedKPI = $scope.document.Additional.Contents;
            }

            $scope.SummaryKPIWeight = function (obj) {
                if (obj.length > 0)
                {
                    for(i=0;i<=obj.length -1;i++)
                    {
                        $scope.KPIWeight = parseInt($scope.KPIWeight) + parseInt(obj[i].Content.Weight);
                    }
                }
                
            }
            /*============================================================
                All function to validate when need to create request
            ==============================================================*/

            /*Finish*/


            function UrlExists(url) {
                var http = new XMLHttpRequest();
                http.open('HEAD', url, false);
                http.send();
                return http.status != 404;
            }
            

        }])
        .directive('expand', function () {
            return {
                restrict: 'A',
                controller: ['$scope', function ($scope) {
                    $scope.$on('onExpandAll', function (event, args) {
                        $scope.expanded = args.expanded;
                    });
                }]
            };
        });
})();