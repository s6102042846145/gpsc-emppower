﻿(function () {
    angular.module('ESSMobile')
        .controller('IDPActualScController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$q', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $q, $mdDialog) {
            $scope.Textcategory = 'IDPA';
            $scope.userData = getToken(CONFIG.USER);

            $scope.column_header = [
              { text: $scope.Text[$scope.Textcategory].NO, width: '50px' },
              { text: $scope.Text[$scope.Textcategory].REQUESTNO, width: '200px' },
              { text: $scope.Text[$scope.Textcategory].CREATEDDATE, width: '200px' },
              { text: $scope.Text[$scope.Textcategory].IPD_NAME, width: '400px' },
              { text: $scope.Text[$scope.Textcategory].STATUS, width: '200px' },
              { text: '',width: '200px' }
            ];

            var url = $location.path();
            var newString = url.replace("/", "");

            // insert start
            $scope.data = {
                selectYear: '',
                IDP_Plan_List:[],
            }

            $scope.GetYearSelect = function()
            {
                var today = $filter('date')(new Date(), 'yyyy');
                $scope.year_select_list = ['2017', '2018'];
            }
            $scope.GetYearSelect();

            var GetMasterUIOnWorking = false;
            $scope.GetMasterUI = function () {
                var URL = CONFIG.SERVER + 'HRPD/GetIDPActual';
                var oRequestParameter = { InputParameter: { "ActualYear": $scope.data.selectYear, "RequestorNo": $scope.userData.EmployeeID, "CompanyCode": $scope.userData.CompanyCode }, CurrentEmployee: $scope.userData }
                if (GetMasterUIOnWorking) return;
                GetMasterUIOnWorking = true;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.data.IDP_Plan_List = response.data;
                    GetMasterUIOnWorking = false;
                }, function errorCallback(response) {
                    console.log('error Corp IDPActualScController InitialConfig.', response);
                    GetMasterUIOnWorking = false;
                });
            }

            function InitialConfig() {
                $scope.setBlockAction(true, $scope.Text["SYSTEM"]["PROCESSING"]);
            }
            /* ====== Inital data before load control (Finish)====== */


            // event
            $scope.changeYear = function () {
                if ($scope.data.selectYear) {
                    $scope.GetMasterUI();
                }
            }

            $scope.OtherParam = function (object) {
                return object.ActivityGroupID + "," + object.ActivityTypeID + "," + object.ActivityID;
            }
            $scope.openRequest = function (requestno, companyCode, keyMaster, isOwner, isDataOwner, requestType, BoxDescription) {
                //(id, keyMaster, isOwner, isDataOwner, requestType, BoxDescription)
                $location.path('/frmViewRequest/' + requestno + '/' + companyCode + '/' + keyMaster + '/' + isOwner + '/' + isDataOwner + '/' + encodeURIComponent(BoxDescription));
                //$location.path('/frmViewRequest/' + id + '/' + keyMaster + '/' + isOwner + '/' + isDataOwner + '/' + isDataOwner);
            };

            $scope.getDateFormate = function (date) {
                return getDateFormate(date);
            }
            function getDateFormate(date) { return $filter('date')(date, 'd/M/yyyy'); }

        }])
        .directive('expand', function () {
            return {
                restrict: 'A',
                controller: ['$scope', function ($scope) {
                    $scope.$on('onExpandAll', function (event, args) {
                        $scope.expanded = args.expanded;
                    });
                }]
            }; 
        });
})();
