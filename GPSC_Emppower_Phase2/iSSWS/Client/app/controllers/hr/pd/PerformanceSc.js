﻿(function () {
    angular.module('ESSMobile')
        .controller('PerformaneScCtrl', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$q', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $q, $mdDialog) {
            $scope.Textcategory = 'KPICORP';
            $scope.content.HeaderText = $scope.Text[$scope.Textcategory].TITLE;

            $scope.SessionID = "";
            $scope.selected = {
                year: "",
                position: ""

            };
            $scope.score = {
                behavior: "",
                performance: "",
                final: "",
            }

            $scope.column_header = [
            , , , ,
            ];
            $scope.column_header = [
               { text: $scope.Text[$scope.Textcategory].DURATION_TIME, width: '40%' },
               { text: $scope.Text[$scope.Textcategory].ACTIVITIES, width: '20%' },
               { text: $scope.Text[$scope.Textcategory].STATUS, width: '20%' },
               { text: $scope.Text[$scope.Textcategory].DOCUMENT, width: '20%' },
               //{ text: 'Situation', width: '25%' },
               //{ text: 'Action', width: '25%' },
               //{ text: 'Result', width: '25%' },
            ];

            $scope.master = {
                documentList: [],
                positions: [],
                years:[]

            };

            /* ====== wizard form control ====== */
           
            /* ====== !wizard form control ====== */
            $scope.init = function () {
                getMaster();
            }

            function getMaster() {
                setDefaultFunction();
                //getSelectAbleYear();
                $scope.GetActivitiesGroup();
            }
            $scope.SearchConsole = function(item)
            {
                $scope.SessionID = item.ActivityGroupID;
                $scope.GetConsole();
            }
            $scope.GetActivitiesGroup = function () {
                var URL = CONFIG.SERVER + 'HRPD/GetActivitiesGroup';
                var oRequestParameter = { CurrentEmployee: getToken(CONFIG.USER), }
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.MasterActivitiesGroup = response.data;
                    if($scope.MasterActivitiesGroup.length > 0)
                    {
                        $scope.SessionID = response.data[0].ActivityGroupID;
                        $scope.GetConsole();
                    }
                }, function errorCallback(response) {
                    console.log('error TravelExpenseContentController InitialConfig.', response);
                });
            }
            
            $scope.GetConsole = function () {
                var URL = CONFIG.SERVER + 'HRPD/GetActivitiesConsole';
                var oRequestParameter = { InputParameter: { "ActivityGroupID": $scope.SessionID }, CurrentEmployee: getToken(CONFIG.USER) }
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.ActivitiesConsole = response.data;
                    console.log(response.data);
                }, function errorCallback(response) {
                    console.log('error TravelExpenseContentController InitialConfig.', response);
                });
            }

          
            function setDefaultFunction() {
                $scope.selected.year = "2018";
                $scope.selected.position = "p1";

                getScore($scope.selected.year);
            }
            function getSelectAbleYear()
            {

            }

            $scope.getPosition = function (pId) {
                if (!$scope.master.positions) return;
                for (var i = 0; i < $scope.master.positions.length; i++) {
                    if (pId === $scope.master.positions[i].id) {
                        return $scope.master.positions[i];
                    }
                }
            }
         
            function getScore(year) {

                // call service
                $scope.score.behavior = 20
                $scope.score.performance = 80
                $scope.score.final = 100
            }


            function InitialConfig() {
            }

            // event function
            $scope.yearOnChange = function (year) {
                // do something when year on change;
            }
            $scope.positionOnChange = function (position) {
                // do something when position on change
            }
            $scope.createDoc = function (object) {
                
            }
            $scope.viewDoc = function (doc) {
                // navigate to destination
            }
            $scope.editDoc = function (doc) {
                // navigate to destination
            }

            $scope.OtherParam = function(object)
            {
                return object.ActivityGroupID + "," + object.ActivityTypeID + "," + object.ActivityID;
            }
            $scope.openRequest = function (requestno, companyCode, keyMaster, isOwner, isDataOwner, requestType, BoxDescription) {
                //(id, keyMaster, isOwner, isDataOwner, requestType, BoxDescription)
                $location.path('/frmViewRequest/' + requestno + '/' + companyCode + '/' + keyMaster + '/' + isOwner + '/' + isDataOwner + '/' + encodeURIComponent(BoxDescription));
                //$location.path('/frmViewRequest/' + id + '/' + keyMaster + '/' + isOwner + '/' + isDataOwner + '/' + isDataOwner);
            };
            /* ====== Inital data before load control (Finish)====== */
          
            

        }])
       
})();
