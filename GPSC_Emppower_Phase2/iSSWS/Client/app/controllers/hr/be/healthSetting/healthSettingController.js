﻿(function () {
    angular.module("ESSMobile").controller("healthSettingController", [
        "$scope",
        "$http",
        "$routeParams",
        "$location",
        "$filter",
        "CONFIG",
        "$q",
        "$mdDialog",
        function (
            $scope,
            $http,
            $routeParams,
            $location,
            $filter,
            CONFIG,
            $q,
            $mdDialog
        ) {
            console.log("healthSettingController", CONFIG.SERVER);
            $scope.isHideMultiCompany = true;
            $scope.GetConfingMultiCompany = function () {
                var URL = CONFIG.SERVER + "Employee/GetConfigMultiCompany";
                var oRequestParameter = {
                    InputParameter: {},
                    CurrentEmployee: $scope.employeeData,
                    Requestor: $scope.requesterData,
                    Creator: getToken(CONFIG.USER),
                };
                $http({
                    method: "POST",
                    url: URL,
                    data: oRequestParameter,
                }).then(
                    function successCallback(response) {
                        if (response.data === 1) {
                            $scope.isHideMultiCompany = false;
                        }

                        $scope.loader.enable = false;
                    },
                    function errorCallback(response) {
                        $scope.loader.enable = false;
                    }
                );
            };
            $scope.GetConfingMultiCompany();

            $scope.loadingData = false;
            $scope.Remove = "0";
            $scope.loader.enable = true;
            $scope.employeeData = getToken(CONFIG.USER);
            $scope.ListYear = [];
            $scope.HeathInsConfig = [];
            $scope.HealthSave = [];

            $scope.SelectCompanyCode = $scope.employeeData.CompanyCode;

            $scope.GetAuthorizationCompany = function () {
                var URL = CONFIG.SERVER + "workflow/GetAuthorizationCompany";
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {},
                    CurrentEmployee: $scope.employeeData,
                    Requestor: $scope.requesterData,
                    Creator: getToken(CONFIG.USER),
                };
                $http({
                    method: "POST",
                    url: URL,
                    data: oRequestParameter,
                }).then(
                    function successCallback(response) {
                        $scope.CompanyList = response.data;

                        if ($scope.CompanyList.length > 0) {
                            $scope.ddlCompany = $scope.CompanyList[0].CompanyCode;
                        }

                        $scope.loader.enable = false;
                    },
                    function errorCallback(response) {
                        $scope.loader.enable = false;
                    }
                );
            };
            $scope.GetAuthorizationCompany();

            $scope.GetHealthInsuranceConfig = function () {
                var URL = CONFIG.SERVER + "HRBE/GetHealthInsuranceConfig";
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { SelectCompany: $scope.SelectCompanyCode },
                    CurrentEmployee: $scope.employeeData,
                };
                $http({
                    method: "POST",
                    url: URL,
                    data: oRequestParameter,
                }).then(
                    function successCallback(response) {
                        console.log(response.data);

                        for (let index = 0; index < response.data.length; index++) {
                            const element = response.data[index];
                            if (element.Status !== "0") {
                                element.BeginDate = formatDate(element.BeginDate)
                                element.EndDate = formatDate(element.EndDate)
                                $scope.HeathInsConfig.push(element)
                            }

                        }
                        $scope.loader.enable = false;
                    },
                    function errorCallback(response) {
                        $scope.loader.enable = false;
                    }
                );
            };
            $scope.GetHealthInsuranceConfig();


            $scope.SaveHealthInsuranceConfig = function () {
                console.log($scope.UniFormNew);

                var URL = CONFIG.SERVER + "HRBE/SaveHealthInsuranceConfig";
                for (let index = 0; index < $scope.HealthSave.length; index++) {
                    const element = $scope.HealthSave[index];
                    console.log(element, index);
                    var oRequestParameter = {
                        InputParameter: {
                            SelectCompany: $scope.SelectCompanyCode,
                            HealthConfigID: element.HealthConfigID,
                            Year: element.Year,
                            Status: element.Status !== "Y" ? element.Status : "Y",
                            BeginDate: element.BeginDate,
                            EndDate: element.EndDate,
                        },
                    };
                    $http({
                        method: "POST",
                        url: URL,
                        data: oRequestParameter,
                    }).then(
                        function successCallback(response) {
                            console.log(response.data);

                            $scope.loader.enable = false;
                        },
                        function errorCallback(response) {
                            console.log(response.data);
                            $scope.loader.enable = false;
                        }
                    );
                    $scope.HealthSave = []
                }
            }

            $scope.changeCompany = function (CompanyCode) {
                $scope.SelectCompanyCode = CompanyCode;
                console.log(CompanyCode, $scope.SelectCompanyCode);
            };

            $scope.getIndex = function (index) {
                for (var i = 0; i < $scope.ListYear.length; i++) {
                    if ($scope.ListYear[i].YearID === index) return i;
                }
            };

            GetYear();
            function GetYear() {
                var URL = CONFIG.SERVER + "HRPY/GetYearTavi50PeriodSetting";
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { SelectCompany: $scope.SelectCompanyCode },
                    CurrentEmployee: $scope.employeeData,
                    Requestor: $scope.requesterData,
                    Creator: getToken(CONFIG.USER),
                };
                $http({
                    method: "POST",
                    url: URL,
                    data: oRequestParameter,
                }).then(
                    function successCallback(response) {
                        $scope.ListYear = response.data;
                        $scope.loader.enable = false;
                    },
                    function errorCallback(response) {
                        $scope.loader.enable = false;
                    }
                );
            }

            $scope.addNew = function () {
                $scope.HeathInsConfig.push({
                    HealthConfigID: 0,
                    BeginDate: "",
                    EndDate: "",
                    Year: 0,
                    Status: "1",
                });
            };

            function formatDate(date) {
                var d = new Date(date),
                    month = "" + (d.getMonth() + 1),
                    day = "" + d.getDate(),
                    year = d.getFullYear();

                if (month.length < 2) month = "0" + month;
                if (day.length < 2) day = "0" + day;

                return [year, month, day].join("-");
            }


            $scope.setSelected = function (newDate, type, item) {
                let date = formatDate(newDate);
                if (type === "BeginDate") {
                    item.BeginDate = date
                } else if (type === "EndDate") {
                    item.EndDate = date
                }


                if (
                    $scope.HealthSave.filter(
                        (x) => x.HealthConfigID === item.HealthConfigID
                    ).length === 0
                ) {
                    $scope.HealthSave.push(item);
                }

            }

            $scope.remove = function (item) {
                item.Status = $scope.Remove;
                if (
                    $scope.HealthSave.filter(
                        (x) => x.HealthConfigID === item.HealthConfigID
                    ).length === 0
                ) {
                    $scope.HealthSave.push(item);
                }
            };

            $scope.onSubmit = function () {
                console.log($scope.HealthSave);
                if ($scope.HealthSave.BeginDate != "" && $scope.HealthSave.EndDate != "") {
                    $scope.SaveHealthInsuranceConfig();
                } else {
                    alert("Select BeginDate or EndDate")
                }

            }

            $scope.selectYear = function (item, value) {
                console.log(item, value);
            }
        },
    ]);
})();
