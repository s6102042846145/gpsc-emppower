﻿(function () {
    angular.module('ESSMobile')
        .controller('EducationAllowanceViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {
            //#### FRAMEWORK FUNCTION ### START
            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
            $scope.ChildAction.SetData = function () {
                //Do something ...
            };

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                //Do something ...
            };

            var employeeDate = getToken(CONFIG.USER);
            $scope.ChildAction.SetData();
            //#### FRAMEWORK FUNCTION ### END

            //#### OTHERS FUNCTION ### START

            //Do something ...
            //Start
            $scope.init = function () {
                GetEducationAllowance();
            }

            $scope.EducationAllowanceList = [];

            //GetEducationAllowance
            function GetEducationAllowance() {
                var URL = CONFIG.SERVER + 'HRBE/GetEducationAllowance';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "RequestNo": $scope.document.RequestNo, "KeyYear": ""  }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: getToken(CONFIG.USER)
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.EducationAllowanceList = response.data;
                }, function errorCallback(response) {
                    // Error
                    console.log('Error GetEducationAllowance', response);
                });
            }


            //#### OTHERS FUNCTION ### END
        }]);
})();