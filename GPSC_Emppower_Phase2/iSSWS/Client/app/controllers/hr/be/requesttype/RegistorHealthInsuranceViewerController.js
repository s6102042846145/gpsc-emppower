﻿(function () {
    angular.module('ESSMobile')
        .controller('RegistorHealthInsuranceViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {
            //#### FRAMEWORK FUNCTION ### START
            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
            $scope.getDateFormat = function (date) {
                return $filter('date')(date, 'dd/MM/yyyy');
            }

            $scope.ChildAction.SetData = function () {
                //Do something ...
                $scope.RegistorDate = $scope.getDateFormat($scope.document.Additional.BEHealthInsuranceData.CreateDate);
            };

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                //Do something ...
            };

            var employeeDate = getToken(CONFIG.USER);
            $scope.ChildAction.SetData();
            //#### FRAMEWORK FUNCTION ### END

            $scope.HealthInsAllowanceItemList = [];
            $scope.GetFullName = "";
            $scope.FamilyMember = 0;
            $scope.PaymentAmount = 0;
            $scope.FAMILYMEMBER = $scope.Text["FAMILYMEMBER"];
         
            $scope.init = function () {
                GetHealthInsAllowanceItem();
                getFamilyData();
                getPersonalData();
            }

            $scope.getTotal = function () {
                var total = 0;
                for (var i = 0; i < $scope.HealthInsAllowanceItemList.length; i++) {
                    var item = $scope.HealthInsAllowanceItemList[i];
                    total += item.TotalAmount;
                    $scope.GetFullName = item.FullName;
                    $scope.FamilyMember = item.RelationshipType;
                    $scope.PaymentAmount = item.PaymentAmount;
                }
                return total;
            }


            //GetHealthInsAllowanceItem
            function GetHealthInsAllowanceItem() {
                var URL = CONFIG.SERVER + 'HRBE/GetHealthInsAllowanceItem';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "RequestNo": $scope.document.RequestNo}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.HealthInsAllowanceItemList = response.data;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    // Error
                    //console.log('Error GetEducationAllowance', response);
                    $scope.loader.enable = false;
                });
            }


            function getFamilyData() {
                // ------ get person data with employeeid and begindate -------//
                var oEmployeeData = getToken(CONFIG.USER);
                var checkDate = $filter('date')(new Date(), 'yyyy-MM-dd');
                var URL = CONFIG.SERVER + 'HRPA/GetPersonalFamilyByBE';
                var oRequestParameter = {
                    InputParameter: { "checkDate": checkDate }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                    //data: getToken(CONFIG.USER)
                }).then(function successCallback(response) {
                    // Success
                    $scope.FamilyMemberList = response.data.FamilyData;

                }, function errorCallback(response) {
                });
            }

            function getPersonalData() {
                // ------ get person data with employeeid and begindate -------//
                var oEmployeeData = getToken(CONFIG.USER);
                var checkDate = $filter('date')(new Date(), 'yyyy-MM-dd');
                var URL = CONFIG.SERVER + 'HRPA/GetPersonalData';
                var oRequestParameter = {
                    InputParameter: { "EmployeeID": oEmployeeData.EmployeeID, "checkDate": checkDate }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                    //data: getToken(CONFIG.USER)
                }).then(function successCallback(response) {
                    // Success
                    console.log('Personal response.data', response.data);
                    $scope.Fullname = response.data.PersonalData.FirstName + ' ' + response.data.PersonalData.LastName;
                    $scope.BeginDate = response.data.PersonalData.BeginDate;
                    //$scope.RegistorDate = new Date;

                }, function errorCallback(response) {
                });
            }

        }]);
})();

