﻿(function () {
    angular.module('ESSMobile')
        .controller('CremationEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal) {
            //#### FRAMEWORK FUNCTION ### START

            $scope.getMinLimit = function () {
                var nextDate = new Date($scope.document.Additional.BEBereavementInfo.BereavementDate);
                nextDate.setDate(nextDate.getDate() - 1);
                return nextDate.toISOString();
            }

            $scope.FAMILYMEMBER = $scope.Text["FAMILYMEMBER"];
            $scope.CountFam = [];
            //for (var i = 1; i <= 1; i++) {
            //    $scope.CountFam.push(i);
            //}


            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
            $scope.ChildAction.SetData = function () {
                //Do something ...
            };

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                for (var i = 0; i < 5; i++) {
                    if ($scope.document.Additional.BEBereavementItemData[i]) {
                        if ($scope.document.Additional.BEBereavementItemData[i].BereavementType == null) {
                            $scope.document.Additional.BEBereavementItemData[i].BereavementType = '999';

                        }
                    }
                }


                $scope.document.ErrorText = '';
                $scope.document.Additional.BEBereavementInfo.ErrorText = '';
                $scope.document.Additional.BEBereavementInfo.BereavementDate = $filter('date')($scope.document.Additional.BEBereavementInfo.BereavementDate, 'yyyy-MM-dd');
                $scope.document.Additional.BEBereavementInfo.FuneralDate = $filter('date')($scope.document.Additional.BEBereavementInfo.FuneralDate, 'yyyy-MM-dd');
                $scope.document.Additional.BEBereavementInfo.FuneralBeginDate = $filter('date')($scope.document.Additional.BEBereavementInfo.FuneralBeginDate, 'yyyy-MM-dd');
                $scope.document.Additional.BEBereavementInfo.FuneralEndDate = $filter('date')($scope.document.Additional.BEBereavementInfo.FuneralEndDate, 'yyyy-MM-dd');
                $scope.document.Additional.BEBereavementInfo.CremationDate = $filter('date')($scope.document.Additional.BEBereavementInfo.CremationDate, 'yyyy-MM-dd');
                $scope.document.Additional.BEBereavementInfo.PresidentDate = $filter('date')($scope.document.Additional.BEBereavementInfo.PresidentDate, 'yyyy-MM-dd');
            };

            var employeeDate = getToken(CONFIG.USER);
            $scope.ChildAction.SetData();
            //#### FRAMEWORK FUNCTION ### END

            //#### OTHERS FUNCTION ### START 


            if (!$scope.document.Additional.BEBereavementInfo.ReligionId) {
                $scope.document.Additional.BEBereavementInfo.ReligionId = "0";
            }


            // Add BereavementInfo
            $scope.addCountFam = function () {

                var data_count = $scope.document.Additional.BEBereavementItemData.length;

                if (data_count <= 5) {
                    var obj = {
                        Id: "-1",
                        BereavementType: "-1",
                        SubsidyAmount: 0,
                        PaymentFuneralAmount: 0,
                        Amount: 0,
                        DeceasedInDuty: false
                    };

                    $scope.document.Additional.BEBereavementItemData.push(obj);
                }
            };

            $scope.deleteCountFam = function (index) {
                if ($scope.document.Additional.BEBereavementItemData[index]) {
                    $scope.document.Additional.BEBereavementItemData.splice(index, 1);
                }
                $scope.CalAmount_All();
            };

            // Load Religion
            $scope.listReligion = [];
            $scope.loadReligion = function () {

                var URL = CONFIG.SERVER + 'HRBE/GetReligionAll';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.listReligion = response.data;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.loadReligion();

            //เช็ค admin 
            $scope.document.Additional.BEBereavementInfo.RoleAdmin = 0;
            angular.forEach($scope.document.ActionBy.UserRoles, function (value, key) {
                var newvalue = value.replace("#", "");

                if ($scope.document.Additional.BEBereavementInfo.RoleAdminName.indexOf(newvalue) !== -1) {
                    $scope.document.Additional.BEBereavementInfo.RoleAdmin = 1;
                }
            });

            /// pop up
            $scope.showAlert = function (ev) {
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title(ev)
                        .ok('ตกลง')
                );
            };

            /// เช็คค่า ErrorText
            $scope.$watch('document.Additional.BEBereavementInfo.ErrorText', function (newValue, oldValue) {

                if (newValue != null) {
                    if (newValue != '') {

                        setTimeout(function () {
                            $scope.showAlert(newValue);
                        }, 300);

                    }
                }

            });

            $scope.setSelectedDOB_BereavementDate = function (selectedDate) {
                $scope.document.Additional.BEBereavementInfo.BereavementDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
                if ($scope.document.Additional.BEBereavementInfo.FuneralDate < $scope.document.Additional.BEBereavementInfo.BereavementDate)
                    $scope.document.Additional.BEBereavementInfo.FuneralDate = $scope.document.Additional.BEBereavementInfo.BereavementDate;
                if ($scope.document.Additional.BEBereavementInfo.FuneralBeginDate < $scope.document.Additional.BEBereavementInfo.BereavementDate)
                    $scope.document.Additional.BEBereavementInfo.FuneralBeginDate = $scope.document.Additional.BEBereavementInfo.BereavementDate;
                if ($scope.document.Additional.BEBereavementInfo.FuneralEndDate < $scope.document.Additional.BEBereavementInfo.BereavementDate)
                    $scope.document.Additional.BEBereavementInfo.FuneralEndDate = $scope.document.Additional.BEBereavementInfo.BereavementDate;
                if ($scope.document.Additional.BEBereavementInfo.CremationDate < $scope.document.Additional.BEBereavementInfo.BereavementDate)
                    $scope.document.Additional.BEBereavementInfo.CremationDate = $scope.document.Additional.BEBereavementInfo.BereavementDate;
                if ($scope.document.Additional.BEBereavementInfo.PresidentDate < $scope.document.Additional.BEBereavementInfo.BereavementDate)
                    $scope.document.Additional.BEBereavementInfo.PresidentDate = $scope.document.Additional.BEBereavementInfo.BereavementDate;
            };

            $scope.setSelectedDOB_FuneralDate = function (selectedDate) {
                $scope.document.Additional.BEBereavementInfo.FuneralDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };

            $scope.setSelectedDOB_FuneralBeginDate = function (selectedDate) {
                $scope.document.Additional.BEBereavementInfo.FuneralBeginDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };

            $scope.setSelectedDOB_FuneralEndDate = function (selectedDate) {
                $scope.document.Additional.BEBereavementInfo.FuneralEndDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };

            $scope.setSelectedDOB_CremationDate = function (selectedDate) {
                $scope.document.Additional.BEBereavementInfo.CremationDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };

            $scope.setSelectedDOB_PresidentDate = function (selectedDate) {
                $scope.document.Additional.BEBereavementInfo.PresidentDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };

            /* Before
            $scope.CalAmount = function (i) {

                console.log('calamount');
                var BereavementType = $scope.document.Additional.BEBereavementItemData[i].BereavementType;

                if (BereavementType > 200 && BereavementType != 999) {
                    BereavementType = 2;
                }
                //จำนวนเงินที่ได้รับ
                if (BereavementType) {
                    var Type1 = $scope.document.Additional.BEBereavementConfigData.find(x => x.FamilyType == BereavementType && x.BereavementType == 1);
                    if (Type1) {
                        var Amount = Type1.Quota;
                        var MinValue = Type1.MinValue;
                        if (Type1 != 'undefined') {
                            if (Type1.Type == 'Value') {
                                if (Amount < MinValue) {
                                    $scope.document.Additional.BEBereavementItemData[i].SubsidyAmount = MinValue;

                                } else {
                                    $scope.document.Additional.BEBereavementItemData[i].SubsidyAmount = Amount;
                                }
                            } else if (Type1.Type == 'SalaryRate') {
                                var Salary = $scope.document.Additional.BEBereavementInfo.Salary;
                                var CalSalary = Amount * Salary;
                                if (CalSalary < MinValue) {
                                    $scope.document.Additional.BEBereavementItemData[i].SubsidyAmount = MinValue;
                                } else {
                                    $scope.document.Additional.BEBereavementItemData[i].SubsidyAmount = CalSalary;
                                }
                            }
                        }
                    } else {
                        $scope.document.Additional.BEBereavementItemData[i].SubsidyAmount = 0;
                    }


                    //จำนวนเงินชดเชย
                    var Type2 = $scope.document.Additional.BEBereavementConfigData.find(x => x.FamilyType == BereavementType && x.BereavementType == 2);
                    if (Type2) {
                        var Amount = Type2.Quota;
                        var MinValue = Type2.MinValue;
                        if (Type2 != 'undefined') {
                            if (Type2.Type == 'Value') {
                                if (Amount < MinValue) {
                                    $scope.document.Additional.BEBereavementItemData[i].PaymentFuneralAmount = MinValue;
                                } else {
                                    $scope.document.Additional.BEBereavementItemData[i].PaymentFuneralAmount = Amount;
                                }
                            } else if (Type2.Type == 'SalaryRate') {
                                var Salary = $scope.document.Additional.BEBereavementInfo.Salary;
                                var CalSalary = Amount * Salary;
                                if (CalSalary < MinValue) {
                                    $scope.document.Additional.BEBereavementItemData[i].PaymentFuneralAmount = MinValue;
                                } else {
                                    $scope.document.Additional.BEBereavementItemData[i].PaymentFuneralAmount = CalSalary;
                                }
                            }
                        }
                    } else {
                        $scope.document.Additional.BEBereavementItemData[i].PaymentFuneralAmount = 0;
                    }

                    $scope.document.Additional.BEBereavementItemData[i].Amount = $scope.document.Additional.BEBereavementItemData[i].SubsidyAmount + $scope.document.Additional.BEBereavementItemData[i].PaymentFuneralAmount;

                    //จำนวนเงินรวม
                    var TotalAmount = 0;
                    for (var i = 0; i < 5; i++) {
                        if ($scope.document.Additional.BEBereavementItemData[i]) {
                            if ($scope.document.Additional.BEBereavementItemData[i].Amount) {
                                TotalAmount += parseFloat($scope.document.Additional.BEBereavementItemData[i].Amount);
                            }
                        }
                    }
                    $scope.document.Additional.BEBereavementInfo.TotalAmount = TotalAmount;
                }
            };
            */

            $scope.newCalAmount = function (item) {

                var TotalAmount = 0;

                // 1. check duplicate 
                var count_duplicate = 0;
                var id_temp = item.Id;
                var bereavementType_temp = "";
                if ($scope.document.Additional.BEBereavementItemData.length > 0) {

                    for (var i = 0; i < $scope.document.Additional.BEBereavementItemData.length; i++) {

                        if ($scope.document.Additional.BEBereavementItemData[i].Id > -1
                            && $scope.document.Additional.BEBereavementItemData[i].Id == id_temp) {
                            count_duplicate++;
                        }
                    }

                    if (count_duplicate > 1) {

                        item.SubsidyAmount = 0;
                        item.PaymentFuneralAmount = 0;
                        item.Amount = 0;

                        //จำนวนเงินรวม
                        TotalAmount = 0;
                        for (var i = 0; i < $scope.document.Additional.BEBereavementItemData.length; i++) {
                            TotalAmount = TotalAmount + parseFloat($scope.document.Additional.BEBereavementItemData[i].Amount);
                        }

                        $scope.document.Additional.BEBereavementInfo.TotalAmount = TotalAmount;

                        var modalInstance = $uibModal.open({
                            animation: $scope.animationsEnabled,
                            templateUrl: 'errorExportPdf.ng-popup.html',
                            controller: 'errorExportPdfController',
                            backdrop: 'static',
                            keyboard: false,
                            size: 'sp',
                            resolve: {
                                items: function () {
                                    var data = {
                                        requesterData: ""
                                    };
                                    return data;
                                }
                            }
                        });
                        item.Id = "-1";
                        return;
                    }
                }

                // 2.หา bereavementType
                if ($scope.document.Additional.FamilyData.length > 0) {

                    for (var i = 0; i < $scope.document.Additional.FamilyData.length; i++) {

                        if ($scope.document.Additional.FamilyData[i].UniqueBereavementId == item.Id) {
                            item.BereavementType = $scope.document.Additional.FamilyData[i].FamilyMember;
                        }
                    }
                }

                if (item.Id > -1) {

                    var BereavementType = item.BereavementType;
                    if (BereavementType > 200 && BereavementType != 999) {
                        BereavementType = 2;
                    }
                    //จำนวนเงินที่ได้รับ
                    if (BereavementType) {
                        var Type1 = $scope.document.Additional.BEBereavementConfigData.find(x => x.FamilyType == BereavementType && x.BereavementType == 1);
                        if (Type1) {
                            var Amount = Type1.Quota;
                            var MinValue = Type1.MinValue;
                            if (Type1 != 'undefined') {
                                if (Type1.Type == 'Value') {
                                    if (Amount < MinValue) {
                                        item.SubsidyAmount = MinValue;
                                    } else {
                                        item.SubsidyAmount = Amount;
                                    }
                                } else if (Type1.Type == 'SalaryRate') {
                                    var Salary = $scope.document.Additional.BEBereavementInfo.Salary;
                                    var CalSalary = Amount * Salary;
                                    if (CalSalary < MinValue) {
                                        item.SubsidyAmount = MinValue;
                                    } else {
                                        item.SubsidyAmount = CalSalary;
                                    }
                                }
                            }
                        } else {
                            item.SubsidyAmount = 0;
                        }


                        //จำนวนเงินชดเชย
                        var Type2 = $scope.document.Additional.BEBereavementConfigData.find(x => x.FamilyType == BereavementType && x.BereavementType == 2);
                        if (Type2) {
                            var Amount = Type2.Quota;
                            var MinValue = Type2.MinValue;
                            if (Type2 != 'undefined') {
                                if (Type2.Type == 'Value') {
                                    if (Amount < MinValue) {
                                        item.PaymentFuneralAmount = MinValue;
                                    } else {
                                        item.PaymentFuneralAmount = Amount;
                                    }
                                } else if (Type2.Type == 'SalaryRate') {
                                    var Salary = $scope.document.Additional.BEBereavementInfo.Salary;
                                    var CalSalary = Amount * Salary;
                                    if (CalSalary < MinValue) {
                                        item.PaymentFuneralAmount = MinValue;
                                    } else {
                                        item.PaymentFuneralAmount = CalSalary;
                                    }
                                }
                            }
                        } else {

                            item.PaymentFuneralAmount = 0;
                        }

                        item.Amount = item.SubsidyAmount + item.PaymentFuneralAmount;

                        //จำนวนเงินรวม
                        //var TotalAmount = 0;
                        //for (var i = 0; i < $scope.document.Additional.BEBereavementItemData.length; i++) {
                        //    TotalAmount = TotalAmount + parseFloat($scope.document.Additional.BEBereavementItemData[i].Amount);
                        //}
                        //for (var i = 0; i < $scope.document.Additional.BEBereavementItemData.length; i++) {
                        //    if ($scope.document.Additional.BEBereavementItemData[i]) {
                        //        if ($scope.document.Additional.BEBereavementItemData[i].Amount) {
                        //            TotalAmount += parseFloat(item.Amount);
                        //        }
                        //    }
                        //}

                        // $scope.document.Additional.BEBereavementInfo.TotalAmount = TotalAmount;
                    }

                }
                else {

                    item.SubsidyAmount = 0;
                    item.PaymentFuneralAmount = 0;
                    item.Amount = 0;
                }

                //จำนวนเงินรวม
                
                for (var i = 0; i < $scope.document.Additional.BEBereavementItemData.length; i++) {
                    TotalAmount = TotalAmount + parseFloat($scope.document.Additional.BEBereavementItemData[i].Amount);
                }

                $scope.document.Additional.BEBereavementInfo.TotalAmount = TotalAmount;


            };


            $scope.CalAmount_All = function () {
                if ($scope.document.Additional.BEBereavementItemData.length > 0) {
                    for (var i = 0; i < $scope.document.Additional.BEBereavementItemData.length; i++) {

                        if ($scope.document.Additional.BEBereavementItemData[i]) {
                            if ($scope.document.Additional.BEBereavementItemData[i].BereavementType != 999) {
                                if ($scope.document.Additional.BEBereavementInfo.ResponsibleFuneral == 1) {

                                    $scope.newCalAmount($scope.document.Additional.BEBereavementItemData[i]);

                                } else {
                                    $scope.document.Additional.BEBereavementItemData[i].SubsidyAmount = 0;
                                    $scope.document.Additional.BEBereavementItemData[i].PaymentFuneralAmount = 0;
                                    $scope.document.Additional.BEBereavementItemData[i].Amount = 0;
                                    $scope.document.Additional.BEBereavementInfo.TotalAmount = 0;
                                }
                            }
                        }
                    }
                } else {
                    $scope.document.Additional.BEBereavementInfo.TotalAmount = 0;
                }

            };


            //#### OTHERS FUNCTION ### END
        }]);
})();