﻿(function () {
    angular.module('ESSMobile')
        .controller('RegistorHealthInsuranceEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {
            //#### FRAMEWORK FUNCTION ### START

            $scope.dateNow = (new Date()).toISOString().slice(0, 10);
            $scope.FAMILYMEMBER = $scope.Text["FAMILYMEMBER"];
            $scope.TITLENAME = $scope.Text["TITLENAME"];
            $scope.getDateFormat = function (date) {
                return $filter('date')(date, 'dd/M/yyyy');
            }
            $scope.SetAmount = function () {
               
                //for (var i = 0; i < 10; i++) {
                //    if ($scope.document.Additional.BEHealthInsAllowanceItemData[i].AmountExcludeVAT == 0) {
                //        $scope.document.Additional.BEHealthInsAllowanceItemData[i].AmountExcludeVAT = '';
                //    }
                //    if ($scope.document.Additional.BEHealthInsAllowanceItemData[i].VAT == 0) {
                //        $scope.document.Additional.BEHealthInsAllowanceItemData[i].VAT = '';
                //    }
                //    if ($scope.document.Additional.BEHealthInsAllowanceItemData[i].TotalAmount == 0) {
                //        $scope.document.Additional.BEHealthInsAllowanceItemData[i].TotalAmount = '';
                //    }
                //}

            };
            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
         
            $scope.ChildAction.SetData = function () {
                getPersonalData();
                $scope.SetAmount();
                getFamilyData();
                
               
               // console.log('RegistorDate', $scope.RegistorDate);

            };

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                
               

               
               
               

            };




            var employeeDate = getToken(CONFIG.USER);
            $scope.ChildAction.SetData();

            function getFamilyData() {
                // ------ get person data with employeeid and begindate -------//
                var oEmployeeData = getToken(CONFIG.USER);
                var checkDate = $filter('date')(new Date(), 'yyyy-MM-dd');
                var URL = CONFIG.SERVER + 'HRPA/GetPersonalFamilyByBE';
                var oRequestParameter = {
                    InputParameter: { "checkDate": checkDate }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                    //data: getToken(CONFIG.USER)
                }).then(function successCallback(response) {
                    // Success
                    $scope.FamilyMemberList = response.data.FamilyData;

                }, function errorCallback(response) {
                });
            }

            function getPersonalData() {
                // ------ get person data with employeeid and begindate -------//
                var oEmployeeData = getToken(CONFIG.USER);
                var checkDate = $filter('date')(new Date(), 'yyyy-MM-dd');
                var URL = CONFIG.SERVER + 'HRPA/GetPersonalData';
                var oRequestParameter = {
                    InputParameter: { "EmployeeID": oEmployeeData.EmployeeID, "checkDate": checkDate }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                    //data: getToken(CONFIG.USER)
                }).then(function successCallback(response) {
                    // Success
                    console.log('Personal response.data', response.data);
                    $scope.Fullname = response.data.PersonalData.FirstName + ' ' + response.data.PersonalData.LastName;
                    $scope.BeginDate = response.data.PersonalData.BeginDate;
                    $scope.RegistorDate = $scope.document.Additional.BEHealthInsuranceData.CreateDate;
                    //$scope.RegistorDate = new Date;

                }, function errorCallback(response) {
                });
            }
           
        }]);
})();