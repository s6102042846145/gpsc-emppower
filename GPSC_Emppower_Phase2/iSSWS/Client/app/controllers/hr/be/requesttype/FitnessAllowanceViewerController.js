﻿(function () {
    angular.module('ESSMobile')
        .controller('FitnessAllowanceViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {
            //#### FRAMEWORK FUNCTION ### START
            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
            $scope.ChildAction.SetData = function () {
                //Do something ...
            };

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                //Do something ...
            };

            var employeeDate = getToken(CONFIG.USER);
            $scope.ChildAction.SetData();
            //#### FRAMEWORK FUNCTION ### END

            $scope.FitnessAllowanceItemList = [];
            $scope.PaymentAmount = 0;

            $scope.init = function () {
                GetFitnessAllowanceItem();
            }

            $scope.getTotal = function () {
                var total = 0;
                for (var i = 0; i < $scope.FitnessAllowanceItemList.length; i++) {
                    var item = $scope.FitnessAllowanceItemList[i];
                    total += item.TotalAmount;
                    $scope.PaymentAmount = item.PaymentAmount;
                }
                return total;
            }

            //GetFitnessAllowanceItem
            function GetFitnessAllowanceItem() {
                var URL = CONFIG.SERVER + 'HRBE/GetFitnessAllowanceItem';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "RequestNo": $scope.document.RequestNo }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.FitnessAllowanceItemList = response.data;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    // Error
                    //console.log('Error GetFitnessAllowance', response);
                    $scope.loader.enable = false;
                });
            }
        }]);
})();