﻿(function () {
    angular.module('ESSMobile')
        .controller('HealthInsuranceViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {
            //#### FRAMEWORK FUNCTION ### START
            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
            $scope.ChildAction.SetData = function () {
                //Do something ...
            };

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                //Do something ...
            };

            var employeeDate = getToken(CONFIG.USER);
            $scope.ChildAction.SetData();
            //#### FRAMEWORK FUNCTION ### END

            $scope.HealthInsAllowanceItemList = [];
            $scope.GetFullName = "";
            $scope.FamilyMember = 0;
            $scope.PaymentAmount = 0;
            $scope.FAMILYMEMBER = $scope.Text["FAMILYMEMBER"];
         
            $scope.init = function () {
                GetHealthInsAllowanceItem();
            }

            $scope.getTotal = function () {
                var total = 0;
                for (var i = 0; i < $scope.HealthInsAllowanceItemList.length; i++) {
                    var item = $scope.HealthInsAllowanceItemList[i];
                    total += item.TotalAmount;
                    $scope.GetFullName = item.FullName;
                    $scope.FamilyMember = item.RelationshipType;
                    $scope.PaymentAmount = item.PaymentAmount;
                }
                return total;
            }


            //GetHealthInsAllowanceItem
            function GetHealthInsAllowanceItem() {
                var URL = CONFIG.SERVER + 'HRBE/GetHealthInsAllowanceItem';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "RequestNo": $scope.document.RequestNo}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.HealthInsAllowanceItemList = response.data;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    // Error
                    //console.log('Error GetEducationAllowance', response);
                    $scope.loader.enable = false;
                });
            }
        }]);
})();

