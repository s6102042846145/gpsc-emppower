﻿(function () {
    angular.module('ESSMobile')
        .controller('HealthInsuranceEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {
            //#### FRAMEWORK FUNCTION ### START

            $scope.dateNow = (new Date()).toISOString().slice(0, 10);
            $scope.FAMILYMEMBER = $scope.Text["FAMILYMEMBER"];
            $scope.SetAmount = function () {
               
                for (var i = 0; i < 10; i++) {
                    if ($scope.document.Additional.BEHealthInsAllowanceItemData[i].AmountExcludeVAT == 0) {
                        $scope.document.Additional.BEHealthInsAllowanceItemData[i].AmountExcludeVAT = '';
                    }
                    if ($scope.document.Additional.BEHealthInsAllowanceItemData[i].VAT == 0) {
                        $scope.document.Additional.BEHealthInsAllowanceItemData[i].VAT = '';
                    }
                    if ($scope.document.Additional.BEHealthInsAllowanceItemData[i].TotalAmount == 0) {
                        $scope.document.Additional.BEHealthInsAllowanceItemData[i].TotalAmount = '';
                    }
                }

            };
            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
            $scope.ChildAction.SetData = function () {
                
                $scope.SetAmount();

            };

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                
                $scope.document.ErrorText = '';
                $scope.document.Additional.BEHealthInsAllowanceData.ErrorText = '';

                for (var i = 0; i < 10; i++) {
                    if (!$scope.document.Additional.BEHealthInsAllowanceItemData[i].AmountExcludeVAT) {
                        $scope.document.Additional.BEHealthInsAllowanceItemData[i].AmountExcludeVAT = '0'
                    }

                    if (!$scope.document.Additional.BEHealthInsAllowanceItemData[i].VAT) {
                        $scope.document.Additional.BEHealthInsAllowanceItemData[i].VAT = '0'
                    }

                    if (!$scope.document.Additional.BEHealthInsAllowanceItemData[i].TotalAmount) {
                        $scope.document.Additional.BEHealthInsAllowanceItemData[i].TotalAmount = '0'
                    }

                    if ($scope.document.Additional.BEHealthInsAllowanceItemData[i] != null) {

                        $scope.document.Additional.BEHealthInsAllowanceItemData[i].RecieptDate = $filter('date')($scope.document.Additional.BEHealthInsAllowanceItemData[i].RecieptDate, 'yyyy-MM-dd');

                    }

                }
               
               

                if ($scope.document.Additional.BEHealthInsAllowanceData.RelationshipType == null) {
                    $scope.document.Additional.BEHealthInsAllowanceData.RelationshipType = '0'
                   
                }

            };

            var employeeDate = getToken(CONFIG.USER);
            $scope.ChildAction.SetData();
            //#### FRAMEWORK FUNCTION ### END

            //#### OTHERS FUNCTION ### START 

            //เช็ค admin 
            $scope.document.Additional.BEHealthInsAllowanceData.RoleAdmin = 0;
            angular.forEach($scope.document.ActionBy.UserRoles, function (value, key) {
                var newvalue = value.replace("#", "");

                if ($scope.document.Additional.BEHealthInsAllowanceData.RoleAdminName.indexOf(newvalue) !== -1) {
                    $scope.document.Additional.BEHealthInsAllowanceData.RoleAdmin = 1;
                }
            });


            /// pop up
            $scope.showAlert = function (ev) {
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title(ev)
                        .ok('ตกลง')
                );
            };

            /// เช็คค่า ErrorText
            $scope.$watch('document.Additional.BEHealthInsAllowanceData.ErrorText', function (newValue, oldValue) {

                if (newValue != null) {
                    if (newValue != '') {
                        $scope.SetAmount();
                        setTimeout(function () {
                             $scope.showAlert(newValue);
                        }, 300);

                    }
                }

            });




            $scope.CountVendor = [];
            for (var i = 1; i <= 10; i++) {
               
                $scope.CountVendor.push(i);
            }

            $scope.setSelectedDOB = function (selectedDate,index) {

                $scope.document.Additional.BEHealthInsAllowanceItemData[index].RecieptDate = $filter('date')(selectedDate, 'yyyy-MM-dd');

            };

            $scope.CalTotalAmount = function (selectedDate, index) {
                var TotalAmount = 0;
                for (var i = 0; i < 10; i++) {
                    if ($scope.document.Additional.BEHealthInsAllowanceItemData[i].TotalAmount) {
                       
                        TotalAmount += parseFloat($scope.document.Additional.BEHealthInsAllowanceItemData[i].TotalAmount);
                    }
                }
                $scope.document.Additional.BEHealthInsAllowanceData.TotalAmount = TotalAmount;
               
                GetQuotaUse();
            };


            $scope.CalTotalVat = function (i) {

                var TotalVat = 0;

                if ($scope.document.Additional.BEHealthInsAllowanceItemData[i].AmountExcludeVAT) {
                    TotalVat = parseFloat($scope.document.Additional.BEHealthInsAllowanceItemData[i].AmountExcludeVAT);
                }

                if ($scope.document.Additional.BEHealthInsAllowanceItemData[i].VAT) {
                    TotalVat = parseFloat($scope.document.Additional.BEHealthInsAllowanceItemData[i].VAT);
                }

                if ($scope.document.Additional.BEHealthInsAllowanceItemData[i].AmountExcludeVAT && $scope.document.Additional.BEHealthInsAllowanceItemData[i].VAT) {
                    TotalVat = parseFloat($scope.document.Additional.BEHealthInsAllowanceItemData[i].AmountExcludeVAT) + parseFloat($scope.document.Additional.BEHealthInsAllowanceItemData[i].VAT);

                }

                $scope.document.Additional.BEHealthInsAllowanceItemData[i].TotalAmount = TotalVat;
                $scope.CalTotalAmount();
            };

            $scope.$watch('document.Additional.BEHealthInsAllowanceData.Year', function (value) {
                GetQuotaUse();
            });

            $scope.$watch('document.Additional.BEHealthInsAllowanceData.RelationshipType', function (value) {
                GetQuotaUse();
            });

            function GetQuotaUse() {


                var Year = "";

                for (var i = 0; i < 10; i++) {

                    if ($scope.document.Additional.BEHealthInsAllowanceItemData[i].VendorName != null) {
                        Year = $filter('date')($scope.document.Additional.BEHealthInsAllowanceItemData[i].RecieptDate, 'yyyy');
                    }

                }

                //var Year = $scope.document.Additional.BEHealthInsAllowanceData.Year;
                var RelationshipType = $scope.document.Additional.BEHealthInsAllowanceData.RelationshipType
                var URL = CONFIG.SERVER + 'HRBE/HealthInsAllowanceCheckQuotaUse';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "RequestNo": $scope.document.RequestNo, "Year": Year, "RelationshipType": RelationshipType }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.document.Requestor
                    , Creator: getToken(CONFIG.USER)

                };

                if (Year != null && RelationshipType!=null) {
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        var QuotaUsed = response.data;

                        var Quota = 0
                        if (RelationshipType) { 
                        Quota = $scope.document.Additional.BEHealthInsAllowanceConfigData.find(x => x.RelationshipType == RelationshipType).Quota;
                        }
                            // เช็คจำนวนเงินที่เบิกเกินกำหนด
                        var QuotaNew = Quota - QuotaUsed;

                        //alert(QuotaUsed + ' ' + Quota + ' ' + QuotaNew);
                        if (QuotaNew <= 0) {
                            $scope.document.Additional.BEHealthInsAllowanceData.PaymentAmount = 0;
                        }
                        else {
                            //ยอดเงินตามที่สามารถเบิกได้จริงตามโควต้า
                            Quota = Quota - QuotaUsed;
                            var AmountReimburse = Quota - $scope.document.Additional.BEHealthInsAllowanceData.TotalAmount;
                            if (AmountReimburse < 0) {
                                $scope.document.Additional.BEHealthInsAllowanceData.PaymentAmount = Quota;
                               
                            }
                            else {
                                $scope.document.Additional.BEHealthInsAllowanceData.PaymentAmount = $scope.document.Additional.BEHealthInsAllowanceData.TotalAmount;
                               
                            }
                        }

                    }, function errorCallback(response) {
                        // Error
                        console.log('Error GetQuotaUse', response);
                    });
                }


            }

            //#### OTHERS FUNCTION ### END
        }]);
})();