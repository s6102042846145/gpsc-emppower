﻿(function () {
    angular.module('ESSMobile')
        .controller('FitnessAllowanceEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG,$mdDialog) {
            //#### FRAMEWORK FUNCTION ### START
            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen

            $scope.dateNow = (new Date()).toISOString().slice(0, 10);
            $scope.SetAmount = function () {

                for (var i = 0; i < 10; i++) {
                    if ($scope.document.Additional.BEFitnessAllowanceItemData[i].AmountExcludeVAT == 0) {
                        $scope.document.Additional.BEFitnessAllowanceItemData[i].AmountExcludeVAT = '';
                    }
                    if ($scope.document.Additional.BEFitnessAllowanceItemData[i].VAT == 0) {
                        $scope.document.Additional.BEFitnessAllowanceItemData[i].VAT = '';
                    }
                    if ($scope.document.Additional.BEFitnessAllowanceItemData[i].TotalAmount == 0) {
                        $scope.document.Additional.BEFitnessAllowanceItemData[i].TotalAmount = '';
                    }
                }

            };

            $scope.ChildAction.SetData = function () {

                $scope.SetAmount();

            };

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                $scope.document.ErrorText = '';
                $scope.document.Additional.BEFitnessAllowanceData.ErrorText = '';

                for (var i = 0; i < 10; i++) {
                    if (!$scope.document.Additional.BEFitnessAllowanceItemData[i].AmountExcludeVAT) {
                        $scope.document.Additional.BEFitnessAllowanceItemData[i].AmountExcludeVAT = '0';
                    }

                    if (!$scope.document.Additional.BEFitnessAllowanceItemData[i].VAT) {
                        $scope.document.Additional.BEFitnessAllowanceItemData[i].VAT = '0';
                    }

                    if (!$scope.document.Additional.BEFitnessAllowanceItemData[i].TotalAmount) {
                        $scope.document.Additional.BEFitnessAllowanceItemData[i].TotalAmount = '0';
                    }

                    if ($scope.document.Additional.BEFitnessAllowanceItemData[i] != null) {

                        $scope.document.Additional.BEFitnessAllowanceItemData[i].RecieptDate = $filter('date')($scope.document.Additional.BEFitnessAllowanceItemData[i].RecieptDate, 'yyyy-MM-dd');

                    }

                }
            };

            var employeeDate = getToken(CONFIG.USER);
            $scope.ChildAction.SetData();
            //#### FRAMEWORK FUNCTION ### END

            //#### OTHERS FUNCTION ### START 

            //เช็ค admin 
            $scope.document.Additional.BEFitnessAllowanceData.RoleAdmin = 0;
            angular.forEach($scope.document.ActionBy.UserRoles, function (value, key) {
                var newvalue = value.replace("#", "");

                if ($scope.document.Additional.BEFitnessAllowanceData.RoleAdminName.indexOf(newvalue) !== -1) {
                    $scope.document.Additional.BEFitnessAllowanceData.RoleAdmin = 1;
                }
            });



            /// pop up
            $scope.showAlert = function (ev) {
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title(ev)
                        .ok('ตกลง')
                );
            };

            /// เช็คค่า ErrorText
            $scope.$watch('document.Additional.BEFitnessAllowanceData.ErrorText', function (newValue, oldValue) {

                if (newValue != null) {
                    if (newValue != '') {
                        $scope.SetAmount();
                        setTimeout(function () {
                           
                            $scope.showAlert(newValue);
                           
                        }, 300);

                    }
                }

            });




            $scope.CountVendor = [];
            for (var i = 1; i <= 10; i++) {

                $scope.CountVendor.push(i);
            }

            $scope.setSelectedDOB = function (selectedDate, index) {

                $scope.document.Additional.BEFitnessAllowanceItemData[index].RecieptDate = $filter('date')(selectedDate, 'yyyy-MM-dd');

            };

            $scope.CalTotalAmount = function () {

                var TotalAmount = 0;
                for (var i = 0; i < 10; i++) {
                    if ($scope.document.Additional.BEFitnessAllowanceItemData[i].TotalAmount) {
                        TotalAmount += parseFloat($scope.document.Additional.BEFitnessAllowanceItemData[i].TotalAmount);
                    }

                }
                $scope.document.Additional.BEFitnessAllowanceData.TotalAmount = TotalAmount;
                GetQuotaUse();
            };


            $scope.CalTotalVat = function (i) {
               
                var TotalVat = 0;

                if ($scope.document.Additional.BEFitnessAllowanceItemData[i].AmountExcludeVAT) {
                    TotalVat = parseFloat($scope.document.Additional.BEFitnessAllowanceItemData[i].AmountExcludeVAT)
                }

                if ($scope.document.Additional.BEFitnessAllowanceItemData[i].VAT) {
                    TotalVat = parseFloat($scope.document.Additional.BEFitnessAllowanceItemData[i].VAT)
                }

                if ($scope.document.Additional.BEFitnessAllowanceItemData[i].AmountExcludeVAT && $scope.document.Additional.BEFitnessAllowanceItemData[i].VAT ) {
                    TotalVat = parseFloat($scope.document.Additional.BEFitnessAllowanceItemData[i].AmountExcludeVAT) + parseFloat($scope.document.Additional.BEFitnessAllowanceItemData[i].VAT);
                    
                }

                $scope.document.Additional.BEFitnessAllowanceItemData[i].TotalAmount = TotalVat;
                $scope.CalTotalAmount();
            };




           



            function GetQuotaUse() {

                var Year = "";

                for (var i = 0; i < 10; i++) {
                   
                    if ($scope.document.Additional.BEFitnessAllowanceItemData[i].VendorName != null) {
                        Year = $filter('date')($scope.document.Additional.BEFitnessAllowanceItemData[i].RecieptDate, 'yyyy');
                    }

                }
              

                var URL = CONFIG.SERVER + 'HRBE/FitnessAllowanceCheckQuotaUse';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "RequestNo": $scope.document.RequestNo, "Year": Year }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.document.Requestor
                    , Creator: getToken(CONFIG.USER)
                  
                };

                if (Year != null) {
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        var QuotaUsed = response.data.split(',')[1];
                        var Quota = response.data.split(',')[0];
                        // เช็คจำนวนเงินที่เบิกเกินกำหนด
                        var QuotaNew = Quota - QuotaUsed;

                        //alert(QuotaUsed + ' ' + Quota + ' ' + QuotaNew);
                        if (QuotaNew <= 0)
                        {
                            $scope.document.Additional.BEFitnessAllowanceData.PaymentAmount = 0;
                        }
                        else
                        {
                            //ยอดเงินตามที่สามารถเบิกได้จริงตามโควต้า
                            Quota = Quota - QuotaUsed;
                            var AmountReimburse = Quota - $scope.document.Additional.BEFitnessAllowanceData.TotalAmount;
                            if (AmountReimburse < 0)
                            {
                                 $scope.document.Additional.BEFitnessAllowanceData.PaymentAmount = Quota;
                            }
                            else
                            {
                                $scope.document.Additional.BEFitnessAllowanceData.PaymentAmount = $scope.document.Additional.BEFitnessAllowanceData.TotalAmount;
                            }
                        }

                    }, function errorCallback(response) {
                        // Error
                        console.log('Error GetQuotaUse', response);
                    });
                }


            }

            

            //#### OTHERS FUNCTION ### END
        }]);
})();