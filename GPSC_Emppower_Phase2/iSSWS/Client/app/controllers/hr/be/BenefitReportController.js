﻿(function () {
    angular.module('ESSMobile')
        .controller('BenefitReportController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {



            // Get Config Multi Company
            $scope.isHideMultiCompany = true;
            $scope.GetConfingMultiCompany = function () {
                var URL = CONFIG.SERVER + 'Employee/GetConfigMultiCompany';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data === 1) {
                        $scope.isHideMultiCompany = false;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetConfingMultiCompany();


            $scope.category = [{ name: '1' }, { name: '2' }, { name: '3' }, { name: '4' }, { name: '5' }];


            $scope.isActiveFlexibleBenefitDashboard = true;
            $scope.isActiveHealthInsuranceDashboard = true;
            $scope.GetIsActiveHealthInsuranceDashboard = function () {
                var URL = CONFIG.SERVER + 'HRBE/GetIsActiveHealthInsuranceDashboard';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.isActiveHealthInsuranceDashboard = response.data;
                    if (!$scope.isActiveHealthInsuranceDashboard) {
                        $scope.category.splice(4, 1);
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetIsActiveHealthInsuranceDashboard();

            $scope.GetIsActiveFlexibleBenefitDashboard = function () {
                var URL = CONFIG.SERVER + 'HRBE/GetIsActiveFlexibleBenefitDashboard';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.isActiveFlexibleBenefitDashboard = response.data;
                    if (!$scope.isActiveFlexibleBenefitDashboard) {
                        $scope.category.splice(2, 1);
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetIsActiveFlexibleBenefitDashboard();


            $scope.GetIsActiveHealthInsuranceDashboard_ByCompany = function () {
                var URL = CONFIG.SERVER + 'HRBE/GetIsActiveHealthInsuranceDashboard_ByCompany';
                var oRequestParameter = {
                    InputParameter: { "SelectCompanyCode": $scope.SelectCompanyCode }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.isActiveHealthInsuranceDashboard = response.data;
                    if (!$scope.isActiveHealthInsuranceDashboard) {
                        $scope.category.splice(4, 1);
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };

            $scope.GetIsActiveFlexibleBenefitDashboard_ByCompany = function () {
                var URL = CONFIG.SERVER + 'HRBE/GetIsActiveFlexibleBenefitDashboard_ByCompany';
                var oRequestParameter = {
                    InputParameter: { "SelectCompanyCode": $scope.SelectCompanyCode }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.isActiveFlexibleBenefitDashboard = response.data;
                    if (!$scope.isActiveFlexibleBenefitDashboard) {
                        $scope.category.splice(2, 1);
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };


            $scope.employeeData = getToken(CONFIG.USER);
            $scope.SelectCompanyCode = $scope.employeeData.CompanyCode;

            $scope.back = function () {
                var url = '/frmViewContent/5000';
                $location.path(url);
            };

            $scope.GetAuthorizationCompany = function () {
                var URL = CONFIG.SERVER + 'workflow/GetAuthorizationCompany';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.CompanyList = response.data;

                    if ($scope.CompanyList.length > 0) {
                        $scope.ddlCompany = $scope.CompanyList[0].CompanyCode;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }
            $scope.GetAuthorizationCompany();


            $scope.changeCompany = function (CompanyCode) {
                $scope.SelectCompanyCode = CompanyCode;

                $scope.category = [{ name: '1' }, { name: '2' }, { name: '3' }, { name: '4' }, { name: '5' }];

                $scope.GetIsActiveHealthInsuranceDashboard_ByCompany();
                $scope.GetIsActiveFlexibleBenefitDashboard_ByCompany();
            };


            function download(dataurl, filename) {
                var a = document.createElement("a");
                a.href = dataurl;
                a.setAttribute("download", filename);
                var b = document.createEvent("MouseEvents");
                b.initEvent("click", false, true);
                a.dispatchEvent(b);
                return false;
            }

            $scope.file_download = "";

            //GetReport
            $scope.GetReport = function (type_download) {

                var str_category_export = "";
                var str_status_export = "";
                var type_file = "";
                $scope.loader.enable = true;
                angular.forEach($scope.selection_category, function (value) {
                    str_category_export += value + ",";
                });

                angular.forEach($scope.selection_status, function (value) {
                    str_status_export += value + ",";
                });

                if (type_download == 'PDF') {
                    type_file = 'pdf';
                } else {
                    type_file = 'xlsx';

                }


                var URL = CONFIG.SERVER + 'HRBE/GetReport';

                $scope.employeeData = getToken(CONFIG.USER);
                console.log('set employee-->' + $scope.employeeData);
                var UpdateDate_From = $scope.date_from_select;
                var UpdateDate_To = $scope.date_to_select;
                var oRequestParameter = {
                    InputParameter: {
                        "UpdateDate_From": UpdateDate_From
                        , "UpdateDate_To": UpdateDate_To
                        , Type: str_category_export
                        , Status: str_status_export
                        , ReportName: $scope.Text["BE_REPORT"].REPORT_NAME
                        , LanguageCode: $scope.employeeData.Language
                        , Employee_id: $scope.employeeData.EmployeeID
                        , EmployeeName: $scope.employeeData.Name
                        , ExportType: type_download //"PDF" //EXCEL
                        , DateText: $scope.Text["BE_REPORT"].DATETEXT
                        , TimeText: $scope.Text["BE_REPORT"].TIMETEXT
                        , T_EMPLOYEEID: $scope.Text['BENEFITREPORT'].T_EMPLOYEEID
                        , T_FULLNAME: $scope.Text['BENEFITREPORT'].T_FULLNAME
                        , T_REQUESTNO: $scope.Text['BENEFITREPORT'].T_REQUESTNO
                        , T_CATEGORYBENEFIT: $scope.Text['BENEFITREPORT'].T_CATEGORYBENEFIT
                        , T_CREATEDATE: $scope.Text['BENEFITREPORT'].T_CREATEDATE
                        , T_UPDATE: $scope.Text['BENEFITREPORT'].T_UPDATE
                        , T_AMOUNT: $scope.Text['BENEFITREPORT'].T_AMOUNT
                        , T_STATUS: $scope.Text['BENEFITREPORT'].T_STATUS
                        , T_POSTSAP: $scope.Text['BENEFITREPORT'].T_POSTSAP
                        , T_POSTMESSAGE: $scope.Text['BENEFITREPORT'].POSTMESSAGE
                        , "SelectCompany": $scope.SelectCompanyCode
                    }

                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    console.log('success');
                    var file_name = "BEReport" + $scope.employeeData.EmployeeID;
                    if (type_download == "PDF") {
                        file_name += ".pdf";
                    } else if (type_download == "EXCEL") {
                        file_name += ".xls";
                    }
                    var url = CONFIG.SERVER + 'Client/Report/' + file_name;
                    download(url, file_name);
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    console.log(response);

                    $scope.loader.enable = false;
                });
            };

            $scope.data = {
                nPageSize: 10,
                arrFilterd: [],
                arrSplited: [],
                selectedColumn: 0,
                tFilter: '',
                pages: []
            };
            //---------------------- -- SET DATE  -- -----------------------------------------
            $scope.date_from_select = new Date().toISOString().split("T")[0];
            $scope.date_to_select = new Date().toISOString().split("T")[0];


            $scope.setdatefrom = function (selectedDateFrom) {
                $scope.date_from_select = $filter('date')(selectedDateFrom, 'yyyy-MM-dd');
            };

            $scope.setdateto = function (selectedDateTo) {
                $scope.date_to_select = $filter('date')(selectedDateTo, 'yyyy-MM-dd');
            };

            //------------------------ CHECK BOX -------------------------------------------------------



            $scope.status = ['DRAFT', 'WAITFORVERIFY', 'COMPLETED', 'POSTED'];
            $scope.selection_status = [];
            $scope.selection_category = [];

            $scope.toggleSelection_status = function toggleSelection_status(gender) {
                var idx = $scope.selection_status.indexOf(gender);
                if (idx > -1) {
                    $scope.selection_status.splice(idx, 1);
                }
                else {
                    $scope.selection_status.push(gender);
                }
            };

            //$scope.toggleSelection_category = function toggleSelection_category(gender) {
            //    var idx = $scope.selection_category.indexOf(gender);
            //    if (idx > -1) {
            //        $scope.selection_category.splice(idx, 1);
            //    }
            //    else {
            //        $scope.selection_category.push(gender);
            //    }
            //};


            // select all items
            $scope.selectAll = function () {
  
                $scope.selection_category = [];
                angular.forEach($scope.category, function (item) {
                    if ($scope.selectedAll == true) {
                        $scope.selection_category.push(item.name);
                    } else {
                        $scope.selection_category = [];
                    }
                    item.Selected = $scope.selectedAll; 
                });

                //alert($scope.selectedAll);
                //alert($scope.selection_category);
            };

            // use the array "every" function to test if ALL items are checked
            $scope.checkIfAllSelected = function () {

                $scope.selectedAll = $scope.category.every(function (item) {

                    return item.Selected == true
                });
            };


            $scope.checkboxSelected = function () {
                $scope.selection_category = [];

                angular.forEach($scope.category, function (item) {
                    if (item.Selected == true) {
                        $scope.selection_category.push(item.name);
                    } 
                });
            };

            //ONCLICK
            $scope.get_report = function () {

                if (new Date($scope.date_from_select) > new Date($scope.date_to_select)) {

                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popupContainer')))
                            .clickOutsideToClose(true)
                            .title('วันที่เริ่มต้นต้องน้อยกว่าวันที่สิ้นสุด!')
                            .ok('ตกลง')
                    );

                    return false;
                }

                var str_category = "";
                var str_status = "";

                angular.forEach($scope.selection_category, function (value) {
                    str_category += value + ",";
                });

                angular.forEach($scope.selection_status, function (value) {
                    str_status += value + ",";
                });


                GetBenefitReport($scope.date_from_select, $scope.date_to_select, str_category, str_status);
            };

            //RESET
            $scope.reset_report = function () {
                $scope.date_from_select = new Date().toISOString().split("T")[0];
                $scope.date_to_select = new Date().toISOString().split("T")[0];
                $scope.selection_status = [];
                $scope.selection_category = [];
                $scope.selectedAll = [];
                $scope.data = [];

                angular.forEach($scope.category, function (item) {
                    item.Selected = false;
                });
            }

            //EXPORT REPORT
            $scope.export_report = function () {

            }

            //GET DATA REPORT
            function GetBenefitReport(date_from, date_to, benefit_type, benefit_status) {
                var URL = CONFIG.SERVER + 'HRBE/GetBenefitReport';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "UpdateDate_From": date_from, "UpdateDate_To": date_to, "Benefit_Type": benefit_type, "Benefit_Status": benefit_status, "language": $scope.employeeData.Language, "SelectCompany": $scope.SelectCompanyCode }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.data = response.data;

                    if ($scope.data.length == 0) {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.querySelector('#popupContainer')))
                                .clickOutsideToClose(true)
                                .title('ไม่พบข้อมูล!')
                                .ok('ตกลง')
                        );
                    };

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }

            //Pagination of nested objects in angularjs
            $scope.pagin = {
                currentPage: 1, itemPerPage: '10', numPage: 1
            }
            $scope.filterDatas = function (searchTxt) {
                if (!$scope.data || !$scope.data.length) return [];
                var datas = $filter('filter')($scope.data, searchTxt);
                datas = $filter('filter')(datas, $scope.advacneOptionFilter);

                return datas;
            }
            $scope.genDatas = function (searchTxt) {
                var datas = $scope.filterDatas(searchTxt);
                setNumOfPage(datas);


                var res = [];

                if ($scope.pagin.numPage > 1) {
                    datas.forEach(function (value, i) {
                        var from = (parseInt($scope.pagin.currentPage) - 1) * parseInt($scope.pagin.itemPerPage);
                        var to = from + parseInt($scope.pagin.itemPerPage);

                        if (from <= i && to > i)
                            res.push(value);
                    });
                } else {
                    res = datas.slice();
                }

                return res;
            }
            var setNumOfPage = function (datas) {
                var totalItems = 0;
                if (datas) totalItems = datas.length;
                $scope.pagin.numPage = Math.ceil(totalItems / $scope.pagin.itemPerPage);
                if ($scope.pagin.numPage <= 0) $scope.pagin.numPage = 1;

                if ($scope.pagin.currentPage > $scope.pagin.numPage) $scope.pagin.currentPage = 1;
            }
            $scope.changePage = function (targetPage) {
                $scope.pagin.currentPage = angular.copy(targetPage);
            }
            $scope.getPages = function () {
                var pages = [];

                for (var i = 0; i < $scope.pagin.numPage; i++) {
                    pages.push((i + 1));
                }

                return pages;
            }
        }]);
})();