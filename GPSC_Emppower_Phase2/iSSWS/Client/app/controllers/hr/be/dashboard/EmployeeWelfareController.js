﻿(function () {
    angular.module('ESSMobile')
        .controller('EmployeeWelfareController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', '$timeout', '$sce', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal, $timeout, $sce) {


            $scope.profile = getToken(CONFIG.USER);
            $scope.isActiveFlexibleBenefitDashboard = true;
            $scope.isActiveHealthInsuranceDashboard = true;
            $scope.GetIsActiveFlexibleBenefitDashboard = function () {
                var URL = CONFIG.SERVER + 'HRBE/GetIsActiveFlexibleBenefitDashboard';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.isActiveFlexibleBenefitDashboard = response.data;

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetIsActiveFlexibleBenefitDashboard();

            $scope.GetIsActiveHealthInsuranceDashboard = function () {
                var URL = CONFIG.SERVER + 'HRBE/GetIsActiveHealthInsuranceDashboard';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.isActiveHealthInsuranceDashboard = response.data;

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetIsActiveHealthInsuranceDashboard();


            $scope.list_all_benefit = [];
            for (var i = 1; i <= 8; i++) {

                var data_tab = {
                    TabId: 0,
                    TextCode:""
                };
                switch (i)
                {
                    case 1:
                        data_tab.TabId = "1";
                        data_tab.TextCode = "Absence";
                        break;
                    case 2:
                        data_tab.TabId = "2";
                        data_tab.TextCode = "ProvidentFund";
                        break;
                    case 3:
                        data_tab.TabId = "3";
                        data_tab.TextCode = "FlexibleBenefit";
                        break;
                    case 4:
                        data_tab.TabId = "4";
                        data_tab.TextCode = "EducationChild";
                        break;
                    case 5:
                        data_tab.TabId = "5";
                        data_tab.TextCode = "Fitness";
                        break;
                    case 6:
                        data_tab.TabId = "6";
                        data_tab.TextCode = "HeathCareParent";
                        break;
                    case 7:
                        data_tab.TabId = "7";
                        data_tab.TextCode = "WelfareOther";
                        break;
                    case 8:
                        data_tab.TabId = "8";
                        data_tab.TextCode = "AmountRevice";
                        break;
                }
                $scope.list_all_benefit.push(data_tab);
            }

            $scope.openPopupPincode = function () {

                var modalInstance = $uibModal.open({
                    animation: $scope.animationsEnabled,
                    templateUrl: 'openPincodeWelfare.ng-popup.html',
                    controller: 'openPincodeWelfareController',
                    backdrop: 'static',
                    keyboard: false,
                    size: 'sp',
                    resolve: {
                        items: function () {
                            var data = {
                                requestor: $scope.requesterData,
                                text: $scope.Text,
                                fnForgetPincode: function () {
                                    $scope.forgetPincode();
                                },
                                fnLoadEmployeeWelfare: function (pincode, year) {
                                    $scope.verifyPincode(pincode, year);
                                }
                            };
                            return data;
                        }
                    }
                });
            };
            $scope.openPopupPincode();

            $scope.forgetPincode = function () {
                $location.path('frmViewContent/907');
            };

            $scope.renderHtml = function (html_code) {
                return $sce.trustAsHtml(html_code);
            };
            $scope.tabMenu = 0;
            $scope.dataPincode = "";
            $scope.selectyear = "";
            $scope.list_year = [];
            $scope.verifyPincode = function (pincode) {

                var URL = CONFIG.SERVER + 'HRBE/VerifyPincdeBenefitsEmployee';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {
                        Pincode: pincode
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {

                    if (response.data.IsVerifyPincode) {
                        // Default Absence
                        $scope.tabMenu = "1";
                        $scope.dataPincode = pincode;
                        $scope.list_year = response.data.ListYear;
                        $scope.selectyear = $scope.list_year[0].toString();
                        $scope.loadAbsenceByEmployee($scope.dataPincode, $scope.selectyear);
                    }
                    else {
                        $scope.tabMenu = 0;
                        $scope.dataPincode = "";
                        $scope.selectyear = "";
                        $scope.openPopupPincode();
                    }
                    $scope.loader.enable = false;
                }, function (response) {

                    $scope.loader.enable = false;
                });

            };

            $scope.changeTabmenu = function (tabId, year) {
                console.log(tabId,year);
                $scope.tabMenu = tabId;
                $scope.selectyear = year;
                switch (tabId) {
                    case "1": $scope.loadAbsenceByEmployee($scope.dataPincode, year); break;
                    case "2": $scope.loadProvidentfundByEmployee($scope.dataPincode, year); break;
                    case "3": $scope.loadAlternativeWelfareByEmployee($scope.dataPincode, year); break;
                    case "4": $scope.loadChildEducationByEmployee($scope.dataPincode, year); break;
                    case "5": $scope.loadFitnessByEmployee($scope.dataPincode, year); break;
                    case "6": $scope.loadHealthParentByEmployee($scope.dataPincode, year); break;
                    case "7": $scope.loadAnnounceWelfare($scope.dataPincode, year); break;
                    case "8": $scope.loadAmountWorkingByEmployee($scope.dataPincode, year); break;
                }

            };

            $scope.changeYear = function (tabId, year) {

                $scope.tabMenu = tabId;
                $scope.selectyear = year;

                switch (tabId) {
                    case "1": $scope.loadAbsenceByEmployee($scope.dataPincode, $scope.selectyear); break;
                    case "2": $scope.loadProvidentfundByEmployee($scope.dataPincode, $scope.selectyear); break;
                    case "3": $scope.loadAlternativeWelfareByEmployee($scope.dataPincode, $scope.selectyear); break;
                    case "4": $scope.loadChildEducationByEmployee($scope.dataPincode, $scope.selectyear); break;
                    case "5": $scope.loadFitnessByEmployee($scope.dataPincode, $scope.selectyear); break;
                    case "6": $scope.loadHealthParentByEmployee($scope.dataPincode, $scope.selectyear); break;
                    case "7": $scope.loadAnnounceWelfare($scope.dataPincode, $scope.selectyear); break;
                    case "8": $scope.loadAmountWorkingByEmployee($scope.dataPincode, $scope.selectyear); break;
                }
            };

            $scope.list_absence = [];
            $scope.list_absence_row1 = [];
            $scope.list_absence_row2 = [];
            $scope.list_absence_row3 = [];
            $scope.list_absence_row4 = [];
            $scope.list_absence_row5 = [];
            $scope.loadAbsenceByEmployee = function (pincode, year) {

                if (!pincode)
                    return;

                if (!year)
                    return;

                var URL = CONFIG.SERVER + 'HRBE/LoadAbsenceByEmployee';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {
                        Pincode: pincode,
                        Year: year
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {

                    $scope.list_absence_row1 = [];
                    $scope.list_absence_row2 = [];
                    $scope.list_absence_row3 = [];
                    $scope.list_absence_row4 = [];
                    $scope.list_absence_row5 = [];

                    if (response.data.length > 0) {
                        for (var i = 0; i < response.data.length; i++) {
                            if (i <= 3)
                                $scope.list_absence_row1.push(response.data[i]);
                            else if (i <= 7)
                                $scope.list_absence_row2.push(response.data[i]);
                            else if (i <= 11)
                                $scope.list_absence_row3.push(response.data[i]);
                            else if (i <= 15)
                                $scope.list_absence_row4.push(response.data[i]);
                            else if (i <= 19)
                                $scope.list_absence_row5.push(response.data[i]);
                        }
                    }
                    $scope.list_absence = response.data;
                    $scope.loader.enable = false;

                }, function (response) {
                    $scope.loader.enable = false;
                });

            };

            $scope.model_providentfundByEmployee = {};
            $scope.loadProvidentfundByEmployee = function (pincode, year) {

                if (!pincode)
                    return;

                if (!year)
                    return;

                var URL = CONFIG.SERVER + 'HRBE/LoadProvidentfundByEmployee';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {
                        Pincode: pincode,
                        Year: year
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {

                    console.log(response);
                    $scope.model_providentfundByEmployee = response.data;
                    $scope.loader.enable = false;

                }, function (response) {
                    $scope.loader.enable = false;
                });
            };

            $scope.model_reimburseByEmployee = {};
            $scope.loadAlternativeWelfareByEmployee = function (pincode, year) {

                if (!pincode)
                    return;

                if (!year)
                    return;

                var URL = CONFIG.SERVER + 'HRBE/LoadAlternativeWelfareByEmployee';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {
                        Pincode: pincode,
                        Year: year
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {

                    $scope.model_reimburseByEmployee = response.data;
                    $scope.loader.enable = false;

                }, function (response) {
                    $scope.loader.enable = false;
                });
            };

            $scope.model_ChildEducationByEmployee = {};
            $scope.loadChildEducationByEmployee = function (pincode, year) {

                if (!pincode)
                    return;

                if (!year)
                    return;

                var URL = CONFIG.SERVER + 'HRBE/LoadChildEducationByEmployee';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {
                        Pincode: pincode,
                        Year: year
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {

                    $scope.model_ChildEducationByEmployee = response.data;
                    console.log(response);
                    $scope.loader.enable = false;

                }, function (response) {
                    $scope.loader.enable = false;
                });

            }; 

            $scope.model_FitnessByEmployee = {};
            $scope.loadFitnessByEmployee = function (pincode, year) {

                if (!pincode)
                    return;

                if (!year)
                    return;

                var URL = CONFIG.SERVER + 'HRBE/LoadFitnessByEmployee';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {
                        Pincode: pincode,
                        Year: year
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {
                    console.log(response);
                    $scope.model_FitnessByEmployee = response.data;
                    $scope.loader.enable = false;
                }, function (response) {
                    $scope.loader.enable = false;
                });
            };

            $scope.model_HealthParentByEmployee = {};
            $scope.loadHealthParentByEmployee = function (pincode, year) {

                if (!pincode)
                    return;

                if (!year)
                    return;

                var URL = CONFIG.SERVER + 'HRBE/LoadHealthParentByEmployee';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {
                        Pincode: pincode,
                        Year: year
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {

                    $scope.model_HealthParentByEmployee = response.data;
                    console.log(response);
                    $scope.loader.enable = false;

                }, function (response) {
                    $scope.loader.enable = false;
                });

            };

            $scope.model_list_annountment = [];
            $scope.loadAnnounceWelfare = function (pincode, year) {

                if (!pincode)
                    return;

                if (!year)
                    return;

                var URL = CONFIG.SERVER + 'HRBE/LoadAnnounceWelfare';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {
                        Pincode: pincode,
                        Year: year
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {

                    console.log(response);
                    $scope.model_list_annountment = response.data;

                    $scope.loader.enable = false;

                }, function (response) {
                    $scope.loader.enable = false;
                });

            };

            $scope.model_income_employee = {};
            $scope.loadAmountWorkingByEmployee = function (pincode, year) {

                if (!pincode)
                    return;

                if (!year)
                    return;

                var URL = CONFIG.SERVER + 'HRBE/LoadAmountWorkingByEmployee';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {
                        Pincode: pincode,
                        Year: year
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {
                    $scope.model_income_employee = response.data;
                    $scope.loader.enable = false;
                }, function (response) {
                    $scope.loader.enable = false;
                });

            };


        }]).controller('openPincodeWelfareController', ['items', '$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', '$uibModalInstance', function (items, $scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal, $uibModalInstance) {

            $scope.MsgValidate = "";
            $scope.OldPinCode = "";
            $scope.Text = items.text;

            $scope.getValidatePinCode = function () {

                if (!$scope.OldPinCode) {
                    $scope.MsgValidate = "กรุณาระบุ PINCODE";
                    return;
                }

                items.fnLoadEmployeeWelfare($scope.OldPinCode, 2020);
                $uibModalInstance.close();
            };

            $scope.forgetPincode = function () {
                $uibModalInstance.close();
                items.fnForgetPincode();
            };

            $scope.closeModal = function () {
                $uibModalInstance.close();
            };

        }]);
})();