﻿(function () {
    angular.module('ESSMobile')
        .controller('DataExample1Controller', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', '$timeout', '$q', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal, $timeout, $q) {


            /* Auto Complete */

            $scope.item_orgunit = [{
                "OrgUnit": "23300098",
                "ParentOrgUnit": "23300258",
                "OrgName": "ERV",
                "OrgFullName": "หน่วยซ่อมบำรุงโรงไฟฟ้าพื้นที่ระยอง",
                "OrgLevel": null,
                "OrgPath": "CEO->COO->ECE->ERV",
                "OrgUnitPath": "23300075->23300204->23300258->23300098",
                "OrgDisplay": "===>ERV"
            },
            {
                "OrgUnit": "23300099",
                "ParentOrgUnit": "23300098",
                "OrgName": "OEH",
                "OrgFullName": "แผนกบำรุงรักษาไฟฟ้า",
                "OrgLevel": null,
                "OrgPath": "CEO->COO->ECE->ERV->OEH",
                "OrgUnitPath": "23300075->23300204->23300258->23300098->23300099",
                "OrgDisplay": "====>OEH"
            },
            {
                "OrgUnit": "23300138",
                "ParentOrgUnit": "23300098",
                "OrgName": "OLH",
                "OrgFullName": "แผนกเสถียรภาพระบบโรงงาน",
                "OrgLevel": null,
                "OrgPath": "CEO->COO->ECE->ERV->OLH",
                "OrgUnitPath": "23300075->23300204->23300258->23300098->23300138",
                "OrgDisplay": "====>OLH"
            },
            {
                "OrgUnit": "23300163",
                "ParentOrgUnit": "23300098",
                "OrgName": "OVH",
                "OrgFullName": "แผนกบํารุงรักษาเครื่องกล",
                "OrgLevel": null,
                "OrgPath": "CEO->COO->ECE->ERV->OVH",
                "OrgUnitPath": "23300075->23300204->23300258->23300098->23300163",
                "OrgDisplay": "====>OVH"
            }];

            $scope.querySearchOrgUnit = querySearchOrgUnit;
            $scope.selectedItemChange = selectedItemChange;
            $scope.searchTextChange = searchTextChange;
            $scope.simulateQuery = true;

            function querySearchOrgUnit(query) {

                var results = query ? $scope.item_orgunit.filter(createFilterFor(query)) : $scope.item_orgunit,deferred;
                if ($scope.simulateQuery) {
                    deferred = $q.defer();
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 250, false);
                    return deferred.promise;
                } else {
                    return results;
                }
            }

            function createFilterFor(query) {
                //var lowercaseQuery = query.toLowerCase();
                return function filterFn(orgunit) {
                    var textquery = orgunit.OrgDisplay;
                    return textquery.indexOf(query) >= 0;
                };

            }

            function searchTextChange(text_query) {
            };


            var tempResult;
            function selectedItemChange(orgUnit) {
                // Assign ค่าและค้นหาจำนวนเงิน
                if (orgUnit) {

                    if (tempResult) {

                        if (orgUnit.OrgUnit != tempResult.OrgUnit) {
                            // มีการเลือก Orgunit ใหม่ยิง API
                            console.log('เปลี่ยน Org');
                        }
                        else {
                            // ไม่มีการเปลี่ยน Orgunit
                            console.log('ไม่เปลี่ยน Org');
                        }
                        
                    }
                    else {
                        tempResult = orgUnit;
                        // ยิง API
                        console.log('ดึงข้อมูลครั้งแรก');
                    }

                }
            }

            
            $scope.tryToSelect = function (text) {
                if (text) {
                    for (var i = 0; i < $scope.item_orgunit.length; i++) {
                        if ($scope.item_orgunit[i].OrgDisplay == text) {
                            tempResult = $scope.item_orgunit[i];
                            break;
                        }
                    }
                    $scope.searchText = '';
                    $scope.autocomplete.selectedItem = null;
                }
            };

            $scope.checkText = function (text) {

                var result = null;
                for (var i = 0; i < $scope.item_orgunit.length; i++) {
                    if ($scope.item_orgunit[i].OrgDisplay == text) {
                        tempResult = $scope.item_orgunit[i];
                        break;
                    }
                }

                if (result) {
                    $scope.searchText = result.OrgDisplay;
                    $scope.selectedItemChange(result);
                    $scope.autocomplete.selectedItem = result;
                } else if (tempResult) {
                    $scope.searchText = tempResult.OrgDisplay;
                    $scope.selectedItemChange(tempResult);
                    $scope.autocomplete.selectedItem = tempResult;
                }
            };

            /* End Auto Complete */

            const optionsnumber = {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            };

            var arr_month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
            var arr_overtime = [50000, 60000, 70000, 80000, 90000, 100000, 110000, 120000, 130000, 140000, 150000, 160000];
            var arr_salary = [100000, 200000, 300000, 400000, 500000, 600000, 700000, 800000, 900000, 1000000, 1100000, 1200000];
            var arr_proportionOvertime = [10.52, 20.52, 30.52, 40.52, 50.52, 60.52, 70.52, 80.52, 90.52, 95.52, 98.52, 50];

            $scope.overtimeInYear = {
                labels: arr_month,
                datasetOverride: [
                    {
                        label: "Salary",
                        borderWidth: 1,
                        yAxisID: 'y-axis-1',
                        type: 'bar'
                    },
                    {
                        label: "Overtime",
                        borderWidth: 1,
                        yAxisID: 'y-axis-1',
                        type: 'bar'
                    },
                    {
                        label: "%OT to Salary",
                        borderWidth: 3,
                        yAxisID: 'y-axis-2',
                        type: 'line'
                    }
                ],
                data: [arr_salary,
                    arr_overtime,
                    arr_proportionOvertime
                ],
                //series: "series",
                //kcolours: ['#C1C1C1', '#CE4116', '#872E13'],
                colours: [{ // default
                    backgroundColor: "#C1C1C1",
                    borderColor: "#C1C1C1",
                }, { // default
                    backgroundColor: "#CE4116",
                    borderColor: "#CE4116",

                }, { // default

                    borderColor: "#872E13",
                    backgroundColor: "#FFFFFF"
                }],
                options: {
                    tooltips: {
                        callbacks: {
                            //labelColor: function (tooltipItem, chart) {
                            //    var dataset = chart.config.data.datasets[tooltipItem.datasetIndex];
                            //    return {
                            //        borderColor: dataset.backgroundColor,
                            //        backgroundColor: dataset.backgroundColor
                            //    }
                            //},

                            label: function (tooltipItem, data) {
                                var label = data.datasets[tooltipItem.datasetIndex].label || '';

                                if (label) {
                                    label += ': ';
                                }
                                label += data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toLocaleString('en', optionsnumber);
                                return label;
                            }
                        }
                    },
                    legend: {
                        display: true,
                        position: 'top'
                    },
                    scales: {
                        xAxes: [{
                            stacked: true,
                            categoryPercentage: .2,
                            barPercentage: 2,
                            ticks: {
                                beginAtZero: true
                            }
                        }],
                        yAxes: [{
                            id: 'y-axis-1',
                            type: 'linear',
                            display: true,
                            position: 'left',
                            stacked: true,
                            ticks: {
                                beginAtZero: true,
                                callback: function (label, index, labels) {
                                    return label.toLocaleString('en', optionsnumber);
                                }
                            }
                        }, {
                            id: 'y-axis-2',
                            type: 'linear',
                            display: true,
                            position: 'right',
                            stacked: true,
                            ticks: {
                                beginAtZero: true,
                                suggestedMin: 0,
                                suggestedMax: 100,
                                callback: function (label, index, labels) {
                                    return label + "%";
                                }
                            }
                        }]
                    }
                }
            };




            const options = {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            };
            //OT Baht by Level
            //เปอร์เซน
            var test1 = [10, 60, 40, 30, 25, 30];
            var test2 = [70, 30, 55, 50, 25, 40];
            var test3 = [20, 10, 5, 20, 50, 30];

            //จำนวนคน
            var count_baht1 = [2400, 5400, 3600, 3400, 5500, 5300];
            var count_baht2 = [35, 78, 78, 21, 76, 13];
            var count_baht3 = [87, 54, 54, 54, 23, 55];

            var dataset = [
                count_baht1, count_baht2, count_baht3
            ];
            $scope.employeeByLevel = {
                labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                datasetOverride: [
                    {
                        label: "Junior (Level 1-2)",
                        borderWidth: 1,
                        type: 'bar'
                    },
                    {
                        label: "Junior (Level 3-4)",
                        borderWidth: 1,
                        type: 'bar'
                    },
                    {
                        label: "Junior (Level 5-8)",
                        borderWidth: 1,
                        type: 'bar'
                    },
                    {
                        label: "Senior (Level 9-10)",
                        borderWidth: 1,
                        type: 'bar'
                    }
                ],
                data: [
                    test1
                    , test2
                    , test3
                ],
                series: "series",
                colours: ['#C1C1C1', '#C1C1C1', '#CE4116', '#872E13'],
                options: {
                    tooltips: {
                        callbacks: {
                            label: function (tooltipItem, data) {
                                var label = data.datasets[tooltipItem.datasetIndex].label || '';

                                if (label) {
                                    label += ': ';
                                }

                                label += dataset[tooltipItem.datasetIndex][tooltipItem.index] + "(" + tooltipItem.yLabel + " % )";
                                return label;
                            }
                        }
                    },
                    legend: {
                        display: true,
                        position: 'top'
                    },
                    scales: {
                        xAxes: [{
                            stacked: true,
                            categoryPercentage: .2,
                            barPercentage: 2,
                            ticks: {
                                beginAtZero: true
                            }
                        }],
                        yAxes: [{
                            id: 'y-axis-1',
                            type: 'linear',
                            display: true,
                            position: 'left',
                            stacked: true,
                            ticks: {
                                beginAtZero: true,
                                suggestedMin: 0,
                                suggestedMax: 100
                            }
                        }]
                    }
                }

            };

            $scope.listYearLeaveRequest = [2019, 2020, 2021];
            $scope.expression = "<div class='font-18'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut laboret dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehende Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut laboret dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisiut liquip ex ea commodo consequat. Duis aute irure dolor in reprehende</div>"
            $scope.expressionxxx = "<div class='font-14'>จะนับทุกๆ 7 วัน โดยเริ่มตั้งแต่วันที่ 1 และสิ้นสุดวันที่สิ้นเดือน ของทุกเดือน (เศษวันที่เหลือของแต่ละเดือน คิดเป็น 1 สัปดาห์) <br>- กรณีเบิกค่าล่วงเวลาครบ 36 ชั่วโมง/สัปดาห์ (ลำดับการอนุมัติตามสายบังคับบัญชาปกติ คือระดับ ผจ.ส่วน โดยไม่ถึง CEO) <br>- กรณีเบิกค่าล่วงเวลาเกิน 36 ชั่วโมง/สัปดาห์ (ลำดับการอนุมัติตามสายบังคับบัญชาของสายงานปกติ คือระดับรองฯ โดยไม่ถึง CEO)<br>- (สำหรับพนักงานระดับ 2-8) กรณีเบิกค่าล่วงเวลาเกิน 120 ชั่วโมง/เดือน (ลำดับการอนุมัติตามสายบังคับบัญชาของสายงานสูงสุดไปจนถึง CEO) <br>- (สำหรับพนักงานระดับ 9-10) กรณีเบิกค่าล่วงเวลาเกิน 48 ชั่วโมง/เดือน (ลำดับการอนุมัติตามสายบังคับบัญชาของสายงานสูงสุดไปจนถึง CEO) <br>- กรณีเบิกค่าล่วงเวลาในวันหยุด (ลำดับการอนุมัติตามสายบังคับบัญชาปกติ คือระดับ ผจ.ฝ่าย อาวุโสขึ้นไปโดยไม่ถึง CEO (การคิดจำนวนชั่วโมงในแต่ละกรณีจะคิดรายการค่าล่วงเวลาที่รออนุมัติ และเสร็จสมบูรณ์แล้วเท่านั้น) และพนักงานสามารถกรอกข้อมูลเบิกค่าล่วงเวลาย้อนหลังได้ 7 วันทำการ </div>";


            $scope.slides = [
                {
                    title: "images 1",
                    image: 'images/16_9.png'
                },
                {
                    title: "images 2",
                    image: 'images/16_9.png'
                },
                {
                    title: "images 3",
                    image: 'images/16_9.png'
                },
                {
                    title: "images 4",
                    image: 'images/16_9.png'
                },
                {
                    title: "images 5",
                    image: 'images/16_9.png'
                }
            ];

            $timeout(function () {

                $(".ayodia-slider").not('.slick-initialized').slick({
                    dots: true,
                    touchMove: false,
                    autoplay: true,
                    autoplaySpeed: 3000,
                    prevArrow: `<div class='carousel-prev'><svg xmlns="http://www.w3.org/2000/svg" width="43.725" height="43.725" viewBox="0 0 43.725 43.725">
                        <path style="fill:#eac39d;" d="M21.862,0A21.862,21.862,0,1,0,43.725,21.862,21.862,21.862,0,0,0,21.862,0Zm0,40.992a19.13,19.13,0,1,1,19.13-19.13A19.129,19.129,0,0,1,21.862,40.992Z"/>
                        <path style="fill:#eac39d;" d="M171.316,116.8l-10.931,10.931a1.366,1.366,0,0,0,0,1.927l10.931,10.931,1.927-1.94-9.961-9.961,9.961-9.961Z" transform="translate(-146.324 -106.825)"/>
                        </svg></div>`,
                    nextArrow: `<div class='carousel-next'><svg xmlns="http://www.w3.org/2000/svg" width="43.725" height="43.725" viewBox="0 0 43.725 43.725">
                        <path style="fill:#eac39d;" d="M21.862,0A21.862,21.862,0,1,1,0,21.862,21.862,21.862,0,0,1,21.862,0Zm0,40.992a19.13,19.13,0,1,0-19.13-19.13A19.129,19.129,0,0,0,21.862,40.992Z"/>
                        <path style="fill:#eac39d;" d="M161.914,116.8l10.931,10.931a1.366,1.366,0,0,1,0,1.927l-10.931,10.931-1.927-1.94,9.961-9.961-9.961-9.961Z" transform="translate(-143.18 -106.825)"/>
                         </svg></div>`,
                    dotsClass: "carousel-dots"
                });

                $('.image-popup-no-margins').magnificPopup({
                    type: 'image',
                    closeOnContentClick: true,
                    closeBtnInside: false,
                    fixedContentPos: true,
                    mainClass: 'mfp-no-margins mfp-with-zoom',
                    image: {
                        verticalFit: true
                    },
                    zoom: {
                        enabled: true,
                        duration: 300
                    }
                });
                $(".hide-menu-btn").click(function () {
                    $(".ayodia-slider").slick('slickGoTo', 0);
                });
            }, 0);

            $scope.DEMO = {
                name_1: "ลาป่วยมีใบแพทย์ 1",
                value_Result_1: 5,
                value_All_1: 10,
                value_percent_1: '50%',

                name_2: "ลาป่วยมีใบแพทย์ 2",
                value_Result_2: 10,
                value_All_2: 10,
                value_percent_2: '100%',

                name_3: "ลาป่วยมีใบแพทย์ 3",
                value_Result_3: 0,
                value_All_3: 10,
                value_percent_3: '0%',

                name_4: "ลาป่วยมีใบแพทย์ 4",
                value_Result_4: 1,
                value_All_4: 10,
                value_percent_4: '10%',

                name_5: "เจ็บป่วยเนื่องจากการทำงานเกิน 180วัน จ่าย 60%",
                value_Result_5: 3,
                value_All_5: 10,
                value_percent_5: '30%',

            }
            $scope.DEMO2 = [
                {
                    name: "ด.ช. มังคุด จันทคุด",
                    value_Result: '4,200.00',
                    value_All: '10,000.00',
                    value_percent: '4.2%'
                },
                {
                    name: "ด.ช. มังคุด จันทคุด",
                    value_Result: '10,000.00',
                    value_All: '10,000.00',
                    value_percent: '100%'
                }, {
                    name: " ด.ญ ปะสิน จันทคุด",
                    value_Result: '0',
                    value_All: '40,000.00',
                    value_percent: '0%'
                }, {
                    name: " ด.ญ ปะวัติ จันทคุด",
                    value_Result: '30,000.00',
                    value_All: '40,000.00',
                    value_percent: '70%'
                }
            ];
        }]);
})();