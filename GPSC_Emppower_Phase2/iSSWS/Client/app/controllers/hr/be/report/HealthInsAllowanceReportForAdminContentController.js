﻿(function () {
    angular.module('ESSMobile')
        .controller('HealthInsAllowanceReportForAdminContentController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal) {

            $scope.back = function () {
                var url = '/frmViewContent/5000';
                $location.path(url);
            };

            // Get Config Multi Company
            $scope.isHideMultiCompany = true;
            $scope.GetConfingMultiCompany = function () {
                var URL = CONFIG.SERVER + 'Employee/GetConfigMultiCompany';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data === 1) {
                        $scope.isHideMultiCompany = false;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetConfingMultiCompany();

            $scope.employeeData = getToken(CONFIG.USER);
            $scope.SelectCompanyCode = $scope.employeeData.CompanyCode;

            $scope.GetAuthorizationCompany = function () {
                var URL = CONFIG.SERVER + 'workflow/GetAuthorizationCompany';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.CompanyList = response.data;
                    $scope.GetYearConfig();
                    if ($scope.CompanyList.length > 0) {
                        $scope.ddlCompany = $scope.CompanyList[0].CompanyCode;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }
            $scope.GetAuthorizationCompany();


            $scope.changeCompany = function (CompanyCode) {
                $scope.SelectCompanyCode = CompanyCode;
                if ($scope.SelectCompanyCode) {
                    GetOrganization();
                }
                //GetReportType();
                $scope.GetYearConfig();
            };




            $scope.GetYearConfig = function () {
                var URL = CONFIG.SERVER + 'HRBE/GetListYearHealthInsAllowance';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {
                        SelectCompany: $scope.selectCompanyCode
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.list_year = response.data;
                    if ($scope.list_year.length > 0) {
                        $scope.model.Year = response.data[0];
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };






            $scope.splitOffCycle = function (OffCycle, keyMonth) {

                var text_month_detail = "";
                var fields = OffCycle.split('|');
                var DateOffCycle = fields[0];
                var IsOffCycle = fields[1];
                if (IsOffCycle === '1') {

                    if (DateOffCycle) {
                        var str_split = DateOffCycle.toString().substring(2, 4);
                        var int_splint = parseInt(str_split);
                        text_month_detail = $scope.TextMonth["D_M" + int_splint.toString()];

                        var day_data = DateOffCycle.toString().substring(0, 2);
                        var month_data = DateOffCycle.toString().substring(2, 4);
                        var year_data = DateOffCycle.toString().substring(4, 8);
                        var concat_data = day_data + '/' + month_data + '/' + year_data;


                        return text_month_detail + " (" + concat_data + ") ";
                    }
                }
                else {
                    text_month_detail = $scope.TextMonth["D_M" + keyMonth];
                    return text_month_detail;
                }
            };

            GetListEmp();

            function download(dataurl, filename) {
                var a = document.createElement("a");
                a.href = dataurl;
                a.setAttribute("download", filename);
                var b = document.createEvent("MouseEvents");
                b.initEvent("click", false, true);
                a.dispatchEvent(b);
                return false;
            }
            $scope.rdoSelected = "1";
            $scope.file_download = "";

            $scope.TextMonth = $scope.Text["SYSTEM"];
            $scope.ListYear;

            $scope.IsShowPeriod = true;
            $scope.ddlReportType;
            $scope.ListReportType;
            $scope.ddlYear;
            $scope.ddlPeriod;
            $scope.tmpPeriod;
            $scope.ListPeriod;
            $scope.ListEmp = [];
            $scope.Organization;
            $scope.TreeOrganization = [];
            $scope.SelectedEmployeeID = '';
            $scope.SelectedEmployeeName = '';
            $scope.SelectedPositionID = '';
            $scope.SelectedPositionName = '';
            $scope.SelectedOrgUnitID = '';
            $scope.SelectedOrgUnitName = '';
            $scope.ObjEmp;

            $scope.SelectedListOrg;

            $scope.isValidate = false;
            $scope.IsSelectedEmployee = false;


            /* =========================== edit */
            $scope.YearList = [];


            /* =========================== end edit */





            $scope.init = function () {
                GetReportType();

                /* =========================== edit */
                GetYears();

                /* =========================== end edit */
            };


            /* =========================== edit */
            function GetYears() {

                var URL = CONFIG.SERVER + 'HRBE/GetYearsAdminSetting';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "SelectCompany": $scope.SelectCompanyCode }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    //console.log("GetYears", response.data);
                    angular.forEach(response.data, function (value, key) {
                        $scope.YearList.push(value.YearName);
                    });

                    if ($scope.YearList.length >= 1) {
                        $scope.ddlYear = $scope.YearList[0].toString();
                        $scope.GetListEffectiveDateSetting();
                    }

                }, function errorCallback(response) {
                });
            }

            /* =========================== end edit */


            /* =========================== edit */
            //ONCLICK
            $scope.get_report = function () {

                if (new Date($scope.date_from_select) > new Date($scope.date_to_select)) {

                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popupContainer')))
                            .clickOutsideToClose(true)
                            .title('วันที่เริ่มต้นต้องน้อยกว่าวันที่สิ้นสุด!')
                            .ok('ตกลง')
                    );

                    return false;
                }

                var str_category = "";
                var str_status = "";

                angular.forEach($scope.selection_category, function (value) {
                    str_category += value + ",";
                });

                angular.forEach($scope.selection_status, function (value) {
                    str_status += value + ",";
                });


                GetHealthInsAllowanceReport($scope.date_from_select, $scope.date_to_select, str_category, str_status);
            };

            //GET DATA REPORT
            function GetHealthInsAllowanceReport(date_from, date_to, benefit_type, benefit_status) {
                var URL = CONFIG.SERVER + 'HRBE/GetHealthInsAllowanceReport';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "UpdateDate_From": date_from, "UpdateDate_To": date_to, "Benefit_Type": benefit_type, "Benefit_Status": benefit_status, "language": $scope.employeeData.Language, "SelectCompany": $scope.SelectCompanyCode }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.data = response.data;

                    if ($scope.data.length == 0) {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.querySelector('#popupContainer')))
                                .clickOutsideToClose(true)
                                .title('ไม่พบข้อมูล!')
                                .ok('ตกลง')
                        );
                    };

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }
            //Pagination of nested objects in angularjs
            $scope.pagin = {
                currentPage: 1, itemPerPage: '10', numPage: 1
            }
            $scope.filterDatas = function (searchTxt) {
                if (!$scope.data || !$scope.data.length) return [];
                var datas = $filter('filter')($scope.data, searchTxt);
                datas = $filter('filter')(datas, $scope.advacneOptionFilter);

                return datas;
            }
            $scope.genDatas = function (searchTxt) {
                var datas = $scope.filterDatas(searchTxt);
                setNumOfPage(datas);


                var res = [];

                if ($scope.pagin.numPage > 1) {
                    datas.forEach(function (value, i) {
                        var from = (parseInt($scope.pagin.currentPage) - 1) * parseInt($scope.pagin.itemPerPage);
                        var to = from + parseInt($scope.pagin.itemPerPage);

                        if (from <= i && to > i)
                            res.push(value);
                    });
                } else {
                    res = datas.slice();
                }

                return res;
            }
            var setNumOfPage = function (datas) {
                var totalItems = 0;
                if (datas) totalItems = datas.length;
                $scope.pagin.numPage = Math.ceil(totalItems / $scope.pagin.itemPerPage);
                if ($scope.pagin.numPage <= 0) $scope.pagin.numPage = 1;

                if ($scope.pagin.currentPage > $scope.pagin.numPage) $scope.pagin.currentPage = 1;
            }
            $scope.changePage = function (targetPage) {
                $scope.pagin.currentPage = angular.copy(targetPage);
            }
            $scope.getPages = function () {
                var pages = [];

                for (var i = 0; i < $scope.pagin.numPage; i++) {
                    pages.push((i + 1));
                }

                return pages;
            }
            /* =========================== end edit */






































            function GetListEmp() {
                var URL = CONFIG.SERVER + 'HRPY/GetAllEmployeeInINFOTYPE0001';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "SelectCompany": $scope.SelectCompanyCode }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.ListEmp = response.data;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }

            GetYear();
            function GetYear() {
                var URL = CONFIG.SERVER + 'HRPY/GetYearPayrollReportAdmin';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "ByOrganization": $scope.rdoSelected, "SelectCompany": $scope.SelectCompanyCode }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.ListYear = response.data;
                    if ($scope.ListYear != null && $scope.ListYear.length > 0) {
                        $scope.ddlYear = $scope.ListYear[0].YearID;
                        GetPeriodPayrollReportAdmin($scope.ListYear[0].YearID);
                    }
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }


            GetOrganization();
            function GetOrganization() {
                var URL = CONFIG.SERVER + 'HRPY/GetOrganization';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "SubjectID": $scope.contentInfo.SubjectID, "SelectCompany": $scope.SelectCompanyCode }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.TreeOrganization = [];

                    $scope.Organization = response.data;
                    console.log("$scope.Organization", $scope.Organization)
                    $scope.Organization.forEach(ele => {
                        ele.IsExpanding = true;
                        ele.OrgChild = [];
                        ele.Selected = false;
                        ele.Disable = false;
                        //$scope.TreeOrganization[j].Objective = [];
                    });

                    for (var i = 0; i < $scope.Organization.length; i++) {
                        if ($scope.Organization[i].OrgLevel == 0) {
                            $scope.TreeOrganization.push($scope.Organization[i]);
                        }
                        else if ($scope.Organization[i].OrgLevel == 1) {
                            for (var j = 0; j < $scope.TreeOrganization.length; j++) {
                                if ($scope.TreeOrganization[0].OrgUnit == $scope.Organization[i].OrgParent) {
                                    $scope.TreeOrganization[0].OrgChild.push($scope.Organization[i]);
                                    break;
                                }
                            }
                        }
                        else if ($scope.Organization[i].OrgLevel == 2) {
                            for (var k = 0; k < $scope.TreeOrganization[0].OrgChild.length; k++) {
                                if ($scope.TreeOrganization[0].OrgChild[k].OrgUnit == $scope.Organization[i].OrgParent) {
                                    $scope.TreeOrganization[0].OrgChild[k].OrgChild.push($scope.Organization[i]);
                                    break;
                                }
                            }
                        }
                        else if ($scope.Organization[i].OrgLevel == 3) {
                            for (var m = 0; m < $scope.TreeOrganization[0].OrgChild.length; m++) {
                                for (var n = 0; n < $scope.TreeOrganization[0].OrgChild[m].OrgChild.length; n++) {
                                    if ($scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgUnit == $scope.Organization[i].OrgParent) {
                                        $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild.push($scope.Organization[i]);
                                        break;
                                    }
                                }
                            }
                        }
                        else if ($scope.Organization[i].OrgLevel == 4) {
                            for (var m = 0; m < $scope.TreeOrganization[0].OrgChild.length; m++) {
                                for (var n = 0; n < $scope.TreeOrganization[0].OrgChild[m].OrgChild.length; n++) {
                                    for (var x = 0; x < $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild.length; x++) {
                                        if ($scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgUnit == $scope.Organization[i].OrgParent) {
                                            $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild.push($scope.Organization[i]);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        else if ($scope.Organization[i].OrgLevel == 5) {
                            for (var m = 0; m < $scope.TreeOrganization[0].OrgChild.length; m++) {
                                for (var n = 0; n < $scope.TreeOrganization[0].OrgChild[m].OrgChild.length; n++) {
                                    for (var x = 0; x < $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild.length; x++) {
                                        for (var y = 0; y < $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild.length; y++) {
                                            if ($scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild[y].OrgUnit == $scope.Organization[i].OrgParent) {
                                                $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild[y].OrgChild.push($scope.Organization[i]);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }


            //function GetYearPayrollReportAdminByEmpNoSelected() {
            //    var URL = CONFIG.SERVER + 'HRPY/GetYearPayrollReportAdminByEmpNoSelected';
            //    $scope.employeeData = getToken(CONFIG.USER);
            //    var oRequestParameter = {
            //        InputParameter: { }
            //        , CurrentEmployee: $scope.employeeData
            //        , Requestor: $scope.requesterData
            //        , Creator: getToken(CONFIG.USER)
            //    };
            //    $http({
            //        method: 'POST',
            //        url: URL,
            //        data: oRequestParameter
            //    }).then(function successCallback(response) {
            //        $scope.ListYear = response.data;
            //        if ($scope.ListYear != null && $scope.ListYear.length > 0) {
            //            $scope.ddlYear = $scope.ListYear[0].YearID;
            //            GetPeriodPayrollReportAdmin($scope.ListYear[0].YearID);
            //        }
            //        $scope.loader.enable = false;
            //    }, function errorCallback(response) {
            //        $scope.loader.enable = false;
            //    });
            //}


            $scope.changeRadioSelected = function () {

                //if ($scope.rdoSelected == "2") {
                //    $scope.IsSelectedEmployee = true;
                //}

                $scope.SelectedEmployeeID = '';
                $scope.SelectedEmployeeName = '';
                $scope.SelectedPositionID = '';
                $scope.SelectedPositionName = '';
                $scope.SelectedOrgUnitID = '';
                $scope.SelectedOrgUnitName = '';
                $scope.ObjEmp = null;

                $scope.clearTreeList($scope.Organization[0]);

                GetYear();
            };

            $scope.changeReportType = function () {
                if ($scope.ddlReportType == 'PAYSLIP') {
                    $scope.IsShowPeriod = true;

                    GetYear();
                }
                else if ($scope.ddlReportType == 'TAX') {
                    $scope.IsShowPeriod = false;

                    GetYearsTavi50();
                }
            };


            function GetYearsTavi50() {
                var URL = CONFIG.SERVER + 'HRPY/GetYearTavi50';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.ListYear = [];
                    angular.forEach(response.data, function (value, key) {
                        var obj = { YearID: value, YearName: value };
                        $scope.ListYear.push(obj);
                    });
                    if ($scope.ListYear != null && $scope.ListYear.length > 0) {
                        $scope.ddlYear = $scope.ListYear[0].YearID;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }


            $scope.test_data = function (item) {
                console.log(item);
            };

            $scope.getSelectedEmployee = function (item) {

                $scope.MsgValidate = '';

                $scope.ObjEmp = null;
                $scope.SelectedEmployeeID = item.EmployeeID;
                $scope.SelectedEmployeeName = item.Name;
                $scope.SelectedPositionID = item.Position;

                GetEmpData(item.EmployeeID, item.Position);
            };

            //$scope.getSelectedEmployee = function (empID, empName, positionID) {

            //    console.log(empID, empName, positionID);

            //    $scope.ObjEmp = null;
            //    $scope.IsSelectedEmployee = false;
            //    $scope.SelectedEmployeeID = empID;
            //    $scope.SelectedEmployeeName = empName;
            //    $scope.SelectedPositionID = positionID;

            //    GetEmpData(empID, positionID);
            //};

            function GetEmpData(empID, positionID) {
                var URL = CONFIG.SERVER + 'HRPY/GetEmpData';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "EmployeeID": empID, "PositionID": positionID, "SelectCompany": $scope.SelectCompanyCode }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.ObjEmp = response.data;

                    if ($scope.ObjEmp != null && $scope.ObjEmp.length > 0) {
                        $scope.SelectedPositionName = $scope.ObjEmp[0].PositionName;
                        $scope.SelectedOrgUnitID = $scope.ObjEmp[0].OrgUnitID;
                        $scope.SelectedOrgUnitName = $scope.ObjEmp[0].OrgUnitName;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }

            $scope.GetPeriodPayrollReportAdmin = function () {
                GetPeriodPayrollReportAdmin($scope.ddlYear);
            };

            $scope.ChangePeriod = function (selected) {
                $scope.tmpPeriod = selected;
            };

            //$scope.getIndex = function (index) {
            //    for (var i = 0; i < $scope.ListPeriod.length; i++) {
            //        if ($scope.ListPeriod[i].MONTH_KEY === index)
            //            return i;
            //    }
            //};



            $scope.onViewReport = function () {

                console.log('export');

                $scope.SelectedListOrg = '';

                for (var m = 0; m < $scope.TreeOrganization[0].OrgChild.length; m++) {
                    if ($scope.TreeOrganization[0].OrgChild[m].Selected) {
                        $scope.SelectedListOrg += $scope.TreeOrganization[0].OrgChild[m].OrgUnit + ',';
                    }
                    for (var n = 0; n < $scope.TreeOrganization[0].OrgChild[m].OrgChild.length; n++) {
                        if ($scope.TreeOrganization[0].OrgChild[m].OrgChild[n].Selected) {
                            $scope.SelectedListOrg += $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgUnit + ',';
                        }
                        for (var x = 0; x < $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild.length; x++) {
                            if ($scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].Selected) {
                                $scope.SelectedListOrg += $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgUnit + ',';
                            }
                            for (var y = 0; y < $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild.length; y++) {
                                if ($scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild[y].Selected) {
                                    $scope.SelectedListOrg += $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild[y].OrgUnit + ',';
                                }
                                for (var z = 0; z < $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild[y].OrgChild.length; z++) {
                                    if ($scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild[y].OrgChild[z].Selected) {
                                        $scope.SelectedListOrg += $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild[y].OrgChild[z].OrgUnit + ',';
                                    }
                                    for (var i = 0; i < $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild[y].OrgChild[z].OrgChild.length; i++) {
                                        if ($scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild[y].OrgChild[z].OrgChild[i].Selected) {
                                            $scope.SelectedListOrg += $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild[y].OrgChild[z].OrgChild[i].OrgUnit + ',';
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if ($scope.rdoSelected == "1") {
                    if ($scope.ddlReportType == 'PAYSLIP') {
                        //$scope.SelectedEmployeeID = '';


                        $scope.SelectedEmployeeID = $scope.employeeData.EmployeeID;
                        $scope.SelectedEmployeeName = $scope.employeeData.Name;
                        $scope.SelectedPositionName = $scope.employeeData.Position;
                        $scope.SelectedOrgUnitName = $scope.employeeData.OrgUnitName;
                        $scope.GetPaySlipExport('PDF', '1', $scope.SelectedListOrg);
                    }
                    else if ($scope.ddlReportType == 'TAX') {

                        //$scope.SelectedEmployeeID = '';

                        $scope.SelectedEmployeeID = $scope.employeeData.EmployeeID;
                        $scope.GetTaxReportExport('PDF', '1', $scope.SelectedListOrg);
                    }
                }
                else {
                    if ($scope.SelectedEmployeeID == '') {
                        $scope.MsgValidate = $scope.Text["PAYROLLREPORTFORADMIN"].SELECTEMPLOYEE;
                    }
                    else {
                        $scope.MsgValidate = '';
                        if ($scope.ddlReportType == 'PAYSLIP') {
                            $scope.GetPaySlipExport('PDF', '2', '');
                        }
                        else if ($scope.ddlReportType == 'TAX') {
                            $scope.GetTaxReportExport('PDF', '2', '');
                        }
                    }
                }
            };

            function GetPeriodPayrollReportAdmin(key_year) {
                var URL = CONFIG.SERVER + 'HRPY/GetPeriodPayrollReportAdmin';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "Year": key_year, "ByOrganization": $scope.rdoSelected, "SelectCompany": $scope.SelectCompanyCode }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    console.log(response.data);
                    $scope.ListPeriod = response.data;

                    if ($scope.ListPeriod != null && $scope.ListPeriod.length > 0) {

                        $scope.ddlPeriod = $scope.ListPeriod[0].MONTH_VALUE;
                        $scope.tmpPeriod = $scope.ListPeriod[0].MONTH_VALUE;
                    }

                    // หาก OffCycle
                    if ($scope.ListPeriod.length > 0) {

                        //var obj = {
                        //    MONTH_KEY: "1",
                        //    MONTH_VALUE: "31012020|1",
                        //    MONTH_NAME: ""
                        //};
                        //$scope.ListPeriod.push(obj);

                        angular.forEach($scope.ListPeriod, function (value, key) {
                            var OffCycle = $scope.splitOffCycle(value.MONTH_VALUE, value.MONTH_KEY);
                            value.MONTH_NAME = OffCycle;
                        });
                    }


                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }

            function GetReportType() {
                var URL = CONFIG.SERVER + 'HRPY/GetReportType';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "SelectCompany": $scope.SelectCompanyCode }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.ListReportType = response.data;

                    if ($scope.ListReportType != null && $scope.ListReportType.length > 0) {
                        $scope.ddlReportType = $scope.ListReportType[0].ReportTypeValue;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }


            $scope.GetPaySlipExport = function (type_download, ByOrg, ListOrg) {
                var str_category_export = "";
                var str_status_export = "";
                var type_file = "";
                $scope.loader.enable = true;
                angular.forEach($scope.selection_category, function (value) {
                    str_category_export += value + ",";
                });

                angular.forEach($scope.selection_status, function (value) {
                    str_status_export += value + ",";
                });

                if (type_download == 'PDF') {
                    type_file = 'pdf';
                } else {
                    type_file = 'xlsx';
                }

                var URL = CONFIG.SERVER + 'HRPY/GetPaySlipExportForAdminExport';

                $scope.employeeData = getToken(CONFIG.USER);
                console.log('set employee-->' + $scope.employeeData);
                var oRequestParameter = {
                    InputParameter: {
                        Type: str_category_export
                        , Status: str_status_export
                        , ReportName: $scope.Text["APPLICATION"].PAYSLIPREPORT
                        , LanguageCode: $scope.employeeData.Language
                        , Employee_id: $scope.SelectedEmployeeID
                        , EmployeeName: $scope.SelectedEmployeeName
                        , PositionName: $scope.SelectedPositionName
                        , OrgUnitName: $scope.SelectedOrgUnitName
                        , ExportType: type_download //"PDF" //EXCEL
                        , DateText: $scope.Text["BE_REPORT"].DATETEXT
                        , TimeText: $scope.Text["BE_REPORT"].TIMETEXT
                        , T_SUBJECT: $scope.Text["PAYSLIPREPORT"].SUBJECT
                        , T_COLCODE: $scope.Text["PAYSLIPREPORT"].COLCODE
                        , T_COLINCOME: $scope.Text["PAYSLIPREPORT"].COLINCOME
                        , T_COLAMOUNT: $scope.Text["PAYSLIPREPORT"].COLAMOUNT
                        , T_COLOUTCOME: $scope.Text["PAYSLIPREPORT"].COLOUTCOME
                        , T_COLREMARK: $scope.Text["PAYSLIPREPORT"].COLREMARK
                        , T_COLTOTALINCOME: $scope.Text["PAYSLIPREPORT"].COLTOTALINCOME
                        , T_COLTOTALOUTCOME: $scope.Text["PAYSLIPREPORT"].COLTOTALOUTCOME
                        , T_COLNET: $scope.Text["PAYSLIPREPORT"].COLNET
                        , T_COLINCOMECOLLECTION: $scope.Text["PAYSLIPREPORT"].COLINCOMECOLLECTION
                        , T_COLTAXCOLLECTION: $scope.Text["PAYSLIPREPORT"].COLTAXCOLLECTION
                        , T_COLTOTALCOLLECTION: $scope.Text["PAYSLIPREPORT"].COLTOTALCOLLECTION
                        , T_COLPROVIDENTFUND: $scope.Text["PAYSLIPREPORT"].COLPROVIDENTFUND
                        , T_COLSOCIALSECURITY: $scope.Text["PAYSLIPREPORT"].COLSOCIALSECURITY
                        , T_COLPAID: $scope.Text["PAYSLIPREPORT"].COLPAID
                        , T_BANKINTRO: $scope.Text["PAYSLIPREPORT"].BANKINTRO
                        , T_CONTACT: $scope.Text["PAYSLIPREPORT"].CONTACT
                        , PeriodSetting: $scope.tmpPeriod
                        , EmpPayslip: $scope.SelectedEmployeeID
                        , ByOrg: ByOrg
                        , ListOrg: ListOrg
                        , "SelectCompany": $scope.SelectCompanyCode
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data > 0) {

                        var file_name = "PaySlipReportForAdmin" + $scope.SelectedEmployeeID;
                        if (type_download == "PDF") {
                            file_name += ".pdf";
                        } else if (type_download == "EXCEL") {
                            file_name += ".xls";
                        }
                        var url = CONFIG.SERVER + 'Client/Report/' + file_name;
                        download(url, file_name);
                    }
                    else {
                        var modalInstance = $uibModal.open({
                            animation: $scope.animationsEnabled,
                            templateUrl: 'errorExportPdf.ng-popup.html',
                            controller: 'errorExportPdfController',
                            backdrop: 'static',
                            keyboard: false,
                            size: 'sp',
                            resolve: {
                                items: function () {
                                    var data = {
                                        requesterData: ""
                                    };
                                    return data;
                                }
                            }
                        });
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };

            $scope.GetTaxReportExport = function (type_download, ByOrg, ListOrg) {
                var str_category_export = "";
                var str_status_export = "";
                var type_file = "";
                $scope.loader.enable = true;
                angular.forEach($scope.selection_category, function (value) {
                    str_category_export += value + ",";
                });

                angular.forEach($scope.selection_status, function (value) {
                    str_status_export += value + ",";
                });

                if (type_download == 'PDF') {
                    type_file = 'pdf';
                } else {
                    type_file = 'xlsx';
                }
                var URL = CONFIG.SERVER + 'HRPY/Get50TaviReportForAdminExport';

                $scope.employeeData = getToken(CONFIG.USER);
                console.log('set employee-->' + $scope.employeeData);
                var oRequestParameter = {
                    InputParameter: {
                        Type: str_category_export
                        , Status: str_status_export
                        , ReportName: $scope.Text["APPLICATION"].TAXREPORT
                        , LanguageCode: $scope.employeeData.Language
                        , Employee_id: $scope.SelectedEmployeeID
                        , EmployeeName: $scope.SelectedEmployeeName
                        , PositionName: $scope.SelectedPositionName
                        , OrgUnitName: $scope.SelectedOrgUnitName
                        , ExportType: type_download //"PDF" //EXCEL
                        , DateText: $scope.Text["BE_REPORT"].DATETEXT
                        , TimeText: $scope.Text["BE_REPORT"].TIMETEXT
                        , Year: $scope.ddlYear
                        , PinCode: ''
                        , Emp50Tavi: $scope.SelectedEmployeeID
                        , ByOrg: ByOrg
                        , ListOrg: ListOrg
                        , "SelectCompany": $scope.SelectCompanyCode
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data > 0) {

                        var file_name = "ใบรับรองภาษีปี" + $scope.ddlYear + "_ForAdmin" + $scope.SelectedEmployeeID;

                        if (type_download == "PDF") {
                            file_name += ".pdf";
                        } else if (type_download == "EXCEL") {
                            file_name += ".xls";
                        }
                        var url = CONFIG.SERVER + 'Client/Report/' + file_name;
                        download(url, file_name);
                    }
                    else {

                        var modalInstance = $uibModal.open({
                            animation: $scope.animationsEnabled,
                            templateUrl: 'errorExportPdf.ng-popup.html',
                            controller: 'errorExportPdfController',
                            backdrop: 'static',
                            keyboard: false,
                            size: 'sp',
                            resolve: {
                                items: function () {
                                    var data = {
                                        requesterData: ""
                                    };
                                    return data;
                                }
                            }
                        });

                    }

                    $scope.loader.enable = false;

                }, function errorCallback(response) {
                    console.log(response);

                    $scope.loader.enable = false;
                });
            };

            $scope.clearTreeList = function (obj) {

                obj.Selected = false;
                for (var i = 0; i < obj.OrgChild.length; i++) {
                    obj.OrgChild[i].Selected = false;
                    for (var j = 0; j < obj.OrgChild[i].OrgChild.length; j++) {
                        obj.OrgChild[i].OrgChild[j].Selected = false;
                        for (var k = 0; k < obj.OrgChild[i].OrgChild[j].OrgChild.length; k++) {
                            obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = false;
                            for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = false;
                                for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = false;
                                }
                            }
                        }
                    }
                }

            };


            $scope.selectOrgLV1 = function (items, obj) {
                if (!items) return;

                if (obj.Selected == true) {
                    obj.Selected = false;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = false;
                        for (var j = 0; j < obj.OrgChild[i].OrgChild.length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = false;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].OrgChild.length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = false;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = false;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = false;
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    obj.Selected = true;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = true;
                        for (var j = 0; j < obj.OrgChild[i].OrgChild.length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = true;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].OrgChild.length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = true;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = true;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = true;
                                    }
                                }
                            }
                        }
                    }
                }
            };

            $scope.selectOrgLV2 = function (items, obj) {
                if (!items) return;

                if (obj.Selected == true) {
                    obj.Selected = false;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = false;
                        for (var j = 0; j < obj.OrgChild[i].OrgChild.length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = false;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].OrgChild.length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = false;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = false;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = false;
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    obj.Selected = true;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = true;
                        for (var j = 0; j < obj.OrgChild[i].OrgChild.length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = true;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].OrgChild.length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = true;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = true;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = true;
                                    }
                                }
                            }
                        }
                    }
                }
            };

            $scope.selectOrgLV3 = function (items, obj) {
                if (!items) return;

                if (obj.Selected == true) {
                    obj.Selected = false;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = false;
                        for (var j = 0; j < obj.OrgChild[i].OrgChild.length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = false;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].OrgChild.length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = false;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = false;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = false;
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    obj.Selected = true;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = true;
                        for (var j = 0; j < obj.OrgChild[i].OrgChild.length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = true;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].OrgChild.length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = true;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = true;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = true;
                                    }
                                }
                            }
                        }
                    }
                }
            };

            $scope.selectOrgLV4 = function (items, obj) {
                if (!items) return;

                if (obj.Selected == true) {
                    obj.Selected = false;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = false;
                        for (var j = 0; j < obj.OrgChild[i].OrgChild.length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = false;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].OrgChild.length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = false;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = false;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = false;
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    obj.Selected = true;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = true;
                        for (var j = 0; j < obj.OrgChild[i].OrgChild.length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = true;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].OrgChild.length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = true;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = true;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = true;
                                    }
                                }
                            }
                        }
                    }
                }
            };

            $scope.selectOrgLV5 = function (items, obj) {
                if (!items) return;

                if (obj.Selected == true) {
                    obj.Selected = false;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = false;
                        for (var j = 0; j < obj.OrgChild[i].OrgChild.length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = false;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].OrgChild.length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = false;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = false;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = false;
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    obj.Selected = true;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = true;
                        for (var j = 0; j < obj.OrgChild[i].length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = true;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = true;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = true;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = true;
                                    }
                                }
                            }
                        }
                    }
                }

            };


            $scope.selectOrgLV6 = function (items, obj) {
                if (!items) return;

                if (obj.Selected == true) {
                    obj.Selected = false;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = false;
                        for (var j = 0; j < obj.OrgChild[i].OrgChild.length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = false;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].OrgChild.length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = false;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = false;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = false;
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    obj.Selected = true;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = true;
                        for (var j = 0; j < obj.OrgChild[i].length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = true;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = true;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = true;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = true;
                                    }
                                }
                            }
                        }
                    }
                }

            };

            $scope.exists = function (item) {
                if (!item) return;
                return $scope.data.selectedList.indexOf(item) > -1;
            };

            $scope.exists_group = function (items) {
                if (!items) return;
                var count = 0;
                for (var i = 0; i < items.length; i++) {
                    if (isSelected(items[i])) {
                        count++;
                    }
                }
                return count == items.length;
            };

            $scope.exists_groups = function (items) {
                if (!items) return;
                var count = 0;
                var length = 0;
                for (var i = 0; i < items.length; i++) {
                    for (var a = 0; a < items[i].Indicator.length; a++) {
                        if (isSelected(items[i].Indicator[a])) {
                            count++;
                        }
                        length++;
                    }
                }
                return count == length;
            };

            function isSelected(item) {
                if (!item) return;

                for (var i = 0; i < $scope.data.selectedList.length; i++) {
                    if (item.ObjectiveID === $scope.data.selectedList[i].ObjectiveID && item.IndicatorID == $scope.data.selectedList[i].IndicatorID) {
                        return true;
                    }
                }
                return false;
            }


            // #AutoComplete EmployeeList
            function createFilterForListEmp(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    if (!x) return false;
                    var source = angular.lowercase(x.EmployeeID1 + ":" + x.EmployeeName1);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }

            $scope.querySearchListEmp = function (query) {
                if (!$scope.ListEmp) return;
                var results = angular.copy(query ? $scope.ListEmp.filter(createFilterForListEmp(query)) : $scope.ListEmp), deferred;
                results = results.splice(0, 500);
                return results;
            };

            $scope.selectedItemListEmpChange = function (item) {
                if (angular.isDefined(item) && item != null) {
                    $scope.ap_pdr_text.cc = item.EmployeeID1 + " : " + item.EmployeeName1;
                    //$scope.TempEmp.CCCode = item.EmployeeID1;
                    tempResult_cc = angular.copy(item);
                }
                else {
                    $scope.ap_pdr_text.cc = '';
                }
            };

            $scope.tryToSelect_cc = function (text) {
                $scope.ap_pdr_text.cc = '';
                //$scope.TempEmp.CCCode = '';
            };

            $scope.checkText_cc = function (text) {
                if (text == '') {
                    $scope.ap_pdr_text.cc = '';
                    //$scope.TempEmp.CCCode = '';
                }
                else
                    $scope.ap_pdr_text.cc = text;
            };




            // Open Modal Search Customer
            $scope.openModalSearchCustomer = function () {

                var modalInstance = $uibModal.open({
                    animation: $scope.animationsEnabled,
                    templateUrl: 'modalSearchCustomer.ng-popup.html',
                    controller: 'modalSearchCustomerController',
                    backdrop: 'static',
                    keyboard: false,
                    size: 'lg',
                    resolve: {
                        items: function () {
                            var data = {
                                Text: $scope.Text,
                                requesterData: $scope.requesterData,
                                SelectCompany: $scope.SelectCompanyCode,
                                fnSeelctEmployee: function (item) {
                                    $scope.getSelectedEmployee(item);
                                }
                            };
                            return data;
                        }
                    }
                });
            };

            $scope.clearDataCustomer = function () {

                $scope.SelectedEmployeeID = '';
                $scope.SelectedEmployeeName = '';
                $scope.SelectedPositionID = '';
                $scope.SelectedPositionName = '';
                $scope.SelectedOrgUnitID = '';
                $scope.SelectedOrgUnitName = '';
                $scope.ObjEmp = null;

            };


            $scope.exportReport = function () {
                //console.log(1111111);
                var URL = CONFIG.SERVER + 'HRBE/ExportReportHealthInsAllowanceByAdmin';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {
                        SelectCompany: $scope.selectCompanyCode,
                        //Year: $scope.model.Year
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                var config = {
                    responseType: "arraybuffer",
                    cache: false
                };

                var apiData = { url: "", fileName: "" };
                var now = new Date();
                var now_time = now.getUTCFullYear() + "" + now.getUTCMonth() + "" + now.getUTCDate() + "" + now.getHours() + "" + now.getUTCMinutes() + "" + now.getUTCSeconds();
                apiData.fileName = "ReportHealthInsAllowance" + now_time + ".xlsx";

                $scope.loader.enable = true;
                $http.post(URL, oRequestParameter, config)
                    .then(function successCallback(response) {

                        var blob = new Blob([response.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
                        saveAs(blob, apiData.fileName);
                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                    });

            };







        }])
        .controller('errorExportPdfController', ['items', '$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', '$uibModalInstance', function (items, $scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal, $uibModalInstance) {

            $scope.closeModal = function () {
                $uibModalInstance.close();
            };

        }])
        .controller('modalSearchCustomerController', ['items', '$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', '$uibModalInstance', function (items, $scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal, $uibModalInstance) {

            console.log(items);
            $scope.text_db = items.Text;  // Text;
            $scope.requestorData = items.requesterData; // Requestor
            $scope.SelectCompanyCode = items.SelectCompany;
            $scope.list_emp = [];

            $scope.searchEmployee = function (textSearch) {

                var URL = CONFIG.SERVER + 'HRPY/GetEmployeeAllSystem';

                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {
                        "SEARCHTEXT": textSearch, "SelectCompany": $scope.SelectCompanyCode
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requestorData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    console.log(response);

                    $scope.list_emp = response.data.Table;

                }, function errorCallback(response) {
                    console.log(response);
                });

            };

            $scope.selectEmployee = function (item) {

                items.fnSeelctEmployee(item);
                $uibModalInstance.close();
            };

            $scope.closeModal = function () {
                $uibModalInstance.close();
            };

        }]);
})();
