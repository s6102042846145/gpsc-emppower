﻿(function () {
    angular.module('ESSMobile')
        .controller('FitnessAllowanceContentController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {
            //Declare
            $scope.data = {
                nPageSize: 10,
                arrFilterd: [],
                arrSplited: [],
                selectedColumn: 0,
                tFilter: '',
                pages: []
            };
            $scope.FitnessAllowanceQuotaList = [];
            $scope.YearsList = [];
            $scope.filtered = '';
            $scope.check_quota_balance = 0;
            $scope.check_Enroll = 0;
            $scope.loader.enable = true;
            $scope.keyYear_select = '';
            $scope.dateVariable = new Date();
            $scope.years = $scope.dateVariable.getFullYear();
            $scope.prop = {
                "type": "select",
                "name": $scope.years,
                "value": $scope.years,
                "values": $scope.YearsList
            }

            //Start
            $scope.init = function () {
                GetFitnessAllowance($scope.years);
                GetFitnessAllowanceQuotaList($scope.years);
                GetFitnessAllowanceCheckEnroll($scope.years);
                GetYears();
                $scope.keyYear_select = $scope.dateVariable.getFullYear();
            }

            //Get Data => WHERE Year
            $scope.filterYear = function (key_year) {
                $scope.keyYear_select = key_year;
                GetFitnessAllowance(key_year);
                GetFitnessAllowanceQuotaList(key_year);
                GetFitnessAllowanceCheckEnroll(key_year);
            };

            //GetFitnessAllowance
            function GetFitnessAllowance(key_year) {
                var URL = CONFIG.SERVER + 'HRBE/GetFitnessAllowance';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "RequestNo": "ALL", "KeyYear": key_year }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.data = response.data;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    // Error
                    console.log('Error GetFitnessAllowance', response);
                    $scope.loader.enable = false;
                });
            }

            //GetFitnessAllowanceQuotaList
            function GetFitnessAllowanceQuotaList(key_year) {
                var URL = CONFIG.SERVER + 'HRBE/GetFitnessAllowanceQuota';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "Years": key_year }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    //console.log('GetFitnessAllowanceQuotaList', $scope.QuotaList);
                    //Check Balance = 0
                    $scope.check_quota_balance = 0;
                    angular.forEach(response.data, function (value, key) {
                        if (value.Balance > 0) {
                            $scope.check_quota_balance += 1;
                        }
                    });

                    $scope.FitnessAllowanceQuotaList = response.data;
                    $scope.loader.enable = false;

                }, function errorCallback(response) {
                    // Error
                    console.log('Error GetFitnessAllowanceQuotaList', response);
                    $scope.loader.enable = false;
                });
            }

            //GetYears
            function GetYears() {
                var URL = CONFIG.SERVER + 'HRBE/GetYears';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    angular.forEach(response.data, function (value, key) {
                        $scope.YearsList.push(value.YearName);
                    });

                    $scope.loader.enable = false;

                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                });
            }

            //Check Enroll
            function GetFitnessAllowanceCheckEnroll(key_year) {
                var URL = CONFIG.SERVER + 'HRBE/FitnessAllowanceCheckEnroll';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "KeyYear": key_year }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.check_Enroll = 0;
                    $scope.check_Enroll = response.data;
                    //alert(response.data);
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    // Error
                    $scope.check_Enroll = 0;
                    $scope.loader.enable = false;
                });
            }

            //Go To ViewRequest
            $scope.openRequest = function (id, companyCode, keyMaster, isOwner, isDataOwner, requestType) {
                //$location.path('/frmViewRequest/' + id + '/' + companyCode + '/' + keyMaster + '/' + isOwner + '/' + isDataOwner);
                $location.path('/frmViewRequest/' + id + '/' + companyCode + '/null/false/true');
            };


            /// pop up
            $scope.BE_TEXT = $scope.Text["BE_TEXT"];
            $scope.showAlertFitnessAllowance = function () {
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title($scope.BE_TEXT.YEAR_DRAWMONEY + ' ' + $scope.keyYear_select + ' ' + $scope.BE_TEXT.YEAR_DRAWMONEYT)
                        .ok('ตกลง')
                );
            };

            //Pagination of nested objects in angularjs
            $scope.pagin = {
                currentPage: 1, itemPerPage: '10', numPage: 1
            }
            $scope.filterDatas = function (searchTxt) {
                if (!$scope.data || !$scope.data.length) return [];
                var datas = $filter('filter')($scope.data, searchTxt);
                datas = $filter('filter')(datas, $scope.advacneOptionFilter);

                return datas;
            }
            $scope.genDatas = function (searchTxt) {
                var datas = $scope.filterDatas(searchTxt);
                setNumOfPage(datas);


                var res = [];

                if ($scope.pagin.numPage > 1) {
                    datas.forEach(function (value, i) {
                        var from = (parseInt($scope.pagin.currentPage) - 1) * parseInt($scope.pagin.itemPerPage);
                        var to = from + parseInt($scope.pagin.itemPerPage);

                        if (from <= i && to > i)
                            res.push(value);
                    });
                } else {
                    res = datas.slice();
                }

                return res;
            }
            var setNumOfPage = function (datas) {
                var totalItems = 0;
                if (datas) totalItems = datas.length;
                $scope.pagin.numPage = Math.ceil(totalItems / $scope.pagin.itemPerPage);
                if ($scope.pagin.numPage <= 0) $scope.pagin.numPage = 1;

                if ($scope.pagin.currentPage > $scope.pagin.numPage) $scope.pagin.currentPage = 1;
            }
            $scope.changePage = function (targetPage) {
                $scope.pagin.currentPage = angular.copy(targetPage);
            }
            $scope.getPages = function () {
                var pages = [];

                for (var i = 0; i < $scope.pagin.numPage; i++) {
                    pages.push((i + 1));
                }

                return pages;
            }
        }]);
})();