﻿(function () {
    angular.module('ESSMobile')
        .controller('CremationSystemContentController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {

            $scope.CremationHistory = [];
            $scope.ConfigImageBereavement = '';
            $scope.FAMILYMEMBER = $scope.Text["FAMILYMEMBER"];

            $scope.init = function () {
                GetCremationHistory();
                GetConfigImageBereavement();
            };

            //GetCremationHistory
            function GetCremationHistory() {
                var URL = CONFIG.SERVER + 'HRBE/GetBereavementInfoHistory';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    //console.log(response.data);
                    $scope.CremationHistory = response.data;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }


            //GetConfigImageBereavement
            function GetConfigImageBereavement() {
                var URL = CONFIG.SERVER + 'HRBE/GetConfigImageBereavement';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.ConfigImageBereavement = response.data;
                    //console.log(response.data);
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }

            //Sum Total
            $scope.getTotal = function () {
                var total = 0;
                for (var i = 0; i < $scope.CremationHistory.length; i++) {
                    var item = $scope.CremationHistory[i];
                    total += item.Amount;
                }
                return total;
            };

            //Go To ViewRequest
            $scope.openRequest = function (id, companyCode, keyMaster, isOwner, isDataOwner, requestType) {
                //$location.path('/frmViewRequest/' + id + '/' + companyCode + '/' + keyMaster + '/' + isOwner + '/' + isDataOwner);
                $location.path('/frmViewRequest/' + id + '/' + companyCode + '/null/false/true');
            };
        }]);
})();