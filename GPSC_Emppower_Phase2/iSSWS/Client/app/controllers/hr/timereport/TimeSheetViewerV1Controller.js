﻿(function () {
angular.module('ESSMobile')
    .controller('TimeSheetViewerV1Controller', ['$scope', '$http', '$routeParams', '$location', 'CONFIG', function ($scope, $http, $routeParams, $location, CONFIG) {
        //#### FRAMEWORK FUNCTION ### START
        //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
        $scope.ChildAction.SetData = function () {
            $scope.Textcategory = 'TMREPORT_TIMESHEET';

        }

        //LoadData Function : Use to add some logic to Additional dataset before take any action
        $scope.ChildAction.LoadData = function () {
            //Do something
        }

        var employeeDate = getToken(CONFIG.USER);
        $scope.ChildAction.SetData();
        //#### FRAMEWORK FUNCTION ### END

        //#### OTHERS FUNCTION ### START 

        // *** TASK ***

        //console.log('timesheets.', $scope.document.Additional.Detail);
        $scope.timesheetsForMobile = angular.copy($scope.document.Additional.Detail);
        $scope.timesheetsForDesktop = angular.copy($scope.document.Additional.Detail);

        $scope.SumRecordHour = function (data) {
            var totalRecordHour = 0;
            var detail = $scope.document.Additional.Detail;
            if (angular.isUndefined(data)) {
                for (var i = 0 ; i < detail.length ; i++) {
                    totalRecordHour += detail[i].RecordHour;
                }
            }
            else {
                for (var i = 0 ; i < detail.length ; i++) {
                    if (data == detail[i].Date) {
                        totalRecordHour += detail[i].RecordHour;
                    }
                }
            }
            return totalRecordHour;
        };
        $scope.CountDistinctJobID = function () {
            var countJob = 0;
            var iJobId = new Array();
            var detail = $scope.document.Additional.Detail;
            if (!angular.isUndefined(detail)) {
                for (var i = 0; i < detail.length; i++) {
                    if (iJobId.filter(function (data) { return data == detail[i].JobID }) == '') {
                        countJob += 1;
                        iJobId.push(detail[i].JobID);
                    }
                }
            }
            return countJob;
        };
        $scope.sumHourByJobID = function (JobID) {
            var totalRecordHour = 0;
            var detail = $scope.document.Additional.Detail;
            for (var i = 0 ; i < detail.length ; i++) {
                if (JobID == detail[i].JobID) {
                    totalRecordHour += detail[i].RecordHour;
                }
            }
            return totalRecordHour;
        };



        // *** OT ***

        $scope.SumRequestOTHour = function () {
            var totalRequestOTHour = 0;
            var cal_dailyOTList = $scope.document.dailyOTList;
            if (!angular.isUndefined(cal_dailyOTList)) {
                for (var i = 0 ; i < cal_dailyOTList.length ; i++) {
                    totalRequestOTHour += cal_dailyOTList[i].RequestOTHour10 + cal_dailyOTList[i].RequestOTHour15 + cal_dailyOTList[i].RequestOTHour30;
                }
            }

            return CalOTHour(totalRequestOTHour);
        };
        
        $scope.RequestOTHour10 = function (data_minuted) {
            return CalOTHour(data_minuted);
        };
        $scope.RequestOTHour15 = function (data_minuted) {
            return CalOTHour(data_minuted);
        };
        $scope.RequestOTHour30 = function (data_minuted) {
            return CalOTHour(data_minuted);
        };
        function CalOTHour(data_minuted) {
            var othour = { Hour: 0, Minute: 0 };
            if (!angular.isUndefined(data_minuted)) {
                othour.Hour = data_minuted / 60;
                othour.Minute = data_minuted % 60;
            }
            return parseFloat(othour.Hour + '.' + othour.Minute);
        };
        $scope.SumOT = function (min10, min15, min30) {
            return ($scope.RequestOTHour10(min10) + $scope.RequestOTHour15(min15) + $scope.RequestOTHour30(min30)).toFixed(1);
        };
        function dynamicSort(property) {
            var sortOrder = 1;
            if (property[0] === "-") {
                sortOrder = -1;
                property = property.substr(1);
            }
            return function (a, b) {
                var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
                return result * sortOrder;
            }
        }
        function dynamicSortMultiple() {
            /*
            * save the arguments object as it will be overwritten
            * note that arguments object is an array-like object
            * consisting of the names of the properties to sort by
            */
            var props = arguments;
            return function (obj1, obj2) {
                var i = 0, result = 0, numberOfProperties = props.length;
                /* try getting a different result from 0 (equal)
                * as long as we have extra properties to compare
                */
                while (result === 0 && i < numberOfProperties) {
                    result = dynamicSort(props[i])(obj1, obj2);
                    i++;
                }
                return result;
            }
        }
        function fnToDate(dateStr) {
            var parts = dateStr.split("/");
            return new Date(parts[2], parts[1] - 1, parts[0]);
        }
        $scope.GetDailyOTLogByEmployee = function () {
            var header = $scope.document.Additional.Header[0];
            var tm = angular.copy($scope.document.Additional.Detail);
            var detail = tm.sort(dynamicSortMultiple("Date"));
            var minDate = detail[0].Date;
            var maxDate = detail[detail.length - 1].Date;
            //// check username, password with webservice
            if (!angular.isUndefined(header.RequestorNo)) {
                var BeginDate = fnToDate(minDate.substr(8, 2) + '/' + minDate.substr(5, 2) + '/' + minDate.substr(0, 4))
                var EndDate = fnToDate(maxDate.substr(8, 2) + '/' + maxDate.substr(5, 2) + '/' + maxDate.substr(0, 4))
                var URL = CONFIG.SERVER + 'HRTM/GetDailyOTLogByEmployeeID';
                var param = { EmployeeId: header.RequestorNo, BeginDate: BeginDate, EndDate: EndDate };
                $http({
                    method: 'POST',
                    url: URL,
                    data: param
                }).then(function successCallback(response) {
                    // Success
                    $scope.document.dailyOTList = response.data;
                    //console.log('dailyOTList.', $scope.document.dailyOTList);
                }, function errorCallback(response) {
                    // Error
                    console.log('error boxcontroller.', response);
                });

            };

        };
        $scope.GetDailyOTLogByEmployee();

        var PeriodDate = $scope.document.Additional.Header[0].PeriodDate;
        var URL = CONFIG.SERVER + 'HRTM/GetDayTemplateFortimesheet/' + PeriodDate.substr(0, 10);
        $http({
            method: 'POST',
            url: URL,
            data: getToken(CONFIG.USER)
        }).then(function successCallback(response) {
            // Success
            $scope.document.dayTemplate = response.data;
            //console.log('dayTemplate(DayTemplate).', $scope.document.dayTemplate);
        }, function errorCallback(response) {
            // Error
            console.log('error boxcontroller.', response);
        });

        $scope.SumOTByDate = function (date) {
            var totalRequestOTHour = 0.0;
            var cal_dailyOTList = $scope.document.dailyOTList;
            if (!angular.isUndefined(cal_dailyOTList)) {
                for (var i = 0 ; i < cal_dailyOTList.length; i++) {
                    if (cal_dailyOTList[i].BeginDate == date) {
                        totalRequestOTHour += cal_dailyOTList[i].RequestOTHour10 + cal_dailyOTList[i].RequestOTHour15 + cal_dailyOTList[i].RequestOTHour30;
                    }
                }
            }
            return CalOTHour(totalRequestOTHour);
        };

        $scope.SumOT10 = function () {
            var totalRequestOTHour = 0.0;
            var cal_dailyOTList = $scope.document.dailyOTList;
            if (!angular.isUndefined(cal_dailyOTList)) {
                for (var i = 0 ; i < cal_dailyOTList.length; i++) {
                    totalRequestOTHour += cal_dailyOTList[i].RequestOTHour10;
                }
            }
            return CalOTHour(totalRequestOTHour);
        };

        $scope.SumOT15 = function () {
            var totalRequestOTHour = 0.0;
            var cal_dailyOTList = $scope.document.dailyOTList;
            if (!angular.isUndefined(cal_dailyOTList)) {
                for (var i = 0 ; i < cal_dailyOTList.length; i++) {
                    totalRequestOTHour += cal_dailyOTList[i].RequestOTHour15;
                }
            }
            return CalOTHour(totalRequestOTHour);
        };

        $scope.SumOT30 = function () {
            var totalRequestOTHour = 0.0;
            var cal_dailyOTList = $scope.document.dailyOTList;
            if (!angular.isUndefined(cal_dailyOTList)) {
                for (var i = 0 ; i < cal_dailyOTList.length; i++) {
                    totalRequestOTHour += cal_dailyOTList[i].RequestOTHour30;
                }
            }
            return CalOTHour(totalRequestOTHour);
        };


        $scope.getDayCode = function (date) {
            if (!angular.isUndefined($scope.document.dayTemplate)) {
                for (var i = 0 ; i < $scope.document.dayTemplate.length; i++) {
                    if ($scope.document.dayTemplate[i].Day_Date == date) {
                        return $scope.document.dayTemplate[i].Day_Code;
                    }
                }
            }
        };

        $scope.getDayClass = function (date) {
            if (!angular.isUndefined($scope.document.dayTemplate)) {
                for (var i = 0 ; i < $scope.document.dayTemplate.length; i++) {
                    if ($scope.document.dayTemplate[i].Day_Date == date && ($scope.document.dayTemplate[i].IsDayOff || $scope.document.dayTemplate[i].IsHoliday)) {
                        return '-txt-org';
                    }
                }
            }
            return '-txt-blue';
        };

        //#### OTHERS FUNCTION ### END
    }]);
})();