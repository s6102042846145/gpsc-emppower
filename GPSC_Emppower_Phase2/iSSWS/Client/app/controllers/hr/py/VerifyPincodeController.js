﻿(function () {
    angular.module('ESSMobile')
        .controller('VerifyPincodeController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$window', '$sce', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $window, $sce) {


            $scope.model = {
                pinCode: ""
            };

            $scope.errorMessage = "";
            $scope.isShowIframe = false;
            $scope.link_path = "";
            $scope.sentPincode = function () {


                if (!$scope.model.pinCode) {
                    $scope.errorMessage = "กรุณาระบุ PINCODE";
                    return;
                }

                var oEmployeeData = getToken(CONFIG.USER);

                var URL = CONFIG.SERVER + 'HRPY/GetAllocateBudget';
                var oRequestParameter = {
                    InputParameter: { "PinCode": $scope.model.pinCode }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.loader.enable = false;
                    $scope.errorMessage = "";
                    console.log(response);
                    if (response.data.IsStatus) {
                        //$scope.isShowIframe = true;
                        $scope.link_path = response.data.URL + "?" + response.data.EncryptString;
                        //$scope.link_path = "https://gpsc-web-p02.pttgrp.corp/GPSC_BudgetAllocation_Test/RedirectExt/EvalLetter.aspx?" + response.data.EncryptString;
                        $window.open($scope.link_path , '_blank');
                    }
                    else {
                        $scope.isShowIframe = false;
                        $scope.errorMessage = "Pincode ไม่ถูกต้อง";
                    }

                }, function errorCallback(response) {
                    console.log(response);
                    $scope.loader.enable = false;
                });

            };

            $scope.trustSrc = function (src) {
                return $sce.trustAsResourceUrl(src);
            };

            $scope.movie = { src: "https://gpsc-web-p02.pttgrp.corp/GPSC_BudgetAllocation_Test/RedirectExt/EvalLetter?1ExmJkNhzNO3Up5dNzYeQE54j9wc9sJhGZTXQVbzyj95vjILn2TU1ppxur2uL0NCn6FWFiLVE90G2AxXbJmpVg==", title: "Egghead.io AngularJS Binding" };

        }]);
})();
