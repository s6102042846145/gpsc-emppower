﻿(function () {
    angular.module('ESSMobile')
        .controller('TaxReportPeriodSettingController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal) {

            $scope.back = function () {
                var url = '/frmViewContent/5001';
                $location.path(url);
            };

            // Get Config Multi Company
            $scope.isHideMultiCompany = true;
            $scope.GetConfingMultiCompany = function () {
                var URL = CONFIG.SERVER + 'Employee/GetConfigMultiCompany';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data === 1) {
                        $scope.isHideMultiCompany = false;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetConfingMultiCompany();

            $scope.employeeData = getToken(CONFIG.USER); 
            $scope.TextPeriod = $scope.Text["SYSTEM"];
            $scope.ListYear;
            $scope.YearsList = [];
            
            $scope.TaxReportPeriodSetting_Delete = '';
            $scope.SavePeriodSetting;
            $scope.MsgSave = '';

            $scope.ddlYear;
            $scope.dateVariable = new Date();
            $scope.defaultYear = $scope.dateVariable.getFullYear();
            $scope.ListPeriod;
            
            $scope.TaxReportPeriodSetting;
            $scope.ListTaxReportPeriodSetting;
            $scope.SelectCompanyCode = $scope.employeeData.CompanyCode;


            $scope.GetAuthorizationCompany = function () {
                var URL = CONFIG.SERVER + 'workflow/GetAuthorizationCompany';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.CompanyList = response.data;

                    if ($scope.CompanyList.length > 0) {
                        $scope.ddlCompany = $scope.CompanyList[0].CompanyCode;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetAuthorizationCompany();


            $scope.changeCompany = function (CompanyCode) {
                $scope.SelectCompanyCode = CompanyCode;
            GetYear();
                getTaxReportPeriodSetting();
            };


            GetYear();
            function GetYear() {
                var URL = CONFIG.SERVER + 'HRPY/GetYearTavi50PeriodSetting';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "SelectCompany": $scope.SelectCompanyCode }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.ListYear = response.data;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }

            $scope.init = function () {
                //console.log($scope.Text['ACCOUNT_SETTING']['SAVE_COMPLETED']);
                //console.log($scope.Text['SYSTEM']['CLOSE_DOWN']);
                getTaxReportPeriodSetting();
            };

            function findWithAttr(array, value) {
                for (var i = 0; i < array.length; i += 1) {
                    if (array[i] == value) {
                        return i;
                    }
                }
            }

            
            $scope.onLoadTaxReportPeriodSetting = function () {
                getTaxReportPeriodSetting();
            };

            function getTaxReportPeriodSetting() {
                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPY/GetTavi50PeriodSetting';
                var oRequestParameter = {
                    InputParameter: { "SelectCompany": $scope.SelectCompanyCode }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.TaxReportPeriodSetting = response.data;
                    //console.log($scope.TaxReportPeriodSetting);
                }, function errorCallback(response) {
                });
            }

            $scope.getIndex = function (index) {
                for (var i = 0; i < $scope.ListYear.length; i++) {
                    if ($scope.ListYear[i].YearID === index)
                        return i;
                }
            };

            $scope.onSaveSetting = function () {
                $scope.MsgSave = '';
                saveTaxReportPeriodSetting();
            };
            function saveTaxReportPeriodSetting() {
                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPY/SaveTavi50PeriodSetting';

                var oRequestParameter = {
                    InputParameter: { "DataPeriodSetting": $scope.TaxReportPeriodSetting, "SelectCompany": $scope.SelectCompanyCode }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.SavePeriodSetting = response.data;

                    var modalInstance = $uibModal.open({
                        animation: $scope.animationsEnabled,
                        templateUrl: 'completeModalPincode.ng-popup.html',
                        controller: 'completeModalPincodeController',
                        backdrop: 'static',
                        keyboard: false,
                        size: 'sp',
                        resolve: {
                            items: function () {
                                var data = {
                                    msg: $scope.Text['ACCOUNT_SETTING']['SAVE_COMPLETED'],
                                    text: $scope.Text['SYSTEM']['CLOSE_DOWN']
                                };
                                return data;
                            }
                        }
                    });
                    

                }, function errorCallback(response) {
                });
            }

            //ADD ROW DATA
            $scope.addNew = function () {
                var gen_periodID = Math.max.apply(Math, $scope.TaxReportPeriodSetting.map(function (o) { return o.periodID; })) + 1;
                var currentYear = new Date().getFullYear();
                var dNow = currentYear + '-02-01T00:00:00';

                var maxYear = currentYear - 1;
                for (var i = 0; i < $scope.TaxReportPeriodSetting.length; ++i) {
                    if ($scope.TaxReportPeriodSetting[i].periodYear > maxYear) maxYear = $scope.TaxReportPeriodSetting[i].periodYear;
                }
                
                $scope.TaxReportPeriodSetting.push({
                    'periodID': gen_periodID,
                    //'periodYear': currentYear,
                    'periodYear': maxYear + 1,
                    'effectiveDate': dNow,
                    'isActive': true, 
                    'signatureImageBase64': ""
                });
            };

            $scope.remove = function (select_periodID) {
                var newDataList = [];

                angular.forEach($scope.TaxReportPeriodSetting, function (value) {
                    if (value.periodID != select_periodID) {
                        newDataList.push(value);
                    }

                    if (value.periodID == select_periodID) {
                        $scope.TaxReportPeriodSetting_Delete += select_periodID + ",";
                    }
                });

                $scope.TaxReportPeriodSetting = newDataList;
            };

            
            $scope.setSelected = function (selectedDate, type, id) {
                $scope.TaxReportPeriodSetting.forEach(function (v) {
                    if (type == 'effectiveDate') {
                        if (v.periodID == id) v.effectiveDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
                    }
                });
            };

            $scope.downloadSelect = function (index) {
                var a = document.createElement("a");
                a.href = $scope.TaxReportPeriodSetting[index].signatureImageBase64;
                a.download = "Signature_" + $scope.TaxReportPeriodSetting[index].periodYear + ".jpeg";
                a.click();
            };

            $scope.uploadSelect = function (index) {
                console.log(angular.element("[name='uploadInput']"))
                angular.element("[name='uploadInput']")[index].click();

                angular.element("[name='uploadInput']")[index].onchange = function (e) {
                    getBase64(e.target.files[0]).then(r => {
                        $scope.TaxReportPeriodSetting[index].signatureImageBase64 = r;
                        $scope.$apply();
                    });

                };
            };

            $scope.reloadSelect = function (index) {
                angular.element("[name='uploadInput']")[index].click();

                angular.element("[name='uploadInput']")[index].onchange = function (e) {
                    getBase64(e.target.files[0]).then(r => {
                        $scope.TaxReportPeriodSetting[index].signatureImageBase64 = r;
                        $scope.$apply();
                    });

                };
            };

            function getBase64(file) {
                return new Promise((resolve, reject) => {
                    const reader = new FileReader();
                    reader.readAsDataURL(file);
                    reader.onload = () => resolve(reader.result);
                    reader.onerror = error => reject(error);
                });
            }

        }]).controller('completeModalPincodeController', ['items', '$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', '$uibModalInstance', function (items, $scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal, $uibModalInstance) {
  

        }]);
})();
