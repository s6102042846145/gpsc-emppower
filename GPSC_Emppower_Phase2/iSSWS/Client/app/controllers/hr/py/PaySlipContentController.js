﻿(function () {
    angular.module('ESSMobile')
        .controller('PaySlipContentController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal) {

            $scope.OldPinCode = "";
            $scope.countIncorrectPinCode = 0;

            $scope.splitOffCycle = function (OffCycle, keyMonth) {

                var text_month_detail = "";
                var fields = OffCycle.split('|');
                var DateOffCycle = fields[0];
                var IsOffCycle = fields[1];
                if (IsOffCycle === '1') {

                    if (DateOffCycle) {
                        var str_split = DateOffCycle.toString().substring(2, 4);
                        var int_splint = parseInt(str_split);
                        text_month_detail = $scope.TextMonth["D_M" + int_splint.toString()];

                        var day_data = DateOffCycle.toString().substring(0, 2);
                        var month_data = DateOffCycle.toString().substring(2, 4);
                        var year_data = DateOffCycle.toString().substring(4, 8);
                        var concat_data = day_data + '/' + month_data + '/' + year_data;

                        return text_month_detail + " (" + concat_data + ") ";
                    }
                }
                else {
                    text_month_detail = $scope.TextMonth["D_M" + keyMonth];
                    return text_month_detail;
                }
            };

            


            function download(dataurl, filename) {
                var a = document.createElement("a");
                a.href = dataurl;
                a.setAttribute("download", filename);
                var b = document.createEvent("MouseEvents");
                b.initEvent("click", false, true);
                a.dispatchEvent(b);
                return false;
            }

            $scope.countIncorrectPinCode = 0;
            $scope.file_download = "";

            $scope.TextMonth = $scope.Text["SYSTEM"];
            $scope.YearsList = [];

            $scope.ddlYear;
            $scope.ddlPeriod;
            $scope.ListPeriod;
            $scope.PayslipData = [];
            $scope.PayslipPeriodSetting;
            $scope.ListPayslipPeriodSetting;

            $scope.OldPinCode = '';
            $scope.isValidate = false;


            $scope.init = function () {
                GetYears();
            };


            //function GetIP() {
            //    var URL = CONFIG.SERVER + 'HRPY/GetIP';
            //    $scope.employeeData = getToken(CONFIG.USER);
            //    var oRequestParameter = {
            //        InputParameter: {}
            //        , CurrentEmployee: $scope.employeeData
            //        , Requestor: $scope.requesterData
            //        , Creator: getToken(CONFIG.USER)
            //    };
            //    $http({
            //        method: 'POST',
            //        url: URL,
            //        data: oRequestParameter
            //    }).then(function successCallback(response) {

            //        $scope.IP = response.data;

            //        $scope.loader.enable = false;
            //    }, function errorCallback(response) {
            //        $scope.loader.enable = false;
            //    });
            //}


            //function alertPin() {
            //    var oEmployeeData = getToken(CONFIG.USER);
            //    var URL = CONFIG.SERVER + 'HRPY/AlertIncorrectPIN';
            //    var oRequestParameter = {
            //        InputParameter: {}
            //        , CurrentEmployee: oEmployeeData
            //        , Requestor: $scope.requesterData
            //    };

            //    $http({
            //        method: 'POST',
            //        url: URL,
            //        data: oRequestParameter
            //    }).then(function successCallback(response) {
            //    }, function errorCallback(response) {
            //    });
            //}

            $scope.year_display = "";
            $scope.year_month_display = "";
            $scope.tempYear = "";
            $scope.tempMonth = "";
            $scope.temp_select_payslip = function (year, month) {
                $scope.tempYear = year;
                $scope.tempMonth = month;
            };

            function GetYears() {
                var URL = CONFIG.SERVER + 'HRPY/GetPaySlipYear';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    angular.forEach(response.data, function (value, key) {
                        $scope.YearsList.push(value);
                    });
                    $scope.ddlYear = $scope.YearsList[0];
                    GetListPayslipPeriodSetting($scope.YearsList[0]);

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }

            $scope.filterYear = function (key_year) {
                GetListPayslipPeriodSetting(key_year);
            };

            $scope.GetListPayslipPeriodSetting = function () {
                GetListPayslipPeriodSetting($scope.ddlYear);
            };

            function GetListPayslipPeriodSetting(key_year) {

                $scope.tempYear = key_year;

                var URL = CONFIG.SERVER + 'HRPY/GetListPayslipPeriodSetting';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "Year": key_year }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.ListPeriod = response.data;

                    $scope.ListPeriod.reverse();

                    if ($scope.ListPeriod !== null && $scope.ListPeriod.length > 0) {

                        var month_select = $scope.ListPeriod.filter(function (element) {
                            return element.MONTH_SELECT == true;
                        });

                        if (month_select.length > 0) {
                            $scope.ddlPeriod = month_select[0].MONTH_VALUE;
                        }
                        else
                            $scope.ddlPeriod = $scope.ListPeriod[0].MONTH_VALUE;

                        $scope.tempMonth = $scope.ddlPeriod;
                    }



                    // หาก OffCycle
                    if ($scope.ListPeriod.length > 0) {

                        angular.forEach($scope.ListPeriod, function (value, key) {
                            var OffCycle = $scope.splitOffCycle(value.MONTH_VALUE, value.MONTH_KEY);
                            value.MONTH_NAME = OffCycle;
                        });
                    }

                    //console.log(response.data);

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }

            $scope.zeroPad = function (num) {
                return num.toString().padStart(3, "0");
            };

            $scope.monthSelect = function (str_datetime) {
                if (str_datetime) {
                    var str_split = str_datetime.toString().substring(2, 4);
                    var int_splint = parseInt(str_split);
                    var text_month_detail = $scope.TextMonth["D_M" + int_splint.toString()];
                    return text_month_detail;
                }
                return "";
            };

            $scope.onLoadPaySlip = function () {
                var modalInstance = $uibModal.open({
                    animation: $scope.animationsEnabled,
                    templateUrl: 'openPincode.ng-popup.html',
                    controller: 'PaySlipContentController',
                    backdrop: 'static',
                    keyboard: false,
                    size: 'sp',
                    resolve: {
                        items: function () {
                            var data = {
                                requestor: $scope.requesterData,
                                text: $scope.Text,
                                loadPayslip: function () {
                                    $scope.getPayslip();
                                },
                                fnForgetPincode: function () {
                                    $scope.forgetPincode();
                                }
                            };
                            return data;
                        }
                    }
                });
                //getValidatePinCode();
            };

            $scope.getPayslip = function () {
                $scope.PayslipData = [];


                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPY/GetPaySlipReportData';
                var oRequestParameter = {
                    InputParameter: { "PeriodSetting": $scope.ddlPeriod, "PinCode": $scope.OldPinCode }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.PayslipData = response.data;
                    //if ($scope.PayslipData.length == 0) {
                    //$scope.promtDialogPincode();
                    //}

                    $scope.year_display = $scope.tempYear;
                    $scope.year_month_display = $scope.tempMonth;

                }, function errorCallback(response) {
                });
            };

            $scope.alertPin = function () {
                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPY/AlertIncorrectPIN';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                }, function errorCallback(response) {
                });

            };

            $scope.onLoadPaySlipPeriodSetting = function () {
                getPaySlipPeriodSetting();
            };

            function getPaySlipPeriodSetting() {
                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPY/GetPeriodSettingAll';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.PayslipPeriodSetting = response.data;
                }, function errorCallback(response) {
                });
            }

            //GetReport
            $scope.GetPaySlipExport = function (type_download) {
                var str_category_export = "";
                var str_status_export = "";
                var type_file = "";
                $scope.loader.enable = true;
                angular.forEach($scope.selection_category, function (value) {
                    str_category_export += value + ",";
                });

                angular.forEach($scope.selection_status, function (value) {
                    str_status_export += value + ",";
                });

                if (type_download === 'PDF') {
                    type_file = 'pdf';
                } else {
                    type_file = 'xlsx';
                }

                var URL = CONFIG.SERVER + 'HRPY/GetPaySlipExport';

                $scope.employeeData = getToken(CONFIG.USER);
                console.log('set employee-->' + $scope.employeeData);
                var oRequestParameter = {
                    InputParameter: {
                        Type: str_category_export
                        , Status: str_status_export
                        , ReportName: $scope.Text["APPLICATION"].PAYSLIPREPORT
                        , LanguageCode: $scope.employeeData.Language
                        , Employee_id: $scope.employeeData.EmployeeID
                        , EmployeeName: $scope.employeeData.Name
                        , PositionName: ''
                        , OrgUnitName: ''
                        , ExportType: type_download //"PDF" //EXCEL
                        , DateText: $scope.Text["BE_REPORT"].DATETEXT
                        , TimeText: $scope.Text["BE_REPORT"].TIMETEXT
                        , T_SUBJECT: $scope.Text["PAYSLIPREPORT"].SUBJECT
                        , T_COLCODE: $scope.Text["PAYSLIPREPORT"].COLCODE
                        , T_COLINCOME: $scope.Text["PAYSLIPREPORT"].COLINCOME
                        , T_COLAMOUNT: $scope.Text["PAYSLIPREPORT"].COLAMOUNT
                        , T_COLOUTCOME: $scope.Text["PAYSLIPREPORT"].COLOUTCOME
                        , T_COLREMARK: $scope.Text["PAYSLIPREPORT"].COLREMARK
                        , T_COLTOTALINCOME: $scope.Text["PAYSLIPREPORT"].COLTOTALINCOME
                        , T_COLTOTALOUTCOME: $scope.Text["PAYSLIPREPORT"].COLTOTALOUTCOME
                        , T_COLNET: $scope.Text["PAYSLIPREPORT"].COLNET
                        , T_COLINCOMECOLLECTION: $scope.Text["PAYSLIPREPORT"].COLINCOMECOLLECTION
                        , T_COLTAXCOLLECTION: $scope.Text["PAYSLIPREPORT"].COLTAXCOLLECTION
                        , T_COLTOTALCOLLECTION: $scope.Text["PAYSLIPREPORT"].COLTOTALCOLLECTION
                        , T_COLPROVIDENTFUND: $scope.Text["PAYSLIPREPORT"].COLPROVIDENTFUND
                        , T_COLSOCIALSECURITY: $scope.Text["PAYSLIPREPORT"].COLSOCIALSECURITY
                        , T_COLPAID: $scope.Text["PAYSLIPREPORT"].COLPAID
                        , T_BANKINTRO: $scope.Text["PAYSLIPREPORT"].BANKINTRO
                        , T_CONTACT: $scope.Text["PAYSLIPREPORT"].CONTACT
                        , PeriodSetting: $scope.ddlPeriod
                        , EmpPayslip: $scope.requesterData.EmployeeID
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    console.log('success');
                    var file_name = "PaySlipReport" + $scope.employeeData.EmployeeID;
                    if (type_download === "PDF") {
                        file_name += ".pdf";
                    } else if (type_download === "EXCEL") {
                        file_name += ".xls";
                    }
                    var url = CONFIG.SERVER + 'Client/Report/' + file_name;
                    download(url, file_name);
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    console.log(response);

                    $scope.loader.enable = false;
                });
            };

            $scope.forgetPincode = function () {
                $location.path('frmViewContent/907');
            };
            //Modify by Nipon Supap รวม VerifyPinCode เข้าด้วยกัน 24/06/2020
            $scope.promtDialogPincode = function () {
                $mdDialog.show({
                    controller: DialogControllerPincode,
                    templateUrl: 'views/common/dialog/pincodeInput.tmpl.html',
                    parent: angular.element(document.body),
                    //targetEvent: ev,
                    clickOutsideToClose: true,
                    locals: {
                        params: {
                            title: 'PinCOde Dialog',
                            data: '',
                            columns: '',
                            btnSave: $scope.Text['SYSTEM']['BUTTON_OK'],
                            btnCaancel: $scope.Text['ACTION']['CANCEL'],
                            forgetPincode: $scope.Text['PAYSLIPDATA']['NEWPINREQUEST'],
                            OldPinCode: $scope.OldPinCode,
                            MsgValidate: $scope.MsgValidate
                        }
                    },
                })
                    .then(function (item) {
                        if (!item) {
                            $scope.MsgValidate = "กรุณาระบุ PINCODE";
                            $scope.promtDialogPincode();
                        }
                        else {
                            $scope.OldPinCode = item;
                            $scope.getValidatePinCode();
                        }
                        //$scope.getPayslip();
                    }, function () {

                    });
            };
            function DialogControllerPincode($scope, $mdDialog, params) {
                $scope.data = angular.copy(params.data);
                $scope.columns = angular.copy(params.columns);
                $scope.Title = params.Title;
                $scope.forgetPincodeText = params.forgetPincode;
                $scope.BTN_SAVE = params.btnSave;
                $scope.BTN_CANCEL = params.btnCaancel;
                $scope.OldPinCode = params.OldPinCode;
                if (!$scope.OldPinCode) {
                    $scope.MsgValidate = "กรุณาระบุ PINCODE";
                }
                else
                    $scope.MsgValidate = params.MsgValidate;

                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.save = function (answer) {
                    $mdDialog.hide(answer);
                };

                $scope.forgetLikePincode = function () {
                    $mdDialog.cancel();
                    $location.path('frmViewContent/907');
                }
            }

            $scope.getValidatePinCode = function () {
                if (!$scope.OldPinCode) {
                    $scope.MsgValidate = "กรุณาระบุ PINCODE";
                    return;
                }

                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPY/VerifyPinCode';
                var oRequestParameter = {
                    InputParameter: { "PinCode": $scope.OldPinCode }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.isValidate = response.data;
                    if ($scope.isValidate) {
                        $scope.countIncorrectPinCode = 0;
                        $scope.MsgValidate = '';
                        $scope.getPayslip();
                        //$uibModalInstance.close();
                    }
                    else {

                        if ($scope.countIncorrectPinCode > 2) {
                            $scope.alertPin();
                        }

                        $scope.countIncorrectPinCode += 1;
                        $scope.MsgValidate = 'กรุณาใส่ PINCODE ให้ถูกต้อง';
                        $scope.promtDialogPincode();
                    }
                }, function errorCallback(response) {
                });
            };
            $scope.alertPin = function () {
                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPY/AlertIncorrectPIN';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                }, function errorCallback(response) {
                });
            };

        }]);
        //.controller('openPincodeController', ['items', '$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', '$uibModalInstance', function (items, $scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal, $uibModalInstance) {

        //    $scope.MsgValidate = "";
        //    $scope.OldPinCode = "";
        //    $scope.Text = items.text;
        //    $scope.countIncorrectPinCode = 0;

        //    $scope.getValidatePinCode = function () {

        //        if (!$scope.OldPinCode) {
        //            $scope.MsgValidate = "กรุณาระบุ PINCODE";
        //            return;
        //        }

        //        var oEmployeeData = getToken(CONFIG.USER);
        //        var URL = CONFIG.SERVER + 'HRPY/VerifyPinCode';
        //        var oRequestParameter = {
        //            InputParameter: { "PinCode": $scope.OldPinCode }
        //            , CurrentEmployee: oEmployeeData
        //            , Requestor: items.requestor
        //        };

        //        $http({
        //            method: 'POST',
        //            url: URL,
        //            data: oRequestParameter
        //        }).then(function successCallback(response) {
        //            $scope.isValidate = response.data;
        //            if ($scope.isValidate) {
        //                $scope.countIncorrectPinCode = 0;
        //                $scope.MsgValidate = '';
        //                items.loadPayslip();
        //                $uibModalInstance.close();
        //            }
        //            else {

        //                if ($scope.countIncorrectPinCode > 2) {
        //                    $scope.alertPin();
        //                }

        //                $scope.countIncorrectPinCode += 1;
        //                $scope.MsgValidate = 'กรุณาใส่ PINCODE ให้ถูกต้อง';
        //            }
        //        }, function errorCallback(response) {
        //        });
        //    };

        //    $scope.alertPin = function () {

        //        var oEmployeeData = getToken(CONFIG.USER);
        //        var URL = CONFIG.SERVER + 'HRPY/AlertIncorrectPIN';
        //        var oRequestParameter = {
        //            InputParameter: {}
        //            , CurrentEmployee: oEmployeeData
        //            , Requestor: items.requestor
        //        };

        //        $http({
        //            method: 'POST',
        //            url: URL,
        //            data: oRequestParameter
        //        }).then(function successCallback(response) {
        //        }, function errorCallback(response) {
        //        });
        //    };

        //    $scope.closeModal = function () {
        //        $uibModalInstance.close();
        //    };
            
        //    $scope.forgetPincode = function () {
        //        $uibModalInstance.close();
        //        items.fnForgetPincode();
        //    };


        //}])
        //.controller('exampleModalController', ['items', '$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', '$uibModalInstance', function (items, $scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal, $uibModalInstance) {

        //}]);
})();
