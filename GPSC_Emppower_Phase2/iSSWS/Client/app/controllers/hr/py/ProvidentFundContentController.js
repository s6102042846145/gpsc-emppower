﻿(function () {
    angular.module('ESSMobile')
        .controller('ProvidentFundContentController', ['$scope', '$http', '$routeParams', '$location', 'DTOptionsBuilder', '$route', 'CONFIG', '$mdDialog', '$filter', '$timeout', '$q', '$log', function ($scope, $http, $routeParams, $location, DTOptionsBuilder, $route, CONFIG, $mdDialog, $filter, $timeout, $q, $log) {
            //#### FRAMEWORK FUNCTION ### START
            $scope.personalfamilyloader = false;
            $scope.Textcategory = "FAMILY";
            $scope.PROVIDENTFUND = $scope.Text["PROVIDENTFUND"];
            $scope.COUNTRY = $scope.Text["COUNTRY"];
            $scope.NATIONALITY = $scope.Text["NATIONALITY"];
            $scope.TITLENAME = $scope.Text["TITLENAME"];
            $scope.content.isShowHeader = true;
            $scope.TEXTProvidentFundDetail = [];
            $scope.FUNDPATTERN;
            $scope.CheckPeriod = [];
            $scope.FundQuarterData = [];
            $scope.YearsList = [];
            $scope.years = '';
            $scope.dateVariable = new Date();
            $scope.loader.enable = true;
            $scope.years = $scope.dateVariable.getFullYear();
            $scope.prop = {
                "type": "select",
                "name": $scope.years,
                "value": $scope.years,
                "values": $scope.YearsList
            };

            $scope.series = [];
            $scope.data = [];
            $scope.labels = ['Quarter1', 'Quarter2', 'Quarter3', 'Quarter4'];
            $scope.options = { legend: { display: true } }; // missing 

            $scope.SelectCompanyCode = $scope.employeeData.CompanyCode;

            $scope.init = function () {
                //TAB1
                //รายละเอียดกองทุน && รูปภาพข้อระเบียบกกองทุน
                getCheckPeriod();
                getProvidentFundData();
                getTextProvidentFundDetail();
                //TAB2
                getProvidentMemberData();

                //TAB3
                GetYears();
                GetFundQuarter($scope.years);
            }

            //------------------- START TAB1 ----------------------------
            function getProvidentFundData() {
                var oEmployeeData = getToken(CONFIG.USER);
                var checkDate = $filter('date')(new Date(), 'yyyy-MM-dd');
                var URL = CONFIG.SERVER + 'HRPY/GetProvidentFundData';
                var oRequestParameter = {
                    InputParameter: { "EmployeeID": oEmployeeData.EmployeeID }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.ProvidentFund = response.data;
                    var strFund = "FUND_";
                    $scope.FUNDPATTERN = strFund.concat($scope.ProvidentFund.ProvidentFundNumber);
                    $scope.DictFileAttachmentList = response.data.dictFileAttachmentList;
                }, function errorCallback(response) {
                });
            }

            function getTextProvidentFundDetail() {
                var URL = CONFIG.SERVER + 'HRPY/GetTextProvidentFundDetail';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "KeyType": "GETDATA", "KeyCode": "TEXTINFO", "KeyValue":"" }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.TEXTProvidentFundDetail = response.data;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }
            //------------------- END TAB1 ----------------------------

            //------------------- START TAB2 ----------------------------
            function getProvidentMemberData() {
                var oEmployeeData = getToken(CONFIG.USER);
                var checkDate = $filter('date')(new Date(), 'yyyy-MM-dd');
                var URL = CONFIG.SERVER + 'HRPY/GetProvidentMemberData';
                var oRequestParameter = {
                    InputParameter: { "EmployeeID": oEmployeeData.EmployeeID }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                    //data: getToken(CONFIG.USER)
                }).then(function successCallback(response) {
                    // Success
                    $scope.ProvidentMemberList = response.data;
                    //$scope.DictFileAttachmentList = response.data.dictFileAttachmentList;
                }, function errorCallback(response) {
                });
            }

            function getCheckPeriod() {
                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPY/GetCheckPeriod';
                var oRequestParameter = {
                    InputParameter: { "EmployeeID": oEmployeeData.EmployeeID }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.CheckPeriod = response.data;
                }, function errorCallback(response) {
                });
            }
            //------------------- END TAB2 ----------------------------


            //------------------- START TAB3 ----------------------------
            function GetYears() {

                var URL = CONFIG.SERVER + 'HRBE/GetYears';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.YearsList.splice(0, $scope.YearsList.length);
                    angular.forEach(response.data, function (value, key) {
                        $scope.YearsList.push(value.YearName);
                    });

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }

            function GetFundQuarter(key_year) {

                var URL = CONFIG.SERVER + 'HRPY/GetFundQuarter';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "KeyType": "SHOWGRAPH", "KeyYear": key_year, "KeyValue": "", "SelectCompany": $scope.SelectCompanyCode }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.FundQuarterData = response.data;

                    var Quarter_array = [];
                    var Quarter_val = [];

                    angular.forEach(response.data, function (value, key) {
                        if (value.FundID) {
                            var text_fund = $scope.Text["PROVIDENTFUND"][value.FundID];
                            //var text_fund = value.FundID;
                            //$scope.series.push(text_fund);
                        }

                        //if (value.Quarter1) {
                            Quarter_val.push(value.Quarter1);
                        //}

                        //if (value.Quarter2) {
                            Quarter_val.push(value.Quarter2);
                        //}

                        //if (value.Quarter3) {
                            Quarter_val.push(value.Quarter3);
                        //}

                        //if (value.Quarter4) {
                            Quarter_val.push(value.Quarter4);

                        //}

                        Quarter_array.push(Quarter_val);
                        Quarter_val = [];

                    });

                    $scope.series = ['Option 1', 'Option 2', 'Option 3', 'Option 4', 'Option 5'];
                    $scope.options = {
                        scales: {
                            yAxes: [
                                {
                                    id: 'y-axis-1',
                                    type: 'linear',
                                    display: true,
                                    position: 'left',
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }
                            ]
                        }
                    };
                    $scope.data = Quarter_array;
                    //console.log($scope.data);
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }

            $scope.colors = [$scope.Text["PROVIDENTFUND"]['COLOR1'], $scope.Text["PROVIDENTFUND"]['COLOR2'], $scope.Text["PROVIDENTFUND"]['COLOR3'], $scope.Text["PROVIDENTFUND"]['COLOR4'], $scope.Text["PROVIDENTFUND"]['COLOR5']];

            $scope.datasetOverride = [{
                borderWidth: 3,
                pointRadius: 5,
                pointHitRadius: 5,
                pointBorderColor: $scope.Text["PROVIDENTFUND"]['COLOR1'],
                pointBackgroundColor: $scope.Text["PROVIDENTFUND"]['COLOR1'],
                backgroundColor: "#eeeeee05"
            }, {
                    borderWidth: 3,
                    pointRadius: 5,
                    pointHitRadius: 5,
                    pointBorderColor: $scope.Text["PROVIDENTFUND"]['COLOR2'],
                    pointBackgroundColor: $scope.Text["PROVIDENTFUND"]['COLOR2'],
                    backgroundColor: "#eeeeee05",
                }, {
                    borderWidth: 3,
                    pointRadius: 5,
                    pointHitRadius: 5,
                    pointBorderColor: $scope.Text["PROVIDENTFUND"]['COLOR3'],
                    pointBackgroundColor: $scope.Text["PROVIDENTFUND"]['COLOR3'],
                    backgroundColor: "#eeeeee05",
                }, {
                    borderWidth: 3,
                    pointRadius: 5,
                    pointHitRadius: 5,
                    pointBorderColor: $scope.Text["PROVIDENTFUND"]['COLOR4'],
                    pointBackgroundColor: $scope.Text["PROVIDENTFUND"]['COLOR4'],
                    backgroundColor: "#eeeeee05",
                }, {
                    borderWidth: 3,
                    pointRadius: 5,
                    pointHitRadius: 5,
                    pointBorderColor: $scope.Text["PROVIDENTFUND"]['COLOR5'],
                    pointBackgroundColor: $scope.Text["PROVIDENTFUND"]['COLOR5'],
                    backgroundColor: "#eeeeee05",
                }];

            $scope.filterYear = function (key_year) {
                GetFundQuarter(key_year);
                //GetGraph();
            };

            //------------------- END TAB3 ----------------------------
        }]);
})();