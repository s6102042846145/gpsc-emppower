﻿(function () {
    angular.module('ESSMobile')
        .controller('PinSettingContentController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal) {


            $scope.isValidateNewPIN;
            $scope.isValidate = false;
            $scope.MsgValidate = '';

            $scope.MsgChange = '';
            $scope.MsgRequestNewPIN = '';
            $scope.isRequestNewPIN;

            $scope.SetOldPinCode = '';
            $scope.SetNewPinCode = '';
            $scope.ChangeSetPinCode;

            $scope.init = function () {

            };

            $scope.isShowSubmitPincode = false;
            $scope.isChangePasswordNoDuplicate = false;
            $scope.verifyDuplicatePassword = function () {

                if ($scope.SetNewPinCode && $scope.ConfirmSetNewPinCode) {

                    if ($scope.SetNewPinCode === $scope.ConfirmSetNewPinCode) {
                        $scope.isChangePasswordNoDuplicate = false;
                    }
                    else {
                        $scope.isChangePasswordNoDuplicate = true;
                    }
                }
                else {
                    $scope.isChangePasswordNoDuplicate = false;
                }
                $scope.verifyDataInput();
            };
            $scope.verifyDataInput = function () {

                if ($scope.SetOldPinCode && $scope.SetNewPinCode && $scope.ConfirmSetNewPinCode) {

                    if ($scope.isChangePasswordNoDuplicate == false) {
                        $scope.isShowSubmitPincode = true;
                    }
                    else
                        $scope.isShowSubmitPincode = false;
                }
                else {
                    $scope.isShowSubmitPincode = false;
                }
            };

            // Vertify And Change Pincode
            $scope.onChangePinCode = function () {
                $scope.MsgChange = '';
                getValidatePinCode();
            };
            function getValidatePinCode() {
                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPY/ValidatePinCodeSetting';
                var oRequestParameter = {
                    InputParameter: {
                        "PinCode": $scope.SetOldPinCode,
                        "NewPinCode": $scope.SetNewPinCode,
                        "ConfirmNewPinCode": $scope.ConfirmSetNewPinCode
                    }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    console.log(response);

                    var modalInstance = "";
                    $scope.isValidateNewPIN = response.data;
                    if ($scope.isValidateNewPIN[0].RESULT) {

                        $scope.isValidate == $scope.isValidateNewPIN[0].RESULT;
                        $scope.MsgValidate = '';

                        modalInstance = $uibModal.open({
                            animation: $scope.animationsEnabled,
                            templateUrl: 'completeModalPincode.ng-popup.html',
                            controller: 'completeModalPincodeController',
                            backdrop: 'static',
                            keyboard: false,
                            size: 'sp',
                            resolve: {
                                items: function () {
                                    var data = {
                                        resultValidateNewPIN: $scope.isValidateNewPIN,
                                        text: $scope.Text
                                    };
                                    return data;
                                }
                            }
                        });

                        getChangePinCode();
                    }
                    else {

                        //$scope.isValidate == $scope.isValidateNewPIN[0].RESULT;
                        //$scope.MsgValidate = $scope.isValidateNewPIN[0].MSG_RESULT;
                        modalInstance = $uibModal.open({
                            animation: $scope.animationsEnabled,
                            templateUrl: 'errorModalPincode.ng-popup.html',
                            controller: 'errorModalPincodeController',
                            backdrop: 'static',
                            keyboard: false,
                            size: 'sp',
                            resolve: {
                                items: function () {
                                    var data = {
                                        resultValidateNewPIN: $scope.isValidateNewPIN,
                                        text: $scope.Text
                                    };
                                    return data;
                                }
                            }
                        });

                    }
                }, function errorCallback(response) {
                });
            }
            function getChangePinCode() {
                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPY/ChangePinCode';
                var oRequestParameter = {
                    InputParameter: { "PinCode": $scope.SetOldPinCode, "NewPinCode": $scope.SetNewPinCode }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.ChangeSetPinCode = response.data;
                    $scope.MsgChange = 'Change Success';
                }, function errorCallback(response) {
                });
            }

            // Request New Pincode Sent To Email
            $scope.modelForgetPincode = function () {

                var modalInstance = $uibModal.open({
                    animation: $scope.animationsEnabled,
                    templateUrl: 'forgetPincodeChangePincode.ng-popup.html',
                    controller: 'forgetPincodeChangePincodeController',
                    backdrop: 'static',
                    keyboard: false,
                    size: 'sp',
                    resolve: {
                        items: function () {
                            var data = {
                                requesterData: $scope.requesterData,
                                text:$scope.Text
                            };
                            return data;
                        }
                    }
                });
            };

        }]).controller('forgetPincodeChangePincodeController', ['items', '$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', '$uibModalInstance', function (items, $scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal, $uibModalInstance) {

            $scope.requesterData = items.requesterData;
            $scope.Text = items.text;
            $scope.onRequestNEWPin = function () {
                $scope.MsgRequestNewPIN = '';
                getRequestNEWPin();
            };

            function getRequestNEWPin() {
                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPY/RequestNEWPin';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    //$scope.isRequestNewPIN = response.data;
                    //$scope.MsgRequestNewPIN = 'Send Mail Success';
                }, function errorCallback(response) {
                });
            }

            $scope.onRequestNEWPin();

            $scope.closeModal = function () {
                $uibModalInstance.close();
            };

        }])
        .controller('errorModalPincodeController', ['items', '$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', '$uibModalInstance', function (items, $scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal, $uibModalInstance) {

            $scope.msg_result = "";
            $scope.msg_result = items.resultValidateNewPIN[0].MSG_RESULT;
            $scope.Text = items.text;
            $scope.closeModal = function () {
                $uibModalInstance.close();
            };

        }]).controller('completeModalPincodeController', ['items', '$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', '$uibModalInstance', function (items, $scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal, $uibModalInstance) {

            $scope.msg_result = "";
            $scope.msg_result = items.resultValidateNewPIN[0].MSG_RESULT;
            $scope.Text = items.text;
            $scope.closeModal = function () {
                $uibModalInstance.close();
            };

        }]);
})();
