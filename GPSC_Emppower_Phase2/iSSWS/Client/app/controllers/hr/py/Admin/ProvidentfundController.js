﻿(function () {
    angular.module('ESSMobile')
        .controller('ProvidentfundController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {

            $scope.importProvidentFund = function () {
                var url = '/frmViewContent/920';
                $location.path(url);
            };

            // Get Config Multi Company
            $scope.isHideMultiCompany = true;
            $scope.GetConfingMultiCompany = function () {
                var URL = CONFIG.SERVER + 'Employee/GetConfigMultiCompany';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data === 1) {
                        $scope.isHideMultiCompany = false;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetConfingMultiCompany();

            $scope.model_export = {
                Year: "",
                Month: "",
                FundId: ""
            };

            // Support Admin Multi Company
            $scope.employeeData = getToken(CONFIG.USER);
            $scope.selectCompanyCode = $scope.employeeData.CompanyCode; // Default CompanyCode GPSC
            $scope.CompanyList = [];

            // Get List Authorization Comapny In Employee Admin
            $scope.GetAuthorizationCompany = function () {
                var URL = CONFIG.SERVER + 'workflow/GetAuthorizationCompany';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.CompanyList = response.data;
                    $scope.loadYear();

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetAuthorizationCompany();

            // Change Company Reload Data Export Report
            $scope.changeCompany = function () {

                $scope.loadYear();
            };

            $scope.provident_year = [];
            $scope.loadYear = function () {

                var URL = CONFIG.SERVER + 'HRPY/GetYearProvidentfund';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {
                        SelectCompany: $scope.selectCompanyCode
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data.length > 0) {
                        $scope.provident_year = response.data;
                        $scope.model_export.Year = response.data[0];
                        $scope.loadMonth(response.data[0]);
                    }
                    else {
                        $scope.provident_year = [];
                        $scope.model_export.Year = "";
                    }
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });

            };

            $scope.provident_month = [];
            $scope.loadMonth = function (year) {

                var URL = CONFIG.SERVER + 'HRPY/GetMonthProvidentfund';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {
                        SelectCompany: $scope.selectCompanyCode,
                        YearProvidentfund: year
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data.length > 0) {
                        $scope.provident_month = response.data;
                        $scope.model_export.Month = response.data[0];
                        $scope.loadTextProvident(year, response.data[0]);
                    }
                    else {
                        $scope.provident_month = [];
                        $scope.model_export.Month = "";
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });

            };

            $scope.provident_text = [];
            $scope.loadTextProvident = function (year, month) {

                var URL = CONFIG.SERVER + 'HRPY/GetTextProvidentfund';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {
                        SelectCompany: $scope.selectCompanyCode,
                        YearProvidentfund: year,
                        MonthProvidentfund: month
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    console.log(response);
                    if (response.data.length > 0) {
                        $scope.provident_text = response.data;
                        $scope.model_export.FundId = response.data[0].FundID;
                    }
                    else {
                        $scope.provident_text = [];
                        $scope.model_export.FundId = "";
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };

            $scope.exportReport = function () {

                if (!$scope.model_export.Year) {
                    return;
                }

                if (!$scope.model_export.Month) {
                    return;
                }

                if (!$scope.model_export.FundId) {
                    return;
                }

                var URL = CONFIG.SERVER + 'HRPY/ExportExcelProvidentFund';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {
                        SelectCompany: $scope.selectCompanyCode,
                        Year: $scope.model_export.Year,
                        Month: $scope.model_export.Month,
                        FundId: $scope.model_export.FundId
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                var config = {
                    responseType: "arraybuffer",
                    cache: false
                };

                var apiData = { url: "", fileName: "" };
                var now = new Date();
                var now_time = now.getUTCFullYear() + "" + now.getUTCMonth() + "" + now.getUTCDate() + "" + now.getHours() + "" + now.getUTCMinutes() + "" + now.getUTCSeconds();

                
                var sCompany = "";
                if ($scope.CompanyList.length > 0) {
                    for (var i = 0; i < $scope.CompanyList.length; i++) {
                        if ($scope.CompanyList[i].CompanyCode == $scope.selectCompanyCode) {
                            sCompany = $scope.CompanyList[i].Name;
                        }
                    }
                }

                var pCompany = "";
                if ($scope.provident_text.length > 0) {
                    for (var i = 0; i < $scope.provident_text.length; i++) {
                        if ($scope.provident_text[i].FundID == $scope.model_export.FundId) {
                            pCompany = $scope.provident_text[i].CompanyID;
                        }
                    }
                }

                apiData.fileName = sCompany + "_" + pCompany + "_" + $scope.model_export.FundId + "_" + $scope.model_export.Year + $scope.model_export.Month + ".xlsx";

                $scope.loader.enable = true;
                $http.post(URL, oRequestParameter, config)
                    .then(function successCallback(response) {

                        var blob = new Blob([response.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
                        saveAs(blob, apiData.fileName);
                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                    });

            };

        }]);
})();
