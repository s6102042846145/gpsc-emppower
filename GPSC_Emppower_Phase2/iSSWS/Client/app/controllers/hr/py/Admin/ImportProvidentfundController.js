﻿(function () {
    angular.module('ESSMobile')
        .controller('ImportProvidentfundController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', 'Upload', '$uibModal', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, Upload, $uibModal) {

            $scope.back = function () {
                var url = '/frmViewContent/919';
                $location.path(url);
            };

            // Get Config Multi Company
            $scope.isHideMultiCompany = true;
            $scope.GetConfingMultiCompany = function () {
                var URL = CONFIG.SERVER + 'Employee/GetConfigMultiCompany';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data === 1) {
                        $scope.isHideMultiCompany = false;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetConfingMultiCompany();

            $scope.profile = getToken(CONFIG.USER);
            if ($scope.profile) {

                if ($scope.profile.Language === "TH") {
                    $scope.firstText = "หน้าแรก";
                    $scope.lastText = "หน้าสุดท้าย";
                    $scope.previousText = "ก่อน";
                    $scope.nextText = "ถัดไป";
                } else {
                    $scope.firstText = "First";
                    $scope.lastText = "Last";
                    $scope.previousText = "Previous";
                    $scope.nextText = "Next";
                }

            }


            // Support Admin Multi Company
            $scope.employeeData = getToken(CONFIG.USER);
            $scope.selectCompanyCode = $scope.employeeData.CompanyCode; // Default CompanyCode GPSC
            $scope.CompanyList = [];

            // Get List Authorization Comapny In Employee Admin
            $scope.GetAuthorizationCompany = function () {
                var URL = CONFIG.SERVER + 'workflow/GetAuthorizationCompany';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.CompanyList = response.data;

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetAuthorizationCompany();

            // Change Company Reload Data Export Report
            $scope.changeCompany = function () {

            };

            $scope.fileData = "";

            $scope.list_providentfund = [];
            $scope.currentPage = 1;
            $scope.totalItem = 0;
            $scope.list_providentfund_all = [];
            $scope.model_data = [];
            $scope.disable_save = true;
            $scope.importProvident = function () {

                if (!$scope.fileData) {
                    var modalInstance = $uibModal.open({
                        animation: $scope.animationsEnabled,
                        templateUrl: 'errorImportProvident.ng-popup.html',
                        controller: 'errorImportProvidentController',
                        backdrop: 'static',
                        keyboard: false,
                        size: 'sp',
                        resolve: {
                            items: function () {
                                var data = {
                                    msg: $scope.Text['ImportProvidentfundByAdmin']['NotFundTextFile'],
                                    text: $scope.Text
                                };
                                return data;
                            }
                        }
                    });

                    return;
                }

                $scope.loader.enable = true;
                var URL = CONFIG.SERVER + 'HRPY/ImportDataProvidentfund';
                Upload.upload({
                    url: URL,
                    method: 'POST',
                    data:
                    {
                        file: $scope.fileData
                    }
                }).then(function (resp) {

                    if (resp.data.IsError == false) {
                        $scope.model_data = resp.data;
                        $scope.list_providentfund_all = angular.copy(resp.data.list_provident_detail);
                        $scope.list_providentfund = resp.data.Pagination_data;
                        $scope.totalItem = resp.data.TotalPage;

                    } else {
                        $scope.list_providentfund_all = [];
                        $scope.list_providentfund = [];
                        $scope.totalItem = 0;
                        $scope.model_data = [];
                        $scope.disable_export = true;

                        var modalInstance = $uibModal.open({
                            animation: $scope.animationsEnabled,
                            templateUrl: 'errorImportProvident.ng-popup.html',
                            controller: 'errorImportProvidentController',
                            backdrop: 'static',
                            keyboard: false,
                            size: 'sp',
                            resolve: {
                                items: function () {
                                    var data = {
                                        msg: $scope.Text['ImportProvidentfundByAdmin']['Error'],
                                        text: $scope.Text
                                    };
                                    return data;
                                }
                            }
                        });
                    }
                    $scope.loader.enable = false;
                }, function (resp) {

                    var modalInstance = $uibModal.open({
                        animation: $scope.animationsEnabled,
                        templateUrl: 'errorImportProvident.ng-popup.html',
                        controller: 'errorImportProvidentController',
                        backdrop: 'static',
                        keyboard: false,
                        size: 'sp',
                        resolve: {
                            items: function () {
                                var data = {
                                    msg: $scope.Text['ImportProvidentfundByAdmin']['Error'],
                                    text: $scope.Text
                                };
                                return data;
                            }
                        }
                    });

                    console.log('Error status: ' + resp.status);
                    $scope.loader.enable = false;
                }, function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                });

            };

            $scope.nextPage = function () {
                if ($scope.list_providentfund_all.length > 0) {

                    var URL = CONFIG.SERVER + 'HRPY/PaginationImportDataProvidentfund';
                    $scope.employeeData = getToken(CONFIG.USER);
                    var oRequestParameter = {
                        InputParameter: {
                            "Page": $scope.currentPage,
                            "ItemPerPage": 50
                        }
                        , CurrentEmployee: $scope.employeeData
                        , Requestor: $scope.requesterData
                        , Creator: getToken(CONFIG.USER)
                        , Provident_fund_all: $scope.list_providentfund_all
                    };

                    $scope.loader.enable = true;
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {

                        $scope.list_providentfund = response.data;

                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                    });
                }
            };

            $scope.disable_export = true;
            $scope.model_export = {
                Year: "",
                Month: "",
                FundId: "",
                CompanyId: "",
                FileName: ""
            };
            $scope.saveToDatabase = function () {

                var URL = CONFIG.SERVER + 'HRPY/ImportProvidentfundSaveToDatabase';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {
                        SelectCompany: $scope.selectCompanyCode
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                    , CompanyId: $scope.model_data.CompanyId
                    , FundId: $scope.model_data.FundId
                    , NavDate: $scope.model_data.NavDate
                    , NavValue: $scope.model_data.NavValue
                    , FileName: $scope.model_data.File
                    , Year: $scope.model_data.Year
                    , Month: $scope.model_data.Month
                    , Provident_fund_all: $scope.model_data.list_provident_detail
                };

                $scope.loader.enable = true;

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    //console.log(response);

                    $scope.loader.enable = false;
                    var modalInstance = $uibModal.open({
                        animation: $scope.animationsEnabled,
                        templateUrl: 'completeImportProvident.ng-popup.html',
                        controller: 'completeImportProvidentController',
                        backdrop: 'static',
                        keyboard: false,
                        size: 'sp',
                        resolve: {
                            items: function () {
                                var data = {
                                    msg: $scope.Text['ImportProvidentfundByAdmin']['Complete'],
                                    text: $scope.Text
                                };
                                return data;
                            }
                        }
                    });

                    if (response.data.PF_HeaderId > 0) {
                        $scope.disable_export = false;
                        $scope.model_export.Year = response.data.Year;
                        $scope.model_export.Month = response.data.Month;
                        $scope.model_export.FundId = response.data.FundId;
                        $scope.model_export.CompanyId = response.data.CompanyId;
                        $scope.model_export.FileName = "";
                    }
                    else {

                        $scope.disable_export = true;
                        $scope.model_export = {
                            Year: "",
                            Month: "",
                            FundId: "",
                            CompanyId: "",
                            FileName: ""
                        };
                    }


                }, function errorCallback(response) {

                    var modalInstance = $uibModal.open({
                        animation: $scope.animationsEnabled,
                        templateUrl: 'errorImportProvident.ng-popup.html',
                        controller: 'errorImportProvidentController',
                        backdrop: 'static',
                        keyboard: false,
                        size: 'sp',
                        resolve: {
                            items: function () {
                                var data = {
                                    msg: $scope.Text['ImportProvidentfundByAdmin']['Error'],
                                    text: $scope.Text
                                };
                                return data;
                            }
                        }
                    });
                    $scope.disable_export = true;

                    $scope.loader.enable = false;
                });
            };

            $scope.exportExcelProvident = function () {

                if (!$scope.model_export.Year) {
                    return;
                }

                var URL = CONFIG.SERVER + 'HRPY/ExportExcelProvidentFund';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {
                        SelectCompany: $scope.selectCompanyCode,
                        Year: $scope.model_export.Year,
                        Month: $scope.model_export.Month,
                        FundId: $scope.model_export.FundId
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                var config = {
                    responseType: "arraybuffer",
                    cache: false
                };

                var apiData = { url: "", fileName: "" };
                var now = new Date();
                var now_time = now.getUTCFullYear() + "" + now.getUTCMonth() + "" + now.getUTCDate() + "" + now.getHours() + "" + now.getUTCMinutes() + "" + now.getUTCSeconds();

                //apiData.fileName = "ReportProvidentfund" + now_time + ".xlsx";

                var sCompany = "";
                if ($scope.CompanyList.length > 0) {
                    for (var i = 0; i < $scope.CompanyList.length; i++) {
                        if ($scope.CompanyList[i].CompanyCode == $scope.selectCompanyCode) {
                            sCompany = $scope.CompanyList[i].Name;
                        }
                    }
                }

                apiData.fileName = sCompany + "_" + $scope.model_export.CompanyId + "_" + $scope.model_export.FundId + "_" + $scope.model_export.Year + $scope.model_export.Month + ".xlsx";

                $scope.loader.enable = true;
                $http.post(URL, oRequestParameter, config)
                    .then(function successCallback(response) {


                        var blob = new Blob([response.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
                        saveAs(blob, apiData.fileName);
                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                    });

            };

        }])
        .controller('errorImportProvidentController', ['items', '$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', '$uibModalInstance', function (items, $scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal, $uibModalInstance) {

            $scope.msg_result = "";
            $scope.msg_result = items.msg;
            $scope.Text = items.text;
            $scope.closeModal = function () {
                $uibModalInstance.close();
            };

        }])
        .controller('completeImportProvidentController', ['items', '$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', '$uibModalInstance', function (items, $scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal, $uibModalInstance) {

            $scope.msg_result = "";
            $scope.msg_result = items.msg;
            $scope.Text = items.text;
            $scope.closeModal = function () {
                $uibModalInstance.close();
            };

        }]);
})();
