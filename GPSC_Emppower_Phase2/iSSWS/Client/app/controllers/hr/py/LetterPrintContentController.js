﻿(function () {
    angular.module('ESSMobile')
        .controller('LetterPrintContentController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {


            function download(dataurl, filename) {
                var a = document.createElement("a");
                a.href = dataurl;
                a.setAttribute("download", filename);
                var b = document.createEvent("MouseEvents");
                b.initEvent("click", false, true);
                a.dispatchEvent(b);
                return false;
            }

            // Get Config Multi Company
            $scope.isHideMultiCompany = true;
            $scope.GetConfingMultiCompany = function () {
                var URL = CONFIG.SERVER + 'Employee/GetConfigMultiCompany';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data === 1) {
                        $scope.isHideMultiCompany = false;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetConfingMultiCompany();

            $scope.employeeData = getToken(CONFIG.USER);
            $scope.file_download = "";
            $scope.TextLetterType = $scope.Text["LETTERTYPE"];

            $scope.ddlPeriod;
            $scope.ListPeriod;
            $scope.LetterList;


            $scope.dateVariable = new Date();
            var yyyy = $scope.dateVariable.getFullYear();
            var m = $scope.dateVariable.getMonth() + 1;
            var mm = m < 10 ? '0' + m : m;
            $scope.yyyymm = yyyy.toString() + mm.toString();


            $scope.SelectCompanyCode = $scope.employeeData.CompanyCode;
            $scope.GetAuthorizationCompany = function () {
                var URL = CONFIG.SERVER + 'workflow/GetAuthorizationCompany';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.CompanyList = response.data;

                    if ($scope.CompanyList.length > 0) {
                        $scope.ddlCompany = $scope.CompanyList[0].CompanyCode;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }
            $scope.GetAuthorizationCompany();


            $scope.changeCompany = function (CompanyCode) {
                $scope.SelectCompanyCode = CompanyCode;
                GetPeriod();
            };


            $scope.init = function () {
                GetPeriod();
            };


            function GetPeriod() {
                var URL = CONFIG.SERVER + 'HRPY/GetPeriodLetter';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "SelectCompany": $scope.SelectCompanyCode }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
     
                    $scope.ListPeriod = response.data;
                    if ($scope.ListPeriod != null && $scope.ListPeriod.length > 0) {
                        $scope.ddlPeriod = $scope.ListPeriod[$scope.getIndex($scope.yyyymm)].PeriodValue;
                        GetLetterListByPeriod($scope.ddlPeriod);
                    }
                    

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }


            $scope.getIndex = function (index) {
                for (var i = 0; i < $scope.ListPeriod.length; i++) {
                    if ($scope.ListPeriod[i].PeriodValue === index)
                        return i;
                }
            };


            $scope.filterYear = function (key_year) {
                GetListPayslipPeriodSetting(key_year);
            };


            $scope.GetLetterListByPeriod = function () {
                GetLetterListByPeriod($scope.ddlPeriod);
            };


            function GetLetterListByPeriod(key_period) {

                var URL = CONFIG.SERVER + 'HRPY/GetLetterByAdmin';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "Period": key_period, "SelectCompany": $scope.SelectCompanyCode }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.LetterList = response.data;
                   
                    //if ($scope.LetterList != null && $scope.LetterList.length > 0) 
                    //{
                       
                    //}

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }

            $scope.PrintLetter = function (empIDrequestNo, type_download) {

                if (angular.isDefined(empIDrequestNo)) {
                    var URL = CONFIG.SERVER + 'HRPY/GetLetterExportForAdmin';
                    $scope.employeeData = getToken(CONFIG.USER);
                    var oRequestParameter = {
                        InputParameter:
                        {
                            "ItemKey": empIDrequestNo,
                            "TypeDoc": type_download,
                            "SelectCompany": $scope.SelectCompanyCode

                        }
                        , CurrentEmployee: $scope.employeeData
                        , Requestor: $scope.requesterData
                        , Creator: getToken(CONFIG.USER)
                    };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {

                        var arrEmpID = empIDrequestNo.split('|');

                        var file_name = "LetterReportForAdmin" + arrEmpID[0];

                        //if (arrEmpID[0] == $scope.employeeData.EmployeeID) {
                        //    file_name += ".pdf";
                        //} else if (arrEmpID[0] != $scope.employeeData.EmployeeID) {
                        //    file_name += ".xls";
                        //}

                        if (type_download === "WORD") {
                            file_name += ".doc";
                        }
                        else if (type_download === "PDF") {
                            file_name += ".pdf";
                        }
                        else if (type_download === "EXCEL") {
                            file_name += ".xls";
                        }
                        else {
                            file_name += ".pdf";
                        }

                        var url = CONFIG.SERVER + 'Client/Report/' + file_name;
                        download(url, file_name);

                        $scope.GetLetterListByPeriod();

                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                    });
                }

            };



            $scope.ViewLetter = function (requestNo) {
                if (angular.isDefined(requestNo)) {
                    var URL = CONFIG.SERVER + 'HRPY/GetLetterDocument';
                    $scope.employeeData = getToken(CONFIG.USER);
                    var oRequestParameter = {
                        InputParameter: { "RequestNo": requestNo, "SelectCompany": $scope.SelectCompanyCode}
                        , CurrentEmployee: $scope.employeeData
                        , Requestor: $scope.requesterData
                        , Creator: getToken(CONFIG.USER)
                    };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.LetterDocument = response.data;

                        if (typeof cordova != 'undefined') {
                            console.log('');
                        } else {
                            var path = CONFIG.SERVER + 'Client/index.html#!/frmViewRequest/' + requestNo + '/' + $scope.requesterData.CompanyCode + '/' + $scope.LetterDocument.__keyMaster + '/' + 'true' + '/' + 'false';
                            //$location.path('/frmViewRequest/' + requestNo + '/' + '0245' + '/' + '8122' + '/' + 'true' + '/' + 'false');
                            window.open(path, '_blank');
                        } 

                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                    });
                }
            }
        }]);
})();
