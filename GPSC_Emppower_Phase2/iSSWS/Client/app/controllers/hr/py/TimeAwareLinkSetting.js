﻿(function () {
    angular.module('ESSMobile')
        .controller('TimeAwareLinkSettingController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {

            $scope.employeeData = getToken(CONFIG.USER);
            $scope.TimeAwareLinkData_Edit = [];
            $scope.TimeAwareLinkData_New = [];
            $scope.ChkUpdate = true;

            $scope.back = function () {
                var url = '/frmViewContent/5001';
                $location.path(url);
            };

            // Get Config Multi Company
            $scope.isHideMultiCompany = true;
            $scope.GetConfingMultiCompany = function () {
                var URL = CONFIG.SERVER + 'Employee/GetConfigMultiCompany';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data === 1) {
                        $scope.isHideMultiCompany = false;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetConfingMultiCompany();


            $scope.SelectCompanyCode = $scope.employeeData.CompanyCode;


            $scope.GetAuthorizationCompany = function () {
                var URL = CONFIG.SERVER + 'workflow/GetAuthorizationCompany';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.CompanyList = response.data;

                    if ($scope.CompanyList.length > 0) {
                        $scope.ddlCompany = $scope.CompanyList[0].CompanyCode;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }
            $scope.GetAuthorizationCompany();


            $scope.changeCompany = function (CompanyCode) {
                $scope.SelectCompanyCode = CompanyCode;
                GetAllTimeAwareLink();
            };


            var tmpTimeAwareLinkData = null;


            $scope.loader.enable = true;


            $scope.init = function () {
                GetAllTimeAwareLink();
            }

            $scope.filterYear = function (key_year) {
                $scope.years = key_year;
                GetFundQuarter(key_year);
                GetPeriodSetting(key_year);
            };



            function GetAllTimeAwareLink() {
                var URL = CONFIG.SERVER + 'HRPY/GetAllTimeAwareLink';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "SelectCompany": $scope.SelectCompanyCode }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.TimeAwareLinkData_New = response.data;

                    tmpTimeAwareLinkData = JSON.parse(JSON.stringify($scope.TimeAwareLinkData_New));

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }
        

            $scope.onSubmit = function () {
                $scope.TimeAwareLinkData_Edit = [];

                for (var i = 0; i < $scope.TimeAwareLinkData_New.length; i += 1) {
                    if ($scope.TimeAwareLinkData_New[i].BeginDate != tmpTimeAwareLinkData[i].BeginDate ||
                        $scope.TimeAwareLinkData_New[i].EndDate != tmpTimeAwareLinkData[i].EndDate)
                    {
                        $scope.TimeAwareLinkData_Edit.push({
                            'LinkID': $scope.TimeAwareLinkData_New[i].LinkID,
                            'BeginDate': $scope.TimeAwareLinkData_New[i].BeginDate,
                            'EndDate': $scope.TimeAwareLinkData_New[i].EndDate,
                        });
                    }
                }        

                $scope.TimeAwareLinkData_Edit.forEach(function (v) {
                    if ($scope.ChkUpdate == true) {
                        UpdateTimeAwareLink(v.LinkID, v.BeginDate, v.EndDate);
                    }
                });

                if ($scope.ChkUpdate == true) {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popupContainer')))
                            .clickOutsideToClose(true)
                            .title('บันทึกข้อมูลสำเร็จ!')
                            .ok('ตกลง')
                    );
                }

                GetAllTimeAwareLink();
            };


            function UpdateTimeAwareLink(LinkID, BeginDate, EndDate) {
                var URL = CONFIG.SERVER + 'HRPY/UpdateTimeAwareLink';
                var oEmployeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "LinkID": LinkID, "BeginDate": BeginDate, "EndDate": EndDate, "SelectCompany": $scope.SelectCompanyCode}
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.ChkUpdate = true;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.ChkUpdate = false;
                    $scope.loader.enable = false;
                });
            }



        
            var date = new Date();
            $scope.GetDate = ('0' + date.getDate()).slice(-2) + '/' + ('0' + (date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear();
            $scope.setSelected = function (selectedDate, type, id) {
                $scope.FundPeriodSettingData.forEach(function (v) {

                    if (type == 'BeginDate') {
                        if (v.SettingID == id) v.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
                    }

                    if (type == 'EndDate') {
                        if (v.SettingID == id) v.EndDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
                    }

                    if (type == 'EffectiveDate') {
                        if (v.SettingID == id) v.EffectiveDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
                    }
                });
            };

            

            // Editor options.
            $scope.options = {
                language: 'en',
                allowedContent: true,
                entities: false
            };
        }]);
})();