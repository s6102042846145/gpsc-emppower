﻿(function () {
    angular.module('ESSMobile')
        .controller('PaySlipPeriodSettingController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal) {

            $scope.back = function () {
                var url = '/frmViewContent/5001';
                $location.path(url);
            };

            // Get Config Multi Company
            $scope.isHideMultiCompany = true;
            $scope.GetConfingMultiCompany = function () {
                var URL = CONFIG.SERVER + 'Employee/GetConfigMultiCompany';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data === 1) {
                        $scope.isHideMultiCompany = false;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetConfingMultiCompany();

            $scope.employeeData = getToken(CONFIG.USER);
            $scope.TextPeriod = $scope.Text["SYSTEM"];
            $scope.ListMonth;
            $scope.YearsList = [];

            $scope.PaySlipPeriodSetting_Delete = '';
            $scope.SavePeriodSetting;
            $scope.MsgSave = '';

            $scope.ddlYear;
            $scope.dateVariable = new Date();
            $scope.defaultYear = $scope.dateVariable.getFullYear();
            $scope.ListPeriod;

            $scope.PayslipPeriodSetting;
            $scope.ListPayslipPeriodSetting;
            $scope.SelectCompanyCode = $scope.employeeData.CompanyCode;


            $scope.GetAuthorizationCompany = function () {
                var URL = CONFIG.SERVER + 'workflow/GetAuthorizationCompany';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.CompanyList = response.data;

                    if ($scope.CompanyList.length > 0) {
                        $scope.ddlCompany = $scope.CompanyList[0].CompanyCode;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetAuthorizationCompany();


            $scope.changeCompany = function (CompanyCode) {
                $scope.SelectCompanyCode = CompanyCode;
                GetYears();
                getPaySlipPeriodSetting();
            };


            GetMonth();
            function GetMonth() {
                var URL = CONFIG.SERVER + 'HRPY/GetMonthPaySlipPeriodSetting';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "SelectCompany": $scope.SelectCompanyCode }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.ListMonth = response.data;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }

            $scope.init = function () {
                GetYears();
                getPaySlipPeriodSetting();
            };

            function findWithAttr(array, value) {
                for (var i = 0; i < array.length; i += 1) {
                    if (array[i] == value) {
                        return i;
                    }
                }
            }


            $scope.changeM = function (item) {

                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPY/SetEffectiveDateByDefaultPaymentDate';
                var oRequestParameter = {
                    InputParameter: { "Month": item.periodMonth, "Year": item.periodYear, "SelectCompany": $scope.SelectCompanyCode }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    item.effectiveDate = response.data;
                }, function errorCallback(response) {
                });
            };

            function GetYears() {
                var URL = CONFIG.SERVER + 'HRPY/GetYearPaySlipPeriodSetting';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "SelectCompany": $scope.SelectCompanyCode }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    angular.forEach(response.data, function (value, key) {
                        $scope.YearsList.push(value);
                    });
                    $scope.ddlYear = $scope.YearsList[findWithAttr($scope.YearsList, $scope.defaultYear)];

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }


            $scope.
                onLoadPaySlipPeriodSetting = function () {
                    getPaySlipPeriodSetting();
                };

            function getPaySlipPeriodSetting() {
                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPY/GetPeriodSettingAll';
                var oRequestParameter = {
                    InputParameter: { "SelectCompany": $scope.SelectCompanyCode }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.PayslipPeriodSetting = response.data;
                }, function errorCallback(response) {
                });
            }

            $scope.getIndex = function (index) {
                for (var i = 0; i < $scope.ListMonth.length; i++) {
                    if ($scope.ListMonth[i].MonthID == index)
                        return i;
                }
            };

            $scope.onSaveSetting = function () {
                $scope.MsgSave = '';
                savePaySlipPeriodSetting();
            };
            function savePaySlipPeriodSetting() {
                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPY/SavePeriodSetting';
                var oRequestParameter = {
                    InputParameter: { "DataPeriodSetting": $scope.PayslipPeriodSetting, "SelectCompany": $scope.SelectCompanyCode }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.SavePeriodSetting = response.data;

                    var modalInstance = $uibModal.open({
                        animation: $scope.animationsEnabled,
                        templateUrl: 'completeModalPincode.ng-popup.html',
                        controller: 'completeModalPincodeController',
                        backdrop: 'static',
                        keyboard: false,
                        size: 'sp',
                        resolve: {
                            items: function () {
                                var data = {
                                    msg: $scope.Text['ACCOUNT_SETTING']['SAVE_COMPLETED'],
                                    text: $scope.Text
                                };
                                return data;
                            }
                        }
                    });
                    $scope.MsgSave = 'Save Success';

                }, function errorCallback(response) {
                });
            }

            //ADD ROW DATA
            $scope.addNew = function () {
                var gen_periodID = Math.max.apply(Math, $scope.PayslipPeriodSetting.map(function (o) { return o.periodID; })) + 1;

                var dNow = $scope.ddlYear + '-01-01T00:00:00';

                $scope.PayslipPeriodSetting.push({
                    'periodID': gen_periodID,
                    'active': true,
                    'isOffCycle': false,
                    'isDraft': false,
                    'periodYear': $scope.ddlYear,
                    'periodMonth': 1,
                    //'offCycleDate': '0001-01-01T00:00:00',
                    'offCycleDate': $scope.ddlYear + '-01-01T00:00:00',
                    'effectiveDate': dNow,
                });
            };

            $scope.remove = function (select_periodID) {
                console.log(select_periodID);
                var newDataList = [];

                angular.forEach($scope.PayslipPeriodSetting, function (value) {
                    if (value.periodID != select_periodID) {
                        newDataList.push(value);
                    }

                    if (value.periodID == select_periodID) {
                        $scope.PaySlipPeriodSetting_Delete += select_periodID + ",";
                    }
                });

                $scope.PayslipPeriodSetting = newDataList;
            };


            //$scope.changeOffCycle = function (item) {

            //    console.log(item);
            //    if (item.isOffCycle) {
            //        var newDate = new Date(item.periodYear, item.periodMonth, 1);
            //        item.offCycleDate = newDate;
            //    }
            //};


            $scope.setSelected = function (selectedDate, type, id) {

                console.log(selectedDate);

                $scope.PayslipPeriodSetting.forEach(function (v) {
                    if (type == 'effectiveDate') {
                        if (v.periodID == id) v.effectiveDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
                    }

                    if (type == 'offCycleDate') {
                        if (v.periodID == id) v.offCycleDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
                    }
                });
            };

            $scope.recheckData = function (periodID, bool, periodYear) {
                if ($scope.PayslipPeriodSetting.length > 0) {

                    angular.forEach($scope.PayslipPeriodSetting, function (obj, key) {

                        if (obj.periodYear === periodYear) {
                            if (obj.periodID === periodID)
                                obj.IsEvaluationHistory = bool;
                            else
                                obj.IsEvaluationHistory = false;
                        }
                    });
                }
            };

        }]).controller('completeModalPincodeController', ['items', '$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', '$uibModalInstance', function (items, $scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal, $uibModalInstance) {

            $scope.msg_result = "";
            $scope.msg_result = items.msg;
            $scope.Text = items.text;
            $scope.closeModal = function () {
                $uibModalInstance.close();
            };

        }]);
})();
