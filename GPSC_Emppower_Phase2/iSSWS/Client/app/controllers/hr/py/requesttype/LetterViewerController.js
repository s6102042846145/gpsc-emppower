﻿(function () {
angular.module('ESSMobile')
    .controller('LetterViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {

        $scope.Textcategory = "HRLE";
        $scope.TextLetterType = $scope.Text["LETTERTYPE"];


        function download(dataurl, filename) {
            var a = document.createElement("a");
            a.href = dataurl;
            a.setAttribute("download", filename);
            var b = document.createEvent("MouseEvents");
            b.initEvent("click", false, true);
            a.dispatchEvent(b);
            return false;
        }


        $scope.EnabledVacation = false;
        $scope.EnabledLetterAdminPrint = false;
        
        $scope.ChildAction.SetData = function () {
            $scope.Letter = $scope.document.Additional.Letter;
            GetEnabledVacation();
            GetEnabledLetterAdminPrint();
        };

        $scope.ChildAction.LoadData = function () {
           
        };

        var employeeDate = getToken(CONFIG.USER);
        $scope.ChildAction.SetData();

        function GetEnabledVacation() {
            var URL = CONFIG.SERVER + 'HRPY/GetEnabledVacation';
            $scope.employeeData = getToken(CONFIG.USER);
            var oRequestParameter = {
                InputParameter: { "LetterTypeID": $scope.document.Additional.Letter[0].LetterTypeID }
                , CurrentEmployee: $scope.employeeData
                , Requestor: $scope.requesterData
                , Creator: getToken(CONFIG.USER)
            };
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                $scope.EnabledVacation = response.data;
                
                $scope.loader.enable = false;
            }, function errorCallback(response) {
                $scope.loader.enable = false;
            });
        }


        function GetEnabledLetterAdminPrint() {
            var URL = CONFIG.SERVER + 'HRPY/GetEnabledLetterAdminPrint';
            $scope.employeeData = getToken(CONFIG.USER);
            var oRequestParameter = {
                InputParameter: { "DocumentStatus": $scope.document.CurrentFlowItemCode }
                , CurrentEmployee: $scope.employeeData
                , Requestor: $scope.requesterData
                , Creator: getToken(CONFIG.USER)
            };
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                $scope.EnabledLetterAdminPrint = response.data;
                
                $scope.loader.enable = false;
            }, function errorCallback(response) {
                $scope.loader.enable = false;
            });
        }


        $scope.PrintLetter = function (empIDrequestNo, type_download) {
            if (angular.isDefined(empIDrequestNo)) {
                var URL = CONFIG.SERVER + 'HRPY/GetLetterExport';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "ItemKey": empIDrequestNo }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    var arr = empIDrequestNo.split('|');

                    //var file_name = "LetterReport" + $scope.employeeData.EmployeeID;
                    var file_name = "LetterReport" + arr[0];
                    if (type_download == "PDF") {
                        file_name += ".pdf"
                    } else if (type_download == "EXCEL") {
                        file_name += ".xls"
                    }
                    var url = CONFIG.SERVER + 'Client/Report/' + file_name;
                    download(url, file_name);

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }

        };

    }]);
})();