﻿(function () {
    angular.module('ESSMobile')
        .controller('ProvidentMemberEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {

            $scope.Textcategory = "FAMILY";
            $scope.Title;
            $scope.Gender;
            $scope.MaritalStatus;
            $scope.Nationality;
            $scope.Language;
            $scope.Religion;
            $scope.Country;
            $scope.Province;
            $scope.TitleName;
            $scope.GenderName;
            $scope.NationalityName;
            $scope.MaritalStatusName;
            $scope.ReligionName;
            $scope.CountryName;
            $scope.ProvinceName;

            $scope.AddMember = function () {

                var URL = CONFIG.SERVER + 'HRPY/GetEmptyMember';
                $http({
                    method: 'POST',
                    url: URL
                }).then(function successCallback(response) {
                    $scope.document.Additional.ProvidentFundMember.push(response.data);
                    for (var i = 0; i < $scope.document.Additional.ProvidentFundMember.length; i++) {
                        var ChildNo = ((i + 1) < 10) ? '0' + (i + 1).toString() : (i + 1).toString();
                        $scope.document.Additional.ProvidentFundMember[i].ChildNo = ChildNo;

                    }

                    $scope.selectedIndex = $scope.document.Additional.ProvidentFundMember.length - 1;
                }, function errorCallback(response) {
                    console.log('error GetEmptyMember.', response);
                });


            }


            $scope.ChildAction.SetData = function () {
                getPersonSelectData();
                //getProvinceSelectData('TH');

                if ($scope.document.Additional.ProvidentFundMember.length == 0) {
                    $scope.AddMember();
                }

            };

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                $scope.document.ErrorText = '';
                $scope.document.Additional.ErrorText = '';
                for (var i = 0; i < $scope.document.Additional.ProvidentFundMember.length; i++) {
                 
                    $scope.document.Additional.ProvidentFundMember[i].BirthDate = $filter('date')($scope.document.Additional.ProvidentFundMember[i].BirthDate, 'yyyy-MM-dd');

                }
            };

            var employeeDate = getToken(CONFIG.USER);
            $scope.ChildAction.SetData();
            //#### FRAMEWORK FUNCTION ### END

            //#### OTHERS FUNCTION ### START 
            function getPersonSelectData() {
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetPersonSelectData/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.Title = response.data.Title;
                    $scope.Gender = response.data.Gender;
                    $scope.MaritalStatus = response.data.MaritalStatus;
                    $scope.Nationality = response.data.Nationality;
                    $scope.Language = response.data.Language;
                    $scope.Religion = response.data.Religion;
                    $scope.Country = response.data.Country;

                }, function errorCallback(response) {
                    // Error
                    console.log('error getPersonSelectData.', response);
                });
            }


            //function getProvinceSelectData(Code) {
            //    var oRequestParameter = {
            //        InputParameter: { "Code": Code }
            //        , CurrentEmployee: getToken(CONFIG.USER)
            //        , Requestor: $scope.requesterData
            //    };
            //    var URL = CONFIG.SERVER + 'HRPA/GetProvinceByCountryCode';
            //    $http({
            //        method: 'POST',
            //        url: URL,
            //        data: oRequestParameter
            //    }).then(function successCallback(response) {
            //        // Success
            //        $scope.Province = response.data.Province;

              
            //    }, function errorCallback(response) {
            //        // Error
            //        console.log('error getProvinceSelectData.', response);
            //    });
            //}
          
            $scope.RemoveMember = function (item) {
                var index = $scope.document.Additional.ProvidentFundMember.indexOf(item);
                $scope.document.Additional.Check_Delete = 1;
                $scope.document.Additional.ProvidentFundMember.splice(index, 1);
                for (var i = 0; i < $scope.document.Additional.ProvidentFundMember.length; i++) {
                    var ChildNo = ((i + 1) < 10) ? '0' + (i + 1).toString() : (i + 1).toString();
                    $scope.document.Additional.ProvidentFundMember[i].ChildNo = ChildNo;
                   
                }
            }


            $scope.setSelectedDOB = function (selectedDate,item) {

                item.BirthDate = $filter('date')(selectedDate, 'yyyy-MM-dd');

            };

            
            /// pop up
            $scope.showAlert = function (ev) {
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title(ev)
                        .ok('ตกลง')
                );
            };

            /// เช็คค่า ErrorText
            $scope.$watch('document.Additional.ErrorText', function (newValue, oldValue) {

                if (newValue != null) {
                    if (newValue != '') {

                        setTimeout(function () {
                            $scope.showAlert(newValue);
                        }, 300);

                    }
                }

            });


            //#### OTHERS FUNCTION ### END
        }]);
})();