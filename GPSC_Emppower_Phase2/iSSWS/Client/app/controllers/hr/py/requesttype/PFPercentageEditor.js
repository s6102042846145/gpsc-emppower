﻿(function () {
    angular.module('ESSMobile')
        .controller('PFPercentageEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {
            //#### FRAMEWORK FUNCTION ### START

            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
            $scope.ChildAction.SetData = function () {

            };

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                $scope.document.ErrorText = '';
                $scope.document.Additional.PF_ProvidentFundLog.ErrorText = '';
            };

            var employeeDate = getToken(CONFIG.USER);
            $scope.ChildAction.SetData();
            //#### FRAMEWORK FUNCTION ### END

            //#### OTHERS FUNCTION ### START 

            /// pop up
            $scope.showAlert = function (ev) {
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title(ev)
                        .ok('ตกลง')
                );
            };

            /// เช็คค่า ErrorText
            $scope.$watch('document.Additional.PF_ProvidentFundLog.ErrorText', function (newValue, oldValue) {

                if (newValue != null) {
                    if (newValue != '') {

                        setTimeout(function () {
                            $scope.showAlert(newValue);
                        }, 300);

                    }
                }

            });

            //#### OTHERS FUNCTION ### END
        }]);
})();