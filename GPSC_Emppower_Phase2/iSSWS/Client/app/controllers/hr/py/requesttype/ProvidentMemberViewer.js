﻿(function () {
    angular.module('ESSMobile')
        .controller('ProvidentMemberViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {
            //#### FRAMEWORK FUNCTION ### START
            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
            $scope.ChildAction.SetData = function () {
                //Do something ...
            };

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                //Do something ...
            };

            $scope.Textcategory = "FAMILY";
            $scope.PROVIDENTFUND = $scope.Text["PROVIDENTFUND"];
            $scope.COUNTRY = $scope.Text["COUNTRY"];
            $scope.NATIONALITY = $scope.Text["NATIONALITY"];
            $scope.TITLENAME = $scope.Text["TITLENAME"];
            $scope.content.isShowHeader = true;
            $scope.TEXTProvidentFundDetail = [];
            $scope.FUNDPATTERN;

            var employeeDate = getToken(CONFIG.USER);
            $scope.ChildAction.SetData();


            $scope.init = function () {
                getProvidentMemberData();
            }

            function getProvidentMemberData() {
                var oEmployeeData = getToken(CONFIG.USER);
                var checkDate = $filter('date')(new Date(), 'yyyy-MM-dd');
                var URL = CONFIG.SERVER + 'HRPY/GetProvidentMemberData';
                var oRequestParameter = {
                    InputParameter: { "EmployeeID": oEmployeeData.EmployeeID }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                    //data: getToken(CONFIG.USER)
                }).then(function successCallback(response) {
                    // Success
                    $scope.ProvidentMemberList = response.data;
                    //$scope.DictFileAttachmentList = response.data.dictFileAttachmentList;
                }, function errorCallback(response) {
                });
            }
        }]);
})();