﻿(function () {
    angular.module('ESSMobile')
        .controller('LetterEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {

           
            $scope.Textcategory = "HRLE";
            $scope.EnabledVacation = false;
            $scope.TextLetterType = $scope.Text["LETTERTYPE"];

            $scope.ChildAction.SetData = function () {
                GetLetterSelectData();
                GetEnabledVacation();

            };

            $scope.ChildAction.LoadData = function () {

            };

            var employeeDate = getToken(CONFIG.USER);
            $scope.ChildAction.SetData();


            function getWithAttr(array, attr, value) {
                var objReturn = [];
                for (var i = 0; i < array.length; i += 1) {
                    if (array[i][attr] === value) {
                        objReturn.push(array[i]);
                    }
                }
                return objReturn;
            }

            function findWithAttr(array, attr, value) {
                for (var i = 0; i < array.length; i += 1) {
                    if (array[i][attr] === value) {
                        return i;
                    }
                }
            }

            function checkWithValue(array, attr, value) {
                var result = false;
                for (var i = 0; i < array.length; i += 1) {
                    if (array[i][attr] === value) {
                        result = true;
                        break;
                    }
                }
                return result;
            }

            // โชว์ประเทศที่ 2 และ 3
            $scope.isshowCountry2 = false;
            $scope.isshowCountry3 = false;
            $scope.addCountry = function () {

                if ($scope.isshowCountry2 === false) {
                    $scope.isshowCountry2 = true;
                    $scope.document.Additional.Letter[0].IsUseEmbassy2 = true;
                }
                else if ($scope.isshowCountry3 === false) {
                    $scope.isshowCountry3 = true;
                    $scope.document.Additional.Letter[0].IsUseEmbassy3 = true;
                }
            };

            $scope.deleteAddCountry = function (row_delete) {
                if (row_delete === 2) {

                    if ($scope.isshowCountry3 === true) {
                        $scope.isshowCountry3 = false;
                        $scope.document.Additional.Letter[0].EmbassyID3 = "0";
                        $scope.document.Additional.Letter[0].EmbassyName3 = "-- กรุณาเลือก --";
                        $scope.document.Additional.Letter[0].IsUseEmbassy3 = false;
                    }
                    else {
                        $scope.isshowCountry2 = false;
                        $scope.document.Additional.Letter[0].EmbassyID2 = "0";
                        $scope.document.Additional.Letter[0].EmbassyName2 = "-- กรุณาเลือก --";
                        $scope.document.Additional.Letter[0].IsUseEmbassy2 = false;
                    }
                }
                if (row_delete === 3) {
                    $scope.isshowCountry3 = false;
                    $scope.document.Additional.Letter[0].EmbassyID3 = "0";
                    $scope.document.Additional.Letter[0].EmbassyName3 = "-- กรุณาเลือก --";
                    $scope.document.Additional.Letter[0].IsUseEmbassy3 = false;
                }

            };

            $scope.clearControlCertificate = function () {
                $scope.document.Additional.Letter[0].EmbassyID = "0";
                $scope.document.Additional.Letter[0].EmbassyName = "-- กรุณาเลือก --";

                $scope.document.Additional.Letter[0].EmbassyID2 = "0";
                $scope.document.Additional.Letter[0].EmbassyName2 = "-- กรุณาเลือก --";
                $scope.document.Additional.Letter[0].IsUseEmbassy2 = false;
                $scope.isshowCountry2 = false;
                
                $scope.document.Additional.Letter[0].EmbassyID3 = "0";
                $scope.document.Additional.Letter[0].EmbassyName3 = "-- กรุณาเลือก --";
                $scope.document.Additional.Letter[0].IsUseEmbassy3 = false;
                $scope.isshowCountry3 = false;
                
            };


            function GetLetterSelectData() {
                var oEmployeeData = getToken(CONFIG.USER);
                var checkDate = $filter('date')(new Date(), 'yyyy-MM-dd');
                var URL = CONFIG.SERVER + 'HRPY/GetLetterSelectData';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter

                }).then(function successCallback(response) {

                    $scope.LetterTypeList = response.data.LetterType;

                    console.log(response.data);

                    if ($scope.LetterTypeList != null && $scope.LetterTypeList.length > 0) {
                        if ($scope.document.Additional.Letter[0].LetterTypeID == undefined || $scope.document.Additional.Letter[0].LetterTypeID == -1) {

                            $scope.document.Additional.Letter[0].LetterTypeID = $scope.LetterTypeList[0];
                            $scope.document.Additional.HeaderInfo[0].LetterTypeName = $scope.TextLetterType[$scope.LetterTypeList[0]];

                        }
                        GetSignByManagerList();
                    }

                    $scope.LetterEmbassyList = response.data.LetterEmb;
                    //if ($scope.LetterEmbassyList != null && $scope.LetterEmbassyList.length > 0) {
                    //    if ($scope.document.Additional.Letter[0].EmbassyID == undefined || $scope.document.Additional.Letter[0].EmbassyID == -1) {
                    //        $scope.document.Additional.Letter[0].EmbassyID = $scope.LetterEmbassyList[0].EmbassyID;
                    //    }
                    //}


                }, function errorCallback(response) {
                });
            }

            $scope.ChangeLetterType = function () {
                $scope.document.Additional.HeaderInfo[0].LetterTypeName = '';
                $scope.document.Additional.HeaderInfo[0].LetterTypeName = $scope.TextLetterType[$scope.document.Additional.Letter[0].LetterTypeID];

                $scope.document.Additional.Letter[0].SignID = '';
                $scope.document.Additional.Letter[0].SignName = '';

                $scope.SignByManagerList = null;
                $scope.EnabledVacation = false;
                GetSignByManagerList();
                GetEnabledVacation();
                // เพิ่มเติม Clear Control ของหนังสือรับรอง
                $scope.clearControlCertificate();
            };

            $scope.ChangeSignByManager = function () {
                if (checkWithValue($scope.SignByManagerList, 'SignValue', $scope.document.Additional.Letter[0].SignID)) {
                    $scope.document.Additional.Letter[0].SignName = $scope.SignByManagerList[findWithAttr($scope.SignByManagerList, 'SignValue', $scope.document.Additional.Letter[0].SignID)].SignText;
                }
            };

            $scope.ChangeEmbassy = function () {
                if ($scope.LetterEmbassyList != null && $scope.LetterEmbassyList.length > 0) {
                    $scope.document.Additional.Letter[0].EmbassyName = $scope.LetterEmbassyList[findWithAttr($scope.LetterEmbassyList, 'EmbassyID', $scope.document.Additional.Letter[0].EmbassyID)].EmbassyName;
                }
            };

            $scope.ChangeEmbassy2 = function () {
                if ($scope.LetterEmbassyList != null && $scope.LetterEmbassyList.length > 0) {
                    $scope.document.Additional.Letter[0].EmbassyName2 = $scope.LetterEmbassyList[findWithAttr($scope.LetterEmbassyList, 'EmbassyID', $scope.document.Additional.Letter[0].EmbassyID2)].EmbassyName;
                }
            };

            $scope.ChangeEmbassy3 = function () {
                if ($scope.LetterEmbassyList != null && $scope.LetterEmbassyList.length > 0) {
                    $scope.document.Additional.Letter[0].EmbassyName3 = $scope.LetterEmbassyList[findWithAttr($scope.LetterEmbassyList, 'EmbassyID', $scope.document.Additional.Letter[0].EmbassyID3)].EmbassyName;
                }
            };

            function GetSignByManagerList() {
                var URL = CONFIG.SERVER + 'HRPY/GetSignByManager';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "LetterTypeID": $scope.document.Additional.Letter[0].LetterTypeID }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.SignByManagerList = response.data;

                    if ($scope.SignByManagerList != null && $scope.SignByManagerList.length > 0) {
                        if ($scope.document.Additional.Letter[0].SignID == undefined || $scope.document.Additional.Letter[0].SignID == '') {
                            $scope.document.Additional.Letter[0].SignID = $scope.SignByManagerList[0].SignValue;
                            $scope.document.Additional.Letter[0].SignName = $scope.SignByManagerList[0].SignText;
                        }
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }

            function GetEnabledVacation() {
                var URL = CONFIG.SERVER + 'HRPY/GetEnabledVacation';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "LetterTypeID": $scope.document.Additional.Letter[0].LetterTypeID }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.EnabledVacation = response.data;
                    if (!$scope.EnabledVacation) {
                        $scope.document.Additional.Letter[0].EmbassyID = -1;
                        $scope.document.Additional.Letter[0].EmbassyName = '';

                        $scope.document.Additional.Letter[0].EmbassyID2 = -1;
                        $scope.document.Additional.Letter[0].EmbassyName2 = '';

                        $scope.document.Additional.Letter[0].EmbassyID3 = -1;
                        $scope.document.Additional.Letter[0].EmbassyName3 = '';
                    }
                    else {

                        if ($scope.LetterEmbassyList != null && $scope.LetterEmbassyList.length > 0) {
                            if ($scope.document.Additional.Letter[0].EmbassyID == undefined || $scope.document.Additional.Letter[0].EmbassyID == -1) {
                                $scope.document.Additional.Letter[0].EmbassyID = $scope.LetterEmbassyList[0].EmbassyID;
                                $scope.document.Additional.Letter[0].EmbassyName = $scope.LetterEmbassyList[0].EmbassyName;

                                $scope.document.Additional.Letter[0].EmbassyID2 = $scope.LetterEmbassyList[0].EmbassyID;
                                $scope.document.Additional.Letter[0].EmbassyName2 = $scope.LetterEmbassyList[0].EmbassyName;

                                $scope.document.Additional.Letter[0].EmbassyID3 = $scope.LetterEmbassyList[0].EmbassyID;
                                $scope.document.Additional.Letter[0].EmbassyName3 = $scope.LetterEmbassyList[0].EmbassyName;
                            }
                            else {

                            }
                        }
                    }
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }

            $scope.setSelectedBeginDate = function (selectedDate) {
                $scope.document.Additional.Letter[0].VacationBegin = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };
            $scope.setSelectedEndDate = function (selectedDate) {
                $scope.document.Additional.Letter[0].VacationEnd = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };

            $scope.setSelectedBeginDate2 = function (selectedDate) {
                $scope.document.Additional.Letter[0].VacationBegin2 = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };
            $scope.setSelectedEndDate2 = function (selectedDate) {
                $scope.document.Additional.Letter[0].VacationEnd2 = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };

            $scope.setSelectedBeginDate3 = function (selectedDate) {
                $scope.document.Additional.Letter[0].VacationBegin3 = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };
            $scope.setSelectedEndDate3 = function (selectedDate) {
                $scope.document.Additional.Letter[0].VacationEnd3 = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };

            $scope.validateDate = function () {

                


            };

        }]);
})();