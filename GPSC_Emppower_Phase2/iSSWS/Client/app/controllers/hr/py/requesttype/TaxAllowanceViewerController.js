﻿(function () {
angular.module('ESSMobile')
    .controller('TaxAllowanceViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {
        //#### FRAMEWORK FUNCTION ### START
        $scope.Textcategory = "TAXALLOWANCE";
        $scope.ClaimType;
        $scope.ClaimTypeName;
        $scope.ClaimTypeNameOld;

        //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
        $scope.ChildAction.SetData = function () {
            //Do something ...
            $scope.TaxAllowance = $scope.document.Additional.TaxAllowance;
            $scope.TaxAllowanceOld = $scope.document.Additional.TaxAllowanceOld;
            getTaxAllowanceEditData();
        };

        //LoadData Function : Use to add some logic to Additional dataset before take any action
        $scope.ChildAction.LoadData = function () {
            //Do something ...
            
        };

        var employeeDate = getToken(CONFIG.USER);
        $scope.ChildAction.SetData();
        //#### FRAMEWORK FUNCTION ### END

        //#### OTHERS FUNCTION ### START

        function findWithAttr(array, attr, value) {
            for (var i = 0; i < array.length; i += 1) {
                if (array[i][attr] == value) {
                    return i;
                }
            }
        }

        function getTaxAllowanceEditData() {
            var oRequestParameter = {
                InputParameter: {}
                , CurrentEmployee: getToken(CONFIG.USER)
            };
            var URL = CONFIG.SERVER + 'HRPY/GetTaxAllowanceEditData';
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                $scope.ClaimType = response.data;


                if ($scope.ClaimType != 'null' && $scope.ClaimType.length > 0) {
                    //New data
                    if ($scope.document.Additional.TaxAllowance.SpouseAllowance == undefined || $scope.document.Additional.TaxAllowance.SpouseAllowance == 'null') {
                        $scope.document.Additional.TaxAllowance.SpouseAllowance = $scope.ClaimType[0].Key;
                        $scope.ClaimTypeName = $scope.ClaimType[0].Description;
                    }
                    else {
                        $scope.ClaimTypeName = $scope.ClaimType[findWithAttr($scope.ClaimType, 'Key', $scope.document.Additional.TaxAllowance.SpouseAllowance)].Description;
                    }
                    //Old data
                    if ($scope.document.Additional.TaxAllowanceOld.SpouseAllowance == undefined || $scope.document.Additional.TaxAllowanceOld.SpouseAllowance == 'null') {
                        $scope.document.Additional.TaxAllowanceOld.SpouseAllowance = $scope.ClaimType[0].Key;
                        $scope.ClaimTypeNameOld = $scope.ClaimType[0].Description;
                    }
                    else {
                        $scope.ClaimTypeNameOld = $scope.ClaimType[findWithAttr($scope.ClaimType, 'Key', $scope.document.Additional.TaxAllowanceOld.SpouseAllowance)].Description;
                    }
                }


            }, function errorCallback(response) {
                // Error
                console.log('error RequestController.', response);
            });
        }

        //#### OTHERS FUNCTION ### END
    }]);
})();