﻿(function () {
    angular.module('ESSMobile')
        .controller('ProvidentFundSettingController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {


            // Get Config Multi Company
            $scope.isHideMultiCompany = true;
            $scope.GetConfingMultiCompany = function () {
                var URL = CONFIG.SERVER + 'Employee/GetConfigMultiCompany';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data === 1) {
                        $scope.isHideMultiCompany = false;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetConfingMultiCompany();

            $scope.back = function () {
                var url = '/frmViewContent/5001';
                $location.path(url);
            };

            $scope.employeeData = getToken(CONFIG.USER);
            $scope.FundQuarterData = [];
            $scope.FundPeriodSettingData = [];
            $scope.YearsList = [];
            $scope.years = '';
            $scope.dateVariable = new Date();
            $scope.loader.enable = true;
            $scope.years = $scope.dateVariable.getFullYear();
            $scope.prop = {
                "type": "select",
                "name": $scope.years,
                "value": $scope.years,
                "values": $scope.YearsList
            };
            $scope.inputs = [];
            $scope.value_data = '';
            $scope.PeriodSettingID_Delete = '';
            $scope.HtmlEditor = '';
            $scope.num = 0;
            $scope.SelectCompanyCode = $scope.employeeData.CompanyCode;

            $scope.msgErrorValidate2_1 = $scope.Text["PROVIDENTFUND"].STARTDATE;
            $scope.msgErrorValidate2_2 = $scope.Text["PROVIDENTFUND"].VALIDATE_DATE;

            $scope.msgErrorValidate3_1 = $scope.Text["PROVIDENTFUND"].EFFECTIVEDATE;
            $scope.msgErrorValidate3_2 = $scope.Text["PROVIDENTFUND"].VALIDATE_EFFECTIVEDATE;


            $scope.GetAuthorizationCompany = function () {
                var URL = CONFIG.SERVER + 'workflow/GetAuthorizationCompany';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.CompanyList = response.data;

                    if ($scope.CompanyList.length > 0) {
                        $scope.ddlCompany = $scope.CompanyList[0].CompanyCode;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetAuthorizationCompany();


            $scope.changeCompany = function (CompanyCode) {
                $scope.SelectCompanyCode = CompanyCode;
                GetYears();
                GetFundQuarter($scope.years);
                GetPeriodSetting($scope.years);
                getTextProvidentFundDetail();
            };

            //Start
            $scope.init = function () {
                //TAB1
                GetYears();
                GetFundQuarter($scope.years);

                //TAB2
                GetPeriodSetting($scope.years);

                //TAB3
                getTextProvidentFundDetail();
            };

            $scope.filterYear = function (key_year) {
                $scope.years = key_year;
                GetFundQuarter(key_year);
                GetPeriodSetting(key_year);
            };


            //------------------ START TAB1-------------------------------------------------------
            //GetYears
            function GetYears() {

                var URL = CONFIG.SERVER + 'HRBE/GetYearsAdminSetting';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "SelectCompany": $scope.SelectCompanyCode }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.YearsList.splice(0, $scope.YearsList.length);
                    angular.forEach(response.data, function (value, key) {
                        $scope.YearsList.push(value.YearName);
                    });

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }

            //GetFundQuarter
            function GetFundQuarter(key_year) {

                var URL = CONFIG.SERVER + 'HRPY/GetFundQuarter';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "KeyType": "GETGRAPH", "KeyYear": key_year, "KeyValue": "", "SelectCompany": $scope.SelectCompanyCode }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.FundQuarterData = response.data;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }

            //SaveFundQuarter
            function SaveFundQuarter(key_year, key_value) {

                var URL = CONFIG.SERVER + 'HRPY/GetFundQuarter';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "KeyType": "MODIFYGRAPH", "KeyYear": key_year, "KeyValue": key_value, "SelectCompany": $scope.SelectCompanyCode  }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popupContainer')))
                            .clickOutsideToClose(true)
                            .title('บันทึกข้อมูลสำเร็จ!')
                            .ok('ตกลง')
                    );

                    $scope.loader.enable = false;
                    }, function errorCallback(response) {

                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.querySelector('#popupContainer')))
                                .clickOutsideToClose(true)
                                .title('บันทึกข้อมูลไม่สำเร็จ กรุณาลองใหม่อีกครั้ง!')
                                .ok('ตกลง')
                        );

                    $scope.loader.enable = false;
                });
            }

             //ON CLICK SEND DATA
            $scope.onSubmit = function () {

                var save_val = '';

                //UPDATE 
                angular.forEach($scope.FundQuarterData, function (value, key) {

                    if (value.FundID) {
                        save_val += value.FundID + "|";
                    }

                    if (value.Quarter1 || value.Quarter1 == 0 || value.Quarter1 == '') {
                        if (value.Quarter1 == 0 || value.Quarter1 == '') {
                            save_val += "N" + "|";
                        } else {
                            save_val += value.Quarter1 + "|";
                        }
                    }

                    if (value.Quarter2 || value.Quarter2 == 0 || value.Quarter2 == '') {
                        if (value.Quarter2 == 0 || value.Quarter2 == '') {
                            save_val += "N" + "|";
                        } else {
                            save_val += value.Quarter2 + "|";
                        }
                    }

                    if (value.Quarter3 || value.Quarter3 == 0 || value.Quarter3 == '') {

                        if (value.Quarter3 == 0 || value.Quarter3 == '') {
                            save_val += "N" + "|";
                        } else {
                            save_val += value.Quarter3 + "|";
                        }

                    }

                    if (value.Quarter4 || value.Quarter4 == 0 || value.Quarter4 == '') {
                        if (value.Quarter4 == 0 || value.Quarter4 == '') {
                            save_val += "N" + "|,";
                        } else {
                            save_val += value.Quarter4 + "|,";
                        }
                    }
                });

                //console.log($scope.years, save_val);
                SaveFundQuarter($scope.years, save_val)
            };

            //------------------ END TAB1---------------------------------------------------------





            //------------------ START TAB2 ------------------------------------------------------
            //FUNCTION GET DATA
            function GetPeriodSetting(key_year) {
                var URL = CONFIG.SERVER + 'HRPY/GetPeriodSetting';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "KeyType": "GETDATA", "KeyYear": key_year, "KeyValue": "", "SelectCompany": $scope.SelectCompanyCode }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.FundPeriodSettingData = response.data;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }

            //FUNCTION DATETE AND MODIFY
            function SavePeriodSetting(key_year, key_value) {

                var URL = CONFIG.SERVER + 'HRPY/GetPeriodSetting';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "KeyType": "SAVEDATA", "KeyYear": key_year, "KeyValue": key_value, "SelectCompany": $scope.SelectCompanyCode }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popupContainer')))
                            .clickOutsideToClose(true)
                            .title('บันทึกข้อมูลสำเร็จ!')
                            .ok('ตกลง')
                    );

                    $scope.loader.enable = false;
                    }, function errorCallback(response) {

                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.querySelector('#popupContainer')))
                                .clickOutsideToClose(true)
                                .title('บันทึกข้อมูลไม่สำเร็จ กรุณาลองใหม่อีกครั้ง!')
                                .ok('ตกลง')
                        );

                    $scope.loader.enable = false;
                });
            }

            //ADD ROW DATA
            $scope.addNew = function () {
                var gen_id = "INSERT_" + $scope.num++;

                $scope.FundPeriodSettingData.push({
                    'SettingID': gen_id,
                    'BeginDate': "",
                    'EndDate': "",
                    'EffectiveDate': "",
                    'EnableOption': 0,
                    'EnablePercentage': 0,
                });
            };

            //DELETE DATA
            $scope.remove = function (select_delete) {
                var newDataList = [];

                angular.forEach($scope.FundPeriodSettingData, function (value) {
                    if (value.SettingID != select_delete) {
                        newDataList.push(value);
                    }

                    if (value.SettingID == select_delete) {
                          $scope.PeriodSettingID_Delete += select_delete + ",";
                    }
                });

                $scope.FundPeriodSettingData = newDataList;
            };

            //ON CLICK SEND DATA
            $scope.onSubmitPeriodSetting = function () {
            
                var save_val = '';
                var Checkdate = 0;
                var tmpEndDate;
                var sErrorBeginDate = '';
                var sErrorEndDate = '';
                var tmpEffectiveDate;
                var sErrorBelowEffectiveDate = '';
                var sErrorTopEffectiveDate = '';

                //UPDATE & ADD
                angular.forEach($scope.FundPeriodSettingData, function (value, key) {

                    if (value.SettingID) {
                        save_val += value.SettingID + "|";
                    }

                    if (value.BeginDate) {
                        save_val += $filter('date')(value.BeginDate, 'yyyy-MM-dd') + "|";
                    }

                    if (value.EndDate) {
                        save_val += $filter('date')(value.EndDate, 'yyyy-MM-dd')  + "|";
                    }

                    if (value.EffectiveDate) {
                        save_val += $filter('date')(value.EffectiveDate, 'yyyy-MM-dd') + "|";
                    }

                    if (value.EnableOption || value.EnableOption == 0) {
                        save_val += value.EnableOption + "|";
                    }

                    if (value.EnablePercentage || value.EnablePercentage == 0) {
                        save_val += value.EnablePercentage + "|,";
                    }

                    if (new Date(value.BeginDate) > new Date(value.EndDate)) {
                        Checkdate = 1;
                        sErrorBeginDate = $filter('date')(value.BeginDate, 'dd/MM/yyyy');
                        sErrorEndDate = $filter('date')(value.EndDate, 'dd/MM/yyyy');
                    }

                    if (new Date(value.BeginDate) <= new Date(tmpEndDate)) {
                        Checkdate = 2;
                        sErrorBeginDate = $filter('date')(value.BeginDate, 'dd/MM/yyyy');
                        sErrorEndDate = $filter('date')(tmpEndDate, 'dd/MM/yyyy');
                    }

                    if (new Date($filter('date')(value.EffectiveDate, 'yyyy-MM-dd')) < new Date(tmpEffectiveDate)) {
                        Checkdate = 3;
                        sErrorBelowEffectiveDate = $filter('date')(tmpEffectiveDate, 'dd/MM/yyyy');
                        sErrorTopEffectiveDate = $filter('date')(value.EffectiveDate, 'dd/MM/yyyy');
                    }

                    tmpEndDate = $filter('date')(value.EndDate, 'yyyy-MM-dd');
                    tmpEffectiveDate = $filter('date')(value.EffectiveDate, 'yyyy-MM-dd');
                });

                if (Checkdate == 1) {

                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popupContainer')))
                            .clickOutsideToClose(true)
                            .title('วันที่เริ่มต้น ' + sErrorBeginDate + ' ต้องน้อยกว่าวันที่สิ้นสุด ' + sErrorEndDate)
                            .ok('ตกลง')
                    );

                    return false;
                }
                else if (Checkdate == 2) {

                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popupContainer')))
                            .clickOutsideToClose(true)
                            .title($scope.msgErrorValidate2_1 + ' ' + sErrorBeginDate + ' ' + $scope.msgErrorValidate2_2)
                            .ok('ตกลง')
                    );

                    return false;
                }
                else if (Checkdate == 3) {

                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popupContainer')))
                            .clickOutsideToClose(true)
                            .title($scope.msgErrorValidate3_1 + ' ' + sErrorTopEffectiveDate + ' ' + $scope.msgErrorValidate3_2 + ' ' + sErrorBelowEffectiveDate)
                            .ok('ตกลง')
                    );

                    return false;
                }
                else {
                    //console.log($scope.years, save_val);
                    SavePeriodSetting($scope.years, save_val)
                }
            }

            //MODIFY DATE
            var date = new Date();
            $scope.GetDate = ('0' + date.getDate()).slice(-2) + '/' + ('0' + (date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear();
            $scope.setSelected = function (selectedDate, type, id) {
                $scope.FundPeriodSettingData.forEach(function (v) {

                    if (type == 'BeginDate') {
                        if (v.SettingID == id) v.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
                    }

                    if (type == 'EndDate') {
                        if (v.SettingID == id) v.EndDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
                    }

                    if (type == 'EffectiveDate') {
                        if (v.SettingID == id) v.EffectiveDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
                    }
                });
            };
            //------------------ END TAB2---------------------------------------------------------





            //------------------ START TAB3-------------------------------------------------------
            function getTextProvidentFundDetail() {
                var URL = CONFIG.SERVER + 'HRPY/GetTextProvidentFundDetail';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "KeyType": "GETDATA", "KeyCode": "TEXTINFO", "KeyValue": "", "SelectCompany": $scope.SelectCompanyCode }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    angular.forEach(response.data, function (value, key) {
                        if (value.Value) {
                            $scope.HtmlEditor = value.Value;
                        }
                    });

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }

            $scope.onSubmitTextEditor = function () {
                var URL = CONFIG.SERVER + 'HRPY/GetTextProvidentFundDetail';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "KeyType": "SAVEDATA", "KeyCode": "TEXTINFO", "KeyValue": $scope.HtmlEditor, "SelectCompany": $scope.SelectCompanyCode }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popupContainer')))
                            .clickOutsideToClose(true)
                            .title('บันทึกข้อมูลสำเร็จ!')
                            .ok('ตกลง')
                    );

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }


            CKEDITOR.on('instanceReady', function (evt) {
                evt.editor.dataProcessor.htmlFilter.addRules({
                    elements: {
                        img: function (el) {
                            el.addClass('img_i1');
                        }
                    }
                });
            });


            // Editor options.
            $scope.options = {
                language: 'en',
                allowedContent: true,
                entities: false
            };

            //------------------ END TAB3---------------------------------------------------------
        }]);
})();