﻿(function () {
    angular.module('ESSMobile')
        .controller('CreatePINCodeSettingController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {

            $scope.isValidate = false;
            $scope.isValidateTicket = false;
            $scope.MsgValidate = '';
            $scope.MsgChange = '';
            $scope.isCreatePinCode;

            $scope.ParamTicketClass = 'NEWPIN';
            //$scope.ParamTicketID = $routeParams.TicketID;
            $scope.ParamTicketID = $routeParams.companycode;

            $scope.init = function () {
                getValidateTicket();
            }

            $scope.onCreatePinCode = function () {
                $scope.MsgChange = '';
                $scope.MsgValidate = '';
                getValidatePinCodePolicy();
            };

            function getValidatePinCodePolicy() {
                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPY/ValidateNewPinCode';
                var oRequestParameter = {
                    InputParameter: { "NewPinCode": $scope.SetNewPinCode, "ConfirmNewPinCode": $scope.ConfirmSetNewPinCode }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.isValidateNewPIN = response.data;

                    if ($scope.isValidateNewPIN[0].RESULT) {
                        $scope.isValidate == $scope.isValidateNewPIN[0].RESULT;
                        $scope.MsgValidate = '';
                        getCreateNEWPin();
                    }
                    else {
                        $scope.isValidate == $scope.isValidateNewPIN[0].RESULT;
                        $scope.MsgValidate = $scope.isValidateNewPIN[0].MSG_RESULT;
                    }
                }, function errorCallback(response) {
                });
            }

            function getValidateTicket() {
                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPY/ValidateTicket';
                var oRequestParameter = {
                    InputParameter: { "TicketClass": $scope.ParamTicketClass, "TicketID": $scope.ParamTicketID }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.isValidateTicket = response.data;

                }, function errorCallback(response) {
                });
            }


            function getCreateNEWPin() {
                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPY/CreateNEWPin';
                var oRequestParameter = {
                    InputParameter: { "TicketID": $scope.ParamTicketID, "NewPinCode": $scope.SetNewPinCode }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.isCreatePinCode = response.data;
                    $scope.MsgChange = 'Create Success';
                }, function errorCallback(response) {
                    $scope.MsgValidate = 'TICKETINVALID';
                });
            }


        }]);
})();
