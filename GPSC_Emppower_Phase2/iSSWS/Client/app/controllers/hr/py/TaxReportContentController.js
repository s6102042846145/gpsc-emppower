﻿(function () {
    angular.module('ESSMobile')
        .controller('TaxReportContentController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal) {

            $scope.OldPinCode = "";
            $scope.countIncorrectPinCode = 0;

            function download(dataurl, filename) {
                var a = document.createElement("a");
                a.href = dataurl;
                a.setAttribute("download", filename);
                var b = document.createEvent("MouseEvents");
                b.initEvent("click", false, true);
                a.dispatchEvent(b);
                return false;
            }

            $scope.countIncorrectPinCode = 0;
            $scope.file_download = "";

            $scope.TextMonth = $scope.Text["SYSTEM"];
            $scope.YearsList = [];

            $scope.ddlYear = '';
            $scope.ddlPeriod;
            $scope.ListPeriod;
            $scope.TaxReportData = [];
            $scope.PayslipPeriodSetting;
            $scope.ListPayslipPeriodSetting;
            $scope.MsgValidate = '';

            $scope.OldPinCode = '';
            $scope.isValidate = false;


            $scope.init = function () {
                GetYears();
            };

            function GetYears() {
                var URL = CONFIG.SERVER + 'HRPY/GetYearTavi50';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    angular.forEach(response.data, function (value, key) {
                        $scope.YearsList.push(value);
                    });
                    $scope.ddlYear = $scope.YearsList[0];

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }


            $scope.zeroPad = function (num) {
                return num.toString().padStart(3, "0");
            };

            function getTaxReport(pincode) {

                $scope.OldPinCode = pincode;
                $scope.TaxReportData = [];

                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPY/GetTaxDataV2';
                var oRequestParameter = {
                    InputParameter: { "Year": $scope.ddlYear, "PinCode": $scope.OldPinCode }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.TaxReportData = response.data;

                    //if ($scope.TaxReportData.length == 0) {
                    //    if ($scope.countIncorrectPinCode > 2) {
                    //        $scope.alertPin();
                    //    }

                    //    $scope.countIncorrectPinCode += 1;
                    //    $scope.MsgValidate = 'กรุณาใส่ PINCODE ให้ถูกต้อง';
                    //}

                    //if (response.data.length <= 0) {

                    //    var modalInstance = $uibModal.open({
                    //        animation: $scope.animationsEnabled,
                    //        templateUrl: 'alertModalNoData.ng-popup.html',
                    //        controller: 'alertModalNoDataController',
                    //        backdrop: 'static',
                    //        keyboard: false,
                    //        size: 'sp',
                    //        resolve: {
                    //            items: function () {
                    //                var data = {
                    //                    requestorData: $scope.requesterData,
                    //                    text: $scope.Text,
                    //                    loadtaxReport: function (pincode) {
                    //                        getTaxReport(pincode);
                    //                    },
                    //                    fnForgetPassword: function () {
                    //                        $scope.forgetPassword();
                    //                    }
                    //                };
                    //                return data;
                    //            }
                    //        }
                    //    });

                    //}

                }, function errorCallback(response) {
                });
            }
            $scope.alertPin = function () {
                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPY/AlertIncorrectPIN';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                }, function errorCallback(response) {
                });

            };

            //GetTaxReport
            $scope.GetTaxReportExport = function (type_download) {
                var str_category_export = "";
                var str_status_export = "";
                var type_file = "";
                $scope.loader.enable = true;
                angular.forEach($scope.selection_category, function (value) {
                    str_category_export += value + ",";
                });

                angular.forEach($scope.selection_status, function (value) {
                    str_status_export += value + ",";
                });

                if (type_download === 'PDF') {
                    type_file = 'pdf';
                } else {
                    type_file = 'xlsx';
                }
                var URL = CONFIG.SERVER + 'HRPY/Get50TaviReportExport';

                $scope.employeeData = getToken(CONFIG.USER);
                console.log('set employee-->' + $scope.employeeData);
                var oRequestParameter = {
                    InputParameter: {
                        Type: str_category_export
                        , Status: str_status_export
                        , ReportName: $scope.Text["APPLICATION"].TAXREPORT
                        , LanguageCode: $scope.employeeData.Language
                        , Employee_id: $scope.employeeData.EmployeeID
                        , EmployeeName: $scope.employeeData.Name
                        , ExportType: type_download //"PDF" //EXCEL
                        , DateText: $scope.Text["BE_REPORT"].DATETEXT
                        , TimeText: $scope.Text["BE_REPORT"].TIMETEXT
                        , Year: $scope.ddlYear
                        , PinCode: $scope.OldPinCode
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    var file_name = "ใบรับรองภาษีปี" + $scope.ddlYear + "_" + $scope.employeeData.EmployeeID;
                    if (type_download === "PDF") {
                        file_name += ".pdf";
                    } else if (type_download === "EXCEL") {
                        file_name += ".xls";
                    }
                    var url = CONFIG.SERVER + 'Client/Report/' + file_name;
                    download(url, file_name);
                    $scope.loader.enable = false;
                }, function errorCallback(response) {

                    $scope.loader.enable = false;
                });
            };


            $scope.openModalPincode = function () {

                var modalInstance = $uibModal.open({
                    animation: $scope.animationsEnabled,
                    templateUrl: 'openPincodeTaxRepor.ng-popup.html',
                    controller: 'openPincodeTaxReportController',
                    backdrop: 'static',
                    keyboard: false,
                    size: 'sp',
                    resolve: {
                        items: function () {
                            var data = {
                                requestorData: $scope.requesterData,
                                text: $scope.Text,
                                loadtaxReport: function (pincode) {
                                    getTaxReport(pincode);
                                },
                                fnForgetPassword: function () {
                                    $scope.forgetPassword();
                                }
                            };
                            return data;
                        }
                    }
                });
            };


            $scope.forgetPassword = function () {
                $location.path('frmViewContent/907');
            };

            //Modify by Nipon Supap รวม VerifyPinCode เข้าด้วยกัน 24/06/2020
            $scope.promtDialogPincode = function () {
                $mdDialog.show({
                    controller: DialogControllerPincode,
                    templateUrl: 'views/common/dialog/pincodeInput.tmpl.html',
                    parent: angular.element(document.body),
                    //targetEvent: ev,
                    clickOutsideToClose: true,
                    locals: {
                        params: {
                            title: 'PinCOde Dialog',
                            data: '',
                            columns: '',
                            btnSave: $scope.Text['SYSTEM']['BUTTON_OK'],
                            btnCaancel: $scope.Text['ACTION']['CANCEL'],
                            forgetPincode: $scope.Text['PAYSLIPDATA']['NEWPINREQUEST'],
                            OldPinCode: $scope.OldPinCode,
                            MsgValidate: $scope.MsgValidate,
                        }
                    },
                })
                    .then(function (item) {                        
                        if (!item) {
                            $scope.MsgValidate = "กรุณาระบุ PINCODE";
                            $scope.promtDialogPincode();
                        }
                        else {
                            $scope.OldPinCode = item;
                            $scope.getValidatePinCode();
                        }
                        //getTaxReport(item);
                    }, function () {

                    });
            };
            function DialogControllerPincode($scope, $mdDialog, params) {
                $scope.data = angular.copy(params.data);
                $scope.columns = angular.copy(params.columns);
                $scope.Title = params.Title;
                $scope.forgetPincodeText = params.forgetPincode;
                $scope.BTN_SAVE = params.btnSave;
                $scope.BTN_CANCEL = params.btnCaancel;
                $scope.OldPinCode = params.OldPinCode;
                if (!$scope.OldPinCode) {
                    $scope.MsgValidate = "กรุณาระบุ PINCODE";
                }
                else
                    $scope.MsgValidate = params.MsgValidate;

                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.save = function (answer) {
                    $mdDialog.hide(answer);
                };

                $scope.forgetLikePincode = function () {
                    $mdDialog.cancel();
                    $location.path('frmViewContent/907');
                }
            }
            $scope.getValidatePinCode = function () {
                if (!$scope.OldPinCode) {
                    $scope.MsgValidate = "กรุณาระบุ PINCODE";
                    return;
                }

                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPY/VerifyPinCode';
                var oRequestParameter = {
                    InputParameter: { "PinCode": $scope.OldPinCode }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.isValidate = response.data;
                    if ($scope.isValidate) {
                        $scope.countIncorrectPinCode = 0;
                        $scope.MsgValidate = '';
                        getTaxReport($scope.OldPinCode);
                        //$uibModalInstance.close();
                    }
                    else {

                        if ($scope.countIncorrectPinCode > 2) {
                            $scope.alertPin();
                        }

                        $scope.countIncorrectPinCode += 1;
                        $scope.MsgValidate = 'กรุณาใส่ PINCODE ให้ถูกต้อง';
                        $scope.promtDialogPincode();
                    }
                }, function errorCallback(response) {
                });
            };
            $scope.alertPin = function () {
                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPY/AlertIncorrectPIN';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                }, function errorCallback(response) {
                });
            };
        }]);
        //.controller('openPincodeTaxReportController', ['items', '$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', '$uibModalInstance', function (items, $scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal, $uibModalInstance) {


        //    $scope.requesterData = items.requestorData;
        //    $scope.Text = items.text;
        //    $scope.OldPinCode = '';
        //    $scope.MsgValidate = '';

        //    function alertPin() {
        //        var oEmployeeData = getToken(CONFIG.USER);
        //        var URL = CONFIG.SERVER + 'HRPY/AlertIncorrectPIN';
        //        var oRequestParameter = {
        //            InputParameter: {}
        //            , CurrentEmployee: oEmployeeData
        //            , Requestor: $scope.requesterData
        //        };

        //        $http({
        //            method: 'POST',
        //            url: URL,
        //            data: oRequestParameter
        //        }).then(function successCallback(response) {
        //        }, function errorCallback(response) {
        //        });
        //    }

        //    function getValidatePinCode() {
        //        var oEmployeeData = getToken(CONFIG.USER);
        //        var URL = CONFIG.SERVER + 'HRPY/VerifyPinCode';
        //        var oRequestParameter = {
        //            InputParameter: { "PinCode": $scope.OldPinCode }
        //            , CurrentEmployee: oEmployeeData
        //            , Requestor: $scope.requesterData
        //        };

        //        $http({
        //            method: 'POST',
        //            url: URL,
        //            data: oRequestParameter
        //        }).then(function successCallback(response) {
        //            $scope.isValidate = response.data;

        //            if ($scope.isValidate) {
        //                $scope.countIncorrectPinCode = 0;
        //                $scope.MsgValidate = '';
        //                items.loadtaxReport($scope.OldPinCode);
        //                $uibModalInstance.close();
        //            }
        //            else {
        //                if ($scope.countIncorrectPinCode > 2) {
        //                    alertPin();
        //                }
        //                $scope.countIncorrectPinCode += 1;
        //                $scope.MsgValidate = 'กรุณาใส่ PINCODE ให้ถูกต้อง';
        //            }
        //        }, function errorCallback(response) {
        //        });
        //    }


        //    $scope.onLoadTaxReport = function () {
        //        getValidatePinCode();
        //    };

        //    $scope.closeModal = function () {
        //        $uibModalInstance.close();
        //    };

        //    $scope.onRequestNEWPin = function () {
        //        $uibModalInstance.close();
        //        items.fnForgetPassword();
        //    };

            

        //}])
        //.controller('alertModalSentPincodeController', ['items', '$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', '$uibModalInstance', function (items, $scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal, $uibModalInstance) {
        //    $scope.Text = items.text;
        //    $scope.closeModal = function () {
        //        $uibModalInstance.close();
        //    };

        //}])
        //.controller('alertModalNoDataController', ['items', '$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', '$uibModalInstance', function (items, $scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal, $uibModalInstance) {

        //    $scope.Text = items.text;
        //    $scope.closeModal = function () {
        //        $uibModalInstance.close();
        //    };

        //}]);
})();
