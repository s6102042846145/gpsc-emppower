﻿(function () {
angular.module('ESSMobile')
    .controller('TaxAllowanceContentController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {
        //#### FRAMEWORK FUNCTION ### START
        /******************* variable start ********************/
        $scope.personalaccountloader = false;
        $scope.Textcategory = "TAXALLOWANCE";
        $scope.TaxAllowanceData;
        $scope.ClaimType;
        $scope.ClaimTypeName;
        $scope.content.isShowHeader = true;
        $scope.IsShowTimeAwareLink = false;

        /******************* variable end ********************/

        /******************* listener start ********************/
        $scope.init = function () {
            init();
        }
        /******************* listener end ********************/

        /******************* action start ********************/
        function init() {
            getTimeAwareLink();
            getTaxAllowanceData();
        }
        /******************* action end ********************/

        /******************* service caller  start ********************/

        function getTimeAwareLink() {
            var oEmployeeData = getToken(CONFIG.USER);
            var URL = CONFIG.SERVER + 'HRPA/ShowTimeAwareLink';
            var oRequestParameter = {
                InputParameter: { "LinkID": 2 }
                , CurrentEmployee: oEmployeeData
                , Requestor: $scope.requesterData
            };

            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                $scope.IsShowTimeAwareLink = response.data;
            }, function errorCallback(response) {
            });
        }



        function getTaxAllowanceData() {
            var oEmployeeData = getToken(CONFIG.USER);
            var checkDate = $filter('date')(new Date(), 'yyyy-MM-dd');
            var URL = CONFIG.SERVER + 'HRPY/GetTaxAllowanceData';
            var oRequestParameter = {
                InputParameter: { "EmployeeID": oEmployeeData.EmployeeID }
                , CurrentEmployee: oEmployeeData
                , Requestor: $scope.requesterData
            };

            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                $scope.TaxAllowanceData = response.data;
                getTaxAllowanceEditData();
                }, function errorCallback(response) {
            });
        }

        function findWithAttr(array, attr, value) {
            for (var i = 0; i < array.length; i += 1) {
                if (array[i][attr] == value) {
                    return i;
                }
            }
        }

        function getTaxAllowanceEditData() {
            var oEmployeeData = getToken(CONFIG.USER);
            var checkDate = $filter('date')(new Date(), 'yyyy-MM-dd');
            var URL = CONFIG.SERVER + 'HRPY/GetTaxAllowanceEditData';
            var oRequestParameter = {
                InputParameter: { "EmployeeID": oEmployeeData.EmployeeID }
                , CurrentEmployee: getToken(CONFIG.USER)
            };
           
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                $scope.ClaimType = response.data;
                
                $scope.ClaimTypeName = $scope.ClaimType[findWithAttr($scope.ClaimType, 'Key', $scope.TaxAllowanceData.SpouseAllowance)].Description;
               
            }, function errorCallback(response) {
            });
        }

        function getLink() {
            var oEmployeeData = getToken(CONFIG.USER);
            var URL = CONFIG.SERVER + 'HRPY/GetLinkTAXDEDUCTIONWEB';
            var oRequestParameter = {
                InputParameter: { }
                , CurrentEmployee: oEmployeeData
            };

            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                $scope.LinkTAXDEDUCTIONWEB = response.data;
            }, function errorCallback(response) {
            });
        }
        getLink();


        /******************* service caller  end ********************/
    }]);
})();