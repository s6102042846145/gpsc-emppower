﻿(function () {
    angular.module('ESSMobile')
        .controller('PFChangeRateAmountPlanReportController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal) {


            // Get Config Multi Company
            $scope.isHideMultiCompany = true;
            $scope.GetConfingMultiCompany = function () {
                var URL = CONFIG.SERVER + 'Employee/GetConfigMultiCompany';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data === 1) {
                        $scope.isHideMultiCompany = false;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetConfingMultiCompany();

            $scope.back = function () {
                var url = '/frmViewContent/5000';
                $location.path(url);
            };

            function download(dataurl, filename) {
                var a = document.createElement("a");
                a.href = dataurl;
                a.setAttribute("download", filename);
                var b = document.createEvent("MouseEvents");
                b.initEvent("click", false, true);
                a.dispatchEvent(b);
                return false;
            }

            $scope.ddlYear = "";
            $scope.ddlEffectiveDate = "";
            $scope.ddlExportType = "PDF";

            $scope.YearList = [];
            $scope.EffectiveDateList = [];

            $scope.init = function () {
                //GetYears();
            };

            function GetYears() {

                var URL = CONFIG.SERVER + 'HRBE/GetYearsAdminSetting';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {
                        SelectCompany: $scope.selectCompanyCode
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    angular.forEach(response.data, function (value, key) {
                        $scope.YearList.push(value.YearName);
                    });

                    if ($scope.YearList.length >= 1) {
                        $scope.ddlYear = $scope.YearList[0].toString();
                        $scope.GetListEffectiveDateSetting();
                    }

                }, function errorCallback(response) {
                });
            }

            function GetPeriodSetting(key_year) {
                var URL = CONFIG.SERVER + 'HRPY/GetPeriodSetting';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {
                        "KeyType": "GETDATA",
                        "KeyYear": key_year,
                        "KeyValue": "",
                        "SelectCompany": $scope.selectCompanyCode
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.EffectiveDateList = [];

                    angular.forEach(response.data, function (value, key) {
                        if (value.EnablePercentage) {
                            var check = true;

                            angular.forEach($scope.EffectiveDateList, function (v, k) {
                                if (v == value.EffectiveDate) {
                                    check = false;
                                }
                            });

                            if (check) {
                                $scope.EffectiveDateList.push(value.EffectiveDate);
                            }
                        }
                    });

                    if ($scope.EffectiveDateList.length > 0) {
                        $scope.ddlEffectiveDate = $scope.EffectiveDateList[0];
                    }
                    else {
                        $scope.ddlEffectiveDate = "";
                    }

                }, function errorCallback(response) {
                });
            }

            $scope.GetListEffectiveDateSetting = function () {
                GetPeriodSetting($scope.ddlYear);
            };

            // Export Report
            function GetReport(type_download) {
                var str_category_export = "";
                var str_status_export = "";

                $scope.loader.enable = true;
                angular.forEach($scope.selection_category, function (value) {
                    str_category_export += value + ",";
                });

                angular.forEach($scope.selection_status, function (value) {
                    str_status_export += value + ",";
                });


                var URL = CONFIG.SERVER + 'HRPY/GetPFChangeRateAmountPlanReport';

                $scope.employeeData = getToken(CONFIG.USER);
                var effectiveDate = $scope.ddlEffectiveDate;
                var oRequestParameter = {
                    InputParameter: {
                        SelectCompany: $scope.selectCompanyCode
                        , EffectiveDate: effectiveDate
                        , ReportType: 2
                        , ReportName: "รายงานสรุปการแก้ไขอัตราเงินสะสมกองทุนสำรองเลี้ยงชีพ"
                        , LanguageCode: $scope.employeeData.Language
                        , Employee_id: $scope.employeeData.EmployeeID
                        , CompanyCode: $scope.employeeData.CompanyCode
                        , EmployeeName: $scope.employeeData.Name
                        , ExportType: type_download //"PDF" //EXCEL

                        , DateText: $scope.Text["BE_REPORT"].DATETEXT
                        , TimeText: $scope.Text["BE_REPORT"].TIMETEXT

                        , T_NO: $scope.Text['PFPLANREPORT'].T_NO
                        , T_EMPLOYEEID: $scope.Text['PFPLANREPORT'].T_EMPLOYEEID
                        , T_FULLNAME: $scope.Text['PFPLANREPORT'].T_FULLNAME
                        , T_EMPLOYEESAVINGRATEOLD: $scope.Text['PFPLANRATEREPORT'].T_EMPLOYEESAVINGRATEOLD
                        , T_EMPLOYEESAVINGRATENEW: $scope.Text['PFPLANRATEREPORT'].T_EMPLOYEESAVINGRATENEW
                        , T_REQUESTNO: $scope.Text['PFPLANREPORT'].T_REQUESTNO
                        , T_SUBMITDATE: $scope.Text['PFPLANREPORT'].T_SUBMITDATE
                        , T_ACTIONDATE: $scope.Text['PFPLANREPORT'].T_ACTIONDATE
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    console.log('success');
                    var file_name = "PFChangeRateAmountPlanReport" + $scope.employeeData.EmployeeID;
                    if (type_download == "PDF") {
                        file_name += ".pdf";
                    } else if (type_download == "EXCEL") {
                        file_name += ".xls";
                    }
                    var url = CONFIG.SERVER + 'Client/Report/' + file_name;
                    download(url, file_name);
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    console.log(response);

                    $scope.loader.enable = false;
                });
            }

            $scope.GetReportByType = function () {
                if ($scope.ddlEffectiveDate) {
                    GetReport($scope.ddlExportType);
                }
                else {
                    var modalInstance = $uibModal.open({
                        animation: $scope.animationsEnabled,
                        templateUrl: 'errorModalEffectiveDate.ng-popup.html',
                        controller: 'completeModalPincodeController',
                        backdrop: 'static',
                        keyboard: false,
                        size: 'sp',
                        resolve: {
                            items: function () {
                                var data = {
                                    msg: $scope.Text['PFPLANREPORT']['T_REQUIREDATE'],
                                    text: $scope.Text['SYSTEM']['CLOSE_DOWN']
                                };
                                return data;
                            }
                        }
                    });
                }
            }

            $scope.GetData = function () {
                GetPFChangePlanReport($scope.ddlEffectiveDate);
            };

            // Get Data Report
            function GetPFChangePlanReport(effectiveDate) {
                var URL = CONFIG.SERVER + 'HRPY/GetPFChangePlanList';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "EffectiveDate": effectiveDate, "ReportType": 2, "language": $scope.employeeData.Language }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.data = response.data;

                    if ($scope.data.length == 0) {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.querySelector('#popupContainer')))
                                .clickOutsideToClose(true)
                                .title('ไม่พบข้อมูล!')
                                .ok('ตกลง')
                        );
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }
            $scope.data = {
                nPageSize: 10,
                arrFilterd: [],
                arrSplited: [],
                selectedColumn: 0,
                tFilter: '',
                pages: []
            };
            //---------------------- -- SET DATE  -- -----------------------------------------
            $scope.effectiveDate = new Date().toISOString().split("T")[0];
            $scope.date_to_select = new Date().toISOString().split("T")[0];


            $scope.setDate = function (selectedDate) {
                $scope.effectiveDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };


            //RESET
            $scope.reset_report = function () {
                //$scope.effectiveDate = new Date().toISOString().split("T")[0];
                //$scope.data = [];

                //angular.forEach($scope.category, function (item) {
                //    item.Selected = false;
                //});



                if ($scope.YearList.length >= 1) {
                    $scope.ddlYear = $scope.YearList[0].toString();
                    $scope.GetListEffectiveDateSetting();
                }

                //$scope.ddlYear = "";
                //$scope.ddlEffectiveDate = "";
            };



            // ******** Multi Company ******

            $scope.employeeData = getToken(CONFIG.USER);
            $scope.selectCompanyCode = $scope.employeeData.CompanyCode;
            $scope.CompanyList = [];
            // List Company
            $scope.GetAuthorizationCompany = function () {
                var URL = CONFIG.SERVER + 'workflow/GetAuthorizationCompany';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.CompanyList = response.data;
                    if ($scope.selectCompanyCode) {
                        GetYears();
                    }
                    $scope.loader.enable = false;

                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetAuthorizationCompany();

            // Change Comapany
            $scope.changeCompany = function () {

                if ($scope.selectCompanyCode) {
                    GetYears();
                }
            };

            // ********************************

            //Pagination of nested objects in angularjs
            $scope.pagin = {
                currentPage: 1, itemPerPage: '10', numPage: 1
            };
            $scope.filterDatas = function (searchTxt) {
                if (!$scope.data || !$scope.data.length) return [];
                var datas = $filter('filter')($scope.data, searchTxt);
                datas = $filter('filter')(datas, $scope.advacneOptionFilter);

                return datas;
            };
            $scope.genDatas = function (searchTxt) {
                var datas = $scope.filterDatas(searchTxt);
                setNumOfPage(datas);


                var res = [];

                if ($scope.pagin.numPage > 1) {
                    datas.forEach(function (value, i) {
                        var from = (parseInt($scope.pagin.currentPage) - 1) * parseInt($scope.pagin.itemPerPage);
                        var to = from + parseInt($scope.pagin.itemPerPage);

                        if (from <= i && to > i)
                            res.push(value);
                    });
                } else {
                    res = datas.slice();
                }

                return res;
            };
            var setNumOfPage = function (datas) {
                var totalItems = 0;
                if (datas) totalItems = datas.length;
                $scope.pagin.numPage = Math.ceil(totalItems / $scope.pagin.itemPerPage);
                if ($scope.pagin.numPage <= 0) $scope.pagin.numPage = 1;

                if ($scope.pagin.currentPage > $scope.pagin.numPage) $scope.pagin.currentPage = 1;
            };
            $scope.changePage = function (targetPage) {
                $scope.pagin.currentPage = angular.copy(targetPage);
            };
            $scope.getPages = function () {
                var pages = [];

                for (var i = 0; i < $scope.pagin.numPage; i++) {
                    pages.push((i + 1));
                }

                return pages;
            };

        }]);
})();
