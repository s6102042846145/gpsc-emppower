﻿(function () {
    angular.module('ESSMobile')
        .controller('DutyPaymentContentController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal) {


            $scope.modelPeriod = {
                selectPeriod: ""
            };

            $scope.period_data = [];
            $scope.loadPeriod = function () {

                var URL = CONFIG.SERVER + 'HRTM/GetPeriodDutyPayment';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.period_data = response.data;

                    if (response.data.length > 0) {

                        $scope.loadDataPeriod(response.data[response.data.length-1].PeriodValue);
                        $scope.modelPeriod.selectPeriod = response.data[response.data.length-1].PeriodValue;

                    }
                    $scope.loader.enable = false;

                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.loadPeriod();

            $scope.list_dataDutyPayment = [];
            $scope.obj_dailyWs = {};
            $scope.loadDataPeriod = function (valuePeriod) {

                console.log(valuePeriod);
                var URL = CONFIG.SERVER + 'HRTM/GetListDataPeriodDutyPayment';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                    , SelectedPeriod: valuePeriod
                };

                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.list_dataDutyPayment = response.data.list_duty;
                    $scope.obj_dailyWs = response.data.duty_dailyWs;
                    $scope.loader.enable = false;

                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };


            $scope.view_duty_payment = function (requestNo) {

                if (angular.isDefined(requestNo)) {

                    //var URL = CONFIG.SERVER + 'HRTM/GetDutyPaymentDocument';
                    //$scope.employeeData = getToken(CONFIG.USER);
                    //var oRequestParameter = {
                    //    InputParameter: {}
                    //    , CurrentEmployee: $scope.employeeData
                    //    , Requestor: $scope.requesterData
                    //    , Creator: getToken(CONFIG.USER)
                    //    , DutyPayment: requestNo
                    //};

                    //oRequestParameter.RequestNo = requestNo;
                    //$http({
                    //    method: 'POST',
                    //    url: URL,
                    //    data: oRequestParameter
                    //}).then(function successCallback(response) {

                    //    $scope.dutyPaymentDocument = response.data;

                        if (typeof cordova !== 'undefined') {
                            console.log("");
                        } else {
                            //var path = CONFIG.SERVER + 'Client/index.html#!/frmViewRequest/' + requestNo + '/' + $scope.requesterData.CompanyCode + '/' + $scope.dutyPaymentDocument.__keyMaster + '/' + 'true' + '/' + 'false';
                            var path = CONFIG.SERVER + 'Client/index.html#!/frmViewRequest/' + requestNo + '/' + $scope.requesterData.CompanyCode + '/' + null + '/' + 'true' + '/' + 'true';
                            window.open(path, '_blank');
                        }

                    //    $scope.loader.enable = false;
                    //}, function errorCallback(response) {
                    //    $scope.loader.enable = false;
                    //});
                }
            }

        }]);
})();