﻿(function () {
angular.module('ESSMobile')
    .controller('AbsenceEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {
        //#### FRAMEWORK FUNCTION ### START
        //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
        $scope.ChildAction.SetData = function () {
            $scope.startDate = $scope.document.Additional.INFOTYPE2001[0].BeginDate;
            $scope.endDate = $scope.document.Additional.INFOTYPE2001[0].EndDate;
            $scope.formData = {};
            var URL = CONFIG.SERVER + 'hrtm/GetAbsenceTypeByEmployeeID/' + employeeDate.EmployeeID + '/' + employeeDate.Language;
            $http({
                method: 'POST',
                url: URL,
                data: employeeDate
            }).then(function successCallback(response) {
                // Success
                $scope.absenceTypes = response.data;
                $scope.document.Additional.INFOTYPE2001[0].SubType = ($scope.document.Additional.INFOTYPE2001[0].SubType == '') ? $scope.absenceTypes[0].Key : $scope.document.Additional.INFOTYPE2001[0].SubType;
                console.log('current AbsenceType', $scope.document.Additional.INFOTYPE2001[0].SubType);
                //console.log('AbsenceTypes.', $scope.absenceTypes);
                $scope.initialAbsenceForm();

            }, function errorCallback(response) {
                // Error
                console.log('error AbsenceEditorController.', response);
                $scope.absenceTypes = {

                };
            });

        }

        //LoadData Function : Use to add some logic to Additional dataset before take any action
        $scope.ChildAction.LoadData = function () {
            
        }

        var employeeDate = getToken(CONFIG.USER);
        $scope.ChildAction.SetData();
        //#### FRAMEWORK FUNCTION ### END


        //#### OTHERS FUNCTION ### START 
        $scope.initialAbsenceForm = function () {
            console.log('initial absence.');
            var list = $scope.absenceTypes;
            var index;
            var absT = null;
            for (index = 0; index < list.length; index++) {
                if (list[index].Key == $scope.document.Additional.INFOTYPE2001[0].SubType) {
                    absT = list[index];
                    break;
                }
            }
            if (absT != null && absT.AllDayFlag == false) {
                $scope.getDivideDailyWorkSchedule($scope.document.Additional.INFOTYPE2001[0].BeginDate.substr(0, 10));
            }
            console.log('document.', $scope.document);
        };

        $scope.parseDate = function (dateString) {
            var from = dateString.split("/");
            var dateObj = new Date(from[2], from[1] - 1, from[0]); // (year,month,date)
            return dateObj;
        };

        $scope.absenceTypeChange = function () {
            console.log('absenceTypeChange.', $scope.document.Additional.INFOTYPE2001[0].SubType);
            var list = $scope.absenceTypes;
            var index;
            var absT = null;
            for (index = 0; index < list.length; index++) {
                if (list[index].Key == $scope.document.Additional.INFOTYPE2001[0].SubType) {
                    absT = list[index];
                    break;
                }
            }
            $scope.document.Additional.INFOTYPE2001[0].AllDayFlag = absT.AllDayFlag;
            if (absT.AllDayFlag == false) {
                $scope.getDivideDailyWorkSchedule($scope.document.Additional.INFOTYPE2001[0].BeginDate.substr(0, 10));
            }
        };

        $scope.setSelectedBeginDate = function (selectedDate) {
            console.log('begin date.', selectedDate);
            $scope.document.Additional.INFOTYPE2001[0].BeginDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
            console.log('begin date.', $scope.document.Additional.INFOTYPE2001[0].BeginDate);
        };

        $scope.setSelectedEndDate = function (selectedDate) {
            console.log('end date.', selectedDate);
            $scope.document.Additional.INFOTYPE2001[0].EndDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
            console.log('end date.', $scope.document.Additional.INFOTYPE2001[0].EndDate);
        };

        $scope.halfdayChange = function () {
            console.log('ddl halfdayChange.', $scope.formData.selectedDailyWorkSchedule);
            var times = $scope.formData.selectedDailyWorkSchedule.split(' - ');
            $scope.document.Additional.INFOTYPE2001[0].BeginTime = times[0];
            $scope.document.Additional.INFOTYPE2001[0].EndTime = times[1];
            console.log('document.', $scope.document);
        };

        $scope.getDivideDailyWorkSchedule = function (dateString) {
            var URL = CONFIG.SERVER + 'hrtm/DivideDailyWorkSchedule/' + dateString;
            $http({
                method: 'POST',
                url: URL,
                data: employeeDate
            }).then(function successCallback(response) {
                // Success
                console.log('getDivideDailyWorkSchedule start.');
                var timeArray = [];
                for (var key in response.data) {
                    if (response.data.hasOwnProperty(key)) {
                        timeArray.push({
                            value: response.data[key],
                            text: response.data[key]
                        });
                    }
                }

                // initial time dropdown
                var t = timeArray[0].value;
                if ($scope.document.Additional.INFOTYPE2001[0].BeginTime 
                    && $scope.document.Additional.INFOTYPE2001[0].EndTime
                    && $scope.document.Additional.INFOTYPE2001[0].BeginTime != '00:00:00' 
                    && $scope.document.Additional.INFOTYPE2001[0].EndTime != '00:00:00') {
                    var startTime = $scope.document.Additional.INFOTYPE2001[0].BeginTime.substr(0, 5);
                    var endTime = $scope.document.Additional.INFOTYPE2001[0].EndTime.substr(0, 5);
                    t = startTime + ' - ' + endTime;
                }
                $scope.formData.selectedDailyWorkSchedule = t;

                $scope.dailyWorkSchedule = timeArray;
                console.log('getDivideDailyWorkSchedule.', timeArray);

            }, function errorCallback(response) {
                // Error
                var timeArray = [];
                timeArray.push({
                    value: '',
                    text: '-'
                });
                $scope.formData.selectedDailyWorkSchedule = '';
                $scope.dailyWorkSchedule = [];
                console.log('error getDivideDailyWorkSchedule.', response);

            });
        };

        $scope.setSelectedDate = function (selectedDate) {
            console.log('date.', selectedDate);
            $scope.document.Additional.INFOTYPE2001[0].BeginDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
            $scope.document.Additional.INFOTYPE2001[0].EndDate = $scope.document.Additional.INFOTYPE2001[0].BeginDate;
            $scope.getDivideDailyWorkSchedule($filter('date')(selectedDate, 'yyyy-MM-dd'));
            console.log('date.', $scope.document.Additional.INFOTYPE2001[0].EndDate);
            console.log('document.', $scope.document);
        };

        $scope.getAbsenceSubEditor = function () {
            var editor = $scope.document.Editor;
            var list = $scope.absenceTypes;
            var index;
            var absT = null;
            for (index = 0; index < list.length; index++) {
                if (list[index].Key == $scope.document.Additional.INFOTYPE2001[0].SubType) {
                    absT = list[index];
                    break;
                }
            }
            var templateName;
            if (absT!= null && absT.AllDayFlag == false) {
                templateName = 'absenceeditor_halfday';
            } else {
                templateName = 'absenceeditor_fullday';
            }
            return 'views/hr/tm/data/' + templateName + '.html?t=' + $scope.runtime;
        }
        //#### OTHERS FUNCTION ### END 
    }]);
})();