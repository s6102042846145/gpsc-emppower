﻿(function () {
angular.module('ESSMobile')
    .controller('AbsenceQuotaController', ['$scope', '$http', '$routeParams', '$location', 'CONFIG', function ($scope, $http, $routeParams, $location, CONFIG) {

        var URL = CONFIG.SERVER + 'HRTM/GetAbsenceQuotaByEmployeeID';
        $http({
            method: 'POST',
            url: URL,
            data: $scope.employeeData
        }).then(function successCallback(response) {
            // Success
            $scope.documents = response.data;
            console.log('document.', $scope.documents);

        }, function errorCallback(response) {
            console.log('error AbsenceQuotaController.', response);
            //$scope.document = {
            //    'RequestId': '1',
            //    'RequesterId': '00547304'
            //};
        });

        $scope.Textcategory = 'HRTMABSENCEQUOTA';

    }]);
})();