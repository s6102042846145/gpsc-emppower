﻿(function () {
angular.module('ESSMobile')
    .controller('AbsenceListController', ['$scope', '$http', '$routeParams', '$location', 'CONFIG', function ($scope, $http, $routeParams, $location, CONFIG) {

        var currentPeriod = new Date().getFullYear();
        $scope.Emp_period = [currentPeriod, currentPeriod - 1, currentPeriod-2];
        $scope.selectedPeriod = $scope.Emp_period[0];

        $scope.Textcategory = 'HRTMABSENCE';

        var URL = CONFIG.SERVER + 'HRTM/GetAbsenceListByEmployeeID/' + currentPeriod;  // + $scope.selectedPeriod;
        $http({
            method: 'POST',
            url: URL,
            data: $scope.employeeData
        }).then(function successCallback(response) {
            // Success
            $scope.documents = response.data;
            console.log('document.', $scope.documents);

        }, function errorCallback(response) {
            // Error
            console.log('error AbsenceListController.', response);
        });


        $scope.getRequestViewerTemplate = function (r) {
            var viewer = $scope.document.Viewer;
            return 'views/' + viewer + '.html?t=' + r;
        }


        $scope.doRequestAction = function () {
            console.log('doRequestAction.', $scope.selectedPeriod);
            var URL = CONFIG.SERVER + 'HRTM/GetAbsenceListByEmployeeID/' + $scope.selectedPeriod;
            $http({
                method: 'POST',
                url: URL,
                data: $scope.employeeData
            }).then(function successCallback(response) {
                // Success
                $scope.documents = response.data;
                console.log('document.', $scope.documents);

            }, function errorCallback(response) {
                // Error
                console.log('error AbsenceListController.', response);
            });
        };

        $scope.GetSummOfDays = function (group) {
            var sum = 0;
            for (var i=0; i < group.length; i++) {
                sum = sum + Number(group[i].AbsenceDays);
            }
            return sum;
        };
        
        
    }]);
})();