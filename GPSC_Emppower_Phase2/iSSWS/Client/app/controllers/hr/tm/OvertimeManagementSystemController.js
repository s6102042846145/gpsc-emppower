﻿(function () {
    angular.module('ESSMobile')
        .controller('OvertimeManagementSystemController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {

            //$scope.subGroup = parseInt($scope.employeeData.EmpSubGroup);


            $scope.employeeData = getToken(CONFIG.USER);
            $scope.selectedPeriod = "";
            $scope.IsShowCreateSummaryOT = false;
            $scope.oRequestParameter = {
                InputParameter: {}
                , CurrentEmployee: $scope.employeeData
                , Requestor: $scope.requesterData
                , Creator: getToken(CONFIG.USER)
                , Language: $scope.employeeData.Language
                , SelectedPeriod: $scope.selectedPeriod
            };


            $scope.GetEmpSubGroup = function () {
                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/GetEmpSubGroup',
                    data: $scope.oRequestParameter
                }).then(function successCallback(response) {
                    $scope.subGroup = response.data;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetEmpSubGroup();


            $scope.GetIsShowCreateSummaryOT = function () {
                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/GetIsShowCreateSummaryOT',
                    data: $scope.oRequestParameter
                }).then(function successCallback(response) {
                    $scope.IsShowCreateSummaryOT = response.data;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetIsShowCreateSummaryOT();


            $scope.GetPeriodOTList = function () {
                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/GetPeriodOTList',
                    data: $scope.oRequestParameter
                }).then(function successCallback(response) {
                    $scope.periods = response.data;
                    if ($scope.periods) {
                        $scope.selectedPeriod = $scope.getCurrentMonth();
                    }
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetPeriodOTList();

            $scope.GetDailyOTList = function () {
                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/GetDailyOTList',
                    data: $scope.oRequestParameter
                }).then(function successCallback(response) {
                    $scope.dailyOTs = response.data;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };

            $scope.$watch('selectedPeriod', function (value) {
                if ($scope.selectedPeriod) {
                    $scope.oRequestParameter.SelectedPeriod = $scope.selectedPeriod;
                    $scope.GetDailyOTList();
                }
            });
            $scope.changePeriod = function (value) {
                if (value) {
                    $scope.oRequestParameter.SelectedPeriod = value;
                    $scope.GetDailyOTList();
                }
            };

            $scope.GetOTWorkType = function () {
                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/GetOTWorkType',
                    data: $scope.oRequestParameter
                }).then(function successCallback(response) {
                    $scope.oTWorkTypes = response.data;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetOTWorkType();

            $scope.getTypeName = function (typeId) {
                if ($scope.oTWorkTypes) {
                    $scope.attTypes = $scope.oTWorkTypes.filter(function (item) {
                        return item.__otWorkTypeID === typeId;
                    });
                    return $scope.attTypes[0].__otWorkTypeDesc;
                }
                return "";
            };

            $scope.calH = function (item) {
                if (item) {
                    return (item / 60).toFixed(2);
                }
                return 0;
            };

            $scope.View = function (requestNo) {
                if (angular.isDefined(requestNo)) {

                    if (typeof cordova != 'undefined') {
                        console.log();
                    } else {

                        var path = CONFIG.SERVER + 'Client/index.html#!/frmViewRequest/' + requestNo + '/' + $scope.requesterData.CompanyCode + '/' + null + '/' + 'true' + '/' + 'true';
                        //window.open(path, '_blank');

                        $scope.rootNewWindowTab(path, '', 1300, 700);
                    }
                }
            };

            $scope.sumRequestOTHour10 = function (items) {
                var result = 0;
                angular.forEach(items, function (item) {
                    result += parseFloat($scope.calH(item._RequestOTHour10));
                });
                return result;
            };
            $scope.sumRequestOTHour15 = function (items) {
                var result = 0;
                angular.forEach(items, function (item) {
                    result += parseFloat($scope.calH(item._RequestOTHour15));
                });
                return result;
            };
            $scope.sumRequestOTHour30 = function (items) {
                var result = 0;
                angular.forEach(items, function (item) {
                    result += parseFloat($scope.calH(item._RequestOTHour30));
                });
                return result;
            };

            //สรุป OT 
            $scope.selectedPeriodSummaryOT = "";
            $scope.GetPeriodSummaryOTList = function () {
                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/GetPeriodSummaryOTList',
                    data: $scope.oRequestParameter
                }).then(function successCallback(response) {
                    $scope.periodSummaryOTs = response.data;
                    if ($scope.periodSummaryOTs) {
                        $scope.selectedPeriodSummaryOT = $scope.periodSummaryOTs[$scope.periodSummaryOTs.length - 1].PeriodValue;
                    }
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetPeriodSummaryOTList();
            $scope.GetListEmpResponsible = function () {
                $scope.oRequestParameter.SubjectID = $scope.contentInfo.SubjectID;
                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/GetListEmpResponsible',
                    data: $scope.oRequestParameter
                }).then(function successCallback(response) {
                    $scope.empResponsibles = response.data;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };

            $scope.$watch('selectedPeriodSummaryOT', function (value) {
                if ($scope.selectedPeriodSummaryOT) {
                    $scope.oRequestParameter.Period = $scope.selectedPeriodSummaryOT;
                    $scope.GetListEmpResponsible();
                }
            });


            $scope.changePeriodSummaryOT = function (value) {
                if (value) {
                    $scope.oRequestParameter.Period = value;
                    $scope.GetListEmpResponsible();
                }
            };

        }]);
})();