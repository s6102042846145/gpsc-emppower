﻿(function () {
angular.module('ESSMobile')
    .controller('ChangeAbsenceViewerController', ['$scope', '$http', '$routeParams', '$location', 'CONFIG', function ($scope, $http, $routeParams, $location, CONFIG) {
        //#### FRAMEWORK FUNCTION ### START
        //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
        $scope.ChildAction.SetData = function () {
            $scope.Textcategory = 'HRTMABSENCE';

            $scope.templateControl.isDeleteRequest = true;

            console.log('ItemKeys.', $scope.itemKeys);

            var URL = CONFIG.SERVER + 'hrtm/GetAbsenceTypeByEmployeeID/' + employeeDate.EmployeeID + '/' + employeeDate.Language;
            $http({
                method: 'POST',
                url: URL,
                data: employeeDate
            }).then(function successCallback(response) {
                // Success
                var list = response.data;
                var index;
                for (index = 0; index < list.length; index++) {
                    if (list[index].Key == $scope.document.Additional.INFOTYPE2001[0].SubType) {
                        $scope.absenceTypeName = list[index].Description;
                        break;
                    }
                }
                console.log('AbsenceTypeName.', $scope.absenceTypeName);

            }, function errorCallback(response) {
                // Error
                console.log('error AbsenceViewerController.', response);
                $scope.absenceTypeName = '-';
            });
        }

        //LoadData Function : Use to add some logic to Additional dataset before take any action
        $scope.ChildAction.LoadData = function () {
            //Do something
        }

        var employeeDate = getToken(CONFIG.USER);
        $scope.ChildAction.SetData();
        //#### FRAMEWORK FUNCTION ### END

        //#### OTHERS FUNCTION ### START 
        //#### OTHERS FUNCTION ### END
    }]);
})();