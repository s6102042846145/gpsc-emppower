﻿(function () {
angular.module('ESSMobile')
    .controller('ChangeAbsenceEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {
        //#### FRAMEWORK FUNCTION ### START
        //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
        $scope.ChildAction.SetData = function () {

            $scope.templateControl.isDeleteRequest = true;

            console.log('ChangeAbsenceEditorController isDeleteRequest.', $scope.templateControl.isDeleteRequest);
            console.log('ItemKeys.', $scope.itemKeys);
            if ($scope.itemKeys != null) {


                $scope.itemKeys[2] = new Date($scope.itemKeys[2].substr(0, 4), $scope.itemKeys[2].substr(4, 2) - 1, $scope.itemKeys[2].substr(6, 2));//'20160330';
                $scope.itemKeys[3] = new Date($scope.itemKeys[3].substr(0, 4), $scope.itemKeys[3].substr(4, 2) - 1, $scope.itemKeys[3].substr(6, 2));//'20160331';

                $scope.EmployeeID = $scope.itemKeys[0];
                $scope.absenceType = $scope.itemKeys[1];
                $scope.beginDate = $filter('date')($scope.itemKeys[2], 'yyyy-MM-ddT00:00:00');
                $scope.endDate = $filter('date')($scope.itemKeys[3], 'yyyy-MM-ddT00:00:00');

                $scope.document.Additional.INFOTYPE2001[0].EmployeeID = $scope.EmployeeID;
                $scope.document.Additional.INFOTYPE2001[0].AbsenceType = $scope.absenceType;
                $scope.document.Additional.INFOTYPE2001[0].SubType = $scope.absenceType;
                $scope.document.Additional.INFOTYPE2001[0].BeginDate = $scope.beginDate;
                $scope.document.Additional.INFOTYPE2001[0].EndDate = $scope.endDate;
            }



            console.log('document.', $scope.document);

            var URL = CONFIG.SERVER + 'hrtm/GetAbsenceTypeByEmployeeID/' + employeeDate.EmployeeID + '/' + employeeDate.Language;
            $http({
                method: 'POST',
                url: URL,
                data: employeeDate
            }).then(function successCallback(response) {
                // Success
                var list = response.data;
                var index;
                var absT = null;
                for (index = 0; index < list.length; index++) {
                    if (list[index].Key == $scope.document.Additional.INFOTYPE2001[0].SubType) {
                        absT = list[index];
                        $scope.absenceTypeName = list[index].Description;
                        break;
                    }
                }
                if (absT != null) {
                    $scope.absenceTypeIsHalfDay = !absT.AllDayFlag;
                }
                console.log('AbsenceTypeName.', $scope.absenceTypeName);


            }, function errorCallback(response) {
                // Error
                console.log('error AbsenceViewerController.', response);
                $scope.absenceTypeName = '-';
            });
        }

        //LoadData Function : Use to add some logic to Additional dataset before take any action
        $scope.ChildAction.LoadData = function () {
            //Do something
        }

        var employeeDate = getToken(CONFIG.USER);
        $scope.ChildAction.SetData();
        //#### FRAMEWORK FUNCTION ### END

        //#### OTHERS FUNCTION ### START 
        //#### OTHERS FUNCTION ### END
    }]);
})();