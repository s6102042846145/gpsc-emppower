﻿(function () {
    angular.module('ESSMobile')
        .controller('ReportAdminController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal) {

            $scope.leftReport = [];
            $scope.rightReport = [];

            $scope.loadGroupReport = function () {

                var URL = CONFIG.SERVER + 'HRTM/GetGroupReportAdmin';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {
                        ReportGroupId: 5000
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.leftReport = response.data.LeftReport;
                    $scope.rightReport = response.data.RightReport;

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.loadGroupReport();

            $scope.next = function (item) {

                var url = '/frmViewContent/' + item.SubjectReportSubID;
                $location.path(url);
            };

        }]);
})();