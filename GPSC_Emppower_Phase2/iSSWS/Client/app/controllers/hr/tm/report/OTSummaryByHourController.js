﻿(function () {
    angular.module('ESSMobile')
        .controller('OTSummaryByHourController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal) {


            // Get Config Multi Company
            $scope.isHideMultiCompany = true;
            $scope.GetConfingMultiCompany = function () {
                var URL = CONFIG.SERVER + 'Employee/GetConfigMultiCompany';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data === 1) {
                        $scope.isHideMultiCompany = false;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetConfingMultiCompany();

            function download(dataurl, filename) {
                var a = document.createElement("a");
                a.href = dataurl;
                a.setAttribute("download", filename);
                var b = document.createEvent("MouseEvents");
                b.initEvent("click", false, true);
                a.dispatchEvent(b);
                return false;
            }

            $scope.list_year = [];

            var d = new Date();
            $scope.model = {
                Year:"",
                ExportTye: "EXCEL"
            };

            $scope.back = function () {
                var url = '/frmViewContent/5000';
                $location.path(url);
            };

            // Support Admin Multi Company
            $scope.employeeData = getToken(CONFIG.USER);
            $scope.selectCompanyCode = $scope.employeeData.CompanyCode; // Default CompanyCode GPSC
            $scope.CompanyList = [];

            // Get List Authorization Comapny In Employee Admin
            $scope.GetAuthorizationCompany = function () {
                var URL = CONFIG.SERVER + 'workflow/GetAuthorizationCompany';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.CompanyList = response.data;
                    $scope.GetYearConfig();

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetAuthorizationCompany();

            // Change Company Reload Data Export Report
            $scope.changeCompany = function () {
                $scope.GetYearConfig();
            };

            $scope.GetYearConfig = function () {
                var URL = CONFIG.SERVER + 'HRTM/GetListYearOTLogByHour';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {
                        SelectCompany: $scope.selectCompanyCode
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.list_year = response.data;
                    if ($scope.list_year.length > 0) {
                        $scope.model.Year = response.data[0];
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };


            $scope.setBeginDate = function (selectedDate) {
                $scope.model.StartDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };

            $scope.setEndDate = function (selectedDate) {
                $scope.model.EndDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };


            $scope.exportReport = function () {

                var URL = CONFIG.SERVER + 'HRTM/ExportReportOTLogByHour';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {
                        SelectCompany: $scope.selectCompanyCode,
                        Year: $scope.model.Year
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                var config = {
                    responseType: "arraybuffer",
                    cache: false
                };

                var apiData = { url: "", fileName: "" };
                var now = new Date();
                var now_time = now.getUTCFullYear() + "" + now.getUTCMonth() + "" + now.getUTCDate() + "" + now.getHours() + "" + now.getUTCMinutes() + "" + now.getUTCSeconds();
                apiData.fileName = "ReportOTLogByHour" + now_time + ".xlsx";

                $scope.loader.enable = true;
                $http.post(URL, oRequestParameter, config)
                    .then(function successCallback(response) {

                        var blob = new Blob([response.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
                        saveAs(blob, apiData.fileName);
                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                    });

            };

            
        }]);
})();