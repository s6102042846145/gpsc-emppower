﻿(function () {
    angular.module('ESSMobile')
        .controller('AttendanceMonthlyReportController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal) {

            $scope.back = function () {
                var url = '/frmViewContent/5000';
                $location.path(url);
            };

            var d = new Date();
            $scope.model = {
                StartDate: $filter('date')(d, 'yyyy-MM-dd'),
                EndDate: $filter('date')(d, 'yyyy-MM-dd')
                //ListOrgTree: "",
                //ExportTye: "EXCEL"
            };

            function download(dataurl, filename) {
                var a = document.createElement("a");
                a.href = dataurl;
                a.setAttribute("download", filename);
                var b = document.createEvent("MouseEvents");
                b.initEvent("click", false, true);
                a.dispatchEvent(b);
                return false;
            }

            $scope.setBeginDate = function (selectedDate) {
                $scope.model.StartDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };

            $scope.setEndDate = function (selectedDate) {
                $scope.model.EndDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };

            // Get Config Multi Company
            $scope.isHideMultiCompany = true;
            $scope.GetConfingMultiCompany = function () {
                var URL = CONFIG.SERVER + 'Employee/GetConfigMultiCompany';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data === 1) {
                        $scope.isHideMultiCompany = false;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetConfingMultiCompany();

            $scope.employeeData = getToken(CONFIG.USER);
            $scope.SelectCompanyCode = $scope.employeeData.CompanyCode;

            $scope.GetAuthorizationCompany = function () {
                var URL = CONFIG.SERVER + 'workflow/GetAuthorizationCompany';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.CompanyList = response.data;

                    if ($scope.CompanyList.length > 0) {
                        $scope.ddlCompany = $scope.CompanyList[0].CompanyCode;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }
            $scope.GetAuthorizationCompany();


            $scope.changeCompany = function (CompanyCode) {
                $scope.SelectCompanyCode = CompanyCode;
                if ($scope.SelectCompanyCode) {
                    GetOrganization();
                }
                GetReportType();
            };


            $scope.splitOffCycle = function (OffCycle, keyMonth) {

                var text_month_detail = "";
                var fields = OffCycle.split('|');
                var DateOffCycle = fields[0];
                var IsOffCycle = fields[1];
                if (IsOffCycle === '1') {

                    if (DateOffCycle) {
                        var str_split = DateOffCycle.toString().substring(2, 4);
                        var int_splint = parseInt(str_split);
                        text_month_detail = $scope.TextMonth["D_M" + int_splint.toString()];

                        var day_data = DateOffCycle.toString().substring(0, 2);
                        var month_data = DateOffCycle.toString().substring(2, 4);
                        var year_data = DateOffCycle.toString().substring(4, 8);
                        var concat_data = day_data + '/' + month_data + '/' + year_data;


                        return text_month_detail + " (" + concat_data + ") ";
                    }
                }
                else {
                    text_month_detail = $scope.TextMonth["D_M" + keyMonth];
                    return text_month_detail;
                }
            };

            GetListEmp();

            function download(dataurl, filename) {
                var a = document.createElement("a");
                a.href = dataurl;
                a.setAttribute("download", filename);
                var b = document.createEvent("MouseEvents");
                b.initEvent("click", false, true);
                a.dispatchEvent(b);
                return false;
            }
            $scope.rdoSelected = "1";
            $scope.file_download = "";

            $scope.TextMonth = $scope.Text["SYSTEM"];

            $scope.ListReportType;
            $scope.ListEmp = [];
            $scope.Organization;
            $scope.TreeOrganization = [];
            $scope.SelectedEmployeeID = '';
            $scope.SelectedEmployeeName = '';
            $scope.SelectedPositionID = '';
            $scope.SelectedPositionName = '';
            $scope.SelectedOrgUnitID = '';
            $scope.SelectedOrgUnitName = '';
            $scope.ObjEmp;

            $scope.SelectedListOrg;

            $scope.isValidate = false;
            $scope.IsSelectedEmployee = false;


            $scope.init = function () {
                GetReportType();
            };

            function GetListEmp() {
                var URL = CONFIG.SERVER + 'HRPY/GetAllEmployeeInINFOTYPE0001';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "SelectCompany": $scope.SelectCompanyCode }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.ListEmp = response.data;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }


            GetOrganization();
            function GetOrganization() {
                var URL = CONFIG.SERVER + 'HRPY/GetOrganizationForReport';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "SubjectID": $scope.contentInfo.SubjectID, "SelectCompany": $scope.SelectCompanyCode }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.TreeOrganization = [];

                    $scope.Organization = response.data;

                    $scope.Organization.forEach(ele => {
                        ele.IsExpanding = true;
                        ele.OrgChild = [];
                        ele.Selected = false;
                        ele.Disable = false;
                        //$scope.TreeOrganization[j].Objective = [];
                    });

                    for (var i = 0; i < $scope.Organization.length; i++) {
                        if ($scope.Organization[i].OrgLevel == 0) {
                            $scope.TreeOrganization.push($scope.Organization[i]);
                        }
                        else if ($scope.Organization[i].OrgLevel == 1) {
                            for (var j = 0; j < $scope.TreeOrganization.length; j++) {
                                if ($scope.TreeOrganization[0].OrgUnit == $scope.Organization[i].OrgParent) {
                                    $scope.TreeOrganization[0].OrgChild.push($scope.Organization[i]);
                                    break;
                                }
                            }
                        }
                        else if ($scope.Organization[i].OrgLevel == 2) {
                            for (var k = 0; k < $scope.TreeOrganization[0].OrgChild.length; k++) {
                                if ($scope.TreeOrganization[0].OrgChild[k].OrgUnit == $scope.Organization[i].OrgParent) {
                                    $scope.TreeOrganization[0].OrgChild[k].OrgChild.push($scope.Organization[i]);
                                    break;
                                }
                            }
                        }
                        else if ($scope.Organization[i].OrgLevel == 3) {
                            for (var m = 0; m < $scope.TreeOrganization[0].OrgChild.length; m++) {
                                for (var n = 0; n < $scope.TreeOrganization[0].OrgChild[m].OrgChild.length; n++) {
                                    if ($scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgUnit == $scope.Organization[i].OrgParent) {
                                        $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild.push($scope.Organization[i]);
                                        break;
                                    }
                                }
                            }
                        }
                        else if ($scope.Organization[i].OrgLevel == 4) {
                            for (var m = 0; m < $scope.TreeOrganization[0].OrgChild.length; m++) {
                                for (var n = 0; n < $scope.TreeOrganization[0].OrgChild[m].OrgChild.length; n++) {
                                    for (var x = 0; x < $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild.length; x++) {
                                        if ($scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgUnit == $scope.Organization[i].OrgParent) {
                                            $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild.push($scope.Organization[i]);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        else if ($scope.Organization[i].OrgLevel == 5) {
                            for (var m = 0; m < $scope.TreeOrganization[0].OrgChild.length; m++) {
                                for (var n = 0; n < $scope.TreeOrganization[0].OrgChild[m].OrgChild.length; n++) {
                                    for (var x = 0; x < $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild.length; x++) {
                                        for (var y = 0; y < $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild.length; y++) {
                                            if ($scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild[y].OrgUnit == $scope.Organization[i].OrgParent) {
                                                $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild[y].OrgChild.push($scope.Organization[i]);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }


            $scope.changeRadioSelected = function () {

                $scope.MsgValidate = '';

                $scope.SelectedEmployeeID = '';
                $scope.SelectedEmployeeName = '';
                $scope.SelectedPositionID = '';
                $scope.SelectedPositionName = '';
                $scope.SelectedOrgUnitID = '';
                $scope.SelectedOrgUnitName = '';
                $scope.ObjEmp = null;

                $scope.clearTreeList($scope.Organization[0]);
            };


            $scope.test_data = function (item) {
                console.log(item);
            };

            $scope.getSelectedEmployee = function (item) {

                $scope.MsgValidate = '';

                $scope.ObjEmp = null;
                $scope.SelectedEmployeeID = item.EmployeeID;
                $scope.SelectedEmployeeName = item.Name;
                $scope.SelectedPositionID = item.Position;

                GetEmpData(item.EmployeeID, item.Position);
            };


            function GetEmpData(empID, positionID) {
                var URL = CONFIG.SERVER + 'HRPY/GetEmpData';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "EmployeeID": empID, "PositionID": positionID, "SelectCompany": $scope.SelectCompanyCode }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.ObjEmp = response.data;

                    if ($scope.ObjEmp != null && $scope.ObjEmp.length > 0) {
                        $scope.SelectedPositionName = $scope.ObjEmp[0].PositionName;
                        $scope.SelectedOrgUnitID = $scope.ObjEmp[0].OrgUnitID;
                        $scope.SelectedOrgUnitName = $scope.ObjEmp[0].OrgUnitName;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }


            $scope.onViewReport = function () {
                console.log('show report');
            };



            $scope.GetReport = function (exportType) {

                console.log('export');

                $scope.SelectedListOrg = '';

                for (var m = 0; m < $scope.TreeOrganization[0].OrgChild.length; m++) {
                    if ($scope.TreeOrganization[0].OrgChild[m].Selected) {
                        $scope.SelectedListOrg += $scope.TreeOrganization[0].OrgChild[m].OrgUnit + ',';
                    }
                    for (var n = 0; n < $scope.TreeOrganization[0].OrgChild[m].OrgChild.length; n++) {
                        if ($scope.TreeOrganization[0].OrgChild[m].OrgChild[n].Selected) {
                            $scope.SelectedListOrg += $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgUnit + ',';
                        }
                        for (var x = 0; x < $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild.length; x++) {
                            if ($scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].Selected) {
                                $scope.SelectedListOrg += $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgUnit + ',';
                            }
                            for (var y = 0; y < $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild.length; y++) {
                                if ($scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild[y].Selected) {
                                    $scope.SelectedListOrg += $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild[y].OrgUnit + ',';
                                }
                                for (var z = 0; z < $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild[y].OrgChild.length; z++) {
                                    if ($scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild[y].OrgChild[z].Selected) {
                                        $scope.SelectedListOrg += $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild[y].OrgChild[z].OrgUnit + ',';
                                    }
                                    for (var i = 0; i < $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild[y].OrgChild[z].OrgChild.length; i++) {
                                        if ($scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild[y].OrgChild[z].OrgChild[i].Selected) {
                                            $scope.SelectedListOrg += $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild[y].OrgChild[z].OrgChild[i].OrgUnit + ',';
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if ($scope.rdoSelected == "1") {
                    $scope.SelectedEmployeeID = $scope.employeeData.EmployeeID;
                    $scope.SelectedEmployeeName = $scope.employeeData.Name;
                    $scope.SelectedPositionName = $scope.employeeData.Position;
                    $scope.SelectedOrgUnitName = $scope.employeeData.OrgUnitName;
                    //$scope.GetAttendanceMonthlyExport(exportType, '1', $scope.SelectedListOrg);

                    if ($scope.SelectedListOrg == '') {
                        $scope.MsgValidate = $scope.Text["TAXREPORT"].SELECTORG;
                    }
                    else {
                        $scope.MsgValidate = '';
                        $scope.GetAttendanceMonthlyExport(exportType, '1', $scope.SelectedListOrg);
                    }

                }
                else {
                    if ($scope.SelectedEmployeeID == '') {
                        $scope.MsgValidate = $scope.Text["AttendanceMonthlyReport"].SELECTEMPLOYEE;
                    }
                    else {
                        $scope.MsgValidate = '';
                        $scope.GetAttendanceMonthlyExport(exportType, '2', '');
                        //$scope.GetAttendanceMonthlyExport('PDF', '2', '');

                    }
                }
            };


            function GetReportType() {
                var URL = CONFIG.SERVER + 'HRPY/GetReportType';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: { "SelectCompany": $scope.SelectCompanyCode }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.ListReportType = response.data;

                    if ($scope.ListReportType != null && $scope.ListReportType.length > 0) {
                        $scope.ddlReportType = $scope.ListReportType[0].ReportTypeValue;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }


            $scope.GetAttendanceMonthlyExport = function (type_download, ByOrg, ListOrg) {
                var str_category_export = "";
                var str_status_export = "";
                var type_file = "";
                $scope.loader.enable = true;
                angular.forEach($scope.selection_category, function (value) {
                    str_category_export += value + ",";
                });

                angular.forEach($scope.selection_status, function (value) {
                    str_status_export += value + ",";
                });

                if (type_download == 'PDF') {
                    type_file = 'pdf';
                } else {
                    type_file = 'xlsx';
                }

                if (type_download == 'RDLC') {
                    var URL = CONFIG.SERVER + 'HRTM/ShowAttendanceMonthlyReport';

                    $scope.employeeData = getToken(CONFIG.USER);
                    console.log('set employee-->' + $scope.employeeData);
                    var oRequestParameter = {
                        InputParameter: {
                            Type: str_category_export
                            , Status: str_status_export
                            , ReportName: $scope.Text["APPLICATION"].AttendanceMonthlyReport
                            , LanguageCode: $scope.employeeData.Language
                            , Employee_id: $scope.SelectedEmployeeID
                            , EmployeeName: $scope.SelectedEmployeeName
                            , PositionName: $scope.SelectedPositionName
                            , OrgUnitName: $scope.SelectedOrgUnitName
                            , ExportType: type_download //"PDF" //EXCEL
                            , ByOrg: ByOrg
                            , ListOrg: ListOrg
                            , FromDate: $scope.model.StartDate
                            , ToDate: $scope.model.EndDate
                            , "SelectCompany": $scope.SelectCompanyCode
                        }
                        , CurrentEmployee: $scope.employeeData
                        , Requestor: $scope.requesterData

                    };

                    $scope.loader.enable = true;
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {

                        var file_name = "rptAttendanceReport" + $scope.requesterData.EmployeeID;

                        //if ($scope.model.ExportTye === "PDF") {
                        //    file_name += ".pdf";
                        //}
                        //else if ($scope.model.ExportTye === "EXCEL") {
                            file_name += ".xls";
                        //}

                        var url = CONFIG.SERVER + 'Client/Report/' + file_name;
                        download(url, file_name);

                        $scope.loader.enable = false;

                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                    });

                } else {
                    var URL = CONFIG.SERVER + 'HRTM/GetAttendanceMonthlyExport';

                    $scope.employeeData = getToken(CONFIG.USER);
                    console.log('set employee-->' + $scope.employeeData);
                    var oRequestParameter = {
                        InputParameter: {
                            Type: str_category_export
                            , Status: str_status_export
                            , ReportName: $scope.Text["APPLICATION"].AttendanceMonthlyReport
                            , LanguageCode: $scope.employeeData.Language
                            , Employee_id: $scope.SelectedEmployeeID
                            , EmployeeName: $scope.SelectedEmployeeName
                            , PositionName: $scope.SelectedPositionName
                            , OrgUnitName: $scope.SelectedOrgUnitName
                            , ExportType: type_download //"PDF" //EXCEL
                            , ByOrg: ByOrg
                            , ListOrg: ListOrg
                            , FromDate: $scope.model.StartDate
                            , ToDate: $scope.model.EndDate
                            , "SelectCompany": $scope.SelectCompanyCode
                        }
                        , CurrentEmployee: $scope.employeeData
                        , Requestor: $scope.requesterData

                    };

                    var config = {
                        responseType: "arraybuffer",
                        cache: false
                    };

                    var apiData = { url: "", fileName: "" };
                    var now = new Date();
                    var now_time = now.getUTCFullYear() + "" + now.getUTCMonth() + "" + now.getUTCDate() + "" + now.getHours() + "" + now.getUTCMinutes() + "" + now.getUTCSeconds();
                    apiData.fileName = "ReportAttendanceMonthly" + now_time + ".xlsx";

                    $scope.loader.enable = true;
                    $http.post(URL, oRequestParameter, config)
                        .then(function successCallback(response) {

                            var blob = new Blob([response.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
                            saveAs(blob, apiData.fileName);
                            $scope.loader.enable = false;
                        }, function errorCallback(response) {
                            $scope.loader.enable = false;
                        });

                }
                
            };

            $scope.clearTreeList = function (obj) {

                obj.Selected = false;
                for (var i = 0; i < obj.OrgChild.length; i++) {
                    obj.OrgChild[i].Selected = false;
                    for (var j = 0; j < obj.OrgChild[i].OrgChild.length; j++) {
                        obj.OrgChild[i].OrgChild[j].Selected = false;
                        for (var k = 0; k < obj.OrgChild[i].OrgChild[j].OrgChild.length; k++) {
                            obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = false;
                            for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = false;
                                for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = false;
                                }
                            }
                        }
                    }
                }

            };

            $scope.selectOrgLV1 = function (items, obj) {
                if (!items) return;

                if (obj.Selected == true) {
                    obj.Selected = false;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = false;
                        for (var j = 0; j < obj.OrgChild[i].OrgChild.length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = false;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].OrgChild.length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = false;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = false;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = false;
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    obj.Selected = true;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = true;
                        for (var j = 0; j < obj.OrgChild[i].OrgChild.length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = true;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].OrgChild.length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = true;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = true;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = true;
                                    }
                                }
                            }
                        }
                    }
                }
            };

            $scope.selectOrgLV2 = function (items, obj) {
                if (!items) return;

                if (obj.Selected == true) {
                    obj.Selected = false;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = false;
                        for (var j = 0; j < obj.OrgChild[i].OrgChild.length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = false;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].OrgChild.length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = false;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = false;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = false;
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    obj.Selected = true;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = true;
                        for (var j = 0; j < obj.OrgChild[i].OrgChild.length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = true;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].OrgChild.length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = true;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = true;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = true;
                                    }
                                }
                            }
                        }
                    }
                }
            };

            $scope.selectOrgLV3 = function (items, obj) {
                if (!items) return;

                if (obj.Selected == true) {
                    obj.Selected = false;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = false;
                        for (var j = 0; j < obj.OrgChild[i].OrgChild.length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = false;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].OrgChild.length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = false;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = false;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = false;
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    obj.Selected = true;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = true;
                        for (var j = 0; j < obj.OrgChild[i].OrgChild.length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = true;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].OrgChild.length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = true;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = true;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = true;
                                    }
                                }
                            }
                        }
                    }
                }
            };

            $scope.selectOrgLV4 = function (items, obj) {
                if (!items) return;

                if (obj.Selected == true) {
                    obj.Selected = false;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = false;
                        for (var j = 0; j < obj.OrgChild[i].OrgChild.length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = false;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].OrgChild.length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = false;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = false;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = false;
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    obj.Selected = true;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = true;
                        for (var j = 0; j < obj.OrgChild[i].OrgChild.length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = true;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].OrgChild.length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = true;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = true;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = true;
                                    }
                                }
                            }
                        }
                    }
                }
            };

            $scope.selectOrgLV5 = function (items, obj) {
                if (!items) return;

                if (obj.Selected == true) {
                    obj.Selected = false;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = false;
                        for (var j = 0; j < obj.OrgChild[i].OrgChild.length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = false;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].OrgChild.length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = false;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = false;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = false;
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    obj.Selected = true;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = true;
                        for (var j = 0; j < obj.OrgChild[i].length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = true;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = true;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = true;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = true;
                                    }
                                }
                            }
                        }
                    }
                }

            };


            $scope.selectOrgLV6 = function (items, obj) {
                if (!items) return;

                if (obj.Selected == true) {
                    obj.Selected = false;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = false;
                        for (var j = 0; j < obj.OrgChild[i].OrgChild.length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = false;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].OrgChild.length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = false;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = false;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = false;
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    obj.Selected = true;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = true;
                        for (var j = 0; j < obj.OrgChild[i].length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = true;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = true;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = true;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = true;
                                    }
                                }
                            }
                        }
                    }
                }

            };

            $scope.exists = function (item) {
                if (!item) return;
                return $scope.data.selectedList.indexOf(item) > -1;
            };

            $scope.exists_group = function (items) {
                if (!items) return;
                var count = 0;
                for (var i = 0; i < items.length; i++) {
                    if (isSelected(items[i])) {
                        count++;
                    }
                }
                return count == items.length;
            };

            $scope.exists_groups = function (items) {
                if (!items) return;
                var count = 0;
                var length = 0;
                for (var i = 0; i < items.length; i++) {
                    for (var a = 0; a < items[i].Indicator.length; a++) {
                        if (isSelected(items[i].Indicator[a])) {
                            count++;
                        }
                        length++;
                    }
                }
                return count == length;
            };

            function isSelected(item) {
                if (!item) return;

                for (var i = 0; i < $scope.data.selectedList.length; i++) {
                    if (item.ObjectiveID === $scope.data.selectedList[i].ObjectiveID && item.IndicatorID == $scope.data.selectedList[i].IndicatorID) {
                        return true;
                    }
                }
                return false;
            }


            // #AutoComplete EmployeeList
            function createFilterForListEmp(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    if (!x) return false;
                    var source = angular.lowercase(x.EmployeeID1 + ":" + x.EmployeeName1);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }

            $scope.querySearchListEmp = function (query) {
                if (!$scope.ListEmp) return;
                var results = angular.copy(query ? $scope.ListEmp.filter(createFilterForListEmp(query)) : $scope.ListEmp), deferred;
                results = results.splice(0, 500);
                return results;
            };

            $scope.selectedItemListEmpChange = function (item) {
                if (angular.isDefined(item) && item != null) {
                    $scope.ap_pdr_text.cc = item.EmployeeID1 + " : " + item.EmployeeName1;
                    //$scope.TempEmp.CCCode = item.EmployeeID1;
                    tempResult_cc = angular.copy(item);
                }
                else {
                    $scope.ap_pdr_text.cc = '';
                }
            };

            $scope.tryToSelect_cc = function (text) {
                $scope.ap_pdr_text.cc = '';
                //$scope.TempEmp.CCCode = '';
            };

            $scope.checkText_cc = function (text) {
                if (text == '') {
                    $scope.ap_pdr_text.cc = '';
                    //$scope.TempEmp.CCCode = '';
                }
                else
                    $scope.ap_pdr_text.cc = text;
            };




            // Open Modal Search Customer
            $scope.openModalSearchCustomer = function () {

                var modalInstance = $uibModal.open({
                    animation: $scope.animationsEnabled,
                    templateUrl: 'modalSearchCustomerAttendanceMonthly.ng-popup.html',
                    controller: 'modalSearchCustomerAttendanceMonthlyController',
                    backdrop: 'static',
                    keyboard: false,
                    size: 'spp',
                    resolve: {
                        items: function () {
                            var data = {
                                Text: $scope.Text,
                                requesterData: $scope.requesterData,
                                SelectCompany: $scope.SelectCompanyCode,
                                fnSeelctEmployee: function (item) {
                                    $scope.getSelectedEmployee(item);
                                }
                            };
                            return data;
                        }
                    }
                });
            };

            $scope.clearDataCustomer = function () {

                $scope.SelectedEmployeeID = '';
                $scope.SelectedEmployeeName = '';
                $scope.SelectedPositionID = '';
                $scope.SelectedPositionName = '';
                $scope.SelectedOrgUnitID = '';
                $scope.SelectedOrgUnitName = '';
                $scope.ObjEmp = null;

            };
        }])
        .controller('errorExportPdfController', ['items', '$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', '$uibModalInstance', function (items, $scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal, $uibModalInstance) {

            $scope.closeModal = function () {
                $uibModalInstance.close();
            };

        }])
        .controller('modalSearchCustomerAttendanceMonthlyController', ['items', '$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', '$uibModalInstance', function (items, $scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal, $uibModalInstance) {

            console.log(items);
            $scope.text_db = items.Text;  // Text;
            $scope.requestorData = items.requesterData; // Requestor
            $scope.SelectCompanyCode = items.SelectCompany;
            $scope.list_emp = [];

            $scope.searchEmployee = function (textSearch) {

                var URL = CONFIG.SERVER + 'HRPY/GetEmployeeAllSystem';

                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {
                        "SEARCHTEXT": textSearch, "SelectCompany": $scope.SelectCompanyCode
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requestorData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    console.log(response);

                    $scope.list_emp = response.data.Table;

                }, function errorCallback(response) {
                    console.log(response);
                });

            };

            $scope.selectEmployee = function (item) {

                items.fnSeelctEmployee(item);
                $uibModalInstance.close();
            };

            $scope.closeModal = function () {
                $uibModalInstance.close();
            };

        }]);
})();