﻿(function () {
    angular.module('ESSMobile')
        .controller('OTLogReportController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal) {

            $scope.isHideMultiCompany = true;
            $scope.GetConfingMultiCompany = function () {
                var URL = CONFIG.SERVER + 'Employee/GetConfigMultiCompany';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data === 1) {
                        $scope.isHideMultiCompany = false;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetConfingMultiCompany();

            var d = new Date();
            $scope.model = {
                StartDate: $filter('date')(d, 'yyyy-MM-dd'),
                EndDate: $filter('date')(d, 'yyyy-MM-dd'),
                ListStatus: "",
                ListOrgTree: "",
                ExportTye: "EXCEL"
            };

            $scope.back = function () {
                var url = '/frmViewContent/5000';
                $location.path(url);
            };

            // Support Admin Multi Company
            $scope.employeeData = getToken(CONFIG.USER);
            $scope.selectCompanyCode = $scope.employeeData.CompanyCode; // Default CompanyCode GPSC
            $scope.CompanyList = [];

            // Get List Authorization Comapny In Employee Admin
            $scope.GetAuthorizationCompany = function () {
                var URL = CONFIG.SERVER + 'workflow/GetAuthorizationCompany';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.CompanyList = response.data;

                    // จะได้ List Company มาไว้ใน Dropdown
                    //if ($scope.CompanyList.length > 0) {
                    //    $scope.selectCompanyCode = $scope.CompanyList[0].CompanyCode;
                    //}

                    // Company Is Value
                    if ($scope.selectCompanyCode) {
                        $scope.GetOrganization();
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetAuthorizationCompany();

            // Change Company Reload Data Export Report
            $scope.changeCompany = function () {
                if ($scope.selectCompanyCode) {
                    $scope.GetOrganization();
                }
            };

            function download(dataurl, filename) {
                var a = document.createElement("a");
                a.href = dataurl;
                a.setAttribute("download", filename);
                var b = document.createEvent("MouseEvents");
                b.initEvent("click", false, true);
                a.dispatchEvent(b);
                return false;
            }

            // Tree Organizetion
            $scope.TreeOrganization = [];
            $scope.GetOrganization = function () {
                var URL = CONFIG.SERVER + 'HRPY/GetOrganization';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {
                        "SelectCompany": $scope.selectCompanyCode,
                        "SubjectID": $scope.contentInfo.SubjectID
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.TreeOrganization = [];

                    $scope.Organization = response.data;

                    $scope.Organization.forEach(ele => {
                        ele.IsExpanding = true;
                        ele.OrgChild = [];
                        ele.Selected = false;
                        ele.Disable = false;
                        //$scope.TreeOrganization[j].Objective = [];
                    });

                    for (var i = 0; i < $scope.Organization.length; i++) {
                        if ($scope.Organization[i].OrgLevel == 0) {
                            $scope.TreeOrganization.push($scope.Organization[i]);
                        }
                        else if ($scope.Organization[i].OrgLevel == 1) {
                            for (var j = 0; j < $scope.TreeOrganization.length; j++) {
                                if ($scope.TreeOrganization[0].OrgUnit == $scope.Organization[i].OrgParent) {
                                    $scope.TreeOrganization[0].OrgChild.push($scope.Organization[i]);
                                    break;
                                }
                            }
                        }
                        else if ($scope.Organization[i].OrgLevel == 2) {
                            for (var k = 0; k < $scope.TreeOrganization[0].OrgChild.length; k++) {
                                if ($scope.TreeOrganization[0].OrgChild[k].OrgUnit == $scope.Organization[i].OrgParent) {
                                    $scope.TreeOrganization[0].OrgChild[k].OrgChild.push($scope.Organization[i]);
                                    break;
                                }
                            }
                        }
                        else if ($scope.Organization[i].OrgLevel == 3) {
                            for (var m = 0; m < $scope.TreeOrganization[0].OrgChild.length; m++) {
                                for (var n = 0; n < $scope.TreeOrganization[0].OrgChild[m].OrgChild.length; n++) {
                                    if ($scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgUnit == $scope.Organization[i].OrgParent) {
                                        $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild.push($scope.Organization[i]);
                                        break;
                                    }
                                }
                            }
                        }
                        else if ($scope.Organization[i].OrgLevel == 4) {
                            for (var m = 0; m < $scope.TreeOrganization[0].OrgChild.length; m++) {
                                for (var n = 0; n < $scope.TreeOrganization[0].OrgChild[m].OrgChild.length; n++) {
                                    for (var x = 0; x < $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild.length; x++) {
                                        if ($scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgUnit == $scope.Organization[i].OrgParent) {
                                            $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild.push($scope.Organization[i]);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        else if ($scope.Organization[i].OrgLevel == 5) {
                            for (var m = 0; m < $scope.TreeOrganization[0].OrgChild.length; m++) {
                                for (var n = 0; n < $scope.TreeOrganization[0].OrgChild[m].OrgChild.length; n++) {
                                    for (var x = 0; x < $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild.length; x++) {
                                        for (var y = 0; y < $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild.length; y++) {
                                            if ($scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild[y].OrgUnit == $scope.Organization[i].OrgParent) {
                                                $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild[y].OrgChild.push($scope.Organization[i]);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            // $scope.GetOrganization(); // Load Organizetion

            // Select Organizetion
            $scope.selectOrgLV1 = function (items, obj) {
                if (!items) return;

                if (obj.Selected == true) {
                    obj.Selected = false;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = false;
                        for (var j = 0; j < obj.OrgChild[i].OrgChild.length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = false;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].OrgChild.length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = false;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = false;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = false;
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    obj.Selected = true;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = true;
                        for (var j = 0; j < obj.OrgChild[i].OrgChild.length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = true;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].OrgChild.length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = true;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = true;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = true;
                                    }
                                }
                            }
                        }
                    }
                }
            };
            $scope.selectOrgLV2 = function (items, obj) {
                if (!items) return;

                if (obj.Selected == true) {
                    obj.Selected = false;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = false;
                        for (var j = 0; j < obj.OrgChild[i].OrgChild.length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = false;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].OrgChild.length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = false;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = false;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = false;
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    obj.Selected = true;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = true;
                        for (var j = 0; j < obj.OrgChild[i].OrgChild.length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = true;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].OrgChild.length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = true;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = true;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = true;
                                    }
                                }
                            }
                        }
                    }
                }
            };
            $scope.selectOrgLV3 = function (items, obj) {
                if (!items) return;

                if (obj.Selected == true) {
                    obj.Selected = false;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = false;
                        for (var j = 0; j < obj.OrgChild[i].OrgChild.length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = false;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].OrgChild.length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = false;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = false;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = false;
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    obj.Selected = true;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = true;
                        for (var j = 0; j < obj.OrgChild[i].OrgChild.length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = true;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].OrgChild.length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = true;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = true;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = true;
                                    }
                                }
                            }
                        }
                    }
                }
            };
            $scope.selectOrgLV4 = function (items, obj) {
                if (!items) return;

                if (obj.Selected == true) {
                    obj.Selected = false;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = false;
                        for (var j = 0; j < obj.OrgChild[i].OrgChild.length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = false;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].OrgChild.length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = false;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = false;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = false;
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    obj.Selected = true;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = true;
                        for (var j = 0; j < obj.OrgChild[i].OrgChild.length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = true;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].OrgChild.length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = true;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = true;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = true;
                                    }
                                }
                            }
                        }
                    }
                }
            };
            $scope.selectOrgLV5 = function (items, obj) {
                if (!items) return;

                if (obj.Selected == true) {
                    obj.Selected = false;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = false;
                        for (var j = 0; j < obj.OrgChild[i].OrgChild.length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = false;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].OrgChild.length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = false;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = false;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = false;
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    obj.Selected = true;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = true;
                        for (var j = 0; j < obj.OrgChild[i].length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = true;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = true;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = true;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = true;
                                    }
                                }
                            }
                        }
                    }
                }

            };

            $scope.selectOrgLV6 = function (items, obj) {
                if (!items) return;

                if (obj.Selected == true) {
                    obj.Selected = false;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = false;
                        for (var j = 0; j < obj.OrgChild[i].OrgChild.length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = false;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].OrgChild.length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = false;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = false;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = false;
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    obj.Selected = true;
                    for (var i = 0; i < obj.OrgChild.length; i++) {
                        obj.OrgChild[i].Selected = true;
                        for (var j = 0; j < obj.OrgChild[i].length; j++) {
                            obj.OrgChild[i].OrgChild[j].Selected = true;
                            for (var k = 0; k < obj.OrgChild[i].OrgChild[j].length; k++) {
                                obj.OrgChild[i].OrgChild[j].OrgChild[k].Selected = true;
                                for (var m = 0; m < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild.length; m++) {
                                    obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].Selected = true;
                                    for (var n = 0; n < obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild.length; n++) {
                                        obj.OrgChild[i].OrgChild[j].OrgChild[k].OrgChild[m].OrgChild[n].Selected = true;
                                    }
                                }
                            }
                        }
                    }
                }

            };

            $scope.listStatusReport = [];
            $scope.loadStatusReport = function () {
                var i;
                for (i = 0; i < 10; i++) {
                    if (i === 7) {
                        continue;
                    }
                    var txtStatus = $scope.Text['DailyOTLogStatus'][i.toString()];
                    var objStatus = {
                        TextStatus: txtStatus,
                        Value: i,
                        flag: false
                    };
                    $scope.listStatusReport.push(objStatus);
                }
            };
            $scope.loadStatusReport();

            $scope.setBeginDate = function (selectedDate) {
                $scope.model.StartDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };

            $scope.setEndDate = function (selectedDate) {
                $scope.model.EndDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };

            $scope.is_check_all = false;
            $scope.checkall = function () {

                if ($scope.is_check_all === false) {
                    $scope.is_check_all = true;
                }
                else {
                    $scope.is_check_all = false;
                }

                if ($scope.is_check_all == true) {
                    angular.forEach($scope.listStatusReport, function (obj, key) {
                        obj.flag = true;
                    });
                } else {
                    angular.forEach($scope.listStatusReport, function (obj, key) {
                        obj.flag = false;
                    });
                }
            };

            $scope.exportReport = function (type) {

                var modalInstance = "";

                if ($scope.listStatusReport.length <= 0) {
                    // ไม่มีรายการ Status Error 
                    return;
                }

                $scope.tempSelectStatus = $scope.listStatusReport.filter(function (element) {
                    return element.flag === true;
                });

                if ($scope.tempSelectStatus.length <= 0) {

                    modalInstance = $uibModal.open({
                        animation: $scope.animationsEnabled,
                        templateUrl: 'alertWarning.ng-popup.html',
                        controller: 'alertWarningController',
                        backdrop: 'static',
                        keyboard: false,
                        size: 'sp',
                        resolve: {
                            items: function () {
                                var data = {
                                    typeError: 2,
                                    text: $scope.Text
                                };
                                return data;
                            }
                        }
                    });
                    $scope.model.ListStatus = "";
                    return;
                }

                $scope.SelectedListOrg = '';
                for (var m = 0; m < $scope.TreeOrganization[0].OrgChild.length; m++) {
                    if ($scope.TreeOrganization[0].OrgChild[m].Selected) {
                        $scope.SelectedListOrg += $scope.TreeOrganization[0].OrgChild[m].OrgUnit + ',';
                    }
                    for (var n = 0; n < $scope.TreeOrganization[0].OrgChild[m].OrgChild.length; n++) {
                        if ($scope.TreeOrganization[0].OrgChild[m].OrgChild[n].Selected) {
                            $scope.SelectedListOrg += $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgUnit + ',';
                        }
                        for (var x = 0; x < $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild.length; x++) {
                            if ($scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].Selected) {
                                $scope.SelectedListOrg += $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgUnit + ',';
                            }
                            for (var y = 0; y < $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild.length; y++) {
                                if ($scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild[y].Selected) {
                                    $scope.SelectedListOrg += $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild[y].OrgUnit + ',';
                                }
                                for (var z = 0; z < $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild[y].OrgChild.length; z++) {
                                    if ($scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild[y].OrgChild[z].Selected) {
                                        $scope.SelectedListOrg += $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild[y].OrgChild[z].OrgUnit + ',';
                                    }
                                    for (var i = 0; i < $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild[y].OrgChild[z].OrgChild.length; i++) {
                                        if ($scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild[y].OrgChild[z].OrgChild[i].Selected) {
                                            $scope.SelectedListOrg += $scope.TreeOrganization[0].OrgChild[m].OrgChild[n].OrgChild[x].OrgChild[y].OrgChild[z].OrgChild[i].OrgUnit + ',';
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // Org Tree
                if (!$scope.SelectedListOrg) {
                    modalInstance = $uibModal.open({
                        animation: $scope.animationsEnabled,
                        templateUrl: 'alertWarning.ng-popup.html',
                        controller: 'alertWarningController',
                        backdrop: 'static',
                        keyboard: false,
                        size: 'sp',
                        resolve: {
                            items: function () {
                                var data = {
                                    typeError: 3,
                                    text: $scope.Text
                                };
                                return data;
                            }
                        }
                    });
                    return;
                }

                $scope.model.ListOrgTree = $scope.SelectedListOrg;

                // Status Report
                var i = 0;
                angular.forEach($scope.tempSelectStatus, function (obj, key) {

                    if (i === 0) {
                        $scope.model.ListStatus = obj.Value;
                        i++;
                    }
                    else {
                        $scope.model.ListStatus = $scope.model.ListStatus + "," + obj.Value;
                    }
                });


                var URL = CONFIG.SERVER + 'HRTM/ExportOtLogReportByAdmin';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {
                        SelectCompany: $scope.selectCompanyCode,
                        StartDate: $scope.model.StartDate,
                        EndDate: $scope.model.EndDate,
                        ListStatus: $scope.model.ListStatus,
                        ListOrgTree: $scope.model.ListOrgTree,
                        ExportTye: $scope.model.ExportTye
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    var file_name = "OTLogReportForAdmin" + $scope.requesterData.EmployeeID;

                    if ($scope.model.ExportTye === "PDF") {
                        file_name += ".pdf";
                    }
                    else if ($scope.model.ExportTye === "EXCEL") {
                        file_name += ".xls";
                    }

                    var url = CONFIG.SERVER + 'Client/Report/' + file_name;
                    download(url, file_name);

                    $scope.loader.enable = false;

                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };

        }])
        .controller('alertWarningController', ['items', '$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', '$uibModalInstance', function (items, $scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal, $uibModalInstance) {

            $scope.Text = items.text;
            $scope.typeError = items.typeError;

            $scope.txt_warnig = '';
            if ($scope.typeError === 2) {
                $scope.txt_warnig = $scope.Text['OTLOGREPORT']['EXEPTION_STATUS_NULL'];
            } else if ($scope.typeError === 3) {
                $scope.txt_warnig = $scope.Text['OTLOGREPORT']['EXEPTION_ORG_NULL'];
            }

            $scope.closeModal = function () {
                $uibModalInstance.close();
            };

        }]);
})();