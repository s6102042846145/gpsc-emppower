﻿(function () {
    angular.module('ESSMobile')
        .controller('ChangeWorkingTimeTypeController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal) {

            // day , month , year now
            $scope.date_now = new Date();
            $scope.year_now = $scope.date_now.getFullYear().toString();
            $scope.IsCheckUserRole = false;

            $scope.subGroup = 0;
            $scope.GetEmpSubGroup = function () {

                var URL = CONFIG.SERVER + 'HRTM/GetEmpSubGroup';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.subGroup = response.data;
                    console.log(response);
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetEmpSubGroup();

            $scope.GetIsCheckUserRole = function () {

                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/GetIsCheckUserRole',
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.IsCheckUserRole = response.data;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetIsCheckUserRole();

            // Content By Year
            $scope.listContentChangeWorking = [];
            $scope.loadContentByYear = function (dataYear) {

                var URL = CONFIG.SERVER + 'HRTM/GetContentChangeWorkingByYear';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {
                        Year: dataYear
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.listContentChangeWorking = response.data;
                    $scope.loader.enable = false;

                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });

            };

            // List Year
            $scope.objSelectYear = {
                YearSelect: ""
            };
            $scope.listYear = [];
            $scope.loadYear = function () {

                var URL = CONFIG.SERVER + 'HRTM/GetYearAreaWorkschedule';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.listYear = [];
                    if (response.data.length > 0) {

                        // search index year now
                        var index_year_now = -1;
                        for (var i = 0; i < response.data.length; i++) {
                            if (response.data[i] == $scope.year_now) {
                                index_year_now = i;
                                break;
                            }
                        }

                        $scope.listYear = response.data;
                        if (index_year_now > -1) {
                            $scope.objSelectYear.YearSelect = $scope.listYear[index_year_now];
                            $scope.loadContentByYear($scope.objSelectYear.YearSelect);
                        }
                        else {
                            $scope.objSelectYear.YearSelect = $scope.listYear[0];
                            $scope.loadContentByYear($scope.objSelectYear.YearSelect);
                        }
                    }
                    $scope.loader.enable = false;

                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });

            };
            $scope.loadYear();

            $scope.viewDocument = function (requestNo) {

                if (angular.isDefined(requestNo)) {

                    //var URL = CONFIG.SERVER + 'HRTM/GetPeriodDocument';
                    //$scope.employeeData = getToken(CONFIG.USER);

                    //var oRequestParameter = {
                    //    InputParameter: {
                    //        RequestNo: requestNo
                    //    }
                    //    , CurrentEmployee: $scope.employeeData
                    //    , Requestor: $scope.requesterData
                    //    , Creator: getToken(CONFIG.USER)
                    //};

                    //oRequestParameter.RequestNo = requestNo;
                    //$http({
                    //    method: 'POST',
                    //    url: URL,
                    //    data: oRequestParameter
                    //}).then(function successCallback(response) {
                    //    $scope.LetterDocument = response.data;

                    //    console.log(response);

                    if (typeof cordova != 'undefined') {
                        console.log('');
                    } else {
                        //var path = CONFIG.SERVER + 'Client/index.html#!/frmViewRequest/' + requestNo + '/' + $scope.requesterData.CompanyCode + '/' + $scope.LetterDocument.__keyMaster + '/' + 'true' + '/' + 'false';
                        var path = CONFIG.SERVER + 'Client/index.html#!/frmViewRequest/' + requestNo + '/' + $scope.requesterData.CompanyCode + '/' + null + '/' + 'true' + '/' + 'true';

                        //window.open(path, '_blank');
                        $scope.rootNewWindowTab(path, '', 1300, 700);
                    }

                    //    $scope.loader.enable = false;
                    //}, function errorCallback(response) {
                    //    $scope.loader.enable = false;
                    //});
                }

            };
        }]);
})();