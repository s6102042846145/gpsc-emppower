﻿(function () {
    angular.module('ESSMobile')
        .controller('OTSummaryByAdminAndSupervisorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', '$q', '$timeout', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal, $q, $timeout) {

            var optionsnumber = {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            };

            $scope.profile = getToken(CONFIG.USER);

            $scope.navigation_load_chart = 0;

            var d = new Date();
            var begin_year = d.getFullYear();
            var end_year = d.getFullYear();

            $scope.list_unit_all = [];
            $scope.loadOrgunit = function (begin_year, end_year) {

                var URL = CONFIG.SERVER + 'HRTM/GetAllOrgUnitByRole';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {
                        sYear: begin_year,
                        eYear: end_year
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {

                    $scope.list_unit_all = response.data;
                    $scope.loader.enable = false;

                    // โหลด Tab 1 เสมอสำหรับ Load Org Unit ครั้งแรก
                    if ($scope.list_unit_all.length > 0) {
                        $scope.navigation_load_chart = 1;
                        $scope.SelectTabl1_YTD_OvertimeandSalary();
                    }
                }, function (response) {
                    console.log(response);
                    $scope.loader.enable = false;
                });
            };
            $scope.loadOrgunit(begin_year, end_year);

            $scope.list_year_tab2 = [];
            $scope.select_year_tab2 = "";
            $scope.loadYearTab2 = function () {

                var URL = CONFIG.SERVER + 'HRTM/GetYearDashboard2';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                //$scope.loader.enable = true;
                $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {

                    //$scope.loader.enable = false;
                    $scope.list_year_tab2 = response.data;
                    $scope.select_year_tab2 = response.data[0];

                }, function (response) {
                    console.log(response);
                    $scope.loader.enable = false;
                });

            };
            $scope.loadYearTab2();

            $scope.list_year_tab3 = [];
            $scope.begin_year_tab3 = "";
            $scope.end_year_tab3 = "";
            $scope.loadYearTab3 = function () {

                var URL = CONFIG.SERVER + 'HRTM/GetYearDashboard3';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {
                    $scope.list_year_tab3 = response.data;
                    $scope.begin_year_tab3 = response.data[0];
                    $scope.end_year_tab3 = response.data[0];

                }, function (response) {
                    console.log(response);
                    $scope.loader.enable = false;
                });

            };
            $scope.loadYearTab3();


            $scope.select_month_tab2 = "01";
            $scope.dashboard_month = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];

            // เข้าครั้งแรก
            $scope.demo = '1';
            $scope.demo2_1 = '1';
            $scope.demo3_1 = '1';
            $scope.change_tab = function (tabId) {

                $scope.demo = tabId;
                switch (tabId) {
                    case '1': $scope.SelectTabl1_YTD_OvertimeandSalary();
                        $scope.navigation_load_chart = 1;
                        break;
                    case '2': $scope.loadDashboardOvertimeBUAndOTReasons();
                        $scope.navigation_load_chart = 2.1;
                        break;
                    case '3': $scope.loadSummaryOTAndSalary_Year();
                        $scope.navigation_load_chart = 3.1;
                        break;
                }
            };

            $scope.change_sub_tab2 = function (subTabId) {

                var URL = CONFIG.SERVER + 'HRTM/GetAllOrgUnitByRole';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {
                        sYear: begin_year,
                        eYear: end_year
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {

                    $scope.list_unit_all = response.data;
                    $scope.loader.enable = false;

                    $scope.demo2_1 = subTabId;
                    switch (subTabId) {
                        case '1': $scope.loadDashboardOvertimeBUAndOTReasons();
                            $scope.navigation_load_chart = 2.1;
                            break;
                        case '2': $scope.loadDashboardOvertimeAndSalaryInYear();
                            $scope.navigation_load_chart = 2.2;
                            break;
                        case '3':
                            $scope.navigation_load_chart = 2.3;
                            $scope.loadDashboardSummaryOTAndSalaryMonth(); break;
                    }


                }, function (response) {
                    console.log(response);
                    $scope.loader.enable = false;
                });


            };

            $scope.change_sub_tab3 = function (subTabId) {

                var URL = CONFIG.SERVER + 'HRTM/GetAllOrgUnitByRole';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {
                        sYear: begin_year,
                        eYear: end_year
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {

                    $scope.list_unit_all = response.data;
                    $scope.loader.enable = false;

                    $scope.demo3_1 = subTabId;
                    switch (subTabId) {
                        case '1':
                            $scope.loadSummaryOTAndSalary_Year();
                            $scope.navigation_load_chart = 3.1;
                            break;
                        case '2':
                            $scope.loadOvertimeDataAnalysisReport();
                            $scope.navigation_load_chart = 3.2;
                            break;
                    }

                }, function (response) {
                    console.log(response);
                    $scope.loader.enable = false;
                });

            };

            $scope.SplitOrganization = function () {

                $scope.data_org = "";
                if ($scope.now_unit === "0") {
                    if ($scope.list_unit_all.length > 0) {

                        for (var i = 0; i < $scope.list_unit_all.length; i++) {
                            if (i != 0)
                                $scope.data_org += "," + $scope.list_unit_all[i].OrgUnit;
                            else
                                $scope.data_org = $scope.list_unit_all[i].OrgUnit;
                        }
                    }
                }
                else {
                    if ($scope.list_unit_all.length > 0) {

                        for (var i = 0; i < $scope.list_unit_all.length; i++) {

                            var is_dup = false;
                            is_dup = $scope.list_unit_all[i].OrgUnitPath.includes($scope.now_unit);
                            if (is_dup) {
                                if ($scope.data_org == '') {
                                    $scope.data_org = $scope.list_unit_all[i].OrgUnit;

                                }
                                else {
                                    $scope.data_org += "," + $scope.list_unit_all[i].OrgUnit;
                                }
                            }
                        }
                    }
                }

                return $scope.data_org;
            };

            $scope.reloadOrgUnit = function (tab, begin_year, end_year, month, unit) {

                var URL = CONFIG.SERVER + 'HRTM/GetAllOrgUnitByRole';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {
                        sYear: begin_year,
                        eYear: end_year
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {

                    $scope.list_unit_all = response.data;
                    $scope.loader.enable = false;

                    switch (tab) {
                        case 1: break;
                        case 2:
                            $scope.loadAllDashbaordTab2(unit, begin_year, month);
                            break;
                        case 3:
                            $scope.load_grap2_2(unit, begin_year);
                            break;
                        case 4:
                            $scope.loadDashboardTabl2_3(unit, begin_year, month);
                            break;
                        case 5:
                            $scope.load_chart3_1_1();
                            $scope.load_chart3_1_2();
                            break;
                        case 6:
                            $scope.load_chart3_2_1();
                            $scope.load_chart3_2_2();
                            break;
                    }

                }, function (response) {
                    console.log(response);
                    $scope.loader.enable = false;
                });

            };

            $scope.searchText = '';
            $scope.autocomplete = {
                selectedItem: null
            };
            $scope.querySearchOrgUnit = querySearchOrgUnit;
            $scope.selectedItemChange = selectedItemChange;
            $scope.searchTextChange = searchTextChange;
            $scope.simulateQuery = true;

            // Auto Search Text Complete

            function querySearchOrgUnit(query) {

                var results = query ? $scope.list_unit_all.filter(createFilterFor(query)) : $scope.list_unit_all, deferred;
                if ($scope.simulateQuery) {
                    deferred = $q.defer();
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 250, false);
                    return deferred.promise;
                } else {
                    return results;
                }
            }

            function createFilterFor(query) {
                var lowercaseQuery = query.toLowerCase(query);
                return function filterFn(orgunit) {
                    var textquery = orgunit.OrgDisplay;
                    return textquery.toLowerCase().indexOf(lowercaseQuery) >= 0;
                };

            }

            function searchTextChange(text_query) {
            }

            var tempResult;
            function selectedItemChange(orgUnit) {
                // Assign ค่าและค้นหาจำนวนเงิน
                if (orgUnit) {

                    if (tempResult) {

                        if (orgUnit.OrgUnit != tempResult.OrgUnit) {
                            tempResult = orgUnit;

                            $scope.now_unit = tempResult.OrgUnit.toString();
                            switch ($scope.navigation_load_chart) {
                                case 1:
                                    $scope.loadDashbaordTab1($scope.now_unit);
                                    break;
                                case 2.1:
                                    $scope.loadAllDashbaordTab2($scope.now_unit, $scope.select_year_tab2, $scope.select_month_tab2);
                                    break;
                                case 2.2:
                                    $scope.load_grap2_2($scope.now_unit, $scope.select_year_tab2);
                                    break;
                                case 2.3:
                                    $scope.load_grap2_2($scope.now_unit, $scope.select_year_tab2);
                                    break;
                                case 3.1:
                                    $scope.load_chart3_1_1();
                                    $scope.load_chart3_1_2();
                                    break;
                                case 3.2:
                                    $scope.load_chart3_2_1();
                                    $scope.load_chart3_2_2();
                                    break;

                            }
                        }
                    }
                    else {
                        tempResult = orgUnit;
                        switch ($scope.navigation_load_chart) {
                            case 1:
                                $scope.loadDashbaordTab1(tempResult.OrgUnit);
                                break;
                        }
                    }
                }
            }

            $scope.tryToSelect = function (text) {
                if (text) {
                    for (var i = 0; i < $scope.list_unit_all.length; i++) {
                        if ($scope.list_unit_all[i].OrgDisplay == text) {
                            tempResult = $scope.list_unit_all[i];
                            break;
                        }
                    }
                    $scope.searchText = '';
                    $scope.autocomplete.selectedItem = null;
                }
            };

            $scope.checkText = function (text) {

                var result = null;
                for (var i = 0; i < $scope.list_unit_all.length; i++) {
                    if ($scope.list_unit_all[i].OrgDisplay == text) {
                        tempResult = $scope.list_unit_all[i];
                        break;
                    }
                }

                if (result) {
                    $scope.searchText = result.OrgDisplay;
                    $scope.selectedItemChange(result);
                    $scope.autocomplete.selectedItem = result;
                } else if (tempResult) {
                    $scope.searchText = tempResult.OrgDisplay;
                    $scope.selectedItemChange(tempResult);
                    $scope.autocomplete.selectedItem = tempResult;
                }
            };

            /* End Auto Complete */

            // tab 1
            $scope.SelectTabl1_YTD_OvertimeandSalary = function () {

                $scope.model_tab1 = {
                    salary: 0,
                    overtime_salary: 0
                };

                if (!tempResult)
                    $scope.now_unit = "0";
                else
                    $scope.now_unit = tempResult.OrgUnit.toString();

                $scope.loadDashbaordTab1 = function (org_unit) {
                    $scope.data_org = "";
                    if (org_unit === "0") {

                        if ($scope.list_unit_all.length > 0) {

                            for (var i = 0; i < $scope.list_unit_all.length; i++) {
                                if (i != 0)
                                    $scope.data_org += "," + $scope.list_unit_all[i].OrgUnit;
                                else
                                    $scope.data_org = $scope.list_unit_all[i].OrgUnit;
                            }
                        }
                    }
                    else {

                        if ($scope.list_unit_all.length > 0) {

                            for (var i = 0; i < $scope.list_unit_all.length; i++) {

                                var is_dup = false;
                                is_dup = $scope.list_unit_all[i].OrgUnitPath.includes(org_unit);
                                if (is_dup) {
                                    if ($scope.data_org == "") {
                                        $scope.data_org = $scope.list_unit_all[i].OrgUnit;

                                    }
                                    else {
                                        $scope.data_org += "," + $scope.list_unit_all[i].OrgUnit;
                                    }
                                }
                            }
                        }
                    }

                    var URL = CONFIG.SERVER + 'HRTM/GetYTDOvertimeAndSalary';
                    $scope.employeeData = getToken(CONFIG.USER);
                    var oRequestParameter = {
                        InputParameter: {
                            ListOrg: $scope.data_org
                        }
                        , CurrentEmployee: $scope.employeeData
                        , Requestor: $scope.requesterData
                        , Creator: getToken(CONFIG.USER)
                    };

                    $scope.loader.enable = true;
                    $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {

                        $scope.model_tab1.salary = response.data.Salary;
                        $scope.model_tab1.overtime_salary = response.data.Overtime;
                        $scope.overtime = $scope.model_tab1.overtime_salary;
                        $scope.salary = $scope.model_tab1.salary;

                        //YTD Overtime and Salary
                        $scope.chartData = {
                            labels: ["ค่าล่วงเวลา", "เงินเดือน"],
                            data: [$scope.model_tab1.overtime_salary, $scope.model_tab1.salary],
                            colours: ['#1e325a', '#E0E0E0'],
                            options: {
                                tooltips: {
                                    callbacks: {
                                        // this callback is used to create the tooltip label
                                        label: function (tooltipItem, data) {
                                            // get the data label and data value to display
                                            // convert the data value to local string so it uses a comma seperated number
                                            var dataLabel = data.labels[tooltipItem.index];
                                            var value = ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toLocaleString('en', optionsnumber);

                                            // make this isn't a multi-line label (e.g. [["label 1 - line 1, "line 2, ], [etc...]])
                                            if (Chart.helpers.isArray(dataLabel)) {
                                                // show value on first line of multiline label
                                                // need to clone because we are changing the value
                                                dataLabel = dataLabel.slice();
                                                dataLabel[0] += value;
                                            } else {
                                                dataLabel += value;
                                            }

                                            // return the text to display on the tooltip
                                            return dataLabel;
                                        }
                                    }
                                }
                                //legend: {
                                //    display: true,
                                //    position: 'right'
                                //}
                            }
                        };

                        $scope.loader.enable = false;

                    }, function (response) {
                        console.log(response);
                        $scope.loader.enable = false;
                    });
                };
                $scope.loadDashbaordTab1($scope.now_unit);
            };

            // tab 2 - 1
            $scope.loadDashboardOvertimeBUAndOTReasons = function () {

                //$scope.now_unit = "0";
                if (!tempResult)
                    $scope.now_unit = "0";
                else
                    $scope.now_unit = tempResult.OrgUnit.toString();
                $scope.select_year_tab2 = $scope.list_year_tab2[0];
                $scope.select_month_tab2 = $scope.dashboard_month[0];

                //Overtime by Business Unit in this month 
                $scope.Load_tab2_Grap1 = function (unit, year, month) {

                    $scope.now_unit = unit;
                    $scope.select_year_tab2 = year;
                    $scope.select_month_tab2 = month;

                    var data_org = $scope.SplitOrganization();

                    var URL = CONFIG.SERVER + 'HRTM/DashboardOvertimebyBusinessUnitinmonth';
                    $scope.employeeData = getToken(CONFIG.USER);
                    var oRequestParameter = {
                        InputParameter: {
                            ListOrg: data_org,
                            sYear: year,
                            sMonth: month,
                            OrgSelect: $scope.now_unit
                        }
                        , CurrentEmployee: $scope.employeeData
                        , Requestor: $scope.requesterData
                        , Creator: getToken(CONFIG.USER)
                    };

                    $scope.loader.enable = true;
                    $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {

                        console.log(response);
                        var datasetBaht = response.data.Amount;

                        $scope.overtimeBusiness = {
                            labels: response.data.UnitName,
                            data: response.data.PercentageOverTime,
                            series: "series",
                            colours: response.data.CodeColor,
                            options: {
                                tooltips: {
                                    callbacks: {
                                        label: function (tooltipItem, data) {
                                            var label = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toLocaleString('en', optionsnumber) + " %";
                                            return label;
                                        }
                                    }
                                },
                                scales: {
                                    xAxes: [{
                                        categoryPercentage: .1,
                                        barPercentage: 2,
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }],
                                    yAxes: [{
                                        ticks: {
                                            beginAtZero: true,
                                            suggestedMin: 0,
                                            suggestedMax: 100,
                                            callback: function (label, index, labels) {
                                                return label + "%";
                                            }
                                        }
                                    }]
                                }
                            }
                        };

                        // LoadChart 2
                        $scope.LoadTab2_Grap2(unit, year, month);

                    }, function (response) {
                        console.log(response);
                        $scope.loader.enable = false;
                    });
                };

                //TOP OT Reasons
                $scope.LoadTab2_Grap2 = function (unit, year, month) {

                    var data_org = $scope.SplitOrganization();

                    var URL = CONFIG.SERVER + 'HRTM/GetDashboardOTReason';
                    $scope.employeeData = getToken(CONFIG.USER);
                    var oRequestParameter = {
                        InputParameter: {
                            ListOrg: data_org,
                            sYear: $scope.select_year_tab2,
                            sMonth: $scope.select_month_tab2
                        }
                        , CurrentEmployee: $scope.employeeData
                        , Requestor: $scope.requesterData
                        , Creator: getToken(CONFIG.USER)
                    };

                    $scope.loader.enable = true;
                    $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {
                        $scope.loader.enable = false;
                        $scope.overtimeReasons = {
                            labels: response.data.ListTextOT,
                            data: response.data.ListPercentageOT,
                            series: "series",
                            colours: ['#00ADEE', '#00ADEE', '#00ADEE', '#00ADEE', '#00ADEE', '#00ADEE', '#00ADEE', '#00ADEE'],
                            options: {
                                tooltips: {
                                    callbacks: {
                                        label: function (tooltipItem, data) {
                                            var label = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toLocaleString('en', optionsnumber) + " %";
                                            return label;
                                        }
                                    }
                                },
                                scales: {
                                    yAxes: [{
                                        categoryPercentage: .2,
                                        barPercentage: 2,
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }],
                                    xAxes: [{
                                        ticks: {
                                            beginAtZero: true,
                                            suggestedMin: 0,
                                            suggestedMax: 100,
                                            callback: function (label, index, labels) {
                                                return label + "%";
                                            }
                                        }
                                    }]
                                }
                            }
                        };
                    }, function (response) {
                        console.log(response);
                        $scope.loader.enable = false;
                    });
                };

                $scope.loadAllDashbaordTab2 = function (unit, year, month) {

                    $scope.now_unit = unit;
                    $scope.select_year_tab2 = year;
                    $scope.select_month_tab2 = month;

                    $scope.Load_tab2_Grap1(unit, year, month);
                };
                $scope.loadAllDashbaordTab2($scope.now_unit, $scope.select_year_tab2, $scope.select_month_tab2);
            };

            // tab 2 - 2
            $scope.loadDashboardOvertimeAndSalaryInYear = function () {

                //$scope.now_unit = "0";
                if (!tempResult)
                    $scope.now_unit = "0";
                else
                    $scope.now_unit = tempResult.OrgUnit.toString();

                $scope.select_year_tab2 = $scope.list_year_tab2[0];

                //Overtime and Salary in Year xxxx
                $scope.load_grap2_2 = function (unit, year) {

                    $scope.now_unit = unit;
                    $scope.select_year_tab2 = year;

                    var data_org = $scope.SplitOrganization();

                    var URL = CONFIG.SERVER + 'HRTM/GetDashbaordOvertimeAndSalaryByMonth';
                    $scope.employeeData = getToken(CONFIG.USER);
                    var oRequestParameter = {
                        InputParameter: {
                            ListOrg: data_org,
                            sYear: year
                        }
                        , CurrentEmployee: $scope.employeeData
                        , Requestor: $scope.requesterData
                        , Creator: getToken(CONFIG.USER)
                    };

                    $scope.loader.enable = true;
                    $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {


                        var arr_month = response.data.Month;
                        var arr_overtime = response.data.Overtime;
                        var arr_salary = response.data.Salary;
                        var arr_proportionOvertime = response.data.ProportionOvertime;

                        $scope.overtimeInYear = {
                            labels: arr_month,
                            datasetOverride: [
                                {
                                    label: "Salary",
                                    borderWidth: 1,
                                    yAxisID: 'y-axis-1',
                                    type: 'bar'
                                },
                                {
                                    label: "Overtime",
                                    borderWidth: 1,
                                    yAxisID: 'y-axis-1',
                                    type: 'bar'
                                },
                                {
                                    label: "%OT to Salary",
                                    borderWidth: 3,
                                    yAxisID: 'y-axis-2',
                                    type: 'line'
                                }
                            ],
                            data: [arr_salary,
                                arr_overtime,
                                arr_proportionOvertime
                            ],
                            series: "series",
                            colours: ['#C1C1C1', '#00adee', '#1e325a'],
                            options: {
                                tooltips: {
                                    callbacks: {
                                        label: function (tooltipItem, data) {
                                            var label = data.datasets[tooltipItem.datasetIndex].label || '';

                                            if (label) {
                                                label += ': ';
                                            }
                                            label += data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toLocaleString('en', optionsnumber);
                                            return label;
                                        }
                                    }
                                },
                                legend: {
                                    display: true,
                                    position: 'top'
                                },
                                scales: {
                                    xAxes: [{
                                        stacked: true,
                                        categoryPercentage: .2,
                                        barPercentage: 2,
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }],
                                    yAxes: [{
                                        id: 'y-axis-1',
                                        type: 'linear',
                                        display: true,
                                        position: 'left',
                                        stacked: true,
                                        ticks: {
                                            beginAtZero: true,
                                            callback: function (label, index, labels) {
                                                return label.toLocaleString('en', optionsnumber);
                                            }
                                        }
                                    }, {
                                        id: 'y-axis-2',
                                        type: 'linear',
                                        display: true,
                                        position: 'right',
                                        stacked: true,
                                        ticks: {
                                            beginAtZero: true,
                                            suggestedMin: 0,
                                            suggestedMax: 100,
                                            callback: function (label, index, labels) {
                                                return label + "%";
                                            }
                                        }
                                    }]
                                }
                            }
                        };

                        $scope.loader.enable = false;

                    }, function (response) {
                        console.log(response);
                        $scope.loader.enable = false;
                    });



                };
                $scope.load_grap2_2($scope.now_unit, $scope.select_year_tab2);
            };

            // tab 2 - 3
            $scope.loadDashboardSummaryOTAndSalaryMonth = function () {

                //$scope.now_unit = "0";
                if (!tempResult)
                    $scope.now_unit = "0";
                else
                    $scope.now_unit = tempResult.OrgUnit.toString();
                $scope.select_year_tab2 = $scope.list_year_tab2[0];
                $scope.select_month_tab2 = $scope.dashboard_month[0];

                // OT : Salary (6 Months)
                $scope.load_chart2_3_1 = function () {

                    var data_org = $scope.SplitOrganization();

                    var URL = CONFIG.SERVER + 'HRTM/GetDashbaordOTSalary6Months';
                    $scope.employeeData = getToken(CONFIG.USER);
                    var oRequestParameter = {
                        InputParameter: {
                            ListOrg: data_org,
                            sYear: $scope.select_year_tab2,
                            sMonth: $scope.select_month_tab2
                        }
                        , CurrentEmployee: $scope.employeeData
                        , Requestor: $scope.requesterData
                        , Creator: getToken(CONFIG.USER)
                    };

                    $scope.loader.enable = true;
                    $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {
                        console.log(response);

                        var arr_month = response.data.Month;
                        var arr_overtime = response.data.Overtime;
                        var arr_proportionOvertime = response.data.ProportionOvertime;
                        var arr_salary = response.data.Salary;

                        //OT : Salary (6 Months)
                        $scope.overtimeSalaryInMonths = {
                            labels: arr_month,
                            datasetOverride: [
                                {
                                    label: "Salary",
                                    borderWidth: 1,
                                    yAxisID: 'y-axis-1',
                                    type: 'bar'
                                },
                                {
                                    label: "Overtime",
                                    borderWidth: 1,
                                    yAxisID: 'y-axis-1',
                                    type: 'bar'
                                },
                                {
                                    label: "%OT to Salary",
                                    borderWidth: 3,
                                    yAxisID: 'y-axis-2',
                                    type: 'line'
                                }
                            ],
                            data: [
                                arr_salary,
                                arr_overtime,
                                arr_proportionOvertime
                            ],
                            series: "series",
                            colours: ['#C1C1C1', '#00adee', '#1e325a'],
                            options: {
                                tooltips: {
                                    callbacks: {
                                        label: function (tooltipItem, data) {
                                            var label = data.datasets[tooltipItem.datasetIndex].label || '';
                                            if (label) {
                                                label += ': ';
                                            }
                                            label += data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toLocaleString('en', optionsnumber);
                                            return label;
                                        }
                                    }
                                },
                                legend: {
                                    display: true,
                                    position: 'top'
                                },
                                scales: {
                                    xAxes: [{
                                        categoryPercentage: .3,
                                        barPercentage: 1,
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }],
                                    yAxes: [{
                                        id: 'y-axis-1',
                                        type: 'linear',
                                        display: true,
                                        position: 'left',
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Baht'
                                        },
                                        ticks: {
                                            beginAtZero: true,
                                            callback: function (label, index, labels) {
                                                return label.toLocaleString('en', optionsnumber);
                                            }
                                        }
                                    }, {
                                        id: 'y-axis-2',
                                        type: 'linear',
                                        display: true,
                                        position: 'right',
                                        ticks: {
                                            beginAtZero: true,
                                            suggestedMin: 0,
                                            suggestedMax: 100,
                                            callback: function (label, index, labels) {
                                                return label + "%";
                                            }
                                        }
                                    }]
                                }
                            }
                        };
                        $scope.loader.enable = false;

                        $scope.load_chart2_3_2();

                    }, function (response) {
                        console.log(response);
                        $scope.loader.enable = false;
                    });
                };

                // OT : Hours (6 Months)
                $scope.load_chart2_3_2 = function () {

                    var data_org = $scope.SplitOrganization();

                    var URL = CONFIG.SERVER + 'HRTM/GetDashbaordOTHours6Months';
                    $scope.employeeData = getToken(CONFIG.USER);
                    var oRequestParameter = {
                        InputParameter: {
                            ListOrg: data_org,
                            sYear: $scope.select_year_tab2,
                            sMonth: $scope.select_month_tab2
                        }
                        , CurrentEmployee: $scope.employeeData
                        , Requestor: $scope.requesterData
                        , Creator: getToken(CONFIG.USER)
                    };

                    $scope.loader.enable = true;
                    $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {

                        var arr_month = response.data.Month;
                        var arr_overtimeAmount = response.data.OvertimePay;
                        var arr_OvertimeHour = response.data.OvertimeHour;
                        var arr_salary = response.data.Salary;

                        //OT Hours (6 Months)
                        $scope.overtimeHoursInMonths = {
                            labels: arr_month,
                            datasetOverride: [
                                {
                                    label: "Salary",
                                    borderWidth: 1,
                                    yAxisID: 'y-axis-1',
                                    type: 'bar'
                                },
                                {
                                    label: "Overtime",
                                    borderWidth: 1,
                                    yAxisID: 'y-axis-1',
                                    type: 'bar'
                                },
                                {
                                    label: "OT Hour",
                                    borderWidth: 3,
                                    yAxisID: 'y-axis-2',
                                    type: 'line'
                                }
                            ],
                            data: [
                                arr_salary,
                                arr_overtimeAmount,
                                arr_OvertimeHour
                            ],
                            series: "series",
                            colours: ['#C1C1C1', '#00adee', '#1e325a'],
                            options: {
                                tooltips: {
                                    callbacks: {
                                        label: function (tooltipItem, data) {
                                            var label = data.datasets[tooltipItem.datasetIndex].label || '';

                                            if (label) {
                                                label += ': ';
                                            }
                                            label += data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toLocaleString('en', optionsnumber);
                                            return label;
                                        }
                                    }
                                },
                                legend: {
                                    display: true,
                                    position: 'top'
                                },
                                scales: {
                                    xAxes: [{
                                        categoryPercentage: .3,
                                        barPercentage: 1,
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }],
                                    yAxes: [{
                                        id: 'y-axis-1',
                                        type: 'linear',
                                        display: true,
                                        position: 'left',
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Baht'
                                        },
                                        ticks: {
                                            beginAtZero: true,
                                            callback: function (label, index, labels) {
                                                return label.toLocaleString('en', optionsnumber);
                                            }
                                        }
                                    }, {
                                        id: 'y-axis-2',
                                        type: 'linear',
                                        display: true,
                                        position: 'right',
                                        ticks: {
                                            beginAtZero: true
                                        },
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Hour'
                                        }
                                    }]
                                }
                            }
                        };

                        $scope.loader.enable = false;
                    }, function (response) {
                        console.log(response);
                        $scope.loader.enable = false;
                    });
                };

                $scope.loadDashboardTabl2_3 = function (unit, year, month) {

                    $scope.now_unit = unit;
                    $scope.select_year_tab2 = year;
                    $scope.select_month_tab2 = month;

                    $scope.load_chart2_3_1();

                };
                $scope.loadDashboardTabl2_3($scope.now_unit, $scope.select_year_tab2, $scope.select_month_tab2);
            };

            // tab 3 - 1
            $scope.loadSummaryOTAndSalary_Year = function () {

                //$scope.now_unit = "0";
                if (!tempResult)
                    $scope.now_unit = "0";
                else
                    $scope.now_unit = tempResult.OrgUnit.toString();

                $scope.begin_year_tab3 = $scope.list_year_tab3[0];
                $scope.end_year_tab3 = $scope.list_year_tab3[0];


                //OT : Salary (xxxx – xxxx)
                $scope.load_chart3_1_1 = function () {

                    var data_org = $scope.SplitOrganization();

                    var URL = CONFIG.SERVER + 'HRTM/GetDashboardSummarySalaryOvertimePayByYear';
                    $scope.employeeData = getToken(CONFIG.USER);
                    var oRequestParameter = {
                        InputParameter: {
                            ListOrg: data_org,
                            beginYear: $scope.begin_year_tab3,
                            endYear: $scope.end_year_tab3
                        }
                        , CurrentEmployee: $scope.employeeData
                        , Requestor: $scope.requesterData
                        , Creator: getToken(CONFIG.USER)
                    };

                    $scope.loader.enable = true;
                    $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {
                        var arr_year = response.data.TotalYear;
                        var arr_overtime_year = response.data.TotalOvertimePayYear;
                        var arr_ratioOvertime_year = response.data.TotalRatioOvertimePayYear;
                        var arr_salary_year = response.data.TotalSalaryYear;
                        $scope.overtimeSalaryInYear = {
                            labels: arr_year,
                            datasetOverride: [
                                {
                                    label: "Salary",
                                    borderWidth: 1,
                                    yAxisID: 'y-axis-1',
                                    type: 'bar'
                                },
                                {
                                    label: "Overtime",
                                    borderWidth: 1,
                                    yAxisID: 'y-axis-1',
                                    type: 'bar'
                                },
                                {
                                    label: "%OT to Salary",
                                    borderWidth: 3,
                                    yAxisID: 'y-axis-2',
                                    type: 'line'
                                }
                            ],
                            data: [
                                arr_salary_year,
                                arr_overtime_year,
                                arr_ratioOvertime_year
                            ],
                            series: "series",
                            colours: ['#C1C1C1', '#00adee', '#1e325a'],
                            options: {
                                tooltips: {
                                    callbacks: {
                                        label: function (tooltipItem, data) {
                                            var label = data.datasets[tooltipItem.datasetIndex].label || '';

                                            if (label) {
                                                label += ': ';
                                            }
                                            label += data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toLocaleString('en', optionsnumber);
                                            return label;
                                        }
                                    }
                                },
                                legend: {
                                    display: true,
                                    position: 'top'
                                },
                                scales: {
                                    xAxes: [{
                                        categoryPercentage: .3,
                                        barPercentage: 1,
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }],
                                    yAxes: [{
                                        id: 'y-axis-1',
                                        type: 'linear',
                                        display: true,
                                        position: 'left',
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Baht'
                                        },
                                        ticks: {
                                            beginAtZero: true,
                                            callback: function (label, index, labels) {
                                                return label.toLocaleString('en', optionsnumber);
                                            }
                                        }
                                    }, {
                                        id: 'y-axis-2',
                                        type: 'linear',
                                        display: true,
                                        position: 'right',
                                        ticks: {
                                            beginAtZero: true,
                                            suggestedMin: 0,
                                            suggestedMax: 100,
                                            callback: function (label, index, labels) {
                                                return label + "%";
                                            }
                                        }
                                    }]
                                }
                            }
                        };
                        $scope.loader.enable = false;
                    }, function (response) {
                        $scope.loader.enable = false;
                    });
                };

                //OT Hours (xxxx – xxxx)
                $scope.load_chart3_1_2 = function () {

                    var data_org = $scope.SplitOrganization();

                    var URL = CONFIG.SERVER + 'HRTM/GetDashboardSummarySalaryOvertimeHourByYear';
                    $scope.employeeData = getToken(CONFIG.USER);
                    var oRequestParameter = {
                        InputParameter: {
                            ListOrg: data_org,
                            beginYear: $scope.begin_year_tab3,
                            endYear: $scope.end_year_tab3
                        }
                        , CurrentEmployee: $scope.employeeData
                        , Requestor: $scope.requesterData
                        , Creator: getToken(CONFIG.USER)
                    };

                    $scope.loader.enable = true;
                    $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {

                        var arr_year = response.data.TotalYear;
                        var arr_salary = response.data.TotalSalaryYear;
                        var arr_ot_pay = response.data.TotalOvertimePayYear;
                        var arr_ot_hour = response.data.TotalOvertimeHourYear;

                        $scope.overtimeHoursInYear = {
                            labels: arr_year,
                            datasetOverride: [
                                {
                                    label: "Salary",
                                    borderWidth: 1,
                                    yAxisID: 'y-axis-1',
                                    type: 'bar'
                                },
                                {
                                    label: "Overtime",
                                    borderWidth: 1,
                                    yAxisID: 'y-axis-1',
                                    type: 'bar'
                                },
                                {
                                    label: "OT Hour",
                                    borderWidth: 3,
                                    yAxisID: 'y-axis-2',
                                    type: 'line'
                                }
                            ],
                            data: [
                                arr_salary,
                                arr_ot_pay,
                                arr_ot_hour
                            ],
                            series: "series",
                            colours: ['#C1C1C1', '#00adee', '#1e325a'],
                            options: {
                                tooltips: {
                                    callbacks: {
                                        label: function (tooltipItem, data) {
                                            var label = data.datasets[tooltipItem.datasetIndex].label || '';

                                            if (label) {
                                                label += ': ';
                                            }
                                            label += data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toLocaleString('en', optionsnumber);
                                            return label;
                                        }
                                    }
                                },
                                legend: {
                                    display: true,
                                    position: 'top'
                                },
                                scales: {
                                    xAxes: [{
                                        categoryPercentage: .3,
                                        barPercentage: 1,
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }],
                                    yAxes: [{
                                        id: 'y-axis-1',
                                        type: 'linear',
                                        display: true,
                                        position: 'left',
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Baht'
                                        },
                                        ticks: {
                                            beginAtZero: true,
                                            callback: function (label, index, labels) {
                                                return label.toLocaleString('en', optionsnumber);
                                            }
                                        }
                                    }, {
                                        id: 'y-axis-2',
                                        type: 'linear',
                                        display: true,
                                        position: 'right',
                                        ticks: {
                                            beginAtZero: true,
                                            suggestedMin: 0,
                                            suggestedMax: 5000,
                                            stepSize: 500,
                                            callback: function (label, index, labels) {
                                                return label.toLocaleString('en', optionsnumber);
                                            }
                                        },
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Hour'
                                        }
                                    }]
                                }
                            }
                        };


                    }, function (response) {
                        console.log(response);
                        $scope.loader.enable = false;
                    });
                };


                $scope.loadDashboardTabl3_1 = function (unit, begin_year, end_year) {

                    $scope.now_unit = unit;
                    $scope.begin_year_tab3 = begin_year;
                    $scope.end_year_tab3 = end_year;

                    if (end_year < begin_year)
                        $scope.begin_year_tab3 = end_year;

                    $scope.reloadOrgUnit(5, begin_year, end_year, 0, unit);

                    //$scope.load_chart3_1_1();
                    //$scope.load_chart3_1_2();

                };
                $scope.loadDashboardTabl3_1($scope.now_unit, $scope.begin_year_tab3, $scope.end_year_tab3);
            };

            // tab 3 - 2
            $scope.loadOvertimeDataAnalysisReport = function () {

                //$scope.now_unit = "0";
                if (!tempResult)
                    $scope.now_unit = "0";
                else
                    $scope.now_unit = tempResult.OrgUnit.toString();

                $scope.begin_year_tab3 = $scope.list_year_tab3[0];
                $scope.end_year_tab3 = $scope.list_year_tab3[0];

                //OT : No. of Employee by Level
                $scope.load_chart3_2_1 = function () {

                    var data_org = $scope.SplitOrganization();

                    var URL = CONFIG.SERVER + 'HRTM/GetDashboardOTEmployeebyLevel';
                    $scope.employeeData = getToken(CONFIG.USER);
                    var oRequestParameter = {
                        InputParameter: {
                            ListOrg: data_org,
                            beginYear: $scope.begin_year_tab3,
                            endYear: $scope.end_year_tab3
                        }
                        , CurrentEmployee: $scope.employeeData
                        , Requestor: $scope.requesterData
                        , Creator: getToken(CONFIG.USER)
                    };

                    $scope.loader.enable = true;
                    $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {
                        // จำนวนคน
                        var dataset = [
                            response.data.EmployeeLevel1To2
                            , response.data.EmployeeLevel3To8
                            , response.data.EmployeeLevel9To10
                        ];
                        $scope.employeeByLevel = {
                            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                            datasetOverride: [
                                {
                                    label: "Junior (Level 1-2)",
                                    borderWidth: 1,
                                    type: 'bar'
                                },
                                {
                                    label: "Junior (Level 3-8)",
                                    borderWidth: 1,
                                    type: 'bar'
                                },
                                {
                                    label: "Senior (Level 9-10)",
                                    borderWidth: 1,
                                    type: 'bar'
                                }
                            ],
                            data: [
                                response.data.PercentageEmployeeLevel1To2
                                , response.data.PercentageEmployeeLevel3To8
                                , response.data.PercentageEmployeeLevel9To10
                            ],
                            series: "series",
                            colours: ['#C1C1C1', '#00adee', '#1e325a'],
                            options: {
                                tooltips: {
                                    callbacks: {
                                        label: function (tooltipItem, data) {
                                            var label = data.datasets[tooltipItem.datasetIndex].label || '';

                                            if (label) {
                                                label += ': ';
                                            }

                                            //label += dataset[tooltipItem.datasetIndex][tooltipItem.index].toLocaleString('en', optionsnumber) + " ( " + tooltipItem.yLabel + " % )";
                                            label += dataset[tooltipItem.datasetIndex][tooltipItem.index] + " ( " + tooltipItem.yLabel + " % )";
                                            return label;
                                        }
                                    }
                                },
                                legend: {
                                    display: true,
                                    position: 'top'
                                },
                                scales: {
                                    xAxes: [{
                                        stacked: true,
                                        categoryPercentage: .2,
                                        barPercentage: 2,
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }],
                                    yAxes: [{
                                        id: 'y-axis-1',
                                        type: 'linear',
                                        display: true,
                                        position: 'left',
                                        stacked: true,
                                        ticks: {
                                            beginAtZero: true,
                                            suggestedMin: 0,
                                            suggestedMax: 100,
                                            callback: function (label, index, labels) {
                                                return label + "%";
                                            }
                                        }
                                    }]
                                }
                            }

                        };
                        $scope.loader.enable = false;
                    }, function (response) {
                        $scope.loader.enable = false;
                    });

                };

                //OT Baht by Level
                $scope.load_chart3_2_2 = function () {

                    var data_org = $scope.SplitOrganization();

                    var URL = CONFIG.SERVER + 'HRTM/GetDashboardOTEmployeebyBath';
                    $scope.employeeData = getToken(CONFIG.USER);
                    var oRequestParameter = {
                        InputParameter: {
                            ListOrg: data_org,
                            beginYear: $scope.begin_year_tab3,
                            endYear: $scope.end_year_tab3
                        }
                        , CurrentEmployee: $scope.employeeData
                        , Requestor: $scope.requesterData
                        , Creator: getToken(CONFIG.USER)
                    };

                    $scope.loader.enable = true;
                    $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {

                        // จำนวนบาท
                        var datasetBaht = [
                            response.data.OTAmountLevel1To2
                            , response.data.OTAmountLevel3To8
                            , response.data.OTAmountLevel9To10
                        ];

                        // จำนวน % บาท
                        var percentageBath = [
                            response.data.PercentageOTAmountLevel1To2
                            , response.data.PercentageOTAmountLevel3To8
                            , response.data.PercentageOTAmountLevel9To10
                        ];

                        $scope.bahtByLevel = {
                            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                            datasetOverride: [
                                {
                                    label: "Junior (Level 1-2)",
                                    borderWidth: 1,
                                    type: 'bar'
                                },
                                {
                                    label: "Junior (Level 3-8)",
                                    borderWidth: 1,
                                    type: 'bar'
                                },
                                {
                                    label: "Senior (Level 9-10)",
                                    borderWidth: 1,
                                    type: 'bar'
                                }
                            ],
                            data: percentageBath,
                            series: "series",
                            colours: ['#C1C1C1', '#00adee', '#1e325a'],
                            options: {
                                tooltips: {
                                    callbacks: {
                                        label: function (tooltipItem, data) {
                                            var label = data.datasets[tooltipItem.datasetIndex].label || '';

                                            if (label) {
                                                label += ': ';
                                            }

                                            label += datasetBaht[tooltipItem.datasetIndex][tooltipItem.index].toLocaleString('en', optionsnumber) + " ( " + tooltipItem.yLabel + " % )";
                                            return label;
                                        }
                                    }
                                },
                                legend: {
                                    display: true,
                                    position: 'top'
                                },
                                scales: {
                                    xAxes: [{
                                        stacked: true,
                                        categoryPercentage: .2,
                                        barPercentage: 2,
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }],
                                    yAxes: [{
                                        id: 'y-axis-1',
                                        type: 'linear',
                                        display: true,
                                        position: 'left',
                                        stacked: true,
                                        ticks: {
                                            beginAtZero: true,
                                            suggestedMin: 0,
                                            suggestedMax: 100,
                                            callback: function (label, index, labels) {
                                                return label + "%";
                                            }
                                        }
                                    }]
                                },
                                plugins: {
                                    datalabels: {
                                        color: 'white',
                                        font: {
                                            weight: 'bold'
                                        },
                                        formatter: function (value, context) {
                                            return Math.round(value) + '%';
                                        }
                                    }
                                }
                            }
                        };

                        $scope.loader.enable = false;
                    }, function (response) {
                        console.log(response);
                        $scope.loader.enable = false;
                    });
                };

                $scope.loadDashboardTabl3_2 = function (unit, begin_year, end_year) {

                    $scope.now_unit = unit;
                    $scope.begin_year_tab3 = begin_year;
                    $scope.end_year_tab3 = end_year;


                    if (end_year < begin_year)
                        $scope.begin_year_tab3 = end_year;


                    $scope.reloadOrgUnit(6, begin_year, end_year, 0, unit);

                    //$scope.load_chart3_2_1();
                    //$scope.load_chart3_2_2();

                };
                $scope.loadDashboardTabl3_2($scope.now_unit, $scope.begin_year_tab3, $scope.end_year_tab3);
            };

        }]);
})();