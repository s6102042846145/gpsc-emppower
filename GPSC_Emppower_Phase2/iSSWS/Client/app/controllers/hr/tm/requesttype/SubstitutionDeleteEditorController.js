﻿(function () {
    angular.module('ESSMobile')
        .controller('SubstitutionDeleteEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {

            $scope.loadData = function(requestNo) {

                var URL = CONFIG.SERVER + 'HRTM/documentCancelSubstitue';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {
                        RequestSubstituteNo: requestNo
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    // List รายการยกเลิก
                    $scope.document.Additional.INFOTYPE2003 = response.data;

                    if (response.data.length > 0) {

                        // หาก BeginDate - EndDate
                        $scope.dataBeginDate = "999-12-31T00:00:00";
                        $scope.dataEndDate = "0001-01-01T00:00:00";
                        angular.forEach(response.data, function (value, key) {

                            if (value.BeginDate < $scope.dataBeginDate) {

                                $scope.document.Additional.InfoTable[0].BeginDate = value.BeginDate;
                                $scope.dataBeginDate = value.BeginDate;
                            }

                            if (value.BeginDate > $scope.dataEndDate) {

                                $scope.document.Additional.InfoTable[0].EndDate = value.EndDate;
                                $scope.dataBeginDate = value.dataEndDate;
                            }

                        });

                        $scope.loadEmployeeData($scope.document.Additional.INFOTYPE2003[0].Substitute);

                    }
                    

                    

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };

            $scope.loadEmployeeData = function(employeeId) {

                var URL = CONFIG.SERVER + 'HRTM/GetObjectEmployeeEmppower';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {
                        EmployeeId: employeeId
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data.length > 0) {
                        $scope.loadSupervisorPosition(response.data[0]);
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };

            $scope.loadSupervisorPosition = function(employeeData) {

                var URL = CONFIG.SERVER + 'HRTM/GetSupervisorPositionByEmpPositionData';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {
                        PositionIdEmployee: employeeData.PositionID
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.document.Additional.InfoSubstitute[0].SUBSTITUTE_APPROVERPOSITION = response.data;
                    $scope.document.Additional.InfoSubstitute[0].SubstituteId = employeeData.EmployeeID;
                    $scope.document.Additional.InfoSubstitute[0].SubstituteName = employeeData.EmployeeName;
                    $scope.document.Additional.InfoSubstitute[0].PostionId = employeeData.PositionID;
                    $scope.document.Additional.InfoSubstitute[0].PostionName = employeeData.PositionName;
                    $scope.document.Additional.InfoSubstitute[0].OrganizationId = employeeData.OrgUnitID;
                    $scope.document.Additional.InfoSubstitute[0].OrganizationName = employeeData.OrgUnitName;

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });


            };

            $scope.ChildAction.SetData = function() {
                if ($routeParams.otherParam) {
                    $scope.loadData($routeParams.otherParam);
                }
                
            };

            $scope.ChildAction.LoadData = function () {

            };

            var employeeDate = getToken(CONFIG.USER);
            $scope.ChildAction.SetData();


           
            
            

        }]);
})();