﻿(function () {
    angular.module('ESSMobile')
        .controller('MonthlyOTViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {

            $scope.employeeData = getToken(CONFIG.USER);

            $scope.oRequestParameter = {
                InputParameter: {}
                , CurrentEmployee: $scope.employeeData
                , Requestor: $scope.requesterData
                , Creator: getToken(CONFIG.USER)
                , Language: $scope.employeeData.Language
            };


            $scope.minBeginDate = $scope.document.Additional.DailyOTLog[0].BeginDate;
            $scope.maxBeginDate = $scope.document.Additional.DailyOTLog[$scope.document.Additional.DailyOTLog.length - 1].BeginDate;
            $scope.GetTimePairOTSummary = function () {
                $scope.loader.enable = true;

                var oRequestParameter = {
                    InputParameter: {
                        "EmployeeID": $scope.document.Additional.DailyOTLog[0].employeeId,
                        "BeginDate": $filter('date')($scope.minBeginDate, 'yyyy-MM-ddT00:00:00'),
                        "EndDate": $filter('date')($scope.maxBeginDate, 'yyyy-MM-ddT00:00:00')
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/GetTimePairOTSummary',
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    if (response.data) {
                        $scope.objTimePairOTSummary = response.data;
                        if ($scope.objTimePairOTSummary.length > 0) {
                            angular.forEach($scope.document.Additional.DailyOTLog, function (item) {

                                var result = getWithAttr($scope.objTimePairOTSummary, 'Date', $filter('date')(item.BeginDate, 'yyyy-MM-ddT00:00:00'));

                                if (result.length > 0) {
                                    item.Type = result[0].Type;
                                    if (result[0].Type == 'OFF') {
                                        item.ClockIn = result[0].ClockIn;
                                        item.ClockOut = result[0].ClockOut;
                                    }
                                    else {
                                        if (result[0].CountAbsAtt > 0 && result[0].IsAllDayFlag) {
                                            item.ClockIn = result[0].WorkBegin;
                                            item.ClockOut = result[0].WorkEnd;
                                        }
                                        else if (result[0].CountAbsAtt > 0 && !result[0].IsAllDayFlag) {
                                            if (result[0].ClockIn != null && result[0].ClockIn <= result[0].AbsAttBegin) {
                                                item.ClockIn = result[0].ClockIn;
                                            }
                                            else if (result[0].ClockIn != null && result[0].ClockIn > result[0].AbsAttBegin) {
                                                item.ClockIn = result[0].AbsAttBegin;
                                                item.ColorIn = 'Blue';
                                            }

                                            if (result[0].ClockOut != null && result[0].ClockOut < result[0].AbsAttEnd) {
                                                item.ClockOut = result[0].AbsAttEnd;
                                                item.ColorOut = 'Blue';
                                            }
                                            else if (result[0].ClockOut != null && result[0].ClockOut >= result[0].AbsAttEnd) {
                                                item.ClockOut = result[0].ClockOut;
                                            }
                                        }
                                    }

                                }
                            });
                        }
                    }
                    $scope.loader.enable = false;

                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetTimePairOTSummary();


            function getWithAttr(array, attr, value) {
                var objReturn = [];
                for (var i = 0; i < array.length; i += 1) {
                    if (array[i][attr] === value) {
                        objReturn.push(array[i]);
                        break;
                    }
                }
                return objReturn;
            }


            $scope.GetOTWorkType = function () {
                //$scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/GetOTWorkType',
                    data: $scope.oRequestParameter
                }).then(function successCallback(response) {
                    $scope.oTWorkTypes = response.data;
                    //$scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetOTWorkType();

            $scope.getTypeName = function (typeId) {
                if ($scope.oTWorkTypes) {
                    $scope.attTypes = $scope.oTWorkTypes.filter(function (item) {
                        return item.__otWorkTypeID === typeId;
                    });
                    return $scope.attTypes[0].__otWorkTypeDesc;
                }
                return "";
            };

            $scope.calH = function (item) {
                if (item) {
                    return (item / 60).toFixed(2);
                }
                return 0;
            };

            $scope.sumRequestOTHour = function () {
                var temp = 0;
                angular.forEach($scope.document.Additional.DailyOTLog, function (item) {
                    if (item.IsPayroll) {
                        temp += parseFloat(item.RequestOTHour10);
                        temp += parseFloat(item.RequestOTHour15);
                        temp += parseFloat(item.RequestOTHour30);
                    }
                });
                return (temp / 60).toFixed(2);;
            };

            $scope.sumEvaOTHour = function () {
                var temp = 0;
                angular.forEach($scope.document.Additional.DailyOTLog, function (item) {
                    temp += item.EvaOTHour10;
                    temp += item.EvaOTHour15;
                    temp += item.EvaOTHour30;
                });
                return (temp / 60).toFixed(2);;
            };

            $scope.sumFinalOTHour = function () {
                var temp = 0;
                angular.forEach($scope.document.Additional.DailyOTLog, function (item) {
                    if (item.IsPayroll) {
                        temp += parseFloat(item.FinalOTHour10);
                        temp += parseFloat(item.FinalOTHour15);
                        temp += parseFloat(item.FinalOTHour30);
                    }
                });
                return (temp / 60).toFixed(2);
            };

            $scope.sumRequestOTHour2 = function () {
                var temp = 0;
                angular.forEach($scope.document.Additional.AllDailyOTLog, function (item) {

                    if (item.IsPayroll && item.Status == 3) {

                        temp += parseFloat(item.RequestOTHour10);
                        temp += parseFloat(item.RequestOTHour15);
                        temp += parseFloat(item.RequestOTHour30);
                    }

                });
                return (temp / 60).toFixed(2);
            };

            $scope.sumEvaOTHour2 = function () {
                var temp = 0;
                angular.forEach($scope.document.Additional.AllDailyOTLog, function (item) {

                    if (item.IsPayroll && item.Status == 3) {
                        temp += item.EvaOTHour10;
                        temp += item.EvaOTHour15;
                        temp += item.EvaOTHour30;
                    }
                });
                return (temp / 60).toFixed(2);
            };

            $scope.sumFinalOTHour2 = function () {
                var temp = 0;
                angular.forEach($scope.document.Additional.AllDailyOTLog, function (item) {

                    if (item.IsPayroll && item.Status == 3) {
                        temp += parseFloat(item.FinalOTHour10);
                        temp += parseFloat(item.FinalOTHour15);
                        temp += parseFloat(item.FinalOTHour30);
                    }

                });
                return (temp / 60).toFixed(2);
            };

            $scope.setFinalOTHour10 = function (item) {
                item.FinalOTHour10 = (item._FinalOTHour10 * 60).toFixed(2);
            };
            $scope.setFinalOTHour15 = function (item) {
                item.FinalOTHour15 = (item._FinalOTHour15 * 60).toFixed(2);
            };
            $scope.setFinalOTHour30 = function (item) {
                item.FinalOTHour30 = (item._FinalOTHour30 * 60).toFixed(2);
            };

            angular.forEach($scope.document.Additional.DailyOTLog, function (item) {
                if (item.OTClockINOUT != null && item.OTClockINOUT.length == 0) {
                    item.OTClockINOUT = null;
                }
            });
            angular.forEach($scope.document.Additional.AllDailyOTLog, function (item) {
                if (item.OTClockINOUT != null && item.OTClockINOUT.length == 0) {
                    item.OTClockINOUT = null;
                }
            });


            $scope.employee = "";
            $scope.GetEmployeeByEmpID = function () {
                //$scope.loader.enable = true;
                $scope.oRequestParameter.EmployeeID = $scope.document.Additional.DailyOTLog[0].employeeId;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/GetEmployeeByEmpID',
                    data: $scope.oRequestParameter
                }).then(function successCallback(response) {
                    if (response.data) {
                        $scope.employee = response.data[0].EmployeeID + " " + response.data[0].EmployeeName;
                    }

                    //$scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }
            $scope.GetEmployeeByEmpID();

            $scope.selectedPeriodSummaryOT = "";
            $scope.GetPeriodSummaryOTList = function () {
                //$scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/GetPeriodSummaryOTList',
                    data: $scope.oRequestParameter
                }).then(function successCallback(response) {
                    $scope.periodSummaryOTs = response.data;
                    if ($scope.periodSummaryOTs) {
                        $scope.period = $scope.periodSummaryOTs[$scope.periodSummaryOTs.length - 1].PeriodValue;
                    }
                    //$scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }
            $scope.GetPeriodSummaryOTList();

            $scope.getPeriodName = function (periodId) {
                if ($scope.periodSummaryOTs) {
                    $scope.periodTypes = $scope.periodSummaryOTs.filter(function (item) {
                        return item.PeriodValue === periodId;
                    });
                    return $scope.periodTypes[0].PeriodText;
                }
                return "";
            }


            $scope.GetTimePairAllDailyOTLog = function () {
                $scope.loader.enable = true;

                var AllDailyOTLogStatus3 = getAllDailyOTLogWithStatus3($scope.document.Additional.AllDailyOTLog, 'Status', 3);

                if (AllDailyOTLogStatus3.length > 0) {
                    $scope.minDate = AllDailyOTLogStatus3[0].BeginDate;
                    $scope.maxDate = AllDailyOTLogStatus3[AllDailyOTLogStatus3.length - 1].BeginDate;

                    var oRequestParameter = {
                        InputParameter: {
                            "EmployeeID": AllDailyOTLogStatus3[0].EmployeeID,
                            "BeginDate": $filter('date')($scope.minDate, 'yyyy-MM-ddT00:00:00'),
                            "EndDate": $filter('date')($scope.maxDate, 'yyyy-MM-ddT00:00:00')
                        }
                        , CurrentEmployee: $scope.employeeData
                        , Requestor: $scope.requesterData
                        , Creator: getToken(CONFIG.USER)
                    };

                    $http({
                        method: 'POST',
                        url: CONFIG.SERVER + 'HRTM/GetTimePairAllDailyOTLog',
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        if (response.data) {
                            $scope.objTimePairAllDailyOTLog = response.data;
                            if ($scope.objTimePairAllDailyOTLog.length > 0) {
                                angular.forEach($scope.document.Additional.AllDailyOTLog, function (item) {

                                    if (item.Status == 3) {
                                        var result = getWithAttr($scope.objTimePairAllDailyOTLog, 'Date', $filter('date')(item.BeginDate, 'yyyy-MM-ddT00:00:00'));

                                        if (result.length > 0) {
                                            item.Type = result[0].Type;
                                            if (result[0].Type == 'OFF') {
                                                item.ClockIn = result[0].ClockIn;
                                                item.ClockOut = result[0].ClockOut;
                                            }
                                            else {
                                                if (result[0].CountAbsAtt > 0 && result[0].IsAllDayFlag) {
                                                    item.ClockIn = result[0].WorkBegin;
                                                    item.ClockOut = result[0].WorkEnd;
                                                }
                                                else if (result[0].CountAbsAtt > 0 && !result[0].IsAllDayFlag) {
                                                    if (result[0].ClockIn != null && result[0].ClockIn <= result[0].AbsAttBegin) {
                                                        item.ClockIn = result[0].ClockIn;
                                                    }
                                                    else if (result[0].ClockIn != null && result[0].ClockIn > result[0].AbsAttBegin) {
                                                        item.ClockIn = result[0].AbsAttBegin;
                                                        item.ColorIn = 'Blue';
                                                    }

                                                    if (result[0].ClockOut != null && result[0].ClockOut < result[0].AbsAttEnd) {
                                                        item.ClockOut = result[0].AbsAttEnd;
                                                        item.ColorOut = 'Blue';
                                                    }
                                                    else if (result[0].ClockOut != null && result[0].ClockOut >= result[0].AbsAttEnd) {
                                                        item.ClockOut = result[0].ClockOut;
                                                    }
                                                }
                                            }

                                        }
                                    }
                                });
                            }
                        }
                        $scope.loader.enable = false;

                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                    });
                }
            };
            $scope.GetTimePairAllDailyOTLog();


            function getAllDailyOTLogWithStatus3(array, attr, value) {
                var objReturn = [];
                for (var i = 0; i < array.length; i += 1) {
                    if (array[i][attr] === value) {
                        objReturn.push(array[i]);
                    }
                }
                return objReturn;
            }


            $scope.ChildAction.SetData = function () {
                //GetCalendar();

            };

            $scope.ChildAction.LoadData = function () {

            };

            $scope.ChildAction.SetData();

        }]);
})();