﻿(function () {
    angular.module('ESSMobile')
        .controller('AbsenceViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {


            $scope.ABSENCETYPE = $scope.Text["ABSENCETYPE"];

            $scope.employeeDelegateData = {};
            
            $scope.init = function () {

            };

            $scope.ChildAction.SetData = function () {

                $scope.AbsenceData = $scope.document.Additional.INFOTYPE2001;

                // คนแลกกะมีค่าโหลดข้อมูลคนแลกกะมาใส่
                if ($scope.document.Additional.DELEGATEDATA[0].DelegateTo) {

                    var URL = CONFIG.SERVER + 'HRTM/GetObjectEmployeeEmppower';
                    $scope.employeeData = getToken(CONFIG.USER);

                    var oRequestParameter = {
                        InputParameter: {
                            EmployeeId: $scope.document.Additional.DELEGATEDATA[0].DelegateTo
                        }
                        , CurrentEmployee: $scope.employeeData
                        , Requestor: $scope.requesterData
                        , Creator: getToken(CONFIG.USER)
                    };

                    $scope.loader.enable = true;

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {

                        console.log(response.data);

                        $scope.employeeDelegateData = response.data[0];

                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                    });

                }
            };

            $scope.ChildAction.LoadData = function () {

            };


            var employeeDate = getToken(CONFIG.USER);
            $scope.ChildAction.SetData();

            $scope.textAbsenceType = '92#' + $scope.document.Additional.INFOTYPE2001[0].AbsenceType;

            // Get All Type Leave
            $scope.list_leavetype_all = [];
            $scope.load_leavetype_all = function () {

                var URL = CONFIG.SERVER + 'HRTM/GetAbsenceTypeList';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data.length > 0) {
                        $scope.list_leavetype_all = response.data;
                    }
                    else {
                        $scope.model.leave_time_id = 0;
                    }

                    $scope.loader.enable = false;

                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.load_leavetype_all();

            $scope.getTextDescriptionAbsense = function (textAbsense) {

                if ($scope.list_leavetype_all.length > 0) {

                    var newArray = $scope.list_leavetype_all.filter(function (item) {
                        return item.Key === textAbsent.toString();
                    });

                    return newArray[0].Description;
                }
                return "";
            };

            $scope.loadDataAbsenceDay = function () {

                var URL = CONFIG.SERVER + 'HRTM/GetCalAbsence';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                    , AbsenceType: $scope.document.Additional.INFOTYPE2001[0].AbsenceType
                    , BeginDate: $filter('date')($scope.document.Additional.INFOTYPE2001[0].BeginDate, 'yyyy-MM-ddT00:00:00')
                    , EndDate: $filter('date')($scope.document.Additional.INFOTYPE2001[0].EndDate, 'yyyy-MM-ddT00:00:00')
                    , BeginTime: $scope.document.Additional.INFOTYPE2001[0].BeginTime
                    , EndTime: $scope.document.Additional.INFOTYPE2001[0].EndTime
                    , AllDayFlag: $scope.document.Additional.INFOTYPE2001[0].AllDayFlag
                    , RequestNo: $scope.document.RequestNo
                };

                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.document.Additional.INFOTYPE2001[0].AbsenceHours = response.data.AbsenceHours;
                    $scope.document.Additional.INFOTYPE2001[0].AbsenceDays = response.data.AbsenceDays;
                    $scope.document.Additional.INFOTYPE2001[0].PayrollDays = response.data.PayrollDays;
                    $scope.loader.enable = false;

                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.loadDataAbsenceDay();

        }]);
})();