﻿(function () {
    angular.module('ESSMobile')
        .controller('DailyOTEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal) {

            //console.log($scope.document)
            $scope.TextMonth = $scope.Text["SYSTEM"];
            $scope.employeeData = getToken(CONFIG.USER);
            $scope.selectedBeginH = "";
            $scope.selectedBeginM = "";
            $scope.selectedEndH = "";
            $scope.selectedEndM = "";
            //console.log($scope.requesterData);
            if ($routeParams.otherParam) {
                $scope.document.Additional.InfoTable[0].Period = $routeParams.otherParam;
            }
            $scope.selectedPeriod = $scope.document.Additional.InfoTable[0].Period;

            var y = parseInt($scope.document.Additional.InfoTable[0].Period.substring(0, 4));
            var m = parseInt($scope.document.Additional.InfoTable[0].Period.substring(4, 6));

            $scope.maxlimit = new Date(y, m, 0).toISOString();
            $scope.minlimit = new Date(y, m - 1, 1).toISOString();

            $scope.oRequestParameter = {
                InputParameter: {}
                , CurrentEmployee: $scope.employeeData
                , Requestor: $scope.requesterData
                , Creator: getToken(CONFIG.USER)
                , Language: $scope.employeeData.Language
                , SelectedPeriod: $scope.document.Additional.InfoTable[0].Period
                , Period: $scope.document.Additional.InfoTable[0].Period
            };

            //angular.forEach($scope.document.Additional.InfoTable, function (item) {
            //    item.REQUESTORLEVEL= $scope.requesterData.EmpSubGroup,
            //});

            $scope.periodText = "";
            $scope.GetPeriodOTCreate = function () {
                //$scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/GetPeriodOTCreate',
                    data: $scope.oRequestParameter
                }).then(function successCallback(response) {
                    $scope.periodOTCreates = response.data;
                    if ($scope.periodOTCreates) {
                        if ($scope.periodOTCreates && $scope.document.Additional.InfoTable[0].Period) {
                            $scope.periods = $scope.periodOTCreates.filter(function (item) {
                                return item.PeriodValue === $scope.document.Additional.InfoTable[0].Period;
                            });
                            if ($scope.periods[0]) {
                                $scope.periodText = $scope.periods[0].PeriodText;
                            }
                            else {
                                var text_month_detail = "";
                                var str_month = $scope.document.Additional.InfoTable[0].Period.toString().substring(4, 6);
                                var str_year = $scope.document.Additional.InfoTable[0].Period.toString().substring(0, 4);
                                var int_month = parseInt(str_month);

                                text_month_detail = $scope.TextMonth["D_M" + int_month.toString()]
                                $scope.periodText = text_month_detail + " " + str_year;
                            }
                        }
                    }
                    //$scope.loader.enable = false;
                }, function errorCallback(response) {
                    //$scope.loader.enable = false;
                });
            };
            $scope.GetPeriodOTCreate();
            $scope.OFFs = [];
            $scope.GetListTimesheet = function () {
                $scope.oRequestParameter.PeriodValue = $scope.document.Additional.InfoTable[0].Period;
                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/GetListTimesheet',
                    data: $scope.oRequestParameter
                }).then(function successCallback(response) {

                    angular.forEach(response.data.TimeSheets, function (item) {
                        if (item.Type == "OFF" || item.Type == "HOL") {
                            var temp = new Date(item.Date);
                            $scope.OFFs.push(new Date(temp.getFullYear(), temp.getMonth() + 1, temp.getDate()));
                        }
                    });
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            //$scope.GetListTimesheet();
            $scope.temp = false;
            $scope.GetHoliday = function () {
                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/GetHoliday',
                    data: $scope.oRequestParameter
                }).then(function successCallback(response) {

                    angular.forEach(response.data, function (item) {
                        //console.log(item.Year);
                        $scope.OFFs.push(new Date(item.Year, parseInt(item.Month) - 1, parseInt(item.Day)));
                    });
                    $scope.temp = true;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetHoliday();

            


            $scope.selectManager = "";

            if ($scope.document.Additional.InfoTable && $scope.document.Additional.InfoTable.length > 0) {
                $scope.selectManager = $scope.document.Additional.InfoTable[0].REPORTTO;
            }

            $scope.$watch('selectManager', function (value) {
                if ($scope.selectManager) {
                    angular.forEach($scope.document.Additional.InfoTable, function (item) {
                        item.REPORTTO = $scope.selectManager;
                    });
                }
            });

            $scope.GetManagerOTCreate = function () {
                //$scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/GetManagerOTCreate',
                    data: $scope.oRequestParameter
                }).then(function successCallback(response) {
                    $scope.managerOTCreates = response.data;
                    //$scope.loader.enable = false;
                }, function errorCallback(response) {
                    //$scope.loader.enable = false;
                });
            };
            $scope.GetManagerOTCreate();

            $scope.GetOTWorkType = function () {
                //$scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/GetOTWorkType',
                    data: $scope.oRequestParameter
                }).then(function successCallback(response) {
                    $scope.oTWorkTypes = response.data;
                    //$scope.loader.enable = false;
                }, function errorCallback(response) {
                    //$scope.loader.enable = false;
                });
            };
            $scope.GetOTWorkType();
            
            $scope.CalculateOT = function () {

                $scope.document.ErrorText = "";
                $scope.document.HasError = false;
                $scope.content.isShowError = false;

                $scope.oRequestParameter.SelectedPeriod = $scope.document.Additional.InfoTable[0].Period;
                $scope.oRequestParameter.OTs = [];

                angular.forEach($scope.document.Additional.InfoTable, function (item) {
                    var temp = {
                        Description: item.description,
                        OTDATE: item.OTDATE,
                        BegindTimeH: item.selectedBeginH,
                        BegindTimeM: item.selectedBeginM,
                        EndTimeH: item.selectedEndH,
                        EndTimeM: item.selectedEndM
                    };
                    $scope.oRequestParameter.OTs.push(temp);
                });

                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/CalculateOT',
                    data: $scope.oRequestParameter
                }).then(function successCallback(response) {
                    $scope.oTLogs = response.data;
                    //$scope.document.Additional.InfoTable[0].TOTALAMOUNT = 7; /

                    var i = 0;
                    angular.forEach($scope.oTLogs, function (item) {
                        $scope.document.Additional.InfoTable[i].TOTALAMOUNT = item._TOTALAMOUNT;
                        $scope.document.Additional.DailyOTLog[i].OTimeEval = item.OTimeEval;
                        $scope.document.Additional.DailyOTLog[i].ClockTimePairs = item._ClockTimePairs;
                        $scope.document.Additional.DailyOTLog[i].FinalTimePairs = item._FinalTimePairs;
                        $scope.document.Additional.DailyOTLog[i].EvaTimePairs = item._EvaTimePairs;
                        $scope.document.Additional.DailyOTLog[i].Remark = item._Remark;
                        if (item._OTClockINOUT.length > 0) {
                            $scope.document.Additional.DailyOTLog[i].OTClockINOUT = item._OTClockINOUT;
                        }

                        if (item._OTimeEval != null && item._OTimeEval.length == 0) {
                            $scope.document.Additional.DailyOTLog[i].oTimeEval = null;
                        } else {
                            $scope.document.Additional.DailyOTLog[i].oTimeEval = item._OTimeEval;
                        }

                        $scope.document.Additional.DailyOTLog[i].RefRequestNo = item._RefRequestNo;
                        $scope.document.Additional.DailyOTLog[i].Description = item._Description;
                        $scope.document.Additional.DailyOTLog[i].RequestNo = item._RequestNo;
                        $scope.document.Additional.DailyOTLog[i].IsPayroll = item._IsPayroll;
                        $scope.document.Additional.DailyOTLog[i].FinalOTHour30 = item._FinalOTHour30;
                        $scope.document.Additional.DailyOTLog[i].FinalOTHour15 = item._FinalOTHour15;
                        $scope.document.Additional.DailyOTLog[i].FinalOTHour10 = item._FinalOTHour10;
                        $scope.document.Additional.DailyOTLog[i].EvaOTHour30 = item._EvaOTHour30;
                        $scope.document.Additional.DailyOTLog[i].EvaOTHour15 = item._EvaOTHour15;
                        $scope.document.Additional.DailyOTLog[i].EvaOTHour10 = item._EvaOTHour10;
                        $scope.document.Additional.DailyOTLog[i].Status = item._Status;
                        $scope.document.Additional.DailyOTLog[i].OTWorkTypeID = $scope.document.Additional.InfoTable[i].OTTYPE;
                        $scope.document.Additional.DailyOTLog[i].RequestOTHour30 = item._RequestOTHour30;
                        $scope.document.Additional.DailyOTLog[i].RequestOTHour15 = item._RequestOTHour15;
                        $scope.document.Additional.DailyOTLog[i].RequestOTHour10 = item._RequestOTHour10;
                        $scope.document.Additional.DailyOTLog[i].EndDate = item._EndDate;
                        $scope.document.Additional.DailyOTLog[i].BeginDate = item._BeginDate;
                        $scope.document.Additional.DailyOTLog[i].EmployeeID = item._EmployeeID;
                        $scope.document.Additional.DailyOTLog[i].CREATOR = $scope.oRequestParameter.Creator.EmployeeID;
                        i++;
                    });

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    //console.log(response);
                    $scope.document.ErrorText = response.data.ExceptionMessage;
                    $scope.document.HasError = true;
                    $scope.content.isShowError = true;
                    $scope.loader.enable = false;
                });
            };

            $scope.setOTDATE = function (selectedDate, index) {
                $scope.document.Additional.InfoTable[index].OTDATE = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };

            $scope.hours = ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23'];
            $scope.minutes = ['00', '30'];
            

            $scope.selectTime = function (item) {
                if (item.selectedBeginH && item.selectedBeginM) {
                    item.BEGINTIME = item.selectedBeginH + ":" + item.selectedBeginM;
                }
                if (item.selectedEndH && item.selectedEndM) {
                    item.ENDTIME = item.selectedEndH + ":" + item.selectedEndM;
                }
            };

            $scope.calH = function (item) {
                if (item) {
                    return (item / 60).toFixed(1);
                }
                return "";
            };
      
            $scope.addInfo = function () {
                $scope.document.ErrorText = "";
                $scope.document.HasError = false;
                $scope.content.isShowError = false;
                $scope.document.Additional.InfoTable.push({
                    "TOTALAMOUNT": null,
                    "OTDATE": null,
                    "REQUESTORLEVEL": $scope.requesterData.EmpSubGroup,
                    "OTTYPE": null,
                    "BEGINTIME": null,
                    "ENDTIME": null,
                    "REPORTTO": $scope.selectManager,
                    "Period": $scope.selectedPeriod,
                    "selectedBeginH": '00',
                    "selectedBeginM": '00',
                    "selectedEndH": '00',
                    "selectedEndM": '00'
                });

                $scope.document.Additional.DailyOTLog.push({
                    "TOTALAMOUNT": null,
                    "oTimeEval": null,
                    "ClockTimePairs": null,
                    "FinalTimePairs": null,
                    "EvaTimePairs": null,
                    "Remark": null,
                    "OTClockINOUT": null,
                    "EmployeeID": null,
                    "BeginDate": null,
                    "EndDate": null,
                    "RequestOTHour10": null,
                    "RequestOTHour15": null,
                    "RequestOTHour30": null,
                    "Status": null,
                    "EvaOTHour10": null,
                    "EvaOTHour15": null,
                    "EvaOTHour30": null,
                    "FinalOTHour10": null,
                    "FinalOTHour15": null,
                    "FinalOTHour30": null,
                    "IsPayroll": null,
                    "RequestNo": null,
                    "Description": null,
                    "RefRequestNo": null,
                    "OTWorkTypeID": null,
                    "CREATOR": null
                });
            };

            $scope.deleteItem = function (index) {
                Swal.fire({
                    title: $scope.Text['ACCOUNT_SETTING']['CONFIRM_DELETE'],
                    //text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: $scope.Text['SYSTEM']['BUTTON_YES'],
                    cancelButtonText: $scope.Text['SYSTEM']['BUTTON_NO']
                }).then((result) => {
                    if (result.value) {
                        $scope.document.Additional.InfoTable.splice(index, 1);
                        $scope.document.Additional.DailyOTLog.splice(index, 1);

                        $scope.$apply();
                    }
                });

            };

            $scope.getTypeName = function (typeId) {
                if ($scope.oTWorkTypes && typeId) {
                    $scope.attTypes = $scope.oTWorkTypes.filter(function (item) {
                        return item.__otWorkTypeID === typeId;
                    });
                    return $scope.attTypes[0].__otWorkTypeDesc;
                }
                return "";
            };

            $scope.addInfoByModal = function (item) {
                $scope.document.ErrorText = "";
                $scope.document.HasError = false;
                $scope.content.isShowError = false;
                $scope.document.Additional.InfoTable.push(item);
                $scope.document.Additional.DailyOTLog.push({
                    "TOTALAMOUNT": null,
                    "oTimeEval": null,
                    "ClockTimePairs": null,
                    "FinalTimePairs": null,
                    "EvaTimePairs": null,
                    "Remark": null,
                    "OTClockINOUT": null,
                    "EmployeeID": null,
                    "BeginDate": null,
                    "EndDate": null,
                    "RequestOTHour10": null,
                    "RequestOTHour15": null,
                    "RequestOTHour30": null,
                    "Status": null,
                    "EvaOTHour10": null,
                    "EvaOTHour15": null,
                    "EvaOTHour30": null,
                    "FinalOTHour10": null,
                    "FinalOTHour15": null,
                    "FinalOTHour30": null,
                    "IsPayroll": null,
                    "RequestNo": null,
                    "Description": null,
                    "RefRequestNo": null,
                    "OTWorkTypeID": null,
                    "CREATOR": null
                });
                $scope.CalculateOT();
            };

            $scope.editInfo = function (index, item) {
                $scope.document.Additional.InfoTable[index] = item;
                $scope.CalculateOT();
            };

            $scope.openDaily = function (mode, index, item) {
                if (item == null) {
                    item = {
                        "TOTALAMOUNT": null,
                        "OTDATE": null,
                        "REQUESTORLEVEL": $scope.requesterData.EmpSubGroup,
                        "OTTYPE": null,
                        "BEGINTIME": null,
                        "ENDTIME": null,
                        "REPORTTO": $scope.selectManager,
                        "selectedBeginH": '00',
                        "selectedBeginM": '00',
                        "selectedEndH": '00',
                        "selectedEndM": '00'
                    };
                } 
          
                var modalInstance = $uibModal.open({
                    animation: $scope.animationsEnabled,
                    templateUrl: 'dailyModal.ng-popup.html',
                    controller: 'dailyModalController',
                    backdrop: 'static',
                    keyboard: false,
                    size: 'sp',
                    resolve: {
                        items: function () {
                            var data = {
                                types: $scope.oTWorkTypes,
                                text: $scope.Text,
                                mode: mode,
                                model: item,
                                offs: $scope.OFFs,
                                maxlimit: $scope.maxlimit,
                                minlimit: $scope.minlimit,
                                Save: function (a) {
                                    if (mode == 'add') {
                                        a.Period = $scope.selectedPeriod;
                                        $scope.addInfoByModal(a);
                                    } else if (mode == 'edit') {
                                        $scope.editInfo(index, a);
                                    } 
                                }
                            };
                            return data;
                        }
                    }
                });
                //getValidatePinCode();
            };


            $scope.ChildAction.SetData = function () {
                //GetCalendar();

            };

            $scope.ChildAction.LoadData = function () {
                //$scope.CalculateOT();
            };
            
            $scope.ChildAction.SetData();

        }])
        .controller('dailyModalController', ['items', '$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', '$uibModalInstance', function (items, $scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal, $uibModalInstance) {

            //console.log(items);
            $scope.model = items.model;
            $scope.mode = items.mode;
            $scope.Text = items.text;
            $scope.oTWorkTypes = items.types;
            $scope.maxlimit = items.maxlimit;
            $scope.minlimit = items.minlimit;
            $scope.OFFs = items.offs;
            

            $scope.save = function () {
                items.Save($scope.model);
                $uibModalInstance.close();
            };


            $scope.hours = ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24'];
            $scope.minutes = ['00', '30'];

            $scope.selectTime = function (item) {
                if (item.selectedBeginH && item.selectedBeginM) {
                    item.BEGINTIME = item.selectedBeginH + ":" + item.selectedBeginM;
                }
                if (item.selectedEndH && item.selectedEndM) {
                    item.ENDTIME = item.selectedEndH + ":" + item.selectedEndM;
                }
            };

            $scope.setOTDATE = function (selectedDate) {
                $scope.model.OTDATE = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };

        }]);
})();