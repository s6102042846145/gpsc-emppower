﻿(function () {
    angular.module('ESSMobile')
        .controller('PeriodViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {


            // Sotring Data
            $scope.loadSortingData = function () {

                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                    , CreatePeriod: $scope.document.Additional
                };

                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/DocumentPeriodSort',
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.document.Additional = response.data;

                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });

            };
            $scope.loadSortingData();
            

            $scope.ChildAction.SetData = function () {
                //GetCalendar();

            };

            $scope.ChildAction.LoadData = function () {

            };

            $scope.ChildAction.SetData();
            
        }]);
})();