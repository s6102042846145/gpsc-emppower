﻿(function () {
    angular.module('ESSMobile')
        .controller('DutyPaymentViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {

            $scope.ChildAction.SetData = function () {

            };

            $scope.ChildAction.LoadData = function () {

                
            };


            var employeeDate = getToken(CONFIG.USER);
            $scope.ChildAction.SetData();

            $scope.text_desciption = "";
            $scope.loadPeriod = function () {

                var URL = CONFIG.SERVER + 'HRTM/GetPeriodDutyPayment';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    var year = $scope.document.Data[0].DUTYDATE.substring(0, 4);
                    var month = $scope.document.Data[0].DUTYDATE.substring(5, 7);
                    var concat_data = year + month;

                    $scope.valid_period = response.data.filter(function (element) {
                        return element.PeriodValue === concat_data;
                    });

                    if ($scope.valid_period.length > 0) {
                        $scope.text_desciption = $scope.valid_period[0].PeriodText;
                    }

                    console.log(response);

                    $scope.loader.enable = false;

                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.loadPeriod();



            $scope.data_dailyWs = {};
            $scope.loaddailyType = function () {

                var URL = CONFIG.SERVER + 'HRTM/GetDataDaliyWs';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                    , ListDocumentDutyPayment: $scope.document.Additional.DutyPaymentLog
                };

                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    console.log(response.data);
                    $scope.data_dailyWs = response.data;
                    $scope.loader.enable = false;

                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.loaddailyType();


            $scope.loadPeriod = function () {

                var URL = CONFIG.SERVER + 'HRTM/GetPeriodDutyPayment';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.period_data = response.data;

                    $scope.valid_period = $scope.period_data.filter(function (element) {
                        return element.PeriodValue === $scope.document.Additional.PeriodDutyPayment[0].SelectPeriod;
                    });

                    if ($scope.valid_period.length > 0) {
                        $scope.text_desciption = $scope.valid_period[0].PeriodText;
                    }

                    $scope.loader.enable = false;

                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.loadPeriod();
          



        }]);
})();