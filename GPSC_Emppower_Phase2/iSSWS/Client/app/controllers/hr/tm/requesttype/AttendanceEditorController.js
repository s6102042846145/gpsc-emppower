﻿(function () {
    angular.module('ESSMobile')
        .controller('AttendanceEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {
            
            
            $scope.date_now = new Date();
            $scope.employeeData = getToken(CONFIG.USER);
            //console.log($scope.employeeData.HiringDate);
            //console.log($scope.document);
           
            $scope.minlimit = $scope.employeeData.HiringDate;

             //$scope.maxlimit = "2020-4-15";
            //date - min - limit="{{minlimit}}" date - max - limit="{{maxlimit}}"

            $scope.oRequestParameter = {
                InputParameter: {}
                , CurrentEmployee: $scope.employeeData
                , Requestor: $scope.requesterData
                //, Requestor: $scope.document.Requestor
                , Creator: getToken(CONFIG.USER)
                , Language: $scope.employeeData.Language
            };

            $scope.GetAttendanceTypeList = function () {
                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/GetAttendanceTypeList',
                    data: $scope.oRequestParameter
                }).then(function successCallback(response) {
                    $scope.attendanceTypes = response.data;
                    if ($scope.attendanceTypes.length > 0) {
                        if (!$scope.temp.AttendanceType)
                            $scope.temp.AttendanceType = $scope.attendanceTypes[0].Key;
                    }

                    //$scope.loader.enable = false;
                }, function errorCallback(response) {
                    //$scope.loader.enable = false;
                });
            };

            
            $scope.GetAttendanceTime = function () {
                if (!$scope.temp.AttendanceType)
                    return;
                $scope.oRequestParameter.AttendanceDate = $scope.temp.BeginDate;
                $scope.oRequestParameter.AttendanceDateTo = $scope.temp.EndDate;
                $scope.oRequestParameter.AttendanceType = $scope.temp.AttendanceType;

                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/GetAttendanceTime',
                    data: $scope.oRequestParameter
                }).then(function successCallback(response) {
                    $scope.attendanceTimes = response.data;
                    if ($scope.attendanceTimes.objAttTime.length > 0) {
                        if ($scope.temp.AttendanceType != '2080' && $scope.temp.AttendanceType != '2015' && $scope.temp.AttendanceType != '2025' && $scope.temp.AttendanceType != '2030' && $scope.temp.AttendanceType != '2035') {
                            if ($scope.temp.BeginTime == "00:00:00" && !$scope.temp.AllDayFlag) {
                                $scope.temp.selectTime = $scope.attendanceTimes.objAttTime[0].AttID;
                            }
                            else if (!$scope.temp.AllDayFlag) {
                                $scope.temp.selectTime = $scope.temp.BeginTime.substring(0, 5) + " - " + $scope.temp.EndTime.substring(0, 5);
                            } else {
                                $scope.temp.selectTime = $scope.attendanceTimes.objAttTime[$scope.attendanceTimes.objAttTime.length - 1].AttID
                            }
                        }
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
          


            //$scope.GetCalendar = function () {
            //    $scope.loader.enable = true;
            //    $http({
            //        method: 'POST',
            //        url: CONFIG.SERVER + 'HRTM/GetCalendar',
            //        data: $scope.oRequestParameter
            //    }).then(function successCallback(response) {

            //        $scope.loader.enable = false;
            //    }, function errorCallback(response) {
            //        $scope.loader.enable = false;
            //    });
            //};
            //$scope.GetCalendar();
            
            $scope.changeTime = function (a) {
                if (a) {
                    $scope.temp.selectTime = a;
                }
                
            };
            
            $scope.$watchGroup(['temp.selectTime'], function (value) {
                if ($scope.temp.selectTime) {
                    $scope.temp.BeginTime = $scope.temp.selectTime.substring(0, 5) + ":00";
                    $scope.temp.EndTime = $scope.temp.selectTime.substring(8, 13) + ":00";
                }
            });
            $scope.$watchGroup(['temp.AttendanceType', 'temp.BeginDate', 'temp.EndDate', 'temp.BeginTime', 'temp.EndTime'], function (value) {
              
                $scope.GetAttendanceTime();
                $scope.temp.SubType = $scope.temp.AttendanceType;
                $scope.temp.AllDayFlag = false;

                if ($scope.temp.AttendanceType == '2070' || $scope.temp.AttendanceType == '2065' || $scope.temp.AttendanceType == '2040' || $scope.temp.AttendanceType == '2050' || $scope.temp.AttendanceType == '2080') {
                    $scope.temp.EndDate = $scope.temp.BeginDate;
                }

                if ($scope.temp.AttendanceType == '2080' || $scope.temp.AttendanceType == '2015' || $scope.temp.AttendanceType == '2025' || $scope.temp.AttendanceType == '2030' || $scope.temp.AttendanceType == '2035') {
                    $scope.temp.BeginTime = "00:00:00";
                    $scope.temp.EndTime = "00:00:00";
                    $scope.temp.AllDayFlag = true;
                } else {
                    if ($scope.attendanceTimes && $scope.attendanceTimes.objAttTime.length>0) {
                        if ($scope.temp.BeginDate != $scope.temp.EndDate
                            || ($scope.temp.BeginDate != $scope.temp.EndDate && $scope.temp.selectTime == $scope.attendanceTimes.objAttTime[$scope.attendanceTimes.objAttTime.length - 1].AttID)) {
                            $scope.temp.AllDayFlag = true;
                            $scope.temp.BeginTime = "00:00:00";
                            $scope.temp.EndTime = "00:00:00";
                        }

                        if ($scope.temp.selectTime == $scope.attendanceTimes.objAttTime[$scope.attendanceTimes.objAttTime.length - 1].AttID) {
                            $scope.temp.AllDayFlag = true;
                            $scope.temp.BeginTime = "00:00:00";
                            $scope.temp.EndTime = "00:00:00";
                        }
                    }
                }


                $scope.getArchiveTimepairAttendance();
            });
            
            //$scope.$watchGroup(['temp.selectTime', 'temp.BeginTime', 'temp.EndTime'], function (value) {
            //    $scope.temp.AllDayFlag = false;
            //    if ($scope.temp.AttendanceType == '2080' || $scope.temp.AttendanceType == '2015' || $scope.temp.AttendanceType == '2025' || $scope.temp.AttendanceType == '2030' || $scope.temp.AttendanceType == '2035') {
            //        $scope.temp.BeginTime = "00:00:00";
            //        $scope.temp.EndTime = "00:00:00";
            //        $scope.temp.AllDayFlag = true;
            //    } else {
            //        if ($scope.attendanceTimes && $scope.attendanceTimes.objAttTime.length > 0) {
            //            if ($scope.temp.BeginDate != $scope.temp.EndDate
            //                || ($scope.temp.BeginDate != $scope.temp.EndDate && $scope.temp.selectTime == $scope.attendanceTimes.objAttTime[$scope.attendanceTimes.objAttTime.length - 1].AttID)) {
            //                $scope.temp.AllDayFlag = true;
            //                $scope.temp.BeginTime = "00:00:00";
            //                $scope.temp.EndTime = "00:00:00";
            //            }

            //            if ($scope.temp.selectTime == $scope.attendanceTimes.objAttTime[$scope.attendanceTimes.objAttTime.length - 1].AttID) {
            //                $scope.temp.AllDayFlag = true;
            //                $scope.temp.BeginTime = "00:00:00";
            //                $scope.temp.EndTime = "00:00:00";
            //            }
            //        }
            //    }
            //});

            
            $scope.setBeginDate = function (selectedDate) {
                $scope.temp.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                if ($scope.temp.BeginDate > $scope.temp.EndDate) {
                    $scope.temp.EndDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                }
             
                if ($scope.temp.AttendanceType == '2070' || $scope.temp.AttendanceType == '2065' || $scope.temp.AttendanceType == '2040' || $scope.temp.AttendanceType == '2050' || $scope.temp.AttendanceType == '2080') {
                    $scope.temp.EndDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                }
            };

            $scope.setEndDate = function (selectedDate) {
                $scope.temp.EndDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                if ($scope.temp.BeginDate > $scope.temp.EndDate) {
                    $scope.temp.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                }
            };

            $scope.temps = [];
            $scope.validateForAdd = function (item) {

                $scope.oRequestParameter.ListAttendance = $scope.document.Additional.INFOTYPE2002;
                $scope.oRequestParameter.NewAttendance = item;
                if ($scope.index_edit == null) {
                    $scope.oRequestParameter.RowId = -1;
                } else {
                    $scope.oRequestParameter.RowId = $scope.index_edit;
                }

                $scope.oRequestParameter.DocumentState = "";
                $scope.oRequestParameter.Type = item.AttendanceType;

                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/ValidateListAttendance',
                    data: $scope.oRequestParameter
                }).then(function successCallback(response) {
                    if (response.data.Valid) {
                        $scope.CalculateAttendanceEntry(item);
                    }
                    $scope.document.ErrorText = "";
                    $scope.document.HasError = false;
                    $scope.content.isShowError = false;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.document.ErrorText = response.data.ExceptionMessage;
                    $scope.document.HasError = true;
                    $scope.content.isShowError = true;
                    $scope.loader.enable = false;
                });
            };

            $scope.getTypeName = function (typeId) {
                if ($scope.attendanceTypes) {
                    $scope.attTypes = $scope.attendanceTypes.filter(function (item) {
                        return item.Key === typeId;
                    });
                    return $scope.attTypes[0].Description;
                }
                return "";
            };

            $scope.CalculateAttendanceEntry = function (item) {
                $scope.oRequestParameter.Info = item;
                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/CalculateAttendanceEntry',
                    data: $scope.oRequestParameter
                }).then(function successCallback(response) {
                    item.PayrollDays = response.data.PayrollDays;
                    item.AttendanceHours = response.data.AttendanceHours;
                    item.AttendanceDays = response.data.AttendanceDays;

                    var _item = angular.copy(item)

                    $scope.timetexts = $scope.attendanceTimes.objAttTime.filter(function (item) {
                        return item.AttID === $scope.temp.selectTime;
                    });
                    console.log($scope.timetexts[0].AttName);
                    _item.TimeText = $scope.timetexts[0].AttName

                    if ($scope.index_edit != null) {
                        $scope.document.Additional.INFOTYPE2002[$scope.index_edit] = _item;
                    } else {
                        $scope.document.Additional.INFOTYPE2002.push(_item);
                    }
                    $scope.index_edit = null;
                    $scope.getNewItem();

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };


            $scope.temp = {};
            $scope.getNewItem = function () {
                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/GetINFOTYPE2002',
                    data: $scope.oRequestParameter
                }).then(function successCallback(response) {
                    $scope.temp = response.data;
                    $scope.GetAttendanceTypeList();
                    //$scope.loader.enable = false;
                }, function errorCallback(response) {
                    //$scope.loader.enable = false;
                });
            };
            $scope.getNewItem();

            $scope.index_edit = null;
            $scope.deleteItem = function (index) {
                Swal.fire({
                    title: $scope.Text['ACCOUNT_SETTING']['CONFIRM_DELETE'],
                    //text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: $scope.Text['SYSTEM']['BUTTON_YES'],
                    cancelButtonText: $scope.Text['SYSTEM']['BUTTON_NO'],
                }).then((result) => {
                    if (result.value) {
                        $scope.document.Additional.INFOTYPE2002.splice(index, 1);
                        $scope.$apply();
                    }
                });

            };

            $scope.editItem = function (index, item) {
                $scope.index_edit = index;
                $scope.temp = angular.copy(item);
            };

            $scope.cancel = function () {
                $scope.index_edit = null;
                $scope.getNewItem();
            };


            $scope.getArchiveTimepairAttendance = function () {
                $scope.loader.enable = true;
                if ($scope.temp.AttendanceType) {
                    $scope.oRequestParameter.AttendanceDate = $scope.temp.BeginDate;
                    $scope.oRequestParameter.AttendanceDateTo = $scope.temp.EndDate;
                    $scope.oRequestParameter.AttendanceType = $scope.temp.AttendanceType;

                    $http({
                        method: 'POST',
                        url: CONFIG.SERVER + 'HRTM/GetArchiveTimepairAttendance',
                        data: $scope.oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.archiveTimepairAttendance = response.data;
                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                    });
                }
            };

            $scope.GetStatusClass = function (item) {
                //var dateNow = new Date();
                //var date = new Date(item.Date)
                //if (item.Type == 'OFF' || item.Type == 'HOL')
                //    return 'active';
                //if (dateNow > date && (item.ClockIn == null || item.ClockOut == null))
                //    return 'state-error';

                var dateNow = new Date();
                var date = new Date(item.Date);
                if (item.Type == 'OFF' || item.Type == 'HOL')
                    return 'active';

                //if (dateNow > date && (item.ClockIn == null || item.ClockOut == null))
                //    return 'state-error';

                var dataDatetimeNow = $filter('date')(dateNow, 'yyyy-MM-ddT00:00:00');
                var dataDateTimeTimeSheet = $filter('date')(date, 'yyyy-MM-ddT00:00:00');

                //if (dateNow > date)
                //{
                //    if (!item.IsCorrectPolicy) {
                //        if (item.ClockIn == null || item.ClockOut == null) {
                //            return 'state-error';
                //        }
                //    }
                //}

                if (dataDatetimeNow == dataDateTimeTimeSheet) {
                    return '';
                }

                if (item.IsNoScanCard) {
                    return '';
                }

                if (dataDatetimeNow > dataDateTimeTimeSheet) {

                    if (!item.IsCorrectPolicy) {
                        if (item.ClockIn == null || item.ClockOut == null) {
                            return 'state-error';
                        }
                    }
                }

                return '';
            };
            $scope.sumCLOCKINLATE = function (items) {
                if (items) {
                    var temps = items.filter(a => a.ClockInLate);
                    return temps.length;
                }

            };
            $scope.sumCLOCKOUTEARLY = function (items) {
                if (items) {
                    var temps = items.filter(a => a.ClockoutEarly);
                    return temps.length;
                }
            };
            $scope.sumCLOCKABNORMAL = function (items) {
                if (items) {
                    var temps = items.filter(a => a.ClockNoneWork);
                    return temps.length;
                }
            };
            $scope.ChildAction.SetData = function () {
                //GetCalendar();

            };

            $scope.ChildAction.LoadData = function () {

            };

            $scope.ChildAction.SetData();

        }]);
})();