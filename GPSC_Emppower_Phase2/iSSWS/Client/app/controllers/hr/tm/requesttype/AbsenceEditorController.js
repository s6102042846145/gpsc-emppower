﻿(function () {
    angular.module('ESSMobile')
        .controller('AbsenceEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal) {
            console.log('appsent');
            $scope.searchDataEmployee = {
                textSearch: ""
            };

            $scope.employeeSubstitute = {
                empId: "",
                empName: "",
                empPosition: "",
                empOrganize: ""
            };
            $scope.showTextSubStitute = false;

            // Ayodia Implement
            // $scope.document.Additional.INFOTYPE2001; เป็นข้อมูลตั้งต้นจาก framework โดยจะ geenrate มาให้่ทุกครั้งตามประเภท Request  type
            // $scope.InputFormTravel.BeginDate = $filter('date')(moment(BeginDateEndate_a[0], "DD/MM/YYYY").toDate(), 'yyyy-MM-ddT00:00:00');
            $scope.ChildAction.SetData = function () {


                if ($scope.document.Additional.DELEGATEDATA[0].DelegateTo) {

                    var URL = CONFIG.SERVER + 'HRTM/GetObjectEmployeeEmppower';
                    $scope.employeeData = getToken(CONFIG.USER);

                    var oRequestParameter = {
                        InputParameter: {
                            EmployeeId: $scope.document.Additional.DELEGATEDATA[0].DelegateTo
                        }
                        , CurrentEmployee: $scope.employeeData
                        , Requestor: $scope.requesterData
                        , Creator: getToken(CONFIG.USER)
                    };

                    $scope.loader.enable = true;

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {

                        $scope.showTextSubStitute = true;

                        $scope.employeeSubstitute = {
                            empId: response.data[0].EmployeeID,
                            empName: response.data[0].EmployeeName,
                            empPosition: response.data[0].PositionName,
                            empOrganize: response.data[0].OrgUnitName
                        };

                        $scope.searchDataEmployee.textSearch = $scope.document.Additional.DELEGATEDATA[0].DelegateTo;

                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                    });
                }

            };

            $scope.ChildAction.LoadData = function () {

            };

            var employeeDate = getToken(CONFIG.USER);
            $scope.ChildAction.SetData();


            $scope.date_now = new Date();
            $scope.model = {
                AbsentPeriod: ""
            };




            $scope.listEmp = [];
            //$scope.searhceEmployee = function (textData) {
            //    if (textData.trim()) {

            //        var URL = CONFIG.SERVER + 'HRTM/SearchEmployeeDataEmppower';
            //        $scope.employeeData = getToken(CONFIG.USER);
            //        var oRequestParameter = {
            //            InputParameter: {
            //                "SEARCHTEXT": textData
            //            }
            //            , CurrentEmployee: $scope.employeeData
            //            , Requestor: $scope.requesterData
            //            , Creator: getToken(CONFIG.USER)
            //        };

            //        $http({
            //            method: 'POST',
            //            url: URL,
            //            data: oRequestParameter
            //        }).then(function successCallback(response) {

            //            $scope.loader.enable = false;
            //            var modalInstance = $uibModal.open({
            //                animation: $scope.animationsEnabled,
            //                templateUrl: 'ListEmployeeSubstitute.ng-popup.html',
            //                controller: 'ListEmployeeSubstituteController',
            //                backdrop: 'static',
            //                keyboard: false,
            //                size: 'lg',
            //                resolve: {
            //                    items: function () {
            //                        var data = {
            //                            employee: response.data.Table,
            //                            textDescription: $scope.Text,
            //                            fnShowEmployee: function (item) {
            //                                $scope.selectEmployeeSubstitute(item);
            //                            }
            //                        };
            //                        return data;
            //                    }
            //                }
            //            });


            //        }, function errorCallback(response) {
            //            $scope.loader.enable = false;
            //        });
            //    }
            //    else {

            //        $scope.document.Additional.DELEGATEDATA[0].DelegateTo = "";
            //        $scope.employeeSubstitute.empId = "";
            //        $scope.employeeSubstitute.empName = "";
            //        $scope.employeeSubstitute.empPosition = "";
            //        $scope.employeeSubstitute.empOrganize = "";
            //        $scope.showTextSubStitute = false;
            //    }
            //};

            $scope.selectEmployeeSubstitute = function (emp) {

                $scope.employeeSubstitute.empId = emp.EmployeeID;
                $scope.employeeSubstitute.empName = emp.EmployeeName;
                $scope.employeeSubstitute.empPosition = emp.PositionName;
                $scope.employeeSubstitute.empOrganize = emp.OrgUnitName;

                $scope.document.Additional.DELEGATEDATA[0].DelegateTo = emp.EmployeeID;
                $scope.showTextSubStitute = true;
            };



            $scope.setSelectedBegin = function (selectedDate) {

                $scope.document.Additional.INFOTYPE2001[0].BeginDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                $scope.document.Additional.DELEGATEDATA[0].BeginDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');

                if ($scope.document.Additional.INFOTYPE2001[0].BeginDate > $scope.document.Additional.INFOTYPE2001[0].EndDate) {
                    $scope.document.Additional.INFOTYPE2001[0].EndDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                    $scope.document.Additional.DELEGATEDATA[0].EndDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                }

                $scope.getAbsenceTime();
            };

            $scope.setSelectedEnd = function (selectedDate) {

                console.log(selectedDate);
                $scope.document.Additional.INFOTYPE2001[0].EndDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                $scope.document.Additional.DELEGATEDATA[0].EndDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');

                if ($filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00') < $scope.document.Additional.INFOTYPE2001[0].BeginDate) {
                    $scope.document.Additional.INFOTYPE2001[0].BeginDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                    $scope.document.Additional.DELEGATEDATA[0].BeginDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                }


                $scope.getAbsenceTime();

            };

            // Get All Type Leave
            $scope.list_leavetype_all = [];
            $scope.load_leavetype_all = function () {

                var URL = CONFIG.SERVER + 'HRTM/GetAbsenceTypeList';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data.length > 0) {
                        $scope.list_leavetype_all = response.data;
                        if (!$scope.document.Additional.INFOTYPE2001[0].AbsenceType) {

                            if ($routeParams.otherParam) {

                                $scope.checktype = $scope.list_leavetype_all.filter(function (element) {
                                    return element.Key == $routeParams.otherParam;
                                });

                                if ($scope.checktype.length > 0)
                                    $scope.document.Additional.INFOTYPE2001[0].AbsenceType = $scope.data_test = $routeParams.otherParam;
                                else
                                    $scope.document.Additional.INFOTYPE2001[0].AbsenceType = $scope.list_leavetype_all[0].Key; // Default ลาพักผ่อนประจำปี
                            }
                            else {
                                $scope.document.Additional.INFOTYPE2001[0].AbsenceType = $scope.list_leavetype_all[0].Key; // Default ลาพักผ่อนประจำปี
                            }
                        }

                        $scope.getAbsenceTime();
                    }
                    else {
                        $scope.model.leave_time_id = 0;
                    }

                    $scope.loader.enable = false;

                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.load_leavetype_all();

            // Get Time Period
            $scope.list_period = [];
            $scope.list_index_period = [];
            $scope.getAbsenceTime = function () {

                // พวกลาครึ่งวันที่เป็นวันเดียวกับ BeginDate = EndDate
                if ($scope.document.Additional.INFOTYPE2001[0].AbsenceType == '0210'
                    || $scope.document.Additional.INFOTYPE2001[0].AbsenceType == '0230'
                    || $scope.document.Additional.INFOTYPE2001[0].AbsenceType == '0310') {

                    $scope.document.Additional.INFOTYPE2001[0].EndDate = $scope.document.Additional.INFOTYPE2001[0].BeginDate;
                    $scope.document.Additional.DELEGATEDATA[0].EndDate = $scope.document.Additional.DELEGATEDATA[0].BeginDate;
                }



                var URL = CONFIG.SERVER + 'HRTM/GetAbsenceTime';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {
                        "AbsenceDate": $filter('date')($scope.document.Additional.INFOTYPE2001[0].BeginDate, 'yyyy-MM-ddT00:00:00'),
                        "AbsenceDateTo": $filter('date')($scope.document.Additional.INFOTYPE2001[0].EndDate, 'yyyy-MM-ddT00:00:00'),
                        "AbsenceType": $scope.document.Additional.INFOTYPE2001[0].AbsenceType
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.list_period = response.data.objAbsTime;
                    $scope.list_index_period = Object.keys(response.data.objAbsTime);
                    if ($scope.list_index_period.length > 0) {

                        if (!$scope.document.Additional.INFOTYPE2001[0].AbsentPeriod) {
                            $scope.document.Additional.INFOTYPE2001[0].AbsentPeriod = $scope.list_index_period[0];
                        }


                        var text_change = $scope.list_period[$scope.list_index_period[0]];
                        if (text_change === "ทั้งวัน" || text_change === "Full day") {

                            $scope.document.Additional.INFOTYPE2001[0].BeginTime = "00:00:00";
                            $scope.document.Additional.INFOTYPE2001[0].EndTime = "00:00:00";
                            $scope.document.Additional.INFOTYPE2001[0].TimeText = $scope.Text["HRTMABSENCE"].FULLDAY;

                            $scope.calPayrollIsAllDay();

                            //Add by Nipon 17/09/2020 
                            //var dateOut1 = new Date($scope.document.Additional.INFOTYPE2001[0].BeginDate);
                            //var dateOut2 = new Date($scope.document.Additional.INFOTYPE2001[0].EndDate);

                            ////var diffDays = Math.abs(dateOut2.getDate() - dateOut1.getDate()) + 1;

                            //var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds    
                            //var diffDays = Math.round(Math.abs((dateOut1.getTime() - dateOut2.getTime()) / (oneDay))) + 1;

                            //$scope.document.Additional.INFOTYPE2001[0].PayrollDays = diffDays;
                            //
                        }
                        else {
                            $scope.document.Additional.INFOTYPE2001[0].BeginTime = $scope.document.Additional.INFOTYPE2001[0].AbsentPeriod.substring(0, 5) + ":00";
                            $scope.document.Additional.INFOTYPE2001[0].EndTime = $scope.document.Additional.INFOTYPE2001[0].AbsentPeriod.substring(8, 13) + ":00";
                            var oTimeText = $scope.list_period[$scope.document.Additional.INFOTYPE2001[0].AbsentPeriod];
                            $scope.document.Additional.INFOTYPE2001[0].TimeText = oTimeText;

                            //Add by Nipon 17/09/2020 
                            $scope.document.Additional.INFOTYPE2001[0].PayrollDays = 0.5;
                            //
                        }


                    }
                    else {
                        $scope.document.Additional.INFOTYPE2001[0].BeginTime = "00:00:00";
                        $scope.document.Additional.INFOTYPE2001[0].EndTime = "00:00:00";
                    }

                    $scope.document.Additional.INFOTYPE2001[0].AllDayFlag = response.data.stateIsFullday;

                    $scope.getTimeInOut_CardSwipeTime();

                    $scope.loader.enable = false;

                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };


            $scope.calPayrollIsAllDay = function () {
                var URL = CONFIG.SERVER + 'HRTM/CalAbsenceIsAllDay';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                    , AbsenceType: $scope.document.Additional.INFOTYPE2001[0].AbsenceType
                    , BeginDate: $filter('date')($scope.document.Additional.INFOTYPE2001[0].BeginDate, 'yyyy-MM-ddT00:00:00')
                    , EndDate: $filter('date')($scope.document.Additional.INFOTYPE2001[0].EndDate, 'yyyy-MM-ddT00:00:00')
                };

                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.document.Additional.INFOTYPE2001[0].PayrollDays = response.data.PayrollDays;

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };

            // Change Period
            $scope.choose_period = function (timespan) {

                var text_time = $scope.list_period[timespan];
                if (text_time === "ทั้งวัน" || text_time === "Full day") {

                    $scope.document.Additional.INFOTYPE2001[0].AllDayFlag = true;
                    $scope.document.Additional.INFOTYPE2001[0].BeginTime = "00:00:00";
                    $scope.document.Additional.INFOTYPE2001[0].EndTime = "00:00:00";
                    $scope.document.Additional.INFOTYPE2001[0].TimeText = $scope.Text["HRTMABSENCE"].FULLDAY;

                    $scope.calPayrollIsAllDay();

                    //Add by Nipon 17/09/2020 
                    //var dateOut1 = new Date($scope.document.Additional.INFOTYPE2001[0].BeginDate);
                    //var dateOut2 = new Date($scope.document.Additional.INFOTYPE2001[0].EndDate);

                    //var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds    
                    //var diffDays = Math.round(Math.abs((dateOut1.getTime() - dateOut2.getTime()) / (oneDay))) + 1;

                    //$scope.document.Additional.INFOTYPE2001[0].PayrollDays = diffDays;
                    //
                }
                else {

                    $scope.document.Additional.INFOTYPE2001[0].BeginTime = timespan.substring(0, 5) + ":00";
                    $scope.document.Additional.INFOTYPE2001[0].EndTime = timespan.substring(8, 13) + ":00";
                    $scope.document.Additional.INFOTYPE2001[0].AllDayFlag = false;

                    //Add by Nipon 17/09/2020
                    $scope.document.Additional.INFOTYPE2001[0].PayrollDays = 0.5;
                    //

                    var oTimeText = $scope.list_period[timespan];
                    $scope.document.Additional.INFOTYPE2001[0].TimeText = oTimeText;
                }

                $scope.getTimeInOut_CardSwipeTime();
            };

            // เวลาเข้า - ออก และเวลารูดบัตร
            $scope.dataTimeSheet = {
                TimeSheets: []
                , TimeElements: []
                , IsVisibleTimeElement: false
                , IsVisibleTimePair: false
            };
            $scope.count_work_last = 0;
            $scope.getTimeInOut_CardSwipeTime = function () {

                $scope.objTimeElement = [];
                $scope.objTimePair = [];

                var URL = CONFIG.SERVER + 'HRTM/GetArchiveTimepair';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {
                        "AbsenceDate": $filter('date')($scope.document.Additional.INFOTYPE2001[0].BeginDate, 'yyyy-MM-ddT00:00:00'),
                        "AbsenceDateTo": $filter('date')($scope.document.Additional.INFOTYPE2001[0].EndDate, 'yyyy-MM-ddT00:00:00'),
                        "AbsenceType": $scope.document.Additional.INFOTYPE2001[0].AbsenceType,
                        "IsFullDay": $scope.document.Additional.INFOTYPE2001[0].AllDayFlag
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $scope.count_work_last = 0;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.dataTimeSheet.TimeSheets = response.data.TimeSheets;
                    $scope.check_work_late = response.data.TimeSheets.filter(function (element) {
                        return element.ClockInLate == true;
                    });
                    $scope.count_work_last = $scope.check_work_late.length;

                    $scope.dataTimeSheet.TimeElements = response.data.TimeElements;

                    $scope.dataTimeSheet.IsVisibleTimeElement = response.data.IsVisibleTimeElement;
                    $scope.dataTimeSheet.IsVisibleTimePair = response.data.IsVisibleTimePair;


                    $scope.document.Additional.INFOTYPE2001[0].BeginDate = $filter('date')($scope.document.Additional.INFOTYPE2001[0].BeginDate, 'yyyy-MM-ddT00:00:00');
                    $scope.document.Additional.INFOTYPE2001[0].EndDate = $filter('date')($scope.document.Additional.INFOTYPE2001[0].EndDate, 'yyyy-MM-ddT00:00:00');

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };


            $scope.openModalEmployeeSubstitute = function () {

                var modalInstance = $uibModal.open({
                    animation: $scope.animationsEnabled,
                    templateUrl: 'modalListEmployeeDataSubstituteAbsence.ng-popup.html',
                    controller: 'modalListEmployeeDataSubstituteAbsenceController',
                    backdrop: 'static',
                    keyboard: false,
                    size: 'lg',
                    resolve: {
                        items: function () {
                            var data = {
                                Requestor: $scope.requesterData,
                                Text: $scope.Text,
                                fnShowEmployee: function (item) {
                                    $scope.selectEmployeeSubstitute(item);
                                }
                            };
                            return data;
                        }
                    }
                });
            };

            $scope.clearEmployeeSubstitute = function () {

                $scope.document.Additional.DELEGATEDATA[0].DelegateTo = "";
                $scope.employeeSubstitute.empId = "";
                $scope.employeeSubstitute.empName = "";
                $scope.employeeSubstitute.empPosition = "";
                $scope.employeeSubstitute.empOrganize = "";
                $scope.showTextSubStitute = false;

            };

            $scope.GetStatusClass = function (item) {
                //var dateNow = new Date();
                //var date = new Date(item.Date)
                //if (item.Type == 'OFF' || item.Type == 'HOL')
                //    return 'active';
                //if (dateNow > date && (item.ClockIn == null || item.ClockOut == null))
                //    return 'state-error';

                var dateNow = new Date();
                var date = new Date(item.Date);

                if (item.Type == 'OFF' || item.Type == 'HOL')
                    return 'active';

                var dataDatetimeNow = $filter('date')(dateNow, 'yyyy-MM-ddT00:00:00');
                var dataDateTimeTimeSheet = $filter('date')(date, 'yyyy-MM-ddT00:00:00');

                if (dataDatetimeNow == dataDateTimeTimeSheet) {
                    return '';
                }

                if (item.IsNoScanCard) {
                    return '';
                }


                if (dataDatetimeNow > dataDateTimeTimeSheet) {

                    if (!item.IsCorrectPolicy) {
                        if (item.ClockIn == null || item.ClockOut == null) {
                            return 'state-error';
                        }
                    }
                }

                return '';
            };

        }])
        .controller('ListEmployeeSubstituteController', ['items', '$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', '$uibModalInstance', function (items, $scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal, $uibModalInstance) {


            $scope.list_emp = [];
            console.log(items.employee);
            $scope.list_emp = items.employee;
            $scope.text_db = items.textDescription;

            if (items.employee.length > 0) {
                $scope.modelSelectCustomer = {
                    employeeId: items.employee[0].EmployeeID
                };
            }
            else {

                $scope.modelSelectCustomer = {
                    employeeId: 0
                };
            }

            $scope.selectEmployee = function (employeeId) {

                if ($scope.list_emp.length > 0) {

                    $scope.filterSelectEmployee = $scope.list_emp.filter(function (element) {
                        return element.EmployeeID == employeeId;
                    });

                    if ($scope.filterSelectEmployee.length > 0) {

                        var dataSelect = $scope.filterSelectEmployee[0];

                        items.fnShowEmployee(dataSelect);
                        $uibModalInstance.close();
                    }
                }



            };

            $scope.closeModel = function () {

                $uibModalInstance.close();
            };

        }])
        .controller('modalListEmployeeDataSubstituteAbsenceController', ['items', '$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', '$uibModalInstance', function (items, $scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal, $uibModalInstance) {

            $scope.text_db = items.Text;  // Text;
            $scope.requestorData = items.Requestor; // Requestor

            $scope.modelTextSearch = {
                TextData: ""
            };

            $scope.list_emp = [];
            $scope.searchEmployee = function (textSearch) {

                var URL = CONFIG.SERVER + 'HRTM/SearchEmployeeDataEmppower';

                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {
                        "SEARCHTEXT": textSearch
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.list_emp = response.data.Table;

                }, function errorCallback(response) {
                    console.log(response);
                });

            };

            $scope.selectEmployee = function (item) {

                items.fnShowEmployee(item);
                $uibModalInstance.close();
            };


            $scope.closeModel = function () {

                $uibModalInstance.close();
            };

        }]);
})();