﻿(function () {
    angular.module('ESSMobile')
        .controller('DutyPaymentMonthlyEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {


            $scope.ChildAction.SetData = function () {

            };

            $scope.ChildAction.LoadData = function () {

            };

            var employeeDate = getToken(CONFIG.USER);
            $scope.ChildAction.SetData();

      
            $scope.period_data = [];
            $scope.text_desciption = "";
            $scope.loadPeriod = function () {

                var URL = CONFIG.SERVER + 'HRTM/GetPeriodDutyPayment';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                //$scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.period_data = response.data;

                    $scope.valid_period = $scope.period_data.filter(function (element) {
                        return element.PeriodValue === $scope.document.Additional.PeriodDutyPayment[0].SelectPeriod; // ปรับเป็น config
                    });

                    if ($scope.valid_period.length > 0) {
                        $scope.text_desciption = $scope.valid_period[0].PeriodText;
                    }

                    //$scope.loader.enable = false;

                }, function errorCallback(response) {
                    //$scope.loader.enable = false;
                });
            };
            $scope.loadPeriod();

            $scope.list_duty_payment = {
                objDutyPaymentTable: [],
                objDictDailyWS: {},
                objListDutyTime: []
            };
            $scope.list_convert_duty_payment = [];
            $scope.loadCalendar = function () {

                var URL = CONFIG.SERVER + 'HRTM/GetListEditorDutyPayment';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                    , PeriodDutyPayment: $scope.document.Additional.PeriodDutyPayment[0].SelectPeriod
                };

                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.list_convert_duty_payment = [];
                    console.log(response);

                    $scope.list_duty_payment = response.data;
                    if (response.data.objDutyPaymentTable.length > 0) {

                        angular.forEach(response.data.objDutyPaymentTable, function (item) {

                            if (response.data.objDictDailyWS[item.__dutyDate]) {

                                var dailyWs = response.data.objDictDailyWS[item.__dutyDate];

                                var filterSelectDutyPayment = response.data.objListDutyTime.filter(function (element) {
                                    return element.__dayWork === !dailyWs.IsDayOffOrHoliday;
                                });

                                if (filterSelectDutyPayment.length > 0) {

                                    var convertObjDutyPaymentLog = {
                                        Ischeck: false,
                                        RequestNo: item.__requestNo,
                                        ItemNo: item.__itemNo,
                                        EmployeeID: item.__employeeID,
                                        DutyDate: item.__dutyDate,
                                        BeginDate: item.__beginDate,
                                        EndDate: item.__endDate,
                                        Status: item.__status,
                                        RefRequestNo: item.__refRequestNo,
                                        detaildailyWs: dailyWs,
                                        listSelect: filterSelectDutyPayment
                                    };

                                    // check 
                                    if ($scope.document.Additional.DutyPaymentLog.length > 0) {

                                        var check_select_data = $scope.document.Additional.DutyPaymentLog.filter(function (element) {
                                            return element.DutyDate === item.__dutyDate;
                                        });

                                        if (check_select_data.length > 0) {
                                            convertObjDutyPaymentLog.Ischeck = true;
                                        }
                                    }


                                    $scope.list_convert_duty_payment.push(convertObjDutyPaymentLog);
                                }
                            }
                        });

                    }

                    $scope.loader.enable = false;

                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });

            };

            $scope.selectTransaction = function () {

                $scope.document.Additional.DutyPaymentLog = [];
                $scope.document.Additional.InfoTable = [];

                if ($scope.list_convert_duty_payment.length > 0) {

                    $scope.list_select_duty_payment = $scope.list_convert_duty_payment.filter(function (element) {
                        return element.Ischeck === true;
                    });

                    if ($scope.list_select_duty_payment.length > 0) {

                        angular.forEach($scope.list_select_duty_payment, function (key) {

                            var beginTime = "00:00:00";
                            var endTime = "00:00:00";
                            if (key.listSelect.length > 0) {
                                beginTime = key.listSelect[0].__beginTime + ":00";
                                endTime = key.listSelect[0].__endTime + ":00";
                            }

                            var paymentLog = {
                                RequestNo: key.RequestNo,
                                ItemNo: key.ItemNo,
                                EmployeeID: key.EmployeeID,
                                DutyDate: key.DutyDate,
                                BeginDate: $filter('date')(key.BeginDate, 'yyyy-MM-ddT' + beginTime),
                                EndDate: $filter('date')(key.EndDate, 'yyyy-MM-ddT' + endTime),
                                Status: "",
                                RefRequestNo: ""
                            };

                            $scope.document.Additional.DutyPaymentLog.push(paymentLog);

                        });

                        var infoTableMock = {
                            TOTALAMOUNT: $scope.document.Additional.DutyPaymentLog.length,
                            DUTYDATE: $scope.document.Additional.DutyPaymentLog[0].DutyDate,
                            REQUESTORLEVEL: $scope.document.Requestor.EmpSubGroup
                        };
                        $scope.document.Additional.InfoTable.push(infoTableMock);
                    }

                }
            };

            if ($routeParams.otherParam) {

                var objSelectPeriod = {
                    SelectPeriod: $routeParams.otherParam
                };

                $scope.document.Additional.PeriodDutyPayment.push(objSelectPeriod);
                $scope.loadCalendar();

            }
            else {

                // view => editor อีกครั้ง
                $scope.loadCalendar();

            }



            //$scope.mockData = function () {

            //    var paymentLog1 = {
            //        RequestNo: "",
            //        ItemNo: "9a46670-8684-4f4b-99f1-d9e4e7102e2b",
            //        EmployeeID: "23560073",
            //        DutyDate: "2020-02-01T00:00:00",
            //        BeginDate: "2020-02-01T08:00:00",
            //        EndDate: "2020-02-02T08:00:00",
            //        Status: "",
            //        RefRequestNo: ""
            //    };

            //    var paymentLog2 = {
            //        RequestNo: "",
            //        ItemNo: "6763ef16-cbae-4baa-9e96-49846199c2a9",
            //        EmployeeID: "23560073",
            //        DutyDate: "2020-02-02T00:00:00",
            //        BeginDate: "2020-02-02T08:00:00",
            //        EndDate: "2020-02-03T08:00:00",
            //        Status: "",
            //        RefRequestNo: ""
            //    };

            //    var infoTableMock = {
            //        TOTALAMOUNT: 2,
            //        DUTYDATE: "2020-02-01T00:00:00",
            //        REQUESTORLEVEL: $scope.document.Requestor.EmpSubGroup
            //    };

            //    console.log(infoTableMock);

            //    $scope.document.Additional.DutyPaymentLog.push(paymentLog1);
            //    $scope.document.Additional.DutyPaymentLog.push(paymentLog2);
            //    $scope.document.Additional.InfoTable.push(infoTableMock);
            //};
            //$scope.mockData();

        }]);
})();