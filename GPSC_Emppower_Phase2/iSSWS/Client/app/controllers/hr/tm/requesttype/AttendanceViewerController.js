﻿(function () {
    angular.module('ESSMobile')
        .controller('AttendanceViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {

            console.log($scope.document);

            $scope.oRequestParameter = {
                InputParameter: {}
                , CurrentEmployee: $scope.employeeData
                , Requestor: $scope.requesterData
                , Creator: getToken(CONFIG.USER)
                , Language: $scope.employeeData.Language
            };
            $scope.attType = "";
            $scope.ChildAction.LoadData = function () {

            };
            $scope.EnabledVacation = true;
            $scope.EnabledLetterAdminPrint = true;

            $scope.ChildAction.SetData = function () {
                $scope.INFOTYPE2002 = $scope.document.Additional.INFOTYPE2002;
            };

            $scope.ChildAction.SetData();

            $scope.GetAttendanceTypeList = function () {
                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/GetAttendanceTypeList',
                    data: $scope.oRequestParameter
                }).then(function successCallback(response) {
                    $scope.attendanceTypes = response.data;

                    //$scope.attTypes = $scope.attendanceTypes.filter(function (item) {
                    //    return item.Key === $scope.INFOTYPE2002[0].AttendanceType;
                    //});
                    //$scope.attType = $scope.attTypes[0].Description;

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }
            $scope.GetAttendanceTypeList();

            $scope.getTypeName = function (typeId) {
                if ($scope.attendanceTypes) {
                    $scope.attTypes = $scope.attendanceTypes.filter(function (item) {
                        return item.Key === typeId;
                    });
                    return $scope.attTypes[0].Description;
                }
                return "";
            }
            

        }]);
})();