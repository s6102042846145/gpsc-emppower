﻿(function () {
    angular.module('ESSMobile')
        .controller('TimesheetManagementViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {

            //console.log($scope.document);

            $scope.employeeData = getToken(CONFIG.USER);
            $scope.selectedPeriodTimesheetMatching = $routeParams.otherParam;
            $scope.oRequestParameter = {
                InputParameter: {}
                , CurrentEmployee: $scope.employeeData
                , Requestor: $scope.requesterData
                , Creator: getToken(CONFIG.USER)
                , Language: $scope.employeeData.Language
            };
            

            $scope.GetPeriodTimesheet = function () {
                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/GetPeriodTimesheet',
                    data: $scope.oRequestParameter
                }).then(function successCallback(response) {
                    $scope.periodTimesheets = response.data;

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetPeriodTimesheet();


            $scope.GetStatusClass = function (item, index) {
                var dateNow = new Date();
                var date = new Date(item.Date);

                var masterTimepair = $scope.document.Additional.MasterTimePair[index];


                if ((masterTimepair.ClockIN == null && item.ClockIN != null) || (masterTimepair.ClockOUT == null && item.ClockOUT != null)) {
                    return 'edit';
                }
                else if ((masterTimepair.ClockIN != null && item.ClockIN == null) || (masterTimepair.ClockOUT != null && item.ClockOUT == null)) {
                    return 'edit';
                }
                else if (masterTimepair.ClockIN != null && item.ClockIN != null) {

                    if (!isEquivalent(masterTimepair.ClockIN, item.ClockIN)) {
                        return 'edit';
                    }

                    // เพิ่มเติมดักเงื่อนไขเวลาออกด้วย 
                    if (masterTimepair.ClockOUT != null && item.ClockOUT != null) {
                        if (!isEquivalent(masterTimepair.ClockOUT, item.ClockOUT)) {
                            return 'edit';
                        }
                    }
                }
                else if (masterTimepair.ClockOUT != null && item.ClockOUT != null) {
                    if (!isEquivalent(masterTimepair.ClockOUT, item.ClockOUT)) {
                        return 'edit';
                    }
                }

                if (item.DWSCode == 'OFF' || item.DWSCode == 'HOL')
                    return 'active';

                if (item.IsNoScan) {
                    return '';
                }


                if (dateNow > date && (item.ClockIN == null || item.ClockOUT == null)) {
                    return 'state-error';
                }
            };

            function isEquivalent(a, b) {
                // Create arrays of property names
                var aProps = Object.getOwnPropertyNames(a);
                var bProps = Object.getOwnPropertyNames(b);

                // If number of properties is different,
                // objects are not equivalent
                if (aProps.length != bProps.length) {
                    return false;
                }

                for (var i = 0; i < aProps.length; i++) {
                    var propName = aProps[i];

                    // If values of same property are not equal,
                    // objects are not equivalent
                    if (a[propName] !== b[propName]) {
                        return false;
                    }
                }

                // If we made it this far, objects
                // are considered equivalent
                return true;
            }

            $scope.ChildAction.SetData = function () {
                //GetCalendar();

            };

            $scope.ChildAction.LoadData = function () {

            };

            $scope.ChildAction.SetData();
            
        }]);
})();