﻿(function () {
    angular.module('ESSMobile')
        .controller('MonthlyOTEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', '$timeout', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal, $timeout) {

            if ($routeParams.otherParam) {
                $scope.document.Additional.DailyOTLog[0].employeeId = $routeParams.otherParam.split('|')[0];
                $scope.document.Additional.DailyOTLog[0].period = $routeParams.otherParam.split('|')[1];
            }


            $scope.employeeData = getToken(CONFIG.USER);

            $scope.oRequestParameter = {
                InputParameter: {}
                , CurrentEmployee: $scope.employeeData
                , Requestor: $scope.requesterData
                , Creator: getToken(CONFIG.USER)
                , Language: $scope.employeeData.Language
            };

            $scope.minBeginDate = $scope.document.Additional.DailyOTLog[0].BeginDate;
            $scope.maxBeginDate = $scope.document.Additional.DailyOTLog[$scope.document.Additional.DailyOTLog.length - 1].BeginDate;
            $scope.GetTimePairOTSummary = function () {
                $scope.loader.enable = true;

                var oRequestParameter = {
                    InputParameter: {
                        "EmployeeID": $scope.document.Additional.DailyOTLog[0].employeeId,
                        "BeginDate": $filter('date')($scope.minBeginDate, 'yyyy-MM-ddT00:00:00'),
                        "EndDate": $filter('date')($scope.maxBeginDate, 'yyyy-MM-ddT00:00:00')
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/GetTimePairOTSummary',
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    if (response.data) {
                        $scope.objTimePairOTSummary = response.data;
                        if ($scope.objTimePairOTSummary.length > 0) {
                            angular.forEach($scope.document.Additional.DailyOTLog, function (item) {

                                if (item.DefaultIsPayroll != '1') {                                    
                                    item.IsPayroll = true;
                                    item.DefaultIsPayroll = '1';
                                }
                                
                                var result = getWithAttr($scope.objTimePairOTSummary, 'Date', $filter('date')(item.BeginDate, 'yyyy-MM-ddT00:00:00'));
                                
                                if (result.length > 0) {
                                    item.Type = result[0].Type;
                                    if (result[0].Type == 'OFF') {
                                        item.ClockIn = result[0].ClockIn;
                                        item.ClockOut = result[0].ClockOut;
                                    }
                                    else {
                                        if (result[0].CountAbsAtt > 0 && result[0].IsAllDayFlag) {
                                            item.ClockIn = result[0].WorkBegin;
                                            item.ClockOut = result[0].WorkEnd;
                                        }
                                        else if (result[0].CountAbsAtt > 0 && !result[0].IsAllDayFlag) {
                                            if (result[0].ClockIn != null && result[0].ClockIn <= result[0].AbsAttBegin) {
                                                item.ClockIn = result[0].ClockIn;
                                            }
                                            else if (result[0].ClockIn != null && result[0].ClockIn > result[0].AbsAttBegin) {
                                                item.ClockIn = result[0].AbsAttBegin;
                                                item.ColorIn = 'Blue';
                                            }

                                            if (result[0].ClockOut != null && result[0].ClockOut < result[0].AbsAttEnd) {
                                                item.ClockOut = result[0].AbsAttEnd;
                                                item.ColorOut = 'Blue';
                                            }
                                            else if (result[0].ClockOut != null && result[0].ClockOut >= result[0].AbsAttEnd) {
                                                item.ClockOut = result[0].ClockOut;
                                            }
                                        }
                                    }

                                }
                            });
                            $scope.getCalClockInClockOut();
                        }
                    }
                    //$scope.loader.enable = false;
                    
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetTimePairOTSummary();


            $scope.getCalClockInClockOut = function () {

                $scope.oRequestParameter.EmployeeID = $scope.document.Additional.DailyOTLog[0].employeeId;
                $scope.oRequestParameter.SelectedPeriod = $scope.document.Additional.DailyOTLog[0].period;
                $scope.oRequestParameter.OTs = [];

                angular.forEach($scope.document.Additional.DailyOTLog, function (item) {
                    var temp = {
                        BeginDate: item.BeginDate,
                        EndDate: item.EndDate,
                        ClockIn: item.ClockIn,
                        ClockOut: item.ClockOut
                    }
                    $scope.oRequestParameter.OTs.push(temp);
                });

                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/CalClockInClockOutSummaryOT',
                    data: $scope.oRequestParameter
                }).then(function successCallback(response) {
                    if (response.data) {
                        $scope.objCalClockInClockOut = response.data;

                        var i = 0;
                        var chkFinalOTNoValue = false;
                        angular.forEach($scope.objCalClockInClockOut, function (item) {


                            chkFinalOTNoValue = false;
                            if ((typeof $scope.document.Additional.DailyOTLog[i]._FinalOTHour10 === 'undefined' || $scope.document.Additional.DailyOTLog[i]._FinalOTHour10 === '') &&
                                (typeof $scope.document.Additional.DailyOTLog[i]._FinalOTHour15 === 'undefined' || $scope.document.Additional.DailyOTLog[i]._FinalOTHour15 === '') &&
                                (typeof $scope.document.Additional.DailyOTLog[i]._FinalOTHour30 === 'undefined' || $scope.document.Additional.DailyOTLog[i]._FinalOTHour30 === ''))
                            {
                                chkFinalOTNoValue = true;
                            }


                            if (item._RequestOTHour30 != 0) {
                                $scope.document.Additional.DailyOTLog[i].EvaOTHour30 = item._RequestOTHour30;

                                if (chkFinalOTNoValue) {
                                    item._RequestOTHour30 = item._RequestOTHour30.toFixed(0);
                                    $scope.document.Additional.DailyOTLog[i]._FinalOTHour30 = item._RequestOTHour30 / 60;
                                    $scope.document.Additional.DailyOTLog[i].FinalOTHour30 = item._RequestOTHour30;
                                }
                            }

                            if (item._RequestOTHour15 != 0) {
                                $scope.document.Additional.DailyOTLog[i].EvaOTHour15 = item._RequestOTHour15;

                                if (chkFinalOTNoValue) {
                                    $scope.document.Additional.DailyOTLog[i]._FinalOTHour15 = item._RequestOTHour15 / 60;
                                    $scope.document.Additional.DailyOTLog[i].FinalOTHour15 = item._RequestOTHour15;
                                }
                            }

                            if (item._RequestOTHour10 != 0) {
                                $scope.document.Additional.DailyOTLog[i].EvaOTHour10 = item._RequestOTHour10;

                                if (chkFinalOTNoValue) {
                                    $scope.document.Additional.DailyOTLog[i]._FinalOTHour10 = item._RequestOTHour10 / 60;
                                    $scope.document.Additional.DailyOTLog[i].FinalOTHour10 = item._RequestOTHour10;
                                }
                            }


                            //if (item._RequestOTHour30 != 0) {
                            //    $scope.document.Additional.DailyOTLog[i].EvaOTHour30 = item._RequestOTHour30;

                            //    if ((typeof $scope.document.Additional.DailyOTLog[i]._FinalOTHour10 === 'undefined' || $scope.document.Additional.DailyOTLog[i]._FinalOTHour10 === '') &&
                            //        (typeof $scope.document.Additional.DailyOTLog[i]._FinalOTHour15 === 'undefined' || $scope.document.Additional.DailyOTLog[i]._FinalOTHour15 === '') &&
                            //        (typeof $scope.document.Additional.DailyOTLog[i]._FinalOTHour30 === 'undefined' || $scope.document.Additional.DailyOTLog[i]._FinalOTHour30 === ''))
                            //    {
                            //        $scope.document.Additional.DailyOTLog[i]._FinalOTHour30 = item._RequestOTHour30 / 60;
                            //        $scope.document.Additional.DailyOTLog[i].FinalOTHour30 = item._RequestOTHour30;
                            //    }
                            //}

                            //if (item._RequestOTHour15 != 0) {
                            //    $scope.document.Additional.DailyOTLog[i].EvaOTHour15 = item._RequestOTHour15;

                            //    if ((typeof $scope.document.Additional.DailyOTLog[i]._FinalOTHour10 === 'undefined' || $scope.document.Additional.DailyOTLog[i]._FinalOTHour10 === '') &&
                            //        (typeof $scope.document.Additional.DailyOTLog[i]._FinalOTHour15 === 'undefined' || $scope.document.Additional.DailyOTLog[i]._FinalOTHour15 === '') &&
                            //        (typeof $scope.document.Additional.DailyOTLog[i]._FinalOTHour30 === 'undefined' || $scope.document.Additional.DailyOTLog[i]._FinalOTHour30 === ''))
                            //    {
                            //        $scope.document.Additional.DailyOTLog[i]._FinalOTHour15 = item._RequestOTHour15 / 60;
                            //        $scope.document.Additional.DailyOTLog[i].FinalOTHour15 = item._RequestOTHour15;
                            //    }
                            //}

                            //if (item._RequestOTHour10 != 0) {
                            //    $scope.document.Additional.DailyOTLog[i].EvaOTHour10 = item._RequestOTHour10;

                            //    if ((typeof $scope.document.Additional.DailyOTLog[i]._FinalOTHour10 === 'undefined' || $scope.document.Additional.DailyOTLog[i]._FinalOTHour10 === '') &&
                            //        (typeof $scope.document.Additional.DailyOTLog[i]._FinalOTHour15 === 'undefined' || $scope.document.Additional.DailyOTLog[i]._FinalOTHour15 === '') &&
                            //        (typeof $scope.document.Additional.DailyOTLog[i]._FinalOTHour30 === 'undefined' || $scope.document.Additional.DailyOTLog[i]._FinalOTHour30 === ''))
                            //    {
                            //        $scope.document.Additional.DailyOTLog[i]._FinalOTHour10 = item._RequestOTHour10 / 60;
                            //        $scope.document.Additional.DailyOTLog[i].FinalOTHour10 = item._RequestOTHour10;
                            //    }
                            //}      

                            if ((typeof $scope.document.Additional.DailyOTLog[i]._FinalOTHour10 === 'undefined' || $scope.document.Additional.DailyOTLog[i]._FinalOTHour10 === '') &&
                                (typeof $scope.document.Additional.DailyOTLog[i]._FinalOTHour15 === 'undefined' || $scope.document.Additional.DailyOTLog[i]._FinalOTHour15 === '') &&
                                (typeof $scope.document.Additional.DailyOTLog[i]._FinalOTHour30 === 'undefined' || $scope.document.Additional.DailyOTLog[i]._FinalOTHour30 === '')) 
                            {
                                if ($scope.document.Additional.DailyOTLog[i].RequestOTHour30 != 0) {
                                    $scope.document.Additional.DailyOTLog[i].RequestOTHour30 = $scope.document.Additional.DailyOTLog[i].RequestOTHour30.toFixed(0);
                                    $scope.document.Additional.DailyOTLog[i]._FinalOTHour30 = $scope.document.Additional.DailyOTLog[i].RequestOTHour30 / 60;
                                    $scope.document.Additional.DailyOTLog[i].FinalOTHour30 = $scope.document.Additional.DailyOTLog[i].RequestOTHour30;
                                }

                                if ($scope.document.Additional.DailyOTLog[i].RequestOTHour15 != 0) {
                                    $scope.document.Additional.DailyOTLog[i]._FinalOTHour15 = $scope.document.Additional.DailyOTLog[i].RequestOTHour15 / 60;
                                    $scope.document.Additional.DailyOTLog[i].FinalOTHour15 = $scope.document.Additional.DailyOTLog[i].RequestOTHour15;
                                }

                                if ($scope.document.Additional.DailyOTLog[i].RequestOTHour10 != 0) {
                                    $scope.document.Additional.DailyOTLog[i]._FinalOTHour10 = $scope.document.Additional.DailyOTLog[i].RequestOTHour10 / 60;
                                    $scope.document.Additional.DailyOTLog[i].FinalOTHour10 = $scope.document.Additional.DailyOTLog[i].RequestOTHour10;
                                }
                            }

                            i++;
                        });
                    }
                    $scope.loader.enable = false;

                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };


            function getWithAttr(array, attr, value) {
                var objReturn = [];
                for (var i = 0; i < array.length; i += 1) {
                    if (array[i][attr] === value) {
                        objReturn.push(array[i]);
                        break;
                    }
                }
                return objReturn;
            }

            $scope.GetOTWorkType = function () {
                //$scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/GetOTWorkType',
                    data: $scope.oRequestParameter
                }).then(function successCallback(response) {
                    $scope.oTWorkTypes = response.data;
                    //$scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetOTWorkType();

            $scope.getTypeName = function (typeId) {
                if ($scope.oTWorkTypes) {
                    $scope.attTypes = $scope.oTWorkTypes.filter(function (item) {
                        return item.__otWorkTypeID === typeId;
                    });
                    return $scope.attTypes[0].__otWorkTypeDesc;
                }
                return "";
            };

            $scope.calH = function (item) {
                if (item) {
                    return (item / 60).toFixed(2);
                }
                return 0;
            };

            $scope.sumRequestOTHour = function () {
                var temp = 0;
                angular.forEach($scope.document.Additional.DailyOTLog, function (item) {
                    if (item.IsPayroll) {
                        temp += parseFloat(item.RequestOTHour10);
                        temp += parseFloat(item.RequestOTHour15);
                        temp += parseFloat(item.RequestOTHour30);
                    }
                });
                return (temp / 60).toFixed(2);
            };
            $scope.sumEvaOTHour = function () {
                var temp = 0;
                angular.forEach($scope.document.Additional.DailyOTLog, function (item) {
                    temp += item.EvaOTHour10;
                    temp += item.EvaOTHour15;
                    temp += item.EvaOTHour30;
                });
                return (temp / 60).toFixed(2);
            };
            $scope.sumFinalOTHour = function () {
                var temp = 0;
                angular.forEach($scope.document.Additional.DailyOTLog, function (item) {
                    if (item.IsPayroll) {
                        temp += parseFloat(item.FinalOTHour10);
                        temp += parseFloat(item.FinalOTHour15);
                        temp += parseFloat(item.FinalOTHour30);
                    }
                });
                return (temp / 60).toFixed(2);
            };

            $scope.sumRequestOTHour2 = function () {
                var temp = 0;
                angular.forEach($scope.document.Additional.AllDailyOTLog, function (item) {

                    if (item.IsPayroll && item.Status == 3) {

                        temp += parseFloat(item.RequestOTHour10);
                        temp += parseFloat(item.RequestOTHour15);
                        temp += parseFloat(item.RequestOTHour30);
                    }


                });
                return (temp / 60).toFixed(2);
            };
            $scope.sumEvaOTHour2 = function () {
                var temp = 0;
                angular.forEach($scope.document.Additional.AllDailyOTLog, function (item) {

                    if (item.IsPayroll && item.Status == 3) {
                        temp += item.EvaOTHour10;
                        temp += item.EvaOTHour15;
                        temp += item.EvaOTHour30;
                    }
                });
                return (temp / 60).toFixed(2);
            };
            $scope.sumFinalOTHour2 = function () {
                var temp = 0;
                angular.forEach($scope.document.Additional.AllDailyOTLog, function (item) {
                    if (item.IsPayroll && item.Status == 3) {
                        temp += parseFloat(item.FinalOTHour10);
                        temp += parseFloat(item.FinalOTHour15);
                        temp += parseFloat(item.FinalOTHour30);
                    }
                });
                return (temp / 60).toFixed(2);
            };

            $scope.setFinalOTHour10 = function (item) {
           
                if (typeof item._FinalOTHour10 === 'undefined') {
                    item.FinalOTHour10 = 0;
                    item._FinalOTHour10 = 0;
                }
                else {
                    item.FinalOTHour10 = (item._FinalOTHour10 * 60).toFixed(2);
                    item.FinalOTHour10 = parseFloat(item.FinalOTHour10);
                }

                if (item._FinalOTHour10 == null) {
                    delete item._FinalOTHour10;
                }
            };
            $scope.setFinalOTHour15 = function (item) {

                if (typeof item._FinalOTHour15 === 'undefined') {
                    item.FinalOTHour15 = 0;
                    item._FinalOTHour15 = 0;
                }
                else {
                    item.FinalOTHour15 = (item._FinalOTHour15 * 60).toFixed(2);
                    item.FinalOTHour15 = parseFloat(item.FinalOTHour15);
                }

                if (item._FinalOTHour15 == null) {
                    delete item._FinalOTHour15;
                }
            };
            $scope.setFinalOTHour30 = function (item) {

                if (typeof item._FinalOTHour30 === 'undefined') {
                    item.FinalOTHour30 = 0;
                    item._FinalOTHour30 = 0;
                }
                else {
                    item.FinalOTHour30 = (item._FinalOTHour30 * 60).toFixed(2);
                    item.FinalOTHour30 = parseFloat(item.FinalOTHour30);
                }

                if (item._FinalOTHour30 == null) {
                    delete item._FinalOTHour30;
                }
            };

            angular.forEach($scope.document.Additional.DailyOTLog, function (item) {
                if (item.OTClockINOUT != null && item.OTClockINOUT.length == 0) {
                    item.OTClockINOUT = null;
                }
            });
            angular.forEach($scope.document.Additional.AllDailyOTLog, function (item) {
                if (item.OTClockINOUT != null && item.OTClockINOUT.length == 0) {
                    item.OTClockINOUT = null;
                }
            });

            $scope.employee = "";
            $scope.GetEmployeeByEmpID = function () {
                //$scope.loader.enable = true;
                $scope.oRequestParameter.EmployeeID = $scope.document.Additional.DailyOTLog[0].employeeId;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/GetEmployeeByEmpID',
                    data: $scope.oRequestParameter
                }).then(function successCallback(response) {
                    if (response.data) {
                        $scope.employee = response.data[0].EmployeeID + " " + response.data[0].EmployeeName;
                    }

                    //$scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetEmployeeByEmpID();

            $scope.selectedPeriodSummaryOT = "";
            $scope.GetPeriodSummaryOTList = function () {
                //$scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/GetPeriodSummaryOTList',
                    data: $scope.oRequestParameter
                }).then(function successCallback(response) {
                    $scope.periodSummaryOTs = response.data;
                    if ($scope.periodSummaryOTs) {
                        $scope.period = $scope.periodSummaryOTs[$scope.periodSummaryOTs.length - 1].PeriodValue;
                    }
                    //$scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetPeriodSummaryOTList();

            $scope.getPeriodName = function (periodId) {
                if ($scope.periodSummaryOTs && periodId) {
                    $scope.periodTypes = $scope.periodSummaryOTs.filter(function (item) {
                        return item.PeriodValue === periodId;
                    });
                    return $scope.periodTypes[0].PeriodText;
                }
                return "";
            };


            $scope.refreshData = function () {
                $scope.oRequestParameter.EmployeeID = $scope.document.Additional.DailyOTLog[0].employeeId;
                $scope.oRequestParameter.Period = $scope.document.Additional.DailyOTLog[0].period;
                //$scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/GetListSummaryOT',
                    data: $scope.oRequestParameter
                }).then(function successCallback(response) {
                    $scope.model = response.data;
                    var employee = angular.copy($scope.document.Additional.DailyOTLog[0].employeeId);
                    var period = angular.copy($scope.document.Additional.DailyOTLog[0].period);
                    $scope.document.Additional.DailyOTLog = $scope.model.DailyOTLog;
                    $scope.document.Additional.AllDailyOTLog = $scope.model.AllDailyOTLog;
                    $scope.document.Additional.DailyOTLog[0].employeeId = employee;
                    $scope.document.Additional.DailyOTLog[0].period = period;
                    angular.forEach($scope.document.Additional.DailyOTLog, function (item) {
                        if (item.OTClockINOUT != null && item.OTClockINOUT.length == 0) {
                            item.OTClockINOUT = null;
                        }
                    });
                    angular.forEach($scope.document.Additional.AllDailyOTLog, function (item) {
                        if (item.OTClockINOUT != null && item.OTClockINOUT.length == 0) {
                            item.OTClockINOUT = null;
                        }
                    });
                    //$scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };


            $scope.isCheckAllPayroll = false;
            $scope.checkAllPayroll = function (status) {
                if ($scope.document.Additional.DailyOTLog.length > 0) {

                    angular.forEach($scope.document.Additional.DailyOTLog, function (value, key) {
                        if (status)
                            value.IsPayroll = true;
                        else
                            value.IsPayroll = false;
                    });
                }
            };


            
            $scope.GetTimePairAllDailyOTLog = function () {
              
                $scope.loader.enable = true;

                var AllDailyOTLogStatus3 = getAllDailyOTLogWithStatus3($scope.document.Additional.AllDailyOTLog, 'Status', 3);

                if (AllDailyOTLogStatus3.length > 0) {
                    $scope.minDate = AllDailyOTLogStatus3[0].BeginDate;
                    $scope.maxDate = AllDailyOTLogStatus3[AllDailyOTLogStatus3.length - 1].BeginDate;

                    var oRequestParameter = {
                        InputParameter: {
                            "EmployeeID": AllDailyOTLogStatus3[0].EmployeeID,
                            "BeginDate": $filter('date')($scope.minDate, 'yyyy-MM-ddT00:00:00'),
                            "EndDate": $filter('date')($scope.maxDate, 'yyyy-MM-ddT00:00:00')
                        }
                        , CurrentEmployee: $scope.employeeData
                        , Requestor: $scope.requesterData
                        , Creator: getToken(CONFIG.USER)
                    };

                    $http({
                        method: 'POST',
                        url: CONFIG.SERVER + 'HRTM/GetTimePairAllDailyOTLog',
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        if (response.data) {
                            $scope.objTimePairAllDailyOTLog = response.data;
                            if ($scope.objTimePairAllDailyOTLog.length > 0) {
                                angular.forEach($scope.document.Additional.AllDailyOTLog, function (item) {

                                    if (item.Status == 3) {
                                        var result = getWithAttr($scope.objTimePairAllDailyOTLog, 'Date', $filter('date')(item.BeginDate, 'yyyy-MM-ddT00:00:00'));

                                        if (result.length > 0) {
                                            item.Type = result[0].Type;
                                            if (result[0].Type == 'OFF') {
                                                item.ClockIn = result[0].ClockIn;
                                                item.ClockOut = result[0].ClockOut;
                                            }
                                            else {
                                                if (result[0].CountAbsAtt > 0 && result[0].IsAllDayFlag) {
                                                    item.ClockIn = result[0].WorkBegin;
                                                    item.ClockOut = result[0].WorkEnd;
                                                }
                                                else if (result[0].CountAbsAtt > 0 && !result[0].IsAllDayFlag) {
                                                    if (result[0].ClockIn != null && result[0].ClockIn <= result[0].AbsAttBegin) {
                                                        item.ClockIn = result[0].ClockIn;
                                                    }
                                                    else if (result[0].ClockIn != null && result[0].ClockIn > result[0].AbsAttBegin) {
                                                        item.ClockIn = result[0].AbsAttBegin;
                                                        item.ColorIn = 'Blue';
                                                    }

                                                    if (result[0].ClockOut != null && result[0].ClockOut < result[0].AbsAttEnd) {
                                                        item.ClockOut = result[0].AbsAttEnd;
                                                        item.ColorOut = 'Blue';
                                                    }
                                                    else if (result[0].ClockOut != null && result[0].ClockOut >= result[0].AbsAttEnd) {
                                                        item.ClockOut = result[0].ClockOut;
                                                    }
                                                }
                                            }

                                        }
                                    }
                                });
                            }
                        }
                        $scope.loader.enable = false;

                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                    });
                }
            };
            $scope.GetTimePairAllDailyOTLog();


            function getAllDailyOTLogWithStatus3(array, attr, value) {
                var objReturn = [];
                for (var i = 0; i < array.length; i += 1) {
                    if (array[i][attr] === value) {
                        objReturn.push(array[i]);
                    }
                }
                return objReturn;
            }


            $scope.ChildAction.SetData = function () {
                //GetCalendar();

            };

            $scope.ChildAction.LoadData = function () {

            };

            $scope.ChildAction.SetData();

        }]);

})();