﻿(function () {
    angular.module('ESSMobile')
        .controller('DailyOTViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {

            $scope.TextMonth = $scope.Text["SYSTEM"];
            $scope.employeeData = getToken(CONFIG.USER);

            $scope.oRequestParameter = {
                InputParameter: {}
                , CurrentEmployee: $scope.employeeData
                , Requestor: $scope.requesterData
                , Creator: getToken(CONFIG.USER)
                , Language: $scope.employeeData.Language
            };

            $scope.calH = function (item) {
                if (item) {
                    return (item / 60).toFixed(1);
                }
                return 0;
            };

            //$scope.GetManagerOTCreate = function () {
            //    $scope.loader.enable = true;
            //    $http({
            //        method: 'POST',
            //        url: CONFIG.SERVER + 'HRTM/GetManagerOTCreate',
            //        data: $scope.oRequestParameter
            //    }).then(function successCallback(response) {
            //        $scope.managerOTCreates = response.data;
            //        $scope.loader.enable = false;
            //    }, function errorCallback(response) {
            //        $scope.loader.enable = false;
            //    });
            //}
            //$scope.GetManagerOTCreate();
            //$scope.getManagerName = function (managerId) {
            //    //if ($scope.managerOTCreates) {
            //    //    $scope.attTypes = $scope.managerOTCreates.filter(function (item) {
            //    //        return item.__otWorkTypeID === managerId;
            //    //    });
            //    //    return $scope.attTypes[0].__otWorkTypeDesc;
            //    //}
            //    //return "";
            //}

            $scope.periodText = "";
            $scope.GetPeriodOTCreate = function () {
                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/GetPeriodOTCreate',
                    data: $scope.oRequestParameter
                }).then(function successCallback(response) {
                    $scope.periodOTCreates = response.data;
                    if ($scope.periodOTCreates) {
                        if ($scope.periodOTCreates && $scope.document.Additional.InfoTable[0].Period) {
                            $scope.periods = $scope.periodOTCreates.filter(function (item) {
                                return item.PeriodValue === $scope.document.Additional.InfoTable[0].Period;
                            });
                            //$scope.periodText = $scope.periods[0].PeriodText;
                            if ($scope.periods[0]) {
                                $scope.periodText = $scope.periods[0].PeriodText;
                            }
                            else {
                                var text_month_detail = "";
                                var str_month = $scope.document.Additional.InfoTable[0].Period.toString().substring(4, 6);
                                var str_year = $scope.document.Additional.InfoTable[0].Period.toString().substring(0, 4);
                                var int_month = parseInt(str_month);

                                text_month_detail = $scope.TextMonth["D_M" + int_month.toString()]
                                $scope.periodText = text_month_detail + " " + str_year;
                            }
                        }
                    }
                    //$scope.loader.enable = false;
                }, function errorCallback(response) {
                    //$scope.loader.enable = false;
                });
            };
            $scope.GetPeriodOTCreate();

            $scope.CalculateOT = function () {

                $scope.document.ErrorText = "";
                $scope.document.HasError = false;
                $scope.content.isShowError = false;

                $scope.oRequestParameter.SelectedPeriod = $scope.document.Additional.InfoTable[0].Period;
                $scope.oRequestParameter.OTs = [];

                angular.forEach($scope.document.Additional.InfoTable, function (item) {
                    var temp = {
                        Description: item.description,
                        OTDATE: item.OTDATE,
                        BegindTimeH: item.selectedBeginH,
                        BegindTimeM: item.selectedBeginM,
                        EndTimeH: item.selectedEndH,
                        EndTimeM: item.selectedEndM
                    };
                    $scope.oRequestParameter.OTs.push(temp);
                });

                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/CalculateOT',
                    data: $scope.oRequestParameter
                }).then(function successCallback(response) {
                    $scope.oTLogs = response.data;

                    var i = 0;
                    angular.forEach($scope.oTLogs, function (item) {
                        $scope.document.Additional.InfoTable[i].TOTALAMOUNT = item._TOTALAMOUNT;
                        $scope.document.Additional.DailyOTLog[i].OTimeEval = item.OTimeEval;
                        $scope.document.Additional.DailyOTLog[i].ClockTimePairs = item._ClockTimePairs;
                        $scope.document.Additional.DailyOTLog[i].FinalTimePairs = item._FinalTimePairs;
                        $scope.document.Additional.DailyOTLog[i].EvaTimePairs = item._EvaTimePairs;
                        $scope.document.Additional.DailyOTLog[i].Remark = item._Remark;
                        if (item._OTClockINOUT.length > 0) {
                            $scope.document.Additional.DailyOTLog[i].OTClockINOUT = item._OTClockINOUT;
                        }

                        if (item._OTimeEval != null && item._OTimeEval.length == 0) {
                            $scope.document.Additional.DailyOTLog[i].oTimeEval = null;
                        } else {
                            $scope.document.Additional.DailyOTLog[i].oTimeEval = item._OTimeEval;
                        }

                        $scope.document.Additional.DailyOTLog[i].RefRequestNo = item._RefRequestNo;
                        $scope.document.Additional.DailyOTLog[i].Description = item._Description;
                        $scope.document.Additional.DailyOTLog[i].RequestNo = item._RequestNo;
                        $scope.document.Additional.DailyOTLog[i].IsPayroll = item._IsPayroll;
                        $scope.document.Additional.DailyOTLog[i].FinalOTHour30 = item._FinalOTHour30;
                        $scope.document.Additional.DailyOTLog[i].FinalOTHour15 = item._FinalOTHour15;
                        $scope.document.Additional.DailyOTLog[i].FinalOTHour10 = item._FinalOTHour10;
                        $scope.document.Additional.DailyOTLog[i].EvaOTHour30 = item._EvaOTHour30;
                        $scope.document.Additional.DailyOTLog[i].EvaOTHour15 = item._EvaOTHour15;
                        $scope.document.Additional.DailyOTLog[i].EvaOTHour10 = item._EvaOTHour10;
                        $scope.document.Additional.DailyOTLog[i].Status = item._Status;
                        $scope.document.Additional.DailyOTLog[i].OTWorkTypeID = $scope.document.Additional.InfoTable[i].OTTYPE;
                        $scope.document.Additional.DailyOTLog[i].RequestOTHour30 = item._RequestOTHour30;
                        $scope.document.Additional.DailyOTLog[i].RequestOTHour15 = item._RequestOTHour15;
                        $scope.document.Additional.DailyOTLog[i].RequestOTHour10 = item._RequestOTHour10;
                        $scope.document.Additional.DailyOTLog[i].EndDate = item._EndDate;
                        $scope.document.Additional.DailyOTLog[i].BeginDate = item._BeginDate;
                        $scope.document.Additional.DailyOTLog[i].EmployeeID = item._EmployeeID;
                        $scope.document.Additional.DailyOTLog[i].CREATOR = $scope.oRequestParameter.Creator.EmployeeID;
                        i++;
                    });

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.CalculateOT();


            $scope.GetOTWorkType = function () {
                //$scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/GetOTWorkType',
                    data: $scope.oRequestParameter
                }).then(function successCallback(response) {
                    $scope.oTWorkTypes = response.data;
                    //$scope.loader.enable = false;
                }, function errorCallback(response) {
                    //$scope.loader.enable = false;
                });
            };

            $scope.GetOTWorkType();

            $scope.getTypeName = function (typeId) {
                if ($scope.oTWorkTypes) {
                    $scope.attTypes = $scope.oTWorkTypes.filter(function (item) {
                        return item.__otWorkTypeID === typeId;
                    });
                    return $scope.attTypes[0].__otWorkTypeDesc;
                }
                return "";
            };

            $scope.textWarningOverQuota = null;
            $scope.CheckoverQuota = function () {

                $scope.listDatetime = [];

                if ($scope.document.Additional.DailyOTLog.length > 0) {

                    for (var i = 0; i < $scope.document.Additional.DailyOTLog.length; i++) {

                        if ($scope.document.Additional.DailyOTLog[i].oTimeEval != null
                            && $scope.document.Additional.DailyOTLog[i].oTimeEval.length > 0) {

                            for (var j = 0; j < $scope.document.Additional.DailyOTLog[i].oTimeEval.length; j++) {

                                var dataBeginDate = $filter('date')($scope.document.Additional.DailyOTLog[i].oTimeEval[j].BeginDate, 'yyyy-MM-dd');
                                var dataEndDate = $filter('date')($scope.document.Additional.DailyOTLog[i].oTimeEval[j].EndDate, 'yyyy-MM-dd');

                                var checDatetimeDuplicate = [];
                                if (dataBeginDate == dataEndDate) {

                                    // เบิก OT ไม่มีการข้ามวัน

                                    checDatetimeDuplicate = $scope.listDatetime.filter(function (a) {
                                        return a == dataBeginDate;
                                    });

                                    if (checDatetimeDuplicate.length == 0) {
                                        $scope.listDatetime.push(dataBeginDate);
                                    }
                                }
                                else {

                                    // เบิก OT  มีการข้ามวัน

                                    checDatetimeDuplicate = $scope.listDatetime.filter(function (a) {
                                        return a == dataBeginDate;
                                    });

                                    if (checDatetimeDuplicate.length == 0) {
                                        $scope.listDatetime.push(dataBeginDate);
                                    }

                                    checDatetimeDuplicate = $scope.listDatetime.filter(function (a) {
                                        return a == dataEndDate;
                                    });

                                    if (checDatetimeDuplicate.length == 0) {
                                        $scope.listDatetime.push(dataEndDate);
                                    }
                                }
                            }
                        }
                    }

                    if ($scope.listDatetime.length > 0) {

                        var URL = CONFIG.SERVER + 'HRTM/GetCheckOTOverMsg';

                        $scope.employeeData = getToken(CONFIG.USER);

                        var oRequestParameter = {
                            InputParameter: {}
                            , CurrentEmployee: $scope.employeeData
                            , Requestor: $scope.requesterData
                            , Creator: getToken(CONFIG.USER)
                            , ListDataDateTime: $scope.listDatetime
                        };

                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {

                            $scope.textWarningOverQuota = response.data.OTStatus;

                        }, function errorCallback(response) {
                            console.log(response);
                        });

                    }
                }
            };
            $scope.CheckoverQuota();
            

            $scope.ChildAction.SetData = function () {
                //GetCalendar();

            };

            $scope.ChildAction.LoadData = function () {

            };

            $scope.ChildAction.SetData();

        }]);
})();