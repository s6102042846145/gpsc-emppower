﻿(function () {
    angular.module('ESSMobile')
        .controller('DeleteDailyOTEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {

            $scope.employeeData = getToken(CONFIG.USER);

            $scope.oRequestParameter = {
                InputParameter: {}
                , CurrentEmployee: $scope.employeeData
                , Requestor: $scope.requesterData
                , Creator: getToken(CONFIG.USER)
                , Language: $scope.employeeData.Language
            };

            angular.forEach($scope.document.Additional.DailyOTLog, function (item) {
                if (item.OTClockINOUT && item.OTClockINOUT.length==0)
                     item.OTClockINOUT = null;
            });

            $scope.calH = function (item) {
                if (item) {
                    return (item / 60).toFixed(2);
                }
                return 0;
            }

            $scope.GetOTWorkType = function () {
                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'HRTM/GetOTWorkType',
                    data: $scope.oRequestParameter
                }).then(function successCallback(response) {
                    $scope.oTWorkTypes = response.data;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            }
            $scope.GetOTWorkType();

            $scope.getTypeName = function (typeId) {
                if ($scope.oTWorkTypes) {
                    $scope.attTypes = $scope.oTWorkTypes.filter(function (item) {
                        return item.__otWorkTypeID === typeId;
                    });
                    return $scope.attTypes[0].__otWorkTypeDesc;
                }
                return "";
            }

            $scope.ChildAction.SetData = function () {

            };

            $scope.ChildAction.LoadData = function () {

            };

            $scope.ChildAction.SetData();

        }]);
})();