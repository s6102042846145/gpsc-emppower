﻿(function () {
    angular.module('ESSMobile')
        .controller('SettingSystemForAdminController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal) {

            $scope.leftSystem = [];
            $scope.rightSystem = [];

            $scope.loadGroupSetupForAdmin = function () {

                var URL = CONFIG.SERVER + 'HRTM/GetGroupSetupForAdmin';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {
                        ReportGroupId: 5001
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.leftSystem = response.data.LeftSystem;
                    $scope.rightSystem = response.data.ReightSystem;

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.loadGroupSetupForAdmin();

            $scope.next = function (item) {

                var url = '/frmViewContent/' + item.SubjectReportSubID;
                $location.path(url);
            };

        }]);
})();