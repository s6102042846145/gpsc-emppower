﻿(function () {
    angular.module('ESSMobile')
        .controller('EmployeeController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$uibModal', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $uibModal) {


            function download(dataurl, filename) {
                var a = document.createElement("a");
                a.href = dataurl;
                a.setAttribute("download", filename);
                var b = document.createEvent("MouseEvents");
                b.initEvent("click", false, true);
                a.dispatchEvent(b);
                return false;
            }

            $scope.firstText = "";
            $scope.lastText = "";
            $scope.previousText = "";
            $scope.nextText = "";

            $scope.profile = getToken(CONFIG.USER);
            if ($scope.profile) {

                if ($scope.profile.Language === "TH") {
                    $scope.firstText = "หน้าแรก";
                    $scope.lastText = "หน้าสุดท้าย";
                    $scope.previousText = "ก่อน";
                    $scope.nextText = "ถัดไป";
                } else {
                    $scope.firstText = "First";
                    $scope.lastText = "Last";
                    $scope.previousText = "Previous";
                    $scope.nextText = "Next";
                }

            }

            $scope.remark;
            $scope.PATextcategory = 'CVREPORT';
            $scope.EDUCATIONLEVEL = $scope.Text["EDUCATIONLEVEL"];
            $scope.INSTITUTE = $scope.Text["INSTITUTE"];
            $scope.COUNTRY = $scope.Text["COUNTRY"];
            $scope.CERTIFICATE = $scope.Text["CERTIFICATE"];
            $scope.BRANCH = $scope.Text["BRANCH"];
            $scope.AREA = $scope.Text["AREA"];

            $scope.textHeader = "เรียกดูข้อมูลประวัติพนักงาน";
            $scope.sslPincode = "";
            $scope.isShowCv = false;

            $scope.currentPage = 1;
            $scope.totalItem = 8;


            $scope.forgetPincode = function () {
                $location.path('frmViewContent/907');
            };

            // Modal
            $scope.openPincodeCV = function () {

                var modalInstance = $uibModal.open({
                    animation: $scope.animationsEnabled,
                    templateUrl: 'openPincodeCv.ng-popup.html',
                    controller: 'openPincodeCvController',
                    backdrop: 'static',
                    keyboard: false,
                    size: 'sp',
                    resolve: {
                        items: function () {
                            var data = {
                                requestor: $scope.requesterData,
                                text: $scope.Text,
                                loader: $scope.loader,
                                fnForgetPincode: function () {
                                    $scope.forgetPincode();
                                },
                                fnLoadPersonalCv: function (data, ssl) {
                                    $scope.loadPersonalCv(data, ssl);
                                },
                                fnClearPersonalCv: function () {
                                    $scope.clearPersonalCv();
                                }

                            };
                            return data;
                        }
                    }
                });
            };
            $scope.openPincodeCV();

            // Load CV
            $scope.CVdata = {};
            $scope.educationList = [];
            $scope.workHistoryList = [];
            $scope.salaryRateList = [];
            $scope.isExportdata = false;
            $scope.loadPersonalCv = function (personal, ssl) {

                $scope.isExportdata = personal.IsExportPptx;
                $scope.isShowCv = personal.IsPincodeValid;
                $scope.sslPincode = ssl;
                $scope.CVdata = personal.Personal;
                $scope.educationList = personal.Education.oResult;
                $scope.workHistoryList = personal.Working.ListWorkPeriod;
                $scope.salaryRateList = personal.Salary;
                $scope.assessmentHistory = personal.AssessmentHistory;

                if (personal.Working.ListWorkPeriod.length > 0) {

                    $scope.currentPage = personal.Working.Page;
                    $scope.totalItem = personal.Working.Total;
                }

            };

            // Export PPTX
            $scope.exportPPTX = function () {

                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPA/ExportPowerPointEmployee';
                var oRequestParameter = {
                    InputParameter: {
                        PinCode: $scope.sslPincode
                    }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    //console.log(response);
                    if (response.data) {
                        var url = CONFIG.SERVER + 'Client/Report/' + response.data;
                        //console.log(url);
                        download(url, response.data);
                    }
                    $scope.loader.enable = false;

                }, function errorCallback(response) {
                        $scope.loader.enable = false;
                });
            };

            // Clear CV
            $scope.clearPersonalCv = function () {
                $scope.isShowCv = false;
                $scope.CVdata = {};
                $scope.educationList = [];
                $scope.workHistoryList = [];
                $scope.salaryRateList = [];
            };

            $scope.loadHistoryWorking = function () {

                if ($scope.isShowCv) {

                    $scope.loader.enable = true;
                    var oEmployeeData = getToken(CONFIG.USER);
                    var URL = CONFIG.SERVER + 'HRPA/HistoryWorking';
                    var oRequestParameter = {
                        InputParameter: {
                            "PinCode": $scope.sslPincode,
                            "Page": $scope.currentPage,
                            "ItemPerPage": 8
                        }
                        , CurrentEmployee: oEmployeeData
                        , Requestor: $scope.requesterData
                        , Creator: getToken(CONFIG.USER)
                    };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {

                        $scope.workHistoryList = response.data.ListWorkPeriod;
                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                    });

                }
            };



            //$scope.employeeData = getToken(CONFIG.USER);
            //var URL = CONFIG.SERVER + 'HRPA/GetEmployeeAllData';
            //var oRequestParameter = {
            //    InputParameter: { EmployeeID: $scope.requesterData.EmployeeID }
            //    , CurrentEmployee: getToken(CONFIG.USER)
            //    , Requestor: $scope.requesterData
            //    , Creator: getToken(CONFIG.USER)

            //};
            //$http({
            //    method: 'POST',
            //    url: URL,
            //    data: oRequestParameter
            //}).then(function successCallback(response) {
            //    $scope.CVdata = response.data;
            //}, function errorCallback(response) {
            //});


            //URL = CONFIG.SERVER + 'HRPA/GetAllPersonalEducationHistory';
            //oRequestParameter = {
            //    InputParameter: { EmployeeID: $scope.requesterData.EmployeeID }
            //    , CurrentEmployee: getToken(CONFIG.USER)
            //    , Requestor: $scope.requesterData
            //    , Creator: getToken(CONFIG.USER)

            //};
            //$http({
            //    method: 'POST',
            //    url: URL,
            //    data: oRequestParameter
            //    //data: getToken(CONFIG.USER)
            //}).then(function successCallback(response) {
            //    $scope.educationList = response.data.oResult;
            //}, function errorCallback(response) {
            //});


            //URL = CONFIG.SERVER + 'HRPA/GetworkHistory';
            //oRequestParameter = {
            //    InputParameter: { EmployeeID: $scope.requesterData.EmployeeID }
            //    , CurrentEmployee: getToken(CONFIG.USER)
            //    , Requestor: $scope.requesterData
            //    , Creator: getToken(CONFIG.USER)

            //};
            //$http({
            //    method: 'POST',
            //    url: URL,
            //    data: oRequestParameter
            //    //data: getToken(CONFIG.USER)
            //}).then(function successCallback(response) {
            //    $scope.workHistoryList = response.data;
            //}, function errorCallback(response) {
            //});


            //URL = CONFIG.SERVER + 'HRPA/GetSalaryRate';
            //oRequestParameter = {
            //    InputParameter: { EmployeeID: $scope.requesterData.EmployeeID }
            //    , CurrentEmployee: getToken(CONFIG.USER)
            //    , Requestor: $scope.requesterData
            //    , Creator: getToken(CONFIG.USER)
            //};
            //$http({
            //    method: 'POST',
            //    url: URL,
            //    data: oRequestParameter
            //    //data: getToken(CONFIG.USER)
            //}).then(function successCallback(response) {
            //    $scope.salaryRateList = response.data;
            //}, function errorCallback(response) {
            //});

            //$scope.max = 200;

            //$scope.random = function () {
            //    var value = Math.floor(Math.random() * 100 + 1);
            //    var type;

            //    if (value < 25) {
            //        type = 'success';
            //    } else if (value < 50) {
            //        type = 'info';
            //    } else if (value < 75) {
            //        type = 'warning';
            //    } else {
            //        type = 'danger';
            //    }

            //    $scope.showWarning = type === 'danger' || type === 'warning';

            //    $scope.dynamic = value;
            //    $scope.type = type;
            //};

            //$scope.random();

            //$scope.randomStacked = function () {
            //    $scope.stacked = [];
            //    var types = ['success', 'info', 'warning', 'danger'];

            //    for (var i = 0, n = Math.floor(Math.random() * 4 + 1); i < n; i++) {
            //        var index = Math.floor(Math.random() * 4);
            //        $scope.stacked.push({
            //            value: Math.floor(Math.random() * 30 + 1),
            //            type: types[index]
            //        });
            //    }
            //};

            //$scope.randomStacked();

        }])
        .controller('openPincodeCvController', ['items', '$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', '$uibModalInstance', function (items, $scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal, $uibModalInstance) {

            $scope.MsgValidate = "";
            $scope.OldPinCode = "";
            $scope.Text = items.text;
            $scope.loading = items.loader;
            $scope.countIncorrectPinCode = 0;

            var lastPincodeTime = localStorage.getItem("lastPincodeTime");
            var lastPincode = localStorage.getItem("lastPincode");
            if (lastPincodeTime) {
                lastPincodeTime = new Date(lastPincodeTime);
            } else {
                lastPincodeTime = new Date(2000, 1, 1);
                lastPincode = '';
                localStorage.setItem("lastPincodeTime", JSON.stringify(lastPincodeTime));
                localStorage.setItem("lastPincode", JSON.stringify(lastPincode));
            }

            $scope.getValidatePinCode = function () {

                if (!$scope.OldPinCode) {
                    $scope.MsgValidate = "กรุณาระบุ PINCODE";
                    return;
                }

                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPA/PerosonalCv';
                var oRequestParameter = {
                    InputParameter: {
                        "PinCode": $scope.OldPinCode,
                        "Page": 1,
                        "ItemPerPage": 8
                    }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: items.requestor
                };

                $scope.loading = true;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.loading = false;
                    $scope.isValidate = response.data.IsPincodeValid;
                    if ($scope.isValidate) {
                        localStorage.setItem("lastPincodeTime", new Date());
                        localStorage.setItem("lastPincode", $scope.OldPinCode);

                        $scope.countIncorrectPinCode = 0;
                        $scope.MsgValidate = '';
                        items.fnLoadPersonalCv(response.data, $scope.OldPinCode);
                        $uibModalInstance.close();
                    }
                    else {

                        if ($scope.countIncorrectPinCode > 2) {
                            $scope.alertPin();
                            items.fnClearPersonalCv();
                        }

                        $scope.countIncorrectPinCode += 1;
                        $scope.MsgValidate = 'กรุณาใส่ PINCODE ให้ถูกต้อง';
                    }
                }, function errorCallback(response) {
                });
            };

            if (new Date() - lastPincodeTime < 1000 * 60 * 3) {
                // skip pin code
                $scope.OldPinCode = lastPincode;
                $scope.getValidatePinCode();
                localStorage.setItem("lastPincodeTime", JSON.stringify(new Date()));
            }

            $scope.alertPin = function () {

                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPY/AlertIncorrectPIN';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: oEmployeeData
                    , Requestor: items.requestor
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                }, function errorCallback(response) {
                });
            };

            $scope.closeModal = function () {
                $uibModalInstance.close();
            };

            $scope.forgetPincode = function () {
                $uibModalInstance.close();
                items.fnForgetPincode();
            };


        }]);
})();