﻿(function () {
    angular.module('ESSMobile')
        .controller('PersonalAddressContentController', ['$scope', '$http', '$routeParams', '$location', 'DTOptionsBuilder', '$route', 'CONFIG', '$mdDialog', '$filter', '$timeout', '$q', '$log', function ($scope, $http, $routeParams, $location, DTOptionsBuilder, $route, CONFIG, $mdDialog, $filter, $timeout, $q, $log) {
            $scope.PATextcategory = 'HRPAPERSONALADDRESS';
            $scope.ADDRESSTYPE = $scope.Text["ADDRESSTYPE"];
            $scope.TEXTCOUNTRY = $scope.Text["COUNTRY"];
            //$scope.content.Header = $scope.Text[$scope.PATextcategory].CONSOLE_HEADSUBJECT;
            $scope.itemKey = angular.isUndefined($routeParams.itemKey) && $routeParams.itemKey != '0' ? 'null' : $routeParams.itemKey;
            $scope.employeeData = getToken(CONFIG.USER);
            $scope.AddressInfo;
            $scope.ErrorMessage = '';
            $scope.Country;
            $scope.CountryName;

            // ------ get history employee data of one man -------//

            var oRequestParameter = {
                InputParameter: { }
                , CurrentEmployee: getToken(CONFIG.USER)
                , Requestor: $scope.requesterData
            };
            var URL = CONFIG.SERVER + 'HRPA/GetPersonalAddress';
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                console.log('document.', response.data);

                //$scope.AddressInfo = response.data;

                $scope.AddressInfo = response.data.oResult;
                $scope.DictFileAttachmentList = response.data.dictFileAttachmentList;

                getAllCountryData();
                //for (var i = 0; i < $scope.AddressInfo.length; i++) {
                //    $scope.AddressInfo[i].NoSpecify = $scope.ContactInfo[i].EndDate == '9999-12-31T00:00:00';
                //}

            }, function errorCallback(response) {
                // Error
                console.log('error RequestController.', response);
            });



            function getAllCountryData() {
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetAllCountryData/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.Country = response.data.Country;

                    if ($scope.Country != 'null' && $scope.Country.length > 0) {
                    //    $scope.CountryName = $scope.Country[findWithAttr($scope.Country, 'CountryCode', $scope.AddressInfo[0].Country)].CountryName;
                    }
                }, function errorCallback(response) {
                    // Error
                    console.log('error getAllCountryData.', response);
                });
            }

            getRequestTypeFileSet();
            function getRequestTypeFileSet() {
                var oEmployeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    //InputParameter: { "EmployeeID": oEmployeeData.EmployeeID, "RequestTypeID": 602, "RequestSubType": "" }
                    InputParameter: { "RequestTypeID": 602, "RequestSubType": "" }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'Workflow/GetRequestTypeFileSet/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.FileAttachmentList = response.data;
                }, function errorCallback(response) {
                    // Error
                    console.log('error getRequestTypeFileSet.', response);
                });
            }

            function findWithAttr(array, attr, value) {
                for (var i = 0; i < array.length; i += 1) {
                    if (array[i][attr] === value) {
                        return i;
                    }
                }
            }

        }]);
})();