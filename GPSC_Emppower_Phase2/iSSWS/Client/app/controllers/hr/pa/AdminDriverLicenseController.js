﻿(function () {
    angular.module('ESSMobile')
        .controller('AdminDriverLicenseController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal) {

            //#### FRAMEWORK FUNCTION ### START

            //#### FRAMEWORK FUNCTION ### END

            //#### OTHERS FUNCTION ### START

            //Do something ...

            //#### OTHERS FUNCTION ### END

            // Get Config Multi Company
            $scope.isHideMultiCompany = true;
            $scope.GetConfingMultiCompany = function () {
                var URL = CONFIG.SERVER + 'Employee/GetConfigMultiCompany';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data === 1) {
                        $scope.isHideMultiCompany = false;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetConfingMultiCompany();

            $scope.back = function () {
                var url = '/frmViewContent/5000';
                $location.path(url);
            };

            $scope.profile = getToken(CONFIG.USER);
            if ($scope.profile) {

                if ($scope.profile.Language === "TH") {
                    $scope.firstText = "หน้าแรก";
                    $scope.lastText = "หน้าสุดท้าย";
                    $scope.previousText = "ก่อน";
                    $scope.nextText = "ถัดไป";
                } else {
                    $scope.firstText = "First";
                    $scope.lastText = "Last";
                    $scope.previousText = "Previous";
                    $scope.nextText = "Next";
                }

            }

            $scope.currentPage = 1;
            $scope.totalItem = 0;
            $scope.ItemPerPage = 20;
            $scope.list_driver_license = [];
            $scope.loadDriverLicense = function () {

                //console.log($scope.currentPage);
                var URL = CONFIG.SERVER + 'HRPA/GetDriverLicenseEmployeeAll';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {
                        "Page": $scope.currentPage,
                        "ItemPerPage": $scope.ItemPerPage,
                        "sCompany": $scope.selectCompanyCode
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.list_driver_license = response.data.ListPersonalDriverLicenseData;
                    $scope.totalItem = response.data.Total;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.list_driver_license = [];
                    $scope.totalItem = 0;
                    $scope.loader.enable = false;
                });

            };

            $scope.exportExcel = function () {

                var URL = CONFIG.SERVER + 'HRPA/ExportExcelDriverLicenseEmployeeAll';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                var config = {
                    responseType: "arraybuffer",
                    cache: false
                };

                var apiData = { url: "", fileName: "" };
                var now = new Date();
                var now_time = now.getUTCFullYear() + "" + now.getUTCMonth() + "" + now.getUTCDate() + "" + now.getHours() + "" + now.getUTCMinutes() + "" + now.getUTCSeconds();
                apiData.fileName = "ReportDriverLicense" + now_time + ".xlsx";

                $scope.loader.enable = true;
                $http.post(URL, oRequestParameter, config)
                    .then(function successCallback(response) {

                        var blob = new Blob([response.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
                        saveAs(blob, apiData.fileName);
                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                    });
            };

            // Support Admin Multi Company
            $scope.employeeData = getToken(CONFIG.USER);
            $scope.selectCompanyCode = $scope.employeeData.CompanyCode; // Default CompanyCode GPSC
            $scope.CompanyList = [];
            // Get List Authorization Comapny In Employee Admin
            $scope.GetAuthorizationCompany = function () {
                var URL = CONFIG.SERVER + 'workflow/GetAuthorizationCompany';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.CompanyList = response.data;
                    $scope.loadDriverLicense();

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetAuthorizationCompany();

            // Change Company Reload Data Export Report
            $scope.changeCompany = function () {
                $scope.loadDriverLicense();
                $scope.currentPage = 1;
            };

        }]);
})();