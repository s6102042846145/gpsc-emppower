﻿(function () {
    angular.module('ESSMobile')
        .controller('OrganizationStructureController', ['$scope', '$http', '$routeParams', '$location', 'CONFIG', function ($scope, $http, $routeParams, $location, CONFIG) {

                var URL = CONFIG.SERVER + 'Employee/GetOrganizationStructure';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                    //data: getToken(CONFIG.USER)
                }).then(function successCallback(response) {
                    // Success
                    $scope.OrganizationStructureList = response.data;
                }, function errorCallback(response) {
                }); 
        }]);
})();