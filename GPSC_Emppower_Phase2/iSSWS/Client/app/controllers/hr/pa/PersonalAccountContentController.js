﻿(function () {
    angular.module('ESSMobile')
        .controller('PersonalAccountContentController', ['$scope', '$http', '$routeParams', '$location', 'DTOptionsBuilder', '$route', 'CONFIG', '$mdDialog', '$filter', '$timeout', '$q', '$log', function ($scope, $http, $routeParams, $location, DTOptionsBuilder, $route, CONFIG, $mdDialog, $filter, $timeout, $q, $log) {
            //#### FRAMEWORK FUNCTION ### START
            /******************* variable start ********************/
            $scope.personalaccountloader = false;
            $scope.Textcategory = "HRPABANKDETAIL";
            $scope.content.isShowHeader = true;

            /******************* variable end ********************/


            /******************* listener start ********************/
            $scope.init = function () {
                init();
            }

            /******************* listener end ********************/


            /******************* action start ********************/
            function init() {
                // debug mode
                getBankDetail();
            }

            /******************* action end ********************/


            /******************* service caller  start ********************/
            function getBankDetail() {
                // ------ get person data with employeeid and begindate -------//
                var oEmployeeData = getToken(CONFIG.USER);
                var checkDate = $filter('date')(new Date(), 'yyyy-MM-dd');
                var URL = CONFIG.SERVER + 'HRPA/GetBankDetail';
                var oRequestParameter = {
                    InputParameter: { "EmployeeID": oEmployeeData.EmployeeID }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                    //data: getToken(CONFIG.USER)
                }).then(function successCallback(response) {
                    // Success
                    $scope.BankDetail = response.data;
                    getBank($scope.BankDetail.Bank);
                }, function errorCallback(response) {
                });
            }

            function findWithAttr(array, attr, value) {
                for (var i = 0; i < array.length; i += 1) {
                    if (array[i][attr] === value) {
                        return i;
                    }
                }
            }

            function getBank(Code) {
                // ------ get person data with employeeid and begindate -------//
                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPA/GetBank';
                var oRequestParameter = {
                    InputParameter: { "Code": Code }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                    //data: getToken(CONFIG.USER)
                }).then(function successCallback(response) {
                    // Success
                    $scope.BankName = response.data.Description;
                }, function errorCallback(response) {
                });
            }

            
            /******************* service caller  end ********************/


            //#### FRAMEWORK FUNCTION ### END

            //#### OTHERS FUNCTION ### START

            //Do something ...

            //#### OTHERS FUNCTION ### END
        }]);
})();