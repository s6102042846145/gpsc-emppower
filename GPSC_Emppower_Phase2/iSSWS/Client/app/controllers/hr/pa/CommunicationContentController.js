﻿(function () {
    angular.module('ESSMobile')
        .controller('CommunicationContentController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {
            $scope.PATextcategory = 'PERSONALADMINISTRATION'; 
            $scope.CONTACTcategory = 'HRPAPERSONALCONTACT';
            $scope.content.Header = $scope.Text[$scope.PATextcategory].CONSOLE_HEADSUBJECT;
            $scope.itemKey = angular.isUndefined($routeParams.itemKey) && $routeParams.itemKey != '0' ? 'null' : $routeParams.itemKey;
            $scope.employeeData = getToken(CONFIG.USER);
            $scope.ContactInfo;
            $scope.isResign = false;
            $scope.ResignDate = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.ErrorMessage = '';
            $scope.IsViewCommunicationOnly = true;

            // ------ get history employee data of one man -------//

            function getIsViewCommunicationOnly() {
                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPA/GetIsViewCommunicationOnly';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.IsViewCommunicationOnly = response.data;
                }, function errorCallback(response) {
                });
            }
            getIsViewCommunicationOnly();

            var oRequestParameter = {
                InputParameter: {}
                , CurrentEmployee: getToken(CONFIG.USER)
                , Requestor: $scope.requesterData
            };
            var URL = CONFIG.SERVER + 'HRPA/GetCommunicationData';
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                console.log('document.', response.data);

                $scope.ContactInfo = response.data;
                for (var i = 0; i < $scope.ContactInfo.length; i++) {
                    $scope.ContactInfo[i].NoSpecify = $scope.ContactInfo[i].EndDate == '9999-12-31T00:00:00';
                }
                //getContactSelectData();

            }, function errorCallback(response) {
                // Error
                console.log('error RequestController.', response);
            });

            getRequestTypeFileSet();
            function getRequestTypeFileSet() {
                var oEmployeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    //InputParameter: { "EmployeeID": oEmployeeData.EmployeeID, "RequestTypeID": 603, "RequestSubType": "" }
                    InputParameter: { "RequestTypeID": 603, "RequestSubType": "" }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'Workflow/GetRequestTypeFileSet/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.FileAttachmentList = response.data;
                }, function errorCallback(response) {
                    // Error
                    console.log('error getRequestTypeFileSet.', response);
                });
            }

        }]);
})();