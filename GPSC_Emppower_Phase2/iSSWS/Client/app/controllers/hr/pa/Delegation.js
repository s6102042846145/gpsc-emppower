﻿(function () {
    angular.module('ESSMobile')
        .controller('DelegationController', ['$scope', '$http', '$routeParams', '$location', 'DTOptionsBuilder', '$route', 'CONFIG', '$mdDialog', '$filter', '$timeout', '$q','$log', function ($scope, $http, $routeParams, $location, DTOptionsBuilder, $route, CONFIG, $mdDialog, $filter, $timeout,$q,$log) {
          
            $scope.PATextcategory = 'DELEGATION';
            $scope.content.Header = $scope.Text[$scope.PATextcategory].SUBJECT;
            $scope.content.isShowHeader = false;
            $scope.PositionTextcategory = 'POSITION';
            $scope.EmpGroupTextcategory = 'EMPGROUP';
            $scope.EmpSubGroupTextcategory = 'EMPSUBGROUP';
            $scope.OrganizationTextcategory = 'ORGANIZATION';
            $scope.AreaTextcategory = 'AREA';
            $scope.SubAreaTextcategory = 'SUBAREA';

           
            /******************* variable start ********************/
            $scope.delegationAddAndEditorMode = true;
            $scope.delegationloader = false;
            var isEdit = false;
            $scope.persons = {
                delegator: {},
                delegated: {}
            };
            $scope.delegatedPersons =[];
            $scope.oDelegation;
            $scope.delegationList;
            $scope.myDate = new Date();
            $scope.minDate = new Date(
                $scope.myDate.getFullYear(),
                $scope.myDate.getMonth(),
                $scope.myDate.getDate());
            $scope.dateSet = {
                years: [],
                month: [],
                selectedYear: '',
                selectedMonth:'',
            }
            $scope.startDate = null;
            $scope.endDate = null;
            $scope.canSetStart;
            
            $scope.delegatedDocument = [];
            $scope.selectDelegatePosition;
            $scope.delegationId;
            $scope.delegationRemark;
            $scope.initFirst = false;
            $scope.status = {
                isView: false
            }
            
            /******************* variable end ********************/




            /******************* listener start ********************/
            $scope.init = function () {
                init();
            }
           
            $scope.add = function () {
                add();
            }
            $scope.remove = function (oDelegate, ev) {
                remove(oDelegate, ev);
            }
            $scope.edit = function (oDelegate) {
                edit(oDelegate); 
            }
           
            $scope.submit = function () {
                submit();
            };
            $scope.cancel = function () {
                cancel();
            }
            
            $scope.checkMaxDate = function () {
                checkMaxDate();
            }
            $scope.getDateFormate = function (date) {
                return getDateFormate(date);
            }
            $scope.getDelegationList = function () {
                if ($scope.initFirst) {
                    $scope.delegationloader = true;
                    reqGetDelegationList();
                }
                
            }
            $scope.View = function (requestNo) {
                if (angular.isDefined(requestNo)) {
                    var path = CONFIG.SERVER + 'Client/index.html#!/frmViewRequest/' + requestNo + '/' + $scope.requesterData.CompanyCode + '/' + null + '/' + 'true' + '/' + 'true';
                    window.open(path, '_blank');
                }
                //view(oDelegate);
            }
            $scope.back = function () {
                back();
            }
            /******************* listener end ********************/



            $scope.selectAll = function () {
                angular.forEach($scope.delegatedDocument, function (item, key) {
                    item.IsCheck = true;
                });
            }
            $scope.unSelectAll = function () {
                angular.forEach($scope.delegatedDocument, function (item, key) {
                    item.IsCheck = false;
                });
            }
            

            /******************* action start ********************/

            function init() {
                $scope.delegationloader = true;
                $scope.initFirst = true;
                $scope.persons.delegator = getToken(CONFIG.USER);
                createYearAndMonth();
                reqGetDelegationList();
                reqGetDocumentType();
                reqGeDelegationObj();
                
                // debug mode
                //add();
                $scope.delegationAddAndEditorMode = false;
            }

            function edit(oDelegate) {
                isEdit = true;
                setDefaultValue(oDelegate);
            }
            function remove(oDelegate, ev) {
                var confirm = $mdDialog.confirm()
                  .title($scope.Text[$scope.PATextcategory].M3)
                  .textContent($scope.Text[$scope.PATextcategory].M1)
                  .targetEvent(ev)
                  .ok('OK')
                  .cancel($scope.Text['SYSTEM'].BUTTON_NO);

                $mdDialog.show(confirm).then(function () {
                    
                   
                    $scope.delegationloader = true
                    reqRemoveDelegation(oDelegate);
                   
                }, function () {
                   
                });
            }
            
            function add() {
                isEdit = false;
                setDefaultValue();
            }

       

            function submit() {

                if (validation()) {
                    $scope.delegationloader = true

                    $scope.oDelegation.DetailList = $scope.delegatedDocument;
                    $scope.oDelegation.DelegateID = $scope.delegationId;
                    $scope.oDelegation.BeginDate = $scope.startDate;
                    $scope.oDelegation.EndDate = $scope.endDate;
                    $scope.oDelegation.DelegateFromPosition = $scope.selectDelegatePosition;
                    $scope.oDelegation.DelegateTo = $scope.persons.delegated.EmployeeID;
                    $scope.oDelegation.DelegateToPosition = $scope.persons.delegated.PositionID;
                    $scope.oDelegation.Remark = $scope.delegationRemark;
                    console.debug($scope.oDelegation);
                    reqCreateDelegation();
                } else {
                    return;
                }
               
            }

            function cancel() {

                clear();
                // out editor mode;
                $scope.delegationAddAndEditorMode = false;
            }
            function back() {
                $scope.status.isView = false;
                $scope.delegationAddAndEditorMode = false;
                clear();
            }

            function mAlert(message){
                $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title($scope.Text[$scope.PATextcategory].M3)
                       .textContent(message)
                       .ok('OK')
                   );
            }

            function view(oDelegate) {


                $scope.status.isView = true;
                console.debug(oDelegate);
                console.debug($scope.delegatedDocument);
                

                // set id 
                $scope.delegationId = oDelegate.DelegateID;


                // set delegator
                $scope.persons.delegator.Name = oDelegate.DelegateFromName;
                $scope.persons.delegator.EmployeeID = oDelegate.DelegateFrom;
                $scope.persons.delegator.OrgUnit = oDelegate.DelegateFromOrg;
                $scope.persons.delegator.OrgUnitName = oDelegate.DelegateFromOrgName;
                $scope.selectDelegatePosition = oDelegate.DelegateFromPosition + ":" + oDelegate.DelegateFromPositionName
           
                // set delegated
               
                $scope.persons.delegated = jQuery.extend(true, {}, $scope.persons.delegator);
                $scope.persons.delegated.Name = oDelegate.DelegateToName;
                $scope.persons.delegated.EmployeeID = oDelegate.DelegateTo;
                
                $scope.persons.delegated.OrgUnitName = oDelegate.DelegateToOrgName;
                $scope.persons.delegated.OrgUnit = oDelegate.DelegateToOrg;
                $scope.persons.delegated.Position = oDelegate.DelegateToPositionName;
                $scope.persons.delegated.PositionID = oDelegate.DelegateToPosition;
                $scope.startDate = new Date(oDelegate.BeginDate).addHours(-7);
                $scope.endDate = new Date(oDelegate.EndDate).addHours(-7);
                 // set check document
                    for (var i = 0; i < oDelegate.DetailList.length ; i++) {
                        if (oDelegate.DetailList[i].IsCheck) {
                            $scope.delegatedDocument[i].IsCheck = true;
                        } else {
                            console.log(i);
                            console.debug($scope.delegatedDocument[i]);
                            $scope.delegatedDocument[i].IsCheck = false;
                        }
                    }
                $scope.delegationRemark = oDelegate.Remark;

                //console.debug(oDelegate);
                //console.debug($scope.persons.delegated);
               


                
                $scope.delegationAddAndEditorMode = true;

            }
            /******************* action end ********************/

            





            /******************* function start ********************/
            // set default value
            function setDefaultValue(oDelegate) {
               

                $scope.persons.delegator = getToken(CONFIG.USER);
                $scope.canSetStart = true;
                // display add/edit  and hide delegate table
                $scope.delegationAddAndEditorMode = true;

                if (isEdit) {
                    console.debug('set default edit');
                    // set start date / end date
                    $scope.startDate = new Date(oDelegate.BeginDate);
                    $scope.endDate = new Date(oDelegate.EndDate);
                    $scope.startDate.addHours(-7);
                    $scope.endDate.addHours(-7)
                  
                    if (new Date($scope.startDate) < new Date()) {
                        $scope.canSetStart = false;
                    }

                    // set delegation id
                    $scope.delegationId = oDelegate.DelegateID;

                    // set reason
                    $scope.delegationRemark = oDelegate.Remark;

                    // set position
                    var positionIndex = -1;
                    //console.debug($scope.persons.delegator.AllPosition);
                    //console.debug(oDelegate.DelegateFromPosition);
                    for (var i = 0; i < $scope.persons.delegator.AllPosition.length; i++) {
                        if ($scope.persons.delegator.AllPosition[i].ObjectID == oDelegate.DelegateFromPosition) {
                            positionIndex = i;
                            $scope.selectDelegatePosition = $scope.persons.delegator.AllPosition[i].ObjectID;
                        }
                    }

                    // set delegate person
                    
                    for (var i = 0; i < $scope.delegatedPersons.length; i++) {
                        //console.debug('---------------------');
                        //console.debug($scope.delegatedPersons[i].EmployeeID);
                        //console.debug(oDelegate.DelegateTo);
                        if ($scope.delegatedPersons[i].EmployeeID == oDelegate.DelegateTo) {
                            console.debug('set item');
                            $scope.autocomplete.selectedItem = $scope.delegatedPersons[i];
                            break;
                        }
                    }
                    // set check document
                    for (var i = 0; i < oDelegate.DetailList.length; i++) {
                       
                        if (oDelegate.DetailList[i].IsCheck) {
                            $scope.delegatedDocument[i].IsCheck = true;
                        }else{
                            $scope.delegatedDocument[i].IsCheck = false;
                        }
                    }
                  
                    // not found
                    if (positionIndex == -1) { mAlert($scope.Text[$scope.PATextcategory].M4); return; }

                } else {
                    //console.debug('set default add');
                    clear();
                    $scope.selectDelegatePosition = $scope.persons.delegator.AllPosition[0].ObjectID;
                   
                    
                }
            }
            function validation() {
                
                if ($scope.persons.delegator) { } else { mAlert($scope.Text[$scope.PATextcategory].M5); return false; };
                if ($scope.persons.delegated) { } else { mAlert($scope.Text[$scope.PATextcategory].M6); return false; };
                if ($scope.selectDelegatePosition) { } else { mAlert($scope.Text[$scope.PATextcategory].M7); return false; };
                if ($scope.delegationRemark) { } else { mAlert($scope.Text[$scope.PATextcategory].M10); return false; };
                var nDocSelected = 0;
                for (var i = 0 ; i < $scope.delegatedDocument.length; i++) {
                    if ($scope.delegatedDocument[i].IsCheck == true) {
                        nDocSelected++;
                    }
                }
                if (nDocSelected != 0) { } else { mAlert($scope.Text[$scope.PATextcategory].M8); return false; }

                return true;

            }
            function clear() {
                $scope.myDate = new Date();
                $scope.minDate = new Date(
                    $scope.myDate.getFullYear(),
                    $scope.myDate.getMonth(),
                    $scope.myDate.getDate());

                $scope.startDate = null;
                $scope.endDate = null;
                $scope.persons.delegated = null;
                $scope.selectDelegatePosition = null;
                $scope.autocomplete.selectedItem=null;
                $scope.delegationId = '0';
                $scope.delegationRemark = null;
                for (var i = 0 ; i < $scope.delegatedDocument.length; i++) {
                    $scope.delegatedDocument[i].IsCheck = false
                }
            }
            function createYearAndMonth() {
                //console.debug('init year month');
                $scope.dateSet = {
                    years: [],
                    month: [],
                    selectedYear: '',
                    selectedMonth: '',
                }
                var today = new Date();
                var maxYear = parseInt(today.getFullYear())+10;
                $scope.dateSet.selectedYear = today.getFullYear();
                for (var year = 1990 ; year <=maxYear ; year++) {
                    $scope.dateSet.years.push(year);
                }
                for (var month = 1 ; month <= 12 ; month++) {
                    $scope.dateSet.month.push($scope.Text['SYSTEM']['D_M' + month]);
                }
                $scope.dateSet.selectedMonth = $scope.dateSet.month[parseInt(today.getMonth()) ];
            }
            function getMonth(tMonth) {
                for (var i = 0; i < $scope.dateSet.month.length; i++) {
                    if (tMonth == $scope.dateSet.month[i]) {
                        return i+1;
                    }
                      
                }
                return parseInt(today.getMonth());
            }
            function checkMaxDate() {if( $scope.startDate >  $scope.endDate){ $scope.endDate = $scope.startDate;}}
            function getDateFormate(date) { return $filter('date')(date, 'd/M/yyyy'); }
            Date.prototype.addHours = function (h) {
                this.setHours(this.getHours() + h);
                return this;
            }
            /******************* function end ********************/


            $scope.setSelectedBegin = function (selectedDate) {
                $scope.startDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
            };

            $scope.setSelectedEnd = function (selectedDate) {
                $scope.endDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
            };




            /******************* service caller  start ********************/
            function reqGetDocumentType(employeeId) {
                var oRequestParameter = { CurrentEmployee: $scope.persons.delegator };
                var URL = CONFIG.SERVER + 'workflow/GetAllRequestType';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.delegatedDocument = response.data;

                }, function errorCallback(response) {
                    console.debug(response);
                    console.debug('Fail to call');
                    mAlert($scope.Text[$scope.PATextcategory].M9);
                });
            }

            function reqGeDelegationObj( ) {
                var InputParameter = {
                    DelegateFromID: $scope.persons.delegator.EmployeeID,
                };
                var oRequestParameter = { InputParameter:InputParameter, CurrentEmployee: $scope.persons.delegator};

                var URL = CONFIG.SERVER + 'workflow/GetDelegateObject';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.oDelegation = response.data;
                    var oAllEmp = response.data.AllEmployeePossibleToDelegated;
                    var aAllEmp = [];
                    for (var key in oAllEmp) {
                        var arr = oAllEmp[key];
                        for (var i = 0 ; i < arr.length; i++) {
                            arr[i].KEY_DELEGATED_POSITION = key;
                            aAllEmp.push(arr[i]);
                        }
                    }
                    
                    $scope.delegatedPersons = aAllEmp;
                
                }, function errorCallback(response) {
                    console.debug(response);
                    console.debug('Fail to call');
                    mAlert($scope.Text[$scope.PATextcategory].M9);
                });
            }
            $scope._month = null;
            function reqGetDelegationList() {
                var year = $scope.dateSet.selectedYear+"";
                var month = getMonth($scope.dateSet.selectedMonth) + "";
                $scope._month = month;
                var oRequestParameter = { InputParameter: { Year: year, Month: month }, CurrentEmployee: $scope.persons.delegator };

                var URL = CONFIG.SERVER + 'workflow/GetDelegateDataByCriteria';
            
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    
                    if (response.data.length > 0) {
                        $scope.delegationList = response.data;
                       
                    } else {
                        $scope.delegationList = [];
                        console.debug("Delegation list not found");
                    }
                    $scope.delegationloader = false;
                   // console.debug(response);
                }, function errorCallback(response) {
                    console.debug(response);
                    console.debug('Fail to call');
                    $scope.delegationloader = false;
                    mAlert($scope.Text[$scope.PATextcategory].M9);
                
                });
            }

            function reqCreateDelegation() {
                var oRequestParameter = { InputParameter: { DelegateHeader: $scope.oDelegation }, CurrentEmployee: $scope.persons.delegator };
                var URL = CONFIG.SERVER + 'workflow/SaveDelegateHeader';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    reqGetDelegationList();
                    $scope.delegationAddAndEditorMode = false;
                }, function errorCallback(response) {
                    console.debug(response);
                    console.debug('Fail to call');
                    $scope.delegationAddAndEditorMode = false;
                    mAlert($scope.Text[$scope.PATextcategory].M9);
                });
            }


            function reqRemoveDelegation(oDelegate) {

              
                var oRequestParameter = { InputParameter: { DelegateHeader: oDelegate }, CurrentEmployee: $scope.persons.delegator };
                var URL = CONFIG.SERVER + 'workflow/DeleteDelegateHeader';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if ($scope.delegationList.length == 0) return;

                    for (var i = 0; i < $scope.delegationList.length; i++) {
                        if ($scope.delegationList[i].DelegateID == oDelegate.DelegateID) {
                            $scope.delegationList.splice(i, 1);
                        }
                    }
                    mAlert($scope.Text[$scope.PATextcategory].M2);
                    $scope.delegationloader = false;
                    return;
                }, function errorCallback(response) {
                    console.debug(response);
                    console.debug('Fail to call');
                    $scope.delegationloader = false;
                    mAlert($scope.Text[$scope.PATextcategory].M9);
                });
            }
            /******************* service caller  end ********************/





            /********************  auto complete employee start ******************* */
            $scope.simulateQuery = true;
            $scope.isDisabled = false;
            $scope.querySearchEmployee = querySearchEmployee;
            $scope.selectedItemChange = selectedItemChange;
            $scope.searchTextChange = searchTextChange;
            $scope.newData = newData;
            $scope.autocomplete = {
                selectedItem: null
            }
            function newData(employee) {
            }
            function querySearchEmployee(query) {
                var delegatedPersons_filterd_by_position = $filter('filter')($scope.delegatedPersons, { KEY_DELEGATED_POSITION: $scope.selectDelegatePosition });
                var results = query ? delegatedPersons_filterd_by_position.filter(createFilterFor(query)) : delegatedPersons_filterd_by_position, deferred;
                if ($scope.simulateQuery) {
                    deferred = $q.defer();
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 250, false);
                    return deferred.promise;
                } else {
                    return results;
                }
            }
            function searchTextChange(text) {
            }
            function selectedItemChange(person) {
                if (angular.isDefined(person)) {
                    $scope.persons.delegated = person;
                    console.log(person);
                }
            }
            function createFilterFor(query) {

                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(employee) {
                    // text conbined to search
                    var textquery = employee.EmployeeID + ':' + employee.Name;
                    return (textquery.indexOf(lowercaseQuery) >= 0);
                };

            }

            var tempResult;
            $scope.tryToSelect = function (text) {
                for (var i = 0; i < $scope.delegatedPersons.length; i++) {
                    if (($scope.delegatedPersons[i].EmployeeID + ":" + $scope.delegatedPersons[i].Name) == text || $scope.delegatedPersons[i].Name == text) {
                        tempResult = $scope.delegatedPersons[i];
                        break;
                    }
                }
                $scope.searchText = '';
                $scope.autocomplete.selectedItem = null;
                $scope.persons.delegated = null;
            }
            $scope.checkText = function (text) {
                var result = null;
                for (var i = 0; i < $scope.delegatedPersons.length; i++) {
                    if (($scope.delegatedPersons[i].EmployeeID + ":" + $scope.delegatedPersons[i].Name) == text || $scope.delegatedPersons[i].Name == text) {
                        result = $scope.delegatedPersons[i];
                        break;
                    }
                }
                if (result) {
                    $scope.searchText = result.EmployeeID + ":" + result.Name;
                    $scope.selectedItemChange(result);
                    $scope.autocomplete.selectedItem = result;
                } else if (tempResult) {
                    $scope.searchText = tempResult.EmployeeID + ":" + tempResult.Name;
                    $scope.selectedItemChange(tempResult);
                    $scope.autocomplete.selectedItem = tempResult;
                }
            }
            /********************  auto complete employee end ******************* */

            /*
                M1:คุณต้องการที่จะลบรายการมอบสิทธิ์รายการนี้หรือไม่
                M2:รายการมอบสิทธิ์ดังกล่าวถูกลบออกจากระบบเรียบร้อยแล้ว
                M3:แจ้งเตือน
                M4:ไม่สามารถดึงค่า default position ได้จาก method setDefaultValue กรุณาติดต่อ developer
                M5:ไม่พบผู้มอบสิทธ์
                M6:โปรดเลือกผู้รับสิทธ์
                M7:โปรดเลือกตำแหน่งที่จะโอนสิทธิ์
                M8:โปรดเลือกเอกสารที่จะโอนสิทธิ์
                M9:Internal Server Error, please contact administrator ระบบเซิฟเวอร์มีปัญหากรุณาติดต่อผู้ดูแลระบบ
                M10:Please give some reason for delegation โปรดให้เหตุผลและความจำเป็นในการโอนสิทธิ์
            */


        }]);

})();

