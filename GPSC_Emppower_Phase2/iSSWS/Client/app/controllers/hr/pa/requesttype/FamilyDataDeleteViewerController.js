﻿(function () {
    angular.module('ESSMobile')
        .controller('FamilyDataDeleteViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {
            //#### FRAMEWORK FUNCTION ### START
            $scope.Textcategory = "FAMILY";
            $scope.FAMILYMEMBER = $scope.Text["FAMILYMEMBER"];
            $scope.COUNTRY = $scope.Text["COUNTRY"];
            $scope.NATIONALITY = $scope.Text["NATIONALITY"];
            $scope.TITLENAME = $scope.Text["TITLENAME"];

            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
            $scope.ChildAction.SetData = function () {
                //Do something ...
                $scope.FamilyData = $scope.document.Additional.FamilyData;
            };

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                //Do something ...
            };

            var employeeDate = getToken(CONFIG.USER);
            $scope.ChildAction.SetData();
            //#### FRAMEWORK FUNCTION ### END

            //#### OTHERS FUNCTION ### START

            //Do something ...

            //#### OTHERS FUNCTION ### END
        }]);
})();