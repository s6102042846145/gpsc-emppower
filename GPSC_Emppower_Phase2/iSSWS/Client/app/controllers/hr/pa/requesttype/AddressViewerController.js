﻿(function () {
    angular.module('ESSMobile')
        .controller('AddressViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {
            //#### FRAMEWORK FUNCTION ### START
            $scope.PATextcategory = 'HRPAPERSONALADDRESS';
            $scope.ADDRESSTYPE = $scope.Text["ADDRESSTYPE"];
            $scope.TEXTCOUNTRY = $scope.Text["COUNTRY"];
            $scope.content.Header = $scope.Text[$scope.PATextcategory].CONSOLE_HEADSUBJECT;
            $scope.itemKey = angular.isUndefined($routeParams.itemKey) && $routeParams.itemKey != '0' ? 'null' : $routeParams.itemKey;
            $scope.employeeData = getToken(CONFIG.USER);
            $scope.ContactInfo;
            $scope.CategoryCode;
            $scope.BeginDate;
            $scope.EndDate;
            $scope.DataText;
            $scope.Country;
            $scope.CountryName;


            $scope.AddrType = $scope.document.Additional.PersonalAddressType[0].Key;

            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
            $scope.ChildAction.SetData = function () {
                //Do something ...

                //$scope.PersonalAddr = $scope.document.Additional.PersonalAddr;
                //$scope.PersonalAddrNew = $scope.document.Additional.PersonalAddrNew;
                //getAddressSelectData();

                $scope.PersonalAddr = $filter('filter')($scope.document.Additional.PersonalAddr, { AddressType: $scope.AddrType });
                $scope.PersonalAddrNew = $filter('filter')($scope.document.Additional.PersonalAddrNew, { AddressType: $scope.AddrType });
          
            };

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                //Do something ...
            };

            var employeeDate = getToken(CONFIG.USER);
            $scope.ChildAction.SetData();
            //#### FRAMEWORK FUNCTION ### END

            //#### OTHERS FUNCTION ### START

            //Do something ...

            function getAllCountryData() {
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetAllCountryData/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.Country = response.data.Country;

                    if ($scope.Country != 'null' && $scope.Country.length > 0) {
                        //$scope.CountryName = $scope.Country[findWithAttr($scope.Country, 'CountryCode', $scope.document.Additional.PersonalAddrNew[0].Country)].CountryName;
                    }
                }, function errorCallback(response) {
                    // Error
                    console.log('error getAllCountryData.', response);
                });
            }

            function findWithAttr(array, attr, value) {
                for (var i = 0; i < array.length; i += 1) {
                    if (array[i][attr] === value) {
                        return i;
                    }
                }
            }

            //function getAddressSelectData() {
            //    var oRequestParameter = {
            //        InputParameter: { }
            //        , CurrentEmployee: getToken(CONFIG.USER)
            //        , Requestor: $scope.requesterData
            //    };
            //    var URL = CONFIG.SERVER + 'HRPA/GetPersonalAddress';
            //    $http({
            //        method: 'POST',
            //        url: URL,
            //        data: oRequestParameter
            //    }).then(function successCallback(response) {
            //        // Success
            //        $scope.PersonalAddr = $scope.PersonalAddr = $filter('filter')(response.data, { AddressType: $scope.AddrType });;

            //        getAllCountryData();
            //        console.log('GetPersonalAddressEdit.', $scope.PersonalAddr)
            //    }, function errorCallback(response) {
            //        // Error
            //        console.log('error RequestController.', response);
            //    });
            //}


            //#### OTHERS FUNCTION ### END
        }]);
})();