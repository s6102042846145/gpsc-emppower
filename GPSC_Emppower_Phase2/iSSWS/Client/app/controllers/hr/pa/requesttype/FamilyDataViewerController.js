﻿(function () {
    angular.module('ESSMobile')
        .controller('FamilyDataViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {
            //#### FRAMEWORK FUNCTION ### START
            $scope.Textcategory = "FAMILY";
            $scope.FAMILYMEMBER = $scope.Text["FAMILYMEMBER"];
            $scope.COUNTRY = $scope.Text["COUNTRY"];
            $scope.NATIONALITY = $scope.Text["NATIONALITY"];
            $scope.TITLENAME = $scope.Text["TITLENAME"];

            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
            $scope.ChildAction.SetData = function () {
                //Do something ...
                getPAConfiguration();
                $scope.FamilyData = $scope.document.Additional.FamilyData;
                $scope.FamilyData_OLD = $scope.document.Additional.FamilyData_OLD;
            };

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                //Do something ...
            };

            var employeeDate = getToken(CONFIG.USER);
            $scope.ChildAction.SetData();
            //#### FRAMEWORK FUNCTION ### END

            //#### OTHERS FUNCTION ### START

            //Do something ...
            function getPAConfiguration() {
                var oRequestParameter = {
                    InputParameter: { "CategoryName": "Family" }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetPAConfigurationList/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.PAConfiguration = response.data;
                }, function errorCallback(response) {
                    // Error
                    console.log('error getPAConfiguration.', response);
                });
            }
            //#### OTHERS FUNCTION ### END
        }]);
})();