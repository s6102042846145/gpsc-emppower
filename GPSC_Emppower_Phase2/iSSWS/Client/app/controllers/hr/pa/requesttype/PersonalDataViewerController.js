﻿(function () {
    angular.module('ESSMobile')
        .controller('PersonalDataViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {
            //#### FRAMEWORK FUNCTION ### START
            $scope.Textcategory = "HRPAPERSONALDATA";
            $scope.Title;
            $scope.Gender;
            $scope.MaritalStatus;
            $scope.Nationality;
            $scope.Language;
            $scope.Religion;
            $scope.Country;
            $scope.Province;

            $scope.TitleName;
            $scope.GenderName;
            $scope.NationalityName;
            $scope.MaritalStatusName;
            $scope.ReligionName;
            $scope.CountryName;
            $scope.BirthPlace;

            $scope.IDCardNo;


            $scope.TitleName_OLD;
            $scope.GenderName_OLD;
            $scope.NationalityName_OLD;
            $scope.MaritalStatusName_OLD;
            $scope.ReligionName_OLD;
            $scope.CountryName_OLD;
            $scope.BirthPlace_OLD;

            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
            $scope.ChildAction.SetData = function () {
                //Do something ...
                getPAConfiguration();
                $scope.PersonalData = $scope.document.Additional.PersonalData;
                $scope.PersonalData_OLD = $scope.document.Additional.PersonalData_OLD;
                getPersonSelectData();
                getIDCardData();
                getProvinceSelectData($scope.PersonalData.BirthCity);
                getProvinceSelectData_OLD($scope.PersonalData_OLD.BirthCity);
            };

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                //Do something ...
            };

            var employeeDate = getToken(CONFIG.USER);
            $scope.ChildAction.SetData();
            //#### FRAMEWORK FUNCTION ### END

            //#### OTHERS FUNCTION ### START

            //Do something ...
            function getPAConfiguration() {
                var oRequestParameter = {
                    InputParameter: { "CategoryName": "PersonalData,PersonalDataAdditional" }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetPAConfigurationList/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.PAConfiguration = response.data;
                }, function errorCallback(response) {
                    // Error
                    console.log('error getPAConfiguration.', response);
                });
            }

            function getIDCardData() {
                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPA/GetIDCardData';
                var oRequestParameter = {
                    InputParameter: { "EmployeeID": $scope.PersonalData.EmployeeID }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.IDCardNo = response.data;
                }, function errorCallback(response) {
                });
            }

            function findWithAttr(array, attr, value) {
                for (var i = 0; i < array.length; i += 1) {
                    if (array[i][attr] === value) {
                        return i;
                    }
                }
            }

            function getPersonSelectData() {
                var oRequestParameter = {
                    InputParameter: { }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetPersonSelectData/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.Title = response.data.Title;
                    $scope.Gender = response.data.Gender;
                    $scope.MaritalStatus = response.data.MaritalStatus;
                    $scope.Nationality = response.data.Nationality;
                    $scope.Language = response.data.Language;
                    $scope.Religion = response.data.Religion;
                    $scope.Country = response.data.Country;

                    if ($scope.Title != 'null' && $scope.Title.length > 0) {
                        $scope.TitleName = $scope.Title[findWithAttr($scope.Title, 'Key', $scope.PersonalData.TitleID)].Description;;
                        $scope.TitleName_OLD = $scope.Title[findWithAttr($scope.Title, 'Key', $scope.PersonalData_OLD.TitleID)].Description;;
                    }
                    if ($scope.Gender != 'null' && $scope.Gender.length > 0) {
                        $scope.GenderName = $scope.Gender[findWithAttr($scope.Gender, 'Key', $scope.PersonalData.Gender)].Description;
                        $scope.GenderName_OLD = $scope.Gender[findWithAttr($scope.Gender, 'Key', $scope.PersonalData_OLD.Gender)].Description;
                    }
                    if ($scope.Nationality != 'null' && $scope.Nationality.length > 0) {
                        $scope.NationalityName = $scope.Nationality[findWithAttr($scope.Nationality, 'Key', $scope.PersonalData.Nationality)].Name;
                        $scope.NationalityName_OLD = $scope.Nationality[findWithAttr($scope.Nationality, 'Key', $scope.PersonalData_OLD.Nationality)].Name;
                    }
                    if ($scope.MaritalStatus != 'null' && $scope.MaritalStatus.length > 0) {
                        $scope.MaritalStatusName = $scope.MaritalStatus[findWithAttr($scope.MaritalStatus, 'Key', $scope.PersonalData.MaritalStatus)].Description;
                        $scope.MaritalStatusName_OLD = $scope.MaritalStatus[findWithAttr($scope.MaritalStatus, 'Key', $scope.PersonalData_OLD.MaritalStatus)].Description;
                    }
                    if ($scope.Religion != 'null' && $scope.Religion.length > 0) {
                        $scope.ReligionName = $scope.Religion[findWithAttr($scope.Religion, 'Key', $scope.PersonalData.Religion)].Description;
                        $scope.ReligionName_OLD = $scope.Religion[findWithAttr($scope.Religion, 'Key', $scope.PersonalData_OLD.Religion)].Description;
                    }
                    if ($scope.Country != 'null' && $scope.Country.length > 0) {
                        $scope.CountryName = $scope.Country[findWithAttr($scope.Country, 'CountryCode', $scope.PersonalData.BirthCity)].CountryName;
                        $scope.CountryName_OLD = $scope.Country[findWithAttr($scope.Country, 'CountryCode', $scope.PersonalData_OLD.BirthCity)].CountryName;
                    }
                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                });
            }

            function getProvinceSelectData(Code) {
                var oRequestParameter = {
                    InputParameter: { "Code": Code }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetProvinceByCountryCode';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.Province = response.data.Province;

                    if ($scope.Province != 'null' && $scope.Province.length > 0) {
                        $scope.BirthPlace = $scope.Province[findWithAttr($scope.Province, 'ProvinceCode', $scope.PersonalData.BirthPlace)].ProvinceName;
                    }
                    if ($scope.Province != 'null' && $scope.Province.length > 0) {
                        indexOfProvince = findWithAttr($scope.Province, 'ProvinceCode', $scope.PersonalData.BirthPlace);
                        if (indexOfProvince == undefined || indexOfProvince == 'null') {
                            $scope.BirthPlace = $scope.PersonalData.BirthPlace;
                        } else {
                            $scope.BirthPlace = $scope.Province[indexOfProvince].ProvinceName;
                        }
                    } else {
                        $scope.BirthPlace = $scope.PersonalData.BirthPlace;
                    }
                }, function errorCallback(response) {
                    // Error
                    console.log('error getProvinceSelectData.', response);
                });
            }

            function getProvinceSelectData_OLD(Code) {
                var oRequestParameter = {
                    InputParameter: { "Code": Code }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetProvinceByCountryCode';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.Province = response.data.Province;

                    if ($scope.Province != 'null' && $scope.Province.length > 0) {
                        indexOfProvince = findWithAttr($scope.Province, 'ProvinceCode', $scope.PersonalData_OLD.BirthPlace);
                        if (indexOfProvince == undefined || indexOfProvince == 'null') {
                            $scope.BirthPlace_OLD = $scope.PersonalData_OLD.BirthPlace;
                        } else {
                            $scope.BirthPlace_OLD = $scope.Province[indexOfProvince].ProvinceName;
                        }
                    } else {
                        $scope.BirthPlace_OLD = $scope.PersonalData_OLD.BirthPlace;
                    }
                }, function errorCallback(response) {
                    // Error
                    console.log('error getProvinceSelectData_OLD.', response);
                });
            }


            //#### OTHERS FUNCTION ### END
        }]);
})();