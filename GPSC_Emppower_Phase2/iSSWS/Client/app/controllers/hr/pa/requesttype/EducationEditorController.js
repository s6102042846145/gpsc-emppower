﻿(function () {
angular.module('ESSMobile')
    .controller('EducationEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {
        //#### FRAMEWORK FUNCTION ### START
        $scope.Textcategory = "HRPAPERSONALEDUCATION";
        $scope.EDUCATIONLEVEL = $scope.Text["EDUCATIONLEVEL"];
        $scope.INSTITUTE = $scope.Text["INSTITUTE"];
        $scope.COUNTRY = $scope.Text["COUNTRY"];
        $scope.CERTIFICATE = $scope.Text["CERTIFICATE"]; 
        $scope.TempBranch = null;

        console.log($scope.requesterData);
        //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
        $scope.ChildAction.SetData = function () {
            GetEducationSelectData();
        };

        //LoadData Function : Use to add some logic to Additional dataset before take any action
        $scope.ChildAction.LoadData = function () {
            //Do something ...
        };

        var employeeDate = getToken(CONFIG.USER);
        $scope.ChildAction.SetData();
        //#### FRAMEWORK FUNCTION ### END

        //#### OTHERS FUNCTION ### START 

        function getWithAttr(array, attr, value) {
            var objReturn = [];
            for (var i = 0; i < array.length; i += 1) {
                if (array[i][attr] === value) {
                    objReturn.push(array[i]);
                }
            }
            return objReturn;
        }

        function findWithAttr(array, attr, value) {
            for (var i = 0; i < array.length; i += 1) {
                if (array[i][attr] === value) {
                    return i;
                }
            }
        }

        function checkWithValue(array, attr, value) {
            var result = false;
            for (var i = 0; i < array.length; i += 1) {
                if (array[i][attr] === value) {
                    result = true;
                    break;
                }
            }
            return result;
        }

        $scope.EduLevelCode = $scope.document.Additional.Education.EducationLevelCode;

        function GetEducationSelectData() {
            // ------ get person data with employeeid and begindate -------//
            var oEmployeeData = getToken(CONFIG.USER);
            var checkDate = $filter('date')(new Date(), 'yyyy-MM-dd');
            var URL = CONFIG.SERVER + 'HRPA/GetEducationSelectData';
            var oRequestParameter = {
                InputParameter: { EducationLevelCode : $scope.document.Additional.Education.EducationLevelCode} 
                , CurrentEmployee: oEmployeeData
                , Requestor: $scope.requesterData
            };

            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
                //data: getToken(CONFIG.USER)
            }).then(function successCallback(response) {
                // Success
                $scope.Educationlevellist = response.data.educationlevel;
                $scope.Institutelist = response.data.institute;
                $scope.Countrylist = response.data.country;
                $scope.Certificatelist = response.data.certificate;
                $scope.Educationgrouplist = response.data.educationgroup;

                $scope.TempBranch = response.data.branch1;

                if ($scope.document.Additional.Education.EducationLevelCode != null && $scope.document.Additional.Education.EducationLevelCode != '') {

                    $scope.Branch1list = getWithAttr($scope.TempBranch, 'EducationLevelCode', $scope.document.Additional.Education.EducationLevelCode);
                    $scope.Branch2list = getWithAttr($scope.TempBranch, 'EducationLevelCode', $scope.document.Additional.Education.EducationLevelCode);
                }


                $scope.Educationlevellist.unshift({ EducationLevelCode: "", EducationLevelText: $scope.Text["SYSTEM"]["PLEASE_SELECT"], IsActive: true });

                $scope.Educationgrouplist.unshift({ EducationGroupCode: "", EducationGroupDesc: "" });
                if ($scope.Branch1list != null && $scope.Branch1list.length > 0) {
                    $scope.Branch1list.unshift({ EducationLevelCode: "", BranchCode: "", BranchText: "" });
                }
                if ($scope.Branch2list != null && $scope.Branch2list.length > 0) {
                    $scope.Branch2list.unshift({ EducationLevelCode: "", BranchCode: "", BranchText: "" });
                }

                if ($scope.Educationlevellist != null && $scope.Educationlevellist.length > 0) {
                    if ($scope.document.Additional.Education.EducationLevelCode == undefined || $scope.document.Additional.Education.EducationLevelCode == '') {
                        $scope.document.Additional.Education.EducationLevelCode = $scope.Educationlevellist[0].EducationLevelCode;
                        $scope.EducationLevelName = $scope.Educationlevellist[0].EducationLevelText;
                    } else {
                        if (checkWithValue($scope.Educationlevellist, 'EducationLevelCode', $scope.document.Additional.Education.EducationLevelCode)) {
                            $scope.EducationLevelName = $scope.Educationlevellist[findWithAttr($scope.Educationlevellist, 'EducationLevelCode', $scope.document.Additional.Education.EducationLevelCode)].EducationLevelText;
                        }
                        else
                        {

                        }
                    }
                }

                if ($scope.Institutelist != null && $scope.Institutelist.length > 0) {
                    if ($scope.document.Additional.Education.InstituteCode == undefined || $scope.document.Additional.Education.InstituteCode == '') {
                        $scope.document.Additional.Education.InstituteCode = $scope.Institutelist[0].InstituteCode;
                        $scope.InstitutelistName = $scope.Institutelist[0].InstituteText;
                    } else {
                        if (checkWithValue($scope.Institutelist, 'InstituteCode', $scope.document.Additional.Education.InstituteCode)) {
                            $scope.InstitutelistName = $scope.Institutelist[findWithAttr($scope.Institutelist, 'InstituteCode', $scope.document.Additional.Education.InstituteCode)].InstituteText;
                        }
                    }
                }

                if ($scope.Countrylist != null && $scope.Countrylist.length > 0) {
                    if ($scope.document.Additional.Education.CountryCode == undefined || $scope.document.Additional.Education.CountryCode == '') {
                        $scope.document.Additional.Education.CountryCode = $scope.Countrylist[0].CountryCode;
                        $scope.CountryName = $scope.Countrylist[0].CountryName;
                    } else {
                        if (checkWithValue($scope.Countrylist, 'CountryCode', $scope.document.Additional.Education.CountryCode)) {
                            $scope.CountryName = $scope.Countrylist[findWithAttr($scope.Countrylist, 'CountryCode', $scope.document.Additional.Education.CountryCode)].CountryName;
                        }
                    }
                }

                if ($scope.Certificatelist != null && $scope.Certificatelist.length > 0) {
                    if ($scope.document.Additional.Education.CertificateCode == undefined || $scope.document.Additional.Education.CertificateCode == '') {
                        $scope.document.Additional.Education.CertificateCode = $scope.Certificatelist[0].CertificateCode;
                        $scope.CertificateName = $scope.Certificatelist[0].CertificateDescription;
                    } else {
                        $scope.CertificateName = $scope.Certificatelist[findWithAttr($scope.Certificatelist, 'CertificateCode', $scope.document.Additional.Education.CertificateCode)].CertificateDescription;
                    }
                }

                if ($scope.Educationgrouplist != null && $scope.Educationgrouplist.length > 0) {
                    if ($scope.document.Additional.Education.EducationGroupCode == undefined || $scope.document.Additional.Education.EducationGroupCode == '') {
                        $scope.document.Additional.Education.EducationGroupCode = $scope.Educationgrouplist[0].EducationGroupCode;
                        $scope.EducationgroupName = $scope.Certificatelist[0].EducationGroupDesc;
                    } else {
                        if (checkWithValue($scope.Educationgrouplist, 'EducationGroupCode', $scope.document.Additional.Education.EducationGroupCode)) {
                            $scope.EducationgroupName = $scope.Educationgrouplist[findWithAttr($scope.Educationgrouplist, 'EducationGroupCode', $scope.document.Additional.Education.EducationGroupCode)].EducationGroupDesc;
                        }
                    }
                }

                if ($scope.Branch1list != null && $scope.Branch1list.length > 0) {
                    if ($scope.document.Additional.Education.Branch1 == undefined || $scope.document.Additional.Education.Branch1 == '') {
                        $scope.document.Additional.Education.Branch1 = $scope.Branch1list[0].BranchCode;
                        $scope.Branch1Name = $scope.Branch1list[0].BranchText;
                    } else {
                        if (checkWithValue($scope.Branch1list, 'BranchCode', $scope.document.Additional.Education.Branch1)) {
                            $scope.Branch1Name = $scope.Branch1list[findWithAttr($scope.Branch1list, 'BranchCode', $scope.document.Additional.Education.Branch1)].BranchText;
                        }
                    }
                }

                if ($scope.Branch2list != null && $scope.Branch2list.length > 0) {
                    if ($scope.document.Additional.Education.Branch2 == undefined || $scope.document.Additional.Education.Branch2 == '') {
                        $scope.document.Additional.Education.Branch2 = $scope.Branch2list[0].BranchCode;
                        $scope.Branch2Name = $scope.Branch2list[0].BranchText;
                    } else {
                        if (checkWithValue($scope.Branch2list, 'BranchCode', $scope.document.Additional.Education.Branch2)) {
                            $scope.Branch2Name = $scope.Branch2list[findWithAttr($scope.Branch2list, 'BranchCode', $scope.document.Additional.Education.Branch2)].BranchText;
                        }
                    }
                }

            }, function errorCallback(response) {
            });
        }

        $scope.getEducationLevel = function () {
            if ($scope.Educationlevellist != null && $scope.Educationlevellist.length > 0) {
                $scope.EducationLevelName = $scope.Educationlevellist[findWithAttr($scope.Educationlevellist, 'EducationLevelCode', $scope.document.Additional.Education.EducationLevelCode)].EducationLevelText;
            }
            $scope.Branch1list = null;
            $scope.Branch2list = null;
            if ($scope.document.Additional.Education.EducationLevelCode != null && $scope.document.Additional.Education.EducationLevelCode != '')
            {
                $scope.Branch1list = getWithAttr($scope.TempBranch, 'EducationLevelCode', $scope.document.Additional.Education.EducationLevelCode);
                $scope.Branch2list = getWithAttr($scope.TempBranch, 'EducationLevelCode', $scope.document.Additional.Education.EducationLevelCode);
                if ($scope.Branch1list != null && $scope.Branch1list.length > 0) {
                    $scope.Branch1list.unshift({ EducationLevelCode: "", BranchCode: "", BranchText: "" });
                }
                if ($scope.Branch2list != null && $scope.Branch2list.length > 0) {
                    $scope.Branch2list.unshift({ EducationLevelCode: "", BranchCode: "", BranchText: "" });
                }
                $scope.Branch1Name = $scope.Branch1list[0].BranchText;
                $scope.Branch2Name = $scope.Branch2list[0].BranchText;

                getCertificateByEducationLevel($scope.document.Additional.Education.EducationLevelCode);
            }
        }

        $scope.getInstitutelist = function () {
            if ($scope.Institutelist != null && $scope.Institutelist.length > 0) {       
                $scope.InstitutelistName = $scope.Institutelist[findWithAttr($scope.Institutelist, 'InstituteCode', $scope.document.Additional.Education.InstituteCode)].InstituteText;
            }
        }

        $scope.getCountry = function () {
            if ($scope.Countrylist != null && $scope.Countrylist.length > 0) {
                $scope.CountryName = $scope.Countrylist[findWithAttr($scope.Countrylist, 'CountryCode', $scope.document.Additional.Education.CountryCode)].CountryName;
            }
        }

        $scope.getCertificate = function () {
            if ($scope.Certificatelist != null && $scope.Certificatelist.length > 0) {
                $scope.CertificateName = $scope.Certificatelist[findWithAttr($scope.Certificatelist, 'CertificateCode', $scope.document.Additional.Education.CertificateCode)].CertificateDescription;
            }
        }

        $scope.setSelectedBeginDate = function (selectedDate) {
            $scope.document.Additional.Education.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
        };
        $scope.setSelectedEndDate = function (selectedDate) {
            $scope.document.Additional.Education.EndDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
        };

        $scope.getEducationgroup = function () {
            if ($scope.Educationgrouplist != null && $scope.Educationgrouplist.length > 0) {
                $scope.EducationgroupName = $scope.Educationgrouplist[findWithAttr($scope.Educationgrouplist, 'EducationGroupCode', $scope.document.Additional.Education.EducationGroupCode)].EducationGroupDesc;
            }
        }

        
        $scope.getBranch1 = function () {
            if ($scope.Branch1list != null && $scope.Branch1list.length > 0) {
                $scope.Branch1Name = $scope.Branch1list[findWithAttr($scope.Branch1list, 'BranchCode', $scope.document.Additional.Education.Branch1)].BranchText;
            }
        }

        $scope.getBranch2 = function () {
            if ($scope.Branch2list != null && $scope.Branch2list.length > 0) {
                $scope.Branch2Name = $scope.Branch2list[findWithAttr($scope.Branch2list, 'BranchCode', $scope.document.Additional.Education.Branch2)].BranchText;
            }
        }


        function getCertificateByEducationLevel(EducationLevel) {
            var oEmployeeData = getToken(CONFIG.USER);
            var URL = CONFIG.SERVER + 'HRPA/GetCertificateByEducationLevel';
            var oRequestParameter = {
                InputParameter: { 'EducationLevel': EducationLevel }
                , CurrentEmployee: oEmployeeData
                , Requestor: $scope.requesterData
            };

            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                $scope.Certificatelist = [];
                $scope.Certificatelist = response.data;

            }, function errorCallback(response) {
                $scope.name = 'error';
            });

        }

        //#### OTHERS FUNCTION ### END
    }]);
})();