﻿(function () {
    angular.module('ESSMobile')
        .controller('DeleteDelegationEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$q', '$timeout', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $q, $timeout) {
            
            console.log($scope.document);
            $scope.PATextcategory = 'DELEGATION';
            $scope.oRequestParameter = {
                InputParameter: {}
                , CurrentEmployee: $scope.employeeData
                , Requestor: $scope.requesterData
                , Creator: getToken(CONFIG.USER)
                , Language: $scope.employeeData.Language
            };


            $scope.ChildAction.SetData = function () {
                //GetCalendar();

            };

            $scope.ChildAction.LoadData = function () {

                //$scope.document.Additional.DELEGATE[0].AllEmployeePossibleToDelegated = null;
                //alert('a');
            };

            $scope.ChildAction.SetData();

        }]);
})();