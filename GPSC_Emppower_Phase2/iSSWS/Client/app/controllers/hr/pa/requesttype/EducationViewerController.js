﻿(function () {
angular.module('ESSMobile')
    .controller('EducationViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {
        //#### FRAMEWORK FUNCTION ### START
        $scope.Textcategory = "HRPAPERSONALEDUCATION";
        $scope.EDUCATIONLEVEL = $scope.Text["EDUCATIONLEVEL"];
        $scope.INSTITUTE = $scope.Text["INSTITUTE"];
        $scope.COUNTRY = $scope.Text["COUNTRY"];
        $scope.CERTIFICATE = $scope.Text["CERTIFICATE"]; 
        $scope.EDUCATIONGROUP = $scope.Text["EDUCATIONGROUP"];
        $scope.BRANCH = $scope.Text["BRANCH"]; 
        //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
        $scope.ChildAction.SetData = function () {
            //Do something ...
            $scope.Education = $scope.document.Additional.Education;
            $scope.EducationOld = $scope.document.Additional.EducationOld;

        };

        //LoadData Function : Use to add some logic to Additional dataset before take any action
        $scope.ChildAction.LoadData = function () {
            //Do something ...
            
        };

        var employeeDate = getToken(CONFIG.USER);
        $scope.ChildAction.SetData();
        //#### FRAMEWORK FUNCTION ### END

        //#### OTHERS FUNCTION ### START

        //Do something ...

        //#### OTHERS FUNCTION ### END
    }]);
})();