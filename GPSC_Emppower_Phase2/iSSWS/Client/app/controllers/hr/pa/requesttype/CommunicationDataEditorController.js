﻿(function () {
    angular.module('ESSMobile')
        .controller('CommunicationDataEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {
            //#### FRAMEWORK FUNCTION ### START
            $scope.PATextcategory = "PERSONALADMINISTRATION";
            $scope.content.Header = $scope.Text[$scope.PATextcategory].CONSOLE_HEADSUBJECT;
            $scope.employeeData = getToken(CONFIG.USER);
            $scope.CategoryCode;
            $scope.BeginDate;
            $scope.EndDate;
            $scope.DataText;       

            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
            $scope.ChildAction.SetData = function () {
                //Do something ...
                getContactSelectData();
            };

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                //Do something ...

            };

            $scope.ChildAction.SetData();
            //#### FRAMEWORK FUNCTION ### END

            //#### OTHERS FUNCTION ### START 

            //Do something ...
            function findWithAttr(array, attr, value) {
                for (var i = 0; i < array.length; i += 1) {
                    if (array[i][attr] === value) {
                        return i;
                    }
                }
            }


            function getContactSelectData() {
                var oRequestParameter = {
                    InputParameter: { }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetCommunicationData';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.ContData = response.data;
                    console.log('GetContactDataEdit.', $scope.ContactData)
                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                });
            }

            //#### OTHERS FUNCTION ### END
        }]);
})();