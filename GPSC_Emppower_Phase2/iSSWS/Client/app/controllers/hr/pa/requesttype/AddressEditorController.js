﻿(function () {
    angular.module('ESSMobile')
        .controller('AddressEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {
            //#### FRAMEWORK FUNCTION ### START
            $scope.PATextcategory = "HRPAPERSONALADDRESS";
            $scope.ADDRESSTYPE = $scope.Text["ADDRESSTYPE"];
            $scope.TEXTCOUNTRY = $scope.Text["COUNTRY"];
            $scope.content.Header = $scope.Text[$scope.PATextcategory].CONSOLE_HEADSUBJECT;
            $scope.employeeData = getToken(CONFIG.USER);
            $scope.CategoryCode;
            $scope.BeginDate;
            $scope.EndDate;
            $scope.DataText;
            $scope.Country;
            $scope.CountryName;
           
            $scope.CopyAddress = false;
            $scope.CopyHomeAddress = function (addrT) {
                $scope.CopyAddress = !$scope.CopyAddress;
                if ($scope.CopyAddress == true && addrT == '3') {

                    $scope.document.Additional.PersonalAddrNew[1].AddressNo = $scope.document.Additional.PersonalAddrNew[0].AddressNo;
                    $scope.document.Additional.PersonalAddrNew[1].Street = $scope.document.Additional.PersonalAddrNew[0].Street;
                    $scope.document.Additional.PersonalAddrNew[1].District = $scope.document.Additional.PersonalAddrNew[0].District;
                    $scope.document.Additional.PersonalAddrNew[1].Province = $scope.document.Additional.PersonalAddrNew[0].Province;
                    $scope.document.Additional.PersonalAddrNew[1].Country = $scope.document.Additional.PersonalAddrNew[0].Country;
                    $scope.document.Additional.PersonalAddrNew[1].Postcode = $scope.document.Additional.PersonalAddrNew[0].Postcode;
                    $scope.document.Additional.PersonalAddrNew[1].Telephone = $scope.document.Additional.PersonalAddrNew[0].Telephone;
                    //console.log($scope.CopyAddress + " " + addrT + " Click " + $scope.document.Additional.PersonalAddrNew[1].AddressNo);
                }
                else {
                    $scope.document.Additional.PersonalAddrNew[1].AddressNo = $scope.document.Additional.PersonalAddr[1].AddressNo;
                    $scope.document.Additional.PersonalAddrNew[1].Street = $scope.document.Additional.PersonalAddr[1].Street;
                    $scope.document.Additional.PersonalAddrNew[1].District = $scope.document.Additional.PersonalAddr[1].District;
                    $scope.document.Additional.PersonalAddrNew[1].Province = $scope.document.Additional.PersonalAddr[1].Province;
                    $scope.document.Additional.PersonalAddrNew[1].Country = $scope.document.Additional.PersonalAddr[1].Country;
                    $scope.document.Additional.PersonalAddrNew[1].Postcode = $scope.document.Additional.PersonalAddr[1].Postcode;
                    $scope.document.Additional.PersonalAddrNew[1].Telephone = $scope.document.Additional.PersonalAddr[1].Telephone;

                }
               
            };

            $scope.AddrType = $scope.document.Additional.PersonalAddressType[0].Key;

            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
            $scope.ChildAction.SetData = function () {
                //Do something ...
                getAllCountryData();
                getAddressSelectData();
            };

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                //Do something ...

            };

            $scope.ChildAction.SetData();
            //#### FRAMEWORK FUNCTION ### END

            //#### OTHERS FUNCTION ### START 

            //Do something ...

            function getAllCountryData() {
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetAllCountryData/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.Country = response.data.Country;

                    if ($scope.Country != 'null' && $scope.Country.length > 0) {
                        $scope.CountryName = $scope.Country[findWithAttr($scope.Country, 'CountryCode', $scope.document.Additional.PersonalAddrNew[0].Country)].CountryName;
                    }
                }, function errorCallback(response) {
                    // Error
                    console.log('error getAllCountryData.', response);
                });
            }

            function findWithAttr(array, attr, value) {
                for (var i = 0; i < array.length; i += 1) {
                    if (array[i][attr] === value) {
                        return i;
                    }
                }
            }

            function getAddressSelectData() {
                var oRequestParameter = {
                    InputParameter: { }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetPersonalAddress';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.PersonalAddr = response.data;

                    console.log('GetPersonalAddressEdit.', $scope.PersonalAddr);
                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                });
            }

            $scope.getCountry = function () {
                if ($scope.Country != 'null' && $scope.Country.length > 0) {
                    $scope.CountryName = $scope.Country[findWithAttr($scope.Country, 'CountryCode', $scope.document.Additional.PersonalAddrNew[0].Country)].CountryName;

                }
            };

          
           
            //#### OTHERS FUNCTION ### END
        }]);
})();