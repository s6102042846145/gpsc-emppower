﻿(function () {
angular.module('ESSMobile')
    .controller('CVErrorViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {
        //#### FRAMEWORK FUNCTION ### START
        //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
        $scope.PATextcategory = 'CVREPORT';
        $scope.EDUCATIONLEVEL = $scope.Text["EDUCATIONLEVEL"];
        $scope.INSTITUTE = $scope.Text["INSTITUTE"];
        $scope.COUNTRY = $scope.Text["COUNTRY"];
        $scope.CERTIFICATE = $scope.Text["CERTIFICATE"]; 
        $scope.BRANCH = $scope.Text["BRANCH"]; 
        $scope.ChildAction.SetData = function () {
            //Do something ...
        };

        //LoadData Function : Use to add some logic to Additional dataset before take any action
        $scope.ChildAction.LoadData = function () {
            //Do something ...
        };

        var employeeDate = getToken(CONFIG.USER);
        $scope.ChildAction.SetData();
        //#### FRAMEWORK FUNCTION ### END

        //#### OTHERS FUNCTION ### START

        //Do something ...

        //#### OTHERS FUNCTION ### END
    }]);
})();