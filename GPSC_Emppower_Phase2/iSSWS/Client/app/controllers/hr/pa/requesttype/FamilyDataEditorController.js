﻿(function () {
    angular.module('ESSMobile')
        .controller('FamilyDataEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {
            //#### FRAMEWORK FUNCTION ### START
            $scope.Textcategory = "FAMILY";
            $scope.FAMILYMEMBER = $scope.Text["FAMILYMEMBER"];

            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
            $scope.ChildAction.SetData = function () {
                //Do something ...
                getPAConfiguration();
                getFamilySelectData($scope.document.Additional.FamilyData_OLD.FamilyMember);
                $scope.document.Additional.FamilyData.BirthDate = $filter('date')($scope.document.Additional.FamilyData.BirthDate, 'yyyy-MM-dd');
                $scope.document.Additional.FamilyData.BeginDate = $filter('date')($scope.document.Additional.FamilyData.BeginDate, 'yyyy-MM-dd');
                $scope.document.Additional.FamilyData_OLD.BirthDate = $filter('date')($scope.document.Additional.FamilyData_OLD.BirthDate, 'yyyy-MM-dd');
                $scope.document.Additional.FamilyData_OLD.BeginDate = $filter('date')($scope.document.Additional.FamilyData_OLD.BeginDate, 'yyyy-MM-dd');

                $scope.ChildNoList = [];
                for (i = 1; i <= 10; i++) {
                    $scope.ChildNoList.push(i)
                }
            };

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                //Do something ...
                if ($scope.document.Additional.FamilyData.FamilyMember != '1') {
                    $scope.document.Additional.FamilyData.SpouseID = "";
                    $scope.document.Additional.FamilyData.MotherSpouseID = "";
                    $scope.document.Additional.FamilyData.FatherSpouseID = "";
                }
                if ($scope.document.Additional.FamilyData.FamilyMember != '11') {
                    $scope.document.Additional.FamilyData.FatherID = "";
                }
                if ($scope.document.Additional.FamilyData.FamilyMember != '12') {
                    $scope.document.Additional.FamilyData.MotherID = "";
                }

                if ($scope.document.Additional.FamilyData.FamilyMember != '2') {
                    if ($scope.document.Additional.FamilyData_OLD.ChildNo == undefined) {
                        $scope.document.Additional.FamilyData.ChildNo = "";
                    } else {
                        $scope.document.Additional.FamilyData.ChildNo = $scope.document.Additional.FamilyData_OLD.ChildNo;
                    }
                }

                if ($scope.document.Additional.FamilyData.Name == undefined) {
                    $scope.document.Additional.FamilyData.Name = "";
                }
                if ($scope.document.Additional.FamilyData.Surname == undefined) {
                    $scope.document.Additional.FamilyData.Surname = "";
                }
                if ($scope.document.Additional.FamilyData.CityOfBirth == undefined) {
                    $scope.document.Additional.FamilyData.CityOfBirth = "";
                }
                if ($scope.document.Additional.FamilyData.BirthPlace == undefined) {
                    $scope.document.Additional.FamilyData.BirthPlace = "";
                }
                if ($scope.document.Additional.FamilyData.Address == undefined) {
                    $scope.document.Additional.FamilyData.Address = "";
                }
                if ($scope.document.Additional.FamilyData.Street == undefined) {
                    $scope.document.Additional.FamilyData.Street = "";
                }
                if ($scope.document.Additional.FamilyData.District == undefined) {
                    $scope.document.Additional.FamilyData.District = "";
                }
                if ($scope.document.Additional.FamilyData.City == undefined) {
                    $scope.document.Additional.FamilyData.City = "";
                }
                if ($scope.document.Additional.FamilyData.Postcode == undefined) {
                    $scope.document.Additional.FamilyData.Postcode = "";
                }
                if ($scope.document.Additional.FamilyData.TelephoneNo == undefined) {
                    $scope.document.Additional.FamilyData.TelephoneNo = "";
                }
                if ($scope.document.Additional.FamilyData.FatherID == undefined) {
                    $scope.document.Additional.FamilyData.FatherID = "";
                }
                if ($scope.document.Additional.FamilyData.MotherID == undefined) {
                    $scope.document.Additional.FamilyData.MotherID = "";
                }
                if ($scope.document.Additional.FamilyData.SpouseID == undefined) {
                    $scope.document.Additional.FamilyData.SpouseID = "";
                }
                if ($scope.document.Additional.FamilyData.FatherSpouseID == undefined) {
                    $scope.document.Additional.FamilyData.FatherSpouseID = "";
                }
                if ($scope.document.Additional.FamilyData.MotherSpouseID == undefined) {
                    $scope.document.Additional.FamilyData.MotherSpouseID = "";
                }
                if ($scope.document.Additional.FamilyData.Dead == undefined) {
                    $scope.document.Additional.FamilyData.Dead = "";
                }

            };

            $scope.ChildAction.SetData();
            //#### FRAMEWORK FUNCTION ### END

            //#### OTHERS FUNCTION ### START 

            //Do something ...
            function getPAConfiguration() {
                var oRequestParameter = {
                    InputParameter: { "CategoryName": "Family" }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetPAConfigurationList/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.PAConfiguration = response.data;
                }, function errorCallback(response) {
                    // Error
                    console.log('error getPAConfiguration.', response);
                });
            }

            function findWithAttr(array, attr, value) {
                for (var i = 0; i < array.length; i += 1) {
                    if (array[i][attr] === value) {
                        return i;
                    }
                }
            }

            function getFamilySelectData(FamilyMember) {
                var oRequestParameter = {
                    InputParameter: { "FamilyMember": FamilyMember}
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetFamilySelectData';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.FamilyMember = response.data.FamilyMember;
                    $scope.Title = response.data.Title;
                    $scope.Country = response.data.Country;
                    $scope.Nationality = response.data.Nationality;

                    if ($scope.FamilyMember != 'null' && $scope.FamilyMember.length > 0) {
                        if ($scope.document.Additional.FamilyData.FamilyMember == undefined || $scope.document.Additional.FamilyData.FamilyMember == 'null' || $scope.document.Additional.FamilyData.FamilyMember == '') {
                            $scope.document.Additional.FamilyData.FamilyMember = $scope.FamilyMember[0].Key;
                            $scope.FamilyMemberName = $scope.FamilyMember[0].Description;
                        } else {
                            $scope.FamilyMemberName = $scope.FamilyMember[findWithAttr($scope.FamilyMember, 'Key', $scope.document.Additional.FamilyData.FamilyMember)].Description;
                        }
                    }
                    if ($scope.Title != 'null' && $scope.Title.length > 0) {
                        if ($scope.document.Additional.FamilyData.TitleName == undefined || $scope.document.Additional.FamilyData.TitleName == 'null') {
                            $scope.document.Additional.FamilyData.TitleName = $scope.Title[0].Key;
                            $scope.TitleName = $scope.Title[0].Description;
                        } else {
                            $scope.TitleName = $scope.Title[findWithAttr($scope.Title, 'Key', $scope.document.Additional.FamilyData.TitleName)].Description;
                        }
                    }
                    if ($scope.Country != 'null' && $scope.Country.length > 0) {
                        //$scope.CityOfBirthName = $scope.Country[findWithAttr($scope.Country, 'CountryCode', $scope.document.Additional.FamilyData.CityOfBirth)].CountryName;
                        if ($scope.document.Additional.FamilyData.CityOfBirth == undefined || $scope.document.Additional.FamilyData.CityOfBirth == 'null') {
                            $scope.document.Additional.FamilyData.CityOfBirth = $scope.Country[0].CountryCode;
                            $scope.CityOfBirthName = $scope.Country[0].CountryName;
                        } else {
                            $scope.CityOfBirthName = $scope.Country[findWithAttr($scope.Country, 'CountryCode', $scope.document.Additional.FamilyData.CityOfBirth)].CountryName;
                        }
                        //$scope.CountryName = $scope.Country[findWithAttr($scope.Country, 'CountryCode', $scope.document.Additional.FamilyData.Country)].CountryName;
                        if ($scope.document.Additional.FamilyData.Country == undefined || $scope.document.Additional.FamilyData.Country == 'null') {
                            $scope.document.Additional.FamilyData.Country = $scope.Country[0].CountryCode;
                            $scope.CountryName = $scope.Country[0].CountryName;
                        } else {
                            $scope.CountryName = $scope.Country[findWithAttr($scope.Country, 'CountryCode', $scope.document.Additional.FamilyData.Country)].CountryName;
                        }
                    }
                    if ($scope.Nationality != 'null' && $scope.Nationality.length > 0) {
                        //$scope.NationalityName = $scope.Nationality[findWithAttr($scope.Nationality, 'Key', $scope.document.Additional.FamilyData.Nationality)].Name;
                        if ($scope.document.Additional.FamilyData.Nationality == undefined || $scope.document.Additional.FamilyData.Nationality == 'null') {
                            $scope.document.Additional.FamilyData.Nationality = $scope.Nationality[0].Key;
                            $scope.NationalityName = $scope.Nationality[0].Name;
                        } else {
                            $scope.NationalityName = $scope.Nationality[findWithAttr($scope.Nationality, 'Key', $scope.document.Additional.FamilyData.Country)].Name;
                        }
                    }

                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                });
            }

            $scope.getFamilyMember = function () {
                if ($scope.FamilyMember != 'null' && $scope.FamilyMember.length > 0) {
                    $scope.FamilyMemberName = $scope.FamilyMember[findWithAttr($scope.FamilyMember, 'Key', $scope.document.Additional.FamilyData.FamilyMember)].Description;
                }
            }

            $scope.getTitle = function () {
                if ($scope.Title != 'null' && $scope.Title.length > 0) {
                    $scope.TitleName = $scope.Title[findWithAttr($scope.Title, 'Key', $scope.document.Additional.FamilyData.TitleName)].Description;;
                }
            }

            $scope.getNationality = function () {
                if ($scope.Nationality != 'null' && $scope.Nationality.length > 0) {
                    $scope.NationalityName = $scope.Nationality[findWithAttr($scope.Nationality, 'Key', $scope.document.Additional.FamilyData.Nationality)].Name;
                }
            }
            $scope.getCountry = function () {
                if ($scope.Country != 'null' && $scope.Country.length > 0) {
                    $scope.CountryName = $scope.Country[findWithAttr($scope.Country, 'CountryCode', $scope.document.Additional.FamilyData.Country)].CountryName;
                }
            }
            $scope.getCityOfBirth = function () {
                if ($scope.Country != 'null' && $scope.Country.length > 0) {
                    $scope.CityOfBirthName = $scope.Country[findWithAttr($scope.Country, 'CountryCode', $scope.document.Additional.FamilyData.CityOfBirth)].CountryName;
                }
            }

            $scope.setSelectedDOB = function (selectedDate) {
                $scope.document.Additional.FamilyData.BirthDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };
            $scope.setSelectedDeadDate = function (selectedDate) {
                $scope.document.Additional.FamilyData.MaritalEffectiveDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
                $scope.document.Additional.FamilyData.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };

            //#### OTHERS FUNCTION ### END
        }]);
})();