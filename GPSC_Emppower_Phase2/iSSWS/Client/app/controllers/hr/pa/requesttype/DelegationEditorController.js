﻿(function () {
    angular.module('ESSMobile')
        .controller('DelegationEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$q', '$timeout', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $q, $timeout) {

            $scope.oRequestParameter = {
                InputParameter: {}
                , CurrentEmployee: $scope.employeeData
                , Requestor: $scope.requesterData
                , Creator: getToken(CONFIG.USER)
                , Language: $scope.employeeData.Language
            };

            $scope.PATextcategory = 'DELEGATION';

            $scope.selectDelegatePosition;
            $scope.persons = {
                delegator: {},
                delegated: null
            };
            $scope.delegatedPersons = [];


            // $scope.persons.delegator = getToken(CONFIG.USER);  // ใช้ไม่ได้เพราะหากมีการสวมสิทธิ์ มันจะไม่ขึ้นต่ำแหน่งของคนส่วมสิทธิ์

            $scope.selectAll = function () {
                angular.forEach($scope.document.Additional.DELEGATE[0].DetailList, function (item, key) {
                    item.IsCheck = true;
                });
            };
            $scope.unSelectAll = function () {
                angular.forEach($scope.document.Additional.DELEGATE[0].DetailList, function (item, key) {
                    item.IsCheck = false;
                });
            };

            var oAllEmp = $scope.document.Additional.DELEGATE[0].AllEmployeePossibleToDelegated;
            var aAllEmp = [];
            for (var key in oAllEmp) {
                var arr = oAllEmp[key];
                for (var i = 0; i < arr.length; i++) {
                    arr[i].KEY_DELEGATED_POSITION = key;
                    aAllEmp.push(arr[i]);
                }
            }

            $scope.delegatedPersons = aAllEmp;

            $scope.setSelectedBegin = function (selectedDate) {
                $scope.document.Additional.DELEGATE[0].BeginDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
            };

            $scope.setSelectedEnd = function (selectedDate) {
                $scope.document.Additional.DELEGATE[0].EndDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
            };


            /********************  auto complete employee start ******************* */
            $scope.simulateQuery = true;
            $scope.isDisabled = false;
            $scope.querySearchEmployee = querySearchEmployee;
            $scope.selectedItemChange = selectedItemChange;
            $scope.searchTextChange = searchTextChange;
            $scope.newData = newData;
            $scope.autocomplete = {
                selectedItem: null
            };
            function newData(employee) {
            }
            function querySearchEmployee(query) {
                var delegatedPersons_filterd_by_position = $filter('filter')($scope.delegatedPersons, 
                    { KEY_DELEGATED_POSITION: $scope.selectDelegatePosition });
                var results = query ? delegatedPersons_filterd_by_position.filter(createFilterFor(query)) : delegatedPersons_filterd_by_position, deferred;
                if ($scope.simulateQuery) {
                    deferred = $q.defer();
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 250, false);
                    return deferred.promise;
                } else {
                    return results;
                }
            }
            function searchTextChange(text) {
            }


            function selectedItemChange(person) {
                if (angular.isDefined(person)) {
                    $scope.document.Additional.DELEGATE[0].DelegateTo = person.EmployeeID;
                    $scope.document.Additional.DELEGATE[0].DelegateToPosition = person.PositionID;
                    $scope.document.Additional.DELEGATE[0].DelegateToName = person.Name;

                    $scope.document.Additional.DELEGATE[0].DelegateToOrg = person.OrgUnit;
                    $scope.document.Additional.DELEGATE[0].DelegateToOrgName = person.OrgUnitName;
                    $scope.document.Additional.DELEGATE[0].DelegateToPositionName = person.Position;

                }
            }
            function createFilterFor(query) {

                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(employee) {
                    // text conbined to search
                    var textquery = employee.EmployeeID + ':' + employee.Name;
                    return (textquery.indexOf(lowercaseQuery) >= 0);
                };

            }

            var tempResult;
            $scope.tryToSelect = function (text) {
                for (var i = 0; i < $scope.delegatedPersons.length; i++) {
                    if (($scope.delegatedPersons[i].EmployeeID + ":" + $scope.delegatedPersons[i].Name) == text || $scope.delegatedPersons[i].Name == text) {
                        tempResult = $scope.delegatedPersons[i];
                        break;
                    }
                }
                $scope.searchText = '';
                $scope.autocomplete.selectedItem = null;
                $scope.persons.delegated = null;
            };


            $scope.checkText = function (text) {
                var result = null;
                for (var i = 0; i < $scope.delegatedPersons.length; i++) {
                    if (($scope.delegatedPersons[i].EmployeeID + ":" + $scope.delegatedPersons[i].Name) == text || $scope.delegatedPersons[i].Name == text) {
                        result = $scope.delegatedPersons[i];
                        break;
                    }
                }
                if (result) {
                    $scope.searchText = result.EmployeeID + ":" + result.Name;
                    $scope.selectedItemChange(result);
                    $scope.autocomplete.selectedItem = result;
                } else if (tempResult) {
                    $scope.searchText = tempResult.EmployeeID + ":" + tempResult.Name;
                    $scope.selectedItemChange(tempResult);
                    $scope.autocomplete.selectedItem = tempResult;
                }
            };
            $scope.checkText($scope.document.Additional.DELEGATE[0].DelegateToName);
            /********************  auto complete employee end ******************* */


            // Group Delegate
            $scope.selectAllGroup = function () {

                if ($scope.document.Additional.DELEGATE[0].DetailGroup.length > 0) {

                    angular.forEach($scope.document.Additional.DELEGATE[0].DetailGroup, function (group, key_group) {
                        group.IsCheckGroupping = true;
                        angular.forEach(group.GroupDetail, function (group_detil, key_group_detail) {
                            group_detil.IsCheckDetailGroupping = true;
                        });
                    });

                    $scope.checkSelectDelegateGroupDetailAll();
                }

            };

            $scope.unselectAllGroup = function () {

                if ($scope.document.Additional.DELEGATE[0].DetailGroup.length > 0) {

                    angular.forEach($scope.document.Additional.DELEGATE[0].DetailGroup, function (group, key_group) {
                        group.IsCheckGroupping = false;
                        angular.forEach(group.GroupDetail, function (group_detil, key_group_detail) {
                            group_detil.IsCheckDetailGroupping = false;
                        });
                    });

                    $scope.checkSelectDelegateGroupDetailAll();
                }
            };

            $scope.selectDelegateGroup = function (group,flag) {

                if (group.GroupDetail.length > 0) {

                    angular.forEach(group.GroupDetail, function (value, key) {

                        value.IsCheckDetailGroupping = flag;
                    });
                    $scope.checkSelectDelegateGroupDetailAll();
                }
            };

            $scope.selectDelegateDetail = function (group, detail, flag) {

                var i;
                if ($scope.document.Additional.DELEGATE[0].DetailGroup.length > 0) {

                    for (i = 0; i < $scope.document.Additional.DELEGATE[0].DetailGroup.length; i++) {

                        if (group.TextCodeGroupping === $scope.document.Additional.DELEGATE[0].DetailGroup[i].TextCodeGroupping) {

                            // Assign ค่าให้เพราะมัน Two way binding ไม่ทัน
                            angular.forEach($scope.document.Additional.DELEGATE[0].DetailGroup[i].GroupDetail, function (value, key) {

                                if (value.RequestTypeID === detail.RequestTypeID)
                                    value.IsCheckDetailGroupping = flag;
                            });

                            // Check ว่ามีการ Check ทั้งหมดแล้วหรอยัง
                            var is_check = false;
                            angular.forEach($scope.document.Additional.DELEGATE[0].DetailGroup[i].GroupDetail, function (value, key) {

                                if (value.IsCheckDetailGroupping)
                                    is_check = true;

                            });

                            if (is_check) {
                                $scope.document.Additional.DELEGATE[0].DetailGroup[i].IsCheckGroupping = true;
                            }
                            else {
                                $scope.document.Additional.DELEGATE[0].DetailGroup[i].IsCheckGroupping = false;
                            }

                            break;
                        }
                    }
                    $scope.checkSelectDelegateGroupDetailAll();
                }

            };

            $scope.checkSelectDelegateGroupDetailAll = function () {

                if ($scope.document.Additional.DELEGATE[0].DetailGroup.length > 0) {

                    angular.forEach($scope.document.Additional.DELEGATE[0].DetailGroup, function (value, key) {

                        angular.forEach(value.GroupDetail, function (value2, key2) {

                            // Assign flag ให้ model กว้าง
                            angular.forEach($scope.document.Additional.DELEGATE[0].DetailList, function (value_detail_list, key_detail_list) {

                                if (value_detail_list.RequestTypeID === value2.RequestTypeID) {
                                    value_detail_list.IsCheck = value2.IsCheckDetailGroupping;
                                }
                            });
                        });
                    });
                }
            };


            $scope.ChildAction.SetData = function () {
                //GetCalendar();
            };

            $scope.ChildAction.LoadData = function () {

                //$scope.document.Additional.DELEGATE[0].AllEmployeePossibleToDelegated = null;
                //alert('a');

            };

            $scope.ChildAction.SetData();



        }]);
})();