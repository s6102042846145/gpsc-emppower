﻿(function () {
angular.module('ESSMobile')
    .controller('CVErrorEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {

        $scope.PATextcategory = 'CVREPORT';      
        
        //#### FRAMEWORK FUNCTION ### START
        //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
        $scope.ChildAction.SetData = function () {
            //Do something ...
            //var actionDoc = angular.copy($scope.document);
        };

        //LoadData Function : Use to add some logic to Additional dataset before take any action
        $scope.ChildAction.LoadData = function () {
            //Do something ...
        };

        var employeeDate = getToken(CONFIG.USER);
        $scope.ChildAction.SetData();
        //#### FRAMEWORK FUNCTION ### END

        //#### OTHERS FUNCTION ### START 

        //Do something ...

        //#### OTHERS FUNCTION ### END
    }]);
})();