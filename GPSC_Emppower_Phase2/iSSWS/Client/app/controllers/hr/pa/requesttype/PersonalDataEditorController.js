﻿(function () {
    angular.module('ESSMobile')
        .controller('PersonalDataEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {
            //#### FRAMEWORK FUNCTION ### START
            $scope.Textcategory = "HRPAPERSONALDATA";
            $scope.Title;
            $scope.Gender;
            $scope.MaritalStatus;
            $scope.Nationality;
            $scope.Language;
            $scope.Religion;
            $scope.Country;
            $scope.Province;

            $scope.TitleName;
            $scope.GenderName;
            $scope.NationalityName;
            $scope.MaritalStatusName;
            $scope.ReligionName;
            $scope.CountryName;
            $scope.ProvinceName;

            $scope.IDCardNo;

            //SetData Function : Use to add some logic to Additional dataset before set to any control in screen
            $scope.ChildAction.SetData = function () {
                //Do something ...
                getPAConfiguration();
                getPersonSelectData();
                getIDCardData();
                getProvinceSelectData($scope.document.Additional.PersonalData.BirthCity);
                $scope.document.Additional.PersonalData.DOB = $filter('date')($scope.document.Additional.PersonalData.DOB, 'yyyy-MM-dd');
                $scope.document.Additional.PersonalData.BeginDate = $filter('date')($scope.document.Additional.PersonalData.BeginDate, 'yyyy-MM-dd');
                $scope.document.Additional.PersonalData.MaritalEffectiveDate = $filter('date')($scope.document.Additional.PersonalData.MaritalEffectiveDate, 'yyyy-MM-dd');
                $scope.document.Additional.PersonalData_OLD.DOB = $filter('date')($scope.document.Additional.PersonalData_OLD.DOB, 'yyyy-MM-dd');
                $scope.document.Additional.PersonalData_OLD.BeginDate = $filter('date')($scope.document.Additional.PersonalData_OLD.BeginDate, 'yyyy-MM-dd');
                $scope.document.Additional.PersonalData_OLD.MaritalEffectiveDate = $filter('date')($scope.document.Additional.PersonalData_OLD.MaritalEffectiveDate, 'yyyy-MM-dd');

                if ($scope.document.Additional.PersonalData.MaritalEffectiveDate == '0001-01-01') {
                    $scope.OldEffectiveDate = $scope.document.Additional.PersonalData.MaritalEffectiveDate;
                    $scope.document.Additional.PersonalData.MaritalEffectiveDate = null;
                    $scope.isCheckEffectiveDate = true;
                    $scope.isShowEffectiveDate = false;
                }
                else {
                    $scope.OldEffectiveDate = $scope.document.Additional.PersonalData.MaritalEffectiveDate;
                    $scope.isCheckEffectiveDate = false;
                    $scope.isShowEffectiveDate = true;
                }
            };

            $scope.checkEffectiveDate = function (chk) {
                if (chk) {
                    $scope.document.Additional.PersonalData.MaritalEffectiveDate = null;
                    $scope.isShowEffectiveDate = false;
                }
                else {
                    if ($scope.OldEffectiveDate == '0001-01-01') {
                        $scope.document.Additional.PersonalData.MaritalEffectiveDate = $filter('date')(new Date(), 'yyyy-MM-dd');
                    }
                    else {
                        $scope.document.Additional.PersonalData.MaritalEffectiveDate = $scope.OldEffectiveDate;
                    }
                    
                    $scope.isShowEffectiveDate = true;
                }
            };

            //LoadData Function : Use to add some logic to Additional dataset before take any action
            $scope.ChildAction.LoadData = function () {
                //Do something ...
                if ($scope.document.Additional.PersonalData.FirstName == undefined) {
                    $scope.document.Additional.PersonalData.FirstName = "";
                }
                if ($scope.document.Additional.PersonalData.LastName == undefined) {
                    $scope.document.Additional.PersonalData.LastName = "";
                }
                if ($scope.document.Additional.PersonalData.BirthCity == undefined) {
                    $scope.document.Additional.PersonalData.BirthCity = "";
                }
                if ($scope.document.Additional.PersonalData.BirthPlace == undefined) {
                    $scope.document.Additional.PersonalData.BirthPlace = "";
                }
            };

            $scope.ChildAction.SetData();
            //#### FRAMEWORK FUNCTION ### END

            //#### OTHERS FUNCTION ### START 

            //Do something ...
            function getPAConfiguration() {
                var oRequestParameter = {
                    InputParameter: { "CategoryName": "PersonalData,PersonalDataAdditional" }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetPAConfigurationList/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.PAConfiguration = response.data;
                }, function errorCallback(response) {
                    // Error
                    console.log('error getPAConfiguration.', response);
                });
            }


            function getIDCardData() {
                var oEmployeeData = getToken(CONFIG.USER);
                var URL = CONFIG.SERVER + 'HRPA/GetIDCardData';
                var oRequestParameter = {
                    InputParameter: { "EmployeeID": $scope.document.Additional.PersonalData.EmployeeID }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.IDCardNo = response.data;
                }, function errorCallback(response) {
                });
            }

            function findWithAttr(array, attr, value) {
                for (var i = 0; i < array.length; i += 1) {
                    if (array[i][attr] === value) {
                        return i;
                    }
                }
            }

            function getPersonSelectData() {
                var oRequestParameter = {
                    InputParameter: { }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetPersonSelectData/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.Title = response.data.Title;
                    $scope.Gender = response.data.Gender;
                    $scope.MaritalStatus = response.data.MaritalStatus;
                    $scope.Nationality = response.data.Nationality;
                    $scope.Language = response.data.Language;
                    $scope.Religion = response.data.Religion;
                    $scope.Country = response.data.Country;

                    if ($scope.Title != 'null' && $scope.Title.length > 0) {
                        $scope.TitleName = $scope.Title[findWithAttr($scope.Title, 'Key', $scope.document.Additional.PersonalData.TitleID)].Description;;
                    }
                    if ($scope.Gender != 'null' && $scope.Gender.length > 0) {
                        $scope.GenderName = $scope.Gender[findWithAttr($scope.Gender, 'Key', $scope.document.Additional.PersonalData.Gender)].Description;
                    }
                    if ($scope.Nationality != 'null' && $scope.Nationality.length > 0) {
                        $scope.NationalityName = $scope.Nationality[findWithAttr($scope.Nationality, 'Key', $scope.document.Additional.PersonalData.Nationality)].Name;
                    }
                    if ($scope.MaritalStatus != 'null' && $scope.MaritalStatus.length > 0) {
                        $scope.MaritalStatusName = $scope.MaritalStatus[findWithAttr($scope.MaritalStatus, 'Key', $scope.document.Additional.PersonalData.MaritalStatus)].Description;
                    }
                    if ($scope.Religion != 'null' && $scope.Religion.length > 0) {
                        $scope.ReligionName = $scope.Religion[findWithAttr($scope.Religion, 'Key', $scope.document.Additional.PersonalData.Religion)].Description;
                    }
                    if ($scope.Country != 'null' && $scope.Country.length > 0) {
                        $scope.CountryName = $scope.Country[findWithAttr($scope.Country, 'CountryCode', $scope.document.Additional.PersonalData.BirthCity)].CountryName;
                    }
                }, function errorCallback(response) {
                    // Error
                    console.log('error getPersonSelectData.', response);
                });
            }

            $scope.getTitle = function () {
                if ($scope.Title != 'null' && $scope.Title.length > 0) {
                    $scope.TitleName = $scope.Title[findWithAttr($scope.Title, 'Key', $scope.document.Additional.PersonalData.TitleID)].Description;;
                }
            }
            $scope.getGender = function () {
                if ($scope.Gender != 'null' && $scope.Gender.length > 0) {
                    $scope.GenderName = $scope.Gender[findWithAttr($scope.Gender, 'Key', $scope.document.Additional.PersonalData.Gender)].Description;
                }
            }
            $scope.getNationality = function () {
                if ($scope.Nationality != 'null' && $scope.Nationality.length > 0) {
                    $scope.NationalityName = $scope.Nationality[findWithAttr($scope.Nationality, 'Key', $scope.document.Additional.PersonalData.Nationality)].Name;
                }
            }
            $scope.getMaritalStatus = function () {
                if ($scope.MaritalStatus != 'null' && $scope.MaritalStatus.length > 0) {
                    $scope.MaritalStatusName = $scope.MaritalStatus[findWithAttr($scope.MaritalStatus, 'Key', $scope.document.Additional.PersonalData.MaritalStatus)].Description;
                }
            }
            $scope.getReligion = function () {
                if ($scope.Religion != 'null' && $scope.Religion.length > 0) {
                    $scope.ReligionName = $scope.Religion[findWithAttr($scope.Religion, 'Key', $scope.document.Additional.PersonalData.Religion)].Description;
                }
            }
            $scope.getCountry = function () {
                if ($scope.Country != 'null' && $scope.Country.length > 0) {
                    $scope.CountryName = $scope.Country[findWithAttr($scope.Country, 'CountryCode', $scope.document.Additional.PersonalData.BirthCity)].CountryName;

                    oRequestParameter = {
                        InputParameter: { "Code": $scope.document.Additional.PersonalData.BirthCity }
                        , CurrentEmployee: getToken(CONFIG.USER)
                        , Requestor: $scope.requesterData
                    };
                    var URL = CONFIG.SERVER + 'HRPA/GetProvinceByCountryCode';
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        $scope.Province = response.data.Province;

                        if ($scope.Province != 'null' && $scope.Province.length > 0) {
                            $scope.IsVisibleProvince = true;
                            indexOfProvince = findWithAttr($scope.Province, 'ProvinceCode', $scope.document.Additional.PersonalData.BirthPlace);
                            if (indexOfProvince == undefined || indexOfProvince == 'null') {
                                $scope.document.Additional.PersonalData.BirthPlace = $scope.Province[0].ProvinceCode;
                                $scope.ProvinceName = $scope.Province[0].ProvinceName;
                            } else {
                                $scope.ProvinceName = $scope.Province[indexOfProvince].ProvinceName;
                            }
                        } else {
                            $scope.IsVisibleProvince = false;
                            $scope.document.Additional.PersonalData.BirthPlace = '';
                            $scope.ProvinceName = '';
                        }
                    }, function errorCallback(response) {
                        // Error
                        console.log('error getProvinceSelectData.', response);
                    });
                }
            }
            $scope.getProvince = function () {
                if ($scope.Province != 'null' && $scope.Province.length > 0) {
                    $scope.ProvinceName = $scope.Province[findWithAttr($scope.Province, 'ProvinceCode', $scope.document.Additional.PersonalData.BirthPlace)].ProvinceName;
                }
            }
            $scope.setSelectedDOB = function (selectedDate) {
                $scope.document.Additional.PersonalData.DOB = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };
            $scope.setSelectedEffectiveDate = function (selectedDate) {
                $scope.document.Additional.PersonalData.MaritalEffectiveDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };
            $scope.setSelectedBeginDate = function (selectedDate) {
                $scope.document.Additional.PersonalData.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };

            function getProvinceSelectData(Code) {
                var oRequestParameter = {
                    InputParameter: { "Code": Code }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetProvinceByCountryCode';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.Province = response.data.Province;

                    if ($scope.Province != 'null' && $scope.Province.length > 0) {
                        $scope.IsVisibleProvince = true;
                        indexOfProvince = findWithAttr($scope.Province, 'ProvinceCode', $scope.document.Additional.PersonalData.BirthPlace);
                        if (indexOfProvince == undefined || indexOfProvince == 'null') {
                            $scope.document.Additional.PersonalData.BirthPlace = $scope.Province[0].ProvinceCode;
                            $scope.ProvinceName = $scope.Province[0].ProvinceName;
                        } else {
                            $scope.ProvinceName = $scope.Province[indexOfProvince].ProvinceName;
                        }
                    } else {
                        $scope.IsVisibleProvince = false;
                    }
                }, function errorCallback(response) {
                    // Error
                    console.log('error getProvinceSelectData.', response);
                });
            }
            //#### OTHERS FUNCTION ### END
        }]);
})();