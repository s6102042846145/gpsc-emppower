﻿(function () {
    angular.module('ESSMobile')
        .controller('PersonalDriverLicenseController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal) {

            //#### FRAMEWORK FUNCTION ### START

            //#### FRAMEWORK FUNCTION ### END

            //#### OTHERS FUNCTION ### START

            //Do something ...

            //#### OTHERS FUNCTION ### END
            $scope.profile = getToken(CONFIG.USER);
            $scope.model = {
                EffectiveDate: "0001-01-01T00:00:00",
                EmployeeID: null,
                ExpireDate: "0001-01-01T00:00:00",
                FullNameEN: null,
                FullNameTH: null,
                ItemID: 0,
                LicenseID: null
            };

            // Load DriverLicense
            $scope.loadPersonalDriverLicense = function () {

                var URL = CONFIG.SERVER + 'HRPA/GetDriverLicenseByEmployee';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data.ItemID <= 0) {
                        $scope.openModal();

                        $scope.model = {
                            EffectiveDate: "0001-01-01T00:00:00",
                            EmployeeID: null,
                            ExpireDate: "0001-01-01T00:00:00",
                            FullNameEN: null,
                            FullNameTH: null,
                            ItemID: 0,
                            LicenseID: null
                        };
                    }
                    else {
                        $scope.model = response.data;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });

            };
            $scope.loadPersonalDriverLicense();

            // Text Warning Employee
            $scope.openModal = function () {
                var modalInstance = $uibModal.open({
                    animation: $scope.animationsEnabled,
                    templateUrl: 'DriverLicenseError.ng-popup.html',
                    controller: 'DriverLicenseErrorController',
                    backdrop: 'static',
                    keyboard: false,
                    size: 'sp', 
                    resolve: {
                        items: function () {
                            var data = {
                                text: $scope.Text
                            };
                            return data;
                        }
                    }
                });
            };

        }])
        .controller('DriverLicenseErrorController', ['items', '$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', '$uibModalInstance', function (items, $scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal, $uibModalInstance) {

            $scope.closeModal = function () {
                $uibModalInstance.close();
            };
        }]);
})();