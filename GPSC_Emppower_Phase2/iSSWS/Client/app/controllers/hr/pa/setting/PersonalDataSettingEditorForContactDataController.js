﻿(function () {
    angular.module('ESSMobile')
        .controller('PersonalDataSettingEditorForContactDataController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {
            $scope.PATextcategory = 'PERSONALADMINISTRATION';
            $scope.content.Header = $scope.Text[$scope.PATextcategory].CONSOLE_HEADSUBJECT;
            $scope.SubType = [];
            $scope.Path = "views/hr/pa/setting/partial/ContactEditor.html";
            $scope.ShowEmployeeID = true;
            $scope.CanEditPeriod = false;
            $scope.itemKey = angular.isUndefined($routeParams.itemKey) && $routeParams.itemKey != '0' ? 'null' : $routeParams.itemKey.split('|')[0];
            $scope.employeeData = getToken(CONFIG.USER);
            $scope.BeginDate = angular.isUndefined($routeParams.itemKey) && $routeParams.itemKey != '0' ? 'null' : $filter('date')($routeParams.itemKey.split('|')[1], 'yyyy-MM-dd');
            $scope.SubTypeDefult = angular.isUndefined($routeParams.itemKey) && $routeParams.itemKey != '0' ? 'null' : $routeParams.itemKey.split('|')[2];
            $scope.ContactData;
            $scope.NoSpecify = {
                val: false
            };


            $scope.getDateFormate = function (date) {
                return $filter('date')(date, 'd/M/yyyy');
            }

            $scope.getDate = function (date) {
                var oDate = new Date(date);
                return oDate;
            }

            $scope.getDateNew = function (date) {
                var oDate = new Date(date);
                return oDate;
            }

            $scope.setSelectedBeginDate = function (selectedDate) {
                $scope.ContactData.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };
            $scope.setSelectedEndDate = function (selectedDate) {
                $scope.ContactData.EndDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
                console.log(($scope.ContactData.EndDate == '9999-12-31'));
                $scope.NoSpecify.val = ($scope.ContactData.EndDate == '9999-12-31');
            };
            $scope.onCheckBoxChange = function () {

                if ($scope.NoSpecify.val == true) {
                    $scope.ContactData.EndDate = '9999-12-31';
                }
                else {
                    $scope.ContactData.EndDate = $scope.ContactData.BeginDate;
                }
            }

            $scope.OnClickAddContact = function () {
                var ContactDataObj = {
                    EmployeeID: $scope.itemKey,
                    BeginDate: $filter('date')(new Date(), 'yyyy-MM-ddT00:00:00'),
                    EndDate: '9999-12-31T00:00:00',
                    NoSpecify: true,
                    CategoryCode: '',
                    DataText: ''
                };
                $scope.ContactData.push(ContactDataObj);
            }
            $scope.OnClickDeleteContact = function (index) {
                $scope.ContactData.splice(index, 1);
            };
            $scope.setSelectedResignDate = function (selectedDate) {
                $scope.ResignDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };
            $scope.onClickCancel = function () {
                $location.path('/frmViewContent/0/');
            }

            //$scope.onClickSave = function (ev) {
            //    if (validate()) {

            //        //********************
            //        $scope.onClickEditSave();
            //        //********************

            //        var DateArr = sortPeriod();
            //        $scope.OrgDataIntegrated = [];
            //        INFOTYPEIntegration(DateArr);
            //        $scope.OrgData = angular.copy($scope.OrgDataIntegrated);
            //        InsertINFOTYPEAllForEmployee(ev);
            //    }
            //    else {
            //        $mdDialog.show(
            //                    $mdDialog.alert()
            //                        .clickOutsideToClose(true)
            //                        .title($scope.Text['PERSONALADMINISTRATION'].EXCEPTION)
            //                        .textContent($scope.Text['PERSONALADMINISTRATION'][$scope.ErrorMessage])
            //                        .ariaLabel($scope.Text['PERSONALADMINISTRATION'].EXCEPTION)
            //                        .ok($scope.Text['SYSTEM'].BUTTON_OK)
            //                        .targetEvent(ev));
            //    }
            //};

            //function sortPeriod() {
            //    var DateArr = [];
            //    var tempPersonData = angular.copy($scope.PersonData);
            //    var tempOrgData = angular.copy($scope.OrgData);
            //    var tempPositionData = angular.copy($scope.PositionData.filter(Is100()));
            //    for (var k = 0; k < tempOrgData.length; k++) {
            //        if (!DateArr.DupDate(new Date(tempOrgData[k].BeginDate.setHours(0))))
            //            DateArr.push(tempOrgData[k].BeginDate);
            //        if (!DateArr.DupDate(new Date(tempOrgData[k].EndDate.setHours(0))))
            //            DateArr.push(tempOrgData[k].EndDate);
            //    }

            //    for (var k = 0; k < tempPositionData.length; k++) {
            //        if (!DateArr.DupDate(new Date(tempPositionData[k].BeginDate.setHours(0))))
            //            DateArr.push(tempPositionData[k].BeginDate);
            //        if (!DateArr.DupDate(new Date(tempPositionData[k].EndDate.setHours(0))))
            //            DateArr.push(tempPositionData[k].EndDate);
            //    }

            //    for (var k = 0; k < tempPersonData.length; k++) {
            //        if (!DateArr.DupDate(new Date(tempPersonData[k].BeginDate.setHours(0))))
            //            DateArr.push(tempPersonData[k].BeginDate);
            //        if (!DateArr.DupDate(new Date(tempPersonData[k].EndDate.setHours(0))))
            //            DateArr.push(tempPersonData[k].EndDate);
            //    }
            //    return DateArr.sort(function (a, b) { return b < a });
            //}

            //function validate() {
            //    $scope.ErrorMessage = 'PLEASE_INSERT_ALL_FIELDS';
            //    $scope.ContactDataCopy = [];
            //    var oReturn = true;

            //    for (var i = 0; i < $scope.ContactData.length; i++) {
            //        if (!$scope.ContactData[i].CategoryCode) { return false; }
            //        else if (!$scope.ContactData[i].DataText) { return false; }
            //        else if ($scope.ContactDataCopy.length == 0)
            //            $scope.ContactDataCopy.push(angular.copy($scope.ContactData[i]));
            //        else if (checkDupContactData($scope.ContactData[i])) {
            //            return false;
            //        }
            //    }
            //    return true;
            //}
            //function checkDupContactData(obj) {
            //    for (var j = 0; j < $scope.ContactDataCopy.length; j++) {
            //        if (((obj.BeginDate <= $scope.ContactDataCopy[j].EndDate && obj.BeginDate >= $scope.ContactDataCopy[j].BeginDate)
            //            || ($scope.ContactDataCopy[j].BeginDate <= obj.EndDate && $scope.ContactDataCopy[j].BeginDate >= obj.BeginDate))
            //            && $scope.ContactDataCopy[j].CategoryCode == obj.CategoryCode) {
            //            $scope.ErrorMessage = 'CONTRACT_DUPLICATE_PERIOD';
            //            return true;
            //        }
            //    }
            //    $scope.ContactDataCopy.push(angular.copy(obj));
            //    return false;
            //}

            function InsertINFOTYPEAllForEmployee(ev) {
                // ------ insert data to database -------//
                var oRequestParameter = {
                    InputParameter: { "EmployeeID": $scope.itemKey, "PersonData": $scope.PersonData, "OrgData": $scope.OrgData, "ContData": $scope.ContactData, "PositionData": $scope.PositionData }
                , CurrentEmployee: getToken(CONFIG.USER)
                };
                var URL = CONFIG.SERVER + 'Employee/InsertINFOTYPEAllForEmployee/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $mdDialog.show(
                                $mdDialog.alert()
                                    .clickOutsideToClose(true)
                                    .title($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                    .textContent($scope.Text['ACCOUNT_SETTING'].GO_BACK)
                                    .ariaLabel($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                    .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                    .targetEvent(ev));
                    $scope.PersonData.EmployeeID = response.data;
                    $location.path('/frmViewContent/0/');

                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                });
                // ------ ------------------------------------ -------//
            }

            $scope.onClickEditSave = function () {
                var oReturn = true;

                for (var i = 0; i < $scope.ContactData.length; i++) {
                    $scope.ContactData[i].Name = 'z';
                    //$scope.ContactData[i].BeginDate = '2015-10-01';
                    //$scope.ContactData[i].EndDate = '9999-12-31';

                }


                //if ($scope.ContactData.CategoryCode.trim().length <= 0) oReturn = false;
                //if ($scope.ContactData.DataText.trim().length <= 0) oReturn = false;
                if (oReturn == true) {
                    console.log($scope.ContactData);
                    // ------ update data to database -------//
                    var oRequestParameter = {
                        InputParameter: { "ContData": $scope.ContactData[0] }
                                                , CurrentEmployee: getToken(CONFIG.USER)
                    };
                    var URL = CONFIG.SERVER + 'Employee/UpdateINFOTYPE0105';
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        alert('COMPLETE');
                        $location.path('/frmViewContent/409/' + $scope.itemKey);
                    }, function errorCallback(response) {
                        // Error
                        console.log('error RequestController.', response);
                    });
                    // ------ ------------------------------------ -------//
                }
            };
            $scope.onClickAddSave = function () {
                var oReturn = true;
                if ($scope.ContactData.Name.trim().length <= 0) oReturn = false;
                if ($scope.ContactData.CategoryCode.trim().length <= 0) oReturn = false;
                if ($scope.ContactData.DataText.trim().length <= 0) oReturn = false;
                if (oReturn == true) {
                    console.log($scope.ContactData);
                    var oRequestParameterr = {
                        InputParameter: { "EmployeeID": $scope.employeeData.EmployeeID, "DataObjectType": "Contact", "BeginDate": $scope.ContData.BeginDate.toString('yyyy-MM-dd'), "EndDate": $scope.ContData.EndDate, "CategoryCode": $scope.ContData.CategoryCode }
                                                , CurrentEmployee: getToken(CONFIG.USER)
                    };
                    var urlll = CONFIG.SERVER + 'Employee/CheckStatus';
                    $http({
                        method: 'POST',
                        url: urlll,
                        data: oRequestParameterr
                    }).then(function successCallback(response) {
                        // Success
                        console.log('eiei', response);
                        var oRequestParameter = {
                            InputParameter: { "EmployeeID": $scope.employeeData.EmployeeID, "ContactData": $scope.ContactData }
                                                , CurrentEmployee: getToken(CONFIG.USER)
                        };
                        var insertURL = CONFIG.SERVER + 'Employee/InsertINFOTYPE0105';
                        if (response.data.CanInsert == true && response.data.Duplicate == true) {
                            if (confirm($scope.Text['PERSONALADMINISTRATION'].DuplicatePeriod) == true) {
                                InsertINFOTYPE0105(oRequestParameter, insertURL);
                            }
                        }
                        else if (response.data.CanInsert == true) {
                            InsertINFOTYPE0105(oRequestParameter, insertURL);
                        }
                        else {
                            alert($scope.Text['PERSONALADMINISTRATION'].StaccatoPeriod);
                        }
                    }, function errorCallback(response) {
                        // Error
                        console.log('error RequestController.', response);
                    });
                }
            };
            if ($scope.BeginDate != null) {
                //if ($routeParams.itemKey.split('|')[3] != null) { $scope.CanEditPeriod = true; }
                $scope.CanEditPeriod = true;
                $scope.EditMode = true;
                // ------ get person data with employeeid and begindate -------//
                var oRequestParameter = {
                    InputParameter: { "EmployeeID": $scope.employeeData.EmployeeID, "CategoryCode": "" }
                                            , CurrentEmployee: getToken(CONFIG.USER)
                };
                var URL = CONFIG.SERVER + 'HRPA/INFOTYPE0105GetByCategoryCode';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.ContactData = response.data;
                    console.log('GetContactDataEdit.', $scope.ContactData)
                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                });
                // ------ ------------------------------------ -------//
            }
            else {
                var date = new Date();
                var dateStr = date.getFullYear() + '-' + ("0" + (date.getMonth() + 1)).slice(-2) + '-' + ("0" + date.getDate()).slice(-2);
                $scope.ContactData = {
                    EmployeeID: $scope.employeeData.EmployeeID,
                    BeginDate: dateStr,
                    EndDate: '9999-12-31',
                    Name: '',
                    SubType: '',
                    DataText: ''
                };
                var oRequestParameter = {
                    InputParameter: { "EmployeeID": $scope.employeeData.EmployeeID, "BeginDate": $scope.BeginDate, "SubType": $scope.SubTypeDefult }
                                            , CurrentEmployee: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER)
                };
                var URL = CONFIG.SERVER + 'Employee/INFOTYPE0001GetAllHistory';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.ContactData.Name = response.data[0].Name;
                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                });
                $scope.NoSpecify.val = ($scope.ContactData.EndDate == '9999-12-31');
                $scope.CanEditPeriod = true;
            }
            // ------ get all select data -------//
            var oRequestParameter = {
                InputParameter: {}
                , CurrentEmployee: getToken(CONFIG.USER)
            };
            var URL = CONFIG.SERVER + 'Employee/GetContactSelectData';
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                console.log('select-datana.', response.data);
                $scope.SubType = response.data.SubType;
            }, function errorCallback(response) {
                // Error
                console.log('error RequestController.', response);
            });
            // ------ ------------------------------------ -------//

            function InsertINFOTYPE0105(oRequestParameter, URL) {
                // ------ insert data to database -------//
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    alert('COMPLETE');
                    $location.path('/frmViewContent/409/' + oRequestParameter.InputParameter["EmployeeID"]);
                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                });
                // ------ ------------------------------------ -------//
            }
            console.log('PersonalDataSettingEditorForContactDataController.');

        }]);
})();