﻿(function () {
    angular.module('ESSMobile')
        .controller('PersonalDataSettingEditorForOrganizationDataController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {
            $scope.PATextcategory = 'PERSONALADMINISTRATION';
            $scope.content.Header = $scope.Text[$scope.PATextcategory].CONSOLE_HEADSUBJECT;
            $scope.EmpGroupTextcategory = 'EMPGROUP';
            $scope.EmpGroup = [];
            $scope.EmpSubGroupTextcategory = 'EMPSUBGROUP';
            $scope.EmpSubGroup = [];
            $scope.CostCenterSelector = [];
            $scope.PositionSelector = [];
            $scope.OrgUnitSelector = [];
            $scope.AreaSelector = [];
            $scope.SubAreaSelector = [];
            $scope.SubAreaObject;
            $scope.Path = "views/hr/pa/setting/partial/OrganizationEditor.html";
            $scope.ShowEmployeeID = true;
            $scope.CanEditPeriod = false;
            $scope.itemKey = angular.isUndefined($routeParams.itemKey) && $routeParams.itemKey != '0' ? 'null' : $routeParams.itemKey.split('|')[0];
            $scope.BeginDate = angular.isUndefined($routeParams.itemKey) && $routeParams.itemKey != '0' ? 'null' : $filter('date')($routeParams.itemKey.split('|')[1], 'yyyy-MM-dd');
            $scope.OrgData;
            $scope.onSelectedArea = function (ObjectID) {
                $scope.SubAreaSelector = $scope.SubAreaObject[ObjectID];
            }
            $scope.NoSpecify = {
                val: false
            };
            $scope.setSelectedBeginDate = function (selectedDate) {
                $scope.OrgData.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };
            $scope.setSelectedEndDate = function (selectedDate) {
                $scope.OrgData.EndDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
                console.log(($scope.OrgData.EndDate == '9999-12-31'));
                $scope.NoSpecify.val = ($scope.OrgData.EndDate == '9999-12-31');
            };
            $scope.onCheckBoxChange = function () {

                if ($scope.NoSpecify.val == true) {
                    $scope.OrgData.EndDate = '9999-12-31';
                } else {
                    $scope.OrgData.EndDate = $scope.OrgData.BeginDate;
                }
            }

            $scope.onClickCancel = function () {
                $location.path('/frmViewContent/409/' + $scope.itemKey);
            }

            $scope.onClickEditSave = function () {
                var oReturn = true;
                if ($scope.OrgData.Name.trim().length <= 0) oReturn = false;
                if ($scope.OrgData.Area.trim().length <= 0) oReturn = false;
                if ($scope.OrgData.SubArea.trim().length <= 0) oReturn = false;
                if ($scope.OrgData.EmpGroup.trim().length <= 0) oReturn = false;
                if ($scope.OrgData.EmpSubGroup.trim().length <= 0) oReturn = false;
                if ($scope.OrgData.OrgUnit.trim().length <= 0) oReturn = false;
                if ($scope.OrgData.CostCenter.trim().length <= 0) oReturn = false;
                if ($scope.OrgData.Position.trim().length <= 0) oReturn = false;
                if (oReturn == true) {
                    var oRequestParameter = {
                        InputParameter: { "EmployeeID": $scope.itemKey, "OrgData": $scope.OrgData }
                                                , CurrentEmployee: getToken(CONFIG.USER)
                    };
                    // ------ update data to database -------//
                    var URL = CONFIG.SERVER + 'Employee/UpdateINFOTYPE0001';
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        alert('COMPLETE');
                        $location.path('/frmViewContent/409/' + $scope.itemKey);
                    }, function errorCallback(response) {
                        // Error
                        console.log('error RequestController.', response);
                    });
                    // ------ ------------------------------------ -------//
                }
            };

            $scope.getPosition = function (Organize) {
                var URL = CONFIG.SERVER + 'Employee/GetPositionByOrganizeForHRMaster';
                var oRequestParameter = { InputParameter: { "Organize": Organize }, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.PositionSelector = response.data;
                    if (angular.isDefined($routeParams.itemKey)) {
                        $scope.OrgData.Position = $scope.PositionSelector[findWithAttr($scope.PositionSelector, 'AlternativeShortName', $scope.OrgData.Position)].ObjectID;
                        //$scope.getEmpSubGroup($scope.OrgData.Position);
                    }
                    else {
                        $scope.OrgData.Position = '';
                        $scope.getCostCenter(Organize);
                    }

                }, function errorCallback(response) {
                    console.log('error Position list', response);

                });
            };

            $scope.onClickAddSave = function () {
                var oReturn = true;
                if ($scope.OrgData.Name.trim().length <= 0) oReturn = false;
                if ($scope.OrgData.Area.trim().length <= 0) oReturn = false;
                if ($scope.OrgData.SubArea.trim().length <= 0) oReturn = false;
                if ($scope.OrgData.EmpGroup.trim().length <= 0) oReturn = false;
                if ($scope.OrgData.EmpSubGroup.trim().length <= 0) oReturn = false;
                if ($scope.OrgData.OrgUnit.trim().length <= 0) oReturn = false;
                if ($scope.OrgData.CostCenter.trim().length <= 0) oReturn = false;
                if ($scope.OrgData.Position.trim().length <= 0) oReturn = false;
                if (oReturn == true) {
                    console.log($scope.OrgData);
                    var oRequestParameterr = {
                        InputParameter: { "EmployeeID": $scope.itemKey, "DataObjectType": "Org", "BeginDate": $scope.OrgData.BeginDate.toString('yyyy-MM-dd'), "EndDate": $scope.OrgData.EndDate }
                                                , CurrentEmployee: getToken(CONFIG.USER)
                    };
                    var urlll = CONFIG.SERVER + 'Employee/CheckStatus';
                    $http({
                        method: 'POST',
                        url: urlll,
                        data: oRequestParameterr
                    }).then(function successCallback(response) {
                        // Success
                        console.log('eiei', response);
                        var oRequestParameter = {
                            InputParameter: { "EmployeeID": $scope.itemKey, "OrgData": $scope.OrgData }
                                                , CurrentEmployee: getToken(CONFIG.USER)
                        };
                        var insertURL = CONFIG.SERVER + 'Employee/InsertINFOTYPE0001';
                        if (response.data.CanInsert == true && response.data.Duplicate == true) {
                            if (confirm($scope.Text['PERSONALADMINISTRATION'].DuplicatePeriod) == true) {
                                InsertINFOTYPE0001(oRequestParameter, insertURL);
                            }
                        }
                        else if (response.data.CanInsert == true) {
                            InsertINFOTYPE0001(oRequestParameter, insertURL);
                        }
                        else {
                            $scope.SubAreaSelector = $scope.SubAreaObject[$scope.OrgData.Area];
                            alert($scope.Text['PERSONALADMINISTRATION'].StaccatoPeriod);
                        }
                    }, function errorCallback(response) {
                        // Error
                        console.log('error RequestController.', response);
                    });
                }
            };
            if ($scope.BeginDate != null && $scope.BeginDate != 'null') {
                //if ($routeParams.itemKey.split('|')[2] != null) { $scope.CanEditPeriod = true; }
                $scope.CanEditPeriod = true;
                // ------ get person data with employeeid and begindate -------//
                var oRequestParameter = {
                    InputParameter: { "EmployeeID": $scope.itemKey, "BeginDate": $scope.BeginDate }
                                            , CurrentEmployee: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER)
                };
                var URL = CONFIG.SERVER + 'Employee/INFOTYPE0001Get';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    console.log('GetOrgDataEdit.', response.data);
                    $scope.OrgData = response.data;
                    $scope.OrgData.BeginDate = $filter('date')(response.data.BeginDate, 'yyyy-MM-dd');
                    $scope.OrgData.EndDate = $filter('date')(response.data.EndDate, 'yyyy-MM-dd');
                    $scope.OrgData.EmployeeID = $scope.itemKey;
                    console.log($scope.OrgData.EndDate);
                    $scope.NoSpecify.val = ($scope.OrgData.EndDate == '9999-12-31');
                    console.log('GetOrgDataEdit.', $scope.OrgData);
                    GetOrganizationSelectData();
                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                });
                // ------ ------------------------------------ -------//
            }
            else {
                var date = new Date();
                var dateStr = date.getFullYear() + '-' + ("0" + (date.getMonth() + 1)).slice(-2) + '-' + ("0" + date.getDate()).slice(-2);
                $scope.OrgData = {
                    EmployeeID: $scope.itemKey,
                    BeginDate: dateStr,
                    EndDate: '9999-12-31',
                    Name: '',
                    Area: '',
                    SubArea: '',
                    EmpGroup: '',
                    EmpSubGroup: '',
                    OrganizationName: '',
                    CostCenter: '',
                    Position: ''
                };
                var oRequestParameter = {
                    InputParameter: { "EmployeeID": $scope.itemKey, "BeginDate": $scope.BeginDate, "SubType": $scope.SubTypeDefult }
                                            , CurrentEmployee: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER)
                };
                var URL = CONFIG.SERVER + 'Employee/INFOTYPE0001GetAllHistory';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.OrgData.Name = response.data[0].Name;
                    GetOrganizationSelectData();
                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                });
                $scope.NoSpecify.val = ($scope.OrgData.EndDate == '9999-12-31');
                $scope.CanEditPeriod = true;
            }

            function GetOrganizationSelectData() {
                // ------ get all select data -------//
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: getToken(CONFIG.USER)
                };
                var URL = CONFIG.SERVER + 'Employee/GetOrganizationSelectData';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    console.log('select-datana.', response.data);
                    $scope.EmpGroup = response.data.EmpGroup;
                    $scope.EmpSubGroup = response.data.EmpSubGroup;
                    $scope.CostCenterSelector = response.data.CostCenter;
                    $scope.PositionSelector = response.data.Position;
                    $scope.OrgUnitSelector = response.data.Organize;
                    $scope.AreaSelector = response.data.Area;
                    $scope.SubAreaObject = response.data.SubArea;

                    if ($scope.OrgData.Name != '') {
                        $scope.OrgData.Area = $scope.AreaSelector[findWithAttr($scope.AreaSelector, 'ObjectID'/*'AlternativeShortName'*/, $scope.OrgData.Area)].ObjectID;
                        $scope.SubAreaSelector = response.data.SubArea[$scope.OrgData.Area];
                        $scope.OrgData.SubArea = $scope.SubAreaSelector[findWithAttr($scope.SubAreaSelector, 'ObjectID'/*'AlternativeShortName'*/, $scope.OrgData.SubArea)].ObjectID;
                        $scope.OrgData.OrgUnit = $scope.OrgUnitSelector[findWithAttr($scope.OrgUnitSelector, 'AlternativeShortName', $scope.OrgData.OrgUnit)].ObjectID;
                        $scope.OrgData.EmpGroup = $scope.EmpGroup[findWithAttr($scope.EmpGroup, 'ObjectID', $scope.OrgData.EmpGroup.split(':')[0])].ObjectID;
                        if ($scope.OrgData.EmpSubGroup.length == 1) $scope.OrgData.EmpSubGroup = "0" + $scope.OrgData.EmpSubGroup.toString();
                        $scope.OrgData.EmpSubGroup = $scope.EmpSubGroup[findWithAttr($scope.EmpSubGroup, 'ObjectID', $scope.OrgData.EmpSubGroup)].ObjectID;
                        //$scope.OrgData.Position = $scope.PositionSelector[findWithAttr($scope.PositionSelector, 'AlternativeShortName', $scope.OrgData.PositionID)].ObjectID;
                        $scope.OrgData.Position = $scope.OrgData.PositionID;
                        //$scope.OrgData.CostCenter = $scope.CostCenterSelector[findWithAttr($scope.CostCenterSelector, 'AlternativeShortName', $scope.OrgData.CostCenter)].ObjectID;
                    }
                    console.log('select.', response.data)
                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                });
                // ------ ------------------------------------ -------//
            }

            function InsertINFOTYPE0001(oRequestParameter, URL) {
                // ------ insert data to database -------//
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    alert('COMPLETE');
                    $location.path('/frmViewContent/409/' + oRequestParameter.InputParameter["EmployeeID"]);
                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                });
                // ------ ------------------------------------ -------//
            }

            function findWithAttr(array, attr, value) {
                for (var i = 0; i < array.length; i += 1) {
                    if (array[i][attr] === value) {
                        return i;
                    }
                }
            }

            console.log('PersonalDataSettingEditorForOrganizationDataController.');

        }]);
})();