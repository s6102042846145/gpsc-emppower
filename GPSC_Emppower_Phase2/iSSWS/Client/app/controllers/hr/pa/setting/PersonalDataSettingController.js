﻿(function () {
    angular.module('ESSMobile')
        .controller('PersonalDataSettingController', ['$scope', '$http', '$routeParams', '$location', 'DTOptionsBuilder', '$route', 'CONFIG', '$mdDialog', '$filter', '$timeout', function ($scope, $http, $routeParams, $location, DTOptionsBuilder, $route, CONFIG, $mdDialog, $filter, $timeout) {
            $scope.PATextcategory = 'PERSONALADMINISTRATION';
            $scope.content.Header = $scope.Text[$scope.PATextcategory].CONSOLE_HEADSUBJECT;
            $scope.content.isShowHeader = false;
            $scope.PositionTextcategory = 'POSITION';
            $scope.EmpGroupTextcategory = 'EMPGROUP';
            $scope.EmpSubGroupTextcategory = 'EMPSUBGROUP';
            $scope.OrganizationTextcategory = 'ORGANIZATION';
            $scope.AreaTextcategory = 'AREA';
            $scope.SubAreaTextcategory = 'SUBAREA';
            $scope.message = "";
            $scope.currentPage = 0;
            $scope.pageSize = 10;
            $scope.EmployeeID = '';
            //$scope.ggggg = function () {
            //    $scope.message = $scope.EmployeeID;
            //    console($("#DataTables_Table_0_filter").children);
            //}


            // test list script start

            $scope.data= {
                nPageSize:10,
                arrFilterd:[],
                arrSplited:[],
                selectedColumn:0,
                tFilter:'',
                pages:[]
            };
            $scope.filteredItem = [];
            $scope.$watch('search', function(filter){ 
              filterArry(filter)
            });
            function filterArry(filter){
                $scope.filteredItem = $filter('filter')($scope.EmployeeData, filter);
                var expectedPage =0;
                var i,j,temparray,chunk = 10;
                $scope.data.arrSplited=[];
                for (i=0,j=$scope.filteredItem.length; i<j; i+=chunk) {
                    temparray = $scope.filteredItem.slice(i,i+chunk);
                    $scope.data.arrSplited.push(temparray);
                    expectedPage++
                }
                $scope.data.selectedColumn = 0;
            }
            $scope.pageLeft = function () {if($scope.data.selectedColumn>0){ $scope.data.selectedColumn--;}}
            $scope.pageRight = function () {if($scope.data.selectedColumn < $scope.data.arrSplited.length-1){$scope.data.selectedColumn++;}}
            /*
            $( document ).ready(function() { resetSize(); $timeout(function(){ resetSize();},2500);});
            jQuery(window).resize(function() { resetSize();});
            function resetSize(){
                $('#paggingWidth').width($('#widthruler').width());
                $('#sticky_main_bar').width($('#widthruler').width());
                $("#sticky_main_bar").css("position", "fixed");
            }
            */

            // test list script end

            $scope.OnClickCopyEmployee = function (id) {
                $location.path('/frmViewContent/410/' + id);
            }
            $scope.OnClickAddEmployee = function () {
                //alert(id);
                $location.path('/frmViewContent/410/');
            }
            $scope.OnClickEditEmployee = function (id) {
                //alert(id);
                $location.path('/frmViewContent/409/' + id);
            }
            $scope.OnClickDeleteEmployee = function (id,ev) {
                
                var confirm = $mdDialog.confirm()
                    .title($scope.Text['ACCOUNT_SETTING'].WANT_TO_DELETE)
                    .textContent($scope.Text['ACCOUNT_SETTING'].CAN_NOT_ROLLBACK_PLEASE_CONFIRM)
                    .ariaLabel($scope.Text['ACCOUNT_SETTING'].WANT_TO_DELETE)
                    .targetEvent(ev)
                    .ok($scope.Text['SYSTEM'].BUTTON_YES)
                    .cancel($scope.Text['SYSTEM'].BUTTON_NO);
                $mdDialog.show(confirm).then(function() {
                    var oRequestParameter = { InputParameter: { "EmployeeID": id }, CurrentEmployee: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER) };
                    var url = CONFIG.SERVER + 'Employee/DeletePersonManagementData';
                    $scope.loader.conetent = true;
                    $http({
                        method: 'POST',
                        data: oRequestParameter,
                        url: url
                    }).then(function successCallback(response) {
                        // Success
                        $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['ACCOUNT_SETTING'].DELETE_COMPLETED)
                                .textContent($scope.Text['ACCOUNT_SETTING'].CAN_NOT_ROLLBACK)
                                .ariaLabel($scope.Text['ACCOUNT_SETTING'].DELETE_COMPLETED)
                                .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                .targetEvent(ev)
                        );
                         $scope.loader.conetent = false;
                        //alert('ลบข้อมูลเรียบร้อย');
                        $route.reload();
                    }, function errorCallback(response) {
                        $scope.loader.conetent = false;
                        // Error
                        console.log('error RequestController.', response);
                    });
                }, function() {
      
                });
                /*
                if (confirm("ต้องการจะลบข้อมูลใช่หรือไม่") == true) {
                    var oRequestParameter = { InputParameter: { "EmployeeID": id }, CurrentEmployee: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER) };
                    var url = CONFIG.SERVER + 'Employee/DeletePersonManagementData';
                    $http({
                        method: 'POST',
                        data: oRequestParameter,
                        url: url
                    }).then(function successCallback(response) {
                        // Success
                        alert('ลบข้อมูลเรียบร้อย');
                        $route.reload();
                    }, function errorCallback(response) {
                        // Error
                        console.log('error RequestController.', response);
                    });
                }
                */
            }
            $scope.EmployeeData = [];
            $scope.DataTable = [];
            $scope.CountData = 0;
            $scope.dtOptions = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength($scope.pageSize)/*.withOption('searching', false)*/.withOption('lengthChange', false).withOption('ordering', false);

            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER) };
            //var oEmployeeData = { CurrentEmployee: getToken(CONFIG.USER) };
            var URL = CONFIG.SERVER + 'Employee/INFOTYPE0001GetAllEmployeeHistory';
            $scope.loader.conetent = true;
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                console.log('document.', response.data);
                //$scope.CountData = response.data.length;
                $scope.EmployeeData = response.data;
                console.log('data.', $scope.EmployeeData)
                // display list
                filterArry('');
                 $scope.loader.conetent = false;
            }, function errorCallback(response) {
                // Error
                console.log('error RequestController.', response);
                $scope.EmployeeData = null;
                 $scope.loader.conetent = false;
            });
            console.log('PersonalDataSettingController');












        }]);

})();

