﻿(function () {
    angular.module('ESSMobile')
        .controller('PersonalDataSettingEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', '$route', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, $route, CONFIG, $mdDialog) {
            $scope.PATextcategory = 'PERSONALADMINISTRATION'; // Personal data Setting (Edit) fixed text
            $scope.content.Header = $scope.Text[$scope.PATextcategory].CONSOLE_HEADSUBJECT;
            $scope.itemKey = angular.isUndefined($routeParams.itemKey) && $routeParams.itemKey != '0' ? 'null' : $routeParams.itemKey;
            $scope.PersonData;
            $scope.OrganizationData;
            $scope.ContactData;
            $scope.isResign = false;
            $scope.ResignDate = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.ErrorMessage = '';

            $scope.getDateFormate = function (date) {
                return $filter('date')(date, 'd/M/yyyy');
            }

            $scope.getDate = function (date) {
                var oDate = new Date(date);
                return oDate;
            }

            $scope.getDateNew = function (date) {
                var oDate = new Date(date);
                return oDate;
            }

            function getPersonSelectData() {
                var oRequestParameter = {
                    InputParameter: {}
                , CurrentEmployee: getToken(CONFIG.USER)
                };
                var URL = CONFIG.SERVER + 'Employee/GetPersonSelectData/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    console.log('select-datana.', response.data);
                    $scope.Prefix = response.data.Prefix;
                    $scope.Gender = response.data.Gender;
                    $scope.MaritalStatus = response.data.MaritalStatus;
                    $scope.Nationality = response.data.Nationality;
                    $scope.Language = response.data.Language;
                    $scope.Religion = response.data.Religion;
                    for (var j = 0; j < $scope.PersonData.length; j++) {
                        $scope.PersonData[j].TitleName = angular.copy($scope.PersonData[j].TitleID);
                        if ($scope.PersonData[j].TitleID) $scope.PersonData[j].TitleID = $scope.Prefix[findWithAttr($scope.Prefix, 'AlternativeShortName', $scope.PersonData[j].TitleID)].ObjectID;
                        if ($scope.PersonData[j].Gender != '') $scope.PersonData[j].Gender = $scope.Gender[findWithAttr($scope.Gender, 'AlternativeShortName', $scope.PersonData[j].Gender)].ObjectID;
                        if ($scope.PersonData[j].MaritalStatus) $scope.PersonData[j].MaritalStatus = $scope.MaritalStatus[findWithAttr($scope.MaritalStatus, 'AlternativeShortName', $scope.PersonData[j].MaritalStatus)].ObjectID;
                        if ($scope.PersonData[j].Nationality) $scope.PersonData[j].Nationality = $scope.Nationality[findWithAttr($scope.Nationality, 'AlternativeShortName', $scope.PersonData[j].Nationality)].ObjectID;
                        if ($scope.PersonData[j].Language != '') $scope.PersonData[j].Language = $scope.Language[findWithAttr($scope.Language, 'AlternativeShortName', $scope.PersonData[j].Language)].ObjectID;
                        if ($scope.PersonData[j].Religion != '') $scope.PersonData[j].Religion = $scope.Religion[findWithAttr($scope.Religion, 'AlternativeShortName', $scope.PersonData[j].Religion)].ObjectID;
                    }
                    console.log('data.', $scope.TitleID)
                    /*if (angular.isDefined($routeParams.itemKey)) {
                        $scope.getEmployeeData($routeParams.itemKey);
                    }
                    else {
                        var date = new Date();
                        var dateStr = date.getFullYear() + '-' + ("0" + (date.getMonth() + 1)).slice(-2) + '-' + ("0" + date.getDate()).slice(-2);
                        $scope.PersonData = {
                            EmployeeID: null,
                            BeginDate: dateStr,
                            EndDate: '9999-12-31',
                            TitleID: '',
                            FirstName: '',
                            LastName: '',
                            NickName: '',
                            DOB: '',
                            Gender: '1',
                            MaritalStatus: '',
                            Nationality: '',
                            Language: '',
                            Religion: ''
                        };
                        $scope.NoSpecify.val = ($scope.PersonData.EndDate == '9999-12-31');
                    }*/

                    $scope.CanEditPeriod = true;
                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                });
            }

            function getContactSelectData() {
                var oRequestParameter = {
                    InputParameter: {}
                , CurrentEmployee: getToken(CONFIG.USER)
                };
                var URL = CONFIG.SERVER + 'Employee/GetContactSelectData';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    console.log('select-datana.', response.data);
                    $scope.SubType = response.data.SubType;
                    /*if (angular.isUndefined($scope.ContData)) {
                        $scope.ContData = {
                            EmployeeID: $scope.PersonData.EmployeeID,
                            BeginDate: $scope.PersonData.BeginDate,
                            EndDate: $scope.PersonData.EndDate,
                            Name: $scope.OrgData.Name,
                            CategoryCode: '',
                            DataText: ''
                        };
                    }
                    else {
                        $scope.ContData.BeginDate = $filter('date')($scope.ContData.BeginDate, 'yyyy-MM-dd');
                        $scope.ContData.EndDate = $filter('date')($scope.ContData.EndDate, 'yyyy-MM-dd');
                    }*/
                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                });
            }

            function getOrganizationSelectData() {
                var oRequestParameter = {
                    InputParameter: {}
                , CurrentEmployee: getToken(CONFIG.USER)
                };
                var URL = CONFIG.SERVER + 'Employee/GetOrganizationSelectData';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    console.log('select-datana.', response.data);
                    $scope.EmpGroup = response.data.EmpGroup;
                    $scope.EmpSubGroup = response.data.EmpSubGroup;
                    //$scope.CostCenterSelector = response.data.CostCenter;
                    $scope.PositionSelectorMaster = response.data.Position;
                    $scope.PositionSelector = angular.copy(response.data.Position);
                    $scope.OrgUnitSelector = response.data.Organize;
                    $scope.AreaSelector = response.data.Area;
                    $scope.SubAreaObject = response.data.SubArea;
                    if (angular.isUndefined($scope.OrgData)) {
                        $scope.OrgData = {
                            EmployeeID: $scope.PersonData.EmployeeID,
                            BeginDate: $scope.PersonData.BeginDate,
                            EndDate: $scope.PersonData.EndDate,
                            Name: $scope.Prefix[findWithAttr($scope.Prefix, 'ObjectID', $scope.PersonData.TitleID)].AlternativeShortName + $scope.PersonData.FirstName + ' ' + $scope.PersonData.LastName,
                            Area: '',
                            SubArea: '',
                            EmpGroup: '',
                            EmpSubGroup: '',
                            OrgUnit: '',
                            CostCenter: '',
                            Position: ''
                        };
                    }
                    else {
                        for (var j = 0; j < $scope.OrgData.length; j++) {
                            //$scope.OrgData[j].Area = $scope.AreaSelector[findWithAttr($scope.AreaSelector, 'AlternativeShortName', $scope.OrgData[j].Area)].ObjectID;
                            $scope.SubAreaSelector = response.data.SubArea[$scope.OrgData[j].Area];
                            //$scope.OrgData[j].SubArea = $scope.SubAreaSelector[findWithAttr($scope.SubAreaSelector, 'AlternativeShortName', $scope.OrgData[j].SubArea)].ObjectID;
                            $scope.OrgData[j].OrgUnit = $scope.OrgUnitSelector[findWithAttr($scope.OrgUnitSelector, 'AlternativeShortName', $scope.OrgData[j].OrgUnit)].ObjectID;
                            //$scope.getPosition($scope.OrgData[j].OrgUnit);
                            $scope.getCostCenter($scope.OrgData[j].OrgUnit);
                            $scope.OrgData[j].EmpGroup = $scope.OrgData[j].EmpGroup.split(':')[0];
                            if ($scope.OrgData[j].EmpSubGroup.length == 1) $scope.OrgData[j].EmpSubGroup = "0" + $scope.OrgData[j].EmpSubGroup.toString();
                        }
                        //$scope.OrgData.EmpGroup = $scope.EmpGroup[findWithAttr($scope.EmpGroup, 'ObjectID', $scope.OrgData.EmpGroup.split(':')[0])].ObjectID;
                        //$scope.OrgData.EmpSubGroup = $scope.EmpSubGroup[findWithAttr($scope.EmpSubGroup, 'ObjectID', $scope.OrgData.EmpSubGroup)].ObjectID;
                        //$scope.OrgData.Position = $scope.PositionSelector[findWithAttr($scope.PositionSelector, 'AlternativeShortName', $scope.OrgData.Position)].ObjectID;
                        //$scope.OrgData.CostCenter = $scope.CostCenterSelector[findWithAttr($scope.CostCenterSelector, 'AlternativeShortName', $scope.OrgData.CostCenter)].ObjectID;
                    }
                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                });
            }

            $scope.getPosition = function (Organize) {
                var URL = CONFIG.SERVER + 'Employee/GetPositionByOrganize';
                var oRequestParameter = { InputParameter: { "Organize": Organize }, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.PositionSelector = response.data;
                    if (angular.isDefined($routeParams.itemKey)) {
                        if ($scope.OrgData.Position.length > 0) $scope.OrgData.Position = $scope.PositionSelector[findWithAttr($scope.PositionSelector, 'AlternativeShortName', $scope.OrgData.Position)].ObjectID;
                        //$scope.getEmpSubGroup($scope.OrgData.Position);
                    }
                    else {
                        $scope.OrgData.Position = '';
                        $scope.getCostCenter(Organize);
                    }

                }, function errorCallback(response) {
                    console.log('error Position list', response);

                });
            };

            $scope.getCostCenter = function (Organize) {
                var URL = CONFIG.SERVER + 'Employee/GetCostCenterByOrganize';
                var oRequestParameter = { InputParameter: { "Organize": Organize }, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.CostCenterSelector = response.data;
                    if (angular.isDefined($routeParams.itemKey)) {
                        for (var j = 0; j < $scope.OrgData.length; j++) {
                            if ($scope.OrgData[j].CostCenter.length > 0) $scope.OrgData[j].CostCenter = $scope.CostCenterSelector[findWithAttr($scope.CostCenterSelector, 'ObjectID', $scope.OrgData[j].CostCenter)].ObjectID;
                        }
                        //$scope.getEmpSubGroup($scope.OrgData.Position);
                    }
                    else {
                        $scope.OrgData.CostCenter = '';
                    }
                }, function errorCallback(response) {
                    console.log('error Position list', response);

                });
            };

            $scope.getTitleName = function (index) {
                $scope.PersonData[index].TitleName = $scope.Prefix[findWithAttr($scope.Prefix, 'ObjectID', $scope.PersonData[index].TitleID)].AlternativeShortName;
            }

            function findWithAttr(array, attr, value) {
                for (var i = 0; i < array.length; i += 1) {
                    if (array[i][attr] === value) {
                        return i;
                    }
                }
            }

            $scope.onCheckBoxChange = function ($index) {
                if ($scope.PositionData[$index].NoSpecify == true) {
                    $scope.PositionData[$index].EndDate = $scope.getDate('9999-12-31T00:00:00');
                } else {
                    $scope.PositionData[$index].EndDate = $scope.PositionData[$index].BeginDate;
                }
            }

            $scope.onCheckBoxConChange = function ($index) {
                if ($scope.ContactData[$index].NoSpecify == true) {
                    $scope.ContactData[$index].EndDate = $scope.getDate('9999-12-31T00:00:00');
                } else {
                    $scope.ContactData[$index].EndDate = $scope.ContactData[$index].BeginDate;
                }
            }

            $scope.setPersonDataEndDate = function (index) {
                var tempDate = angular.copy(new Date($scope.PersonData[index].BeginDate));
                $scope.PersonData[index - 1].EndDate = $scope.getDate(tempDate.setDate(tempDate.getDate() - 1));
            }

            $scope.setOrgDataEndDate = function (index) {
                var tempDate = angular.copy(new Date($scope.OrgData[index].BeginDate));
                $scope.OrgData[index - 1].EndDate = $scope.getDate(tempDate.setDate(tempDate.getDate() - 1));
            }

            $scope.setContactDataEndDate = function (index) {
                var tempDate = angular.copy(new Date($scope.ContactData[index].BeginDate));
                $scope.ContactData[index - 1].EndDate = $scope.getDate(tempDate.setDate(tempDate.getDate() - 1));
            }

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $scope.OnClickAddEmployee = function () {
                var PersonDataObj = {
                    EmployeeID: $scope.itemKey,
                    BeginDate: $filter('date')(new Date(), 'yyyy-MM-ddT00:00:00'),
                    EndDate: '9999-12-31T00:00:00',
                    TitleID: '',
                    FirstName: '',
                    LastName: '',
                    NickName: '',
                    FullNameEn: '',
                    DOB: $filter('date')(new Date(), 'yyyy-MM-ddT00:00:00'),
                    Gender: '',
                    Nationality: '',
                    MaritalStatus: '',
                    Language: '',
                    Religion: ''
                };
                $scope.PersonData.push(PersonDataObj);
                $scope.setPersonDataEndDate($scope.PersonData.length - 1);
            }
            $scope.OnClickDeleteEmployee = function (index) {
                if (index == ($scope.PersonData.length - 1)) {
                    $scope.PersonData[index - 1].EndDate = $scope.getDate('9999-12-31T00:00:00');
                    $scope.PersonData.splice(index, 1);
                }
                else {
                    $scope.PersonData.splice(index, 1);
                    $scope.setPersonDataEndDate(index);
                }
            };
            $scope.OnClickAddPosition = function () {
                //$location.path('/frmViewContent/411/' + $scope.itemKey);
                var PositionDataObj = {
                    ObjectType: 'POSITION',
                    ObjectID: '',
                    NextObjectType: 'EMPLOYEE',
                    NextObjectID: $scope.itemKey,
                    Relation: 'A008',
                    BeginDate: $filter('date')(new Date(), 'yyyy-MM-ddT00:00:00'),
                    EndDate: '9999-12-31T00:00:00',
                    NoSpecify: true,
                    PercentValue: "0"
                };
                $scope.PositionData.push(PositionDataObj);
            }
            $scope.OnClickDeletePosition = function (index) {
                $scope.PositionData.splice(index, 1);
            };
            $scope.OnClickAddOrganization = function () {
                var OrgDataObj = {
                    EmployeeID: $scope.itemKey,
                    BeginDate: $filter('date')(new Date(), 'yyyy-MM-ddT00:00:00'),
                    EndDate: '9999-12-31T00:00:00',
                    Name: '',
                    Area: '',
                    SubArea: '',
                    EmpGroup: '',
                    EmpSubGroup: '',
                    OrgUnit: '',
                    Position: '',
                    CostCenter: ''
                };
                $scope.OrgData.push(OrgDataObj);
                $scope.setOrgDataEndDate($scope.OrgData.length - 1);
            }
            $scope.OnClickDeleteOrganization = function (index) {
                if (index == ($scope.OrgData.length - 1)) {
                    $scope.OrgData[index - 1].EndDate = $scope.getDate('9999-12-31T00:00:00');
                    $scope.OrgData.splice(index, 1);
                }
                else {
                    $scope.OrgData.splice(index, 1);
                    $scope.setOrgDataEndDate(index);
                }
            };
            $scope.OnClickAddContact = function () {
                var ContactDataObj = {
                    EmployeeID: $scope.itemKey,
                    BeginDate: $filter('date')(new Date(), 'yyyy-MM-ddT00:00:00'),
                    EndDate: '9999-12-31T00:00:00',
                    NoSpecify: true,
                    CategoryCode: '',
                    DataText: ''
                };
                $scope.ContactData.push(ContactDataObj);
            }
            $scope.OnClickDeleteContact = function (index) {
                $scope.ContactData.splice(index, 1);
            };
            $scope.setSelectedResignDate = function (selectedDate) {
                $scope.ResignDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };
            $scope.onClickCancel = function () {
                $location.path('/frmViewContent/401/');
            }

            function checkDupPosition100(obj) {
                for (var j = 0; j < $scope.PositionData100.length; j++) {
                    if ((obj.BeginDate <= $scope.PositionData100[j].EndDate && obj.BeginDate >= $scope.PositionData100[j].BeginDate)
                        || ($scope.PositionData100[j].BeginDate <= obj.EndDate && $scope.PositionData100[j].BeginDate >= obj.BeginDate)) {
                        $scope.ErrorMessage = 'POSTION_100_DUPLICATE_PERIOD';
                        return true;
                    }
                }
                $scope.PositionData100.push(angular.copy(obj));
                return false;
            }

            function checkDupContactData(obj) {
                for (var j = 0; j < $scope.ContactDataCopy.length; j++) {
                    if (((obj.BeginDate <= $scope.ContactDataCopy[j].EndDate && obj.BeginDate >= $scope.ContactDataCopy[j].BeginDate)
                        || ($scope.ContactDataCopy[j].BeginDate <= obj.EndDate && $scope.ContactDataCopy[j].BeginDate >= obj.BeginDate))
                        && $scope.ContactDataCopy[j].CategoryCode == obj.CategoryCode) {
                        $scope.ErrorMessage = 'CONTRACT_DUPLICATE_PERIOD';
                        return true;
                    }
                }
                $scope.ContactDataCopy.push(angular.copy(obj));
                return false;
            }
            
            function validate() {
                $scope.ErrorMessage = 'PLEASE_INSERT_ALL_FIELDS';
                $scope.PositionData100 = [];
                $scope.ContactDataCopy = [];
                var oReturn = true;
                for (var i = 0; i < $scope.PersonData.length; i++) {
                    if (!$scope.PersonData[i].TitleID) { return false; }
                    else if (!$scope.PersonData[i].FirstName) { return false; }
                    else if (!$scope.PersonData[i].LastName) { return false; }
                    else if (!$scope.PersonData[i].FullNameEn) { return false; }
                    else if (!$scope.PersonData[i].NickName) { return false; }
                    else if (!$scope.PersonData[i].MaritalStatus) { return false; }
                    else if (!$scope.PersonData[i].Nationality) { return false; }
                    else if (!$scope.PersonData[i].Religion) { return false; }
                    else if (!$scope.PersonData[i].Language) { return false; }

                }
                ///////
                for (var i = 0; i < $scope.OrgData.length; i++) {
                    if (!$scope.OrgData[i].Name) { return false; }
                    else if (!$scope.OrgData[i].Area) { return false; }
                    else if (!$scope.OrgData[i].SubArea) { return false; }
                    else if (!$scope.OrgData[i].EmpGroup) { return false; }
                    else if (!$scope.OrgData[i].EmpSubGroup) { return false; }
                    else if (!$scope.OrgData[i].OrgUnit) { return false; }
                    else if (!$scope.OrgData[i].Position) { return false; }
                    else if (!$scope.OrgData[i].CostCenter) { return false; }
                }

                ///////
                for (var i = 0; i < $scope.PositionData.length; i++) {
                    if (!$scope.PositionData[i].ObjectType) { return false; }
                    else if (!$scope.PositionData[i].ObjectID) { return false; }
                    else if (!$scope.PositionData[i].NextObjectType) { return false; }
                    else if (!$scope.PositionData[i].NextObjectID) { return false; }
                    else if (!$scope.PositionData[i].Relation) { return false; }
                        //else if (!$scope.PositionData[i].PercentValue) { return false; }
                    else if ($scope.PositionData[i].PercentValue == "100") {
                        if ($scope.PositionData100.length == 0)
                            $scope.PositionData100.push(angular.copy($scope.PositionData[i]));
                        else if (checkDupPosition100($scope.PositionData[i])) {
                            return false;
                        }
                    }
                }
                ///////
                for (var i = 0; i < $scope.ContactData.length; i++) {
                    if (!$scope.ContactData[i].CategoryCode) { return false; }
                    else if (!$scope.ContactData[i].DataText) { return false; }
                    else if ($scope.ContactDataCopy.length == 0)
                        $scope.ContactDataCopy.push(angular.copy($scope.ContactData[i]));
                    else if (checkDupContactData($scope.ContactData[i])) {
                        return false;
                    }
                }
                return true;
            }

            $scope.onClickSave = function (ev) {
                if (validate()) {
                    var DateArr = sortPeriod();
                    $scope.OrgDataIntegrated = [];
                    INFOTYPEIntegration(DateArr);
                    $scope.OrgData = angular.copy($scope.OrgDataIntegrated);
                    InsertINFOTYPEAllForEmployee(ev);
                    /*$mdDialog.show(
                                $mdDialog.alert()
                                    .clickOutsideToClose(true)
                                    .title('5555555555')
                                    .textContent('โหดสัส รัสเซีย โหดเกิ้น!!')
                                    .ariaLabel('WTF')
                                    .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                    .targetEvent(ev));*/
                }
                else {
                    $mdDialog.show(
                                $mdDialog.alert()
                                    .clickOutsideToClose(true)
                                    .title($scope.Text['PERSONALADMINISTRATION'].EXCEPTION)
                                    .textContent($scope.Text['PERSONALADMINISTRATION'][$scope.ErrorMessage])
                                    .ariaLabel($scope.Text['PERSONALADMINISTRATION'].EXCEPTION)
                                    .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                    .targetEvent(ev));
                }
            };
            function Is100() {
                return function filterFn(item) {
                    return item.PercentValue == "100";
                };
            }

            
            function newOrgData(OrgObj, PosObj, PersObj, Begindate, EndDate) {
                var tempEndDate = new Date(EndDate);
                tempEndDate.setSeconds(tempEndDate.getSeconds() - 1);
                var OrgDataObj = {
                    EmployeeID: $scope.itemKey,
                    BeginDate: Begindate,
                    EndDate: tempEndDate,
                    Name: PersObj.TitleName + PersObj.FirstName + ' ' + PersObj.LastName,
                    Area: OrgObj.Area,
                    SubArea: OrgObj.SubArea,
                    EmpGroup: OrgObj.EmpGroup,
                    EmpSubGroup: OrgObj.EmpSubGroup,
                    OrgUnit: PosObj.OrgUnit,
                    Position: PosObj.ObjectID,
                    CostCenter: OrgObj.CostCenter
                };
                return OrgDataObj;
            }

            Array.prototype.findIndexBetweenDate = function (value) {
                var ret = 0;
                for (var i = 0, _len = this.length; i < _len; i++) {
                    if (value <= this[i].EndDate && value >= this[i].BeginDate)
                            return i;
                }
                return ret;
            };

            function INFOTYPEIntegration(DateArr) {
                for (var k = 0; k < DateArr.length; k++) {
                    if ((DateArr.length - 1) != k) {
                        var tempOrg = $scope.OrgData[$scope.OrgData.findIndexBetweenDate(DateArr[k])];
                        var tempPers = $scope.PersonData[$scope.PersonData.findIndexBetweenDate(DateArr[k])];
                        var tempPos = $scope.PositionData[$scope.PositionData.findIndexBetweenDate(DateArr[k])];
                        $scope.OrgDataIntegrated.push(newOrgData(tempOrg, tempPos, tempPers, DateArr[k], DateArr[k + 1]));
                    }
                }
            }

            Array.prototype.DupDate = function (value) {
                for (var i = 0, _len = this.length; i < _len; i++) {
                    if ($filter('date')(new Date(this[i]), 'yyyy-MM-dd') == $filter('date')(new Date(value), 'yyyy-MM-dd')) return true;
                }
                return false;
            };

            function sortPeriod() {
                var DateArr = [];
                var tempPersonData = angular.copy($scope.PersonData);
                var tempOrgData= angular.copy($scope.OrgData);
                var tempPositionData = angular.copy($scope.PositionData.filter(Is100()));
                for (var k = 0; k < tempOrgData.length; k++) {
                    if (!DateArr.DupDate(new Date(tempOrgData[k].BeginDate.setHours(0))))
                        DateArr.push(tempOrgData[k].BeginDate);
                    if (!DateArr.DupDate(new Date(tempOrgData[k].EndDate.setHours(0))))
                        DateArr.push(tempOrgData[k].EndDate);
                }

                for (var k = 0; k < tempPositionData.length; k++) {
                    if (!DateArr.DupDate(new Date(tempPositionData[k].BeginDate.setHours(0))))
                        DateArr.push(tempPositionData[k].BeginDate);
                    if (!DateArr.DupDate(new Date(tempPositionData[k].EndDate.setHours(0))))
                        DateArr.push(tempPositionData[k].EndDate);
                }

                for (var k = 0; k < tempPersonData.length; k++) {
                    if (!DateArr.DupDate(new Date(tempPersonData[k].BeginDate.setHours(0))))
                        DateArr.push(tempPersonData[k].BeginDate);
                    if (!DateArr.DupDate(new Date(tempPersonData[k].EndDate.setHours(0))))
                        DateArr.push(tempPersonData[k].EndDate);
                }
                return DateArr.sort(function (a, b) { return b < a });
            }

            function InsertINFOTYPEAllForEmployee(ev) {
                // ------ insert data to database -------//
                var oRequestParameter = {
                    InputParameter: { "EmployeeID": $scope.itemKey, "PersonData": $scope.PersonData, "OrgData": $scope.OrgData, "ContData": $scope.ContactData, "PositionData": $scope.PositionData }
                , CurrentEmployee: getToken(CONFIG.USER)
                };
                var URL = CONFIG.SERVER + 'Employee/InsertINFOTYPEAllForEmployee/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $mdDialog.show(
                                $mdDialog.alert()
                                    .clickOutsideToClose(true)
                                    .title($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                    .textContent($scope.Text['ACCOUNT_SETTING'].GO_BACK)
                                    .ariaLabel($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                    .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                    .targetEvent(ev));
                    $scope.PersonData.EmployeeID = response.data;
                    $location.path('/frmViewContent/401/');
                    /*if (response.data.length > 7) {
                        
                    }
                    else {
                        $mdDialog.show(
                                $mdDialog.alert()
                                    .clickOutsideToClose(true)
                                    .title($scope.Text['ACCOUNT_SETTING'].CAN_NOT_DELETE)
                                    .textContent($scope.Text['ACCOUNT_SETTING'].DATA_DUPLICATE)
                                    .ariaLabel($scope.Text['ACCOUNT_SETTING'].CAN_NOT_DELETE)
                                    .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                    .targetEvent(ev)
                            );
                    }*/

                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                });
                // ------ ------------------------------------ -------//
            }

            $scope.OnClickResignEmployee = function () {
                //console.log(begindate.split('/')[2] + '-' + begindate.split('/')[1] + '-' + begindate.split('/')[0]);
                if (confirm("ต้องการจะลาออกใช่หรือไม่") == true) {
                    var oRequestParameter = {
                        InputParameter: { "EmployeeID": $scope.itemKey, "EndDate": $filter('date')($scope.ResignDate, 'yyyy-MM-dd') }
                                            , CurrentEmployee: getToken(CONFIG.USER)
                    };
                    var url = CONFIG.SERVER + 'Employee/Resign';
                    $http({
                        method: 'POST',
                        data: oRequestParameter,
                        url: url
                    }).then(function successCallback(response) {
                        // Success
                        alert('ลาออกเรียบร้อย');
                        $route.reload();
                    }, function errorCallback(response) {
                        // Error
                        console.log('error RequestController.', response);
                    });
                }
            }

            // ------ get history employee data of one man -------//
            var oRequestParameter = {
                InputParameter: { "EmployeeID": $scope.itemKey }
                                            , CurrentEmployee: getToken(CONFIG.USER)
            };
            var URL = CONFIG.SERVER + 'Employee/GetDataHistory';
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                console.log('document.', response.data);
                $scope.PersonData = response.data.PersonData;
                for (var i = 0; i < $scope.PersonData.length; i++) {
                    if ($scope.PersonData[i].DOB != '0001-01-01T00:00:00Z') { $scope.PersonData[i].DOB = $filter('date')($scope.PersonData[i].DOB, 'yyyy-MM-dd'); }
                    else { $scope.PersonData[i].DOB = ''; }
                    $scope.PersonData[i].EmployeeID = $scope.itemKey;
                }
                $scope.OrgData = response.data.OrganizationData;
                $scope.ContactData = response.data.ContactData;
                for (var i = 0; i < $scope.ContactData.length; i++) {
                    $scope.ContactData[i].NoSpecify = $scope.ContactData[i].EndDate == '9999-12-31T00:00:00';
                }
                console.log('data.', $scope.EmployeeData)
                getPersonSelectData();
                getContactSelectData();
                getOrganizationSelectData();
            }, function errorCallback(response) {
                // Error
                console.log('error RequestController.', response);
                $scope.PersonData = null;
            });

            var oRequestParameter = {
                InputParameter: { "NextObjectID": $scope.itemKey }
                                            , CurrentEmployee: getToken(CONFIG.USER)
            };


            var oRequestParameter = {
                InputParameter: { "NextObjectID": $scope.itemKey }
                                           , CurrentEmployee: getToken(CONFIG.USER)
            };
            var URL = CONFIG.SERVER + 'Employee/GetBelongToRelation';
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                console.log('document.', response.data);
                $scope.OrgSelect = response.data;
                for (var j = 0; j < $scope.OrgSelect.length; j++) {
                    $scope.OrgSelect[j].ObjectName = $scope.OrgSelect[j].ObjectID.split(':')[1];
                    $scope.OrgSelect[j].ObjectID = $scope.OrgSelect[j].ObjectID.split(':')[0];
                    //$scope.OrgSelect[j].PercentValue = $scope.OrgSelect[j].PercentValue.toString();
                    $scope.OrgSelect[j].NextObjectName = $scope.OrgSelect[j].NextObjectID.split(':')[1];
                    $scope.OrgSelect[j].NextObjectID = $scope.OrgSelect[j].NextObjectID.split(':')[0];

                }

                var URL = CONFIG.SERVER + 'Employee/GetPositionForPerson';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    console.log('document.', response.data);
                    $scope.PositionData = response.data;
                    for (var j = 0; j < $scope.PositionData.length; j++) {
                        $scope.PositionData[j].AlternativeShortName = $scope.PositionData[j].ObjectID.split(':')[1];
                        $scope.PositionData[j].ObjectID = $scope.PositionData[j].ObjectID.split(':')[0];
                        $scope.PositionData[j].NextObjectID = $scope.PositionData[j].NextObjectID.split(':')[0];
                        $scope.PositionData[j].PercentValue = $scope.PositionData[j].PercentValue.toString();
                        //if ($scope.PositionData[j].EndDate == '9999-12-30T17:00:00.000Z')
                        $scope.PositionData[j].NoSpecify = $scope.PositionData[j].EndDate == '9999-12-31T00:00:00';
                        for (var k = 0; k < $scope.OrgSelect.length; k++) {
                            if ($scope.PositionData[j].ObjectID == $scope.OrgSelect[k].ObjectID) {
                                $scope.PositionData[j].OrgUnit = $scope.OrgSelect[k].NextObjectID;
                            }
                        }

                    }
                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                    $scope.PersonData = null;
                });
            }, function errorCallback(response) {
                // Error
                console.log('error RequestController.', response);
            });
            // ------ ------------------------------------ -------//

            console.log('PersonalDataSettingEditorController.');
        }]);
})();