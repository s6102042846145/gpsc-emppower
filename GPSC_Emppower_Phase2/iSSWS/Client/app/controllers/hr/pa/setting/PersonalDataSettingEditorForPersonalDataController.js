﻿(function () {
    angular.module('ESSMobile')
        .controller('PersonalDataSettingEditorForPersonalDataController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {
            $scope.PATextcategory = 'PERSONALADMINISTRATION';
            $scope.content.Header = $scope.Text[$scope.PATextcategory].CONSOLE_HEADSUBJECT;
            $scope.Gender = [];
            $scope.Prefix = [];
            $scope.MaritalStatus = [];
            $scope.Nationality = [];
            $scope.Religion = [];
            $scope.LANGUAGETextcategory = 'LANGUAGE';
            $scope.Language = [];

            $scope.Path = "views/hr/pa/setting/partial/PersonEditor.html";
            $scope.ShowEmployeeID = true;
            $scope.CanEditPeriod = false;
            $scope.itemKey = angular.isUndefined($routeParams.itemKey) && $routeParams.itemKey != '0' ? 'null' : $routeParams.itemKey.split('|')[0];
            $scope.BeginDate = angular.isUndefined($routeParams.itemKey) && $routeParams.itemKey != '0' ? 'null' : $scope.DOB = $filter('date')($routeParams.itemKey.split('|')[1], 'yyyy-MM-dd');
            $scope.PersonData;
            $scope.NoSpecify = {
                val: false
            };
            $scope.setSelectedDOB = function (selectedDate) {
                $scope.PersonData.DOB = $filter('date')(selectedDate, 'yyyy-MM-dd');
                var datecompare1 = new Date($scope.PersonData.DOB);
                var datecompare2 = new Date();
                if (datecompare1 > datecompare2) {
                    $scope.PersonData.DOB = datecompare2.getFullYear() + '-' + ("0" + (datecompare2.getMonth() + 1)).slice(-2) + '-' + ("0" + datecompare2.getDate()).slice(-2);
                }
            };
            $scope.setSelectedBeginDate = function (selectedDate) {
                $scope.PersonData.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };
            $scope.setSelectedEndDate = function (selectedDate) {
                $scope.PersonData.EndDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
                console.log(($scope.PersonData.EndDate == '9999-12-31'));
                $scope.NoSpecify.val = ($scope.PersonData.EndDate == '9999-12-31');
            };
            $scope.onCheckBoxChange = function () {

                if ($scope.NoSpecify.val == true) {
                    $scope.PersonData.EndDate = '9999-12-31';
                } else {
                    $scope.PersonData.EndDate = $scope.PersonData.BeginDate;
                }
            }
            $scope.onClickEditSave = function () {
                var oReturn = true;
                if ($scope.PersonData.TitleID.trim().length <= 0) oReturn = false;
                if ($scope.PersonData.FirstName.trim().length <= 0) oReturn = false;
                if ($scope.PersonData.LastName.trim().length <= 0) oReturn = false;
                if ($scope.PersonData.FullNameEn.trim().length <= 0) oReturn = false;
                if ($scope.PersonData.NickName.trim().length <= 0) oReturn = false;
                if ($scope.PersonData.DOB.trim().length <= 0) oReturn = false;
                if ($scope.PersonData.MaritalStatus.trim().length <= 0) oReturn = false;
                if ($scope.PersonData.Nationality.trim().length <= 0) oReturn = false;
                if ($scope.PersonData.Religion.trim().length <= 0) oReturn = false;
                if ($scope.PersonData.Language.trim().length <= 0) oReturn = false;
                if (oReturn == true) {
                    console.log($scope.PersonData);
                    // ------ update data to database -------//
                    var oRequestParameter = {
                        InputParameter: { "EmployeeID": $scope.itemKey, "PersonData": $scope.PersonData }
                                                , CurrentEmployee: getToken(CONFIG.USER)
                    };
                    var URL = CONFIG.SERVER + 'Employee/UpdateINFOTYPE0002And0182';
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        alert('COMPLETE');
                        $location.path('/frmViewContent/409/' + $scope.itemKey);
                    }, function errorCallback(response) {
                        // Error
                        console.log('error RequestController.', response);
                    });
                    // ------ ------------------------------------ -------//
                }
            };

            $scope.onClickCancel = function () {
                $location.path('/frmViewContent/409/' + $scope.itemKey);
            }

            $scope.onClickAddSave = function () {
                var oReturn = true;
                if ($scope.PersonData.TitleID.trim().length <= 0) oReturn = false;
                if ($scope.PersonData.FirstName.trim().length <= 0) oReturn = false;
                if ($scope.PersonData.LastName.trim().length <= 0) oReturn = false;
                if ($scope.PersonData.FullNameEn.trim().length <= 0) oReturn = false;
                if ($scope.PersonData.NickName.trim().length <= 0) oReturn = false;
                if ($scope.PersonData.DOB.trim().length <= 0) oReturn = false;
                if ($scope.PersonData.MaritalStatus.trim().length <= 0) oReturn = false;
                if ($scope.PersonData.Nationality.trim().length <= 0) oReturn = false;
                if ($scope.PersonData.Religion.trim().length <= 0) oReturn = false;
                if ($scope.PersonData.Language.trim().length <= 0) oReturn = false;
                if (oReturn == true) {
                    //console.log($scope.PersonData);
                    var oRequestParameterr = {
                        InputParameter: { "EmployeeID": $scope.itemKey, "DataObjectType": "Person", "BeginDate": $scope.PersonData.BeginDate.toString('yyyy-MM-dd'), "EndDate": $scope.PersonData.EndDate }
                                                , CurrentEmployee: getToken(CONFIG.USER)
                    };
                    var urlll = CONFIG.SERVER + 'Employee/CheckStatus';
                    $http({
                        method: 'POST',
                        url: urlll,
                        data: oRequestParameterr
                    }).then(function successCallback(response) {
                        // Success
                        console.log('eiei', response);
                        var oRequestParameter = {
                            InputParameter: { "EmployeeID": $scope.itemKey, "PersonData": $scope.PersonData }
                                                , CurrentEmployee: getToken(CONFIG.USER)
                        };
                        var insertURL = CONFIG.SERVER + 'Employee/InsertINFOTYPE0002And0182';
                        if (response.data.CanInsert == true && response.data.Duplicate == true) {
                            if (confirm($scope.Text['PERSONALADMINISTRATION'].DuplicatePeriod) == true) {
                                InsertINFOTYPE0002(oRequestParameter, insertURL);
                                //$location.path('/frmViewContent/409/' + $scope.itemKey);
                            }
                        }
                        else if (response.data.CanInsert == true) {
                            InsertINFOTYPE0002(oRequestParameter, insertURL);
                            //$location.path('/frmViewContent/409/' + $scope.itemKey);
                        }
                        else {
                            alert($scope.Text['PERSONALADMINISTRATION'].StaccatoPeriod);
                        }
                    }, function errorCallback(response) {
                        // Error
                        console.log('error RequestController.', response);
                    });
                }
            };

            if ($scope.BeginDate != null && $scope.BeginDate != '') {
                //if ($routeParams.itemKey.split('|')[2] != null) { $scope.CanEditPeriod = true; }
                $scope.CanEditPeriod = true;
                // ------ get person data with employeeid and begindate -------//
                var oRequestParameter = {
                    InputParameter: { "EmployeeID": $scope.itemKey, "BeginDate": $scope.BeginDate }
                                            , CurrentEmployee: getToken(CONFIG.USER)
                };
                //alert(oRequestParameter.InputParameter["EmployeeID"]);
                var URL = CONFIG.SERVER + 'Employee/INFOTYPE0002And0182Get';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success $filter('date')(response.data.BeginDate, 'yyyy-MM-dd')
                    console.log('GetPersonDataEdit.', response.data)
                    $scope.PersonData = response.data;
                    $scope.PersonData.EmployeeID = $scope.itemKey;
                    $scope.PersonData.BeginDate = $filter('date')(response.data.BeginDate, 'yyyy-MM-dd');
                    $scope.PersonData.EndDate = $filter('date')(response.data.EndDate, 'yyyy-MM-dd');
                    if ($scope.PersonData.DOB != '0001-01-01T00:00:00Z') { $scope.PersonData.DOB = $filter('date')($scope.PersonData.DOB, 'yyyy-MM-dd'); }
                    else { $scope.PersonData.DOB = ''; }
                    console.log($scope.PersonData.EndDate);
                    $scope.NoSpecify.val = ($scope.PersonData.EndDate == '9999-12-31');

                    
                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                    //$scope.PersonData = null;
                });
                // ------ ------------------------------------ -------//
            }
            else {
                var date = new Date();
                var dateStr = date.getFullYear() + '-' + ("0" + (date.getMonth() + 1)).slice(-2) + '-' + ("0" + date.getDate()).slice(-2);
                $scope.PersonData = {
                    EmployeeID: $scope.itemKey,
                    BeginDate: dateStr,
                    EndDate: '9999-12-31',
                    TitleID: '',
                    FirstName: '',
                    LastName: '',
                    NickName: '',
                    DOB: '',
                    Gender: '1',
                    MaritalStatus: '',
                    Nationality: '',
                    Language: '',
                    Religion: ''
                };
                $scope.NoSpecify.val = ($scope.PersonData.EndDate == '9999-12-31');
                $scope.CanEditPeriod = true;
            }
            // ------ get all select data -------//
            var oRequestParameter = { InputParameter: {  }
                                    , CurrentEmployee: getToken(CONFIG.USER)
            };
            var URL = CONFIG.SERVER + 'Employee/GetPersonSelectData';
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                console.log('select-datana.', response.data);
                $scope.Prefix = response.data.Prefix;
                $scope.Gender = response.data.Gender;
                $scope.MaritalStatus = response.data.MaritalStatus;
                $scope.Nationality = response.data.Nationality;
                $scope.Language = response.data.Language;
                $scope.Religion = response.data.Religion;
                if ($scope.PersonData.TitleID != '') $scope.PersonData.TitleID = $scope.Prefix[findWithAttr($scope.Prefix, 'AlternativeShortName', $scope.PersonData.TitleID)].ObjectID;
                if ($scope.PersonData.Gender != '') $scope.PersonData.Gender = $scope.Gender[findWithAttr($scope.Gender, 'AlternativeShortName', $scope.PersonData.Gender)].ObjectID;
                if ($scope.PersonData.MaritalStatus != '') $scope.PersonData.MaritalStatus = $scope.MaritalStatus[findWithAttr($scope.MaritalStatus, 'AlternativeShortName', $scope.PersonData.MaritalStatus)].ObjectID;
                if ($scope.PersonData.Nationality != '') $scope.PersonData.Nationality = $scope.Nationality[findWithAttr($scope.Nationality, 'AlternativeShortName', $scope.PersonData.Nationality)].ObjectID;
                if ($scope.PersonData.Language != '') $scope.PersonData.Language = $scope.Language[findWithAttr($scope.Language, 'AlternativeShortName', $scope.PersonData.Language)].ObjectID;
                if ($scope.PersonData.Religion != '') $scope.PersonData.Religion = $scope.Religion[findWithAttr($scope.Religion, 'AlternativeShortName', $scope.PersonData.Religion)].ObjectID;
                
                console.log('PersonData.', $scope.PersonData);
            }, function errorCallback(response) {
                // Error
                console.log('error RequestController.', response);
            });

            function InsertINFOTYPE0002(oRequestParameter, URL) {
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    alert('COMPLETE');
                    $location.path('/frmViewContent/409/' + oRequestParameter.InputParameter["EmployeeID"]);
                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                });
                // ------ ------------------------------------ -------//
            }
            // ------ ------------------------------------ -------//
            function findWithAttr(array, attr, value) {
                for (var i = 0; i < array.length; i += 1) {
                    if (array[i][attr] === value) {
                        return i;
                    }
                }
            }

            console.log('PersonalDataSettingEditorForPersonalDataController.');

        }]);
})();

