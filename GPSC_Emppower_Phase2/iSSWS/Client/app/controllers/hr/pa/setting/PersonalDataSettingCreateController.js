﻿(function () {
    angular.module('ESSMobile')
        .controller('PersonalDataSettingCreateController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {
            $scope.PATextcategory = 'PERSONALADMINISTRATION';
            $scope.content.Header = $scope.Text[$scope.PATextcategory].CONSOLE_HEADSUBJECT;
            $scope.IsNewEmployeeID = true;
            $scope.CanEditPeriod = true;
            $scope.emailPattern = "/^([a-zA-Z0-9])+([a-zA-Z0-9._%+-])+@([a-zA-Z0-9_.-])+\.(([a-zA-Z]){2,6})$/";
            $scope.count = 0;
            $scope.UrlArray = [{ Name: "PersonalData", Url: "PersonEditor" }
                              , { Name: "OrganizationData", Url: "OrganizationEditor" }
                              , { Name: "ContactData", Url: "ContactEditor" }];
            $scope.Path = "views/hr/pa/setting/partial/" + $scope.UrlArray[$scope.count].Url + ".html";
            $scope.onSelectedArea = function (ObjectID) {
                $scope.SubAreaSelector = $scope.SubAreaObject[ObjectID];
            }

            $scope.onClickCancel = function () {
                $location.path('/frmViewContent/401');
            }

            $scope.Next = function () {
                var oReturn = true;
                if ($scope.count == 0) {
                    if ($scope.PersonData.EmployeeID.trim().length < 8) oReturn = false;
                    if ($scope.PersonData.TitleID.trim().length <= 0) oReturn = false;
                    if ($scope.PersonData.FirstName.trim().length <= 0) oReturn = false;
                    if ($scope.PersonData.LastName.trim().length <= 0) oReturn = false;
                    if ($scope.PersonData.FullNameEn.trim().length <= 0) oReturn = false;
                    if ($scope.PersonData.NickName.trim().length <= 0) oReturn = false;
                    if ($scope.PersonData.DOB.trim().length <= 0) oReturn = false;
                    if ($scope.PersonData.MaritalStatus.trim().length <= 0) oReturn = false;
                    if ($scope.PersonData.Nationality.trim().length <= 0) oReturn = false;
                    if ($scope.PersonData.Religion.trim().length <= 0) oReturn = false;
                    if ($scope.PersonData.Language.trim().length <= 0) oReturn = false;
                    if (oReturn == true) {
                        //SaveINFOTYPE0002And0182();

                        getOrganizationSelectData();
                        if (angular.isDefined($scope.OrgData)) $scope.OrgData.Name = $scope.Prefix[findWithAttr($scope.Prefix, 'ObjectID', $scope.PersonData.TitleID)].AlternativeShortName + $scope.PersonData.FirstName + ' ' + $scope.PersonData.LastName;
                    }
                }
                else if ($scope.count == 1) {
                    //Name: '',
                    //Area: '',
                    //SubArea: '',
                    //EmpGroup: '',
                    //EmpSubGroup: '',
                    //OrganizationName: '',
                    //CostCenter: '',
                    //Position: ''
                    if ($scope.OrgData.Name.trim().length <= 0) oReturn = false;
                    if ($scope.OrgData.Area.trim().length <= 0) oReturn = false;
                    if ($scope.OrgData.SubArea.trim().length <= 0) oReturn = false;
                    if ($scope.OrgData.EmpGroup.trim().length <= 0) oReturn = false;
                    if ($scope.OrgData.EmpSubGroup.trim().length <= 0) oReturn = false;
                    if ($scope.OrgData.OrgUnit.trim().length <= 0) oReturn = false;
                    if ($scope.OrgData.CostCenter.trim().length <= 0) oReturn = false;
                    if ($scope.OrgData.Position.trim().length <= 0) oReturn = false;
                    if (oReturn == true) {

                        getContactSelectData();
                    }
                }
                if (oReturn == true) {
                    $scope.count = $scope.count + 1;
                    $scope.Path = "views/hr/pa/setting/partial/" + $scope.UrlArray[$scope.count].Url + ".html";

                }
            }

            $scope.onClickSave = function (ev) {
                var oReturn = true;
                if ($scope.ContData.Name.trim().length <= 0) oReturn = false;
                if ($scope.ContData.CategoryCode.trim().length <= 0) oReturn = false;
                if ($scope.ContData.DataText.trim().length <= 0) oReturn = false;
                if (oReturn == true) {
                    InsertINFOTYPE0002And0182And0001And0105(ev);
                    //alert('oh yeahh!');
                }
            }

            $scope.Previous = function () {
                $scope.count = $scope.count - 1;
                $scope.Path = "views/hr/pa/setting/partial/" + $scope.UrlArray[$scope.count].Url + ".html";
            }
            $scope.Gender = [];
            $scope.Prefix = [];
            $scope.MaritalStatus = [];
            $scope.Nationality = [];
            $scope.Religion = [];
            $scope.Language = [];
            $scope.EmpGroup = [];
            $scope.EmpSubGroup = [];
            $scope.SubType = [];
            $scope.CostCenterSelector = [];
            $scope.PositionSelector = [];
            $scope.OrgUnitSelector = [];
            $scope.PersonData;
            $scope.OrgData;
            $scope.ContData;
            $scope.NoSpecify = {
                val: false
            };

            $scope.getPosition = function (Organize) {
                var URL = CONFIG.SERVER + 'Employee/GetPositionByOrganizeForHRMaster';
                var oRequestParameter = { InputParameter: { "Organize": Organize }, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.PositionSelector = response.data;
                    if (angular.isDefined($routeParams.itemKey)) {
                        if ($scope.OrgData.Position.length > 0) $scope.OrgData.Position = $scope.PositionSelector[findWithAttr($scope.PositionSelector, 'AlternativeShortName', $scope.OrgData.Position)].ObjectID;
                        //$scope.getEmpSubGroup($scope.OrgData.Position);
                    }
                    else {
                        $scope.OrgData.Position = '';
                        $scope.getCostCenter(Organize);
                    }

                }, function errorCallback(response) {
                    console.log('error Position list', response);

                });
            };

            $scope.getCostCenter = function (Organize) {
                var URL = CONFIG.SERVER + 'Employee/GetCostCenterByOrganize';
                var oRequestParameter = { InputParameter: { "Organize": Organize }, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.CostCenterSelector = response.data;
                    if (angular.isDefined($routeParams.itemKey)) {
                        if ($scope.OrgData.CostCenter.length > 0) $scope.OrgData.CostCenter = $scope.CostCenterSelector[findWithAttr($scope.CostCenterSelector, 'AlternativeShortName', $scope.OrgData.CostCenter)].ObjectID;
                        //$scope.getEmpSubGroup($scope.OrgData.Position);
                    }
                    else {
                        $scope.OrgData.CostCenter = '';
                    }
                }, function errorCallback(response) {
                    console.log('error Position list', response);

                });
            };

            $scope.setSelectedDOB = function (selectedDate) {
                $scope.PersonData.DOB = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };
            $scope.setSelectedBeginDate = function (selectedDate) {
                if ($scope.count == 0) {
                    $scope.PersonData.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
                }
                else if ($scope.count == 1) {
                    $scope.OrgData.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
                }
                else {
                    $scope.ContData.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
                }
            };
            $scope.setSelectedEndDate = function (selectedDate) {
                if ($scope.count == 0) {
                    $scope.PersonData.EndDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
                    $scope.NoSpecify.val = ($scope.PersonData.EndDate == '9999-12-31');
                }
                else if ($scope.count == 1) {
                    $scope.OrgData.EndDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
                    $scope.NoSpecify.val = ($scope.OrgData.EndDate == '9999-12-31');
                }
                else {
                    $scope.ContData.EndDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
                    $scope.NoSpecify.val = ($scope.ContData.EndDate == '9999-12-31');
                }

            };
            $scope.onCheckBoxChange = function () {
                if ($scope.NoSpecify.val == true) {
                    if ($scope.count == 0) {
                        $scope.PersonData.EndDate = '9999-12-31';
                    }
                    else if ($scope.count == 1) {
                        $scope.OrgData.EndDate = '9999-12-31';
                    }
                    else {
                        $scope.ContData.EndDate = '9999-12-31';
                    }
                } else {
                    if ($scope.count == 0) {
                        $scope.PersonData.EndDate = $scope.PersonData.BeginDate;
                    }
                    else if ($scope.count == 1) {
                        $scope.OrgData.EndDate = $scope.OrgData.BeginDate;
                    }
                    else {
                        $scope.ContData.EndDate = $scope.ContData.BeginDate;
                    }
                }
            }

            $scope.getEmpSubGroup = function (Position) {
                var oRequestParameter = {
                    InputParameter: { "ObjectID": Position, "BeginDate": "9999-12-31", "EndDate": "9999-12-31" }
                , CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER)
                };
                var URL = CONFIG.SERVER + 'Employee/INFOTYPE1013Get';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    console.log('select-datana.', response.data);
                    $scope.OrgData.EmpGroup = response.data.EmpGroup;
                    $scope.OrgData.EmpSubGroup = response.data.EmpSubGroup;
                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                });
            }

            var oRequestParameter = {
                InputParameter: {}
                , CurrentEmployee: getToken(CONFIG.USER)
            };
            var URL = CONFIG.SERVER + 'Employee/GetPersonSelectData/';
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                console.log('select-datana.', response.data);
                $scope.Prefix = response.data.Prefix;
                $scope.Gender = response.data.Gender;
                $scope.MaritalStatus = response.data.MaritalStatus;
                $scope.Nationality = response.data.Nationality;
                $scope.Language = response.data.Language;
                $scope.Religion = response.data.Religion;
                console.log('data.', $scope.TitleID)
                if (angular.isDefined($routeParams.itemKey)) {
                    $scope.getEmployeeData($routeParams.itemKey);
                }
                else {
                    var date = new Date();
                    var dateStr = date.getFullYear() + '-' + ("0" + (date.getMonth() + 1)).slice(-2) + '-' + ("0" + date.getDate()).slice(-2);
                    $scope.PersonData = {
                        EmployeeID: null,
                        BeginDate: dateStr,
                        EndDate: '9999-12-31',
                        TitleID: '',
                        FirstName: '',
                        LastName: '',
                        NickName: '',
                        DOB: '',
                        Gender: '1',
                        MaritalStatus: '',
                        Nationality: '',
                        Language: '',
                        Religion: ''
                    };
                    $scope.NoSpecify.val = ($scope.PersonData.EndDate == '9999-12-31');
                }

                $scope.CanEditPeriod = true;
            }, function errorCallback(response) {
                // Error
                console.log('error RequestController.', response);
            });

            $scope.getEmployeeData = function (EmployeeID) {
                var oRequestParameter = {
                    InputParameter: { "EmployeeID": EmployeeID }
                , CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER)
                };
                var URL = CONFIG.SERVER + 'Employee/GetEmployeeAllData';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    console.log('select-datana.', response.data);
                    $scope.PersonData = response.data.PersonData;
                    $scope.OrgData = response.data.OrgData;
                    $scope.ContData = response.data.ContData;
                    //$scope.SubAreaSelector = response.data.SubArea[$scope.OrgData.Area];
                    if ($scope.PersonData.TitleID != '') $scope.PersonData.TitleID = $scope.Prefix[findWithAttr($scope.Prefix, 'AlternativeShortName', $scope.PersonData.TitleID)].ObjectID;
                    if ($scope.PersonData.Gender != '') $scope.PersonData.Gender = $scope.Prefix[findWithAttr($scope.Gender, 'AlternativeShortName', $scope.PersonData.Gender)].ObjectID;
                    if ($scope.PersonData.MaritalStatus != '') $scope.PersonData.MaritalStatus = $scope.MaritalStatus[findWithAttr($scope.MaritalStatus, 'AlternativeShortName', $scope.PersonData.MaritalStatus)].ObjectID;
                    if ($scope.PersonData.Nationality != '') $scope.PersonData.Nationality = $scope.Nationality[findWithAttr($scope.Nationality, 'AlternativeShortName', $scope.PersonData.Nationality)].ObjectID;
                    if ($scope.PersonData.Language != '') $scope.PersonData.Language = $scope.Language[findWithAttr($scope.Language, 'AlternativeShortName', $scope.PersonData.Language)].ObjectID;
                    if ($scope.PersonData.Religion != '') $scope.PersonData.Religion = $scope.Religion[findWithAttr($scope.Religion, 'AlternativeShortName', $scope.PersonData.Religion)].ObjectID;
                    $scope.PersonData.BeginDate = $filter('date')($scope.PersonData.BeginDate, 'yyyy-MM-dd');
                    $scope.PersonData.EndDate = $filter('date')($scope.PersonData.EndDate, 'yyyy-MM-dd');
                    if ($scope.PersonData.DOB != '0001-01-01T00:00:00Z') { $scope.PersonData.DOB = $filter('date')($scope.PersonData.DOB, 'yyyy-MM-dd'); }
                    else { $scope.PersonData.DOB = ''; }
                    $scope.NoSpecify.val = ($scope.PersonData.EndDate == '9999-12-31');
                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                });
            }

            function InsertINFOTYPE0002And0182And0001And0105(ev) {
                // ------ insert data to database -------//
                //$scope.OrgData.Area = $scope.OrgData.Area.ObjectID;
                //$scope.OrgData.SubArea = $scope.OrgData.SubArea.ObjectID;
                //$scope.OrgData.OrgUnit = $scope.OrgData.OrgUnit.ObjectID;
                //$scope.OrgData.Position = $scope.OrgData.Position.ObjectID;
                //$scope.OrgData.CostCenter = $scope.OrgData.CostCenter.ObjectID;
                var oRequestParameter = {
                    InputParameter: { "PersonData": $scope.PersonData, "OrgData": $scope.OrgData, "ContData": $scope.ContData }
                , CurrentEmployee: getToken(CONFIG.USER)
                };
                var URL = CONFIG.SERVER + 'Employee/InsertINFOTYPE0002And0182And0001And0105/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    if (response.data.length > 7) {
                        $mdDialog.show(
                                $mdDialog.alert()
                                    .clickOutsideToClose(true)
                                    .title($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                    .textContent($scope.Text['ACCOUNT_SETTING'].GO_BACK)
                                    .ariaLabel($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                    .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                    .targetEvent(ev));
                        $scope.PersonData.EmployeeID = response.data;
                        $location.path('/frmViewContent/409/' + response.data);
                    }
                    else {
                        $mdDialog.show(
                                $mdDialog.alert()
                                    .clickOutsideToClose(true)
                                    .title($scope.Text['ACCOUNT_SETTING'].CAN_NOT_DELETE)
                                    .textContent($scope.Text['ACCOUNT_SETTING'].DATA_DUPLICATE)
                                    .ariaLabel($scope.Text['ACCOUNT_SETTING'].CAN_NOT_DELETE)
                                    .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                    .targetEvent(ev)
                            );
                    }
                    //alert('COMPLETE');
                    //console.log('COMPLETE', response.data);

                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                });
                // ------ ------------------------------------ -------//
            }

            function getOrganizationSelectData() {
                var oRequestParameter = {
                    InputParameter: {}
                , CurrentEmployee: getToken(CONFIG.USER)
                };
                var URL = CONFIG.SERVER + 'Employee/GetOrganizationSelectData';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    console.log('select-datana.', response.data);
                    $scope.EmpGroup = response.data.EmpGroup;
                    $scope.EmpSubGroup = response.data.EmpSubGroup;
                    //$scope.CostCenterSelector = response.data.CostCenter;
                    //$scope.PositionSelector = response.data.Position;
                    $scope.OrgUnitSelector = response.data.Organize;
                    $scope.AreaSelector = response.data.Area;
                    $scope.SubAreaObject = response.data.SubArea;
                    if (angular.isUndefined($scope.OrgData)) {
                        $scope.OrgData = {
                            EmployeeID: $scope.PersonData.EmployeeID,
                            BeginDate: $scope.PersonData.BeginDate,
                            EndDate: $scope.PersonData.EndDate,
                            Name: $scope.Prefix[findWithAttr($scope.Prefix, 'ObjectID', $scope.PersonData.TitleID)].AlternativeShortName + $scope.PersonData.FirstName + ' ' + $scope.PersonData.LastName,
                            Area: '',
                            SubArea: '',
                            EmpGroup: '',
                            EmpSubGroup: '',
                            OrgUnit: '',
                            CostCenter: '',
                            Position: ''
                        };
                    }
                    else {
                        $scope.OrgData.Area = $scope.AreaSelector[findWithAttr($scope.AreaSelector, 'AlternativeShortName', $scope.OrgData.Area)].ObjectID;
                        $scope.SubAreaSelector = response.data.SubArea[$scope.OrgData.Area];
                        $scope.OrgData.SubArea = $scope.SubAreaSelector[findWithAttr($scope.SubAreaSelector, 'AlternativeShortName', $scope.OrgData.SubArea)].ObjectID;
                        $scope.OrgData.OrgUnit = $scope.OrgUnitSelector[findWithAttr($scope.OrgUnitSelector, 'AlternativeShortName', $scope.OrgData.OrgUnit)].ObjectID;
                        $scope.getPosition($scope.OrgData.OrgUnit);
                        $scope.getCostCenter($scope.OrgData.OrgUnit);
                        $scope.OrgData.EmpGroup = $scope.OrgData.EmpGroup.split(':')[0];
                        if ($scope.OrgData.EmpSubGroup.length == 1) $scope.OrgData.EmpSubGroup = "0" + $scope.OrgData.EmpSubGroup.toString();
                        //$scope.OrgData.EmpGroup = $scope.EmpGroup[findWithAttr($scope.EmpGroup, 'ObjectID', $scope.OrgData.EmpGroup.split(':')[0])].ObjectID;
                        //$scope.OrgData.EmpSubGroup = $scope.EmpSubGroup[findWithAttr($scope.EmpSubGroup, 'ObjectID', $scope.OrgData.EmpSubGroup)].ObjectID;
                        //$scope.OrgData.Position = $scope.PositionSelector[findWithAttr($scope.PositionSelector, 'AlternativeShortName', $scope.OrgData.Position)].ObjectID;
                        //$scope.OrgData.CostCenter = $scope.CostCenterSelector[findWithAttr($scope.CostCenterSelector, 'AlternativeShortName', $scope.OrgData.CostCenter)].ObjectID;
                    }
                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                });
            }

            function getContactSelectData() {
                var oRequestParameter = {
                    InputParameter: {}
                , CurrentEmployee: getToken(CONFIG.USER)
                };
                var URL = CONFIG.SERVER + 'Employee/GetContactSelectData';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    console.log('select-datana.', response.data);
                    $scope.SubType = response.data.SubType;
                    if (angular.isUndefined($scope.ContData)) {
                        $scope.ContData = {
                            EmployeeID: $scope.PersonData.EmployeeID,
                            BeginDate: $scope.PersonData.BeginDate,
                            EndDate: $scope.PersonData.EndDate,
                            Name: $scope.OrgData.Name,
                            CategoryCode: '',
                            DataText: ''
                        };
                    }
                    else {
                        $scope.ContData.BeginDate = $filter('date')($scope.ContData.BeginDate, 'yyyy-MM-dd');
                        $scope.ContData.EndDate = $filter('date')($scope.ContData.EndDate, 'yyyy-MM-dd');
                    }
                }, function errorCallback(response) {
                    // Error
                    console.log('error RequestController.', response);
                });
            }

            function findWithAttr(array, attr, value) {
                for (var i = 0; i < array.length; i += 1) {
                    if (array[i][attr] === value) {
                        return i;
                    }
                }
            }
            console.log('PersonalDataSettingCreateController.');

        }]);
})();