﻿(function () {
    angular.module('ESSMobile')
        .controller('PersonalFamilyContentController', ['$scope', '$http', '$routeParams', '$location', 'DTOptionsBuilder', '$route', 'CONFIG', '$mdDialog', '$filter', '$timeout', '$q', '$log', function ($scope, $http, $routeParams, $location, DTOptionsBuilder, $route, CONFIG, $mdDialog, $filter, $timeout, $q, $log) {
            //#### FRAMEWORK FUNCTION ### START
            /******************* variable start ********************/
            $scope.personalfamilyloader = false;
            $scope.Textcategory = "FAMILY";
            $scope.FAMILYMEMBER = $scope.Text["FAMILYMEMBER"];
            $scope.COUNTRY = $scope.Text["COUNTRY"];
            $scope.NATIONALITY = $scope.Text["NATIONALITY"];
            $scope.TITLENAME = $scope.Text["TITLENAME"];
            $scope.content.isShowHeader = true;

            /******************* variable end ********************/


            /******************* listener start ********************/
            $scope.init = function () {
                init();
            }

            /******************* listener end ********************/


            /******************* action start ********************/
            function init() {
                // debug mode
                getPAConfiguration();
                getFamilyData();
            }

            /******************* action end ********************/


            /******************* service caller  start ********************/
            function getPAConfiguration() {
                var oRequestParameter = {
                    InputParameter: { "CategoryName": "Family" }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };
                var URL = CONFIG.SERVER + 'HRPA/GetPAConfigurationList/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.PAConfiguration = response.data;
                }, function errorCallback(response) {
                    // Error
                    console.log('error getPAConfiguration.', response);
                });
            }

            function getFamilyData() {
                // ------ get person data with employeeid and begindate -------//
                var oEmployeeData = getToken(CONFIG.USER);
                var checkDate = $filter('date')(new Date(), 'yyyy-MM-dd');
                var URL = CONFIG.SERVER + 'HRPA/GetPersonalFamily';
                var oRequestParameter = {
                    InputParameter: { }
                    , CurrentEmployee: oEmployeeData
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                    //data: getToken(CONFIG.USER)
                }).then(function successCallback(response) {
                    // Success
                    $scope.FamilyMemberList = response.data.FamilyData;
                    $scope.DictFileAttachmentList = response.data.dictFileAttachmentList;
                }, function errorCallback(response) {
                });
            }

            /******************* service caller  end ********************/


            //#### FRAMEWORK FUNCTION ### END

            //#### OTHERS FUNCTION ### START

            //Do something ...

            //#### OTHERS FUNCTION ### END
        }]);
})();