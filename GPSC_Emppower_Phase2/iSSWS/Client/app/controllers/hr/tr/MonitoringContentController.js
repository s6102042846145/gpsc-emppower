﻿(function () {
    angular.module('ESSMobile')
        .controller('MonitoringContentController', ['$scope', '$http', '$routeParams', '$location', 'DTOptionsBuilder', '$route', 'CONFIG', '$mdDialog', '$filter', '$timeout', '$q', '$log', function ($scope, $http, $routeParams, $location, DTOptionsBuilder, $route, CONFIG, $mdDialog, $filter, $timeout, $q, $log) {
            //['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$timeout', '$q', '$log', 'DTOptionsBuilder', 'DTColumnDefBuilder', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $timeout, $q, $log, DTOptionsBuilder, DTColumnDefBuilder) {


            $scope.Textcategory = 'MONITORING';
            $scope.content.isShowHeader = false;
            $scope.HeaderText = $scope.Text[$scope.Textcategory].CONSOLE_HEADSUBJECT;
            $scope.content.Header = $scope.HeaderText;

            $scope.profile = (getToken(CONFIG.USER));
            $scope.settings = {
                UserRoles: null
            };



            $scope.requestTypes = {
                selected: -1
            };
            $scope.requestType = [];

            $scope.formData = {
                requestType: null,
                requestNoFrom: '',
                requestNoTo: '',
                beginDate: '',
                endDate: '',
                userRoles: null,
                requestEmpNo: ''
            };

            $scope.setDefaultUserRoles = function () {
                var oRequestParameter = {
                    InputParameter: {}, CurrentEmployee: $scope.employeeData, Requestor: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER)
                };
                var URL = CONFIG.SERVER + 'HRTR/GetUserRoleForMonitoring';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data != null) {
                        $scope.settings.UserRoles = response.data;
                        $scope.initSettingSearch();
                        //$scope.formData.userRoles = response.data[0];
                        //$scope.formDataUserRoleDescription = response.data[0].TextDescription;
                    }

                }, function errorCallback(response) {

                });

                //if ($scope.profile.UserRoles != null && $scope.profile.UserRoles.length > 0) {
                //    $scope.settings.UserRoles = $scope.profile.UserRoles;
                //    $scope.formData.userRoles = $scope.settings.UserRoles[0];
                //}
            }
            $scope.formDataUserRoleDescription = "";
            $scope.getRoleDescription = function () {
                var UserRolejson = JSON.parse($scope.formData.userRoles);
                for (var i = 0 ; $scope.settings.UserRoles.length; i++) {
                    if ($scope.settings.UserRoles[i].Role == UserRolejson.Role) {
                        $scope.formDataUserRoleDescription = $scope.settings.UserRoles[i].TextDescription;
                        break;
                    }
                }

            }


            $scope.setDefaultUserRoles();

            console.log('oProfile', $scope.profile);

            $scope.setSelectedBeginDate = function (selectedDate) {
                $scope.formData.beginDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
                if ($scope.formData.beginDate > $scope.formData.endDate) {
                    $scope.formData.endDate = $scope.formData.beginDate;
                }
            };

            $scope.setSelectedEndDate = function (selectedDate) {
                $scope.formData.endDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };

            $scope.clickSearchMonitoring = function () {
                $scope.searchMonitoring();
            };

            $scope.getRequestType = function () {
                var oRequestParameter = {
                    InputParameter: {}, CurrentEmployee: $scope.employeeData, Requestor: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER)
                };
                var URL = CONFIG.SERVER + 'workflow/GetAllRequestType';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data != null) {
                        $scope.requestType = response.data;
                    }

                }, function errorCallback(response) {

                });
            }
            $scope.getRequestType();


            $scope.loaderSearching = false;
            $scope.searchMonitoring = function () {
                var oCondition;
                var URL;
                var tryUserRole;
                try {
                    tryUserRole = JSON.parse($scope.formData.userRoles).Role;
                }
                catch (e) {
                    tryUserRole = $scope.formData.userRoles.Role;
                }
                oCondition = {

                    requestType: (($scope.requestTypes.selected != null) ? $scope.requestTypes.selected : -1),
                    requestNoFrom: (($scope.formData.requestNoFrom == '') ? 'null' : $scope.formData.requestNoFrom),
                    //  requestNoTo: (($scope.formData.requestNoTo == '') ? 'null' : $scope.formData.requestNoTo),
                    beginDate: (($scope.formData.beginDate == '') ? 'null' : $scope.formData.beginDate),
                    endDate: (($scope.formData.endDate == '') ? 'null' : $scope.formData.endDate),
                    userRoles: (($scope.formData.userRoles == '') ? 'null' : tryUserRole),
                    requestEmpNo: (($scope.formData.requestEmpNo == '') ? 'null' : $scope.formData.requestEmpNo)
                };
                setFromParamsUrl();
                URL = CONFIG.SERVER + 'HRTR/SearchMonitoring/';
                var oRequestParameter = {
                    InputParameter: {
                        "requestType": oCondition.requestType, "requestNoFrom": oCondition.requestNoFrom, "requestNoTo": "null",
                        "beginDate": oCondition.beginDate, "endDate": oCondition.endDate, "userRoles": oCondition.userRoles, "requestEmpNo": oCondition.requestEmpNo
                    }, CurrentEmployee: $scope.employeeData
                };
                $scope.loaderSearching = true;
                $scope.Monitor = [];
                filterArry('');
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.Monitor = response.data;
                    $scope.loaderSearching = false;
                    filterArry('');
                    console.log('oMoni', $scope.Monitor);
                }, function errorCallback(response) {
                    $scope.loaderSearching = false;
                    console.log('error MonitoringContentController SearchMonitoring.', response);
                });
            };





            $scope.selectedItemChange = function (text) {


                if (angular.isDefined(text)) {
                    var activeElement = document.activeElement;
                    console.debug(activeElement);
                    if (activeElement) {
                        activeElement.blur();
                    }
                    $scope.requestTypes.selected = text.RequestTypeID;
                }
                else {
                    $scope.requestTypes.selected = -1;
                }
                console.log('oooReq', $scope.requestTypes.selected);
            }

            $scope.searchTextChange = function (text) {

            }
            $scope.searchText = "";
            $scope.clearInput = function () {
                $scope.requestTypes.selected = -1;
                $scope.searchText = "";
                $scope.formData.requestNoFrom = "";
                $scope.formData.requestNoTo = "";
                $scope.startDate = "";
                $scope.endDate = "";
                $scope.formData.endDate = "";
                $scope.formData.beginDate = "";
                $scope.formData.userRoles = $scope.settings.UserRoles[0];
                $scope.formDataUserRoleDescription = $scope.settings.UserRoles[0].TextDescription;
                $scope.formData.requestEmpNo = "";
            }

            function setFromParamsUrl() {
                localStorage.setItem('search_monitor_p1', $scope.requestTypes.selected);
                localStorage.setItem('search_monitor_p2', $scope.searchText);
                localStorage.setItem('search_monitor_p3', $scope.formData.requestNoFrom);
                localStorage.setItem('search_monitor_p4', $scope.formData.requestNoTo);
                localStorage.setItem('search_monitor_p5', $scope.startDate);
                localStorage.setItem('search_monitor_p6', $scope.endDate);
                localStorage.setItem('search_monitor_p7', $scope.formData.endDate);
                localStorage.setItem('search_monitor_p8', $scope.formData.beginDate);
                localStorage.setItem('search_monitor_p9', JSON.stringify($scope.formData.userRoles));
                localStorage.setItem('search_monitor_p10', $scope.formDataUserRoleDescription);
                localStorage.setItem('search_monitor_p11', $scope.formData.requestEmpNo);
            }
            $scope.initSettingSearch = function () {
                if (localStorage.getItem('search_monitor_p1') == null || localStorage.getItem('search_monitor_p3') == null) {
                    $scope.clearInput();
                } else {
                    $scope.requestTypes.selected = localStorage.getItem('search_monitor_p1');
                    var st =  $scope.searchText = localStorage.getItem('search_monitor_p2');
                    if (st) {
                        $scope.searchText = st;
                    }
                    $scope.formData.requestNoFrom = localStorage.getItem('search_monitor_p3');
                    $scope.formData.requestNoTo = localStorage.getItem('search_monitor_p4');
                    $scope.startDate = localStorage.getItem('search_monitor_p5');
                    $scope.endDate = localStorage.getItem('search_monitor_p6');
                    $scope.formData.endDate = localStorage.getItem('search_monitor_p7');
                    $scope.formData.beginDate = localStorage.getItem('search_monitor_p8');
                    $scope.formData.userRoles = JSON.parse(localStorage.getItem('search_monitor_p9'));
                    $scope.formDataUserRoleDescription = localStorage.getItem('search_monitor_p10');
                    $scope.formData.requestEmpNo = localStorage.getItem('search_monitor_p11');
                }

            }
            var tempResult;
            $scope.tryToSelect = function (text) {
                for (var i = 0; i < $scope.requestType.length; i++) {
                    if ($scope.requestType[i].RequestTypeName == text) {
                        tempResult = $scope.requestType[i];
                        break;
                    }
                }
                $scope.searchText = '';
            }
            $scope.checkText = function (text) {
                var result = null;
                for (var i = 0; i < $scope.requestType.length; i++) {
                    if ($scope.requestType[i].RequestTypeName == text) {
                        result = $scope.requestType[i];
                        break;
                    }
                }
                if (result) {
                    $scope.searchText = result.RequestTypeName;
                    $scope.selectedItemChange(result.RequestTypeName);
                    $scope.autocomplete.selectedItem = result;
                } else if (tempResult) {
                    $scope.searchText = tempResult.RequestTypeName;
                    $scope.selectedItemChange(tempResult.RequestTypeName);
                    $scope.autocomplete.selectedItem = tempResult;
                }
            }






            $scope.querySearchRequestType = function (query) {

                var results = query ? $scope.requestType.filter(createFilterFor(query)) : $scope.requestType, deferred;

                if ($scope.simulateQuery) {
                    deferred = $q.defer();
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 250, false);
                    return deferred.promise;
                } else {
                    return results;
                }
            }
            function createFilterFor(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(ettag) {
                    var source = angular.lowercase(ettag.RequestTypeName);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }

            $scope.getDateFormate = function (date) {
                return getDateFormate(date);
            }
            function getDateFormate(date) { return $filter('date')(date, 'd/M/yyyy'); }
            Date.prototype.addHours = function (h) {
                this.setHours(this.getHours() + h);
                return this;
            }




            // pagging
            $scope.data = {
                nPageSize: 10,
                arrFilterd: [],
                arrSplited: [],
                selectedColumn: 0,
                tFilter: '',
                pages: []
            };
            $scope.filteredItem = [];
            //$scope.$watch('search', function(filter){ 
            //  filterArry(filter)
            //});
            function filterArry(filter) {
                $scope.filteredItem = $filter('filter')($scope.Monitor, filter);
                var expectedPage = 0;
                var i, j, temparray, chunk = 10;
                $scope.data.arrSplited = [];
                for (i = 0, j = $scope.filteredItem.length; i < j; i += chunk) {
                    temparray = $scope.filteredItem.slice(i, i + chunk);
                    $scope.data.arrSplited.push(temparray);
                    expectedPage++
                }
                $scope.data.selectedColumn = 0;
            }
            $scope.pageLeft = function () { if ($scope.data.selectedColumn > 0) { $scope.data.selectedColumn--; } }
            $scope.pageRight = function () { if ($scope.data.selectedColumn < $scope.data.arrSplited.length - 1) { $scope.data.selectedColumn++; } }

            $scope.cutofftext = function () {
                if ($scope.searchText.length >= 100) {
                    $scope.searchText = $scope.searchText.substring(0, 100);

                }
                
            }
        }]);
})();