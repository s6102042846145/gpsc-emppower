﻿(function () {
    angular.module('ESSMobile')
        .controller('_MasterEditGeneralExpenseController', ['$scope', '$http', 'CONFIG', function ($scope, $http, CONFIG) {

            $scope.employeeData = getToken(CONFIG.USER);

            $scope.settings = {
                ProjectCodeMode: '',
                EnableOverideCostCenter: false,
                EnableOverideIO: false,
                ExpenseTypeGroupInfo: {
                    prefix: 'GE',
                    travelTypeID: 0,
                    isDomestic: 'D'
                },
                ReceiptToleranceAllow: 0,
                ENABLE_PROJECTCOST: false,
                Master: []
            };
            $scope.masterTR = {
                CostCenterList: [],
                OrgUnitList: [],
                IOList: [],
                ProjectList: [],

            };
           

            /*==============================
                 Initial Scope and variable
             ===============================*/

            /*========================
                Initial Function
            ==========================*/
            //Get all master data
            
            $scope.GetMasterdata = function () {
                var URL = CONFIG.SERVER + 'HRTR/GetMasterData';
                var oRequestParameter = { InputParameter: { "CreateDate": $scope.document.CreatedDate }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                return $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.settings.Master.CostCenterList = response.data.CostCenterList;
                    $scope.settings.Master.ProjectList = response.data.ProjectList;
                    $scope.settings.Master.OrgUnitList = response.data.OrganizationList;
                    $scope.settings.Master.IOList = response.data.InternalOrderList;
                    $scope.settings.Master.CostcenterDistributionList = response.data.CostCenterDistributionList;

                    // enco much use dont remove
                    $scope.masterTR.CostCenterList = angular.copy(response.data.CostCenterList);
                    $scope.masterTR.OrgUnitList = angular.copy(response.data.OrganizationList);
                    $scope.masterTR.IOList = angular.copy(response.data.InternalOrderList);
                    $scope.masterTR.ProjectList = angular.copy(response.data.ProjectList);
                   // console.debug(response);
                    return response;
                },
              function errorCallback(response) {
                  console.error(response);
                  return response;
              });
            };
            
            var initialExpenseReceipt = function () {
                var URL = CONFIG.SERVER + 'HRTR/InitialExpenseReceipt_New';
                var oInputParameter = {
                    "CreateDate": $scope.document.CreatedDate
                };
                var oRequestParameter = { InputParameter: oInputParameter, CurrentEmployee: $scope.employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                return $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    //var projectcodeMode = response.data.ProjectCodeMode.toString().toUpperCase();
                    //$scope.settings.ProjectCodeMode = projectcodeMode;
                    //var isOverrideCostCenter = response.data.EnableOverideCostCenter.toString().toLowerCase();
                    //$scope.settings.EnableOverideCostCenter = (isOverrideCostCenter == 'true') ? true : false;

                    //var isOverrideIO = response.data.EnableOverideIO.toString().toLowerCase();
                    //$scope.settings.EnableOverideIO = (isOverrideIO == 'true') ? true : false;
                    //$scope.data.EnableOverideCostcenter = (isOverrideCostCenter == 'true') ? true : false;
                    //$scope.data.EnableOverideIO = (isOverrideIO == 'true') ? true : false;

                    var isEnableProjectCost = response.data.EnableProjectCost.toString().toLowerCase();
                    $scope.settings.EnableProjectCost = (isEnableProjectCost == 'true') ? true : false;
                   
                    //$scope.settings.ReceiptToleranceAllow = response.data.receiptToleranceAllow;

                    // SET 'RECEIPT_LIMIT_DATE_09182017'
                    $scope.RECEIPT_LIMIT_DATE = {
                        RP_LIMIT_MINDATE: response.data.RP_LIMIT_MINDATE,
                        RP_LIMIT_MAXDATE: response.data.RP_LIMIT_MAXDATE,
                        GE_LIMIT_MINDATE: response.data.GE_LIMIT_MINDATE,
                        GE_LIMIT_MAXDATE: response.data.GE_LIMIT_MAXDATE,
                        GE_ALLOW_SELECTDATE_OUTOFPERIOD: response.data.GE_ALLOW_SELECTDATE_OUTOFPERIOD,
                        RP_ALLOW_SELECTDATE_OUTOFPERIOD: response.data.RP_ALLOW_SELECTDATE_OUTOFPERIOD,
                        GE_VALIDATE_EXPENSETYPE_TIMECONSTRAIN: response.data.GE_VALIDATE_EXPENSETYPE_TIMECONSTRAIN,
                        RP_VALIDATE_EXPENSETYPE_TIMECONSTRAIN: response.data.RP_VALIDATE_EXPENSETYPE_TIMECONSTRAIN
                    }
                    return response;
                }, function errorCallback(response) {

                    return response;
                });
            };


            $scope.init_MasterEditGeneralExpenseController = function () {
                // initialExpenseReceipt();
                var isEnableProjectCost = false; //20170804 Add By Ratchatawan W.
                //$scope.GetMasterdata();
            }
            $scope.init_MasterEditGeneralExpenseController();
        }]);
})();