﻿(function () {
    angular.module('ESSMobile')
        .controller('TravelExpenseContentController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', 'DTOptionsBuilder', 'DTColumnDefBuilder', '$mdDialog', '$mdMenu', function ($scope, $http, $routeParams, $location, $filter, CONFIG, DTOptionsBuilder, DTColumnDefBuilder, $mdDialog, $mdMenu) {
            $scope.DateNow = new Date();
            $scope.TravelExpenseDetail = [{}];
            $scope.content.isShowHeader = false;
            $scope.pagin = {
                currentPage: 1, itemPerPage: '10', numPage: 1, cbbItemPerPage: ['5', '10', '20', '50'], pages:[]
            }
            $scope.formData = {
                IsAdvancedSearch: '', SelYear: '', SelMonth: '', SelRequestType: '', SelDocumentNo: '', SelBeginDate: $filter('date')($scope.DateNow, 'yyyy-MM-dd'), SelEndDate: $filter('date')($scope.DateNow, 'yyyy-MM-dd'), SelTravelType: '', SelCountry: ''
                , EnablePettyCash: ''
            };
            $scope.Textcategory = 'TRAVELEXPENSE';
            $scope.HeaderText = $scope.Text[$scope.Textcategory].TITLE;
            $scope.content.Header = $scope.HeaderText;
            $scope.InitialConfig = function () {
                //Load Configuratioin for Search Data
                var URL = CONFIG.SERVER + 'HRTR/GetTravelConfigurationForSearch';
                var oRequestParameter = { InputParameter: { "YEARKEY": "TRAVELEXPENSE" }, CurrentEmployee: getToken(CONFIG.USER), Requestor: $scope.requesterData }
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    //$scope.expenseStatus = response.data;
                    console.log('Response TravelExpende', response.data);
                    $scope.SearchYear = response.data['YEAR'];
                    $scope.SearchMonth = response.data['MONTH'];
                    $scope.SearchRequestType = response.data['REQUESTTYPE'];
                    $scope.SearchTravelType = response.data['TRAVELTYPE'];
                    $scope.SearchCountry = response.data['COUNTRY'];
                    $scope.formData.EnablePettyCash = response.data["ENABLEPETTYCASH"];
                    $scope.formData.EnableGroupChange = response.data["ENABLEGROUPCHANGE"];
                    $scope.formData.EnableGroupCancel = response.data["ENABLEGROUPCANCEL"];
                    $scope.formData.EnablePersonalChange = response.data["ENABLEPERSONALCHANGE"];
                    $scope.formData.EnablePersonalCancel = response.data["ENABLEPERSONALCANCEL"];
                    $scope.formData.EnableCorpCard = response.data["ENABLECORPCARD"];

                    // bind default value 
                    if ($scope.SearchYear.length > 0) {
                        $scope.formData.SelYear = new Date().getFullYear().toString();
                    }
                    $scope.formData.SelMonth = "-1";//((new Date()).getMonth() + 1).toString();

                    if ($scope.SearchRequestType.length > 0) {
                        $scope.formData.SelRequestType = angular.copy($scope.SearchRequestType[0]);
                    }
                    if ($scope.SearchTravelType.length > 0) {
                        $scope.formData.SelTravelType = angular.copy($scope.SearchTravelType[0]);
                    }
                    if ($scope.SearchCountry.length > 0) {
                        $scope.formData.SelCountry = angular.copy($scope.SearchCountry[0]);
                    }

                    if (angular.isDefined($scope.content.ContentParam) && $scope.content.ContentParam != "") {
                        var array = $scope.content.ContentParam.split('&&');
                        $scope.formData.IsAdvancedSearch = array[0] === "true";
                        $scope.formData.SelYear = array[2];
                        $scope.formData.SelMonth = array[1];
                        $scope.formData.SelRequestType.Key = array[3];
                        $scope.formData.IsSearchClick = array[4] === "true";
                        if ($scope.formData.IsAdvancedSearch) {
                            $scope.formData.SelDocumentNo = array[4];
                            $scope.formData.SelBeginDate = array[5];
                            $scope.formData.SelEndDate = array[6];
                            $scope.formData.SelTravelType.TravelTypeID = array[7];
                            $scope.formData.SelCountry.CountryCode = array[8];
                            $scope.formData.IsSearchClick = array[9] === "true";
                        }
                    }
                    $scope.SearchTravelExpense();
                }, function errorCallback(response) {
                    // Error
                    console.log('error TravelExpenseContentController InitialConfig.', response);
                });
            }

            $scope.ValidateCancelGroup = function (RequestNo, RequesterEmployeeID) {

                var oRequestParameter = { InputParameter: { "TravelRequestNo": RequestNo }, CurrentEmployee: getToken(CONFIG.USER), Requestor: $scope.requesterData }

                //get Expense Type Tag
                var URL = CONFIG.SERVER + 'HRTR/ValidateCancelTravelGroupRequest/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data == "pass") {
                        $scope.CreateNewWithReference(110, RequestNo, RequesterEmployeeID,'');
                    }
                    else {
                        //$mdDialog.show(
                        //  $mdDialog.alert()
                        //    .clickOutsideToClose(true)
                        //    .title('INFORMATION')
                        //    .textContent(response.data)
                        //    .ok('OK')
                        //);

                        $mdDialog.show(
                         $mdDialog.alert()
                           .clickOutsideToClose(false)
                           .title($scope.Text['SYSTEM']['INFORMATION'])
                           .textContent(response.data)
                           .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                        );
                    }


                }, function errorCallback(response) {
                    // Error
                    console.log('error ValidateCancelGroup.', response);
                });

            }

            /*============================================================
                All function to validate when need to create request
            ==============================================================*/
            $scope.Validate_TravelerCreateTravelWithCashAdvance = function () {

                var oRequestParameter = { CurrentEmployee: getToken(CONFIG.USER), Requestor: $scope.requesterData }

                //get Expense Type Tag
                var URL = CONFIG.SERVER + 'HRTR/Validate_TravelerCreateTravelWithCashAdvance/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data == "pass") {
                        $scope.CreateNew(104);
                    }
                    else {
                        $mdDialog.show(
                         $mdDialog.alert()
                           .clickOutsideToClose(false)
                           .title($scope.Text['SYSTEM']['INFORMATION'])
                           .htmlContent(response.data)
                           .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                        );
                    }


                }, function errorCallback(response) {
                    // Error
                    console.log('error Validate_EditTravelPersonal.', response);
                });
            }

            $scope.Validate_TravelerCreateCashAdvance = function (RequestNo, RequesterEmployeeID, RequestTypeID) {

                var oRequestParameter = { InputParameter: { "TravelRequestNo": RequestNo }, CurrentEmployee: getToken(CONFIG.USER), Requestor: $scope.requesterData }

                //get Expense Type Tag
                var URL = CONFIG.SERVER + 'HRTR/Validate_TravelerCreateCashAdvance/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data == "pass") {
                        $scope.CreateNewWithReference(RequestTypeID, RequestNo, RequesterEmployeeID,'');
                    }
                    else {
                        $mdDialog.show(
                         $mdDialog.alert()
                           .clickOutsideToClose(false)
                           .title($scope.Text['SYSTEM']['INFORMATION'])
                           .htmlContent(response.data)
                           .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                        );
                    }


                }, function errorCallback(response) {
                    // Error
                    console.log('error Validate_EditTravelPersonal.', response);
                });
            }

            $scope.Validate_TravelerCreatePettyCash = function (RequestNo, RequesterEmployeeID, RequestTypeID) {

                var oRequestParameter = { InputParameter: { "TravelRequestNo": RequestNo }, CurrentEmployee: getToken(CONFIG.USER), Requestor: $scope.requesterData }

                //get Expense Type Tag
                var URL = CONFIG.SERVER + 'HRTR/Validate_TravelerCreatePettyCash/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data == "pass") {
                        $scope.CreateNewWithReference(RequestTypeID, RequestNo, RequesterEmployeeID,'');
                    }
                    else {
                        $mdDialog.show(
                         $mdDialog.alert()
                           .clickOutsideToClose(false)
                           .title($scope.Text['SYSTEM']['INFORMATION'])
                           .textContent(response.data)
                           .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                        );
                    }


                }, function errorCallback(response) {
                    // Error
                    console.log('error Validate_EditTravelPersonal.', response);
                });
            }

            $scope.Validate_Report = function (RequestNo, RequesterEmployeeID, RequestTypeID) {

                var oRequestParameter = { InputParameter: { "TravelRequestNo": RequestNo }, CurrentEmployee: getToken(CONFIG.USER), Requestor: $scope.requesterData }

                //get Expense Type Tag
                var URL = CONFIG.SERVER + 'HRTR/Validate_Report/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data == "pass") {
                        $scope.CreateNewWithReference(RequestTypeID, RequestNo, RequesterEmployeeID,'');
                    }
                    else {
                        $mdDialog.show(
                         $mdDialog.alert()
                           .clickOutsideToClose(false)
                           .title($scope.Text['SYSTEM']['INFORMATION'])
                           .textContent(response.data)
                           .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                        );
                    }


                }, function errorCallback(response) {
                    // Error
                    console.log('error Validate_EditTravelPersonal.', response);
                });
            }

            $scope.Validate_EditOrCancelTravelPersonal = function (RequestNo, RequesterEmployeeID, RequestTypeID) {

                var oRequestParameter = { InputParameter: { "TravelRequestNo": RequestNo }, CurrentEmployee: getToken(CONFIG.USER), Requestor: $scope.requesterData }

                //get Expense Type Tag
                var URL = CONFIG.SERVER + 'HRTR/Validate_EditOrCancelTravelPersonal/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data == "pass") {
                        $scope.CreateNewWithReference(RequestTypeID, RequestNo, RequesterEmployeeID,'');
                    }
                    else {
                        $mdDialog.show(
                         $mdDialog.alert()
                           .clickOutsideToClose(false)
                           .title($scope.Text['SYSTEM']['INFORMATION'])
                           .textContent(response.data)
                           .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                        );
                    }


                }, function errorCallback(response) {
                    // Error
                    console.log('error Validate_EditTravelPersonal.', response);
                });
            }

            $scope.Validate_EditTravelGroup = function (RequestNo, RequesterEmployeeID, RequestTypeID) {

                var oRequestParameter = { InputParameter: { "TravelRequestNo": RequestNo }, CurrentEmployee: getToken(CONFIG.USER), Requestor: $scope.requesterData }

                //get Expense Type Tag
                var URL = CONFIG.SERVER + 'HRTR/Validate_EditTravelGroup/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data == "pass") {
                        $scope.CreateNewWithReference(RequestTypeID, RequestNo, RequesterEmployeeID,'');
                    }
                    else {
                        $mdDialog.show(
                         $mdDialog.alert()
                           .clickOutsideToClose(false)
                           .title($scope.Text['SYSTEM']['INFORMATION'])
                           .textContent(response.data)
                           .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                        );
                    }


                }, function errorCallback(response) {
                    // Error
                    console.log('error Validate_EditTravelPersonal.', response);
                });
            }

            $scope.Validate_CancelTravelGroup = function (RequestNo, RequesterEmployeeID, RequestTypeID) {

                var oRequestParameter = { InputParameter: { "TravelRequestNo": RequestNo }, CurrentEmployee: getToken(CONFIG.USER), Requestor: $scope.requesterData }

                //get Expense Type Tag
                var URL = CONFIG.SERVER + 'HRTR/Validate_CancelTravelGroup/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data == "pass") {
                        $scope.CreateNewWithReference(RequestTypeID, RequestNo, RequesterEmployeeID,'');
                    }
                    else {
                        $mdDialog.show(
                         $mdDialog.alert()
                           .clickOutsideToClose(false)
                           .title($scope.Text['SYSTEM']['INFORMATION'])
                           .textContent(response.data)
                           .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                        );
                    }


                }, function errorCallback(response) {
                    // Error
                    console.log('error Validate_EditTravelPersonal.', response);
                });
            }
            /*Finish*/

            $scope.ViewCoverSheetTR = function (compCode, requestNo) {
                $scope.loader.enable = true; // เปิด

                //ตรวจสอบว่ามี File ที่เป็น Freeze path อยู่หรือไม่ หากมีให้เปิดขึ้นมา หากไม่มีให้ Gen Coversheet ใหม่
                var freezeURL = CONFIG.SERVER + 'Client/files/FreezePath/' + compCode + '/' + requestNo + '/CoverSheet' + requestNo + '.pdf?t=' + (new Date()).getTime().toString();
                if (UrlExists(freezeURL)) {
                    window.open(freezeURL);
                    $scope.loader.enable = false; // เปิด
                    //IsLog Count Print
                    $scope.GetCoverSheetLogPrint(requestNo, 1);
                    $scope.SearchTravelExpense();
                }
            }

            $scope.ViewCoverSheetCA = function (compCode, requestNo) {
                $scope.loader.enable = true; // เปิด

                //ตรวจสอบว่ามี File ที่เป็น Freeze path อยู่หรือไม่ หากมีให้เปิดขึ้นมา หากไม่มีให้ Gen Coversheet ใหม่
                    var freezeURL = CONFIG.SERVER + 'Client/files/FreezePath/' + compCode + '/' + requestNo + '/CoverSheetCA' + $scope.employeeData.RequesterEmployeeID + '.pdf?t=' + (new Date()).getTime().toString();
                if (UrlExists(freezeURL)) {
                    window.open(freezeURL);
                    $scope.loader.enable = false; // เปิด
                    //IsLog Count Print
                    $scope.GetCoverSheetLogPrint(requestNo, 1);
                    $scope.SearchTravelExpense();
                }
            }

            function UrlExists(url) {
                var http = new XMLHttpRequest();
                http.open('HEAD', url, false);
                http.send();
                return http.status != 404;
            }

            //Nun Add CoverSheet
            $scope.ViewCoverSheetReport = function (compCode, requestNo) {

                $scope.loader.enable = true; // เปิด

                //ตรวจสอบว่ามี File ที่เป็น Freeze path อยู่หรือไม่ หากมีให้เปิดขึ้นมา หากไม่มีให้ Gen Coversheet ใหม่
                    var freezeURL = CONFIG.SERVER + 'Client/files/FreezePath/' + compCode + '/' + requestNo + '/CoverSheetReport' + $scope.employeeData.RequesterEmployeeID + '.pdf?t=' + (new Date()).getTime().toString();
                if (UrlExists(freezeURL)) {
                    window.open(freezeURL);
                    $scope.loader.enable = false; // เปิด
                    //IsLog Count Print
                    $scope.GetCoverSheetLogPrint(requestNo, 1);
                    $scope.SearchTravelExpense();
                }

                //CommentBy: Ratchatawan W. (30 Jun 2017) เนื่องจากเปลี่ยนมาเป็น FreezePath
                //var URL = CONFIG.SERVER + 'HRTR/MergeOpenTravelReportCoverSheet';
                //var oRequestParameter = {
                //    InputParameter: { 'RequestNo': requestNo }, CurrentEmployee: $scope.employeeData, Requestor: $scope.requesterData //CommentBy: Ratchatawan W. (21 Dec 2016), Requestor: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER) 
                //};
                //$http({
                //    method: 'POST',
                //    url: URL,
                //    data: oRequestParameter
                //}).then(function successCallback(response) {
                //    if (response.data != null && response.data) {

                //        URL = CONFIG.SERVER + 'Client/files/' + compCode + '/' + requestNo + '/CoverSheetReport' + $scope.employeeData.RequesterEmployeeID + '.pdf';
                //        window.open(URL);
                //        $scope.loader.enable = false; // เปิด
                //    }
                //    console.log('MergeOpenTravelReportCoverSheet.', response.data);
                //}, function errorCallback(response) {
                //    $scope.loader.enable = false; // เปิด
                //    console.log('error MergeOpenTravelReportCoverSheet.', response);
                //    $mdDialog.show(
                //        $mdDialog.alert()
                //        .clickOutsideToClose(false)
                //        .title($scope.Text['SYSTEM']['WARNING'])
                //        .textContent($scope.Text['SYSTEM']['FILENOTFOUND'])
                //        .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                //    );
                //});
            }

            $scope.ViewCoverSheetCorpCardReport = function (compCode, requestNo) {

                $scope.loader.enable = true; // เปิด

                //ตรวจสอบว่ามี File ที่เป็น Freeze path อยู่หรือไม่ หากมีให้เปิดขึ้นมา หากไม่มีให้ Gen Coversheet ใหม่
                    var freezeURL = CONFIG.SERVER + 'Client/files/FreezePath/' + compCode + '/' + requestNo + '/CoverSheetReport' + $scope.employeeData.RequesterEmployeeID + '.pdf?t=' + (new Date()).getTime().toString();
                if (UrlExists(freezeURL)) {
                    window.open(freezeURL);
                    $scope.loader.enable = false; // เปิด
                    //IsLog Count Print
                    $scope.GetCoverSheetLogPrint(requestNo, 1);
                    $scope.SearchTravelExpense();
                }

                //CommentBy: Ratchatawan W. (30 Jun 2017) เนื่องจากเปลี่ยนมาเป็น FreezePath
                //var URL = CONFIG.SERVER + 'HRTR/MergeOpenTravelReportCoverSheetCorpCard';
                //var oRequestParameter = {
                //    InputParameter: { 'RequestNo': requestNo }, CurrentEmployee: $scope.employeeData, Requestor: $scope.requesterData //CommentBy: Ratchatawan W. (21 Dec 2016), Requestor: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER) 
                //};
                //$http({
                //    method: 'POST',
                //    url: URL,
                //    data: oRequestParameter
                //}).then(function successCallback(response) {
                //    if (response.data != null && response.data) {

                //        URL = CONFIG.SERVER + 'Client/files/' + compCode + '/' + requestNo + '/CoverSheetReport' + $scope.employeeData.RequesterEmployeeID + '.pdf';
                //        window.open(URL);
                //        $scope.loader.enable = false; // เปิด
                //    }
                //    console.log('MergeOpenTravelReportCoverSheet.', response.data);
                //}, function errorCallback(response) {
                //    $scope.loader.enable = false; // เปิด
                //    console.log('error MergeOpenTravelReportCoverSheet.', response);
                //    $mdDialog.show(
                //        $mdDialog.alert()
                //        .clickOutsideToClose(false)
                //        .title($scope.Text['SYSTEM']['WARNING'])
                //        .textContent($scope.Text['SYSTEM']['FILENOTFOUND'])
                //        .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                //    );
                //});
            }

            $scope.ViewCoverSheetPettyCashReport = function (compCode, requestNo) {

                $scope.loader.enable = true; // เปิด

                //ตรวจสอบว่ามี File ที่เป็น Freeze path อยู่หรือไม่ หากมีให้เปิดขึ้นมา หากไม่มีให้ Gen Coversheet ใหม่
                    var freezeURL = CONFIG.SERVER + 'Client/files/FreezePath/' + compCode + '/' + requestNo + '/CoverSheetReport' + $scope.employeeData.RequesterEmployeeID + '.pdf?t='+(new Date()).getTime().toString();
                if (UrlExists(freezeURL)) {
                    window.open(freezeURL);
                    $scope.loader.enable = false; // เปิด
                    //IsLog Count Print
                    $scope.GetCoverSheetLogPrint(requestNo, 1);
                    $scope.SearchTravelExpense();
                }

                //CommentBy: Ratchatawan W. (30 Jun 2017) เนื่องจากเปลี่ยนมาเป็น FreezePath
                //var URL = CONFIG.SERVER + 'HRTR/MergeOpenTravelReportCoverSheetPettyCash';
                //var oRequestParameter = {
                //    InputParameter: { 'RequestNo': requestNo }, CurrentEmployee: $scope.employeeData, Requestor: $scope.requesterData //CommentBy: Ratchatawan W. (21 Dec 2016), Requestor: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER) 
                //};
                //$http({
                //    method: 'POST',
                //    url: URL,
                //    data: oRequestParameter
                //}).then(function successCallback(response) {
                //    if (response.data != null && response.data) {

                //        URL = CONFIG.SERVER + 'Client/files/' + compCode + '/' + requestNo + '/CoverSheetReport' + $scope.employeeData.RequesterEmployeeID + '.pdf';
                //        window.open(URL);
                //        $scope.loader.enable = false; // เปิด
                //    }
                //    console.log('MergeOpenTravelReportCoverSheet.', response.data);
                //}, function errorCallback(response) {
                //    $scope.loader.enable = false; // เปิด
                //    console.log('error MergeOpenTravelReportCoverSheet.', response);
                //    $mdDialog.show(
                //        $mdDialog.alert()
                //        .clickOutsideToClose(false)
                //        .title($scope.Text['SYSTEM']['WARNING'])
                //        .textContent($scope.Text['SYSTEM']['FILENOTFOUND'])
                //        .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                //    );
                //});
            }
            $scope.clearInput = function () {


                $scope.formData.SelRequestType.Key = $scope.SearchRequestType[0].Key;
                $scope.formData.SelDocumentNo = '';
                $scope.formData.SelBeginDate = '';
                $scope.formData.SelEndDate = '';
                $scope.formData.SelTravelType.TravelTypeID = $scope.SearchTravelType[0].TravelTypeID;
                $scope.formData.SelCountry.CountryCode = '';
            }

            $scope.FilterTravelExpense = function (item) {
                var beginDate = new Date(item.BeginDate);
                var endDate = new Date(item.EndDate);

                return ($scope.formData.SelYear >= beginDate.getFullYear() && $scope.formData.SelYear <= endDate.getFullYear() && $scope.formData.SelMonth >= beginDate.getMonth()+1 && $scope.formData.SelMonth <= endDate.getMonth()+1);
            }

            $scope.SearchTravelExpense = function () {

                var URL = CONFIG.SERVER + 'HRTR/SearchTravelExpense_New';
                var oRequestParameter = {
                    InputParameter: {
                        "YEAR": $scope.formData.SelYear
                        , "MONTH": $scope.formData.SelMonth
                        , "REQUESTTYPE": $scope.formData.SelRequestType.Key
                        , "REQUESTNO": $scope.formData.SelDocumentNo
                        , "BEGINDATE": $scope.formData.SelBeginDate
                        , "ENDDATE": $scope.formData.SelEndDate
                        , "TRAVELTYPE": $scope.formData.SelTravelType.TravelTypeID
                        , "COUNTRY": $scope.formData.SelCountry.CountryCode
                        , "ISADVANCEDSEARCH": $scope.formData.IsAdvancedSearch
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    //, Creator: getToken(CONFIG.USER)
                };
                //console.log('oRequestParameter', oRequestParameter);

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.TravelExpense = response.data.TravelRequest;
                    for (var i = 0; i < $scope.TravelExpense.length; i++) {
                        $scope.TravelExpense[i].CropCards = [];
                        for (var a = 0; a < response.data.CropCards.length; a++) {
                            if ($scope.TravelExpense[i].TRRequestNo == response.data.CropCards[a].TRRequestNo) {
                                $scope.TravelExpense[i].CropCards.push(response.data.CropCards[a]);
                            }
                        }
                    }


                    if ($scope.formData.IsSearchClick) {
                        $scope.content.ContentParam = $scope.formData.IsAdvancedSearch + '&&' + $scope.formData.SelMonth + '&&' + $scope.formData.SelYear + '&&' + $scope.formData.SelRequestType.Key;
                        if ($scope.formData.IsAdvancedSearch) {
                            $scope.content.ContentParam = $scope.content.ContentParam + '&&' + $scope.formData.SelDocumentNo + '&&' + $scope.formData.SelBeginDate + '&&' + $scope.formData.SelEndDate + '&&' + $scope.formData.SelTravelType.TravelTypeID + '&&' + $scope.formData.SelCountry.CountryCode;
                        }
                        $scope.content.ContentParam = $scope.content.ContentParam + '&&' + $scope.formData.IsSearchClick;
                        var tempurl = "";
                        var temp = document.URL.substring(document.URL.indexOf("/101") + 4, document.URL.length);
                        if (temp.indexOf("&&") > 0) {
                            var tempStr = temp.split('/')[temp.split('/').length - 1];
                            temp = temp.substring(0, temp.indexOf(tempStr) - 1);
                        }
                        if ($scope.content.ContentParam) {
                            tempurl = "/frmViewContent/101" + decodeURIComponent(temp) + "/" + $scope.content.ContentParam;
                        }
                        else {
                            tempurl = "/frmViewContent/101" + decodeURIComponent(temp);
                        }
                        $location.path(tempurl, false);
                    }
                    else {
                        $scope.formData.IsSearchClick = false;
                    }
                    $scope.updatePages();
                    $scope.SearchCreditCardByEmployeeID();
                    //console.log('$scope.content.ContentParam', $scope.content.ContentParam)
                }, function errorCallback(response) {
                    // Error
                    console.log('error TravelExpense SearchTravelExpense_New', response);

                });
            };

            //Add New CreditCardByEmployeeID by Nipon Supap 07-02-2017 14:15 PM
            $scope.SearchCreditCardByEmployeeID = function () {
                var URL = CONFIG.SERVER + 'HRTR/SearchCreditCardByEmployeeID';
                var oRequestParameter = {
                    InputParameter: {
                        "YEAR": $scope.formData.SelYear
                        , "MONTH": $scope.formData.SelMonth
                        , "REQUESTTYPE": $scope.formData.SelRequestType.Key
                        , "REQUESTNO": $scope.formData.SelDocumentNo
                        , "BEGINDATE": $scope.formData.SelBeginDate
                        , "ENDDATE": $scope.formData.SelEndDate
                        , "TRAVELTYPE": $scope.formData.SelTravelType.TravelTypeID
                        , "COUNTRY": $scope.formData.SelCountry.CountryCode
                        , "ISADVANCEDSEARCH": $scope.formData.IsAdvancedSearch
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.TravelExpenseCreditCardByEmployeeID = response.data;
                    if ($scope.formData.IsSearchClick) {
                        $scope.content.ContentParam = $scope.formData.IsAdvancedSearch + '&&' + $scope.formData.SelMonth + '&&' + $scope.formData.SelYear + '&&' + $scope.formData.SelRequestType.Key;
                        if ($scope.formData.IsAdvancedSearch) {
                            $scope.content.ContentParam = $scope.content.ContentParam + '&&' + $scope.formData.SelDocumentNo + '&&' + $scope.formData.SelBeginDate + '&&' + $scope.formData.SelEndDate + '&&' + $scope.formData.SelTravelType.TravelTypeID + '&&' + $scope.formData.SelCountry.CountryCode;
                        }
                        $scope.content.ContentParam = $scope.content.ContentParam + '&&' + $scope.formData.IsSearchClick;
                        var tempurl = "";
                        var temp = document.URL.substring(document.URL.indexOf("/101") + 4, document.URL.length);
                        if (temp.indexOf("&&") > 0) {
                            var tempStr = temp.split('/')[temp.split('/').length - 1];
                            temp = temp.substring(0, temp.indexOf(tempStr) - 1);
                        }
                        if ($scope.content.ContentParam) {
                            tempurl = "/frmViewContent/101" + decodeURIComponent(temp) + "/" + $scope.content.ContentParam;
                        }
                        else {
                            tempurl = "/frmViewContent/101" + decodeURIComponent(temp);
                        }
                        $location.path(tempurl, false);
                    }
                    else {
                        $scope.formData.IsSearchClick = false;
                    }
                    //console.log('$scope.content.ContentParam', $scope.content.ContentParam)
                }, function errorCallback(response) {
                    // Error
                    console.log('error TravelExpense SearchCreditCardByEmployeeID', response);

                });
            };

            //Dict GetTransactionStatusGroup
            var oRequestParameter = { InputParameter: { "Mode": "TravelExpense" }, CurrentEmployee: $scope.employeeData, Requestor: $scope.requesterData }

            //get Expense Type Tag
            var URL = CONFIG.SERVER + 'HRTR/GetTransactionStatusGroup/';
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                $scope.GetTransactionStatus = response.data;
                console.log('GetTransactionStatus.', $scope.GetTransactionStatus);

            }, function errorCallback(response) {
                // Error
                console.log('error ExpenseTypeSettingEditorController.', response);
            });


            $scope.SearchTravelExpenseDetail = function (data, index, expanded) {
                if (expanded == true) {
                    var URL = CONFIG.SERVER + 'HRTR/SearchTravelExpenseDetail';
                    var oRequestParameter = {
                        InputParameter: {
                            "REQUESTNO": data
                        }
                        , CurrentEmployee: getToken(CONFIG.USER)
                        , Requestor: $scope.requesterData
                        , Creator: getToken(CONFIG.USER)
                    };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        console.log(response.data);
                        $scope.TravelExpenseDetail[index] = response.data;

                    }, function errorCallback(response) {
                        // Error
                        console.log('error TravelExpense SearchTravelExpenseDetail', response);

                    });
                }
            };
            //Add by Nipon Supap 17-11-2017 Support-003 เปลี่ยนสีตอนกด พิมพ์
            $scope.IsChangeColor = false;
            $scope.GetCoverSheetLogPrint = function (oRequestNo, oIsLog) {
                if (oRequestNo != null) {
                    var URL = CONFIG.SERVER + 'HRTR/GetCoverSheetLogPrint';
                    var oRequestParameter = { InputParameter: { "IsLog": oIsLog, "RequestNo": oRequestNo }, CurrentEmployee: $scope.employeeData, Requestor: $scope.requesterData }
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        var abc = oRequestNo;
                        if (response.data.length > 0 && response.data[0] != null)
                            $scope.IsChangeColor = response.data[0].Column1;
                        else
                            $scope.IsChangeColor = false;
                    }, function errorCallback(response) {
                        // Error
                        console.log('error TravelExpense SearchCreditCardByEmployeeID', response);

                    });
                }
            };


            $scope.dtOptions = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(15).withOption('lengthChange', false).withOption('ordering', false);
            $scope.dtOptionsSub = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(15).withOption('lengthChange', false).withOption('ordering', false);
            $scope.dtOptionsSubDetail = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(15).withOption('lengthChange', false).withOption('ordering', false);
            $scope.setSelectedBeginDate = function (selectedDate) {
                $scope.formData.SelBeginDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };
            $scope.setSelectedEndDate = function (selectedDate) {
                $scope.formData.SelEndDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };
            $scope.openRequest = function (id, keyMaster, isOwner, isDataOwner, requestType, BoxDescription) {
                $location.path('/frmViewRequest/' + id + '/' + keyMaster + '/' + isOwner + '/' + isDataOwner + '/' + encodeURIComponent(BoxDescription));
            };
            $scope.openRequestWthSimulate = function (id, keyMaster, isOwner, isDataOwner, requestType, BoxDescription) {
                $location.path('/frmViewRequestWithReSimulate/' + id + '/' + keyMaster + '/' + isOwner + '/' + isDataOwner + '/' + encodeURIComponent(BoxDescription));
            };
            $scope.getTravelExpenseFilter = function () {
                return $filter('filter')($scope.TravelExpense, $scope.searchText);
            };
            $scope.getTravelExpenseSlice = function () {
                var expenseList = $scope.getTravelExpenseFilter();
                if (!expenseList) return null;
                return expenseList.slice((($scope.pagin.currentPage - 1) * $scope.pagin.itemPerPage), (($scope.pagin.currentPage) * $scope.pagin.itemPerPage));
            };
            $scope.updatePages = function () {
                var pages = [];
                if (!$scope.TravelExpense) return [];
                $scope.pagin.numPage = Math.ceil($scope.getTravelExpenseFilter().length / $scope.pagin.itemPerPage);
                for (var i = 0; i < $scope.pagin.numPage; i++) {
                    pages.push((i + 1));
                }
                $scope.pagin.currentPage = 1;
                $scope.pagin.pages = pages;
            };
            $scope.movePage = function (step) {
                newPage = $scope.pagin.currentPage + step;
                if (newPage < 1 || newPage > $scope.pagin.numPage) return;
                $scope.changePage(newPage);
            };
            $scope.changePage = function (page) {
                $scope.pagin.currentPage = page;
            };
            $scope.$watch('searchText', function (newValue, oldValue) {
                if (oldValue != newValue) {
                    $scope.updatePages();
                    $scope.pagin.currentPage = 1;
                }
            }, true);
        }])
        .directive('expand', function () {
            return {
                restrict: 'A',
                controller: ['$scope', function ($scope) {
                    $scope.$on('onExpandAll', function (event, args) {
                        $scope.expanded = args.expanded;
                    });
                }]
            };
        });
})();