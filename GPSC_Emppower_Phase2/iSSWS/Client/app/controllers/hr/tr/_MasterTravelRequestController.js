﻿/*==========================================================================================
Created By: Ratchatawan W.
Created Date: 31 Mar 2017
Remark: This function use initially all master data and configuration for all travel request
===========================================================================================*/
(function () {
    angular.module('ESSMobile')
        .controller('TravelRequestMasterController', ['$scope', '$http', 'CONFIG', '$mdDialog', function ($scope, $http, CONFIG, $mdDialog) {
            /*==============================
                 Initial Scope and variable
             ===============================*/
            $scope.TravelGroupRequest = angular.copy($scope.document.Additional);
            $scope.Textcategory = 'TRAVELREQUEST';
            $scope.employeeData = getToken(CONFIG.USER);
            $scope.doc_type = 'TR';

            $scope.settings = {
                ExpenseTypeGroupInfo: {
                    isDomestic: ''
                },
                Master: []
            };
            $scope.data = {
                model: {
                    ready: false
                },
                EnableOverideCostCenter: false,
                EnableOverideIO: false,
                CAMode: '',
                BACKDATE: '',
                ProjectCodeMode: '',
                DefaultTransportationTypeCode: '',
                DefaultTransportationTypeName: '',
                Readonly: false,
                EnableOverideCostCenter_: false
            };
            $scope.masterTR = {
                CostCenterList: [],
                OrgUnitList: [],
                IOList: [],
                ProjectList: [],

            };




            $scope.TravelGroupConfig = { GroupMode: '', TravelType: '', Phone: '', MobilePhone: '' }

            /*========================
                Initial Function
            ==========================*/
            //Get all master data
            $scope.GetMasterdata = function () {
                var URL = CONFIG.SERVER + 'HRTR/GetMasterData';
                var oRequestParameter = { InputParameter: { "CreateDate": $scope.document.CreatedDate }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                return $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.settings.Master.CostCenterList = response.data.CostCenterList;
                    $scope.settings.Master.ProjectList = response.data.ProjectList;
                    $scope.settings.Master.OrgUnitList = response.data.OrganizationList;
                    $scope.settings.Master.IOList = response.data.InternalOrderList;
                    $scope.settings.Master.CostcenterDistributionList = response.data.CostCenterDistributionList;
                    $scope.settings.Master.TransportationTypeList = response.data.TransportationTypeList;
                    $scope.settings.Master.PersonalVehicleList = response.data.PersonalVehicleList;

                    // enco much use dont remove
                    $scope.masterTR.CostCenterList = angular.copy(response.data.CostCenterList);
                    $scope.masterTR.OrgUnitList = angular.copy(response.data.OrganizationList);
                    $scope.masterTR.IOList = angular.copy(response.data.InternalOrderList);
                    $scope.masterTR.ProjectList = angular.copy(response.data.ProjectList);

                    return response;
                },
              function errorCallback(response) {
                  console.error(response);
                  return response;
              });
            };
            $scope.GetMasterdata();

            //Get all travel object
            $scope.GetTravelModel = function () {
                var URL = CONFIG.SERVER + 'HRTR/GetTravelModel';
                var oRequestParameter = { InputParameter: { "RequestNo": $scope.document.RequestNo }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                return $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.data.tmpTravelSchedulePlace = response.data.TravelSchedulePlace[0];
                    $scope.data.tmpProjectCostDistribution = response.data.ProjectCostDistribution[0];
                    $scope.data.tmpExchangeRateCapture = response.data.ExchangeRateCapture[0];
                    $scope.data.tmpTraveler = response.data.Traveler[0];
                    $scope.data.tmpGroupBudget = response.data.GroupBudget[0];
                    $scope.data.tmpPersonalBudget = response.data.PersonalBudget[0];
                },
                function errorCallback(response) {
                    console.log('error InitialConfig.', response);
                });
            };

            //Load Configuratioin for Search Data
            $scope.GetTravelRequestConfig = function () {
                var URL = CONFIG.SERVER + 'HRTR/GetTravelRequestConfig';
                var oRequestParameter = { InputParameter: { 'ReuestType': 'TR' }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor }
                return $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.TravelGroupConfig.GroupMode = response.data['GROUPMODE'];
                    $scope.TravelGroupConfig.TravelType = response.data['TRAVELTYPE'];
                    $scope.data.EnableOverideCostCenter = response.data.ENABLEOVERIDECOSTCENTER;
                    $scope.data.EnableOverideCostCenter_ = response.data.ENABLEOVERIDECOSTCENTER;
                    $scope.data.EnableOverideIO = response.data['ENABLEOVERIDEIO'];
                    $scope.data.CAMode = response.data['CASHADVANCEMODE'];
                    $scope.data.BACKDATE = response.data['BACKDATE'];
                    $scope.data.ProjectCodeMode = response.data['ProjectCodeMode'];
                    $scope.data.DefaultTransportationTypeCode = response.data['TransportationTypeCode'];
                    $scope.data.DefaultTransportationTypeName = response.data['TransportationTypeName'];
                    if (angular.isUndefined($scope.TravelGroupRequest.Status) || $scope.TravelGroupRequest.Status == '') {
                        $scope.TravelGroupRequest.GroupSelect = response.data['DEFAULTREQUESTGROUPAPPROVE'];
                       // $scope.TravelGroupRequest.TravelTypeID = 1;
                    }
                    //$scope.onChangeGroupType($scope.TravelGroupRequest.GroupSelect); // hide by argif to fixed cash Advance
                    $scope.EvenGetExpenseTypeByTag();
                    $scope.setBlockAction(false, "");
                }, function errorCallback(response) {
                    // Error
                    console.log('error  InitialConfig.', response);
                    $scope.setBlockAction(false, "");
                });
            };



            $scope.GetReversalReason = function () {
                var URL = CONFIG.SERVER + 'HRTR/GetAllReversalReasonCode';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                return $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.reversalReason = response.data;
                }, function errorCallback(response) {
                    $scope.reversalReason = [];
                    console.log('error GetAllReversalReasonCode.', response);
                });
            }

            $scope.GetExpenseRateTypeForLookup = function () {
                var URL = CONFIG.SERVER + 'HRTR/GetExpenseRateTypeForLookup';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                return $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    if (angular.isUndefined($scope.masterData)) {
                        $scope.masterData = {};
                    }
                    $scope.masterData.objExpenseRateTypeForLookup = response.data;
                }, function errorCallback(response) {
                    console.log('error getExpenseRateTypeForLookup.', response);
                });
            }



            $scope.isDomesticTravel = function (TravelGroupRequest) {
                /* return : 'D' = Domestic, 'O' = Oversea, 'B' = Both domestic and oversea */
                var oTravelSchedules = TravelGroupRequest.TravelSchedulePlaces;
                var personalCountryCode = $scope.document.Requestor.AreaSetting.CountryCode;
                var countDomestic = 0;
                var countOversea = 0;
                for (var i = 0; i < oTravelSchedules.length; i++) {
                    if (personalCountryCode == oTravelSchedules[i].CountryCode) {
                        countDomestic++;
                    } else {
                        countOversea++;
                    }
                }
                if (countDomestic == 0) {
                    return 'O';
                } else if (countOversea == 0) {
                    return 'D';
                }
                return 'B';
            };

            $scope.CheckUploadFiles = function () {
                $scope.TravelGroupRequest.HasFileSet = false;
                for (i = 0; i < $scope.document.FileSet.Attachments.length; i++) {
                    if (!$scope.document.FileSet.Attachments[i].IsDelete) {
                        $scope.TravelGroupRequest.HasFileSet = true;
                        break;
                    }
                }
            };


            $scope.CheckExpenseTypeGroupBudgetSelect = function () {
                if (angular.isDefined($scope.ExpenseTypeGroup) && angular.isDefined($scope.data.GroupBudgets)) {
                    $scope.data.HideGroupExpenseAddButton = false;
                    if ($scope.data.GroupBudgets.length >= $scope.ExpenseTypeGroup.length) {
                        $scope.data.HideGroupExpenseAddButton = true;
                    }
                }
            }

            $scope.OnAddGroupCashAdvance = function (GroupBudget) {
                //add data to GroupCashAdvances
                var oGroupCashAdvance = {
                    "RequestNo": GroupBudget.RequestNo,
                    "GroupBudgetID": GroupBudget.GroupBudgetID,
                    "ExpenseTypeGroupID": GroupBudget.ExpenseTypeGroupID,
                    "Name": GroupBudget.Name,
                    "Remark": GroupBudget.Remark,
                    "TotalAmount": GroupBudget.TotalAmount
                };
                $scope.data.Travelers[0].oCashAdvance.GroupCashAdvances.push(oGroupCashAdvance);
            };

            $scope.onChangeGroupType = function (data) {
                if ($scope.data.GroupBudgets.length) {
                    $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(false)
                       .title($scope.Text['SYSTEM']['WARNING'])
                       .textContent($scope.Text['SYSTEM']['CANTCHANGEIFBUDGETS'])
                       .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                    $scope.TravelGroupRequest.GroupSelect = !$scope.TravelGroupRequest.GroupSelect;
                }
                else {
                    if (!data) {
                        // Personal Travel
                        $scope.data.TempTravelers = [];
                        angular.copy($scope.data.Travelers, $scope.data.TempTravelers)
                        if (angular.isDefined($scope.data.TempTravelers) && $scope.data.TempTravelers.length > 0) {
                            $scope.data.Travelers = [];
                            $scope.data.Travelers.push($scope.data.TempTravelers[0]);
                        }

                        if (angular.isDefined($scope.data.TempGroupBudgets) && $scope.data.TempGroupBudgets.length > 0) {
                            var temp = angular.copy($scope.data.GroupBudgets);
                            $scope.data.GroupBudgets = [];
                            $scope.data.GroupBudgets = angular.copy($scope.data.TempGroupBudgets);
                            $scope.data.TempGroupBudgets = [];
                            $scope.data.TempGroupBudgets = angular.copy(temp);
                        } else {
                            $scope.data.TempGroupBudgets = [];
                        }
                    }
                    else {
                        // Group Travel 
                        if (angular.isDefined($scope.data.TempTravelers)) {
                            angular.copy($scope.data.TempTravelers, $scope.data.Travelers)
                        }
                        if (angular.isDefined($scope.data.TempGroupBudgets) && $scope.data.TempGroupBudgets.length > 0) {
                            var temp = angular.copy($scope.data.GroupBudgets);
                            $scope.data.GroupBudgets = [];
                            $scope.data.GroupBudgets = angular.copy($scope.data.TempGroupBudgets);
                            $scope.data.TempGroupBudgets = [];
                            $scope.data.TempGroupBudgets = angular.copy(temp);
                        }
                    }

                    if (!$scope.data.GroupBudgets.length) {
                        $scope.data.Travelers[0].oCashAdvance.GroupCashAdvances = [];
                    }
                    else {
                        $scope.data.Travelers[0].oCashAdvance.GroupCashAdvances = [];
                        for (var i = 0; i < $scope.data.GroupBudgets.length; i++) {
                            $scope.OnAddGroupCashAdvance(angular.copy($scope.data.GroupBudgets[i]));
                        }
                    }
                    $scope.CalculateTravelGroupRequest();
                }
            }

            //CalculateTravelGroupRequest
            $scope.CalculateTravelGroupRequest = function (resetTransportation) {
                $scope.setBlockAction(true, $scope.Text["SYSTEM"]["PROCESSING"]);
                var URL = CONFIG.SERVER + 'HRTR/CalculateTravelGroupRequest';
                if ($scope.data.GroupBudgets && $scope.data.GroupBudgets.length) {
                    for (var i = 0; i < $scope.data.GroupBudgets.length; i++) {
                        if ($scope.data.GroupBudgets[i]) {
                            if (!$scope.data.GroupBudgets[i].ExpenseTypeGroupID) {
                                $scope.data.GroupBudgets[i].ExpenseTypeGroupID = 0;
                                $scope.data.Travelers[0].oCashAdvance.GroupCashAdvances[i].ExpenseTypeGroupID = 0;
                            }
                            if (!$scope.data.GroupBudgets[i].CostCenter) {
                                $scope.data.GroupBudgets[i].CostCenter = '0';
                            }
                            if (!$scope.data.GroupBudgets[i].AlternativeIOOrg) {
                                $scope.data.GroupBudgets[i].AlternativeIOOrg = '0';
                            }
                            if (!$scope.data.GroupBudgets[i].IO) {
                                $scope.data.GroupBudgets[i].IO = '0';
                            }
                            if (!$scope.data.GroupBudgets[i].TotalAmount) {
                                $scope.data.GroupBudgets[i].TotalAmount = 0;
                                $scope.data.Travelers[0].oCashAdvance.GroupCashAdvances[i].Amount = 0;
                                $scope.data.Travelers[0].oCashAdvance.GroupCashAdvances[i].TotalAmount = 0;
                            }
                        }
                    }
                }
                //Set data
                $scope.TravelGroupRequest.ExchangeRateCaptures = $scope.data.ExchangeRateCaptures;
                $scope.TravelGroupRequest.TravelSchedulePlaces = $scope.data.TravelSchedulePlaces;
                $scope.TravelGroupRequest.ProjectCostDistributions = $scope.data.ProjectCostDistributions;
                $scope.data.Travelers[0].oCashAdvance.PaymentFlagCaptures = $scope.data.PaymentFlagCaptures;
                $scope.TravelGroupRequest.Travelers = $scope.data.Travelers;
                $scope.TravelGroupRequest.GroupBudgets = $scope.data.GroupBudgets;
                $scope.CheckPersonalVehicle();
                var oRequestParameter = { InputParameter: { 'TRAVELGROUPREQUEST': $scope.TravelGroupRequest, 'RESETDEFAULT': !($scope.document.CurrentFlowItemCode == "WAIT_FOR_POST" || $scope.document.CurrentFlowItemCode == "WAIT_FOR_FIX_POST"), "RESETTRANSPOTATIONTYPE": resetTransportation }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                return $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.TravelGroupRequest = response.data;
                    $scope.data.ExchangeRateCaptures = $scope.TravelGroupRequest.ExchangeRateCaptures;
                    $scope.data.TravelSchedulePlaces = $scope.TravelGroupRequest.TravelSchedulePlaces;
                    //$scope.data.ProjectCostDistributions = $scope.TravelGroupRequest.ProjectCostDistributions;
                    //$scope.data.Travelers = $scope.TravelGroupRequest.Travelers;
                    for (var i = 0; i < $scope.TravelGroupRequest.Travelers.length; i++) {
                        $scope.data.Travelers[i].TotalAllowanceAmount = $scope.TravelGroupRequest.Travelers[i].TotalAllowanceAmount;
                        $scope.data.Travelers[i].TotalAccommodationAmount = $scope.TravelGroupRequest.Travelers[i].TotalAccommodationAmount;
                        $scope.data.Travelers[i].TotalAmount = $scope.TravelGroupRequest.Travelers[i].TotalAmount;
                        $scope.data.Travelers[i].TotalDayAmount = $scope.TravelGroupRequest.Travelers[i].TotalDayAmount;
                        $scope.data.Travelers[i].TotalNightAmount = $scope.TravelGroupRequest.Travelers[i].TotalNightAmount;
                        $scope.data.Travelers[i].PersonalBudgets.TotalAmount = $scope.TravelGroupRequest.Travelers[i].PersonalBudgets.TotalAmount;
                        $scope.data.Travelers[i].Perdiums = $scope.TravelGroupRequest.Travelers[i].Perdiums;
                        $scope.data.Travelers[i].TransportationTypeName = $scope.TravelGroupRequest.Travelers[i].TransportationTypeName;
                        $scope.data.Travelers[i].TransportationTypeCode = $scope.TravelGroupRequest.Travelers[i].TransportationTypeCode;
                        //var temp = $scope.data.Travelers[i].oCashAdvance.GroupCashAdvances;
                        $scope.TravelGroupRequest.Travelers[i].oCashAdvance.GroupCashAdvances = angular.copy($scope.data.Travelers[i].oCashAdvance.GroupCashAdvances); // because Group Cash Advance calculate on client by Sutthiphong M. 
                        $scope.data.Travelers[i].oCashAdvance = $scope.TravelGroupRequest.Travelers[i].oCashAdvance;
                        $scope.data.Travelers[i].oPettyCash = $scope.TravelGroupRequest.Travelers[i].oPettyCash;
                        $scope.data.Travelers[i].RequireRemark = $scope.TravelGroupRequest.Travelers[i].RequireRemark;
                    }
                    //$scope.data.GroupBudgets = $scope.TravelGroupRequest.GroupBudgets;  //  by ford
                    //$scope.data.ProjectCostDistributionTemp = angular.copy($scope.data.ProjectCostDistributions);
                    $scope.data.PaymentFlagCaptures = $scope.data.Travelers[0].oCashAdvance.PaymentFlagCaptures;
                    $scope.data.Estimate = $scope.TravelGroupRequest.TotalAmount;
                    //calculate new tag everytime
                    $scope.EvenGetExpenseTypeByTag();
                    /*if ($scope.data.TravelSchedulePlaces.length && $scope.data.Travelers[0].TransportationTypeCode == '') {
                        if ($scope.settings.ExpenseTypeGroupInfo.isDomestic == 'O' || $scope.settings.ExpenseTypeGroupInfo.isDomestic == 'B') {
                            var temp = $scope.settings.Master.TransportationTypeList[$scope.settings.Master.TransportationTypeList.findIndexWithAttr('IsPlane', true)];
                            $scope.data.Travelers[0].TransportationTypeCode = temp.TransportationTypeCode;
                            $scope.employeeDataTemp = getToken(CONFIG.USER);
                            $scope.data.Travelers[0].TransportationTypeName = $scope.employeeDataTemp.Language == "TH" ? temp.NameTH : temp.NameEN;
                        } else {
                            $scope.data.Travelers[0].TransportationTypeCode = $scope.data.DefaultTransportationTypeCode;
                            $scope.data.Travelers[0].TransportationTypeName = $scope.data.DefaultTransportationTypeName;
                        }
                    }*/
                    $scope.setBlockAction(false, "");
                },
                function errorCallback(response) {
                    console.log('error InitialConfig.', response);
                    $scope.setBlockAction(false, "");
                });
            }
            //Even of GetExpenseTypeByTag
            $scope.EvenGetExpenseTypeByTag = function () {

                if ($scope.data.TravelSchedulePlaces.length > 0) {
                    $scope.settings.ExpenseTypeGroupInfo.isDomestic = $scope.isDomesticTravel($scope.TravelGroupRequest)

                    $scope.GetExpenseTypeGroupBudgetByTag();
                    if (!$scope.expensetypetravelgroup || !angular.isDefined($scope.settings.ExpenseTypeGroupInfo.BeforeIsDometic) || $scope.settings.ExpenseTypeGroupInfo.BeforeIsDometic != $scope.settings.ExpenseTypeGroupInfo.isDomestic) {
                        $scope.GetExpenseTypePersonalBudgetByTag();
                        $scope.settings.ExpenseTypeGroupInfo.BeforeIsDometic = $scope.settings.ExpenseTypeGroupInfo.isDomestic;
                    }

                }
                else {
                    $scope.ExpenseTypeGroup = undefined;
                    $scope.expensetypetravelgroup = undefined;
                    //$scope.data.PersonalBudgets = undefined;
                    //$scope.data.GroupBudgets = undefined;

                }
                if (angular.isUndefined($scope.settings.ExpenseTypeGroupInfo.isDomestic_Buffer)) {
                    $scope.settings.ExpenseTypeGroupInfo.isDomestic_Buffer = $scope.settings.ExpenseTypeGroupInfo.isDomestic;
                }
                //if ($scope.settings.ExpenseTypeGroupInfo.isDomestic_Buffer != $scope.settings.ExpenseTypeGroupInfo.isDomestic) {
                //    $scope.document.ErrorText = "eeeee";
                //    $scope.document.HasError = true;
                //}
            }
            // ExpenseType for GroupBudget
            $scope.GetExpenseTypeGroupBudgetByTag = function () {
                var URL = CONFIG.SERVER + 'HRTR/GetExpenseTypeGroupByTag';
                var groupTagValue = "TR_ETG_TravelRequest_";
                //if ($scope.TravelGroupRequest.GroupSelect) { //if group
                //    groupTagValue += "Group_";
                //}
                //else {
                //    groupTagValue += "Personal_";
                //}

                if ($scope.settings.ExpenseTypeGroupInfo.isDomestic == 'D') {
                    groupTagValue += "Domestic";
                } else if ($scope.settings.ExpenseTypeGroupInfo.isDomestic == 'O') {
                    groupTagValue += "Overseas";
                } else if ($scope.settings.ExpenseTypeGroupInfo.isDomestic == 'B') {
                    var tempGroupTagValue = groupTagValue + "Domestic";
                    tempGroupTagValue += ',' + groupTagValue + "Overseas";
                    groupTagValue = tempGroupTagValue;
                }
                if (!angular.isDefined($scope.TempgroupTagValue) || $scope.TempgroupTagValue != groupTagValue || angular.isUndefined($scope.ExpenseTypeGroup) || $scope.ExpenseTypeGroup == null || $scope.ExpenseTypeGroup.length == 0) {
                    var oRequestParameter = {
                        InputParameter: {
                            "grouptag": groupTagValue, "tag": ""
                        }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor
                    };

                    return $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.ExpenseTypeGroup = response.data;
                        $scope.CheckExpenseTypeGroupBudgetSelect();
                        $scope.TempgroupTagValue = groupTagValue;
                        console.log('GetExpenseTypeGroupByTag', $scope.ExpenseTypeGroup);

                    }, function errorCallback(response) {
                        console.log('error GetExpenseTypeGroupByTag', response);

                    });
                }
            }
            // ExpenseType for PersonalBudget
            $scope.GetExpenseTypePersonalBudgetByTag = function () {
                var URL = CONFIG.SERVER + 'HRTR/GetExpenseTypeGroupByTag';
                var groupTagValue = "TR_ETG_TravelRequest";//"TR_ETG_TravelRequest_Traveler";
                if ($scope.settings.ExpenseTypeGroupInfo.isDomestic == 'D') {
                    groupTagValue += "_Domestic";
                } else if ($scope.settings.ExpenseTypeGroupInfo.isDomestic == 'O') {
                    groupTagValue += "_Overseas";
                } else if ($scope.settings.ExpenseTypeGroupInfo.isDomestic == 'B') {
                    var tempGroupTagValue = groupTagValue + "_Domestic";
                    tempGroupTagValue += ',' + groupTagValue + "_Overseas";
                    groupTagValue = tempGroupTagValue;
                }
                var oRequestParameter = { InputParameter: { "grouptag": groupTagValue, "tag": "" }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };

                return $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.expensetypetravelgroup = response.data;
                    console.log('GetExpenseTypeTravelerByTag', $scope.expensetypetravelgroup);

                }, function errorCallback(response) {
                    console.log('error GetExpenseTypeTravelerByTag', response);
                });
            }




            //GET CURRENT TRAVELER
            $scope.GetEditTraveler = function (specific_employeeid) {
                var index = $.map($scope.data.Travelers, function (obj, index) {
                    if (obj.EmployeeID == specific_employeeid) {
                        return index;
                    }
                })
                return index;
            }

            $scope.CheckPersonalVehicle = function () {
                $scope.data.IsNotPersonalVehicle = true;
                for (i = 0; i < $scope.data.Travelers.length; i++) {
                    if ($scope.data.Travelers[i].RequireRemark) {
                        $scope.data.IsNotPersonalVehicle = false;
                        break;
                    }
                }
            };
        }]);
})();