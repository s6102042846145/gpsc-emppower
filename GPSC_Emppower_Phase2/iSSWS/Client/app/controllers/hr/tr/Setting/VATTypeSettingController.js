﻿(function () {
    angular.module('ESSMobile')
        .controller('VATTypeSettingController', ['$scope', '$http', '$routeParams', '$location', '$window', '$mdDialog', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $window, $mdDialog, $filter, CONFIG) {

            console.log('VATTypeSettingController');
            $scope.requestType = $routeParams.id;
            $scope.formData = { searchKeyword: '' };
            $scope.firstLoad = true;

            $scope.Textcategory = 'ACCOUNT_SETTING';

            $scope.Textcategory2 = 'SYSTEM';
            $scope.content.isShowHeader = false;

            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }

            var URL = CONFIG.SERVER + 'HRTR/GetAllVATType/';
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                $scope.documents = response.data;
                $scope.masterDocuments = response.data;
                console.log('document.', $scope.documents);

            }, function errorCallback(response) {
                // Error
                console.log('error VATTypeSettingController.', response);
            });


            $scope.CreateNewVAT = function () {
                $location.path('/frmViewContent/309');
            };

            $scope.EditVAT = function (vatcode) {
                $location.path('/frmViewContent/309/' + vatcode);
            };

            $scope.setSelectedBeginDate = function (selectedDate) {
                $scope.formData.selectedBeginDate = selectedDate;
                $scope.formData.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
            };

            $scope.setSelectedEndDate = function (selectedDate) {
                $scope.formData.selectedEndDate = selectedDate;
                $scope.formData.EndDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
            };

            var search = function (item, keyword) {
                // Search Criteria
                if (!keyword
                    || (item.VATCode.toLowerCase().indexOf(keyword) != -1)
                    || (item.Description.toLowerCase().indexOf(keyword) != -1)
                    || (item.GLAccount.toLowerCase().indexOf(keyword) != -1)
                    || (item.Percent.toString().toLowerCase().indexOf(keyword) != -1)
                    ) {
                    return true;
                }
                return false;
            };

            function isBetween(begin, end) {
                return function filterFn(x) {
                    return (begin <= new Date(x.BeginDate) && new Date(x.BeginDate) <= end)
                        || (begin <= new Date(x.EndDate) && new Date(x.EndDate) <= end)
                        || (new Date(x.BeginDate) <= begin && new begin <= new Date(x.BeginDate))
                        || (new Date(x.EndDate) <= end && end <= new Date(x.EndDate));
                };
            }

            $scope.searchInBox = function () {
                $scope.firstLoad = false;
                console.log('searchInBox.', $scope.formData.searchKeyword);
                if (!$scope.formData.searchKeyword) {
                    console.log('just reset documents.');
                    if ($scope.formData.BeginDate && $scope.formData.EndDate)
                        $scope.documents = $scope.masterDocuments.filter(isBetween($scope.formData.selectedBeginDate, $scope.formData.selectedEndDate));
                    else
                        $scope.documents = $scope.masterDocuments;
                }
                else {
                    var count = $scope.masterDocuments.length;
                    var docs = [];
                    for (var i = 0; i < count; i++) {
                        if (search($scope.masterDocuments[i], $scope.formData.searchKeyword.toLowerCase())) {
                            docs.push($scope.masterDocuments[i]);
                        }
                    }
                    if ($scope.formData.BeginDate && $scope.formData.EndDate)
                        $scope.documents = docs.filter(isBetween($scope.formData.selectedBeginDate, $scope.formData.selectedEndDate));
                    else
                        $scope.documents = docs;
                }
            };

            $scope.DeleteConfirm = function (vatcode, ev) {
                var confirm = $mdDialog.confirm()
                    .title($scope.Text['ACCOUNT_SETTING'].WANT_TO_DELETE)
                    .textContent($scope.Text['ACCOUNT_SETTING'].CAN_NOT_ROLLBACK_PLEASE_CONFIRM)
                    .ariaLabel($scope.Text['ACCOUNT_SETTING'].WANT_TO_DELETE)
                    .targetEvent(ev)
                    .ok($scope.Text['SYSTEM'].BUTTON_YES)
                    .cancel($scope.Text['SYSTEM'].BUTTON_NO);
                $mdDialog.show(confirm).then(function () {
                    //delete
                    //var URL = CONFIG.SERVER + 'HRTR/DeleteVATType/';
                    var oRequestParameter = { InputParameter: { "VATCODE": vatcode }, CurrentEmployee: getToken(CONFIG.USER) }
                    var URL = CONFIG.SERVER + 'HRTR/DeleteVATType/';
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        if (response.data) {
                            // Success
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .clickOutsideToClose(true)
                                    .title($scope.Text['ACCOUNT_SETTING'].DELETE_COMPLETED)
                                    .textContent($scope.Text['ACCOUNT_SETTING'].CAN_NOT_ROLLBACK)
                                    .ariaLabel($scope.Text['ACCOUNT_SETTING'].DELETE_COMPLETED)
                                    .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                    .targetEvent(ev)
                            );
                        }
                        else {
                            // Fail
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .clickOutsideToClose(true)
                                    .title($scope.Text['ACCOUNT_SETTING'].CAN_NOT_DELETE)
                                    .textContent($scope.Text['ACCOUNT_SETTING'].DATA_ACTIVE)
                                    .ariaLabel($scope.Text['ACCOUNT_SETTING'].CAN_NOT_DELETE)
                                    .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                    .targetEvent(ev)
                            );
                        }
                        var URL = CONFIG.SERVER + 'HRTR/GetAllVATType/';
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            // Success
                            $scope.documents = response.data;
                            $scope.masterDocuments = response.data;
                            console.log('document.', $scope.documents);

                        }, function errorCallback(response) {
                            // Error
                            console.log('error VATTypeSettingController.', response);
                        });

                    }, function errorCallback(response) {
                        // Error
                        console.log('error VATTypeSettingController.', response);
                    });
                }, function () {

                });
            }

        }]);
})();