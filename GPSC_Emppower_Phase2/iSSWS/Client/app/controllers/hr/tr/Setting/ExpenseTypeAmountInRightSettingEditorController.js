﻿(function () {
    angular.module('ESSMobile')
        .controller('ExpenseTypeAmountInRightSettingEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$timeout', '$q', '$log', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $timeout, $q, $log, $mdDialog) {

            $scope.AmountInRight = {
                ExpenseTypeID: '',
                BeginDate: $filter('date')(new Date(), 'yyyy-MM-ddT00:00:00'),
                EndDate: '9999-12-31T00:00:00',
                AmountInRight: '',
                NoSpecific: true,
                searchETText: ''
            };

            $scope.init = function () {
                var URL = CONFIG.SERVER + 'HRTR/GetAllExpenseType/';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.expenseTypeList = response.data;
                    if ($scope.itemKey != "null") {
                        getExpenseTypeAmountInRight();
                    }
                    else {
                        $scope.ready = true;
                    }
                }, function errorCallback(response) {
                    console.log('error GetAllExpenseType.', response);
                });
            }

            function getExpenseTypeAmountInRight() {
                oRequestParameter = { InputParameter: { "RentRateID": $scope.itemKey }, CurrentEmployee: getToken(CONFIG.USER) }
                URL = CONFIG.SERVER + 'HRTR/GetExpenseTypeAmountInRightByID/';

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.AmountInRight = response.data;
                    $scope.AmountInRight.NoSpecific = ($scope.AmountInRight.EndDate == '9999-12-31T00:00:00');
                    $scope.checkText_ET($scope.AmountInRight.ExpenseTypeID);
                    $scope.ready = true;
                }, function errorCallback(response) {
                    console.log('error GetExpenseTypeAmountInRightByID.', response);
                });
            }

            var employeeData = getToken(CONFIG.USER);
            console.log(employeeData);
            $scope.content.isShowHeader = true;
            $scope.content.isShowBackIcon = true;
            $scope.HasError = false;
            $scope.ErrorText = "";

            $scope.Textcategory = 'ACCOUNT_SETTING';
            $scope.content.Header = $scope.Text[$scope.Textcategory].EXPENSETYPEAMOUNTINRIGHT_SETTING;
            $scope.Textcategory2 = 'SYSTEM';


            $scope.onBlurAmountInRight = function () {
                // allow only string numeric (use with string only)
                if (($scope.AmountInRight.AmountInRight != null && $scope.AmountInRight.AmountInRight != '' && isNaN($scope.AmountInRight.AmountInRight)) || $scope.AmountInRight.AmountInRight.indexOf('-') >= 0 || $scope.AmountInRight.AmountInRight.indexOf('+') >= 0) {
                    $scope.AmountInRight.AmountInRight = $scope.tempAmountInRight;
                } else {
                    $scope.tempAmountInRight = $scope.AmountInRight.AmountInRight;
                }
            };

            $scope.addEndDate = function () {
                if ($scope.AmountInRight.NoSpecific == true) {
                    $scope.AmountInRight.EndDate = "9999-12-31T00:00:00";
                }
                else if ($scope.AmountInRight.EndDate == "9999-12-31T00:00:00") {
                    $scope.AmountInRight.EndDate = $scope.AmountInRight.BeginDate;
                }
            };

            $scope.setSelectedBeginDate = function (selectedDate) {
                $scope.AmountInRight.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                if ($scope.AmountInRight.BeginDate > $scope.AmountInRight.EndDate) {
                    $scope.AmountInRight.EndDate = $scope.AmountInRight.BeginDate;
                }
                $scope.AmountInRight.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
            };

            $scope.setSelectedEndDate = function (selectedDate) {
                $scope.AmountInRight.EndDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                $scope.AmountInRight.NoSpecific = ($scope.AmountInRight.EndDate == "9999-12-31T00:00:00");
            };

            //onsave
            $scope.SaveData = function (ev) {
                var valid = true;
                if (!$scope.AmountInRight.ExpenseTypeID) valid = false;
                else if (!$scope.AmountInRight.AmountInRight) valid = false;

                if (!valid) {
                    $scope.HasError = true;
                    $scope.ErrorText = $scope.Text['ACCOUNT_SETTING'].PLEASE_INSERT_ALL_FIELDS;
                }
                else {
                    var oRequestParameter = { InputParameter: { "AMOUNTINRIGHT": $scope.AmountInRight }, CurrentEmployee: $scope.employeeData, Requestor: getToken(CONFIG.USER) }

                    var URL = CONFIG.SERVER + 'HRTR/SaveExpenseTypeAmountInRight/';
                    $scope.HasError = false;
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        var output = response.data;
                        if (output > 0) {
                            $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                .textContent($scope.Text['ACCOUNT_SETTING'].GO_BACK)
                                .ariaLabel($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                .targetEvent(ev));
                            $location.path('/frmViewContent/328');
                        }
                        else {
                            //duplicate
                            $scope.HasError = true;
                            if ($scope.AmountInRight.RentRateID) {
                                $scope.ErrorText = $scope.Text['ACCOUNT_SETTING'].DATA_ACTIVE;
                            }
                            else {
                                $scope.ErrorText = "Expense Type is duplicated";
                            }
                        }

                    }, function errorCallback(response) {
                        // Error
                        console.log('error ExpenseTypeAmountInRightSettingEditorController Save.', response);
                    });
                }
            }

            //auto complete Expense Type 
            $scope.querySearchET = function (query) {
                $scope.AmountInRight.ExpenseTypeID = '';
                var results = query ? $scope.expenseTypeList.filter(createFilterForET(query)) : $scope.expenseTypeList, deferred;
                if ($scope.simulateQuery) {
                    deferred = $q.defer();
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 1000, false);
                    return deferred.promise;
                } else {
                    return results;
                }
            };

            $scope.selectedItemETChange = function (item) {
                if (angular.isDefined(item) && item != null) {
                    $scope.AmountInRight.searchETText = item.Name;
                    $scope.AmountInRight.ExpenseTypeID = item.ExpenseTypeID;
                }
                else {
                    $scope.AmountInRight.searchETText = '';
                    $scope.AmountInRight.ExpenseTypeID = '';
                }
            };

            function createFilterForET(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(et) {
                    var source = angular.lowercase(et.Name);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }

            $scope.checkText_ET = function (text) {
                var result = null;
                if (text) {
                    for (var i = 0; i < $scope.expenseTypeList.length; i++) {
                        if ($scope.expenseTypeList[i].ExpenseTypeID == text || $scope.expenseTypeList[i].Name == text) {
                            result = $scope.expenseTypeList[i];
                            break;
                        }
                    }
                }
                if (result) {
                    $scope.selectedItemETChange(result);
                }
                else {
                    $scope.selectedItemETChange(null);
                }
            }
        }]);
})();

