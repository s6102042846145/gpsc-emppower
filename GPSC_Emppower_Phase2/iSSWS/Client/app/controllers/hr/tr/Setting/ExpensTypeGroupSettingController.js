﻿(function () {
    angular.module('ESSMobile')
        .controller('ExpensTypeGroupSettingController', ['$scope', '$http', '$routeParams', '$location', '$window', '$mdDialog', 'CONFIG', function ($scope, $http, $routeParams, $location, $window, $mdDialog, CONFIG) {

            console.log('ExpensTypeGroupSettingController');
            $scope.requestType = $routeParams.id;
            $scope.formData = { searchKeyword: '' };
            $scope.firstLoad = true;
            $scope.expenseTypeTagList = [];
            $scope.expenseTypeList = [];
            $scope.expenseTypeTagList.length = 0;
            $scope.expenseTypeList.length = 0;
            $scope.Textcategory = 'ACCOUNT_SETTING';
            $scope.Textcategory2 = 'SYSTEM';
            $scope.content.isShowHeader = false;

            

            //get ExpenseTypeGroupTag for List
            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }
            var URL = CONFIG.SERVER + 'HRTR/GetAllExpenseTypeGroupTag/';
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                $scope.exptypegrouptag = response.data;
                //$scope.exptypegrouptagMaster = response.data;
                console.log('exptypegrouptag.', $scope.exptypegrouptag);

            }, function errorCallback(response) {
                // Error
                console.log('error ExpensTypeGroupSettingEditorController.', response);
            });

            //get ExpenseTypeGroupTag for List
            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }
            URL = CONFIG.SERVER + 'HRTR/GetAllExpenseTypeGroupMapping/';
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                $scope.exptypegroupmapping = response.data;
                //$scope.exptypegrouptagMaster = response.data;
                console.log('exptypegroupmapping.', $scope.exptypegroupmapping);

            }, function errorCallback(response) {
                // Error
                console.log('error ExpensTypeGroupSettingEditorController.', response);
            });

            //get ExpenseType
            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }
            URL = CONFIG.SERVER + 'HRTR/GetExpenseTypeAllForLookup/';
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                $scope.GetExpenseType = response.data;
                //$scope.exptypegrouptagMaster = response.data;
                console.log('GetExpenseType.', $scope.GetExpenseType);

            }, function errorCallback(response) {
                // Error
                console.log('error ExpensTypeGroupSettingEditorController.', response);
            });
            

            //GetAllExpenseType
            oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }
            URL = CONFIG.SERVER + 'HRTR/GetAllExpenseType/';
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                $scope.exptype = response.data;
                //$scope.exptypeMaster = response.data;
                console.log('exptype.', $scope.exptype);

            }, function errorCallback(response) {
                // Error
                console.log('error ExpensTypeGroupSettingEditorController.', response);
            });

            //GetAllExpenseType
            oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }
            URL = CONFIG.SERVER + 'HRTR/GetAllExpenseTypeGroupTagMapping/';
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                $scope.exptypegrouptagmapping = response.data;
                //$scope.exptypeMaster = response.data;
                console.log('exptypegrouptagmapping.', $scope.exptypegrouptagmapping);

            }, function errorCallback(response) {
                // Error
                console.log('error ExpensTypeGroupSettingEditorController.', response);
            });

            var URL = CONFIG.SERVER + 'HRTR/GetAllExpenseTypeGroup/';
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                $scope.expenseTypeGroupList = response.data;
                $scope.masterDocuments = response.data;
                
                console.log('expenseTypeGroupList', $scope.expenseTypeGroupList);
            }, function errorCallback(response) {
                // Error
                console.log('error ExpensTypeGroupSettingController.', response);
            });

            


            $scope.CreateNewExpenseTypeGroup = function () {
                $location.path('/frmViewContent/310');
            };

            $scope.EditExpenseTypeGroup = function (vatcode) {
                $location.path('/frmViewContent/310/' + vatcode);
            };

            var search = function (item, keyword) {
                // Search Criteria
                if (!keyword
                    || (item.Name.toLowerCase().indexOf(keyword) != -1)
                    || (item.Remark.toLowerCase().indexOf(keyword) != -1)
                    || ($scope.exptypegrouptagmapping.filter(mappingWithKey(item.ExpenseTypeGroupID)).propIndexOf('Description', keyword) >= 0)
                    ) {
                    return true;
                }
                return false;
            };

            function mappingWithKey(ExpenseTypeGroupID) {
                return function filterFn(et) {
                    return et.ExpenseTypeGroupID == ExpenseTypeGroupID;
                };
            }


            $scope.searchInBox = function () {
                $scope.firstLoad = false;
                console.log('searchInBox.', $scope.formData.searchKeyword);
                if (!$scope.formData.searchKeyword) {
                    console.log('just reset documents.');
                    $scope.expenseTypeGroupList = $scope.masterDocuments;
                }
                else {
                    var count = $scope.masterDocuments.length;
                    var docs = [];
                    for (var i = 0; i < count; i++) {
                        if (search($scope.masterDocuments[i], $scope.formData.searchKeyword.toLowerCase())) {
                            docs.push($scope.masterDocuments[i]);
                        }
                    }
                    $scope.expenseTypeGroupList = docs;
                }
            };

            $scope.DeleteConfirm = function (expgroupID, ev) {
                var confirm = $mdDialog.confirm()
                    .title($scope.Text['ACCOUNT_SETTING'].WANT_TO_DELETE)
                    .textContent($scope.Text['ACCOUNT_SETTING'].CAN_NOT_ROLLBACK_PLEASE_CONFIRM)
                    .ariaLabel($scope.Text['ACCOUNT_SETTING'].WANT_TO_DELETE)
                    .targetEvent(ev)
                    .ok($scope.Text['SYSTEM'].BUTTON_YES)
                    .cancel($scope.Text['SYSTEM'].BUTTON_NO);
                $mdDialog.show(confirm).then(function () {
                    //delete
                    //var URL = CONFIG.SERVER + 'HRTR/DeleteVATType/';
                    var oRequestParameter = { InputParameter: { "EXPGROUPID": expgroupID }, CurrentEmployee: getToken(CONFIG.USER) }
                    var URL = CONFIG.SERVER + 'HRTR/DeleteExpenseTypeGroup/';
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success

                        $scope.IsNotused = response.data;
                        if ($scope.IsNotused == true) {
                            $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['ACCOUNT_SETTING'].DELETE_COMPLETED)
                                .textContent($scope.Text['ACCOUNT_SETTING'].CAN_NOT_ROLLBACK)
                                .ariaLabel($scope.Text['ACCOUNT_SETTING'].DELETE_COMPLETED)
                                .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                .targetEvent(ev)
                            );
                            var URL = CONFIG.SERVER + 'HRTR/GetAllExpenseTypeGroup/';
                            $http({
                                method: 'POST',
                                url: URL,
                                data: oRequestParameter
                            }).then(function successCallback(response) {
                                // Success
                                $scope.expenseTypeGroupList = response.data;
                                console.log('expenseTypeGroupList.', $scope.expenseTypeGroupList);

                            }, function errorCallback(response) {
                                // Error
                                console.log('error ExpensTypeGroupSettingController.', response);
                            });
                        }
                        else {
                            $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['ACCOUNT_SETTING'].CAN_NOT_DELETE)
                                .textContent($scope.Text['ACCOUNT_SETTING'].DATA_ACTIVE)
                                .ariaLabel($scope.Text['ACCOUNT_SETTING'].CAN_NOT_DELETE)
                                .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                .targetEvent(ev)
                            );
                            //alert('This group is inused. Cannot Delete');
                        }

                    }, function errorCallback(response) {
                        // Error
                        console.log('error ExpensTypeGroupSettingController.', response);
                    });
                }, function () {

                });

            }



        }]);
})();