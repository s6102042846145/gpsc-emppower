﻿(function () {
    angular.module('ESSMobile')
        .controller('ExpenseRateSettingEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$timeout', '$q', '$log', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $timeout, $q, $log, $mdDialog) {

            console.log('ExpenseRateSettingEditorController');
            $scope.content.isShowHeader = true;
            $scope.content.isShowBackIcon = true;
            $scope.Textcategory = 'ACCOUNT_SETTING';
            $scope.content.Header = $scope.Text[$scope.Textcategory].EXPENSERATE_TITLE;
            $scope.Textcategory2 = 'SYSTEM';
            $scope.ExpenseRate = { IsUnlimit: false };

            $scope.init = function () {
                getExpenseRateSelector();
            }

            function getExpenseRateSelector() {
                var oRequestParameter = { InputParameter: { "EXPENSERATEID": $scope.itemKey }, CurrentEmployee: getToken(CONFIG.USER) };
                var URL = CONFIG.SERVER + 'HRTR/GetAllExpenseRateSelector/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.traveltype = response.data.TravelType;
                    $scope.DayFractionGroup = response.data.DayFractionRoundingRuleGroup;
                    $scope.currencyList = response.data.Currency;
                    $scope.PersonalArealist = response.data.PersonalArea;
                    $scope.ExpenseRateTypelist = response.data.ExpenseRateType;
                    $scope.ExpenseAreaGroupList = response.data.ExpenseAreaGroup;
                    console.log('selector.', response.data);

                    if ($scope.itemKey != "null") {
                        getExpenseRate()
                    }
                    else {
                        $scope.ExpenseRate.BeginDate = $filter('date')(new Date(), 'yyyy-MM-ddT00:00:00');
                        $scope.ExpenseRate.EndDate = "9999-12-31T00:00:00";
                        $scope.ExpenseRate.EndDateUndefine = true;
                        $scope.ExpenseRate.PersonalAreaCodes = [];
                        $scope.ready = true;
                    }
                }, function errorCallback(response) {
                    console.log('error GetAllExpenseRateSelector.', response);
                });
            }

            function getExpenseRate() {
                var URL = CONFIG.SERVER + 'HRTR/GetExpenseRateByExpenseRateID/';
                var oRequestParameter = { InputParameter: { "EXPENSERATEID": $scope.itemKey }, CurrentEmployee: getToken(CONFIG.USER) };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.ExpenseRate = response.data;
                    $scope.ExpenseRate.Currency = response.data.Currency;
                    $scope.searchCurrencyText = response.data.Currency;
                    $scope.ExpenseRate.NoCalIncomeCurrency = response.data.NoCalIncomeCurrency;
                    $scope.searchNoCalIncomeCurrencyText = response.data.NoCalIncomeCurrency;
                    if ($scope.ExpenseRate.TravelTypeID == -1) {
                        $scope.ExpenseRate.AllTravelType = true;
                    }
                    else {
                        $scope.ExpenseRate.AllTravelType = false;
                    }
                    if ($scope.ExpenseRate.EndDate == "9999-12-31T00:00:00") {
                        $scope.ExpenseRate.EndDateUndefine = true;
                    }
                    else {
                        $scope.ExpenseRate.EndDateUndefine = false;
                    }
                    if ($scope.ExpenseRate.PersonalAreaCode == "*") {
                        $scope.ExpenseRate.PersonalAreaCodes = [];
                        $scope.ExpenseRate.PersonalAreaCodeALL = true;
                    }
                    else {
                        $scope.ExpenseRate.PersonalAreaCodes.push($scope.ExpenseRate.PersonalAreaCode);
                    }
                    $scope.ready = true;
                }, function errorCallback(response) {
                    console.log('error GetExpenseRateByExpenseRateID.', response);
                });
            }

            
            $scope.setSelectedBeginDate = function (selectedDate) {
                $scope.ExpenseRate.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                if ($scope.ExpenseRate.BeginDate > $scope.ExpenseRate.EndDate) {
                    $scope.ExpenseRate.EndDate = $scope.ExpenseRate.BeginDate;
                }
            };

            $scope.setSelectedEndDate = function (selectedDate) {
                $scope.ExpenseRate.EndDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                $scope.ExpenseRate.EndDateUndefine = ($scope.ExpenseRate.EndDate == '9999-12-31T00:00:00');
            };
            
            
            $scope.addEndDate = function () {
                if ($scope.ExpenseRate.EndDateUndefine == true) {
                    $scope.ExpenseRate.EndDate = "9999-12-31T00:00:00";
                }
                else if ($scope.ExpenseRate.EndDate == "9999-12-31T00:00:00") {
                    $scope.ExpenseRate.EndDate = $scope.ExpenseRate.BeginDate;
                }
            };

            //onsave
            $scope.SaveData = function (ev) {
                var URL = "";
                var tempCurrency = angular.copy($scope.ExpenseRate.Currency);
                var tempNoCalIncomeCurrency = angular.copy($scope.ExpenseRate.NoCalIncomeCurrency);
                var valid = true;
                if (!$scope.ExpenseRate.ContractID) valid = false;
                else if (!$scope.ExpenseRate.AllTravelType && !$scope.ExpenseRate.TravelTypeID) valid = false;
                else if (!$scope.ExpenseRate.ExpenseRateTypeID) valid = false;
                else if (!$scope.ExpenseRate.RoundingGroupID) valid = false;
                else if (!$scope.ExpenseRate.AreaGroupID) valid = false;
                else if (!$scope.ExpenseRate.MinEmpSubGroup) valid = false;
                else if (!$scope.ExpenseRate.MaxEmpSubGroup) valid = false;
                else if (!$scope.ExpenseRate.BeginDate) valid = false;
                else if (!$scope.ExpenseRate.EndDate) valid = false;
                else if (!$scope.ExpenseRate.Value.toString().length) valid = false;
                else if (!$scope.ExpenseRate.Currency) valid = false;
                else if (!$scope.ExpenseRate.NoCalIncome.toString().length) valid = false;
                else if (!$scope.ExpenseRate.NoCalIncomeCurrency) valid = false;
                else if (!$scope.ExpenseRate.PersonalAreaCodeALL && $scope.ExpenseRate.PersonalAreaCodes.length <= 0) valid = false;
                else if (!$scope.ExpenseRate.ReferenceURL) valid = false;
                else {
                    //$scope.ExpenseRate.Currency = $scope.ExpenseRate.Currency.Code;
                    //$scope.ExpenseRate.NoCalIncomeCurrency = $scope.ExpenseRate.NoCalIncomeCurrency.Code;
                    if ($scope.ExpenseRate.PersonalAreaCodeALL) {
                        $scope.ExpenseRate.PersonalAreaCodes = [];
                        $scope.ExpenseRate.PersonalAreaCodes.push('*');
                    }
                    if ($scope.ExpenseRate.AllTravelType) $scope.ExpenseRate.TravelTypeID = -1;
                }

                if (!valid) {
                    //$scope.ExpenseRate.Currency = angular.copy(tempCurrency);
                    //$scope.ExpenseRate.NoCalIncomeCurrency = angular.copy(tempNoCalIncomeCurrency);
                    $scope.HasError = true;
                    $scope.ErrorText = $scope.Text['ACCOUNT_SETTING'].PLEASE_INSERT_ALL_FIELDS;
                }
                else {
                    oRequestParameter = { InputParameter: { "ExpenseRate": $scope.ExpenseRate }, CurrentEmployee: $scope.employeeData, Requestor: getToken(CONFIG.USER) };
                    URL = CONFIG.SERVER + 'HRTR/SaveExpenseRate';
                    $scope.HasError = false;
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        //var output = response.data;
                        if (response.data) {
                            $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                .textContent($scope.Text['ACCOUNT_SETTING'].GO_BACK)
                                .ariaLabel($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                .targetEvent(ev));
                            $location.path('/frmViewContent/304');
                        }
                        else {
                            $scope.HasError = true;
                            $scope.ErrorText = "Update failed";
                        }
                    }, function errorCallback(response) {
                        // Error
                        console.log('error ExpenseRateSettingEditorController Save.', response);
                    });
                }
            }
            /*
            $scope.onAfterValidateFunction = function (event, fileList) {
                //console.log('objCashAdvanceReturns.', $scope.data.CashAdvanceReturns);
                var rowNo = $(event.target).attr('data-row-id');
                $scope.ExpenseRate.FileName = fileList[0].filename;
                $scope.ExpenseRate.FileStringBase64 = fileList[0].base64;
            };
            */

            //auto complete Currency 
            $scope.simulateQuery = false;
            $scope.querySearchCurrency = function (query) {
                if ($scope.ExpenseRate.Currency != query) $scope.ExpenseRate.Currency = '';
                var results = angular.copy(query ? $scope.currencyList.filter(createFilterForProject(query)) : $scope.currencyList), deferred;
                deferred = $q.defer();
                if ($scope.simulateQuery) {
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 1000, false);
                    return deferred.promise;
                } else {
                    deferred.resolve(results)
                    return deferred.promise;
                }
            };

            $scope.querySearchNoCalIncomeCurrency = function (query) {
                if ($scope.ExpenseRate.NoCalIncomeCurrency != query) $scope.ExpenseRate.NoCalIncomeCurrency = '';
                var results = angular.copy(query ? $scope.currencyList.filter(createFilterForProject(query)) : $scope.currencyList), deferred;
                deferred = $q.defer();
                if ($scope.simulateQuery) {
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 1000, false);
                    return deferred.promise;
                } else {
                    deferred.resolve(results)
                    return deferred.promise;
                }
            };

            $scope.selectedItemCurrencyChange = function (item) {
                if (angular.isDefined(item) && item != null) {
                    $scope.ExpenseRate.Currency = item.Code;
                    $scope.searchCurrencyText = item.Code;
                }
                else {
                    $scope.ExpenseRate.Currency = '';
                }
            };

            $scope.selectedItemNoCalIncomeCurrencyChange = function (item) {
                if (angular.isDefined(item) && item != null) {
                    $scope.ExpenseRate.NoCalIncomeCurrency = item.Code;
                    $scope.searchNoCalIncomeCurrencyText = item.Code;
                }
                else {
                    $scope.ExpenseRate.NoCalIncomeCurrency = '';
                }
            };

            function createFilterForProject(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(curruncy) {
                    var source = angular.lowercase(curruncy.Code);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }

            $scope.checkText_Currency = function (text) {
                var result = null;
                if (text) {
                    for (var i = 0; i < $scope.currencyList.length; i++) {
                        if (angular.lowercase($scope.currencyList[i].Code) == text) {
                            result = $scope.currencyList[i];
                            break;
                        }
                    }
                }
                if (result) {
                    $scope.selectedItemCurrencyChange(result);
                }
                else {
                    $scope.selectedItemCurrencyChange(null);
                }
            }

            $scope.checkText_NoCalIncomeCurrency = function (text) {
                var result = null;
                if (text) {
                    for (var i = 0; i < $scope.currencyList.length; i++) {
                        if (angular.lowercase($scope.currencyList[i].Code) == text) {
                            result = $scope.currencyList[i];
                            break;
                        }
                    }
                }
                if (result) {
                    $scope.selectedItemNoCalIncomeCurrencyChange(result);
                }
                else {
                    $scope.selectedItemNoCalIncomeCurrencyChange(null);
                }
            }

            //$scope.$watch('ExpenseRate.Currency', function () {
            //    alert('hey, myVar has changed! >> ', $scope.ExpenseRate.Currency.toString());
            //});
        }]);
})();