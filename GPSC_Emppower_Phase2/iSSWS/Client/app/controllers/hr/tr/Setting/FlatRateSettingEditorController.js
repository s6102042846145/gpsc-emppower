﻿(function () {
    angular.module('ESSMobile')
        .controller('FlatRateSettingEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$timeout', '$q', '$log', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $timeout, $q, $log, $mdDialog) {

            console.log('FlatRateSettingEditorController');

            var employeeData = getToken(CONFIG.USER);
            console.log(employeeData);
            $scope.content.isShowHeader = true;
            $scope.content.isShowBackIcon = true;
            $scope.requestType = $routeParams.id;
            $scope.data = {};
            $scope.Language = getToken(CONFIG.USER).Language;
            $scope.FlatRateGroup = {
                Description: '',
                BeginDate: $filter('date')(new Date(), 'yyyy-MM-ddT00:00:00'),
                EndDate: '9999-12-31T00:00:00',
                NoSpecific: true,
                FlatRateList: []
            }

            $scope.addRow = function () {
                var FlatRate = {
                    searchSourceText: '',
                    SourceLocationID: '',
                    searchDestinationText: '',
                    DestinationLocationID: '',
                    Rate: 0
                };
                $scope.FlatRateGroup.FlatRateList.push(FlatRate);
            };

            $scope.removeRow = function (index) {
                $scope.FlatRateGroup.FlatRateList.splice(index, 1);
            };

            //auto complete FlatRate
            $scope.querySearchSource = function (query, index) {
                $scope.FlatRateGroup.FlatRateList[index].SourceLocationID = '';
                var results = query ? $scope.FlatRateLocation.filter(createFilterForFlatRate(query)) : $scope.FlatRateLocation, deferred;
                if ($scope.simulateQuery) {
                    deferred = $q.defer();
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 1, false);
                    return deferred.promise;
                } else {
                    return results;
                }
            };

            $scope.selectedItemSourceChange = function (item, index) {
                if (angular.isDefined(item) && item != null) {
                    $scope.FlatRateGroup.FlatRateList[index].searchSourceText = item.Value;
                    $scope.FlatRateGroup.FlatRateList[index].SourceLocationID = item.Key;
                }
                else {
                    $scope.FlatRateGroup.FlatRateList[index].searchSourceText = '';
                    $scope.FlatRateGroup.FlatRateList[index].SourceLocationID = '';
                }
            };

            $scope.checkText_Source = function (text, index) {
                var result = null;
                if (text) {
                    for (var i = 0; i < $scope.FlatRateLocation.length; i++) {
                        if ($scope.FlatRateLocation[i].Key == text || $scope.FlatRateLocation[i].Value == text) {
                            result = $scope.FlatRateLocation[i];
                            break;
                        }
                    }
                }
                if (result) {
                    $scope.selectedItemSourceChange(result, index);
                }
                else {
                    $scope.selectedItemSourceChange(null, index);
                }
            }

            $scope.querySearchDestination = function (query, index) {
                $scope.FlatRateGroup.FlatRateList[index].DestinationLocationID = '';
                var results = query ? $scope.FlatRateLocation.filter(createFilterForFlatRate(query)) : $scope.FlatRateLocation, deferred;
                if ($scope.simulateQuery) {
                    deferred = $q.defer();
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 1, false);
                    return deferred.promise;
                } else {
                    return results;
                }
            };

            $scope.selectedItemDestinationChange = function (item, index) {
                if (angular.isDefined(item) && item != null) {
                    $scope.FlatRateGroup.FlatRateList[index].searchDestinationText = item.Value;
                    $scope.FlatRateGroup.FlatRateList[index].DestinationLocationID = item.Key;
                }
                else {
                    $scope.FlatRateGroup.FlatRateList[index].searchDestinationText = '';
                    $scope.FlatRateGroup.FlatRateList[index].DestinationLocationID = '';
                }
            };

            function createFilterForFlatRate(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(ettag) {
                    var source = angular.lowercase(ettag.Value);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }

            function createFilterDuplicateForFlatRate(arr) {
                return function filterFn(ettag) {
                    return !(arr.duplicateProp('Name', ettag.Value));
                };
            }

            $scope.checkText_Destination = function (text, index) {
                var result = null;
                if (text) {
                    for (var i = 0; i < $scope.FlatRateLocation.length; i++) {
                        if ($scope.FlatRateLocation[i].Key == text || $scope.FlatRateLocation[i].Value == text) {
                            result = $scope.FlatRateLocation[i];
                            break;
                        }
                    }
                }
                if (result) {
                    $scope.selectedItemDestinationChange(result, index);
                }
                else {
                    $scope.selectedItemDestinationChange(null, index);
                }
            }

            $scope.setSelectedBeginDate = function (selectedDate) {
                $scope.FlatRateGroup.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                if ($scope.FlatRateGroup.BeginDate > $scope.FlatRateGroup.EndDate) {
                    $scope.FlatRateGroup.EndDate = $scope.FlatRateGroup.BeginDate;
                }
            };

            $scope.setSelectedEndDate = function (selectedDate) {
                $scope.FlatRateGroup.EndDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                $scope.FlatRateGroup.NoSpecific = ($scope.FlatRateGroup.EndDate == '9999-12-31T00:00:00');
            };

            $scope.addEndDate = function () {
                if ($scope.FlatRateGroup.NoSpecific == true) {
                    $scope.FlatRateGroup.EndDate = "9999-12-31T00:00:00";
                }
                else if ($scope.FlatRateGroup.EndDate == "9999-12-31T00:00:00") {
                    $scope.FlatRateGroup.EndDate = $scope.FlatRateGroup.BeginDate;
                }
            };

            $scope.HasError = false;
            $scope.ErrorText = "";
            //$scope.VATCodeDisable = false;


            $scope.Textcategory = 'ACCOUNT_SETTING';
            $scope.content.Header = $scope.Text[$scope.Textcategory].FLATRATE_SETTING;
            $scope.Textcategory2 = 'SYSTEM';

            $scope.init = function () {
                getFlatRateLocation();
            }

            function getFlatRateLocation() {
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }
                var URL = CONFIG.SERVER + 'HRTR/GetFlatRateLocationAll/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.FlatRateLocation = response.data;

                    //onload Check Create/Edit
                    if ($scope.itemKey != "null") {
                        getFlateRateGroup();
                    }
                    else {
                        $scope.ready = true;
                    }

                }, function errorCallback(response) {
                    console.log('error GetFlatRateLocationAll.', response);
                });
            }

            function getFlateRateGroup() {
                var oRequestParameter = { InputParameter: { "FlatRateGroupID": $scope.itemKey }, CurrentEmployee: getToken(CONFIG.USER) }
                var URL = CONFIG.SERVER + 'HRTR/GetFlatRateGroup/';
                $scope.VATCodeDisable = true;

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.FlatRateGroup = response.data;
                    $scope.FlatRateGroup.NoSpecific = ($scope.FlatRateGroup.EndDate == '9999-12-31T00:00:00');
                    getFlateRate();
                    
                }, function errorCallback(response) {
                    console.log('error GetFlatRateGroup.', response);
                });
            }

            function getFlateRate() {
                var URL = CONFIG.SERVER + 'HRTR/GetFlatRate/';
                var oRequestParameter = { InputParameter: { "FlatRateGroupID": $scope.FlatRateGroup.FlatGroupID }, CurrentEmployee: getToken(CONFIG.USER) }
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.FlatRateGroup.FlatRateList = response.data;
                    for (var i = 0; i < $scope.FlatRateGroup.FlatRateList.length; i++) {
                        $scope.checkText_Source($scope.FlatRateGroup.FlatRateList[i].SourceLocationID, i);
                        $scope.checkText_Destination($scope.FlatRateGroup.FlatRateList[i].DestinationLocationID, i);
                        //$scope.FlatRateGroup.FlatRateList[i].SourceObject = $scope.FlatRateLocation[$scope.FlatRateLocation.findIndexWithAttr('Key', $scope.FlatRateGroup.FlatRateList[i].SourceLocationID)];
                        //$scope.FlatRateGroup.FlatRateList[i].DestinationObject = $scope.FlatRateLocation[$scope.FlatRateLocation.findIndexWithAttr('Key', $scope.FlatRateGroup.FlatRateList[i].DestinationLocationID)];
                    }
                    $scope.ready = true;
                }, function errorCallback(response) {
                    console.log('error GetFlatRate.', response);
                });
            }

            //onsave
            $scope.SaveData = function (ev) {
                var valid = true;
                if (!$scope.FlatRateGroup.Description) valid = false;
                else if (!$scope.FlatRateGroup.FlatRateList.length) valid = false;
                else if ($scope.FlatRateGroup.FlatRateList.nullProp('SourceLocationID') || $scope.FlatRateGroup.FlatRateList.nullProp('DestinationLocationID') || $scope.FlatRateGroup.FlatRateList.nullProp('Rate')) valid = false;

                if (!valid) {
                    $scope.HasError = true;
                    $scope.ErrorText = $scope.Text['ACCOUNT_SETTING'].PLEASE_INSERT_ALL_FIELDS;
                }
                else {
                    var oRequestParameter = { InputParameter: { "FlatRateGroup": $scope.FlatRateGroup }, CurrentEmployee: $scope.employeeData, Requestor: getToken(CONFIG.USER) }

                    var URL = CONFIG.SERVER + 'HRTR/SaveFlatRateGroup/';
                    console.log(URL);
                    $scope.HasError = false;
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        var output = response.data;
                        if (output > 1) {
                            $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                .textContent($scope.Text['ACCOUNT_SETTING'].GO_BACK)
                                .ariaLabel($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                .targetEvent(ev));
                            $location.path('/frmViewContent/323');
                        }
                        else {
                            //duplicate
                            $scope.HasError = true;
                            $scope.ErrorText = $scope.Text['ACCOUNT_SETTING'].DATA_DUPLICATE;
                        }

                    }, function errorCallback(response) {
                        // Error
                        console.log('error FlatRateSettingEditorController Save.', response);
                    });
                }
            }
        }]);
})();

