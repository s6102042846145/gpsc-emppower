﻿(function () {
    angular.module('ESSMobile')
        .controller('ExpenseRateSettingController', ['$scope', '$http', '$routeParams', '$location', '$window', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $window, CONFIG, $mdDialog) {

            console.log('ExpenseRateSettingController');
            $scope.requestType = $routeParams.id;
            $scope.formData = { searchKeyword: '' };
            $scope.firstLoad = true;

            $scope.Textcategory = 'ACCOUNT_SETTING';

            $scope.Textcategory2 = 'SYSTEM';
            $scope.content.isShowHeader = false;

            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }

            var URL = CONFIG.SERVER + 'HRTR/GetAllExpenseRate/';
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                $scope.ExpRate = response.data;
                $scope.masterDocuments = response.data;
                console.log('ExpRate.', $scope.ExpRate);

            }, function errorCallback(response) {
                // Error
                console.log('error ExpenseRateSettingController.', response);
            });


            $scope.CreateNewVAT = function () {
                $location.path('/frmViewContent/312');
            };

            $scope.EditVAT = function (vatcode) {
                $location.path('/frmViewContent/312/' + vatcode);
            };

            var search = function (item, keyword) {
                // Search Criteria
                var PersonalArea = item.PersonalAreaCode == "*" ? $scope.Text['ACCOUNT_SETTING'].ALL : item.AreaDescription;
                var TravelType = TravelTypeID == -1 ? $scope.Text['ACCOUNT_SETTING'].ALL : item.THCode;
                if (!keyword
                    || (item.AreaGroup.toLowerCase().indexOf(keyword) != -1)//1 
                    || (item.ContractNo.toLowerCase().indexOf(keyword) != -1) //1
                    || (item.DayFractionRoundingRuleGroup.toLowerCase().indexOf(keyword) != -1)//1
                    || (item.ExpenseRateType.toLowerCase().indexOf(keyword) != -1) //1
                    || (item.Value.toString().toLowerCase().indexOf(keyword) != -1) //1
                    || (item.NoCalIncome.toString().toLowerCase().indexOf(keyword) != -1) //1 //BeginDate//EndDate
                    || (!isNaN(keyword) && item.MinEmpSubGroup <= parseInt(keyword) && parseInt(keyword) <= item.MaxEmpSubGroup)
                    || (TravelType.toLowerCase().indexOf(keyword) != -1)
                    || (PersonalArea.toLowerCase().indexOf(keyword) != -1)
                    ) {
                    return true;
                }
                return false;
            };


            $scope.searchInBox = function () {
                $scope.firstLoad = false;
                console.log('searchInBox.', $scope.formData.searchKeyword);
                if (!$scope.formData.searchKeyword) {
                    console.log('just reset documents.');
                    $scope.ExpRate = $scope.masterDocuments;
                }
                else {
                    var count = $scope.masterDocuments.length;
                    var docs = [];
                    for (var i = 0; i < count; i++) {
                        if (search($scope.masterDocuments[i], $scope.formData.searchKeyword.toLowerCase())) {
                            docs.push($scope.masterDocuments[i]);
                        }
                    }
                    $scope.ExpRate = docs;
                }
            };

            $scope.DeleteConfirm = function (ExpenseRateID, ev) {
                var confirm = $mdDialog.confirm()
                    .title($scope.Text['ACCOUNT_SETTING'].WANT_TO_DELETE)
                    .textContent($scope.Text['ACCOUNT_SETTING'].CAN_NOT_ROLLBACK_PLEASE_CONFIRM)
                    .ariaLabel($scope.Text['ACCOUNT_SETTING'].WANT_TO_DELETE)
                    .targetEvent(ev)
                    .ok($scope.Text['SYSTEM'].BUTTON_YES)
                    .cancel($scope.Text['SYSTEM'].BUTTON_NO);
                $mdDialog.show(confirm).then(function () {
                    //delete
                    //var URL = CONFIG.SERVER + 'HRTR/DeleteVATType/';
                    var oRequestParameter = { InputParameter: { "EXPENSERATEID": ExpenseRateID }, CurrentEmployee: getToken(CONFIG.USER) };
                    var URL = CONFIG.SERVER + 'HRTR/DeleteExpenseRate/';
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        var URL = CONFIG.SERVER + 'HRTR/GetAllExpenseRate/';
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            // Success
                            $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['ACCOUNT_SETTING'].DELETE_COMPLETED)
                                .textContent($scope.Text['ACCOUNT_SETTING'].CAN_NOT_ROLLBACK)
                                .ariaLabel($scope.Text['ACCOUNT_SETTING'].DELETE_COMPLETED)
                                .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                .targetEvent(ev)
                            );
                            $scope.ExpRate = response.data;
                            $scope.masterDocuments = response.data;
                            console.log('ExpRate.', $scope.ExpRate);

                        }, function errorCallback(response) {
                            // Error
                            console.log('error ExpenseRateSettingController.', response);
                        });
                    }, function errorCallback(response) {
                        // Error
                        console.log('error ExpenseRateSettingController.', response);
                    });

                    //get again


                });


            }



        }]);
})();