﻿(function () {
    angular.module('ESSMobile')
        .controller('CreditCardSettingController', ['$scope', '$http', '$routeParams', '$filter', '$location', '$window', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $filter, $location, $window, CONFIG, $mdDialog) {

            console.log('CreditCardSettingController');
            $scope.requestType = $routeParams.id;
            $scope.formData = { searchKeyword: '' };
            $scope.firstLoad = true;

            $scope.Textcategory = 'ACCOUNT_SETTING';
            $scope.Textcategory2 = 'SYSTEM';
            $scope.content.isShowHeader = false;
            /*var URL = CONFIG.SERVER + 'HRTR/GetAllINFOTYPE0001';
            var dateToday = new Date();
            var oRequestParameter = { InputParameter: { "CreateDate": $filter('date')(dateToday, 'yyyy-MM-ddT00:00:00') }, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };
            
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                $scope.employeeDataList = response.data;
                console.log('employeeDataList', $scope.employeeDataList);

            }, function errorCallback(response) {
                console.log('error Position list', response);

            });
            */
            $scope.getINFOTYPE0001ForLookup = function () {
                var URL = CONFIG.SERVER + 'HRTR/GetINFOTYPE0001ForLookup';
                var dateToday = new Date();
                var oRequestParameter = { InputParameter: { "CreateDate": $filter('date')(new Date(), 'yyyy-MM-ddT00:00:00') }, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.objINFOTYPE0001Lookup = response.data;
                    console.log('GetINFOTYPE0001ForLookup.', $scope.objINFOTYPE0001Lookup);
                }, function errorCallback(response) {
                    console.log('error CreditCardSettingController GetINFOTYPE0001ForLookup.', response);
                });
            };

            var oRequestParameter = { InputParameter: { "CreateDate": $filter('date')(new Date(), 'yyyy-MM-ddT00:00:00') }, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };
            var URL = CONFIG.SERVER + 'HRTR/GetAllCreditCard/';
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                $scope.CreditCardList = response.data;
                $scope.masterDocuments = response.data;
                console.log('CreditCardList.', $scope.CreditCardList);
                $scope.getINFOTYPE0001ForLookup();
            }, function errorCallback(response) {
                // Error
                console.log('error CreditCardSettingController.', response);
            });


            $scope.CreateNew = function () {
                $location.path('/frmViewContent/316');
            };

            $scope.Edit = function (cardno, empID) {
                $location.path('/frmViewContent/316/' + cardno + '*' + empID);
            };

            var search = function (item, keyword) {
                // Search Criteria
                //var temp = $scope.getName(item.EmployeeID);
                var temp = $scope.objINFOTYPE0001Lookup[item.EmployeeID];
                if (!keyword
                    || (item.CreditCardNo.toLowerCase().indexOf(keyword) != -1)
                    || (item.EmployeeID.toLowerCase().indexOf(keyword) != -1)
                    || (temp.toLowerCase().indexOf(keyword) != -1)
                    || (item.PaymentDueDate.toString().toLowerCase().indexOf(keyword) != -1)
                    ) {
                    return true;
                }
                return false;
            };



            $scope.getName = function (id) {
                var name = "";
                for (var i = 0; i < $scope.employeeDataList.length; i++) {
                    if ($scope.employeeDataList[i].EmployeeID == id) {
                        name = $scope.employeeDataList[i].Name;
                    }
                }

                return name;
            };


            $scope.searchInBox = function () {
                $scope.firstLoad = false;
                console.log('searchInBox.', $scope.formData.searchKeyword);
                if (!$scope.formData.searchKeyword) {
                    console.log('just reset documents.');
                    $scope.CreditCardList = $scope.masterDocuments;
                }
                else {
                    var count = $scope.masterDocuments.length;
                    var docs = [];
                    for (var i = 0; i < count; i++) {
                        if (search($scope.masterDocuments[i], $scope.formData.searchKeyword.toLowerCase())) {
                            docs.push($scope.masterDocuments[i]);
                        }
                    }
                    $scope.CreditCardList = docs;
                }
            };

            $scope.DeleteConfirm = function (creditcard, employeeid, ev) {
                var confirm = $mdDialog.confirm()
                    .title($scope.Text['ACCOUNT_SETTING'].WANT_TO_DELETE)
                    .textContent($scope.Text['ACCOUNT_SETTING'].CAN_NOT_ROLLBACK_PLEASE_CONFIRM)
                    .ariaLabel($scope.Text['ACCOUNT_SETTING'].WANT_TO_DELETE)
                    .targetEvent(ev)
                    .ok($scope.Text['SYSTEM'].BUTTON_YES)
                    .cancel($scope.Text['SYSTEM'].BUTTON_NO);
                $mdDialog.show(confirm).then(function () {
                    var URL = CONFIG.SERVER + 'HRTR/DeleteCreditCard/';
                    var oRequestParameter = { InputParameter: { "CreditCard": creditcard, "EmployeeID": employeeid }, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        if (response.data == true) {
                            var oRequestParameter = { InputParameter: { "CreateDate": $filter('date')(new Date(), 'yyyy-MM-ddT00:00:00') }, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };
                            var URL = CONFIG.SERVER + 'HRTR/GetAllCreditCard/';
                            $http({
                                method: 'POST',
                                url: URL,
                                data: oRequestParameter
                            }).then(function successCallback(response) {
                                // Success
                                $scope.CreditCardList = response.data;
                                $scope.masterDocuments = response.data;
                                $mdDialog.show(
                                $mdDialog.alert()
                                    .clickOutsideToClose(true)
                                    .title($scope.Text['ACCOUNT_SETTING'].DELETE_COMPLETED)
                                    .textContent($scope.Text['ACCOUNT_SETTING'].CAN_NOT_ROLLBACK)
                                    .ariaLabel($scope.Text['ACCOUNT_SETTING'].DELETE_COMPLETED)
                                    .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                    .targetEvent(ev)
                                );
                            }, function errorCallback(response) {
                                // Error
                                console.log('error CreditCardSettingController.', response);
                            });
                        }
                        else {
                            $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['ACCOUNT_SETTING'].CAN_NOT_DELETE)
                                .textContent($scope.Text['ACCOUNT_SETTING'].DATA_ACTIVE)
                                .ariaLabel($scope.Text['ACCOUNT_SETTING'].CAN_NOT_DELETE)
                                .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                .targetEvent(ev)
                            );
                        }
                    }, function errorCallback(response) {
                        // Error
                        console.log('error CreditCardSettingController.', response);
                    });
                });
            }

        }]);
})();