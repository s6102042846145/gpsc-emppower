﻿(function () {
    angular.module('ESSMobile')
        .controller('ExpenseTypeAmountInRightSettingController', ['$scope', '$http', '$routeParams', '$location', '$window', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $window, CONFIG, $mdDialog) {

            console.log('ExpenseTypeAmountInRightSettingEditorController');
            $scope.requestType = $routeParams.id;
            $scope.formData = { searchKeyword: '' };
            $scope.firstLoad = true;

            $scope.Textcategory = 'ACCOUNT_SETTING';

            $scope.Textcategory2 = 'SYSTEM';
            $scope.content.isShowHeader = false;

            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }

            var URL = CONFIG.SERVER + 'HRTR/GetAllExpenseTypeAmountInRight/';
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                $scope.documents = response.data;
                $scope.masterDocuments = response.data;
                console.log('document.', $scope.documents);

            }, function errorCallback(response) {
                // Error
                console.log('error ExpenseTypeAmountInRightSettingController.', response);
            });

            var URL = CONFIG.SERVER + 'HRTR/GetExpenseTypeAllForLookup/';
            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                $scope.GetexpenseType = response.data;
                //$scope.masterDocuments = response.data;
                console.log('GetexpenseType.', $scope.GetexpenseType);

            }, function errorCallback(response) {
                // Error
                console.log('error ExpensTypeGroupSettingController.', response);
            });

            $scope.CreateNewVAT = function () {
                $location.path('/frmViewContent/329');
            };

            $scope.EditVAT = function (vatcode) {
                $location.path('/frmViewContent/329/' + vatcode);
            };

            var search = function (item, keyword) {
                // Search Criteria
                if (!keyword
                    || (item.AmountInRight.toString().toLowerCase().indexOf(keyword) != -1)
                    || ($scope.GetexpenseType[item.ExpenseTypeID].toLowerCase().indexOf(keyword) != -1)

                    ) {
                    return true;
                }
                return false;
            };

            function getExpenseType(carID) {
                var temp = "";
                for (var i = 0; i < $scope.CarTypeList.length; i++) {
                    if ($scope.CarTypeList[i].CarTypeID == carID) {
                        temp = $scope.CarTypeList[i].Description;
                    }
                }
                return temp;
            }


            $scope.searchInBox = function () {
                $scope.firstLoad = false;
                console.log('searchInBox.', $scope.formData.searchKeyword);
                if (!$scope.formData.searchKeyword) {
                    console.log('just reset documents.');
                    $scope.documents = $scope.masterDocuments;
                }
                else {
                    var count = $scope.masterDocuments.length;
                    var docs = [];
                    for (var i = 0; i < count; i++) {
                        if (search($scope.masterDocuments[i], $scope.formData.searchKeyword.toLowerCase())) {
                            docs.push($scope.masterDocuments[i]);
                        }
                    }
                    $scope.documents = docs;
                }
            };

            $scope.DeleteConfirm = function (RentRateID, ev) {
                var confirm = $mdDialog.confirm()
                    .title($scope.Text['ACCOUNT_SETTING'].WANT_TO_DELETE)
                    .textContent($scope.Text['ACCOUNT_SETTING'].CAN_NOT_ROLLBACK_PLEASE_CONFIRM)
                    .ariaLabel($scope.Text['ACCOUNT_SETTING'].WANT_TO_DELETE)
                    .targetEvent(ev)
                    .ok($scope.Text['SYSTEM'].BUTTON_YES)
                    .cancel($scope.Text['SYSTEM'].BUTTON_NO);
                $mdDialog.show(confirm).then(function () {
                    //delete
                    var oRequestParameter = { InputParameter: { "RENTRATEID": RentRateID }, CurrentEmployee: getToken(CONFIG.USER) }
                    var URL = CONFIG.SERVER + 'HRTR/DeleteExpenseTypeAmountInRight/';
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        if (response.data) {
                            var URL = CONFIG.SERVER + 'HRTR/GetAllExpenseTypeAmountInRight/';
                            $http({
                                method: 'POST',
                                url: URL,
                                data: oRequestParameter
                            }).then(function successCallback(response) {
                                // Success
                                $scope.documents = response.data;
                                $scope.masterDocuments = response.data;
                                $mdDialog.show(
                                $mdDialog.alert()
                                    .clickOutsideToClose(true)
                                    .title($scope.Text['ACCOUNT_SETTING'].DELETE_COMPLETED)
                                    .textContent($scope.Text['ACCOUNT_SETTING'].CAN_NOT_ROLLBACK)
                                    .ariaLabel($scope.Text['ACCOUNT_SETTING'].DELETE_COMPLETED)
                                    .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                    .targetEvent(ev)
                                );
                            }, function errorCallback(response) {
                                // Error
                                console.log('error MileageRateSettingController.', response);
                            });
                        }
                        else {
                            $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['ACCOUNT_SETTING'].CAN_NOT_DELETE)
                                .textContent($scope.Text['ACCOUNT_SETTING'].DATA_ACTIVE)
                                .ariaLabel($scope.Text['ACCOUNT_SETTING'].CAN_NOT_DELETE)
                                .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                .targetEvent(ev)
                            );
                        }
                    }, function errorCallback(response) {
                        // Error
                        console.log('error MileageRateSettingController.', response);
                    });
                });
            }
        }]);
})();