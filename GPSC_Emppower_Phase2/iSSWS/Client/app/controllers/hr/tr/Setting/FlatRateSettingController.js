﻿(function () {
    angular.module('ESSMobile')
        .controller('FlatRateSettingController', ['$scope', '$http', '$routeParams', '$location', '$window', 'CONFIG', '$timeout', '$q', '$log', '$mdDialog', function ($scope, $http, $routeParams, $location, $window, CONFIG, $timeout, $q, $log, $mdDialog) {

            console.log('FlatRateSettingController');
            $scope.requestType = $routeParams.id;
            $scope.formData = { searchKeyword: '' };
            $scope.firstLoad = true;
            $scope.FlatRateList = [{}];
            $scope.FlatRateList.length = 0;

            $scope.Textcategory = 'ACCOUNT_SETTING';

            $scope.Textcategory2 = 'SYSTEM';
            $scope.content.isShowHeader = false;
            $scope.expanded = false;
            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }



            var URL = CONFIG.SERVER + 'HRTR/GetAllFlatRateGroup/';
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                $scope.flatrategroup = response.data;
                for (var j = 0; j < $scope.flatrategroup.length; j++) {
                    var URL = CONFIG.SERVER + 'HRTR/GetFlatRate/';
                    var oRequestParameter = { InputParameter: { "FlatRateGroupID": $scope.flatrategroup[j].FlatGroupID }, CurrentEmployee: getToken(CONFIG.USER) }
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        if (response.data.length) {
                            $scope.flatrategroup[$scope.flatrategroup.findIndexWithAttr('FlatGroupID', response.data[0].FlatGroupID)].FlatRateList = response.data;
                        }
                        $scope.masterDocuments = angular.copy($scope.flatrategroup);
                    }, function errorCallback(response) {
                        // Error
                        console.log('error FlatRateSettingController.', response);
                    });
                    console.log('$scope.flatrategroup', $scope.flatrategroup);
                }

                URL = CONFIG.SERVER + 'HRTR/GetFlatRateLocationForLookup/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.flatrateLocation = response.data;
                }, function errorCallback(response) {
                    // Error
                    console.log('error FlatRateSettingController.', response);
                });
                console.log('flatrategroup.', $scope.flatrategroup);
            }, function errorCallback(response) {
                // Error
                console.log('error FlatRateSettingController.', response);
            });

            $scope.CreateNewVAT = function () {
                $location.path('/frmViewContent/324');
            };

            $scope.EditVAT = function (id) {
                $location.path('/frmViewContent/324/' + id);
            };

            var search = function (item, keyword) {
                if (!keyword
                    || (item.FlatGroupID.toString().toLowerCase().indexOf(keyword) != -1)
                    || (item.Description.toLowerCase().indexOf(keyword) != -1)
                    || (item.FlatRateList.likeWithMappingPropIndexOf($scope.flatrateLocation, 'SourceLocationID', keyword) != -1)
                    || (item.FlatRateList.likeWithMappingPropIndexOf($scope.flatrateLocation, 'DestinationLocationID', keyword) != -1)
                    || (item.FlatRateList.likePropIndexOf('Rate', keyword) != -1)
                    ) {
                    return true;
                }
                return false;
            };

            Array.prototype.likePropIndexOf = function (prop, val) {
                for (var i = 0, _len = this.length; i < _len; i++) {
                    if (this[i][prop] && this[i][prop].toString().toLowerCase().indexOf(val) > -1)
                        return i;
                }
                return -1;
            };

            Array.prototype.likeWithMappingPropIndexOf = function (func, prop, val) {
                for (var i = 0, _len = this.length; i < _len; i++) {
                    if (this[i][prop] && func[this[i][prop]].toLowerCase().indexOf(val) > -1)
                        return i;
                }
                return -1;
            };

            function findAccount(id) {
                var temp = "";
                for (var i = 0; i < $scope.flatrategroup.length; i++) {
                    if ($scope.flatrategroup[i].FlatGroupID == id) {
                        temp = $scope.flatrategroup[i].Description;
                    }
                }
                return temp;
            }


            $scope.searchInBox = function () {
                $scope.firstLoad = false;
                if (!$scope.formData.searchKeyword) {
                    $scope.flatrategroup = $scope.masterDocuments;
                }
                else {
                    var count = $scope.masterDocuments.length;
                    var docs = [];
                    for (var i = 0; i < count; i++) {
                        if (search($scope.masterDocuments[i], $scope.formData.searchKeyword.toLowerCase())) {
                            docs.push($scope.masterDocuments[i]);
                        }
                    }
                    $scope.flatrategroup = docs;
                }
            };

            $scope.DeleteConfirm = function (FlatGroupID, ev) {
                var confirm = $mdDialog.confirm()
                    .title($scope.Text['ACCOUNT_SETTING'].WANT_TO_DELETE)
                    .textContent($scope.Text['ACCOUNT_SETTING'].CAN_NOT_ROLLBACK_PLEASE_CONFIRM)
                    .ariaLabel($scope.Text['ACCOUNT_SETTING'].WANT_TO_DELETE)
                    .targetEvent(ev)
                    .ok($scope.Text['SYSTEM'].BUTTON_YES)
                    .cancel($scope.Text['SYSTEM'].BUTTON_NO);
                $mdDialog.show(confirm).then(function () {
                    var oRequestParameter = { InputParameter: { "FlatGroupID": FlatGroupID }, CurrentEmployee: getToken(CONFIG.USER) }
                    var URL = CONFIG.SERVER + 'HRTR/DeleteFlatRateGroup/';
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        if (response.data) {
                            var URL = CONFIG.SERVER + 'HRTR/GetAllFlatRateGroup/';
                            $http({
                                method: 'POST',
                                url: URL,
                                data: oRequestParameter
                            }).then(function successCallback(response) {
                                // Success
                                $scope.flatrategroup = response.data;
                                for (var i = 0; i < $scope.flatrategroup.length; i++) {
                                    var URL = CONFIG.SERVER + 'HRTR/GetFlatRate/';
                                    var oRequestParameter = { InputParameter: { "FlatRateGroupID": $scope.flatrategroup[i].FlatGroupID }, CurrentEmployee: getToken(CONFIG.USER) }
                                    $http({
                                        method: 'POST',
                                        url: URL,
                                        data: oRequestParameter
                                    }).then(function successCallback(response) {
                                        // Success
                                        if (response.data.length) {
                                            $scope.flatrategroup[$scope.flatrategroup.findIndexWithAttr('FlatGroupID', response.data[0].FlatGroupID)].FlatRateList = response.data;
                                        }
                                    }, function errorCallback(response) {
                                        // Error
                                        console.log('error FlatRateSettingController.', response);
                                    });
                                }
                                URL = CONFIG.SERVER + 'HRTR/GetFlatRateLocationForLookup/';
                                $http({
                                    method: 'POST',
                                    url: URL,
                                    data: oRequestParameter
                                }).then(function successCallback(response) {
                                    // Success
                                    $mdDialog.show(
                                    $mdDialog.alert()
                                        .clickOutsideToClose(true)
                                        .title($scope.Text['ACCOUNT_SETTING'].DELETE_COMPLETED)
                                        .textContent($scope.Text['ACCOUNT_SETTING'].CAN_NOT_ROLLBACK)
                                        .ariaLabel($scope.Text['ACCOUNT_SETTING'].DELETE_COMPLETED)
                                        .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                        .targetEvent(ev)
                                    );
                                    $scope.flatrateLocation = response.data;

                                }, function errorCallback(response) {
                                    // Error
                                    console.log('error FlatRateSettingController.', response);
                                });
                                console.log('flatrategroup.', $scope.flatrategroup);
                            }, function errorCallback(response) {
                                // Error
                                console.log('error FlatRateSettingController.', response);
                            });
                        }
                        else {
                            $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['ACCOUNT_SETTING'].CAN_NOT_DELETE)
                                .textContent($scope.Text['ACCOUNT_SETTING'].DATA_ACTIVE)
                                .ariaLabel($scope.Text['ACCOUNT_SETTING'].CAN_NOT_DELETE)
                                .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                .targetEvent(ev)
                            );
                        }

                    }, function errorCallback(response) {
                        // Error
                        console.log('error FlatRateSettingController.', response);
                    });
                });
            }

        }])
})();