﻿(function () {
    angular.module('ESSMobile')
        .controller('ExpensTypeGroupSettingEditorController', ['$scope', '$http', '$routeParams', '$location', '$mdDialog', 'CONFIG', '$timeout', '$q', '$log', function ($scope, $http, $routeParams, $location, $mdDialog, CONFIG, $timeout, $q, $log) {

            console.log('ExpensTypeGroupSettingEditorController');

            $scope.content.isShowHeader = true;
            $scope.content.isShowBackIcon = true;
            $scope.Textcategory = 'ACCOUNT_SETTING';
            $scope.content.Header = $scope.Text[$scope.Textcategory].EXPENSEGROUP_TITLE;
            $scope.Textcategory2 = 'SYSTEM';
            $scope.oExpTypeGroup = {
                ExpenseTypeGroupID: 0,
                Name: '',
                Remark: '',
                IsRequireAcceptance: false,
                searchETGTagText: [],
                ExpenseTypeGroupTag: [],
                searchETText: [],
                ExpenseType: []
            };

            $scope.addRow = function (ev) {
                if ($scope.oExpTypeGroup.searchETGTagText.hasNull() && $scope.oExpTypeGroup.searchETGTagText.length > 0)
                    $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['ACCOUNT_SETTING'].CAN_NOT_ADD_ROW)
                                .textContent($scope.Text['ACCOUNT_SETTING'].PREVIOUS_ROW_IS_NULL)
                                .ariaLabel($scope.Text['ACCOUNT_SETTING'].THANK)
                                .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                .targetEvent(ev));
                else {
                    $scope.oExpTypeGroup.searchETGTagText.push('');
                    $scope.oExpTypeGroup.ExpenseTypeGroupTag.push('');
                }
            };

            $scope.removeRow = function (index) {
                $scope.oExpTypeGroup.searchETGTagText.splice(index, 1);
                $scope.oExpTypeGroup.ExpenseTypeGroupTag.splice(index, 1);
            };

            $scope.addRowExpenseType = function (ev) {
                if ($scope.oExpTypeGroup.searchETText.hasNull() && $scope.oExpTypeGroup.searchETText.length > 0)
                    $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['ACCOUNT_SETTING'].CAN_NOT_ADD_ROW)
                                .textContent($scope.Text['ACCOUNT_SETTING'].PREVIOUS_ROW_IS_NULL)
                                .ariaLabel($scope.Text['ACCOUNT_SETTING'].THANK)
                                .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                .targetEvent(ev));
                else {
                    $scope.oExpTypeGroup.searchETText.push(null);
                    $scope.oExpTypeGroup.ExpenseType.push(null);
                }
            };

            $scope.removeRowExpenseType = function (index) {
                $scope.oExpTypeGroup.searchETText.splice(index, 1);
                $scope.oExpTypeGroup.ExpenseType.splice(index, 1);
            };

            $scope.init = function () {
                getExpenseTypeGroupTag();
            }

            function getExpenseTypeGroupTag() {
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }
                var URL = CONFIG.SERVER + 'HRTR/GetAllExpenseTypeGroupTag/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.exptypegrouptag = response.data;
                    getExpenseType();
                }, function errorCallback(response) {
                    console.log('error GetAllExpenseTypeGroupTag.', response);
                });
            }

            function getExpenseType() {
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }
                var URL = CONFIG.SERVER + 'HRTR/GetAllExpenseType/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.exptype = response.data;
                    if ($scope.itemKey != "null") {
                        getExpenseTypeGruop();
                    }
                    else {
                        $scope.ready = true;
                    }
                }, function errorCallback(response) {
                    console.log('error GetAllExpenseType.', response);
                });
            }

            function getExpenseTypeGruop() {
                var URL = CONFIG.SERVER + 'HRTR/GetExpenseTypeGroup/';
                var oRequestParameter = { InputParameter: { "ExpenseTypeGroupID": $scope.itemKey }, CurrentEmployee: getToken(CONFIG.USER) }

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.oExpTypeGroup = response.data[0];
                    getExpenseTypeGroupTagMapping();
                }, function errorCallback(response) {
                    console.log('error GetExpenseTypeGroup.', response);
                });
            }

            function getExpenseTypeGroupTagMapping() {
                var oRequestParameter = { InputParameter: { "ExpenseTypeGroupID": $scope.itemKey }, CurrentEmployee: $scope.employeeData, Requestor: getToken(CONFIG.USER) };
                var URL = CONFIG.SERVER + 'HRTR/GetExpenseTypeGroupTagMapping/';

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    if (!$scope.oExpTypeGroup.searchETGTagText) $scope.oExpTypeGroup.searchETGTagText = [];
                    for (var i = 0; i < response.data.length; i++) {
                        $scope.oExpTypeGroup.searchETGTagText.push(response.data[i].Description);
                        $scope.oExpTypeGroup.ExpenseTypeGroupTag.push(response.data[i].ETGTagID);
                    }
                    getExpenseTypeGroupMapping();
                }, function errorCallback(response) {
                    console.log('error GetExpenseTypeGroupTagMapping.', response);
                });
            }

            function getExpenseTypeGroupMapping() {
                var oRequestParameter = { InputParameter: { "ExpenseTypeGroupID": $scope.itemKey }, CurrentEmployee: $scope.employeeData, Requestor: getToken(CONFIG.USER) };
                var URL = CONFIG.SERVER + 'HRTR/GetExpenseTypeGroupMapping/';

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    if (!$scope.oExpTypeGroup.searchETText) $scope.oExpTypeGroup.searchETText = [];
                    for (var i = 0; i < response.data.length; i++) {
                        var temp = $scope.exptype[$scope.exptype.findIndexWithAttr('ExpenseTypeID', response.data[i].ExpenseTypeID)];
                        $scope.oExpTypeGroup.searchETText.push(temp.Name);
                        $scope.oExpTypeGroup.ExpenseType.push(response.data[i].ExpenseTypeID);
                    }
                    $scope.ready = true;
                }, function errorCallback(response) {
                    console.log('error GetExpenseTypeGroupMapping.', response);
                });
            }


            $scope.SaveData = function (expgroupdID, ev) {
                var valid = true;
                if (!$scope.oExpTypeGroup.Name) valid = false;
                else if (!$scope.oExpTypeGroup.Remark) valid = false;
                else if ($scope.oExpTypeGroup.ExpenseTypeGroupTag.length <= 0) valid = false;
                else if ($scope.oExpTypeGroup.ExpenseType.length <= 0) valid = false;
                else {
                    for (var i = 0; i < $scope.oExpTypeGroup.ExpenseTypeGroupTag.length; i++) {
                        if (!$scope.oExpTypeGroup.ExpenseTypeGroupTag[i]) {
                            valid = false;
                            break;
                        }
                    }

                    for (var i = 0; i < $scope.oExpTypeGroup.ExpenseType.length; i++) {
                        if (!$scope.oExpTypeGroup.ExpenseType[i]) {
                            valid = false;
                            break;
                        }
                    }
                }

                if (!valid) {
                    $scope.HasError = true;
                    $scope.ErrorText = $scope.Text['ACCOUNT_SETTING'].PLEASE_INSERT_ALL_FIELDS;

                }
                else {
                    var oRequestParameter = { InputParameter: { "ExpenseTypeGroup": $scope.oExpTypeGroup }, CurrentEmployee: $scope.employeeData, Requestor: getToken(CONFIG.USER) }

                    var URL = CONFIG.SERVER + 'HRTR/SaveExpenseTypeGroup/';
                    console.log(URL);
                    $scope.HasError = false;
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        var output = response.data;
                        if (output > 0) {
                            $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                .textContent($scope.Text['ACCOUNT_SETTING'].GO_BACK)
                                .ariaLabel($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                .targetEvent(ev));
                            $location.path('/frmViewContent/301');
                        }
                        else {
                            //duplicate
                            $scope.HasError = true;
                            if ($scope.oExpTypeGroup.ExpenseTypeGroupID) {
                                $scope.ErrorText = $scope.Text['ACCOUNT_SETTING'].DATA_ACTIVE;
                            }
                            else {
                                $scope.ErrorText = $scope.Text['ACCOUNT_SETTING'].DATA_DUPLICATE;
                            }
                        }

                    }, function errorCallback(response) {
                        console.log('error SaveExpenseTypeGroup Save.', response);
                    });
                }
            }

            //auto complete ExpenseTypeGroupTag  
            function createFilterForETGTag(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(tag) {
                    var source = angular.lowercase(tag.Description);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }

            function createFilterDuplicateForETGTag(arr) {
                return function filterFn(tag) {
                    return !(arr.duplicate(tag.GroupTagID));
                };
            }

            $scope.querySearchETGTag = function (query, index) {
                if (!$scope.exptypegrouptag) return;
                $scope.oExpTypeGroup.ExpenseTypeGroupTag[index] = '';
                var results = angular.copy(query ? $scope.exptypegrouptag.filter(createFilterForETGTag(query)).filter(createFilterDuplicateForETGTag($scope.oExpTypeGroup.ExpenseTypeGroupTag)) : $scope.exptypegrouptag.filter(createFilterDuplicateForETGTag($scope.oExpTypeGroup.ExpenseTypeGroupTag)));
                results = results.splice(0, 30);
                return results;
            };

            $scope.selectedItemETGTagChange = function (item, index) {
                if (angular.isDefined(item) && item != null) {
                    $scope.oExpTypeGroup.ExpenseTypeGroupTag[index] = item.GroupTagID;
                    $scope.oExpTypeGroup.searchETGTagText[index] = item.Description;
                }
                else {
                    $scope.oExpTypeGroup.searchETGTagText[index] = '';
                    $scope.oExpTypeGroup.ExpenseTypeGroupTag[index] = '';
                }
            };

            /*var tempResult_ETGTag;
            $scope.tryToSelect_ETGTag = function (index) {
                $scope.oExpTypeGroup.searchETGTagText[index] = '';
                //$scope.oExpTypeGroup.ExpenseTypeGroupTag[index] = '';
            }*/

            $scope.checkText_ETGTag = function (text, index) {
                var result = null;
                if (text) {
                    for (var i = 0; i < $scope.exptypegrouptag.length; i++) {
                        if ($scope.exptypegrouptag[i].Description == text || $scope.exptypegrouptag[i].GroupTagID == text) {
                            result = $scope.exptypegrouptag[i];
                            break;
                        }
                    }
                }
                if (result) {
                    $scope.selectedItemETGTagChange(result, index);
                } 
                else {
                    $scope.selectedItemETGTagChange(null, index);
                }
            }
            //auto complete ExpenseTypeGroupTag


            //auto complete ExpenseType
            function createFilterForET(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(et) {
                    var source = angular.lowercase(et.Name);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }

            function createFilterDuplicateForET(arr) {
                return function filterFn(et) {
                    return !(arr.duplicate(et.ExpenseTypeID));
                };
            }

            $scope.querySearchET = function (query,index) {
                if (!$scope.exptype) return;
                $scope.oExpTypeGroup.ExpenseType[index] = '';
                var results = angular.copy(query ? $scope.exptype.filter(createFilterDuplicateForET($scope.oExpTypeGroup.ExpenseType)).filter(createFilterForET(query)) : $scope.exptype.filter(createFilterDuplicateForET($scope.oExpTypeGroup.ExpenseType)));
                results = results.splice(0, 30);
                return results;
            };

            $scope.selectedItemETChange = function (item, index) {
                if (angular.isDefined(item) && item != null) {
                    $scope.oExpTypeGroup.searchETText[index] = item.Name;
                    $scope.oExpTypeGroup.ExpenseType[index] = item.ExpenseTypeID;
                }
                else {
                    $scope.oExpTypeGroup.searchETText[index] = '';
                    $scope.oExpTypeGroup.ExpenseType[index] = '';
                }
            };

            /*var tempResult_ET;
            $scope.tryToSelect_ET = function (index) {
                $scope.oExpTypeGroup.searchETText[index] = '';
                $scope.oExpTypeGroup.ExpenseType[index] = '';
            }*/

            $scope.checkText_ET = function (text, index) {
                var result = null;
                if (text) {
                    for (var i = 0; i < $scope.exptype.length; i++) {
                        if ($scope.exptype[i].Name == text || $scope.exptype[i].ExpenseTypeID == text) {
                            result = $scope.exptype[i];
                            break;
                        }
                    }
                }
                if (result) {
                    $scope.selectedItemETChange(result, index);
                } else {
                    $scope.selectedItemETChange(null, index);
                }
            }
            //auto complete ExpenseType

        }]);
})();