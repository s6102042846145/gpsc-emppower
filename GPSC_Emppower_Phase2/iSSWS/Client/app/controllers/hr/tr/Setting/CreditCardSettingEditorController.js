﻿(function () {
    angular.module('ESSMobile')
        .controller('CreditCardSettingEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$timeout', '$q', '$log', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $timeout, $q, $log, $mdDialog) {

            console.log('CreditCardSettingEditorController');
            $scope.content.isShowHeader = true;
            $scope.content.isShowBackIcon = true;
            var employeeData = getToken(CONFIG.USER);

            $scope.requestType = $routeParams.id;
            $scope.formData = { searchKeyword: '' };
            $scope.data = {};
            $scope.CreditCard = {
                CreditCardNo: '',
                EmployeeID: '',
                IsActive: true,
                IssueDate: $filter('date')(new Date(), 'yyyy-MM-ddT00:00:00'),
                ExpireDate: $filter('date')(new Date(), 'yyyy-MM-ddT00:00:00'),
                PaymentDueDate: ''
            };
            $scope.HasError = false;
            $scope.ErrorText = "";
            $scope.CardNumberDisable = false;

            $scope.Textcategory = 'ACCOUNT_SETTING';
            $scope.content.Header = $scope.Text[$scope.Textcategory].CC_TITLE;
            $scope.Textcategory2 = 'SYSTEM';

            var res = $scope.itemKey.split("*");

            var URL = CONFIG.SERVER + 'HRTR/GetAllINFOTYPE0001';
            var dateToday = new Date();
            var oRequestParameter = { InputParameter: { "CreateDate": $filter('date')(dateToday, 'yyyy-MM-ddT00:00:00') }, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };

            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                $scope.employeeDataList = response.data;

                
                // Check Edit
                if ($scope.itemKey != "null") {
                    var oRequestParameter = { InputParameter: { "CardNo": res[0], "EmployeeID": res[1] }, CurrentEmployee: getToken(CONFIG.USER) }
                    var URL = CONFIG.SERVER + 'HRTR/GetCreditCard/';
                    $scope.CardNumberDisable = true;

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        $scope.CreditCard = response.data;
                        for (var j = 0; j < $scope.employeeDataList.length; j++) {
                            if ($scope.employeeDataList[j].EmployeeID == $scope.CreditCard.EmployeeID) {
                                $scope.CreditCard.EmployeeIDObject = $scope.employeeDataList[j];
                                break;
                            }
                        }
                        
                        console.log('setCreditCard.', $scope.CreditCard);
                    }, function errorCallback(response) {
                        // Error
                        console.log('error CreditCardSettingEditorController.', response);
                    });
                }

            }, function errorCallback(response) {
                console.log('error Position list', response);

            });

            $scope.setSelectedIssueDate = function (selectedDate) {
                $scope.CreditCard.IssueDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                if ($scope.CreditCard.IssueDate > $scope.CreditCard.ExpireDate) {
                    $scope.CreditCard.ExpireDate = $scope.CreditCard.IssueDate;
                }
            };

            $scope.setSelectedExpireDate = function (selectedDate) {
                $scope.CreditCard.ExpireDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                
            };

            //onsave
            $scope.SaveData = function (ev) {
                var valid = true;
                if (!$scope.CreditCard.CreditCardNo) valid = false;
                else if (!$scope.CreditCard.EmployeeID) valid = false;
                else if (!$scope.CreditCard.IssueDate) valid = false;
                else if (!$scope.CreditCard.ExpireDate) valid = false;
                else if (!$scope.CreditCard.PaymentDueDate) valid = false;

                if (!valid) {
                        $scope.HasError = true;
                        $scope.ErrorText = $scope.Text['ACCOUNT_SETTING'].PLEASE_INSERT_ALL_FIELDS;
                }
                else 
                {
                    var oRequestParameter = { InputParameter: { "CreditCard": $scope.CreditCard }, CurrentEmployee: $scope.employeeData, Requestor: getToken(CONFIG.USER) }

                    var URL = CONFIG.SERVER + 'HRTR/SaveCreditCard/';
                    $scope.HasError = false;
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        var output = response.data;
                        if (output > 0) {
                            $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                .textContent($scope.Text['ACCOUNT_SETTING'].GO_BACK)
                                .ariaLabel($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                .targetEvent(ev));
                            $location.path('/frmViewContent/308');
                        }
                        else {
                            $scope.HasError = true;
                            $scope.ErrorText = "Save failed";
                        }

                    }, function errorCallback(response) {
                        // Error
                        console.log('error CreditCardSettingEditorController Save.', response);
                    });
                }
            };

            //auto complete Employee
            $scope.querySearchEmployee = function (query) {
                var results = query ? $scope.employeeDataList.filter(createFilterForEmployee(query)) : $scope.employeeDataList, deferred;
                if ($scope.simulateQuery) {
                    deferred = $q.defer();
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 1000, false);
                    return deferred.promise;
                } else {
                    return results;
                }
            };

            $scope.selectedItemEmployeeChange = function (item) {
                if (angular.isDefined(item)) {
                    $scope.CreditCard.EmployeeIDObject = item;
                    $scope.CreditCard.EmployeeID = item.EmployeeID;
                }
                else {
                    $scope.CreditCard.EmployeeIDObject = null;
                    $scope.CreditCard.EmployeeID = '';
                }
            };

            $scope.newData = function (data) {
                alert("Sorry! You'll need to create a Constitution for " + data + " first!");
            };

            function createFilterForEmployee(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    var Employee = angular.lowercase(x.EmployeeID + ' : ' + x.Name);
                    return (Employee.indexOf(lowercaseQuery) >= 0);
                };
            }

        }]);
})();

