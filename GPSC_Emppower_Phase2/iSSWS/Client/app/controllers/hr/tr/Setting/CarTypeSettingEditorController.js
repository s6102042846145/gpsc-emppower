﻿(function () {
    angular.module('ESSMobile')
        .controller('CarTypeSettingEditorController', ['$scope', '$http', '$routeParams', '$location', 'CONFIG', function ($scope, $http, $routeParams, $location, CONFIG) {

            console.log('CarTypeSettingEditorControleer');
            $scope.requestType = $routeParams.id;
            $scope.content.isShowHeader = true;
            $scope.content.isShowBackIcon = true;
            $scope.Textcategory = 'ACCOUNT_SETTING';
            $scope.content.Header = $scope.Text[$scope.Textcategory].CARTYPE_TITLE;

            $scope.Textcategory2 = 'SYSTEM';
            $scope.CarType = {
                DescriptionTH: '',
                DescriptionEN: '',
                VATTypes: []
            };

            $scope.init = function () {
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }
                var URL = CONFIG.SERVER + 'HRTR/GetAllVATType/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.vattype = response.data;
                    if ($scope.itemKey != "null") {
                        getCarType();
                    }
                    else {
                        $scope.ready = true;
                    }
                }, function errorCallback(response) {
                    console.log('error GetAllVATType.', response);
                });
            }

            function getCarType() {
                var oRequestParameter = { InputParameter: { "CARTYPEID": $scope.itemKey }, CurrentEmployee: getToken(CONFIG.USER) }
                var URL = CONFIG.SERVER + 'HRTR/GetCarType/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.CarType = response.data;
                    getCarTypeVATType();
                }, function errorCallback(response) {
                    // Error
                    console.log('error CarTypeSettingEditorControleer.', response);
                });
            }

            function getCarTypeVATType() {
                var oRequestParameter = { InputParameter: { "CARTYPEID": $scope.itemKey }, CurrentEmployee: $scope.employeeData, Requestor: getToken(CONFIG.USER) };
                var URL = CONFIG.SERVER + 'HRTR/GetCarTypeVATType/';

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.CarType.VATTypes = [];
                    for (var i = 0; i < response.data.length; i++) {
                        $scope.CarType.VATTypes.push(response.data[i].VATCode);
                    }
                    $scope.ready = true;
                    //reload VATType
                    /*var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }
                    //get VAT Type
                    var URL = CONFIG.SERVER + 'HRTR/GetAllVATType/';
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        $scope.vattype = response.data;
                        $scope.CarTypeVATType = [];
                        if ($scope.carTypeVATTypeList[0] == "*") {

                        }
                        else {
                            
                        }

                    }, function errorCallback(response) {
                        // Error
                        console.log('error CarTypeSettingEditorController.', response);
                    });*/
                }, function errorCallback(response) {
                    // Error
                    console.log('error CarTypeSettingEditorControleer Save.', response);
                });
            }
           

            //onload Check Create/Edit
            var URL = "";
            if ($scope.itemKey != "null") {
                


            }

            $scope.addRow = function () {
                var tag = {
                    name: $scope.name
                };

                $scope.tablelist.push(tag);
            };

            $scope.removeRow = function (index) {
                $scope.tablelist.splice(index, 1);
            };










            $scope.SaveData = function (expID) {

                var oExpType = {
                    ExpenseTypeID: ($scope.itemKey != "null") ? $scope.itemKey : -1,
                    Name: $scope.ExpType.Name,
                    GLAccount: $scope.ExpType.GLAccount,
                    OrderType: $scope.ExpType.OrderType,
                    IncomeGLAccount: $scope.ExpType.IncomeGLAccount

                };

                var oExpenseTypeTagList = [];
                for (var i = 0; i < $scope.tablelist.length; i++) {
                    oExpenseTypeTagList.push($scope.tablelist[i].TagCode.TagCode);
                }

                var URL = "";

                var valid = true;
                if ($scope.ExpType.Name == "") valid = false;
                else if ($scope.ExpType.GLAccount == "") valid = false;
                else if ($scope.ExpType.OrderType == "") valid = false;
                else if ($scope.ExpType.IncomeGLAccount == "") valid = false;

                if (!valid) {

                    $scope.HasError = true;
                    $scope.ErrorText = $scope.Text['ACCOUNT_SETTING'].PLEASE_INSERT_ALL_FIELDS;

                }
                    //check duplicate
                else if ($scope.itemKey == "null") { //create
                    var oAction = "create";

                    var oRequestParameter = { InputParameter: { "EXPTYPE": oExpType, "EXPTYPETAGLIST": oExpenseTypeTagList, "ACTION": oAction }, CurrentEmployee: $scope.employeeData, Requestor: getToken(CONFIG.USER) }

                    URL = CONFIG.SERVER + 'HRTR/SaveExpenseType/';
                    console.log(URL);
                    $scope.HasError = false;
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        var output = response.data;
                        if (output == 1) {
                            $location.path('/frmViewContent/302');
                        }
                        else {
                            //duplicate
                            $scope.HasError = true;
                            $scope.ErrorText = "Expense Type Name is duplicated";
                        }

                    }, function errorCallback(response) {
                        // Error
                        console.log('error ExpensTypeSettingEditorController Save.', response);
                    });
                }
                else //update
                {
                    var oAction = "update";
                    var oRequestParameter = { InputParameter: { "EXPGROUPTYPE": oExpType, "EXPTYPEIDLIST": oExpenseTypeTagList, "ACTION": oAction }, CurrentEmployee: $scope.employeeData, Requestor: getToken(CONFIG.USER) }
                    URL = CONFIG.SERVER + 'HRTR/SaveExpenseType';
                    $scope.HasError = false;
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        var output = response.data;
                        if (output == 1) {
                            $location.path('/frmViewContent/302');
                        }
                        else {
                            $scope.HasError = true;
                            $scope.ErrorText = "Update failed";
                        }

                    }, function errorCallback(response) {
                        // Error
                        console.log('error ExpensTypeSettingEditorController Save.', response);
                    });
                }
            }

            $scope.testlog = function (obj) {
                console.log('testlog', obj);
            }


        }]);
})();