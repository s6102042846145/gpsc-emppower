﻿(function () {
    angular.module('ESSMobile')
        .controller('DayFractionRoundingRuleSettingEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$timeout', '$q', '$log', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $timeout, $q, $log, $mdDialog) {

            $scope.content.isShowHeader = true;
            $scope.content.isShowBackIcon = true;
            var employeeData = getToken(CONFIG.USER);

            $scope.requestType = $routeParams.id;
            $scope.DayFractionRoundingRule = {
                //DayFractionID: '',
                MinHour: '',
                MaxHour: '',
                BeginDate: $filter('date')(new Date(), 'yyyy-MM-ddT00:00:00'),
                EndDate: '9999-12-31T00:00:00',
                EndDateUndefine: true,
                ReturnValue: '',
                Divisor: '',
                DayFractionRoundingRuleGroups: [],
                searchDayFractionRoundingRuleText: []
            };

            $scope.Textcategory = 'ACCOUNT_SETTING';
            $scope.content.Header = $scope.Text[$scope.Textcategory].DFR_TITLE;
            $scope.Textcategory2 = 'SYSTEM';
            $scope.DayFractionGroupList = [];

            $scope.addRow = function (ev) {
                if ($scope.DayFractionRoundingRule.DayFractionRoundingRuleGroups.hasNull() && $scope.DayFractionRoundingRule.DayFractionRoundingRuleGroups.length > 0)
                    $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['ACCOUNT_SETTING'].CAN_NOT_ADD_ROW)
                                .textContent($scope.Text['ACCOUNT_SETTING'].PREVIOUS_ROW_IS_NULL)
                                .ariaLabel($scope.Text['ACCOUNT_SETTING'].THANK)
                                .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                .targetEvent(ev));
                else {
                    $scope.DayFractionRoundingRule.searchDayFractionRoundingRuleText.push('');
                    $scope.DayFractionRoundingRule.DayFractionRoundingRuleGroups.push('');
                }
            };

            $scope.removeRow = function (index) {
                $scope.DayFractionRoundingRule.searchDayFractionRoundingRuleText.splice(index, 1);
                $scope.DayFractionRoundingRule.DayFractionRoundingRuleGroups.splice(index, 1);
            };

            $scope.init = function () {
                var oRequestParameter = { InputParameter: { "DayFractionRoundingRuleID": $scope.itemKey }, CurrentEmployee: getToken(CONFIG.USER) }
                var URL = CONFIG.SERVER + 'HRTR/GetAllDayFractionRoundingRuleGroup/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.DayFractionGroup = response.data;
                    console.log('DayFractionGroup.', $scope.DayFractionGroup);

                    //onload Check Create/Edit
                    if ($scope.itemKey != "null") {
                        getDayFractionRoundingRule();
                    }
                    else { $scope.ready = true; }

                }, function errorCallback(response) {
                    console.log('error GetAllDayFractionRoundingRuleGroup.', response);
                });
            }
            
            function getDayFractionRoundingRule() {
                var oRequestParameter = { InputParameter: { "DayFractionRoundingRuleID": $scope.itemKey }, CurrentEmployee: getToken(CONFIG.USER) }
                var URL = CONFIG.SERVER + 'HRTR/GetDayFractionRoundingRule/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.DayFractionRoundingRule = response.data;

                    if ($scope.DayFractionRoundingRule.ReturnValue == "-1") {
                        $scope.DayFractionRoundingRule.AllReturnValue = true;
                    }
                    else {
                        $scope.DayFractionRoundingRule.AllReturnValue = false;
                    }

                    if ($scope.DayFractionRoundingRule.EndDate == "9999-12-31T00:00:00") {
                        $scope.DayFractionRoundingRule.EndDateUndefine = true;
                    }
                    else {
                        $scope.DayFractionRoundingRule.EndDateUndefine = false;
                    }
                    getDayFractionRoundingRuleGroupMapping();
                    
                }, function errorCallback(response) {
                    console.log('error GetDayFractionRoundingRule.', response);
                });
            }

            function getDayFractionRoundingRuleGroupMapping() {
                var URL = CONFIG.SERVER + 'HRTR/GetDayFractionRoundingRuleGroupMapping/';
                var oRequestParameter = { InputParameter: { "DayFractionRoundingRuleID": $scope.itemKey }, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    var temp = null;
                    $scope.DayFractionRoundingRule.searchDayFractionRoundingRuleText = [];
                    for (var i = 0; i < response.data.length; i++) {
                        var temp = $scope.DayFractionGroup[$scope.DayFractionGroup.findIndexWithAttr('RoundingRuleGroupID', response.data[i].RoundingRuleGroupID)];
                        $scope.DayFractionRoundingRule.searchDayFractionRoundingRuleText.push(temp.Description);
                        $scope.DayFractionRoundingRule.DayFractionRoundingRuleGroups.push(temp.RoundingRuleGroupID);
                    }
                    $scope.ready = true;
                }, function errorCallback(response) {
                    console.log('error GetDayFractionRoundingRuleGroupMapping', response);
                });
            }

            $scope.addEndDate = function () {
                if ($scope.DayFractionRoundingRule.EndDateUndefine == true) {
                    $scope.DayFractionRoundingRule.EndDate = "9999-12-31T00:00:00";
                }
                else if ($scope.DayFractionRoundingRule.EndDate == "9999-12-31T00:00:00") {
                    $scope.DayFractionRoundingRule.EndDate = $scope.DayFractionRoundingRule.BeginDate;
                }
            };

            $scope.setSelectedBeginDate = function (selectedDate) {
                $scope.DayFractionRoundingRule.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                if ($scope.DayFractionRoundingRule.BeginDate > $scope.DayFractionRoundingRule.EndDate) {
                    $scope.DayFractionRoundingRule.EndDate = $scope.DayFractionRoundingRule.BeginDate;
                }
            };

            $scope.setSelectedEndDate = function (selectedDate) {
                $scope.DayFractionRoundingRule.EndDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                $scope.DayFractionRoundingRule.EndDateUndefine = ($scope.DayFractionRoundingRule.EndDate == '9999-12-31T00:00:00');
            };

            $scope.onAllReturnValueChange = function () {
                if ($scope.DayFractionRoundingRule.AllReturnValue == true) {
                    $scope.DayFractionRoundingRule.ReturnValue = -1;
                }
                else {
                    $scope.DayFractionRoundingRule.ReturnValue = '';
                }
            };

            //onsave
            $scope.SaveData = function (ev) {
                var URL = "";
                var valid = true;
                if ($scope.DayFractionRoundingRule.MinHour < 0) valid = false;
                else if (!$scope.DayFractionRoundingRule.MaxHour) valid = false;
                else if (!$scope.DayFractionRoundingRule.BeginDate) valid = false;
                else if (!$scope.DayFractionRoundingRule.EndDate) valid = false;
                else if (!$scope.DayFractionRoundingRule.ReturnValue) valid = false;
                else if (!$scope.DayFractionRoundingRule.Divisor) valid = false;
                else if (!$scope.DayFractionRoundingRule.DayFractionRoundingRuleGroups.length) valid = false;
                else {
                    for (var i = 0; i < $scope.DayFractionRoundingRule.DayFractionRoundingRuleGroups.length; i++) {
                        if (!$scope.DayFractionRoundingRule.DayFractionRoundingRuleGroups[i]) {
                            valid = false;
                            break;
                        }
                    }
                }

                if (!valid) {
                    $scope.DayFractionRoundingRule.DayFractionRoundingRuleGroups = angular.copy(tempDayFractionRoundingRuleGroups);
                    $scope.HasError = true;
                    $scope.ErrorText = $scope.Text['ACCOUNT_SETTING'].PLEASE_INSERT_ALL_FIELDS;
                }
                else {
                    var oRequestParameter = { InputParameter: { "DayFractionRoundingRule": $scope.DayFractionRoundingRule }, CurrentEmployee: $scope.employeeData, Requestor: getToken(CONFIG.USER) }

                    URL = CONFIG.SERVER + 'HRTR/SaveDayFractionRoundingRule/';
                    console.log(URL);
                    $scope.HasError = false;
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        var output = response.data;
                        if (output) {
                            $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                .textContent($scope.Text['ACCOUNT_SETTING'].GO_BACK)
                                .ariaLabel($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                .targetEvent(ev));
                            $location.path('/frmViewContent/306');
                        }
                        else {
                            //duplicate
                            $scope.HasError = true;
                            $scope.ErrorText = "DayFractionRoundingRule is duplicated";
                        }

                    }, function errorCallback(response) {
                        // Error
                        console.log('error ProjectSettingEditorController Save.', response);
                    });
                }
            };


            //auto complete Employee 
            $scope.simulateQuery = true;
            $scope.querySearchDayFractionRoundingRule = function (query, index) {
                $scope.DayFractionRoundingRule.DayFractionRoundingRuleGroups[index] = '';
                var results = query ? $scope.DayFractionGroup.filter(createFilterForDayFractionRoundingRule(query)).filter(createFilterDuplicateForDayFractionRoundingRule($scope.DayFractionRoundingRule.DayFractionRoundingRuleGroups)) : $scope.DayFractionGroup.filter(createFilterDuplicateForDayFractionRoundingRule($scope.DayFractionRoundingRule.DayFractionRoundingRuleGroups)), deferred;
                deferred = $q.defer();
                if ($scope.simulateQuery) {
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 250, false);
                    return deferred.promise;
                } else {
                    deferred.resolve(results);
                    return deferred.promise;
                }
            };

            $scope.selectedItemDayFractionRoundingRuleChange = function (item, index) {
                if (angular.isDefined(item) && item != null) {
                    $scope.DayFractionRoundingRule.searchDayFractionRoundingRuleText[index] = item.Description;
                    $scope.DayFractionRoundingRule.DayFractionRoundingRuleGroups[index] = item.RoundingRuleGroupID;
                }
                else {
                    $scope.DayFractionRoundingRule.searchDayFractionRoundingRuleText[index] = null;
                    $scope.DayFractionRoundingRule.DayFractionRoundingRuleGroups[index] = '';
                }
            };

            function createFilterForDayFractionRoundingRule(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(dfrr) {
                    var source = angular.lowercase(dfrr.Description);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }

            function createFilterDuplicateForDayFractionRoundingRule(arr) {
                return function filterFn(dfrr) {
                    return !(arr.duplicateProp('Description', dfrr.Description));
                };
            }

            $scope.checkText_DayFractionRoundingRule = function (text, index) {
                var result = null;
                if (text) {
                    for (var i = 0; i < $scope.DayFractionGroup.length; i++) {
                        if ($scope.DayFractionGroup[i].Description == text || $scope.DayFractionGroup[i].RoundingRuleGroupID == text) {
                            result = $scope.DayFractionGroup[i];
                            break;
                        }
                    }
                }
                if (result) {
                    $scope.selectedItemDayFractionRoundingRuleChange(result, index);
                }
                else {
                    $scope.selectedItemDayFractionRoundingRuleChange(null, index);
                }
            }
        }]);
})();

