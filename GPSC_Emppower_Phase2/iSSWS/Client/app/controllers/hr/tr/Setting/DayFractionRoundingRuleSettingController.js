﻿(function () {
    angular.module('ESSMobile')
        .controller('DayFractionRoundingRuleSettingController', ['$scope', '$http', '$routeParams', '$location', '$window', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $window, CONFIG, $mdDialog) {

            console.log('DayFractionRoundingRuleSettingController');
            $scope.requestType = $routeParams.id;
            $scope.formData = { searchKeyword: '' };
            $scope.firstLoad = true;

            $scope.Textcategory = 'ACCOUNT_SETTING';
            $scope.Textcategory2 = 'SYSTEM';
            $scope.content.isShowHeader = false;

            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }

            var URL = CONFIG.SERVER + 'HRTR/GetAllDayFractionRoundingRule/';
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                $scope.DayFraction = response.data;
                $scope.masterDocuments = response.data;
                //------------------------------------------------------------------------------------//
                oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }
                URL = CONFIG.SERVER + 'HRTR/GetAllDayFractionRoundingRuleGroupMapping/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.DayFractionGroupMapping = response.data;
                    for (var i = 0; i < response.data.length; i++) {
                        var index = $scope.DayFraction.findIndexWithAttr('DayFractionID', response.data[i].DayFractionID);
                        if (index >= 0)
                            $scope.DayFraction[index].DayFractionRoundingRuleGroups.push(response.data[i].RoundingRuleGroupID);
                    }
                    $scope.masterDocuments = angular.copy($scope.DayFraction);
                    console.log('DayFractionGroupMapping.', $scope.DayFraction);

                }, function errorCallback(response) {
                    // Error
                    console.log('error DayFractionRoundingRuleSettingController.', response);
                });
                //------------------------------------------------------------------------------------//
            }, function errorCallback(response) {
                // Error
                console.log('error DayFractionRoundingRuleSettingController.', response);
            });




            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }
            var URL = CONFIG.SERVER + 'HRTR/GetDayFractionRoundingRuleGroupForLookup/';
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                $scope.GetDayFractionRoundingRuleGroup = response.data;

                console.log('GetDayFractionRoundingRuleGroup.', $scope.GetDayFractionRoundingRuleGroup);

            }, function errorCallback(response) {
                // Error
                console.log('error DayFractionRoundingRuleSettingController.', response);
            });


            $scope.CreateNewVAT = function () {
                $location.path('/frmViewContent/314');
            };

            $scope.EditVAT = function (vatcode) {
                $location.path('/frmViewContent/314/' + vatcode);
            };

            var search = function (item, keyword) {
                // Search Criteria
                if (!keyword
                    || (item.ReturnValue.toString().toLowerCase().indexOf(keyword) != -1)
                    || (item.Divisor.toString().toLowerCase().indexOf(keyword) != -1)
                    || (item.MinHour.toString().toLowerCase().indexOf(keyword) != -1)
                    || (item.MaxHour.toString().toLowerCase().indexOf(keyword) != -1)
                    || (item.DayFractionRoundingRuleGroups.likeWithMappingIndexOf($scope.GetDayFractionRoundingRuleGroup, keyword) != -1)
                    ) {
                    return true;
                }
                return false;
            };


            $scope.searchInBox = function () {
                $scope.firstLoad = false;
                console.log('searchInBox.', $scope.formData.searchKeyword);
                if (!$scope.formData.searchKeyword) {
                    console.log('just reset documents.');
                    $scope.DayFraction = $scope.masterDocuments;
                }
                else {
                    var count = $scope.masterDocuments.length;
                    var docs = [];
                    for (var i = 0; i < count; i++) {
                        if (search($scope.masterDocuments[i], $scope.formData.searchKeyword.toLowerCase())) {
                            docs.push($scope.masterDocuments[i]);
                        }
                    }
                    $scope.DayFraction = docs;
                }
            };

            $scope.DeleteConfirm = function (darfrac, ev) {
                var confirm = $mdDialog.confirm()
                    .title($scope.Text['ACCOUNT_SETTING'].WANT_TO_DELETE)
                    .textContent($scope.Text['ACCOUNT_SETTING'].CAN_NOT_ROLLBACK_PLEASE_CONFIRM)
                    .ariaLabel($scope.Text['ACCOUNT_SETTING'].WANT_TO_DELETE)
                    .targetEvent(ev)
                    .ok($scope.Text['SYSTEM'].BUTTON_YES)
                    .cancel($scope.Text['SYSTEM'].BUTTON_NO);
                $mdDialog.show(confirm).then(function () {
                    //delete
                    //var URL = CONFIG.SERVER + 'HRTR/DeleteVATType/';
                    var oRequestParameter = { InputParameter: { "DayFractionRoundingRule": darfrac }, CurrentEmployee: getToken(CONFIG.USER) }
                    var URL = CONFIG.SERVER + 'HRTR/DeleteDayFractionRoundingRule/';
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        if (response.data) {
                            var URL = CONFIG.SERVER + 'HRTR/GetAllDayFractionRoundingRule/';
                            $http({
                                method: 'POST',
                                url: URL,
                                data: oRequestParameter
                            }).then(function successCallback(response) {
                                // Success
                                $mdDialog.show(
                                $mdDialog.alert()
                                    .clickOutsideToClose(true)
                                    .title($scope.Text['ACCOUNT_SETTING'].DELETE_COMPLETED)
                                    .textContent($scope.Text['ACCOUNT_SETTING'].CAN_NOT_ROLLBACK)
                                    .ariaLabel($scope.Text['ACCOUNT_SETTING'].DELETE_COMPLETED)
                                    .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                    .targetEvent(ev)
                                );
                                $scope.DayFraction = response.data;
                                //$scope.DayFractionGroupMapping = response.data;
                                for (var i = 0; i < $scope.DayFractionGroupMapping.length; i++) {
                                    var index = $scope.DayFraction.findIndexWithAttr('DayFractionID', $scope.DayFractionGroupMapping[i].DayFractionID);
                                    if (index >= 0)
                                        $scope.DayFraction[index].DayFractionRoundingRuleGroups.push($scope.DayFractionGroupMapping[i].RoundingRuleGroupID);
                                }
                                $scope.masterDocuments = angular.copy($scope.DayFraction);
                                console.log('DayFraction.', $scope.DayFraction);

                            }, function errorCallback(response) {
                                // Error
                                console.log('error DayFractionRoundingRuleSettingController.', response);
                            });
                        }
                        else {
                            $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['ACCOUNT_SETTING'].CAN_NOT_DELETE)
                                .textContent($scope.Text['ACCOUNT_SETTING'].DATA_ACTIVE)
                                .ariaLabel($scope.Text['ACCOUNT_SETTING'].CAN_NOT_DELETE)
                                .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                .targetEvent(ev)
                            );
                        }

                    }, function errorCallback(response) {
                        // Error
                        console.log('error DayFractionRoundingRuleSettingController.', response);
                    });
                });
            }

            //GetCountryForLookup
            //$scope.getCountryForLookup = function () {
            //    var URL = CONFIG.SERVER + 'HRTR/GetCountryForLookup';

            //    var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };
            //    $http({
            //        method: 'POST',
            //        url: URL,
            //        data: oRequestParameter
            //    }).then(function successCallback(response) {
            //        $scope.objCountryForLookup = response.data;
            //        console.log('GetCountryForLookup.', $scope.objCountryForLookup);
            //    }, function errorCallback(response) {
            //        console.log('error DayFractionRoundingRuleSettingController GetCountryForLookup.', response);
            //    });
            //};
            //$scope.getCountryForLookup();

        }]);
})();