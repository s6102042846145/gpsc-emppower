﻿(function () {
    angular.module('ESSMobile')
        .controller('ExpenseTypeSettingEditorController', ['$scope', '$http', '$routeParams', '$location', 'CONFIG', '$timeout', '$q', '$log', '$mdDialog', function ($scope, $http, $routeParams, $location, CONFIG, $timeout, $q, $log, $mdDialog) {

            $scope.requestType = $routeParams.id;
            $scope.content.isShowHeader = true;
            $scope.content.isShowBackIcon = true;
            $scope.Textcategory = 'ACCOUNT_SETTING';
            $scope.content.Header = $scope.Text['ACCOUNT_SETTING'].EXPENSE_TYPE_TITLE;
            $scope.Textcategory2 = 'SYSTEM';
            $scope.ExpType = {
                Name: '',
                GLAccount: '',
                IncomeGLAccount: '',
                VATTypes: [],
                searchETTagText: [],
                ExpenseTypeTags: []
            };

            $scope.init = function () {
                getExpenseTypeTag();
            }

            function getExpenseTypeTag() {
                var URL = CONFIG.SERVER + 'HRTR/GetAllExpenseTypeTag/';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.exptag = response.data;
                    getVatType();
                }, function errorCallback(response) {
                    console.log('error GetAllExpenseTypeTag.', response);
                });
            }

            function getVatType() {
                var URL = CONFIG.SERVER + 'HRTR/GetAllVATType/';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.vattype = response.data;
                    if ($scope.itemKey != "null") {
                        getExpenseTypeByID();
                    }
                    else { $scope.ready = true; }
                }, function errorCallback(response) {
                    console.log('error GetAllVATType.', response);
                });
            }

            //Get Order Type
            /*
            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }
            var URL = CONFIG.SERVER + 'HRTR/GetAllOrderType/';
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                $scope.ordertype = response.data;
                console.log('ordertype.', $scope.ordertype);

            }, function errorCallback(response) {
                // Error
                console.log('error ExpenseTypeSettingEditorController.', response);
            });
            */

            function getExpenseTypeByID() {
                var URL = CONFIG.SERVER + 'HRTR/GetExpenseTypeByID/';
                var oRequestParameter = { InputParameter: { "ExpenseTypeID": $scope.itemKey }, CurrentEmployee: getToken(CONFIG.USER) };
                $scope.VATCodeDisable = true;

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.ExpType.ExpenseTypeID = response.data.ExpenseTypeID;
                    $scope.ExpType.Name = response.data.Name;
                    $scope.ExpType.GLAccount = response.data.GLAccount;
                    $scope.ExpType.IncomeGLAccount = response.data.IncomeGLAccount;
                    getTagMapping();

                }, function errorCallback(response) {
                    console.log('error GetExpenseTypeByID.', response);
                });
            }

            function getTagMapping() {
                var oRequestParameter = { InputParameter: { "ExpenseTypeID": $scope.itemKey }, CurrentEmployee: $scope.employeeData, Requestor: getToken(CONFIG.USER) };
                var URL = CONFIG.SERVER + 'HRTR/GetExpenseTypeTagMapping/';

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    for (var i = 0; i < response.data.length; i++) {
                        var temp = $scope.exptag[$scope.exptag.findIndexWithAttr('TagCode', response.data[i].TagCode)];
                        $scope.ExpType.ExpenseTypeTags[i] = temp.TagCode;
                        $scope.ExpType.searchETTagText[i] = temp.Description;
                    }
                    getVATTypeMapping();
                }, function errorCallback(response) {
                    console.log('error GetExpenseTypeTagMapping.', response);
                });
            }

            function getVATTypeMapping() {
                var oRequestParameter = { InputParameter: { "ExpenseTypeID": $scope.itemKey }, CurrentEmployee: $scope.employeeData, Requestor: getToken(CONFIG.USER) };
                var URL = CONFIG.SERVER + 'HRTR/GetExpenseTypeVATType/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    for (var i = 0; i < response.data.length; i++) {
                        $scope.ExpType.VATTypes.push(response.data[i].VATCode);
                    }
                    $scope.ready = true;
                }, function errorCallback(response) {
                    console.log('error GetExpenseTypeVATType.', response);
                });
            }

            $scope.addRow = function (ev) {
                if ($scope.ExpType.ExpenseTypeTags.hasNull() && $scope.ExpType.ExpenseTypeTags.length > 0)
                    $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['ACCOUNT_SETTING'].CAN_NOT_ADD_ROW)
                                .textContent($scope.Text['ACCOUNT_SETTING'].PREVIOUS_ROW_IS_NULL)
                                .ariaLabel($scope.Text['ACCOUNT_SETTING'].THANK)
                                .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                .targetEvent(ev));
                else {
                    $scope.ExpType.ExpenseTypeTags.push('');
                    $scope.ExpType.searchETTagText.push('');
                }
            };

            $scope.removeRow = function (index) {
                $scope.ExpType.searchETTagText.splice(index, 1);
                $scope.ExpType.ExpenseTypeTags.splice(index, 1);
            };

            $scope.SaveData = function (ev) {

                var URL = "";
                var valid = true;
                if (!$scope.ExpType.Name) valid = false;
                else if (!$scope.ExpType.GLAccount && !$scope.ExpType.IncomeGLAccount) valid = false;
                else if ($scope.ExpType.VATTypes.length <= 0) valid = false;
                else if ($scope.ExpType.ExpenseTypeTags.length <= 0) valid = false;
                else {
                    if ($scope.ExpType.ExpenseTypeTags.length) {
                        for (var i = 0; i < $scope.ExpType.ExpenseTypeTags.length; i++) {
                            if (!$scope.ExpType.ExpenseTypeTags[i]) {
                                valid = false;
                                break;
                            }
                        }
                    }
                    else {
                        valid = false;
                    }
                }
                if (!valid) {
                    $scope.HasError = true;
                    $scope.ErrorText = $scope.Text['ACCOUNT_SETTING'].PLEASE_INSERT_ALL_FIELDS;

                }
                else {
                    var oRequestParameter = { InputParameter: { "EXPTYPE": $scope.ExpType }, CurrentEmployee: $scope.employeeData, Requestor: getToken(CONFIG.USER) }

                    URL = CONFIG.SERVER + 'HRTR/SaveExpenseType/';
                    console.log(URL);
                    $scope.HasError = false;
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        var output = response.data;
                        if (output > 0) {
                            $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                .textContent($scope.Text['ACCOUNT_SETTING'].GO_BACK)
                                .ariaLabel($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                .targetEvent(ev));
                            $location.path('/frmViewContent/302');
                        }
                        else {
                            //duplicate
                            $scope.HasError = true;
                            if ($scope.oExpTypeGroup) {
                                $scope.ErrorText = $scope.Text['ACCOUNT_SETTING'].DATA_ACTIVE;
                            }
                            else {
                                $scope.ErrorText = "Expense Type Name is duplicated";
                            }
                        }

                    }, function errorCallback(response) {
                        // Error
                        console.log('error ExpensTypeSettingEditorController Save.', response);
                    });
                }
            }

            //auto complete Expense Type Tag
            $scope.simulateQuery = true;
            $scope.querySearchETTag = function (query, index) {
                $scope.ExpType.ExpenseTypeTags[index] = '';
                var results = query ? $scope.exptag.filter(createFilterForETTag(query)).filter(createFilterDuplicateForETTag($scope.ExpType.ExpenseTypeTags)) : $scope.exptag.filter(createFilterDuplicateForETTag($scope.ExpType.ExpenseTypeTags)), deferred;
                if ($scope.simulateQuery) {
                    deferred = $q.defer();
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 1, false);
                    return deferred.promise;
                } else {
                    return results;
                }
            };

            $scope.selectedItemETTagChange = function (item, index) {
                if (angular.isDefined(item) && item != null) {
                    $scope.ExpType.searchETTagText[index] = item.Description;
                    $scope.ExpType.ExpenseTypeTags[index] = item.TagCode;
                }
                else {
                    $scope.ExpType.searchETTagText[index] = '';
                    $scope.ExpType.ExpenseTypeTags[index] = '';
                }
            };

            function createFilterForETTag(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(ettag) {
                    var source = angular.lowercase(ettag.Description);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }

            function createFilterDuplicateForETTag(arr) {
                return function filterFn(ettag) {
                    return !(arr.duplicate(ettag.TagCode));
                };
            }

            $scope.checkText_ETTag = function (text, index) {
                var result = null;
                if (text) {
                    for (var i = 0; i < $scope.exptag.length; i++) {
                        if ($scope.exptag[i].Description == text || $scope.exptag[i].TagCode == text) {
                            result = $scope.exptag[i];
                            break;
                        }
                    }
                }
                if (result) {
                    $scope.selectedItemETTagChange(result, index);
                }
                else {
                    $scope.selectedItemETTagChange(null, index);
                }
            }
        }]);
})();