﻿(function () {
    angular.module('ESSMobile')
        .controller('MileageRateSettingEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$timeout', '$q', '$log', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $timeout, $q, $log, $mdDialog) {

            console.log('MileageRateSettingEditorController');

            var employeeData = getToken(CONFIG.USER);
            $scope.content.isShowHeader = true;
            $scope.content.isShowBackIcon = true;
            $scope.requestType = $routeParams.id;
            $scope.data = {};
            $scope.Language = employeeData.Language;

            $scope.MileageRate = {
                BeginDate: $filter('date')(new Date(), 'yyyy-MM-ddT00:00:00'),
                EndDate: $filter('date')(new Date(), 'yyyy-MM-ddT00:00:00'),
                FuelRate: '',
                NoCalIncomeRate: '',
                FuelRateCurrency: '',
                FuelRateCurrencyObject: null,
                NoCalIncomRateCurrency: '',
                NoCalIncomRateCurrencyObject: null,
                TransportationTypeCodes: [],
                searchTTCText: []
            };

            $scope.addRow = function (ev) {
                if ($scope.MileageRate.searchTTCText.hasNull() && $scope.MileageRate.searchTTCText.length > 0)
                    $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['ACCOUNT_SETTING'].CAN_NOT_ADD_ROW)
                                .textContent($scope.Text['ACCOUNT_SETTING'].PREVIOUS_ROW_IS_NULL)
                                .ariaLabel($scope.Text['ACCOUNT_SETTING'].THANK)
                                .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                .targetEvent(ev));
                else {
                    $scope.MileageRate.searchTTCText.push('');
                    $scope.MileageRate.TransportationTypeCodes.push('');
                }
            };

            $scope.removeRow = function (index) {
                $scope.MileageRate.searchTTCText.splice(index, 1);
                $scope.MileageRate.TransportationTypeCodes.splice(index, 1);
            };

            $scope.HasError = false;
            $scope.ErrorText = "";


            $scope.Textcategory = 'ACCOUNT_SETTING';
            $scope.content.Header = $scope.Text[$scope.Textcategory].MILEAGERATE_SETTING;
            $scope.Textcategory2 = 'SYSTEM';

            $scope.init = function () {
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }
                var URL = CONFIG.SERVER + 'HRTR/GetAllCurrency/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.currencyList = response.data;
                    getTransportationType();
                }, function errorCallback(response) {
                    console.log('error GetAllCurrency.', response);
                });
            }

            function getTransportationType() {
                oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) }
                URL = CONFIG.SERVER + 'HRTR/GetAllTransportationType/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.transportationtype = response.data;
                    if ($scope.itemKey != "null") {
                        getMileageRateByID();
                        
                    }
                    else {
                        $scope.ready = true;
                    }

                }, function errorCallback(response) {
                    console.log('error GetAllTransportationType.', response);
                });
            }

            function getMileageRateByID() {
                var oRequestParameter = { InputParameter: { "MileageRateID": $scope.itemKey }, CurrentEmployee: getToken(CONFIG.USER) }
                var URL = CONFIG.SERVER + 'HRTR/GetMileageRateByID/';
                $scope.VATCodeDisable = true;

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.MileageRate = response.data;
                    $scope.checkText_FuelRateCurrency($scope.MileageRate.FuelRateCurrency);
                    $scope.checkText_NoCalIncomRateCurrency($scope.MileageRate.NoCalIncomRateCurrency);
                    getMileageRateTransportationTypeMapping();
                    
                }, function errorCallback(response) {
                    console.log('error GetMileageRateByID.', response);
                });
            }

            function getMileageRateTransportationTypeMapping() {
                oRequestParameter = { InputParameter: { "MileageRateID": $scope.itemKey }, CurrentEmployee: getToken(CONFIG.USER) }
                URL = CONFIG.SERVER + 'HRTR/GetMileageRateTransportationTypeMapping/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.MileageRate.searchTTCText = [];
                    for (var i = 0; i < response.data.length; i++) {
                        var temp = $scope.transportationtype[$scope.transportationtype.findIndexWithAttr('TransportationTypeCode', response.data[i].TransportationTypeCode)];
                        $scope.MileageRate.TransportationTypeCodes.push(response.data[i].TransportationTypeCode);
                        $scope.MileageRate.searchTTCText.push(temp.NameTH);
                    }
                    $scope.ready = true;
                }, function errorCallback(response) {
                    console.log('error GetMileageRateTransportationTypeMapping.', response);
                });
            }

            $scope.setSelectedBeginDate = function (selectedDate) {
                $scope.MileageRate.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                if ($scope.MileageRate.BeginDate > $scope.MileageRate.EndDate) {
                    $scope.MileageRate.EndDate = $scope.MileageRate.BeginDate;
                }
                $scope.MileageRate.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
            };

            $scope.setSelectedEndDate = function (selectedDate) {
                $scope.MileageRate.EndDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
            };

            $scope.addEndDate = function () {
                if ($scope.MileageRate.NoSpecific == true) {
                    $scope.MileageRate.EndDate = "9999-12-31T00:00:00";
                }
                else if ($scope.MileageRate.EndDate == "9999-12-31T00:00:00") {
                    $scope.MileageRate.EndDate = $scope.MileageRate.BeginDate;
                }
            };

            //onsave
            $scope.SaveData = function (ev) {
                var valid = true;
                if (!$scope.MileageRate.FuelRate) valid = false;
                else if ($scope.MileageRate.NoCalIncomeRate < 0) valid = false;
                else if (!$scope.MileageRate.FuelRateCurrency) valid = false;
                else if (!$scope.MileageRate.NoCalIncomRateCurrency) valid = false;
                else if (!$scope.MileageRate.TransportationTypeCodes.length) valid = false;

                if (!valid) {
                    $scope.HasError = true;
                    $scope.ErrorText = $scope.Text['ACCOUNT_SETTING'].PLEASE_INSERT_ALL_FIELDS;
                }
                else {
                    var oRequestParameter = { InputParameter: { "MILEAGERATE": $scope.MileageRate }, CurrentEmployee: $scope.employeeData, Requestor: getToken(CONFIG.USER) };
                    var URL = CONFIG.SERVER + 'HRTR/SaveMileageRate/';
                    $scope.HasError = false;
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        var output = response.data;
                        if (output > 0) {
                            $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                .textContent($scope.Text['ACCOUNT_SETTING'].GO_BACK)
                                .ariaLabel($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                .targetEvent(ev));
                            $location.path('/frmViewContent/321');
                        }
                        else {
                            $scope.HasError = true;
                            $scope.ErrorText = "VAT Code is duplicated";
                        }
                    }, function errorCallback(response) {
                        // Error
                        console.log('error MileageRateSettingEditorController Save.', response);
                    });
                }
            }

            //auto complete Currency 
            $scope.simulateQuery = false;
            $scope.querySearchCurrency = function (query) {
                $scope.MileageRate.FuelRateCurrency = '';
                var results = query ? $scope.currencyList.filter(createFilterForCurrency(query)) : $scope.currencyList, deferred;
                deferred = $q.defer();
                if ($scope.simulateQuery) {
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 300, false);
                    return deferred.promise;
                } else {
                    deferred.resolve(results);
                    return deferred.promise;
                }
            }; 

            $scope.querySearchNoCalIncomRateCurrency = function (query) {
                $scope.MileageRate.NoCalIncomRateCurrency = '';
                var results = query ? $scope.currencyList.filter(createFilterForCurrency(query)) : $scope.currencyList, deferred;
                deferred = $q.defer();
                if ($scope.simulateQuery) {
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 300, false);
                    return deferred.promise;
                } else {
                    deferred.resolve(results);
                    return deferred.promise;
                }
            };

            $scope.selectedItemFuelRateCurrencyChange = function (item) {
                if (angular.isDefined(item)) {
                    $scope.MileageRate.searchFuelRateCurrencyText = item.Code;
                    $scope.MileageRate.FuelRateCurrency = item.Code;
                }
                else {
                    $scope.MileageRate.searchFuelRateCurrencyText = '';
                    $scope.MileageRate.FuelRateCurrency = '';
                }
            };

            $scope.selectedItemNoCalIncomRateCurrencyChange = function (item) {
                if (angular.isDefined(item)) {
                    $scope.MileageRate.searchNoCalIncomRateCurrencyText = item.Code;
                    $scope.MileageRate.NoCalIncomRateCurrency = item.Code;
                }
                else {
                    $scope.MileageRate.searchNoCalIncomRateCurrencyText = '';
                    $scope.MileageRate.NoCalIncomRateCurrency = '';
                }
            };

            function createFilterForCurrency(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(curruncy) {
                    var source = angular.lowercase(curruncy.Code);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }

            $scope.checkText_FuelRateCurrency = function (text) {
                var result = null;
                if (text) {
                    for (var i = 0; i < $scope.currencyList.length; i++) {
                        if ($scope.currencyList[i].Code == text || $scope.currencyList[i].Code == text) {
                            result = $scope.currencyList[i];
                            break;
                        }
                    }
                }
                if (result) {
                    $scope.selectedItemFuelRateCurrencyChange(result);
                }
                else {
                    $scope.selectedItemFuelRateCurrencyChange(null);
                }
            }

            $scope.checkText_NoCalIncomRateCurrency = function (text) {
                var result = null;
                if (text) {
                    for (var i = 0; i < $scope.currencyList.length; i++) {
                        if ($scope.currencyList[i].Code == text || $scope.currencyList[i].Code == text) {
                            result = $scope.currencyList[i];
                            break;
                        }
                    }
                }
                if (result) {
                    $scope.selectedItemNoCalIncomRateCurrencyChange(result);
                }
                else {
                    $scope.selectedItemNoCalIncomRateCurrencyChange(null);
                }
            }

            //auto complete TransportationType
            $scope.querySearchTTC = function (query, index) {
                $scope.MileageRate.TransportationTypeCodes[index] = '';
                var results = query ? $scope.transportationtype.filter(createFilterForTTC(query)).filter(createFilterDuplicateForTTC($scope.MileageRate.TransportationTypeCodes)) : $scope.transportationtype.filter(createFilterDuplicateForTTC($scope.MileageRate.TransportationTypeCodes)), deferred;
                deferred = $q.defer();
                if ($scope.simulateQuery) {
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 300, false);
                    return deferred.promise;
                } else {
                    deferred.resolve(results);
                    return deferred.promise;
                }
            };

            $scope.selectedItemTTCChange = function (item, index) {
                if (angular.isDefined(item)) {
                    $scope.MileageRate.searchTTCText[index] = item.NameTH;
                    $scope.MileageRate.TransportationTypeCodes[index] = item.TransportationTypeCode;
                }
                else {
                    $scope.MileageRate.searchTTCText[index] = '';
                    $scope.MileageRate.TransportationTypeCodes[index] = '';
                }
            };

            //$scope.selectedItemTTCENChange = function (item, index) {
            //    if (angular.isDefined(item)) {
            //        $scope.MileageRate.TransportationTypeCodeObjects[index] = item;
            //        $scope.MileageRate.TransportationTypeCodes[index] = item.TransportationTypeCode;
            //    }
            //    else {
            //        $scope.MileageRate.TransportationTypeCodeObjects[index] = null;
            //        $scope.MileageRate.TransportationTypeCodes[index] = '';
            //    }
            //};

            function createFilterForTTC(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    var TransportationTypeCode = angular.lowercase(x.TransportationTypeCode);
                    return (TransportationTypeCode.indexOf(lowercaseQuery) >= 0);
                };
            }

            function createFilterDuplicateForTTC(arr) {
                return function filterFn(ttc) {
                    return !(arr.duplicate(ttc.TransportationTypeCode));
                };
            }

            $scope.checkText_TTC = function (text, index) {
                var result = null;
                if (text) {
                    for (var i = 0; i < $scope.transportationtype.length; i++) {
                        if ($scope.transportationtype[i].TransportationTypeCode == text || $scope.transportationtype[i].NameTH == text) {
                            result = $scope.transportationtype[i];
                            break;
                        }
                    }
                }
                if (result) {
                    $scope.selectedItemTTCChange(result, index);
                }
                else {
                    $scope.selectedItemTTCChange(null, index);
                }
            }
        }]);
})();

