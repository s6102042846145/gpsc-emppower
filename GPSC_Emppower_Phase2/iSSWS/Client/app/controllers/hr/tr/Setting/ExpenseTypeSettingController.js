﻿(function () {
    angular.module('ESSMobile')
        .controller('ExpenseTypeSettingController', ['$scope', '$http', '$routeParams', '$location', '$window', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $window, CONFIG, $mdDialog) {

            console.log('ExpenseTypeSettingController');
            $scope.requestType = $routeParams.id;
            $scope.formData = { searchKeyword: '' };
            $scope.firstLoad = true;

            $scope.Textcategory = 'ACCOUNT_SETTING';
            $scope.Textcategory2 = 'SYSTEM';
            $scope.content.isShowHeader = false;


            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }
            URL = CONFIG.SERVER + 'HRTR/GetExpenseTypeTagForLookup/';
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                $scope.GetExpenseTypeTag = response.data;
                //$scope.exptypegrouptagMaster = response.data;
                console.log('GetExpenseTypeTag.', $scope.GetExpenseTypeTag);

            }, function errorCallback(response) {
                // Error
                console.log('error ExpensTypeGroupSettingEditorController.', response);
            });
            

            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }

            var URL = CONFIG.SERVER + 'HRTR/GetAllExpenseType/';
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                $scope.exptype = response.data;
                
                $scope.masterDocuments = angular.copy($scope.exptype);
                
                oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }
                URL = CONFIG.SERVER + 'HRTR/GetAllExpenseTypeVATType/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.ExpenseTypeVATType = response.data;
                    for (var i = 0; i < response.data.length; i++) {
                        var index = $scope.exptype.findIndexWithAttr('ExpenseTypeID', response.data[i].ExpenseTypeID);
                        if (index >= 0)
                            $scope.exptype[index].VATTypes.push(response.data[i].VATCode);
                    }
                    console.log('GetAllExpenseTypeVATType.', $scope.exptype);

                }, function errorCallback(response) {
                    // Error
                    console.log('error ExpensTypeGroupSettingEditorController.', response);
                });

                oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }
                URL = CONFIG.SERVER + 'HRTR/GetAllExpenseTypeTagMapping/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.ExpenseTypeTag = response.data;
                    for (var i = 0; i < response.data.length; i++) {
                        var index = $scope.exptype.findIndexWithAttr('ExpenseTypeID', response.data[i].ExpenseTypeID);
                        if (index >= 0)
                            $scope.exptype[index].ExpenseTypeTags.push(response.data[i].TagCode);
                    }
                    console.log('ExpenseTypeTag.', $scope.ExpenseTypeTag);

                }, function errorCallback(response) {
                    // Error
                    console.log('error ExpensTypeGroupSettingEditorController.', response);
                });
            }, function errorCallback(response) {
                // Error
                console.log('error ExpenseTypeSettingController.', response);
            });


            $scope.CreateNewVAT = function () {
                $location.path('/frmViewContent/311');
            };

            $scope.EditVAT = function (vatcode) {
                $location.path('/frmViewContent/311/' + vatcode);
            };

            var search = function (item, keyword) {
                // Search Criteria
                if (!keyword
                    || (item.Name.toLowerCase().indexOf(keyword) != -1)
                    || (item.GLAccount.toLowerCase().indexOf(keyword) != -1)
                    || (item.IncomeGLAccount.toLowerCase().indexOf(keyword) != -1)
                    //|| (item.VATCode.toLowerCase().indexOf(keyword) != -1)    //.propIndexOf('Description', keyword)
                    || (item.VATTypes.likeIndexOf(keyword) != -1)
                    || (item.ExpenseTypeTags.likeWithMappingIndexOf($scope.GetExpenseTypeTag, keyword) != -1)
                    || (item.OrderType.toLowerCase().indexOf(keyword) != -1)
                    ) {
                    return true;
                }
                return false;
            };

            $scope.searchInBox = function () {
                if ($scope.firstLoad) {
                    $scope.masterDocuments = angular.copy($scope.exptype);
                    $scope.firstLoad = false;
                }
                console.log('searchInBox.', $scope.formData.searchKeyword);
                if (!$scope.formData.searchKeyword) {
                    console.log('just reset documents.');
                    $scope.exptype = angular.copy($scope.masterDocuments);
                }
                else {
                    var count = $scope.masterDocuments.length;
                    var docs = [];
                    for (var i = 0; i < count; i++) {
                        if (search($scope.masterDocuments[i], $scope.formData.searchKeyword.toLowerCase())) {
                            docs.push($scope.masterDocuments[i]);
                        }
                    }
                    $scope.exptype = docs;
                }
            };

            $scope.DeleteConfirm = function (expID,ev) {
                var confirm = $mdDialog.confirm()
                    .title($scope.Text['ACCOUNT_SETTING'].WANT_TO_DELETE)
                    .textContent($scope.Text['ACCOUNT_SETTING'].CAN_NOT_ROLLBACK_PLEASE_CONFIRM)
                    .ariaLabel($scope.Text['ACCOUNT_SETTING'].WANT_TO_DELETE)
                    .targetEvent(ev)
                    .ok($scope.Text['SYSTEM'].BUTTON_YES)
                    .cancel($scope.Text['SYSTEM'].BUTTON_NO);
                $mdDialog.show(confirm).then(function () {
                    //delete
                    var oRequestParameter = { InputParameter: { "EXPTYPEID": expID }, CurrentEmployee: getToken(CONFIG.USER) }
                    var URL = CONFIG.SERVER + 'HRTR/DeleteExpenseType/';
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        if (response.data) {
                            // Success
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .clickOutsideToClose(true)
                                    .title($scope.Text['ACCOUNT_SETTING'].DELETE_COMPLETED)
                                    .textContent($scope.Text['ACCOUNT_SETTING'].CAN_NOT_ROLLBACK)
                                    .ariaLabel($scope.Text['ACCOUNT_SETTING'].DELETE_COMPLETED)
                                    .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                    .targetEvent(ev)
                            );
                        }
                        else {
                            // Fail
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .clickOutsideToClose(true)
                                    .title($scope.Text['ACCOUNT_SETTING'].CAN_NOT_DELETE)
                                    .textContent($scope.Text['ACCOUNT_SETTING'].DATA_ACTIVE)
                                    .ariaLabel($scope.Text['ACCOUNT_SETTING'].CAN_NOT_DELETE)
                                    .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                    .targetEvent(ev)
                            );
                        }
                        var URL = CONFIG.SERVER + 'HRTR/GetAllExpenseType/';
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            $scope.exptype = response.data;
                            for (var i = 0; i < $scope.ExpenseTypeVATType.length; i++) {
                                var index = $scope.exptype.findIndexWithAttr('ExpenseTypeID', $scope.ExpenseTypeVATType[i].ExpenseTypeID);
                                if (index >= 0)
                                    $scope.exptype[index].VATTypes.push($scope.ExpenseTypeVATType[i].VATCode);
                            }
                            for (var i = 0; i < $scope.ExpenseTypeTag.length; i++) {
                                var index = $scope.exptype.findIndexWithAttr('ExpenseTypeID', $scope.ExpenseTypeTag[i].ExpenseTypeID);
                                if (index >= 0)
                                    $scope.exptype[index].ExpenseTypeTags.push($scope.ExpenseTypeTag[i].TagCode);
                            }
                            $scope.masterDocuments = angular.copy($scope.exptype);
                            console.log('document.', $scope.documents);

                        }, function errorCallback(response) {
                            // Error
                            console.log('error ExpenseTypeSettingController.', response);
                        });


                    }, function errorCallback(response) {
                        // Error
                        console.log('error ExpenseTypeSettingController.', response);
                    });
                });

            }



        }]);
})();