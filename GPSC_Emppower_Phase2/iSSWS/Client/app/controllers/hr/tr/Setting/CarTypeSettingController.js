﻿(function () {
    angular.module('ESSMobile')
        .controller('CarTypeSettingController', ['$scope', '$http', '$routeParams', '$location', '$window', 'CONFIG', function ($scope, $http, $routeParams, $location, $window, CONFIG) {

            console.log('CarTypeSettingController');
            $scope.requestType = $routeParams.id;
            $scope.formData = { searchKeyword: '' };
            $scope.firstLoad = true;

            $scope.Textcategory = 'ACCOUNT_SETTING';
            $scope.content.isShowHeader = false;

            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }

            var URL = CONFIG.SERVER + 'HRTR/GetAllCarType/';
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                $scope.documents = response.data;
                $scope.masterDocuments = response.data;
                console.log('CarTypeList.', $scope.documents);

            }, function errorCallback(response) {
                // Error
                console.log('error CarRegistrationListController.', response);
            });            

            
            $scope.CreateNew = function () {
                $location.path('/frmViewContent/318');
            };

            $scope.Edit = function (itemID) {
                $location.path('/frmViewContent/318/' + itemID);
            };

            var search = function (item, keyword) {
                // Search Criteria
                if (!keyword
                    || (item.Description.toLowerCase().indexOf(keyword) != -1)
                    ) {
                    return true;
                }
                return false;
            };


            $scope.searchInBox = function () {
                $scope.firstLoad = false;
                console.log('searchInBox.', $scope.formData.searchKeyword);
                if (!$scope.formData.searchKeyword) {
                    console.log('just reset documents.');
                    $scope.documents = $scope.masterDocuments;
                }
                else {
                    var count = $scope.masterDocuments.length;
                    var docs = [];
                    for (var i = 0; i < count; i++) {
                        if (search($scope.masterDocuments[i], $scope.formData.searchKeyword.toLowerCase())) {
                            docs.push($scope.masterDocuments[i]);
                        }
                    }
                    $scope.documents = docs;
                }
            };
            /*
            $scope.DeleteConfirm = function (itemID) {
                if ($window.confirm('Please confirm to delete')) {
                    //delete
                    //var URL = CONFIG.SERVER + 'HRTR/DeleteVATType/';
                    var oRequestParameter = { InputParameter: { "CarRegisID": itemID }, CurrentEmployee: getToken(CONFIG.USER) }
                    var URL = CONFIG.SERVER + 'HRTR/DeleteCarRegistration/';
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {

                        // Success
                        oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }
                        var URL = CONFIG.SERVER + 'HRTR/GetAllCarType/';
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            // Success
                            $scope.documents = response.data;
                            $scope.masterDocuments = response.data;
                            console.log('CarTypeList.', $scope.documents);

                        }, function errorCallback(response) {
                            // Error
                            console.log('error CarRegistrationListController.', response);
                        });


                    }, function errorCallback(response) {
                        // Error
                        console.log('error CarTypeSettingController.', response);
                    });

                    //get again


                }
                */

            $scope.DeleteConfirm = function (itemID, ev) {
                var confirm = $mdDialog.confirm()
                    .title($scope.Text['ACCOUNT_SETTING'].WANT_TO_DELETE)
                    .textContent($scope.Text['ACCOUNT_SETTING'].CAN_NOT_ROLLBACK_PLEASE_CONFIRM)
                    .ariaLabel($scope.Text['ACCOUNT_SETTING'].WANT_TO_DELETE)
                    .targetEvent(ev)
                    .ok($scope.Text['SYSTEM'].BUTTON_YES)
                    .cancel($scope.Text['SYSTEM'].BUTTON_NO);
                $mdDialog.show(confirm).then(function () {
                    //delete
                    var oRequestParameter = { InputParameter: { "VATCODE": itemID }, CurrentEmployee: getToken(CONFIG.USER) }
                    var URL = CONFIG.SERVER + 'HRTR/DeleteTravelType/';
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        if (response.data) {
                            // Success
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .clickOutsideToClose(true)
                                    .title($scope.Text['ACCOUNT_SETTING'].DELETE_COMPLETED)
                                    .textContent($scope.Text['ACCOUNT_SETTING'].CAN_NOT_ROLLBACK)
                                    .ariaLabel($scope.Text['ACCOUNT_SETTING'].DELETE_COMPLETED)
                                    .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                    .targetEvent(ev)
                            );
                        }
                        else {
                            // Fail
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .clickOutsideToClose(true)
                                    .title($scope.Text['ACCOUNT_SETTING'].CAN_NOT_DELETE)
                                    .textContent($scope.Text['ACCOUNT_SETTING'].DATA_ACTIVE)
                                    .ariaLabel($scope.Text['ACCOUNT_SETTING'].CAN_NOT_DELETE)
                                    .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                    .targetEvent(ev)
                            );
                        }
                        var URL = CONFIG.SERVER + 'HRTR/GetAllVATType/';
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            // Success
                            $scope.documents = response.data;
                            $scope.masterDocuments = response.data;
                            console.log('document.', $scope.documents);

                        }, function errorCallback(response) {
                            // Error
                            console.log('error VATTypeSettingController.', response);
                        });

                    }, function errorCallback(response) {
                        // Error
                        console.log('error VATTypeSettingController.', response);
                    });
                }, function () {

                });
            }

        }]);
})();