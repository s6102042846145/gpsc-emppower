﻿(function () {
    angular.module('ESSMobile')
        .controller('VATTypeSettingEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', '$mdDialog', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, $mdDialog, CONFIG) {

            console.log('VATTypeSettingEditorController');

            var employeeData = getToken(CONFIG.USER);
            console.log(employeeData);
            $scope.content.isShowHeader = true;
            $scope.content.isShowBackIcon = true;
            $scope.data = {};
            $scope.data.NoSpecific = true;
            $scope.HasError = false;
            $scope.ErrorText = "";
            $scope.oVATType = {
                VATCode: '',
                Description: '',
                Percent: '',
                GLAccount: '',
                BeginDate: $filter('date')(new Date(), 'yyyy-MM-ddT00:00:00'),
                EndDate: '9999-12-31T00:00:00',
                IsDefault: false
            };

            $scope.Textcategory = 'ACCOUNT_SETTING';
            $scope.content.Header = $scope.Text[$scope.Textcategory].VATTYPE;
            $scope.Textcategory2 = 'SYSTEM';


            //onload Check Create/Edit
            if ($scope.itemKey != "null") {
                var oRequestParameter = { InputParameter: { "VATCODE": $scope.itemKey }, CurrentEmployee: getToken(CONFIG.USER) }
                var URL = CONFIG.SERVER + 'HRTR/GetVATType/';
                $scope.VATCodeDisable = true;

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.oVATType = response.data;
                    if ($scope.oVATType.EndDate == '9999-12-31T00:00:00') $scope.data.NoSpecific = true;
                    else $scope.data.NoSpecific = false;

                }, function errorCallback(response) {
                    // Error
                    console.log('error GetVATType.', response);
                });
            }
            else {
                $scope.VATCodeDisable = false;
            }

            $scope.addEndDate = function () {
                if ($scope.data.NoSpecific == true) {
                    $scope.oVATType.EndDate = "9999-12-31T00:00:00";
                }
                else {
                    $scope.oVATType.EndDate = $scope.oVATType.BeginDate;
                }
            };

            $scope.setSelectedBeginDate = function (selectedDate) {
                $scope.oVATType.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
            };

            $scope.setSelectedEndDate = function (selectedDate) {
                $scope.oVATType.EndDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
            };

            $scope.onVATChange = function () {
                $scope.oVATType.ExpensePart = 0;
                $scope.oVATType.VATPart = $scope.oVATType.Percent;
            }

            $scope.onPartChange = function () {
                //if ($scope.oVATType.Percent < ($scope.oVATType.ExpensePart + $scope.oVATType.VATPart))
                    $scope.oVATType.Percent = $scope.oVATType.ExpensePart + $scope.oVATType.VATPart;
            }

            //onsave
            $scope.SaveData = function (ev) {
                var valid = true;
                if (!$scope.oVATType.VATCode) valid = false;
                else if (!$scope.oVATType.Description) valid = false;
                else if (!$scope.oVATType.VATCode) valid = false;
                else if ($scope.oVATType.Percent < 0) valid = false;
                else if ($scope.oVATType.ExpensePart < 0) valid = false;
                else if ($scope.oVATType.VATPart < 0) valid = false;
                else if (!$scope.oVATType.GLAccount) valid = false;

                if (!valid) {
                    $scope.HasError = true;
                    $scope.ErrorText = $scope.Text['ACCOUNT_SETTING'].PLEASE_INSERT_ALL_FIELDS;
                }
                    //check duplicate
                else {
                    var oAction = "update";
                    if ($scope.itemKey == "null") oAction = "create";
                    var oRequestParameter = { InputParameter: { "VATTYPE": $scope.oVATType, "ACTION": oAction }, CurrentEmployee: $scope.employeeData, Requestor: getToken(CONFIG.USER) }

                    URL = CONFIG.SERVER + 'HRTR/SaveVATType/';
                    $scope.HasError = false;
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        var output = response.data;
                        if (output == 1) {
                            $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                .textContent($scope.Text['ACCOUNT_SETTING'].GO_BACK)
                                .ariaLabel($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                .targetEvent(ev));
                            $location.path('/frmViewContent/303');
                        }
                        else {
                            $scope.HasError = true;
                            if ($scope.itemKey == "null") $scope.ErrorText = "VAT Code is duplicated";
                            else if (output == 0) $scope.ErrorText = "ไม่สามารถบันทึกได้ เนื่องจากมีรายการใช้งาน VAT Type นี้";
                        }

                    }, function errorCallback(response) {
                        // Error
                        console.log('error VATTypeSettingEditorController Save.', response);
                    });
                }
            }
        }]);
})();

