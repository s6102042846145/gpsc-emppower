﻿(function () {
    angular.module('ESSMobile')
        .controller('ProjectSettingController', ['$scope', '$http', '$routeParams', '$location', '$window', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $window, CONFIG, $mdDialog) {

            console.log('ProjectSettingController');
            $scope.requestType = $routeParams.id;
            $scope.formData = { searchKeyword: '' };
            $scope.firstLoad = true;

            $scope.Textcategory = 'ACCOUNT_SETTING';
            $scope.Textcategory2 = 'SYSTEM';
            $scope.content.isShowHeader = false;
            $scope.ProjectAssignmentList = [];
            $scope.ProjectAssignmentList.length = 0;
            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }

            var URL = CONFIG.SERVER + 'HRTR/GetAllProject/';
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                $scope.documents = response.data;
                //$scope.masterDocuments = response.data;
                //for (var i = 0; i < $scope.masterDocuments.length; i++) {
                URL = CONFIG.SERVER + 'HRTR/GetAllProjectAssignment/';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.ProjectAssignmentList = response.data;
                    for (var i = 0; i < response.data.length; i++) {
                        var index = $scope.documents.findIndexWithAttr('ProjectCode', response.data[i].ProjectCode);
                        if (index >= 0)
                            $scope.documents[index].ProjectAssignments.push(response.data[i].EmployeeID);
                    }
                    $scope.masterDocuments = angular.copy($scope.documents);
                    /*if (response.data.length) {
                        var index = $scope.documents.findIndexWithAttr('ProjectCode', response.data[0].ProjectCode);
                        $scope.documents[index].ProjectAssignments = response.data;
                    }
                    $scope.masterDocuments = angular.copy($scope.documents);*/
                }, function errorCallback(response) {
                    // Error
                    console.log('error ProjectSettingEditorController.', response);
                });
                //}
                //$scope.masterDocuments = response.data;
                console.log('document.', $scope.documents);

            }, function errorCallback(response) {
                // Error
                console.log('error ProjectSettingController.', response);
            });
            /*
            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }

            var URL = CONFIG.SERVER + 'HRTR/GetAllProjectAssignment/';
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                $scope.projectAssignment = response.data;
                
                console.log('projectAssignment.', $scope.projectAssignment);

            }, function errorCallback(response) {
                // Error
                console.log('error MileageRateSettingController.', response);
            });
            */
            $scope.date = new Date();
            var URL = CONFIG.SERVER + 'HRTR/GetINFOTYPE0001ForLookup/';
            var oRequestParameter = { InputParameter: { "CreateDate": $scope.date }, CurrentEmployee: getToken(CONFIG.USER) }

            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                $scope.GetEmployee = response.data;

                console.log('GetEmployee.', $scope.GetEmployee);

            }, function errorCallback(response) {
                // Error
                console.log('error MileageRateSettingController.', response);
            });



            $scope.CreateNewVAT = function () {
                $location.path('/frmViewContent/315');
            };

            $scope.EditVAT = function (vatcode) {
                $location.path('/frmViewContent/315/' + vatcode);
            };

            var search = function (item, keyword) {
                // Search Criteria
                var pname = item.ProjectCode + " : " + item.Name;
                if (!keyword
                    || (item.Name.toLowerCase().indexOf(keyword) != -1)
                    || (item.ProjectCode.toLowerCase().indexOf(keyword) != -1)
                    || (pname.toLowerCase().indexOf(keyword) != -1)
                    || (item.ProjectAssignments.likeWithMappingIndexOf($scope.GetEmployee, keyword) != -1)
                    || (item.ProjectAssignments.length == 0 && $scope.Text['ACCOUNT_SETTING'].ALL.toLowerCase().indexOf(keyword) != -1)
                    ) {
                    return true;
                }
                /*for (var i = 0; i < $scope.ProjectAssignmentList.length; i++) {
                    for (var j = 0; j < $scope.ProjectAssignmentList[i].length; j++) {
                        if ($scope.ProjectAssignmentList[i][j].ProjectCode == item.ProjectCode) {
                            return true;
                        }
                    }
                }*/
                return false;
            };


            $scope.searchInBox = function () {
                $scope.firstLoad = false;
                console.log('searchInBox.', $scope.formData.searchKeyword);
                if (!$scope.formData.searchKeyword) {
                    console.log('just reset documents.');
                    $scope.documents = angular.copy($scope.masterDocuments);
                }
                else {
                    var count = $scope.masterDocuments.length;
                    var docs = [];
                    for (var i = 0; i < count; i++) {


                        if (search($scope.masterDocuments[i], $scope.formData.searchKeyword.toLowerCase())) {
                            docs.push($scope.masterDocuments[i]);
                        }
                        $scope.documents = docs;




                    }

                }
            };

            $scope.DeleteConfirm = function (projectcode, ev) {
                var confirm = $mdDialog.confirm()
                    .title($scope.Text['ACCOUNT_SETTING'].WANT_TO_DELETE)
                    .textContent($scope.Text['ACCOUNT_SETTING'].CAN_NOT_ROLLBACK_PLEASE_CONFIRM)
                    .ariaLabel($scope.Text['ACCOUNT_SETTING'].WANT_TO_DELETE)
                    .targetEvent(ev)
                    .ok($scope.Text['SYSTEM'].BUTTON_YES)
                    .cancel($scope.Text['SYSTEM'].BUTTON_NO);
                $mdDialog.show(confirm).then(function () {
                    //delete
                    //var URL = CONFIG.SERVER + 'HRTR/DeleteVATType/';
                    var oRequestParameter = { InputParameter: { "PROJECTCODE": projectcode }, CurrentEmployee: getToken(CONFIG.USER) }
                    var URL = CONFIG.SERVER + 'HRTR/DeleteProject/';
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {

                        //  case cannot remove project cause using in some document
                        if (response.data.length > 0) {

                            var list_used_doc = '';
                            for (var i = 0 ; i < response.data.length ; i++) {
                                list_used_doc += response.data[i].RequestNo+" ";
                            }
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .clickOutsideToClose(true)
                                    .title($scope.Text['ACCOUNT_SETTING'].DELETE_UNCOMPLETED)
                                    .textContent($scope.Text['ACCOUNT_SETTING'].USING_IN_SOME_DOCUMENT + "   " + list_used_doc)
                                    .ariaLabel($scope.Text['ACCOUNT_SETTING'].DELETE_UNCOMPLETED)
                                    .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                    .targetEvent(ev)
                                );
                            return;
                        }
                        // Success
                        var URL = CONFIG.SERVER + 'HRTR/GetAllProject/';
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            // Success
                            if (response.data) {
                                $mdDialog.show(
                                $mdDialog.alert()
                                    .clickOutsideToClose(true)
                                    .title($scope.Text['ACCOUNT_SETTING'].DELETE_COMPLETED)
                                    .textContent($scope.Text['ACCOUNT_SETTING'].CAN_NOT_ROLLBACK)
                                    .ariaLabel($scope.Text['ACCOUNT_SETTING'].DELETE_COMPLETED)
                                    .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                    .targetEvent(ev)
                                );
                                $scope.documents = response.data;
                                //$scope.ProjectAssignmentList = response.data;
                                for (var i = 0; i < $scope.ProjectAssignmentList.length; i++) {
                                    var index = $scope.documents.findIndexWithAttr('ProjectCode', $scope.ProjectAssignmentList[i].ProjectCode);
                                    if (index >= 0)
                                        $scope.documents[index].ProjectAssignments.push($scope.ProjectAssignmentList[i].EmployeeID);
                                }
                                $scope.masterDocuments = angular.copy($scope.documents);
                            }
                            else {
                                $mdDialog.show(
                                $mdDialog.alert()
                                    .clickOutsideToClose(true)
                                    .title($scope.Text['ACCOUNT_SETTING'].CAN_NOT_DELETE)
                                    .textContent($scope.Text['ACCOUNT_SETTING'].DATA_ACTIVE)
                                    .ariaLabel($scope.Text['ACCOUNT_SETTING'].CAN_NOT_DELETE)
                                    .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                    .targetEvent(ev)
                                );
                                //alert('This group is inused. Cannot Delete');
                            }
                        }, function errorCallback(response) {
                            // Error
                            console.log('error ProjectSettingController.', response);
                        });


                    }, function errorCallback(response) {
                        // Error
                        console.log('error ProjectSettingController.', response);
                    });

                    //get again


                });
            }






        }]);
})();