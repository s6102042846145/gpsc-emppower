﻿(function () {
    angular.module('ESSMobile')
        .controller('TravelTypeSettingEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {

            console.log('TravelTypeSettingEditorController');
            $scope.content.isShowHeader = true;
            $scope.content.isShowBackIcon = true;
            $scope.Textcategory = 'ACCOUNT_SETTING';
            $scope.content.Header = $scope.Text[$scope.Textcategory].TRAVELTYPE;
            $scope.Textcategory2 = 'SYSTEM';
            $scope.TravelType = {
                TravelTypeName: '',
                THCode: '',
                ENCode: '',
                BeginDate: $filter('date')(new Date(), 'yyyy-MM-ddT00:00:00'),
                EndDate: '9999-12-31T00:00:00'
            };
            $scope.AreaCheck = false;
            $scope.TravelType.PermissionALL = false;
            $scope.TravelType.IOALL = false;

            $scope.SaveData = function (ev) {
                var valid = true;
                if (!$scope.TravelType.THCode) valid = false;
                else if (!$scope.TravelType.ENCode) valid = false;
                else if (!$scope.TravelType.BeginDate) valid = false;
                else if (!$scope.TravelType.EndDate) valid = false;

                if (!valid) {
                    $scope.HasError = true;
                    $scope.ErrorText = $scope.Text['ACCOUNT_SETTING'].PLEASE_INSERT_ALL_FIELDS;
                }
                else {
                    var oRequestParameter = { InputParameter: { "TravelType": $scope.TravelType }, CurrentEmployee: $scope.employeeData, Requestor: getToken(CONFIG.USER) };
                    var URL = CONFIG.SERVER + 'HRTR/SaveTravelType/';
                    $scope.HasError = false;
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        var output = response.data;
                        if (output > 0) {
                            $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                .textContent($scope.Text['ACCOUNT_SETTING'].GO_BACK)
                                .ariaLabel($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                .targetEvent(ev));
                            $location.path('/frmViewContent/305');
                        }
                        else {
                            $scope.HasError = true;
                            $scope.ErrorText = $scope.Text['ACCOUNT_SETTING'].DATA_DUPLICATE;
                        }
                    }, function errorCallback(response) {
                        console.log('error SaveTravelType Save.', response);
                    });
                }
            }

            $scope.addEndDate = function () {
                if ($scope.TravelType.NoSpecific == true) {
                    $scope.TravelType.EndDate = "9999-12-31T00:00:00";
                }
                else if ($scope.TravelType.EndDate == "9999-12-31T00:00:00") {
                    $scope.TravelType.EndDate = $scope.TravelType.BeginDate;
                }
            };

            $scope.setSelectedBeginDate = function (selectedDate) {
                $scope.TravelType.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                if ($scope.TravelType.BeginDate > $scope.TravelType.EndDate) {
                    $scope.TravelType.EndDate = $scope.TravelType.BeginDate;
                }
                $scope.TravelType.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
            };

            $scope.setSelectedEndDate = function (selectedDate) {
                $scope.TravelType.EndDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                $scope.TravelType.NoSpecific = ($scope.TravelType.EndDate == "9999-12-31T00:00:00");
            };

            $scope.init = function () {
                getOrganization();
            }

            function getOrganization() {
                var oRequestParameter = { InputParameter: { "CreateDate": $filter('date')(dateToday, 'yyyy-MM-ddT00:00:00') }, CurrentEmployee: getToken(CONFIG.USER) }
                var URL = CONFIG.SERVER + 'HRTR/GetInternalOrderAll/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.InternalOrder = response.data;
                    getArea();
                }, function errorCallback(response) {
                    console.log('error GetInternalOrderAll.', response);
                });
            }

            var dateToday = new Date();

            function getTravelType() {
                var oRequestParameter = { InputParameter: { "TRAVELTYPEID": $scope.itemKey }, CurrentEmployee: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) }
                var URL = CONFIG.SERVER + 'HRTR/GetTravelType/';

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.TravelType = response.data[0];
                    $scope.TravelType.NoSpecific = ($scope.TravelType.EndDate == '9999-12-31T00:00:00');
                    $scope.masterDocuments = response.data;
                    $scope.ready = true;
                    //getTravelTypePermission();
                    //getTravelTypeIO();
                }, function errorCallback(response) {
                    console.log('error GetTravelType.', response);
                });
            }

            function getTravelTypePermission() {
                //Get TravelTypePermission
                var oRequestParameter = { InputParameter: { "TRAVELTYPEID": $scope.itemKey }, CurrentEmployee: getToken(CONFIG.USER) }
                URL = CONFIG.SERVER + 'HRTR/GetTravelTypePermission/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.TravelTypePermission = response.data;
                    if ($scope.TravelTypePermission[0].PersonalAreaCode == "*") {
                        //$scope.AreaCheck = true;
                        $scope.TravelType.PermissionALL = true;
                    }
                    else {
                        for (var i = 0; i < $scope.TravelTypePermission.length; i++) {

                            for (var j = 0; j < $scope.WorkingArea.length; j++) {
                                if ($scope.WorkingArea[j].PersonalAreaCode == $scope.TravelTypePermission[i].PersonalAreaCode) {
                                    temp = $scope.WorkingArea[j];
                                    break;
                                }
                            }
                            $scope.ExpType.VATType.push(temp.PersonalAreaCode);
                        }
                    }
                    console.log('TravelTypePermission', $scope.TravelTypePermission);


                }, function errorCallback(response) {
                    // Error
                    console.log('error TravelTypeSettingEditorController.', response);
                });
            }

            function getTravelTypeIO() {
                var oRequestParameter = { InputParameter: { "TRAVELTYPEID": $scope.itemKey }, CurrentEmployee: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) }
                URL = CONFIG.SERVER + 'HRTR/GetTravelTypeIO/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.TravelTypeIO = response.data;
                    if ($scope.TravelTypeIO.length == 0) {
                        $scope.TravelType.IOALL = true;
                    }
                    else {
                        var temp = null;
                        for (var i = 0; i < $scope.TravelTypeIO.length; i++) {

                            for (var j = 0; j < $scope.InternalOrder.length; j++) {
                                if ($scope.InternalOrder[j].ObjectID == $scope.TravelTypeIO[i].ObjectID) {
                                    temp = $scope.InternalOrder[j];
                                    break;
                                }
                            }
                            $scope.TravelType.IO.push(temp.ObjectID);
                        }
                    }
                }, function errorCallback(response) {
                    console.log('error GetTravelTypeIO.', response);
                });
            }

            function getArea() {
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }
                var URL = CONFIG.SERVER + 'HRTR/GetAllPersonalArea/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.WorkingArea = response.data;
                    if ($scope.itemKey != "null") {
                        getTravelType();
                    }
                    else {
                        $scope.ready = true;
                    }
                }, function errorCallback(response) {
                    console.log('error GetAllPersonalArea.', response);
                });
            }

        }]);
})();