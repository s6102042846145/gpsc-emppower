﻿(function () {
    angular.module('ESSMobile')
        .controller('CarRegistrationSettingController', ['$scope', '$http', '$routeParams', '$location', '$window', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $window, CONFIG, $mdDialog) {
            $scope.requestType = $routeParams.id;
            $scope.formData = { searchKeyword: '' };
            $scope.firstLoad = true;

            $scope.Textcategory = 'ACCOUNT_SETTING';
            $scope.content.isShowHeader = false;

            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }

            var URL = CONFIG.SERVER + 'HRTR/GetAllCarType/';
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                $scope.CarTypeList = response.data;

                URL = CONFIG.SERVER + 'HRTR/GetAllCarRegistration/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.CarRegistrationList = response.data;
                    $scope.masterDocuments = response.data;
                }, function errorCallback(response) {
                    // Error
                    console.log('error CarRegistrationListController.', response);
                });
            }, function errorCallback(response) {
                // Error
                console.log('error CarRegistrationListController.', response);
            });

            $scope.CreateNew = function () {
                $location.path('/frmViewContent/320');
            };

            $scope.Edit = function (itemID) {
                $location.path('/frmViewContent/320/' + itemID);
            };

            var search = function (item, keyword) {
                // Search Criteria
                var temp = findCarType(item.CarTypeID);
                if (!keyword
                    || (item.CarRegistrationDetail.toLowerCase().indexOf(keyword) != -1)
                    || (item.VATCode.toLowerCase().indexOf(keyword) != -1)
                    || (temp.toLowerCase().indexOf(keyword) != -1)
                    ) {
                    return true;
                }
                return false;
            };

            function findCarType(carID) {
                var temp = "";
                for (var i = 0; i < $scope.CarTypeList.length; i++) {
                    if ($scope.CarTypeList[i].CarTypeID == carID) {
                        temp = $scope.CarTypeList[i].Description;
                    }
                }
                return temp;
            }

            $scope.searchInBox = function () {
                $scope.firstLoad = false;
                console.log('searchInBox.', $scope.formData.searchKeyword);
                if (!$scope.formData.searchKeyword) {
                    console.log('just reset documents.');
                    $scope.CarRegistrationList = $scope.masterDocuments;
                }
                else {
                    var count = $scope.masterDocuments.length;
                    var docs = [];
                    for (var i = 0; i < count; i++) {
                        if (search($scope.masterDocuments[i], $scope.formData.searchKeyword.toLowerCase())) {
                            docs.push($scope.masterDocuments[i]);
                        }
                    }
                    $scope.CarRegistrationList = docs;
                }
            };

            $scope.DeleteConfirm = function (itemID, ev) {
                var confirm = $mdDialog.confirm()
                    .title($scope.Text['ACCOUNT_SETTING'].WANT_TO_DELETE)
                    .textContent($scope.Text['ACCOUNT_SETTING'].CAN_NOT_ROLLBACK_PLEASE_CONFIRM)
                    .ariaLabel($scope.Text['ACCOUNT_SETTING'].WANT_TO_DELETE)
                    .targetEvent(ev)
                    .ok($scope.Text['SYSTEM'].BUTTON_YES)
                    .cancel($scope.Text['SYSTEM'].BUTTON_NO);
                $mdDialog.show(confirm).then(function () {
                    var oRequestParameter = { InputParameter: { "CarRegisID": itemID }, CurrentEmployee: getToken(CONFIG.USER) }
                    var URL = CONFIG.SERVER + 'HRTR/DeleteCarRegistration/';
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        if (response.data == true) {
                            oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }
                            var URL = CONFIG.SERVER + 'HRTR/GetAllCarRegistration/';
                            $http({
                                method: 'POST',
                                url: URL,
                                data: oRequestParameter
                            }).then(function successCallback(response) {
                                // Success
                                $scope.CarRegistrationList = response.data;
                                $scope.masterDocuments = response.data;
                                $mdDialog.show(
                                $mdDialog.alert()
                                    .clickOutsideToClose(true)
                                    .title($scope.Text['ACCOUNT_SETTING'].DELETE_COMPLETED)
                                    .textContent($scope.Text['ACCOUNT_SETTING'].CAN_NOT_ROLLBACK)
                                    .ariaLabel($scope.Text['ACCOUNT_SETTING'].DELETE_COMPLETED)
                                    .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                    .targetEvent(ev)
                                );
                            }, function errorCallback(response) {
                                // Error
                                console.log('error CarRegistrationSettingController.', response);
                            });
                        }
                        else {
                            $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['ACCOUNT_SETTING'].CAN_NOT_DELETE)
                                .textContent($scope.Text['ACCOUNT_SETTING'].DATA_ACTIVE)
                                .ariaLabel($scope.Text['ACCOUNT_SETTING'].CAN_NOT_DELETE)
                                .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                .targetEvent(ev)
                            );
                        }
                    }, function errorCallback(response) {
                        // Error
                        console.log('error CarRegistrationSettingController.', response);
                    });
                });
            }
        }]);
})();