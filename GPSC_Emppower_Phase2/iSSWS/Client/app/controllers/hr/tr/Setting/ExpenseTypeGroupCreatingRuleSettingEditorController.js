﻿(function () {
    angular.module('ESSMobile')
        .controller('ExpenseTypeGroupCreatingRuleSettingEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$timeout', '$q', '$log', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $timeout, $q, $log, $mdDialog) {

            console.log('ExpenseTypeGroupCreatingRuleSettingEditorController');

            $scope.CreatingRule = {
                ExpenseTypeGroupID: '',
                searchETGText: '',
                BeginDate: $filter('date')(new Date(), 'yyyy-MM-ddT00:00:00'),
                EndDate: '9999-12-31T00:00:00',
                NoSpecific: true,
                MinEmpSubGroup: '',
                MaxEmpSubGroup: '',
                AreaGroupID: '',
                searchAreaGroupText: '',
                MinValue: '',
                MaxValue: '',
                AccummulateOffset: '',
                AccummulateOffsetUnit: '',
                MinTravelDay: '',
                MaxTravelDay: '',
                Description: ''
            };

            $scope.init = function () {
                var URL = CONFIG.SERVER + 'HRTR/GetAllExpenseTypeGroup/';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.expenseTypeGroupList = response.data;
                    getAreaGroup();
                }, function errorCallback(response) {
                    console.log('error GetAllExpenseTypeGroup.', response);
                });
            }

            function getAreaGroup() {
                var URL = CONFIG.SERVER + 'HRTR/GetAllExpenseAreaGroup/';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.ExpenseAreaGroupList = response.data;
                    if ($scope.itemKey != "null") {
                        getExpenseTypeGroupCreatingRule();
                    }
                    else{
                        $scope.ready = true;
                    }
                }, function errorCallback(response) {
                    console.log('error GetAllExpenseAreaGroup.', response);
                });
            }

            function getExpenseTypeGroupCreatingRule() {
                var oRequestParameter = { InputParameter: { "ETGID": $scope.itemKey }, CurrentEmployee: getToken(CONFIG.USER) }
                var URL = CONFIG.SERVER + 'HRTR/GetExpenseTypeGroupCreatingRule/';

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.CreatingRule = response.data;
                    $scope.CreatingRule.NoSpecific = ($scope.CreatingRule.EndDate == '9999-12-31T00:00:00');
                    $scope.checkText_ETG($scope.CreatingRule.ExpenseTypeGroupID);
                    $scope.checkText_AreaGroup($scope.CreatingRule.AreaGroupID);
                    //$scope.CreatingRule.ExpenseTypeGroupIDObject = $scope.expenseTypeGroupList[$scope.expenseTypeGroupList.findIndexWithAttr('ExpenseTypeGroupID', $scope.CreatingRule.ExpenseTypeGroupID)];
                    //$scope.CreatingRule.AreaGroupIDObject = $scope.ExpenseAreaGroupList[$scope.ExpenseAreaGroupList.findIndexWithAttr('AreaGroupID', $scope.CreatingRule.AreaGroupID)];
                    $scope.ready = true;
                }, function errorCallback(response) {
                    console.log('error GetExpenseTypeGroupCreatingRule.', response);
                });
            }

            var employeeData = getToken(CONFIG.USER);
            console.log(employeeData);
            $scope.content.isShowHeader = true;
            $scope.content.isShowBackIcon = true;
            $scope.data = {};
            $scope.data.NoSpecific = true;
            $scope.HasError = false;
            $scope.ErrorText = "";

            $scope.Textcategory = 'ACCOUNT_SETTING';
            $scope.content.Header = $scope.Text[$scope.Textcategory].EXPENSETYPEGROUP_CREATINGRULE_SETTING;
            $scope.Textcategory2 = 'SYSTEM';

            $scope.onBlurMinEmpSubGroup = function () {
                if (($scope.CreatingRule.MinEmpSubGroup != null && $scope.CreatingRule.MinEmpSubGroup != '' && isNaN($scope.CreatingRule.MinEmpSubGroup)) || $scope.CreatingRule.MinEmpSubGroup.indexOf('.') >= 0 || $scope.CreatingRule.MinEmpSubGroup.indexOf('-') >= 0 || $scope.CreatingRule.MinEmpSubGroup.indexOf('+') >= 0) {
                    $scope.CreatingRule.MinEmpSubGroup = '';
                }
            };

            $scope.onBlurMaxEmpSubGroup = function () {
                if (($scope.CreatingRule.MaxEmpSubGroup != null && $scope.CreatingRule.MaxEmpSubGroup != '' && isNaN($scope.CreatingRule.MaxEmpSubGroup)) || $scope.CreatingRule.MaxEmpSubGroup.indexOf('.') >= 0 || $scope.CreatingRule.MaxEmpSubGroup.indexOf('-') >= 0 || $scope.CreatingRule.MaxEmpSubGroup.indexOf('+') >= 0) {
                    $scope.CreatingRule.MaxEmpSubGroup = '';
                }
            };

            $scope.onBlurMinValue = function () {
                if (($scope.CreatingRule.MinValue != null && $scope.CreatingRule.MinValue != '' && isNaN($scope.CreatingRule.MinValue)) || $scope.CreatingRule.MinValue.indexOf('-') >= 0 || $scope.CreatingRule.MinValue.indexOf('+') >= 0) {
                    $scope.CreatingRule.MinValue = $scope.tempMinValue;
                } else {
                    $scope.tempMinValue = $scope.CreatingRule.MinValue;
                }
            };

            $scope.onBlurMaxValue = function () {
                if (($scope.CreatingRule.MaxValue != null && $scope.CreatingRule.MaxValue != '' && isNaN($scope.CreatingRule.MaxValue)) || $scope.CreatingRule.MaxValue.indexOf('-') >= 0 || $scope.CreatingRule.MaxValue.indexOf('+') >= 0) {
                    $scope.CreatingRule.MaxValue = $scope.tempMaxValue;
                } else {
                    $scope.tempMaxValue = $scope.CreatingRule.MaxValue;
                }
            };

            $scope.onBlurAccummulateOffset = function () {
                if (($scope.CreatingRule.AccummulateOffset != null && $scope.CreatingRule.AccummulateOffset != '' && isNaN($scope.CreatingRule.AccummulateOffset)) || $scope.CreatingRule.AccummulateOffset.indexOf('.') >= 0 || $scope.CreatingRule.AccummulateOffset.indexOf('-') >= 0 || $scope.CreatingRule.AccummulateOffset.indexOf('+') >= 0) {
                    $scope.CreatingRule.AccummulateOffset = $scope.tempAccummulateOffset;
                } else {
                    $scope.tempAccummulateOffset = $scope.CreatingRule.AccummulateOffset;
                }
            };

            $scope.onBlurMinTravelDay = function () {
                if (($scope.CreatingRule.MinTravelDay != null && $scope.CreatingRule.MinTravelDay != '' && isNaN($scope.CreatingRule.MinTravelDay)) || $scope.CreatingRule.MinTravelDay.indexOf('.') >= 0 || $scope.CreatingRule.MinTravelDay.indexOf('-') >= 0 || $scope.CreatingRule.MinTravelDay.indexOf('+') >= 0) {
                    $scope.CreatingRule.MinTravelDay = $scope.tempMinTravelDay;
                } else {
                    $scope.tempMinTravelDay = $scope.CreatingRule.MinTravelDay;
                }
            };

            $scope.onBlurMaxTravelDay = function () {
                if (($scope.CreatingRule.MaxTravelDay != null && $scope.CreatingRule.MaxTravelDay != '' && isNaN($scope.CreatingRule.MaxTravelDay)) || $scope.CreatingRule.MaxTravelDay.indexOf('.') >= 0 || $scope.CreatingRule.MaxTravelDay.indexOf('-') >= 0 || $scope.CreatingRule.MaxTravelDay.indexOf('+') >= 0) {
                    $scope.CreatingRule.MaxTravelDay = $scope.tempMaxTravelDay;
                } else {
                    $scope.tempMaxTravelDay = $scope.CreatingRule.MaxTravelDay;
                }
            };

            $scope.addEndDate = function () {
                if ($scope.CreatingRule.NoSpecific == true) {
                    $scope.CreatingRule.EndDate = "9999-12-31T00:00:00";
                }
                else if ($scope.CreatingRule.EndDate == "9999-12-31T00:00:00") {
                    $scope.CreatingRule.EndDate = $scope.CreatingRule.BeginDate;
                }
            };

            $scope.setSelectedBeginDate = function (selectedDate) {
                $scope.CreatingRule.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                if ($scope.CreatingRule.BeginDate > $scope.CreatingRule.EndDate) {
                    $scope.CreatingRule.EndDate = $scope.CreatingRule.BeginDate;
                }
                $scope.CreatingRule.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
            };

            $scope.setSelectedEndDate = function (selectedDate) {
                $scope.CreatingRule.EndDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                $scope.CreatingRule.NoSpecific = ($scope.CreatingRule.EndDate == "9999-12-31T00:00:00");
            };

            //onsave
            $scope.SaveData = function (ev) {
                var valid = true;
                if (!$scope.CreatingRule.ExpenseTypeGroupID) valid = false;
                else if (!$scope.CreatingRule.MinEmpSubGroup.toString().length) valid = false;
                else if (!$scope.CreatingRule.MaxEmpSubGroup.toString().length) valid = false;
                else if (!$scope.CreatingRule.AreaGroupID) valid = false;
                else if (!$scope.CreatingRule.MinValue.toString().length) valid = false;
                else if (!$scope.CreatingRule.MaxValue.toString().length) valid = false;
                else if (!$scope.CreatingRule.AccummulateOffset.toString().length) valid = false;
                else if (!$scope.CreatingRule.AccummulateOffsetUnit) valid = false;
                else if (!$scope.CreatingRule.MinTravelDay.toString().length) valid = false;
                else if (!$scope.CreatingRule.MaxTravelDay.toString().length) valid = false;
                else if (!$scope.CreatingRule.Description) valid = false;

                if (!valid) {
                    $scope.HasError = true;
                    $scope.ErrorText = $scope.Text['ACCOUNT_SETTING'].PLEASE_INSERT_ALL_FIELDS;
                }
                else {
                    var oRequestParameter = { InputParameter: { "CREATINGRULE": $scope.CreatingRule }, CurrentEmployee: $scope.employeeData, Requestor: getToken(CONFIG.USER) };
                    var URL = CONFIG.SERVER + 'HRTR/SaveExpenseTypeGroupCreatingRule/';
                    $scope.HasError = false;
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        var output = response.data;
                        if (output > 0) {
                            $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                .textContent($scope.Text['ACCOUNT_SETTING'].GO_BACK)
                                .ariaLabel($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                .targetEvent(ev));
                            $location.path('/frmViewContent/326');
                        }
                        else {
                            $scope.HasError = true;
                            $scope.ErrorText = $scope.Text['ACCOUNT_SETTING'].DATA_DUPLICATE;
                        }
                    }, function errorCallback(response) {
                        // Error
                        console.log('error ExpenseTypeGroupCreatingRuleSettingEditorController Save.', response);
                    });
                }
            }

            //auto complete Expense Type Group 
            $scope.simulateQuery = true;
            $scope.querySearchETG = function (query) {
                $scope.CreatingRule.ExpenseTypeGroupID = '';
                var results = query ? $scope.expenseTypeGroupList.filter(createFilterForETG(query)) : $scope.expenseTypeGroupList, deferred;
                if ($scope.simulateQuery) {
                    deferred = $q.defer();
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 300, false);
                    return deferred.promise;
                } else {
                    return results;
                }
            };

            $scope.selectedItemETGChange = function (item) {
                if (angular.isDefined(item) && item != null) {
                    $scope.CreatingRule.searchETGText = item.Remark;
                    $scope.CreatingRule.ExpenseTypeGroupID = item.ExpenseTypeGroupID;
                }
                else {
                    $scope.CreatingRule.searchETGText = '';
                    $scope.CreatingRule.ExpenseTypeGroupID = '';
                }
            };

            function createFilterForETG(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    var source = angular.lowercase(x.Remark);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }

            $scope.checkText_ETG = function (text) {
                var result = null;
                if (text) {
                    for (var i = 0; i < $scope.expenseTypeGroupList.length; i++) {
                        if ($scope.expenseTypeGroupList[i].ExpenseTypeGroupID == text || $scope.expenseTypeGroupList[i].Remark == text) {
                            result = $scope.expenseTypeGroupList[i];
                            break;
                        }
                    }
                }
                if (result) {
                    $scope.selectedItemETGChange(result);
                }
                else {
                    $scope.selectedItemETGChange(null);
                }
            }

            //auto complete Area Group
            $scope.simulateQuery = true;
            $scope.querySearchAreaGroup = function (query) {
                $scope.CreatingRule.AreaGroupID = '';
                var results = query ? $scope.ExpenseAreaGroupList.filter(createFilterForAreaGroup(query)) : $scope.ExpenseAreaGroupList, deferred;
                if ($scope.simulateQuery) {
                    deferred = $q.defer();
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 300, false);
                    return deferred.promise;
                } else {
                    return results;
                }
            };

            $scope.selectedItemAreaGroupChange = function (item) {
                if (angular.isDefined(item) && item != null) {
                    $scope.CreatingRule.searchAreaGroupText = item.Description;
                    $scope.CreatingRule.AreaGroupID = item.AreaGroupID;
                }
                else {
                    $scope.CreatingRule.searchAreaGroupText = '';
                    $scope.CreatingRule.AreaGroupID = '';
                }
            };

            function createFilterForAreaGroup(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    var source = angular.lowercase(x.Description);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }

            $scope.checkText_AreaGroup = function (text) {
                var result = null;
                if (text) {
                    for (var i = 0; i < $scope.ExpenseAreaGroupList.length; i++) {
                        if ($scope.ExpenseAreaGroupList[i].AreaGroupID == text || $scope.ExpenseAreaGroupList[i].Description == text) {
                            result = $scope.ExpenseAreaGroupList[i];
                            break;
                        }
                    }
                }
                if (result) {
                    $scope.selectedItemAreaGroupChange(result);
                }
                else {
                    $scope.selectedItemAreaGroupChange(null);
                }
            }
        }]);
})();

