﻿(function () {
    angular.module('ESSMobile')
        .controller('CarRegistrationSettingEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$timeout', '$q', '$log', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $timeout, $q, $log, $mdDialog) {
            console.log('CarRegistrationSettingEditorController');
            $scope.content.isShowHeader = true;
            $scope.content.isShowBackIcon = true;
            $scope.CarRegistration = {
                CarRegistrationDetail: '',
                CarTypeID: '',
                VATCode: '',
                BeginDate: $filter('date')(new Date(), 'yyyy-MM-ddT00:00:00'),
                EndDate: '9999-12-31T00:00:00',
                NoSpecify: true
            };
            $scope.HasError = false;
            $scope.ErrorText = "";
            $scope.Textcategory = 'ACCOUNT_SETTING';
            $scope.content.Header = $scope.Text[$scope.Textcategory].CARREGISTRATION_TITLE;

            var res = $scope.itemKey.split("*");
            var oRequestParameter = { InputParameter: { "ItemID": res[0], "EmployeeID": res[1] }, CurrentEmployee: getToken(CONFIG.USER) }
            var URL = CONFIG.SERVER + 'HRTR/GetAllCarType/';
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                $scope.CarTypeList = response.data;

                URL = CONFIG.SERVER + 'HRTR/GetAllVATType/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.VATTypeList = response.data;
                    
                    if ($scope.itemKey != "null") {
                        URL = CONFIG.SERVER + 'HRTR/GetCarRegistration/';
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            // Success
                            $scope.CarRegistration = response.data;
                            $scope.CarRegistration.NoSpecify = ($scope.CarRegistration.EndDate == "9999-12-31T00:00:00");
                            
                        }, function errorCallback(response) {
                            // Error
                            console.log('error CarRegistrationSettingEditorController.', response);
                        });
                    }
                }, function errorCallback(response) {
                    // Error
                    console.log('error CarRegistrationSettingEditorController.', response);
                });
            }, function errorCallback(response) {
                // Error
                console.log('error CarRegistrationSettingEditorController.', response);
            });

            $scope.UndefinedEndDateChanged = function () {
                if ($scope.CarRegistration.NoSpecify == true) {
                    $scope.CarRegistration.EndDate = "9999-12-31T00:00:00";
                }
                else if ($scope.CarRegistration.EndDate == "9999-12-31T00:00:00") {
                    $scope.CarRegistration.EndDate = $scope.CarRegistration.BeginDate;
                }
            };

            $scope.setSelectedBeginDate = function (selectedDate) {
                $scope.CarRegistration.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
            };

            $scope.setSelectedEndDate = function (selectedDate) {
                $scope.CarRegistration.EndDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                $scope.CarRegistration.NoSpecify = ($scope.CarRegistration.EndDate == '9999-12-31T00:00:00');
            };

            $scope.SaveData = function (ev) {
                var valid = true;
                if (!$scope.CarRegistration.CarRegistrationDetail) valid = false;
                else if (!$scope.CarRegistration.CarTypeID) valid = false;
                else if (!$scope.CarRegistration.VATCode) valid = false;
                else if (!$scope.CarRegistration.BeginDate) valid = false;
                else if (!$scope.CarRegistration.EndDate) valid = false;

                if (!valid) {
                    $scope.HasError = true;
                    $scope.ErrorText = $scope.Text['ACCOUNT_SETTING'].PLEASE_INSERT_ALL_FIELDS;
                }
                else {
                    var oRequestParameter = { InputParameter: { "CarRegistration": $scope.CarRegistration }, CurrentEmployee: $scope.employeeData, Requestor: getToken(CONFIG.USER) }

                    var URL = CONFIG.SERVER + 'HRTR/SaveCarRegistration/';
                    $scope.HasError = false;
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        var output = response.data;
                        if (output > 0) {
                            $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                .textContent($scope.Text['ACCOUNT_SETTING'].GO_BACK)
                                .ariaLabel($scope.Text['ACCOUNT_SETTING'].SAVE_COMPLETED)
                                .ok($scope.Text['SYSTEM'].BUTTON_OK)
                                .targetEvent(ev));
                            $location.path('/frmViewContent/319');
                        }
                        else {
                            $scope.HasError = true;
                            $scope.ErrorText = "Save failed";
                        }

                    }, function errorCallback(response) {
                        // Error
                        console.log('error CarRegistrationSettingEditorController Save.', response);
                    });
                }
            };
        }]);
})();

