﻿(function () {
    angular.module('ESSMobile')
        .controller('GeneralExpenseContentController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', 'DTOptionsBuilder', 'DTColumnDefBuilder', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, DTOptionsBuilder, DTColumnDefBuilder, $mdDialog) {
            $scope.Textcategory = 'EXPENSE';
            $scope.content.isShowHeader = false;
            $scope.HeaderText = $scope.Text[$scope.Textcategory].CONSOLE_HEADSUBJECT;
            $scope.content.Header = $scope.HeaderText;
            $scope.pagin = {
                currentPage: 1, itemPerPage: '10', numPage: 1, cbbItemPerPage: [ '5', '10', '20', '50'], pages: []
            }
            // data tables config
            //$scope.dtOptions = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(15).withOption('lengthChange', false).withOption('ordering', false);
            //$scope.dtColumnDefs = [
            //    DTColumnDefBuilder.newColumnDef(0).notSortable(),
            //    DTColumnDefBuilder.newColumnDef(1).notSortable(),
            //    DTColumnDefBuilder.newColumnDef(2).notSortable(),
            //    DTColumnDefBuilder.newColumnDef(3).notSortable(),
            //    DTColumnDefBuilder.newColumnDef(4).notSortable(),
            //    DTColumnDefBuilder.newColumnDef(5).notSortable(),
            //    DTColumnDefBuilder.newColumnDef(6).notSortable(),
            //    DTColumnDefBuilder.newColumnDef(7).notSortable(),
            //    DTColumnDefBuilder.newColumnDef(8).notSortable()
            //];

            //var employeeData = getToken(CONFIG.USER);


            $scope.formData = {
                isAdvancedSearch: false,
                year: '',
                month: '',
                beginDate: $filter('date')(new Date(), 'yyyy-MM-dd'),
                endDate: $filter('date')(new Date(), 'yyyy-MM-dd'),
                status: 'ALL',
                costCenter: '',
                io: '',
                TravelRequestNo: '',
                PettyCashName: ''
            };

            $scope.settings = {
                ENABLEPETTYCASH: false,
                Currency: $scope.employeeData.AreaSetting.CurrencyCode,
                GE_ENABLE_TRAVEL_REF: false,
                ENABLE_PROJECTCOST: false,
            };

            $scope.setSelectedBeginDate = function (selectedDate) {
                $scope.formData.beginDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
                if ($scope.formData.beginDate > $scope.formData.endDate) {
                    $scope.formData.endDate = $scope.formData.beginDate;
                }
            };

            $scope.setSelectedEndDate = function (selectedDate) {
                $scope.formData.endDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };

            //var fnGetGEStatus = function () {

            //    var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER) };

            //    var URL = CONFIG.SERVER + 'HRTR/GetGEStatus';
            //    $http({
            //        method: 'POST',
            //        url: URL,
            //        data: oRequestParameter
            //    }).then(function successCallback(response) {
            //        // Success
            //        $scope.expenseStatus = response.data;

            //    }, function errorCallback(response) {
            //        // Error
            //        console.log('error GeneralExpenseContentController GetGEStatus.', response);

            //    });
            //};
            //fnGetGEStatus();

            $scope.getStatusText = function (key) {
                if (typeof ($scope.expenseStatus) != 'undefined') {
                    for (var i = 0; i < $scope.expenseStatus.length; i++) {
                        if ($scope.expenseStatus[i].Key == key) {
                            return $scope.expenseStatus[i].Value;
                        }
                    }
                    return '-';
                }
            };

            //$scope.getCostCenterAll = function () {
            //    var URL = CONFIG.SERVER + 'HRTR/GetCostCenter';
            //    var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };
            //    $http({
            //        method: 'POST',
            //        url: URL,
            //        data: oRequestParameter
            //    }).then(function successCallback(response) {

            //        var oAll = { "CostCenterCode": "", "LongDesc": "ALL :" };
            //        var oCost = response.data;

            //        oCost.unshift(oAll);
            //        $scope.objCostcenter = oCost;
            //        //$scope.objCostcenter = response.data;

            //    }, function errorCallback(response) {
            //        console.log('error GeneralExpenseContentController GetCostcenter.', response);
            //    });
            //}
            //$scope.getCostCenterAll();


            //$scope.getInternalOrderAll = function (CostCenter) {
            //    var URL = CONFIG.SERVER + 'HRTR/GetInternalOrder/';
            //    var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };
            //    $http({
            //        method: 'POST',
            //        url: URL,
            //        data: oRequestParameter
            //    }).then(function successCallback(response) {

            //        var oAll = { "OrderID": "", "Description": "ALL :" };
            //        var oIO = response.data;

            //        oIO.unshift(oAll);
            //        $scope.objIO = oIO;
            //        //$scope.objIO = response.data;

            //    }, function errorCallback(response) {
            //        console.log('error GeneralExpenseContentController GetInternalOrder.', response);
            //    });
            //};
            //$scope.getInternalOrderAll();


            $scope.searchGeneralExpense = function () {

                console.log('$scope.formData.month', $scope.formData.month)
                var oCondition;

                var URL;
                if ($scope.formData.isAdvancedSearch) {

                    oCondition = {
                        beginDate: (($scope.formData.beginDate == '') ? 'null' : $scope.formData.beginDate),
                        endDate: (($scope.formData.endDate == '') ? 'null' : $scope.formData.endDate),
                        costCenter: (($scope.formData.costCenter == '') ? 'null' : $scope.formData.costCenter),
                        io: (($scope.formData.io == '') ? 'null' : $scope.formData.io),
                        status: (($scope.formData.status == '') ? 'ALL' : $scope.formData.status),
                        year: 'null',
                        month: 'null',
                        isCondition: true,
                        TravelRequestNo: $scope.formData.TravelRequestNo,
                        PettyCashName: $scope.formData.PettyCashName,
                    };

                } else {
                    oCondition = {
                        beginDate: 'null',
                        endDate: 'null',
                        costCenter: 'null',
                        io: 'null',
                        status: 'ALL',
                        year: $scope.formData.year,
                        month: $scope.formData.month,
                        isCondition: false,
                        TravelRequestNo: '',
                        PettyCashName: '',
                    };
                    //var p1 = $scope.formData.year;
                    //var p2 = $scope.formData.month;
                    //URL = CONFIG.SERVER + 'HRTR/SearchGeneralExpense/' + p1 + '/' + p2;
                }
                
                URL = CONFIG.SERVER + 'HRTR/SearchGeneralExpense/';
                var oRequestParameter = {
                    InputParameter: {
                        "beginDate": oCondition.beginDate, "endDate": oCondition.endDate, "costCenter": oCondition.costCenter,
                        "io": oCondition.io, "status": oCondition.status, "year": oCondition.year, "month": oCondition.month, "isCondition": oCondition.isCondition
                        , "TravelRequestNo": oCondition.TravelRequestNo
                        , "PettyCashName": oCondition.PettyCashName
                    }, CurrentEmployee: $scope.employeeData //CommentBy: Ratchatawan W. (21 Dec 2016), Requestor: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER)
                    , Requestor: $scope.requesterData
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.expenses = response.data;
                    //console.log('GeneralExpenseReports.', $scope.expenses);
                    if ($scope.formData.IsSearchClick) {
                        if ($scope.formData.isAdvancedSearch) {
                            $scope.content.ContentParam = $scope.formData.isAdvancedSearch + '&&' + $scope.formData.beginDate + '&&' + $scope.formData.endDate + '&&' + $scope.formData.costCenter + '&&' + $scope.formData.io + '&&' + $scope.formData.status;
                        } else {
                            $scope.content.ContentParam = $scope.formData.isAdvancedSearch + '&&' + $scope.formData.month + '&&' + $scope.formData.year;
                        }
                        $scope.content.ContentParam = $scope.content.ContentParam + '&&' + $scope.formData.IsSearchClick;
                        var tempurl = "";
                        var temp = document.URL.substring(document.URL.indexOf("/102") + 4, document.URL.length);
                        if (temp.indexOf("&&") > 0) {
                            var tempStr = temp.split('/')[temp.split('/').length - 1];
                            temp = temp.substring(0, temp.indexOf(tempStr) - 1);
                        }
                        if ($scope.content.ContentParam) {
                            tempurl = "/frmViewContent/102" + decodeURIComponent(temp) + "/" + $scope.content.ContentParam;
                        }
                        else {
                            tempurl = "/frmViewContent/102" + decodeURIComponent(temp);
                        }
                        $location.path(tempurl, false);
                    }
                    else {
                        $scope.formData.IsSearchClick = false;
                    }
                    $scope.updatePages();
                }, function errorCallback(response) {
                    // Error
                    console.log('error GeneralExpenseContentController SearchGeneralExpense.', response);

                });
            };

            //$scope.ViewCoverSheet = function (requestNo, compCode) {
            //    //URL = CONFIG.SERVER + 'Client/files/' + compCode + '/' + requestNo + "/CoverSheetGE" + requestNo + ".pdf";
            //    //window.open(URL);
            //    $scope.loader.enable = true; // เปิด

            //    URL = CONFIG.SERVER + 'HRTR/GenerateGeneralExpenseReportCoverSheet/';
            //    var oRequestParameter = { InputParameter: { "RequestNo": requestNo }, CurrentEmployee: $scope.employeeData, Requestor: $scope.requesterData };//CommentBy: Ratchatawan W. (21 Dec 2016), Requestor: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER) };
            //    $http({
            //        method: 'POST',
            //        url: URL,
            //        data: oRequestParameter
            //    }).then(function successCallback(response) {
            //        if (response.data) {
            //            URL = CONFIG.SERVER + 'Client/files/' + compCode + '/' + requestNo + "/CoverSheetGE" + requestNo + ".pdf";
            //            window.open(URL);
            //            $scope.loader.enable = false; // เปิด

            //        }
            //        else {

            //            $scope.loader.enable = false; // เปิด
            //            $mdDialog.show(
            //             $mdDialog.alert()
            //               .clickOutsideToClose(false)
            //               .title($scope.Text['SYSTEM']['WARNING'])
            //               .textContent($scope.Text['SYSTEM']['FILENOTFOUND'])
            //               .ok($scope.Text['SYSTEM']['BUTTON_OK'])
            //            );
            //        }
            //        //window.open("data:application/pdf;base64, " + escape(response.data));
            //    }, function errorCallback(response) {
            //        $scope.loader.enable = false; // เปิด
            //        $mdDialog.show(
            //            $mdDialog.alert()
            //            .clickOutsideToClose(false)
            //            .title($scope.Text['SYSTEM']['WARNING'])
            //            .textContent(response.data)
            //            .ok($scope.Text['SYSTEM']['BUTTON_OK'])
            //        );
            //    });
            //};

            $scope.ViewCoverSheetNew = function (requestNo, compCode) {

              //URL = CONFIG.SERVER + 'Client/files/' + compCode + '/' + requestNo + "/CoverSheet.pdf";
                //window.open(URL);

                $scope.loader.enable = true; // เปิด

                //ตรวจสอบว่ามี File ที่เป็น Freeze path อยู่หรือไม่ หากมีให้เปิดขึ้นมา หากไม่มีให้ Gen Coversheet ใหม่
                var freezeURL = CONFIG.SERVER + 'Client/files/FreezePath/' + compCode + '/' + requestNo + '/CoverSheetGE' + requestNo + '.pdf?t='+(new Date()).getTime().toString();
                if (UrlExists(freezeURL)) {
                    window.open(freezeURL);
                    $scope.loader.enable = false; // เปิด
                    $scope.GetCoverSheetLogPrint(requestNo, 1);
                    $scope.searchGeneralExpense();
                }

                // test read filw with steam 
                //var URL = CONFIG.SERVER + 'HRTR/OpenPDFWithPath';
                //var oRequestParameter = { InputParameter: { 'RequestNo': requestNo, 'TYPE': 'CoverSheetGE' }, CurrentEmployee: $scope.employeeData, Requestor: $scope.requesterData };
                //$http({
                //    method: 'POST',
                //    url: URL,
                //    data: oRequestParameter
                //}).then(function successCallback(response) {
                //    $scope.loader.enable = false;
                //    console.log('OpenPDFWithPath.', response.data);
                //}, function errorCallback(response) {
                //    $scope.loader.enable = false; // เปิด
                //    console.log('error MergeOpenTravelRequestWithCACoverSheet.', response);
                //    $mdDialog.show(
                //        $mdDialog.alert()
                //        .clickOutsideToClose(false)
                //        .title($scope.Text['SYSTEM']['WARNING'])
                //        .textContent($scope.Text['SYSTEM']['FILENOTFOUND'])
                //        .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                //    );
                //});

                //URL = CONFIG.SERVER + 'HRTR/GetGeneralExpenseReportNewCoverSheet';
                //var oRequestParameter = { InputParameter: { "RequestNo": requestNo }, CurrentEmployee: $scope.employeeData, Requestor: $scope.requesterData };//CommentBy: Ratchatawan W. (21 Dec 2016), Requestor: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER) };
                //if (typeof cordova != 'undefined') {
                //    cordova.InAppBrowser.open("data:application/pdf;base64, " + escape(response.data), '_system', 'location=no');
                //} else {
                //    $http({
                //        method: 'POST',
                //        url: URL,
                //        data: oRequestParameter
                //    }).then(function successCallback(response) {
                //        console.log(response);
                //        window.open("data:application/pdf;base64, " + escape(response.data));
                //        $scope.loader.enable = false; // เปิด

                //    }, function errorCallback(response) {
                //        $scope.loader.enable = false; // เปิด
                //        $mdDialog.show(
                //            $mdDialog.alert()
                //            .clickOutsideToClose(false)
                //            .title($scope.Text['SYSTEM']['WARNING'])
                //            .textContent(response.data)
                //            .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                //        );
                //    });
                //}
            };


            function UrlExists(url) {
                var http = new XMLHttpRequest();
                http.open('HEAD', url, false);
                http.send();
                return http.status != 404;
            }



            $scope.clearInput = function () {
                $scope.formData.beginDate ='';
                $scope.formData.endDate ='';
                $scope.formData.costCenter='';
                $scope.formData.io='';
                $scope.formData.status = $scope.expenseStatus[0].Key;
                $scope.formData.TravelRequestNo='';
                $scope.formData.PettyCashName ='';
            }
            $scope.clickSearchGE = function () {
                //console.log('search ge.', $scope.formData);
                $scope.formData.IsSearchClick = true;
                $scope.searchGeneralExpense();
            };

           
            //Dict GetTransactionStatusGroup
            var oRequestParameter = { InputParameter: { "Mode": "GeneralExpense" }, CurrentEmployee: getToken(CONFIG.USER), Requestor: $scope.requesterData }

            //get Expense Type Tag
            var URL = CONFIG.SERVER + 'HRTR/GetTransactionStatusGroup/';
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                $scope.GetTransactionStatus = response.data;
                //console.log('exptag.', $scope.exptag);

            }, function errorCallback(response) {
                // Error
                console.log('error GeneralExpenseContentController.', response);
            });


            $scope.getGeneralExpenseMasterData = function () {

                var oRequestParameter = { InputParameter: { "YEARKEY": "GENERALEXPENSE" }, CurrentEmployee: $scope.employeeData, Requestor: $scope.requesterData };//CommentBy: Ratchatawan W. (21 Dec 2016), Requestor: $scope.employeeData, Creator: $scope.employeeData };
                var URL = CONFIG.SERVER + 'HRTR/GetGeneralExpenseMasterData';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    //console.log('GetGeneralExpenseMasterData', response.data);

                    if (response.data != null) {
                        // Status
                        $scope.expenseStatus = response.data.Status;

                        // CostCenter
                        var oAll = { "CostCenterCode": "", "LongDesc": "ALL :" };
                        var oCost = response.data.CostCenter;
                        oCost.unshift(oAll);
                        $scope.objCostcenter = oCost;

                        // IO
                        var oAll = { "OrderID": "", "Description": "ALL :" };
                        var oIO = response.data.IO;
                        oIO.unshift(oAll);
                        $scope.objIO = oIO;

                        // Year
                        $scope.expenseYear = response.data.Year;
                        if ($scope.expenseYear.length > 0) {
                            $scope.formData.year = new Date().getFullYear().toString();
                        }

                        // Month
                        $scope.expenseMonth = response.data.Month;
                        if ($scope.expenseMonth.length > 0) {
                            $scope.formData.month = ((new Date()).getMonth() + 1).toString();
                        }

                        if (angular.isDefined($scope.content.ContentParam) && $scope.content.ContentParam != "") {
                            var array = $scope.content.ContentParam.split('&&');
                            $scope.formData.isAdvancedSearch = array[0] === "true";
                            if (!$scope.formData.isAdvancedSearch) {
                                $scope.formData.year = array[2];
                                $scope.formData.month = array[1];
                                $scope.formData.IsSearchClick = array[3] === "true";
                            }
                            else {
                                $scope.formData.beginDate = array[1];
                                $scope.formData.endDate = array[2];
                                $scope.formData.costCenter = array[3];
                                $scope.formData.io = array[4];
                                $scope.formData.status = array[5];
                                $scope.formData.IsSearchClick = array[6] === "true";
                            }
                        }

                        $scope.settings.GE_ENABLE_TRAVEL_REF = response.data.GE_ENABLE_TRAVEL_REF;
                        $scope.settings.ENABLEPETTYCASH = response.data.ENABLEPETTYCASH;
                        
                    }

                    $scope.searchGeneralExpense();

                }, function errorCallback(response) {
                    // Error
                    console.log('error GetGeneralExpenseMasterData.', response);

                });
            };
            $scope.getGeneralExpenseMasterData();

            $scope.getCostCenterForLookup = function () {
                var URL = CONFIG.SERVER + 'HRTR/GetCostCenterForLookup';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: $scope.requesterData };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.objCostcenterLookup = response.data;
                    //console.log('GetCostCenterForLookup.', $scope.objCostcenterLookup);
                }, function errorCallback(response) {
                    console.log('error GeneralExpenseContentController GetCostCenterForLookup.', response);
                });
            };
            $scope.getCostCenterForLookup();



            $scope.getIOForLookup = function () {
                var URL = CONFIG.SERVER + 'HRTR/GetInternalOrderObjForLookup';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: $scope.requesterData };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.objIOLookupObj = response.data;
                    //console.log('GetInternalOrderObjForLookup.', $scope.objIOLookupObj);
                }, function errorCallback(response) {
                    console.log('error GeneralExpenseContentController GetInternalOrderObjForLookup.', response);
                });
            };
            $scope.getIOForLookup();

            //Add by Nipon Supap 04-12-2017 Support-003 เปลี่ยนสีตอนกด พิมพ์
            $scope.IsChangeColor = false;
            $scope.GetCoverSheetLogPrint = function (oRequestNo, oIsLog) {
                var URL = CONFIG.SERVER + 'HRTR/GetCoverSheetLogPrint';
                var oRequestParameter = { InputParameter: { "IsLog": oIsLog, "RequestNo": oRequestNo }, CurrentEmployee: $scope.employeeData, Requestor: $scope.requesterData }
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    if (response.data.length > 0 && response.data[0] != null)
                        $scope.IsChangeColor = response.data[0].Column1;
                    else
                        $scope.IsChangeColor = false;
                }, function errorCallback(response) {
                    // Error
                    console.log('error TravelExpense SearchCreditCardByEmployeeID', response);

                });
            };

            $scope.getGeneralExpenseFilter = function () {
                return $filter('filter')($scope.expenses, $scope.searchText);
            };
            $scope.getGeneralExpenseSlice = function () {
                var expenseList = $scope.getGeneralExpenseFilter();
                if (!expenseList) return null;
                return expenseList.slice((($scope.pagin.currentPage - 1) * $scope.pagin.itemPerPage), (($scope.pagin.currentPage) * $scope.pagin.itemPerPage));
            };
            $scope.updatePages = function () {
                var pages = [];
                if (!$scope.expenses) return [];
                $scope.pagin.numPage = Math.ceil($scope.getGeneralExpenseFilter().length / $scope.pagin.itemPerPage);
                for (var i = 0; i < $scope.pagin.numPage; i++) {
                    pages.push((i + 1));
                }
                $scope.pagin.currentPage = 1;
                $scope.pagin.pages = pages;
            }
            $scope.movePage = function (step) {
                newPage = $scope.pagin.currentPage + step;
                if (newPage < 1 || newPage > $scope.pagin.numPage) return;
                $scope.changePage(newPage);
            }
            $scope.changePage = function (page) {
                $scope.pagin.currentPage = page;
            }
            $scope.$watch('searchText', function (newValue, oldValue) {
                if (oldValue != newValue) {
                    $scope.updatePages();
                    $scope.pagin.currentPage = 1;
                }
            }, true);
        }]);
})();