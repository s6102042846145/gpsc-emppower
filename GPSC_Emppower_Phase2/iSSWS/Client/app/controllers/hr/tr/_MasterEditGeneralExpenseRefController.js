﻿(function () {
    angular.module('ESSMobile')
        .controller('_MasterEditGeneralExpenseRefController', ['$scope', '$http', 'CONFIG', function ($scope, $http, CONFIG) {

            $scope.employeeData = getToken(CONFIG.USER);

            $scope.settings = {
                ProjectCodeMode: '',
                EnableOverideCostCenter: false,
                EnableOverideIO: false,
                ExpenseTypeGroupInfo: {
                    prefix: 'GE_RefTravel',
                    travelTypeID: 0,
                    isDomestic: 'D'
                },
                ReceiptToleranceAllow: 0,
                EnableProjectCost: false,
                Master: []
            };

           

            /*==============================
                 Initial Scope and variable
             ===============================*/

            /*========================
                Initial Function
            ==========================*/
            //Get all master data
            $scope.GetMasterdata = function () {
                var URL = CONFIG.SERVER + 'HRTR/GetMasterData';
                var oRequestParameter = { InputParameter: { "CreateDate": $scope.document.CreatedDate }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                return $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.settings.Master.CostCenterList = response.data.CostCenterList;
                    $scope.settings.Master.ProjectList = response.data.ProjectList;
                    $scope.settings.Master.OrgUnitList = response.data.OrganizationList;
                    $scope.settings.Master.IOList = response.data.InternalOrderList;
                    $scope.settings.Master.CostcenterDistributionList = response.data.CostCenterDistributionList;
                    console.debug(response);
                    return response;
                },
              function errorCallback(response) {
                  console.error(response);
                  return response;
              });
            };


        }]);
})();