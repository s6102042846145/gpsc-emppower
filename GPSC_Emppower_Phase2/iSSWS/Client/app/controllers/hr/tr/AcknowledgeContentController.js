﻿(function () {
    angular.module('ESSMobile')
        .controller('AcknowledgeContentController', ['$rootScope', '$route', '$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', 'DTOptionsBuilder', 'DTColumnDefBuilder', '$mdDialog', '$mdMenu', function ($rootScope, $route,$scope, $http, $routeParams, $location, $filter, CONFIG, DTOptionsBuilder, DTColumnDefBuilder, $mdDialog, $mdMenu) {
            $scope.DateNow = new Date();
            $scope.TravelExpenseDetail = [{}];
            $scope.content.isShowHeader = false;

            $scope.formData = {
                IsAdvancedSearch: '', SelYear: '', SelMonth: '', SelRequestType: '', SelDocumentNo: '', SelBeginDate: $filter('date')($scope.DateNow, 'yyyy-MM-dd'), SelEndDate: $filter('date')($scope.DateNow, 'yyyy-MM-dd'), SelTravelType: '', SelCountry: ''
                , EnablePettyCash: ''
            };
            $scope.Textcategory = 'TRAVELEXPENSE';
            $scope.HeaderText = $scope.Text['APPLICATION'].ACCOUNT_ACKNOWLEDGE_PETTYCASH;
            $scope.content.Header = $scope.HeaderText;
            //Guild Check
            $scope.checkAllGuild = false;
            $scope.selectedGuild = [];
            $scope.selectedRequestNo = [];
            $scope.formData.SelDocumentNo = "";

            $scope.column_header = [
                '', 'RequestNo', 'Guild Action', 'Guild Date','Comment', 'Custodian Action', 'Custodian Action Date', 'Custodian Remark'];
            $scope.column_header_history = [
                'RequestNo', 'Guild Action', 'Guild Date', 'Custodian Action', 'Custodian Action Date', 'Custodian Remark', 'Acknowledge Date'];

            $scope.InitialConfig = function () {
                //Load Configuratioin for Search Data
                var URL = CONFIG.SERVER + 'HRTR/GetTravelConfigurationForSearch';
                var oRequestParameter = { InputParameter: { "YEARKEY": "TRAVELEXPENSE" }, CurrentEmployee: getToken(CONFIG.USER), Requestor: $scope.requesterData }
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.expenseStatus = response.data;
                    console.log('Response TravelExpende', response.data);
                    $scope.SearchYear = response.data['YEAR'];
                    $scope.SearchRequestType = response.data['REQUESTTYPE'];

                    // bind default value 
                    if ($scope.SearchYear.length > 0) {
                        $scope.formData.SelYear = new Date().getFullYear().toString();
                    }
                    if ($scope.SearchRequestType.length > 0) {
                        $scope.formData.SelRequestType = angular.copy($scope.SearchRequestType[0]);
                    }

                    if (angular.isDefined($scope.content.ContentParam) && $scope.content.ContentParam != "") {
                        var array = $scope.content.ContentParam.split('&&');
                        $scope.formData.IsAdvancedSearch = array[0] === "true";
                        $scope.formData.SelYear = array[2];
                        $scope.formData.IsSearchClick = array[4] === "true";
                        if ($scope.formData.IsAdvancedSearch) {
                            $scope.formData.SelDocumentNo = array[4];
                            $scope.formData.SelTravelType.TravelTypeID = array[7];
                            $scope.formData.SelCountry.CountryCode = array[8];
                            $scope.formData.IsSearchClick = array[9] === "true";
                        }
                    }

                    $scope.existsGuild(0, $scope.selectedGuild);
                    $scope.toggleGuild(0, $scope.selectedGuild);
                    $scope.SearchGuild();

                }, function errorCallback(response) {
                    // Error
                    console.log('error TravelExpenseContentController InitialConfig.', response);
                });
            }

            //Search Guild for Accounting
            
            $scope.SearchGuild = function (iStatus) {
                $scope.oStatus = '';
                if (iStatus == "99")
                    $scope.oStatus = "99";
                else {
                    if ($scope.selectedGuild.length > 0) {
                        for (i = 0; i <= $scope.selectedGuild.length - 1;) {
                            $scope.oStatus += $scope.selectedGuild[i] + ",";
                            i++;
                        }
                    }
                }

                var URL = CONFIG.SERVER + 'HRTR/SearchGuild';
                var oRequestParameter = { InputParameter: { "RequestNo": $scope.formData.SelDocumentNo, "Status": $scope.oStatus, "Year": $scope.formData.SelYear, "isProgress": $scope.isProgress }, CurrentEmployee: getToken(CONFIG.USER), Requestor: $scope.requesterData }
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.lstGuild = response.data;
                }, function errorCallback(response) {
                    // Error
                    console.log('error SearchGuild.', response);
                });
            }

            $scope.openRequest = function (id, keyMaster, isOwner, isDataOwner, requestType, BoxDescription) {
                $location.path('/frmViewRequest/' + id + '/' + keyMaster + '/' + isOwner + '/' + isDataOwner + '/' + encodeURIComponent(BoxDescription));
            };

            $scope.Acknowledge = function ()
            {
                $scope.xmlRequestNo = "";
                if (oSelectRequestNo.length > 0) {
                    for (i = 0; i <= oSelectRequestNo.length - 1;) {
                        $scope.xmlRequestNo += "<REQUESTNO><value>" + oSelectRequestNo[i].RequestNo + "</value><ActionDate>" + oSelectRequestNo[i].ActionDate + "</ActionDate></REQUESTNO>";
                     
                        //for (j = 0; j <= $scope.lstGuild.length - 1;)
                        //{
                        //    if($scope.selectedRequestNo[i] == $scope.lstGuild[j].RequestNo)
                        //    {
                        //        $scope.xmlRequestNo += "<REQUESTNO><value>" + $scope.selectedRequestNo[i] + "</value><ActionDate>" + $scope.lstGuild[i].ActionDate + "</ActionDate></REQUESTNO>";
                        //        break;
                        //    }
                        //    j++;
                        //}
                        i++;
                    }

                    var URL = CONFIG.SERVER + 'HRTR/Acknowledge';
                    var oRequestParameter = { InputParameter: { "RequestNo": $scope.xmlRequestNo }, CurrentEmployee: getToken(CONFIG.USER) }
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        $scope.SearchGuild(0);
                        // Reload Menu
                        var args = {};
                        $rootScope.$broadcast('onReloadMenu', args);

                        $route.reload();
                    }, function errorCallback(response) {
                        // Error
                        console.log('error Acknowledge.', response);
                    });
                }
                else
                {
                    $mdDialog.show(
                            $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title('Warning')
                            .textContent('Please select Request No.')
                            .ok('OK')
                        );
                }
            }
            //Reload all request and no in box list
            $scope.onReload = function () {


                console.warn('reload');
                var deferred = $q.defer();
                setTimeout(function () {
                    deferred.resolve(true);
                }, 1000);
                return deferred.promise;
            };
            $scope.Reload = function () {

                // Reload Menu
                var args = {};
                $rootScope.$broadcast('onReloadMenu', args);
                $route.reload();
            };

            //Set Page Tab
            $scope.currentNavItem = "Inprogress";
            $scope.isProgress = true;
            $scope.goto = function (page) {
                $scope.ClearData();
                if (page == "page1") {
                    $scope.isProgress = true;
                    $scope.SearchGuild(0);
                }
                else if (page == "page2") {
                    $scope.isProgress = false;
                    $scope.SearchGuild(99);
                }
                console.log("Goto " + page);
            }
            $scope.ClearData = function()
            {
                $scope.oStatus = "";
                $scope.xmlRequestNo = "";
                $scope.formData.SelDocumentNo = "";

                $scope.selectedGuild = [];
                $scope.existsGuild(0, $scope.selectedGuild);
                $scope.toggleGuild(0, $scope.selectedGuild);
            }

            //Guild Check
            $scope.toggleGuild = function (item) {
                var idx = $scope.selectedGuild.indexOf(item);
                if (idx > -1) {
                    $scope.selectedGuild.splice(idx, 1);
                }
                else {
                    $scope.selectedGuild.push(item);
                }
            };
            $scope.existsGuild = function (item) {
                return $scope.selectedGuild.indexOf(item) > -1;
            };

            //Selected RequestNo
            var oSelectRequestNo = [];
            var spuctRequestNo = true;
            $scope.toggleRequestNo = function (item,actionDate) {
                //var idx = $scope.selectedRequestNo.indexOf(item);
                //if (idx > -1) {
                //    $scope.selectedRequestNo.splice(idx, 1);
                //}
                //else {
                //    $scope.selectedRequestNo.push(item);
                //}
                if(oSelectRequestNo.length > 0)
                {
                    $scope.checkAllGuild = false;
                    for (i = 0; i <= oSelectRequestNo.length -1;)
                    {
                        spuctRequestNo = true;
                        if (oSelectRequestNo[i].RequestNo == item && oSelectRequestNo[i].ActionDate == actionDate)
                        {
                            oSelectRequestNo[i].RequestNo = "";
                            oSelectRequestNo[i].ActionDate = "";
                            spuctRequestNo = false;
                            break;
                        }
                        i++;
                    }
                    if(spuctRequestNo)
                        oSelectRequestNo.push({ RequestNo: item, ActionDate: actionDate });
                }
                else
                {
                    oSelectRequestNo.push({ RequestNo: item, ActionDate: actionDate });
                }
            };
            $scope.existsRequestNo = function (item) {
                return $scope.selectedRequestNo.indexOf(item) > -1;
            };

            //Selected All RequestNo
            $scope.selectedAll = false;
            $scope.checkAllRequest = function(item)
            {
                if (item == true)
                {
                    $scope.checkAllGuild = true;
                    oSelectRequestNo = [];
                    for (i = 0; i <= $scope.lstGuild.length - 1;) {
                        if ($scope.lstGuild[i].Custodian_ActionCode != '-') {
                            //$scope.toggleAllRequestNo($scope.lstGuild[i].RequestNo, $scope.lstGuild[i].ActionDate, $scope.selectedRequestNo);
                            $scope.toggleAllRequestNo($scope.lstGuild[i].RequestNo, $scope.lstGuild[i].ActionDate);
                        }
                        i++;
                    }
                    $scope.selectedAll = true;
                }
                else
                    $scope.checkAllGuild = false;
            }
            $scope.toggleAllRequestNo = function (item, actionDate) {
                //var idx = list.indexOf(item);
                //if (idx > -1) {
                //    list.splice(idx, 1);
                //}
                //else {
                //    list.push(item);
                //}
                oSelectRequestNo.push({ RequestNo: item, ActionDate: actionDate });
            };
            $scope.existsAllRequestNo = function (item, list) {
                return list.indexOf(item) > -1;
            };

            $scope.ActionSubstring = function(item)
            {
                return item.substring(0, 4);
            }

        }])
        .directive('expand', function () {
            return {
                restrict: 'A',
                controller: ['$scope', function ($scope) {
                    $scope.$on('onExpandAll', function (event, args) {
                        $scope.expanded = args.expanded;
                    });
                }]
            };
        });
})();