﻿(function () {
    angular.module('ESSMobile')
        .controller('TravelExpenseExchangeRateEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG' , function ($scope, $http, $routeParams, $location, $filter, CONFIG) {
            //ExchangeRate
            $scope.Textcategory = 'EXPENSE';

            var dateToday = new Date();
            dateToday.setDate(dateToday.getDate() - 1);

            var oExchangeTypeID = 0;
            $scope.addRow = function ()
            {
                oExchangeTypeID = oExchangeTypeID + 1;
                $scope.data.ExchangeRateCaptures.push({
                    "RequestNo": $scope.document.RequestNo,
                    "ExchangeRateType": $scope.exchangerateUSD.ExRateType,
                    "FromCurrency": $scope.exchangerateUSD.FromCurrency,
                    "ToCurrency": $scope.exchangerateUSD.ToCurrency,
                    "EffectiveDate": $filter('date')(dateToday, 'yyyy-MM-ddT00:00:00'),
                    "ExchangeTypeID": oExchangeTypeID,
                    "ExchangeRate": $scope.exchangerateUSD.ExchangeRate,
                    "ExchangeRateShow": ($scope.exchangerateUSD.ExchangeRate <= 0 ? 'No Rate' : $scope.exchangerateUSD.ExchangeRate),
                    "RatioFromCurrencyUnit": $scope.exchangerateUSD.FromCurrencyRatio,
                    "RatioToCurrencyUnit": $scope.exchangerateUSD.ToCurrencyRatio
                });
                console.log('addRow', $scope.data.ExchangeRateCaptures);
            };

            $scope.removeRow = function (index) {
                $scope.data.ExchangeRateCaptures.splice(index, 1);
                oExchangeTypeID = oExchangeTypeID - 1;
                console.log('removeRow', $scope.data.ExchangeRateCaptures);

            };

            $scope.ChangeDateFormat = function (date, row) {
                row.EffectiveDate = $filter('date')(date, 'yyyy-MM-ddT00:00:00');
            }

            $scope.SetSelectedBeginDate = function (selectedDate, row, index) {
                row.EffectiveDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                $scope.data.ExchangeRateCaptures[index].EffectiveDate = row.EffectiveDate;

                var URL = CONFIG.SERVER + 'HRTR/LookupExchangeRate/';
                var oRequestParameter = { InputParameter: { "FromCurrency": row.FromCurrency, "EffectiveDate": row.EffectiveDate }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    if (response.data == null) {
                        row.ExchangeRate = 0.00;
                        row.ExchangeRateShow = 'No Rate';
                    }
                    else {
                        row.ExchangeRate = response.data.Rate;
                        $scope.CalculateTravelGroupRequest();
                    }
                   
                   // console.log('ExchangeRateCaptures.', $scope.data.ExchangeRateCaptures);
                }, function errorCallback(response) {
                    console.log('error TravelExpenseExchangeRateEditorController.', response);
                });
            };

            $scope.ChangeCurrency = function (row, index) {
                $scope.data.ExchangeRateCaptures[index].FromCurrency = row.FromCurrency;
                var URL = CONFIG.SERVER + 'HRTR/LookupExchangeRate/';
                var oRequestParameter = { InputParameter: { "FromCurrency": row.FromCurrency, "EffectiveDate": row.EffectiveDate }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    if (response.data == null) {
                        row.ExchangeRate = 0.00;
                        row.ExchangeRateShow = 'No Rate';
                    }
                    else {
                        row.ExchangeRate = response.data.ExchangeRate.Rate;
                    }
                    console.log('ChangeCurrency', $scope.data.ExchangeRateCaptures);
                },
                function errorCallback(response)
                {
                    console.log('error TravelExpenseExchangeRateEditorController.', response);
                });
            };

            $scope.InitialConfig = function()
            {
                //console.log('InitialConfig ExchangeRate');
                var URL = CONFIG.SERVER + 'HRTR/GetAllCurrency/';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.currency = response.data;
                },
                function errorCallback(response)
                {
                    console.log('error TravelExpenseExchangeRateEditorController[InitialConfig]', response);
                });

            }

            $scope.GetExchangeRateUSD = function () {
                var URL = CONFIG.SERVER + 'HRTR/LookupExchangeRate/';
                var oRequestParameter = { InputParameter: { "FromCurrency": "USD", "EffectiveDate": $filter('date')(dateToday, 'yyyy-MM-ddT00:00:00') }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.exchangerateUSD = response.data;
                    console.log('GetExchangeRateUSD', $scope.data.ExchangeRateCaptures);
                    if (!angular.isDefined($scope.data.ExchangeRateCaptures) || $scope.data.ExchangeRateCaptures[0].FromCurrency == '')
                    {
                        $scope.data.ExchangeRateCaptures = [];
                        oExchangeTypeID = oExchangeTypeID + 1;
                        $scope.data.ExchangeRateCaptures.push({
                            "RequestNo": $scope.document.RequestNo,
                            "ExchangeRateType": $scope.exchangerateUSD.ExRateType,
                            "FromCurrency": $scope.exchangerateUSD.FromCurrency,
                            "ToCurrency": $scope.exchangerateUSD.ToCurrency,
                            "EffectiveDate": $filter('date')(dateToday, 'yyyy-MM-ddT00:00:00'),
                            "ExchangeTypeID": oExchangeTypeID,
                            "ExchangeRate": $scope.exchangerateUSD.ExchangeRate,
                            "ExchangeRateShow": ($scope.exchangerateUSD.ExchangeRate <=0 ?'No Rate':$scope.exchangerateUSD.ExchangeRate ),
                            "RatioFromCurrencyUnit": $scope.exchangerateUSD.FromCurrencyRatio,
                            "RatioToCurrencyUnit": $scope.exchangerateUSD.ToCurrencyRatio
                        });
                        console.log('SetDefault', $scope.data.ExchangeRateCaptures);
                    }
                    else
                    {
                        console.log('Retrive From Paren', $scope.data.ExchangeRateCaptures);
                        $scope.data.ExchangeRateCaptures[0].ExchangeRateShow = ($scope.data.ExchangeRateCaptures[0].ExchangeRate <= 0 ? 'No Rate' : $scope.data.ExchangeRateCaptures[0].ExchangeRate);
                    }
                },
                function errorCallback(response) {
                    console.log('error TravelExpenseExchangeRateEditorController.', response);

                });
            }

        }]);

})();



