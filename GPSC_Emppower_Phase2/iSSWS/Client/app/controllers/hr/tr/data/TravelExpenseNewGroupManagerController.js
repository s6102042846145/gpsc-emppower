﻿/// <reference path="TravelExpenseNewGroupManagerController.js" />
(function () {
    angular.module('ESSMobile')
        .controller('TravelExpenseNewGroupManagerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {
            //Traveller excep this

            $scope.$watch('TravelPersonalEdit.NewGroupManager', function () {
                if (angular.isDefined($scope.TravelPersonalEdit.NewGroupManager) && $scope.TravelPersonalEdit.NewGroupManager != '')
                {
                    $scope.GetGroupManagerData($scope.TravelPersonalEdit.NewGroupManager);
                }
            });
        
            $scope.onSelectedNewGroupManager = function (EmployeeID) {
                //$scope.GetGroupManagerData(EmployeeID);
                //$scope.TravelPersonalEdit.NewGroupManager = EmployeeID;
            }

            $scope.GetGroupManagerData = function (EmployeeID) {
                var oRequestParameter = {
                    InputParameter: { "EmployeeID": EmployeeID }
                                                , CurrentEmployee: getToken(CONFIG.USER),
                    Creator: $scope.document.Creator, Requestor: $scope.document.Requestor 
                };
                var URL = CONFIG.SERVER + 'HRTR/GetGroupManagerData';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.GroupManagerData = response.data;
                }, function errorCallback(response) {
                    // Error
                    console.log('error TravelExpenseNewGroupManagerController GetGroupManagerData.', response);
                });
            }

            function findWithAttr(array, attr, value) {
                for (var i = 0; i < array.length; i += 1) {
                    if (array[i][attr] === value) {
                        return i;
                    }
                }
            }
        }]);
})();