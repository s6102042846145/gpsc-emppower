﻿(function () {
    angular.module('ESSMobile')
        .controller('TravelExpenseBorrowGroupEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {

            $scope.Textcategory = 'EXPENSE';
            $scope.TOTALEXPENSE = 0;
            $scope.TOTALBORROW = 0;

            
            var totalExpense = 0;
            var totalBorrow = 0;
                for (var i = 0; i < $scope.groupestimatelist.length; i++) {
                    //var totalExpenseTemp = $scope.groupestimatelist[i].EXPENSE;
                    //var totalBorrowTemp = $scope.groupestimatelist[i].BORROW;
                    totalExpense += $scope.groupestimatelist[i].EXPENSE;
                    totalBorrow += $scope.groupestimatelist[i].BORROW;
                }
                
                $scope.TOTALEXPENSE = totalExpense;
                $scope.TOTALBORROW = totalBorrow;

        }]);

})();



