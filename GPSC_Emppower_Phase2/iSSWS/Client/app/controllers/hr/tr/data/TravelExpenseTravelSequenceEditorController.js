﻿(function () {
    angular.module('ESSMobile')
        .controller('TravelExpenseTravelSequenceEditorController', ['$scope', '$http', '$routeParams', '$location'
            , '$window', '$filter', 'CONFIG', '$q', '$timeout', '$mdDialog', '$rootScope'
            , function ($scope, $http, $routeParams, $location, $window, $filter, CONFIG, $q, $timeout, $mdDialog, $rootScope) {
                //TravelSchedulePlaces
                $scope.Textcategory = 'EXPENSE';
                $scope.Textcategory2 = 'SYSTEM';

                var oSchedulePlaceID = 0;
                var Travelkey = -1;
                var CurrentTravelSchedulePlacesTemp = null;
                var CurrentTravelSchedulePlacesTempCityText = "";
                $scope.provinceSelect = true; //$scope.isDebug = true;
                $scope.districtSelect = false;

                $scope.districtVisible = true;

                $scope.data.ProvinceList = [];
                $scope.data.DistrictList = [];
                $scope.showDialog_traveSequest = false;


                $scope.checkDistrictVisible = function () {
                    //Nun Add Visible Column District 
                    if (angular.isDefined($scope.data.TravelSchedulePlaces) && $scope.data.TravelSchedulePlaces.length > 0) {
                        if (!$scope.data.TravelSchedulePlaces.PropHasValue('DistrictCode')) {
                            $scope.districtVisible = false;
                        } else { $scope.districtVisible = true; }
                    }
                    else {
                        $scope.districtVisible = false;
                    }
                    //
                }
                $scope.checkDistrictVisible();



                //Create temp object when add new employee
                $scope.CreateTempObject = function () {
                    var newTravelSchedulePlaces = $scope.data.TravelSchedulePlaces;
                    $scope.CurrentTravelSchedulePlaces = angular.copy(newTravelSchedulePlaces);
                }
                //Create temp object when edit new employee
                $scope.BindTempObject = function (index) {
                    $scope.CurrentTravelSchedulePlaces = angular.copy($scope.data.TravelSchedulePlaces[index]);

                }
                //Remove Travel Seq
                $scope.removeRow = function (index) {
                    var isBudget = $scope.IsBudgets(false);

                    if (!isBudget) {

                        doRemoveRow(index);

                    } else {

                        var confirm = $mdDialog.confirm()
                            .title($scope.Text['SYSTEM']['WARNING'])
                            .textContent($scope.Text['SYSTEM']['CANTREOVEIFBUDGETS'])
                            .ok($scope.Text['SYSTEM']['BUTTON_YES'])
                            .cancel($scope.Text['SYSTEM']['BUTTON_NO']);
                        $mdDialog.show(confirm).then(function () {
                            doRemoveRow(index);
                        }, function () {
                        });

                    }
                };
                var doRemoveRow = function (index) {
                    $scope.data.TravelSchedulePlaces.splice(index, 1);

                    //Nun Add Visible Column District 
                    if (!$scope.data.TravelSchedulePlaces.PropHasValue('DistrictCode')) {
                        $scope.districtVisible = false;
                    } else { $scope.districtVisible = true; }
                    //

                    oSchedulePlaceID = oSchedulePlaceID - 1;
                    $scope.CalculateTravelGroupRequest(true);
                    $rootScope.$broadcast('CheckExpenseType_FN');
                }
                $scope.AddTravelSeq = function () {
                    //$scope.beginFormWizard();
                    $scope.CreateTempObject();
                    $scope.EmptyForm();

                    oSchedulePlaceID = -1;
                    //$scope.data.TravelSeqEdit = true;
                    Travelkey = -1;
                    $scope.showDialog_traveSequest = true;


                    // set default
                    var filteredItem = $filter('filter')($scope.data.TravelPlaceList, { LocationCode: 'TH' });
                    $scope.autocomplete_location.selectedItem = filteredItem[0];
                    $scope.CurrentTravelSchedulePlaces.LocationName = filteredItem[0].LocationName;
                    $scope.CurrentTravelSchedulePlaces.LocationCode = filteredItem[0].LocationCode;
                    $scope.searchText = filteredItem[0].LocationName;
                    $scope.searchText_province = '';

                    $scope.IsEditCountry = false;
                    $scope.provinceSelect = true;
                    $scope.districtSelect = false;

                    $window.scrollTo(0, 0);
                };
                $scope.IsBudgets = function (showDialogMessage) {
                    var flgCheck = false;
                    if (angular.isDefined($scope.data.Travelers) || $scope.data.Travelers) {
                        for (var i = 0; i < $scope.data.Travelers.length; i++) {
                            if (angular.isDefined($scope.data.Travelers[i].PersonalBudgets) && $scope.data.Travelers[i].PersonalBudgets.length > 0) {
                                flgCheck = true;
                                break;
                            }
                        }
                    }
                    if (!flgCheck) {
                        if (angular.isDefined($scope.data.GroupBudgets) && $scope.data.GroupBudgets.length > 0) {
                            flgCheck = true;
                        }
                    }
                    if (flgCheck && showDialogMessage) {
                        $mdDialog.show(
                            $mdDialog.confirm()
                                .clickOutsideToClose(false)
                                .title($scope.Text['SYSTEM']['WARNING'])
                                .textContent($scope.Text['SYSTEM']['CANTREOVEIFBUDGETS'])
                                .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                        );
                    }
                    return flgCheck;
                }
                $scope.EmptyForm = function () {
                    $scope.CurrentTravelSchedulePlaces.RequestNo = "";
                    $scope.CurrentTravelSchedulePlaces.SchedulePlaceID = -1;
                    if ($scope.data.TravelSchedulePlaces.length > 0) {
                        $scope.CurrentTravelSchedulePlaces.BeginDate = $scope.data.TravelSchedulePlaces[$scope.data.TravelSchedulePlaces.length - 1].EndDate;
                        $scope.CurrentTravelSchedulePlaces.EndDate = $scope.data.TravelSchedulePlaces[$scope.data.TravelSchedulePlaces.length - 1].EndDate;
                    }
                    else {
                        var dateToday = new Date();
                        $scope.CurrentTravelSchedulePlaces.BeginDate = $filter('date')(dateToday, 'yyyy-MM-ddT00:00:00');
                        $scope.CurrentTravelSchedulePlaces.EndDate = $filter('date')(dateToday, 'yyyy-MM-ddT00:00:00');
                    }
                    $scope.autocomplete_location.selectedItem = null;
                    //$scope.autocomplete_location_province.selectedItem = null;
                    //$scope.autocomplete_location_district.selectedItem = null;
                    $scope.searchText = '';
                    $scope.searchText_province = '';
                    $scope.searchText_district = '';
                    $scope.CurrentTravelSchedulePlaces.PlaceName = "";
                    $scope.CurrentTravelSchedulePlaces.DistrictCode = "";
                    $scope.CurrentTravelSchedulePlaces.DistrictName = "";
                    $scope.CurrentTravelSchedulePlaces.RegionCode = "";
                    $scope.CurrentTravelSchedulePlaces.RegionName = "";
                    $scope.CurrentTravelSchedulePlaces.ProvinceCode = "";
                    $scope.CurrentTravelSchedulePlaces.ProvinceName = "";
                    $scope.CurrentTravelSchedulePlaces.LocationCode = "";
                    $scope.CurrentTravelSchedulePlaces.LocationName = "";
                    $scope.CurrentTravelSchedulePlaces.CountryCode = "";
                    $scope.CurrentTravelSchedulePlaces.CountryName = "";
                    $scope.CurrentTravelSchedulePlaces.IsWarning = false;
                    $scope.HasError = false;
                }
                $scope.EditTravelSeq = function (row, index) {
                    //$scope.data.TravelSeqEdit = true;
                    $scope.showDialog_traveSequest = true;
                    $scope.HasError = false;
                    Travelkey = index;
                    // $scope.beginFormWizard();
                    $scope.BindTempObject(index);
                    $scope.provinceSelect = false;
                    $scope.districtSelect = false;
                    $scope.CurrentTravelSchedulePlaces.CityText = angular.copy(row.CityName);
                    //$scope.searchText = angular.copy(row.LocationName);
                    var filteredItemCountry = $filter('filter')($scope.data.TravelPlaceList, { LocationCode: row.LocationCode });
                    var usr = getToken(CONFIG.USER);

                    var filteredItemCountryLNG = filteredItemCountry.filter(function (m) { return m.LocationName.indexOf(usr.Language) >= 0; });
                    $scope.autocomplete_location.selectedItem = filteredItemCountryLNG[0];

                    //if (filteredItemCountry.length > 1) {
                    //    if (usr.Language == 'TH') {
                    //        var filteredItemCountryLNG = filteredItemCountry.filter(function (m) { return m.LocationName.indexOf(usr.Language) >= 0; });
                    //        $scope.autocomplete_location.selectedItem = filteredItemCountry[1];
                    //    }
                    //} else
                    //    $scope.autocomplete_location.selectedItem = filteredItemCountry[0];
                    //if (row.LocationCode == 'TH') {
                    //    //getProvince();
                    //}

                    $scope.IsEditCountry = $scope.IsBudgets(false);
                };
                $scope.SaveTravelSeq = function () {
                    setDate();
                    //validate
                    var valid = true;
                    if ($scope.CurrentTravelSchedulePlaces.PlaceName == "" || angular.isUndefined($scope.CurrentTravelSchedulePlaces.PlaceName)) valid = false;
                    else if ($scope.CurrentTravelSchedulePlaces.LocationCode == "" || angular.isUndefined($scope.CurrentTravelSchedulePlaces.LocationCode)) valid = false;
                    else if ($scope.CurrentTravelSchedulePlaces.BeginDate == "" || angular.isUndefined($scope.CurrentTravelSchedulePlaces.BeginDate)) valid = false;
                    else if ($scope.CurrentTravelSchedulePlaces.EndDate == "" || angular.isUndefined($scope.CurrentTravelSchedulePlaces.EndDate)) valid = false;
                    if (!valid) {
                        $scope.HasError = true;
                        $scope.ErrorText = "Please/ Insert All fields";
                    }
                    else {
                        $scope.finishFormWizard();
                        var oTravelSeq = {
                            "PlaceName": $scope.CurrentTravelSchedulePlaces.PlaceName,
                            "LocationCode": $scope.CurrentTravelSchedulePlaces.LocationCode,
                            "LocationName": $scope.CurrentTravelSchedulePlaces.LocationName,
                            "RegionCode": $scope.CurrentTravelSchedulePlaces.RegionCode,
                            "RegionName": $scope.CurrentTravelSchedulePlaces.RegionName,
                            "ProvinceCode": $scope.CurrentTravelSchedulePlaces.ProvinceCode,
                            "ProvinceName": $scope.CurrentTravelSchedulePlaces.ProvinceName,
                            "DistrictCode": $scope.CurrentTravelSchedulePlaces.DistrictCode,
                            "DistrictName": $scope.CurrentTravelSchedulePlaces.DistrictName,
                            "CountryCode": $scope.CurrentTravelSchedulePlaces.CountryCode,
                            "CountryName": $scope.CurrentTravelSchedulePlaces.CountryName,
                            "BeginDate": $scope.CurrentTravelSchedulePlaces.BeginDate,
                            "EndDate": $scope.CurrentTravelSchedulePlaces.EndDate,
                            "RequestNo": $scope.document.RequestorNo,
                            "SchedulePlaceID": $scope.CurrentTravelSchedulePlaces.SchedulePlaceID
                        };

                        if ($scope.CurrentTravelSchedulePlaces.SchedulePlaceID == -1) { //Add
                            oSchedulePlaceID = $scope.data.TravelSchedulePlaces.length + 1;
                            oTravelSeq.SchedulePlaceID = oSchedulePlaceID;
                            $scope.data.TravelSchedulePlaces.push(oTravelSeq);
                        }
                        else {
                            var chkItems = $scope.data.TravelSchedulePlaces.filter(function (m) { return m.CountryCode != oTravelSeq.CountryCode });
                            if (chkItems.length > 0) {

                                var confirm = $mdDialog.confirm()
                                    .title($scope.Text['SYSTEM']['WARNING'])
                                    .textContent($scope.Text['SYSTEM']['CANTREOVEIFBUDGETS'])
                                    .ok($scope.Text['SYSTEM']['BUTTON_YES'])
                                    .cancel($scope.Text['SYSTEM']['BUTTON_NO']);
                                $mdDialog.show(confirm).then(function () {

                                    $scope.data.TravelSchedulePlaces[Travelkey] = oTravelSeq;
                                    $rootScope.$broadcast('CheckExpenseType_FN');


                                    $scope.CalculateTravelGroupRequest(true);

                                    //$scope.data.TravelSeqEdit = false;
                                    $scope.showDialog_traveSequest = false;
                                    $scope.document.Additional.TravelSchedulePlaces = $scope.data.TravelSchedulePlaces;

                                    //Nun Add Visible Column District 
                                    if (!$scope.data.TravelSchedulePlaces.PropHasValue('DistrictCode')) {
                                        $scope.districtVisible = false;
                                    } else { $scope.districtVisible = true; }

                                }, function () {
                                });

                            } else {
                                $scope.data.TravelSchedulePlaces[Travelkey] = oTravelSeq;

                                $scope.CalculateTravelGroupRequest(true);

                                //$scope.data.TravelSeqEdit = false;
                                $scope.showDialog_traveSequest = false;
                                $scope.document.Additional.TravelSchedulePlaces = $scope.data.TravelSchedulePlaces;

                                //Nun Add Visible Column District 
                                if (!$scope.data.TravelSchedulePlaces.PropHasValue('DistrictCode')) {
                                    $scope.districtVisible = false;
                                } else { $scope.districtVisible = true; }
                            }
                        }


                    }
                };

                $scope.getTravelTransportationTypeIcon = function () {
                    if (!$scope.settings.Master.TransportationTypeList) return 'fa-car';
                    let vehicle = null;
                    for (let i = 0; i < $scope.settings.Master.TransportationTypeList.length; i++) {
                        if ($scope.settings.Master.TransportationTypeList[i].TransportationTypeCode == $scope.data.Travelers[0].TransportationTypeCode) {
                            vehicle = $scope.settings.Master.TransportationTypeList[i];
                            break;
                        }
                    }
                    //let vehicle = $scope.settings.Master.TransportationTypeList.find(m => m.TransportationTypeCode == $scope.document.Additional.Travelers[0].TransportationTypeCode);
                    if (vehicle.IsPlane == true) {
                        return 'fa-plane-departure';
                    } else {
                        return 'fa-car';
                    }
                }

                $scope.InitialConfig = function () {
                    var URL = CONFIG.SERVER + 'HRTR/TravelSchedulePlaceSelection';
                    var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        //$scope.data.TravelPlaceList = response.data;
                        $scope.data.TravelPlaceList = response.data;
                        //getProvince();
                    },
                        function errorCallback(response) {
                            console.log('error InitialConfig.', response);
                        });


                }

                $scope.setSelectedBeginDate = function (selectedDate) {
                    var d1 = Date.parse(selectedDate);
                    var d2 = Date.parse($scope.CurrentTravelSchedulePlaces.EndDate);

                    $scope.CurrentTravelSchedulePlaces.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                    if (d2 < d1) {
                        $scope.CurrentTravelSchedulePlaces.EndDate = $scope.CurrentTravelSchedulePlaces.BeginDate;
                    }
                };
                $scope.setSelectedEndDate = function (selectedDate) {
                    $scope.CurrentTravelSchedulePlaces.EndDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                };
                $scope.goBackRequest = function () {
                    $scope.finishFormWizard();
                    //$scope.data.TravelSeqEdit = false;
                    $scope.showDialog_traveSequest = false;

                }

                /********************  auto complete country start ******************* */
                $scope.simulateQuery = true;
                $scope.searchText = '';
                $scope.autocomplete_location = {
                    selectedItem: null
                }
                $scope.querySearchItem = function (query) {
                    //if ($scope.IsBudgets()) {
                    //    query = $scope.CurrentTravelSchedulePlaces.LocationName;
                    //}

                    var countTryFirst = '';
                    console.log('Travelkey', Travelkey);
                    if ($scope.data.TravelSchedulePlaces != null && $scope.data.TravelSchedulePlaces.length > 0 && (Travelkey != 0 || $scope.data.TravelSchedulePlaces.length > 1)) {

                        let lc = $scope.data.TravelSchedulePlaces[0].LocationCode.split('|');
                        console.log('lc', lc);
                        countTryFirst = lc[0];
                    }

                    if (!$scope.data.TravelPlaceList) return;
                    var results = query ? $scope.data.TravelPlaceList.filter(createFilterFor(query)) : $scope.data.TravelPlaceList, deferred;
                    if (countTryFirst && countTryFirst != '') {
                        if (countTryFirst == 'TH')
                            results = results.filter(function (m) { return m.LocationCode.indexOf('TH') > -1; });// m => m.LocationCode.indexOf('TH') > -1);
                        else
                            results = results.filter(function (m) { return m.LocationCode.indexOf('TH') < 0; }); //(m => m.LocationCode.indexOf('TH') < 0);
                    }

                    if (results.length == 0) {
                        $scope.autocomplete_location.selectedItem = null;
                        $scope.CurrentTravelSchedulePlaces.LocationName = '';
                        $scope.CurrentTravelSchedulePlaces.LocationCode = '';
                        $scope.searchText = '';
                    }
                    if ($scope.simulateQuery) {
                        deferred = $q.defer();
                        $timeout(function () { deferred.resolve(results); }, Math.random() * 50, false);
                        return deferred.promise;
                    } else {
                        return results;
                    }
                }
                $scope.selectedItemChange = function (item) {
                    if (angular.isDefined(item)) {
                        if ($scope.CurrentTravelSchedulePlaces.LocationCode != item.LocationCode) {
                            $scope.autocomplete_location.selectedItem = item;
                            $scope.CurrentTravelSchedulePlaces.LocationName = item.LocationName;
                            $scope.CurrentTravelSchedulePlaces.LocationCode = item.LocationCode;
                            var LocationName_a = item.LocationName.split("|");
                            var LocationCode_a = item.LocationCode.split("|");
                            $scope.CurrentTravelSchedulePlaces.CountryCode = LocationCode_a[0];
                            $scope.CurrentTravelSchedulePlaces.CountryName = LocationName_a[0];
                            $scope.CurrentTravelSchedulePlaces.RegionCode = LocationCode_a[1];
                            $scope.CurrentTravelSchedulePlaces.RegionName = LocationName_a[1];
                            $scope.CurrentTravelSchedulePlaces.ProvinceCode = LocationCode_a[2];
                            $scope.CurrentTravelSchedulePlaces.ProvinceName = LocationName_a[2];
                            $scope.CurrentTravelSchedulePlaces.DistrictCode = LocationCode_a[3];
                            $scope.CurrentTravelSchedulePlaces.DistrictName = LocationName_a[3];

                        }
                        // ควรจะมี flag บอกจาก server ใน country list ว่ามี province ในระบบหรือไม่ เพิ่อทำการเช็คนะจุดนี้ได้เลย TODO
                    }
                }
                function createFilterFor(query) {
                    if (!angular.isDefined(query)) return false;
                    var lowercaseQuery = angular.lowercase(query);
                    return function filterFn(item) {
                        //console.debug(item);
                        var textquery = angular.lowercase(item.LocationNameToDisplay);
                        return (textquery.indexOf(lowercaseQuery) >= 0);
                    };
                }
                var tempResult;
                $scope.tryToSelect = function (text) {
                    for (var i = 0; i < $scope.data.TravelPlaceList.length; i++) {
                        if ($scope.data.TravelPlaceList[i].LocationNameToDisplay == text) {
                            tempResult = $scope.data.TravelPlaceList[i];
                            break;
                        }
                    }
                    $scope.searchText = '';
                }
                $scope.checkText = function (text) {
                    var result = null;
                    for (var i = 0; i < $scope.data.TravelPlaceList.length; i++) {
                        if ($scope.data.TravelPlaceList[i].LocationNameToDisplay == text) {
                            result = $scope.data.TravelPlaceList[i];
                            break;
                        }
                    }
                    if (result) {
                        $scope.searchText = result.LocationNameToDisplay;
                        $scope.selectedItemChange(result);
                        $scope.autocomplete_location.selectedItem = result;
                    } else if (tempResult) {
                        $scope.searchText = tempResult.LocationNameToDisplay;
                        $scope.selectedItemChange(tempResult);
                        $scope.autocomplete_location.selectedItem = tempResult;
                    }
                }
                /********************  auto complete country end ******************* */



                /*Date control*/
                $scope.startDateEndDate = { startDate: new Date(), endDate: new Date() };
                //function initDate() {
                //    var beginDate = moment($scope.CurrentTravelSchedulePlaces.BeginDate).format('DD/MM/YYYY');
                //    var endDate = moment($scope.CurrentTravelSchedulePlaces.EndDate).format('DD/MM/YYYY');
                //    var backDate = moment($scope.data.BACKDATE).format('DD/MM/YYYY');
                //    $('input[name="datefilter"]').daterangepicker({
                //        autoUpdateInput: true,
                //        "startDate": beginDate,
                //        "endDate": endDate,
                //        "minDate": backDate,
                //        locale: {
                //            cancelLabel: 'Clear',
                //            format: 'DD/MM/YYYY'
                //        }
                //    });
                //    $scope.startDateEndDate = beginDate + ' - ' + endDate;
                //}


                //$('input[name="datefilter"]').on('apply.daterangepicker', function (ev, picker) {
                //    $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
                //});

                //$('input[name="datefilter"]').on('cancel.daterangepicker', function (ev, picker) {
                //    $(this).val('');
                //});

                $scope.changeDate = function () {
                    console.log('changedate');
                    console.log('start', $filter('date')($scope.startDateEndDate.startDate._d, 'yyyy-MM-ddT00:00:00'));
                    console.log('endDate', $filter('date')($scope.startDateEndDate.endDate._d, 'yyyy-MM-ddT00:00:00'));
                    //if ($scope.startDateEndDate) {
                    //    var BeginDateEndate_a = $scope.startDateEndDate.split(" - ");

                    //    $scope.CurrentTravelSchedulePlaces.BeginDate = $filter('date')(moment(BeginDateEndate_a[0], "DD/MM/YYYY").toDate(), 'yyyy-MM-ddT00:00:00');
                    //    $scope.CurrentTravelSchedulePlaces.EndDate = $filter('date')(moment(BeginDateEndate_a[1], "DD/MM/YYYY").toDate(), 'yyyy-MM-ddT00:00:00');

                    //}
                }

                var setDate = function () {
                    if ($scope.startDateEndDate) {

                        $scope.CurrentTravelSchedulePlaces.BeginDate = $filter('date')(convertToDate($scope.startDateEndDate.startDate), 'yyyy-MM-ddT00:00:00');
                        $scope.CurrentTravelSchedulePlaces.EndDate = $filter('date')(convertToDate($scope.startDateEndDate.endDate), 'yyyy-MM-ddT00:00:00');

                    }
                }

            }]);
})();


