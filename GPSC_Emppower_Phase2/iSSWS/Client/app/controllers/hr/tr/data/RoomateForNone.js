﻿(function () {
    angular.module('ESSMobile')
        .controller('RoomateForNoneController', ['$scope', '$http', '$routeParams', '$location', '$filter', '$window', '$timeout', '$q', '$log', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, $window, $timeout, $q, $log, CONFIG, $mdDialog) {
            $scope.Textcategory = 'EXPENSE';
            $scope.loader_ = {
                enable: false
            }
            $scope.DefaultOrgUnitID = $scope.Page == 'GeneralExpense' ? $scope.document.Additional.AlternativeIOOrg : $scope.document.Additional.Travelers[0].OrgUnitID;

            $scope.tmpRoomateForNone = {
                CostCenter: [],
                CostCenterObject: [],
                IsAlternativeCostCenter: false,
                AlternativeIOOrg: [],
                AlternativeIOOrgObject: [],
                IsAlternativeIOOrg: false,
                IO: [],
                IOObject: [],
                UsedAmount: 0
            };
            $scope.employeeData = getToken(CONFIG.USER);
            $scope.listRoomateViewer = null;
            /* Guest */
            $scope.searchTextGuest = '';
            $scope.querySearchGuest = function (query) {
                var results = query ? $scope.employeeDataList.filter(createGuestFilterFor(query)) : $scope.employeeDataList, deferred;
                $scope.simulateQuery = true;//return results;
                if ($scope.simulateQuery) {
                    deferred = $q.defer();
                    deferred.resolve(results);//$timeout(function () { deferred.resolve(results); }, Math.random() * 300, false);
                    return deferred.promise;
                } else {
                    return results;
                }
            };

            function checkDuplicateGuest(guestList) {
                return function (item) {
                    if (!item) return false;
                    return !(guestList.duplicateProp('EmployeeID', item.EmployeeID));
                };
            }

            function createGuestFilterFor(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function (item) {
                    if (!item || !item.EmployeeID || !item.EmployeeName) return false;
                    var source = angular.lowercase(item.EmployeeID + ' : ' + item.EmployeeName);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }

            function selectedItemGuestChange(item, index) {
                if (angular.isDefined($scope.autocompleteGuest.selectedItem)) {
                    $scope.guest.newGuest = $scope.autocompleteGuest.selectedItem;
                } else {
                    $scope.guest.newGuest = null;
                }
            }

            $scope.guest = {
                newGuest: null
            };
            $scope.autocompleteGuest = {
                selectedItem: null,
                searchTextGuest: ''
            }
            var tempResultGuest;
            $scope.tryToSelectGuest = function (text) {
                for (var i = 0; i < $scope.employeeDataList.length; i++) {
                    if (($scope.employeeDataList[i].EmployeeID + " : " + $scope.employeeDataList[i].EmployeeName) == text || $scope.employeeDataList[i].EmployeeName == text) {
                        tempResultGuest = $scope.employeeDataList[i];
                        break;
                    }
                } 
                $scope.autocompleteGuest.searchTextGuest = '';
                $scope.autocompleteGuest.selectedItem = null;
            }
            $scope.checkTextGuest = function (text) {
                var result = null;
                for (var i = 0; i < $scope.employeeDataList.length; i++) {
                    if (($scope.employeeDataList[i].EmployeeID + " : " + $scope.employeeDataList[i].EmployeeName) == text || $scope.employeeDataList[i].EmployeeName == text) {
                        result = $scope.employeeDataList[i];
                        break;
                    }
                }
                if (result) {
                    $scope.autocompleteGuest.searchTextGuest = result.EmployeeID + " : " + result.EmployeeName;
                    selectedItemGuestChange(result);
                    $scope.guest.newGuest = angular.copy(result);
                    //$scope.autocompleteGuest.selectedItem = angular.copy(result);
                } else if (tempResultGuest) {
                    $scope.autocompleteGuest.searchTextGuest = tempResultGuest.EmployeeID + " : " + tempResultGuest.EmployeeName;
                    selectedItemGuestChange(tempResultGuest);
                    $scope.guest.newGuest = angular.copy(tempResultGuest);
                    //$scope.autocompleteGuest.selectedItem = angular.copy(tempResultGuest);
                }
            }
            var clearNewGuest = function () {
                $scope.guest.newGuest = null;
            };
            var createGuestObjFromEmployee = function (employee) {

                if (angular.isUndefined(employee) || employee == null) {
                    return {
                        RequestNo: '',
                        ReceiptID: $scope.GE.receiptItem.ReceiptID,
                        ItemID: $scope.GE.receiptItem.ItemID,
                        GuestID: 0,
                        IsEmployee: false,
                        EmployeeID: '',
                        EmployeeName: '',
                        OrgUnit: '',
                        OrgUnitName: '',
                        Position: '',
                        PositionName: '',
                        CompanyCode: '',
                        CompanyName: ''
                    };
                }

                return {
                    RequestNo: '',
                    ReceiptID: $scope.GE.receiptItem.ReceiptID,
                    ItemID: $scope.GE.receiptItem.ItemID,
                    GuestID: 0,
                    IsEmployee: true,
                    EmployeeID: employee.EmployeeID,
                    EmployeeName: employee.EmployeeName,
                    OrgUnit: employee.OrgUnit,
                    OrgUnitName: employee.OrgUnitName,
                    Position: employee.Position,
                    PositionName: employee.PositionName,
                    CompanyCode: employee.CompanyCode,
                    CompanyName: employee.CompanyName
                };
            };

            $scope.receiptItem_AddGuestEmployee = function () {
                if ($scope.guest.newGuest != null) {
                    var isExist = false;
                    $.each($scope.GE.receiptItem.ExpenseReportReceiptItemGuestList, function (i, guest) {
                        if (guest.IsEmployee && guest.EmployeeID == $scope.guest.newGuest.EmployeeID) {
                            isExist = true;
                            return false;
                        }
                    });
                    if (isExist) {
                        alert('Duplicate employee.');
                    } else {
                        console.log($scope.guest.newGuest);
                        $scope.GE.receiptItem.ExpenseReportReceiptItemGuestList.push(createGuestObjFromEmployee($scope.guest.newGuest));
                        clearNewGuest();
                        selectedItemGuestChange(null, 0);
                        $scope.employeeDataList = angular.copy($scope.masterData.employeeDataList.filter(checkDuplicateGuest($scope.GE.receiptItem.ExpenseReportReceiptItemGuestList)));
                    }
                } else {
                    alert($scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['EMPLOYEEID']);
                }
            };

            $scope.receiptItem_AddGuestOther = function () {
                $scope.GE.receiptItem.ExpenseReportReceiptItemGuestList.push(createGuestObjFromEmployee());
            };

            $scope.receiptItem_DeleteGuestEmployee = function (index) {
                if (confirm($scope.Text['SYSTEM']['CONFIRM_DELETE'])) {
                    var deleteIndex = 0;
                    $.each($scope.GE.receiptItem.ExpenseReportReceiptItemGuestList, function (i, guest) {
                        if (guest.IsEmployee) {
                            if (deleteIndex == index) {
                                deleteIndex = i;
                                return false;
                            }
                            deleteIndex++;
                        }
                    });
                    $scope.GE.receiptItem.ExpenseReportReceiptItemGuestList.splice(deleteIndex, 1);
                    $scope.employeeDataList = angular.copy($scope.masterData.employeeDataList.filter(checkDuplicateGuest($scope.GE.receiptItem.ExpenseReportReceiptItemGuestList)));
                }
            };

            $scope.receiptItem_DeleteGuestOther = function (index) {
                if (confirm($scope.Text['SYSTEM']['CONFIRM_DELETE'])) {
                    var deleteIndex = 0;
                    $.each($scope.GE.receiptItem.ExpenseReportReceiptItemGuestList, function (i, guest) {
                        if (!guest.IsEmployee) {
                            if (deleteIndex == index) {
                                deleteIndex = i;
                                return false;
                            }
                            deleteIndex++;
                        }
                    });
                    $scope.GE.receiptItem.ExpenseReportReceiptItemGuestList.splice(deleteIndex, 1);
                }
            };

            /* !Guest */


            $scope.GetCurrentTraveler = function (currentRoommate, index) {
                var result = $scope.TravelersforAccommodation.filter(createFilterForTravelersforAccommodation(currentRoommate));
                if (result.length > 0) {
                    $scope.SetRoommate(result[0], index)
                }
            }

            function createFilterForTravelersforAccommodation(currentRoommate) {
                var lowercaseQuery = angular.lowercase(currentRoommate.EmployeeID);
                return function filterFn(currentTraveller) {
                    var destination = angular.lowercase(currentTraveller.EmployeeID);
                    return (destination == lowercaseQuery);
                };
            }


            $scope.SetRoommate = function (item, index) {
                if (angular.isDefined($scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList) && $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.length > 0) {
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].RequestNo = $scope.document.RequestNo;
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].ReceiptID = $scope.GE.receipt.ReceiptID;
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].ItemID = index;
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].TravelRequestNo = $scope.document.Additional.TravelRequestNo;
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].EmployeeID = item.EmployeeID;
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].EmployeeName = item.EmployeeName;
                    //Addtion by Lucifer
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].ReceiptID = $scope.GE.receiptItem.ReceiptID;
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].ItemID = $scope.GE.receiptItem.ItemID;
                    //End Add

                    var temp_person;
                    var constcenter;
                    var orgUnit;
                    var io;
                    if ($scope.TravelersforAccommodation_Buffer) {
                        for (var i = 0; i < $scope.TravelersforAccommodation_Buffer.length; i++) {
                            if (item.EmployeeID == $scope.TravelersforAccommodation_Buffer[i].EmployeeID) {
                                temp_person = $scope.TravelersforAccommodation_Buffer[i];
                            }
                        }
                    }
                    for (var i = 0; i < $scope.masterData.objCostcenter.length; i++) {
                        if ($scope.masterData.objCostcenter[i].CostCenterCode == temp_person.CostCenter) {
                            constcenter = $scope.masterData.objCostcenter[i]; break;
                        }
                    }
                    //for (var i = 0; i < $scope.masterData.objOrganization.length; i++) {
                    //    if ($scope.masterData.objOrganization[i].ObjectID == temp_person.OrgUnitID) {
                    //        orgUnit = $scope.masterData.objOrganization[i]; break;
                    //    }
                    //}
                    //Add Modify Nipon Supap 17-07-2017
                    for (var i = 0; i < $scope.masterData.objOrganization.length; i++) {
                        if ($scope.masterData.objOrganization[i].ObjectID == temp_person.AlternativeIOOrg) {
                            orgUnit = $scope.masterData.objOrganization[i]; break;
                        }
                    }
                    var IO_LIST = $scope.settings.Master.IOList;

                    for (var i = 0; i < IO_LIST.length; i++) {
                        if (IO_LIST[i].OrderID == temp_person.IO && temp_person.OrgUnitID == IO_LIST[i].ObjectID) {
                            io = IO_LIST[i]; break;
                        }
                    }
                    if (io) {
                        $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].IOObject = io;
                        $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].IO = io.OrderID;
                    }
                    
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].CostCenterObject = constcenter;
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].AlternativeIOOrgObject = orgUnit;
                    
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].CostCenter = constcenter.CostCenterCode;
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].AlternativeIOOrg = orgUnit.ObjectID;
                    //Addtion by Lucifer
                    //$scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].ReceiptID = $scope.GE.receiptItem.ReceiptID;
                    //$scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].ItemID = $scope.GE.receiptItem.ItemID;
                    //End Add
                    $scope.CalculateAccommodation(item, index);
                }
            }
            $scope.GoToSubMain_AddEmployeeRoommate = function () {
                $scope.CheckPerosonalIsSelectedThenSplice();
                if ($scope.TravelersforAccommodation.length > 0) {
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.push(angular.copy($scope.model.data.ExpenseReportReceiptItemRoommate[0]));
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[$scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.length - 1].EmployeeID = $scope.TravelersforAccommodation[0].EmployeeID;
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[$scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.length - 1].EmployeeName = $scope.TravelersforAccommodation[0].EmployeeName;
                    //Bind Master for dropdownlist
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[$scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.length - 1].IsAlternativeCostCenter = $scope.TravelersforAccommodation[0].IsAlternativeCostCenter;
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[$scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.length - 1].CostCenterObject = $scope.settings.Master.CostCenterList[$scope.settings.Master.CostCenterList.findIndexWithAttr('CostCenterCode', $scope.TravelersforAccommodation[0].CostCenter)];
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[$scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.length - 1].CostCenter = $scope.TravelersforAccommodation[0].CostCenter;

                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[$scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.length - 1].IsAlternativeIOOrg = $scope.TravelersforAccommodation[0].IsAlternativeIOOrg;
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[$scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.length - 1].AlternativeIOOrgObject = $scope.settings.Master.OrgUnitList[$scope.settings.Master.OrgUnitList.findIndexWithAttr('ObjectID', $scope.TravelersforAccommodation[0].AlternativeIOOrg)];
                    //Get default io/costcenter from select roommate
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[$scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.length - 1].AlternativeIOOrg = $scope.TravelersforAccommodation[0].AlternativeIOOrg;
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[$scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.length - 1].IOObject = $scope.settings.Master.IOList[$scope.settings.Master.IOList.findIndexWithAttr('OrderID', $scope.TravelersforAccommodation[0].IO)];
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[$scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.length - 1].IO = $scope.TravelersforAccommodation[0].IO;
                    //Addtion by Lucifer
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[$scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.length - 1].ReceiptID = $scope.GE.receiptItem.ReceiptID;
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[$scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.length - 1].ItemID = $scope.GE.receiptItem.ItemID;
                    //End Add 
                    $scope.GetCurrentTraveler($scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[$scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.length - 1], $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.length - 1)
                
                }
                else {
                    $mdDialog.show(
                         $mdDialog.alert()
                           .clickOutsideToClose(false)
                           .title($scope.Text['SYSTEM']['INFORMATION'])
                           .textContent($scope.Text['EXPENSE']['ROOMMATEISEMPTY'])
                           .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                        );
                }
            }

            $scope.GoToSubMain_DeleteRoommate = function (currentIndex) {
                if (angular.isDefined($scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[currentIndex]) && !$scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[currentIndex].IsOwner) {
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.splice(currentIndex, 1);
                }
                $scope.CheckPerosonalIsSelectedThenSplice();

                $scope.sumAmountUsed();
            }

            $scope.GetTravelers = function () {
                var URL = CONFIG.SERVER + 'HRTR/GetBuddyforRoommate';
                console.debug($scope.expenseList_data);
                console.debug('here');
                var oRequestParameter = { InputParameter: { 'TRAVELREQUESTNO': $scope.document.Additional.TravelRequestNo, 'REQUESTNO': $scope.document.RequestNo, 'CHECKINDATE': $scope.expenseList_data.Date1, 'CHECKOUTDATE': $scope.expenseList_data.Date2 }, CurrentEmployee: $scope.employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.TravelersforAccommodation = response.data;
                    if ($scope.TravelersforAccommodation.length == 0) {
                        $mdDialog.show(
                         $mdDialog.alert()
                           .clickOutsideToClose(false)
                           .title($scope.Text['SYSTEM']['INFORMATION'])
                           .textContent($scope.Text['EXPENSE']['INVALIDADDROOMMATE'])
                           .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                       );
                    }
                    $scope.TravelersforAccommodation_Buffer = angular.copy($scope.TravelersforAccommodation);
                    $scope.CheckRoommateSelectedThenSpliceOnChangeCheckInCheckOut();
                    $scope.CheckPerosonalIsSelectedThenSplice();



                }, function errorCallback(response) {
                    $mdDialog.show(
                        $mdDialog.alert()
                        .clickOutsideToClose(false)
                        .title($scope.Text['SYSTEM']['INFORMATION'])
                        .textContent(response.data)
                        .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                });
            };


            $scope.CheckPerosonalIsSelectedThenSplice = function () {
                if (!$scope.isEditor) {

                    $scope.TravelersforAccommodation = angular.copy($scope.TravelersforAccommodation_Buffer);
                    var CountTravelersforAccommodation = $scope.TravelersforAccommodation.length;
                    for (var i = 0; i < $scope.TravelersforAccommodation.length; i++) {
                        if ($scope.TravelersforAccommodation[i].EmployeeID == $scope.document.Requestor.EmployeeID) {
                            $scope.TravelersforAccommodation.splice(i, 1);
                            i--;
                            continue;
                        }
                        for (var j = 0; j < $scope.expenseList_data.ExpenseReportReceiptItemRoommateList.length; j++) {
                            if (angular.isDefined($scope.TravelersforAccommodation[i]) && $scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].EmployeeID == $scope.TravelersforAccommodation[i].EmployeeID) {
                                $scope.TravelersforAccommodation.splice(i, 1);
                                i--;
                                break;
                            }
                        }
                    }
                } else {

                    $scope.TravelersforAccommodation = angular.copy($scope.TravelersforAccommodation_Buffer);
                    var CountTravelersforAccommodation = $scope.TravelersforAccommodation.length;
                    for (var i = 0; i < $scope.TravelersforAccommodation.length; i++) {
                        if ($scope.TravelersforAccommodation[i].EmployeeID == $scope.document.Requestor.EmployeeID) {
                            $scope.TravelersforAccommodation.splice(i, 1);
                            i--;
                            continue;
                        }
                        for (var j = 0; j < $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.length; j++) {
                            if (angular.isDefined($scope.TravelersforAccommodation[i]) && $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].EmployeeID == $scope.TravelersforAccommodation[i].EmployeeID) {
                                $scope.TravelersforAccommodation.splice(i, 1);
                                i--;
                                break;
                            }
                        }
                    }
                }
                        
            }

            $scope.CheckRoommateSelectedThenSpliceOnChangeCheckInCheckOut = function () {
                $scope.TravelersforAccommodation = angular.copy($scope.TravelersforAccommodation_Buffer);
                var ExpenseReportReceiptItemRoommateList_Buffer = [];
                var CountTravelersforAccommodation = $scope.TravelersforAccommodation.length;
                for (var i = 0; i < $scope.TravelersforAccommodation.length; i++) {

                    for (var j = 0; j < $scope.expenseList_data.ExpenseReportReceiptItemRoommateList.length; j++) {
                        if (angular.isDefined($scope.TravelersforAccommodation[i]) && $scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].EmployeeID == $scope.TravelersforAccommodation[i].EmployeeID ) {


                            //$scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].EmployeeName = $scope.TravelersforAccommodation[j].EmployeeName;
                            ////Bind Master for dropdownlist
                            //$scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].IsAlternativeCostCenter = $scope.TravelersforAccommodation[0].IsAlternativeCostCenter;
                            if ($scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].CostCenter) {
                                $scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].CostCenterObject = $scope.masterData.objCostcenter[$scope.masterData.objCostcenter.findIndexWithAttr('CostCenterCode', $scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].CostCenter)];
                                $scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].CostCenter = $scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].CostCenterObject.CostCenterCode;
                                $scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].CostCenterName = $scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].CostCenterObject.LongDesc;
                            }
                            ////Get default io/costcenter from select roommate
                            if ($scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].AlternativeIOOrg) {
                                //$scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].IsAlternativeIOOrg = $scope.TravelersforAccommodation[0].IsAlternativeIOOrg;
                                $scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].AlternativeIOOrgObject = $scope.masterData.objOrganization[$scope.masterData.objOrganization.findIndexWithAttr('ObjectID', $scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].AlternativeIOOrg)];
                                $scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].OrgUnitID = $scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].AlternativeIOOrgObject.ObjectID;
                                $scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].OrgUnitName = $scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].AlternativeIOOrgObject.Text;
                            }

                            if ($scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].IO) {
                                //$scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].AlternativeIOOrg = $scope.TravelersforAccommodation[0].AlternativeIOOrg;
                                $scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].IOObject = $scope.objIOLookupObj[$scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].IO];
                                $scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].IO = $scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].IOObject.OrderID;
                                $scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].IOName = $scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].IOObject.Description;
                            }

                            //set to autocomplete
                            $scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].searchCostCenterText = $scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].CostCenter + ' : ' + $scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].CostCenterName;
                            $scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].searchAlternativeIOOrgText = $scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].OrgUnitID + ' : ' + $scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].OrgUnitName;
                            $scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].searchCostCentesearchIOTextrText = $scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].IO + ' : ' + $scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j].IOName;

                            ExpenseReportReceiptItemRoommateList_Buffer.push(angular.copy($scope.expenseList_data.ExpenseReportReceiptItemRoommateList[j]));
                            break;
                        }
                    }
                    if ($scope.TravelersforAccommodation[i].EmployeeID == $scope.document.Requestor.EmployeeID) {
                        $scope.TravelersforAccommodation.splice(i, 1);
                        i--;
                        continue;
                    }
                 
                }
                if (!$scope.isEditor) {
                    // set for viewer
                    //$scope.listRoomateViewer =  angular.copy(ExpenseReportReceiptItemRoommateList_Buffer);
                    $scope.listRoomateViewer = angular.copy($scope.expenseList_data.ExpenseReportReceiptItemRoommateList);
                }
                if ($scope.expenseList_data.ExpenseReportReceiptItemRoommateList.length == 1 && $scope.expenseList_data.ExpenseReportReceiptItemRoommateList[0].EmployeeID == $scope.employeeData.RequesterEmployeeID) {

                }
                else {
                    $scope.expenseList_data.ExpenseReportReceiptItemRoommateList = angular.copy(ExpenseReportReceiptItemRoommateList_Buffer);
                }
                
              
            }




        $scope.addRow = function () {
            $scope.DefaultOrgUnitID = $scope.Page == 'GeneralExpense' ? $scope.document.Additional.AlternativeIOOrg : $scope.data.Travelers[0].OrgUnitID;

            var oRoomate = angular.copy($scope.tmpRoomateForNone);
         
            oRoomate.CostCenterObject = angular.copy($scope.settings.Master.CostcenterDistributionList[0]);
            oRoomate.CostCenter = oRoomate.CostCenterObject.CostCenterCode;
            oRoomate.AlternativeIOOrgObject = $scope.settings.Master.OrgUnitList[$scope.settings.Master.OrgUnitList.findIndexWithAttr('ObjectID', $scope.DefaultOrgUnitID)];
            oRoomate.AlternativeIOOrg = oRoomate.AlternativeIOOrgObject.ObjectID;
            oRoomate.IOObject = null;
            oRoomate.IO = '';
            oRoomate.UsedAmount = oRoomate.UsedAmount;
            //Addtion by Lucifer
            oRoomate.ReceiptID = $scope.GE.receiptItem.ReceiptID;
            oRoomate.ItemID = $scope.GE.receiptItem.ItemID;
            //End Add
            $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.push(oRoomate);
            //$scope.selectedItemTravelerChange(angular.copy($scope.settings.Master.ProjectList[0]), $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.length - 1);
        };

        $scope.init_data = function () {
            if (!$scope.isEditor) {
                $scope.GetTravelers();
            }
        }

            $scope.removeNewRow = function (index) {
                if (confirm($scope.Text['SYSTEM']['CONFIRM_DELETE'])) {
                    $scope.tmpRoomateForNone.splice(index, 1);
                }
            };


            $scope.selectedItemProjectAVGChange = function (item, index) {
                if (angular.isDefined(item) && item != null) {
                    $log.info('Item changed to ' + JSON.stringify(item));
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].ProjectObject = item;
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].ProjectCode = item.ProjectCode;
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].ProjectName = item.Name;
                    //Nun Add
                    if ($scope.projectlist != null) {
                        if ($scope.projectlist.findIndexWithAttr('ProjectCode', item.ProjectCode) != -1) {
                            var tmpCostcenter = $scope.projectlist[$scope.projectlist.findIndexWithAttr('ProjectCode', item.ProjectCode)].CostCenter;
                            if (tmpCostcenter != null && tmpCostcenter != "") {
                                if (tmpCostcenter != $scope.objCostcenterDistribution[0].CostCenterCode) {
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].IsAlternativeCostCenter = true;
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].CostCenterObject = angular.copy($scope.objCostcenter[$scope.objCostcenter.findIndexWithAttr('CostCenterCode', tmpCostcenter)]);
                                }
                                else {
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].IsAlternativeCostCenter = false;
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].CostCenterObject = angular.copy($scope.objCostcenterDistribution[0]);
                                }
                            }
                            else {
                                $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].IsAlternativeCostCenter = false;
                                $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].CostCenterObject = angular.copy($scope.objCostcenterDistribution[0]);
                            }
                        }
                    }
                    //
                    $scope.DefaultIOOrg(item, index);
                }
                else {
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].ProjectObject = null;
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].ProjectCode = '';
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].ProjectName = '';
                }
            }

            $scope.DefaultIO = function (index) {
                var tempObjIOList = angular.copy($scope.settings.Master.IOList.filter(createFilterForIO(index)));
                if (tempObjIOList.findIndexWithAttr('OrderID', $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].IO) < 0) {
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].IOObject = tempObjIOList.filter(createFilterForIO(index))[0];
                    if ($scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].IOObject) {
                        $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].IO = $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].IOObject.OrderID;
                        $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].IOName = $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].IOObject.Description;
                    }

                    //Nun Add
                    //var tmpIO = $scope.projectlist[$scope.projectlist.findIndexWithAttr('ProjectCode', $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].ProjectCode)].IO;
                    //if (tmpIO != null && tmpIO != "") {
                    //    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].IOObject = angular.copy($scope.settings.Master.IOList[$scope.settings.Master.IOList.findIndexWithAttr('OrderID', tmpIO)]);
                    //}
                    //

                    //var tmpIO = $scope.projectlist[$scope.projectlist.findIndexWithAttr('ProjectCode', $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].ProjectCode)].IO;
                    //if (tmpIO != null && tmpIO != "") {
                        $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].IOObject = angular.copy($scope.settings.Master.IOList[0]);
                    //}

                }

            }

            //Nun 21/2/2017
            $scope.DefaultIOOrg = function (item, index) {
                if (item != null) {
                    if ($scope.projectlist != null) {
                        var PJCode = $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].ProjectCode;
                        if ($scope.projectlist.findIndexWithAttr('ProjectCode', PJCode) != -1) {
                            var tmpIOOrg = $scope.projectlist[$scope.projectlist.findIndexWithAttr('ProjectCode', PJCode)].AlternativeIOOrg;

                            var tmpIO = $scope.projectlist[$scope.projectlist.findIndexWithAttr('ProjectCode', PJCode)].IO;

                            if (tmpIOOrg != null && tmpIOOrg != "") {
                                if (tmpIOOrg != $scope.objCostcenterDistribution[0].AlternativeIOOrg) {
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].IsAlternativeIOOrg = true;
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].AlternativeIOOrgObject = angular.copy($scope.objOrganization[$scope.objOrganization.findIndexWithAttr('ObjectID', tmpIOOrg)]);
                                    if (tmpIO != null && tmpIO != "") {

                                        $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].IOObject = angular.copy($scope.objIO[$scope.objIO.findIndexWithAttr('OrderID', tmpIO)]);

                                    }
                                }
                                else {
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].IsAlternativeIOOrg = false;
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].AlternativeIOOrgObject = angular.copy($scope.objOrganization[$scope.objOrganization.findIndexWithAttr('ObjectID', $scope.DefaultOrgUnitID)]);
                                }
                            }
                            else {
                                $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].IsAlternativeIOOrg = false;
                                $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].AlternativeIOOrgObject = angular.copy($scope.objOrganization[$scope.objOrganization.findIndexWithAttr('ObjectID', $scope.DefaultOrgUnitID)]);
                            }
                            //Addtion by Lucifer
                            $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].ReceiptID = $scope.GE.receiptItem.ReceiptID;
                            $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].ItemID = $scope.GE.receiptItem.ItemID;
                            //End Add by Lucifer

                        }
                    }
                }
            }
            //

            function checkDuplicate(arr, index) {
                return function filterFn(project) {
                    if (index > -1) {
                        if (arr[index].ProjectCode == project.ProjectCode) return true;
                    }
                    return !(arr.duplicateProp('ProjectCode', project.ProjectCode));
                };
            }

            function createProjectFilterFor(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(project) {
                    var source = angular.lowercase(project.ProjectCode + ' : ' + project.Name);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }

            var tempResult;
            $scope.tryToSelect = function (text, $index) {
                for (var i = 0; i < $scope.projectlist.length; i++) {
                    if (($scope.projectlist[i].ProjectCode + " : " + $scope.projectlist[i].Name) == text || $scope.projectlist[i].Name == text) {
                        tempResult = $scope.projectlist[i];
                        break;
                    }
                }
                $scope.searchTextProject = '';
                $scope.selectedItemProjectAVGChange(null, $index);
            }
            $scope.checkText = function (text, $index) {
                var result = null;
                for (var i = 0; i < $scope.projectlist.length; i++) {
                    if (($scope.projectlist[i].ProjectCode + " : " + $scope.projectlist[i].Name) == text || $scope.projectlist[i].Name == text) {
                        result = $scope.projectlist[i];
                        break;
                    }
                }
                if (result) {
                    $scope.searchTextProject = result.ProjectCode + " : " + result.Name;
                    $scope.selectedItemProjectAVGChange(result, $index);
                } else if (tempResult) {
                    $scope.searchTextProject = tempResult.ProjectCode + " : " + tempResult.Name;
                    $scope.selectedItemProjectAVGChange(tempResult, $index);
                }
            }

            $scope.newProjectData = function (employee) {
                alert("Sorry! You'll need to create a Constitution for " + employee + " first!");
            }

            //auto complete IO 
            $scope.querySearchIO = function (query, index) {
                if (!$scope.settings.Master.IOList) return;

                var results = query ? $scope.settings.Master.IOList.filter(createFilterForIO(index)).filter(createFilterSearchForIO(query)) : $scope.settings.Master.IOList.filter(createFilterForIO(index)), deferred;

                results = angular.copy($scope.limMaxArr(results, 30));
                if ($scope.simulateQuery) {
                    deferred = $q.defer();
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 500, false);
                    return deferred.promise;
                } else {
                    return results;
                }
            };

            $scope.selectedItemIOChange = function (item, index) {
                if (angular.isDefined(item) && item != null) {

                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].IOObject = item;
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].IO = item.OrderID;
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].IOName = item.Description;
                }
                else {
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].IOObject = null;
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].IO = '';
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].IOName = '';
                }
            };

            $scope.newData = function (data) {
                alert("Sorry! You'll need to create a Constitution for " + data + " first!");
            };

            function createFilterSearchForIO(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(item) {
                    if (!item) return false;
                    var source = angular.lowercase(item.OrderID + ':' + item.Description);
                    return (source.indexOf(lowercaseQuery) >= 0) && (item);
                };
            }

            function createFilterForIO(index) {
                return function filterFn(item) {
                    if (!item) return false;
                    //if ($scope.data.ProjectCostDistributions[index] && $scope.data.ProjectCostDistributions[index].IsAlternativeIOOrg)
                    return item.ObjectID == $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].AlternativeIOOrg;
                    //else
                    //    return (item.RespCostCenter == $scope.data.ProjectCostDistributions[index].CostCenter && item.ObjectID == 'X');
                };
            }
            var tempResult_budg;
            $scope.tryToSelect_budg = function (text, $index) {
                var IO_LIST = $scope.settings.Master.IOList.filter(createFilterForIO($index));
                for (var i = 0; i < IO_LIST.length; i++) {
                    if ((IO_LIST[i].OrderID + " : " + IO_LIST[i].Description) == text || IO_LIST[i].Description == text) {
                        tempResult_budg = IO_LIST[i];
                        break;
                    }
                }

                $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[$index].searchIOText = '';
                $scope.selectedItemIOChange(null, $index);
            }
            $scope.checkText_budg = function (text, $index) {
                var IO_LIST = $scope.settings.Master.IOList.filter(createFilterForIO($index));
                var result = null;
                for (var i = 0; i < IO_LIST.length; i++) {
                    if ((IO_LIST[i].OrderID + " : " + IO_LIST[i].Description) == text || IO_LIST[i].Description == text) {
                        result = IO_LIST[i];
                        break;
                    }
                }
                if (result) {
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[$index].searchIOText = result.OrderID + " : " + result.Description;
                    $scope.selectedItemIOChange(result, $index);
                } else if (tempResult_budg) {
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[$index].searchIOText = tempResult_budg.OrderID + " : " + tempResult_budg.Description;
                    $scope.selectedItemIOChange(tempResult_budg, $index);
                }
            }


            //$scope.findOrgOrgUnitList = function (Org) {
            //    for (var i = 0; i < $scope.settings.Master.OrgUnitList.length; i++) {
            //        if (Org == $scope.settings.Master.OrgUnitList[i].AlternativeIOOrg)
            //        {

            //            break;
            //        }
            //    }
            //}



            //auto complete CostCenter 
            $scope.querySearchCostCenter = function (query, index) {
                if (!$scope.settings.Master.CostCenterList || !$scope.settings.Master.CostcenterDistributionList) return;
                //var results = query ? $scope.objCostcenterDistribution.filter(createFilterForCostCenter(query)) : $scope.objCostcenterDistribution, deferred;
                if ($scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList && $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].IsAlternativeCostCenter)
                    var results = query ? $scope.settings.Master.CostCenterList.filter(createFilterForCostCenter(query)) : $scope.settings.Master.CostCenterList, deferred;
                else
                    var results = query ? $scope.settings.Master.CostcenterDistributionList.filter(createFilterForCostCenter(query)) : $scope.settings.Master.CostcenterDistributionList, deferred;

                results = angular.copy($scope.limMaxArr(results, 30));
                if ($scope.simulateQuery) {
                    deferred = $q.defer();
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 250, false);
                    return deferred.promise;
                } else {
                    return results;
                }
            };

            $scope.selectedItemCostCenterChange = function (item, index) {
                if (angular.isDefined(item) && item != null) {
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].CostCenterObject = item;
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].CostCenter = item.CostCenterCode;
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].CostCenterName = item.LongDesc;
                }
                else {
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].CostCenterObject = null;
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].CostCenter = '';
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].CostCenterName = '';
                }
            };
            function createFilterForCostCenter(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    if (!x) return false;
                    var source = angular.lowercase(x.CostCenterName);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }
            var tempResult_cc;
            $scope.tryToSelect_cc = function (text, $index) {
                if ($scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList && $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[$index].IsAlternativeCostCenter) {
                    for (var i = 0; i < $scope.settings.Master.CostCenterList.length; i++) {
                        if (($scope.settings.Master.CostCenterList[i].CostCenterCode + " : " + $scope.settings.Master.CostCenterList[i].LongDesc) == text || $scope.settings.Master.CostCenterList[i].LongDesc == text) {
                            tempResult_cc = $scope.settings.Master.CostCenterList[i];
                            break;
                        }
                    }
                } else {
                    for (var i = 0; i < $scope.settings.Master.CostcenterDistributionList.length; i++) {
                        if (($scope.settings.Master.CostcenterDistributionList[i].CostCenterCode + " : " + $scope.settings.Master.CostcenterDistributionList[i].LongDesc) == text || $scope.settings.Master.CostcenterDistributionList[i].LongDesc == text) {
                            tempResult_cc = $scope.settings.Master.CostcenterDistributionList[i];
                            break;
                        }
                    }
                }

                $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[$index].searchCostCenterText = '';
                $scope.selectedItemCostCenterChange(null, $index);
            }
            $scope.checkText_cc = function (text, $index) {
                var result = null;

                if ($scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList && $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[$index].IsAlternativeCostCenter) {
                    for (var i = 0; i < $scope.settings.Master.CostCenterList.length; i++) {
                        if (($scope.settings.Master.CostCenterList[i].CostCenterCode + " : " + $scope.settings.Master.CostCenterList[i].LongDesc) == text || $scope.settings.Master.CostCenterList[i].LongDesc == text) {
                            result = $scope.settings.Master.CostCenterList[i];
                            break;
                        }
                    }
                } else {
                    for (var i = 0; i < $scope.settings.Master.CostcenterDistributionList.length; i++) {
                        if (($scope.settings.Master.CostcenterDistributionList[i].CostCenterCode + " : " + $scope.settings.Master.CostcenterDistributionList[i].LongDesc) == text || $scope.settings.Master.CostcenterDistributionList[i].LongDesc == text) {
                            result = $scope.settings.Master.CostcenterDistributionList[i];
                            break;
                        }
                    }
                }
                if (result) {
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[$index].searchCostCenterText = result.CostCenterCode + " : " + result.LongDesc;
                    $scope.selectedItemCostCenterChange(result, $index);
                } else if (tempResult_cc) {
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[$index].searchCostCenterText = tempResult_cc.CostCenterCode + " : " + tempResult_cc.LongDesc;
                    $scope.selectedItemCostCenterChange(tempResult_cc, $index);
                }
            }




            //auto complete AlternativeIOOrg 
            $scope.querySearchAlternativeIOOrg = function (query, index) {
                if (!$scope.settings.Master.OrgUnitList) return;
                var results = query ? $scope.settings.Master.OrgUnitList.filter(createFilterForAlternativeIOOrg(query)) : $scope.settings.Master.OrgUnitList, deferred;

                results = angular.copy($scope.limMaxArr(results, 30));
                if ($scope.simulateQuery) {
                    deferred = $q.defer();
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 250, false);
                    return deferred.promise;
                } else {
                    return results;
                }
            };


            $scope.selectedItemAlternativeIOOrgChange = function (item, index) {
                if (angular.isDefined(item) && item != null) {
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].AlternativeIOOrgObject = item;
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].AlternativeIOOrg = item.ObjectID;
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].AlternativeIOOrgName = item.Text;
                    //$scope.DefaultIO(index);
                    var tempallIO = angular.copy($scope.settings.Master.IOList.filter(createFilterForIO(index)));
                    if (tempallIO.length) {
                        $scope.selectedItemIOChange(tempallIO[0], index);
                    }
                    else {
                        $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].IOObject = null;
                        $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].IO = '';
                        tempResult_pb_io = null;
                        $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].ap.searchIOText = $scope.Text[$scope.Textcategory]['NO_IO'];//"ไม่พบ IO";
                        //$scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[$index].searchIOText = "ไม่พบ IO";
                    }
                    tempResult_orgb = angular.copy(item);
                }
                else {
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].AlternativeIOOrgObject = null;
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].AlternativeIOOrg = '';
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].AlternativeIOOrgName = '';
                }
                tempResult_budg = '';
            };



            function createFilterForAlternativeIOOrg(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    if (!x) return false;
                    var source = angular.lowercase(x.BudgetOrgName);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }

            var tempResult_orgb;
            $scope.tryToSelect_orgb = function (text, $index, IsAlternativeIOOrg) {
                if (!IsAlternativeIOOrg) { return; }
                for (var i = 0; i < $scope.settings.Master.OrgUnitList.length; i++) {
                    if ($scope.settings.Master.OrgUnitList[i].ObjectID == text.ObjectID) {// || $scope.settings.Master.OrgUnitList[i].Text == text.Text) {
                        tempResult_orgb = $scope.settings.Master.OrgUnitList[i];
                        break;
                    }
                }

                $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[$index].searchAlternativeIOOrgText = '';
                $scope.selectedItemAlternativeIOOrgChange(null, $index);
            }
            $scope.checkText_orgb = function (text, $index) {
                var result = null;
                for (var i = 0; i < $scope.settings.Master.OrgUnitList.length; i++) {
                    if ($scope.settings.Master.OrgUnitList[i].ObjectID == text.ObjectID) {// || $scope.settings.Master.OrgUnitList[i].Text == text) {
                        result = $scope.settings.Master.OrgUnitList[i];
                        break;
                    }
                }
                if (result) {
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[$index].searchAlternativeIOOrgText = result.ObjectID + " : " + result.Text;
                    $scope.selectedItemAlternativeIOOrgChange(result, $index);
                } else if (tempResult_orgb) {
                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[$index].searchAlternativeIOOrgText = tempResult_orgb.ObjectID + " : " + tempResult_orgb.Text;
                    $scope.selectedItemAlternativeIOOrgChange(tempResult_orgb, $index);
                }
            }

            $scope.AlternativeCostCenter = function (group, index) {
                if (!group.IsAlternativeCostCenter) {
                    var temp_person;
                    var constcenter;
                    if ($scope.TravelersforAccommodation_Buffer) {
                        for (var i = 0; i < $scope.TravelersforAccommodation_Buffer.length; i++) {
                            if (group.EmployeeID == $scope.TravelersforAccommodation_Buffer[i].EmployeeID) {
                                temp_person = $scope.TravelersforAccommodation_Buffer[i];
                            }
                        }
                    }

                    for (var i = 0; i < $scope.masterData.objCostcenter.length; i++) {
                        if ($scope.masterData.objCostcenter[i].CostCenterCode == temp_person.CostCenter) {
                            constcenter = $scope.masterData.objCostcenter[i]; break;
                        }
                    }
                    group.CostCenterObject = constcenter;
                    group.CostCenter = constcenter.CostCenterCode;
                    group.CostCenterName = constcenter.LongDesc;
                    //$scope.DefaultIO(index);
                }
                else {
                    //group.CostCenterObject = $scope.settings.Master.CostCenterList[$scope.settings.Master.CostCenterList.findIndexWithAttr('CostCenterCode', $scope.settings.Master.CostcenterDistributionList[0].CostCenterCode)];
                    //group.CostCenter = $scope.settings.Master.CostCenterList[0].CostCenterCode;
                    //group.CostCenterName = $scope.settings.Master.CostCenterList[0].LongDesc;
                    //$scope.DefaultIO(index);
                }
            };

            $scope.AlternativeIOOrg = function (group, index) {
                // change default
                var orgUnit;
                var temp_person;

                if ($scope.TravelersforAccommodation_Buffer) {
                    for (var i = 0; i < $scope.TravelersforAccommodation_Buffer.length; i++) {
                        if (group.EmployeeID == $scope.TravelersforAccommodation_Buffer[i].EmployeeID) {
                            temp_person = $scope.TravelersforAccommodation_Buffer[i];
                        }
                    }

                }
                if (!temp_person) return;
                if (!group.isAlternativeIOOrg) {
                    if (temp_person.OrgUnitID != null) {

                        for (var i = 0; i < $scope.masterData.objOrganization.length; i++) {
                            if ($scope.masterData.objOrganization[i].ObjectID == temp_person.OrgUnitID) {
                                orgUnit = $scope.masterData.objOrganization[i]; break;
                            }
                        }

                        $scope.selectedItemAlternativeIOOrgChange(orgUnit, index);
                    }
                    
                } else {
                    for (var i = 0; i < $scope.masterData.objOrganization.length; i++) {
                        if ($scope.masterData.objOrganization[i].ObjectID == $scope.document.Additional.AlternativeIOOrg) {
                            orgUnit = $scope.masterData.objOrganization[i]; break;
                        }
                    }
                    $scope.selectedItemAlternativeIOOrgChange(orgUnit, index);
                }
                //if (!group.IsAlternativeIOOrg) {
                //    $scope.selectedItemAlternativeIOOrgChange($scope.settings.Master.OrgUnitList[$scope.settings.Master.OrgUnitList.findIndexWithAttr('ObjectID', $scope.TravelersforAccommodation_Buffer.findIndexWithAttr('EmployeeID', $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].EmployeeID).AlternativeIOOrg)], index)
                //    console.log('xxx', $scope.TravelersforAccommodation_Buffer.findIndexWithAttr('EmployeeID', $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].EmployeeID).AlternativeIOOrg);
                //}
            };

            function showAlert(message) {
                $mdDialog.show(
                  $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('INFORMATION')
                    .textContent(message)
                    .ok('OK')
                );
            };


            $scope.sumAmountUsed = function (current, index) {
                if (current.UsedAmount) {
                    var sumTotal = 0;
                    for (var i = 0; i < $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.length; i++) {
                        sumTotal += Number($scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[i].UsedAmount);
                    }
                    if (!isNaN(sumTotal)) {
                        $scope.GE.receiptItemFormData.Amount = Number(sumTotal);
                    }
                }
                else {
                    current.UsedAmount = 0;
                }
            }
            $scope.sumAmountUsed = function () {
                    var sumTotal = 0;
                    for (var i = 0; i < $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.length; i++) {
                        sumTotal += Number($scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[i].UsedAmount);
                    }
                    if (!isNaN(sumTotal)) {
                        $scope.GE.receiptItemFormData.Amount = Number(sumTotal);
                    }
            }
            $scope.onBlurAmountUsed = function (objFormData) {
                var valueAmount = Number(objFormData.UsedAmount);
                valueAmount = isNaN(valueAmount) ? 0 : $scope.MathRounding(valueAmount);
                objFormData.UsedAmount = valueAmount;
            };
        }]);
})();


