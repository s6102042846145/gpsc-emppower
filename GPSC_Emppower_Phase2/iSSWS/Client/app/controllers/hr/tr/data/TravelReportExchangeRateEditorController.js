﻿(function () {
    angular.module('ESSMobile')
        .controller('TravelReportExchangeRateEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', '$window', 'CONFIG', '$q', '$timeout', '$mdDialog','$interval', function ($scope, $http, $routeParams, $location, $filter, $window, CONFIG, $q, $timeout, $mdDialog,$interval) {
            //ExchangeRate
            $scope.Textcategory = 'EXPENSE';

            var employeeData = getToken(CONFIG.USER);

            $scope.ExchangeModel = null;
            $scope.ExchangeModelReady = false;
            $scope.CurrencyReady = false;
            $scope.ExchangeTypeReady = false;

            $scope.NewExchangeRateCapture = null;

            $scope.InitialConfig = function () {
                var getExchangeModel = function () {
                    var URL = CONFIG.SERVER + 'HRTR/GetExchangeModel';
                    var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };

                    return $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.ExchangeModel = response.data;
                        $scope.ExchangeModel.ExchangeRateCapture[0].EffectiveDate = $filter('date')(new Date(), 'yyyy-MM-ddT00:00:00');
                        $scope.ExchangeModel.ExchangeRateCapture[0].ToCurrency = $scope.document.Requestor.AreaSetting.CurrencyCode;
                        $scope.ExchangeModelReady = true;
                        console.log('GetExchangeModel', response.data);
                        return response;
                    },
                    function errorCallback(response) {
                        console.log('error GetExchangeModel', response);
                        return response;
                    });
                };
                var exchangeModelPromise = getExchangeModel();

                var getAllCurrency = function () {
                    var URL = CONFIG.SERVER + 'HRTR/GetAllCurrency';
                    var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };

                    return $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.currency = response.data;
                        $scope.currency2 = [];
                        $.each($scope.currency, function (i, c) {
                            $scope.currency2.push({
                                Code: c.Code
                            });
                        });
                        //console.debug($scope.currency);
                        $scope.CurrencyReady = true;
                        return response;
                    },
                    function errorCallback(response) {
                        $scope.currency = [];
                        console.log('error GetAllCurrency', response);
                        return response;
                    });
                };
                var allCurrencyPromise = getAllCurrency();

                var getAllExchageType = function () {
                    $scope.HaveCashAdvance =  (angular.isDefined($scope.data.CashAdvance) && $scope.data.CashAdvance.TotalAmount > 0);
                    var URL = CONFIG.SERVER + 'HRTR/GetExchangeTypeAll';
                    var oRequestParameter = { InputParameter: { "IsTravelReport": true, "HaveCashAdvance": $scope.HaveCashAdvance }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };

                    return $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.exchangeType = response.data;
                        $scope.ExchangeTypeReady = true;
                        return response;
                    },
                    function errorCallback(response) {
                        $scope.exchangeType = [];
                        console.log('error GetExchangeTypeAll', response);
                        return response;
                    });
                };
                var allExchangeTypePromise = getAllExchageType();


                $q.all([exchangeModelPromise, allCurrencyPromise, allExchangeTypePromise]).then(function (response) {
                    // success
                    if ($scope.data.IsEdit && angular.isDefined($scope.exchangeRateDeferred)) {
                        $scope.exchangeRateDeferred.resolve(response);
                    }
                }, function (response) {
                    // fail
                    if ($scope.data.IsEdit && angular.isDefined($scope.exchangeRateDeferred)) {
                        $scope.exchangeRateDeferred.resolve(response);
                    }
                });
            };
            $scope.InitialConfig();

            var status = {
                islookingup : false
            }
            var mdDialog  = null;
            $scope.lookupExchangeRate = function (objExCapture, isCalculate, calculateFromExchangeRateCapture, isAddNew) {
                if (status.islookingup == true) { return; }
                status.islookingup = true;
                var URL = CONFIG.SERVER + 'HRTR/LookupExchangeRate';
                var oRequestParameter = { InputParameter: { "FromCurrency": objExCapture.FromCurrency, "EffectiveDate": objExCapture.EffectiveDate }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    if (response.data == null) {
                        objExCapture.ExchangeRate = '';
                        if (mdDialog) {
                            if (mdDialog.$$state.status == 1) {
                                mdDialog = $mdDialog.show(
                                $mdDialog.alert()
                                      .clickOutsideToClose(true)
                                      .title('WARNING')
                                      .textContent($scope.Text['EXPENSE']['NOTFOUND_EXCHANGE'] + objExCapture.FromCurrency + $scope.Text['EXPENSE']['PLEASE_INPUT_EXCHANGE_RATE'])
                                      .ok('OK')
                                  );

                            }
                        } else {
                            mdDialog = $mdDialog.show(
                            $mdDialog.alert()
                                    .clickOutsideToClose(true)
                                    .title('WARNING')
                                    .textContent($scope.Text['EXPENSE']['NOTFOUND_EXCHANGE'] + objExCapture.FromCurrency + $scope.Text['EXPENSE']['PLEASE_INPUT_EXCHANGE_RATE'])
                                    .ok('OK')
                                );
                        }
                       
                    } else {
                        objExCapture.ExchangeRate = response.data.Rate;
                        objExCapture.MasterExchangeRate = response.data.Rate;
                        objExCapture.ExchangeRateType = response.data.ExRateType;
                        objExCapture.RatioFromCurrencyUnit = response.data.FromCurrencyRatio;
                        objExCapture.ToCurrency = response.data.ToCurrency;
                        objExCapture.RatioToCurrencyUnit = response.data.ToCurrencyRatio;
                        objExCapture.EffectiveDate = response.data.EffectiveDate;
                        objExCapture.EffectiveDate_date_type = new Date(response.data.EffectiveDate);

                        if (isAddNew) {

                            // overide by cr BP004
                            objExCapture.FromCurrency = '';
                            objExCapture.ExchangeRate = '';
                            objExCapture.MasterExchangeRate = 1;
                            objExCapture.EffectiveDate = $scope.data.ExchangeRateCaptures[0].EffectiveDate;
                            objExCapture.EffectiveDate_date_type = new Date($scope.data.ExchangeRateCaptures[0].EffectiveDate);
                        }
                    }
                    console.log('lookupExchangeRate', angular.copy(objExCapture));
                    if (isCalculate) {
                        if (angular.isDefined(calculateFromExchangeRateCapture)) {
                            $scope.CalculateTravelReport(calculateFromExchangeRateCapture);
                        } else {
                            $scope.CalculateTravelReport();
                        }
                    }
                    $scope.loader.enable = false;
                    status.islookingup = false;
                },
                function errorCallback(response) {
                    objExCapture.ExchangeRate = 1;
                    if (mdDialog) {
                        if (mdDialog.$$state.status == 1) {
                            mdDialog = $mdDialog.show(
                            $mdDialog.alert()
                                  .clickOutsideToClose(true)
                                  .title('WARNING')
                                  .textContent($scope.Text['EXPENSE']['NOTFOUND_EXCHANGE'] + objExCapture.FromCurrency + $scope.Text['EXPENSE']['PLEASE_INPUT_EXCHANGE_RATE'])
                                  .ok('OK')
                              );

                        }
                    } else {
                        mdDialog = $mdDialog.show(
                        $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title('WARNING')
                                .textContent($scope.Text['EXPENSE']['NOTFOUND_EXCHANGE'] + objExCapture.FromCurrency + $scope.Text['EXPENSE']['PLEASE_INPUT_EXCHANGE_RATE'])
                                .ok('OK')
                            );
                    }
                   
                    console.log('error lookupExchangeRate.', response);
                    if (isCalculate) {
                        if (angular.isDefined(calculateFromExchangeRateCapture)) {
                            $scope.CalculateTravelReport(calculateFromExchangeRateCapture);
                        } else {
                            $scope.CalculateTravelReport();
                        }
                    }
                    $scope.loader.enable = false;
                    status.islookingup = false;
                });
            };



            $scope.removeRow = function (index) {
                if ($scope.ValidateExchangeRateHasUse(index)) {
                    alertMessage($scope.Text['SYSTEM']['EXCHANGERATEWASUSED']);
                }
                else if (confirm($scope.Text['SYSTEM']['CONFIRM_DELETE'])) {
                    if (!$scope.data.ExchangeRateCaptures[index].IsLock) {
                        $scope.data.ExchangeRateCaptures.splice(index, 1);
                        $scope.CalculateTravelReport(true);
                    }
                }
            };

            $scope.onChangeEffectiveDate = function (row, selectedDate) {
                row.EffectiveDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                $scope.lookupExchangeRate(row, false, false);

            };

            $scope.onChangeCurrency = function (row) {
                if (row.FromCurrency == '' || !isContainInCurrencyList(row.FromCurrency)) return;
                isOnblurAfterSelect = true;
                $scope.lookupExchangeRate(row, false, false);

            };

            $scope.onChangeExchangeType = function (row) {
                //$scope.CalculateTravelReport(true);
            };

            $scope.onChangeExchangeRate = function (row) {
                if (row.ExchangeRate != null && !isNaN(Number(row.ExchangeRate))) {
                    $scope.CalculateTravelReport(true);
                }
            }





            /* attach file */

            $scope.ExchangeTypeAttachFile = 2;

            $scope.formExchangeAttachment = {
                file: null
            };
            //$scope.objExpenseReportReceiptFile = $scope.ExchangeModel.ExchangeRateCapture[0];

            $scope.onAfterValidateFileFunction = function (event, fileList) {
                //console.log('file validated.', fileList);
                //base64:"VVNFIFtXb3JrZmxvd1JGU10N...EVSIEJZIFNPLk5hbWUNCg=="
                //filename:"stored check text in stored.txt"
                //filesize:811
                //filetype: "text/plain"
                var rowIndex = $(event.target).attr('data-row-id');
                var objEx = $scope.data.ExchangeRateCaptures[rowIndex];

                var newFile = angular.copy($scope.ExchangeModel.ExchangeRateCaptureFile[0]);
                newFile.RequestNo = $scope.document.RequestNo;
                newFile.FileID = -1;
                newFile.FilePath = '';
                newFile.FileName = fileList[0].filename;
                newFile.FileType = fileList[0].filetype;
                newFile.FileContent = fileList[0].base64;
                newFile.FileSize = fileList[0].filesize;
                newFile.ExchangeRateType = objEx.ExchangeRateType;
                newFile.FromCurrency = objEx.FromCurrency;
                newFile.ToCurrency = objEx.ToCurrency;
                newFile.EffectiveDate = objEx.EffectiveDate;
                newFile.IsDelete = false;
                if ($scope.AllowUploadFile(newFile.FileName)) {
                    $mdDialog.show(
                       $mdDialog.alert()
                       .clickOutsideToClose(false)
                       .title($scope.Text['SYSTEM']['WARNING'])
                       .textContent($scope.Text['SYSTEM']['VALIDATE_FILENAME'])
                       .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else if (!$scope.AllowUploadFileType(newFile.FileName)) {
                    $mdDialog.show(
                        $mdDialog.alert()
                        .clickOutsideToClose(false)
                        .title($scope.Text['SYSTEM']['WARNING'])
                        .textContent($scope.Text['SYSTEM']['INVALIDFILETYPE'])
                        .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else if (!$scope.AllowFileNameLength(newFile.FileName.length)) {
                    $mdDialog.show(
                        $mdDialog.alert()
                        .clickOutsideToClose(false)
                        .title($scope.Text['SYSTEM']['WARNING'])
                        .textContent($scope.Text['SYSTEM']['SAP_FILENAME_LENGTH_INVALID'])
                        .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else if (!$scope.AllowFileSize(newFile.FileSize)) {

                    $mdDialog.show(
                        $mdDialog.alert()
                        .clickOutsideToClose(false)
                        .title($scope.Text['SYSTEM']['WARNING'])
                        .textContent($scope.Text['SYSTEM']['INVALIDFILESIZE'])
                        .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else if (!$scope.AllowFileZeroSize(newFile.FileSize)) {
                    $mdDialog.show(
                        $mdDialog.alert()
                        .clickOutsideToClose(false)
                        .title($scope.Text['SYSTEM']['WARNING'])
                        .textContent($scope.Text['SYSTEM']['INVALIDFILESIZE2'])
                        .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else
                {
                    if (angular.isUndefined(objEx.ExchangeRateCaptureFileList) || objEx.ExchangeRateCaptureFileList == null) {
                        objEx.ExchangeRateCaptureFileList = [];
                    }
                    objEx.ExchangeRateCaptureFileList.push(newFile);
                    console.log('exchange.', objEx.ExchangeRateCaptureFileList);
                }
                $scope.formExchangeAttachment.file = null;
                $(event.target).val(null);
            };

            $scope.deleteFileAttach = function (parent, rowIndex) {
                if (confirm($scope.Text['SYSTEM']['CONFIRM_DELETE'])) {
                    if (parent.ExchangeRateCaptureFileList[rowIndex].FileID != -1) {
                        // delete server attach file (set flag delete)
                        parent.ExchangeRateCaptureFileList[rowIndex].IsDelete = true;
                    } else {
                        // delete new attach file (remove from array)
                        parent.ExchangeRateCaptureFileList.splice(rowIndex, 1);
                    }
                }
            };

            $scope.recoveryFileAttach = function (row) {
                row.IsDelete = false;
            };

            $scope.getFileAttach = function (attachment) {
                /*
                EffectiveDate
	
	                "2016-08-09T00:00:00"
                ExchangeRate
	
	                34.1234
                ExchangeRateType
	
	                "BOS"
                ExchangeTypeID
	
	                2
                FileContent
	
	                "77u/eyJJbnB1dFBhcmFtZXRl...2NyaXB0aW9uIjpudWxsfX0="
                FileID
	
	                1
                FileName
	
	                "1.txt"
                FilePath
	
	                "0176\0176160000067-RP002\BillOfExchange\0001"
                FileSize
	
	                5435
                FileType
	
	                "text/plain"
                FromCurrency
	
	                "USD"
                IsDelete
	
	                false
                RequestNo
	
	                "0176160000067-RP002"
                ToCurrency
	
	                "THB"
                */

                /* direct */
                if (angular.isDefined(attachment)) {
                    console.log(attachment);
                    if (typeof cordova != 'undefined') {
                        //cordova.InAppBrowser.open("data:application/octet-stream, " + escape(response.data), '_system', 'location=no');
                    } else {
                        //window.open("data:application/octet-stream, " + escape(response.data));
                        var path = CONFIG.SERVER + 'Client/files/' + attachment.FilePath + '/' + attachment.FileName;
                        $window.open(path);
                        //$window.open('http://localhost:15124/Client/index.html#/frmViewRequest/0013160000069-GE000/0013/8150/False/False');
                    }
                }
                /* !direct */
            };

            $scope.onBlurExchangeRate = function (row) {
                if (row.ExchangeRate == null || row.ExchangeRate == '' || isNaN(row.ExchangeRate) || row.ExchangeRate < 0) {
                    row.ExchangeRate = 1;
                }
            };








            /* New row */

            $scope.addNewRow = function () {


                if ($scope.edittingCurrency) {
                    $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(true)
                       .title('WARNING')
                       .textContent($scope.Text['EXPENSE']['PLEASE_SAVE_CURRECY_BEFORE'])
                       .ok('OK')
                   );
                    return;
                }


                $scope.NewExchangeRateCapture = angular.copy($scope.ExchangeModel.ExchangeRateCapture[0]);
                var oExchangeRateCapture = $scope.NewExchangeRateCapture;
                oExchangeRateCapture.RequestNo = $scope.document.RequestNo;
                oExchangeRateCapture.FromCurrency = 'USD';
                //if (angular.isDefined($scope.exchangeType) && $scope.exchangeType.length > 0) {
                //    oExchangeRateCapture.ExchangeTypeID = $scope.exchangeType[0].ExchangeTypeID;
                //}
                oExchangeRateCapture.ExchangeTypeID = $scope.data.ExchangeRateCaptures[0].ExchangeTypeID;
                var results = $filter('filter')($scope.exchangeType, { ExchangeTypeID: oExchangeRateCapture.ExchangeTypeID });
                $scope.NewExchangeRateCapture_searchText.text = results[0].DescriptionTH;

                $scope.lookupExchangeRate(oExchangeRateCapture, false,null,true);
            };

            $scope.saveNewRow = function () {
                if (isCurrencyValid($scope.NewExchangeRateCapture, true, -1)) {
                    // add to list
                    $scope.data.ExchangeRateCaptures.push($scope.NewExchangeRateCapture);
                    $scope.NewExchangeRateCapture = null;
                    $scope.CalculateTravelReport(true);
                }
            }

            $scope.removeNewRow = function () {
                if (confirm($scope.Text['SYSTEM']['CONFIRM_DELETE'])) {
                    $scope.NewExchangeRateCapture = null;
                }
            };
            $scope.ValidateExchangeRateHasUse = function (index)
            {
                var flg = false;
                //for(i=0;i<$scope.data.ExchangeRateCaptures.length;i++)
                //{
                    for (j = 0; j < $scope.document.Additional.ExpenseReportReceiptList.length; j++)
                    {
                        if (($scope.data.ExchangeRateCaptures[index].FromCurrency == $scope.document.Additional.ExpenseReportReceiptList[j].OriginalCurrencyCode) && ($scope.data.ExchangeRateCaptures[index].ExchangeTypeID == $scope.document.Additional.ExpenseReportReceiptList[j].ExchangeTypeID)) {
                            flg = true;
                            return flg;
                        }
                    }
                //}
                return flg;
            }
            $scope.onChangeNewSelectedEffectiveDate = function (row, selectedDate) {
                row.EffectiveDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                $scope.lookupExchangeRate(row, false);
            };
            //var tempCurrency_ = null
            $scope.onChangeNewCurrency = function (row) {
                if (row.FromCurrency == $scope.NewExchangeRateCapture.FromCurrency)
                if (row.FromCurrency == '' || !isContainInCurrencyList(row.FromCurrency)) return;
                isOnblurAfterSelect = true;
                $scope.lookupExchangeRate(row, false);
            };

            $scope.onChangeNewExchangeType = function (row) {
                // empty
            };

            $scope.onChangeNewExchangeRate = function (row) {
                // empty
            }

            $scope.formNewExchangeAttachment = {
                file: null
            };

            $scope.onAfterValidateNewFileFunction = function (event, fileList) {
                var newFile = angular.copy($scope.ExchangeModel.ExchangeRateCaptureFile[0]);
                newFile.RequestNo = $scope.document.RequestNo;
                newFile.FileID = -1;
                newFile.FilePath = '';
                newFile.FileName = fileList[0].filename;
                newFile.FileType = fileList[0].filetype;
                newFile.FileContent = fileList[0].base64;
                newFile.FileSize = fileList[0].filesize;
                newFile.ExchangeRateType = $scope.NewExchangeRateCapture.ExchangeRateType;
                newFile.FromCurrency = $scope.NewExchangeRateCapture.FromCurrency;
                newFile.ToCurrency = $scope.NewExchangeRateCapture.ToCurrency;
                newFile.EffectiveDate = $scope.NewExchangeRateCapture.EffectiveDate;
                newFile.IsDelete = false;
                if ($scope.AllowUploadFile(newFile.FileName))
                {
                    $mdDialog.show(
                       $mdDialog.alert()
                       .clickOutsideToClose(false)
                       .title($scope.Text['SYSTEM']['WARNING'])
                       .textContent($scope.Text['SYSTEM']['VALIDATE_FILENAME'])
                       .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else if (!$scope.AllowUploadFileType(newFile.FileName))
                {
                    $mdDialog.show(
                        $mdDialog.alert()
                        .clickOutsideToClose(false)
                        .title($scope.Text['SYSTEM']['WARNING'])
                        .textContent($scope.Text['SYSTEM']['INVALIDFILETYPE'])
                        .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else if (!$scope.AllowFileNameLength(newFile.FileName.length))
                {
                    $mdDialog.show(
                        $mdDialog.alert()
                        .clickOutsideToClose(false)
                        .title($scope.Text['SYSTEM']['WARNING'])
                        .textContent($scope.Text['SYSTEM']['SAP_FILENAME_LENGTH_INVALID'])
                        .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else if (!$scope.AllowFileSize(newFile.FileSize))
                {
                    $mdDialog.show(
                        $mdDialog.alert()
                        .clickOutsideToClose(false)
                        .title($scope.Text['SYSTEM']['WARNING'])
                        .textContent($scope.Text['SYSTEM']['INVALIDFILESIZE'])
                        .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else if (!$scope.AllowFileZeroSize(newFile.FileSize)) {
                    $mdDialog.show(
                        $mdDialog.alert()
                        .clickOutsideToClose(false)
                        .title($scope.Text['SYSTEM']['WARNING'])
                        .textContent($scope.Text['SYSTEM']['INVALIDFILESIZE2'])
                        .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else {
                    if (angular.isUndefined($scope.NewExchangeRateCapture.ExchangeRateCaptureFileList) || $scope.NewExchangeRateCapture.ExchangeRateCaptureFileList == null) {
                        $scope.NewExchangeRateCapture.ExchangeRateCaptureFileList = [];
                    }
                    $scope.NewExchangeRateCapture.ExchangeRateCaptureFileList.push(newFile);
                }

                newFile = null;
                $scope.formNewExchangeAttachment.file = null;
                $(event.target).val(null);
            };

            $scope.deleteNewFileAttach = function (rowIndex) {
                if (confirm($scope.Text['SYSTEM']['CONFIRM_DELETE'])) {
                    // delete new attach file (remove from array)
                    $scope.NewExchangeRateCapture.ExchangeRateCaptureFileList.splice(rowIndex, 1);
                }
            };

            $scope.recoveryNewFileAttach = function (rowIndex) {
                $scope.GE.receipt.ExpenseReportReceiptFileList[rowIndex].IsDelete = false;
                //$scope.GE.receipt.HasFileAttached = true;
            };

            $scope.getNewFileAttach = function (attachment) {
                if (angular.isDefined(attachment)) {
                    if (typeof cordova != 'undefined') {
                        //cordova.InAppBrowser.open("data:application/octet-stream, " + escape(response.data), '_system', 'location=no');
                    } else {
                        var path = CONFIG.SERVER + 'Client/files/' + attachment.RequestNo + '/' + ("00000" + attachment.ReceiptID.toString()).substr(-5, 5) + '/' + attachment.FileName;
                        $window.open(path);
                    }
                }
            };

            /* !New row */

            
            //auto complete currency
            $scope.querySearchItem = function(query) {
                if (!angular.isDefined($scope.currency) || !angular.isDefined(query)) return [];
                var results = $filter('filter')($scope.currency, { Code: query });
                return results;
            }
            var tempResult;
            var tempResult_old;
            $scope.validateCurrency = function (currency, $index, isNew, r) {
                var result = null;
                if (!isContainInCurrencyList(currency)) {
                    for (var i = 0; i < $scope.currency.length; i++) {
                        if ($scope.currency[i].Code == currency) {
                            result = $scope.currency[i];
                        }
                    }
                    if (isNew) {
                        if (result) {
                            $scope.NewExchangeRateCapture.FromCurrency = result.Code;
                        } else if (tempResult) {
                            $scope.NewExchangeRateCapture.FromCurrency = tempResult.Code;
                        }
                    } else {
                        if (result) {
                            $scope.data.ExchangeRateCaptures[$index].FromCurrency = result.Code;
                        } else if (tempResult_old) {
                            $scope.data.ExchangeRateCaptures[$index].FromCurrency = tempResult_old.Code;
                        }
                    }
                    return;
                } else {
                    for (var i = 0; i < $scope.currency.length; i++) {
                        if ($scope.currency[i].Code == currency) {
                            result = $scope.currency[i];
                        }
                    }
                    if (isNew) {
                        $scope.NewExchangeRateCapture.FromCurrency = result.Code;
                    } else {
                        $scope.data.ExchangeRateCaptures[$index].FromCurrency = result.Code;
                    }
                    $scope.lookupExchangeRate(r, false, false);
                    return ;
                }
            }
            $scope.tryToSelect = function (text, $index, isNew) {
                if (!$scope.data.IsExchangeRateEdit) { return; }
                if (isNew) {
                    for (var i = 0; i < $scope.currency.length; i++) {
                        if ($scope.currency[i].Code == text) {
                            tempResult = $scope.currency[i];
                            break;
                        }
                    }
                    $scope.NewExchangeRateCapture.FromCurrency = '';
                } else {
                    if (!$scope.data.IsExchangeRateEdit || $scope.data.ExchangeRateCaptures[$index].IsLock || !$scope.data.ExchangeRateCaptures[$index].editing) {
                        return;
                    }
                    for (var i = 0; i < $scope.currency.length; i++) {
                        if ($scope.currency[i].Code == text) {
                            tempResult_old = $scope.currency[i];
                            break;
                        }
                    }
                    
                    $scope.data.ExchangeRateCaptures[$index].FromCurrency = '';
                }
            }



            //auto complete exchange type
            var tempResult_exchange_type;
            var tempResult_old_exchange_type;
            $scope.NewExchangeRateCapture_searchText = {
                text:''
            };
            $scope.querySearchItem_exchange_type = function (query) {
                if (!angular.isDefined($scope.exchangeType) || !angular.isDefined(query)) return [];
                var results = $filter('filter')($scope.exchangeType, { DescriptionTH: query });
                return results;
            }
            $scope.validateExchangeType = function (text, $index, isNew) {
                if (!angular.isDefined($scope.exchangeType)) return;
                var result = null;
                for (var i = 0; i < $scope.exchangeType.length; i++) {
                    if ($scope.exchangeType[i].DescriptionTH == text) {
                        result = $scope.exchangeType[i];
                    }
                }
                if (isNew) {
                    if (result) {
                        $scope.NewExchangeRateCapture_searchText.text = result.DescriptionTH;
                        $scope.NewExchangeRateCapture.ExchangeTypeID = result.ExchangeTypeID;
                    } else if (tempResult_exchange_type) {
                        $scope.NewExchangeRateCapture_searchText.text = tempResult_exchange_type.DescriptionTH;
                        $scope.NewExchangeRateCapture.ExchangeTypeID = tempResult_exchange_type.ExchangeTypeID;
                    }
                } else {
                    if (result) {
                        $scope.data.ExchangeRateCaptures[$index].searchText = result.DescriptionTH;
                        $scope.data.ExchangeRateCaptures[$index].ExchangeTypeID = result.ExchangeTypeID;
                    } else if (tempResult_old_exchange_type) {
                        $scope.data.ExchangeRateCaptures[$index].searchText = tempResult_old_exchange_type.DescriptionTH;
                        $scope.data.ExchangeRateCaptures[$index].ExchangeTypeID = tempResult_old_exchange_type.ExchangeTypeID;
                    }
                }
                return;
             
            }
            $scope.tryToSelectExchangeType = function (text, $index, isNew) {
                if (!$scope.data.IsExchangeRateEdit) {return;}
                if (isNew) {
                    for (var i = 0; i < $scope.exchangeType.length; i++) {
                        if ($scope.exchangeType[i].DescriptionTH == text) {
                            tempResult_exchange_type = $scope.exchangeType[i];
                            break;
                        }
                    }
                    $scope.NewExchangeRateCapture_searchText.text = '';
                } else {
                    if (!$scope.data.IsExchangeRateEdit || $scope.data.ExchangeRateCaptures[$index].IsLock || !$scope.data.ExchangeRateCaptures[$index].editing) {
                        return;
                    }
                    for (var i = 0; i < $scope.exchangeType.length; i++) {
                        if ($scope.exchangeType[i].DescriptionTH == text) {
                            tempResult_old_exchange_type = $scope.exchangeType[i];
                            break;
                        }
                    }
                    $scope.data.ExchangeRateCaptures[$index].searchText = '';
                }
            }

            function isContainInCurrencyList(currency) {
                var results = $filter('filter')($scope.currency, { Code: currency },true);
                if (results.length == 0) {
                    return false;
                }
                return true;

            }

            $scope.currentEditingCurrencyIndex = -1;
            $scope.currencyTemp;
            $scope.editCurrency = function (row, index) {
                $scope.currentEditingCurrencyIndex = index;
                $scope.edittingCurrency = true;
                row.editing = true;
                $scope.currencyTemp = angular.copy(row);
                $scope.setBlockAction(true, $scope.Text['EXPENSE']['PLEASE_SAVE_CURRECY_BEFORE']);
            }

            $scope.saveCurrency = function (row, index) {
                if (!isCurrencyValid(row, false, index)) {
                    return;
                }
                $scope.currentEditingCurrencyIndex = -1;
                $scope.edittingCurrency = false;
                row.editing = false;
                $scope.setBlockAction(false, '');
                if (row.ExchangeRate != null && !isNaN(Number(row.ExchangeRate))) {
                    $scope.CalculateTravelReport(true);
                }
            }
            $scope.undoCurrency = function (row, index,arr) {
                $scope.currentEditingCurrencyIndex = -1;
                $scope.edittingCurrency = false;
                row.editing = false;
                $scope.setBlockAction(false, '');
                arr[index] = angular.copy($scope.currencyTemp);

            }
            
            function isCurrencyValid(row,isNew,index) {
                var exchangeType = null;
                for (var i = 0; i < $scope.exchangeType.length; i++) {
                    if (row.ExchangeTypeID == $scope.exchangeType[i].ExchangeTypeID) {
                        exchangeType = $scope.exchangeType[i];
                        break;
                    }
                }
                if (row.FromCurrency == '') {
                    alertMessage($scope.Text['EXPENSE']['EXCHANGERATECAPTURE_NO_EXCHANGETYPE']);
                    return false;
                }


                // check attached file if the Exchange type Id is 2 (Id of ExchangeTypeAttachFile)
                if (row.ExchangeTypeID == $scope.ExchangeTypeAttachFile) {
                    if (row.ExchangeRateCaptureFileList == null || row.ExchangeRateCaptureFileList.length <= 0) {
                        alertMessage($scope.Text['EXPENSE']['PLEASE_ATTACH_EXCHANGE_FILE']);
                        return false;
                    }
                }

                // exhcange rate not equal to 1
                if (row.ExchangeRate == 1) {
                    alertMessage($scope.Text['EXPENSE']['EXCHANGE_CANNOT_BE_1']);
                    return false;
                }
                // exhcange rate not equal to 0
                if (row.ExchangeRate == 0 || row.ExchangeRate == '') {
                    alertMessage($scope.Text['EXPENSE']['EXCHANGE_CANNOT_BE_0_EMPTY']);
                    return false;
                }

                if (!isContainInCurrencyList(row.FromCurrency)) {
                    alertMessage($scope.Text['EXPENSE']['EXCHANGE_THERE_IS_NO_THIS_CURRECY_IN_SYSTEM']);
                    return false;
                }
                var strExrate = row.ExchangeRate + '';
                var exRate = strExrate.split('.');

                if (exRate.length == 2) {
                    if (exRate[1].length > 5) {
                        alertMessage($scope.Text['EXPENSE']['DECIMAL_POINT_SHOULD_LESS_THAN_5']);
                        return false;
                    }
                }
                

                // not found validate type
                if (exchangeType == null) {
                    // to do create validate and message popup for add new row and edit row for Exchange Rate
                    alertMessage("Excahnge Type not found in validation")
                } 
                else {
                    var isDupExTyp = false;
                    var isDupCur = false;
                    //var isDupCurAndExRate = false;
                    var isDupExEff = false;
                    var isDupExRat = false;
                    var errTxt = '';
                    
                    if (!exchangeType.AllowDupplicateExchangeType) {
                        for (var i = 0; i < $scope.data.ExchangeRateCaptures.length; i++) {
                            if ((index != i || isNew) && $scope.data.ExchangeRateCaptures[i].ExchangeTypeID == row.ExchangeTypeID) {
                                alertMessage($scope.Text['EXPENSE']['VALIDATION_DUPPLICATED_EXCHANGE_TYPE']);
                                return false;;
                            }
                        }
                    }
                    if(!exchangeType.AllowDupplicateCurrency){
                        for (var i = 0; i < $scope.data.ExchangeRateCaptures.length; i++) {
                            if ((index != i || isNew) && $scope.data.ExchangeRateCaptures[i].FromCurrency == row.FromCurrency && $scope.data.ExchangeRateCaptures[i].ExchangeTypeID == row.ExchangeTypeID) {
                                //alertMessage($scope.Text['EXPENSE']['VALIDATION_DUPPLICATED_CURRENCY']);
                                errTxt += ' "'+$scope.Text['EXPENSE']['CURRENCY'] + ','
                                isDupCur = true;
                                break;
                            }
                        }
                    }
                    if(!exchangeType.AllowDupplicateEffectiveDate){
                        for (var i = 0; i < $scope.data.ExchangeRateCaptures.length; i++) {
                            if ((index != i || isNew) && $scope.data.ExchangeRateCaptures[i].EffectiveDate == row.EffectiveDate && $scope.data.ExchangeRateCaptures[i].ExchangeTypeID == row.ExchangeTypeID) {
                                //alertMessage($scope.Text['EXPENSE']['VALIDATION_DUPPLICATED_EFFECTIVE_DATE']);
                                errTxt += ' "' + $scope.Text['EXPENSE']['DATE'] + ','
                                isDupExEff = true;
                                break;
                            }
                        }
                    }
                    if(!exchangeType.AllowDupplicateExchangeRate){
                        for (var i = 0; i < $scope.data.ExchangeRateCaptures.length; i++) {
                            if ((index != i || isNew) && $scope.data.ExchangeRateCaptures[i].ExchangeRate == row.ExchangeRate && $scope.data.ExchangeRateCaptures[i].ExchangeTypeID == row.ExchangeTypeID) {
                                //alertMessage($scope.Text['EXPENSE']['VALIDATION_DUPPLICATED_EXCHANGE_RATE']);
                                errTxt += ' "' + $scope.Text['EXPENSE']['EXCHANGERATE'] + ','
                                isDupExRat = true;
                                break;
                            }
                        }
                    }
                }


                if (!exchangeType.AllowDupplicateCurrency == isDupCur  &&
                    !exchangeType.AllowDupplicateEffectiveDate == isDupExEff &&
                    !exchangeType.AllowDupplicateExchangeRate == isDupExRat ) {
                    errTxt = errTxt.slice(0, -1);
                    var txtErr = $scope.Text['EXPENSE']['VALIDATION_DUPPLICATED_TXT1'];
                    txtErr = txtErr.replace("%%%", errTxt);
                    alertMessage(txtErr);
                    return false;
                }
                return true;
            }

            function alertMessage(text) {
                $mdDialog.show(
                    $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('WARNING')
                    .textContent(text)
                    .ok('OK')
                );
            }
            $scope.getDateFormate = function (date) {
                if (!date) return;
                 var formattedDate = moment(date).format('DD/MM/YYYY');
                return formattedDate;
                //return date.toLocaleDateString();
            };
            $scope.getDateObj = function (row) {
                var date = new Date(row.EffectiveDate);
                row.EffectiveDate_date_type = date;
            }
            $scope.onChangeNewSelectedEffectiveDate_override = function (row, newDate) {
                row.EffectiveDate = moment(newDate).format('DD/MM/YYYY');
                //row.EffectiveDate = newDate.toLocaleDateString();
                $scope.onChangeNewSelectedEffectiveDate(row, newDate)
            };
            $scope.onChangeEffectiveDate_override = function (row, newDate) {
                row.EffectiveDate = moment(newDate).format('DD/MM/YYYY');
                //row.EffectiveDate = newDate.toLocaleDateString();
                $scope.onChangeEffectiveDate(row, newDate)
            };
            $scope.getExchnageRateDescription = function (row, $index, isNew) {
                var waiting_excahnge_type = $interval(function () {
                    if (angular.isDefined($scope.exchangeType)) {
                        $interval.cancel(waiting_excahnge_type);
                        var results = $filter('filter')($scope.exchangeType, { ExchangeTypeID: row.ExchangeTypeID });
                        if (results.length > 0) {
                            row.searchText = results[0].DescriptionTH;
                        }
                        else {
                            row.searchText = '';
                        }
                    }
                }, 200);
            }
            $scope.AllowUploadFile = function (fileName) {
                return CONFIG.FILE_SETTING.ALLOW_FILE.test(fileName);
            }
            $scope.AllowUploadFileType = function (fileName) {
                return CONFIG.FILE_SETTING.ALLOW_FILETYPE.indexOf(fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase()) >= 0;
            }
            $scope.AllowFileNameLength = function (fileNameLength) {
                return CONFIG.FILE_SETTING.ALLOW_FILENAME_LENGTH >= fileNameLength;
            }
            $scope.AllowFileSize = function (fileSize) {
                return CONFIG.FILE_SETTING.ALLOW_FILESIZE >= fileSize;
            }
            $scope.AllowFileZeroSize = function (fileSize) {
                return fileSize > 0;
            }

        }]);

})();



