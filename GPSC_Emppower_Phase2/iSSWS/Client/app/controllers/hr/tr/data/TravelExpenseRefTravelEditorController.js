﻿(function () {
    angular.module('ESSMobile').controller('TravelExpenseRefTravelEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG','$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {
        $scope.TravelRefNo;
        $scope.SearchTravelGroupRequestGetForReference = function () {
            var URL = CONFIG.SERVER + 'HRTR/SearchTravelGroupRequestGetForReference';
            var oRequestParameter = { CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.document.Requestor
            };
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                $scope.TravelGroupRequestGetForReference = response.data;
                $scope.selectItem = "1";
                //if (response.data != null) {
                //    $scope.document.Additional.TravelRequestNo = response.data[0].RequestNo;
                //}
                if ($scope.document.Additional.TravelRequestNo != null && $scope.document.Additional.TravelRequestNo != "")
                {
                    $scope.selectedItemChange($scope.document.Additional.TravelRequestNo);
                    $scope.autcomplete_travelRef.searchText = $scope.document.Additional.TravelRequestNo;
                    $scope.TravelRefNo = $scope.document.Additional.TravelRequestNo;
                }
                else
                {
                    $scope.selectedItemChange($scope.TravelGroupRequestGetForReference[0]);
                    $scope.autcomplete_travelRef.searchText = $scope.TravelGroupRequestGetForReference[0];
                    $scope.TravelRefNo = $scope.TravelGroupRequestGetForReference[0];
                    $scope.data.TravelSeqEdit = true;
                }
                $scope.SearchTravelSchedulePlace();
                
            }, function errorCallback(response) {
                // Error
                console.log('error TravelExpense SearchTravelGroupRequestGetForReference', response);

            });

        };
       
        $scope.SearchTravelSchedulePlace = function () {
            var URL = CONFIG.SERVER + 'HRTR/SearchTravelExpenseDetail';//var URL = CONFIG.SERVER + 'HRTR/GetTravelRequest';
            var oRequestParameter = {
                InputParameter: { "REQUESTNO": $scope.TravelRefNo },
                CurrentEmployee: $scope.employeeData,
                Requestor: $scope.document.Requestor                
            };
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                // Success
                /*
                $scope.data.TravelSchedulePlaces = response.data.TravelSchedulePlaces;
                $scope.document.Additional.TravelSchedulePlaces = response.data.TravelSchedulePlaces;
                $scope.document.Additional.BeginDate = response.data.TravelSchedulePlaces[0].BeginDate;
                $scope.document.Additional.EndDate = response.data.TravelSchedulePlaces[response.data.TravelSchedulePlaces.length - 1].EndDate;
                */
                $scope.data.TravelSchedulePlaces = response.data;
                $scope.document.Additional.TravelSchedulePlaces = response.data;
                $scope.document.Additional.BeginDate = response.data[0].BeginDate;
                $scope.document.Additional.EndDate = response.data[response.data.length - 1].EndDate;
            }, function errorCallback(response) {
                // Error
                console.log('error TravelExpense SearchTravelGroupRequestGetForReference', response);

            });
        }

        var tempResult;
        $scope.autcomplete_travelRef = {
            searchText: ''
        };
        $scope.tryToSelectRefTravelGroupRequest = function (text) {
            $scope.autcomplete_travelRef.searchText = '';
            for (var i = 0; i < $scope.TravelGroupRequestGetForReference.length; i++) {
                if ($scope.TravelGroupRequestGetForReference[i] == text) {
                    tempResult = $scope.TravelGroupRequestGetForReference[i];
                    break;
                }
            }
           
        }
        $scope.checkTextRefTravelGroupRequest = function (text) {
            var result = null;
            for (var i = 0; i < $scope.TravelGroupRequestGetForReference.length; i++) {
                if ($scope.TravelGroupRequestGetForReference[i] == text) {
                    result = $scope.TravelGroupRequestGetForReference[i];
                    $scope.TravelRefNo = $scope.TravelGroupRequestGetForReference[i];
                    break;
                }
            }
            if (result) {
                $scope.selectedItemChange(result);

            } else if (tempResult) {
                $scope.selectedItemChange(tempResult);

            }
            //$scope.SearchTravelSchedulePlace();
        }
        $scope.selectedItemChange = function (result) {
            if (result != null) {
                //if ($scope.document.Additional.TravelRequestNo != null && $scope.document.Additional.TravelRequestNo != "") {
                //    $scope.document.Additional.TravelRequestNo = result.TravelRequestNo;//result.RequestNo;
                //    $scope.autcomplete_travelRef.searchText = result.TravelRequestNo;//result.RequestNo;
                //}
                //else
                //{
                    //if (result.TravelRequestNo != null) {
                    //    $scope.autcomplete_travelRef.searchText = result.TravelRequestNo;
                    //    $scope.document.Additional.TravelRequestNo = result.TravelRequestNo;
                    //    $scope.TravelRefNo = result.RequestNo;
                    //    $scope.SearchTravelSchedulePlace();
                    //}
                    //else {
                        $scope.autcomplete_travelRef.searchText = result;
                        $scope.document.Additional.TravelRequestNo = result;
                        $scope.TravelRefNo = result;
                        $scope.SearchTravelSchedulePlace();
                    //}
                    
                //}
                return;
            }
            $scope.autcomplete_travelRef.searchText = '';
        }
       
        $scope.querySearchRefTravelGroupRequest = function (item) {
            if (!$scope.TravelGroupRequestGetForReference) return;
            var results = item ? $scope.TravelGroupRequestGetForReference.filter(createFilterSearch(item)) : $scope.TravelGroupRequestGetForReference, deferred;
            results = angular.copy($scope.limMaxArr(results,20));
            //var results = query; createFilterSearch
            //if (results.length == 0) {
            //    $scope.selectedItem = null;
            //    $scope.autcomplete_travelRef.searchText = '';
            //}
            if ($scope.simulateQuery) {
                deferred = $q.defer();
                $timeout(function () { deferred.resolve(results); }, Math.random() * 50, false);
                return deferred.promise;
            } else {
                return results;
            }
        }

        function createFilterSearch(query) {
            if (!angular.isDefined(query)) return false;
            var lowercaseQuery = angular.lowercase(query);
            return function filterFn(item) {
                var textquery = angular.lowercase(item);
                return (textquery.indexOf(lowercaseQuery) >= 0);
            };
        }

        }]);
})();


