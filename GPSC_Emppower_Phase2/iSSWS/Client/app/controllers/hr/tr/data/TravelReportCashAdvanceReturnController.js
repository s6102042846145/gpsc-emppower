﻿(function () {
    angular.module('ESSMobile')
        .controller('TravelReportCashAdvanceReturnController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {
            // console.log('PersonalCashAdvance', $scope.PersonalCashAdvance);
            
            
            var oReturnID = 1;
            $scope.addRow = function () {
                var date = new Date();
                var dateStr = date.getFullYear() + '-' + ("0" + (date.getMonth() + 1)).slice(-2) + '-' + ("0" + date.getDate()).slice(-2);
                //alert(dateStr);
                if (angular.isUndefined($scope.data.CashAdvanceReturns) || $scope.data.CashAdvanceReturns == null) {
                    $scope.data.CashAdvanceReturns = [];
                }
                $scope.data.CashAdvanceReturns.push({
                    "RequestNo": $scope.document.RequestNo,
                    "EmployeeID": $scope.data.CashAdvance.EmployeeID,
                    "ReturnID": oReturnID,
                    "ReturnDate": $filter('date')(new Date(), 'yyyy-MM-ddT00:00:00'),
                    "ReturnAmount": 0,
                    "FIDocID": '',
                    "FileAttachment": "\\" + $scope.document.RequestNo + "\\" + $scope.data.CashAdvance.EmployeeID + "\\" + oReturnID + "\\",
                    "FileName": '',
                    "FileStringBase64": ''
                });
                oReturnID++;
                console.log('addRow', $scope.data.CashAdvanceReturns);
            };

            $scope.removeRow = function (index) {
                if (confirm($scope.Text['SYSTEM']['CONFIRM_DELETE'])) {
                    $scope.data.CashAdvanceReturns.splice(index, 1);
                    oReturnID = oReturnID - 1;
                    console.log('removeRow', $scope.data.CashAdvanceReturns);
                }
            };

            $scope.onAfterValidateFunction = function (event, fileList) {
                var rowNo = $(event.target).attr('data-row-id');
                rowNo = Number(rowNo);
                var newFile = fileList[0];
                if ($scope.AllowUploadFile(newFile.filename))
                {
                    $mdDialog.show(
                       $mdDialog.alert()
                       .clickOutsideToClose(false)
                       .title($scope.Text['SYSTEM']['WARNING'])
                       .textContent($scope.Text['SYSTEM']['VALIDATE_FILENAME'])
                       .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else if (!$scope.AllowUploadFileType(newFile.filename))
                {
                    $mdDialog.show(
                        $mdDialog.alert()
                        .clickOutsideToClose(false)
                        .title($scope.Text['SYSTEM']['WARNING'])
                        .textContent($scope.Text['SYSTEM']['INVALIDFILETYPE'])
                        .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else if (!$scope.AllowFileNameLength(newFile.filename.length))
                {
                    $mdDialog.show(
                        $mdDialog.alert()
                        .clickOutsideToClose(false)
                        .title($scope.Text['SYSTEM']['WARNING'])
                        .textContent($scope.Text['SYSTEM']['SAP_FILENAME_LENGTH_INVALID'])
                        .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else if (!$scope.AllowFileSize(newFile.filesize))
                {
                    $mdDialog.show(
                        $mdDialog.alert()
                        .clickOutsideToClose(false)
                        .title($scope.Text['SYSTEM']['WARNING'])
                        .textContent($scope.Text['SYSTEM']['INVALIDFILESIZE'])
                        .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else if (!$scope.AllowFileZeroSize(newFile.filesize)) {
                    $mdDialog.show(
                        $mdDialog.alert()
                        .clickOutsideToClose(false)
                        .title($scope.Text['SYSTEM']['WARNING'])
                        .textContent($scope.Text['SYSTEM']['INVALIDFILESIZE2'])
                        .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else
                {
                    if (rowNo >= 0) {
                        $scope.data.CashAdvanceReturns[rowNo].FileName = fileList[0].filename;
                        $scope.data.CashAdvanceReturns[rowNo].FileStringBase64 = fileList[0].base64;
                        return;
                    } else {
                        alert('error row number.');
                    }
                }
                newFile = null;
                $scope.data.CashAdvanceReturns[rowNo].FileName = '';
                $scope.data.CashAdvanceReturns[rowNo].FileStringBase64 = '';
            };

            $scope.onBlurReturnAmount = function (row) {
                if (row.ReturnAmount == null || row.ReturnAmount == '' || isNaN(row.ReturnAmount) || row.ReturnAmount < 0) {
                    row.ReturnAmount = 0;
                }
            };

            $scope.onChangeReturnAmount = function (row) {
                if (row.ReturnAmount != null && !isNaN(row.ReturnAmount) && row.ReturnAmount < 0) {
                    row.ReturnAmount = 0;
                }
            };

            $scope.onBlurFIDocID = function (row) {
                // allow only string numeric (use with string only)
                if ((row.FIDocID != null && row.FIDocID != '' && isNaN(row.FIDocID)) || row.FIDocID.indexOf('.') >= 0 || row.FIDocID.indexOf('-') >= 0 || row.FIDocID.indexOf('+') >= 0) {
                    row.FIDocID = '';
                }
            };

            $scope.SetSelectedReturnDate = function (selectedDate,$index) {
                $scope.data.CashAdvanceReturns[$index].ReturnDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };

            $scope.AllowFileSize = function (fileSize) {
                return CONFIG.FILE_SETTING.ALLOW_FILESIZE >= fileSize;
            }
            $scope.AllowFileZeroSize = function (fileSize) {
                return fileSize > 0;
            }
        }]);

})();