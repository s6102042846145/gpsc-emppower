﻿(function () {
    angular.module('ESSMobile')
        .controller('TravelRequestPettyCashRefTravelController', ['$scope', '$http', '$routeParams', '$location', '$window', '$filter', 'CONFIG', '$q', '$timeout', '$mdDialog', function ($scope, $http, $routeParams, $location, $window, $filter, CONFIG, $q, $timeout, $mdDialog) {

            $scope.Textcategory = 'EXPENSE';
            $scope.PettyCashList = [];
            $scope.provinceSelect = true;
            $scope.IsEditCountry = false;
            $scope.formData = { EnableIsCustodian: '', PettyCode: '', PettyName: '', PettyCashAmount: '' };
            $scope.data.isReadonly = false;

            $scope.InitialConfig = function () {
                var URL = CONFIG.SERVER + 'HRTR/SearchPettyCashByOrgUnit';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    if (response.data.length > 0) {
                        $scope.PettyCashList = response.data;
                        var result = null;
                        var pageMode = "add";

                        if ($scope.document.RequestTypeID == 116) {
                            //For Report Travel With Petty Cash
                            if ($scope.document.Additional.PettyCode && $scope.document.Additional.PettyName ) {
                                pageMode = "edit";
                                $scope.formData.PettyCashCode = $scope.document.Additional.PettyCode;
                                $scope.formData.PettyCashName = petteyCashName($scope.document.Additional.PettyCode);
                                $scope.data.isReadonly = true;
                            }
                            else {
                                pageMode = "edit";
                                $scope.formData.PettyCashCode = $scope.PettyCashList[0].PettyCashCode;
                                $scope.formData.PettyCashName = $scope.PettyCashList[0].PettyCashName;
                                $scope.data.isReadonly = true;
                            }
                        }
                        else if ($scope.document.RequestTypeID == 105) {
                            //For Travel Request With Petty Cash
                            if ($scope.document.Additional.TravelGroupRequest[0].Travelers[0].oPettyCash.PettyCode && $scope.document.Additional.Travelers[0].oPettyCash.PettyName) {
                                pageMode = "edit";
                                $scope.formData.PettyCashCode = $scope.document.Additional.Travelers[0].oPettyCash.PettyCode;
                                $scope.formData.PettyCashName = $scope.document.Additional.Travelers[0].oPettyCash.PettyName;
                                $scope.data.isReadonly = true;
                            }
                        }
                        else {
                            //For General Expense (Ref, Petty Cash)
                            if ($scope.document.Additional.PettyCode != "" && $scope.document.Additional.PettyName != "") {
                                pageMode = "edit";
                                $scope.formData.PettyCashCode = $scope.document.Additional.PettyCode;
                                $scope.formData.PettyCashName = petteyCashName($scope.document.Additional.PettyCode);
                                $scope.data.isReadonly = true;
                            }
                        }

                        if (pageMode == "edit") {
                            for (var i = 0; i < $scope.PettyCashList.length; i++) {
                                if ($scope.PettyCashList[i].PettyCashCode == $scope.formData.PettyCashCode) {
                                    result = $scope.PettyCashList[i];
                                    break;
                                }
                            }
                            if (result != null) { $scope.selectedItemChange(result); }
                        } else {
                            $scope.data.isReadonly = true;
                            $scope.selectedItemChange($scope.PettyCashList[0]);
                        }
                    }
                },
                function errorCallback(response) {
                    console.log('error InitialConfig.', response);
                });
            }

            function petteyCashName(code) {
                for (var i = 0; i < $scope.PettyCashList.length ; i++) {
                    if ($scope.PettyCashList[i].PettyCashCode === code) {
                        return $scope.PettyCashList[i].PettyCashName;
                        break;
                    }
                }
                return 'XXXXXX'
            }

            //#region ********************  auto complete petty cash start ******************* */
            $scope.simulateQuery = true;
            $scope.searchText_pettycash = {
                text:''
            };
            $scope.autocomplete = {
                selectedItem: null
            }

            //Add New by Nipon Supap 16-02-2017 14:38 PM
            $scope.PettyCustodianGetByCode = function () {
                var URL = CONFIG.SERVER + 'HRTR/SearchPettyCustodianGetByCode';
                var oRequestParameter = {
                    CurrentEmployee: $scope.employeeData
                        , Requestor: $scope.requesterData,
                    InputParameter: {
                        PettyCashCode: $scope.data.PettyCode,
                        PettyCashName: $scope.data.PettyName
                    }
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.formData.EnableIsCustodian = response.data[0]["IsCustodian"];
                    $scope.formData.PettyCashAmount = response.data[0]["PettyAmount"];
                    $scope.formData.PettyCashCode = $scope.data.PettyCode;
                    $scope.formData.PettyCashName = $scope.data.PettyName;
                }, function errorCallback(response) {
                    // Error
                    console.log('error TravelRequest SearchPettyCustodianGetByCode', response);

                });
            };

            $scope.querySearchItem = function (query) {
                if (!$scope.PettyCashList) return;
                var results = query ? $scope.PettyCashList.filter(createFilterFor(query)) : $scope.PettyCashList, deferred;
                if (results.length == 0) {
                    $scope.selectedItem_PrettyCash = null;
                    $scope.searchText_pettycash.text = '';
                }
                return results;
            }

            $scope.selectedItemChange = function (item) {
                if (item) {
                    $scope.selectedItem_PrettyCash = item;
                    $scope.data.PettyCode = item.PettyCashCode;
                    $scope.data.PettyName = item.PettyCashName;
                    $scope.PettyCustodianGetByCode();
                }                
            }
            function createFilterFor(query) {
                if (!angular.isDefined(query)) return false;
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(item) {
                    var textquery = angular.lowercase(item.PettyCashCode + " : " + item.PettyCashName);
                    return (textquery.indexOf(lowercaseQuery) >= 0);
                };
            }
            var tempResult;
            $scope.tryToSelect = function (text) {
                for (var i = 0; i < $scope.PettyCashList.length; i++) {
                    if ($scope.PettyCashList[i].PettyCashCode + " : " + $scope.PettyCashList[i].PettyCashName == text) {
                        tempResult = $scope.PettyCashList[i];
                        break;
                    }
                }
                $scope.searchText_pettycash.text = '';
            }
            $scope.checkText = function (text) {
                var result = null;
                for (var i = 0; i < $scope.PettyCashList.length; i++) {
                    if ($scope.PettyCashList[i].PettyCashCode + " : " + $scope.PettyCashList[i].PettyCashName == text) {
                        result = $scope.PettyCashList[i];
                        break;
                    }
                }
                if (result) {
                    $scope.searchText_pettycash.text = result.PettyCashCode + " : " + result.PettyCashName;
                    $scope.selectedItemChange(result);
                } else if (tempResult) {
                    $scope.searchText_pettycash.text = tempResult.PettyCashCode + " : " + tempResult.PettyCashName;
                    $scope.selectedItemChange(tempResult);
                }
            }
            //#endregion ********************  auto complete petty cash end ******************* */

        }]);

})();


