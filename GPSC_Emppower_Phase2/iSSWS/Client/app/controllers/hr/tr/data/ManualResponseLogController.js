﻿(function () {
    angular.module('ESSMobile')
        .controller('ManualResponseLogController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {

            $scope.Textcategory = 'TRAVELREQUEST';
            $scope.MessageDesc = "";

            $scope.AddManualLogResponse = function () {
                var URL = CONFIG.SERVER + 'HRTR/SavePostExpenseResponse';
                var oRequestParameter = {
                    InputParameter: { "REQUESTNO": $scope.document.RequestNo, "MessageDesc": $scope.MessageDesc },
                    CurrentEmployee: $scope.employeeData,
                    Requestor: $scope.document.Requestor
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.MessageDesc = "";
                    $scope.LoadResponseLogManual();
                }, function errorCallback(response) {
                    console.log('error TravelExpense SavePostExpenseResponse', response);

                });
            }
        }]);

})();