﻿(function () {
    angular.module('ESSMobile')
        .controller('TravelExpenseGroupEstimateEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$timeout', '$q', '$log', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $timeout, $q, $log) {

            var RequestNo = $scope.document.RequestNo;
            //GroupBudgets
            $scope.Textcategory = 'EXPENSE';
            $scope.employeeData = getToken(CONFIG.USER);
            $scope.TravelerIndex = 0;
            $scope.LimitRows = 15;
            $scope.simulateQuery = false;
            $scope.data.HideGroupExpenseAddButton = false;
            $scope.data.IsNewGroupEstimate = {};
            $scope.objOrganizationDefault = {};
            $scope.addRow = function () {
                var oTravelExpenseGroup = angular.copy($scope.data.tmpGroupBudget);

                oTravelExpenseGroup.GroupBudgetID = $scope.guid();
                oTravelExpenseGroup.IsAlternativeIOOrg = false;
                oTravelExpenseGroup.IsAlternativeCostCenter = false;
                tempResult_cc = $scope.objCostcenterDistribution[0];
                oTravelExpenseGroup.CostCenter = tempResult_cc.CostCenterCode;
                oTravelExpenseGroup.CostCenterName = tempResult_cc.LongDesc;
                oTravelExpenseGroup.searchCostCenterText = tempResult_cc.CostCenterCode + ' : ' + tempResult_cc.LongDesc;
                oTravelExpenseGroup.IO = '';
                oTravelExpenseGroup.IOName = '';
                tempResult_org = $scope.objOrganization[$scope.objOrganization.findIndexWithAttr('ObjectID', $scope.data.Travelers[$scope.TravelerIndex].OrgUnitID)];
                $scope.objOrganizationDefault = tempResult_org;
                oTravelExpenseGroup.AlternativeIOOrg = tempResult_org.ObjectID;
                oTravelExpenseGroup.AlternativeIOOrgName = tempResult_org.Text;
                oTravelExpenseGroup.searchAlternativeIOOrgText = tempResult_org.ObjectID + ' : ' + tempResult_org.Text;

                if (angular.isDefined($scope.ExpenseTypeGroup) && $scope.ExpenseTypeGroup.length > 0) {
                    tempResult_etg = $scope.ExpenseTypeGroup[0];
                    oTravelExpenseGroup.ExpenseTypeGroupID = $scope.ExpenseTypeGroup[0].ExpenseTypeGroupID;
                    oTravelExpenseGroup.Name = $scope.ExpenseTypeGroup[0].Name;
                    oTravelExpenseGroup.Remark = $scope.ExpenseTypeGroup[0].Remark;
                    oTravelExpenseGroup.searchExpenseTypeGroupText = $scope.ExpenseTypeGroup[0].Remark;
                }

                $scope.data.GroupBudgets.push(oTravelExpenseGroup);
                $scope.DefaultIO($scope.data.GroupBudgets.length - 1);
                $scope.data.IsNewGroupEstimate[$scope.data.GroupBudgets.length - 1] = true;
                var temp = angular.copy($scope.data.GroupBudgets[$scope.data.GroupBudgets.length - 1]);
                $scope.OnAddGroupCashAdvance(temp);
            };
            $scope.InitialConfig = function () {
                $scope.TravelerIndex = $scope.GetEditTraveler($scope.employeeData.RequesterEmployeeID);
            }
            $scope.DefaultIO = function (index) {
                var tempObjIOList = angular.copy($scope.objIO.filter(createFilterForIO(index)));
                if ($scope.objIO.length && $scope.objIO[0] && $scope.objIO[0].OrderID == '999999999999') {
                    tempObjIOList = angular.copy($scope.objIO);
                }
                if (tempObjIOList.length > 0) {
                    tempResult_io = tempObjIOList[0];
                    if (tempResult_io) {
                        $scope.data.GroupBudgets[index].IO = tempResult_io.OrderID;
                        $scope.data.GroupBudgets[index].IOName = tempResult_io.Description;
                        $scope.data.GroupBudgets[index].searchIOText = tempResult_io.OrderID + ' : ' + tempResult_io.Description;
                    }
                }
            }
            $scope.removeRow = function (index) {
                $scope.data.GroupBudgets.splice(index, 1);
                $scope.data.Travelers[$scope.TravelerIndex].oCashAdvance.GroupCashAdvances.splice(index, 1);
            };
            $scope.ChangeExpenseTypeGroup = function (i) {
                if ($scope.data.Travelers[$scope.TravelerIndex].oCashAdvance.GroupCashAdvances && $scope.data.Travelers[$scope.TravelerIndex].oCashAdvance.GroupCashAdvances.length > 0) {
                    if ($scope.data.GroupBudgets[i].ExpenseTypeGroupID != $scope.data.Travelers[$scope.TravelerIndex].oCashAdvance.GroupCashAdvances[i].ExpenseTypeGroupID) {
                        $scope.data.Travelers[$scope.TravelerIndex].oCashAdvance.GroupCashAdvances[i].ExpenseTypeGroupID = $scope.data.GroupBudgets[i].ExpenseTypeGroupID;
                        $scope.data.Travelers[$scope.TravelerIndex].oCashAdvance.GroupCashAdvances[i].Remark = $scope.data.GroupBudgets[i].Remark;
                    }
                    if ($scope.data.Travelers[$scope.TravelerIndex].oCashAdvance.GroupCashAdvances[i].Amount != $scope.data.GroupBudgets[i].TotalAmount) {
                        $scope.data.Travelers[$scope.TravelerIndex].oCashAdvance.GroupCashAdvances[i].Amount = $scope.data.GroupBudgets[i].TotalAmount;
                        $scope.data.Travelers[$scope.TravelerIndex].oCashAdvance.GroupCashAdvances[i].TotalAmount = $scope.data.GroupBudgets[i].TotalAmount;
                    }
                }

                $scope.data.IsNewGroupEstimate[i] = false;
            };

            $scope.getTotal = function () {
                var total = 0;
                if ($scope.data.GroupBudgets) {
                    total = $scope.data.GroupBudgets.sum('TotalAmount');
                }
                //for (var i = 0; i < $scope.data.GroupBudgets.length; i++) {
                //    if (angular.isString($scope.data.GroupBudgets[i].TotalAmount)) {
                //        total += parseFloat($scope.data.GroupBudgets[i].TotalAmount.replace(',', ''));
                //    }
                //    else {
                //        total += parseFloat($scope.data.GroupBudgets[i].TotalAmount);
                //    }
                //}
                if (angular.isDefined($scope.data.Travelers) && $scope.data.Travelers != null && $scope.data.Travelers.length > 0) {
                    $scope.data.Estimate = $scope.data.Travelers.sum('TotalAmount') + total;
                }
                return total;
            }
            $scope.LimitData = function (objArray) {
                var oResult = [];
                if (objArray != null && objArray && objArray.length > 0) {
                    var oMaxLength = (objArray.length > $scope.LimitRows) ? $scope.LimitRows : objArray.length;
                    /*for (var i = 0; i < oMaxLength; i++) {
                        oResult.push(objArray[i]);
                    }*/
                    oResult = angular.copy(objArray).splice(0, oMaxLength);
                }
                return oResult;
            }

            $scope.objCostcenter = angular.copy($scope.settings.Master.CostCenterList);
            $scope.objCostcenterDistribution = angular.copy($scope.settings.Master.CostcenterDistributionList);
            $scope.objOrganization = angular.copy($scope.settings.Master.OrgUnitList);
            $scope.objIO = angular.copy($scope.settings.Master.IOList);
            
            /*
            //Get CostCenter
            var URL = CONFIG.SERVER + 'HRTR/GetCostCenter';
            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                $scope.objCostcenter = response.data;
                $scope.objCostcenterBuffer = $scope.LimitData($scope.objCostcenter)
                //Get CostCenter Distribution
                URL = CONFIG.SERVER + 'HRTR/GetCostCenterDistribution';
                oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.objCostcenterDistribution = response.data;

                    //Get Organization
                    URL = CONFIG.SERVER + 'HRTR/GetOrganization';
                    oRequestParameter = { InputParameter: { "CreateDate": $scope.document.CreatedDate }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.objOrganization = response.data;
                        $scope.objOrganizationBuffer = $scope.LimitData($scope.objOrganization);
                        // Get IO
                        $scope.InitIO();

                    }, function errorCallback(response) {
                        console.log('error TravelExpenseGroupEstimateEditorController objOrganization.', response);
                    });
                }, function errorCallback(response) {
                    console.log('error TravelExpenseGroupEstimateEditorController GetCostcenterDistribution.', response);
                });
            }, function errorCallback(response) {
                console.log('error GeneralExpenseEditorController GetCostcenter.', response);
            });
            */

            $scope.AlternativeCostCenter = function (item) {
                if (!item.IsAlternativeCostCenter) {
                    item.CostCenter = $scope.objCostcenterDistribution[0].CostCenterCode;
                    item.CostCenterName = $scope.objCostcenterDistribution[0].LongDesc;
                    item.searchCostCenterText = item.CostCenter + ' : ' + item.CostCenterName;
                }
            };
            $scope.AlternativeIOOrg = function (item) {
                if (!item.IsAlternativeIOOrg) {
                    if (!$scope.objOrganizationDefault.ObjectID) {
                        $scope.objOrganizationDefault = $scope.objOrganization[$scope.objOrganization.findIndexWithAttr('ObjectID', $scope.data.Travelers[$scope.TravelerIndex].OrgUnitID)];
                    }
                    item.AlternativeIOOrg = $scope.objOrganizationDefault.ObjectID;
                    item.AlternativeIOOrgName = $scope.objOrganizationDefault.ObjectID; + " : " + $scope.objOrganizationDefault.Text;
                    item.searchAlternativeIOOrgText = $scope.objOrganizationDefault.ObjectID; + " : " + $scope.objOrganizationDefault.Text;
                }
            }
            $scope.InitIO = function () {
                var URL = CONFIG.SERVER + 'HRTR/GetInternalOrderAll/';
                var oRequestParameter = { InputParameter: { "OrgUnit": '' }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.objIO = response.data;
                    if (!$scope.data.Readonly && $scope.data.GroupBudgets) {
                        for (var i = 0; i < $scope.data.GroupBudgets.length; i++) {
                            $scope.data.GroupBudgets[i].ExpenseTypeGroupIDObject = $scope.ExpenseTypeGroup[$scope.ExpenseTypeGroup.findIndexWithAttr('ExpenseTypeGroupID', $scope.data.GroupBudgets[i].ExpenseTypeGroupID)];
                            $scope.data.GroupBudgets[i].AlternativeIOOrgObject = $scope.objOrganization[$scope.objOrganization.findIndexWithAttr('ObjectID', $scope.data.GroupBudgets[i].AlternativeIOOrg)];
                            $scope.data.GroupBudgets[i].CostCenterObject = $scope.objCostcenter[$scope.objCostcenter.findIndexWithAttr('CostCenterCode', $scope.data.GroupBudgets[i].CostCenter)];
                            $scope.data.GroupBudgets[i].IOObject = $scope.objIO[$scope.objIO.findIndexWithAttr('OrderID', $scope.data.GroupBudgets[i].IO)];
                        }
                        //$scope.CheckExpenseTypeGroupBudgetSelect();
                    }
                }, function errorCallback(response) {
                    console.log('error GeneralExpenseEditorController GetInternalOrderByCostCenter.', response);
                });

            };
            $scope.changeEvent = function (index) {
                findDescription(index);
            }
            function findDescription(index) {
                var idEx = $scope.data.GroupBudgets[index].ExpenseTypeGroupID;
                var filteredItem = $filter('filter')($scope.ExpenseTypeGroup, { ExpenseTypeGroupID: idEx });
                if (angular.isDefined(filteredItem) && filteredItem.length > 0) {
                    $scope.data.GroupBudgets[index].description = filteredItem[0].Remark;
                }
            }
            //auto complete ExpenseTypeGroup 
            $scope.querySearchExpenseTypeGroup = function (query, index) {
                if (!$scope.ExpenseTypeGroup) return;
                var results = angular.copy(query ? $scope.ExpenseTypeGroup.filter(createFilterForExpenseTypeGroup(query, $scope.data.GroupBudgets)) : $scope.ExpenseTypeGroup), deferred;
                results = angular.copy(results.splice(0, 15));
                if ($scope.simulateQuery) {
                    deferred = $q.defer();
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 300, false);
                    return deferred.promise;
                } else {
                    return results;
                }
            };
            var tempResult_etg = {};
            $scope.selectedItemExpenseTypeGroupChange = function (item, index) {
                if (angular.isDefined(item) && item != null) {
                    $scope.data.GroupBudgets[index].ExpenseTypeGroupID = item.ExpenseTypeGroupID;
                    $scope.data.GroupBudgets[index].Name = item.Name;
                    $scope.data.GroupBudgets[index].Remark = item.Remark;
                    $scope.data.GroupBudgets[index].searchExpenseTypeGroupText = item.Remark;
                    tempResult_etg = item;
                    $scope.ChangeExpenseTypeGroup(index);
                }
                else {
                    $scope.data.GroupBudgets[index].ExpenseTypeGroupID = '';
                    $scope.data.GroupBudgets[index].Name = '';
                    $scope.data.GroupBudgets[index].Remark = '';
                    $scope.data.GroupBudgets[index].searchExpenseTypeGroupText = '';
                }
            };
            function createFilterForExpenseTypeGroup(query, arr) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    if (!x) return false;
                    var source = angular.lowercase(x.Remark);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }
            function createFilterDuplicateForExpenseTypeGroup(arr, index) {
                return function filterFn(x) {
                    if (!x) return false;
                    var tempArr = angular.copy(arr);
                    if (tempArr[index] && tempArr[index].ExpenseTypeGroupID)
                        tempArr[index].ExpenseTypeGroupID = null;
                    return !(tempArr.duplicateProp('ExpenseTypeGroupID', x.ExpenseTypeGroupID));
                };
            }

            $scope.tryToSelectExpenseTypeGroup = function (item) {
                item.searchExpenseTypeGroupText = '';
            }
            $scope.checkTextExpenseTypeGroup = function (text, $index) {
                var result = null;
                if (text) {
                    for (var i = 0; i < $scope.ExpenseTypeGroup.length; i++) {
                        if ($scope.ExpenseTypeGroup[i].ExpenseTypeGroupID == text || $scope.ExpenseTypeGroup[i].Remark == text || ($scope.ExpenseTypeGroup[i].Name + ' : ' + $scope.ExpenseTypeGroup[i].Remark) == text) {
                            result = $scope.ExpenseTypeGroup[i];
                            break;
                        }
                    }
                }
                if (result) {
                    $scope.selectedItemExpenseTypeGroupChange(result, $index);
                } else if (tempResult_etg) {
                    $scope.selectedItemExpenseTypeGroupChange(tempResult_etg, $index);
                }
            }
            //auto complete CostCenter 
            $scope.querySearchCostCenter = function (query, index) {
                if (!$scope.objCostcenter || !$scope.objCostcenterDistribution) return;
                var results = angular.copy(query ? $scope.objCostcenter.filter(createFilterForCostCenter(query)) : $scope.objCostcenter), deferred;
                results = results.splice(0, 15);
                //$scope.objCostcenterBuffer = angular.copy(results);
                if ($scope.simulateQuery) {
                    deferred = $q.defer();
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 50, false);
                    return deferred.promise;
                } else {
                    return results;
                }
            }
            $scope.selectedItemCostCenterChange = function (item, index) {
                if (item) {
                    $scope.data.GroupBudgets[index].CostCenter = item.CostCenterCode;
                    $scope.data.GroupBudgets[index].CostCenterName = item.LongDesc;
                    $scope.data.GroupBudgets[index].searchCostCenterText = item.CostCenterCode + ' : ' + item.LongDesc;
                    tempResult_cc = item;
                }
            };
            function createFilterForCostCenter(query) {
                var lowercaseQuery = angular.lowercase(query);
                var index = 0;
                return function filterFn(item) {
                    if (!item || index >= $scope.LimitRows) return false;
                    var source = angular.lowercase(item.CostCenterCode + ' : ' + item.LongDesc);
                    if ((source.indexOf(lowercaseQuery) >= 0) && (item)) {
                        index = index + 1;
                    }
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }
            var tempResult_cc = {};
            $scope.tryToSelect_cc = function (item) {
                item.searchCostCenterText = '';
            }
            $scope.checkText_cc = function (text, $index) {
                var result = null;
                if (text) {
                    for (var i = 0; i < $scope.objCostcenter.length; i++) {
                        if (($scope.objCostcenter[i].CostCenterCode + ' : ' + $scope.objCostcenter[i].LongDesc) == text || $scope.objCostcenter[i].CostCenterCode == text) {
                            result = $scope.objCostcenter[i];
                            break;
                        }
                    }
                }
                if (result) {
                    $scope.selectedItemCostCenterChange(result, $index);
                } else if (tempResult_cc) {
                    $scope.selectedItemCostCenterChange(tempResult_cc, $index);
                }
            }

            //auto complete AlternativeIOOrg 
            $scope.querySearchAlternativeIOOrg = function (query, index) {
                if (!$scope.objOrganization) return;
                var results = angular.copy(query ? $scope.objOrganization.filter(createFilterForAlternativeIOOrg(query)) : $scope.objOrganization), deferred;
                results = results.splice(0, 15);
                if ($scope.simulateQuery) {
                    deferred = $q.defer();
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 250, false);
                    return deferred.promise;
                } else {
                    return results;
                }
            };

            var tempResult_org = {};
            $scope.selectedItemAlternativeIOOrgChange = function (item, index) {
                if (angular.isDefined(item) && item != null) {
                    $scope.data.GroupBudgets[index].AlternativeIOOrg = item.ObjectID;
                    $scope.data.GroupBudgets[index].AlternativeIOOrgName = item.Text;
                    $scope.data.GroupBudgets[index].searchAlternativeIOOrgText = item.ObjectID + ' : ' + item.Text;
                    tempResult_org = item;
                    $scope.DefaultIO(index);
                }
                else {
                    $scope.data.GroupBudgets[index].AlternativeIOOrg = '';
                    $scope.data.GroupBudgets[index].AlternativeIOOrgName = '';
                }
            };
            function createFilterForAlternativeIOOrg(query) {
                var lowercaseQuery = angular.lowercase(query);
                var index = 0;
                return function filterFn(item) {
                    if (!item || index >= $scope.LimitRows) return false;
                    var source = angular.lowercase(item.ObjectID + ' : ' + item.Text);
                    if ((source.indexOf(lowercaseQuery) >= 0) && (item)) {
                        index = index + 1;
                    }
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }
            $scope.tryToSelect_orgb = function (item) {
                item.searchAlternativeIOOrgText = '';
            }
            $scope.checkText_orgb = function (text, $index) {
                var result = null;
                if (text) {
                    for (var i = 0; i < $scope.objOrganization.length; i++) {
                        if (($scope.objOrganization[i].ObjectID + ' : ' + $scope.objOrganization[i].Text) == text || $scope.objOrganization[i].ObjectID == text) {
                            result = angular.copy($scope.objOrganization[i]);
                            break;
                        }
                    }
                }
                if (result) {
                    $scope.selectedItemAlternativeIOOrgChange(result, $index);
                } else if (tempResult_org) {
                    $scope.selectedItemAlternativeIOOrgChange(tempResult_org, $index);
                }
            }
            //auto complete IO 
            $scope.querySearchIO = function (query, index) {
                if (!$scope.objIO) return;
                var results = angular.copy(query ? $scope.objIO.filter(createFilterForIO(index)).filter(createFilterSearchForIO(query)) : $scope.objIO.filter(createFilterForIO(index))), deferred;
                results = results.splice(0, 15);
                if ($scope.simulateQuery) {
                    deferred = $q.defer();
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 50, false);
                    return deferred.promise;
                } else {
                    return results;
                }
            };
            $scope.selectedItemIOChange = function (item, index) {
                if (angular.isDefined(item) && item != null) {
                    $scope.data.GroupBudgets[index].IO = item.OrderID;
                    $scope.data.GroupBudgets[index].IOName = item.Description;
                    $scope.data.GroupBudgets[index].searchIOText = item.OrderID + ' : ' + item.Description;
                    tempResult_io = item;
                }
                else {
                    $scope.data.GroupBudgets[index].IO = '';
                    $scope.data.GroupBudgets[index].IOName = '';
                }
            };
            $scope.newData = function (data) {
                alert("Sorry! You'll need to create a Constitution for " + data + " first!");
            };
            function createFilterSearchForIO(query) {
                var lowercaseQuery = angular.lowercase(query);
                var index = 0;
                return function filterFn(item) {
                    if (!item || index >= $scope.LimitRows) return false;
                    var source = angular.lowercase(item.OrderID + ':' + item.Description);
                    if ((source.indexOf(lowercaseQuery) >= 0) && (item)) {
                        index = index + 1;
                    }
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }
            function createFilterForIO(index) {
                return function filterFn(item) {
                    if (!item) return false;
                    return item.ObjectID == $scope.data.GroupBudgets[index].AlternativeIOOrg;
                };
            }
            var tempResult_io = {};
            $scope.tryToSelect_budg = function (item) {
                item.searchIOText = '';
            }
            $scope.checkText_budg = function (text, $index) {
                var result = null;
                var allIOList = angular.copy($scope.objIO.filter(createFilterForIO($index)));
                for (var i = 0; i < allIOList.length; i++) {
                    if ((allIOList[i].OrderID + " : " + allIOList[i].Description) == text || allIOList[i].OrderID == text) {
                        result = allIOList[i];
                        break;
                    }
                }
                if (result) {
                    $scope.selectedItemIOChange(result, $index);
                } else if (tempResult_io) {
                    $scope.selectedItemIOChange(tempResult_io, $index);
                }
                else {
                    $scope.data.GroupBudgets[$index].searchIOText = 'ไม่พบ IO';
                }
            }

            if (!$scope.data.Readonly && $scope.data.GroupBudgets) {
                for (var i = 0; i < $scope.data.GroupBudgets.length; i++) {
                    $scope.checkTextExpenseTypeGroup($scope.data.GroupBudgets[i].ExpenseTypeGroupID, i);
                    $scope.checkText_cc($scope.data.GroupBudgets[i].CostCenter, i);
                    $scope.checkText_orgb($scope.data.GroupBudgets[i].AlternativeIOOrg, i);
                    $scope.checkText_budg($scope.data.GroupBudgets[i].IO, i);
                }
            }

            $scope.isUserRoleAccounting = false;            
            $scope.checkAccountingRole = function () {
                var URL = CONFIG.SERVER + 'HRTR/IsUserRoleAccountingForEditRequest';
                var oRequestParameter = { InputParameter: { "REQUESTNO": $scope.document.RequestNo, 'CompanyCode': $scope.document.Requestor.CompanyCode }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.isUserRoleAccounting = response.data;
                    console.log('IsAccountingRole.', response.data);
                }, function errorCallback(response) {
                    console.log('error IsAccountingRole.', response);
                });
            };
            $scope.checkAccountingRole();
        }]);

})();



