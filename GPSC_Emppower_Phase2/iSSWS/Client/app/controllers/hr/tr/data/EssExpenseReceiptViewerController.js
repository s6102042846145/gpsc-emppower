﻿(function () {
angular.module('ESSMobile')
    .controller('EssExpenseReceiptViewerController', ['$scope', '$http', '$routeParams', '$location', '$window', 'CONFIG', function ($scope, $http, $routeParams, $location, $window, CONFIG) {

        $scope.ExpenseReport = angular.copy($scope.document.Additional);
       

        $scope.getExpenseItemsReceipt = function () {
            var oList = [];
            if (angular.isDefined($scope.ExpenseReport.ExpenseReportReceiptList) && $scope.ExpenseReport.ExpenseReportReceiptList != null) {

                var ExpenseReportReceiptList = $scope.ExpenseReport.ExpenseReportReceiptList;
                var runNo = 1;
                for (var i = 0; i < ExpenseReportReceiptList.length; i++) {
                    var receipt = angular.copy(ExpenseReportReceiptList[i]);
                    if (receipt.IsReceipt && receipt.IsVisible) {
                        var countReceiptItem = receipt.ExpenseReportReceiptItemList.length;
                        receipt.IsFirstRow = false;
                        var sumAllChild = 0;
                        for (var j = 0; j < countReceiptItem; j++) {
                            var receiptItem = receipt.ExpenseReportReceiptItemList[j];
                            sumAllChild += receiptItem.ExpenseReportReceiptItemDetailList.length;
                        }
                        receipt.ItemCount = sumAllChild;
                        receipt.ExpenseReportReceiptItemList = null;
                        receipt.orderID = runNo++;


                        for (var j = 0; j < countReceiptItem; j++) {
                            var receiptItem = ExpenseReportReceiptList[i].ExpenseReportReceiptItemList[j];
                            var countReceiptItemDetail = receiptItem.ExpenseReportReceiptItemDetailList.length;
                            receipt.item = {
                                IsFirstRow: false,
                                ItemCount: countReceiptItemDetail,
                                ItemID: receiptItem.ItemID,
                                ExpenseTypeGroupName: receiptItem.ExpenseTypeGroupName,
                                ExpenseTypeGroupRemark: receiptItem.ExpenseTypeGroupRemark,
                                ExpenseTypeName: receiptItem.ExpenseTypeName
                            };

                            for (var k = 0; k < countReceiptItemDetail; k++) {
                                var tempReceipt = angular.copy(receipt);
                                tempReceipt.realObj = ExpenseReportReceiptList[i];
                                var receiptItemDetail = receiptItem.ExpenseReportReceiptItemDetailList[k];
                                if (j == 0 && k == 0) {
                                    tempReceipt.IsFirstRow = true;
                                }
                                if (k == 0) {
                                    tempReceipt.item.IsFirstRow = true;
                                }
                                tempReceipt.itemDetail = {
                                    CostCenter: receiptItemDetail.CostCenter,
                                    IO: receiptItemDetail.IO,
                                    LocalAmount: receiptItemDetail.LocalAmount,
                                    OriginalAmount: receiptItemDetail.OriginalAmount

                                };
                                oList.push(tempReceipt);
                            }
                        }
                    }
                }

            }
            return oList;
        };
        $scope.expenseItemsReceipt = $scope.getExpenseItemsReceipt();

        $scope.sumVATBaseAmount = function (list, isReceipt) {
            var total = 0;
            angular.forEach(list, function (item) {
                if (item.IsReceipt == isReceipt && item.IsVisible) {
                    //var amount = Number(item.VATBaseAmount);
                    var amount = Number(item.LocalNoVATTotalAmount);
                    total += isNaN(amount) ? 0 : amount;
                }
            });
            return total;
        };

        $scope.sumVATAmount = function (list, isReceipt) {
            var total = 0;
            angular.forEach(list, function (item) {
                if (item.IsReceipt == isReceipt && item.IsVisible) {
                    var amount = Number(item.VATAmount);
                    total += isNaN(amount) ? 0 : amount;
                }
            });
            return total;
        };

        $scope.getCostCenterForLookup = function () {
            var URL = CONFIG.SERVER + 'HRTR/GetCostCenterForLookup';
            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                $scope.objCostcenterLookup = response.data;
                console.log('GetCostCenterForLookup.', $scope.objCostcenterLookup);
            }, function errorCallback(response) {
                console.log('error GeneralExpenseContentController GetCostCenterForLookup.', response);
            });
        };
        $scope.getCostCenterForLookup();



        $scope.getIOForLookup = function () {
            var URL = CONFIG.SERVER + 'HRTR/GetInternalOrderObjForLookup';
            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                $scope.objIOLookupObj = response.data;
                console.log('GetInternalOrderObjForLookup.', $scope.objIOLookupObj);
            }, function errorCallback(response) {
                console.log('error GeneralExpenseContentController GetInternalOrderObjForLookup.', response);
            });
        };
        $scope.getIOForLookup();

        var GetWHTTypeSubsidiseGetAll = function () {
            var URL = CONFIG.SERVER + 'HRTR/GetWHTTypeSubsidiseGetAll';
            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                $scope.tempData.oWHTTypeSubsidise = response.data;

                console.log('GetWHTTypeSubsidiseGetAll.', response.data);
            }, function errorCallback(response) {
                console.log('error GetWHTTypeSubsidiseGetAll.', response);
            });
        };
        GetWHTTypeSubsidiseGetAll();





        $scope.getFileAttach = function (attachment) {
            //var URL = CONFIG.SERVER + 'workflow/GetFile';

            /* via proxy page */
            //var oRequestParameter = { InputParameter: { "RequestNo": attachment.RequestNo, 'ReceiptID': attachment.ReceiptID, "FileID": attachment.FileID } };
            //var MoDule = 'workflow/';
            //var Functional = 'GetFile';
            //var URL = CONFIG.SERVER + MoDule + Functional;
            //// Success
            //$http({
            //    method: 'POST',
            //    url: URL,
            //    data: oRequestParameter
            //}).then(function successCallback(response) {
            //    console.log('ReceiptAttachment getFileAttach.', response.data);
            //    if (typeof cordova != 'undefined') {
            //        cordova.InAppBrowser.open("data:application/octet-stream, " + escape(response.data), '_system', 'location=no');
            //    } else {
            //        window.open("data:application/octet-stream, " + escape(response.data));
            //    }
            //}, function errorCallback(response) {
            //    // Error
            //    console.log('error ReceiptAttachment getFileAttach.', response);
            //});
            /* !via proxy page */

            /* direct */
            if (angular.isDefined(attachment)) {
                if (typeof cordova != 'undefined') {
                    //cordova.InAppBrowser.open("data:application/octet-stream, " + escape(response.data), '_system', 'location=no');
                } else {
                    //window.open("data:application/octet-stream, " + escape(response.data));

                    console.log('objAttachment', attachment);

                    //Nun Modified 28072016********************
                    //var path = CONFIG.SERVER + 'Client/files/' + attachment.RequestNo + '/' + ("00000" + attachment.ReceiptID.toString()).substr(-5, 5) + '/' + attachment.FileName;
                    var path = CONFIG.SERVER + 'Client/files/' + attachment.FilePath + '/' + attachment.FileName;
                    //*****************************************

                    $window.open(path);
                    //$window.open('http://localhost:15124/Client/index.html#/frmViewRequest/0013160000069-GE000/0013/8150/False/False');
                }
            }
            /* !direct */
        };

        var getAllExchageType = function () {
            var URL = CONFIG.SERVER + 'HRTR/GetExchangeTypeAll';
            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };

            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                $scope.exchangeType = response.data;
                $scope.ExchangeTypeReady = true;
            },
            function errorCallback(response) {
                $scope.exchangeType = [];
                console.log('error GetExchangeTypeAll', response);
            });
        };
        getAllExchageType();

        $scope.findExchangeType = function (exchangeTypeID) {
            if (angular.isDefined($scope.exchangeType)) {
                for (var i = 0; i < $scope.exchangeType.length; i++) {
                    if ($scope.exchangeType[i].ExchangeTypeID == exchangeTypeID) {
                        if ($scope.employeeData.Language == 'TH') {
                            return '(' + $scope.exchangeType[i].DescriptionTH + ')';
                        } else {
                            return '(' + $scope.exchangeType[i].DescriptionEN + ')';
                        }

                    }
                }
            }
            return '';
        };

    }]);
})();
