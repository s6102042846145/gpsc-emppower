﻿(function () {
    angular.module('ESSMobile')
        .controller('TravelExpenseProjectEditorForAverageModeController', ['$scope', '$http', '$routeParams', '$location', '$filter', '$window', '$timeout', '$q', '$log', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, $window, $timeout, $q, $log, CONFIG, $mdDialog) {
            $scope.Textcategory = 'EXPENSE';

            //Create temp object when add new employee
            $scope.CreateTempObject = function () {
                var newProjectCostDistributions = $scope.data.tmpProjectCostDistribution;
                return angular.copy(newProjectCostDistributions);
            }

            $scope.InitialProjectCost = function () {
                if (!$scope.data.Readonly) {
                    if ($scope.data.ProjectCostDistributions && $scope.settings.Master.ProjectList) {
                        for (var i = 0; i < $scope.data.ProjectCostDistributions.length; i++) {
                            if ($scope.data.ProjectCostDistributions[i].ProjectCode) $scope.checkText($scope.data.ProjectCostDistributions[i].ProjectCode, i, true);
                            if ($scope.data.ProjectCostDistributions[i].CostCenter) $scope.checkText_cc($scope.data.ProjectCostDistributions[i].CostCenter, i);
                            if ($scope.data.ProjectCostDistributions[i].AlternativeIOOrg) $scope.checkText_orgb($scope.data.ProjectCostDistributions[i].AlternativeIOOrg, i);
                            if ($scope.data.ProjectCostDistributions[i].IO) $scope.checkText_budg($scope.data.ProjectCostDistributions[i].IO, i);
                        }
                    }
                }
            }

            $scope.addRow = function () {
                var oProject = $scope.CreateTempObject();
                $scope.data.ProjectCostDistributions.push(oProject);
                if ($scope.data.ProjectCostDistributions.length == $scope.settings.Master.ProjectList.length) {
                    $scope.hideAddButton = true;
                }
                $scope.selectedItemProjectAVGChange(angular.copy($scope.settings.Master.ProjectList.filter(createFilterDuplicate($scope.data.ProjectCostDistributions))[0]), $scope.data.ProjectCostDistributions.length - 1);
            };

            $scope.removeNewRow = function (index) {
                if (confirm($scope.Text['SYSTEM']['CONFIRM_DELETE'])) {
                    $scope.data.ProjectCostDistributions.splice(index, 1);
                    if ($scope.settings.Master.ProjectList.length > $scope.data.ProjectCostDistributions.length) {
                        $scope.hideAddButton = false;
                    }
                }
            };

            //autocomplete project 
            $scope.querySearchProject = function (query, index) {
                var results = query ? $scope.settings.Master.ProjectList.filter(createFilterDuplicate($scope.data.ProjectCostDistributions)).filter(createProjectFilterFor(query)) : $scope.settings.Master.ProjectList.filter(createFilterDuplicate($scope.data.ProjectCostDistributions)), deferred;

                results = angular.copy($scope.limMaxArr(results, 30));
                if ($scope.simulateQuery) {
                    deferred = $q.defer();
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 300, false);
                    return deferred.promise;
                } else {
                    return results;
                }
            }

            $scope.selectedItemProjectAVGChange = function (item, index, isInit) {
                if (angular.isDefined(item) && item != null) {
                    $scope.data.ProjectCostDistributions[index].ProjectCode = item.ProjectCode;
                    $scope.data.ProjectCostDistributions[index].ProjectName = item.Name;
                    $scope.data.ProjectCostDistributions[index].searchTextProject = item.ProjectCode + ' : ' + item.Name;
                    tempResult = item;
                    if (!isInit) {
                        if (item.CostCenter && $scope.settings.Master.CostcenterDistributionList && $scope.settings.Master.CostCenterList) {
                            $scope.checkText_cc(item.CostCenter, index);
                        }
                        if (item.AlternativeIOOrg) {
                            $scope.checkText_orgb(item.AlternativeIOOrg, index);
                        }
                        if (item.IO) {
                            $scope.checkText_budg(item.IO, index);
                        }
                    }
                }
                else {
                    $scope.data.ProjectCostDistributions[index].ProjectCode = '';
                    $scope.data.ProjectCostDistributions[index].ProjectName = '';
                }
            }

            function createFilterDuplicate(arr) {
                return function filterFn(x) {
                    if (!x) return false;
                    return !(arr.duplicateProp('ProjectCode', x.ProjectCode));
                };
            }

            function createProjectFilterFor(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(project) {
                    var source = angular.lowercase(project.ProjectCode + ' : ' + project.Name);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }

            var tempResult;
            $scope.tryToSelect = function (item) {
                item.searchTextProject = '';
            }
            $scope.checkText = function (text, $index, isInit) {
                var result = null;
                for (var i = 0; i < $scope.settings.Master.ProjectList.length; i++) {
                    if (($scope.settings.Master.ProjectList[i].ProjectCode + " : " + $scope.settings.Master.ProjectList[i].Name) == text || $scope.settings.Master.ProjectList[i].Name == text || $scope.settings.Master.ProjectList[i].ProjectCode == text) {
                        result = $scope.settings.Master.ProjectList[i];
                        break;
                    }
                }
                if (result) {
                    $scope.selectedItemProjectAVGChange(result, $index, isInit);
                } else if (tempResult) {
                    $scope.selectedItemProjectAVGChange(tempResult, $index, isInit);
                }
            }

            $scope.newProjectData = function (employee) {
                alert("Sorry! You'll need to create a Constitution for " + employee + " first!");
            }

            $scope.checkText_budg = function (text, $index) {
                var result = null;
                for (var i = 0; i < $scope.settings.Master.IOList.length; i++) {
                    if ($scope.settings.Master.IOList[i].OrderID == text) {
                        result = $scope.settings.Master.IOList[i];
                        break;
                    }
                }

                if (result) {
                    $scope.data.ProjectCostDistributions[$index].IO = result.OrderID;
                    $scope.data.ProjectCostDistributions[$index].IOName = result.Description;
                } else if (tempResult_budg) {
                    $scope.data.ProjectCostDistributions[$index].IO = '';
                    $scope.data.ProjectCostDistributions[$index].IOName = '';
                }
            }

            $scope.checkText_cc = function (text, $index) {
                var result = null;
                for (var i = 0; i < $scope.settings.Master.CostCenterList.length; i++) {
                    if ($scope.settings.Master.CostCenterList[i].CostCenterCode == text) {
                        result = $scope.settings.Master.CostCenterList[i];
                        break;
                    }
                }
                if (result) {
                    $scope.data.ProjectCostDistributions[$index].CostCenter = result.CostCenterCode;
                    $scope.data.ProjectCostDistributions[$index].CostCenterName = result.LongDesc;
                } else {
                    $scope.data.ProjectCostDistributions[$index].CostCenter = '';
                    $scope.data.ProjectCostDistributions[$index].CostCenterName = '';
                }
            }

            $scope.checkText_orgb = function (text, $index) {
                var result = null;
                for (var i = 0; i < $scope.settings.Master.OrgUnitList.length; i++) {
                    if ($scope.settings.Master.OrgUnitList[i].ObjectID == text) {
                        result = $scope.settings.Master.OrgUnitList[i];
                        break;
                    }
                }
                if (result) {
                    $scope.data.ProjectCostDistributions[$index].AlternativeIOOrg = result.ObjectID;
                    $scope.data.ProjectCostDistributions[$index].AlternativeIOOrgName = result.Text;
                } else if (tempResult_orgb) {
                    $scope.data.ProjectCostDistributions[$index].AlternativeIOOrg = '';
                    $scope.data.ProjectCostDistributions[$index].AlternativeIOOrgName = '';
                }
            }

            function showAlert(message) {
                $mdDialog.show(
                  $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('INFORMATION')
                    .textContent(message)
                    .ok('OK')
                );
            };

            $scope.lookUp_objCostcenter = function (id) {
                if ($scope.masterTR.CostCenterList.length > 0) {
                    for (var i = 0; i < $scope.masterTR.CostCenterList.length; i++) {
                        if (id == $scope.masterTR.CostCenterList[i].CostCenterCode) {
                            return $scope.masterTR.CostCenterList[i].CostCenterCode + ' : ' + $scope.masterTR.CostCenterList[i].LongDesc;
                        }
                    }
                }
                else {
                    if ($scope.settings.Master.CostCenterList) {
                        for (var i = 0; i < $scope.settings.Master.CostCenterList.length; i++) {
                            if (id == $scope.settings.Master.CostCenterList[i].CostCenterCode) {
                                return $scope.settings.Master.CostCenterList[i].CostCenterCode + ' : ' + $scope.settings.Master.CostCenterList[i].LongDesc;
                            }
                        }
                    }
                }
            }

            $scope.lookUp_objOrganization = function (id) {
                if ($scope.masterTR.OrgUnitList.length > 0) {
                    for (var i = 0; i < $scope.masterTR.OrgUnitList.length; i++) {
                        if (id == $scope.masterTR.OrgUnitList[i].ObjectID) {
                            return $scope.masterTR.OrgUnitList[i].ObjectID + ' : ' + $scope.masterTR.OrgUnitList[i].Text;
                        }
                    }
                }
                else
                {
                    for (var i = 0; i < $scope.settings.Master.OrgUnitList.length; i++) {
                        if (id == $scope.settings.Master.OrgUnitList[i].ObjectID) {
                            return $scope.settings.Master.OrgUnitList[i].ObjectID + ' : ' + $scope.settings.Master.OrgUnitList[i].Text;
                        }
                    }
                }
            }

            $scope.lookUp_objIO = function (id) {
                if ($scope.masterTR.IOList.length > 0) {
                    for (var i = 0; i < $scope.masterTR.IOList.length; i++) {
                        if (id == $scope.masterTR.IOList[i].OrderID) {
                            return $scope.masterTR.IOList[i].OrderID + ' : ' + $scope.masterTR.IOList[i].Description;
                        }
                    }
                }
                else
                {
                    if ($scope.settings.Master.IOList) {
                        for (var i = 0; i < $scope.settings.Master.IOList.length; i++) {
                            if (id == $scope.settings.Master.IOList[i].OrderID) {
                                return $scope.settings.Master.IOList[i].OrderID + ' : ' + $scope.settings.Master.IOList[i].Description;
                            }
                        }
                    }
                    
                }
            }

        }]);
})();


