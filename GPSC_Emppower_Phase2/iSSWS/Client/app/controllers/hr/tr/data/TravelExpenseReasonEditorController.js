﻿(function () {
    angular.module('ESSMobile')
            .controller('TravelExpenseReasonEditorController', ['$scope', '$http', '$routeParams', '$location', 'CONFIG', function ($scope, $http, $routeParams, $location, CONFIG) {
                   //place code to do
            $scope.Textcategory = 'TRAVELREQUEST';
            $scope.InitialPersonalVehicle = function () {
                //Load Configuratioin for Search Data GetTravelPersonalVehicle
                var URL = CONFIG.SERVER + 'HRTR/GetTravelPersonalVehicle';
                var oRequestParameter = { InputParameter: { 'ReuestType': 'TR' }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    //VehicleID int
                    //VehicleDescription string
                    //IsSelected bool
                    $scope.data.TRPersonalVehicleList = response.data;// JSON.stringify(response.data);

                }, function errorCallback(response) {
                    // Error
                    console.log('error TravelExpenseReasonEditorController InitialConfig.', response);
                });
            }

        }]);
})();