﻿(function () {
angular.module('ESSMobile')
    .controller('EssExpenseWithoutReceiptViewerController', ['$scope', '$http', '$routeParams', '$location', 'CONFIG', function ($scope, $http, $routeParams, $location, CONFIG) {
 
        $scope.ExpenseReport = angular.copy($scope.document.Additional);

        $scope.getExpenseItemsWithoutReceipt = function () {
            var oList = [];
            if (angular.isDefined($scope.ExpenseReport.ExpenseReportReceiptList) && $scope.ExpenseReport.ExpenseReportReceiptList != null) {

                var ExpenseReportReceiptList = $scope.ExpenseReport.ExpenseReportReceiptList;
                var runNo = 1;
                for (var i = 0; i < ExpenseReportReceiptList.length; i++) {
                    var receipt = angular.copy(ExpenseReportReceiptList[i]);
                    if (!receipt.IsReceipt && receipt.IsVisible) {
                        var countReceiptItem = receipt.ExpenseReportReceiptItemList.length;
                        receipt.IsFirstRow = false;
                        var sumAllChild = 0;
                        for (var j = 0; j < countReceiptItem; j++) {
                            var receiptItem = receipt.ExpenseReportReceiptItemList[j];
                            sumAllChild += receiptItem.ExpenseReportReceiptItemDetailList.length;
                        }
                        receipt.ItemCount = sumAllChild;
                        receipt.ExpenseReportReceiptItemList = null;
                        receipt.ExpenseReportReceiptFileList = null;
                        receipt.orderID = runNo++;


                        for (var j = 0; j < countReceiptItem; j++) {
                            var receiptItem = ExpenseReportReceiptList[i].ExpenseReportReceiptItemList[j];
                            var countReceiptItemDetail = receiptItem.ExpenseReportReceiptItemDetailList.length;
                            receipt.item = {
                                IsFirstRow: false,
                                ItemCount: countReceiptItemDetail,
                                ItemID: receiptItem.ItemID,
                                ExpenseTypeGroupName: receiptItem.ExpenseTypeGroupName,
                                ExpenseTypeGroupRemark: receiptItem.ExpenseTypeGroupRemark,
                                ExpenseTypeName: receiptItem.ExpenseTypeName
                            };

                            for (var k = 0; k < countReceiptItemDetail; k++) {
                                var tempReceipt = angular.copy(receipt);
                                tempReceipt.realObj = ExpenseReportReceiptList[i];
                                var receiptItemDetail = receiptItem.ExpenseReportReceiptItemDetailList[k];
                                if (j == 0 && k == 0) {
                                    tempReceipt.IsFirstRow = true;
                                }
                                if (k == 0) {
                                    tempReceipt.item.IsFirstRow = true;
                                }
                                tempReceipt.itemDetail = {
                                    CostCenter: receiptItemDetail.CostCenter,
                                    IO: receiptItemDetail.IO,
                                    LocalAmount: receiptItemDetail.LocalAmount
                                };
                                oList.push(tempReceipt);
                            }
                        }
                    }
                }

            }
            return oList;
        };
        $scope.expenseItemsWithoutReceipt = $scope.getExpenseItemsWithoutReceipt();

        $scope.sumVATBaseAmount = function (list, isReceipt) {
            var total = 0;
            angular.forEach(list, function (item) {
                if (item.IsReceipt == isReceipt && item.IsVisible) {
                    //var amount = Number(item.VATBaseAmount);
                    var amount = Number(item.LocalNoVATTotalAmount);
                    total += isNaN(amount) ? 0 : amount;
                }
            });
            return total;
        };

        $scope.sumVATAmount = function (list, isReceipt) {
            var total = 0;
            angular.forEach(list, function (item) {
                if (item.IsReceipt == isReceipt && item.IsVisible) {
                    var amount = Number(item.VATAmount);
                    total += isNaN(amount) ? 0 : amount;
                }
            });
            return total;
        };

        $scope.getCostCenterForLookup = function () {
            var URL = CONFIG.SERVER + 'HRTR/GetCostCenterForLookup';
            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                $scope.objCostcenterLookup = response.data;
                console.log('GetCostCenterForLookup.', $scope.objCostcenterLookup);
            }, function errorCallback(response) {
                console.log('error GeneralExpenseContentController GetCostCenterForLookup.', response);
            });
        };
        $scope.getCostCenterForLookup();

       

        $scope.getIOForLookup = function () {
            var URL = CONFIG.SERVER + 'HRTR/GetInternalOrderObjForLookup';
            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                $scope.objIOLookupObj = response.data;
                console.log('GetInternalOrderObjForLookup.', $scope.objIOLookupObj);
            }, function errorCallback(response) {
                console.log('error GeneralExpenseContentController GetInternalOrderObjForLookup.', response);
            });
        };
        $scope.getIOForLookup();

        var getAllExchageType = function () {
            var URL = CONFIG.SERVER + 'HRTR/GetExchangeTypeAll';
            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };

            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                $scope.exchangeType = response.data;
                $scope.ExchangeTypeReady = true;
            },
            function errorCallback(response) {
                $scope.exchangeType = [];
                console.log('error GetExchangeTypeAll', response);
            });
        };
        getAllExchageType();

        $scope.findExchangeType = function (exchangeTypeID) {
            if (angular.isDefined($scope.exchangeType)) {
                for (var i = 0; i < $scope.exchangeType.length; i++) {
                    if ($scope.exchangeType[i].ExchangeTypeID == exchangeTypeID) {
                        if ($scope.employeeData.Language == 'TH') {
                            return '(' + $scope.exchangeType[i].DescriptionTH + ')';
                        } else {
                            return '(' + $scope.exchangeType[i].DescriptionEN + ')';
                        }

                    }
                }
            }
            return '';
        };

    }]);
})();
