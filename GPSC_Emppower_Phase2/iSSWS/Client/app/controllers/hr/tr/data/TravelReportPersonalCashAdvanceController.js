﻿(function () {
    angular.module('ESSMobile')
        .controller('TravelReportPersonalCashAdvanceController', ['$scope', '$http', '$routeParams', '$location', 'CONFIG', function ($scope, $http, $routeParams, $location, CONFIG) {
            $scope.Textcategory = 'TRAVELREQUEST';
            $scope.IndexTraveler = 0;

            //Default Curent Opject
            $scope.data.oPivotPerdiums = null;
            $scope.oColumnPerdiums = [];
            var objColPivot = {
                realObj: null,
                expenseRateTypeID: -1,
                //isCheck: false,
                isShow: true,
                amount: 0,
                chkBoxGroupName: ''
            };

            var objRowPivot = {
                realObj: null,
                colPivot: [],
                beginDate: null,
                endDate: null,
                districtID: '',
                provinceID: -1,
                countryID: -1,
                totalAmount: 0
            };

            $scope.ExpenseRateTagCodeForPostList = [];
          
           
            // Set Perdiums To Pivot 
            var generatePivotPerdium = function () {
                $scope.data.oPivotPerdiums = [];
                $scope.oColumnPerdiums = [];
                if ($scope.data.PerdiumList != null && $scope.data.PerdiumList.length > 0) {
                    var savedBeginDate = '';
                    var savedExpenseRateTypeID = -1;
                    for (var i = 0; i < $scope.data.PerdiumList.length; i++) {
                        if ($scope.data.PerdiumList[i].BeginDate != "0001-01-01T00:00:00") {
                            if (savedBeginDate != $scope.data.PerdiumList[i].BeginDate) {
                                // New row
                                var newRow = angular.copy(objRowPivot);
                                newRow.beginDate = $scope.data.PerdiumList[i].BeginDate;
                                newRow.endDate = $scope.data.PerdiumList[i].EndDate;
                                //var place = findPlace($scope.data.PerdiumList[i].BeginDate);
                                //newRow.provinceID = place[0];
                                //newRow.countryID = place[1];
                                newRow.districtID = $scope.data.PerdiumList[i].DistrictCode;
                                newRow.provinceID = $scope.data.PerdiumList[i].ProvinceCode;
                                newRow.countryID = $scope.data.PerdiumList[i].Country;
                                $scope.data.oPivotPerdiums.push(newRow);
                            }
                            if (savedExpenseRateTypeID != $scope.data.PerdiumList[i].ExpenseRateTypeID) {
                                // New column
                                var newCol = angular.copy(objColPivot);
                                newCol.realObj = $scope.data.PerdiumList[i];
                                newCol.expenseRateTypeID = $scope.data.PerdiumList[i].ExpenseRateTypeID;
                                //newCol.isCheck = $scope.data.PerdiumList[i].IsCheck;
                                newCol.isShow = ($scope.data.PerdiumList[i].ExpenseRateTypeID == 6) ? false : true;
                                newCol.amount = $scope.data.PerdiumList[i].LocalAmount;
                                var groupName = '';
                                if ($.inArray($scope.data.PerdiumList[i].ExpenseRateTypeID, [5, 6]) != -1) {
                                    groupName = 'ABC';
                                } else {
                                    groupName = $scope.data.PerdiumList[i].Group;;
                                }

                                newCol.chkBoxGroupName = groupName;
                                $scope.data.oPivotPerdiums[$scope.data.oPivotPerdiums.length - 1].colPivot.push(newCol);

                                if ($scope.data.oPivotPerdiums.length == 1 && newCol.isShow) {
                                    // save for dynamic column header
                                    $scope.oColumnPerdiums.push($scope.data.PerdiumList[i].ExpenseRateTypeID);
                                }
                            }

                            // Aslways maintain ExpenseRateTypeID, BeginDate.
                            savedExpenseRateTypeID = $scope.data.PerdiumList[i].ExpenseRateTypeID;
                            savedBeginDate = $scope.data.PerdiumList[i].BeginDate;

                        }
                    }


                   
                    var temp_arr = [];
                    for (var i = 0 ; i < $scope.data.oPivotPerdiums[0].colPivot.length; i++) {
                        if ($scope.data.oPivotPerdiums[0].colPivot[i].isShow && $scope.data.oPivotPerdiums[0].colPivot[i].realObj.LocalAmount > 0) {
                            temp_arr.push($scope.data.oPivotPerdiums[0].colPivot[i].realObj);
                        }
                    }
                   
                    var ExpenseRateTagCodeForPostList = groupBy(temp_arr, function (item) {
                        return [item.ExpenseRateTagCodeForPost];
                    });
                    var temp_arr_2 = angular.copy(ExpenseRateTagCodeForPostList);
                    $scope.ExpenseRateTagCodeForPostList = [];
                    for (var i = 0 ; i < temp_arr_2.length; i++) {
                        var oExpenseRateTagCodeForPost = {
                            name: temp_arr_2[i][0].ExpenseRateTagCodeForPost,
                            sum:0,
                            list: temp_arr_2[i]
                        }
                        $scope.ExpenseRateTagCodeForPostList.push(oExpenseRateTagCodeForPost);
                    }

                    
                }
            };
            function groupBy(array, f) {
                var groups = {};
                array.forEach(function (o) {
                    var group = JSON.stringify(f(o));
                    groups[group] = groups[group] || [];
                    
                    groups[group].push(o);
                });
                return Object.keys(groups).map(function (group) {
                    return groups[group];
                })
            }

            //$scope.$watch('data.TravelSchedulePlaces', function () {
            //    generatePivotPerdium();
            //}, true);

            // rebind to preserve two way binding after calculate data
            $scope.$watch('data.PerdiumList', function () {
                generatePivotPerdium();
            });


            
            function findPlace(beginDate) {
                //var oTestBeginDate = new Date(beginDate);
                //if ($scope.data.TravelSchedulePlaces != null) {
                //    for (var i = 0; i < $scope.data.TravelSchedulePlaces.length; i++) {
                //        var oBeginDate = new Date($scope.data.TravelSchedulePlaces[i].BeginDate);
                //        var oEndDate = new Date($scope.data.TravelSchedulePlaces[i].EndDate);
                //        if (oBeginDate <= oTestBeginDate && oTestBeginDate <= oEndDate) {
                //            return [$scope.data.TravelSchedulePlaces[i].CityName, $scope.data.TravelSchedulePlaces[i].CountryName];
                //        }
                //    }
                //}
                //return [];
            }

            $scope.fnCheckChange = function (row, index, isCheck) {
                /*if (row.colPivot[index].chkBoxGroupName == 'ABC') {
                    // If checked/unchecked then uncheck/check other (5, 6) checkbox in group
                    for (var i = 0; i < row.colPivot.length; i++) {
                        if (i != index && row.colPivot[i].chkBoxGroupName == 'ABC') {
                            row.colPivot[i].realObj.IsCheck = !isCheck;
                        }
                    }
                } else {
                    // If checked then uncheck other checkbox in group
                    if (isCheck) {
                        for (var i = 0; i < row.colPivot.length; i++) {
                            if (i != index && row.colPivot[i].chkBoxGroupName == row.colPivot[index].chkBoxGroupName) {
                                row.colPivot[i].realObj.IsCheck = false;
                            }
                        }
                    }
                }*/

                /*if (isCheck && (row.colPivot[index].realObj.ExpenseRateTagCodeForPost == 'OFFSHORENIGHT' || row.colPivot[index].realObj.ExpenseRateTagCodeForPost == 'ACCOMMUTATION')) {
                    for (var i = 0; i < row.colPivot.length; i++) {
                        if (i != index && (row.colPivot[i].realObj.ExpenseRateTagCodeForPost == 'ACCOMMUTATION' || row.colPivot[i].realObj.ExpenseRateTagCodeForPost == 'OFFSHORENIGHT' || row.colPivot[i].realObj.ExpenseRateTagCodeForPost == 'ACCOMMODATION')) {
                            row.colPivot[i].realObj.IsCheck = false;
                        }
                    }
                }
                else if (!isCheck && (row.colPivot[index].realObj.ExpenseRateTagCodeForPost == 'OFFSHORENIGHT' || row.colPivot[index].realObj.ExpenseRateTagCodeForPost == 'ACCOMMUTATION')) {
                    for (var i = 0; i < row.colPivot.length; i++) {
                        if (i != index && row.colPivot[i].realObj.ExpenseRateTagCodeForPost == 'ACCOMMODATION') {
                            row.colPivot[i].realObj.IsCheck = true;
                        }
                    }
                }
                else if (isCheck && !(row.colPivot[index].realObj.ExpenseRateTagCodeForPost == 'OFFSHORENIGHT' || row.colPivot[index].realObj.ExpenseRateTagCodeForPost == 'ACCOMMUTATION')) {
                    for (var i = 0; i < row.colPivot.length; i++) {
                        if (i != index && row.colPivot[i].realObj.ExpenseRateTagCodeForPost == 'ACCOMMODATION') {
                            row.colPivot[i].realObj.IsCheck = true;
                        }
                    }
                }*/
                if (isCheck) {
                    for (var i = 0; i < row.colPivot.length; i++) {
                        if (i != index && row.colPivot[i].chkBoxGroupName == row.colPivot[index].chkBoxGroupName) {
                            row.colPivot[i].realObj.IsCheck = false;
                        }
                    }
                    if (row.colPivot[index].realObj.ExpenseRateTagCodeForPost == 'OFFSHORENIGHT' || row.colPivot[index].realObj.ExpenseRateTagCodeForPost == 'ACCOMMUTATION') {
                        for (var i = 0; i < row.colPivot.length; i++) {
                            if (i != index && (row.colPivot[i].realObj.ExpenseRateTagCodeForPost == 'ACCOMMUTATION' || row.colPivot[i].realObj.ExpenseRateTagCodeForPost == 'OFFSHORENIGHT' || row.colPivot[i].realObj.ExpenseRateTagCodeForPost == 'ACCOMMODATION')) {
                                row.colPivot[i].realObj.IsCheck = false;
                            }
                        }
                    } else {
                        for (var i = 0; i < row.colPivot.length; i++) {
                            if (i != index && row.colPivot[i].realObj.ExpenseRateTagCodeForPost == 'ACCOMMUTATION') {
                                if (row.colPivot[i].realObj.IsCheck == true) {
                                    for (var j = 0; j < row.colPivot.length; j++) {
                                        if (j != index && row.colPivot[j].realObj.ExpenseRateTagCodeForPost == 'ACCOMMODATION') {
                                            row.colPivot[j].realObj.IsCheck = false;
                                            break;
                                        }
                                    }
                                }
                                else {
                                    for (var j = 0; j < row.colPivot.length; j++) {
                                        if (j != index && row.colPivot[j].realObj.ExpenseRateTagCodeForPost == 'ACCOMMODATION') {
                                            row.colPivot[j].realObj.IsCheck = true;
                                        }
                                    }
                                }
                            }
                            /*else {
                                row.colPivot[i].realObj.IsCheck = false;
                            }*/
                        }
                    }
                    
                }
                else if (row.colPivot[index].realObj.ExpenseRateTagCodeForPost == 'OFFSHORENIGHT' || row.colPivot[index].realObj.ExpenseRateTagCodeForPost == 'ACCOMMUTATION') {
                    for (var i = 0; i < row.colPivot.length; i++) {
                        if (i != index && row.colPivot[i].realObj.ExpenseRateTagCodeForPost == 'ACCOMMODATION') {
                            row.colPivot[i].realObj.IsCheck = true;
                        }
                    }
                }
            };

            $scope.fnCheckShow = function (value) {
                return true;
            };

            //GET CURRENT TRAVELER
            $scope.GetEditTraveler = function (specific_employeeid) {
                return 0;
            };

            $scope.rowSum = function (perdiam) {
                //perdiam.colPivot
                //var objColPivot = {
                //    realObj: null,
                //    expenseRateTypeID: -1,
                //    //isCheck: false,
                //    isShow: true,
                //    amount: 0,
                //    chkBoxGroupName: ''
                //};
                var sum = 0;
                if (perdiam.colPivot && perdiam.colPivot.length>0) {
                    for (var i = 0; i < perdiam.colPivot.length; i++) {
                        if (perdiam.colPivot[i].isShow && perdiam.colPivot[i].realObj.IsCheck) {
                            sum += Number(perdiam.colPivot[i].amount);
                        }
                    }
                }
                return sum;
            };

            function createFilterIsShow() {
                return function filterFn(x) {
                    if (!x) return false;
                    return x.isShow;
                };
            }

            $scope.colSum = function (index) {
                //$scope.data.oPivotPerdiums[$scope.data.oPivotPerdiums.length - 1].colPivot.push(newCol);
                var sum = 0;
              


                if ($scope.data.oPivotPerdiums != null) {

                    // clear sum for each expenseRateTag
                    for (var i = 0; i < $scope.ExpenseRateTagCodeForPostList.length; i++) {
                        $scope.ExpenseRateTagCodeForPostList[i].sum = 0;
                    }

                    for (var i = 0; i < $scope.data.oPivotPerdiums.length; i++) {
                        var perdiam = $scope.data.oPivotPerdiums[i];
                        if( perdiam && perdiam.colPivot && perdiam.colPivot.length >0)
                        {
                            var colPivot = angular.copy(perdiam.colPivot.filter(createFilterIsShow()));
                            if (colPivot[index].isShow && colPivot[index].realObj.IsCheck) {
                                sum += Number(colPivot[index].amount);
                            }
                        }
                    }


                    // add sum for each expenseRateTag
                    for (var i = 0; i < $scope.data.oPivotPerdiums.length; i++) {
                        if ($scope.data.oPivotPerdiums[i].colPivot && $scope.data.oPivotPerdiums[i].colPivot.length > 0)
                        {
                            for (var a = 0; a < $scope.data.oPivotPerdiums[i].colPivot.length; a++) {
                                for (var b = 0; b < $scope.ExpenseRateTagCodeForPostList.length; b++) {
                                    if ($scope.data.oPivotPerdiums[i].colPivot[a].realObj.ExpenseRateTagCodeForPost == $scope.ExpenseRateTagCodeForPostList[b].name &&
                                        $scope.data.oPivotPerdiums[i].colPivot[a].isShow &&
                                        $scope.data.oPivotPerdiums[i].colPivot[a].realObj.IsCheck
                                        ) {
                                        $scope.ExpenseRateTagCodeForPostList[b].sum += $scope.data.oPivotPerdiums[i].colPivot[a].amount;
                                    }
                                }
                            }
                        }
                    }
                }
                return sum;
            };

            $scope.allSum = function () {
                var sum = 0;
                if ($scope.data.oPivotPerdiums != null) {
                    for (var i = 0; i < $scope.data.oPivotPerdiums.length; i++) {
                        var perdiam = $scope.data.oPivotPerdiums[i];
                        if (perdiam.colPivot && perdiam.colPivot.length > 0) {
                            for (var k = 0; k < perdiam.colPivot.length; k++) {
                                if (perdiam.colPivot[k].isShow && perdiam.colPivot[k].realObj.IsCheck) {
                                    sum += Number(perdiam.colPivot[k].amount);
                                }
                            }
                        }
                    }
                }
                if (angular.isDefined($scope.childSumData)) {
                    $scope.childSumData.PersonalCashAdvance = sum;
                }
                return sum;
            };


        }]);
 })();