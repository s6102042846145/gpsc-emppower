﻿(function () {
    angular.module('ESSMobile')
        .controller('TravelExpenseGroupCashAdvanceController', ['$scope', '$http', '$routeParams', '$location', 'CONFIG', function ($scope, $http, $routeParams, $location, CONFIG) {
            //place code to do
            console.log('TravelExpenseGroupCashAdvanceController')
            $scope.Textcategory = 'TRAVELREQUEST';
            $scope.IndexTraveler = 0;

            $scope.InitialConfig = function () {
                $scope.IndexTraveler = $scope.GetEditTraveler($scope.document.Requestor.EmployeeID);
            }

            $scope.limitAmount = function (current,index) {
                if (current.TotalAmount) {
                    if (parseFloat(current.TotalAmount) >= $scope.data.GroupBudgets[index].TotalAmount) {
                        current.TotalAmount = $scope.data.GroupBudgets[index].TotalAmount;
                    }
                    else if (parseFloat(current.TotalAmount) < 0.00) {
                        current.TotalAmount = 0.00;
                    }
                }
                else {
                    current.TotalAmount = 0;
                }
            }

            $scope.fnGetGroupBudgetsTotal = function () {
                var total = 0;
                //if ($scope.data.GroupBudgets.length > 0) {
                //    for (var i = 0; i < $scope.data.GroupBudgets.length; i++) {
                //        var TotalAmount = $scope.data.GroupBudgets[i].TotalAmount;
                //        total += parseFloat(TotalAmount);
                //    }
                //}
                return total;
            }
            $scope.fnGetGroupCashAdvancesTotal = function () {
                var total = 0;
                //if ($scope.data.Travelers.length > 0) {
                //    if ($scope.IndexTraveler != null) {
                //        for (var i = 0; i < $scope.data.Travelers[$scope.IndexTraveler].oCashAdvance.GroupCashAdvances.length; i++) {
                //            var Amount = $scope.data.Travelers[$scope.IndexTraveler].oCashAdvance.GroupCashAdvances[i].Amount;
                //            total += parseFloat(Amount);
                //        }
                //    }
                //}
                return total;
            }


        }]);
})();