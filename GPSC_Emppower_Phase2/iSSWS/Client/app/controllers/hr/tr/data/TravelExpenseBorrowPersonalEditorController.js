﻿(function () {
    angular.module('ESSMobile')
        .controller('TravelExpenseBorrowPersonalEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {

            $scope.Textcategory = 'ACCOUNT_SETTING';
            $scope.project = [{ "projectSelect": "" }];
            $scope.nrows = [];

            $scope.addRow = function () {

                $scope.project.push({
                    "projectSelect": ""
                });

                //console.log('row.', $scope.currency);
            };

            $scope.removeRow = function (index) {
                $scope.project.splice(index, 1);
            };

            $scope.getProductDetails = function (row, index) {
                $scope.project[index].projectSelect = row.projectSelect;
                console.log('project', $scope.project);
            }


            var URL = CONFIG.SERVER + 'HRTR/GetAllProject';
            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };

            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                $scope.projectlist = response.data;
                //$scope.projectSelect = "01";
                console.log('projectlist', $scope.projectlist);

            }, function errorCallback(response) {
                console.log('error projectlist.', response);

            });




        }]);

})();



