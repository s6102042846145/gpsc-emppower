﻿(function () {
angular.module('ESSMobile')
.controller('PerdiemDistributionController', ['$scope', '$http', '$routeParams', '$location', 'CONFIG', '$q', '$timeout', '$mdDialog', '$filter', '$interval', function ($scope, $http, $routeParams, $location, CONFIG, $q, $timeout, $mdDialog, $filter, $interval) {


    // C# OBJECT in perdiem distribution 
    // Table name perdium distribution

    // Master Data
    $scope.objCostcenter = [];
    $scope.objCostcenterDistribution = [];
    $scope.objOrganization = [];
    $scope.objIO = [];

    

    // variable
    $scope._list_perd_rows = [];
    $scope.TravelerIndex;
    $scope.searchText = {
        pro: '',
        cc: '',
        orgb: '',
        budg: '',
    }
  
    $scope.ExpenseRateTagCodeForPost;
    $scope.ExpenseRateTagCodeForPost_index;

    $scope.init = function($index) {
  
        $scope.TravelerIndex = 0;
        getMasterData_cc();
        getMasterData_orgb();
        getMasterData_budg();

        
        $scope.ExpenseRateTagCodeForPost = $scope.ExpenseRateTagCodeForPostList[$index].name;
        $scope.ExpenseRateTagCodeForPost_index = $index;
        var item = {
            isCheckedPerdiemDistribution: false,
            list:[],
            name: $scope.ExpenseRateTagCodeForPost
        }
        $scope.document.Additional.PerdiumDistributionList[$scope.ExpenseRateTagCodeForPost] = item;
        $scope.document.Additional.PerdiumDistributionList[$scope.ExpenseRateTagCodeForPost].isCheckedPerdiemDistribution = false;

        if ($scope.data.Readonly) {
            $scope.document.Additional.PerdiumDistributionList[$scope.ExpenseRateTagCodeForPost].isCheckedPerdiemDistribution = true;
        }
        var getReadyMasterData = $interval(function () {
            if ($scope.data.ProjectCostDistributions.length > 0 && $scope.objCostcenter.length > 0 && $scope.objOrganization.length > 0 && $scope.objIO.length > 0) {
                $interval.cancel(getReadyMasterData);
                getPerdiemDistribution();
            }

        }, 200);
    }
  
    $scope.$watch('data.oPivotPerdiums', function () {
        $scope.document.Additional.PerdiumDistributionList[$scope.ExpenseRateTagCodeForPost].isCheckedPerdiemDistribution = false;
    }, true);
    $scope.$watch('data.ProjectCostDistributions', function () {
        $scope.document.Additional.PerdiumDistributionList[$scope.ExpenseRateTagCodeForPost].isCheckedPerdiemDistribution = false;
    }, true);

  
    
    // call servervice
    function getMasterData_cc() {

        //Get CostCenter
        var URL = CONFIG.SERVER + 'HRTR/GetCostCenter';
        var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
        $http({
            method: 'POST',
            url: URL,
            data: oRequestParameter
        }).then(function successCallback(response) {
            $scope.objCostcenter = response.data;

            
        }, function errorCallback(response) {
            console.log('error PerdiemDistributionController GetCostcenter.', response);
        });

        //Get CostCenter Distribution
        URL = CONFIG.SERVER + 'HRTR/GetCostCenterDistribution';
        oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
        $http({
            method: 'POST',
            url: URL,
            data: oRequestParameter
        }).then(function successCallback(response) {
            $scope.objCostcenterDistribution = response.data;

           
        }, function errorCallback(response) {
            console.log('error PerdiemDistributionController GetCostcenterDistribution.', response);
        });

    }
    function getMasterData_orgb() {
        //Get Organization
        URL = CONFIG.SERVER + 'HRTR/GetOrganization';
        oRequestParameter = { InputParameter: { "CreateDate": $scope.document.CreatedDate }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
        $http({
            method: 'POST',
            url: URL,
            data: oRequestParameter
        }).then(function successCallback(response) {
            $scope.objOrganization = response.data;
            // Get IO
            //$scope.getMasterData_budg();
        }, function errorCallback(response) {
            console.log('error PerdiemDistributionController objOrganization.', response);
        });
    }
    function getMasterData_budg() {
        var URL = CONFIG.SERVER + 'HRTR/GetInternalOrderAll/';
        var oRequestParameter = { InputParameter: { "OrgUnit": ''/*group.AlternativeIOOrg*/ }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
        $http({
            method: 'POST',
            url: URL,
            data: oRequestParameter
        }).then(function successCallback(response) {
            $scope.objIO = response.data;
        }, function errorCallback(response) {
            console.log('error PerdiemDistributionController GetInternalOrderAll.', response);
        });
    }
    function getPerdiemDistribution() {
        var URL = CONFIG.SERVER + 'HRTR/GetPerdiumDistributionByRequestID/';
        var oRequestParameter = { InputParameter: { "RequestNo": $scope.document.RequestNo, "ExpenseRateTagCodeForPost": $scope.ExpenseRateTagCodeForPost }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
        $http({
            method: 'POST',
            url: URL,
            data: oRequestParameter
        }).then(function successCallback(response) {
            // build daa
            var data = response.data;
            if (!data) {
                return;
            }
            $scope._list_perd_rows = [];
            
            for (var i = 0; i < data.length; i++) {
                var project = $scope.data.ProjectCostDistributions.filter(function(item) { return item.ProjectCode == data[i].ProjectCode })[0];
                var costcenter = $scope.objCostcenter.filter(function (item) { return item.CostCenterCode == data[i].CostCenter })[0];
                var orgbudget = $scope.objOrganization.filter(function (item) { return item.ObjectID == data[i].AlternativeIOOrg })[0];
                var budget = $scope.objIO.filter(function (item) { return item.OrderID == data[i].IO })[0];
                var item = {
                    ExpenseRateTagCodeForPost: data[i].ExpenseRateTagCodeForPost,
                    project: project,
                    costcenter: costcenter,
                    orgbudget: orgbudget,
                    budget: budget,
                    searchText: {
                        pro: project.ProjectCode + ":" + project.ProjectName,
                        cc: costcenter.CostCenterCode + " : " + costcenter.LongDesc,
                        orgb: orgbudget.ObjectID + " : " + orgbudget.Text,
                        budg: budget.OrderID + " : " + budget.Descriptions,
                    },
                    IsAlternativeCostCenter: data[i].IsAlternativeCostCenter,
                    IsAlternativeIOOrg: data[i].IsAlternativeIOOrg,
                    amount: data[i].OriginalAmount // localAmount
                };
                $scope._list_perd_rows.push(item);
            }
            
            //$scope._list_perd_rows = ;
        }, function errorCallback(response) {
            console.log('error PerdiemDistributionController GetInternalOrderAll.', response);
        });
    }

    // listener
    $scope.addNewRow_list_perd_rows = function (ExpenseRateTagCodeForPost) {
        if ($scope.data.ProjectCostDistributions.length == 0 || $scope.objCostcenter.length == 0 || $scope.objCostcenterDistribution.length == 0 || $scope.objOrganization.length == 0 || $scope.objIO.length == 0) {
            //console.error('Master data not load');
        }
        var orgb = $scope.objOrganization[$scope.objOrganization.findIndexWithAttr('ObjectID', $scope.data.Travelers[$scope.TravelerIndex].OrgUnitID)];
        var item = {
            ExpenseRateTagCodeForPost: ExpenseRateTagCodeForPost,
            project: $scope.data.ProjectCostDistributions[0].ProjectObject,
            costcenter: $scope.objCostcenterDistribution[0],
            orgbudget: orgb,
            budget: null,
            searchText: {
                pro:    $scope.data.ProjectCostDistributions[0].ProjectObject.ProjectCode + ":" + $scope.data.ProjectCostDistributions[0].ProjectObject.Name,
                cc:     $scope.objCostcenterDistribution[0].CostCenterCode + " : " + $scope.objCostcenterDistribution[0].LongDesc,
                orgb:   orgb.ObjectID + " : " + orgb.Text,
                budg:   '',
            },
            IsAlternativeCostCenter: false,
            IsAlternativeIOOrg: false,
            amount:0 // localAmount
        }; 
        $scope._list_perd_rows.push(item);
        $scope.DefaultIO($scope._list_perd_rows.length - 1);
        $scope.document.Additional.PerdiumDistributionList[$scope.ExpenseRateTagCodeForPost].isCheckedPerdiemDistribution = false;
 
    }
    $scope.removAt_list_perd_rows = function ($index) {
        $scope._list_perd_rows.splice($index, 1);
        $scope.document.Additional.PerdiumDistributionList[$scope.ExpenseRateTagCodeForPost].isCheckedPerdiemDistribution = false;
    }

    // function
    $scope.validateValue = function ($index) {
        var row = $scope._list_perd_rows[$index];
        if (Number(row.amount) || Number(row.amount) == 0) {
            row.amount = Number(row.amount);
        } else {
            _alert('Only numeric is allow.'); // fixed
            row.amount = 0;
        }
        $scope.document.Additional.PerdiumDistributionList[$scope.ExpenseRateTagCodeForPost].isCheckedPerdiemDistribution = false;
    }
    $scope.checkDataAndApplyData = function () {

        var sum__ = $scope.ExpenseRateTagCodeForPostList[$scope.ExpenseRateTagCodeForPost_index].sum;

        // validated projecct
        for (var i = 0 ; i < $scope._list_perd_rows.length; i++) {
            var row = $scope._list_perd_rows[i];
            var project = $scope.data.ProjectCostDistributions.filter(function (item) {
                return item.ProjectCode == $scope._list_perd_rows[i].project.ProjectCode
            });
            if (project.length == 0) {
                _alert('Selected project not found at line '+(i+1)); 
                return;
            }
        }

        // validated empty list
        if ($scope._list_perd_rows.length == 0 && sum__ != 0) {
            _alert('Perdiem Distribute list cannot be empty'); // fixed
            $scope.document.Additional.PerdiumDistributionList[$scope.ExpenseRateTagCodeForPost].isCheckedPerdiemDistribution = false;
            return;
        }

        // validate duplicated item
        for (var i = 0 ; i < $scope._list_perd_rows.length; i++) {
            var row = $scope._list_perd_rows[i];
            for (var a = i ; a < $scope._list_perd_rows.length; a++) {
                var row_next = $scope._list_perd_rows[a];
                if (row.project.ProjectCode == row_next.project.ProjectCode &&
                    row.costcenter.CostCenterCode == row_next.costcenter.CostCenterCode &&
                    row.orgbudget.ObjectID == row_next.orgbudget.ObjectID &&
                    row.budget.OrderID == row_next.budget.OrderID &&
                    a != i) {
                    _alert('Found duplicated item at line ' + (i + 1) + ' and line' + (a + 1) + '.'); // fixed
                    $scope.document.Additional.PerdiumDistributionList[$scope.ExpenseRateTagCodeForPost].isCheckedPerdiemDistribution = false;
                    return;
                }
            }
        }
        // validate sumvalue and equal to zero
        var sum = 0;
        for (var i = 0 ; i < $scope._list_perd_rows.length; i++) {
            var row = $scope._list_perd_rows[i];
            sum = sum + row.amount;
            if (row.amount == 0) {
                _alert('Found amount is 0 at line ' + (i + 1)); // fixed
                $scope.document.Additional.PerdiumDistributionList[$scope.ExpenseRateTagCodeForPost].isCheckedPerdiemDistribution = false;
                return;
            }
        }
        sum = sum.toFixed(2);
        
        if (sum != sum__.toFixed(2)) {
            _alert('Sum of all amount must equal to amount of perdiem.'); // fixed
            $scope.document.Additional.PerdiumDistributionList[$scope.ExpenseRateTagCodeForPost].isCheckedPerdiemDistribution = false;
            return;
        }

        // addpter step
        var templist = [];
        for (var i = 0; i < $scope._list_perd_rows.length; i++) {
           
            var item = {
                RequestNo:$scope.document.RequestNo,
                ExpenseRateTagCodeForPost: $scope._list_perd_rows[i].ExpenseRateTagCodeForPost,
                ProjectCode:$scope._list_perd_rows[i].project.ProjectCode,
                CostCenter: $scope._list_perd_rows[i].costcenter.CostCenterCode,
                IO: $scope._list_perd_rows[i].budget.OrderID,
                AlternativeIOOrg: $scope._list_perd_rows[i].orgbudget.ObjectID,
                OriginalAmount: $scope._list_perd_rows[i].amount,
                LocalAmount: $scope._list_perd_rows[i].amount,
                OriginalAmountInRight: 999999,
                LocalAmountInRight: 9999999,
                IsAlternativeCostCenter:$scope._list_perd_rows[i].IsAlternativeCostCenter,
                IsAlternativeIOOrg: $scope._list_perd_rows[i].IsAlternativeIOOrg,
            };
            templist.push(item);
        }



        if (!$scope.document.Additional.PerdiumDistributionList[$scope.ExpenseRateTagCodeForPost]) {
            var keys = Object.keys($scope.document.Additional.PerdiumDistributionList);
            for (var i = 0 ; i < keys.length; i++) {
                if (isNormalInteger(keys[i])) {
                    $scope.document.Additional.PerdiumDistributionList.splice(i, 1);
                    keys.splice(i, 1);
                    i = i - 1;
                }
            }
            var item = {
                isCheckedPerdiemDistribution: false,
                list: [],
                name: $scope.ExpenseRateTagCodeForPost
            }
            $scope.document.Additional.PerdiumDistributionList[$scope.ExpenseRateTagCodeForPost] = item;
        }

        $scope.document.Additional.PerdiumDistributionList[$scope.ExpenseRateTagCodeForPost].list = angular.copy(templist);
        $scope.document.Additional.PerdiumDistributionList[$scope.ExpenseRateTagCodeForPost].isCheckedPerdiemDistribution = true;
    }
    function isNormalInteger(str) {
        var n = Math.floor(Number(str));
        return String(n) === str && n >= 0;
    }
    function _alert(message) {
        $mdDialog.show(
            $mdDialog.alert()
            .clickOutsideToClose(true)
            .title('Warning') // fixed
            .textContent(message)
            .ok('OK') // fixed
        );
    }


    // auto complete project
    var tempResult_project;
    $scope.tryToSelect_project = function (text, $index) {
        for (var i = 0; i < $scope.data.ProjectCostDistributions.length; i++) {
            if (($scope.data.ProjectCostDistributions[i].ProjectObject.ProjectCode + ":" + $scope.data.ProjectCostDistributions[i].ProjectObject.Name) == text || $scope.data.ProjectCostDistributions[i].ProjectObject.Name == text) {
                tempResult_project = $scope.data.ProjectCostDistributions[i].ProjectObject;
                break;
            }
        }
        $scope._list_perd_rows[$index].searchText.pro = '';
        
    }
    $scope.checkText_project = function (text, $index) {
        var result = null;
        for (var i = 0; i < $scope.data.ProjectCostDistributions.length; i++) {
            if (($scope.data.ProjectCostDistributions[i].ProjectObject.ProjectCode + ":" + $scope.data.ProjectCostDistributions[i].ProjectObject.Name) == text || $scope.data.ProjectCostDistributions[i].ProjectObject.Name == text) {
                result = $scope.data.ProjectCostDistributions[i].ProjectObject;
                break;
            }
        }
        if (result) {
            $scope._list_perd_rows[$index].searchText.pro = result.ProjectCode + ":" + result.Name;
            $scope.selectedItem_project(result, $index);
        } else if (tempResult_project) {
            $scope._list_perd_rows[$index].searchText.pro = tempResult_project.ProjectCode + ":" + tempResult_project.Name;
            $scope.selectedItem_project(tempResult_project, $index);
        }
    }
    $scope.selectedItem_project = function (item, $index) {
        $scope._list_perd_rows[$index].project = item;
        $scope.document.Additional.PerdiumDistributionList[$scope.ExpenseRateTagCodeForPost].isCheckedPerdiemDistribution = false;
    }
    $scope.querySearch_project = function (query) {
        var results = query ? $scope.data.ProjectCostDistributions.filter(createFilterFor_project(query)) : $scope.data.ProjectCostDistributions;
        return results;
    }
    function createFilterFor_project(query) {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(item) {
            // text conbined to search
            var textquery =  angular.lowercase(item.ProjectCode + ':' + item.Name);
            return (textquery.indexOf(lowercaseQuery) >= 0);
        };
    }

    //auto complete CostCenter 
    var tempResult_cc;
    $scope.tryToSelect_cc = function (text, $index) {
        if ($scope._list_perd_rows && $scope._list_perd_rows[$index].IsAlternativeCostCenter) {
            for (var i = 0; i < $scope.objCostcenter.length; i++) {
                if (($scope.objCostcenter[i].CostCenterCode + " : " + $scope.objCostcenter[i].LongDesc) == text || $scope.objCostcenter[i].LongDesc == text) {
                    tempResult_cc = $scope.objCostcenter[i];
                    break;
                }
            }
        } else {
            for (var i = 0; i < $scope.objCostcenterDistribution.length; i++) {
                if (($scope.objCostcenterDistribution[i].CostCenterCode + " : " + $scope.objCostcenterDistribution[i].LongDesc) == text || $scope.objCostcenterDistribution[i].LongDesc == text) {
                    tempResult_cc = $scope.objCostcenterDistribution[i];
                    break;
                }
            }
        }

        $scope._list_perd_rows[$index].searchText.cc = '';
        $scope.selectedItemCostCenterChange(null, $index);
    }
    $scope.checkText_cc = function (text, $index) {
        var result = null;

        if ($scope._list_perd_rows && $scope._list_perd_rows[$index].IsAlternativeCostCenter) {
            for (var i = 0; i < $scope.objCostcenter.length; i++) {
                if (($scope.objCostcenter[i].CostCenterCode + " : " + $scope.objCostcenter[i].LongDesc) == text || $scope.objCostcenter[i].LongDesc == text) {
                    result = $scope.objCostcenter[i];
                    break;
                }
            }
        } else {
            for (var i = 0; i < $scope.objCostcenterDistribution.length; i++) {
                if (($scope.objCostcenterDistribution[i].CostCenterCode + " : " + $scope.objCostcenterDistribution[i].LongDesc) == text || $scope.objCostcenterDistribution[i].LongDesc == text) {
                    result = $scope.objCostcenterDistribution[i];
                    break;
                }
            }
        }
        if (result) {
            $scope._list_perd_rows[$index].searchText.cc = result.CostCenterCode + " : " + result.LongDesc;
            $scope.selectedItemCostCenterChange(result, $index);
        } else if (tempResult_cc) {
            $scope._list_perd_rows[$index].searchText.cc = tempResult_cc.CostCenterCode + " : " + tempResult_cc.LongDesc;
            $scope.selectedItemCostCenterChange(tempResult_cc, $index);
        }
    }
    $scope.selectedItemCostCenterChange = function (item, $index) {
        $scope._list_perd_rows[$index].costcenter = item;
        $scope.document.Additional.PerdiumDistributionList[$scope.ExpenseRateTagCodeForPost].isCheckedPerdiemDistribution = false;
    };
    $scope.querySearchCostCenter = function (query, $index) {
        if (!$scope.objCostcenter || !$scope.objCostcenterDistribution) return;
        if ($scope._list_perd_rows && $scope._list_perd_rows[$index].IsAlternativeCostCenter) {
            var results = query ? $scope.objCostcenter.filter(createFilterForCostCenter(query)) : $scope.objCostcenter;
        }
        else {
            var results = query ? $scope.objCostcenterDistribution.filter(createFilterForCostCenter(query)) : $scope.objCostcenterDistribution;
        } 
        return results;
    };
    function createFilterForCostCenter(query) {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(x) {
            if (!x) return false;
            var source = angular.lowercase(x.CostCenterName);
            return (source.indexOf(lowercaseQuery) >= 0);
        };
    }
    $scope.AlternativeCostCenter = function (row, $index) {
        if (!$scope._list_perd_rows[$index].costcenter.IsAlternativeCostCenter) {
            $scope._list_perd_rows[$index].costcenter = $scope.objCostcenterDistribution[0];
        }
        else {
            $scope._list_perd_rows[$index].costcenter = $scope.objCostcenter[$scope.objCostcenter.findIndexWithAttr('CostCenterCode', $scope.objCostcenterDistribution[0].CostCenterCode)];
        }
        $scope._list_perd_rows[$index].searchText.cc = $scope._list_perd_rows[$index].costcenter.CostCenterCode + " : " + $scope._list_perd_rows[$index].costcenter.LongDesc;
    }; //AlternativeIOOrg
    
    //auto complete AlternativeIOOrg 
    var tempResult_orgb;
    $scope.tryToSelect_orgb = function (text, $index, IsAlternativeIOOrg) {
        if (!IsAlternativeIOOrg) { return; }
        for (var i = 0; i < $scope.objOrganization.length; i++) {
            if (($scope.objOrganization[i].ObjectID + " : " + $scope.objOrganization[i].Text) == text || $scope.objOrganization[i].Text == text) {
                tempResult_orgb = $scope.objOrganization[i];
                break;
            }
        }

        $scope._list_perd_rows[$index].searchText.orgb = '';
        $scope.selectedItemAlternativeIOOrgChange(null, $index);
    }
    $scope.checkText_orgb = function (text, $index) {
        var result = null;
        for (var i = 0; i < $scope.objOrganization.length; i++) {
            if (($scope.objOrganization[i].ObjectID + " : " + $scope.objOrganization[i].Text) == text || $scope.objOrganization[i].Text == text) {
                result = $scope.objOrganization[i];
                break;
            }
        }
        if (result) {
            $scope._list_perd_rows[$index].searchText.orgb = result.ObjectID + " : " + result.Text;
            $scope.selectedItemAlternativeIOOrgChange(result, $index);
        } else if (tempResult_orgb) {
            $scope._list_perd_rows[$index].searchText.orgb = tempResult_orgb.ObjectID + " : " + tempResult_orgb.Text;
            $scope.selectedItemAlternativeIOOrgChange(tempResult_orgb, $index);
        }
    }
    $scope.selectedItemAlternativeIOOrgChange = function (item, $index) {
        $scope._list_perd_rows[$index].orgbudget = item;
        $scope.DefaultIO($index);
        $scope.document.Additional.PerdiumDistributionList[$scope.ExpenseRateTagCodeForPost].isCheckedPerdiemDistribution = false;
    };
    $scope.querySearchAlternativeIOOrg = function (query, index) {
        if (!$scope.objOrganization) return;
        var results = query ? $scope.objOrganization.filter(createFilterForAlternativeIOOrg(query)) : $scope.objOrganization, deferred;
        if ($scope.simulateQuery) {
            deferred = $q.defer();
            $timeout(function () { deferred.resolve(results); }, Math.random() * 250, false);
            return deferred.promise;
        } else {
            return results;
        }
    };
    function createFilterForAlternativeIOOrg(query) {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(x) {
            if (!x) return false;
            var source = angular.lowercase(x.BudgetOrgName);
            return (source.indexOf(lowercaseQuery) >= 0);
        };
    }
    $scope.AlternativeIOOrg = function (row, $index) {
        if (!$scope._list_perd_rows[$index].IsAlternativeIOOrg) {
            $scope._list_perd_rows[$index].orgbudget = $scope.objOrganization[$scope.objOrganization.findIndexWithAttr('ObjectID', $scope.data.Travelers[$scope.TravelerIndex].OrgUnitID)];
            $scope._list_perd_rows[$index].searchText.orgb = $scope._list_perd_rows[$index].orgbudget.ObjectID + " : " + $scope._list_perd_rows[$index].orgbudget.Text;
        }
        else {
            if ($scope.data.Travelers[$scope.TravelerIndex].OrgUnitID != $scope._list_perd_rows[$index].orgbudget.AlternativeIOOrg) {
            }
        }
        $scope.DefaultIO($index);
    };
   
    //auto complete IO
    var tempResult_budg;
    $scope.tryToSelect_budg = function (text, $index) {
        var found = false;
        for (var i = 0; i < $scope.objIO.length; i++) {
            if (($scope.objIO[i].OrderID + " : " + $scope.objIO[i].Description) == text || $scope.objIO[i].Description == text) {
                tempResult_budg = $scope.objIO[i];
                found = true;
                break;
            }
        }
        if (!found) {
            tempResult_budg = null;
        }

        $scope._list_perd_rows[$index].searchText.budg = '';
        $scope.selectedItemIOChange(null, $index);
    }
    $scope.checkText_budg = function (text, $index) {
        var result = null;
        for (var i = 0; i < $scope.objIO.length; i++) {
            if (($scope.objIO[i].OrderID + " : " + $scope.objIO[i].Description) == text || $scope.objIO[i].Description == text) {
                result = $scope.objIO[i];
                break;
            }
        }
        if (result) {
            $scope._list_perd_rows[$index].searchText.budg = result.OrderID + " : " + result.Description;
            $scope.selectedItemIOChange(result, $index);
        } else if (tempResult_budg) {
            $scope._list_perd_rows[$index].searchText.budg = tempResult_budg.OrderID + " : " + tempResult_budg.Description;
            $scope.selectedItemIOChange(tempResult_budg, $index);
          
        }
    }
    $scope.selectedItemIOChange = function (item, $index) {
        $scope._list_perd_rows[$index].budget = item;
        $scope.document.Additional.PerdiumDistributionList[$scope.ExpenseRateTagCodeForPost].isCheckedPerdiemDistribution = false;
    };
    $scope.querySearchIO = function (query, $index) {
        if (!$scope.objIO) return;
        var results = query ? $scope.objIO.filter(createFilterForIO($index)).filter(createFilterSearchForIO(query)) : $scope.objIO.filter(createFilterForIO($index));
        return results;
    };
    function createFilterSearchForIO(query) {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(item) {
            if (!item) return false;
            var source = angular.lowercase(item.OrderID + ':' + item.Description);
            return (source.indexOf(lowercaseQuery) >= 0) && (item);
        };
    }
    function createFilterForIO($index) {
        return function filterFn(item) {
                if (!item) {
                    return false;
                } else {
                    if ($scope._list_perd_rows[$index].orgbudget) {
                        return item.ObjectID == $scope._list_perd_rows[$index].orgbudget.ObjectID;
                    } else {
                        return false;
                    }
                    
                };
            }  
    }
    $scope.DefaultIO = function ($index) {
        
        var tempObjIOList = angular.copy($scope.objIO.filter(createFilterForIO($index)));
        if (tempObjIOList.findIndexWithAttr('OrderID', $scope._list_perd_rows[$index].IO) < 0) {
            $scope._list_perd_rows[$index].budget =  angular.copy(tempObjIOList.filter(createFilterForIO($index))[0]);
            if ($scope._list_perd_rows[$index].budget) {
                $scope._list_perd_rows[$index].searchText.budg = $scope._list_perd_rows[$index].budget.OrderID + " : " + $scope._list_perd_rows[$index].budget.Description;
            } else {
                $scope._list_perd_rows[$index].budget = null;
                $scope._list_perd_rows[$index].searchText.budg = '';
            }
            
        }

    }
    


    



}]);
})();