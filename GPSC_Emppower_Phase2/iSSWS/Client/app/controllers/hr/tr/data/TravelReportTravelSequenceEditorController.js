﻿(function () {
    angular.module('ESSMobile')
        .controller('TravelReportTravelSequenceEditorController', ['$scope', '$http', '$routeParams', '$location', '$window', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $window, $filter, CONFIG, $mdDialog) {
            //TravelSchedulePlaces
            $scope.Textcategory = 'EXPENSE';
            $scope.Textcategory2 = 'SYSTEM';
            var oSchedulePlaceID = 0;
            $scope.provinceSelect = true;

            $scope.removeRow = function (index,ev) {
                if (!$scope.IsReceiptOrNewExchangeRate()) {

                    if ($scope.data.TravelSchedulePlaces.length == 1) {
                        $mdDialog.show(
                         $mdDialog.alert()
                           .clickOutsideToClose(false)
                           .title($scope.Text['SYSTEM']['WARNING'])
                           .textContent($scope.Text['EXPENSE']['CANT_REMOVE_LAST_TRAVEL'])
                           .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                        );
                        return;
                    }
                    $scope.showConfirm(index, ev);
                }
            };
            $scope.showConfirm = function (index, ev) {
                // Appending dialog to document.body to cover sidenav in docs app
                var confirm = $mdDialog.confirm()
                      .title($scope.Text['SYSTEM']['WARNING'])
                      .textContent($scope.Text['SYSTEM']['CONFIRM_DELETE'])
                      .targetEvent(ev)
                      .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                      .cancel($scope.Text['SYSTEM']['BUTTON_CANCEL']);

                $mdDialog.show(confirm).then(function () {
                    $scope.data.TravelSchedulePlaces.splice(index, 1);
                    oSchedulePlaceID = oSchedulePlaceID - 1;
                }, function () {
                });
            };
            $scope.IsReceiptOrNewExchangeRate = function ()
            {
                var flgCheck = false;
                if (angular.isDefined($scope.document.Additional.TravelReport) && angular.isDefined($scope.document.Additional.ExpenseReportReceiptList) && $scope.document.Additional.ExpenseReportReceiptList && $scope.document.Additional.ExpenseReportReceiptList.length > 0) {
                    flgCheck = true;
                }
                if (!flgCheck) {
                    if (angular.isDefined($scope.data.ExchangeRateCaptures) && $scope.data.ExchangeRateCaptures.length > 0) {
                        for (var i = 0; i < $scope.data.ExchangeRateCaptures.length; i++) {
                            if (!$scope.data.ExchangeRateCaptures[i].IsLock)
                            {
                                flgCheck = true;
                                break;
                            }
                        }
                    }
                }
                if (flgCheck) {
                    $mdDialog.show(
                     $mdDialog.alert()
                       .clickOutsideToClose(false)
                       .title($scope.Text['SYSTEM']['WARNING'])
                       .textContent($scope.Text['SYSTEM']['CANTREMOVEISRECEIPT_EXCHANGERATE'])
                       .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                return flgCheck;
            }

            $scope.InitialConfig = function () {
                if ($scope.document.RequestTypeID == 113) {
                    $scope.data.Readonly = true;
                }
                
            };

            

            $scope.onSelectedCountry = function (Country) {
                var URL = CONFIG.SERVER + 'HRTR/GetProvinceByCountry/';
                var oRequestParameter = { InputParameter: { "CountryCode": Country }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.ProvinceList = response.data;
                    if ($scope.ProvinceList != null && $scope.ProvinceList.length > 0) {

                        $scope.provinceSelect = true;
                    }
                    else {
                        $scope.provinceSelect = false;
                    }
                    console.log('onSelectedCountry.', $scope.ProvinceList);
                }, function errorCallback(response) {
                    console.log('error onSelectedCountry', response);
                });
            };

            $scope.goBackRequest = function () {
                $scope.finishFormWizard();
                $scope.data.TravelSeqEdit = false;
            };

            $scope.getRemarkText = function (isWarning) {
                if (isWarning) {
                    return $Text['EXPENSE']['AREA_WARNING']
                }
                return '-';
            };

            var initHours = function () {
                var h = [];
                for (var i = 0; i < 24; i++) {
                    var text = ("00" + i.toString()).substr(-2, 2);
                    h.push(text);
                }
                return h;
            };
            $scope.hours = initHours();

            var initMinutes = function () {
                var m = [];
                for (var i = 0; i < 60; i++) {
                    var text = ("00" + i.toString()).substr(-2, 2);
                    m.push(text);
                }
                return m;
            };
            $scope.minutes = initMinutes();

            // datetime event
            $scope.setSelectedBeginDate = function (obj, selectedDate, index) {
                obj.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-ddT' + $scope.formDataTravelReport.travelSequence[index].startHour + ':' + $scope.formDataTravelReport.travelSequence[index].startMin + ':00');
                if (obj.BeginDate > obj.EndDate) {
                    obj.EndDate = obj.BeginDate;
                }
                //$scope.data.TravelSchedulePlaces[index].BeginDate = $filter('date')(selectedDate, 'yyyy-MM-ddTHH:mm:00');
            };

            $scope.setSelectedEndDate = function (obj, selectedDate, index) {
                obj.EndDate = $filter('date')(selectedDate, 'yyyy-MM-ddT' + $scope.formDataTravelReport.travelSequence[index].endHour + ':' + $scope.formDataTravelReport.travelSequence[index].endMin + ':00');
                //$scope.data.TravelSchedulePlaces[index].EndDate = $filter('date')(selectedDate, 'yyyy-MM-ddTHH:mm:00');
            };

          
            $scope.getDate = function (date) {
                var oDate = new Date(date + '+07:00');
                return oDate;
            }
            $scope.checkMaxDate = function (index,obj) {
                checkMaxDate(index, obj);
            }
            function checkMaxDate(index,obj) {
                var startDate = new Date($scope.formDataTravelReport.travelSequence[index].m_startDate);
                var endDate = new Date($scope.formDataTravelReport.travelSequence[index].m_endDate);
                $scope.formDataTravelReport.travelSequence[index].startDate = $scope.formDataTravelReport.travelSequence[index].m_startDate;
                if ($scope.formDataTravelReport.travelSequence[index].m_startDate > $scope.formDataTravelReport.travelSequence[index].m_endDate) {
                    $scope.formDataTravelReport.travelSequence[index].m_endDate = $scope.formDataTravelReport.travelSequence[index].m_startDate;
                    $scope.formDataTravelReport.travelSequence[index].endDate = $scope.formDataTravelReport.travelSequence[index].m_startDate;
                    var BeginDate = $filter('date')($scope.formDataTravelReport.travelSequence[index].m_startDate, 'yyyy-MM-ddTHH:mm:00');
                    console.debug(BeginDate);
                    obj.BeginDate = BeginDate;
                    $scope.setEndDate(index, obj);
                    return;
                }
                var BeginDate = $filter('date')($scope.formDataTravelReport.travelSequence[index].m_startDate, 'yyyy-MM-ddTHH:mm:00');
                console.debug(BeginDate);
                obj.BeginDate = BeginDate;
               
            }
            $scope.setEndDate = function (index, obj) {
                console.log('setEndDate');
                $scope.formDataTravelReport.travelSequence[index].endDate = $scope.formDataTravelReport.travelSequence[index].m_endDate;
                var EndDate = $filter('date')($scope.formDataTravelReport.travelSequence[index].m_endDate, 'yyyy-MM-ddTHH:mm:00');
                console.debug(EndDate);
                obj.EndDate = EndDate;
            }
            $scope.getDateFormate = function(date) {
                return $filter('date')(date, 'd/M/yyyy');
            }
         


            $scope.changeStartHour = function (obj, newHour, index) {
                var objDate = new Date($scope.getLocalDateString(obj.BeginDate));
                objDate.setHours(Number(newHour));
                obj.BeginDate = $filter('date')(objDate, 'yyyy-MM-ddTHH:mm:00');
                console.log('new date.', obj.BeginDate);
                //$scope.data.TravelSchedulePlaces[index].BeginDate = $filter('date')(objDate, 'yyyy-MM-ddTHH:mm:00');
            };

            $scope.changeEndHour = function (obj, newHour, index) {
                var objDate = new Date($scope.getLocalDateString(obj.EndDate));
                objDate.setHours(Number(newHour));
                obj.EndDate = $filter('date')(objDate, 'yyyy-MM-ddTHH:mm:00');
                console.log('new date.', obj.EndDate);
                //$scope.data.TravelSchedulePlaces[index].EndDate = $filter('date')(objDate, 'yyyy-MM-ddTHH:mm:00');
            };

            $scope.changeStartMin = function (obj, newMinute, index) {
                var objDate = new Date($scope.getLocalDateString(obj.BeginDate));
                objDate.setMinutes(Number(newMinute));
                obj.BeginDate = $filter('date')(objDate, 'yyyy-MM-ddTHH:mm:00');
                console.log('new date.', obj.BeginDate);
                //$scope.data.TravelSchedulePlaces[index].BeginDate = $filter('date')(objDate, 'yyyy-MM-ddTHH:mm:00');
            };

            $scope.changeEndMin = function (obj, newMinute, index) {
                var objDate = new Date($scope.getLocalDateString(obj.EndDate));
                objDate.setMinutes(Number(newMinute));
                obj.EndDate = $filter('date')(objDate, 'yyyy-MM-ddTHH:mm:00');
                console.log('new date.', obj.EndDate);
                //$scope.data.TravelSchedulePlaces[index].EndDate = $filter('date')(objDate, 'yyyy-MM-ddTHH:mm:00');
            };
            // !datetime event


            // #region  timeline ui function



            $scope.getTravelTransportationTypeIcon = function () {
                if (!$scope.settings.Master.TransportationTypeList) return 'fa-car';
                let vehicle = null;
                for (let i = 0; i < $scope.settings.Master.TransportationTypeList.length; i++) {
                    if ($scope.settings.Master.TransportationTypeList[i].TransportationTypeCode == $scope.document.Additional.Travelers[0].TransportationTypeCode) {
                        vehicle = $scope.settings.Master.TransportationTypeList[i];
                        break;
                    }
                }
                //let vehicle = $scope.settings.Master.TransportationTypeList.find(m => m.TransportationTypeCode == $scope.document.Additional.Travelers[0].TransportationTypeCode);
                if (vehicle.IsPlane == true) {
                    return 'fa-plane-departure';
                } else {
                    return 'fa-car';
                }
            }


            // #endregion

        }]);
})();



