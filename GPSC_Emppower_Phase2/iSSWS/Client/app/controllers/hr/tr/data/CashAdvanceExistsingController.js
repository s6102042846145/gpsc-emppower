﻿(function () {
    angular.module('ESSMobile')
        .controller('CashAdvanceExistsingController', ['$scope', '$http', '$routeParams', '$location', '$window', '$filter', 'CONFIG', '$q', '$timeout', '$mdDialog', function ($scope, $http, $routeParams, $location, $window, $filter, CONFIG, $q, $timeout, $mdDialog) {

            $scope.CashAdvanceDetail = '';

            $scope.InitialConfig = function () {
                var URL = CONFIG.SERVER + 'HRTR/GetCashAdvanceExisting';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.CashAdvanceDetail = response.data;
                },
                function errorCallback(response) {
                    console.log('error InitialConfig.', response);
                });
            }

        }]);

})();


