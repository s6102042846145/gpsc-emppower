﻿(function () {
    angular.module('ESSMobile')
        .controller('InProgressDocController', ['$scope', '$http', '$routeParams', '$location', '$window', '$filter', 'CONFIG', '$q', '$timeout', '$mdDialog', function ($scope, $http, $routeParams, $location, $window, $filter, CONFIG, $q, $timeout, $mdDialog) {

            $scope.CashAdvanceDetail = '';
            $scope.searchText = '';

            $scope.pagin = {
                currentPage: 1, itemPerPage: '10', numPage: 1, cbbItemPerPage: [ '10','20','50' ]
            }

            $scope.initialInProgressDoc = function () {
                console.log('init in progress doc')
                $scope.pagin.itemPerPage = $scope.pagin.cbbItemPerPage[0];
                console.log('pagin', $scope.pagin)

                console.log('$scope.data.InprogressDocument', $scope.data.InprogressDocument);

                //for (var i = 0; i < 100; i++) {
                //    $scope.data.InprogressDocument.push($scope.data.InprogressDocument[0]);
                //}
            }

            $scope.getDatas = function () {
                var datas = [];
                datas = $scope.data.InprogressDocument.filter(function (m) {
                    return m.RequestNo.indexOf($scope.searchText) != -1
                    || m.Detail.indexOf($scope.searchText) != -1
                    || m.Status.indexOf($scope.searchText) != -1
                });
                setNumOfPage(datas);

                var res = [];

                if ($scope.pagin.numPage > 1) {
                    datas.forEach(function (value, i) {
                        var from = (parseInt($scope.pagin.currentPage) - 1) * parseInt($scope.pagin.itemPerPage);
                        var to = from + parseInt($scope.pagin.itemPerPage);

                        if (from <= i && to > i)
                            res.push(value);
                    });
                } else {
                    res = datas.slice();
                }

                return res;
            }

            var genPaginData = function () {
                setNumOfPage();
            }

            var setNumOfPage = function (datas) {
                var totalItems = 0;
                if (datas) totalItems = datas.length;
                $scope.pagin.numPage = Math.ceil(totalItems / $scope.pagin.itemPerPage);
                if ($scope.pagin.numPage <= 0) $scope.pagin.numPage = 1;
            }

            $scope.getPages = function() {
                var pages = [];

                for (var i = 0; i < $scope.pagin.numPage; i++) {
                    pages.push((i + 1));
                }

                return pages;
            }

            $scope.changePage = function (targetPage) {
                $scope.pagin.currentPage = angular.copy(targetPage);
            }

        }]);

})();


