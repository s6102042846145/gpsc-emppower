﻿(function () {
    angular.module('ESSMobile')
        .controller('TravelExpensePersonalCashAdvanceController', ['$scope', '$http', '$routeParams', '$location', 'CONFIG', function ($scope, $http, $routeParams, $location, CONFIG) {
            //place code to do
            // console.log('TravelExpensePersonalCashAdvanceController')
            $scope.Textcategory = 'TRAVELREQUEST';
            $scope.IndexTraveler = 0;

        

            $scope.limitTotalAmount = function (current,index) {
                if (current.TotalAmount) {
                    if (current.TotalAmount > $scope.data.Travelers[$scope.IndexTraveler].PersonalBudgets[index].TotalAmount) {
                        current.TotalAmount = $scope.data.Travelers[$scope.IndexTraveler].PersonalBudgets[index].TotalAmount;
                    }
                }
                else {
                    current.TotalAmount = 0;
                }
                $scope.fnGetPersonalCashAdvancesTotal();

            }
            $scope.limitTotalAmount_blur = function (current, index) {
                if (current.TotalAmount) {
                }
                else {
                    if (current.TotalAmount == 0) return;
                    current.TotalAmount = $scope.data.Travelers[$scope.IndexTraveler].PersonalBudgets[index].TotalAmount;
                }
                $scope.fnGetPersonalCashAdvancesTotal();
            }

            $scope.InitialConfig = function () {
                $scope.fnGetPersonalCashAdvancesTotal();
            };



            $scope.getTotalDaySalary = function () {
                var total = 0;
                if ($scope.data.Travelers[0].oCashAdvance.IsBorrowALLOWANCE)
                    total = $scope.data.Travelers[0].TotalAllowanceAmount;
                return total;
            };

            $scope.getTotalAccommodation = function () {
                var total = 0;
                if ($scope.data.Travelers[0].oCashAdvance.IsBorrowACCOMODATION)
                    total = $scope.data.Travelers[0].TotalAccommodationAmount;
                return total;
            };

           

            $scope.fnGetPersonalCashAdvancesTotal = function () {
                //var total = $scope.fnGetPerdiamsTotal();
                var total = 0;
                if ($scope.data.Travelers.length > 0) {
                    for (var i = 0; i < $scope.data.Travelers[$scope.IndexTraveler].oCashAdvance.PersonalCashAdvances.length; i++) {
                        total += parseFloat($scope.data.Travelers[$scope.IndexTraveler].oCashAdvance.PersonalCashAdvances[i].TotalAmount);
                    }
                }
                $scope.data.Travelers[$scope.IndexTraveler].oCashAdvance.TotalAmount = total;

                if ($scope.data.Travelers[0].oCashAdvance.IsBorrowALLOWANCE)
                    total += $scope.data.Travelers[0].TotalAllowanceAmount;
                if ($scope.data.Travelers[0].oCashAdvance.IsBorrowACCOMODATION)
                    total += $scope.data.Travelers[0].TotalAccommodationAmount;

                if ($scope.data.CAMode == 'Perdium') {
                    $scope.data.TotalAmount = $scope.data.Travelers[$scope.IndexTraveler].oCashAdvance.GroupCashAdvances.sum('TotalAmount') + total;
                }
                if ($scope.data.IsPersonalCashAdvance == true) {
                    $scope.data.CAEstimate = $scope.data.Travelers[$scope.IndexTraveler].TotalAmount;
                } else {
                    $scope.data.CAEstimate = $scope.data.GroupBudgets.sum('TotalAmount') + $scope.data.Travelers[$scope.IndexTraveler].TotalAmount;
                }

                return total;
            };



        }]);
})();