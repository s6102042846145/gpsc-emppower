﻿(function () {
    angular.module('ESSMobile')
        .controller('TravelExpensePersonalEstimateEditorController_ForAverageMode', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$window', '$timeout', '$q', '$log', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $window, $timeout, $q, $log, $mdDialog) {
            $scope.Textcategory = 'TRAVELEXPENSE';
            $scope.Textcategory2 = 'CURRENCY';
            $scope.Textcategory3 = 'SYSTEM';
            $scope.employeeData = getToken(CONFIG.USER);
            $scope.PersonalEditMode = false;
            $scope.PersonalEditIndex = -1;
            $scope.Personal = null;
            $scope.personalestimateaddedit = false;
            $scope.ErrorText = "";
            $scope.data.HidePersonalExpenseAddButton = false;
            //auto complete
            $scope.simulateQuery = true;
            $scope.isDisabled = false;
            $scope.CreateTempObject = function () {
                var newTraveler = $scope.data.tmpTraveler;
                $scope.Personal = angular.copy(newTraveler);
            }
            $scope.expensetypetravelgroup_use = [];
            $scope.chkfirst = false;
            //Create temp object when add new employee
            $scope.CreateNewPersonalBudget = function () {

                if (angular.isUndefined($scope.Personal.EmployeeID) || $scope.Personal.EmployeeID == null || $scope.Personal.EmployeeID == '') {
                    $mdDialog.show(
                        $mdDialog.alert()
                        .clickOutsideToClose(false)
                        .title($scope.Text['SYSTEM']['CAUTION'])
                        .textContent($scope.Text['SYSTEM']['INVALIDEMPLOYEEID'])
                        .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                    return;
                }

                //default PersonalBudger
                var newPersonalBudget = $scope.data.tmpPersonalBudget;
                newPersonalBudget.IO = $scope.Personal.IO;
                newPersonalBudget.IOName = $scope.Personal.IOName;
                newPersonalBudget.AlternativeIOOrg = $scope.Personal.OrgUnitID;
                newPersonalBudget.IsAlternativeIOOrg = $scope.Personal.IsAlternativeIOOrg;
                newPersonalBudget.IsAlternativeCostCenter = $scope.Personal.IsAlternativeCostCenter;
                newPersonalBudget.CostCenter = $scope.Personal.CostCenter;
                newPersonalBudget.CostCenterName = $scope.Personal.CostCenterName;
                newPersonalBudget.EmployeeID = $scope.Personal.EmployeeID;
                if ($scope.Personal.IsAlternativeIOOrg) {
                    newPersonalBudget.AlternativeIOOrg = $scope.Personal.AlternativeIOOrg;
                }
                if (angular.isDefined($scope.expensetypetravelgroup) && $scope.expensetypetravelgroup.length > 0) {
                    newPersonalBudget.ExpenseTypeGroupID = $scope.expensetypetravelgroup[0].ExpenseTypeGroupID;
                    newPersonalBudget.Remark = $scope.expensetypetravelgroup[0].Remark;
                    newPersonalBudget.Name = $scope.expensetypetravelgroup[0].Name;
                }
                $scope.getAlternative(newPersonalBudget);
                $scope.Personal.PersonalBudgets.push(angular.copy(newPersonalBudget));
            };

            //Create temp object when edit new employee
            $scope.BindTempObject = function (index) {
                $scope.Personal = angular.copy($scope.data.Travelers[index]);
                //if (!$scope.Personal.EmployeeObject) {
                //    $scope.Personal.EmployeeObject = angular.copy($scope.employeeDataList[$scope.employeeDataList.findIndexWithAttr('EmployeeID', $scope.Personal.EmployeeID)]);
                //}
            }

            $scope.$watch('allpositionPersonal ', function () {
                if (angular.isDefined($scope.allpositionPersonal) && $scope.allpositionPersonal.length > 0 && $scope.data.Readonly != true) {
                    if (angular.isDefined($scope.Personal) && $scope.Personal != null && angular.isDefined($scope.Personal.PositionID) && $scope.Personal.PositionID == "") {
                        $scope.Personal.PositionID = $scope.allpositionPersonal[0].ObjectID;
                        $scope.Personal.PositionName = $scope.allpositionPersonal[0].Text;
                    }
                }
            });

            $scope.addpersonalestimate = function () {
                $scope.beginFormWizard();
                $scope.data.TravelPersonalEdit = true;
                $scope.personalestimateaddedit = true;
                $scope.PersonalEditMode = false;
                $scope.CreateTempObject();
                $scope.checkText_Transport($scope.Personal.TransportationTypeCode);
            };

            $scope.isPersonEdit = false;
            $scope.editpersonalestimate = function (row, index) {
                $scope.beginFormWizard();
                $scope.employeeDataList = angular.copy($scope.employeeDataList_Buffer);
                $scope.data.TravelPersonalEdit = true;
                $scope.personalestimateaddedit = true;
                $scope.BindTempObject(index);
                $scope.Personal.searchEmployeeText = $scope.Personal.EmployeeID + ' : ' + $scope.Personal.EmployeeName;
                //$scope.Personal.IO_Buffer = angular.copy($scope.Personal.IO);
                //$scope.Personal.IOName_Buffer = angular.copy($scope.Personal.IOName);
                $scope.EmployeeIDDisable = true;
                $scope.PersonalEditMode = true;
                $scope.PersonalEditIndex = index;
                $scope.getPosition($scope.Personal.EmployeeID);
                $scope.getOrganizationByPositionID($scope.Personal.PositionID);
                $scope.AllCostCenter = row.AllCostCenter;
                //$scope.Personal.IO = angular.copy($scope.Personal.IO_Buffer);
                //$scope.Personal.IOName = angular.copy($scope.Personal.IOName_Buffer);
                $scope.isPersonEdit = true;
                $scope.getTransportationTypeListForCurrentTraveler();
                if (row.CostCenterName == null) {
                    $scope.Personal.CostCenter = $scope.objCostcenter[0].CostCenterCode;
                    $scope.Personal.CostCenterName = $scope.objCostcenter[0].LongDesc;
                }
                if ($scope.chkfirst == false) {
                    $scope.expensetypetravelgroup_use = angular.copy($scope.expensetypetravelgroup);
                    $scope.chkfirst = true;
                }
                else {
                    $scope.expensetypetravelgroup = angular.copy($scope.expensetypetravelgroup_use);
                }
                $scope.CheckExpenseType();
            };

            $scope.getTransportationTypeListForCurrentTraveler = function (resetDefault) {
                var CountryCodes = '';
                for (var i = 0; i < $scope.data.TravelSchedulePlaces.length; i++) {
                    if (i > 0) CountryCodes = CountryCodes + ',';
                    CountryCodes = CountryCodes + $scope.data.TravelSchedulePlaces[i].CountryCode;
                }
                var URL = CONFIG.SERVER + 'HRTR/GetTransportationTypeWithCreatingRule/';
                var oRequestParameter = {
                    InputParameter: { "ReuestType": "TR", "CountryCode": CountryCodes, "TravelTypeID": $scope.TravelGroupRequest.TravelTypeID, "EmployeeID": $scope.Personal.EmployeeID, "PositionID": $scope.Personal.PositionID, "CheckDate": $scope.data.TravelSchedulePlaces[0].BeginDate }
                    , CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.settings.Master.TransportationTypeList = angular.copy(response.data);
                    if (resetDefault) {
                        var temp = $scope.settings.Master.TransportationTypeList[$scope.settings.Master.TransportationTypeList.findIndexWithAttr('IsDefault', true)];
                        $scope.Personal.TransportationTypeCode = temp.TransportationTypeCode;
                        $scope.Personal.TransportationTypeName = temp.TransportationTypeName;
                    }
                }, function errorCallback(response) {
                    console.log('error TravelExpensePersonalEstimateEditor GetTransportationTypeWithCreatingRule.', response);
                });
            };

            $scope.getTransportationTypeRemarkForCurrentTraveler = function (RuleID) {
                $scope.loaderGetPersonalInfo = true;
                var URL = CONFIG.SERVER + 'HRTR/GetTransportationTypeWithCreatingRuleRemark/';
                var oRequestParameter = {
                    InputParameter: { "ReuestType": "TR", "RuleID": RuleID }
                    , CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.settings.Master.PersonalVehicleList = angular.copy(response.data);
                    $scope.loaderGetPersonalInfo = false;
                }, function errorCallback(response) {
                    console.log('error TravelExpensePersonalEstimateEditor GetTransportationTypeWithCreatingRuleRemark.', response);
                });
            };

            $scope.removeRow = function (index) {
                $scope.Personal.PersonalBudgets.splice(index, 1);
            };

            $scope.removeTravelRow = function (index) {
                if ($window.confirm("Please confirm to delete?")) {
                    $scope.data.Travelers.splice(index, 1);
                }
            };



            $scope.ErrorText = "";
            function validateEmptyField() {
                var valid = true;
                if ($scope.Personal.EmployeeID == "" || angular.isUndefined($scope.Personal.EmployeeID)) {
                    valid = false;
                    $scope.ErrorText = $scope.Text['EXPENSE'].VALIDATE_1;
                }
                else if ($scope.Personal.PositionID == "" || angular.isUndefined($scope.Personal.PositionID)) {
                    valid = false;
                    $scope.ErrorText = $scope.Text['EXPENSE'].VALIDATE_2;
                }
                else if ($scope.Personal.OrgUnitID == "" || angular.isUndefined($scope.Personal.OrgUnitID)) {
                    valid = false;
                    $scope.ErrorText = $scope.Text['EXPENSE'].VALIDATE_3;
                }
                else if ($scope.Personal.CostCenter == "" || angular.isUndefined($scope.Personal.CostCenter)) {
                    valid = false;
                    $scope.ErrorText = $scope.Text['EXPENSE'].VALIDATE_4;
                }
                else if ($scope.Personal.TransportationTypeCode == "" || angular.isUndefined($scope.Personal.TransportationTypeCode)) {
                    valid = false;
                    $scope.ErrorText = $scope.Text['EXPENSE'].VALIDATE_5;
                }
                    //else if ($scope.Personal.IO == "" || angular.isUndefined($scope.Personal.IO)) {
                    //    valid = false;
                    //    $scope.ErrorText = $scope.Text['EXPENSE'].VALIDATE_6;
                    //}
                else if ($scope.Personal.PersonalBudgets.length > 0 && ($scope.Personal.PersonalBudgets.isZero('TotalAmount') || $scope.Personal.PersonalBudgets.isZero('ExpenseTypeGroupID'))) {
                    valid = false;
                    $scope.ErrorText = $scope.Text['EXPENSE'].VALIDATE_7;
                }

                    //Modify by Nuntachai 01-06-2017
                else if ($scope.EnableTransportation) {
                    if (angular.isUndefined($scope.Personal.TransportationTypeRemark) || $scope.Personal.TransportationTypeRemark == 0) {
                        valid = false;
                        $scope.ErrorText = $scope.Text['EXPENSE'].VALIDATE_8;
                    }
                }

                return valid;
            }

            function validateDuplicatePersonalBudgets() {
                var valid = true;
                var CodeArr = [];
                for (var i = 0; i < $scope.Personal.PersonalBudgets.length; i++) {
                    var str = $scope.Personal.PersonalBudgets[i].ExpenseTypeGroupID;
                    if (CodeArr.duplicate(str)) {
                        $scope.ErrorText = $scope.Text['EXPENSE'].DUPLICATE_PERSONALBUDGET_EXPENSETYPE;
                        return false;
                    }
                    CodeArr[i] = str;
                }
                return valid;
            }


            var blockSpaming = false;
            $scope.SavePersonalEstimate = function () {
                $scope.isPersonEdit = false;
                $scope.loaderGetPersonalInfo = true; // start at save

                if (blockSpaming) {
                    $scope.loaderGetPersonalInfo = false; // end at save

                    return
                }


                var valid = true;
                valid = validateEmptyField();
                if (!valid) {

                    $scope.HasError = true;
                    //$scope.ErrorText = "Please/ Insert All fields";
                    $scope.loaderGetPersonalInfo = false; // end at save
                    showAlert($scope.ErrorText);

                    return;
                }

                valid = validateDuplicatePersonalBudgets();
                if (!valid) {

                    $scope.HasError = true;
                    $scope.loaderGetPersonalInfo = false; // end at save
                    showAlert($scope.ErrorText);

                    return;
                }

                // if ($scope.document.Additional.TravelGroupRequest[0].TravelSchedulePlaces.length == 0) {
                if ($scope.data.TravelSchedulePlaces.length == 0) {
                    $scope.HasError = true;
                    $scope.ErrorText = "โปรดเลือกสถานที่ที่จะเดินทางในหน้าหลักก่อนทำรายการในหน้านี้";//fixed text
                    $scope.loaderGetPersonalInfo = false; // end at save

                    showAlert($scope.ErrorText);
                    return;
                }

                // valid limit of amount
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                var EXAmountDictionary = [];
                for (var i = 0 ; i < $scope.Personal.PersonalBudgets.length; i++) {
                    EXAmountDictionary.push($scope.Personal.PersonalBudgets[i].ExpenseTypeGroupID + " : " + $scope.Personal.PersonalBudgets[i].TotalAmount);
                }
                console.debug("=============Validation===========");
                console.debug($scope.Personal.PersonalBudgets);
                console.debug("=============End validation===========");

                oRequestParameter.InputParameter = {
                    RequestNo: $scope.document.RequestNo,
                    EXAmountDictionary: EXAmountDictionary,
                    SchedulePlaceList: $scope.data.TravelSchedulePlaces,//$scope.document.Additional.TravelGroupRequest[0].TravelSchedulePlaces,
                    EmployeeID: $scope.Personal.EmployeeID,
                    ReferRequestNo: $scope.document.ReferRequestNo,
                    EmployeePositionID: $scope.Personal.PositionID,
                    OriginalCurrencyCode: 'THB',
                    ExchangeRate: 1,

                }
                blockSpaming = true;
                var URL = CONFIG.SERVER + 'HRTR/ValidateExpenseTypeGroupCreatingRule';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    //console.debug(response);
                    if (response.data == '') {
                        $scope.HasError = false;
                        $scope.ErrorText = "";
                        $scope.summitPersonalEstimate();
                    } else {
                        $scope.HasError = true;
                        $scope.ErrorText = response.data;
                        $scope.loaderGetPersonalInfo = false; // end at save

                        showAlert($scope.ErrorText);
                    }

                    blockSpaming = false;
                    $scope.loaderGetPersonalInfo = false; // end at save

                }, function errorCallback(response) {
                    console.debug(response);
                    console.debug('Fail to call');
                    blockSpaming = false;
                    $mdDialog.show(
                        $mdDialog.alert()
                        .clickOutsideToClose(false)
                        .title($scope.Text['SYSTEM']['WARNING'])
                        .textContent($scope.Text['SYSTEM']['INTERNALSERVER_ERROR'] + '(Function ValidateExpenseTypeGroupCreatingRule)')
                        .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                });
                $scope.loaderGetPersonalInfo = false; // end at save
            }

            $scope.summitPersonalEstimate = function () {
                for (i = 0; i < $scope.settings.Master.TransportationTypeList.length; i++) {
                    if ($scope.settings.Master.TransportationTypeList[i].TransportationTypeCode == $scope.Personal.TransportationTypeCode) {
                        $scope.Personal.RequireRemark = $scope.settings.Master.TransportationTypeList[i].RequireRemark;
                        $scope.TravelGroupRequest.RequireUploadFile = $scope.settings.Master.TransportationTypeList[i].RequireUploadFile;
                        break;
                    }
                }
                if ($scope.PersonalEditMode == true) {
                    $scope.data.Travelers[$scope.PersonalEditIndex] = $scope.Personal;
                }
                else {
                    $scope.data.Travelers.push($scope.Personal);
                }

                $scope.finishFormWizard();
                $scope.data.TravelPersonalEdit = false;
                $scope.personalestimateaddedit = true;
                $scope.PersonalEditMode = false;
                $scope.PersonalEditIndex = -1;
                $scope.CalculateTravelGroupRequest();

            };

            //GetEmployeeByPositionID
            $scope.getOrganizationByPositionID = function (positionID) {
                var URL = CONFIG.SERVER + 'HRTR/GetOrganizationByPositionID';
                var oRequestParameter = { InputParameter: { "PositionID": positionID }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    //$scope.organizationPersonal = response.data; //CommentBy: RAtchatawan W.

                    $scope.Personal.OrgUnitID = response.data.ObjectID;
                    $scope.Personal.OrgUnitName = response.data.Text;
                    if (!$scope.Personal.IsAlternativeIOOrg) {
                        $scope.Personal.AlternativeIOOrg = $scope.Personal.OrgUnitID;
                    }
                    $scope.loaderGetPersonalInfo = false;
                }, function errorCallback(response) {
                    console.log('error Organization', response);

                });
            };
            $scope.CheckIOByOrganization = function () {
                var flgCheck = false;
                for (var i = 0; i < $scope.objIO.length; i++) {
                    if ($scope.Personal.IO == $scope.objIO[i].OrderID) {
                        flgCheck = true;
                        break;
                    }
                }
                return flgCheck;
            }

            $scope.loaderGetPersonalInfo = false; //stop at GetInternalOrderByCostCenter
            $scope.getPosition = function (employeeID) {
                if (employeeID != '') {
                    $scope.loaderGetPersonalInfo = true;
                    var URL = CONFIG.SERVER + 'HRTR/GetAllPosition';
                    var oRequestParameter = { InputParameter: { "EmployeeID": employeeID, "CreateDate": $scope.document.CreatedDate }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.allpositionPersonal = response.data;
                        //GET Default IO / Costcenter for new employee (BEGIN)
                        URL = CONFIG.SERVER + 'Employee/INFOTYPE0001Get';
                        oRequestParameter = { InputParameter: { "EmployeeID": employeeID }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            //Assign to employee
                            $scope.EmployeeOrgAssignment = response.data;
                            if (angular.isUndefined($scope.Personal.PositionID) || $scope.Personal.PositionID == null || $scope.Personal.PositionID == "") {
                                $scope.Personal.PositionID = $scope.EmployeeOrgAssignment.PositionID;
                            }
                            if (!$scope.PersonalEditMode) {
                                $scope.getTransportationTypeListForCurrentTraveler(true);
                                if ($scope.allpositionPersonal.length > 0) {
                                    $scope.Personal.PositionID = $scope.allpositionPersonal[0].ObjectID;
                                    $scope.Personal.PositionName = $scope.allpositionPersonal[0].Text;
                                    $scope.getOrganizationByPositionID($scope.Personal.PositionID);
                                }
                                $scope.Personal.CostCenter = $scope.EmployeeOrgAssignment.CostCenter;
                            }
                            $scope.checkText_Transport($scope.Personal.TransportationTypeCode);

                        }, function errorCallback(response) {
                            console.log('error Employee Data', response);
                        });
                        //GET Default IO / Costcenter for new employee (END)
                    }, function errorCallback(response) {
                        console.log('error Position list', response);
                    });
                }

            };

            //GetInfotype0001List
            var URL = CONFIG.SERVER + 'HRTR/GetAllEmployeeName';
            var oRequestParameter = { InputParameter: { "CreateDate": $scope.document.CreatedDate }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };

            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                $scope.employeeDataList = response.data;
                $scope.employeeDataList_Buffer = angular.copy($scope.employeeDataList);
            }, function errorCallback(response) {
                console.log('error Position list', response);

            });


            var URL = CONFIG.SERVER + 'HRTR/GetTransportationType';
            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                $scope.settings.Master.TransportationTypeList = response.data;
                $scope.data.MasterobjGetTransportationType = response.data;
            }, function errorCallback(response) {
                console.log('error TravelExpenseGroupEstimateEditorController objOrganization.', response);
            });

            $scope.goBackRequest = function () {
                $scope.isPersonEdit = false;
                $scope.finishFormWizard();
                $scope.data.TravelPersonalEdit = false;
                $scope.CreateTempObject();

            };

            $scope.InitialPersonalVehicle = function () {
                //Load Configuratioin for Search Data GetTravelPersonalVehicle
                var URL = CONFIG.SERVER + 'HRTR/GetTravelPersonalVehicle';
                var oRequestParameter = { InputParameter: { 'ReuestType': 'TR' }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.settings.Master.PersonalVehicleList = response.data;

                }, function errorCallback(response) {
                    // Error
                    console.log('error TravelExpenseReasonEditorController InitialConfig.', response);
                });
            }

            function findWithAttr(array, attr, value) {
                for (var i = 0; i < array.length; i += 1) {
                    if (array[i][attr] === value) {
                        return i;
                    }
                }
            }
            $scope.getAllPersonalBudgetAmount = function () {
                var total = 0;
                if ($scope.data.Travelers) {
                    for (var i = 0, _len = $scope.data.Travelers.length; i < _len; i++) {
                        total += $scope.data.Travelers[i].PersonalBudgets.sum("TotalAmount");
                    }
                }
                return total;
            };

            $scope.ChangeExpenseTypeGroup = function (index) {
                var isSameExpenseTypeUse = false;
                for (var loop = 0; loop < $scope.Personal.PersonalBudgets.length; loop++) {
                    if (index != loop) {
                        if ($scope.Personal.PersonalBudgets[index].ExpenseTypeGroupID == $scope.Personal.PersonalBudgets[loop].ExpenseTypeGroupID) {
                            isSameExpenseTypeUse = true;
                        }
                    }
                }
                if (isSameExpenseTypeUse == false) {
                    var oExpenseTypeGroup = $.grep($scope.expensetypetravelgroup, function (e) {
                        return (e.ExpenseTypeGroupID == $scope.Personal.PersonalBudgets[index].ExpenseTypeGroupID);
                    });
                    $scope.Personal.PersonalBudgets[index].Name = oExpenseTypeGroup[0].Name;
                    $scope.Personal.PersonalBudgets[index].Remark = oExpenseTypeGroup[0].Remark;
                } else {
                    $scope.Personal.PersonalBudgets[index].ExpenseTypeGroupID = "";
                    $scope.Personal.PersonalBudgets[index].Name = "";
                    $scope.Personal.PersonalBudgets[index].Remark = "";
                    alert($scope.Text['EXPENSE'].WARNING_CANT_SELECT_DUPPLICATION_GROUPEXPENSE);
                }
            }

            // autocomplete Employee
            $scope.querySearchEmployee = function (query) {
                var results = angular.copy(query ? $scope.employeeDataList.filter(createFilterDuplicateEmployee($scope.data.Travelers)).filter(createFilterForEmployee(query, $scope.data.Travelers)) : $scope.employeeDataList.filter(createFilterDuplicateEmployee($scope.data.Travelers))), deferred;
                if (!query || query.length < 7) { results = results.splice(0, 30); }
                deferred = $q.defer();
                if ($scope.simulateQuery) {
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 300, false);
                    return deferred.promise;
                } else {
                    deferred.resolve(results);
                    return deferred.promise;
                }
            };

            $scope.selectedItemEmployeeChange = function (item, isInit) {
                if (angular.isDefined(item) && item != null) {
                    $scope.Personal.EmployeeID = item.EmployeeID;
                    $scope.Personal.EmployeeName = item.Name;
                    $scope.Personal.PositionID = '';
                    $scope.Personal.searchEmployeeText = item.EmployeeID + ' : ' + item.Name;
                    $scope.getPosition(item.EmployeeID);
                    tempResultEmployee = angular.copy(item);
                    if ($scope.chkfirst == false) {
                        $scope.expensetypetravelgroup_use = angular.copy($scope.expensetypetravelgroup);
                        $scope.chkfirst = true;
                    }
                    else {
                        $scope.expensetypetravelgroup = angular.copy($scope.expensetypetravelgroup_use);
                    }
                    var chkbind = false;
                    if (tempResultEmployee == null) {
                        chkbind = true;
                    }
                    else {
                        if (item.EmployeeID != tempResultEmployee.EmployeeID) {
                            chkbind = true;
                        }
                    }
                    if (chkbind == true) {
                        $scope.chkbind = true;
                        $scope.Personal.PersonalBudgets = [];
                        $scope.CheckExpenseType();
                    }
                }
            };
            $scope.CheckExpenseType = function () {
                var URL = CONFIG.SERVER + 'HRTR/ValidateExpenseTypeGroupCreatingRuleChk';
                var _objexpense = angular.copy($scope.expensetypetravelgroup_use);
                var func = function (objexpense) {
                    var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    var EXAmountDictionary = [];
                    EXAmountDictionary.push(objexpense[0].ExpenseTypeGroupID + " : 1");
                    oRequestParameter.InputParameter = {
                        RequestNo: $scope.document.RequestNo,
                        EXAmountDictionary: EXAmountDictionary,
                        SchedulePlaceList: $scope.data.TravelSchedulePlaces,
                        EmployeeID: $scope.Personal.EmployeeID,
                        ReferRequestNo: $scope.document.ReferRequestNo,
                        EmployeePositionID: $scope.Personal.PositionID,
                        OriginalCurrencyCode: 'THB',
                        ExchangeRate: 1
                    }
                    $http({
                        method: 'POST',
                        async: true,
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        if (response.data != '') {
                            var expeneid = response.data.split("|");
                            for (var j = 0 ; j < $scope.expensetypetravelgroup.length; j++) {
                                if ($scope.expensetypetravelgroup[j].ExpenseTypeGroupID == expeneid[1]) {
                                    $scope.expensetypetravelgroup.splice(j, 1);
                                    break;
                                }
                            }
                            var newobj = [];
                            for (var n = 0; n < $scope.Personal.PersonalBudgets.length; n++) {
                                if ($scope.Personal.PersonalBudgets[n].ExpenseTypeGroupID != expeneid[1]) {
                                    newobj.push($scope.Personal.PersonalBudgets[n]);
                                }
                            }
                            $scope.Personal.PersonalBudgets = newobj;
                        }
                        objexpense.splice(0, 1);
                        if (objexpense.length > 0) {
                            func(objexpense);
                        }
                        else {
                            $scope.loaderGetPersonalInfo = false;  // end
                        }
                    }, function errorCallback(response) {
                        console.debug(response);
                        console.debug('Fail to call');
                        blockSpaming = false;
                        $mdDialog.show(
                            $mdDialog.alert()
                            .clickOutsideToClose(false)
                            .title($scope.Text['SYSTEM']['WARNING'])
                            .textContent($scope.Text['SYSTEM']['INTERNALSERVER_ERROR'] + '(Function ValidateExpenseTypeGroupCreatingRule)')
                            .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                        );
                    });
                }
                $scope.loaderGetPersonalInfo = true;
                func(_objexpense);
            };
            function createFilterForEmployee(query, arr) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    if (!x) return false;
                    var source = angular.lowercase(x.EmployeeID + ' : ' + x.Name);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }

            function createFilterDuplicateEmployee(arr) {
                return function filterFn(x) {
                    if (!x) return false;
                    return !(arr.duplicateProp('EmployeeID', x.EmployeeID));
                };
            }

            var tempResultEmployee;
            $scope.tryToSelectEmployee = function () {
                $scope.Personal.searchEmployeeText = '';
            }
            $scope.checkTextEmployee = function (text) {
                var result = null;
                for (var i = 0; i < $scope.employeeDataList.length; i++) {
                    if (($scope.employeeDataList[i].EmployeeID + " : " + $scope.employeeDataList[i].Name) == text || $scope.employeeDataList[i].EmployeeID == text || $scope.employeeDataList[i].Name == text) {
                        result = $scope.employeeDataList.filter(createFilterDuplicateEmployee($scope.data.Travelers)).filter(createFilterForEmployee(text, $scope.data.Travelers))
                        if (result && result.length > 0) {
                            result = result[0];
                        }
                        else {
                            result = null;
                        }
                        break;
                    }
                }
                if (result) {
                    if ($scope.chkbind == false) {
                        $scope.selectedItemEmployeeChange(result);
                    }
                    $scope.chkbind = false;
                } else if ($scope.tempResultEmployee[$scope.PersonalEditIndex < 0 ? $scope.data.Travelers.length : $scope.PersonalEditIndex]) {
                    //$scope.searchEmployeeText = $scope.tempResultEmployee[$scope.PersonalEditIndex < 0 ? $scope.data.Travelers.length : $scope.PersonalEditIndex].EmployeeID + " : " + $scope.tempResultEmployee[$scope.PersonalEditIndex < 0 ? $scope.data.Travelers.length : $scope.PersonalEditIndex].Name;
                    $scope.selectedItemEmployeeChange($scope.tempResultEmployee[$scope.PersonalEditIndex < 0 ? $scope.data.Travelers.length : $scope.PersonalEditIndex]);
                }
            }
            // end auto employee

            //auto complete Transporter by nipon supap
            var tempResult_Transoprt;
            $scope.tryToSelect_Transport = function (text) {
                for (var i = 0; i < $scope.settings.Master.TransportationTypeList.length; i++) {
                    if ($scope.settings.Master.TransportationTypeList[i].TransportationTypeCode == text || $scope.settings.Master.TransportationTypeList[i].TransportationTypeName == text) {
                        tempResult_Transoprt = $scope.settings.Master.TransportationTypeList[i];
                        break;
                    }
                }
                $scope.searchTransportText = '';
            }
            $scope.checkText_Transport = function (text) {
                var result = null;
                for (var i = 0; i < $scope.settings.Master.TransportationTypeList.length; i++) {
                    if ($scope.settings.Master.TransportationTypeList[i].TransportationTypeName == text || $scope.settings.Master.TransportationTypeList[i].TransportationTypeCode == text) {
                        result = $scope.settings.Master.TransportationTypeList[i];
                        break;
                    }
                }
                if (result) {
                    $scope.selectedItemChangeTransport(result);

                } else if (tempResult_Transoprt) {
                    $scope.selectedItemChangeTransport(tempResult_Transoprt);

                }
            }
            $scope.selectedItemChangeTransport = function (result) {
                if (result != null) {
                    if (result.RequireRemark == true) {
                        $scope.getTransportationTypeRemarkForCurrentTraveler(result.RuleID);
                        $scope.EnableTransportation = true;
                    }
                    else {
                        $scope.EnableTransportation = false;

                        $scope.Personal.TransportationTypeRemark = 0;
                    }
                    $scope.Personal.TransportationTypeCode = result.TransportationTypeCode;
                    $scope.Personal.TransportationTypeName = result.TransportationTypeName;
                    $scope.searchTransportText = result.TransportationTypeName;

                }
            }
            $scope.querySearchItemTransport = function (query) {
                if (!$scope.settings.Master.TransportationTypeList) return;
                var results = query ? $scope.settings.Master.TransportationTypeList.filter(createFilterForTransportatio(query)) : $scope.settings.Master.TransportationTypeList, deferred;
                if (results.length == 0) {
                    $scope.autocomplete.selectedItem = null;
                    $scope.searchText = '';
                }
                if ($scope.simulateQuery) {
                    deferred = $q.defer();
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 50, false);
                    return deferred.promise;
                } else {
                    return results;
                }
            }
            function createFilterForTransportatio(query) {
                if (!angular.isDefined(query)) return false;
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(item) {
                    var textquery = angular.lowercase(item.TransportationTypeName);
                    return (textquery.indexOf(lowercaseQuery) >= 0);
                };
            }

            //End auto

            $scope.getExpensetypetravelgroup = function (item) {
                if (!$scope.expensetypetravelgroup) return '';
                for (var i = 0; i < $scope.expensetypetravelgroup.length; i++) {
                    if (item.ExpenseTypeGroupID == $scope.expensetypetravelgroup[i].ExpenseTypeGroupID) {
                        item.Remark = $scope.expensetypetravelgroup[i].Remark;
                        item.Name = $scope.expensetypetravelgroup[i].Name;
                        item._ExpensetypeDescription = $scope.expensetypetravelgroup[i].Remark;
                        break;
                    }
                }
            }

            $scope.getAlternative = function (item) {
                if (!$scope.objOrganization) return '';
                if ($scope.objOrganization.length <= 0) return '';
                for (var i = 0; i < $scope.objOrganization.length; i++) {
                    if (item.AlternativeIOOrg == $scope.objOrganization[i].ObjectID) {
                        item.AlternativeIOOrgName = $scope.objOrganization[i].ObjectID + " : " + $scope.objOrganization[i].Text;
                        break;
                    }
                }
            }

            $scope.selectedItemPositionChange = function () {
                $scope.getTransportationTypeListForCurrentTraveler(true);
            };

            function showAlert(message) {
                $mdDialog.show(
                  $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('INFORMATION')
                    .textContent(message)
                    .ok('OK')
                );
            };

        }]);

})();