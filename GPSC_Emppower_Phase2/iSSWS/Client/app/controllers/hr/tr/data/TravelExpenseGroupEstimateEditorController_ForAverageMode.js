﻿(function () {
    angular.module('ESSMobile')
        .controller('TravelExpenseGroupEstimateEditorController_ForAverageMode', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$timeout', '$q', '$log', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $timeout, $q, $log) {

            var RequestNo = $scope.document.RequestNo;
            //GroupBudgets
            $scope.Textcategory = 'EXPENSE';
            $scope.employeeData = getToken(CONFIG.USER);
            $scope.TravelerIndex = 0;
            $scope.data.HideGroupExpenseAddButton = false;
            $scope.data.IsNewGroupEstimate = {};
            if ($scope.data.GroupBudgets)
                $scope.oldGropBudgetLength = angular.copy($scope.data.GroupBudgets.length);
            $scope.addRow = function () {
                var oTravelExpenseGroup = angular.copy($scope.data.tmpGroupBudget);

                if (angular.isDefined($scope.ExpenseTypeGroup) && $scope.ExpenseTypeGroup.length > 0) {
                    var oExpenseTypeGroupTemp = angular.copy($scope.ExpenseTypeGroup.filter(createFilterDuplicateForExpenseTypeGroup($scope.data.GroupBudgets)));
                    if (oExpenseTypeGroupTemp.length > 0) {
                        oTravelExpenseGroup.GroupBudgetID = $scope.guid();
                        oTravelExpenseGroup.ExpenseTypeGroupID = oExpenseTypeGroupTemp[0].ExpenseTypeGroupID;
                        oTravelExpenseGroup.Name = oExpenseTypeGroupTemp[0].Name;
                        oTravelExpenseGroup.Remark = oExpenseTypeGroupTemp[0].Remark;
                        oTravelExpenseGroup.searchExpenseTypeGroupText = oExpenseTypeGroupTemp[0].Remark;
                        tempResultExpenseTypeGroup = oExpenseTypeGroupTemp[0];
                    }
                    else {
                        $scope.hideAddButton = true;
                    }
                }
                else {
                    $scope.hideAddButton = true;
                }

                $scope.data.GroupBudgets.push(oTravelExpenseGroup);
                var temp = angular.copy($scope.data.GroupBudgets[$scope.data.GroupBudgets.length - 1]);
                $scope.hideAddButton = ($scope.data.GroupBudgets.length == $scope.ExpenseTypeGroup.length);
                $scope.OnAddGroupCashAdvance(temp);
            };

            $scope.InitialConfig = function () {
                $scope.TravelerIndex = $scope.GetEditTraveler($scope.employeeData.RequesterEmployeeID);
            }



            $scope.removeRow = function (index) {
                $scope.data.GroupBudgets.splice(index, 1);
                $scope.data.Travelers[$scope.TravelerIndex].oCashAdvance.GroupCashAdvances.splice(index, 1);
                $scope.hideAddButton = ($scope.data.GroupBudgets.length == $scope.ExpenseTypeGroup.length);
            };

            $scope.ChangeExpenseTypeGroup = function (i) {
                if ($scope.data.GroupBudgets[i].ExpenseTypeGroupID != $scope.data.Travelers[$scope.TravelerIndex].oCashAdvance.GroupCashAdvances[i].ExpenseTypeGroupID) {
                    $scope.data.Travelers[$scope.TravelerIndex].oCashAdvance.GroupCashAdvances[i].ExpenseTypeGroupID = $scope.data.GroupBudgets[i].ExpenseTypeGroupID;
                    $scope.data.Travelers[$scope.TravelerIndex].oCashAdvance.GroupCashAdvances[i].Remark = $scope.data.GroupBudgets[i].Remark;
                }
                if ($scope.data.Travelers[$scope.TravelerIndex].oCashAdvance.GroupCashAdvances[i].Amount != $scope.data.GroupBudgets[i].TotalAmount) {
                    $scope.data.Travelers[$scope.TravelerIndex].oCashAdvance.GroupCashAdvances[i].Amount = $scope.data.GroupBudgets[i].TotalAmount;
                    $scope.data.Travelers[$scope.TravelerIndex].oCashAdvance.GroupCashAdvances[i].TotalAmount = $scope.data.GroupBudgets[i].TotalAmount;
                }
                $scope.data.IsNewGroupEstimate[i] = false;
            };


            $scope.getTotal = function () {
                var total = 0;
                for (var i = 0; i < $scope.data.GroupBudgets.length; i++) {
                    if (angular.isString($scope.data.GroupBudgets[i].TotalAmount)) {
                        total += parseFloat($scope.data.GroupBudgets[i].TotalAmount.replace(',', ''));
                    }
                    else {
                        total += parseFloat($scope.data.GroupBudgets[i].TotalAmount);
                    }
                }
                if (angular.isDefined($scope.data.Travelers) && $scope.data.Travelers != null && $scope.data.Travelers.length > 0) {
                    $scope.data.Estimate = $scope.data.Travelers.sum('TotalAmount') + total;
                }
                return total;
            }

            $scope.querySearchExpenseTypeGroup = function (query, index) {
                if (!$scope.ExpenseTypeGroup) return;
                var results = query ? $scope.ExpenseTypeGroup.filter(createFilterDuplicateForExpenseTypeGroup($scope.data.GroupBudgets)).filter(createFilterForExpenseTypeGroup(query, $scope.data.GroupBudgets)) : $scope.ExpenseTypeGroup.filter(createFilterDuplicateForExpenseTypeGroup($scope.data.GroupBudgets)), deferred;
                if ($scope.simulateQuery) {
                    deferred = $q.defer();
                    $timeout(function () { deferred.resolve(results); }, Math.random() * 300, false);
                    return deferred.promise;
                } else {
                    return results;
                }
            };

            $scope.selectedItemExpenseTypeGroupChange = function (item, index) {
                if (angular.isDefined(item) && item != null) {
                    $scope.data.GroupBudgets[index].ExpenseTypeGroupID = item.ExpenseTypeGroupID;
                    $scope.data.GroupBudgets[index].Name = item.Name;
                    $scope.data.GroupBudgets[index].Remark = item.Remark;
                    $scope.data.GroupBudgets[index].searchExpenseTypeGroupText = item.Remark;
                    tempResultExpenseTypeGroup = item;
                    $scope.ChangeExpenseTypeGroup(index);
                }
                else {
                    $scope.data.GroupBudgets[index].ExpenseTypeGroupID = '';
                    $scope.data.GroupBudgets[index].Name = '';
                    $scope.data.GroupBudgets[index].Remark = '';
                }
            };

            function createFilterForExpenseTypeGroup(query, arr) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    if (!x) return false;
                    var source = angular.lowercase(x.Remark);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }

            function createFilterDuplicateForExpenseTypeGroup(arr) {
                return function filterFn(x) {
                    if (!x) return false;
                    return !(arr.duplicateProp('ExpenseTypeGroupID', x.ExpenseTypeGroupID));
                };
            }

            var tempResultExpenseTypeGroup;
            $scope.tryToSelectExpenseTypeGroup = function (item) {
                item.searchExpenseTypeGroupText = '';
                item.ExpenseTypeGroupID = '';
            }
            $scope.checkTextExpenseTypeGroup = function (text, $index) {
                var result = null;
                for (var i = 0; i < $scope.ExpenseTypeGroup.length; i++) {
                    if (($scope.ExpenseTypeGroup[i].Name + " : " + $scope.ExpenseTypeGroup[i].Remark) == text || $scope.ExpenseTypeGroup[i].Name == text || $scope.ExpenseTypeGroup[i].Remark == text) {
                        result = $scope.ExpenseTypeGroup[i];
                        break;
                    }
                }
                if (result) {
                    $scope.selectedItemExpenseTypeGroupChange(result, $index);
                } else if (tempResultExpenseTypeGroup) {
                    $scope.selectedItemExpenseTypeGroupChange(tempResultExpenseTypeGroup, $index);
                }
            }

            $scope.InitialGroupBudget = function () {
                if (!$scope.data.Readonly) {
                    if ($scope.data.GroupBudgets && $scope.ExpenseTypeGroup) {
                        if (!$scope.data.Travelers[0].oCashAdvance.GroupCashAdvances) $scope.data.Travelers[$scope.TravelerIndex].oCashAdvance.GroupCashAdvances = [];
                        for (var i = 0; i < $scope.data.GroupBudgets.length; i++) {
                            if (!$scope.data.Travelers[0].oCashAdvance.GroupCashAdvances[i]) $scope.OnAddGroupCashAdvance(angular.copy($scope.data.GroupBudgets[i]));
                            $scope.checkTextExpenseTypeGroup($scope.data.GroupBudgets[i].Remark, i);
                        }
                    }
                }
            }

            $scope.isUserRoleAccounting = false;
            $scope.checkAccountingRole = function () {
                var URL = CONFIG.SERVER + 'HRTR/IsUserRoleAccountingForEditRequest';
                var oRequestParameter = { InputParameter: { "REQUESTNO": $scope.document.RequestNo, 'CompanyCode': $scope.document.Requestor.CompanyCode }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.CurrentEmployee = getToken(CONFIG.USER);
                    $scope.isUserRoleAccounting = response.data;
                    console.log('IsAccountingRole.', response.data);
                }, function errorCallback(response) {
                    console.log('error IsAccountingRole.', response);
                });
            };
            $scope.checkAccountingRole();
        }]);

})();



