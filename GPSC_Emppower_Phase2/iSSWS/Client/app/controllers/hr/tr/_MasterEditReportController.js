﻿/*==========================================================================================
Created By: Ratchatawan W.
Created Date: 31 Mar 2017
Remark: This function use initially all master data and configuration for all travel request
===========================================================================================*/
(function () {
    angular.module('ESSMobile')
        .controller('_MasterEditReportController', ['$scope', '$http', 'CONFIG', function ($scope, $http, CONFIG) {

            $scope.employeeData = getToken(CONFIG.USER);

            $scope.settings = {
                ProjectCodeMode: '',
                EnableOverideCostCenter: false,
                EnableOverideIO: false,
                ExpenseTypeGroupInfo: {
                    prefix: 'TR',
                    travelTypeID: 1,
                    isDomestic: 'D',
                    groupTagName: "",
                    tagNameReceipt: "",
                    tagNameNoReceipt: ""
                },
                MaximumEmpSubGroup: 99,
                ReceiptToleranceAllow: 0,
                EnableProjectCost: false,
                Master: []
            };
            $scope.masterTR = {
                CostCenterList: [],
                OrgUnitList: [],
                IOList: [],
                ProjectList: [],

            }; 
          

            /*==============================
                 Initial Scope and variable
             ===============================*/

            /*========================
                Initial Function
            ==========================*/
            //Get all master data
            $scope.GetMasterdata = function() {
                var URL = CONFIG.SERVER + 'HRTR/GetMasterData';
                var oRequestParameter = { InputParameter: { "CreateDate": $scope.document.CreatedDate }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                return $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.settings.Master.CostCenterList = response.data.CostCenterList;
                    $scope.settings.Master.ProjectList = response.data.ProjectList;
                    $scope.settings.Master.OrgUnitList = response.data.OrganizationList;
                    $scope.settings.Master.IOList = response.data.InternalOrderList;
                    $scope.settings.Master.CostcenterDistributionList = response.data.CostCenterDistributionList;
                    $scope.settings.Master.TransportationTypeList = response.data.TransportationTypeList;
                    $scope.settings.Master.PersonalVehicleList = response.data.PersonalVehicleList;

                    // enco much use dont remove
                    $scope.masterTR.CostCenterList = angular.copy(response.data.CostCenterList);
                    $scope.masterTR.OrgUnitList = angular.copy(response.data.OrganizationList);
                    $scope.masterTR.IOList = angular.copy(response.data.InternalOrderList);
                    $scope.masterTR.ProjectList = angular.copy(response.data.ProjectList);
                    return response;
                },
              function errorCallback(response) {
                  console.error(response);
                  return response;
              });
            };

          
        }]);
})();