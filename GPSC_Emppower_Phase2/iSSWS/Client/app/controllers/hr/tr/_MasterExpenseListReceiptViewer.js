﻿//(function () {
//    angular.module('ESSMobile')
//        .controller('_MasterEditGeneralExpenseController', ['$scope', '$http', 'CONFIG', function ($scope, $http, CONFIG) {
//            $scope.employeeData = getToken(CONFIG.USER);

//            $scope.settings = {
//                ProjectCodeMode: '',
//                EnableOverideCostCenter: false,
//                EnableOverideIO: false,
//                ExpenseTypeGroupInfo: {
//                    prefix: 'GE',
//                    travelTypeID: 0,
//                    isDomestic: 'D'
//                },
//                ReceiptToleranceAllow: 0,
//                ENABLE_PROJECTCOST: false,
//                Master: []
//            };



//            /*==============================
//                 Initial Scope and variable
//             ===============================*/

//            /*========================
//                Initial Function
//            ==========================*/
//            //Get all master data
//            $scope.GetMasterdata = function () {
//                var URL = CONFIG.SERVER + 'HRTR/GetMasterData';
//                var oRequestParameter = { InputParameter: { "CreateDate": $scope.document.CreatedDate }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
//                return $http({
//                    method: 'POST',
//                    url: URL,
//                    data: oRequestParameter
//                }).then(function successCallback(response) {
//                    $scope.settings.Master.CostCenterList = response.data.CostCenterList;
//                    $scope.settings.Master.ProjectList = response.data.ProjectList;
//                    $scope.settings.Master.OrgUnitList = response.data.OrgqanizationList;
//                    $scope.settings.Master.IOList = response.data.InternalOrderList;
//                    $scope.settings.Master.CostcenterDistributionList = response.data.CostCenterDistributionList;
//                    console.debug(response);
//                    return response;
//                },
//              function errorCallback(response) {
//                  console.error(response);
//                  return response;
//              });
//            };

//            var initialExpenseReceipt = function () {
//                var URL = CONFIG.SERVER + 'HRTR/InitialExpenseReceipt';
//                var oInputParameter = {
//                    "CreateDate": $scope.document.CreatedDate
//                };
//                var oRequestParameter = { InputParameter: oInputParameter, CurrentEmployee: $scope.employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
//                return $http({
//                    method: 'POST',
//                    url: URL,
//                    data: oRequestParameter
//                }).then(function successCallback(response) {

//                    //var projectcodeMode = response.data.ProjectCodeMode.toString().toUpperCase();
//                    //$scope.settings.ProjectCodeMode = projectcodeMode;
//                    //var isOverrideCostCenter = response.data.EnableOverideCostCenter.toString().toLowerCase();
//                    //$scope.settings.EnableOverideCostCenter = (isOverrideCostCenter == 'true') ? true : false;

//                    //var isOverrideIO = response.data.EnableOverideIO.toString().toLowerCase();
//                    //$scope.settings.EnableOverideIO = (isOverrideIO == 'true') ? true : false;
//                    //$scope.data.EnableOverideCostcenter = (isOverrideCostCenter == 'true') ? true : false;
//                    //$scope.data.EnableOverideIO = (isOverrideIO == 'true') ? true : false;

//                    var isEnableProjectCost = response.data.EnableProjectCost.toString().toLowerCase();
//                    $scope.settings.EnableProjectCost = (isEnableProjectCost == 'true') ? true : false;

//                    //$scope.settings.ReceiptToleranceAllow = response.data.receiptToleranceAllow;
//                    return response;
//                }, function errorCallback(response) {

//                    return response;
//                });
//            };


//            $scope.init_MasterEditGeneralExpenseController = function () {
//                initialExpenseReceipt();
//                $scope.GetMasterdata(); 
//            }
//            $scope.init_MasterEditGeneralExpenseController();

//        }]);
//})();