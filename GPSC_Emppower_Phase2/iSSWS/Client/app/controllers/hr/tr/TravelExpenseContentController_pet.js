﻿(function () {
    angular.module('ESSMobile')
        .controller('TravelExpenseContentController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', 'DTOptionsBuilder', 'DTColumnDefBuilder', function ($scope, $http, $routeParams, $location, $filter, CONFIG, DTOptionsBuilder, DTColumnDefBuilder) {
            $scope.DateNow = new Date();
           
            $scope.content.isShowHeader = false;
            $scope.formData = { IsAdvancedSearch: '', SelYear: '', SelMonth: '', SelRequestType: '', SelDocumentNo: '', SelBeginDate: $filter('date')($scope.DateNow, 'yyyy-MM-dd'), SelEndDate: $filter('date')($scope.DateNow, 'yyyy-MM-dd'), SelTravelType: '', SelCountry: '' };
            $scope.Textcategory = 'TRAVELEXPENSE';
            $scope.getTextDescriptionByCategory($scope.Textcategory, function () {
                 $scope.HeaderText = $scope.Text[$scope.Textcategory].TITLE;
                //$scope.content.Header = $scope.Text[$scope.Textcategory].TITLE;
            });
            $scope.InitialConfig = function () {
                //Load Configuratioin for Search Data
                var URL = CONFIG.SERVER + 'HRTR/GetTravelConfigurationForSearch';
                var oRequestParameter = { InputParameter: { "YEARKEY": "TRAVELEXPENSE" }, CurrentEmployee: getToken(CONFIG.USER) }
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    //$scope.expenseStatus = response.data;
                    $scope.SearchYear = response.data['YEAR'];
                    $scope.SearchMonth = response.data['MONTH'];
                    $scope.SearchRequestType = response.data['REQUESTTYPE'];
                    $scope.SearchTravelType = response.data['TRAVELTYPE'];
                    $scope.SearchCountry = response.data['COUNTRY'];
                    // bind default value 
                    if ($scope.SearchYear.length > 0) {
                        $scope.formData.SelYear = $scope.SearchYear[0];
                    }
                    $scope.formData.SelMonth = ((new Date()).getMonth() + 1).toString();

                    if ($scope.SearchRequestType.length > 0) {
                        $scope.formData.SelRequestType = $scope.SearchRequestType[0];
                    }
                    if ($scope.SearchTravelType.length > 0) {
                        $scope.formData.SelTravelType = $scope.SearchTravelType[0];
                    }
                    if ($scope.SearchCountry.length > 0) {
                        $scope.formData.SelCountry = $scope.SearchCountry[0];
                    }
                }, function errorCallback(response) {
                    // Error
                    console.log('error TravelExpenseContentController InitialConfig.', response);
                });
            }
            //Nun Add CoverSheet
            $scope.ViewCoverSheet = function (requestNo, compCode) {
               
                URL = CONFIG.SERVER + 'HRTR/GetTravelReportCoverSheet/';
                var oRequestParameter = { InputParameter: { "RequestNo": requestNo }, CurrentEmployee: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER) };


                if (typeof cordova != 'undefined') {
                    cordova.InAppBrowser.open("data:application/pdf;base64, " + escape(response.data), '_system', 'location=no');
                } else {
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        console.log(response);
                        window.open("data:application/pdf;base64, " + escape(response.data));
                    }, function errorCallback(response) {
                    });
                }
            }
            $scope.SearchTravelExpense = function () {

                var URL = CONFIG.SERVER + 'HRTR/SearchTravelExpense';
                var oRequestParameter = {
                    InputParameter: {
                        "YEAR": $scope.formData.SelYear
                        , "MONTH": $scope.formData.SelMonth
                        , "REQUESTTYPE": $scope.formData.SelRequestType.Key
                        , "REQUESTNO": $scope.formData.SelDocumentNo
                        , "BEGINDATE": $scope.formData.SelBeginDate
                        , "ENDDATE": $scope.formData.SelEndDate
                        , "TRAVELTYPE": $scope.formData.SelTravelType.TravelTypeID
                        , "COUNTRY": $scope.formData.SelCountry.CountryCode
                        , "ISADVANCEDSEARCH": $scope.formData.IsAdvancedSearch
                    }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: getToken(CONFIG.USER)
                    , Creator: getToken(CONFIG.USER)
                };
                console.log('oRequestParameter', oRequestParameter);
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.TravelExpense = response.data;

                }, function errorCallback(response) {
                    // Error
                    console.log('error TravelExpense SearchTravelExpense', response);

                });
            };

            
                //Dict GetTransactionStatusGroup
                var oRequestParameter = { InputParameter: { "Mode": "TravelExpense" }, CurrentEmployee: getToken(CONFIG.USER) }

                //get Expense Type Tag
                var URL = CONFIG.SERVER + 'HRTR/GetTransactionStatusGroup/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.GetTransactionStatus = response.data;
                    //console.log('exptag.', $scope.exptag);

                }, function errorCallback(response) {
                    // Error
                    console.log('error ExpenseTypeSettingEditorController.', response);
                });
            

            $scope.SearchTravelExpenseDetail = function (data) {

                var URL = CONFIG.SERVER + 'HRTR/SearchTravelExpenseDetail';
                var oRequestParameter = {
                    InputParameter: {
                        "REQUESTNO": data
                    }
                    , CurrentEmployee: getToken(CONFIG.USER)
                    , Requestor: getToken(CONFIG.USER)
                    , Creator: getToken(CONFIG.USER)
                };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    console.log(response.data);
                    $scope.TravelExpenseDetail = response.data;

                }, function errorCallback(response) {
                    // Error
                    console.log('error TravelExpense SearchTravelExpenseDetail', response);

                });
            };
            $scope.dtOptions = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(15).withOption('lengthChange', false).withOption('ordering', false);
            $scope.dtOptionsSub = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(15).withOption('lengthChange', false).withOption('ordering', false);
            $scope.dtOptionsSubDetail = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(15).withOption('lengthChange', false).withOption('ordering', false);
            $scope.setSelectedBeginDate = function (selectedDate) {
                $scope.formData.SelBeginDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };
            $scope.setSelectedEndDate = function (selectedDate) {
                $scope.formData.SelBeginDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };
            $scope.openRequest = function (id, keyMaster, isOwner, isDataOwner, requestType, BoxDescription) {
                $location.path('/frmViewRequest/' + id + '/' + keyMaster + '/' + isOwner + '/' + isDataOwner + '/' + encodeURIComponent(BoxDescription));
            };
        }])
        .directive('expand', function () {
            return {
                restrict: 'A',
                controller: ['$scope', function ($scope) {
                    $scope.$on('onExpandAll', function (event, args) {
                        $scope.expanded = args.expanded;
                    });
                }]
            };
        });
})();