﻿(function () {
    angular.module('ESSMobile')
        .controller('PettyCashStatusReportController', ['$scope', '$http', '$routeParams', '$location', 'CONFIG', function ($scope, $http, $routeParams, $location, CONFIG) {

            $scope.Textcategory = 'PettyCashStatusReport';

            $scope.ViewReport = function () {
                var MoDule = 'HRTR/';
                var Functional = 'GetPettyCashStatusReportFromBase64String';
                var URL = CONFIG.SERVER + MoDule + Functional;
                // Success
                if (typeof cordova != 'undefined') {
                    cordova.InAppBrowser.open("data:application/pdf;base64, " + escape(response.data), '_system', 'location=no');
                } else {
                    var oTravelReportTransfer = { OrgUnit_ID: '123', IO_ID: '456' };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oTravelReportTransfer
                    }).then(function successCallback(response) {
                        console.log(response);
                        window.open("data:application/pdf;base64, " + escape(response.data));

                    }, function errorCallback(response) {
                        // Error
                        console.log('error PettyCashStatusReportController.', response);
                    });
                }
            }

        }]);
})();