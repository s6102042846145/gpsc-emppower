﻿(function () {
    angular.module('ESSMobile')
        .controller('CreditCardReportController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', 'DTOptionsBuilder', 'DTColumnDefBuilder', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, DTOptionsBuilder, DTColumnDefBuilder, $mdDialog) {
            $scope.Textcategory = 'REPORT_FILTER';
            $scope.TravelReportTransfer = {};
            //*$scope.TravelReportTransfer.ApproveBeginDate = $filter('date')(new Date(), 'yyyy-MM-dd');
            //$scope.TravelReportTransfer.ApproveEndDate = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.TravelReportTransfer.RequestorCompanyCode = ''
            $scope.disableEmployee = true;

            //#Member
            var URL;
            var oRequestParameter;

            //#Events
            $scope.setSelectedBeginDate = function (selectedDate) {
                $scope.TravelReportTransfer.ApproveBeginDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
                if (selectedDate > $scope.TravelReportTransfer.ApproveEndDate)
                    $scope.TravelReportTransfer.ApproveEndDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };
            $scope.setSelectedEndDate = function (selectedDate) {
                $scope.TravelReportTransfer.ApproveEndDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
                if (selectedDate < $scope.TravelReportTransfer.ApproveBeginDate)
                    $scope.TravelReportTransfer.ApproveBeginDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };
            //#Functional
            $scope.GetCompanyList = function () {
                $scope.loader.enable = true;
                var MoDule = 'Share/';
                var Functional = 'GetCompanyList';
                URL = CONFIG.SERVER + MoDule + Functional;
                console.log(URL);
                $http({
                    method: 'POST',
                    url: URL
                }).then(function successCallback(response) {
                    console.log('Finish GetCompanyCode', response.data);
                    $scope.lstCompany = response.data;
                    var c_emp = getToken(CONFIG.USER);
                    $scope.language = c_emp.Language;
                    if ($scope.TravelReportTransfer.RequestorCompanyCode) {
                        $scope.disableEmployee = false;
                    }
                    // filter only response company 
                    URL = CONFIG.SERVER + 'HRTR/GetResponseCompany';
                    oRequestParameter = { InputParameter: '', CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.lstCompany = $scope.lstCompany.filter(createFilterCompany(response.data));
                        
                        if ($scope.lstCompany.length > 1) {
                            for (var i = 0; i < $scope.lstCompany.length; i++) {
                                $scope.lstCompany[i].companyfullname = $scope.lstCompany[i].CompanyCode + " : " + $scope.lstCompany[i].Name;
                                if (c_emp.CompanyCode == $scope.lstCompany[i].CompanyCode) {
                                    $scope.TravelReportTransfer.RequestorCompanyCode = $scope.lstCompany[i].CompanyCode;
                                    $scope.lstCompany[i].companyfullname = $scope.language == 'TH' ? $scope.lstCompany[i].Name + ' : ' + $scope.lstCompany[i].FullNameTH : $scope.lstCompany[i].Name + ':' + $scope.lstCompany[i].FullNameEN;
                                    $scope.ap_pdr_text_company = $scope.lstCompany[i].companyfullname;
                                }
                                else {
                                    $scope.TravelReportTransfer.RequestorCompanyCode = $scope.lstCompany[0].CompanyCode;
                                    $scope.lstCompany[0].companyfullname = $scope.language == 'TH' ? $scope.lstCompany[0].Name + ' : ' + $scope.lstCompany[0].FullNameTH : $scope.lstCompany[0].Name + ':' + $scope.lstCompany[0].FullNameEN;
                                    $scope.ap_pdr_text_company = $scope.lstCompany[0].companyfullname;
                                }
                            }
                        }
                        else if ($scope.lstCompany[0] != null) {
                            $scope.TravelReportTransfer.RequestorCompanyCode = $scope.lstCompany[0].CompanyCode;
                            $scope.lstCompany[0].companyfullname = $scope.language == 'TH' ? $scope.lstCompany[0].Name + ' : ' + $scope.lstCompany[0].FullNameTH : $scope.lstCompany[0].Name + ':' + $scope.lstCompany[0].FullNameEN;
                            $scope.ap_pdr_text_company = $scope.lstCompany[0].companyfullname;

                        }
                        $scope.loader.enable = false;
                        if ($scope.lstCompany[0] != null) {
                            $scope.GetEmployees();
                            $scope.EnableProjectCode();
                            $scope.EnableHaveContract();
                        }

                    }, function errorCallback(response) {
                        console.log('error GetEmployees list', response);
                        $scope.loader.enable = false;
                    });

                }, function errorCallback(response) {
                    // Error
                    console.log('error GetCompanyCode.', response);
                    $scope.loader.enable = false;
                });
            };

            function createFilterCompany(arr) {
                return function filterFn(x) {
                    for (var i = 0; i < arr.length; i++) {
                        if (arr[i] == x.CompanyCode) {
                            return true;
                        }
                    }
                    return false;
                };
            }

            $scope.GetCompanyList();
            $scope.ClearData = function (isAll) {
                $scope.tryToSelect_Emp();
                $scope.TravelReportTransfer.RequestNo = '';
                $scope.TravelReportTransfer.ApproveBeginDate = '';
                $scope.TravelReportTransfer.ApproveEndDate = '';
                $scope.ApproveBeginDate = '';
                $scope.ApproveEndDate = '';
                $scope.TravelReportTransfer.CreditCardNo = '';
                $scope.TravelReportTransfer.ReceiptNoBegin = '';
                $scope.TravelReportTransfer.ReceiptNoEnd = '';
                $scope.TravelReportTransfer.RequestNoBegin = '';
                $scope.TravelReportTransfer.RequestNoEnd = '';
                $scope.TravelReportTransfer.Status = '';
                $scope.ap_pdr_text.StateName = '';
                $scope.ap_pdr_text.creditcard = '';

                $scope.selected = [];
                $scope.DataReport = null;
            }

            var calling_GetEmployees = false;
            $scope.GetEmployees = function (oSearch) {
                if (calling_GetEmployees) return;
                if (oSearch != '') {
                    $scope.loader.enable = true;
                    calling_GetEmployees = true;
                    URL = CONFIG.SERVER + 'HRTR/GetAllEmployeeNameForReport';
                    oRequestParameter = { InputParameter: { "EmployeeSearch": oSearch, "CompanyCode": $scope.TravelReportTransfer.RequestorCompanyCode }, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        console.log('Finish GetEmployees', response.data);
                        $scope.lstEmployeeData = response.data;
                            $scope.TravelReportTransfer.RequestorID = '';
                            $scope.TravelReportTransfer.RequestorName = '';
                            $scope.GetCreditCard();
                        if (response.data.length == 0) {
                            $mdDialog.show(
                              $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title('Warning')
                                .textContent('Employee not found.')
                                .ok('OK')
                            );
                        }
                        $scope.loader.enable = false;
                        calling_GetEmployees = false;

                    }, function errorCallback(response) {
                        console.log('error GetEmployees list', response);
                        $scope.loader.enable = false;
                        calling_GetEmployees = false;
                    });
                }
            };
            $scope.GetPositionByEmployee = function (oEmployeeID) {
                $scope.loader.enable = true;
                $scope.TravelReportTransfer.PositionID = '';
                $scope.TravelReportTransfer.OrgUnitID = '';
                $scope.lstPosition = null;
                $scope.lstOrganization = null;
                URL = CONFIG.SERVER + 'HRTR/GetAllPosition';
                oRequestParameter = { InputParameter: { "EmployeeID": oEmployeeID, "CompanyCode": $scope.TravelReportTransfer.RequestorCompanyCode }, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.lstPosition = response.data;
                    if ($scope.ap_pdr_text.emp) {
                        $scope.TravelReportTransfer.PositionID = $scope.lstPosition[0].ObjectID;
                        $scope.ap_pdr_text.position = $scope.lstPosition[0].PositionName;
                        $scope.GetOrganizationByPositionID($scope.TravelReportTransfer.PositionID);
                    }
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    console.log('error GetPositionByEmployee', response);
                    $scope.loader.enable = false;
                });
            };
            $scope.GetOrganizationByPositionID = function (oPositionID) {
                $scope.loader.enable = true;
                $scope.TravelReportTransfer.OrgUnitID = '';
                $scope.lstOrganization = null;
                URL = CONFIG.SERVER + 'HRTR/GetOrganizationByPositionID';
                oRequestParameter = { InputParameter: { "PositionID": oPositionID, "CompanyCode": $scope.TravelReportTransfer.RequestorCompanyCode }, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    console.log('Finish GetOrganizationByPositionID', response.data);
                    $scope.lstOrganization = response.data;
                    if ($scope.ap_pdr_text.position && $scope.lstOrganization) {
                        $scope.TravelReportTransfer.OrgUnitID = $scope.lstOrganization.ObjectID;
                    }
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    console.log('error GetOrganizationByPositionID', response);
                    $scope.loader.enable = false;
                });
            };
            $scope.GetCreditCard = function (oEmployeeID) {
                $scope.loader.enable = true;

                oRequestParameter = { InputParameter: { "EmployeeID": oEmployeeID, "CompanyCode": $scope.TravelReportTransfer.RequestorCompanyCode }, CurrentEmployee: getToken(CONFIG.USER) }
                URL = CONFIG.SERVER + 'HRTR/GetAllCreditCard/';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.lstCreditCard = response.data;
                    console.log('lstCreditCard.', $scope.lstCreditCard);
                    $scope.loader.enable = false;

                }, function errorCallback(response) {
                    // Error
                    console.log('error lstCreditCard', response);
                    $scope.loader.enable = false;
                });

            };

            $scope.ShowReport = function () {
                $scope.ViewData();
            };
            $scope.ViewData = function () {
                $scope.selected_status = "";
                $scope.selected_statusName = "";
                for (i = 0; i <= $scope.selected.length - 1;) {
                    $scope.selected_status = $scope.selected_status + $scope.selected[i].StateID + ",";
                    $scope.selected_statusName = $scope.selected_statusName + $scope.selected[i].StateText + " - ";
                    i = i + 1
                }
                var oTravelReportTransfer = {
                    CompanyCode: $scope.TravelReportTransfer.RequestorCompanyCode,
                    ClientCompanyID: $scope.TravelReportTransfer.ClientCompanyID,
                    RequestorCompanyCode: $scope.TravelReportTransfer.RequestorCompanyCode,
                    RequestorName: $scope.employeeData.Name,
                    RequestorID: $scope.TravelReportTransfer.RequestorID,
                    OrgUnitID: $scope.TravelReportTransfer.OrgUnitID,
                    PositionID: $scope.TravelReportTransfer.PositionID,
                    ApproveBeginDate: $scope.TravelReportTransfer.ApproveBeginDate,
                    ApproveEndDate: $scope.TravelReportTransfer.ApproveEndDate,
                    CreditCardNo: $scope.TravelReportTransfer.CreditCardNo,
                    ReceiptNoBegin: $scope.TravelReportTransfer.ReceiptNoBegin,
                    ReceiptNoEnd: $scope.TravelReportTransfer.ReceiptNoEnd,
                    RequestNoBegin: $scope.TravelReportTransfer.RequestNoBegin,
                    RequestNoEnd: $scope.TravelReportTransfer.RequestNoEnd,
                    Status: $scope.selected_status,
                    StatusName: $scope.selected_statusName
                };
                URL = CONFIG.SERVER + 'HRTR/GetCreditCardReportNew';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oTravelReportTransfer
                }).then(function successCallback(response) {
                    $scope.DataReport = response.data;
                }, function errorCallback(response) {
                    console.log('error GetCostcenter.', response);
                });
            }
            $scope.ExportToExcel = function () {
                $scope.selected_status = "";
                $scope.selected_statusName = "";
                for (i = 0; i <= $scope.selected.length - 1;) {
                    $scope.selected_status = $scope.selected_status + $scope.selected[i].StateID + ",";
                    $scope.selected_statusName = $scope.selected_statusName + $scope.selected[i].StateText + " - ";
                    i = i + 1
                }
                var oTravelReportTransfer = {
                    CompanyCode: $scope.TravelReportTransfer.RequestorCompanyCode,
                    ClientCompanyID: $scope.TravelReportTransfer.ClientCompanyID,
                    RequestorCompanyCode: $scope.TravelReportTransfer.RequestorCompanyCode,
                    RequestorName: $scope.employeeData.Name,
                    RequestorID: $scope.TravelReportTransfer.RequestorID,
                    OrgUnitID: $scope.TravelReportTransfer.OrgUnitID,
                    PositionID: $scope.TravelReportTransfer.PositionID,
                    ApproveBeginDate: $scope.TravelReportTransfer.ApproveBeginDate,
                    ApproveEndDate: $scope.TravelReportTransfer.ApproveEndDate,
                    CreditCardNo: $scope.TravelReportTransfer.CreditCardNo,
                    ReceiptNoBegin: $scope.TravelReportTransfer.ReceiptNoBegin,
                    ReceiptNoEnd: $scope.TravelReportTransfer.ReceiptNoEnd,
                    RequestNoBegin: $scope.TravelReportTransfer.RequestNoBegin,
                    RequestNoEnd: $scope.TravelReportTransfer.RequestNoEnd,
                    Status: $scope.selected_status,
                    StatusName: $scope.selected_statusName
                };
                var MoDule = 'Report/';
                var Functional = 'PrepareShowReport';
                var URL = CONFIG.SERVER + MoDule + Functional;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oTravelReportTransfer
                }).then(function successCallback(response) {
                    //console.log(response);
                    window.open(CONFIG.SERVER + "WebForms/CreditCardReport.aspx", "_bank");
                }, function errorCallback(response) {
                    console.log('error PostDetailReportController.', response);
                });
            }

            //auto complete company 
            function createFilterForCompany(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    if (!x) return false;
                    var source = angular.lowercase(x.companyfullname);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }
            var tempResult_Company;
            $scope.querySearchCompany = function (query) {
                if (!$scope.lstCompany) return;
                var results = angular.copy(query ? $scope.lstCompany.filter(createFilterForCompany(query)) : $scope.lstCompany), deferred;
                results = results.splice(0, 30);
                return results;
            };
            $scope.selectedItemCompanyChange = function (item) {
                if (angular.isDefined(item) && item != null) {
                    $scope.ap_pdr_text.company = item.companyfullname;
                    $scope.TravelReportTransfer.RequestorCompanyCode = item.CompanyCode;
                    tempResult_Company = angular.copy(item);
                    $scope.EnableProjectCode();
                    $scope.EnableHaveContract();
                    $scope.GetEmployees();
                }
                else {
                    $scope.ap_pdr_text.company = '';
                    $scope.TravelReportTransfer.RequestorCompanyCode = '';
                    $scope.selectedItemEmpChange();
                }
            };
            $scope.tryToSelect_Company = function () {
                $scope.ap_pdr_text.company = '';
                $scope.TravelReportTransfer.RequestorCompanyCode = '';
                $scope.selectedItemEmpChange();
            }
            //auto complete company 

            //Get ModuleSetting
            $scope.EnableProjectCode = function () {
                URL = CONFIG.SERVER + 'HRTR/GetMuduleSettingForReport';
                oRequestParameter = { InputParameter: { "KeyModule": "ENABLE_PROJECTCOST", "CompanyCode": $scope.TravelReportTransfer.RequestorCompanyCode } };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.ENABLE_PROJECTCOST = response.data;
                }, function errorCallback(response) {
                    console.log('error GetEmployees list', response);
                    $scope.loader.enable = false;
                });
            }
            $scope.EnableHaveContract = function () {
                URL = CONFIG.SERVER + 'HRTR/GetMuduleSettingForReport';
                oRequestParameter = { InputParameter: { "KeyModule": "HAVE_CONTRACT_EMPLOYEE", "CompanyCode": $scope.TravelReportTransfer.RequestorCompanyCode } };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.ENABLE_HAVE_CONTRACT_EMPLOYEE = response.data;
                }, function errorCallback(response) {
                    console.log('error GetEmployees list', response);
                    $scope.loader.enable = false;
                });
            }
            //Get ModuleSetting

            //auto complete employee 
            function createFilterForEmp(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    if (!x) return false;
                    var source = angular.lowercase(x.EmployeeName);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }
            var tempResult_Emp;
            $scope.querySearchEmp = function (query) {
                if (!$scope.lstEmployeeData) return;
                var results = angular.copy(query ? $scope.lstEmployeeData.filter(createFilterForEmp(query)) : $scope.lstEmployeeData), deferred;
                results = results.splice(0, 30);
                return results;
            };
            $scope.selectedItemEmpChange = function (item) {
                if (angular.isDefined(item) && item != null) {
                    $scope.ap_pdr_text.emp = item.EmployeeName;
                    $scope.TravelReportTransfer.RequestorID = item.EmployeeID;
                    tempResult_Emp = angular.copy(item);

                    // get position
                    $scope.GetPositionByEmployee($scope.TravelReportTransfer.RequestorID);
                }
                else {
                    $scope.TravelReportTransfer.RequestorID = '';
                    $scope.ap_pdr_text.emp = '';
                    $scope.TravelReportTransfer.PositionID = '';
                    $scope.ap_pdr_text.position = '';
                    $scope.TravelReportTransfer.OrgUnitID = '';
                    if ($scope.lstOrganization) $scope.lstOrganization.ObjectID = '';
                }
            };
            $scope.tryToSelect_Emp = function () {
                $scope.ap_pdr_text.emp = '';
                $scope.TravelReportTransfer.RequestorID = '';
            }
            //auto complete employee 

            //auto complete creditcard  
            function createFilterForProject(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    if (!x) return false;
                    var source = angular.lowercase(x.CostCenterName);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }

            var tempResult_CreditCard;
            $scope.querySearchCreditCard = function (query) {
                if (!$scope.lstCreditCard) return;
                var results = angular.copy(query ? $scope.lstCreditCard.filter(createFilterForProject(query)) : $scope.lstCreditCard), deferred;
                results = results.splice(0, 30);
                return results;
            };
            $scope.selectedItemCreditCardChange = function (item) {
                if (angular.isDefined(item) && item != null) {
                    $scope.ap_pdr_text.creditcard = item.CreditCardNo;
                    $scope.TravelReportTransfer.CreditCardNo = item.CreditCardNo;
                    tempResult_CreditCard = angular.copy(item);
                }
                else {
                    $scope.ap_pdr_text.creditcard = '';
                    $scope.TravelReportTransfer.CreditCardNo = '';
                }
            };
            $scope.tryToSelect_CreditCard = function () {
                $scope.ap_pdr_text.creditcard = '';
                $scope.TravelReportTransfer.CreditCardNo = '';
            }
            //auto complete creditcard

            //auto complete Status
            $scope.GetStatus = function () {
                URL = CONFIG.SERVER + 'HRTR/GetStatusForReport';
                oRequestParameter = { InputParameter: { "ReportID": 4 }, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.lstStatus = response.data;
                }, function errorCallback(response) {
                    console.log('error GetCostcenter.', response);
                });
            }
            $scope.GetStatus();
            function createFilterForStatus(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    if (!x) return false;
                    var source = angular.lowercase(x.StateID + " : " + x.StateText);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }
            var tempResult_State;
            $scope.init_State = function () { }
            $scope.querySearchState = function (query) {
                if (!$scope.lstStatus) return;
                var results = angular.copy(query ? $scope.lstStatus.filter(createFilterForStatus(query)) : $scope.lstStatus), deferred;
                results = results.splice(0, 30);
                return results;
            };
            $scope.selectedItemStateChange = function (item) {
                if (angular.isDefined(item) && item != null) {
                    $scope.ap_pdr_text.StateName = item.StateText;
                    $scope.TravelReportTransfer.Status = item.StateID;
                    $scope.TravelReportTransfer.StatusName = item.StateText;
                    tempResult_State = angular.copy(item);
                }
                else {
                    $scope.ap_pdr_text.StateName = '';
                }
            };
            $scope.tryToSelect_State = function (text) {
                $scope.ap_pdr_text.StateName = '';
                $scope.TravelReportTransfer.StateID = '';
            }
            $scope.checkText_State = function (text) {
                if (text == '') {
                    $scope.ap_pdr_text.StateName = '';
                    $scope.TravelReportTransfer.Status = '';
                    $scope.TravelReportTransfer.StateName = '';
                }
                else
                    $scope.ap_pdr_text.StateName = text;
            }

            //List View
            $scope.selected = [];
            $scope.selected_status = "";
            $scope.selected_statusName = "";
            $scope.toggle = function (item, list) {
                var idx = list.indexOf(item);
                if (idx > -1) {
                    list.splice(idx, 1);
                }
                else {
                    list.push(item);
                }
            };
            $scope.exists = function (item, list) {
                return list.indexOf(item) > -1;
            };
            //auto complete Status
        }]);
})();