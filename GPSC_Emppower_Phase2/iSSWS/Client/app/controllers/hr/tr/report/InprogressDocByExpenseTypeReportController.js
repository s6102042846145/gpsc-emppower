﻿(function () {
    angular.module('ESSMobile')
        .controller('InprogressDocByExpenseTypeReportController', ['$scope', '$http', '$routeParams', '$location', 'CONFIG', '$mdDialog', '$filter', function ($scope, $http, $routeParams, $location, CONFIG, $mdDialog, $filter) {

            $scope.Textcategory = 'REPORT_FILTER';
            $scope.TravelReportTransfer = {};
            //$scope.TravelReportTransfer.TravelBeginDate = moment().format('DD/MM/YYYY');
            //$scope.TravelReportTransfer.TravelEndDate = moment().format('DD/MM/YYYY');
            $scope.TravelReportTransfer.RequestorCompanyCode = '';
            $scope.TravelReportTransfer.Status = '';

            $scope.TravelReportTransfer.ClientCompanyID = '';
            $scope.TravelReportTransfer.RequestorCompanyCode = '';
            $scope.TravelReportTransfer.RequestorID = '';
            $scope.TravelReportTransfer.OrgUnitID = '';
            $scope.TravelReportTransfer.CCCodeBegin = '';
            $scope.TravelReportTransfer.CCCodeEnd = '';
            $scope.TravelReportTransfer.IOCode = '';
            $scope.TravelReportTransfer.ProjectCode = '';
            $scope.TravelReportTransfer.RequestNo = '';
            $scope.TravelReportTransfer.ApproveBeginDate = '';
            $scope.TravelReportTransfer.ApproveEndDate = '';
            $scope.TravelReportTransfer.InprogressStatus = '';
            $scope.TravelReportTransfer.Status = '';
            $scope.IsData = true;
            $scope.disableEmployee = true;
            //#Member
            var URL;
            var oRequestParameter;

            //#Events
            $scope.setSelectedBeginDate = function (selectedDate) {
                $scope.TravelReportTransfer.ApproveBeginDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
                if (selectedDate > $scope.TravelReportTransfer.ApproveEndDate)
                    $scope.TravelReportTransfer.ApproveEndDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };
            $scope.setSelectedEndDate = function (selectedDate) {
                $scope.TravelReportTransfer.ApproveEndDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
                if (selectedDate < $scope.TravelReportTransfer.ApproveBeginDate)
                    $scope.TravelReportTransfer.ApproveBeginDate = $filter('date')(selectedDate, 'yyyy-MM-dd');
            };

            //#Functional
            $scope.GetCompanyList = function () {
                $scope.loader.enable = true;
                var MoDule = 'Share/';
                var Functional = 'GetCompanyList';
                URL = CONFIG.SERVER + MoDule + Functional;
                console.log(URL);
                $http({
                    method: 'POST',
                    url: URL
                }).then(function successCallback(response) {
                    console.log('Finish GetCompanyCode', response.data);
                    $scope.lstCompany = response.data;
                    var c_emp = getToken(CONFIG.USER);
                    $scope.language = c_emp.Language;
                    if ($scope.TravelReportTransfer.RequestorCompanyCode) {
                        $scope.disableEmployee = false;
                    }
                    // filter only response company 
                    URL = CONFIG.SERVER + 'HRTR/GetResponseCompany';
                    oRequestParameter = { InputParameter: '', CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.lstCompany = $scope.lstCompany.filter(createFilterCompany(response.data));

                        if ($scope.lstCompany.length > 1) {
                            for (var i = 0; i < $scope.lstCompany.length; i++) {
                                $scope.lstCompany[i].companyfullname = $scope.lstCompany[i].CompanyCode + " : " + $scope.lstCompany[i].Name;
                                if (c_emp.CompanyCode == $scope.lstCompany[i].CompanyCode) {
                                    $scope.TravelReportTransfer.RequestorCompanyCode = $scope.lstCompany[i].CompanyCode;
                                    $scope.lstCompany[i].companyfullname = $scope.language == 'TH' ? $scope.lstCompany[i].Name + ' : ' + $scope.lstCompany[i].FullNameTH : $scope.lstCompany[i].Name + ':' + $scope.lstCompany[i].FullNameEN;
                                    $scope.ap_pdr_text_company = $scope.lstCompany[i].companyfullname;
                                }
                                else {
                                    $scope.TravelReportTransfer.RequestorCompanyCode = $scope.lstCompany[0].CompanyCode;
                                    $scope.lstCompany[0].companyfullname = $scope.language == 'TH' ? $scope.lstCompany[0].Name + ' : ' + $scope.lstCompany[0].FullNameTH : $scope.lstCompany[0].Name + ':' + $scope.lstCompany[0].FullNameEN;
                                    $scope.ap_pdr_text_company = $scope.lstCompany[0].companyfullname;
                                }
                            }
                        }
                        else if($scope.lstCompany[0] != null) {
                            $scope.TravelReportTransfer.RequestorCompanyCode = $scope.lstCompany[0].CompanyCode;
                            $scope.lstCompany[0].companyfullname = $scope.language == 'TH' ? $scope.lstCompany[0].Name + ' : ' + $scope.lstCompany[0].FullNameTH : $scope.lstCompany[0].Name + ':' + $scope.lstCompany[0].FullNameEN;
                            $scope.ap_pdr_text_company = $scope.lstCompany[0].companyfullname;

                        }
                        $scope.loader.enable = false;
                        if ($scope.lstCompany[0] != null) {
                            $scope.GetEmployees();
                            $scope.EnableProjectCode();
                            $scope.EnableHaveContract();
                        }

                    }, function errorCallback(response) {
                        console.log('error GetEmployees list', response);
                        $scope.loader.enable = false;
                    });

                }, function errorCallback(response) {
                    // Error
                    console.log('error GetCompanyCode.', response);
                    $scope.loader.enable = false;
                });
            }
            $scope.GetCompanyList();

            //Get ModuleSetting
            $scope.EnableProjectCode = function () {
                URL = CONFIG.SERVER + 'HRTR/GetMuduleSettingForReport';
                oRequestParameter = { InputParameter: { "KeyModule": "ENABLE_PROJECTCOST", "CompanyCode": $scope.TravelReportTransfer.RequestorCompanyCode } };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.ENABLE_PROJECTCOST = response.data;
                }, function errorCallback(response) {
                    console.log('error GetEmployees list', response);
                    $scope.loader.enable = false;
                });
            }
            $scope.EnableHaveContract = function () {
                URL = CONFIG.SERVER + 'HRTR/GetMuduleSettingForReport';
                oRequestParameter = { InputParameter: { "KeyModule": "HAVE_CONTRACT_EMPLOYEE", "CompanyCode": $scope.TravelReportTransfer.RequestorCompanyCode } };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.ENABLE_HAVE_CONTRACT_EMPLOYEE = response.data;
                }, function errorCallback(response) {
                    console.log('error GetEmployees list', response);
                    $scope.loader.enable = false;
                });
            }
            //Get ModuleSetting

            function createFilterCompany(arr) {
                return function filterFn(x) {
                    for (var i = 0; i < arr.length; i++) {
                        if (arr[i] == x.CompanyCode) {
                            return true;
                        }
                    }
                    return false;
                };
            }

            var calling_GetEmployees = false;
            $scope.GetEmployees = function (oSearch) {
                if (calling_GetEmployees) return;
                if (oSearch != '') {
                    $scope.loader.enable = true;
                    calling_GetEmployees = true;
                    URL = CONFIG.SERVER + 'HRTR/GetAllEmployeeNameForReport';//'HRTR/GetAllINFOTYPE0001ForReport';
                    oRequestParameter = { InputParameter: { "EmployeeSearch": oSearch, "CompanyCode": $scope.TravelReportTransfer.RequestorCompanyCode }, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };

                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.lstEmployeeData = response.data;
                        //if ($scope.lstEmployeeData != null && angular.isDefined($scope.lstEmployeeData) && $scope.lstEmployeeData.length > 0) {
                            $scope.TravelReportTransfer.RequestorID = '';
                            $scope.TravelReportTransfer.RequestorName = '';
                        //    $scope.GetPositionByEmployee($scope.TravelReportTransfer.RequestorID);
                        //}

                        if (response.data.length == 0) {
                            $mdDialog.show(
                              $mdDialog.alert()
                                .clickOutsideToClose(true)
                                .title('Warning')
                                .textContent('Employee not found.')
                                .ok('OK')
                            );
                        }
                        $scope.loader.enable = false;
                        calling_GetEmployees = false;

                    }, function errorCallback(response) {
                        console.log('error GetEmployees list', response);
                        $scope.loader.enable = false;
                        calling_GetEmployees = false;
                    });
                }
            }
            $scope.GetPositionByEmployee = function (oEmployeeID) {
                $scope.loader.enable = true;
                $scope.TravelReportTransfer.PositionID = '';
                $scope.TravelReportTransfer.OrgUnitID = '';
                $scope.lstPosition = null;
                $scope.lstOrganization = null;
                URL = CONFIG.SERVER + 'HRTR/GetAllPosition';
                oRequestParameter = { InputParameter: { "EmployeeID": oEmployeeID, "CompanyCode": $scope.TravelReportTransfer.RequestorCompanyCode }, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.lstPosition = response.data;
                    if ($scope.ap_pdr_text.emp) {
                        $scope.TravelReportTransfer.PositionID = $scope.lstPosition[0].ObjectID;
                        $scope.TravelReportTransfer.PositionName = $scope.lstPosition[0].PositionName;
                        $scope.ap_pdr_text.position = $scope.lstPosition[0].PositionName;
                        $scope.GetOrganizationByPositionID($scope.TravelReportTransfer.PositionID);
                    }
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    console.log('error GetPositionByEmployee', response);
                    $scope.loader.enable = false;
                });
            }
            $scope.GetOrganizationByPositionID = function (oPositionID) {
                $scope.loader.enable = true;
                $scope.TravelReportTransfer.OrgUnitID = '';
                $scope.lstOrganization = null;
                URL = CONFIG.SERVER + 'HRTR/GetOrganizationByPositionID';
                oRequestParameter = { InputParameter: { "PositionID": oPositionID, "CompanyCode": $scope.TravelReportTransfer.RequestorCompanyCode }, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.lstOrganization = response.data;
                    if ($scope.ap_pdr_text.position && $scope.lstOrganization) {
                        $scope.TravelReportTransfer.OrgUnitID = $scope.lstOrganization.ObjectID;
                    }
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    console.log('error GetOrganizationByPositionID', response);
                    $scope.loader.enable = false;
                });
            };
            $scope.getMasterData = function () {
                var oEmpData = getToken(CONFIG.USER);
                oEmpData.CompanyCode = $scope.TravelReportTransfer.RequestorCompanyCode;
                var URL = CONFIG.SERVER + 'HRTR/GetMasterData';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.lstCostcenter = response.data.CostCenterList;
                    $scope.lstProject = response.data.ProjectList;
                    $scope.lstInternalOrder = response.data.InternalOrderList;
                    $scope.lstOrganization = response.data.OrganizationList;
                    for (var i = 0; i < $scope.lstProject.length; i++) {
                        $scope.lstProject[i].ProjectName = $scope.lstProject[i].ProjectCode + ' : ' + $scope.lstProject[i].Name;
                    }
                }, function errorCallback(response) {
                    console.log('error GetMasterData.', response);
                });
            }
            $scope.getMasterData();

            $scope.ShowReport = function () {
                $scope.ViewData();
            }
            $scope.ViewData = function () {
                $scope.selected_status = "";
                $scope.selected_statusName = "";
                if ($scope.selected.length > 0) {
                    for (i = 0; i <= $scope.selected.length - 1;) {
                        $scope.selected_status += $scope.selected[i].StateID + ",";
                        $scope.selected_statusName = $scope.selected_statusName + $scope.selected[i].StateText + " - ";
                        i++;
                    }
                }
                var oTravelReportTransfer = {
                    EmployeeID: $scope.employeeData.EmployeeID,
                    RequestorCompanyCode: $scope.TravelReportTransfer.RequestorCompanyCode,
                    CompanyCode: $scope.TravelReportTransfer.RequestorCompanyCode,
                    ClientCompanyID: $scope.TravelReportTransfer.ClientCompanyID,
                    ClientCompanyName: $scope.TravelReportTransfer.ClientCompanyName,
                    OrgUnitID: $scope.TravelReportTransfer.OranizationID,
                    RequestorName: $scope.TravelReportTransfer.RequestorName,
                    RequestorID: $scope.TravelReportTransfer.RequestorID,
                    PositionID: $scope.TravelReportTransfer.PositionID,
                    Position: $scope.TravelReportTransfer.PositionName,
                    CostCenterCodeBegin: $scope.TravelReportTransfer.CCCodeBegin,
                    ContractNo: $scope.TravelReportTransfer.ContractNo,
                    Status: $scope.selected_status,
                    StatusName: $scope.selected_statusName
                };
                URL = CONFIG.SERVER + 'HRTR/GetInprogressDocReportNew';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oTravelReportTransfer
                }).then(function successCallback(response) {
                    if (response.data.InprogressDocReport.length > 0) {
                        $scope.IsData = true;
                        $scope.DataReport = response.data;
                        $scope.IsExcel = true;
                    }
                    else {
                        $scope.IsData = false;
                        $scope.IsExcel = false;
                        $scope.DataReport = null;
                        alert("Data not found.");
                    }
                }, function errorCallback(response) {
                    console.log('error GetCostcenter.', response);
                });
            };
            $scope.ExportToExcel = function () {
                $scope.selected_status = "";
                $scope.selected_statusName = "";
                if ($scope.selected.length > 0) {
                    for (i = 0; i <= $scope.selected.length - 1;) {
                        $scope.selected_status += $scope.selected[i].StateID + ",";
                        $scope.selected_statusName = $scope.selected_statusName + $scope.selected[i].StateText + " - ";
                        i++;
                    }
                }
                var oTravelReportTransfer = {
                    ExportType: "EXCEL",
                    EmployeeID: $scope.employeeData.EmployeeID,
                    RequestorCompanyCode: $scope.TravelReportTransfer.RequestorCompanyCode,
                    CompanyCode: $scope.TravelReportTransfer.RequestorCompanyCode,
                    ClientCompanyID: $scope.TravelReportTransfer.ClientCompanyID,
                    ClientCompanyName: $scope.TravelReportTransfer.ClientCompanyName,
                    OrgUnitID: $scope.TravelReportTransfer.OranizationID,
                    RequestorName: $scope.TravelReportTransfer.RequestorName,
                    RequestorID: $scope.TravelReportTransfer.RequestorID,
                    PositionID: $scope.TravelReportTransfer.PositionID,
                    Position: $scope.TravelReportTransfer.PositionName,
                    CostCenterCodeBegin: $scope.TravelReportTransfer.CCCodeBegin,
                    ContractNo: $scope.TravelReportTransfer.ContractNo,
                    Status: $scope.selected_status,
                    StatusName: $scope.selected_statusName
                };
                var MoDule = 'Report/';
                var Functional = 'PrepareShowReport';
                var URL = CONFIG.SERVER + MoDule + Functional;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oTravelReportTransfer
                }).then(function successCallback(response) {
                    //console.log(response);
                    window.open(CONFIG.SERVER + "WebForms/InprogressDocReport.aspx", "_bank");
                }, function errorCallback(response) {
                    console.log('error PostDetailReportController.', response);
                });
            }
            $scope.ExportToPDF = function()
            {
                $scope.selected_status = "";
                $scope.selected_statusName = "";
                if ($scope.selected.length > 0) {
                    for (i = 0; i <= $scope.selected.length - 1;) {
                        $scope.selected_status += $scope.selected[i].StateID + ",";
                        $scope.selected_statusName = $scope.selected_statusName + $scope.selected[i].StateText + " - ";
                        i++;
                    }
                }
                var oTravelReportTransfer = {
                    ExportType: "PDF",
                    EmployeeID: $scope.employeeData.EmployeeID,
                    RequestorCompanyCode: $scope.TravelReportTransfer.RequestorCompanyCode,
                    CompanyCode: $scope.TravelReportTransfer.RequestorCompanyCode,
                    ClientCompanyID: $scope.TravelReportTransfer.ClientCompanyID,
                    ClientCompanyName: $scope.TravelReportTransfer.ClientCompanyName,
                    OrgUnitID: $scope.TravelReportTransfer.OranizationID,
                    RequestorName: $scope.TravelReportTransfer.RequestorName,
                    RequestorID: $scope.TravelReportTransfer.RequestorID,
                    PositionID: $scope.TravelReportTransfer.PositionID,
                    Position: $scope.TravelReportTransfer.PositionName,
                    CostCenterCodeBegin: $scope.TravelReportTransfer.CCCodeBegin,
                    ContractNo: $scope.TravelReportTransfer.ContractNo,
                    RequestNo: $scope.TravelReportTransfer.RequestNo,
                    ApproveBeginDate: $scope.TravelReportTransfer.ApproveBeginDate,
                    ApproveEndDate: $scope.TravelReportTransfer.ApproveEndDate,
                    Status: $scope.selected_status,
                    StatusName: $scope.selected_statusName
                };
                var MoDule = 'Report/';
                var Functional = 'PrepareShowReport';
                var URL = CONFIG.SERVER + MoDule + Functional;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oTravelReportTransfer
                }).then(function successCallback(response) {
                    window.open(CONFIG.SERVER + "WebForms/InprogressDocReport.aspx", "_bank");
                }, function errorCallback(response) {
                    console.log('error PostDetailReportController.', response);
                });
            }

            //auto complete company 
            function createFilterCompany(arr) {
                return function filterFn(x) {
                    for (var i = 0; i < arr.length; i++) {
                        if (arr[i] == x.CompanyCode) {
                            return true;
                        }
                    }
                    return false;
                };
            }
            $scope.ClearData = function (isAll) {
                if (isAll) {
                    $scope.ap_pdr_text.Organization = '';
                    $scope.TravelReportTransfer.OranizationID = '';

                    $scope.ap_pdr_text.emp = '';
                    $scope.TravelReportTransfer.EmployeeID = '';
                    $scope.TravelReportTransfer.EmployeeName = '';
                    $scope.TravelReportTransfer.RequestorID = '';
                    $scope.TravelReportTransfer.RequestorName = '';

                    $scope.ap_pdr_text.position = '';
                    $scope.TravelReportTransfer.PositionID = '';
                    $scope.TravelReportTransfer.PositionName = '';

                    $scope.TravelReportTransfer.ClientCompanyID = '';
                    $scope.TravelReportTransfer.ClientCompanyName = '';
                    $scope.TravelReportTransfer.Status = '';
                    $scope.TravelReportTransfer.StatusName = '';
                    $scope.ap_pdr_text.ClientCompany = '';
                    $scope.ap_pdr_text.StateName = '';
                    $scope.TravelReportTransfer.ContractNo = '';

                    $scope.IsData = true;
                    $scope.IsExcel = false;
                    $scope.DataReport = null;
                    $scope.selected = [];

                    $scope.GetStatus();
                }
            }

            $scope.GetMasterIOReport = function () {
                URL = CONFIG.SERVER + 'HRTR/GetMasterDataIOReport';
                oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.lstInternalOrder = response.data;
                }, function errorCallback(response) {
                    console.log('error GetCostcenter.', response);
                });
            };
            $scope.GetMasterIOReport();

            //auto complete ClientCompany
            function createFilterForCompany(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    if (!x) return false;
                    var source = angular.lowercase(x.companyfullname);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }
            var tempResult_Company;
            $scope.querySearchCompany = function (query) {
                if (!$scope.lstCompany) return;
                var results = angular.copy(query ? $scope.lstCompany.filter(createFilterForCompany(query)) : $scope.lstCompany), deferred;
                results = results.splice(0, 30);
                return results;
            };
            $scope.selectedItemCompanyChange = function (item) {
                if (angular.isDefined(item) && item != null) {
                    $scope.ap_pdr_text_company = item.companyfullname;
                    $scope.TravelReportTransfer.RequestorCompanyCode = item.CompanyCode;
                    tempResult_Company = angular.copy(item);
                    $scope.EnableHaveContract();
                    $scope.EnableProjectCode();
                    $scope.GetEmployees();
                }
                else {
                    $scope.ClearData(true);
                }
            };
            $scope.tryToSelect_Company = function () {
                $scope.ap_pdr_text_company = '';
                $scope.TravelReportTransfer.RequestorCompanyCode = '';
            }
            $scope.checkText_Company = function () {
                if (tempResult_Company != null)
                    $scope.ap_pdr_text_company = tempResult_Company.companyfullname;//$scope.lstCompany[0].companyfullname;
                else
                    $scope.ap_pdr_text_company = $scope.lstCompany[0].companyfullname;
            }
            //auto complete company 

            //auto complete CostCenter
            function createFilterForCostCenter(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    if (!x) return false;
                    var source = angular.lowercase(x.CostCenterCode + ":" + x.Name);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }
            var tempResult_cc;
            $scope.init_CostCenter = function () {
            }
            $scope.querySearchCostCenter = function (query) {
                if (!$scope.lstCostcenter) return;
                var results = angular.copy(query ? $scope.lstCostcenter.filter(createFilterForCostCenter(query)) : $scope.lstCostcenter), deferred;
                results = results.splice(0, 30);
                return results;
            };
            $scope.selectedItemCostCenterChange = function (item) {
                if (angular.isDefined(item) && item != null) {
                    $scope.ap_pdr_text.cc = item.CostCenterCode + " : " + item.Name;
                    $scope.TravelReportTransfer.CCCodeBegin = item.CostCenterCode;
                    tempResult_cc = angular.copy(item);
                }
                else {
                    $scope.ap_pdr_text.cc = '';
                }
            };
            $scope.tryToSelect_cc = function (text) {
                $scope.ap_pdr_text.cc = '';
                $scope.TravelReportTransfer.CCCodeBegin = '';
            }
            $scope.checkText_cc = function (text) {
                if (text == '') {
                    $scope.ap_pdr_text.cc = '';
                    $scope.TravelReportTransfer.CCCodeBegin = '';
                }
                else
                    $scope.ap_pdr_text.cc = text;
            }
            //auto complete CostCenter

            //Oranization List (OrgUnit)
            function createFilterForOrganization(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    if (!x) return false;
                    var source = angular.lowercase(x.ObjectID + " : " + x.Text);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }
            var tempResult_Organization;
            $scope.init_Organization = function () { }
            $scope.querySearchOrganization = function (query) {
                if (!$scope.lstOrganization) return;
                var results = angular.copy(query ? $scope.lstOrganization.filter(createFilterForOrganization(query)) : $scope.lstOrganization), deferred;
                results = results.splice(0, 30);
                return results;
            };
            $scope.selectedItemOrganizationChange = function (item) {
                if (angular.isDefined(item) && item != null) {
                    $scope.ap_pdr_text.Organization = item.ObjectID + " : " + item.Text;
                    $scope.TravelReportTransfer.OranizationID = item.ObjectID;
                    $scope.TravelReportTransfer.OranizationName = item.Text;
                    tempResult_Organization = angular.copy(item);
                }
                else {
                    $scope.ap_pdr_text.Oranization = '';
                }
            };
            $scope.tryToSelect_Organization = function (text) {
                $scope.ap_pdr_text.Organization = '';
                $scope.TravelReportTransfer.Organization = '';
            }
            $scope.checkText_Organization = function (text) {
                if (text == '') {
                    $scope.ap_pdr_text.Organization = '';
                    $scope.TravelReportTransfer.OranizationID = '';
                    $scope.TravelReportTransfer.OranizationName = '';
                }
                else
                    $scope.ap_pdr_text.Organization = text;
            }
            //Oranization List

            //auto complete employee 
            function createFilterForEmp(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    if (!x) return false;
                    var source = angular.lowercase(x.EmployeeName);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }
            var tempResult_Emp;
            $scope.querySearchEmp = function (query) {
                if (!$scope.lstEmployeeData) return;
                var results = angular.copy(query ? $scope.lstEmployeeData.filter(createFilterForEmp(query)) : $scope.lstEmployeeData), deferred;
                results = results.splice(0, 30);
                return results;
            };
            $scope.selectedItemEmpChange = function (item) {
                if (angular.isDefined(item) && item != null) {
                    $scope.ap_pdr_text.emp = item.EmployeeName;
                    $scope.TravelReportTransfer.RequestorID = item.EmployeeID;
                    $scope.TravelReportTransfer.RequestorName = item.EmployeeName;
                    tempResult_Emp = angular.copy(item);
                    $scope.EnableProjectCode();
                    $scope.EnableHaveContract();
                    // get position
                    $scope.GetPositionByEmployee($scope.TravelReportTransfer.RequestorID);
                }
                else {
                    $scope.TravelReportTransfer.RequestorID = '';
                    $scope.ap_pdr_text.emp = '';
                    $scope.TravelReportTransfer.PositionID = '';
                    $scope.ap_pdr_text.position = '';
                    $scope.TravelReportTransfer.OrgUnitID = '';
                    if ($scope.lstOrganization) $scope.lstOrganization.ObjectID = '';
                }
            };
            $scope.tryToSelect_Emp = function () {
                $scope.ap_pdr_text.emp = '';
                $scope.TravelReportTransfer.RequestorID = '';
            }
            //auto complete employee 

            //auto complete position 
            function createFilterForPosition(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    if (!x) return false;
                    var source = angular.lowercase(x.PositionName);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }
            var tempResult_Position;
            $scope.querySearchPosition = function (query) {
                if (!$scope.lstPosition) return;
                var results = angular.copy(query ? $scope.lstPosition.filter(createFilterForPosition(query)) : $scope.lstPosition), deferred;
                results = results.splice(0, 30);
                return results;
            };
            $scope.selectedItemPositionChange = function (item) {
                if (angular.isDefined(item) && item != null) {
                    $scope.ap_pdr_text.position = item.PositionName;
                    $scope.TravelReportTransfer.PositionID = item.ObjectID;
                    $scope.TravelReportTransfer.PositionName = item.PositionName;
                    tempResult_Position = angular.copy(item);

                    // get org
                    $scope.GetOrganizationByPositionID(item.ObjectID);
                }
                else {
                    $scope.ap_pdr_text.position = '';
                    $scope.TravelReportTransfer.RequestorID = '';
                    $scope.TravelReportTransfer.OrgUnitID = '';
                    $scope.lstOrganization.ObjectID = '';
                }
            };
            $scope.tryToSelect_Position = function (text) {
                $scope.ap_pdr_text.position = '';
                $scope.TravelReportTransfer.RequestorID = '';
                $scope.TravelReportTransfer.OrgUnitID = '';
                $scope.lstOrganization.ObjectID = '';
            }
            //auto complete position 

            //auto complete CostCenter
            function createFilterForCostCenter(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    if (!x) return false;
                    var source = angular.lowercase(x.CostCenterCode + ":" + x.Name);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }
            var tempResult_cc;
            $scope.init_CostCenter = function () {
            }
            $scope.querySearchCostCenter = function (query) {
                if (!$scope.lstCostcenter) return;
                var results = angular.copy(query ? $scope.lstCostcenter.filter(createFilterForCostCenter(query)) : $scope.lstCostcenter), deferred;
                results = results.splice(0, 30);
                return results;
            };
            $scope.selectedItemCostCenterChange = function (item) {
                if (angular.isDefined(item) && item != null) {
                    $scope.ap_pdr_text.cc = item.CostCenterCode + " : " + item.Name;
                    $scope.TravelReportTransfer.CCCodeBegin = item.CostCenterCode;
                    tempResult_cc = angular.copy(item);
                }
                else {
                    $scope.ap_pdr_text.cc = '';
                }
            };
            $scope.tryToSelect_cc = function (text) {
                $scope.ap_pdr_text.cc = '';
                $scope.TravelReportTransfer.CCCodeBegin = '';
            }
            $scope.checkText_cc = function (text) {
                if (text == '') {
                    $scope.ap_pdr_text.cc = '';
                    $scope.TravelReportTransfer.CCCodeBegin = '';
                }
                else
                    $scope.ap_pdr_text.cc = text;
            }
            //auto complete CostCenter
            
            //auto complete InternalOrder
            function createFilterForIO(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    if (!x) return false;
                    var source = angular.lowercase(x.OrderID + " : " + x.Description);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }
            var tempResult_io;
            $scope.init_IO = function () {
            }
            $scope.querySearchIO = function (query) {
                if (!$scope.lstInternalOrder) return;
                var results = angular.copy(query ? $scope.lstInternalOrder.filter(createFilterForIO(query)) : $scope.lstInternalOrder), deferred;
                results = results.splice(0, 30);
                return results;
            };
            $scope.selectedItemIOChange = function (item) {
                if (angular.isDefined(item) && item != null) {
                    $scope.ap_pdr_text.io = item.OrderID + " : " + item.Description;
                    $scope.TravelReportTransfer.IOCode = item.OrderID;
                    tempResult_io = angular.copy(item);
                }
                else {
                    //$scope.ap_pdr_text.io = '';
                }
            };
            $scope.tryToSelect_io = function (text) {
                $scope.ap_pdr_text.io = '';
                $scope.TravelReportTransfer.IOCode = '';
            }
            $scope.checkText_io = function (text) {
                if (text == '') {
                    $scope.ap_pdr_text.io = '';
                    $scope.TravelReportTransfer.IOCode = '';
                }
                else
                    $scope.ap_pdr_text.io = text;
            }
            //auto complete InternalOrder

            //auto complete project  
            function createFilterForProject(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    if (!x) return false;
                    var source = angular.lowercase(x.CostCenterName);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }

            var tempResult_Project;
            $scope.querySearchProject = function (query) {
                if (!$scope.lstProject) return;
                var results = angular.copy(query ? $scope.lstProject.filter(createFilterForProject(query)) : $scope.lstProject), deferred;
                results = results.splice(0, 30);
                return results;
            };
            $scope.selectedItemProjectChange = function (item) {
                if (angular.isDefined(item) && item != null) {
                    $scope.ap_pdr_text.project = item.ProjectName;
                    $scope.TravelReportTransfer.ProjectCode = item.ProjectCode;
                    tempResult_Project = angular.copy(item);
                }
                else {
                    $scope.ap_pdr_text.project = '';
                    $scope.TravelReportTransfer.ProjectCode = '';
                }
            };
            $scope.tryToSelect_Project = function () {
                $scope.ap_pdr_text.project = '';
                $scope.TravelReportTransfer.ProjectCode = '';
            }
            //auto complete project

            $scope.GetClientCompanyForBSA = function () {
                URL = CONFIG.SERVER + 'HRTR/GetClientCompanyForBSA';
                oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.lstClientCompany = response.data;
                }, function errorCallback(response) {
                    console.log('error GetCostcenter.', response);
                });
            };
            $scope.GetClientCompanyForBSA();
            //auto complete ClientCompany
            function createFilterForClientCompany(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    if (!x) return false;
                    var source = angular.lowercase(x.Abbrv + " : " + x.CompanyName);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }
            var tempResult_ClientCompany;
            $scope.init_ClientCompany = function () { }
            $scope.querySearchClientCompany = function (query) {
                if (!$scope.lstClientCompany) return;
                var results = angular.copy(query ? $scope.lstClientCompany.filter(createFilterForClientCompany(query)) : $scope.lstClientCompany), deferred;
                results = results.splice(0, 30);
                return results;
            };
            $scope.selectedItemClientCompanyChange = function (item) {
                if (angular.isDefined(item) && item != null) {
                    $scope.ap_pdr_text.ClientCompany = item.Abbrv + " : " + item.CompanyName;
                    $scope.TravelReportTransfer.ClientCompanyID = item.CompanyID;
                    $scope.TravelReportTransfer.ClientCompanyName = item.CompanyName;
                    tempResult_ClientCompany = angular.copy(item);
                    $scope.EnableProjectCode();
                    $scope.EnableHaveContract();
                }
                else {
                    $scope.ap_pdr_text.ClientCompany = '';
                }
            };
            $scope.tryToSelect_ClientCompany = function (text) {
                $scope.ap_pdr_text.ClientCompany = '';
                $scope.TravelReportTransfer.ClientCompanyID = '';
            }
            $scope.checkText_ClientCompany = function (text) {
                if (text == '')
                    $scope.ap_pdr_text.ClientCompany = '';
                else
                    $scope.ap_pdr_text.ClientCompany = text;
            }
            //auto complete ClientCompany

            //Contract No
            $scope.GetContractNoGetAll = function () {
                URL = CONFIG.SERVER + 'HRTR/GetContractNoGetAll';
                oRequestParameter = { InputParameter: { "CompanyCode": $scope.employeeData.CompanyCode }, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    if (response.data.length > 0) {
                        $scope.lstContractNo = response.data;
                    }
                }, function errorCallback(response) {
                    console.log('error GetCostcenter.', response);
                });
            }
            $scope.GetContractNoGetAll();
            function createFilterForContractNo(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    if (!x) return false;
                    var source = angular.lowercase(x.ContractNo + " : " + x.ContractName);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }
            var tempResult_ContractNo;
            $scope.init_ContractNo = function () { }
            $scope.querySearchContractNo = function (query) {
                if (!$scope.lstContractNo) return;
                var results = angular.copy(query ? $scope.lstContractNo.filter(createFilterForContractNo(query)) : $scope.lstContractNo), deferred;
                results = results.splice(0, 30);
                return results;
            };
            $scope.selectedItemContractNoChange = function (item) {
                if (angular.isDefined(item) && item != null) {
                    $scope.ap_pdr_text.StateName = item.ContractName;
                    $scope.TravelReportTransfer.ContractNo = item.ContractNo;
                    $scope.TravelReportTransfer.ContractName = item.ContractName;
                    tempResult_ContractNo = angular.copy(item);
                }
                else {
                    $scope.ap_pdr_text.ContractName = '';
                }
            };
            $scope.tryToSelect_ContractNo = function (text) {
                $scope.ap_pdr_text.ContractName = '';
                $scope.TravelReportTransfer.ContractNo = '';
            }
            $scope.checkText_ContractNo = function (text) {
                if (text == '') {
                    $scope.ap_pdr_text.ContractName = '';
                    $scope.TravelReportTransfer.ContractNo = '';
                    $scope.TravelReportTransfer.ContractName = '';
                }
                else
                    $scope.ap_pdr_text.ContractName = text;
            }
            //Contract No

            //auto complete Status
            $scope.GetStatus = function () {
                URL = CONFIG.SERVER + 'HRTR/GetStatusForReport';
                oRequestParameter = { InputParameter: { "ReportID": 5 }, CurrentEmployee: getToken(CONFIG.USER), Creator: getToken(CONFIG.USER), Requestor: getToken(CONFIG.USER) };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    if (response.data.length > 0) {
                        $scope.lstStatus = response.data;
                        for (i = 0; i <= $scope.lstStatus.length - 1;) {
                            $scope.toggle($scope.lstStatus[i], $scope.selected);
                            i++;
                        }
                    }
                }, function errorCallback(response) {
                    console.log('error GetCostcenter.', response);
                });
            }
            $scope.GetStatus();
            function createFilterForStatus(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(x) {
                    if (!x) return false;
                    var source = angular.lowercase(x.StateID + " : " + x.StateText);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }
            var tempResult_State;
            $scope.init_State = function () { }
            $scope.querySearchState = function (query) {
                if (!$scope.lstStatus) return;
                var results = angular.copy(query ? $scope.lstStatus.filter(createFilterForStatus(query)) : $scope.lstStatus), deferred;
                results = results.splice(0, 30);
                return results;
            };
            $scope.selectedItemStateChange = function (item) {
                if (angular.isDefined(item) && item != null) {
                    $scope.ap_pdr_text.StateName = item.StateText;
                    $scope.TravelReportTransfer.Status = item.StateID;
                    $scope.TravelReportTransfer.StatusName = item.StateText;
                    tempResult_State = angular.copy(item);
                }
                else {
                    $scope.ap_pdr_text.StateName = '';
                }
            };
            $scope.tryToSelect_State = function (text) {
                $scope.ap_pdr_text.StateName = '';
                $scope.TravelReportTransfer.StateID = '';
            }
            $scope.checkText_State = function (text) {
                if (text == '') {
                    $scope.ap_pdr_text.StateName = '';
                    $scope.TravelReportTransfer.Status = '';
                    $scope.TravelReportTransfer.StateName = '';
                }
                else
                    $scope.ap_pdr_text.StateName = text;
            }

            //List View
            $scope.selected = [];
            $scope.selected_status = "";
            $scope.selected_statusName = "";
            $scope.toggle = function (item, list) {
                var idx = list.indexOf(item);
                if (idx > -1) {
                    list.splice(idx, 1);                    
                }
                else {
                    list.push(item);
                }
            };
            $scope.exists = function (item, list) {
                return list.indexOf(item) > -1;
            };
            //auto complete Status

            //Get BusinessArea
            $scope.GetBusinessArea = function () {
                oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER) }
                URL = CONFIG.SERVER + 'HRTR/GetBusinessAreaAll';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.lstBAAll = response.data;
                }, function errorCallback(response) {
                    console.log('error lstVendor', response);
                });
            };
            $scope.GetBusinessArea();

        }]);
})();