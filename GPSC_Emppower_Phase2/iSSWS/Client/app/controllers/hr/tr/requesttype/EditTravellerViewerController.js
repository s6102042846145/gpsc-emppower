﻿(function () {
    angular.module('ESSMobile')
        .controller('EditTravellerViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG','$q', function ($scope, $http, $routeParams, $location, $filter, CONFIG,$q) {
          
            $scope.ChildAction.SetData = function () {
                console.log("SetData");
            }

            $scope.ChildAction.LoadData = function () {
                console.log("LoadData");
            }


            $scope.init = function () {
                var promise_service1 = InitialConfig();
                var promise_service2 = $scope.GetTravelModel();
                var promise_service3 = $scope.GetMasterdata();
                $q.all([promise_service1, promise_service2, promise_service3]).then(function (response) {

                    $scope.data.model.ready = true;
                    console.debug('init success')
                }, function (response) {

                    console.error('init error');

                });

            }

            function InitialConfig() {
                $scope.Page = "TravelExpense";
                //for ExchangeRateControl
                
                $scope.data.TravelSeqEdit = false;
                $scope.data.IsExchangeRateEdit = false;
                $scope.data.IsTravelRequest = true;
                $scope.data.Remark = $scope.TravelPersonalEdit.Remark;
                $scope.TravelGroupRequest = $scope.TravelPersonalEdit.TravelGroupRequest;
                $scope.data.ExchangeRateCaptures = $scope.TravelGroupRequest.ExchangeRateCaptures;
                $scope.data.TravelSchedulePlaces = $scope.TravelGroupRequest.TravelSchedulePlaces;
                $scope.data.ProjectCostDistributions = $scope.TravelGroupRequest.ProjectCostDistributions;
                $scope.data.ProjectCostDistributionTemp = angular.copy($scope.TravelGroupRequest.ProjectCostDistributions);
                $scope.data.Travelers = $scope.TravelGroupRequest.Travelers;
                $scope.data.GroupBudgets = $scope.TravelGroupRequest.GroupBudgets;
                if ($scope.data.Travelers[0].oCashAdvance.PaymentFlagCaptures.length > 0) {
                    $scope.data.PaymentFlagCaptures = $scope.data.Travelers[0].oCashAdvance.PaymentFlagCaptures;
                }
                if ($scope.TravelPersonalEdit.PaymentFlagCaptures.length > 0) {
                    $scope.data.PaymentFlagCaptures = $scope.TravelPersonalEdit.PaymentFlagCaptures;
                }
                $scope.data.Estimate = $scope.TravelGroupRequest.TotalAmount;
                $scope.data.PersonalVehicleSelect = $scope.TravelGroupRequest.PersonalVehicleSelect;
                $scope.TravelGroupRequest.DefaultCurrencyCode = $scope.TravelPersonalEdit.DefaultCurrencyCode;
                $scope.data.DefaultCurrencyCode = $scope.TravelPersonalEdit.DefaultCurrencyCode;
                
                $scope.data.IsGroupManager = true;
                $scope.CheckPersonalVehicle();
                $scope.GetTravelRequestConfig($scope.TravelGroupRequest);
                $scope.data.Readonly = true;
            }
       
            /* ====== wizard form control ====== */
            $scope.wizardFormControl = {
                receiptIsActive: false,
                withoutReceiptIsActive: false
            };

            /* ====== !wizard form control ====== */
        }])
})();