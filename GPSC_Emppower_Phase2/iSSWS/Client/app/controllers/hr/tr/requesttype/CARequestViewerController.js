﻿(function () {
    angular.module('ESSMobile')
        .controller('CARequestViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG','$q', function ($scope, $http, $routeParams, $location, $filter, CONFIG,$q) { 
            $scope.data.TravelPersonalEdit = false;
            $scope.ChildAction.SetData = function () {
            };


            $scope.ChildAction.LoadData = function () {
        
            };


            /* ====== Inital data before load control (Start)====== 
             AddBy: Ratchatawan W. (30 Mar 2017)
             Description: For initial data before load all control in this page
            */
            $scope.init = function () {
                var promise_service1 = InitialConfig();
                var promise_service3 = $scope.GetReversalReason();
                var promise_service4 = $scope.GetExpenseRateTypeForLookup();
                var promise_service5 = $scope.GetTravelModel();
                $q.all([promise_service1, promise_service3, promise_service4, promise_service5]).then(function (response) {

                    $scope.data.model.ready = true;
                    console.debug('init success')
                }, function (response) {

                    console.error('init error');

                    });
            }

            function InitialConfig() {
                $scope.Page = "TravelExpense";
                console.log('InitialConfig');
                //for ExchangeRateControl
                $scope.data.Readonly = true;
                //$scope.data.TravelSeqEdit = false;
                $scope.data.IsExchangeRateEdit = false;
                $scope.data.IsPersonalCashAdvance = true;
                //$scope.data.IsTravelRequest = true;
                $scope.data.ExchangeRateCaptures = $scope.TravelGroupRequest.ExchangeRateCaptures;
                $scope.data.TravelSchedulePlaces = $scope.TravelGroupRequest.TravelSchedulePlaces;
                $scope.data.ProjectCostDistributions = $scope.TravelGroupRequest.ProjectCostDistributions;
                $scope.data.ProjectCostDistributionTemp = angular.copy($scope.TravelGroupRequest.ProjectCostDistributions);
                $scope.data.Travelers = $scope.TravelGroupRequest.Travelers;
                $scope.data.GroupBudgets = $scope.TravelGroupRequest.GroupBudgets;
                $scope.data.Travelers[0].RequestNo = $scope.document.RequestNo;
                $scope.data.PaymentFlagCaptures = $scope.data.Travelers[0].oCashAdvance.PaymentFlagCaptures;
                $scope.data.CAEstimate = $scope.TravelGroupRequest.Travelers[0].TotalAmount;
                $scope.data.TotalAmount = $scope.TravelGroupRequest.Travelers[0].oCashAdvance.TotalAmount;
                $scope.GetTravelRequestConfig();
 
                //Check Current Posting Date BUG012_2 By Nipon Supap 28-11-017
                $scope.IsLastPostingDate = $scope.document.Additional.Travelers[0].oCashAdvance.IsLastPostingDate;

                //if (!$scope.document.Additional.TravelGroupRequest[0].Travelers[0].oCashAdvance.IsOverridePostingDate) {
                //    $scope.document.Additional.Travelers[0].oCashAdvance.PostingDate = $filter('date')(new Date(), 'yyyy-MM-ddT00:00:00');
                //}

                //if (!$scope.document.Additional.TravelGroupRequest[0].Travelers[0].oCashAdvance.IsOverrideBaseLineDate) {
                //    $scope.document.Additional.Travelers[0].oCashAdvance.BaseLineDate = $filter('date')(new Date(), 'yyyy-MM-ddT00:00:00');
                //}
                
            }
            
            $(function () {
                $('input[name="post_date"]').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    locale: {
                        format: 'DD/MM/YYYY'
                    }
                }, function (start, end, label) {
                });
            });

            $scope.setSelectedPostingDate = function () {
                //$scope.document.Additional.Travelers[0].oCashAdvance.PostingDate = $filter('date')(moment(newDate, "DD/MM/YYYY").toDate(), 'yyyy-MM-ddT00:00:00');
                $scope.document.Additional.Travelers[0].oCashAdvance.IsOverridePostingDate = true;
            };

            $scope.setSelectedBaseLineDate = function () {
                //$scope.document.Additional.Travelers[0].oCashAdvance.BaseLineDate = $filter('date')(moment(newDate, "DD/MM/YYYY").toDate(), 'yyyy-MM-ddT00:00:00');
                $scope.document.Additional.Travelers[0].oCashAdvance.IsOverrideBaseLineDate = true;
            };

            $scope.getReasonCodeText = function (reasonCode) {
                if (angular.isDefined($scope.reversalReason) && $scope.reversalReason != null) {
                    for (var i = 0; i < $scope.reversalReason.length; i++) {
                        return $scope.reversalReason[i].Description;
                    }
                }
                return '';
            };

            //add by nipon supap 
            $scope.isUserRoleAccounting = false;
            $scope.checkAccountingRole = function () {
                var URL = CONFIG.SERVER + 'HRTR/IsAccountUser';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.isUserRoleAccounting = response.data;
                    $scope.GetFlagForViewPostingDateAndPaymentDate();
                    console.log('IsAccountingRole.', response.data);
                }, function errorCallback(response) {
                    console.log('error IsAccountingRole.', response);
                });
            };
            $scope.checkAccountingRole();
            $scope.GetFlagForViewPostingDateAndPaymentDate = function () {
                var URL = CONFIG.SERVER + 'HRTR/GetFlagForViewPostingDateAndPaymentDate';
                var oRequestParameter = { InputParameter: { 'isUserRoleAccounting': $scope.isUserRoleAccounting, 'CompanyCode': $scope.document.Requestor.CompanyCode, 'RequestFlowItemCode': $scope.document.CurrentFlowItemCode, 'RequestNo': $scope.document.RequestNo, 'EmployeeID': $scope.employeeData.EmployeeID } };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    if (response.data.length > 0) {
                        $scope.CanViewPostingDate = response.data[0].CanViewPostingDate;
                        $scope.CanViewInAccountRolePostingDate = response.data[0].CanViewInAccountRolePostingDate;
                        $scope.CanViewPaymentDate = response.data[0].CanViewPaymentDate;
                        $scope.CanViewInAccountRolePaymentDate = response.data[0].CanViewInAccountRolePaymentDate;
                        if ($scope.CanViewInAccountRolePaymentDate) {
                            $scope.document.Additional.Travelers[0].oCashAdvance.BaseLineDate = $scope.limitToday;
                        }
                        if ($scope.CanViewInAccountRolePaymentDate) {
                            $scope.document.Additional.Travelers[0].oCashAdvance.PostingDate = $scope.limitToday;
                        }
                    }
                }, function errorCallback(response) {
                    console.error('error EnableOverideIO.', response);
                });
            }

            getOverideProjectCost();
            function getOverideProjectCost() {
                var URL = CONFIG.SERVER + 'HRTR/GetModuleSetting';
                var oRequestParameter = { InputParameter: { 'KEY': 'ENABLE_PROJECTCOST' }, CurrentEmployee: $scope.employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    var str = response.data;
                    var res = str.toLowerCase();
                    $scope.settings.ENABLE_PROJECTCOST = (res == 'true');
                }, function errorCallback(response) {
                    console.error('error EnableOverideIO.', response);
                });
            };

        }]);
})();