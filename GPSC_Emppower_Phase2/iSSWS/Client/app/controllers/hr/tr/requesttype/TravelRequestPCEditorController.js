﻿(function () {
    angular.module('ESSMobile')
        .controller('TravelRequestPCEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$q', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $q) {
            $scope.content.Header = $scope.Text['TRAVELEXPENSE'].REQUESTWITHPETTYCASH_;
            $scope.ChildAction.SetData = function () {
            }
            $scope.ChildAction.LoadData = function () {
                console.log("LoadData");
                if ($scope.data.GroupBudgets && $scope.data.GroupBudgets.length) {
                    for (var i = 0; i < $scope.data.GroupBudgets.length; i++) {
                        /*if (
                            !$scope.data.GroupBudgets[i]
                            || !$scope.data.GroupBudgets[i].ExpenseTypeGroupID
                            || !$scope.data.GroupBudgets[i].CostCenter
                            || !$scope.data.GroupBudgets[i].IO
                            || !$scope.data.GroupBudgets[i].AlternativeIOOrg
                            ) {
                            $scope.data.GroupBudgets.splice(i, 1);
                            $scope.data.Travelers[0].oCashAdvance.GroupCashAdvances.splice(i, 1);
                        }*/
                        if (!$scope.data.GroupBudgets[i] || !$scope.data.GroupBudgets[i].ExpenseTypeGroupID) {
                            $scope.data.GroupBudgets[i].ExpenseTypeGroupID = 0;
                            $scope.data.Travelers[0].oCashAdvance.GroupCashAdvances[i].ExpenseTypeGroupID = 0;
                        }
                        if (!$scope.data.GroupBudgets[i] || !$scope.data.GroupBudgets[i].CostCenter) {
                            $scope.data.GroupBudgets[i].CostCenter = '0';
                        }
                        if (!$scope.data.GroupBudgets[i] || !$scope.data.GroupBudgets[i].AlternativeIOOrg) {
                            $scope.data.GroupBudgets[i].AlternativeIOOrg = '0';
                        }
                        if (!$scope.data.GroupBudgets[i] || !$scope.data.GroupBudgets[i].IO) {
                            $scope.data.GroupBudgets[i].IO = '0';
                        }
                    }
                }
                $scope.TravelGroupRequest.RequestNo = $scope.document.RequestNo;
                $scope.TravelGroupRequest.TravelMode = ($scope.TravelGroupRequest.GroupSelect) ? "G" : "P";
                $scope.TravelGroupRequest.ExchangeRateCaptures = $scope.data.ExchangeRateCaptures;
                $scope.TravelGroupRequest.TravelSchedulePlaces = $scope.data.TravelSchedulePlaces;
                $scope.TravelGroupRequest.ProjectCostDistributions = $scope.data.ProjectCostDistributions;
                $scope.TravelGroupRequest.Travelers = $scope.data.Travelers;
                $scope.TravelGroupRequest.Travelers[0].oPettyCash.PettyCode = $scope.data.PettyCode;
                $scope.TravelGroupRequest.Travelers[0].oPettyCash.PettyName = $scope.data.PettyName;
                $scope.TravelGroupRequest.GroupBudgets = $scope.data.GroupBudgets;
                $scope.TravelGroupRequest.PersonalVehicle = $scope.data.PersonalVehicle; // ($scope.data.PersonalVehicleSelect == true) ? 1 : 2;
                $scope.TravelGroupRequest.DefaultCurrencyCode = $scope.data.DefaultCurrencyCode;
                $scope.TravelGroupRequest.TotalAmount = $scope.data.Estimate;
                $scope.TravelGroupRequest.Travelers[0].oCashAdvance.PaymentFlagCaptures = [];
                $scope.TravelGroupRequest.PaymentFlagCaptures = $scope.data.PaymentFlagCaptures;
                console.log('TravelConfig20/2017', $scope.TravelGroupRequest.Travelers);
                $scope.CheckUploadFiles();
                $scope.document.Additional = $scope.TravelGroupRequest;
            }
           
            /* ====== wizard form control ====== */
            $scope.wizardFormControl = {
                receiptIsActive: false,
                withoutReceiptIsActive: false
            };
            /* ====== !wizard form control ====== */

            /* ====== Inital data before load control (Start)====== 
               AddBy: Ratchatawan W. (30 Mar 2017)
               Description: For initial data before load all control in this page
           */
            $scope.init = function () {
                var promise_service0 = $scope.getTextByCompany($scope.document.Requestor);
                var promise_service1 = InitialConfig();
                var promise_service2 = $scope.GetTravelModel();
                var promise_service3 = $scope.GetMasterdata();
                $q.all([promise_service0,promise_service1, promise_service2, promise_service3]).then(function (response) {

                    $scope.data.model.ready = true;
                    console.debug('init success')
                }, function (response) {

                    console.error('init error');

                });

            }

            function InitialConfig() {
                $scope.setBlockAction(true, $scope.Text["SYSTEM"]["PROCESSING"]);
                $scope.Page = "TravelExpense";
                console.log('InitialConfig');
                $scope.data.Readonly = false;
                $scope.data.TravelSeqEdit = false;
                $scope.data.IsExchangeRateEdit = false;
                $scope.data.IsTravelRequest = true;
                $scope.data.ExchangeRateCaptures = $scope.TravelGroupRequest.ExchangeRateCaptures;
                $scope.data.TravelSchedulePlaces = $scope.TravelGroupRequest.TravelSchedulePlaces;
                $scope.data.ProjectCostDistributions = $scope.TravelGroupRequest.ProjectCostDistributions;
                $scope.data.ProjectCostDistributionTemp = angular.copy($scope.TravelGroupRequest.ProjectCostDistributions);
                for (var i = 0; i < $scope.data.ProjectCostDistributionTemp.length; i++) {
                    $scope.data.ProjectCostDistributionTemp[i].Name = $scope.data.ProjectCostDistributionTemp[i].ProjectName;
                    $scope.data.ProjectCostDistributions[i].Name = $scope.data.ProjectCostDistributionTemp[i].ProjectName;
                }
                $scope.data.Travelers = $scope.TravelGroupRequest.Travelers;
                $scope.data.GroupBudgets = $scope.TravelGroupRequest.GroupBudgets;
                $scope.data.Travelers[0].RequestNo = $scope.document.RequestNo;
                $scope.data.PaymentFlagCaptures = $scope.TravelGroupRequest.PaymentFlagCaptures;
                $scope.data.Estimate = $scope.TravelGroupRequest.TotalAmount;

                $scope.data.PersonalVehicle = $scope.TravelGroupRequest.PersonalVehicle;
                $scope.data.DefaultCurrencyCode = $scope.TravelGroupRequest.DefaultCurrencyCode;
                $scope.CheckPersonalVehicle();
                $scope.GetTravelRequestConfig();
            }

            /* ====== Inital data before load control (Finish)====== */
        }])
        .directive('expand', function () {
            return {
                restrict: 'A',
                controller: ['$scope', function ($scope) {
                    $scope.$on('onExpandAll', function (event, args) {
                        $scope.expanded = args.expanded;
                    });
                }]
            };
        });
})();
