﻿(function () {
    angular.module('ESSMobile')
        .controller('KPIEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$q', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $q, $mdDialog) {
            //$scope.content.Header = $scope.Text['TRAVELEXPENSE'].TRAVEL_DETAIL
            $scope.content.Header = 'KPI Setting'
            $scope.ChildAction.SetData = function () {
            }
            $scope.ChildAction.LoadData = function () {
                console.log("LoadData");
               
            }

            /* ====== wizard form control ====== */
            $scope.wizardFormControl = {
                receiptIsActive: false,
                withoutReceiptIsActive: false
            };
            /* ====== !wizard form control ====== */


            /* ====== Inital data before load control (Start)====== 
                AddBy: Ratchatawan W. (30 Mar 2017)
                Description: For initial data before load all control in this page
            */



            $scope.init = function () {
                //var promise_service0 = $scope.getTextByCompany($scope.document.Requestor);
                //var promise_service1 = InitialConfig();
                //var promise_service2 = $scope.GetTravelModel();
                //var promise_service3 = $scope.GetMasterdata();
                //$q.all([promise_service0, promise_service1, promise_service2, promise_service3]).then(function (response) {

                //    $scope.data.model.ready = true;
                //    console.debug('init success')
                //}, function (response) {

                //    console.error('init error');

                //});

            }
            

            function InitialConfig() {
                $scope.setBlockAction(true, $scope.Text["SYSTEM"]["PROCESSING"]);
                $scope.Page = "TravelExpense";
                console.log('InitialConfig');

            }
            /* ====== Inital data before load control (Finish)====== */

            // insert KPIEditor start
            $scope.data = {
                selectedKPI: [],
            }

            $scope.column_header = [
               'No.', 'ชนิด', 'ชื่อเป้าประสงค์', 'ตัวชี้วัดที่เชื่อมโยง', 'ชื่อตัวชี้วัด', 'น้ำหนัก(%)', 'หน่วยวัด', 'รูปแบบการคำนวน', 'ความถี่ในการประเมินผล', ' ประเภทตัวชี้วัด', 'Min(1)-TH', '(2)-TH', 'Mid(3)-TH', 'Target(4)-TH', 'Max(5)-TH', 'คำอธิบายตัวชี้วัด'
            ];
            $scope.master = {
                type: [
                    {
                        id:0,
                        text:'Indiv'
                    },
                    {
                        id: 1,
                        text: 'Department'
                    }
                ],
                refKPI: [
                    {
                        id: 0,
                        text: 'Other Function Works-TH'
                    }
                ],
                unitCal: [
                    {
                        id:0,
                        text:'Ratio-TH'
                    },
                    {
                        id: 1,
                        text: '%-TH'
                    }
                ],
                typeCal: [
                    {
                        id: 0,
                        text:'ค่าล่าสุด'
                    },
                    {
                        id: 1,
                        text: 'ค่าเฉลี่ย'
                    }
                ],
                freqEst: [
                    {
                        id: 0,
                        text: 'ทุกปี'
                    },
                    {
                        id: 1,
                        text: 'ทุกเดือน'
                    }
                ],
                indexingType: [
                    {
                        id: 0,
                        text: '5-ระดับ'
                    },
                    {
                        id: 1,
                        text: '3-ระดับ'
                    },
                    {
                        id: 2,
                        text: 'ใช่/ไม่'
                    }

                ]

            };
            // dialog KPI Setting
            $scope.selectKPI = function (ev) {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: 'views/common/dialog/dialog1.tmpl.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
              
                    locals: {
                        params: {
                            title: 'KPI content',
                            KPIDataCheckList: [
                                {
                                    title: 'เป้าประสงค์ของตัวชี้วัด 2018',
                                    isExpanding: false,
                                    sub: [
                                        {
                                            id:0,
                                            title: '1 Corporate KPIs',
                                            isExpanding: false,
                                            sub: [
                                                {
                                                    groupId: 0,
                                                    groupTitle: '1 Corporate KPIs',
                                                    id:'1.1',
                                                    title: '1.1 กรุณาระยยชื่อตัวชี้วัดที่ต้องการ',
                                                },
                                                {
                                                    groupId: 0,
                                                    groupTitle: '1 Corporate KPIs',
                                                    id:'1.2',
                                                    title: '1.2 กรุณาระยยชื่อตัวชี้วัดที่ต้องการ',
                                                },
                                                {
                                                    groupId: 0,
                                                    groupTitle: '1 Corporate KPIs',
                                                    id:'1.3',
                                                    title: '1.3 กรุณาระยยชื่อตัวชี้วัดที่ต้องการ',
                                                }
                                            ]
                                        },
                                        {
                                            id: 1,
                                            title: '2 Stakeholder Excellence',
                                            isExpanding: false,
                                            sub: [
                                                {
                                                    groupId: 1,
                                                    groupTitle: '2 Stakeholder Excellence',
                                                    id:'2.1',
                                                    title: '2.1 กรุณาระยยชื่อตัวชี้วัดที่ต้องการ',
                                                },
                                                {
                                                    groupId: 1,
                                                    groupTitle: '2 Stakeholder Excellence',
                                                    id: '2.2',
                                                    title: '2.2 กรุณาระยยชื่อตัวชี้วัดที่ต้องการ',
                                                },
                                                {
                                                    groupId: 1,
                                                    groupTitle: '2 Stakeholder Excellence',
                                                    id: '2.3',
                                                    title: '2.3 กรุณาระยยชื่อตัวชี้วัดที่ต้องการ',
                                                }
                                            ]
                                        },
                                        {
                                            id: 2,
                                            title: '3 Functional KPIs',
                                            isExpanding: false,
                                            sub: [
                                                {
                                                    groupId: 2,
                                                    groupTitle: '3 Functional KPIs',
                                                    id: '3.1',
                                                    title: '3.1 กรุณาระยยชื่อตัวชี้วัดที่ต้องการ',
                                                },
                                                {
                                                    groupId: 2,
                                                    groupTitle: '3 Functional KPIs',
                                                    id: '3.2',
                                                    title: '3.2 กรุณาระยยชื่อตัวชี้วัดที่ต้องการ',
                                                },
                                                {
                                                    groupId: 2,
                                                    groupTitle: '3 Functional KPIs',
                                                    id: '3.3',
                                                    title: '3.3 กรุณาระยยชื่อตัวชี้วัดที่ต้องการ',
                                                }
                                            ]
                                        },
                                        {
                                            id: 3,
                                            title: '4 QSHE $ HPO Complianced',
                                            isExpanding: false,
                                            sub: [
                                                {
                                                    groupId: 3,
                                                    groupTitle: '4 QSHE $ HPO Complianced',
                                                    id: '4.1',
                                                    title: '4.1 กรุณาระยยชื่อตัวชี้วัดที่ต้องการ',
                                                },
                                                {
                                                    groupId: 3,
                                                    groupTitle: '4 QSHE $ HPO Complianced',
                                                    id: '4.2',
                                                    title: '4.2 กรุณาระยยชื่อตัวชี้วัดที่ต้องการ',
                                                },
                                                {
                                                    groupId: 3,
                                                    groupTitle: '4 QSHE $ HPO Complianced',
                                                    id: '4.3',
                                                    title: '4.3 กรุณาระยยชื่อตัวชี้วัดที่ต้องการ',
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    },
                })
                .then(function (selectedKPI) {
                    $scope.data.selectedKPI = angular.copy(selectedKPI);
                    console.log($scope.data.selectedKPI);
                }, function () {
                   
                });

            }

            function DialogController($scope, $mdDialog, params) { // start controller
                $scope.params = params;
                $scope.selectedList = [];
                $scope.hide = function () {
                    $mdDialog.hide();
                };

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.answer = function (answer) {
                    $mdDialog.hide(answer);
                };

                $scope.selectGroupKPI = function (items) {
                    var foundInList = false;
                    var count = 0;
                    for (var i = 0; i < items.length; i++) {
                        if (isSelected(items[i])) {
                            foundInList = true;
                            count++;
                        } 
                    }
                    if (count < items.length) {
                        for (var i = 0; i < items.length; i++) {
                            if (!isSelected(items[i])) {
                                $scope.selectedList.push(items[i]);
                            }
                        }
                    } else {
                        for (var i = 0; i < items.length; i++) {
                            var idx = $scope.selectedList.indexOf(items[i]);
                            if (idx > -1) {
                                $scope.selectedList.splice(idx, 1);
                            }
                        }
                    }
                }
                $scope.selectGroupsKPI = function (items) {
                    var count = 0;
                    var length = 0;
                    for (var i = 0; i < items.length; i++) {
                        for (var a = 0; a < items[i].sub.length; a++) {
                            if (isSelected(items[i].sub[a])) {
                                count++;
                            }
                            length++;
                        }
                    }

                    if (count < length) {
                        for (var i = 0; i < items.length; i++) {
                            for (var a = 0; a < items[i].sub.length; a++) {
                                if (!isSelected(items[i].sub[a])) {
                                    $scope.selectedList.push(items[i].sub[a]);
                                }
                            }
                        }
                    } else {
                        for (var i = 0; i < items.length; i++) {
                            for (var a = 0; a < items[i].sub.length; a++) {
                                var idx = $scope.selectedList.indexOf(items[i].sub[a]);
                                if (idx > -1) {
                                    $scope.selectedList.splice(idx, 1);
                                }
                            }
                           
                        }
                    }

                }

                $scope.selectKPI = function (item) {
                    var idx = $scope.selectedList.indexOf(item);
                    if (idx > -1) {
                        $scope.selectedList.splice(idx, 1);
                    }
                    else {
                        $scope.selectedList.push(item);
                    }
                }
                $scope.exists = function (item) {
                    return $scope.selectedList.indexOf(item) > -1;
                };
                $scope.exists_group = function (items) {
                    var count = 0;
                    for (var i = 0; i < items.length; i++) {
                        if (isSelected(items[i])) {
                            count++;
                        }
                    }
                    return count == items.length;
                }
                $scope.exists_groups = function (items) {
                    var count = 0;
                    var length = 0;
                    for (var i = 0; i < items.length; i++) {
                        for (var a = 0; a < items[i].sub.length; a++) {
                            if (isSelected(items[i].sub[a])) {
                                count++;
                            }
                            length++;
                        }
                    }
                    return count == length;
                }
                $scope.isIndeterminate = function (items) {
                    var count = 0;
                    for (var i = 0; i < items.length; i++) {
                        if (isSelected(items[i])) {
                            count++;
                        }
                    }
                    return count < items.length && count != 0;
                }
                $scope.isIndeterminateGroupKPI = function (items) {
                    var count = 0;
                    var length = 0;
                    for (var i = 0; i < items.length; i++) {
                        for (var a = 0; a < items[i].sub.length; a++) {
                            if (isSelected(items[i].sub[a])) {
                                count++;
                            }
                            length++;
                        }
                    }
                    return count < length && count != 0;
                }

                function isSelected(item) {
                    for (var i = 0; i < $scope.selectedList.length; i++) {
                        if (item.id === $scope.selectedList[i].id) {
                            return true;
                        }
                    }
                    return false;
                }  
            } // end controller

            // insert KPIEditor end

        }])
        .directive('expand', function () {
            return {
                restrict: 'A',
                controller: ['$scope', function ($scope) {
                    $scope.$on('onExpandAll', function (event, args) {
                        $scope.expanded = args.expanded;
                    });
                }]
            };z
        });
})();
