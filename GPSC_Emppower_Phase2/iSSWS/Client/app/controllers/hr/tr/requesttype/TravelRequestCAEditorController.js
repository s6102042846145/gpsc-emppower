﻿(function () {
    angular.module('ESSMobile')
        .controller('TravelRequestCAEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$q', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $q) {
          
            $scope.content.Header = $scope.Text['TRAVELEXPENSE'].REQUESTWITHBORROW_;
            $scope.ChildAction.SetData = function () {
                console.log("SetData");
                console.log($scope.document.Additional);
            }
            $scope.ChildAction.LoadData = function () {
                console.log("LoadData");
                if ($scope.data.GroupBudgets && $scope.data.GroupBudgets.length) {
                    for (var i = 0; i < $scope.data.GroupBudgets.length; i++) {
                        /*if (
                            !$scope.data.GroupBudgets[i]
                            || !$scope.data.GroupBudgets[i].ExpenseTypeGroupID
                            || !$scope.data.GroupBudgets[i].CostCenter
                            || !$scope.data.GroupBudgets[i].IO
                            || !$scope.data.GroupBudgets[i].AlternativeIOOrg
                            )
                        {
                            $scope.data.GroupBudgets.splice(i, 1);
                            $scope.data.Travelers[0].oCashAdvance.GroupCashAdvances.splice(i, 1);
                        }*/
                        if (!$scope.data.GroupBudgets[i] || !$scope.data.GroupBudgets[i].ExpenseTypeGroupID) {
                            $scope.data.GroupBudgets[i].ExpenseTypeGroupID = 0;
                            $scope.data.Travelers[0].oCashAdvance.GroupCashAdvances[i].ExpenseTypeGroupID = 0;
                        }
                        if (!$scope.data.GroupBudgets[i] || !$scope.data.GroupBudgets[i].CostCenter) {
                            $scope.data.GroupBudgets[i].CostCenter = '0';
                        }
                        if (!$scope.data.GroupBudgets[i] || !$scope.data.GroupBudgets[i].AlternativeIOOrg) {
                            $scope.data.GroupBudgets[i].AlternativeIOOrg = '0';
                        }
                        if (!$scope.data.GroupBudgets[i] || !$scope.data.GroupBudgets[i].IO) {
                            $scope.data.GroupBudgets[i].IO = '0';
                        }
                        if (!$scope.data.GroupBudgets[i] || !$scope.data.GroupBudgets[i].TotalAmount) {
                            $scope.data.GroupBudgets[i].TotalAmount = 0;
                            $scope.data.Travelers[0].oCashAdvance.GroupCashAdvances[i].Amount = 0;
                            $scope.data.Travelers[0].oCashAdvance.GroupCashAdvances[i].TotalAmount = 0;
                        }
                    }
                }
                // $scope.TravelGroupRequest.RequestNo = $scope.document.RequestNo;
                $scope.TravelGroupRequest.TravelMode = ($scope.TravelGroupRequest.GroupSelect) ? "G" : "P";
                $scope.TravelGroupRequest.ExchangeRateCaptures = $scope.data.ExchangeRateCaptures;
                $scope.TravelGroupRequest.TravelSchedulePlaces = $scope.data.TravelSchedulePlaces;
                $scope.TravelGroupRequest.ProjectCostDistributions = $scope.data.ProjectCostDistributions;
                $scope.TravelGroupRequest.Travelers = $scope.data.Travelers;
                $scope.TravelGroupRequest.GroupBudgets = $scope.data.GroupBudgets;
                $scope.TravelGroupRequest.PersonalVehicle = $scope.data.PersonalVehicle; // ($scope.data.PersonalVehicleSelect == true) ? 1 : 2;
                $scope.TravelGroupRequest.DefaultCurrencyCode = $scope.data.DefaultCurrencyCode;
                $scope.TravelGroupRequest.Travelers[0].oCashAdvance.PaymentFlagCaptures = $scope.data.PaymentFlagCaptures;
                $scope.TravelGroupRequest.Travelers[0].oCashAdvance.TotalAmount = $scope.data.TotalAmount;
                $scope.TravelGroupRequest.Travelers[0].oCashAdvance.ExchangeRateCaptures = $scope.data.ExchangeRateCaptures;
                $scope.TravelGroupRequest.TotalAmount = $scope.data.Estimate;
                $scope.CheckUploadFiles();
                $scope.document.Additional = $scope.TravelGroupRequest;

               
            }
          
           
            /* ====== wizard form control ====== */
            $scope.wizardFormControl = {
                receiptIsActive: false,
                withoutReceiptIsActive: false
            };

            /* ====== Inital data before load control (Start)====== 
               AddBy: Ratchatawan W. (30 Mar 2017)
               Description: For initial data before load all control in this page
           */
           
            $scope.init = function () {
                var promise_service0 = $scope.getTextByCompany($scope.document.Requestor);
                var promise_service1 = InitialConfig();
                var promise_service3 = $scope.GetReversalReason();
                var promise_service4 = $scope.GetExpenseRateTypeForLookup();
                var promise_service5 = $scope.GetTravelModel();
                var promise_service6 = $scope.GetMasterdata();
                $q.all([promise_service0,promise_service1, promise_service3, promise_service4, promise_service5, promise_service6]).then(function (response) {

                    $scope.data.model.ready = true;
                    console.debug('init success')
                }, function (response) {

                    console.error('init error');

                });

            }

            function InitialConfig() {
                $scope.Page = "TravelExpense";
                console.log('InitialConfig');
                $scope.data.Readonly = false;
                $scope.data.TravelSeqEdit = false;
                $scope.data.IsExchangeRateEdit = false;
                $scope.data.IsTravelRequest = true;
                $scope.data.ExchangeRateCaptures = $scope.TravelGroupRequest.ExchangeRateCaptures;
                $scope.data.TravelSchedulePlaces = $scope.TravelGroupRequest.TravelSchedulePlaces;
                $scope.data.ProjectCostDistributions = $scope.TravelGroupRequest.ProjectCostDistributions;
                $scope.data.ProjectCostDistributionTemp = angular.copy($scope.TravelGroupRequest.ProjectCostDistributions);
                $scope.data.Travelers = $scope.TravelGroupRequest.Travelers;
                $scope.data.GroupBudgets = $scope.TravelGroupRequest.GroupBudgets;
                $scope.data.Travelers[0].RequestNo = $scope.document.RequestNo;
                $scope.data.PaymentFlagCaptures = $scope.data.Travelers[0].oCashAdvance.PaymentFlagCaptures;
                $scope.data.Estimate = $scope.TravelGroupRequest.TotalAmount;
                $scope.data.PersonalVehicle = $scope.TravelGroupRequest.PersonalVehicle;
                $scope.data.DefaultCurrencyCode = $scope.TravelGroupRequest.DefaultCurrencyCode;
                $scope.CheckPersonalVehicle();
                if (!$scope.document.Additional.TravelGroupRequest[0].Travelers[0].oCashAdvance.IsOverridePostingDate) {
                    $scope.document.Additional.Travelers[0].oCashAdvance.PostingDate = $filter('date')(new Date(), 'yyyy-MM-ddT00:00:00');
                }

                if (!$scope.document.Additional.TravelGroupRequest[0].Travelers[0].oCashAdvance.IsOverrideBaseLineDate) {
                    $scope.document.Additional.Travelers[0].oCashAdvance.BaseLineDate = $filter('date')(new Date(), 'yyyy-MM-ddT00:00:00');
                }
                $scope.GetTravelRequestConfig();
            }


       
            /* ====== Inital data before load control (Finish)====== */
            /* ====== !wizard form control ====== */
        }])
        .directive('expand', function () {
            return {
                restrict: 'A',
                controller: ['$scope', function ($scope) {
                    $scope.$on('onExpandAll', function (event, args) {
                        $scope.expanded = args.expanded;
                    });
                }]
            };
        });
})();
