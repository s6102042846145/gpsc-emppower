﻿(function () {
    angular.module('ESSMobile')
        .controller('CancelTravelGroupViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {
            $scope.IsRemarkEdit = false;
            $scope.TRAVELMODE = "G";
            $scope.referRequestNo = '';
            //$scope.TravelGroupRequest = $scope.document.Additional;
            $scope.data = {};
            $scope.data.Readonly = true;
            $scope.Textcategory = 'TRAVELREQUEST';
            $scope.content.Header = $scope.Text[$scope.Textcategory].TITLE;
            $scope.settings = {
                ExpenseTypeGroupInfo: {
                    isDomestic: ''
                },
                Master: []
            };

            $scope.ChildAction.SetData = function () {
                $scope.referRequestNo = $scope.document.Additional.TravelRequestNo;
                $scope.TravelGroupCancel = $scope.document.Additional;
                $scope.TravelGroupRequest = $scope.TravelGroupCancel.TravelGroupRequest;
                $scope.data.Remark = $scope.TravelGroupCancel.Remark;
                $scope.data.Travelers = $scope.TravelGroupCancel.TravelGroupRequest.Travelers;
                //$scope.data.CashAdvance = $scope.data.Travelers[findWithAttr($scope.data.Travelers, 'EmployeeID', $scope.TravelGroupCancel.TravelGroupRequest.GroupManager)].oCashAdvance;
                $scope.data.Travelers = $scope.TravelGroupCancel.TravelGroupRequest.Travelers;
                $scope.data.CashAdvance = $scope.data.Travelers[findWithAttr($scope.data.Travelers, 'EmployeeID', $scope.TravelGroupCancel.TravelGroupRequest.GroupManager)].oCashAdvance;
                if ($scope.data.CashAdvance != null && $scope.data.CashAdvance.TotalAmount > 0) {
                    $scope.data.CashAdvanceReturns = [];
                    $scope.data.CashAdvanceReturns = $scope.TravelGroupCancel.CashAdvanceReturns;
                }
                else {
                    $scope.data.CashAdvance = null;
                }

                

                $scope.isDomesticTravel = function (travelReport) {
                    /* return : 'D' = Domestic, 'O' = Oversea, 'B' = Both domestic and oversea */
                    var oTravelSchedules = travelReport.TravelSchedulePlaces;
                    var personalCountryCode = $scope.employeeData.AreaSetting.CountryCode;
                    var countDomestic = 0;
                    var countOversea = 0;
                    for (var i = 0; i < oTravelSchedules.length; i++) {
                        if (personalCountryCode == oTravelSchedules[i].CountryCode) {
                            countDomestic++;
                        } else {
                            countOversea++;
                        }
                    }
                    if (countDomestic == 0) {
                        return 'O';
                    } else if (countOversea == 0) {
                        return 'D';
                    }
                    return 'B';
                };
                $scope.settings.ExpenseTypeGroupInfo.isDomestic = $scope.isDomesticTravel($scope.TravelGroupRequest);

            };

            $scope.ChildAction.LoadData = function () {
                $scope.document.Additional = $scope.TravelGroupCancel;
                //$scope.TravelGroupCancel.RequestNo = $scope.document.RequestNo;
                //$scope.TravelGroupCancel.TravelRequestNo = $scope.referRequestNo;
                //$scope.TravelGroupCancel.Status = '';
                //$scope.TravelGroupCancel.Remark = $scope.TravelGroupCancel.NewGroupManager.EmployeeID;
                //$scope.TravelGroupCancel.Remark
                //console.log('TravelGroupCancel : ', $scope.TravelGroupCancel);
            };

            function findWithAttr(array, attr, value) {
                for (var i = 0; i < array.length; i += 1) {
                    if (array[i][attr] === value) {
                        return i;
                    }
                }
            }
            $scope.ChildAction.SetData();
            //Get CostCenter
            var URL = CONFIG.SERVER + 'HRTR/GetCostCenter';
            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                if (!$scope.settings.Master) $scope.settings.Master = [];
                $scope.settings.Master.CostCenterList = response.data;
                //$scope.objCostcenter = response.data;
                //console.log('objCostcenter', $scope.objCostcenter);
            }, function errorCallback(response) {
                console.log('error GeneralExpenseEditorController GetCostcenter.', response);
            });

            // Get Organization
            var URL = CONFIG.SERVER + 'HRTR/GetOrganization';
            var oRequestParameter = { InputParameter: { "CreateDate": $scope.document.CreatedDate }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                if (!$scope.settings.Master) $scope.settings.Master = [];
                $scope.settings.Master.OrgUnitList = response.data;
                //$scope.objOrganization = response.data;
            }, function errorCallback(response) {
                console.log('error TravelExpenseGroupEstimateEditorController objOrganization.', response);
            });

            // Get InternalOrder All
            var URL = CONFIG.SERVER + 'HRTR/GetInternalOrderAll/';
            var oRequestParameter = { InputParameter: '', CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                if (!$scope.settings.Master) $scope.settings.Master = [];
                $scope.settings.Master.IOList = response.data;
                //var temp = response.data;
                //$scope.allIO = temp;
            }, function errorCallback(response) {
                console.log('error GeneralExpenseEditorController GetInternalOrderAll.', response);
            });
            
        }]);
})();