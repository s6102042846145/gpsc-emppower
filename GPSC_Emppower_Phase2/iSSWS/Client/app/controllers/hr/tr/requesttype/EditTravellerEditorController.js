﻿(function () {
    angular.module('ESSMobile')
        .controller('EditTravellerEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$q', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $q) {
            $scope.ChildAction.SetData = function () {
                console.log("SetData");
                console.log($scope.document.Additional);
            }
            $scope.ChildAction.LoadData = function () {
                if ($scope.data.GroupBudgets && $scope.data.GroupBudgets.length) {
                    for (var i = 0; i < $scope.data.GroupBudgets.length; i++) {
                        if (!$scope.data.GroupBudgets[i] || !$scope.data.GroupBudgets[i].ExpenseTypeGroupID) {
                            $scope.data.GroupBudgets[i].ExpenseTypeGroupID = 0;
                            $scope.data.Travelers[0].oCashAdvance.GroupCashAdvances[i].ExpenseTypeGroupID = 0;
                        }
                        if (!$scope.data.GroupBudgets[i] || !$scope.data.GroupBudgets[i].CostCenter) {
                            $scope.data.GroupBudgets[i].CostCenter = '0';
                        }
                        if (!$scope.data.GroupBudgets[i] || !$scope.data.GroupBudgets[i].AlternativeIOOrg) {
                            $scope.data.GroupBudgets[i].AlternativeIOOrg = '0';
                        }
                        if (!$scope.data.GroupBudgets[i] || !$scope.data.GroupBudgets[i].IO) {
                            $scope.data.GroupBudgets[i].IO = '0';
                        }
                    }
                }

                $scope.TravelGroupRequest.Travelers[0].oCashAdvance.TotalAmount = $scope.data.TotalAmount;
                $scope.TravelGroupRequest.TotalAmount = $scope.data.Estimate;
                $scope.CheckUploadFiles();

                $scope.TravelPersonalEdit.RequestNo = $scope.document.RequestNo;
                $scope.TravelPersonalEdit.TravelRequestNo = $scope.TravelGroupRequest.RequestNo;
                $scope.TravelPersonalEdit.Remark = $scope.data.Remark;
                $scope.TravelPersonalEdit.EmployeeID = $scope.document.Requestor.EmployeeID;
                $scope.TravelPersonalEdit.TravelGroupRequest = $scope.TravelGroupRequest;
                $scope.TravelPersonalEdit.BeginDate = $scope.TravelGroupRequest.BeginDate;
                $scope.TravelPersonalEdit.EndDate = $scope.TravelGroupRequest.EndDate;
                $scope.document.Additional = $scope.TravelPersonalEdit;
            }

            $scope.init = function () {
                var promise_service1 = InitialConfig();
                var promise_service2 = $scope.GetTravelModel();
                var promise_service3 = $scope.GetMasterdata();
                $q.all([promise_service1, promise_service2, promise_service3]).then(function (response) {

                    $scope.data.model.ready = true;
                    console.debug('init success')
                }, function (response) {

                    console.error('init error');

                });

            }

            function InitialConfig() {
                $scope.Page = "TravelExpense";
                //for ExchangeRateControl
                $scope.data.Readonly = false;
                $scope.data.TravelSeqEdit = false;
                $scope.data.IsExchangeRateEdit = false;
                $scope.data.IsTravelRequest = true;
                $scope.data.IsEditTraller = true;
                $scope.data.Remark = $scope.TravelPersonalEdit.Remark;
                $scope.data.IsRemarkEdit = (!$scope.document.CurrentFlowItemCode || $scope.document.CurrentFlowItemCode == "DRAFT" || $scope.document.CurrentFlowItemCode == "WAIT_FOR_EDIT");
                $scope.data.IsEditTraller = !$scope.data.IsRemarkEdit;
                GetLastedManager($scope.TravelPersonalEdit.TravelRequestNo, $scope.TravelPersonalEdit.TravelGroupRequest.GroupManager);
                BindData($scope.TravelPersonalEdit.TravelGroupRequest);
                $scope.TravelPersonalEdit.CanEdit = !$scope.data.IsRemarkEdit;//true;
                $scope.data.Readonly = true;
            }


            function GetLastedManager(requestNo, groupManager) {
                var URL = CONFIG.SERVER + 'HRTR/GetLastedManagerByRequestNo';
                var oRequestParameter = { InputParameter: { "REQUESTNO": requestNo, "GROUPMANAGER": groupManager }, CurrentEmployee: getToken(CONFIG.USER), Requestor: $scope.document.Requestor, Creator: $scope.document.Creator }
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    //ถ้ามีสิทธิ์ในการอนุมัติตัวเอง
                    if (response.data) {
                        $scope.data.IsEditTraller = !$scope.data.IsRemarkEdit;
                    }
                    else {
                        $scope.data.IsEditTraller = true;
                    }
                    console.log('success GetLastedManagerByRequestNo.', $scope.data.IsEditTraller);

                }, function errorCallback(response) {
                    // Error
                    console.log('error NotCheckLineManager.', response);
                });

            }

            function BindData(oTgrData) {
                $scope.TravelGroupRequest = oTgrData;
                //$scope.GroupSelectBuffer = $scope.TravelGroupRequest.GroupSelect;
                //$scope.TravelGroupRequest.GroupSelect = false;
                $scope.data.ExchangeRateCaptures = $scope.TravelGroupRequest.ExchangeRateCaptures;
                $scope.data.TravelSchedulePlaces = $scope.TravelGroupRequest.TravelSchedulePlaces;
                $scope.data.ProjectCostDistributions = $scope.TravelGroupRequest.ProjectCostDistributions;
                $scope.data.ProjectCostDistributionTemp = angular.copy($scope.TravelGroupRequest.ProjectCostDistributions);
                $scope.data.DefaultTransportationTypeCode = $scope.TravelGroupRequest.Travelers[0].TransportationTypeCode;
                $scope.data.DefaultTransportationTypeName = $scope.TravelGroupRequest.Travelers[0].TransportationTypeName;
                $scope.data.DefaultTransportationTypeRemark = $scope.TravelGroupRequest.Travelers[0].TransportationTypeRemark;
                // No default First Traveller wait for Country Type
                //$scope.TravelGroupRequest.Travelers[0].TransportationTypeCode = '';
                //$scope.TravelGroupRequest.Travelers[0].TransportationTypeName = '';
                $scope.data.Travelers = $scope.TravelGroupRequest.Travelers;
                $scope.data.GroupBudgets = $scope.TravelGroupRequest.GroupBudgets;
                $scope.data.Estimate = $scope.TravelGroupRequest.TotalAmount;

                $scope.data.PersonalVehicleSelect = $scope.TravelGroupRequest.PersonalVehicleSelect;
                $scope.TravelGroupRequest.DefaultCurrencyCode = $scope.TravelPersonalEdit.DefaultCurrencyCode;
                $scope.data.DefaultCurrencyCode = $scope.TravelPersonalEdit.DefaultCurrencyCode;
                $scope.data.PaymentFlagCaptures = $scope.TravelPersonalEdit.PaymentFlagCaptures;

                //Add New Remark by Nipon Supap 20-02-2017 
                //$scope.data.Remark = $scope.TravelGroupRequest.Remark;
                if (angular.isDefined($scope.data.Travelers)) {
                    $scope.data.NewGroupManagerDataSelector = angular.copy($scope.TravelPersonalEdit.TravelGroupRequestHistory.Travelers);
                    if ($scope.TravelGroupRequest.GroupManager == $scope.document.Requestor.EmployeeID && $scope.TravelPersonalEdit.TravelGroupRequestHistory.Travelers.length > 1) {
                        $scope.data.IsGroupManager = true;
                    }
                    for (i = 0; i < $scope.data.NewGroupManagerDataSelector.length; i++) {
                        if ($scope.data.NewGroupManagerDataSelector[i].EmployeeID == $scope.TravelGroupRequest.GroupManager) {
                            $scope.data.NewGroupManagerDataSelector.splice(i, 1);
                        }
                    }
                }
                $scope.GetTravelRequestConfig($scope.TravelGroupRequest);
                //$scope.CheckPersonalVehicle();
                //$scope.EvenGetExpenseTypeByTag();
            }

            /* ====== wizard form control ====== */
            $scope.wizardFormControl = {
                receiptIsActive: false,
                withoutReceiptIsActive: false
            };
            /* ====== !wizard form control ====== */
        }])
})();