﻿(function () {
    angular.module('ESSMobile')
        .controller('PCRequestEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {

            $scope.employeeData = getToken(CONFIG.USER);
            $scope.TravelGroupRequest = $scope.document.Additional;
            $scope.data = {};
            $scope.data.IsPersonalCashAdvance = true;
            $scope.data.Readonly = false;
            $scope.data.TotalAmount = 0;
            $scope.data.PettyCode = '';
            $scope.data.PettyName = '';
            $scope.data.EnableIsCustodian = 0;
            $scope.data.PettyCashAmount = 0;
            $scope.Textcategory = 'TRAVELREQUEST';
            $scope.content.Header = $scope.Text[$scope.Textcategory].TITLE;
            $scope.settings = {
                ExpenseTypeGroupInfo: {
                    isDomestic: '',
                    Master: []
                }
            };
            $scope.InitialConfig = function () {
                //$scope.LoadPaymentFlag();
                $scope.data.Travelers = $scope.TravelGroupRequest.Travelers;
                $scope.data.PaymentFlagCaptures = $scope.data.Travelers[0].oPettyCash.PaymentFlagCaptures;
                //$scope.data.PaymentFlagCaptures = $scope.TravelGroupRequest.PaymentFlagCaptures;
                $scope.LoadConfig();

                var getExpenseRateTypeForLookup = function () {
                    var URL = CONFIG.SERVER + 'HRTR/GetExpenseRateTypeForLookup';
                    var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        if (angular.isUndefined($scope.masterData)) {
                            $scope.masterData = {};
                        }
                        $scope.masterData.objExpenseRateTypeForLookup = response.data;
                    }, function errorCallback(response) {
                        console.log('error getExpenseRateTypeForLookup.', response);
                    });
                };
                getExpenseRateTypeForLookup();

            };

            $scope.LoadConfig = function () {
                var URL = CONFIG.SERVER + 'HRTR/GetModuleSetting';
                var oRequestParameter = { InputParameter: { "KEY": 'CashAdvanceMode' }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.data.CAMode = response.data;

                }, function errorCallback(response) {
                    console.log('error LoadConfig GetModuleSetting.', response);
                });
            }

            $scope.InitPettyCash = function () {
                if (!$scope.document.NewRequest) {
                    $scope.TravelGroupRequest = angular.copy($scope.document.Additional.TravelGroupRequest[0]);
                    $scope.data.Travelers = $scope.TravelGroupRequest.Travelers;
                    $scope.data.PaymentFlagCaptures = $scope.data.Travelers[0].oPettyCash.PaymentFlagCaptures;
                    $scope.data.PettyCode = $scope.data.Travelers[0].oPettyCash.PettyCode;
                    $scope.data.PettyName = $scope.data.Travelers[0].oPettyCash.PettyName;
                }
                else {
                    var oRequestParameter = {
                        InputParameter: { "referRequestNo": $scope.referRequestNo, "EmployeeID": $scope.document.RequestorNo, "PositionID": $scope.document.RequestorPosition, "CompanyCode": $scope.document.RequestorCompanyCode }
                                                    , CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor
                    };
                    var URL = CONFIG.SERVER + 'HRTR/InitPettyCash';
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        var DefaultCurrencyCode = $scope.TravelGroupRequest.DefaultCurrencyCode;
                        $scope.TravelGroupRequest = response.data;
                        $scope.data.Travelers = $scope.TravelGroupRequest.Travelers;
                        $scope.data.Travelers[0].oPettyCash.RequestNo = $scope.document.RequestNo;
                        $scope.data.Travelers[0].oPettyCash.TravelRequestNo = $scope.referRequestNo;
                        $scope.data.Travelers[0].oPettyCash.EmployeeID = $scope.document.RequestorNo;
                        $scope.data.PaymentFlagCaptures = $scope.data.Travelers[0].oPettyCash.PaymentFlagCaptures;
                        console.log('InitPettyCash', response.data);
                    }, function errorCallback(response) {
                        // Error
                        console.log('error InitPettyCash.', response);
                    });
                }


            };

            $scope.ChildAction.LoadData = function () {

                $scope.TravelGroupRequest.Travelers = $scope.data.Travelers;
                $scope.TravelGroupRequest.Travelers[0].oPettyCash.PaymentFlagCaptures = $scope.data.PaymentFlagCaptures;
                $scope.TravelGroupRequest.Travelers[0].oPettyCash.PettyCode = $scope.data.PettyCode;
                $scope.TravelGroupRequest.Travelers[0].oPettyCash.PettyName = $scope.data.PettyName;
                $scope.TravelGroupRequest.Travelers[0].EnableIsCustodian = $scope.data.EnableIsCustodian;
                $scope.TravelGroupRequest.Travelers[0].PettyCashAmount = $scope.data.PettyCashAmount;
                $scope.document.Additional = $scope.TravelGroupRequest;
                console.log('TravelGroupRequest', $scope.TravelGroupRequest);
            };


            //if ($scope.document.NewRequest) {
            $scope.InitPettyCash();
            //}
            //else {
            //    //$scope.GetEditTraveler();
            //    $scope.TravelGroupRequest = $scope.document.Additional;
            //}
            

            $scope.settings.Master = [];
            //Get CostCenter
            var URL = CONFIG.SERVER + 'HRTR/GetCostCenter';
            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                $scope.settings.Master.CostCenterList = response.data;
                //$scope.objCostcenter = response.data;
                //console.log('objCostcenter', $scope.objCostcenter);
            }, function errorCallback(response) {
                console.log('error GeneralExpenseEditorController GetCostcenter.', response);
            });

            // Get Organization
            var URL = CONFIG.SERVER + 'HRTR/GetOrganization';
            var oRequestParameter = { InputParameter: { "CreateDate": $scope.document.CreatedDate }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                $scope.settings.Master.OrgUnitList = response.data;
                //$scope.objOrganization = response.data;
            }, function errorCallback(response) {
                console.log('error TravelExpenseGroupEstimateEditorController objOrganization.', response);
            });

            // Get InternalOrder All
            var URL = CONFIG.SERVER + 'HRTR/GetInternalOrderAll/';
            var oRequestParameter = { InputParameter: '', CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                $scope.settings.Master.IOList = response.data;
                //var temp = response.data;
                //$scope.allIO = temp;
            }, function errorCallback(response) {
                console.log('error GeneralExpenseEditorController GetInternalOrderAll.', response);
            });
        }]);
})();