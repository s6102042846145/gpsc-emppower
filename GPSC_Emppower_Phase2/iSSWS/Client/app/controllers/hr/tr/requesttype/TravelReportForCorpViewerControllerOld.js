﻿(function () {
    angular.module('ESSMobile')
        .controller('TravelReportForCorpViewerControllerOld', ['$scope', '$http', '$routeParams', '$location', '$filter', '$window', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, $window, CONFIG) {
            $scope.employeeData = getToken(CONFIG.USER);
            $scope.TRAVELMODE = "P";
            $scope.isDebug = false;
            $scope.referRequestNo = '';
            /* --- Variable --- */

            $scope.initialReady = false;
            $scope.pageState = {
                travelScheduleBusy: false
            };
            $scope.formDataTravelReport = {
                travelSequence: []
            };

            $scope.TravelGroupConfig = {
                DefaultRequestGroupApprove: '',
                GroupMode: '',
                TravelType: '',
                Phone: '',
                MobilePhone: ''
            };

            $scope.model = {
                ready: false,
                data: null
            };



            $scope.settings = {
                ProjectCodeMode: '',
                EnableOverideCostCenter: false,
                EnableOverideIO: false,
                ENABLE_PROJECTCOST: false,
                ExpenseTypeGroupInfo: {
                    prefix: 'TR',
                    travelTypeID: 1,
                    isDomestic: 'D',
                    groupTagName: "",
                    tagNameReceipt: "",
                    tagNameNoReceipt: ""
                },
                MaximumEmpSubGroup: 99,
                ReceiptToleranceAllow: 0
            };



            $scope.masterData = {
                objCostcenterDistribution: null,
                objCostcenter: null,
                objExpenseRateTypeForLookup: null
            };

            /* --- !Variable --- */

            // panel setting

            $scope.Page = CONFIG.PAGE_SETTING.TRAVEL_REPORT;
            $scope.data = {
                ProjectCostDistributions: [],
                ProjectCostDistributionTemp: [],
                TravelSchedulePlaces: null,
                ExchangeRateCaptures: null,
                Travelers: [],
                GroupBudgets: [],
                PerdiumList: [],
                PaymentFlagCaptures: [],
                Readonly: true,
                IsReturnEdit: false,
                IsExchangeRateEdit: false
            };

            $scope.TravelReport = $scope.document.Additional;
            console.log('oooTravelReportooo', $scope.TravelReport);

            // !panel setting
            //NUN ADD Panel ExpenseReportReceipt
            //$scope.getExpenseItemsReceipt = function () {
            //    var oList = [];
            //    if (angular.isDefined($scope.TravelReport.ExpenseReportReceiptList) && $scope.TravelReport.ExpenseReportReceiptList != null) {

            //        var ExpenseReportReceiptList = $scope.TravelReport.ExpenseReportReceiptList;
            //        for (var i = 0; i < ExpenseReportReceiptList.length; i++) {
            //            var receipt = angular.copy(ExpenseReportReceiptList[i]);
            //            if (receipt.IsReceipt && receipt.IsVisible) {
            //                var countReceiptItem = receipt.ExpenseReportReceiptItemList.length;
            //                receipt.IsFirstRow = false;
            //                var sumAllChild = 0;
            //                for (var j = 0; j < countReceiptItem; j++) {
            //                    var receiptItem = receipt.ExpenseReportReceiptItemList[j];
            //                    sumAllChild += receiptItem.ExpenseReportReceiptItemDetailList.length;
            //                }
            //                receipt.ItemCount = sumAllChild;
            //                receipt.ExpenseReportReceiptItemList = null;
            //                receipt.ExpenseReportReceiptFileList = null;


            //                for (var j = 0; j < countReceiptItem; j++) {
            //                    var receiptItem = ExpenseReportReceiptList[i].ExpenseReportReceiptItemList[j];
            //                    var countReceiptItemDetail = receiptItem.ExpenseReportReceiptItemDetailList.length;
            //                    receipt.item = {
            //                        IsFirstRow: false,
            //                        ItemCount: countReceiptItemDetail,
            //                        ItemID: receiptItem.ItemID,
            //                        ExpenseTypeGroupName: receiptItem.ExpenseTypeGroupName,
            //                        ExpenseTypeGroupRemark: receiptItem.ExpenseTypeGroupRemark,
            //                        ExpenseTypeName: receiptItem.ExpenseTypeName
            //                    };

            //                    for (var k = 0; k < countReceiptItemDetail; k++) {
            //                        var tempReceipt = angular.copy(receipt);
            //                        var receiptItemDetail = receiptItem.ExpenseReportReceiptItemDetailList[k];
            //                        if (j == 0 && k == 0) {
            //                            tempReceipt.IsFirstRow = true;
            //                        }
            //                        if (k == 0) {
            //                            tempReceipt.item.IsFirstRow = true;
            //                        }
            //                        tempReceipt.itemDetail = {
            //                            CostCenter: receiptItemDetail.CostCenter,
            //                            IO: receiptItemDetail.IO,
            //                            OriginalAmount: receiptItemDetail.OriginalAmount
            //                        };
            //                        oList.push(tempReceipt);
            //                    }
            //                }
            //            }
            //        }

            //    }
            //    return oList;
            //};
            //$scope.expenseItemsReceipt = $scope.getExpenseItemsReceipt();

            //$scope.getExpenseItemsWithoutReceipt = function () {
            //    var oList = [];
            //    if (angular.isDefined($scope.TravelReport.ExpenseReportReceiptList) && $scope.TravelReport.ExpenseReportReceiptList != null) {

            //        var ExpenseReportReceiptList = $scope.ExpenseReport.ExpenseReportReceiptList;
            //        for (var i = 0; i < ExpenseReportReceiptList.length; i++) {
            //            var receipt = angular.copy(ExpenseReportReceiptList[i]);
            //            if (!receipt.IsReceipt && receipt.IsVisible) {
            //                var countReceiptItem = receipt.ExpenseReportReceiptItemList.length;
            //                receipt.IsFirstRow = false;
            //                var sumAllChild = 0;
            //                for (var j = 0; j < countReceiptItem; j++) {
            //                    var receiptItem = receipt.ExpenseReportReceiptItemList[j];
            //                    sumAllChild += receiptItem.ExpenseReportReceiptItemDetailList.length;
            //                }
            //                receipt.ItemCount = sumAllChild;
            //                receipt.ExpenseReportReceiptItemList = null;
            //                receipt.ExpenseReportReceiptFileList = null;


            //                for (var j = 0; j < countReceiptItem; j++) {
            //                    var receiptItem = ExpenseReportReceiptList[i].ExpenseReportReceiptItemList[j];
            //                    var countReceiptItemDetail = receiptItem.ExpenseReportReceiptItemDetailList.length;
            //                    receipt.item = {
            //                        IsFirstRow: false,
            //                        ItemCount: countReceiptItemDetail,
            //                        ItemID: receiptItem.ItemID,
            //                        ExpenseTypeGroupName: receiptItem.ExpenseTypeGroupName,
            //                        ExpenseTypeGroupRemark: receiptItem.ExpenseTypeGroupRemark,
            //                        ExpenseTypeName: receiptItem.ExpenseTypeName
            //                    };

            //                    for (var k = 0; k < countReceiptItemDetail; k++) {
            //                        var tempReceipt = angular.copy(receipt);
            //                        var receiptItemDetail = receiptItem.ExpenseReportReceiptItemDetailList[k];
            //                        if (j == 0 && k == 0) {
            //                            tempReceipt.IsFirstRow = true;
            //                        }
            //                        if (k == 0) {
            //                            tempReceipt.item.IsFirstRow = true;
            //                        }
            //                        tempReceipt.itemDetail = {
            //                            CostCenter: receiptItemDetail.CostCenter,
            //                            IO: receiptItemDetail.IO,
            //                            OriginalAmount: receiptItemDetail.OriginalAmount
            //                        };
            //                        oList.push(tempReceipt);
            //                    }
            //                }
            //            }
            //        }

            //    }
            //    return oList;
            //};
            //$scope.expenseItemsWithoutReceipt = $scope.getExpenseItemsWithoutReceipt();

            //$scope.sumVATBaseAmount = function (list, isReceipt) {
            //    var total = 0;
            //    angular.forEach(list, function (item) {
            //        if (item.IsReceipt == isReceipt && item.IsVisible) {
            //            var amount = Number(item.VATBaseAmount);
            //            total += isNaN(amount) ? 0 : amount;
            //        }
            //    });
            //    return total;
            //};

            //$scope.sumVATAmount = function (list, isReceipt) {
            //    var total = 0;
            //    angular.forEach(list, function (item) {
            //        if (item.IsReceipt == isReceipt && item.IsVisible) {
            //            var amount = Number(item.VATAmount);
            //            total += isNaN(amount) ? 0 : amount;
            //        }
            //    });
            //    return total;
            //};

            //function groupTable($rows, startIndex, total) {
            //    if (total === 0) {
            //        return;
            //    }

            //    var i, currentIndex = startIndex, count = 1, lst = [];
            //    var tds = $rows.find('td:eq(' + currentIndex + ')');
            //    var ctrl = $(tds[0]);
            //    lst.push($rows[0]);
            //    for (i = 1; i <= tds.length; i++) {
            //        if (ctrl.text() == $(tds[i]).text()) {
            //            count++;
            //            $(tds[i]).addClass('deleted');
            //            lst.push($rows[i]);
            //        }
            //        else {
            //            if (count > 1) {
            //                ctrl.attr('rowspan', count);
            //                ctrl.attr('style', 'vertical-align:middle');
            //                groupTable($(lst), startIndex + 1, total - 1)
            //            }
            //            count = 1;
            //            lst = [];
            //            ctrl = $(tds[i]);
            //            lst.push($rows[i]);
            //        }
            //    }
            //}

            //$scope.tableReceiptReady = function () {
            //    setTimeout(function () {
            //        groupTable($('#tableReceipt tr:has(td)'), 0, 9);
            //        $('#tableReceipt .deleted').remove();
            //    }, 50);
            //};

            //$scope.tableWithoutReceiptReady = function () {
            //    setTimeout(function () {
            //        groupTable($('#tableWithoutReceipt tr:has(td)'), 0, 8);
            //        $('#tableWithoutReceipt .deleted').remove();
            //    }, 50);
            //};
            //---------------------------------------


            $scope.ChildAction.SetData = function () {
                $scope.referRequestNo = $scope.document.Additional.TravelRequestNo;
                $scope.Textcategory = 'EXPENSE';
                $scope.content.Header = $scope.Text['EXPENSE']['TRAVEL_REPORT'];

                $scope.Textcategory2 = 'TRAVELREQUEST';

                /* Warning from CalculateInfoData */

                if (angular.isDefined($scope.document.Additional.ExpenseInfoData)
                    && angular.isDefined($scope.document.Additional.ExpenseInfoData.IsAccommodationInRight)
                    && angular.isDefined($scope.document.Additional.ExpenseInfoData.AccommodationInRightExceedAmount)
                    && !$scope.document.Additional.ExpenseInfoData.IsAccommodationInRight
                    && $scope.document.Additional.ExpenseInfoData.AccommodationInRightExceedAmount > 0) {
                    $scope.content.isShowDocumentWarning = true;
                    //$scope.content.documentWarningText = $scope.Text['EXPENSE']['WARNING_ACCOMMODATION_WORKFLOW_CHANGE'] + $scope.MathRoundingToString($scope.document.Additional.ExpenseInfoData.AccommodationInRightExceedAmount) + ' ' + $scope.Text['SYSTEM']['BAHT'];
                    //Modify by Nipon Supap 09-05-2017
                    var nAmount = $scope.document.Additional.ExpenseInfoData.AccommodationInRightExceedAmount;
                    var diff = $scope.MathRoundingToString(nAmount);
                    var arrNum = (diff + '').split('.');
                    var sNumINT = new Intl.NumberFormat().format(arrNum[0]);
                    var sNum = sNumINT + '.' + arrNum[1];
                    $scope.content.documentWarningText = $scope.Text['EXPENSE']['WARNING_ACCOMMODATION_WORKFLOW_CHANGE'] + sNum + ' ' + $scope.Text['SYSTEM']['BAHT'];
                }

                /* !Warning from CalculateInfoData */

                $scope.data.ExchangeRateCaptures = $scope.document.Additional.ExchangeRateCaptures;
                $scope.data.TravelSchedulePlaces = $scope.document.Additional.TravelSchedulePlaces;
                $scope.data.ProjectCostDistributions = $scope.document.Additional.ProjectCostDistributions;
                $scope.data.ProjectCostDistributionTemp = angular.copy($scope.document.Additional.ProjectCostDistributions);
                $scope.data.Travelers = $scope.document.Additional.Travelers;
                $scope.data.GroupBudgets = $scope.document.Additional.GroupBudgets;
                $scope.data.PaymentFlagCaptures = $scope.document.Additional.PaymentFlagCaptures;
                $scope.data.PerdiumList = $scope.document.Additional.PerdiumList;
                // CashAdvanceReturn
                $scope.data.CashAdvance = $scope.document.Additional.Travelers[0].oCashAdvance;
                $scope.data.CashAdvanceReturns = $scope.document.Additional.CashAdvanceReturnList;

                /* initial form data */
                $scope.formDataTravelReport.travelSequence = [];
                for (var i = 0; i < $scope.data.TravelSchedulePlaces.length; i++) {
                    var oBeginDate = new Date($scope.data.TravelSchedulePlaces[i].BeginDate);
                    var oEndDate = new Date($scope.data.TravelSchedulePlaces[i].EndDate);
                    var objNewFormData = {
                        startDate: $scope.data.TravelSchedulePlaces[i].BeginDate,
                        startHour: $scope.zeroPaddingText(oBeginDate.getHours(), 2),
                        startMin: $scope.zeroPaddingText(oBeginDate.getMinutes(), 2),
                        endDate: $scope.data.TravelSchedulePlaces[i].EndDate,
                        endHour: $scope.zeroPaddingText(oEndDate.getHours(), 2),
                        endMin: $scope.zeroPaddingText(oEndDate.getMinutes(), 2)
                    };
                    $scope.formDataTravelReport.travelSequence.push(objNewFormData);
                }




                //if ($scope.document.NewRequest) {
                //    $scope.InitTravelReportReference();
                //}
                //else {
                //    $scope.TravelReportReference = $scope.document.Additional;
                //}

                // load settings
                //var getTravelGroupConfig = function () {
                //    var URL = CONFIG.SERVER + 'HRTR/GetTravelRequestConfig';
                //    var oRequestParameter = { InputParameter: { "REQUESTNO": $scope.document.RequestNo }, CurrentEmployee: getToken(CONFIG.USER), Requestor: $scope.document.Requestor, Creator: $scope.document.Creator }
                //    $http({
                //        method: 'POST',
                //        url: URL,
                //        data: oRequestParameter
                //    }).then(function successCallback(response) {
                //        // Success
                //        $scope.TravelGroupConfig.DefaultRequestGroupApprove = response.data['DEFAULTREQUESTGROUPAPPROVE'];
                //        $scope.TravelGroupConfig.GroupMode = response.data['GROUPMODE'];
                //        $scope.TravelGroupConfig.TravelType = response.data['TRAVELTYPE'];

                //    }, function errorCallback(response) {
                //        // Error
                //        console.log('error TravelReport InitialConfig.', response);
                //    });
                //};
                //getTravelGroupConfig();

                var getProjectCodeMode = function () {
                    var URL = CONFIG.SERVER + 'HRTR/GetProjectCodeMode';
                    var oRequestParameter = { InputParameter: { 'OverrideProjectCodeMode': '' }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.settings.ProjectCodeMode = response.data;

                        $scope.document.Additional.ProjectCodeMode = response.data;

                        console.log('GetProjectCodeMode.', response.data);
                        console.log('document.', $scope.document);
                    }, function errorCallback(response) {
                        console.log('error GetProjectCodeMode.', response);
                    });
                };
                getProjectCodeMode();

                var getOverideCostCenter = function () {
                    var URL = CONFIG.SERVER + 'HRTR/GetModuleSetting';
                    var oRequestParameter = { InputParameter: { 'KEY': 'EnableOverideCostCenter' }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.settings.EnableOverideCostCenter = response.data;
                        console.log('EnableOverideCostCenter.', response.data);
                    }, function errorCallback(response) {
                        console.log('error EnableOverideCostCenter.', response);
                    });
                };
                getOverideCostCenter();

                var getOverideIO = function () {
                    var URL = CONFIG.SERVER + 'HRTR/GetModuleSetting';
                    var oRequestParameter = { InputParameter: { 'KEY': 'EnableOverideIO' }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.settings.EnableOverideIO = response.data;
                        console.log('EnableOverideIO.', response.data);
                    }, function errorCallback(response) {
                        console.log('error EnableOverideIO.', response);
                    });
                };
                getOverideIO();
                // !load settings

                // load master data
                var getCostCenterDistribution = function () {
                    var URL = CONFIG.SERVER + 'HRTR/GetCostCenterDistribution';
                    var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.masterData.objCostcenterDistribution = response.data;

                        // set default
                        //if ($scope.masterData.objCostcenterDistribution != null && $scope.masterData.objCostcenterDistribution.length > 0) {
                        //    $scope.document.Additional.CostCenter = $scope.masterData.objCostcenterDistribution[0].CostCenterCode;
                        //    $scope.getIOByCostCenter($scope.masterData.objCostcenterDistribution[0].CostCenterCode);
                        //}

                        console.log('objCostcenterDistribution.', $scope.masterData.objCostcenterDistribution);
                    }, function errorCallback(response) {
                        console.log('error GeneralExpenseEditorController GetCostcenterDistribution.', response);
                    });
                };
                getCostCenterDistribution();

                var getCostCenterAll = function () {
                    var URL = CONFIG.SERVER + 'HRTR/GetCostCenter';
                    var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.masterData.objCostcenter = response.data;

                    }, function errorCallback(response) {
                        console.log('error GeneralExpenseEditorController GetCostcenter.', response);
                    });
                }
                getCostCenterAll();

                var getOrganizationAll = function () {
                    var URL = CONFIG.SERVER + 'HRTR/GetOrganization/';
                    var oRequestParameter = { InputParameter: { "CreateDate": $scope.document.Additional.CreatedDate }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.masterData.objOrganization = response.data;

                        // set default organization
                        //$scope.document.Additional.AlternativeIOOrg = employeeData.OrgUnit;

                    }, function errorCallback(response) {
                        console.log('error GeneralExpenseEditorController objOrganization.', response);
                    });
                };
                getOrganizationAll();

                var getExpenseRateTypeForLookup = function () {
                    var URL = CONFIG.SERVER + 'HRTR/GetExpenseRateTypeForLookup';
                    var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.masterData.objExpenseRateTypeForLookup = response.data;
                        console.log('getExpenseRateTypeForLookup.', response.data);
                    }, function errorCallback(response) {
                        console.log('error getExpenseRateTypeForLookup.', response);
                    });
                };
                getExpenseRateTypeForLookup();

                var getReversalReason = function () {
                    var URL = CONFIG.SERVER + 'HRTR/GetAllReversalReasonCode';
                    var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.reversalReason = response.data;
                    }, function errorCallback(response) {
                        $scope.reversalReason = [];
                        console.log('error GetAllReversalReasonCode.', response);
                    });
                }
                getReversalReason();

                var getCountry = function () {
                    var URL = CONFIG.SERVER + 'HRTR/GetAllCountry';
                    var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.CountryList = response.data;
                    },
                    function errorCallback(response) {
                        console.log('error InitialConfig.', response);
                    });
                };
                getCountry();
                // !load master data

                if (!$scope.document.Additional.IsOverridePostingDate) {
                    $scope.document.Additional.PostingDate = $filter('date')(new Date(), 'yyyy-MM-ddT00:00:00');
                }

                if (!$scope.document.Additional.IsOverrideBaseLineDate) {
                    $scope.document.Additional.BaseLineDate = $filter('date')(new Date(), 'yyyy-MM-ddT00:00:00');
                }

                $scope.getCostCenterForLookup = function () {
                    var URL = CONFIG.SERVER + 'HRTR/GetCostCenterForLookup';
                    var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.objCostcenterLookup = response.data;
                        console.log('GetCostCenterForLookup.', $scope.objCostcenterLookup);
                    }, function errorCallback(response) {
                        console.log('error GeneralExpenseEditorController GetCostCenterForLookup.', response);
                    });
                };
                $scope.getCostCenterForLookup();

                $scope.GetEmployeeDataChangeLanguage = function () {
                    var URL = CONFIG.SERVER + 'HRTR/GetEmployeeDataChangeLanguage';
                    var oRequestParameter = { InputParameter: { "CreateDate": $scope.document.Additional.CreatedDate }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.objEmployee = response.data;
                        console.log('GetEmployeeDataChangeLanguage.', $scope.objCostcenterLookup);
                    }, function errorCallback(response) {
                        console.log('error GeneralExpenseEditorController GetEmployeeDataChangeLanguage.', response);
                    });
                };
                $scope.GetEmployeeDataChangeLanguage();

                $scope.getIOForLookup = function () {
                    var URL = CONFIG.SERVER + 'HRTR/GetInternalOrderObjForLookup';
                    var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.objIOLookupObj = response.data;
                        console.log('GetInternalOrderObjForLookup.', $scope.objIOLookupObj);
                    }, function errorCallback(response) {
                        console.log('error GeneralExpenseEditorController GetInternalOrderObjForLookup.', response);
                    });
                };
                $scope.getIOForLookup();

                $scope.getOrgForLookup = function () {
                    var URL = CONFIG.SERVER + 'HRTR/GetOrganizationForLookup';
                    var oRequestParameter = { InputParameter: { 'CreateDate': $filter('date')(new Date(), 'yyyy-MM-ddT00:00:00') }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.objOrgForLookup = response.data;
                        console.log('GetOrganizationForLookup.', $scope.objOrgForLookup);
                    }, function errorCallback(response) {
                    });
                };
                $scope.getOrgForLookup();

            };
            $scope.ChildAction.SetData();

            $scope.ChildAction.LoadData = function () {

            };

            $scope.setSelectedPostingDate = function (newDate) {
                $scope.document.Additional.PostingDate = $filter('date')(newDate, 'yyyy-MM-ddT00:00:00');
                $scope.document.Additional.IsOverridePostingDate = true;
            };

            $scope.setSelectedBaseLineDate = function (newDate) {
                $scope.document.Additional.BaseLineDate = $filter('date')(newDate, 'yyyy-MM-ddT00:00:00');
                $scope.document.Additional.IsOverrideBaseLineDate = true;
            };

            $scope.getReasonCodeText = function (reasonCode) {
                if (angular.isDefined($scope.reversalReason) && $scope.reversalReason != null) {
                    for (var i = 0; i < $scope.reversalReason.length; i++) {
                        return $scope.reversalReason[i].Description;
                    }
                }
                return '';
            };

            $scope.getFileAttach = function (attachmentFile) {
                /* direct */
                if (angular.isDefined(attachmentFile) && attachmentFile != '') {
                    var path = CONFIG.SERVER + 'Client/files/' + attachmentFile;
                    $window.open(path);
                }
                /* !direct */
            };

            $scope.lookupForCountryName = function (CountryCode) {
                if ($scope.CountryList) {
                    for (var i = 0; i < $scope.CountryList.length; i++) {
                        if ($scope.CountryList[i].CountryCode == CountryCode) {
                            return $scope.CountryList[i].CountryName;
                        }
                    }
                }
                return '-';
            };

            $scope.isUserRoleAccounting = false;
            $scope.checkAccountingRole = function () {
                var URL = CONFIG.SERVER + 'HRTR/IsAccountUser';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.isUserRoleAccounting = response.data;
                    console.log('IsAccountingRole.', response.data);
                }, function errorCallback(response) {
                    console.log('error IsAccountingRole.', response);
                });
            };
            $scope.checkAccountingRole();






















            // ********************************* Expense list viewr ctrl start **********************************************


            // --- variable ---
            $scope.VIEW_MODE = true;
            $scope.expenseDetailMode = false;
            $scope.expenseReceiptDetailMode = false;
            $scope.expenseItemDetailMode = false;
            $scope.expenseList_data;
            $scope.expenseListReceipt_data;
            $scope.expenseList_item_data;
            $scope.expenseList_status = {
                isReceipt: false,
                pageClassName: ''
            };
           

            $scope.panelName = '';

            $scope.tempData = {
                chkWhtTax: false,
                oWHTTypeSubsidise: null,
                sumWHTAmount: 0
            };
            $scope.whtSettings = {
                ENABLEWHT: false,
                ENABLEROW: false
            };
            $scope.tempFormData = {
                invoiceDate: null,
                chkVendorTaxIDDup: null
            };
            $scope.personalCurrency = null;
            $scope.ddlDefault = {
                ExpenseTypeGroup: 0,
                ExpenseType: 0,
                setVatTypeDefault: 0
            };
            var temp;
            $scope.currentView = ''

            // master data
            $scope.moduleSetting;
            $scope.flatRateLocations;
            $scope.oTranType = [];
            $scope.masterData = {
                objCostcenter: [],
                objCostcenterDistribution: [],

            }
            $scope.projectCodes = [];
            $scope.objIO = [];
            $scope.sub_item_objIO = [];
            $scope.expenseItems_ReceiptItem = [];
            $scope.objVendor = [];
            $scope.objCountry = [];
            $scope.setVatTypeDefault = [];
            $scope.moduleSetting = {
                PROJECTCODEMODE_NONE: 'NONE',
                PROJECTCODEMODE_AVERAGE: 'AVERAGE',
                PROJECTCODEMODE_EXACTLY: 'EXACTLY'
            }
            //  --- variable ---


            // flooring call service : get all base master data use in this page
            getAllCountry(); getVender(); getAllProject();
            getCarTypes(); getOrganizationAll(); getCostCenterDistribution();
            getCostCenterAll();   getFlatRateLocations();
            getVatTypes();
            // flooring call service
            getOverideProjectCost();

            // --- listener ---
            $scope.init = function () {
                getOverideCostCenter(); // -- warning await use for set select other cost center
                getOverideIO();// -- warning await use for set select other IO
            }
            $scope.initExpenseDetail = function (obj) {
                initExpenseDetail(obj);
            }
            $scope.initExpenseItemDetail = function (obj) {
                initExpenseItemDetail(obj);
            }
            $scope.initReceiptExpenseItemDetail = function (obj) {
                initReceiptExpenseItemDetail(obj);
            }
            $scope.initExpenseReceiptDetail = function (obj) {
                initExpenseReceiptDetail(obj);
            }
            $scope.closeExpenseDetail = function () {
                closeExpenseDetail();
            }
            $scope.closeExpenseItemDetail = function () {
                closeExpenseItemDetail();
            }
            $scope.closeExpenseReceiptDetail = function () {
                closeExpenseReceiptDetail();
            }
            $scope.getTemplate = function () {
                return 'views/hr/tr/data/panel/' + $scope.panelName + '.html?t=' + $scope.runtime;
            };
            $scope.showFileAttach = function (attachment) {
                showFileAttach(attachment)
            }
            // --- listener ---




            // --- init ---
            function initExpenseDetail(obj) {
                // setting
                $scope.expenseList_status.isReceipt = false;
                $scope.pageClassName = '';
                $scope.currentView = 'noreceipt';
                var reqN = obj.RequestNo;
                if (reqN.substring(reqN.length - 5, reqN.length - 3) == 'GE') {
                    initGeneralExp(obj);
                    $scope.pageClassName = 'GeneralExpenseReport'
                } else {
                    $scope.pageClassName = 'TravelReport';
                    initReport(obj);
                }
                $scope.expenseDetailMode = true;
                $scope.viewDetailMode(true);
            }
            function initReport(obj) {
                $scope.expenseList_data = obj.ExpenseReportReceiptItemList[0];
                $scope.expenseList_data.ReceiptDate = obj.ReceiptDate;
                $scope.expenseList_data.OriginalCurrencyCode = obj.OriginalCurrencyCode;
                $scope.expenseList_data.OriginalTotalAmount = obj.OriginalTotalAmount;
                $scope.expenseList_data.NolimitDate = obj.NolimitDate;

                $scope.settings.ProjectCodeMode = $scope.document.Additional.ProjectCodeMode.toUpperCase(); // es Get by Travel

                $scope.expenseList_data.Amount = $scope.expenseList_data.ExpenseReportReceiptItemDetailList[0].OriginalAmount;
                if ($scope.expenseList_data.ExpenseReportReceiptItemDetailList.length > 0) {
                    var expenseRecieptItem = $scope.expenseList_data.ExpenseReportReceiptItemDetailList[0];
                    if (expenseRecieptItem.IsAlternativeIOOrg) {
                        getIO(expenseRecieptItem.IsAlternativeIOOrg, expenseRecieptItem.AlternativeIOOrg)// warning await 
                    } else {
                        getIO(expenseRecieptItem.IsAlternativeIOOrg, expenseRecieptItem.CostCenter)// warning await 
                    }

                }
                getPanelNameByExpenseType($scope.expenseList_data.ExpenseTypeID); // -- warning await
                console.debug($scope.expenseList_data);
            }
            function initGeneralExp(obj) {
                $scope.expenseList_data = obj.ExpenseReportReceiptItemList[0];
                $scope.expenseList_data.PayTo = obj.PayTo;
                $scope.expenseList_data.ReceiptDate = obj.ReceiptDate;
                $scope.expenseList_data.OriginalCurrencyCode = obj.OriginalCurrencyCode;
                $scope.expenseList_data.OriginalTotalAmount = obj.OriginalTotalAmount;
                $scope.expenseList_data.NolimitDate = obj.NolimitDate;

                $scope.settings.ProjectCodeMode = $scope.document.Additional.ProjectCodeMode.toUpperCase(); // es Get 

                $scope.expenseList_data.Amount = $scope.expenseList_data.ExpenseReportReceiptItemDetailList[0].OriginalAmount;
                if ($scope.expenseList_data.ExpenseReportReceiptItemDetailList.length > 0) {
                    var expenseRecieptItem = $scope.expenseList_data.ExpenseReportReceiptItemDetailList[0];
                    if (expenseRecieptItem.IsAlternativeIOOrg) {
                        getIO(expenseRecieptItem.IsAlternativeIOOrg, expenseRecieptItem.AlternativeIOOrg)// warning await 
                    } else {
                        getIO(expenseRecieptItem.IsAlternativeIOOrg, expenseRecieptItem.CostCenter)// warning await 
                    }

                }
                getPanelNameByExpenseType($scope.expenseList_data.ExpenseTypeID); // -- warning await
                console.debug($scope.expenseList_data);
            }
            function initExpenseItemDetail(obj) {
                $scope.expenseList_item_data = obj;
                if ($scope.expenseList_data.ExpenseReportReceiptItemDetailList.length > 0) {
                    var expenseRecieptItem = $scope.expenseList_data.ExpenseReportReceiptItemDetailList[0];
                    if (expenseRecieptItem.IsAlternativeIOOrg) {
                        getSubIO(expenseRecieptItem.IsAlternativeIOOrg, expenseRecieptItem.AlternativeIOOrg)// warning await 
                    } else {
                        getSubIO(expenseRecieptItem.IsAlternativeIOOrg, expenseRecieptItem.CostCenter)// warning await 
                    }
                }
                $scope.expenseItemDetailMode = true;

            }


            // init main receipt page
            function initExpenseReceiptDetail(obj) {
                $scope.currentView = 'receipt';
                $scope.expenseListReceipt_data = obj;
                console.debug(obj);
                onBlurWHTAmount(); // -- waring await use for  WHT Amount
                getPanelWHT() // -- waring await use for Panel WHT
                checkTaxIDDup();  // -- waring await use for Panel WHT
                getPersonalCurrency(); // -- waring await use for get all personal currency of person
                $scope.expenseItems_ReceiptItem = getExpenseItems_ReceiptItem();
                $scope.expenseReceiptDetailMode = true;
                $scope.viewDetailMode(true);

            }


            // view receipt item
            function initReceiptExpenseItemDetail(obj) {
                $scope.expenseList_status.isReceipt = true;
                $scope.expenseList_item_data = obj;

                if ($scope.expenseList_item_data.ExpenseReportReceiptItemDetailList.length > 0) {
                    var expenseRecieptItem = $scope.expenseList_item_data.ExpenseReportReceiptItemDetailList[0];
                    if (expenseRecieptItem.IsAlternativeIOOrg) {
                        getSubIO(expenseRecieptItem.IsAlternativeIOOrg, expenseRecieptItem.AlternativeIOOrg)// warning await 
                    } else {
                        getSubIO(expenseRecieptItem.IsAlternativeIOOrg, expenseRecieptItem.CostCenter)// warning await 
                    }
                }
                $scope.pageClassName = '';
                var reqN = obj.RequestNo;
                if (reqN.substring(reqN.length - 5, reqN.length - 3) == 'GE') {
                    initGeneralExp_receipt(obj);
                    $scope.pageClassName = 'GeneralExpenseReport'
                } else {
                    $scope.pageClassName = 'TravelReport';
                    initReport_receipt(obj);
                }
                $scope.expenseDetailMode = true;
                $scope.expenseReceiptDetailMode = false;
            }


            function initReport_receipt(obj) {
                $scope.expenseList_data = obj;
                $scope.expenseList_data.ReceiptDate = obj.ReceiptDate;
                $scope.expenseList_data.OriginalCurrencyCode = obj.OriginalCurrencyCode;
                $scope.expenseList_data.OriginalTotalAmount = obj.OriginalTotalAmount;
                $scope.expenseList_data.NolimitDate = obj.NolimitDate;

                $scope.settings.ProjectCodeMode = $scope.document.Additional.ProjectCodeMode.toUpperCase(); // es Get by Travel

                $scope.expenseList_data.Amount = $scope.expenseList_data.ExpenseReportReceiptItemDetailList[0].OriginalAmount;
                if ($scope.expenseList_data.ExpenseReportReceiptItemDetailList.length > 0) {
                    var expenseRecieptItem = $scope.expenseList_data.ExpenseReportReceiptItemDetailList[0];
                    if (expenseRecieptItem.IsAlternativeIOOrg) {
                        getIO(expenseRecieptItem.IsAlternativeIOOrg, expenseRecieptItem.AlternativeIOOrg)// warning await 
                    } else {
                        getIO(expenseRecieptItem.IsAlternativeIOOrg, expenseRecieptItem.CostCenter)// warning await 
                    }

                }
                getPanelNameByExpenseType($scope.expenseList_data.ExpenseTypeID); // -- warning await
                console.debug($scope.expenseList_data);
            }
            function initGeneralExp_receipt(obj) {
                $scope.expenseList_data = obj;
                $scope.expenseList_data.ReceiptDate = obj.ReceiptDate;
                $scope.expenseList_data.OriginalCurrencyCode = obj.OriginalCurrencyCode;
                if (obj.OriginalTotalAmount) {
                    $scope.expenseList_data.OriginalTotalAmount = obj.OriginalTotalAmount;
                } else {
                    $scope.expenseList_data.OriginalTotalAmount = 0;
                    for (var i = 0; i < $scope.expenseList_data.ExpenseReportReceiptItemDetailList.length ; i++) {
                        $scope.expenseList_data.OriginalTotalAmount += $scope.expenseList_data.ExpenseReportReceiptItemDetailList[i].OriginalAmount;
                    }
                }


                $scope.settings.ProjectCodeMode = $scope.document.Additional.ProjectCodeMode.toUpperCase(); // es Get 

                $scope.expenseList_data.Amount = $scope.expenseList_data.ExpenseReportReceiptItemDetailList[0].OriginalAmount;
                if ($scope.expenseList_data.ExpenseReportReceiptItemDetailList.length > 0) {
                    var expenseRecieptItem = $scope.expenseList_data.ExpenseReportReceiptItemDetailList[0];
                    if (expenseRecieptItem.IsAlternativeIOOrg) {
                        getIO(expenseRecieptItem.IsAlternativeIOOrg, expenseRecieptItem.AlternativeIOOrg)// warning await 
                    } else {
                        getIO(expenseRecieptItem.IsAlternativeIOOrg, expenseRecieptItem.CostCenter)// warning await 
                    }

                }
                getPanelNameByExpenseType($scope.expenseList_data.ExpenseTypeID); // -- warning await
                console.debug($scope.expenseList_data);
            }
            // --- init ---


            // -- function -- 
            function closeExpenseDetail() {
                if ($scope.currentView == 'noreceipt') {
                    $scope.expenseDetailMode = false;
                    $scope.expenseItemDetailMode = false;
                    $scope.expenseReceiptDetailMode = false;
                    $scope.viewDetailMode(false);
                } else {
                    $scope.expenseDetailMode = false;
                    $scope.expenseItemDetailMode = false;
                    $scope.expenseReceiptDetailMode = true;
                }

            }
            function closeExpenseItemDetail() {
                $scope.expenseItemDetailMode = false;

            }
            function closeExpenseReceiptDetail() {
                $scope.expenseReceiptDetailMode = false;
                $scope.viewDetailMode(false);
            }
            function getExpenseItems_ReceiptItem() {
                var oList = [];
                if (angular.isDefined($scope.expenseListReceipt_data.ExpenseReportReceiptItemList) && $scope.expenseListReceipt_data.ExpenseReportReceiptItemList != null) {
                    var ExpenseReportReceiptItemList = $scope.expenseListReceipt_data.ExpenseReportReceiptItemList;
                    for (var i = 0; i < ExpenseReportReceiptItemList.length; i++) {
                        var receiptItem = angular.copy(ExpenseReportReceiptItemList[i]);
                        var countReceiptItemDetail = receiptItem.ExpenseReportReceiptItemDetailList.length;
                        receiptItem.IsFirstRow = false;
                        receiptItem.ItemCount = countReceiptItemDetail;
                        receiptItem.ExpenseReportReceiptItemDetailList = null;

                        for (var k = 0; k < countReceiptItemDetail; k++) {
                            var tempReceiptItem = angular.copy(receiptItem);
                            var receiptItemDetail = ExpenseReportReceiptItemList[i].ExpenseReportReceiptItemDetailList[k];
                            if (k == 0) {
                                tempReceiptItem.IsFirstRow = true;
                            }
                            tempReceiptItem.itemDetail = {
                                CostCenter: receiptItemDetail.CostCenter,
                                IO: receiptItemDetail.IO,
                                LocalAmount: receiptItemDetail.LocalAmount,
                                OriginalAmount: receiptItemDetail.OriginalAmount
                            };
                            tempReceiptItem.realItem = ExpenseReportReceiptItemList[i];
                            tempReceiptItem.orderID = i + 1;
                            oList.push(tempReceiptItem);
                        }
                    }

                }
                return oList;
            };
            function onBlurWHTAmount() {
                var whtAmount1 = Number($scope.expenseListReceipt_data.WHTAmount1LC);
                var whtAmount2 = Number($scope.expenseListReceipt_data.WHTAmount2LC);
                var oSumWHT = 0;

                if ($scope.expenseListReceipt_data.WHTType1 != null && $scope.expenseListReceipt_data.WHTType1 != '') {
                    if ($scope.tempData.oWHTTypeSubsidise.indexOf($scope.expenseListReceipt_data.WHTType1) > -1) {
                        //In the array!
                    } else {
                        oSumWHT = oSumWHT + whtAmount1; //Not in the array 
                    }
                }

                if ($scope.expenseListReceipt_data.WHTType2 != null && $scope.expenseListReceipt_data.WHTType2 != '') {
                    if ($scope.tempData.oWHTTypeSubsidise.indexOf($scope.expenseListReceipt_data.WHTType2) > -1) {
                        //In the array!
                    } else {
                        oSumWHT = oSumWHT + whtAmount2; //Not in the array 
                    }
                }
                $scope.tempData.sumWHTAmount = oSumWHT;
            };
            function showFileAttach(attachment) {
                /* direct */
                if (angular.isDefined(attachment)) {
                    if (typeof cordova != 'undefined') {

                    } else {
                        var path = CONFIG.SERVER + 'Client/files/' + attachment.FilePath + '/' + attachment.FileName;
                        $window.open(path);
                    }
                }
                /* !direct */
            };
            // -- function --


            // -- services
            function getPanelNameByExpenseType(expenseTypeID) {
                var URL = CONFIG.SERVER + 'HRTR/GetEditorPanelByExpenseTypeID';
                var oRequestParameter = { InputParameter: { "ExpenseTypeID": expenseTypeID }, CurrentEmployee: $scope.employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    if (response.data && response.data.length > 0) {
                        $scope.panelName = response.data[0];
                    } else {
                        $scope.panelName = '';
                    }
                }, function errorCallback(response) {
                    console.error('error ExpenseReceiptEditorDirective panelName.', response);
                });
            };
            function getFlatRateLocations() {
                var URL = CONFIG.SERVER + 'HRTR/GetFlatRateLocationAll';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: $scope.employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.flatRateLocations = response.data;
                }, function errorCallback(response) {
                    console.error('error GetFlatRateLocationAll.', response);
                });
            };
            function getTransportationTypeByLanguage() {
                var URL = CONFIG.SERVER + 'HRTR/GetTransportationTypeByLanguage';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: $scope.employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.oTranType = response.data;
                }, function errorCallback(response) {
                    console.error('error GeneralExpenseEditorController GetTransportationTypeByLanguage.', response);
                });
            };
            function getCostCenterAll() {
                var URL = CONFIG.SERVER + 'HRTR/GetCostCenter';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: $scope.employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.masterData.objCostcenter = response.data;

                }, function errorCallback(response) {
                    console.error('error GeneralExpenseEditorController GetCostcenter.', response);
                });
            }
            function getCostCenterDistribution() {
                var URL = CONFIG.SERVER + 'HRTR/GetCostCenterDistribution';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: $scope.employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.masterData.objCostcenterDistribution = response.data;
                }, function errorCallback(response) {
                    console.error('error GeneralExpenseEditorController GetCostcenterDistribution.', response);
                });
            };
            function getOverideCostCenter() {
                var URL = CONFIG.SERVER + 'HRTR/GetModuleSetting';
                var oRequestParameter = { InputParameter: { 'KEY': 'EnableOverideCostCenter' }, CurrentEmployee: $scope.employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.settings.EnableOverideCostCenter = response.data;
                }, function errorCallback(response) {
                    console.error('error EnableOverideCostCenter.', response);
                });
            };
            function getOrganizationAll() {
                var URL = CONFIG.SERVER + 'HRTR/GetOrganization/';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: $scope.employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.masterData.objOrganization = response.data;
                }, function errorCallback(response) {
                    console.error('error GeneralExpenseEditorController objOrganization.', response);
                });
            };
            function getOverideIO() {
                var URL = CONFIG.SERVER + 'HRTR/GetModuleSetting';
                var oRequestParameter = { InputParameter: { 'KEY': 'EnableOverideIO' }, CurrentEmployee: $scope.employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.settings.EnableOverideIO = response.data;
                }, function errorCallback(response) {
                    console.error('error EnableOverideIO.', response);
                });
            };
            function getIO(IsAlternativeIOOrg, arg) {
                if (IsAlternativeIOOrg) {
                    var URL = CONFIG.SERVER + 'HRTR/GetInternalOrderByOrgUnit/';
                    var oRequestParameter = { InputParameter: { "OrgUnit": arg }, CurrentEmployee: $scope.employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.objIO = response.data;
                    }, function errorCallback(response) {
                        console.error('error GeneralExpenseEditorController GetInternalOrderByCostCenter.', response);
                    });
                }
                else {
                    var URL = CONFIG.SERVER + 'HRTR/GetInternalOrderByCostCenter/';
                    var oRequestParameter = { InputParameter: { "CostCenter": arg }, CurrentEmployee: $scope.employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.objIO = response.data;
                    }, function errorCallback(response) {
                        console.error('error GeneralExpenseEditorController GetInternalOrderByCostCenter.', response);
                    });
                }
            };
            function getSubIO(IsAlternativeIOOrg, arg) {
                if (IsAlternativeIOOrg) {
                    var URL = CONFIG.SERVER + 'HRTR/GetInternalOrderByOrgUnit/';
                    var oRequestParameter = { InputParameter: { "OrgUnit": arg }, CurrentEmployee: $scope.employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.sub_item_objIO = response.data;
                    }, function errorCallback(response) {
                        console.error('error GeneralExpenseEditorController GetInternalOrderByCostCenter.', response);
                    });
                }
                else {
                    var URL = CONFIG.SERVER + 'HRTR/GetInternalOrderByCostCenter/';
                    var oRequestParameter = { InputParameter: { "CostCenter": arg }, CurrentEmployee: $scope.employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.sub_item_objIO = response.data;
                    }, function errorCallback(response) {
                        console.error('error GeneralExpenseEditorController GetInternalOrderByCostCenter.', response);
                    });
                }
            };
            function getCarTypes() {
                var URL = CONFIG.SERVER + 'HRTR/GetCarTypeAll';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: $scope.employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.carTypes = response.data;
                }, function errorCallback(response) {
                    console.error('error GetCarTypeAll.', response);
                });
            };
            function getAllProject() {
                var URL = CONFIG.SERVER + 'HRTR/GetAllProject';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.projectCodes = response.data;
                }, function errorCallback(response) {
                    $scope.projectCodes = [];
                });
            };
            function getOverideProjectCost() {
                var URL = CONFIG.SERVER + 'HRTR/GetModuleSetting';
                var oRequestParameter = { InputParameter: { 'KEY': 'ENABLE_PROJECTCOST' }, CurrentEmployee: $scope.employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    var str = response.data;
                    var res = str.toLowerCase();
                    $scope.settings.ENABLE_PROJECTCOST = (res == 'true');
                }, function errorCallback(response) {
                    console.error('error EnableOverideIO.', response);
                });
            };

            function getVender() {
                var URL = CONFIG.SERVER + 'HRTR/GetVendorAll';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: $scope.employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.objVendor = response.data;
                    if ($scope.objVendor != null && $scope.objVendor.length > 0) {
                        for (var i = 0; i < $scope.objVendor.length; i++) {
                            if ($scope.objVendor[i].TaxID != null) {
                                $scope.objVendor[i].TaxPersonalID = ' : ' + $scope.objVendor[i].TaxID;
                            }
                            else {
                                if ($scope.objVendor[i].PersonalID != null) {
                                    $scope.objVendor[i].TaxPersonalID = ' : ' + $scope.objVendor[i].PersonalID;
                                }
                                else {
                                    $scope.objVendor[i].TaxPersonalID = '';
                                }
                            }
                        }
                    }
                }, function errorCallback(response) {
                    console.error('error GeneralExpenseEditorController GetVendorAll.', response);
                });
            }
            function checkTaxIDDup() {
                var URL = CONFIG.SERVER + 'HRTR/CheckIsDupTaxID/';
                var oRequestParameter = { InputParameter: { "TAXID": $scope.expenseListReceipt_data.VendorTaxID }, CurrentEmployee: $scope.employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };

                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.tempFormData.chkVendorTaxIDDup = response.data;
                }, function errorCallback(response) {
                    $scope.tempFormData.chkVendorTaxIDDup = 'error';
                    console.error('error GeneralExpenseEditorController checkTaxIDDup.', response);
                });
            };
            function getPanelWHT() {
                var reqN = $scope.expenseListReceipt_data.RequestNo;
                if (reqN.substring(reqN.length - 5, reqN.length - 3) == 'GE') {
                    $scope.pageClassName = 'GeneralExpenseReport'
                } else {
                    $scope.pageClassName = 'TravelReport';
                }
                var sMapType = ($scope.pageClassName == 'GeneralExpenseReport' || $scope.pageClassName == 'TravelReport') ? 'TE_ENABLEWHT' : 'EX_ENABLEWHT';
                var URL = CONFIG.SERVER + 'HRTR/GetPanelWHT';
                var oRequestParameter = { InputParameter: { 'MAPPINGTYPE': sMapType }, CurrentEmployee: $scope.employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.whtSettings.ENABLEWHT = response.data;
                }, function errorCallback(response) {
                    console.error('error GetPanelWHT.', response);
                });
            };
            function getAllCountry() {
                var URL = CONFIG.SERVER + 'HRTR/GetAllCountry';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: $scope.employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.objCountry = response.data;
                }, function errorCallback(response) {
                    console.log('error GeneralExpenseEditorController GetAllCountry.', response);
                });
            }
            function getPersonalCurrency() {
                var URL = CONFIG.SERVER + 'HRTR/GetPersonalExchangeRateCapture';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: $scope.employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.personalCurrency = response.data[0];
                    var reqN = $scope.expenseListReceipt_data.RequestNo;
                    if (reqN.substring(reqN.length - 5, reqN.length - 3) == 'GE') {
                        $scope.travelCurrencyList = response.data;
                    } else {
                        $scope.travelCurrencyList = angular.copy($scope.masterData.travelCurrencyList);
                        if ($scope.travelCurrencyList == null) {
                            $scope.travelCurrencyList = [];
                        }
                        if ($scope.personalCurrency != null) {
                            $scope.travelCurrencyList.unshift(angular.copy($scope.personalCurrency));
                        }
                    }
                }, function errorCallback(response) {
                    console.log('error GeneralExpenseEditorController GetPersonalCurrency.', response);
                });
            };

            function getVatTypes() {
                var URL = CONFIG.SERVER + 'HRTR/GetAllVATType';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: $scope.employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.setVatTypeDefault = response.data;

                    if ($scope.setVatTypeDefault != null && $scope.setVatTypeDefault.length > 0) {
                        $scope.ddlDefault.VATCode = $scope.setVatTypeDefault[0].VATCode;
                    }
                }, function errorCallback(response) {
                    console.log('error GetAllVATType.', response);
                });
            }

            // -- services

            // ***************************************************  Expense list viewr ctrl end *********************************************


            $scope.payToSettings = {
                ENABLEPAYTOEMPLOYEE: false,
                ENABLEPAYTOVENDOR: false,
                ENABLEPAYTOCORPCARD: false
            };
            // call both in receipt and no receipt
            var getRadioPayTo = function () {
                if ($scope.settings.ExpenseTypeGroupInfo.prefix.toUpperCase() == 'RC') {
                    // TravelReport for Copporate
                    $scope.payToSettings.ENABLEPAYTOEMPLOYEE = false;
                    $scope.payToSettings.ENABLEPAYTOVENDOR = false;
                    $scope.payToSettings.ENABLEPAYTOCORPCARD = true;

                } else {
                    var URL = CONFIG.SERVER + 'HRTR/GetRadioPayTo';
                    var oRequestParameter = { InputParameter: { 'PREFIX': $scope.settings.ExpenseTypeGroupInfo.prefix }, CurrentEmployee: $scope.employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {

                        $scope.payToSettings.ENABLEPAYTOEMPLOYEE = response.data[0];
                        $scope.payToSettings.ENABLEPAYTOVENDOR = response.data[1];
                        $scope.payToSettings.ENABLEPAYTOCORPCARD = response.data[2];

                        console.log('GetRadioPayTo.', response.data);
                    }, function errorCallback(response) {
                        console.log('error GetRadioPayTo.', response);
                    });
                }
            };
            getRadioPayTo();

        }]);
})();


















            