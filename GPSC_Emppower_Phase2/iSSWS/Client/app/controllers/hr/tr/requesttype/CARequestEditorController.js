﻿(function () {
    angular.module('ESSMobile')
        .controller('CARequestEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG','$q', function ($scope, $http, $routeParams, $location, $filter, CONFIG,$q) {
            
            //$scope.employeeData = getToken(CONFIG.USER);
            //$scope.TravelGroupRequest = $scope.document.Additional;
            //$scope.data = {};
            $scope.data.IsPersonalCashAdvance = true;
            //$scope.data.Readonly = false;
            //$scope.data.TotalAmount = 0;
            //$scope.Textcategory = 'TRAVELREQUEST';
            //$scope.content.Header = $scope.Text[$scope.Textcategory].TITLE;

            /* ====== Inital data before load control (Start)====== 
             AddBy: Ratchatawan W. (30 Mar 2017)
             Description: For initial data before load all control in this page
            */
            $scope.init = function () {
                var promise_service1 = InitialConfig();
                var promise_service4 = $scope.GetExpenseRateTypeForLookup();
                var promise_service5 = $scope.GetTravelModel();
                $q.all([promise_service1, promise_service4, promise_service5]).then(function (response) {

                    $scope.data.model.ready = true;
                    console.debug('init success')
                }, function (response) {

                    console.error('init error');

                });

            }

            function InitialConfig() {
                $scope.data.Travelers = $scope.TravelGroupRequest.Travelers;
                $scope.data.PaymentFlagCaptures = $scope.data.Travelers[0].oCashAdvance.PaymentFlagCaptures;
                $scope.LoadConfig();
            };

            $scope.LoadConfig = function () {
                var URL = CONFIG.SERVER + 'HRTR/GetModuleSetting';
                var oRequestParameter = { InputParameter: { "KEY": 'CashAdvanceMode' }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.data.CAMode = response.data;

                }, function errorCallback(response) {
                    console.log('error CARequestEditorController GetModuleSetting.', response);
                });
            }

            $scope.InitCashAdvance = function () {
                if (!$scope.document.NewRequest) {
                    $scope.TravelGroupRequest = angular.copy($scope.document.Additional);
                    //Bind data
                    $scope.data.ExchangeRateCaptures = $scope.TravelGroupRequest.ExchangeRateCaptures;
                    $scope.data.TravelSchedulePlaces = $scope.TravelGroupRequest.TravelSchedulePlaces;
                    $scope.data.ProjectCostDistributions = $scope.TravelGroupRequest.ProjectCostDistributions;
                    $scope.data.Travelers = $scope.TravelGroupRequest.Travelers;
                    $scope.data.PaymentFlagCaptures = $scope.data.Travelers[0].oCashAdvance.PaymentFlagCaptures;
                    $scope.data.GroupBudgets = $scope.TravelGroupRequest.GroupBudgets;
                }
                else {
                    var oRequestParameter = {
                        InputParameter: { "referRequestNo": $scope.referRequestNo, "EmployeeID": $scope.document.RequestorNo, "PositionID": $scope.document.RequestorPosition, "CompanyCode": $scope.document.RequestorCompanyCode }
                                                    , CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor
                    };
                    var URL = CONFIG.SERVER + 'HRTR/InitCashAdvance';
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        var DefaultCurrencyCode = $scope.TravelGroupRequest.DefaultCurrencyCode;
                        $scope.TravelGroupRequest = response.data;
                        $scope.TravelGroupRequest.DefaultCurrencyCode = DefaultCurrencyCode;
                        //Bind data
                        $scope.data.ExchangeRateCaptures = $scope.TravelGroupRequest.ExchangeRateCaptures;
                        $scope.data.TravelSchedulePlaces = $scope.TravelGroupRequest.TravelSchedulePlaces;
                        $scope.data.ProjectCostDistributions = $scope.TravelGroupRequest.ProjectCostDistributions;
                        $scope.data.Travelers = $scope.TravelGroupRequest.Travelers;
                        $scope.data.Travelers[0].oCashAdvance.RequestNo = $scope.document.RequestNo;
                        $scope.data.Travelers[0].oCashAdvance.TravelRequestNo = $scope.referRequestNo;
                        $scope.data.Travelers[0].oCashAdvance.EmployeeID = $scope.document.RequestorNo;
                        $scope.data.PaymentFlagCaptures = $scope.data.Travelers[0].oCashAdvance.PaymentFlagCaptures;
                        $scope.data.GroupBudgets = $scope.TravelGroupRequest.GroupBudgets;
                        $scope.data.CAEstimate = $scope.TravelGroupRequest.Travelers[0].TotalAmount;
                    }, function errorCallback(response) {
                        // Error
                        console.log('error CARequestEditorController.', response);
                    });
                }

               
            };

            $scope.ChildAction.LoadData = function () {
                $scope.TravelGroupRequest.ExchangeRateCaptures = $scope.data.ExchangeRateCaptures;
                $scope.TravelGroupRequest.TravelSchedulePlaces = $scope.data.TravelSchedulePlaces;
                $scope.TravelGroupRequest.ProjectCostDistributions = $scope.data.ProjectCostDistributions;
                $scope.TravelGroupRequest.Travelers = $scope.data.Travelers;
                $scope.TravelGroupRequest.Travelers[0].oCashAdvance.PaymentFlagCaptures = $scope.data.PaymentFlagCaptures;
                $scope.TravelGroupRequest.GroupBudgets = $scope.data.GroupBudgets;
                $scope.document.Additional = $scope.TravelGroupRequest;
                $scope.document.Additional.Travelers[0].oCashAdvance.TotalAmount = $scope.data.TotalAmount;
                if ($scope.data.CAMode == 'Perdium') {

                }
            };

            $scope.InitCashAdvance();

        }]);
})();