﻿(function () {
    angular.module('ESSMobile')
        .controller('CancelTravelGroupEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {
            //$scope.TravelGroupRequest = $scope.document.Additional;
            $scope.data = {};
            $scope.TRAVELMODE = "G";
            $scope.data.IsRemarkEdit = true;
            $scope.data.Readonly = true;
            $scope.Textcategory = 'TRAVELREQUEST';
            $scope.content.Header = $scope.Text[$scope.Textcategory].TITLE;
            //$scope.TravelGroupRequest = {};
            $scope.data.CashAdvanceReturns = [];
            $scope.data.IsReturnEdit = true;
            $scope.settings = {
                ExpenseTypeGroupInfo: {
                    isDomestic: ''
                },
                Master: []
            };
            $scope.ChildAction.SetData = function () {
                $scope.TravelGroupCancel = $scope.document.Additional;
                $scope.TravelGroupRequest = $scope.TravelGroupCancel.TravelGroupRequest;
                var oreferRequestNo = $scope.TravelGroupCancel.TravelRequestNo;
                if (angular.isDefined($scope.referRequestNo) && $scope.referRequestNo != "") {
                    oreferRequestNo = $scope.referRequestNo;
                    $scope.TravelGroupCancel.TravelRequestNo = oreferRequestNo;
                }
                if (angular.isDefined($scope.document.RequestNo)) {
                    $scope.TravelGroupCancel.RequestNo = $scope.document.RequestNo;
                }
                $scope.GetTravelRequest(oreferRequestNo);
                //$scope.GetCashAdvance(oreferRequestNo, $scope.TravelGroupCancel.EmployeeID);

                $scope.settings = {
                    ExpenseTypeGroupInfo: {
                        isDomestic: 'D'
                    }

                };

                $scope.isDomesticTravel = function (travelReport) {
                    /* return : 'D' = Domestic, 'O' = Oversea, 'B' = Both domestic and oversea */
                    var oTravelSchedules = travelReport.TravelSchedulePlaces;
                    var personalCountryCode = $scope.employeeData.AreaSetting.CountryCode;
                    var countDomestic = 0;
                    var countOversea = 0;
                    for (var i = 0; i < oTravelSchedules.length; i++) {
                        if (personalCountryCode == oTravelSchedules[i].CountryCode) {
                            countDomestic++;
                        } else {
                            countOversea++;
                        }
                    }
                    if (countDomestic == 0) {
                        return 'O';
                    } else if (countOversea == 0) {
                        return 'D';
                    }
                    return 'B';
                };
                $scope.settings.ExpenseTypeGroupInfo.isDomestic = $scope.isDomesticTravel($scope.TravelGroupRequest);

            };

            $scope.ChildAction.LoadData = function () {
                //$scope.TravelGroupCancel.RequestNo = $scope.document.RequestNo;
                //$scope.TravelGroupCancel.TravelRequestNo = $scope.referRequestNo;
                $scope.TravelGroupCancel.Remark = $scope.data.Remark;
                $scope.TravelGroupCancel.CashAdvanceReturns = $scope.data.CashAdvanceReturns;
                $scope.document.Additional = $scope.TravelGroupCancel;
            };


            $scope.GetTravelRequest = function (referRequestNo) {
                var oEmployeeId = $scope.TravelGroupCancel.TravelGroupRequest.GroupManager;
                if (angular.isDefined($scope.itemKeys)) {
                    oEmployeeId = $scope.itemKeys[0];
                    $scope.TravelGroupCancel.TravelGroupRequest.GroupManager = oEmployeeId;
                }
                else {
                    $scope.data.Remark = $scope.TravelGroupCancel.Remark;
                    $scope.data.CashAdvanceReturns = $scope.TravelGroupCancel.CashAdvanceReturns;
                }
                var oRequestParameter = {
                    InputParameter: { "REQUESTNO": referRequestNo, "GROUPMANAGERID":"" }
                                                , CurrentEmployee: getToken(CONFIG.USER)
                                                , Creator: $scope.document.Creator, Requestor: $scope.document.Requestor
                };
                var URL = CONFIG.SERVER + 'HRTR/GetTravelRequest';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.TravelGroupCancel.TravelGroupRequest = response.data;
                    $scope.TravelGroupRequest = response.data;
                    $scope.data.Travelers = $scope.TravelGroupCancel.TravelGroupRequest.Travelers;
                    $scope.data.CashAdvance = $scope.data.Travelers[findWithAttr($scope.data.Travelers, 'EmployeeID', oEmployeeId)].oCashAdvance;
                    if ($scope.data.CashAdvance != null && $scope.data.CashAdvance.TotalAmount > 0) {
                        var date = new Date();
                        var dateStr = date.getFullYear() + '-' + ("0" + (date.getMonth() + 1)).slice(-2) + '-' + ("0" + date.getDate()).slice(-2);
                        //if ($scope.data.CashAdvanceReturns.length == 0) {
                        //    $scope.data.CashAdvanceReturns.push({
                        //        "RequestNo": $scope.document.RequestNo,
                        //        "EmployeeID": oEmployeeId,
                        //        "ReturnID": 1,
                        //        "ReturnDate": $filter('date')(dateStr, 'yyyy-MM-ddT00:00:00'),
                        //        "ReturnAmount": 0,
                        //        "FIDocID": '',
                        //        "FileAttachment": "\\" + $scope.TravelGroupCancel.RequestNo + "\\" + oEmployeeId + "\\1\\",
                        //        "FileName": '',
                        //        "FileStringBase64": ''
                        //    });
                        //}
                    }
                    //else {
                    //    $scope.data.CashAdvance = null;
                    //}
                    //$scope.NewGroupManagerDataSelector = response.data;
                    //$scope.TravelGroupCancel.NewGroupManager = $scope.NewGroupManagerDataSelector[findWithAttr($scope.NewGroupManagerDataSelector, 'EmployeeID', $scope.TravelGroupCancel.NewGroupManager)];


                    $scope.settings = {
                        ExpenseTypeGroupInfo: {
                            isDomestic: 'D'
                        }

                    };

                    $scope.isDomesticTravel = function (travelReport) {
                        /* return : 'D' = Domestic, 'O' = Oversea, 'B' = Both domestic and oversea */
                        var oTravelSchedules = travelReport.TravelSchedulePlaces;
                        var personalCountryCode = $scope.employeeData.AreaSetting.CountryCode;
                        var countDomestic = 0;
                        var countOversea = 0;
                        for (var i = 0; i < oTravelSchedules.length; i++) {
                            if (personalCountryCode == oTravelSchedules[i].CountryCode) {
                                countDomestic++;
                            } else {
                                countOversea++;
                            }
                        }
                        if (countDomestic == 0) {
                            return 'O';
                        } else if (countOversea == 0) {
                            return 'D';
                        }
                        return 'B';
                    };
                    $scope.settings.ExpenseTypeGroupInfo.isDomestic = $scope.isDomesticTravel($scope.TravelGroupRequest);

                    console.log('$scope.TravelGroupRequest : ', $scope.TravelGroupRequest);
                }, function errorCallback(response) {
                    // Error
                    console.log('error CancelTravellerEditorController GetTravelerList.', response);
                });
            }

            function findWithAttr(array, attr, value) {
                for (var i = 0; i < array.length; i += 1) {
                    if (array[i][attr] === value) {
                        return i;
                    }
                }
            }
            $scope.ChildAction.SetData();
            //Get CostCenter
            var URL = CONFIG.SERVER + 'HRTR/GetCostCenter';
            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                if (!$scope.settings.Master) $scope.settings.Master = [];
                $scope.settings.Master.CostCenterList = response.data;
                //$scope.objCostcenter = response.data;
                //console.log('objCostcenter', $scope.objCostcenter);
            }, function errorCallback(response) {
                console.log('error GeneralExpenseEditorController GetCostcenter.', response);
            });

            // Get Organization
            var URL = CONFIG.SERVER + 'HRTR/GetOrganization';
            var oRequestParameter = { InputParameter: { "CreateDate": $scope.document.CreatedDate }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                if (!$scope.settings.Master) $scope.settings.Master = [];
                $scope.settings.Master.OrgUnitList = response.data;
                //$scope.objOrganization = response.data;
            }, function errorCallback(response) {
                console.log('error TravelExpenseGroupEstimateEditorController objOrganization.', response);
            });

            // Get InternalOrder All
            var URL = CONFIG.SERVER + 'HRTR/GetInternalOrderAll/';
            var oRequestParameter = { InputParameter: '', CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                if (!$scope.settings.Master) $scope.settings.Master = [];
                $scope.settings.Master.IOList = response.data;
                //var temp = response.data;
                //$scope.allIO = temp;
            }, function errorCallback(response) {
                console.log('error GeneralExpenseEditorController GetInternalOrderAll.', response);
            });

            
        }]);
})();