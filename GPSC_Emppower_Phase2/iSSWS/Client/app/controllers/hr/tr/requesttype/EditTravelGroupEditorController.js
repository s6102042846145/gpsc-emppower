﻿(function () {
    angular.module('ESSMobile')
        .controller('EditTravelGroupEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', '$mdDialog', 'CONFIG', '$q', function ($scope, $http, $routeParams, $location, $filter, $mdDialog, CONFIG, $q) {


            $scope.ChildAction.SetData = function () {
            }
            $scope.ChildAction.LoadData = function () {
                console.log("LoadData");
                if ($scope.data.GroupBudgets && $scope.data.GroupBudgets.length) {
                    for (var i = 0; i < $scope.data.GroupBudgets.length; i++) {
                        if (!$scope.data.GroupBudgets[i] || !$scope.data.GroupBudgets[i].ExpenseTypeGroupID) {
                            $scope.data.GroupBudgets[i].ExpenseTypeGroupID = 0;
                            $scope.data.Travelers[0].oCashAdvance.GroupCashAdvances[i].ExpenseTypeGroupID = 0;
                        }
                        if (!$scope.data.GroupBudgets[i] || !$scope.data.GroupBudgets[i].CostCenter) {
                            $scope.data.GroupBudgets[i].CostCenter = '0';
                        }
                        if (!$scope.data.GroupBudgets[i] || !$scope.data.GroupBudgets[i].AlternativeIOOrg) {
                            $scope.data.GroupBudgets[i].AlternativeIOOrg = '0';
                        }
                        if (!$scope.data.GroupBudgets[i] || !$scope.data.GroupBudgets[i].IO) {
                            $scope.data.GroupBudgets[i].IO = '0';
                        }
                    }
                }
                $scope.TravelGroupRequest.Travelers[0].oCashAdvance.PaymentFlagCaptures = $scope.data.PaymentFlagCaptures;
                $scope.TravelGroupRequest.Travelers[0].oCashAdvance.TotalAmount = $scope.data.TotalAmount;
                $scope.TravelGroupRequest.TotalAmount = $scope.data.Estimate;

                $scope.TravelGroupEdit.RequestNo = $scope.document.RequestNo;
                $scope.TravelGroupEdit.TravelRequestNo = $scope.TravelGroupRequest.RequestNo;
                $scope.TravelGroupRequest.ExchangeRateCaptures = $scope.data.ExchangeRateCaptures;
                $scope.TravelGroupRequest.TravelSchedulePlaces = $scope.data.TravelSchedulePlaces;
                $scope.TravelGroupRequest.BeginDate = $scope.data.TravelSchedulePlaces[0].BeginDate;
                $scope.TravelGroupRequest.EndDate = $scope.data.TravelSchedulePlaces[$scope.data.TravelSchedulePlaces.length - 1].EndDate;
                $scope.TravelGroupRequest.ProjectCostDistributions = $scope.data.ProjectCostDistributions;
                $scope.TravelGroupRequest.Travelers = $scope.data.Travelers;
                $scope.TravelGroupRequest.GroupBudgets = $scope.data.GroupBudgets;
                $scope.TravelGroupRequest.PersonalVehicle = $scope.data.PersonalVehicle; // ($scope.data.PersonalVehicleSelect == true) ? 1 : 2;
                $scope.TravelGroupRequest.DefaultCurrencyCode = $scope.data.DefaultCurrencyCode;
                $scope.TravelGroupRequest.TotalAmount = $scope.data.Estimate;
                $scope.TravelGroupEdit.TravelGroupRequest = $scope.TravelGroupRequest;
                $scope.TravelGroupEdit.Remark = $scope.data.Remark;
                $scope.CheckUploadFiles();
                $scope.document.Additional = $scope.TravelGroupEdit;
            }


            $scope.init = function () {
                var promise_service1 = InitialConfig();
                var promise_service2 = $scope.GetTravelModel();
                var promise_service3 = $scope.GetMasterdata();
                $q.all([promise_service1, promise_service2, promise_service3]).then(function (response) {

                    $scope.data.model.ready = true;
                    console.debug('init success')
                }, function (response) {

                    console.error('init error');

                });

            }

            function InitialConfig() {
                $scope.Page = "TravelExpense";
                //for ExchangeRateControl
                $scope.data.Readonly = false;
                $scope.data.TravelSeqEdit = false;
                $scope.data.IsExchangeRateEdit = false;
                $scope.data.IsTravelRequest = true;
                $scope.data.IsEditGroup = true;
                $scope.data.Remark = $scope.TravelGroupEdit.Remark;
                $scope.data.IsRemarkEdit = (!$scope.document.CurrentFlowItemCode  || $scope.document.CurrentFlowItemCode == "DRAFT" || $scope.document.CurrentFlowItemCode == "WAIT_FOR_EDIT");
                $scope.data.IsEditGroup = !$scope.data.IsRemarkEdit;
                CheckLineManager($scope.TravelGroupEdit.TravelRequestNo);
                BindData($scope.TravelGroupEdit.TravelGroupRequest);
                $scope.TravelGroupEdit.CanEdit = !$scope.data.IsRemarkEdit;//true;
                $scope.data.Readonly = !$scope.TravelGroupEdit.CanEdit;
            }

            function CheckLineManager(requestNo) {
                var URL = CONFIG.SERVER + 'HRTR/NotCheckLineManager';
                var oRequestParameter = { InputParameter: { "REQUESTNO": requestNo }, CurrentEmployee: getToken(CONFIG.USER), Requestor: $scope.document.Requestor, Creator: $scope.document.Creator }
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    //ถ้ามีสิทธิ์ในการอนุมัติตัวเอง
                    if (response.data) {
                        $scope.TravelGroupEdit.CanEdit = response.data;
                        $scope.data.Readonly = !response.data;
                        $scope.data.IsRemarkEdit = response.data;
                    }
                    else {
                        $scope.data.Readonly = !$scope.TravelGroupEdit.CanEdit;
                    }
                    console.log('success NotCheckLineManager.', response);

                }, function errorCallback(response) {
                    // Error
                    console.log('error NotCheckLineManager.', response);
                });

            }

            function BindData(oTgrData) {

                $scope.TravelGroupRequest = oTgrData;
                //$scope.TravelGroupRequest.GroupSelect = true;
                $scope.data.Remark = $scope.TravelGroupEdit.Remark;
                $scope.data.ExchangeRateCaptures = $scope.TravelGroupRequest.ExchangeRateCaptures;
                $scope.data.TravelSchedulePlaces = $scope.TravelGroupRequest.TravelSchedulePlaces;
                $scope.data.ProjectCostDistributions = $scope.TravelGroupRequest.ProjectCostDistributions;
                $scope.data.ProjectCostDistributionTemp = angular.copy($scope.TravelGroupRequest.ProjectCostDistributions);
                $scope.data.DefaultTransportationTypeCode = $scope.TravelGroupRequest.Travelers[0].TransportationTypeCode;
                $scope.data.DefaultTransportationTypeName = $scope.TravelGroupRequest.Travelers[0].TransportationTypeName;
                // No default First Traveller wait for Country Type
                //$scope.TravelGroupRequest.Travelers[0].TransportationTypeCode = '';
                //$scope.TravelGroupRequest.Travelers[0].TransportationTypeName = '';
                $scope.data.Travelers = $scope.TravelGroupRequest.Travelers;
                $scope.data.GroupBudgets = $scope.TravelGroupRequest.GroupBudgets;
                if ($scope.data.Travelers[0].oCashAdvance.PaymentFlagCaptures.length > 0) {
                    $scope.data.PaymentFlagCaptures = $scope.data.Travelers[0].oCashAdvance.PaymentFlagCaptures;
                }
                if ($scope.TravelGroupEdit.PaymentFlagCaptures.length > 0) {
                    $scope.data.PaymentFlagCaptures = $scope.TravelGroupEdit.PaymentFlagCaptures;
                }
                $scope.data.Estimate = $scope.TravelGroupRequest.TotalAmount;

                $scope.data.PersonalVehicle = $scope.TravelGroupRequest.PersonalVehicle;
                $scope.TravelGroupRequest.DefaultCurrencyCode = $scope.TravelGroupEdit.DefaultCurrencyCode;
                $scope.data.DefaultCurrencyCode = $scope.TravelGroupEdit.DefaultCurrencyCode;
                //$scope.onChangeGroupType($scope.TravelGroupRequest.GroupSelect);
                $scope.GetTravelRequestConfig($scope.TravelGroupRequest);
                //$scope.CheckPersonalVehicle();
                //$scope.EvenGetExpenseTypeByTag();
            }


            /* ====== wizard form control ====== */
            $scope.wizardFormControl = {
                receiptIsActive: false,
                withoutReceiptIsActive: false
            };
            /* ====== !wizard form control ====== */

        }])
})();