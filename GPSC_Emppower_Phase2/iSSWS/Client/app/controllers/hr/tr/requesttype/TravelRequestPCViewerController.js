﻿(function () {
    angular.module('ESSMobile')
        .controller('TravelRequestPCViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$q', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $q) {
           
            $scope.content.Header = $scope.Text['TRAVELEXPENSE'].REQUESTWITHPETTYCASH_;
            $scope.ChildAction.SetData = function () {
            };


            $scope.ChildAction.LoadData = function () {

            };


            /* ====== wizard form control ====== */

            $scope.wizardFormControl = {
                receiptIsActive: false,
                withoutReceiptIsActive: false
            };

            /* ====== !wizard form control ====== */

            /* ====== Inital data before load control (Start)====== 
                AddBy: Ratchatawan W. (30 Mar 2017)
                Description: For initial data before load all control in this page
            */
            
            $scope.init = function () {
                var promise_service0 = $scope.getTextByCompany($scope.document.Requestor);
                var promise_service1 = InitialConfig();
                var promise_service2 = $scope.GetTravelModel();
                var promise_service3 = $scope.GetMasterdata();
                $q.all([promise_service0,promise_service1, promise_service2, promise_service3]).then(function (response) {

                    $scope.data.model.ready = true;
                    console.debug('init success')
                }, function (response) {

                    console.error('init error');

                });

            }

            function InitialConfig() {
                $scope.Page = "TravelExpense";
                console.log('InitialConfig');
                $scope.data.Readonly = true;
                $scope.data.TravelSeqEdit = false;
                $scope.data.IsExchangeRateEdit = false;
                $scope.data.IsTravelRequest = true;
                $scope.data.ExchangeRateCaptures = $scope.TravelGroupRequest.ExchangeRateCaptures;
                $scope.data.TravelSchedulePlaces = $scope.TravelGroupRequest.TravelSchedulePlaces;
                $scope.data.ProjectCostDistributions = $scope.TravelGroupRequest.ProjectCostDistributions;
                $scope.data.ProjectCostDistributionTemp = angular.copy($scope.TravelGroupRequest.ProjectCostDistributions);
                $scope.data.Travelers = $scope.TravelGroupRequest.Travelers;
                $scope.data.GroupBudgets = $scope.TravelGroupRequest.GroupBudgets;
                $scope.data.Travelers[0].RequestNo = $scope.document.RequestNo;
                $scope.data.PaymentFlagCaptures = $scope.TravelGroupRequest.PaymentFlagCaptures;
                $scope.data.Estimate = $scope.TravelGroupRequest.TotalAmount;
                $scope.data.PersonalVehicle = $scope.TravelGroupRequest.PersonalVehicle;
                $scope.data.DefaultCurrencyCode = $scope.TravelGroupRequest.DefaultCurrencyCode;
                $scope.CheckPersonalVehicle();
                $scope.GetTravelRequestConfig();
            }

            /* ====== Inital data before load control (Finish)====== */
        }])
    .directive('expand', function () {
        return {
            restrict: 'A',
            controller: ['$scope', function ($scope) {
                $scope.$on('onExpandAll', function (event, args) {
                    $scope.expanded = args.expanded;
                });
            }]
        };
    });
})();