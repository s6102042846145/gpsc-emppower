﻿(function () {
    angular.module('ESSMobile')
        .controller('PCRequestViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) { 
            $scope.TravelGroupConfig = { DefaultRequestGroupApprove: '', GroupMode: '', TravelType: '', Phone: '', MobilePhone: '' }
            $scope.TravelGroupRequest = angular.copy($scope.document.Additional.TravelGroupRequest[0]);
            $scope.Textcategory = 'TRAVELREQUEST';
            $scope.employeeData = getToken(CONFIG.USER);
            $scope.data = {};
            $scope.content.Header = $scope.Text[$scope.Textcategory].TITLE;


            $scope.ChildAction.SetData = function () {
            };


            $scope.ChildAction.LoadData = function () {

            };

            $scope.InitialConfig = function () {
                $scope.Page = "TravelExpense";
                console.log('InitialConfig');
                $scope.data = {};
                //for ExchangeRateControl
                $scope.data.Readonly = true;
                //$scope.data.TravelSeqEdit = false;
                $scope.data.IsExchangeRateEdit = false;
                $scope.data.IsPersonalCashAdvance = true;
                //$scope.data.IsTravelRequest = true;
                $scope.data.ExchangeRateCaptures = $scope.TravelGroupRequest.ExchangeRateCaptures;
                $scope.data.TravelSchedulePlaces = $scope.TravelGroupRequest.TravelSchedulePlaces;
                $scope.data.ProjectCostDistributions = $scope.TravelGroupRequest.ProjectCostDistributions;
                $scope.data.ProjectCostDistributionTemp = angular.copy($scope.TravelGroupRequest.ProjectCostDistributions);
                $scope.data.Travelers = $scope.TravelGroupRequest.Travelers;
                $scope.data.GroupBudgets = $scope.TravelGroupRequest.GroupBudgets;
                $scope.data.Travelers[0].RequestNo = $scope.document.RequestNo;
                $scope.data.PaymentFlagCaptures = $scope.data.Travelers[0].oPettyCash.PaymentFlagCaptures;

                $scope.data.CAEstimate = $scope.TravelGroupRequest.Travelers[0].TotalAmount;
                $scope.data.TotalAmount = $scope.TravelGroupRequest.Travelers[0].oCashAdvance.TotalAmount;
                $scope.data.model = { "ready": false };
                $scope.GetTravelModel();
                //Load Configuratioin for Search Data
                var URL = CONFIG.SERVER + 'HRTR/GetTravelRequestConfig';
                var oRequestParameter = { InputParameter: { 'ReuestType': 'TR' }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor }
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.TravelGroupConfig.DefaultRequestGroupApprove = response.data['DEFAULTREQUESTGROUPAPPROVE'];
                    $scope.TravelGroupConfig.GroupMode = response.data['GROUPMODE'];
                    $scope.TravelGroupConfig.TravelType = response.data['TRAVELTYPE'];
                    $scope.data.Mode = response.data['CASHADVANCEMODE'];
                    // bind default value 
                    //console.log($scope.TravelGroupConfig);

                }, function errorCallback(response) {
                    // Error
                    console.log('error TravelRequestEditorController InitialConfig.', response);
                });


                var getReversalReason = function () {
                    var URL = CONFIG.SERVER + 'HRTR/GetAllReversalReasonCode';
                    var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.reversalReason = response.data;
                    }, function errorCallback(response) {
                        $scope.reversalReason = [];
                        console.log('error GetAllReversalReasonCode.', response);
                    });
                }
                getReversalReason();

                //if (!$scope.document.Additional.TravelGroupRequest[0].Travelers[0].oCashAdvance.IsOverridePostingDate) {
                //    $scope.document.Additional.Travelers[0].oCashAdvance.PostingDate = $filter('date')(new Date(), 'yyyy-MM-ddT00:00:00');
                //}

                //if (!$scope.document.Additional.TravelGroupRequest[0].Travelers[0].oCashAdvance.IsOverrideBaseLineDate) {
                //    $scope.document.Additional.Travelers[0].oCashAdvance.BaseLineDate = $filter('date')(new Date(), 'yyyy-MM-ddT00:00:00');
                //}

                var getExpenseRateTypeForLookup = function () {
                    var URL = CONFIG.SERVER + 'HRTR/GetExpenseRateTypeForLookup';
                    var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        if (angular.isUndefined($scope.masterData)) {
                            $scope.masterData = {};
                        }
                        $scope.masterData.objExpenseRateTypeForLookup = response.data;
                    }, function errorCallback(response) {
                        console.log('error getExpenseRateTypeForLookup.', response);
                    });
                };
                getExpenseRateTypeForLookup();
            }

            $scope.GetTravelModel = function () {
                var URL = CONFIG.SERVER + 'HRTR/GetTravelModel';
                var oRequestParameter = { InputParameter: { "RequestNo": $scope.document.RequestNo }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.data.tmpTravelSchedulePlace = response.data.TravelSchedulePlace[0];
                    $scope.data.tmpProjectCostDistribution = response.data.ProjectCostDistribution[0];
                    $scope.data.tmpExchangeRateCapture = response.data.ExchangeRateCapture[0];
                    $scope.data.tmpTraveler = response.data.Traveler[0];
                    $scope.data.tmpGroupBudget = response.data.GroupBudget[0];
                    $scope.data.tmpPersonalBudget = response.data.PersonalBudget[0];
                    $scope.data.model.ready = true;
                },
                function errorCallback(response) {
                    console.log('error InitialConfig.', response);
                });
            }


            //GET CURRENT TRAVELER
            $scope.GetEditTraveler = function (specific_employeeid) {
                var index = $.map($scope.data.Travelers, function (obj, index) {
                    if (obj.EmployeeID == specific_employeeid) {
                        return index;
                    }
                })
                return index;
            }

            $scope.setSelectedPostingDate = function (newDate) {
                $scope.document.Additional.Travelers[0].oCashAdvance.PostingDate = $filter('date')(newDate, 'yyyy-MM-ddT00:00:00');
                $scope.document.Additional.Travelers[0].oCashAdvance.IsOverridePostingDate = true;
            };

            $scope.setSelectedBaseLineDate = function (newDate) {
                $scope.document.Additional.Travelers[0].oCashAdvance.BaseLineDate = $filter('date')(newDate, 'yyyy-MM-ddT00:00:00');
                $scope.document.Additional.Travelers[0].oCashAdvance.IsOverrideBaseLineDate = true;
            };

            $scope.getReasonCodeText = function (reasonCode) {
                if (angular.isDefined($scope.reversalReason) && $scope.reversalReason != null) {
                    for (var i = 0; i < $scope.reversalReason.length; i++) {
                        return $scope.reversalReason[i].Description;
                    }
                }
                return '';
            };

        }]);
})();