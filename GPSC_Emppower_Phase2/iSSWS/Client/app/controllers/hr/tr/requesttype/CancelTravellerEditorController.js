﻿(function () {
    angular.module('ESSMobile')
        .controller('CancelTravellerEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {
            $scope.Textcategory = 'TRAVELREQUEST';
            $scope.TRAVELMODE = "P";
            $scope.content.Header = $scope.Text[$scope.Textcategory].TITLE;
            $scope.data = {};
            $scope.data.IsRemarkEdit = true;
            $scope.data.IsReturnEdit = true;
            $scope.IsGroupManager = false;
            $scope.NewGroupManagerDataSelector = [];
            $scope.data.CashAdvanceReturns = [];
            $scope.ChildAction.SetData = function () {
                $scope.TravelPersonalCancel = $scope.document.Additional;
                var oEmployeeId = $scope.TravelPersonalCancel.EmployeeID;
                var oreferRequestNo = $scope.TravelPersonalCancel.TravelRequestNo;
                if (angular.isDefined($scope.document.RequestNo)) {
                    $scope.TravelPersonalCancel.RequestNo = $scope.document.RequestNo;
                }
                if (angular.isDefined($scope.referRequestNo) && $scope.referRequestNo != "") {
                    oreferRequestNo = $scope.referRequestNo;
                    $scope.TravelPersonalCancel.TravelRequestNo = oreferRequestNo;
                }
                if ($scope.document.NewRequest) {
                    console.log('This is NEW request.');
                    oEmployeeId = $scope.itemKeys[0];
                    $scope.TravelPersonalCancel.EmployeeID = oEmployeeId;
                    $scope.GetTravelRequest(oreferRequestNo);
                }
                else {
                    console.log('This is EDIT request.');
                    $scope.data.CashAdvance = $scope.TravelPersonalCancel.TravelGroupRequest.Travelers[0].oCashAdvance;
                    $scope.data.Remark = $scope.TravelPersonalCancel.Remark;
                    $scope.data.CashAdvanceReturns = $scope.TravelPersonalCancel.CashAdvanceReturns;
                    //$scope.CheckIsGroupManager($scope.TravelPersonalCancel.TravelRequestNo, $scope.TravelPersonalCancel.EmployeeID);
                    $scope.GetNewGroupManagerDataSelector($scope.TravelPersonalCancel.TravelRequestNo, true);
                    //$scope.NewGroupManagerDataSelector = angular.copy($scope.TravelPersonalCancel.NewGroupManagerDataSelector);
                    $scope.IsGroupManager = true;
                }
                //$scope.CheckIsGroupManager();
            };

            $scope.onSelectedNewGroupManager = function () {
                $scope.GetGroupManagerData();
            };

            $scope.ChildAction.LoadData = function () {
                //$scope.TravelPersonalCancel.RequestNo = $scope.document.RequestNo;
                //$scope.TravelPersonalCancel.TravelRequestNo = $scope.referRequestNo;
                //$scope.TravelPersonalCancel.EmployeeID = $scope.itemKeys[0];
                if (angular.isDefined($scope.TravelPersonalCancel.NewGroupManager.EmployeeID)){
                    $scope.TravelPersonalCancel.NewGroupManager = $scope.TravelPersonalCancel.NewGroupManager.EmployeeID;
                }
                $scope.TravelPersonalCancel.Remark = $scope.data.Remark;
                $scope.TravelPersonalCancel.CashAdvanceReturns = $scope.data.CashAdvanceReturns;
                console.log('TravelPersonalCancel : ', $scope.TravelPersonalCancel);
            };

            $scope.CheckIsGroupManager = function (oreferRequestNo,EmployeeID) {
                var oRequestParameter = {
                    InputParameter: { "referRequestNo": oreferRequestNo }
                                                , CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor
                };
                var URL = CONFIG.SERVER + 'HRTR/CheckGroupManager';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.IsGroupManager = (response.data == EmployeeID);
                    //$scope.IsGroupManager = ($scope.TravelPersonalCancel.TravelGroupRequest.GroupManager == $scope.TravelPersonalCancel.EmployeeID);
                    if ($scope.IsGroupManager){ //&& $scope.TravelPersonalCancel.TravelGroupRequest.Travelers.length > 1) {
                        $scope.GetNewGroupManagerDataSelector(oreferRequestNo);
                        //$scope.NewGroupManagerDataSelector = angular.copy($scope.TravelPersonalCancel.TravelGroupRequest.Travelers);
                        //$scope.NewGroupManagerDataSelector.splice(findWithAttr($scope.NewGroupManagerDataSelector, 'EmployeeID', $scope.TravelPersonalCancel.EmployeeID), 1);
                        //$scope.TravelPersonalCancel.NewGroupManager = $scope.NewGroupManagerDataSelector[0];
                        //$scope.GetGroupManagerData($scope.TravelPersonalCancel.NewGroupManager.EmployeeID);
                    }
                    var date = new Date();
                    var dateStr = date.getFullYear() + '-' + ("0" + (date.getMonth() + 1)).slice(-2) + '-' + ("0" + date.getDate()).slice(-2);
                    if ($scope.data.CashAdvance != null && $scope.data.CashAdvance.TotalAmount > 0 && $scope.data.CashAdvance.Status != 'CANCELLED') {
                        //if ($scope.data.CashAdvanceReturns.length == 0) {
                        //    $scope.data.CashAdvanceReturns.push({
                        //        "RequestNo": $scope.document.RequestNo,
                        //        "EmployeeID": $scope.TravelPersonalCancel.EmployeeID,
                        //        "ReturnID": 1,
                        //        "ReturnDate": $filter('date')(dateStr, 'yyyy-MM-ddT00:00:00'),
                        //        "ReturnAmount": 0,
                        //        "FIDocID": '',
                        //        "FileAttachment": "\\" + $scope.document.RequestNo + "\\" + $scope.TravelPersonalCancel.EmployeeID + "\\1\\",
                        //        "FileName": '',
                        //        "FileStringBase64": ''
                        //    });
                        //}
                    }
                }, function errorCallback(response) {
                    // Error
                    console.log('error CancelTravellerEditorController CheckGroupManager.', response);
                });
            };

            $scope.GetGroupManagerData = function () {
                var oRequestParameter = {
                    InputParameter: { "EmployeeID": $scope.TravelPersonalCancel.NewGroupManager }
                                                , CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor
                };
                var URL = CONFIG.SERVER + 'HRTR/GetGroupManagerData';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.GroupManagerData = response.data;
                    //console.log('$scope.GroupManagerData : ', $scope.GroupManagerData);
                }, function errorCallback(response) {
                    // Error
                    console.log('error CancelTravellerEditorController GetGroupManagerData.', response);
                });
            }

            $scope.GetNewGroupManagerDataSelector = function (referRequestNo,notDefualt) {
                var oRequestParameter = {
                    InputParameter: { "RequestNo": referRequestNo }
                                                , CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor
                };
                var URL = CONFIG.SERVER + 'HRTR/GetAvaiableTravelerForCancelTraveller';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.NewGroupManagerDataSelector = response.data;
                    $scope.TravelPersonalCancel.NewGroupManagerDataSelector = response.data;
                    if (!notDefualt) {
                        $scope.TravelPersonalCancel.NewGroupManager = $scope.NewGroupManagerDataSelector[0].EmployeeID;
                    }
                    $scope.GetGroupManagerData();
                }, function errorCallback(response) {
                    // Error
                    console.log('error CancelTravellerEditorController GetTravelerList.', response);
                });
            }

            $scope.GetTravelRequest = function (referRequestNo) {
                var oRequestParameter = {
                    InputParameter: { "REQUESTNO": referRequestNo, "GROUPMANAGERID": $scope.document.Requestor.EmployeeID }
                                                , CurrentEmployee: getToken(CONFIG.USER)
                                                , Creator: $scope.document.Creator, Requestor: $scope.document.Requestor
                };
                var URL = CONFIG.SERVER + 'HRTR/GetTravelRequest';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.TravelPersonalCancel.TravelGroupRequest = response.data;
                    $scope.data.CashAdvance = $scope.TravelPersonalCancel.TravelGroupRequest.Travelers[findWithAttr($scope.TravelPersonalCancel.TravelGroupRequest.Travelers, 'EmployeeID', $scope.TravelPersonalCancel.EmployeeID)].oCashAdvance;
                    $scope.CheckIsGroupManager(referRequestNo, $scope.TravelPersonalCancel.EmployeeID);
                }, function errorCallback(response) {
                    // Error
                    console.log('error CancelTravellerEditorController GetTravelRequest.', response);
                });
            }

            function findWithAttr(array, attr, value) {
                for (var i = 0; i < array.length; i += 1) {
                    if (array[i][attr] === value) {
                        return i;
                    }
                }
            }
            $scope.ChildAction.SetData();
            //Get CostCenter
            var URL = CONFIG.SERVER + 'HRTR/GetCostCenter';
            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                if (!$scope.settings.Master) $scope.settings.Master = [];
                $scope.settings.Master.CostCenterList = response.data;
                //$scope.objCostcenter = response.data;
                //console.log('objCostcenter', $scope.objCostcenter);
            }, function errorCallback(response) {
                console.log('error GeneralExpenseEditorController GetCostcenter.', response);
            });

            // Get Organization
            var URL = CONFIG.SERVER + 'HRTR/GetOrganization';
            var oRequestParameter = { InputParameter: { "CreateDate": $scope.document.CreatedDate }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                if (!$scope.settings.Master) $scope.settings.Master = [];
                $scope.settings.Master.OrgUnitList = response.data;
                //$scope.objOrganization = response.data;
            }, function errorCallback(response) {
                console.log('error TravelExpenseGroupEstimateEditorController objOrganization.', response);
            });

            // Get InternalOrder All
            var URL = CONFIG.SERVER + 'HRTR/GetInternalOrderAll/';
            var oRequestParameter = { InputParameter: '', CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                if (!$scope.settings.Master) $scope.settings.Master = [];
                $scope.settings.Master.IOList = response.data;
                //var temp = response.data;
                //$scope.allIO = temp;
            }, function errorCallback(response) {
                console.log('error GeneralExpenseEditorController GetInternalOrderAll.', response);
            });
            
        }]);
})();