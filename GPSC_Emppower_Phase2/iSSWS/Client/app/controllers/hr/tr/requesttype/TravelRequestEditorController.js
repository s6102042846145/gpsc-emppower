﻿(function () {
    angular.module('ESSMobile')
        .controller('TravelRequestEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG','$q', function ($scope, $http, $routeParams, $location, $filter, CONFIG,$q) {
            $scope.content.Header = $scope.Text['TRAVELEXPENSE'].TRAVEL_DETAIL
            //$scope.data.TravelPersonalEdit = false;
            //TravelGroupRequest
            $scope.ChildAction.SetData = function () {
            }
            $scope.ChildAction.LoadData = function () {
                console.log("LoadData");
                if ($scope.data.GroupBudgets && $scope.data.GroupBudgets.length) {
                    for (var i = 0; i < $scope.data.GroupBudgets.length; i++) {
                        if (!$scope.data.GroupBudgets[i] || !$scope.data.GroupBudgets[i].ExpenseTypeGroupID) {
                            $scope.data.GroupBudgets[i].ExpenseTypeGroupID = 0;
                            $scope.data.Travelers[0].oCashAdvance.GroupCashAdvances[i].ExpenseTypeGroupID = 0;
                        }
                        if (!$scope.data.GroupBudgets[i] || !$scope.data.GroupBudgets[i].CostCenter) {
                            $scope.data.GroupBudgets[i].CostCenter = '0';
                        }
                        if (!$scope.data.GroupBudgets[i] || !$scope.data.GroupBudgets[i].AlternativeIOOrg) {
                            $scope.data.GroupBudgets[i].AlternativeIOOrg = '0';
                        }
                        if (!$scope.data.GroupBudgets[i] || !$scope.data.GroupBudgets[i].IO) {
                            $scope.data.GroupBudgets[i].IO = '0';
                        }
                        if (!$scope.data.GroupBudgets[i] || !$scope.data.GroupBudgets[i].TotalAmount) {
                            $scope.data.GroupBudgets[i].TotalAmount = 0;
                            $scope.data.Travelers[0].oCashAdvance.GroupCashAdvances[i].Amount = 0;
                            $scope.data.Travelers[0].oCashAdvance.GroupCashAdvances[i].TotalAmount = 0;
                        }
                    }
                }
                $scope.TravelGroupRequest.RequestNo = $scope.document.RequestNo;
                $scope.TravelGroupRequest.TravelMode = ($scope.TravelGroupRequest.GroupSelect) ? "G" : "P";
                $scope.TravelGroupRequest.ExchangeRateCaptures = $scope.data.ExchangeRateCaptures;
                $scope.TravelGroupRequest.TravelSchedulePlaces = $scope.data.TravelSchedulePlaces;
                $scope.TravelGroupRequest.ProjectCostDistributions = $scope.data.ProjectCostDistributions;
                $scope.TravelGroupRequest.Travelers = $scope.data.Travelers;
                $scope.TravelGroupRequest.GroupBudgets = $scope.data.GroupBudgets;
                $scope.TravelGroupRequest.PersonalVehicle = $scope.data.PersonalVehicle; // ($scope.data.PersonalVehicleSelect == true) ? 1 : 2;
                $scope.TravelGroupRequest.DefaultCurrencyCode = $scope.data.DefaultCurrencyCode;
                $scope.TravelGroupRequest.TotalAmount = $scope.data.Estimate;
                $scope.TravelGroupRequest.Travelers[0].oCashAdvance.PaymentFlagCaptures = [];
                $scope.TravelGroupRequest.PaymentFlagCaptures = $scope.data.PaymentFlagCaptures;
                $scope.CheckUploadFiles();
                $scope.document.Additional = $scope.TravelGroupRequest;
            }
         
            /* ====== wizard form control ====== */
            $scope.wizardFormControl = {
                receiptIsActive: false,
                withoutReceiptIsActive: false
            };
            /* ====== !wizard form control ====== */


            /* ====== Inital data before load control (Start)====== 
                AddBy: Ratchatawan W. (30 Mar 2017)
                Description: For initial data before load all control in this page
            */

           

            $scope.init = function () {
                var promise_service0 = $scope.getTextByCompany($scope.document.Requestor);
                var promise_service1 = InitialConfig();
                var promise_service2 = $scope.GetTravelModel();
                var promise_service3 = $scope.GetMasterdata();
                $q.all([promise_service0,promise_service1, promise_service2, promise_service3]).then(function (response) {

                    $scope.data.model.ready = true;
                    console.debug('init success')
                }, function (response) {

                    console.error('init error');

                });

            }

            function InitialConfig() {
                $scope.setBlockAction(true, $scope.Text["SYSTEM"]["PROCESSING"]);
                $scope.Page = "TravelExpense";
                console.log('InitialConfig');

                $scope.data.Readonly = false;
                $scope.data.TravelSeqEdit = false;
                $scope.data.IsExchangeRateEdit = false;
                $scope.data.IsTravelRequest = true;
                $scope.data.ExchangeRateCaptures = $scope.TravelGroupRequest.ExchangeRateCaptures;
                $scope.data.TravelSchedulePlaces = $scope.TravelGroupRequest.TravelSchedulePlaces;
                $scope.data.ProjectCostDistributions = $scope.TravelGroupRequest.ProjectCostDistributions;
                $scope.data.ProjectCostDistributionTemp = angular.copy($scope.TravelGroupRequest.ProjectCostDistributions);
                for (var i = 0; i < $scope.data.ProjectCostDistributionTemp.length; i++) {
                    $scope.data.ProjectCostDistributionTemp[i].Name = $scope.data.ProjectCostDistributionTemp[i].ProjectName;
                    $scope.data.ProjectCostDistributions[i].Name = $scope.data.ProjectCostDistributionTemp[i].ProjectName;
                }
                $scope.data.Travelers = $scope.TravelGroupRequest.Travelers;
                $scope.data.GroupBudgets = $scope.TravelGroupRequest.GroupBudgets;
                $scope.data.Travelers[0].RequestNo = $scope.document.RequestNo;
                $scope.data.PaymentFlagCaptures = $scope.TravelGroupRequest.PaymentFlagCaptures;
                $scope.data.Estimate = $scope.TravelGroupRequest.TotalAmount;

                $scope.data.PersonalVehicle = $scope.TravelGroupRequest.PersonalVehicle;
                $scope.data.DefaultCurrencyCode = $scope.TravelGroupRequest.DefaultCurrencyCode;
                $scope.CheckPersonalVehicle();
                $scope.GetTravelRequestConfig();
            }
            /* ====== Inital data before load control (Finish)====== */


       
            //guide.addStep("#console_step_1", "สวัดดีครับ ยินดีต้อนรับเข้าสู่ระบบ DXCare, ก่อนอื่นผมขอแนะนำตัวเว็บและการใช้งานหน้าจอคล้าวๆก่อนนะครับ");
            //guide.addStep("#console_step_1", "หน้าจอแรกเนี้ชื่อว่า Dashboard เป็นหน้าจอที่ท่านสามารถเข้าใช้การขอเบิกค่าใช้จ่ายในการเดินทาง และ ดูข้อมูลในรูปแบบของการ Overview ของตัวท่านเอง");
           
          
            $scope.startGuideLine_RequestEditor = function () {
                var guide = $("body").guide();
                guide.addStep("#request_box_1", "ยินดีต้องรับเข้าสู้หน้าจอการสร้าง Request ครับ หากไม่ต้องหให้เราอธิบายการทำงานสามารถกดที่พื้นที่สีดำ เริ่มกันเลย");
                guide.addStep("#request_box_2", "ส่วนที่หนึ่ง เป็นประเทภการเดินทาง เราสามารถ เลือกประเภทของการเดินทางได้ทั้งหมดสี่แบบคือ ปฎิบัติงาน ประชุม อบรบ และ สัมมนา");
                guide.addStep("#request_box_3", "ส่วนที่ต่อมาเป็น Field สำหรับกรอก เบอร์โทรศัพโดยปกติแล้วระบบจะ Default ค่ามาให้");
                guide.addStep("#request_box_4", "และนี้ก็คือเบอร์โทรศัพเช่นเดียวกัน");
                guide.addStep("#request_box_5", "ข้อมูลเหตุผลและความจำเป็นต้องกรอกทุกครั้ง เพื่อให้นายของท่านหรือเป็นบันทึกไว้ว่าท่านไปปฎิบัติงานที่ไหนอย่างไร");
                guide.addStep("#request_box_6", "ต่อมาเป็นปุ่มการสร้างสถานที่การเดินทาง ท่านสามารถสร้าง list ของสถานที่การเดินทางได้ที่นี้ และเป็นข้อมูลที่สำคัญที่สุดในการสร้างการขออนุมัติการเดินทาง หากท่านไม่เพิ่อมสถานที่การเดินทางก่อนแล้วท่านะจะไม่สามารถเพิ่มค่าใช้จ่ายใดใดได้  นอกจากนี้การสร้างนั้นจะกำหนดให้สร้างได้แค่การเดินทางที่เป็นต่างประเทศต่อต่างประเทศ และในประเทศต่อในประเทศเท่านั้น เพื่อความสะดวกในการ อณุมัติและการทำรายการ");
                guide.addStep("#request_box_7", "ท่านสามารถเพิ่มค่าใช้จ่ายรายบุคคลและเพิ่มผู้ร่วมเดินทางได้ที่นี้ โดยการคลิกที่เครื่องหมายดินสอทางด้านหลังชื่อของท่าน (ปุ่มจะแสดงก็ต่อเมื่อท่านได้ทำการเพิ่มสถานที่การเดินทางแล้ว) ");
                guide.addStep("#attachefile_manager", "หากท่านมีเอกสารเกี่ยวกับการเดินทางต้องการแนบเพิ่มเติมท่านสามารถ อัพโหลดไฟลได้ที่นี้");
                guide.addStep("#reasone_main_request", "ช่องนี้สำหรับใช้ในการใส่เหตุผลเพิ่มเติม ไม่จำเป็นต้องกรอกทุกครั้ง");
                guide.start();
            }

          

        }])
        .directive('expand', function () {
            return {
                restrict: 'A',
                controller: ['$scope', function ($scope) {
                    $scope.$on('onExpandAll', function (event, args) {
                        $scope.expanded = args.expanded;
                    });
                }]
            };
        });
})();
