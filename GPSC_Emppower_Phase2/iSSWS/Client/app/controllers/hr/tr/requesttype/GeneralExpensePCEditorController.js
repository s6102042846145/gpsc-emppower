﻿(function () {
    angular.module('ESSMobile')
        .controller('GeneralExpensePCEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {

            var employeeData = getToken(CONFIG.USER);

            $scope.ChildAction.SetData = function () {
                $scope.Page = 'GeneralExpense';
                $scope.Textcategory = 'EXPENSE';
                $scope.TextcategoryExpType = 'EXPENSETYPE';
                $scope.content.Header = $scope.Text['EXPENSE']['EXPENSE_PETTY'];
                

                $scope.document.Additional.RequestNo = $scope.document.RequestNo;

                /* CostCentet, Organization, IO dropdown */

                //$scope.getIOByCostCenter = function (CostCenter, isSetDefaultIO) {
                //    if (angular.isUndefined(isSetDefaultIO)) {
                //        isSetDefaultIO = false;
                //    }
                //    var URL = CONFIG.SERVER + 'HRTR/GetInternalOrderByCostCenter/';
                //    var oRequestParameter = { InputParameter: { "CostCenter": CostCenter }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                //    $http({
                //        method: 'POST',
                //        url: URL,
                //        data: oRequestParameter
                //    }).then(function successCallback(response) {
                //        $scope.objIO = response.data;
                //        if ($scope.objIO != null && $scope.objIO.length > 0 && isSetDefaultIO) {
                //            // set default
                //            $scope.document.Additional.IO = $scope.objIO[0].OrderID;
                //        }
                //        console.log('objIO.', $scope.objIO);
                //    }, function errorCallback(response) {
                //        console.log('error GetInternalOrderByCostCenter.', response);
                //    });
                //};

                $scope.getIOByOrganization = function (Org, isSetDefaultIO) {
                    if (angular.isUndefined(isSetDefaultIO)) {
                        isSetDefaultIO = false;
                    }
                    var URL = CONFIG.SERVER + 'HRTR/GetInternalOrderByOrgUnit/';
                    var oRequestParameter = { InputParameter: { "OrgUnit": Org }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.objIO = response.data;
                        if ($scope.objIO != null && $scope.objIO.length > 0 && isSetDefaultIO) {
                            // set default
                            $scope.document.Additional.IO = $scope.objIO[0].OrderID;
                        }
                        console.log('objIO.', $scope.objIO);
                    }, function errorCallback(response) {
                        console.log('error getIOByOrganization.', response);
                    });
                };

                $scope.changeIsAlternativeCostCenter = function (isAlternativeCostCenter) {
                    if (!isAlternativeCostCenter) {
                        $scope.document.Additional.CostCenter = $scope.masterData.objCostcenterDistribution[0].CostCenterCode;
                    }
                    //if (!$scope.document.Additional.IsAlternativeIOOrg) {
                    //    $scope.getIOByCostCenter($scope.document.Additional.CostCenter, true);
                    //}
                };

                $scope.changeIsAlternativeIOOrg = function (isAlternativeIOOrg) {
                    // change default
                    if (!isAlternativeIOOrg) {
                        if ($scope.document.Requestor.OrgUnit != null) {
                            $scope.document.Additional.AlternativeIOOrg = $scope.document.Requestor.OrgUnit;
                        }
                        //$scope.getIOByCostCenter($scope.document.Additional.CostCenter, true);
                        $scope.getIOByOrganization($scope.document.Additional.AlternativeIOOrg, true);
                    } else {
                        $scope.getIOByOrganization($scope.document.Additional.AlternativeIOOrg, true);
                    }
                };

                $scope.onSelectedCostCenter = function (selectedCostCenter) {
                    //if (!$scope.document.Additional.IsAlternativeIOOrg) {
                    //    $scope.getIOByCostCenter(selectedCostCenter.CostCenterCode, true);
                    //}
                };

                $scope.onSelectedOrganization = function (selectedOrganization) {
                    //if ($scope.document.Additional.IsAlternativeIOOrg) {
                    $scope.getIOByOrganization($scope.document.Additional.AlternativeIOOrg, true);
                    //}
                };

                /* !CostCentet, Organization, IO dropdown */

                /* Master data */

                $scope.masterDataReady = false;

                $scope.masterData = {
                    projectForLookup: null,
                    exchangeType: null,
                    employeeDataList: null,
                    carTypes: null,
                    flatRateLocations: null,
                    objVendor: null,
                    objCountry: null,
                    objCostcenterLookup: null,
                    objIOLookupObj: null,
                    setVatTypeDefault: null,
                    setPaymentMethod: null,
                    setPaymentSupplement: null,
                    setPaymentTerm: null,
                    oTranType: null,
                    objCostcenter: null,
                    objOrganization: null,
                    objCostcenterDistribution: null,
                    allProjects: null
                };

                $scope.data = {
                    IsEdit: true,
                    Readonly: false,
                    ProjectCostDistributions: null,
                    PaymentFlagCaptures: null,
                    tmpTravelSchedulePlace: null,
                    tmpProjectCostDistribution: null,
                    tmpExchangeRateCapture: null,
                    tmpTraveler: null,
                    tmpGroupBudget: null
                };

                

                var initialExpenseReceipt = function () {
                    var URL = CONFIG.SERVER + 'HRTR/InitialExpenseReceipt_New';
                    var oInputParameter = {
                        "CreateDate": $scope.document.CreatedDate
                    };
                    var oRequestParameter = { InputParameter: oInputParameter, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    return $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.masterData.projectForLookup = response.data.projectForLookup;
                        $scope.masterData.exchangeType = response.data.exchangeType;
                        $scope.masterData.employeeDataList = response.data.employeeDataList;
                        $scope.masterData.carTypes = response.data.carTypes;
                        $scope.masterData.flatRateLocations = response.data.flatRateLocations;
                        $scope.masterData.objVendor = response.data.objVendor;
                        $scope.masterData.objCountry = response.data.objCountry;
                        $scope.masterData.objCostcenterLookup = response.data.objCostcenterLookup;
                        $scope.masterData.objIOLookupObj = response.data.objIOLookupObj;
                        $scope.masterData.setVatTypeDefault = response.data.setVatTypeDefault;
                        $scope.masterData.setPaymentMethod = response.data.setPaymentMethod;
                        $scope.masterData.setPaymentSupplement = response.data.setPaymentSupplement;
                        $scope.masterData.setPaymentTerm = response.data.setPaymentTerm;
                        $scope.masterData.oTranType = response.data.oTranType;

                        $scope.data.tmpTravelSchedulePlace = response.data.TravelModel.TravelSchedulePlace[0];
                        $scope.data.tmpProjectCostDistribution = response.data.TravelModel.ProjectCostDistribution[0];
                        $scope.data.tmpExchangeRateCapture = response.data.TravelModel.ExchangeRateCapture[0];
                        $scope.data.tmpTraveler = response.data.TravelModel.Traveler[0];
                        $scope.data.tmpGroupBudget = response.data.TravelModel.GroupBudget[0];

                        $scope.masterData.objCostcenter = response.data.objCostcenter;
                        $scope.masterData.objOrganization = response.data.objOrganization;

                        var projectcodeMode = response.data.ProjectCodeMode.toString().toUpperCase();
                        $scope.settings.ProjectCodeMode = projectcodeMode;
                        var isOverrideCostCenter = response.data.EnableOverideCostCenter.toString().toLowerCase();
                        $scope.settings.EnableOverideCostCenter = (isOverrideCostCenter == 'true') ? true : false;
                        var isOverrideIO = response.data.EnableOverideIO.toString().toLowerCase();
                        $scope.settings.EnableOverideIO = (isOverrideIO == 'true') ? true : false;


                        var isEnableProjectCost = response.data.EnableProjectCost.toString().toLowerCase();
                        $scope.settings.EnableProjectCost = (isEnableProjectCost == 'true') ? true : false;

                        $scope.masterData.objCostcenterDistribution = response.data.objCostcenterDistribution;
                        $scope.masterData.allProjects = response.data.allProjects;

                        $scope.settings.ReceiptToleranceAllow = response.data.receiptToleranceAllow;

                        $scope.settings.ReceiptLessthanBeforevat = response.data.receiptLessthanBeforevat;

                        // SET 'RECEIPT_LIMIT_DATE_09182017'
                        $scope.RECEIPT_LIMIT_DATE = {
                            RP_LIMIT_MINDATE: response.data.RP_LIMIT_MINDATE,
                            RP_LIMIT_MAXDATE: response.data.RP_LIMIT_MAXDATE,
                            GE_LIMIT_MINDATE: response.data.GE_LIMIT_MINDATE,
                            GE_LIMIT_MAXDATE: response.data.GE_LIMIT_MAXDATE,
                            GE_ALLOW_SELECTDATE_OUTOFPERIOD: response.data.GE_ALLOW_SELECTDATE_OUTOFPERIOD,
                            RP_ALLOW_SELECTDATE_OUTOFPERIOD: response.data.RP_ALLOW_SELECTDATE_OUTOFPERIOD,
                            GE_VALIDATE_EXPENSETYPE_TIMECONSTRAIN: response.data.GE_VALIDATE_EXPENSETYPE_TIMECONSTRAIN,
                            RP_VALIDATE_EXPENSETYPE_TIMECONSTRAIN: response.data.RP_VALIDATE_EXPENSETYPE_TIMECONSTRAIN
                        }
                        return response;
                    }, function errorCallback(response) {
                        $scope.masterData = {
                            projectForLookup: null,
                            exchangeType: null,
                            employeeDataList: null,
                            carTypes: null,
                            flatRateLocations: null,
                            objVendor: null,
                            objCountry: null,
                            objCostcenterLookup: null,
                            objIOLookupObj: null,
                            setVatTypeDefault: null,
                            setPaymentMethod: null,
                            setPaymentSupplement: null,
                            setPaymentTerm: null,
                            oTranType: null,
                            objCostcenter: null,
                            objOrganization: null,
                            objCostcenterDistribution: null,
                            allProjects: null
                        };
                        $scope.settings.ReceiptToleranceAllow = 0;
                        return response;
                    });
                };
                var initPromise = initialExpenseReceipt();

                /* !Master data */

                if ($scope.document.NewRequest) {
                    console.log('This is NEW request.', angular.copy($scope.document));
                    // initial new request
                    $scope.document.Additional.IsAlternativeCostCenter = false;
                    $scope.document.Additional.IsAlternativeIOOrg = false;
                    $scope.document.Additional.AlternativeIOOrg = $scope.document.Requestor.OrgUnit;
                    //$scope.document.Additional.MobilePhone = angular.isDefined(employeeData.MobileNo) ? employeeData.MobileNo : '';
                    //$scope.document.Additional.Phone = angular.isDefined(employeeData.OfficeTelephoneNo) ? employeeData.OfficeTelephoneNo : '';

                    $scope.getIOByOrganization($scope.document.Additional.AlternativeIOOrg, true);
                } else {
                    console.log('This is EDIT request.', angular.copy($scope.document));
                    //if ($scope.document.Additional.IsAlternativeIOOrg) {
                    $scope.getIOByOrganization($scope.document.Additional.AlternativeIOOrg);
                    //} else {
                    //    $scope.getIOByCostCenter($scope.document.Additional.CostCenter);
                    //}
                }

                /* Child control data */

                var objProjects = $scope.document.Additional.ProjectCostDistributions;
                $scope.data.ProjectCostDistributions = (angular.isUndefined(objProjects) || objProjects == null) ? [] : angular.copy(objProjects);
                $scope.data.PaymentFlagCaptures = $scope.document.Additional.PaymentFlagCapture;

                /* !Child control data */

                initPromise.then(function (response) {
                    // success
                    $scope.document.Additional.ProjectCodeMode = $scope.settings.ProjectCodeMode;

                    if ($scope.masterData.objVendor != null && $scope.masterData.objVendor.length > 0) {
                        for (var i = 0; i < $scope.masterData.objVendor.length; i++) {
                            if ($scope.masterData.objVendor[i].TaxID != null) {
                                $scope.masterData.objVendor[i].TaxPersonalID = ' : ' + $scope.masterData.objVendor[i].TaxID;
                            } else {
                                if ($scope.masterData.objVendor[i].PersonalID != null) {
                                    $scope.masterData.objVendor[i].TaxPersonalID = ' : ' + $scope.masterData.objVendor[i].PersonalID;
                                } else {
                                    $scope.masterData.objVendor[i].TaxPersonalID = '';
                                }

                            }
                        }
                    }

                    $scope.objCostcenterLookup = $scope.masterData.objCostcenterLookup; /* remove later */
                    $scope.objIOLookupObj = $scope.masterData.objIOLookupObj /* remove later */

                    if ($scope.masterData.objCostcenterDistribution != null && $scope.masterData.objCostcenterDistribution.length > 0 && $scope.document.Additional.CostCenter == '') {
                        // set default
                        $scope.document.Additional.CostCenter = $scope.masterData.objCostcenterDistribution[0].CostCenterCode;
                        //$scope.getIOByCostCenter($scope.document.Additional.CostCenter, true);
                    }
                    console.log('----- InitialExpenseReceipt completed. -----');
                    $scope.masterDataReady = true;
                }, function (response) {
                    // fail
                    $scope.masterDataReady = false;
                });

                //var URL = CONFIG.SERVER + 'HRTR/GetAllVATType';
                //var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                //$http({
                //    method: 'POST',
                //    url: URL,
                //    data: oRequestParameter
                //}).then(function successCallback(response) {
                //    $scope.setVatType = response.data;
                //}, function errorCallback(response) {
                //    console.log('error GetAllVATType.', response);
                //});
            };
            $scope.ChildAction.SetData();

            $scope.ChildAction.LoadData = function () {
                //if (!angular.isUndefined($scope.document.Additional.ExpenseReportReceiptList)) {
                //    /* fix un-map list obj in table cell */
                //    //for (var key in $scope.document.Additional) {
                //    //    if ($scope.document.Additional.hasOwnProperty(key)) {
                //    //        alert(key + " -> " + $scope.document.Additional[key]);
                //    //    }
                //    //}
                //    //var obj = $scope.document.Additional;
                //    ////console.log('LoadData.', obj[Object.keys(obj)[0]][0]);
                //    //$scope.document.AdditionalJson = JSON.stringify(obj[Object.keys(obj)[0]][0]);
                //}

                /* Rebind child control data */

                $scope.document.Additional.ProjectCostDistributions = $scope.data.ProjectCostDistributions;
                $scope.document.Additional.PaymentFlagCapture = $scope.data.PaymentFlagCaptures;
                $scope.document.Additional.PettyCode = $scope.data.PettyCode;
                $scope.document.Additional.PettyName = $scope.data.PettyName;

                /* !Rebind child control data */
            };

            $scope.setSelectedBeginDate = function (selectedDate) {
                $scope.document.Additional.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                if ($scope.document.Additional.BeginDate > $scope.document.Additional.EndDate) {
                    $scope.document.Additional.EndDate = $scope.document.Additional.BeginDate;
                }
                console.log('begin date.', $scope.document.Additional.BeginDate);
                console.log('end date.', $scope.document.Additional.EndDate);
            };

            $scope.setSelectedEndDate = function (selectedDate) {
                $scope.document.Additional.EndDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                console.log('begin date.', $scope.document.Additional.BeginDate);
                console.log('end date.', $scope.document.Additional.EndDate);
            };

            /* ====== Wizard form control ====== */

            $scope.wizardFormControl = {
                receiptIsActive: false,
                withoutReceiptIsActive: false
            };

            //$scope.isFormWizardActive = function () {
            //    return $scope.wizardFormControl.receiptIsActive || $scope.wizardFormControl.withoutReceiptIsActive;
            //};

            /* ====== !Wizard form control ====== */

            /* Watcher */

            //$scope.grandTotal = 0;
            $scope.$watch('document.Additional.ExpenseReportReceiptList', function (newObj, oldObj) {
                // calculate grand total
                var sumTotal = 0;
                if (angular.isDefined(newObj) && newObj != null) {
                    for (var i = 0; i < newObj.length; i++) {
                        var amount = 0;
                        if (newObj[i].IsVisible) {
                            if (newObj[i].IsReceipt) {
                                amount = Number(newObj[i].LocalTotalAmount);
                            } else {
                                amount = Number(newObj[i].LocalNoVATTotalAmount);
                            }
                        }
                        amount = isNaN(amount) ? 0 : amount;
                        sumTotal += amount;
                    }
                }
                //$scope.grandTotal = sumTotal;
                $scope.document.Additional.TotalAmount = sumTotal;
                console.log('GrandTotal.', sumTotal);
            }, true);

            /* !Watcher */

        }]);
})();