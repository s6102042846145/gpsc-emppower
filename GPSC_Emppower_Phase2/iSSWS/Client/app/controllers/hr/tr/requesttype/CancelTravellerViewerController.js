﻿(function () {
    angular.module('ESSMobile')
        .controller('CancelTravellerViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, CONFIG) {
            $scope.IsRemarkEdit = false;
            $scope.IsGroupManager = true;
            $scope.referRequestNo = '';
            $scope.TRAVELMODE = "P";
            $scope.data = {};
            $scope.data.Readonly = true;
            //$scope.NewGroupManagerDataSelector = [];
            $scope.Textcategory = 'TRAVELREQUEST';
            $scope.content.Header = $scope.Text[$scope.Textcategory].TITLE;

            $scope.ChildAction.SetData = function () {
                console.log('111');
                $scope.referRequestNo = $scope.document.Additional.TravelRequestNo;
                $scope.TravelPersonalCancel = $scope.document.Additional;
                //$scope.TravelGroupCancel = $scope.document.Additional.TravelGroupCancel[0];
                $scope.data.Remark = $scope.TravelPersonalCancel.Remark;

                $scope.data.CashAdvance = $scope.TravelPersonalCancel.TravelGroupRequest.Travelers[findWithAttr($scope.TravelPersonalCancel.TravelGroupRequest.Travelers, 'EmployeeID', $scope.TravelPersonalCancel.EmployeeID)].oCashAdvance;
                $scope.IsGroupManager = ($scope.TravelPersonalCancel.TravelGroupRequest.GroupManager == $scope.TravelPersonalCancel.EmployeeID);
                /*if ($scope.IsGroupManager && $scope.TravelPersonalCancel.TravelGroupRequest.Travelers.length > 1) {
                    
                    //$scope.NewGroupManagerDataSelector = angular.copy($scope.TravelPersonalCancel.TravelGroupRequest.Travelers);
                    //$scope.NewGroupManagerDataSelector.splice(findWithAttr($scope.NewGroupManagerDataSelector, 'EmployeeID', $scope.TravelPersonalCancel.EmployeeID), 1);
                    //$scope.TravelPersonalCancel.NewGroupManager = $scope.NewGroupManagerDataSelector[0].EmployeeID; //$scope.NewGroupManagerDataSelector[findWithAttr($scope.NewGroupManagerDataSelector, 'EmployeeID', $scope.TravelPersonalCancel.NewGroupManager)];
                    //$scope.GetGroupManagerData($scope.TravelPersonalCancel.NewGroupManager);
                }*/
                $scope.GetNewGroupManagerDataSelector($scope.TravelPersonalCancel.TravelRequestNo, true);
                if ($scope.data.CashAdvance != null && $scope.data.CashAdvance.TotalAmount > 0) {
                    $scope.data.CashAdvanceReturns = $scope.TravelPersonalCancel.CashAdvanceReturns;
                }
                
            };

            $scope.ChildAction.LoadData = function () {
                $scope.document.Additional = $scope.TravelPersonalCancel;
            };

            $scope.GetNewGroupManagerDataSelector = function (referRequestNo, notDefualt) {
                var oRequestParameter = {
                    InputParameter: { "RequestNo": referRequestNo }
                                                , CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor
                };
                var URL = CONFIG.SERVER + 'HRTR/GetAvaiableTravelerForCancelTraveller';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.NewGroupManagerDataSelector = response.data;
                    $scope.TravelPersonalCancel.NewGroupManagerDataSelector = response.data;
                    if (!notDefualt) {
                        $scope.TravelPersonalCancel.NewGroupManager = $scope.NewGroupManagerDataSelector[0].EmployeeID;
                    }
                    $scope.GetGroupManagerData($scope.TravelPersonalCancel.NewGroupManager);
                }, function errorCallback(response) {
                    // Error
                    console.log('error CancelTravellerEditorController GetTravelerList.', response);
                });
            }

            $scope.GetGroupManagerData = function (EmployeeID) {
                var oRequestParameter = {
                    InputParameter: { "EmployeeID": EmployeeID }
                                                , CurrentEmployee: getToken(CONFIG.USER)
                                                , Creator: $scope.document.Creator
                                                , Requestor: $scope.document.Requestor
                };
                var URL = CONFIG.SERVER + 'HRTR/GetGroupManagerData';
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.GroupManagerData = response.data;
                    //console.log('$scope.GroupManagerData : ', $scope.GroupManagerData);
                }, function errorCallback(response) {
                    // Error
                    console.log('error CancelTravellerEditorController GetGroupManagerData.', response);
                });
            }

            function findWithAttr(array, attr, value) {
                for (var i = 0; i < array.length; i += 1) {
                    if (array[i][attr] === value) {
                        return i;
                    }
                }
            }
            $scope.ChildAction.SetData();
            //Get CostCenter
            var URL = CONFIG.SERVER + 'HRTR/GetCostCenter';
            var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                if (!$scope.settings.Master) $scope.settings.Master = [];
                $scope.settings.Master.CostCenterList = response.data;
                //$scope.objCostcenter = response.data;
                //console.log('objCostcenter', $scope.objCostcenter);
            }, function errorCallback(response) {
                console.log('error GeneralExpenseEditorController GetCostcenter.', response);
            });

            // Get Organization
            var URL = CONFIG.SERVER + 'HRTR/GetOrganization';
            var oRequestParameter = { InputParameter: { "CreateDate": $scope.document.CreatedDate }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                if (!$scope.settings.Master) $scope.settings.Master = [];
                $scope.settings.Master.OrgUnitList = response.data;
                //$scope.objOrganization = response.data;
            }, function errorCallback(response) {
                console.log('error TravelExpenseGroupEstimateEditorController objOrganization.', response);
            });

            // Get InternalOrder All
            var URL = CONFIG.SERVER + 'HRTR/GetInternalOrderAll/';
            var oRequestParameter = { InputParameter: '', CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
            $http({
                method: 'POST',
                url: URL,
                data: oRequestParameter
            }).then(function successCallback(response) {
                if (!$scope.settings.Master) $scope.settings.Master = [];
                $scope.settings.Master.IOList = response.data;
                //var temp = response.data;
                //$scope.allIO = temp;
            }, function errorCallback(response) {
                console.log('error GeneralExpenseEditorController GetInternalOrderAll.', response);
            });
            

        }]);
})();