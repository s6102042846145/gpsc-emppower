﻿
(function () {
    angular.module('ESSMobile')
        .controller('ReportWOTravelEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', '$window', '$q', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, $window, $q, CONFIG) {



            // init valiable
            $scope.TravelGroupConfig = { GroupMode: '', TravelType: '', Phone: '', MobilePhone: '' }
            $scope.loader.childRequestEnable = true;
            $scope.masterDataReady = false;
            $scope.masterData = {
                projectForLookup: null,
                exchangeType: null,
                employeeDataList: null,
                carTypes: null,
                flatRateLocations: null,
                objVendor: null,
                objCountry: null,
                objCostcenterLookup: null,
                objIOLookupObj: null,
                setVatTypeDefault: null,
                setPaymentMethod: null,
                setPaymentSupplement: null,
                setPaymentTerm: null,
                oTranType: null,
                objCostcenter: null,
                objOrganization: null,
                objCostcenterDistribution: null,
                allProjects: null,
                objExpenseRateTypeForLookup: null,
                travelCurrencyList: null
            };
            $scope.data = {
                IsEdit: true,
                Readonly: false,
                IsReturnEdit: true,
                IsExchangeRateEdit: true,
                ProjectCostDistributions: null,
                TravelSchedulePlaces: null,
                ExchangeRateCaptures: null,
                Travelers: null,
                GroupBudgets: null,
                PerdiumList: null,
                PaymentFlagCaptures: null,
                tmpTravelSchedulePlace: null,
                tmpProjectCostDistribution: null,
                tmpExchangeRateCapture: null,
                tmpTraveler: null,
                tmpGroupBudget: null
            };
            $scope.settings = {
                ProjectCodeMode: '',
                EnableOverideCostCenter: false,
                EnableOverideIO: false,
                ExpenseTypeGroupInfo: {
                    prefix: 'TR',
                    travelTypeID: 1,
                    isDomestic: 'D',
                    groupTagName: "",
                    tagNameReceipt: "",
                    tagNameNoReceipt: ""
                },
                MaximumEmpSubGroup: 99,
                ReceiptToleranceAllow: 0
            };
            var employeeData = getToken(CONFIG.USER);



            //function
            var return_InitReportWOTravel;
            $scope.init_ReportWOTravelEditor = function () {
                $scope.data = {};
                $scope.data.Readonly = false;
                $scope.data.TravelSeqEdit = false;
                $scope.data.IsExchangeRateEdit = false;
                $scope.data.IsTravelRequest = true;
                var promise_InitReportWOTravel = InitReportWOTravel();
                var promise_GetTravelRequestConfig = GetTravelRequestConfig();
                var promise_InitialExpenseReceiptG1 = InitialExpenseReceiptG1();
                var promise_InitialExpenseReceiptG2 = InitialExpenseReceiptG2();
                var promise_InitialExpenseReceiptG3 = InitialExpenseReceiptG3();
                var promise_InitialExpenseReceiptG4 = InitialExpenseReceiptG4();
                var promise_InitialExpenseReceiptG5 = InitialExpenseReceiptG5();
                $q.all([
                    promise_InitReportWOTravel,
                    promise_GetTravelRequestConfig,
                    promise_InitialExpenseReceiptG1,
                    promise_InitialExpenseReceiptG2,
                    promise_InitialExpenseReceiptG3,
                    promise_InitialExpenseReceiptG4,
                    promise_InitialExpenseReceiptG5]
                ).then(function (response) {
                    
                    var doc = $scope.document.Additional.ReportWOTravel[0];
                    // If create document case
                    if ($scope.document.RequestID == 0) {
                        doc.TravelTypeID = 1;
                        
                        doc.CostCenter = angular.copy(doc.Travelers[0].CostCenter);
                        doc.CostCenter_name = angular.copy(doc.Travelers[0].CostCenterName);
                        console.debug($scope.document);
                    }
                    // If edit document case
                    else {

                    }
                    //console.debug($scope.document);

                    //console.debug($scope.document.Additional.ReportWOTravel[0]);

                    //$scope.document.Additional.ReportWOTravel[0] = return_InitReportWOTravel;
                    //console.debug(return_InitReportWOTravel);












                    $scope.masterDataReady = true;
                    $scope.loader.childRequestEnable = false;
                },
                function (response) {
                    // fail
                    $scope.masterDataReady = false;
                    $scope.loader.childRequestEnable = false;
                    return response;
                });

            };

            // services call back fucntion
            var InitReportWOTravel = function () {
                var oRequestParameter = {
                    InputParameter: {
                        "referRequestNo": $scope.referRequestNo,
                        "EmployeeID": $scope.document.RequestorNo,
                        "PositionID": $scope.document.RequestorPosition,
                        "CompanyCode": $scope.document.RequestorCompanyCode
                    },
                    CurrentEmployee: getToken(CONFIG.USER)
                    , Creator: $scope.document.Creator
                    , Requestor: $scope.document.Requestor
                };
                var URL = CONFIG.SERVER + 'HRTR/InitReportWOTravel';
                return $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    return_InitReportWOTravel = response.data;
                }, function errorCallback(response) {
                    // Error
                    $scope.watcherTravel = $scope.$watch('data.TravelSchedulePlaces', function (newObj, oldObj) {
                        if ($scope.initialReady) {
                            // calculate TravelReport
                            if (oldObj != newObj) {
                                $scope.CalculateTravelReport();
                            }
                        }
                    }, true);
                    return response;
                });
            };
            var GetTravelRequestConfig = function () {
                var URL = CONFIG.SERVER + 'HRTR/GetTravelRequestConfig';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor }
                return $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.TravelGroupConfig.GroupMode = response.data['GROUPMODE'];
                    $scope.TravelGroupConfig.TravelType = response.data['TRAVELTYPE'];
                    $scope.data.EnableOverideCostcenter = response.data['ENABLEOVERIDECOSTCENTER'];
                    $scope.data.EnableOverideIO = response.data['ENABLEOVERIDEIO'];
                    $scope.data.BACKDATE = response.data['BACKDATE'];

                    //TransportationType add by Lucifer
                    $scope.data.DefaultTransportationTypeCode = response.data['TransportationTypeCode'];
                    $scope.data.DefaultTransportationTypeName = response.data['TransportationTypeName'];

                    $scope.data.ProjectCodeMode = response.data['ProjectCodeMode'];

                    if (angular.isUndefined($scope.document.Additional.ReportWOTravel[0].oTravelGroupRequest.Status) || $scope.document.Additional.ReportWOTravel[0].oTravelGroupRequest.Status == '') {
                        $scope.document.Additional.ReportWOTravel[0].oTravelGroupRequest.GroupSelect = response.data['DEFAULTREQUESTGROUPAPPROVE'];
                    }
                    $scope.setBlockAction(false, "");
                }, function errorCallback(response) {
                    // Error
                    console.log('error TravelRequestEditorController InitialConfig.', response);
                    $scope.setBlockAction(false, "");
                });
            }
            var InitialExpenseReceiptG1 = function () {
                var URL = CONFIG.SERVER + 'HRTR/InitialExpenseReceiptGroup1';
                var oInputParameter = {
                    "CreateDate": $scope.document.CreatedDate
                };
                var oRequestParameter = { InputParameter: oInputParameter, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                return $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.masterData.projectForLookup = response.data.projectForLookup;
                    $scope.masterData.objCostcenterLookup = response.data.objCostcenterLookup;
                    $scope.masterData.objIOLookupObj = response.data.objIOLookupObj;

                    $scope.masterData.objCostcenterDistribution = response.data.objCostcenterDistribution;
                    $scope.masterData.allProjects = response.data.allProjects;
                    //console.debug('----- G1 completed. -----');
                    return response;
                }, function errorCallback(response) {
                    $scope.masterData = {};
                    return response;
                });
            };
            var InitialExpenseReceiptG2 = function () {
                var URL = CONFIG.SERVER + 'HRTR/InitialExpenseReceiptGroup2';
                var oInputParameter = {
                    "CreateDate": $scope.document.CreatedDate
                };
                var oRequestParameter = { InputParameter: oInputParameter, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                return $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.masterData.setVatTypeDefault = response.data.setVatTypeDefault;

                    $scope.data.tmpTravelSchedulePlace = response.data.TravelModel.TravelSchedulePlace[0];
                    $scope.data.tmpProjectCostDistribution = response.data.TravelModel.ProjectCostDistribution[0];
                    $scope.data.tmpExchangeRateCapture = response.data.TravelModel.ExchangeRateCapture[0];
                    $scope.data.tmpTraveler = response.data.TravelModel.Traveler[0];
                    $scope.data.tmpGroupBudget = response.data.TravelModel.GroupBudget[0];

                    $scope.masterData.objCostcenter = response.data.objCostcenter;
                    $scope.masterData.objOrganization = response.data.objOrganization;

                    var projectcodeMode = response.data.ProjectCodeMode.toString().toUpperCase();
                    $scope.settings.ProjectCodeMode = projectcodeMode;
                    var isOverrideCostCenter = response.data.EnableOverideCostCenter.toString().toLowerCase();
                    $scope.settings.EnableOverideCostCenter = (isOverrideCostCenter == 'true') ? true : false;
                    var isOverrideIO = response.data.EnableOverideIO.toString().toLowerCase();
                    $scope.settings.EnableOverideIO = (isOverrideIO == 'true') ? true : false;

                    $scope.masterData.objExpenseRateTypeForLookup = response.data.objExpenseRateTypeForLookup;

                    $scope.settings.MaximumEmpSubGroup = response.data.MaximumEmpSubGroup;
                    //console.debug('----- G2 completed. -----');
                    return response;
                }, function errorCallback(response) {
                    $scope.masterData = {};
                    return response;
                });
            };
            var InitialExpenseReceiptG3 = function () {
                var URL = CONFIG.SERVER + 'HRTR/InitialExpenseReceiptGroup3';
                var oInputParameter = {
                    "CreateDate": $scope.document.CreatedDate
                };
                var oRequestParameter = { InputParameter: oInputParameter, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                return $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.masterData.employeeDataList = response.data.employeeDataList;

                    $scope.settings.ReceiptToleranceAllow = response.data.ReceiptToleranceAllow;
                   // console.debug('----- G3 completed. -----');
                    return response;
                }, function errorCallback(response) {
                    $scope.masterData.employeeDataList = [];
                    $scope.settings.ReceiptToleranceAllow = 0;
                    return response;
                });
            };
            var InitialExpenseReceiptG4 = function () {
                var URL = CONFIG.SERVER + 'HRTR/InitialExpenseReceiptGroup4';
                var oInputParameter = {
                    "CreateDate": $scope.document.CreatedDate
                };
                var oRequestParameter = { InputParameter: oInputParameter, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                return $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.masterData.exchangeType = response.data.exchangeType;
                    $scope.masterData.carTypes = response.data.carTypes;
                    $scope.masterData.flatRateLocations = response.data.flatRateLocations;
                    $scope.masterData.objVendor = response.data.objVendor;
                    $scope.masterData.objCountry = response.data.objCountry;
                    //console.debug('----- G4 completed. -----');
                    return response;
                }, function errorCallback(response) {
                    $scope.masterData = {
                        projectForLookup: null,
                        exchangeType: null,
                        employeeDataList: null,
                        carTypes: null,
                        flatRateLocations: null,
                        objVendor: null,
                        objCountry: null,
                        objCostcenterLookup: null,
                        objIOLookupObj: null,
                        setVatTypeDefault: null,
                        setPaymentMethod: null,
                        setPaymentSupplement: null,
                        setPaymentTerm: null,
                        oTranType: null,
                        objCostcenter: null,
                        objOrganization: null,
                        objCostcenterDistribution: null,
                        allProjects: null
                    };
                    return response;
                });
            };
            var InitialExpenseReceiptG5 = function () {
                var URL = CONFIG.SERVER + 'HRTR/InitialExpenseReceiptGroup5';
                var oInputParameter = {
                    "CreateDate": $scope.document.CreatedDate
                };
                var oRequestParameter = { InputParameter: oInputParameter, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                return $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.masterData.setPaymentMethod = response.data.setPaymentMethod;
                    $scope.masterData.setPaymentSupplement = response.data.setPaymentSupplement;
                    $scope.masterData.setPaymentTerm = response.data.setPaymentTerm;
                    $scope.masterData.oTranType = response.data.oTranType;
                    //console.debug('----- G5 completed. -----');
                    return response;
                }, function errorCallback(response) {
                    $scope.masterData = {
                    };
                    return response;
                });
            };



            // 
            var tempResult_CostCenter;
            $scope.tryToSelect_CostCenter = function (text) {
                if ($scope.document.Additional.ReportWOTravel[0].IsAlternativeCostCenter) {
                    for (var i = 0; i < $scope.masterData.objCostcenterDistribution.length; i++) {
                        if (($scope.masterData.objCostcenterDistribution[i].CostCenterCode + " : " + $scope.masterData.objCostcenterDistribution[i].LongDesc) == text ||
                            $scope.masterData.objCostcenterDistribution[i].LongDesc == text) {
                            tempResult_CostCenter = $scope.masterData.objCostcenterDistribution[i];
                            break;
                        }
                    }
                }
                else {
                    for (var i = 0; i < $scope.masterData.objCostcenter.length; i++) {
                        if (($scope.masterData.objCostcenter[i].CostCenterCode + " : " + $scope.masterData.objCostcenter[i].LongDesc) == text ||
                            $scope.masterData.objCostcenter[i].LongDesc == text) {
                            tempResult_CostCenter = $scope.masterData.objCostcenter[i];
                            break;
                        }
                    }
                }
                $scope.searchTextProject = '';
                ////$scope.selectedItemProjectChange(null, $index);
                //$scope.autocomplete.selectedItem = null;
                //$scope.persons.delegated = null;
            }
            $scope.checkText_CostCenter = function (text) {
                //var result = null;
                //for (var i = 0; i < $scope.projectlist.length; i++) {
                //    if (($scope.projectlist[i].ProjectCode + " : " + $scope.projectlist[i].Name) == text || $scope.projectlist[i].Name == text) {
                //        result = $scope.projectlist[i];
                //        break;
                //    }
                //}
                //if (result) {
                //    $scope.searchTextProject = result.ProjectCode + " : " + result.Name;
                //    $scope.selectedItemProjectChange(result, $index);
                //    //$scope.autocomplete.selectedItem = result;
                //} else if (tempResult_CostCenter) {
                //    $scope.searchTextProject = tempResult_CostCenter.ProjectCode + " : " + tempResult_CostCenter.Name;
                //    $scope.selectedItemProjectChange(tempResult_CostCenter, $index);
                //    //$scope.autocomplete.selectedItem = tempResult;
                //}
            }
            $scope.querySearch_objCostcenterDistribution = function (query) {
                var results = query ? filterFor_objCostcenterDistribution(query) : $scope.masterData.objCostcenterDistribution;
                return results;
            }

            function filterFor_objCostcenterDistribution(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(project) {
                    var source = angular.lowercase(project.CostCenterCode + ' : ' + project.LongDesc);
                    return (source.indexOf(lowercaseQuery) >= 0);
                };
            }


        }]);
})();

