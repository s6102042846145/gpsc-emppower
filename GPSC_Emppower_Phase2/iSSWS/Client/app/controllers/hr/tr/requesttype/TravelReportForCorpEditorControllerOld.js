﻿(function () {
    angular.module('ESSMobile')
        .controller('TravelReportForCorpEditorControllerOld', ['$scope', '$http', '$routeParams', '$location', '$filter', '$window', '$q', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, $window, $q, CONFIG) {
            $scope.loader.childRequestEnable = true;
            $scope.TRAVELMODE = "P";
            var employeeData = getToken(CONFIG.USER);
            $scope.isDebug = false;
            $scope.exchangeRateDeferred = $q.defer();
            $scope.travelReportPromise = {
                travelReport: null,
                travelSchedulePlace: null,
                exchangeRateCapture: $scope.exchangeRateDeferred.promise,
                projectCode: null,
                perdium: null,
                receipt: null,
                withoutReceipt: null
            };
            $scope.IsReportForCorp = true;

            $scope.getTravelDescription = function (traveltypeID) {
                var oRequestParameter = { InputParameter: { "TRAVELTYPEID": traveltypeID, "LANGUAGE": "EN" }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor }

                var URL = CONFIG.SERVER + 'HRTR/GetTravelType/';
                return $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // success
                    $scope.TravelDescription = response.data;
                    var groupTag1 = "TR_ETG_TravelReport_Traveler";
                    var groupTag2 = "TR_ETG_TravelReport";

                    //TR_ETG_TravelReport_Traveler_Overseas,TR_ETG_TravelReport_Personal_Overseas
                    if ($scope.document.Additional.oTravelGroupRequest.TravelMode == "G") {
                        groupTag2 += "_Group";
                    }
                    else {
                        groupTag2 += "_Personal";
                    }

                    //add destination
                    if ($scope.settings.ExpenseTypeGroupInfo.isDomestic == 'D') {
                        groupTag1 += "_Domestic";
                        groupTag2 += "_Domestic";
                        $scope.settings.ExpenseTypeGroupInfo.groupTagName = groupTag1 + "," + groupTag2;
                        $scope.settings.ExpenseTypeGroupInfo.tagNameReceipt = $scope.settings.ExpenseTypeGroupInfo.prefix + "_" + $scope.TravelDescription[0].ENCode + "Domestic_Receipt";
                        $scope.settings.ExpenseTypeGroupInfo.tagNameNoReceipt = $scope.settings.ExpenseTypeGroupInfo.prefix + "_" + $scope.TravelDescription[0].ENCode + "Domestic_NoReceipt";

                    } else if ($scope.settings.ExpenseTypeGroupInfo.isDomestic == 'O') {
                        groupTag1 += "_Overseas";
                        groupTag2 += "_Overseas";
                        $scope.settings.ExpenseTypeGroupInfo.groupTagName = groupTag1 + "," + groupTag2;
                        $scope.settings.ExpenseTypeGroupInfo.tagNameReceipt = $scope.settings.ExpenseTypeGroupInfo.prefix + "_" + $scope.TravelDescription[0].ENCode + "Overseas_Receipt";
                        $scope.settings.ExpenseTypeGroupInfo.tagNameNoReceipt = $scope.settings.ExpenseTypeGroupInfo.prefix + "_" + $scope.TravelDescription[0].ENCode + "Overseas_NoReceipt";

                    } else if ($scope.settings.ExpenseTypeGroupInfo.isDomestic == 'B') {
                        var gTag1 = groupTag1 + "_Domestic";
                        var gTag2 = groupTag2 + "_Domestic";
                        var gTag3 = groupTag1 + "_Overseas";
                        var gTag4 = groupTag2 + "_Overseas";
                        $scope.settings.ExpenseTypeGroupInfo.groupTagName = gTag1 + "," + gTag2 + "," + gTag3 + "," + gTag4;
                        var typeTagCode = $scope.settings.ExpenseTypeGroupInfo.prefix + "_" + $scope.TravelDescription[0].ENCode;
                        $scope.settings.ExpenseTypeGroupInfo.tagNameReceipt = typeTagCode + 'Domestic_Receipt,' + typeTagCode + 'Overseas_Receipt';
                        $scope.settings.ExpenseTypeGroupInfo.tagNameNoReceipt = typeTagCode + 'Domestic_NoReceipt,' + typeTagCode + 'Overseas_NoReceipt';

                    }
                    return response;
                }, function errorCallback(response) {
                    // fail
                    console.log('error TravelDescription.', response);
                    return response;
                });
            };

            $scope.getDestinationText = function (destination) {
                var returnValue = "";
                if (destination == true)
                    returnValue = "Domestic";
                else
                    returnValue = "Overseas";
                return returnValue;
            };

            $scope.getIOByOrganization = function (Org, isSetDefaultIO) {
                if (angular.isUndefined(isSetDefaultIO)) {
                    isSetDefaultIO = false;
                }
                var URL = CONFIG.SERVER + 'HRTR/GetInternalOrderByOrgUnit/';
                var oRequestParameter = { InputParameter: { "OrgUnit": Org }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.objIO = response.data;
                    if ($scope.objIO != null && $scope.objIO.length > 0 && isSetDefaultIO) {
                        // set default
                        $scope.document.Additional.IO = $scope.objIO[0].OrderID;
                    }
                    console.log('objIO.', $scope.objIO);
                }, function errorCallback(response) {
                    console.log('error getIOByOrganization.', response);
                });
            };

            $scope.changeIsAlternativeCostCenter = function (isAlternativeCostCenter) {
                if (!isAlternativeCostCenter) {
                    $scope.document.Additional.CostCenter = $scope.masterData.objCostcenterDistribution[0].CostCenterCode;
                }
                //if (!$scope.document.Additional.IsAlternativeIOOrg) {
                //    $scope.getIOByCostCenter($scope.document.Additional.CostCenter, true);
                //}
            };

            $scope.changeIsAlternativeIOOrg = function (isAlternativeIOOrg) {
                // change default
                if (!isAlternativeIOOrg) {
                    if ($scope.document.Requestor.OrgUnit != null) {
                        $scope.document.Additional.AlternativeIOOrg = $scope.document.Requestor.OrgUnit;
                    }
                    //$scope.getIOByCostCenter($scope.document.Additional.CostCenter, true);
                    $scope.getIOByOrganization($scope.document.Additional.AlternativeIOOrg, true);
                } else {
                    $scope.getIOByOrganization($scope.document.Additional.AlternativeIOOrg, true);
                }
            };

            $scope.onSelectedCostCenter = function (selectedCostCenter) {
                //if (!$scope.document.Additional.IsAlternativeIOOrg) {
                //    $scope.getIOByCostCenter(selectedCostCenter.CostCenterCode, true);
                //}
            };

            $scope.onSelectedOrganization = function (selectedOrganization) {
                //if ($scope.document.Additional.IsAlternativeIOOrg) {
                $scope.getIOByOrganization($scope.document.Additional.AlternativeIOOrg, true);
                //}
            };

            /* !CostCentet, Organization, IO dropdown */


            $scope.ChildAction.SetData = function () {
                $scope.Page = CONFIG.PAGE_SETTING.TRAVEL_REPORT;
                $scope.Textcategory = 'EXPENSE';
                $scope.content.Header = $scope.Text['EXPENSE']['TRAVEL_REPORT'];
                $scope.Textcategory2 = 'TRAVELREQUEST';

                $scope.initialReady = false;
                $scope.pageState = {
                    travelScheduleBusy: false
                };
                $scope.initReportReady = false;
                $scope.formDataTravelReport = {
                    travelSequence: []
                };

                $scope.ExchangeTypeReady = false;

                //$scope.TravelGroupConfig = {
                //    DefaultRequestGroupApprove: '',
                //    GroupMode: '',
                //    TravelType: '',
                //    Phone: '',
                //    MobilePhone: ''
                //};

                //$scope.model = {
                //    ready: false,
                //    data: null
                //};

                $scope.isUserRoleAccounting = $scope.systemMasterData.IsAccountUser;

                /* Master data */

                $scope.masterDataReady = false;

                $scope.masterData = {
                    projectForLookup: null,
                    exchangeType: null,
                    employeeDataList: null,
                    carTypes: null,
                    flatRateLocations: null,
                    objVendor: null,
                    objCountry: null,
                    objCostcenterLookup: null,
                    objIOLookupObj: null,
                    setVatTypeDefault: null,
                    setPaymentMethod: null,
                    setPaymentSupplement: null,
                    setPaymentTerm: null,
                    oTranType: null,
                    objCostcenter: null,
                    objOrganization: null,
                    objCostcenterDistribution: null,
                    allProjects: null,
                    objExpenseRateTypeForLookup: null,
                    travelCurrencyList: null
                };

                $scope.data = {
                    IsEdit: true,
                    Readonly: false,
                    IsReturnEdit: true,
                    IsExchangeRateEdit: true,
                    ProjectCostDistributions: null,
                    TravelSchedulePlaces: null,
                    ExchangeRateCaptures: null,
                    Travelers: null,
                    GroupBudgets: null,
                    PerdiumList: null,
                    PaymentFlagCaptures: null,
                    tmpTravelSchedulePlace: null,
                    tmpProjectCostDistribution: null,
                    tmpExchangeRateCapture: null,
                    tmpTraveler: null,
                    tmpGroupBudget: null
                };

                var initialExpenseReceiptG1 = function () {
                    var URL = CONFIG.SERVER + 'HRTR/InitialExpenseReceiptGroup1';
                    var oInputParameter = {
                        "CreateDate": $scope.document.CreatedDate
                    };
                    var oRequestParameter = { InputParameter: oInputParameter, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    return $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.masterData.projectForLookup = response.data.projectForLookup;
                        $scope.masterData.objCostcenterLookup = response.data.objCostcenterLookup;
                        $scope.masterData.objIOLookupObj = response.data.objIOLookupObj;

                        $scope.masterData.objCostcenterDistribution = response.data.objCostcenterDistribution;
                        $scope.masterData.allProjects = response.data.allProjects;

                        console.log('----- G1 completed. -----');
                        return response;
                    }, function errorCallback(response) {
                        $scope.masterData = {
                            projectForLookup: null,
                            exchangeType: null,
                            employeeDataList: null,
                            carTypes: null,
                            flatRateLocations: null,
                            objVendor: null,
                            objCountry: null,
                            objCostcenterLookup: null,
                            objIOLookupObj: null,
                            setVatTypeDefault: null,
                            setPaymentMethod: null,
                            setPaymentSupplement: null,
                            setPaymentTerm: null,
                            oTranType: null,
                            objCostcenter: null,
                            objOrganization: null,
                            objCostcenterDistribution: null,
                            allProjects: null
                        };
                        return response;
                    });
                };
                var initPromiseG1 = initialExpenseReceiptG1();

                var initialExpenseReceiptG2 = function () {
                    var URL = CONFIG.SERVER + 'HRTR/InitialExpenseReceiptGroup2';
                    var oInputParameter = {
                        "CreateDate": $scope.document.CreatedDate
                    };
                    var oRequestParameter = { InputParameter: oInputParameter, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    return $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.masterData.setVatTypeDefault = response.data.setVatTypeDefault;

                        $scope.data.tmpTravelSchedulePlace = response.data.TravelModel.TravelSchedulePlace[0];
                        $scope.data.tmpProjectCostDistribution = response.data.TravelModel.ProjectCostDistribution[0];
                        $scope.data.tmpExchangeRateCapture = response.data.TravelModel.ExchangeRateCapture[0];
                        $scope.data.tmpTraveler = response.data.TravelModel.Traveler[0];
                        $scope.data.tmpGroupBudget = response.data.TravelModel.GroupBudget[0];

                        $scope.masterData.objCostcenter = response.data.objCostcenter;
                        $scope.masterData.objOrganization = response.data.objOrganization;

                        var projectcodeMode = response.data.ProjectCodeMode.toString().toUpperCase();
                        $scope.settings.ProjectCodeMode = projectcodeMode;
                        var isOverrideCostCenter = response.data.EnableOverideCostCenter.toString().toLowerCase();
                        $scope.settings.EnableOverideCostCenter = (isOverrideCostCenter == 'true') ? true : false;
                        var isOverrideIO = response.data.EnableOverideIO.toString().toLowerCase();
                        $scope.settings.EnableOverideIO = (isOverrideIO == 'true') ? true : false;

                        var isEnableProjectCost = response.data.EnableProjectCost.toString().toLowerCase();
                        $scope.settings.EnableProjectCost = (isEnableProjectCost == 'true') ? true : false;

                        $scope.masterData.objExpenseRateTypeForLookup = response.data.objExpenseRateTypeForLookup;

                        $scope.settings.MaximumEmpSubGroup = response.data.MaximumEmpSubGroup;

                        console.log('----- G2 completed. -----');
                        return response;
                    }, function errorCallback(response) {
                        $scope.masterData = {
                            projectForLookup: null,
                            exchangeType: null,
                            employeeDataList: null,
                            carTypes: null,
                            flatRateLocations: null,
                            objVendor: null,
                            objCountry: null,
                            objCostcenterLookup: null,
                            objIOLookupObj: null,
                            setVatTypeDefault: null,
                            setPaymentMethod: null,
                            setPaymentSupplement: null,
                            setPaymentTerm: null,
                            oTranType: null,
                            objCostcenter: null,
                            objOrganization: null,
                            objCostcenterDistribution: null,
                            allProjects: null
                        };
                        return response;
                    });
                };
                var initPromiseG2 = initialExpenseReceiptG2();

                var initialExpenseReceiptG3 = function () {
                    var URL = CONFIG.SERVER + 'HRTR/InitialExpenseReceiptGroup3';
                    var oInputParameter = {
                        "CreateDate": $scope.document.CreatedDate
                    };
                    var oRequestParameter = { InputParameter: oInputParameter, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    return $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.masterData.employeeDataList = response.data.employeeDataList;

                        $scope.settings.ReceiptToleranceAllow = response.data.ReceiptToleranceAllow;

                        console.log('----- G3 completed. -----');
                        return response;
                    }, function errorCallback(response) {
                        $scope.masterData.employeeDataList = [];
                        $scope.settings.ReceiptToleranceAllow = 0;
                        return response;
                    });
                };
                var initPromiseG3 = initialExpenseReceiptG3();

                var initialExpenseReceiptG4 = function () {
                    var URL = CONFIG.SERVER + 'HRTR/InitialExpenseReceiptGroup4';
                    var oInputParameter = {
                        "CreateDate": $scope.document.CreatedDate
                    };
                    var oRequestParameter = { InputParameter: oInputParameter, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    return $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.masterData.exchangeType = response.data.exchangeType;
                        $scope.masterData.carTypes = response.data.carTypes;
                        $scope.masterData.flatRateLocations = response.data.flatRateLocations;
                        $scope.masterData.objVendor = response.data.objVendor;
                        $scope.masterData.objCountry = response.data.objCountry;

                        console.log('----- G4 completed. -----');
                        return response;
                    }, function errorCallback(response) {
                        $scope.masterData = {
                            projectForLookup: null,
                            exchangeType: null,
                            employeeDataList: null,
                            carTypes: null,
                            flatRateLocations: null,
                            objVendor: null,
                            objCountry: null,
                            objCostcenterLookup: null,
                            objIOLookupObj: null,
                            setVatTypeDefault: null,
                            setPaymentMethod: null,
                            setPaymentSupplement: null,
                            setPaymentTerm: null,
                            oTranType: null,
                            objCostcenter: null,
                            objOrganization: null,
                            objCostcenterDistribution: null,
                            allProjects: null
                        };
                        return response;
                    });
                };
                var initPromiseG4 = initialExpenseReceiptG4();

                var initialExpenseReceiptG5 = function () {
                    var URL = CONFIG.SERVER + 'HRTR/InitialExpenseReceiptGroup5';
                    var oInputParameter = {
                        "CreateDate": $scope.document.CreatedDate,
                        "ReuestType": "RP"
                    };
                    var oRequestParameter = { InputParameter: oInputParameter, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    return $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.masterData.setPaymentMethod = response.data.setPaymentMethod;
                        $scope.masterData.setPaymentSupplement = response.data.setPaymentSupplement;
                        $scope.masterData.setPaymentTerm = response.data.setPaymentTerm;
                        //$scope.masterData.oTranType = response.data.oTranType;

                        // SET 'RECEIPT_LIMIT_DATE_09182017'
                        $scope.RECEIPT_LIMIT_DATE = {
                            RP_LIMIT_MINDATE: response.data.RP_LIMIT_MINDATE,
                            RP_LIMIT_MAXDATE: response.data.RP_LIMIT_MAXDATE,
                            GE_LIMIT_MINDATE: response.data.GE_LIMIT_MINDATE,
                            GE_LIMIT_MAXDATE: response.data.GE_LIMIT_MAXDATE,
                            GE_ALLOW_SELECTDATE_OUTOFPERIOD: response.data.GE_ALLOW_SELECTDATE_OUTOFPERIOD,
                            RP_ALLOW_SELECTDATE_OUTOFPERIOD: response.data.RP_ALLOW_SELECTDATE_OUTOFPERIOD,
                            GE_VALIDATE_EXPENSETYPE_TIMECONSTRAIN: response.data.GE_VALIDATE_EXPENSETYPE_TIMECONSTRAIN,
                            RP_VALIDATE_EXPENSETYPE_TIMECONSTRAIN: response.data.RP_VALIDATE_EXPENSETYPE_TIMECONSTRAIN
                        }

                        console.log('----- G5 completed. -----');
                        return response;
                    }, function errorCallback(response) {
                        $scope.masterData = {
                            projectForLookup: null,
                            exchangeType: null,
                            employeeDataList: null,
                            carTypes: null,
                            flatRateLocations: null,
                            objVendor: null,
                            objCountry: null,
                            objCostcenterLookup: null,
                            objIOLookupObj: null,
                            setVatTypeDefault: null,
                            setPaymentMethod: null,
                            setPaymentSupplement: null,
                            setPaymentTerm: null,
                            oTranType: null,
                            objCostcenter: null,
                            objOrganization: null,
                            objCostcenterDistribution: null,
                            allProjects: null
                        };
                        return response;
                    });
                };
                var initPromiseG5 = initialExpenseReceiptG5();

                var initialGetMasterData = $scope.GetMasterdata();
                /* !Master data */



                $scope.isDomesticTravel = function (travelReport) {
                    /* return : 'D' = Domestic, 'O' = Oversea, 'B' = Both domestic and oversea */
                    var oTravelSchedules = travelReport.TravelSchedulePlaces;
                    var personalCountryCode = $scope.employeeData.AreaSetting.CountryCode;
                    var countDomestic = 0;
                    var countOversea = 0;
                    for (var i = 0; i < oTravelSchedules.length; i++) {
                        if (personalCountryCode == oTravelSchedules[i].CountryCode) {
                            countDomestic++;
                        } else {
                            countOversea++;
                        }
                    }
                    if (countDomestic == 0) {
                        return 'O';
                    } else if (countOversea == 0) {
                        return 'D';
                    }
                    return 'B';
                };

                $scope.CalculateTravelReport = function (ChangeFromExchangeRate) {
                    $scope.watcherTravel(); // clear watch
                    console.log('---------------StartCalculateTravelReport--------------');
                    $scope.pageState.travelScheduleBusy = true;
                    var URL = CONFIG.SERVER + 'HRTR/CalculateTravelReport';
                    $scope.ChildAction.LoadData();
                    // !Save data

                    var IsReCalculate = true;
                    if (angular.isDefined(ChangeFromExchangeRate) && ChangeFromExchangeRate) {
                        IsReCalculate = false;
                    }
                    var oRequestParameter = { InputParameter: { 'TRAVELREPORT': $scope.document.Additional, 'IsReCalculate': IsReCalculate }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        //console.log('---------------FinishCalculateTravelReport--------------', response.data);
                        //$scope.document.Additional = response.data;
                        $scope.document.Additional.TotalAmount = response.data.TotalAmount;
                        $scope.document.Additional.TotalDay = response.data.TotalDay;
                        $scope.document.Additional.TotalNight = response.data.TotalNight;
                        $scope.document.Additional.TotalAccommodationAmount = response.data.TotalAccommodationAmount;
                        $scope.document.Additional.TravelSchedulePlaceGroupList = response.data.TravelSchedulePlaceGroupList;
                        $scope.document.Additional.PerdiumList = response.data.PerdiumList;

                        $scope.settings.ExpenseTypeGroupInfo.travelTypeID = response.data.oTravelGroupRequest.TravelTypeID;

                        $scope.settings.ExpenseTypeGroupInfo.isDomestic = $scope.isDomesticTravel(response.data);

                        $scope.getTravelDescription($scope.settings.ExpenseTypeGroupInfo.travelTypeID);

                        /* ExchangeRate */

                        if (angular.isUndefined(ChangeFromExchangeRate) || !ChangeFromExchangeRate) {
                            var NewExchangeRateCaptures = response.data.ExchangeRateCaptures;
                            console.log('88888888888888888 New.', angular.copy(NewExchangeRateCaptures));
                            console.log('88888888888888888 Old.', angular.copy($scope.data.ExchangeRateCaptures));
                            // Reload Locked ExchangeRate
                            var count = $scope.data.ExchangeRateCaptures.length;
                            for (var i = count - 1; i >= 0; i--) {
                                if ($scope.data.ExchangeRateCaptures[i].IsLock) {
                                    // delete
                                    $scope.data.ExchangeRateCaptures.splice(i, 1);
                                }
                            }
                            for (var i = NewExchangeRateCaptures.length - 1; i >= 0; i--) {
                                // insert
                                if (NewExchangeRateCaptures[i].IsLock) {
                                    $scope.data.ExchangeRateCaptures.unshift(NewExchangeRateCaptures[i]);
                                }
                            }
                            console.log('88888888888888888 Combine.', angular.copy($scope.data.ExchangeRateCaptures));
                            $scope.document.Additional.ExchangeRateCaptures = $scope.data.ExchangeRateCaptures;
                        }

                        /* !ExchangeRate */



                        $scope.data.TravelSchedulePlaces = response.data.TravelSchedulePlaces;
                        $scope.data.ProjectCostDistributions = response.data.ProjectCostDistributions;
                        $scope.$broadcast('refreshTravelreportProject');
                        //$scope.data.ProjectCostDistributionTemp = angular.copy(response.data.ProjectCostDistributions);
                        $scope.data.Travelers = response.data.Travelers;
                        $scope.data.GroupBudgets = response.data.GroupBudgets;
                        $scope.data.PaymentFlagCaptures = response.data.PaymentFlagCaptures;
                        $scope.data.PerdiumList = response.data.PerdiumList;
                        // CashAdvanceReturn
                        $scope.data.CashAdvance = response.data.Travelers[0].oCashAdvance;
                        $scope.data.CashAdvanceReturns = response.data.CashAdvanceReturnList;

                        /* initial form data */
                        $scope.formDataTravelReport.travelSequence = [];
                        for (var i = 0; i < $scope.data.TravelSchedulePlaces.length; i++) {
                            var oBeginDate = new Date($scope.getLocalDateString($scope.data.TravelSchedulePlaces[i].BeginDate));
                            var oEndDate = new Date($scope.getLocalDateString($scope.data.TravelSchedulePlaces[i].EndDate));
                            var objNewFormData = {
                                startDate: $scope.data.TravelSchedulePlaces[i].BeginDate,
                                startHour: $scope.zeroPaddingText(oBeginDate.getHours(), 2),
                                startMin: $scope.zeroPaddingText(oBeginDate.getMinutes(), 2),
                                endDate: $scope.data.TravelSchedulePlaces[i].EndDate,
                                endHour: $scope.zeroPaddingText(oEndDate.getHours(), 2),
                                endMin: $scope.zeroPaddingText(oEndDate.getMinutes(), 2)
                            };
                            $scope.formDataTravelReport.travelSequence.push(objNewFormData);
                        }
                        /* !initial form data */

                        $scope.pageState.travelScheduleBusy = false;

                        $scope.watcherTravel = $scope.$watch('data.TravelSchedulePlaces', function (newObj, oldObj) {
                            if ($scope.initialReady) {
                                // calculate TravelReport
                                if (oldObj != newObj) {
                                    $scope.CalculateTravelReport();
                                }
                            }
                        }, true);

                    },
                    function errorCallback(response) {
                        console.log('error InitialConfig.', response);
                        $scope.pageState.travelScheduleBusy = false;

                        $scope.watcherTravel = $scope.$watch('data.TravelSchedulePlaces', function (newObj, oldObj) {
                            if ($scope.initialReady) {
                                // calculate TravelReport
                                if (oldObj != newObj) {
                                    $scope.CalculateTravelReport();
                                }
                            }
                        }, true);

                    });
                };

                var getAllExchageType = function (hasCashAdvance) {
                    var URL = CONFIG.SERVER + 'HRTR/GetExchangeTypeAll';
                    var oRequestParameter = { InputParameter: { "IsTravelReport": true, "HaveCashAdvance": hasCashAdvance }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };

                    return $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // success
                        $scope.exchangeType = response.data;
                        $scope.ExchangeTypeReady = true;
                        return response;
                    },
                    function errorCallback(response) {
                        // fail
                        $scope.exchangeType = [];
                        console.log('error GetExchangeTypeAll', response);
                        return response;
                    });
                };

                $scope.InitTravelReport = function () {
                    var oRequestParameter = {
                        InputParameter: {
                            "referRequestNo": $scope.referRequestNo,
                            "EmployeeID": $scope.document.RequestorNo,
                            "PositionID": $scope.document.RequestorPosition,
                            "CompanyCode": $scope.document.RequestorCompanyCode
                        },
                        CurrentEmployee: getToken(CONFIG.USER)
                        , Creator: $scope.document.Creator
                        , Requestor: $scope.document.Requestor
                    };
                    var URL = CONFIG.SERVER + 'HRTR/InitTravelReportForCorpCard';
                    return $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        // Success
                        console.log('----- InitTravelReport. -----', response.data);

                        $scope.document.Additional = response.data;
                        $scope.document.Additional.RequestNo = $scope.document.RequestNo;
                        $scope.document.Additional.TravelRequestNo = $scope.referRequestNo;

                        $scope.settings.ExpenseTypeGroupInfo.travelTypeID = $scope.document.Additional.oTravelGroupRequest.TravelTypeID;
                        $scope.settings.ExpenseTypeGroupInfo.isDomestic = $scope.isDomesticTravel($scope.document.Additional);

                        var travelDescriptionPromise = $scope.getTravelDescription($scope.settings.ExpenseTypeGroupInfo.travelTypeID);

                        //if ($scope.document.Additional.IsAlternativeIOOrg) {
                        $scope.getIOByOrganization($scope.document.Additional.AlternativeIOOrg);
                        //} else {
                        //    $scope.getIOByCostCenter($scope.document.Additional.CostCenter);
                        //}


                        /* Child control data */

                        $scope.data.ExchangeRateCaptures = $scope.document.Additional.ExchangeRateCaptures;
                        $scope.data.TravelSchedulePlaces = $scope.document.Additional.TravelSchedulePlaces;

                        if ($scope.document.Additional.ProjectCostDistributions != null && $scope.document.Additional.ProjectCostDistributions.length == 1) {
                            $scope.document.Additional.ProjectCostDistributions[0].Percentage = 100;
                        }

                        $scope.data.ProjectCostDistributions = $scope.document.Additional.ProjectCostDistributions;
                        //$scope.data.ProjectCostDistributionTemp = angular.copy($scope.document.Additional.ProjectCostDistributions);
                        $scope.initReportReady = true;
                        //$scope.DefaultProject();
                        //for (var i = 0; i < $scope.data.ProjectCostDistributions.length; i++) {
                        //    $scope.data.ProjectCostDistributions[i].ProjectObject = $scope.data.projectlist[$scope.data.projectlist.findIndexWithAttr('ProjectCode', $scope.data.ProjectCostDistributions[i].ProjectCode)];
                        //}
                        $scope.data.Travelers = $scope.document.Additional.Travelers;
                        $scope.data.GroupBudgets = $scope.document.Additional.GroupBudgets;
                        $scope.data.PerdiumList = $scope.document.Additional.PerdiumList;
                        $scope.data.PaymentFlagCaptures = $scope.document.Additional.PaymentFlagCaptures;
                        // TO do asked p kan about this.
                        //var refN = $scope.document.ReferRequestNo;
                        //if (refN.slice(refN.length - 5, refN.length - 3) == 'TR') {
                        //    $scope.data.PaymentFlagCaptures = response.data.PaymentFlagCaptures;
                        //}
                        $scope.data.CashAdvance = $scope.document.Additional.Travelers[0].oCashAdvance;
                        var hasCashAdvance = $scope.data.CashAdvance != null && $scope.data.CashAdvance.TotalAmount > 0;
                        var exchangeTypePromise = getAllExchageType(hasCashAdvance);
                        $scope.data.CashAdvanceReturns = $scope.document.Additional.CashAdvanceReturnList;

                        /* !Child control data */

                        /* initial form data */

                        $scope.formDataTravelReport.travelSequence = [];
                        for (var i = 0; i < $scope.data.TravelSchedulePlaces.length; i++) {
                            var oBeginDate = new Date($scope.getLocalDateString($scope.data.TravelSchedulePlaces[i].BeginDate));
                            var oEndDate = new Date($scope.getLocalDateString($scope.data.TravelSchedulePlaces[i].EndDate));
                            var objNewFormData = {
                                startDate: $scope.data.TravelSchedulePlaces[i].BeginDate,
                                startHour: $scope.zeroPaddingText(oBeginDate.getHours(), 2),
                                startMin: $scope.zeroPaddingText(oBeginDate.getMinutes(), 2),
                                endDate: $scope.data.TravelSchedulePlaces[i].EndDate,
                                endHour: $scope.zeroPaddingText(oEndDate.getHours(), 2),
                                endMin: $scope.zeroPaddingText(oEndDate.getMinutes(), 2)
                            };
                            $scope.formDataTravelReport.travelSequence.push(objNewFormData);
                        }

                        /* !initial form data */

                        var allMasterPromise = $q.all([initPromiseG1, initPromiseG2, initPromiseG3, initPromiseG4, initPromiseG5, initialGetMasterData]).then(function (response) {
                            setMasterData();
                        }, function (response) {
                            // fail
                            $scope.masterDataReady = false;
                        });

                        $scope.initialReady = true;

                        $scope.watcherTravel = $scope.$watch('data.TravelSchedulePlaces', function (newObj, oldObj) {
                            if ($scope.initialReady) {
                                // calculate TravelReport
                                if (oldObj != newObj) {
                                    $scope.CalculateTravelReport();
                                }
                            }
                        }, true);

                        return $q.all([travelDescriptionPromise, exchangeTypePromise, allMasterPromise]).then(function (response) {
                            // success
                            return response;
                        }, function (response) {
                            // fail
                            return response;
                        });
                    }, function errorCallback(response) {
                        // Error

                        $scope.watcherTravel = $scope.$watch('data.TravelSchedulePlaces', function (newObj, oldObj) {
                            if ($scope.initialReady) {
                                // calculate TravelReport
                                if (oldObj != newObj) {
                                    $scope.CalculateTravelReport();
                                }
                            }
                        }, true);

                        console.log('error InitTravelReport.', response);
                        return response;
                    });
                };

                if ($scope.document.NewRequest) {
                    console.log('This is NEW request.', angular.copy($scope.document));
                    $scope.travelReportPromise.travelReport = $scope.InitTravelReport();
                } else {
                    console.log('This is EDIT request.', angular.copy($scope.document));

                    $scope.settings.ExpenseTypeGroupInfo.travelTypeID = $scope.document.Additional.oTravelGroupRequest.TravelTypeID;
                    $scope.settings.ExpenseTypeGroupInfo.isDomestic = $scope.isDomesticTravel($scope.document.Additional);

                    var travelDescriptionPromise = $scope.getTravelDescription($scope.settings.ExpenseTypeGroupInfo.travelTypeID);

                    //if ($scope.document.Additional.IsAlternativeIOOrg) {
                    $scope.getIOByOrganization($scope.document.Additional.AlternativeIOOrg);
                    //} else {
                    //    $scope.getIOByCostCenter($scope.document.Additional.CostCenter);
                    //}

                    /* Child control data */

                    $scope.data.ExchangeRateCaptures = $scope.document.Additional.ExchangeRateCaptures;
                    $scope.data.TravelSchedulePlaces = $scope.document.Additional.TravelSchedulePlaces;
                    $scope.data.ProjectCostDistributions = $scope.document.Additional.ProjectCostDistributions;
                    //$scope.data.ProjectCostDistributionTemp = angular.copy($scope.document.Additional.ProjectCostDistributions);
                    $scope.data.Travelers = $scope.document.Additional.Travelers;
                    $scope.data.GroupBudgets = $scope.document.Additional.GroupBudgets;
                    $scope.data.PerdiumList = $scope.document.Additional.PerdiumList;
                    $scope.data.PaymentFlagCaptures = $scope.document.Additional.PaymentFlagCaptures;
                    $scope.data.CashAdvance = $scope.document.Additional.Travelers[0].oCashAdvance;
                    var hasCashAdvance = $scope.data.CashAdvance != null && $scope.data.CashAdvance.TotalAmount > 0;
                    var exchangeTypePromise = getAllExchageType(hasCashAdvance);
                    $scope.data.CashAdvanceReturns = $scope.document.Additional.CashAdvanceReturnList;

                    /* !Child control data */

                    /* initial form data */

                    $scope.formDataTravelReport.travelSequence = [];
                    for (var i = 0; i < $scope.data.TravelSchedulePlaces.length; i++) {
                        var oBeginDate = new Date($scope.getLocalDateString($scope.data.TravelSchedulePlaces[i].BeginDate));
                        var oEndDate = new Date($scope.getLocalDateString($scope.data.TravelSchedulePlaces[i].EndDate));
                        var objNewFormData = {
                            startDate: $scope.data.TravelSchedulePlaces[i].BeginDate,
                            startHour: $scope.zeroPaddingText(oBeginDate.getHours(), 2),
                            startMin: $scope.zeroPaddingText(oBeginDate.getMinutes(), 2),
                            endDate: $scope.data.TravelSchedulePlaces[i].EndDate,
                            endHour: $scope.zeroPaddingText(oEndDate.getHours(), 2),
                            endMin: $scope.zeroPaddingText(oEndDate.getMinutes(), 2)
                        };
                        $scope.formDataTravelReport.travelSequence.push(objNewFormData);
                    }

                    /* !initial form data */

                    var allMasterPromise = $q.all([initPromiseG1, initPromiseG2, initPromiseG3, initPromiseG4, initPromiseG5, initialGetMasterData]).then(function (response) {
                        setMasterData();
                        return response;
                    }, function (response) {
                        // fail
                        $scope.masterDataReady = false;
                        return response;
                    });

                    $scope.initialReady = true;
                    $scope.initReportReady = true;

                    $scope.watcherTravel = $scope.$watch('data.TravelSchedulePlaces', function (newObj, oldObj) {
                        if ($scope.initialReady) {
                            // calculate TravelReport
                            if (oldObj != newObj) {
                                $scope.CalculateTravelReport();
                            }
                        }
                    }, true);

                    $scope.travelReportPromise.travelReport = $q.all([travelDescriptionPromise, exchangeTypePromise, allMasterPromise]).then(function (response) {
                        // success
                        return response;
                    }, function (response) {
                        // fail
                        return response;
                    });
                }

            };

            function setMasterData() {
                $scope.masterData.oTranType = $scope.settings.Master.TransportationTypeList;
                $scope.document.Additional.ProjectCodeMode = $scope.settings.ProjectCodeMode;
                if ($scope.masterData.objVendor != null && $scope.masterData.objVendor.length > 0) {
                    for (var i = 0; i < $scope.masterData.objVendor.length; i++) {
                        if ($scope.masterData.objVendor[i].TaxID != null) {
                            $scope.masterData.objVendor[i].TaxPersonalID = ' : ' + $scope.masterData.objVendor[i].TaxID;
                        } else {
                            if ($scope.masterData.objVendor[i].PersonalID != null) {
                                $scope.masterData.objVendor[i].TaxPersonalID = ' : ' + $scope.masterData.objVendor[i].PersonalID;
                            } else {
                                $scope.masterData.objVendor[i].TaxPersonalID = '';
                            }

                        }
                    }
                }

                $scope.objCostcenterLookup = $scope.masterData.objCostcenterLookup; /* remove later */
                $scope.objIOLookupObj = $scope.masterData.objIOLookupObj /* remove later */
                $scope.CountryList = $scope.masterData.objCountry /* remove later */
                console.log('----- InitialExpenseReceipt completed. -----');
                $scope.masterDataReady = true;
            }

            $scope.ChildAction.SetData();

            $scope.ChildAction.LoadData = function () {
                /* Child control data */

                $scope.document.Additional.RequestNo = $scope.document.RequestNo;
                $scope.document.Additional.TravelSchedulePlaces = $scope.data.TravelSchedulePlaces;
                $scope.document.Additional.ExchangeRateCaptures = $scope.data.ExchangeRateCaptures;
                $scope.document.Additional.PerdiumList = $scope.data.PerdiumList;
                $scope.document.Additional.CashAdvanceReturnList = $scope.data.CashAdvanceReturns;
                $scope.document.Additional.PaymentFlagCaptures = $scope.data.PaymentFlagCaptures;
                $scope.document.Additional.ProjectCostDistributions = $scope.data.ProjectCostDistributions;

                /* Child control data */
            };



            $scope.lookupForCountryName = function (CountryCode) {
                if ($scope.CountryList) {
                    for (var i = 0; i < $scope.CountryList.length; i++) {
                        if ($scope.CountryList[i].CountryCode == CountryCode) {
                            return $scope.CountryList[i].CountryName;
                        }
                    }
                }
                return '-';
            };

            $scope.findExchangeType = function (exchangeTypeID) {
                if (angular.isDefined($scope.exchangeType)) {
                    for (var i = 0; i < $scope.exchangeType.length; i++) {
                        if ($scope.exchangeType[i].ExchangeTypeID == exchangeTypeID) {
                            if (employeeData.Language == 'TH') {
                                return '(' + $scope.exchangeType[i].DescriptionTH + ')';
                            } else {
                                return '(' + $scope.exchangeType[i].DescriptionEN + ')';
                            }

                        }
                    }
                }
                return '';
            };

            $scope.childSumData = {
                PersonalCashAdvance: 0,
                ExpenseReceipt: 0,
                ExpenseWithoutReceipt: 0
            };
            $scope.sumTravelReportGrandTotal = function () {
                return $scope.childSumData.PersonalCashAdvance + $scope.childSumData.ExpenseReceipt + $scope.childSumData.ExpenseWithoutReceipt;
            };





            /* ====== wizard form control ====== */

            $scope.wizardFormControl = {
                receiptIsActive: false,
                withoutReceiptIsActive: false
            };

            /* ====== !wizard form control ====== */

            /* watcher */
            $scope.grandTotal = 0;
            $scope.$watch('document.Additional.ExpenseReportReceiptList', function (newObj, oldObj) {
                // calculate grand total
                var sumTotal = 0;
                if (angular.isDefined(newObj) && newObj != null) {
                    for (var i = 0; i < newObj.length; i++) {
                        var amount = 0;
                        if (newObj[i].IsVisible) {
                            if (newObj[i].IsReceipt) {
                                amount = Number(newObj[i].OriginalTotalAmount);
                            } else {
                                amount = Number(newObj[i].VATBaseAmount);
                            }
                        }
                        amount = isNaN(amount) ? 0 : amount;
                        sumTotal += amount;
                    }
                }
                $scope.grandTotal = sumTotal;
            }, true);

            //$scope.watcherTravel = $scope.$watch('data.TravelSchedulePlaces', function (newObj, oldObj) {
            //    if ($scope.initialReady) {
            //        // calculate TravelReport
            //        if (oldObj != newObj) {
            //            $scope.CalculateTravelReport();
            //        }
            //    }
            //}, true);

            $scope.$watch('data.ExchangeRateCaptures', function (newObj, oldObj) {
                if ($scope.initialReady) {
                    $scope.masterData.travelCurrencyList = newObj;
                    console.log('-------- TravelReport Update Exchange Rate --------');
                }
            }, true);
            /* !watcher */

            // document ready
            angular.element(document).ready(function () {
                $window.scrollTo(0, 0);
            });


            //$scope.checkAccountingRole = function () {
            //    var URL = CONFIG.SERVER + 'HRTR/IsAccountUser';
            //    var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
            //    $http({
            //        method: 'POST',
            //        url: URL,
            //        data: oRequestParameter
            //    }).then(function successCallback(response) {
            //        $scope.isUserRoleAccounting = response.data;
            //        console.log('IsAccountingRole.', response.data);
            //    }, function errorCallback(response) {
            //        console.log('error IsAccountingRole.', response);
            //    });
            //};
            //$scope.checkAccountingRole();

            var arrPromise = [
                $scope.travelReportPromise.travelReport,
                $scope.travelReportPromise.travelSchedulePlace,
                $scope.travelReportPromise.exchangeRateCapture,
                $scope.travelReportPromise.projectCode,
                $scope.travelReportPromise.perdium,
                $scope.travelReportPromise.receipt,
                $scope.travelReportPromise.withoutReceipt
            ];
            $q.all(arrPromise).then(function (response) {
                // success
                console.log('********************************* finish travelreport *********************************');
                $scope.loader.childRequestEnable = false;
            }, function (response) {
                // fail
                $scope.loader.childRequestEnable = false;
            });

        }]);
})();

