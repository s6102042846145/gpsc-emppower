﻿(function () {
    angular.module('ESSMobile')
        .controller('EditTravelGroupViewerController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$q', function ($scope, $http, $routeParams, $location, $filter, CONFIG,$q) {
           
            $scope.ChildAction.SetData = function () {
            }
            $scope.ChildAction.LoadData = function () {
            }

            $scope.init = function () {
                var promise_service1 = InitialConfig();
                var promise_service2 = $scope.GetTravelModel();
                var promise_service3 = $scope.GetMasterdata();
                $q.all([promise_service1, promise_service2, promise_service3]).then(function (response) {

                    $scope.data.model.ready = true;
                    console.debug('init success')
                }, function (response) {

                    console.error('init error');

                });

            }

            function InitialConfig() {
                $scope.Page = "TravelExpense";
                //for ExchangeRateControl
                $scope.data.Readonly = true;
                $scope.data.TravelSeqEdit = false;
                $scope.data.TravelPersonalEdit = false; //add new by nipon
                $scope.data.IsExchangeRateEdit = false;
                $scope.data.IsTravelRequest = true;
                //$scope.data.Remark = $scope.TravelGroupEdit.Remark ? $scope.TravelGroupEdit.Remark : $scope.TravelGroupEdit.TravelGroupRequest.Remark;
                $scope.data.Remark = $scope.TravelGroupEdit.Remark;
                $scope.TravelGroupRequest = $scope.TravelGroupEdit.TravelGroupRequest;
                $scope.data.ExchangeRateCaptures = $scope.TravelGroupRequest.ExchangeRateCaptures;
                $scope.data.TravelSchedulePlaces = $scope.TravelGroupRequest.TravelSchedulePlaces;
                $scope.data.ProjectCostDistributions = $scope.TravelGroupRequest.ProjectCostDistributions;
                $scope.data.ProjectCostDistributionTemp = angular.copy($scope.TravelGroupRequest.ProjectCostDistributions);
                $scope.data.Travelers = $scope.TravelGroupRequest.Travelers;
                $scope.data.GroupBudgets = $scope.TravelGroupRequest.GroupBudgets;
                if ($scope.data.Travelers[0].oCashAdvance.PaymentFlagCaptures.length > 0) {
                    $scope.data.PaymentFlagCaptures = $scope.data.Travelers[0].oCashAdvance.PaymentFlagCaptures;
                }
                if ($scope.TravelGroupEdit.PaymentFlagCaptures.length > 0) {
                    $scope.data.PaymentFlagCaptures = $scope.TravelGroupEdit.PaymentFlagCaptures;
                }
                $scope.data.Estimate = $scope.TravelGroupRequest.TotalAmount;
                $scope.data.PersonalVehicle = $scope.TravelGroupRequest.PersonalVehicle;
                $scope.TravelGroupRequest.DefaultCurrencyCode = $scope.TravelGroupEdit.DefaultCurrencyCode;
                $scope.data.DefaultCurrencyCode = $scope.TravelGroupEdit.DefaultCurrencyCode;
                $scope.CheckPersonalVehicle();
                $scope.GetTravelRequestConfig($scope.TravelGroupRequest);
            }
           
            /* ====== wizard form control ====== */
            $scope.wizardFormControl = {
                receiptIsActive: false,
                withoutReceiptIsActive: false
            };

          
            /* ====== !wizard form control ====== */
        }])
})();