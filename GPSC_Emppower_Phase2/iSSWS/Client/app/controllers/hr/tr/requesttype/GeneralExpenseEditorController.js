﻿(function () {
angular.module('ESSMobile')
    .controller('GeneralExpenseEditorController', ['$scope', '$http', '$routeParams', '$location', '$filter', '$q', 'CONFIG', function ($scope, $http, $routeParams, $location, $filter, $q, CONFIG) {
        var employeeData = getToken(CONFIG.USER);

        $scope.dateRang = {
            startDate: null, endDate: null
        }

        $scope.ChildAction.SetData = function () {
            $scope.Page = 'GeneralExpense';
            $scope.Textcategory = 'EXPENSE';
            $scope.TextcategoryExpType = 'EXPENSETYPE';

            $scope.document.Additional.RequestNo = $scope.document.RequestNo;
            console.log('$scope.ChildAction.SetData', $scope.document.Additional);
            //$scope.MobilePhone_Requestor = $scope.document.Additional.MobilePhone;

            // add by jirawat jannet @ 26-07-2018
            // set default date rang value from additional date data
            $scope.dateRang.startDate = angular.copy($scope.document.Additional.BeginDate);
            $scope.dateRang.endDate = angular.copy($scope.document.Additional.EndDate);

            /* CostCentet, Organization, IO dropdown */

            $scope.getIOByOrganization = function (Org, isSetDefaultIO) {
                if (angular.isUndefined(isSetDefaultIO)) {
                    isSetDefaultIO = false;
                }
                var URL = CONFIG.SERVER + 'HRTR/GetInternalOrderByOrgUnit/';
                var oRequestParameter = { InputParameter: { "OrgUnit": Org }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.objIO = response.data;
                    if ($scope.objIO != null && $scope.objIO.length > 0 && isSetDefaultIO) {
                        // set default
                        $scope.document.Additional.IO = $scope.objIO[0].OrderID;
                        $scope.selectedItemIoChange($scope.objIO[0], true);
                    }
                    else
                    {
                        for (var i = 0; i < $scope.objIO.length; i++) {
                            if ($scope.objIO[i].OrderID == $scope.document.Additional.IO) {
                                $scope.selectedItemIoChange($scope.objIO[i]);
                                break;
                            }
                        }
                        
                    }
                    //console.log('objIO.', $scope.objIO);
                }, function errorCallback(response) {
                    console.log('error getIOByOrganization.', response);
                });
            };

            $scope.changeIsAlternativeCostCenter = function (isAlternativeCostCenter) {
                if (!isAlternativeCostCenter) {
                    $scope.document.Additional.CostCenter = $scope.masterData.objCostcenterDistribution[0].CostCenterCode;
                }
            };

            $scope.changeIsAlternativeIOOrg = function (isAlternativeIOOrg) {
                // change default
                if (!isAlternativeIOOrg) {
                    if ($scope.document.Requestor.OrgUnit != null) {
                        $scope.document.Additional.AlternativeIOOrg = $scope.document.Requestor.OrgUnit;
                        for (var i = 0; i < $scope.masterData.objOrganization.length; i++) {
                            if ($scope.masterData.objOrganization[i].ObjectID == $scope.document.Additional.AlternativeIOOrg) {
                                $scope.selectedItemOrgChange($scope.masterData.objOrganization[i], true);
                            }
                        }
                    }
                    $scope.getIOByOrganization($scope.document.Additional.AlternativeIOOrg, true);
                } else {
                    $scope.getIOByOrganization($scope.document.Additional.AlternativeIOOrg, true);
                }
            };

            //$scope.onSelectedCostCenter = function (selectedCostCenter) {
            //};

            //$scope.onSelectedOrganization = function (selectedOrganization) {
            //    $scope.getIOByOrganization($scope.document.Additional.AlternativeIOOrg, true);
            //};

            /* !CostCentet, Organization, IO dropdown */

            /* Master data */

            $scope.masterDataReady = false;

            $scope.masterData = {
                projectForLookup: null,
                exchangeType: null,
                employeeDataList: null,
                carTypes: null,
                flatRateLocations: null,
                objVendor: null,
                objCountry: null,
                objCostcenterLookup: null,
                objIOLookupObj: null,
                setVatTypeDefault: null,
                setPaymentMethod: null,
                setPaymentSupplement: null,
                setPaymentTerm: null,
                oTranType: null,
                objCostcenter: null,
                objOrganization: null,
                objCostcenterDistribution: null,
                allProjects: null
            };

            $scope.data = {
                IsEdit: true,
                Readonly: false,
                ProjectCostDistributions: null,
                PaymentFlagCaptures: null,
                tmpTravelSchedulePlace: null,
                tmpProjectCostDistribution: null,
                tmpExchangeRateCapture: null,
                tmpTraveler: null,
                tmpGroupBudget: null,
                EnableOverideCostCenter: false,
                EnableOverideIO: false
            };
            $scope.masterTR = {
                CostCenterList: [],
                OrgUnitList: [],
                IOList: [],
                ProjectList: [],

            };

            //Add new by Nipon Supap 17-05-2017
            //Load Configuratioin for Search Data
            $scope.GetTravelRequestConfig = function () {
                var URL = CONFIG.SERVER + 'HRTR/GetTravelRequestConfig';
                var oRequestParameter = { InputParameter: {}, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor }
                return $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    // Success
                    $scope.data.BACKDATE = response.data['BACKDATE'];
                    $scope.setBlockAction(false, "");
                }, function errorCallback(response) {
                    // Error
                    console.log('error TravelRequestEditorController InitialConfig.', response);
                    $scope.setBlockAction(false, "");
                });
            };
            $scope.GetTravelRequestConfig();
            //$scope.GetMasterdata = function () {
            //    var URL = CONFIG.SERVER + 'HRTR/GetMasterData';
            //    var oRequestParameter = { InputParameter: { "CreateDate": $scope.document.CreatedDate }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
            //    return $http({
            //        method: 'POST',
            //        url: URL,
            //        data: oRequestParameter
            //    }).then(function successCallback(response) {
            //        //$scope.settings.Master.CostCenterList = response.data.CostCenterList;
            //        //$scope.settings.Master.ProjectList = response.data.ProjectList;
            //        //$scope.settings.Master.OrgUnitList = response.data.OrgqanizationList;
            //        //$scope.settings.Master.IOList = response.data.InternalOrderList;
            //        //$scope.settings.Master.CostcenterDistributionList = response.data.CostCenterDistributionList;

            //        // enco much use dont remove
            //        $scope.masterTR.CostCenterList = angular.copy(response.data.CostCenterList);
            //        $scope.masterTR.OrgUnitList = angular.copy(response.data.OrgqanizationList);
            //        $scope.masterTR.IOList = angular.copy(response.data.InternalOrderList);
            //        $scope.masterTR.ProjectList = angular.copy(response.data.ProjectList);
            //        console.debug(response);
            //        return response;
            //    },
            //  function errorCallback(response) {
            //      console.error(response);
            //      return response;
            //  });
            //};
            //$scope.GetMasterdata();

            var initialExpenseReceipt = function () {
                var URL = CONFIG.SERVER + 'HRTR/InitialExpenseReceipt_New';
                var oInputParameter = {
                    "CreateDate": $scope.document.CreatedDate
                };
                var oRequestParameter = { InputParameter: oInputParameter, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                return $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.masterData.projectForLookup = response.data.projectForLookup;
                    $scope.masterData.exchangeType = response.data.exchangeType;
                    $scope.masterData.employeeDataList = response.data.employeeDataList;
                    $scope.masterData.carTypes = response.data.carTypes;
                    $scope.masterData.flatRateLocations = response.data.flatRateLocations;
                    $scope.masterData.objVendor = response.data.objVendor;
                    $scope.masterData.objVendorOnetime = response.data.objVendorOnetime;
                    $scope.masterData.objCountry = response.data.objCountry;
                    $scope.masterData.objCostcenterLookup = response.data.objCostcenterLookup;
                    $scope.masterData.objIOLookupObj = response.data.objIOLookupObj;
                    $scope.masterData.setVatTypeDefault = response.data.setVatTypeDefault;
                    $scope.masterData.setPaymentMethod = response.data.setPaymentMethod;
                    $scope.masterData.setPaymentSupplement = response.data.setPaymentSupplement;
                    $scope.masterData.setPaymentTerm = response.data.setPaymentTerm;
                    $scope.masterData.oTranType = response.data.oTranType;

                    $scope.data.tmpTravelSchedulePlace = response.data.TravelModel.TravelSchedulePlace[0];
                    $scope.data.tmpProjectCostDistribution = response.data.TravelModel.ProjectCostDistribution[0];
                    $scope.data.tmpExchangeRateCapture = response.data.TravelModel.ExchangeRateCapture[0];
                    $scope.data.tmpTraveler = response.data.TravelModel.Traveler[0];
                    $scope.data.tmpGroupBudget = response.data.TravelModel.GroupBudget[0];

                    $scope.masterData.objCostcenter = response.data.objCostcenter;
                    $scope.masterData.objOrganization = response.data.objOrganization;

                    var projectcodeMode = response.data.ProjectCodeMode.toString().toUpperCase();
                    $scope.settings.ProjectCodeMode = projectcodeMode;
                    var isOverrideCostCenter = response.data.EnableOverideCostCenter.toString().toLowerCase();
                    $scope.settings.EnableOverideCostCenter = (isOverrideCostCenter == 'true') ? true : false;

                    var isOverrideIO = response.data.EnableOverideIO.toString().toLowerCase();
                    $scope.settings.EnableOverideIO = (isOverrideIO == 'true') ? true : false;
                    $scope.data.EnableOverideCostCenter = (isOverrideCostCenter == 'true') ? true : false;
                    $scope.data.EnableOverideIO = (isOverrideIO == 'true') ? true : false;

                    var isEnableProjectCost = response.data.EnableProjectCost.toString().toLowerCase();
                    $scope.settings.EnableProjectCost = (isEnableProjectCost == 'true') ? true : false;
                    //

                    $scope.masterData.objCostcenterDistribution = response.data.objCostcenterDistribution;
                    $scope.masterData.allProjects = response.data.allProjects;

                    $scope.settings.ReceiptToleranceAllow = response.data.receiptToleranceAllow;

                    $scope.settings.ReceiptLessthanBeforevat = response.data.receiptLessthanBeforevat;


                    // SET 'RECEIPT_LIMIT_DATE_09182017'
                    $scope.RECEIPT_LIMIT_DATE = {
                        RP_LIMIT_MINDATE: response.data.RP_LIMIT_MINDATE,
                        RP_LIMIT_MAXDATE: response.data.RP_LIMIT_MAXDATE,
                        GE_LIMIT_MINDATE: response.data.GE_LIMIT_MINDATE,
                        GE_LIMIT_MAXDATE: response.data.GE_LIMIT_MAXDATE,
                        GE_ALLOW_SELECTDATE_OUTOFPERIOD: response.data.GE_ALLOW_SELECTDATE_OUTOFPERIOD,
                        RP_ALLOW_SELECTDATE_OUTOFPERIOD: response.data.RP_ALLOW_SELECTDATE_OUTOFPERIOD,
                        GE_VALIDATE_EXPENSETYPE_TIMECONSTRAIN: response.data.GE_VALIDATE_EXPENSETYPE_TIMECONSTRAIN,
                        RP_VALIDATE_EXPENSETYPE_TIMECONSTRAIN: response.data.RP_VALIDATE_EXPENSETYPE_TIMECONSTRAIN
                    }

                    return response;
                }, function errorCallback(response) {
                    $scope.masterData = {
                        projectForLookup: null,
                        exchangeType: null,
                        employeeDataList: null,
                        carTypes: null,
                        flatRateLocations: null,
                        objVendor: null,
                        objVendorOnetime: null,
                        objCountry: null,
                        objCostcenterLookup: null,
                        objIOLookupObj: null,
                        setVatTypeDefault: null,
                        setPaymentMethod: null,
                        setPaymentSupplement: null,
                        setPaymentTerm: null,
                        oTranType: null,
                        objCostcenter: null,
                        objOrganization: null,
                        objCostcenterDistribution: null,
                        allProjects: null
                    };
                    $scope.settings.ReceiptToleranceAllow = 0;
                    return response;
                });
            };
            var masterPromis = $scope.GetMasterdata();
            var initPromise = initialExpenseReceipt();

            /* !Master data */
            if ($scope.document.NewRequest) {
                console.log('This is NEW request.', angular.copy($scope.document));
                $scope.document.Additional.IsAlternativeCostCenter = false;
                $scope.document.Additional.IsAlternativeIOOrg = false;
                $scope.document.Additional.AlternativeIOOrg = $scope.document.Requestor.OrgUnit;
                //$scope.document.Additional.MobilePhone = angular.isDefined(employeeData.MobileNo) ? employeeData.MobileNo : '';
                //$scope.document.Additional.MobilePhone = angular.isDefined($scope.document.Requestor.MobileNo) ? $scope.document.Requestor.MobileNo : '';
                //$scope.document.Additional.Phone = angular.isDefined(employeeData.OfficeTelephoneNo) ? employeeData.OfficeTelephoneNo : '';
                //$scope.document.Additional.Phone = angular.isDefined($scope.document.Requestor.OfficeTelephoneNo) ? $scope.document.Requestor.OfficeTelephoneNo : '';

                $scope.getIOByOrganization($scope.document.Additional.AlternativeIOOrg, true);
            } else {
                console.log('This is EDIT request.', angular.copy($scope.document));
                $scope.getIOByOrganization($scope.document.Additional.AlternativeIOOrg);
            }

            /* Child control data */

            var objProjects = $scope.document.Additional.ProjectCostDistributions;
            $scope.data.ProjectCostDistributions = (angular.isUndefined(objProjects) || objProjects == null) ? [] : angular.copy(objProjects);
            $scope.data.PaymentFlagCaptures = $scope.document.Additional.PaymentFlagCapture;

            /* !Child control data */


            var allMasterPromise = $q.all([initPromise, masterPromis]).then(function (response) {
                // success
                $scope.document.Additional.ProjectCodeMode = $scope.settings.ProjectCodeMode;

                if ($scope.masterData.objVendor != null && $scope.masterData.objVendor.length > 0) {
                    for (var i = 0; i < $scope.masterData.objVendor.length; i++) {
                        if ($scope.masterData.objVendor[i].TaxID != null) {
                            $scope.masterData.objVendor[i].TaxPersonalID = ' : ' + $scope.masterData.objVendor[i].TaxID;
                        } else {
                            if ($scope.masterData.objVendor[i].PersonalID != null) {
                                $scope.masterData.objVendor[i].TaxPersonalID = ' : ' + $scope.masterData.objVendor[i].PersonalID;
                            } else {
                                $scope.masterData.objVendor[i].TaxPersonalID = '';
                            }

                        }
                    }
                }

                if ($scope.masterData.objVendorOnetime != null && $scope.masterData.objVendorOnetime.length > 0) {
                    for (var i = 0; i < $scope.masterData.objVendorOnetime.length; i++) {
                        if ($scope.masterData.objVendorOnetime[i].TaxID != null) {
                            $scope.masterData.objVendorOnetime[i].TaxPersonalID = ' : ' + $scope.masterData.objVendorOnetime[i].TaxID;
                        } else {
                            if ($scope.masterData.objVendorOnetime[i].PersonalID != null) {
                                $scope.masterData.objVendorOnetime[i].TaxPersonalID = ' : ' + $scope.masterData.objVendorOnetime[i].PersonalID;
                            } else {
                                $scope.masterData.objVendorOnetime[i].TaxPersonalID = '';
                            }

                        }
                    }
                }

                $scope.objCostcenterLookup = $scope.masterData.objCostcenterLookup; /* remove later */
                $scope.objIOLookupObj = $scope.masterData.objIOLookupObj /* remove later */

                if ($scope.masterData.objCostcenterDistribution != null && $scope.masterData.objCostcenterDistribution.length > 0 && $scope.document.Additional.CostCenter == '') {
                    // set default
                    $scope.document.Additional.CostCenter = $scope.masterData.objCostcenterDistribution[0].CostCenterCode;
                }
                console.log('----- InitialExpenseReceipt completed. -----');
                $scope.masterDataReady = true;
            }, function (response) {
                // fail
                $scope.masterDataReady = false;
            });

        };
        $scope.ChildAction.SetData();

        $scope.ChildAction.LoadData = function () {
            /* Rebind child control data */
            $scope.document.Additional.ProjectCostDistributions = $scope.data.ProjectCostDistributions;
            $scope.document.Additional.PaymentFlagCapture = $scope.data.PaymentFlagCaptures;
            /* !Rebind child control data */
        };
        
        $scope.setSelectedBeginDate = function (selectedDate) {
            $scope.document.Additional.BeginDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
            if ($scope.document.Additional.BeginDate > $scope.document.Additional.EndDate) {
                $scope.document.Additional.EndDate = $scope.document.Additional.BeginDate;
            }
            console.log('begin date.', $scope.document.Additional.BeginDate);
            console.log('end date.', $scope.document.Additional.EndDate);
        };

        $scope.setSelectedEndDate = function (selectedDate) {
            $scope.document.Additional.EndDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
            console.log('begin date.', $scope.document.Additional.BeginDate);
            console.log('end date.', $scope.document.Additional.EndDate);
        };

        /* ====== Wizard form control ====== */
        $scope.wizardFormControl = {
            receiptIsActive: false,
            withoutReceiptIsActive: false
        };
        /* ====== !Wizard form control ====== */

        /* Watcher */

        //$scope.grandTotal = 0;
        $scope.$watch('document.Additional.ExpenseReportReceiptList', function (newObj, oldObj) {
            // calculate grand total
            var sumTotal = 0;
            if (angular.isDefined(newObj) && newObj != null) {
                for (var i = 0; i < newObj.length; i++) {
                    var amount = 0;
                    if (newObj[i].IsVisible) {
                        if (newObj[i].IsReceipt) {
                            amount = Number(newObj[i].LocalTotalAmount);
                        } else {
                            amount = Number(newObj[i].LocalNoVATTotalAmount);
                        }
                    }
                    amount = isNaN(amount) ? 0 : amount;
                    sumTotal += amount;
                }
            }
            $scope.document.Additional.TotalAmount = sumTotal;
            console.log('GrandTotal.', sumTotal);
        }, true);

        /* !Watcher */


        // auto complete set
        $scope.st = {
            searchCostCenterText: '',
            searchCostCenter_Text: '',
            searchOrgText: '',
            searchIoText:''
        }



        //auto complete CostCenter 
        function createFilterForCostCenter(query) {
            var lowercaseQuery = angular.lowercase(query);
            return function filterFn(x) {
                if (!x) return false;
                var source = angular.lowercase(x.CostCenterName);
                return (source.indexOf(lowercaseQuery) >= 0);
            };
        }

        var tempResult_cc;
        $scope.init_CostCenter = function () {
            for (var i = 0; i < $scope.masterData.objCostcenterDistribution.length; i++) {
                if ($scope.masterData.objCostcenterDistribution[i].CostCenterCode == $scope.document.Additional.CostCenter) {
                    $scope.st.searchCostCenterText = $scope.masterData.objCostcenterDistribution[i].CostCenterName;
                    tempResult_cc = $scope.masterData.objCostcenterDistribution[i];
                }
            }
        }
        $scope.querySearchCostCenter = function (query) {
            if (!$scope.masterData.objCostcenterDistribution) return;
            var results = angular.copy(query ? $scope.masterData.objCostcenterDistribution.filter(createFilterForCostCenter(query)) : $scope.masterData.objCostcenterDistribution), deferred;
            results = results.splice(0, 30);
            if ($scope.simulateQuery) {
                deferred = $q.defer();
                $timeout(function () { deferred.resolve(results); }, Math.random() * 250, false);
                return deferred.promise;
            } else {
                return results;
            }
        };
        $scope.selectedItemCostCenterChange = function (item) {
            if (angular.isDefined(item) && item != null) {
                $scope.st.searchCostCenterText = item.CostCenterName;
                $scope.document.Additional.CostCenter = item.CostCenterCode;
                tempResult_cc = angular.copy(item);
            }
            else {
                $scope.st.searchCostCenterText = '';
            }
        };
        $scope.tryToSelect_cc = function (text) {
            //tempResult_cc = angular.copy(text);
            $scope.st.searchCostCenterText = '';
        }
        $scope.checkText_cc = function (text) {
            var result = null;
            if (text) {

                for (var i = 0; i < $scope.masterData.objCostcenterDistribution.length; i++) {
                    if ($scope.masterData.objCostcenterDistribution[i].CostCenterName == text
                        || $scope.masterData.objCostcenterDistribution[i].CostCenterCode == text) {
                        result = $scope.masterData.objCostcenterDistribution[i];
                        break;
                    }
                }

            }
            if (result) {
                $scope.st.searchCostCenterText = result.CostCenterName;
                $scope.selectedItemCostCenterChange(result, -1);
            } else if (tempResult_cc) {
                $scope.st.searchCostCenterText = tempResult_cc.CostCenterName;
                $scope.selectedItemCostCenterChange(tempResult_cc, -1);
            }
        }

        var tempResult_cc_;
        $scope.init_CostCenter_ = function () {
            for (var i = 0; i < $scope.masterData.objCostcenter.length; i++) {
                if ($scope.masterData.objCostcenter[i].CostCenterCode == $scope.document.Additional.CostCenter) {
                    $scope.st.searchCostCenter_Text = $scope.masterData.objCostcenter[i].CostCenterName;
                    tempResult_cc_ = $scope.masterData.objCostcenter[i];
                }
            }
        }
        $scope.querySearchCostCenter_ = function (query) {
            console.log('masterData.objCostcenter', $scope.masterData.objCostcenter);
            if (!$scope.masterData.objCostcenter) return;
            var results = angular.copy(query ? $scope.masterData.objCostcenter.filter(createFilterForCostCenter(query)) : $scope.masterData.objCostcenter), deferred;
            results = results.splice(0, 30);
            if ($scope.simulateQuery) {
                deferred = $q.defer();
                $timeout(function () { deferred.resolve(results); }, Math.random() * 250, false);
                return deferred.promise;
            } else {
                return results;
            }
        };
        $scope.selectedItemCostCenterChange_ = function (item) {
            if (angular.isDefined(item) && item != null) {
                $scope.st.searchCostCenter_Text = item.CostCenterName;
                $scope.document.Additional.CostCenter = item.CostCenterCode;
                tempResult_cc_ = angular.copy(item);
            }
            else {
                $scope.st.searchCostCenter_Text = '';
            }
        };
        $scope.tryToSelect_cc_ = function (text) {
            //tempResult_cc_ = angular.copy(text);
            $scope.st.searchCostCenter_Text = '';
        }
        $scope.checkText_cc_ = function (text) {
            var result = null;
            if (text) {

                for (var i = 0; i < $scope.masterData.objCostcenter.length; i++) {
                    if ($scope.masterData.objCostcenter[i].CostCenterName == text
                        || $scope.masterData.objCostcenter[i].CostCenterCode == text) {
                        result = $scope.masterData.objCostcenter[i];
                        break;
                    }
                }

            }
            if (result) {
                $scope.st.searchCostCenter_Text = result.CostCenterName;
                $scope.selectedItemCostCenterChange_(result, -1);
            } else if (tempResult_cc_) {
                $scope.st.searchCostCenter_Text = tempResult_cc_.CostCenterName;
                $scope.selectedItemCostCenterChange_(tempResult_cc_, -1);
            }
        }


        //auto complete CostCenter 
        function createFilterForOrg(query) {
            var lowercaseQuery = angular.lowercase(query);
            return function filterFn(x) {
                if (!x) return false;
                var source = angular.lowercase(x.BudgetOrgName);
                return (source.indexOf(lowercaseQuery) >= 0);
            };
        }
        var tempResult_org;
        $scope.init_Org = function () {

            //if ($scope.document.RequestNo == "DUMMY") {
                
            //    for (var i = 0; i < $scope.masterData.objOrganization.length; i++) {
            //        if ($scope.masterData.objOrganization[i].ObjectID == $scope.document.Requestor.OrgUnit) {
            //            $scope.st.searchOrgText = $scope.masterData.objOrganization[i].BudgetOrgName;
            //            tempResult_org = $scope.masterData.objOrganization[i];
            //        }
            //    }
            //}else{
              
            //}

            for (var i = 0; i < $scope.masterData.objOrganization.length; i++) {
                if ($scope.masterData.objOrganization[i].ObjectID == $scope.document.Additional.AlternativeIOOrg) {
                    $scope.st.searchOrgText = $scope.masterData.objOrganization[i].BudgetOrgName;
                    tempResult_org = $scope.masterData.objOrganization[i];
                }
            }
            
        }
        $scope.querySearchOrg = function (query) {
            if (!$scope.masterData.objOrganization) return;
            var results = angular.copy(query ? $scope.masterData.objOrganization.filter(createFilterForOrg(query)) : $scope.masterData.objOrganization), deferred;
            results = results.splice(0, 30);
            if ($scope.simulateQuery) {
                deferred = $q.defer();
                $timeout(function () { deferred.resolve(results); }, Math.random() * 250, false);
                return deferred.promise;
            } else {
                return results;
            }
        };
        $scope.selectedItemOrgChange = function (item,isNotCheckItem) {
            if ( (angular.isDefined(item) && item != null) || !isNotCheckItem) {
                $scope.st.searchOrgText = item.BudgetOrgName;
                $scope.document.Additional.AlternativeIOOrg = item.ObjectID;
                tempResult_org = angular.copy(item);
                $scope.getIOByOrganization($scope.document.Additional.AlternativeIOOrg, true);
            }
            else {
                $scope.st.searchOrgText = '';
            }
        };
        $scope.tryToSelect_org = function (text) {
            //tempResult_org = angular.copy(text);
            $scope.st.searchOrgText = '';
        }
        $scope.checkText_org = function (text) {
            var result = null;
            if (text) {

                for (var i = 0; i < $scope.masterData.objOrganization.length; i++) {
                    if ($scope.masterData.objOrganization[i].Text == text
                        || $scope.masterData.objOrganization[i].ObjectID == text) {
                        result = $scope.masterData.objOrganization[i];
                        break;
                    }
                }

            }
            if (result) {
                $scope.st.searchOrgText = result.BudgetOrgName;
                //$scope.selectedItemOrgChange(result, false);
            } else if (tempResult_org) {
                $scope.st.searchOrgText = tempResult_org.BudgetOrgName;
                $scope.selectedItemOrgChange(tempResult_org, false);
            }
        }


        //auto complete IO 
        function createFilterForIo(query) {
            var lowercaseQuery = angular.lowercase(query);
            return function filterFn(x) {
                if (!x) return false;
                var source = angular.lowercase(x.OrderID + " : " + x.Description);
                return (source.indexOf(lowercaseQuery) >= 0);
            };
        }
        var tempResult_io;
        $scope.init_Io = function () {
            for (var i = 0; i < $scope.objIO.length; i++) {
                if ($scope.objIO[i].ObjectID == $scope.document.Additional.AlternativeIOOrg) {
                    //$scope.st.searchIoText = $scope.objIO.InternalOrderName;
                    $scope.st.searchIoText = $scope.objIO.OrderID + " : " + $scope.objIO.Description;
                    tempResult_io = $scope.objIO[i];
                }
            }
        }
        $scope.querySearchIo = function (query) {
            if (!$scope.objIO) return;
            var results = angular.copy(query ? $scope.objIO.filter(createFilterForIo(query)) : $scope.objIO), deferred;
            results = results.splice(0, 30);
            if ($scope.simulateQuery) {
                deferred = $q.defer();
                $timeout(function () { deferred.resolve(results); }, Math.random() * 250, false);
                return deferred.promise;
            } else {
                return results;
            }
        };
        $scope.selectedItemIoChange = function (item, isNotCheckItem) {
            if ((angular.isDefined(item) && item != null) || !isNotCheckItem) {
                $scope.st.searchIoText = item.OrderID + " : " + item.Description;
              //  $scope.document.Additional.AlternativeIOOrg = (item.ObjectID == null) ? "" : item.ObjectID;
                $scope.document.Additional.IO = item.OrderID;
                tempResult_io = angular.copy(item);
            }
            else {
                $scope.st.searchIoText = '';
            }
        };
        $scope.tryToSelect_Io = function (text) {
            //tempResult_io = angular.copy(text);
            $scope.st.searchIoText = '';
        }
        $scope.checkText_Io = function (text) {
            var result = null;
            if (text) {

                for (var i = 0; i < $scope.objIO.length; i++) {
                    if ($scope.objIO[i].Description == text
                        || $scope.objIO[i].OrderID == text) {
                        result = $scope.objIO[i];
                        break;
                    }
                }
            }
            if (result) {
                $scope.st.searchIoText = result.InternalOrderName;
                //$scope.selectedItemIoChange(result, false);
            } else if (tempResult_io) {
                $scope.st.searchIoText = tempResult_io.InternalOrderName;
                $scope.selectedItemIoChange(tempResult_io, false);
            }
        }
       

        $scope.changeDate = function () {

            //$scope.travelDate.BeginDate = $('#dateFrom[name="datefilter"]').val();
            //$scope.travelDate.EndDate = $('#dateTo[name="datefilter"]').val();
            $scope.document.Additional.BeginDate = $filter('date')($scope.dateRang.startDate, 'yyyy-MM-ddT00:00:00');
            $scope.document.Additional.EndDate = $filter('date')($scope.dateRang.endDate, 'yyyy-MM-ddT00:00:00');
        }
        $scope.initTravelDateCalendar = function () {
            $('input[name="datefilter"]').daterangepicker({
                singleDatePicker: true,
                autoUpdateInput: true,
                "startDate": new Date(),
                "endDate": new Date(),
                locale: {
                    cancelLabel: 'Clear',
                    format: 'DD/MM/YYYY'
                }
            });

            $('input[name="datefilter"]').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY'));
            });

            $('input[name="datefilter"]').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });


            $scope.changeDate();
        } 

    }]);

})();







