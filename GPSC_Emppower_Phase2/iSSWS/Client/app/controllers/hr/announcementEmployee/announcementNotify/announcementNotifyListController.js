﻿(function () {
    angular.module('ESSMobile')
        .controller('announcementNotifyListController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', '$timeout', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal, $timeout) {


            $scope.profile = getToken(CONFIG.USER);
            $scope.page = 1;
            $scope.itemPerPage = 10;
            $scope.totalItem = 0;
            $scope.loadAnnouncementMainNotify = function () {

                var URL = CONFIG.SERVER + 'Announcement/GetAllAnnouncementMainNotify';
                $scope.list_announcement_notify = [];
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {
                        Page: $scope.page,
                        ItemPerPage: $scope.itemPerPage
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {

                    console.log(response.data);
                    $scope.totalItem = response.data.Total;
                    $scope.list_announcement_notify = response.data.List_AnnouncementMain;

                    $scope.loader.enable = false;
                }, function (response) {
                    console.log(response);
                    $scope.loader.enable = false;
                });
            };
            $scope.loadAnnouncementMainNotify();

            $scope.ViewAnnouncementDetail = function (announcementId) {
                var url = '/frmViewContent/7014/' + announcementId;
                $location.path(url);
            };
        }]);
})();