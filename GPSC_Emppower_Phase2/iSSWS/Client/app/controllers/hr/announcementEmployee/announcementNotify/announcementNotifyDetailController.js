﻿(function () {
    angular.module('ESSMobile')
        .controller('announcementNotifyDetailController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', '$timeout', '$sce', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal, $timeout, $sce) {

            if ($routeParams.itemKey) {


                $scope.profile = getToken(CONFIG.USER);
                $scope.model = {};

                var URL = CONFIG.SERVER + 'Announcement/GetAnnouncementMainNotifyDetail';
                $scope.list_announcement_notify = [];
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {
                        AnnouncementId: $routeParams.itemKey
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {

                    $scope.model = response.data;
                    if ($scope.model) {
                        $scope.UpdateStatusReadAnnouncement($routeParams.itemKey);
                    }

                    $scope.loader.enable = false;
                }, function (response) {
                    console.log(response);
                    $scope.loader.enable = false;
                });

                $scope.renderHtml = function (html_code) {
                    return $sce.trustAsHtml(html_code);
                };
                $scope.UpdateStatusReadAnnouncement = function (announcementId) {

                    var URL = CONFIG.SERVER + 'Announcement/SaveViewNotifyAnnouncementMain';
                    $scope.employeeData = getToken(CONFIG.USER);

                    var oRequestParameter = {
                        InputParameter: {
                            AnnouncementId: announcementId
                        }
                        , CurrentEmployee: $scope.employeeData
                        , Requestor: $scope.requesterData
                        , Creator: getToken(CONFIG.USER)
                    };

                    $scope.loader.enable = true;
                    $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {
                        $scope.loader.enable = false;
                    }, function (response) {
                        console.log(response);
                        $scope.loader.enable = false;
                    });
                };
            }
            else
            {
                var url = '/frmViewContent/7013';
                $location.path(url);
            }
        }]);
})();