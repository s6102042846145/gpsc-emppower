﻿(function () {
    angular.module('ESSMobile')
        .controller('groupAnnouncementController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {

            $scope.leftReport = [];
            $scope.rightReport = [];

            $scope.loadGroupReport = function () {

                var URL = CONFIG.SERVER + 'Announcement/GetGroupAnnouncement';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {
                        AnnouncementGroupId: 7000
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    $scope.leftReport = response.data.LeftReport;
                    $scope.rightReport = response.data.RightReport;

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.loadGroupReport();

            $scope.next = function (item) {

                var url = '/frmViewContent/' + item.SubjectReportSubID;
                $location.path(url);
            };
        }]);
})();