﻿(function () {
    angular.module('ESSMobile')
        .controller('listPublicRelationsTextController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {


            $scope.check_all = false;

            // Get Config Multi Company
            $scope.isHideMultiCompany = true;
            $scope.GetConfingMultiCompany = function () {
                var URL = CONFIG.SERVER + 'Employee/GetConfigMultiCompany';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data === 1) {
                        $scope.isHideMultiCompany = false;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetConfingMultiCompany();


            $scope.employeeData = getToken(CONFIG.USER);
            $scope.ddlCompany = $scope.employeeData.CompanyCode;
            $scope.GetAuthorizationCompany = function () {
                var URL = CONFIG.SERVER + 'workflow/GetAuthorizationCompany';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.CompanyList = response.data;

                    if ($scope.CompanyList.length > 0) {
                        $scope.ddlCompany = $scope.CompanyList[0].CompanyCode;
                        $scope.loadListAnnouncementAll();
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetAuthorizationCompany();

            $scope.changeCompany = function (CompanyCode) {
                $scope.ddlCompany = CompanyCode;
                $scope.loadListAnnouncementAll();
            };

            $scope.back = function () {
                var url = '/frmViewContent/7000';
                $location.path(url);
            };

            $scope.CreateNews = function () {
                var url = '/frmViewContent/7008';
                $location.path(url);
            };

            $scope.model_list = [];
            $scope.loadListAnnouncementAll = function () {

                var URL = CONFIG.SERVER + 'Announcement/GetAllGetAnnouncementTextByAdmin';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {
                        CompanyCode: $scope.ddlCompany
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.model_list = response.data;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    console.log(response);
                    $scope.loader.enable = false;
                });
            };

            $scope.deleteAnnouncement = function (announcementId) {

                Swal.fire({
                    title: 'ต้องการลบประกาศ ?',
                    text: "หากคุณ ยืนยัน จะไม่สามารถเปลี่ยนกลับได้",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'ยืนยัน',
                    cancelButtonText: 'ยกเลิก'
                }).then((result) => {

                    if (result.value)
                        $scope.saveDeleteAnnouncement(announcementId);
                });

            };

            $scope.saveDeleteAnnouncement = function (announcementId) {

                var URL = CONFIG.SERVER + 'Announcement/DeleteAnnouncementTextByAdmin';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {
                        AnnouncementId: announcementId,
                        CompanyCode: $scope.ddlCompany
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {

                    $scope.loader.enable = false;
                    Swal.fire($scope.Text['AdminAnnouncementPublicRelationsText']['Delete_Announcement'], '', 'success');
                    $scope.loadListAnnouncementAll();

                }, function (response) {
                    console.log(response);
                    $scope.loader.enable = false;
                });
            };

            $scope.editSequenceAnnouncement = function () {

                if ($scope.model_list.length <= 0) {
                    Swal.fire($scope.Text['AdminAnnouncementPublicRelationsText']['Delete_Announcement'], '', 'error');
                    return;
                }

                var i;
                var count_check = 0;
                for (i = 0; i < $scope.model_list.length; i++) {
                    if ($scope.model_list[i].IsChecked)
                        count_check++;
                }

                if (count_check === 0) {
                    Swal.fire($scope.Text['AdminAnnouncementPublicRelationsText']['Select_Data'], '', 'error');
                    return;
                }

                var list_edit = [];
                for (i = 0; i < $scope.model_list.length; i++) {

                    if ($scope.model_list[i].IsChecked) {
                        var obj = {
                            AnnouncementId: $scope.model_list[i].AnnouncementId,
                            Sequence: $scope.model_list[i].Sequence
                        };
                        list_edit.push(obj);
                    }
                }

                var URL = CONFIG.SERVER + 'Announcement/UpdateSequenceAnnouncementTextByAdmin';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {
                        CompanyCode: $scope.ddlCompany
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                    , list_sequence: list_edit
                };

                $scope.loader.enable = true;
                $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {
                    $scope.loader.enable = false;
                    Swal.fire($scope.Text['AdminAnnouncementPublicRelationsText']['Save_Sequence'], '', 'success');
                    $scope.loadListAnnouncementAll();
                    $scope.check_all = false;
                }, function (response) {
                    console.log(response);
                    $scope.loader.enable = false;
                });

            };

            $scope.editAnnouncement = function (announcementId) {
                var url = '/frmViewContent/7009/' + announcementId + '/' + $scope.ddlCompany;
                $location.path(url);
            };

            $scope.check_data_all = function (ischeck) {

                if ($scope.model_list.length > 0) {
                    var i;
                    if (ischeck) {
                        for (i = 0; i < $scope.model_list.length; i++)
                            $scope.model_list[i].IsChecked = true;
                    }
                    else {
                        for (i = 0; i < $scope.model_list.length; i++)
                            $scope.model_list[i].IsChecked = false;
                    }
                }

            };

        }]);
})();