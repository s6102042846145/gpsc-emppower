﻿(function () {
    angular.module('ESSMobile')
        .controller('createPublicRelationsController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', 'Upload', '$uibModal', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, Upload, $uibModal) {

            var now = new Date();
            var d = new Date();
            d.setDate(d.getDate() + 30);

            $scope.model = {
                Title_TH: "",
                Title_EN: "",
                Link: "",
                EffectiveDate: $filter('date')(now, 'yyyy-MM-dd'),
                ExpireDate: $filter('date')(d, 'yyyy-MM-dd'),
                IsUnlimitExpireDate: false,
                Sequence: 1,
                StatusNews: 1,
                EmployeeId: $scope.requesterData.EmployeeID,
                CompanyCode: $scope.requesterData.CompanyCode,
                File_TH: "",
                File_EN: ""
            };


            $scope.back = function () {
                var url = '/frmViewContent/7004';
                $location.path(url);
            };

            // Get Config Multi Company
            $scope.isHideMultiCompany = true;
            $scope.GetConfingMultiCompany = function () {
                var URL = CONFIG.SERVER + 'Employee/GetConfigMultiCompany';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data === 1) {
                        $scope.isHideMultiCompany = false;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetConfingMultiCompany();

            $scope.setEffectiveDate = function (selectDate) {
                $scope.model.EffectiveDate = $filter('date')(selectDate, 'yyyy-MM-dd');
            };

            $scope.setExpireDate = function (selectDate) {
                $scope.model.ExpireDate = $filter('date')(selectDate, 'yyyy-MM-dd');
            };

            $scope.list_status = [];
            $scope.getStatusAnnouncement = function () {
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {
                        CompanyCode: $scope.ddlCompany
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'Announcement/GetAnnouncementStatus',
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.list_status = response.data;
                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            


            $scope.uploadFile = function (files, language) {

                if (files) {

                    Upload.upload({
                        method: 'POST',
                        url: CONFIG.SERVER + 'Announcement/UploadFileAnnouncementPublicRelations',
                        file: files
                    }).then(function (resp) {

                        if (language === "TH") {
                            $scope.model.File_TH = resp.data;
                        }

                        if (language === "EN") {
                            $scope.model.File_EN = resp.data;
                        }

                    }, function (resp) {
                        console.log('Error status: ' + resp.status);
                    }, function (evt) {
                        //var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                        //console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                    });
                }

            };

            $scope.deleteFile = function (language) {

                if (language === "TH") {
                    $scope.model.File_TH = "";
                }

                if (language === "EN") {
                    $scope.model.File_EN = "";
                }
            };

            $scope.saveNews = function () {

                if (!$scope.model.Title_TH) {
                    Swal.fire($scope.Text['AdminAnnouncementPublicRelations_Create']['Warning_Title_TH'], '', 'warning');
                    return;
                }

                if (!$scope.model.Title_EN) {
                    Swal.fire($scope.Text['AdminAnnouncementPublicRelations_Create']['Warning_Title_EN'], '', 'warning');
                    return;
                }

                if (!$scope.model.Sequence) {
                    Swal.fire($scope.Text['AdminAnnouncementPublicRelations_Create']['Warning_Sequence'], '', 'warning');
                    return;
                }

                if (!$scope.model.File_TH) {
                    Swal.fire("กรุณาใส่ข้อมูลสื่อความภาษาไทย", '', 'warning');
                    return;
                }

                if (!$scope.model.File_EN) {
                    Swal.fire("กรุณาใส่ข้อมูลสื่อความภาษาอังกฤษ", '', 'warning');
                    return;
                }


                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {
                        Title_TH: $scope.model.Title_TH,
                        Title_EN: $scope.model.Title_EN,
                        Link: $scope.model.Link,
                        EffectiveDate: $scope.model.EffectiveDate,
                        ExpireDate: $scope.model.ExpireDate,
                        IsUnlimitExpireDate: $scope.model.IsUnlimitExpireDate,
                        Sequence: $scope.model.Sequence,
                        EmployeeId: $scope.model.EmployeeId,
                        CompanyCode: $scope.ddlCompany,
                        File_TH: $scope.model.File_TH,
                        File_EN: $scope.model.File_EN
                         
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: CONFIG.SERVER + 'Announcement/SaveAnnouncementPublicRelations',
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.status === 204) {

                        Swal.fire($scope.Text['AdminAnnouncementPublicRelations_Create']['SaveComplete'], '', 'success');
                        var url = '/frmViewContent/7004';
                        $location.path(url);
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                        $scope.loader.enable = false;
                        Swal.fire('ไม่สามารถสร้างประกาศได้กรุณาติดต่อ Admin', '', 'error');
                });

            };


            $scope.employeeData = getToken(CONFIG.USER);
            $scope.ddlCompany = $scope.employeeData.CompanyCode;
            $scope.GetAuthorizationCompany = function () {
                var URL = CONFIG.SERVER + 'workflow/GetAuthorizationCompany';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.CompanyList = response.data;

                    if ($scope.CompanyList.length > 0) {
                        $scope.ddlCompany = $scope.CompanyList[0].CompanyCode;
                        $scope.getStatusAnnouncement();
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetAuthorizationCompany();
        }]);
})();