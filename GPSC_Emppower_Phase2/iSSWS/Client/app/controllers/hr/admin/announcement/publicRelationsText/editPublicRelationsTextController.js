﻿(function () {
    angular.module('ESSMobile')
        .controller('editPublicRelationsTextController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {

            $scope.back = function () {
                var url = '/frmViewContent/7007';
                $location.path(url);
            };

            if ($routeParams.itemKey && $routeParams.companycode) {

                // Get Config Multi Company
                $scope.isHideMultiCompany = true;
                $scope.GetConfingMultiCompany = function () {
                    var URL = CONFIG.SERVER + 'Employee/GetConfigMultiCompany';
                    var oRequestParameter = {
                        InputParameter: {}
                        , CurrentEmployee: $scope.employeeData
                        , Requestor: $scope.requesterData
                        , Creator: getToken(CONFIG.USER)
                    };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {

                        if (response.data === 1) {
                            $scope.isHideMultiCompany = false;
                        }

                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                    });
                };
                $scope.GetConfingMultiCompany();

                $scope.model = {};
                $scope.loadAnnouncementText = function () {

                    var URL = CONFIG.SERVER + 'Announcement/GetDataEditAnnouncementTextByAdmin';
                    $scope.employeeData = getToken(CONFIG.USER);

                    var oRequestParameter = {
                        InputParameter: {
                            AnnouncementId: $routeParams.itemKey,
                            CompanyCode: $routeParams.companycode
                        }
                        , CurrentEmployee: $scope.employeeData
                        , Requestor: $scope.requesterData
                        , Creator: getToken(CONFIG.USER)
                    };

                    $scope.loader.enable = true;
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.model = response.data;
                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                        console.log(response);
                        $scope.loader.enable = false;
                    });

                };

                $scope.list_status = [];
                $scope.getStatusAnnouncement = function () {
                    $scope.employeeData = getToken(CONFIG.USER);
                    var oRequestParameter = {
                        InputParameter: {
                            CompanyCode: $routeParams.companycode
                        }
                        , CurrentEmployee: $scope.employeeData
                        , Requestor: $scope.requesterData
                        , Creator: getToken(CONFIG.USER)
                    };

                    $scope.loader.enable = true;
                    $http({
                        method: 'POST',
                        url: CONFIG.SERVER + 'Announcement/GetAnnouncementStatus',
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.list_status = response.data;
                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                    });
                };
                $scope.getStatusAnnouncement();

                $scope.setEffectiveDate = function (selectDate) {
                    $scope.model.EffectiveDate = $filter('date')(selectDate, 'yyyy-MM-ddT00:00:00');
                };

                $scope.setExpireDate = function (selectDate) {
                    $scope.model.ExpireDate = $filter('date')(selectDate, 'yyyy-MM-ddT00:00:00');
                };

                $scope.editAnnouncementText = function () {

                    if (!$scope.model.Title_TH) {
                        Swal.fire($scope.Text['AdminAnnouncementPublicRelationsText_Edit']['Warning_Title_TH'], '', 'warning');
                        return;
                    }

                    if (!$scope.model.Description_TH) {
                        Swal.fire($scope.Text['AdminAnnouncementPublicRelationsText_Edit']['Warning_Description_TH'], '', 'warning');
                        return;
                    }

                    if (!$scope.model.Title_EN) {
                        Swal.fire($scope.Text['AdminAnnouncementPublicRelationsText_Edit']['Warning_Title_EN'], '', 'warning');
                        return;
                    }

                    if (!$scope.model.Description_EN) {
                        Swal.fire($scope.Text['AdminAnnouncementPublicRelationsText_Edit']['Warning_Description_EN'], '', 'warning');
                        return;
                    }

                    if (!$scope.model.Sequence) {
                        Swal.fire($scope.Text['AdminAnnouncementPublicRelationsText_Edit']['Warning_Sequence'], '', 'warning');
                        return;
                    }

                    var URL = CONFIG.SERVER + 'Announcement/UpdateAnnouncementText';
                    $scope.employeeData = getToken(CONFIG.USER);

                    var oRequestParameter = {
                        InputParameter: {
                            Subject_TH: $scope.model.Title_TH
                            , HtmlEditor_TH: $scope.model.Description_TH
                            , Subject_EN: $scope.model.Title_EN
                            , HtmlEditor_EN: $scope.model.Description_EN
                            , EffectiveDate: $scope.model.EffectiveDate
                            , ExpireDate: $scope.model.ExpireDate
                            , IsUnlimitTime: $scope.model.IsUnlimitedExpire
                            , Sequence: $scope.model.Sequence
                            , AnnouncementId: $scope.model.AnnouncementId
                            , AnnouncementStatusId: $scope.model.AnnouncementStatusId
                            , AnnouncementTypeId: $scope.model.AnnouncementTypeId
                            , CompanyCode: $scope.ddlCompany
                        }
                        , CurrentEmployee: $scope.employeeData
                        , Requestor: $scope.requesterData
                        , Creator: getToken(CONFIG.USER)
                    };

                    $scope.loader.enable = true;
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {

                        $scope.loader.enable = false;

                        if (response.status === 204) {
                            Swal.fire($scope.Text['AdminAnnouncementPublicRelationsText_Edit']['SaveComplete'], '', 'success');
                            var url = '/frmViewContent/7007';
                            $location.path(url);
                        }

                       
                    }, function errorCallback(response) {
                        console.log(response);
                        $scope.loader.enable = false;
                    });

                };

                $scope.ddlCompany = $routeParams.companycode.toString();
                $scope.GetAuthorizationCompany = function () {
                    var URL = CONFIG.SERVER + 'workflow/GetAuthorizationCompany';
                    $scope.employeeData = getToken(CONFIG.USER);
                    var oRequestParameter = {
                        InputParameter: {}
                        , CurrentEmployee: $scope.employeeData
                        , Requestor: $scope.requesterData
                        , Creator: getToken(CONFIG.USER)
                    };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.CompanyList = response.data;

                        if ($scope.CompanyList.length > 0) {
                            $scope.loadAnnouncementText();
                        }
                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                    });
                };
                $scope.GetAuthorizationCompany();
            }
        }]);
})();