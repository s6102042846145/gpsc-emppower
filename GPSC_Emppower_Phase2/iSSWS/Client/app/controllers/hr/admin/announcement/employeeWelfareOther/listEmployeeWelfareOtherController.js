﻿(function () {
    angular.module('ESSMobile')
        .controller('listEmployeeWelfareOtherController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', 'Upload', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog) {

            $scope.check_all = false;

            $scope.back = function () {
                var url = '/frmViewContent/7000';
                $location.path(url);
            };

            // Get Config Multi Company
            $scope.isHideMultiCompany = true;
            $scope.GetConfingMultiCompany = function () {
                var URL = CONFIG.SERVER + 'Employee/GetConfigMultiCompany';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data === 1) {
                        $scope.isHideMultiCompany = false;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetConfingMultiCompany();

            $scope.createAnnouncementWelfare = function () {
                var url = '/frmViewContent/7011';
                $location.path(url);
            };

            $scope.listAnnouncementWelfare = [];
            $scope.loadAnnouncementWelfare = function () {

                var URL = CONFIG.SERVER + 'Announcement/GetAnnouncementGeneralEmployeeWelfareByAdmin';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {
                        CompanyCode: $scope.ddlCompany
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {

                    $scope.listAnnouncementWelfare = response.data;

                    $scope.loader.enable = false;

                }, function (response) {
                    console.log(response);
                    $scope.loader.enable = false;
                });
            };

            $scope.deleteAnnouncementWelfare = function (announcementId) {

                Swal.fire({
                    title: 'ต้องการลบประกาศ ?',
                    text: "หากคุณ ยืนยัน จะไม่สามารถเปลี่ยนกลับได้",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'ยืนยัน',
                    cancelButtonText: 'ยกเลิก'
                }).then((result) => {

                    if (result.value)
                        $scope.saveDeleteAnnouncementWelfare(announcementId);
                });

            };
            $scope.saveDeleteAnnouncementWelfare = function (announcementId) {

                var URL = CONFIG.SERVER + 'Announcement/DeleteAnnouncementGeneralEmployeeWelfareByAdmin';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {
                        AnnouncementId: announcementId,
                        CompanyCode: $scope.ddlCompany
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {

                    $scope.loader.enable = false;
                    Swal.fire($scope.Text['AdminImportantNotice']['Delete_Announcement'], '', 'success');
                    $scope.loadAnnouncementWelfare();

                }, function (response) {
                    console.log(response);
                    $scope.loader.enable = false;
                });

            };

            $scope.editAnnouncementWelfare = function (announcementId) {
                var url = '/frmViewContent/7012/' + announcementId + '/' + $scope.ddlCompany;
                $location.path(url);
            };

            $scope.editSequenceAnnouncement = function () {

                if ($scope.listAnnouncementWelfare.length <= 0) {
                    Swal.fire($scope.Text['AdminAnnouncementPublicRelations']['No_Data'], '', 'error');
                    return;
                }

                var i;
                var count_check = 0;
                for (i = 0; i < $scope.listAnnouncementWelfare.length; i++) {
                    if ($scope.listAnnouncementWelfare[i].IsChecked)
                        count_check++;
                }

                if (count_check === 0) {
                    Swal.fire($scope.Text['AdminAnnouncementPublicRelations']['Select_Data'], '', 'error');
                    return;
                }

                var list_edit = [];
                for (i = 0; i < $scope.listAnnouncementWelfare.length; i++) {

                    if ($scope.listAnnouncementWelfare[i].IsChecked) {
                        var obj = {
                            AnnouncementId: $scope.listAnnouncementWelfare[i].AnnouncementId,
                            Sequence: $scope.listAnnouncementWelfare[i].Sequence
                        };
                        list_edit.push(obj);
                    }
                }

                var URL = CONFIG.SERVER + 'Announcement/EditSequenceAnnouncementGeneralEmployeeWelfareByAdmin';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {
                        CompanyCode: $scope.ddlCompany
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                    , list_sequence: list_edit
                };

                $scope.loader.enable = true;
                $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {

                    $scope.loader.enable = false;
                    Swal.fire($scope.Text['AdminAnnouncementPublicRelations']['Save_Sequence'], '', 'success');
                    $scope.loadAnnouncementWelfare();

                    $scope.check_all = false;

                }, function (response) {
                    console.log(response);
                    $scope.loader.enable = false;
                });

            };

            $scope.check_data_all = function (ischeck) {

                if ($scope.listAnnouncementWelfare.length > 0) {
                    var i;
                    if (ischeck) {
                        for (i = 0; i < $scope.listAnnouncementWelfare.length; i++)
                            $scope.listAnnouncementWelfare[i].IsChecked = true;
                    }
                    else {
                        for (i = 0; i < $scope.listAnnouncementWelfare.length; i++)
                            $scope.listAnnouncementWelfare[i].IsChecked = false;
                    }

                }

            };

            $scope.employeeData = getToken(CONFIG.USER);
            $scope.ddlCompany = $scope.employeeData.CompanyCode;
            $scope.GetAuthorizationCompany = function () {
                var URL = CONFIG.SERVER + 'workflow/GetAuthorizationCompany';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.CompanyList = response.data;

                    if ($scope.CompanyList.length > 0) {
                        $scope.ddlCompany = $scope.CompanyList[0].CompanyCode;
                        $scope.loadAnnouncementWelfare();
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetAuthorizationCompany();

            $scope.changeCompany = function (CompanyCode) {
                $scope.ddlCompany = CompanyCode;
                $scope.loadAnnouncementWelfare();
            };

        }]);
})();