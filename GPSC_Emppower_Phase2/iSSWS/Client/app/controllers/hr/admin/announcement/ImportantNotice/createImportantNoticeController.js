﻿(function () {
    angular.module('ESSMobile')
        .controller('createImportantNoticeController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', 'Upload', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, Upload) {

            $scope.back = function () {
                var url = '/frmViewContent/7001';
                $location.path(url);
            };

            // Get Config Multi Company
            $scope.isHideMultiCompany = true;
            $scope.GetConfingMultiCompany = function () {
                var URL = CONFIG.SERVER + 'Employee/GetConfigMultiCompany';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data === 1) {
                        $scope.isHideMultiCompany = false;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetConfingMultiCompany();


            $scope.employeeData = getToken(CONFIG.USER);
            $scope.ddlCompany = $scope.employeeData.CompanyCode;
            $scope.GetAuthorizationCompany = function () {
                var URL = CONFIG.SERVER + 'workflow/GetAuthorizationCompany';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.CompanyList = response.data;

                    if ($scope.CompanyList.length > 0) {
                        $scope.ddlCompany = $scope.CompanyList[0].CompanyCode;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetAuthorizationCompany();

            var now = new Date();
            var d = new Date();
            d.setDate(d.getDate() + 30);

            $scope.model = {
                Title_TH: "",
                Description_TH: "",
                Remark_TH: "",
                Title_EN: "",
                Description_EN: "",
                Remark_EN: "",
                EffectiveDate: $filter('date')(now, 'yyyy-MM-dd'),
                ExpireDate: $filter('date')(d, 'yyyy-MM-dd'),
                IsUnlimitedExpire: false,
                ListPicture_TH: [],
                ListPicture_EN: []
            };

            if ($scope.model.ListPicture_TH.length <= 0) {

                for (var i = 1; i <= 3; i++) {
                    var new_obj = {
                        FileName: "",
                        FileSequence: 0,
                        Language: "TH"
                    };
                    $scope.model.ListPicture_TH.push(new_obj);
                }
            }

            if ($scope.model.ListPicture_EN.length <= 0) {

                for (var j = 1; j <= 3; j++) {
                    var new_obj2 = {
                        FileName: "",
                        FileSequence: 0,
                        Language: "EN"
                    };
                    $scope.model.ListPicture_EN.push(new_obj2);
                }
            }

            $scope.uploadFile = function (files, index, language) {

                if (files) {

                    Upload.upload({
                        method: 'POST',
                        url: CONFIG.SERVER + 'Announcement/UploadFileAnnouncementMain',
                        file: files
                    }).then(function (resp) {

                        if (resp.status === 200) {

                            if (language === 'TH') {

                                if ($scope.model.ListPicture_TH[index]) {
                                    $scope.model.ListPicture_TH[index].FileName = resp.data;
                                    $scope.model.ListPicture_TH[index].FileSequence = 1;
                                    $scope.model.ListPicture_TH[index].Language = language;
                                }
                            }

                            if (language === 'EN') {

                                if ($scope.model.ListPicture_EN[index]) {
                                    $scope.model.ListPicture_EN[index].FileName = resp.data;
                                    $scope.model.ListPicture_EN[index].FileSequence = 1;
                                    $scope.model.ListPicture_EN[index].Language = language;
                                }
                            }

                        }

                    }, function (resp) {
                        console.log('Error status: ' + resp.status);
                    }, function (evt) {
                        //var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                        //console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                    });
                }

            };

            $scope.setEffectiveDate = function (selectDate) {
                $scope.model.EffectiveDate = $filter('date')(selectDate, 'yyyy-MM-dd');
            };

            $scope.setExpireDate = function (selectDate) {
                $scope.model.ExpireDate = $filter('date')(selectDate, 'yyyy-MM-dd');
            };

            $scope.deleteFile = function (index, language) {

                if (language === 'TH')
                    $scope.model.ListPicture_TH[index].FileName = "";

                if (language === 'EN')
                    $scope.model.ListPicture_EN[index].FileName = "";
            };

            $scope.saveAnnouncementMain = function () {

                if (!$scope.model.Title_TH) {
                    Swal.fire($scope.Text['AdminImportantNotice_Create']['Warning_Title_TH'], '', 'warning');
                    return;
                }

                //if (!$scope.model.Description_TH) {
                //    Swal.fire($scope.Text['AdminImportantNotice_Create']['Warning_Desciption_TH'], '', 'warning');
                //    return;
                //}

                if (!$scope.model.Title_EN) {
                    Swal.fire($scope.Text['AdminImportantNotice_Create']['Warning_Title_EN'], '', 'warning');
                    return;
                }

                //if (!$scope.model.Description_EN) {
                //    Swal.fire($scope.Text['AdminImportantNotice_Create']['Warning_Desciption_EN'], '', 'warning');
                //    return;
                //}

                var error = false;
                for (var k = 0; k < $scope.model.ListPicture_TH.length; k++) {

                    if ($scope.model.ListPicture_TH[k].FileName) {

                        if ($scope.model.ListPicture_TH[k].FileSequence == null || $scope.model.ListPicture_TH[k].FileSequence == '') {
                            error = true;
                        }
                    }
                }

                for (var z = 0; z < $scope.model.ListPicture_EN.length; z++) {

                    if ($scope.model.ListPicture_EN[z].FileName) {

                        if ($scope.model.ListPicture_EN[z].FileSequence == null || $scope.model.ListPicture_EN[z].FileSequence == '') {
                            error = true;
                        }
                    }
                }

                if (error) {
                    Swal.fire($scope.Text['AdminImportantNotice_Create']['Warning_Sequence'], '', 'warning');
                    return;
                }


                var URL = CONFIG.SERVER + 'Announcement/SaveAnnouncementMainByAdmin';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {
                        Title_TH: $scope.model.Title_TH,
                        Description_TH: $scope.model.Description_TH,
                        Remark_TH: $scope.model.Remark_TH,
                        Title_EN: $scope.model.Title_EN,
                        Description_EN: $scope.model.Description_EN,
                        Remark_EN: $scope.model.Remark_EN,
                        EffectiveDate: $scope.model.EffectiveDate,
                        ExpireDate: $scope.model.ExpireDate,
                        IsUnlimitedExpire: $scope.model.IsUnlimitedExpire,
                        CompanyCode: $scope.ddlCompany
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                    , ListPicture_TH: $scope.model.ListPicture_TH
                    , ListPicture_EN: $scope.model.ListPicture_EN
                };

                $scope.loader.enable = true;
                $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {

                    $scope.loader.enable = false;
                    if (response.status === 204) {

                        Swal.fire($scope.Text['AdminImportantNotice_Create']['Save_Complete'], '', 'success');
                        var url = '/frmViewContent/7001';
                        $location.path(url);
                    }
                    

                }, function (response) {
                    console.log(response);
                    $scope.loader.enable = false;
                });

            };

        }]);
})();