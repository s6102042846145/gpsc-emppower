﻿(function () {
    angular.module('ESSMobile')
        .controller('createPublicRelationsTextController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal) {

            $scope.back = function () {
                var url = '/frmViewContent/7007';
                $location.path(url);
            };

            // Get Config Multi Company
            $scope.isHideMultiCompany = true;
            $scope.GetConfingMultiCompany = function () {
                var URL = CONFIG.SERVER + 'Employee/GetConfigMultiCompany';
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.data === 1) {
                        $scope.isHideMultiCompany = false;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetConfingMultiCompany();

            $scope.employeeData = getToken(CONFIG.USER);
            $scope.ddlCompany = $scope.employeeData.CompanyCode;
            $scope.GetAuthorizationCompany = function () {
                var URL = CONFIG.SERVER + 'workflow/GetAuthorizationCompany';
                $scope.employeeData = getToken(CONFIG.USER);
                var oRequestParameter = {
                    InputParameter: {}
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {
                    $scope.CompanyList = response.data;

                    if ($scope.CompanyList.length > 0) {
                        $scope.ddlCompany = $scope.CompanyList[0].CompanyCode;
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    $scope.loader.enable = false;
                });
            };
            $scope.GetAuthorizationCompany();


            var date_now = new Date();
            var next_date = new Date();
            next_date.setDate(next_date.getDate() + 30);

            // Editor options.
            $scope.options = {
                language: 'en',
                allowedContent: true,
                entities: false
            };

            $scope.model = {
                Subject_TH: "",
                HtmlEditor_TH: "",
                Subject_EN: "",
                HtmlEditor_EN: "",
                EffectiveDate: $filter('date')(date_now, 'yyyy-MM-dd'),
                ExpireDate: $filter('date')(next_date, 'yyyy-MM-dd'),
                IsUnlimitTime: false,
                Sequence: 1
            };

            $scope.setEffectiveDate = function (selectDate) {
                $scope.model.EffectiveDate = $filter('date')(selectDate, 'yyyy-MM-dd');
            };

            $scope.setExpireDate = function (selectDate) {
                $scope.model.ExpireDate = $filter('date')(selectDate, 'yyyy-MM-dd');
            };

            $scope.createNewsText = function () {

                if (!$scope.model.Subject_TH) {
                    return;
                }

                if (!$scope.model.HtmlEditor_TH) {
                    return;
                }

                if (!$scope.model.Subject_EN) {
                    return;
                }

                if (!$scope.model.HtmlEditor_EN) {
                    return;
                }

                var URL = CONFIG.SERVER + 'Announcement/CreateAnnouncementText';
                $scope.employeeData = getToken(CONFIG.USER);

                var oRequestParameter = {
                    InputParameter: {
                        Subject_TH: $scope.model.Subject_TH
                        , HtmlEditor_TH: $scope.model.HtmlEditor_TH
                        , Subject_EN: $scope.model.Subject_EN
                        , HtmlEditor_EN: $scope.model.HtmlEditor_EN
                        , EffectiveDate: $scope.model.EffectiveDate
                        , ExpireDate: $scope.model.ExpireDate
                        , IsUnlimitTime: $scope.model.IsUnlimitTime
                        , Sequence: $scope.model.Sequence
                        , CompanyCode: $scope.ddlCompany
                    }
                    , CurrentEmployee: $scope.employeeData
                    , Requestor: $scope.requesterData
                    , Creator: getToken(CONFIG.USER)
                };

                $scope.loader.enable = true;
                $http({
                    method: 'POST',
                    url: URL,
                    data: oRequestParameter
                }).then(function successCallback(response) {

                    if (response.status === 204) {
                        Swal.fire($scope.Text['AdminAnnouncementPublicRelationsText_Create']['SaveComplete'], '', 'success');
                        var url = '/frmViewContent/7007';
                        $location.path(url);
                    }

                    $scope.loader.enable = false;
                }, function errorCallback(response) {
                    Swal.fire('ไม่สามารถสร้างประกาศข้อความได้', '', 'error');
                    $scope.loader.enable = false;
                });

            };

        }])
        .controller('completeSaveAnnouncementTextController', ['items', '$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', '$uibModal', '$uibModalInstance', function (items, $scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, $uibModal, $uibModalInstance) {

            $scope.msg_result = "";
            $scope.msg_result = items.msg;
            $scope.Text = items.text;
            $scope.closeModal = function () {
                $uibModalInstance.close();
            };

        }]);
})();