﻿(function () {
    angular.module('ESSMobile')
        .controller('editPublicRelationsController', ['$scope', '$http', '$routeParams', '$location', '$filter', 'CONFIG', '$mdDialog', 'Upload', function ($scope, $http, $routeParams, $location, $filter, CONFIG, $mdDialog, Upload) {

            $scope.back = function () {
                var url = '/frmViewContent/7004';
                $location.path(url);
            };

            if ($routeParams.itemKey && $routeParams.companycode) {

                // Get Config Multi Company
                $scope.isHideMultiCompany = true;
                $scope.GetConfingMultiCompany = function () {
                    var URL = CONFIG.SERVER + 'Employee/GetConfigMultiCompany';
                    var oRequestParameter = {
                        InputParameter: {}
                        , CurrentEmployee: $scope.employeeData
                        , Requestor: $scope.requesterData
                        , Creator: getToken(CONFIG.USER)
                    };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {

                        if (response.data === 1) {
                            $scope.isHideMultiCompany = false;
                        }

                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                    });
                };
                $scope.GetConfingMultiCompany();

                $scope.model = {};
                $scope.loadAnnouncementPublicRelations = function () {

                    var URL = CONFIG.SERVER + 'Announcement/GetAnnouncementPublicRelationsById';
                    $scope.employeeData = getToken(CONFIG.USER);

                    var oRequestParameter = {
                        InputParameter: {
                            AnnouncementId: $routeParams.itemKey,
                            CompanyCode: $routeParams.companycode
                        }
                        , CurrentEmployee: $scope.employeeData
                        , Requestor: $scope.requesterData
                        , Creator: getToken(CONFIG.USER)
                    };

                    $scope.loader.enable = true;
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {

                        $scope.model = response.data;

                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                        console.log(response);
                        $scope.loader.enable = false;
                    });

                };
                
                $scope.list_status = [];
                $scope.getStatusAnnouncement = function () {
                    $scope.employeeData = getToken(CONFIG.USER);
                    var oRequestParameter = {
                        InputParameter: {
                            CompanyCode: $routeParams.companycode
                        }
                        , CurrentEmployee: $scope.employeeData
                        , Requestor: $scope.requesterData
                        , Creator: getToken(CONFIG.USER)
                    };

                    $scope.loader.enable = true;
                    $http({
                        method: 'POST',
                        url: CONFIG.SERVER + 'Announcement/GetAnnouncementStatus',
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.list_status = response.data;
                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                    });
                };
                $scope.getStatusAnnouncement();

                $scope.uploadFile = function (files, language) {

                    if (files) {

                        Upload.upload({
                            method: 'POST',
                            url: CONFIG.SERVER + 'Announcement/UploadFileAnnouncementPublicRelations',
                            file: files
                        }).then(function (resp) {

                            if (language === "TH") {
                                $scope.model.File_TH = resp.data;
                            }

                            if (language === "EN") {
                                $scope.model.File_EN = resp.data;
                            }

                        }, function (resp) {
                            console.log('Error status: ' + resp.status);
                        }, function (evt) {
                            //var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                            //console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                        });
                    }

                };

                $scope.deleteFile = function (language) {

                    if (language === "TH") {
                        $scope.model.File_TH = "";
                    }

                    if (language === "EN") {
                        $scope.model.File_EN = "";
                    }
                };

                $scope.editAnnouncementPublicRelations = function () {

                    if (!$scope.model.Title_TH) {
                        Swal.fire($scope.Text['AdminAnnouncementPublicRelations_Edit']['Warning_Title_TH'], '', 'warning');
                        return;
                    }

                    if (!$scope.model.Title_EN) {
                        Swal.fire($scope.Text['AdminAnnouncementPublicRelations_Edit']['Warning_Title_EN'], '', 'warning');
                        return;
                    }

                    if (!$scope.model.Sequence) {
                        Swal.fire($scope.Text['AdminAnnouncementPublicRelations_Edit']['Warning_Sequence'], '', 'warning');
                        return;
                    }

                    $scope.employeeData = getToken(CONFIG.USER);
                    var oRequestParameter = {
                        InputParameter: {
                            Title_TH: $scope.model.Title_TH,
                            Title_EN: $scope.model.Title_EN,
                            TextLink: $scope.model.TextLink == null ? "" : $scope.model.TextLink,
                            EffectiveDate: $scope.model.EffectiveDate,
                            ExpireDate: $scope.model.ExpireDate,
                            IsUnlimitTime: $scope.model.IsUnlimitedExpire,
                            Sequence: $scope.model.Sequence,
                            AnnouncementId: $scope.model.AnnouncementId,
                            AnnouncementStatusId: $scope.model.AnnouncementStatusId,
                            File_TH_Id: $scope.model.File_TH_Id,
                            File_TH: $scope.model.File_TH == null ? "" : $scope.model.File_TH,
                            File_EN_Id: $scope.model.File_EN_Id,
                            File_EN: $scope.model.File_EN == null ? "" : $scope.model.File_EN,
                            CompanyCode: $routeParams.companycode.toString()
                        }
                        , CurrentEmployee: $scope.employeeData
                        , Requestor: $scope.requesterData
                        , Creator: getToken(CONFIG.USER)
                    };

                    $scope.loader.enable = true;
                    $http({
                        method: 'POST',
                        url: CONFIG.SERVER + 'Announcement/EditAnnouncementPublicRelations',
                        data: oRequestParameter
                    }).then(function successCallback(response) {

                        $scope.loader.enable = false;
                        Swal.fire($scope.Text['AdminAnnouncementPublicRelations_Edit']['SaveComplete'], '', 'success');
                        var url = '/frmViewContent/7004';
                        $location.path(url);

                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                    });
                };

                $scope.ddlCompany = $routeParams.companycode.toString();
                $scope.GetAuthorizationCompany = function () {
                    var URL = CONFIG.SERVER + 'workflow/GetAuthorizationCompany';
                    $scope.employeeData = getToken(CONFIG.USER);
                    var oRequestParameter = {
                        InputParameter: {}
                        , CurrentEmployee: $scope.employeeData
                        , Requestor: $scope.requesterData
                        , Creator: getToken(CONFIG.USER)
                    };
                    $http({
                        method: 'POST',
                        url: URL,
                        data: oRequestParameter
                    }).then(function successCallback(response) {
                        $scope.CompanyList = response.data;

                        if ($scope.CompanyList.length > 0) {
                            $scope.loadAnnouncementPublicRelations();
                        }
                        $scope.loader.enable = false;
                    }, function errorCallback(response) {
                        $scope.loader.enable = false;
                    });
                };
                $scope.GetAuthorizationCompany();
            }
        }]);
})();