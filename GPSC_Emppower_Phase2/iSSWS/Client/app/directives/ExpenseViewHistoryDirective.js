﻿(function () {
    "use strict";
    angular.module('ESSMobile')
        .directive('essExpenseViewHistory', function () {
            return {
                restrict: 'AE',
                scope: {
                    Text:'=text',
                    beginFormWizard: '&',
                    finishFormWizard: '&',
                    wizardFormControl: '=',
                    mainDocument: '=document',
                    loader: '=',
                    getTextByCompany: '&'
                },
                templateUrl: 'views/hr/tr/data/essExpenseViewHistory.html',
                controller: ['$scope', '$http', '$routeParams', '$location', '$filter', '$window', 'CONFIG', '$timeout', '$q', '$log', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, $window, CONFIG, $timeout, $q, $log, $mdDialog) {

                    $scope.runtime = Date.now();
                    $scope.ChildAction = {};
                    $scope.blockAction = {
                        isBLock: false,
                        blockMessage: ''
                    };
                    $scope.getFileFromPath = function (attachment) {
                        /* direct */
                        if (angular.isDefined(attachment)) {
                            if (typeof cordova != 'undefined') {

                            } else if (attachment.FilePath) {
                                var path = CONFIG.SERVER + 'Client/files/' + attachment.FilePath + '/' + attachment.FileName;
                                console.debug(path);
                                $window.open(path);
                            } else {
                                var path = CONFIG.SERVER + 'Client/files/' + attachment;
                                console.debug(path);
                                $window.open(path);

                            }
                        }
                        /* !direct */
                    }
                    $scope.getTextByCompany = function (oRequestor) {
                        //$scope.RequestorText = angular.copy($scope.Text);
                        var oRequestParameter = { InputParameter: {}, Requestor: oRequestor }
                        var URL = CONFIG.SERVER + 'workflow/GetTextDescriptionBySystem/';
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            // Success
                            if (response.data != null) {
                                $scope.RequestorText = response.data;
                            }
                            console.log('----------- TextDescription. -----------', angular.copy($scope.Text));
                        }, function errorCallback(response) {
                            // Error
                            $scope.RequestorText = angular.copy($scope.Text);
                            console.log('error MainController TextDescription.', response);
                        });
                    };
                    $scope.setBlockAction = function (isBLock, blockMessage) {
                        $scope.blockAction.isBLock = isBLock;
                        $scope.blockAction.blockMessage = blockMessage;
                    };
                    /* fixed parameters */
                    $scope.keyMaster = 'null';
                    $scope.isOwner = false;
                    $scope.isDataOwner = false;
                    /* !fixed parameters */

                }],
                link: function (scope, element, attrs, controller) {

                }
            };
        });
})();