﻿(function () {
    "use strict";
    angular.module('ESSMobile')
        .directive('essExpenseWithoutReceiptEditor', function () {
            return {
                restrict: 'AE',
                scope: {
                    Text: '=text',
                    beginFormWizard: '&',
                    finishFormWizard: '&',
                    wizardFormControl: '=',
                    document: '=',
                    grouptagname: '@',
                    tagname: '@',
                    projectCodes: '=',
                    settings: '=',
                    masterData: '=',
                    isEditor: '=',
                    childSumData: '=',
                    moduleSetting: '=',
                    systemMasterData: '=',
                    receiptLimitDate: '='
                },
                templateUrl: 'views/hr/tr/data/essExpenseWithoutReceipt.html',
                controller: ['$scope', '$http', '$routeParams', '$location', '$filter', '$window', 'CONFIG', '$timeout', '$q', '$log', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, $window, CONFIG, $timeout, $q, $log, $mdDialog) {

                    // add by jirawat jannet @ 26-07-2018
                    // ป้องกัน amount reset ค่าหลังจาก initial ค่าตอนเปิด dialog
                    var isInitialDialogValue = false;

                    $scope.p_message = function (message, type) {
                        if (type == 'w') {
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .clickOutsideToClose(true)
                                    .title('Warning')
                                    .textContent(message)
                                    .ariaLabel('Warning')
                                    .ok('OK')
                            );
                        } else {
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .clickOutsideToClose(true)
                                    .title($scope.Text['SYSTEM']['INFORMATION'])
                                    .textContent(message)
                                    .ariaLabel($scope.Text['SYSTEM']['INFORMATION'])
                                    .ok('OK')
                            );
                        }
                    }


                    $scope.toggleDefaultIOAndCC = function (item) {
                        if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].IsIOAndCCFollowByDefault) {
                            item.IO = angular.copy($scope.document.Additional.IO);
                            item.CostCenter = angular.copy($scope.document.Additional.CostCenter);
                        }
                    }



                    $scope.runtime = Date.now();
                    $scope.employeeData = getToken(CONFIG.USER);
                    var employeeData = getToken(CONFIG.USER);
                    // Map main table
                    var obj = $scope.document;
                    $scope.ExpenseReport = obj.Additional

                    $scope.isReceipt = false;
                    $scope.Textcategory = 'EXPENSE';

                    /* --- Variable --- */

                    $scope.model = {
                        ready: true,
                        data: $scope.systemMasterData.ExpenseModel
                    };

                    $scope.ddlDefault = {
                        ExpenseTypeGroup: 0,
                        ExpenseType: 0,
                        setVatTypeDefault: 0
                    };

                    $scope.ddlDefaultCarType = '';

                    $scope.GetCarRegistration = function () {
                        var URL = CONFIG.SERVER + 'HRTR/GetAllCarRegistration/';
                        var oRequestParameter = { InputParameter: {}, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };

                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            // Success
                            $scope.CarRegistrationList = response.data;
                        }, function errorCallback(response) {
                            // Error
                            console.log('error CarRegistrationListController.', response);
                        });

                    }

                    // master
                    $scope.isUserRoleAccounting = $scope.systemMasterData.IsAccountUser;
                    $scope.projectForLookup = $scope.masterData.projectForLookup;
                    $scope.exchangeType = $scope.masterData.exchangeType;
                    $scope.ExchangeTypeReady = true;
                    $scope.employeeDataList = $scope.masterData.employeeDataList;
                    $scope.employeeDataList_Buffer = angular.copy($scope.employeeDataList);
                    $scope.carTypes = $scope.masterData.carTypes;
                    $scope.flatRateLocations = $scope.masterData.flatRateLocations;
                    $scope.objVendor = $scope.masterData.objVendor;
                    $scope.objCountry = $scope.masterData.objCountry;
                    $scope.objCostcenterLookup = $scope.masterData.objCostcenterLookup;
                    $scope.objIOLookupObj = $scope.masterData.objIOLookupObj;
                    $scope.setVatTypeDefault = $scope.masterData.setVatTypeDefault;
                    if ($scope.setVatTypeDefault != null && $scope.setVatTypeDefault.length > 0) {
                        $scope.ddlDefault.VATCode = $scope.setVatTypeDefault[0].VATCode;
                    }
                    $scope.setPaymentMethod = $scope.masterData.setPaymentMethod;
                    $scope.setPaymentSupplement = $scope.masterData.setPaymentSupplement;
                    $scope.setPaymentTerm = $scope.masterData.setPaymentTerm;
                    $scope.oTranType = $scope.masterData.oTranType;

                    $scope.getExpenseTypeByGroupID = function (expenseTypeGroupID, isResetDefault) {
                        if (angular.isUndefined(isResetDefault)) {
                            isResetDefault = false;
                        }
                        var ExpenseTypeGroupInfo = $scope.settings.ExpenseTypeGroupInfo;
                        var URL = CONFIG.SERVER + 'HRTR/GetExpenseTypeByGroupID/';
                        var oRequestParameter = {
                            InputParameter: {
                                "ExpenseTypeGroupID": expenseTypeGroupID,
                                "PrefixTagCode": ExpenseTypeGroupInfo.prefix,
                                "IsReceipt": "False",
                                "TravelTypeID": ExpenseTypeGroupInfo.travelTypeID,
                                "IsDomestic": ExpenseTypeGroupInfo.isDomestic
                            }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor
                        };
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            $scope.expenseType = response.data;
                            if (isResetDefault || !$scope.GE.receiptItemEdit) {
                                if ($scope.expenseType != null && $scope.expenseType.length > 0 && $scope.GE.receiptItem != null) {
                                    $scope.GE.receiptItem.ExpenseTypeID = $scope.expenseType[0].ExpenseTypeID.toString();
                                    $scope.ddlDefault.ExpenseTypeName = $scope.expenseType[0].Name.toString();
                                    if ($scope.GE.receiptItem != null) {
                                        $scope.GE.receiptItem.ExpenseTypeName = $scope.ddlDefault.ExpenseTypeName;
                                    }
                                }
                            }
                            if ($scope.GE.receiptItem != null) {
                                $scope.getPanelNameByExpenseType($scope.GE.receiptItem.ExpenseTypeID);
                            }
                            //console.log('expenseType.', $scope.expenseType);
                        }, function errorCallback(response) {
                            $scope.expenseType = [];
                            console.log('error expenseType.', response);
                        });
                    };

                    $scope.GetExpenseTypeGroupByTag = function (groupTag, tag) {
                        var URL = CONFIG.SERVER + 'HRTR/GetExpenseTypeGroupByTag/';

                        var oRequestParameter = { InputParameter: { "grouptag": groupTag, "tag": tag, "TravelRequestNo": $scope.document.Additional ? $scope.document.Additional.TravelRequestNo : '' }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                        console.log('grouptag', groupTag);
                        console.log('TagName', tag);
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            $scope.expenseTypeGroup = response.data;
                            if ($scope.expenseTypeGroup != null && $scope.expenseTypeGroup.length > 0) {
                                $scope.ddlDefault.ExpenseTypeGroupID = $scope.expenseTypeGroup[0].ExpenseTypeGroupID.toString();

                                //--------Nun Add ExpenseTypeGroup Name and Remark 09/06/2016
                                $scope.ddlDefault.ExpenseTypeGroupName = $scope.expenseTypeGroup[0].Name.toString();
                                $scope.ddlDefault.ExpenseTypeGroupRemark = $scope.expenseTypeGroup[0].Remark.toString();
                                //-----------------------------------------------------------
                                $scope.getExpenseTypeByGroupID($scope.ddlDefault.ExpenseTypeGroupID);

                            }
                            //console.log('expenseTypeGroup.', $scope.expenseTypeGroup);

                            /* get expense type */

                        }, function errorCallback(response) {
                            console.log('error expenseTypeGroup.', response);
                        });
                    }

                    // check page name
                    if ($scope.settings.ExpenseTypeGroupInfo.prefix == 'GE') {
                        $scope.pageClassName = 'GeneralExpenseReport';
                    } else {
                        $scope.pageClassName = 'TravelReport';
                    }


                    $scope.DefaultCostCenterIOObj = {
                        CostCenter: '',
                        OrgUnitID: '',
                        IO: '',
                        IsAlternativeCostCenter: false,
                        IsAlternativeIOOrg: false
                    };
                    $scope.DefaultCostCenterIO = {
                        CostCenter: '',
                        OrgUnitID: '',
                        IO: '',
                        IsAlternativeCostCenter: false,
                        IsAlternativeIOOrg: false
                    };
                    $scope.DefaultCostCenterIOByGroup = [];

                    if ($scope.pageClassName == 'GeneralExpenseReport') {
                        // default costcenter, io
                        $scope.DefaultCostCenterIO.CostCenter = $scope.ExpenseReport.CostCenter;
                        $scope.DefaultCostCenterIO.OrgUnitID = $scope.ExpenseReport.AlternativeIOOrg;
                        $scope.DefaultCostCenterIO.IO = $scope.ExpenseReport.IO;
                        $scope.DefaultCostCenterIO.IsAlternativeCostCenter = $scope.ExpenseReport.IsAlternativeCostCenter;
                        $scope.DefaultCostCenterIO.IsAlternativeIOOrg = $scope.ExpenseReport.IsAlternativeIOOrg;
                        
                        $scope.limitMinDate = formatDateString($scope.ExpenseReport.BeginDate);
                        $scope.limitMaxDate = formatDateString($scope.ExpenseReport.EndDate);
                        $scope.savedLimitMinDate = formatDateString($scope.ExpenseReport.BeginDate);
                        $scope.savedLimitMaxDate = formatDateString($scope.ExpenseReport.EndDate);

                        $scope.$watchGroup(['ExpenseReport.CostCenter', 'ExpenseReport.AlternativeIOOrg', 'ExpenseReport.IO'], function (newValues, oldValues, scp) {
                            scp.DefaultCostCenterIO.CostCenter = scp.ExpenseReport.CostCenter;
                            scp.DefaultCostCenterIO.OrgUnitID = scp.ExpenseReport.AlternativeIOOrg;
                            scp.DefaultCostCenterIO.IO = scp.ExpenseReport.IO;
                            scp.DefaultCostCenterIO.IsAlternativeCostCenter = scp.ExpenseReport.IsAlternativeCostCenter;
                            scp.DefaultCostCenterIO.IsAlternativeIOOrg = scp.ExpenseReport.IsAlternativeIOOrg;
                            console.log('DefaultCostCenterIO.', scp.DefaultCostCenterIO);
                        });

                        $scope.$watchGroup(['ExpenseReport.BeginDate', 'ExpenseReport.EndDate'], function (newValues, oldValues, scp) {
                            scp.limitMinDate = formatDateString(scp.ExpenseReport.BeginDate);
                            scp.limitMaxDate = formatDateString(scp.ExpenseReport.EndDate);
                            scp.savedLimitMinDate = formatDateString(scp.ExpenseReport.BeginDate);
                            scp.savedLimitMaxDate = formatDateString(scp.ExpenseReport.EndDate);
                        });

                        console.log('######################################### 1.1.', $scope.grouptagname);
                        console.log('######################################### 1.2.', $scope.tagname);
                        $scope.GetExpenseTypeGroupByTag($scope.grouptagname, $scope.tagname);

                    } else if ($scope.pageClassName == 'TravelReport') {
                        // default costcenter, io
                        $scope.DefaultCostCenterIO.CostCenter = $scope.document.Additional.CostCenter;
                        $scope.DefaultCostCenterIO.OrgUnitID = $scope.document.Additional.AlternativeIOOrg;
                        $scope.DefaultCostCenterIO.IO = $scope.document.Additional.IO;
                        $scope.DefaultCostCenterIO.IsAlternativeCostCenter = $scope.document.Additional.IsAlternativeCostCenter;
                        $scope.DefaultCostCenterIO.IsAlternativeIOOrg = $scope.document.Additional.IsAlternativeIOOrg;

                        // default costcenter, io by ExpenseTypeGroupID
                        //if ($scope.document.Additional.GroupBudgets != null) {
                        //    $.each($scope.document.Additional.GroupBudgets, function (i, item) {
                        //        var obj = angular.copy($scope.DefaultCostCenterIOObj);
                        //        obj.CostCenter = item.CostCenter;
                        //        obj.OrgUnitID = item.AlternativeIOOrg;
                        //        obj.IO = item.IO;
                        //        obj.IsAlternativeCostCenter = item.IsAlternativeCostCenter;
                        //        obj.IsAlternativeIOOrg = item.IsAlternativeIOOrg;
                        //        $scope.DefaultCostCenterIOByGroup[item.ExpenseTypeGroupID] = obj;
                        //    });
                        //}
                        //if ($scope.document.Additional.Travelers[0] != null && $scope.document.Additional.Travelers[0].PersonalBudgets != null) {
                        //    $.each($scope.document.Additional.Travelers[0].PersonalBudgets, function (i, item) {
                        //        var obj = angular.copy($scope.DefaultCostCenterIOObj);
                        //        obj.CostCenter = item.CostCenter;
                        //        obj.OrgUnitID = item.AlternativeIOOrg;
                        //        obj.IO = item.IO;
                        //        obj.IsAlternativeCostCenter = item.IsAlternativeCostCenter;
                        //        obj.IsAlternativeIOOrg = item.IsAlternativeIOOrg;
                        //        $scope.DefaultCostCenterIOByGroup[item.ExpenseTypeGroupID] = obj;
                        //    });
                        //}
                        //console.log('$$$$$$$$$$$$$$ default costcenter, io by ExpenseTypeGroupID $$$$$$$$$$$$$$.', $scope.DefaultCostCenterIOByGroup);
                        
                        if (angular.isDefined($scope.data) && angular.isDefined($scope.data.TravelSchedulePlaces) && $scope.data.TravelSchedulePlaces != null && $scope.data.TravelSchedulePlaces.length > 0) {
                            $scope.limitMinDate = formatDateString($scope.data.TravelSchedulePlaces[0].BeginDate);
                            $scope.limitMaxDate = formatDateString($scope.data.TravelSchedulePlaces[$scope.data.TravelSchedulePlaces.length - 1].EndDate);
                            $scope.savedLimitMinDate = formatDateString($scope.data.TravelSchedulePlaces[0].BeginDate);
                            $scope.savedLimitMaxDate = formatDateString($scope.data.TravelSchedulePlaces[$scope.data.TravelSchedulePlaces.length - 1].EndDate);
                        } else {
                            $scope.limitMinDate = formatDateString($scope.document.Additional.oTravelGroupRequest.BeginDate);
                            $scope.limitMaxDate = formatDateString($scope.document.Additional.oTravelGroupRequest.EndDate);
                            $scope.savedLimitMinDate = formatDateString($scope.document.Additional.oTravelGroupRequest.BeginDate);
                            $scope.savedLimitMaxDate = formatDateString($scope.document.Additional.oTravelGroupRequest.EndDate);
                        }

                        console.log('######################################### 1.1.', $scope.settings.ExpenseTypeGroupInfo.groupTagName);
                        console.log('######################################### 1.2.', $scope.settings.ExpenseTypeGroupInfo.tagNameNoReceipt);
                        $scope.GetExpenseTypeGroupByTag($scope.settings.ExpenseTypeGroupInfo.groupTagName, $scope.settings.ExpenseTypeGroupInfo.tagNameNoReceipt);

                        $scope.$watchGroup(['document.Additional.CostCenter', 'document.Additional.AlternativeIOOrg', 'document.Additional.IO'], function (newValues, oldValues, scp) {
                            scp.DefaultCostCenterIO.CostCenter = scp.document.Additional.CostCenter;
                            scp.DefaultCostCenterIO.OrgUnitID = scp.document.Additional.AlternativeIOOrg;
                            scp.DefaultCostCenterIO.IO = scp.document.Additional.IO;
                            scp.DefaultCostCenterIO.IsAlternativeCostCenter = scp.document.Additional.IsAlternativeCostCenter;
                            scp.DefaultCostCenterIO.IsAlternativeIOOrg = scp.document.Additional.IsAlternativeIOOrg;
                            console.log('DefaultCostCenterIO.', scp.DefaultCostCenterIO);
                        });

                        $scope.$watch('document.Additional.TravelSchedulePlaces', function (newValue, oldValue) {
                            if (angular.isDefined(newValue) && newValue != null && newValue.length > 0) {
                                $scope.limitMinDate = formatDateString(newValue[0].BeginDate);
                                $scope.limitMaxDate = formatDateString(newValue[newValue.length - 1].EndDate);
                                $scope.savedLimitMinDate = formatDateString(newValue[0].BeginDate);
                                $scope.savedLimitMaxDate = formatDateString(newValue[newValue.length - 1].EndDate);
                            } else {
                                $scope.limitMinDate = formatDateString($scope.document.Additional.oTravelGroupRequest.BeginDate);
                                $scope.limitMaxDate = formatDateString($scope.document.Additional.oTravelGroupRequest.EndDate);
                                $scope.savedLimitMinDate = formatDateString($scope.document.Additional.oTravelGroupRequest.BeginDate);
                                $scope.savedLimitMaxDate = formatDateString($scope.document.Additional.oTravelGroupRequest.EndDate);
                            }
                        }, true);

                        $scope.$watch('settings.ExpenseTypeGroupInfo.tagNameNoReceipt', function (newObj, oldObj) {
                            if (newObj != oldObj) {
                                console.log('######################################### 2.1.', $scope.settings.ExpenseTypeGroupInfo.groupTagName);
                                console.log('######################################### 2.2.', $scope.settings.ExpenseTypeGroupInfo.tagNameNoReceipt);
                                $scope.GetExpenseTypeGroupByTag($scope.settings.ExpenseTypeGroupInfo.groupTagName, $scope.settings.ExpenseTypeGroupInfo.tagNameNoReceipt);
                            }
                        });

                        /* default costcenter, io by ExpenseTypeGroupID */

                        $scope.$watch('document.Additional.GroupBudgets', function (newValue, oldValue) {
                            if (newValue != null) {
                                $.each(newValue, function (i, item) {
                                    var obj = angular.copy($scope.DefaultCostCenterIOObj);
                                    obj.CostCenter = item.CostCenter;
                                    obj.OrgUnitID = item.AlternativeIOOrg;
                                    obj.IO = item.IO;
                                    obj.IsAlternativeCostCenter = item.IsAlternativeCostCenter;
                                    obj.IsAlternativeIOOrg = item.IsAlternativeIOOrg;
                                    $scope.DefaultCostCenterIOByGroup[item.ExpenseTypeGroupID] = obj;
                                });
                            }
                            console.log('$$$$$$$$$$$$$$ default costcenter, io by ExpenseTypeGroupID 1 $$$$$$$$$$$$$$.', angular.copy($scope.DefaultCostCenterIOByGroup));
                        });

                        $scope.$watch('document.Additional.Travelers[0].PersonalBudgets', function (newValue, oldValue) {
                            if (newValue != null) {
                                $.each(newValue, function (i, item) {
                                    var obj = angular.copy($scope.DefaultCostCenterIOObj);
                                    obj.CostCenter = item.CostCenter;
                                    obj.OrgUnitID = item.AlternativeIOOrg;
                                    obj.IO = item.IO;
                                    obj.IsAlternativeCostCenter = item.IsAlternativeCostCenter;
                                    obj.IsAlternativeIOOrg = item.IsAlternativeIOOrg;
                                    $scope.DefaultCostCenterIOByGroup[item.ExpenseTypeGroupID] = obj;
                                });
                            }
                            console.log('$$$$$$$$$$$$$$ default costcenter, io by ExpenseTypeGroupID 2 $$$$$$$$$$$$$$.', angular.copy($scope.DefaultCostCenterIOByGroup));
                        });

                        /* !default costcenter, io by ExpenseTypeGroupID */

                    }

                    function formatDateString(value) {
                        if (value.length == 10) {
                            value += 'T00:00:00';
                        } else if (value.length >= 33) {
                            value = value.substr(0, 22);
                        }
                        return value + '+07:00';
                    }

                    $scope.wizardFormFlag = {
                        isAddGEReceipt: false,
                        isAddGEReceiptItem: false,
                        isAddGEReceiptItemDetail: false
                    };

                    $scope.GE = {
                        receipt: null,
                        receiptEdit: false,
                        receiptEditObj: null,

                        receiptItem: null,
                        receiptItemEdit: false,
                        receiptItemEditObj: null,
                        receiptItemFormData: {
                            Amount: 0,
                            AmountInRight: 9999999999,
                            oTransportationType: null

                            , tmpEmployeeName: ''
                            , tmpOrgUnitName: ''
                            , tmpCompanyName: ''
                        },

                        receiptItemDetail: null,
                        receiptItemDetailEdit: false,
                        receiptItemDetailEditObj: null,
                        receiptItemDetailFormData: {
                            ProjectCode: '',
                            FlatLocationFilter: { Key: '!' },
                        }
                    };

                    $scope.panelName = '';

                    $scope.tempFormData = {
                        invoiceDate: null
                    };
                    $scope.tempData = {
                        chkWhtTax: false,
                        oWHTTypeSubsidise: null,
                        sumWHTAmount: 0
                    };
                    
                    //$scope.projectForLookup = null;
                    //$scope.getProjectForLookup = function () {
                    //    var URL = CONFIG.SERVER + 'HRTR/GetProjectForLookup';
                    //    var oRequestParameter = { InputParameter: {}, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    //    $http({
                    //        method: 'POST',
                    //        url: URL,
                    //        data: oRequestParameter
                    //    }).then(function successCallback(response) {
                    //        $scope.projectForLookup = response.data;
                    //    }, function errorCallback(response) {
                    //        $scope.projectForLookup = null;
                    //        console.log('error getProjectForLookup.', response);
                    //    });
                    //};
                    //$scope.getProjectForLookup();

                   
                    $scope.whtSettings = {
                        ENABLEWHT: false,
                        ENABLEROW: false
                    };

                    $scope.travelCurrencyList = [];

                    //var getAllExchageType = function () {
                    //    var URL = CONFIG.SERVER + 'HRTR/GetExchangeTypeAll';
                    //    var oRequestParameter = { InputParameter: {}, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };

                    //    $http({
                    //        method: 'POST',
                    //        url: URL,
                    //        data: oRequestParameter
                    //    }).then(function successCallback(response) {
                    //        $scope.exchangeType = response.data;
                    //        $scope.ExchangeTypeReady = true;
                    //    },
                    //    function errorCallback(response) {
                    //        $scope.exchangeType = [];
                    //        console.log('error GetExchangeTypeAll', response);
                    //    });
                    //};
                    //getAllExchageType();

                    $scope.findExchangeType = function (exchangeTypeID) {
                        if (angular.isDefined($scope.exchangeType)) {
                            for (var i = 0; i < $scope.exchangeType.length; i++) {
                                if ($scope.exchangeType[i].ExchangeTypeID == exchangeTypeID) {
                                    if (employeeData.Language == 'TH') {
                                        return '(' + $scope.exchangeType[i].DescriptionTH + ')';
                                    } else {
                                        return '(' + $scope.exchangeType[i].DescriptionEN + ')';
                                    }

                                }
                            }
                        }
                        return '';
                    };

                    $scope.findDropdownExchangeType = function (exchangeTypeID, exchangeRate) {
                        if (angular.isDefined($scope.exchangeType) && exchangeRate > 1) {
                            for (var i = 0; i < $scope.exchangeType.length; i++) {
                                if ($scope.exchangeType[i].ExchangeTypeID == exchangeTypeID) {
                                    if (employeeData.Language == 'TH') {
                                        return '(' + exchangeRate + ' : ' + $scope.exchangeType[i].DescriptionTH + ')';
                                    } else {
                                        return '(' + exchangeRate + ' : ' + $scope.exchangeType[i].DescriptionEN + ')';
                                    }

                                }
                            }
                        } else {
                            return '(' + exchangeRate + ')';
                        }
                    };



                    /* --- !Variable --- */
                    
                    $scope.MathRounding = function (amount) {
                        var value = Math.round(amount * 100) / 100;
                        value = Number(value.toFixed(2));
                        return value;
                    };

                    $scope.multiplyExchangeRate = function (originalAmount, exchangeRate, fromCurrencyRatio, toCurrencyRatio) {
                     
                        var amount = (originalAmount * exchangeRate * toCurrencyRatio) / fromCurrencyRatio;
                        amount = $scope.MathRounding(amount);
                        return amount;
                        
                    };

                    

                    var getPanelWHT = function () {
                        var sMapType = ($scope.pageClassName == 'GeneralExpenseReport' || $scope.pageClassName == 'TravelReport')  ? 'TE_ENABLEWHT' : 'EX_ENABLEWHT' ;
   
                            var URL = CONFIG.SERVER + 'HRTR/GetPanelWHT';
                            var oRequestParameter = { InputParameter: { 'MAPPINGTYPE': sMapType }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                            $http({
                                method: 'POST',
                                url: URL,
                                data: oRequestParameter
                            }).then(function successCallback(response) {
                                $scope.whtSettings.ENABLEWHT = response.data;

                                console.log('GetPanelWHT.', response.data);
                            }, function errorCallback(response) {
                                console.log('error GetPanelWHT.', response);
                            });                     
                    };
                    getPanelWHT();

                    var getWHTRow = function () {
                        $scope.whtSettings.ENABLEROW = ($scope.pageClassName == 'GeneralExpenseReport' || $scope.pageClassName == 'TravelReport') ? false : true;
                    };
                    getWHTRow();


                    $scope.getExpenseItems_WithoutReceipt = function () {
                        var oList = [];
                        if (angular.isDefined($scope.ExpenseReport.ExpenseReportReceiptList) && $scope.ExpenseReport.ExpenseReportReceiptList != null) {

                            var ExpenseReportReceiptList = $scope.ExpenseReport.ExpenseReportReceiptList;
                            var runNo = 1;
                            for (var i = 0; i < ExpenseReportReceiptList.length; i++) {
                                var receipt = angular.copy(ExpenseReportReceiptList[i]);
                                if (!receipt.IsReceipt && receipt.IsVisible) {
                                    var countReceiptItem = receipt.ExpenseReportReceiptItemList.length;
                                    receipt.IsFirstRow = false;
                                    var sumAllChild = 0;
                                    for (var j = 0; j < countReceiptItem; j++) {
                                        var receiptItem = receipt.ExpenseReportReceiptItemList[j];
                                        sumAllChild += receiptItem.ExpenseReportReceiptItemDetailList.length;
                                    }
                                    receipt.ItemCount = sumAllChild;
                                    receipt.ExpenseReportReceiptItemList = null;
                                    receipt.ExpenseReportReceiptFileList = null;
                                    receipt.orderID = runNo++;

                                    for (var j = 0; j < countReceiptItem; j++) {
                                        var receiptItem = ExpenseReportReceiptList[i].ExpenseReportReceiptItemList[j];
                                        var countReceiptItemDetail = receiptItem.ExpenseReportReceiptItemDetailList.length;
                                        receipt.item = {
                                            IsFirstRow: false,
                                            ItemCount: countReceiptItemDetail,
                                            ItemID: receiptItem.ItemID,
                                            ExpenseTypeName: receiptItem.ExpenseTypeName
                                        };

                                        for (var k = 0; k < countReceiptItemDetail; k++) {
                                            var tempReceipt = angular.copy(receipt);
                                            var receiptItemDetail = receiptItem.ExpenseReportReceiptItemDetailList[k];
                                            if (j == 0 && k == 0) {
                                                tempReceipt.IsFirstRow = true;
                                            }
                                            if (k == 0) {
                                                tempReceipt.item.IsFirstRow = true;
                                            }
                                            tempReceipt.itemDetail = {
                                                CostCenter: receiptItemDetail.CostCenter,
                                                IO: receiptItemDetail.IO,
                                                LocalAmount: receiptItemDetail.LocalAmount,
                                                ItemID: (k + 1)
                                            };

                                            tempReceipt.realItem = ExpenseReportReceiptList[i];
                                            oList.push(tempReceipt);
                                        }
                                    }
                                }
                            }

                        }
                        return oList;
                    };
                    $scope.expenseItems_WithoutReceipt = $scope.getExpenseItems_WithoutReceipt();

                

                    /* --- Method --- */

                    $scope.getFileAttach = function (attachment) {
                        /* direct */
                        if (angular.isDefined(attachment) && attachment != '') {
                            var path = CONFIG.SERVER + 'Client/files/' + attachment.FilePath + '/' + attachment.FileName;
                            $window.open(path);
                        }
                        /* !direct */
                    };








                    $scope.getIOByOrganization = function (Org, isSetDefaultIO) {
                        if (angular.isUndefined(isSetDefaultIO)) {
                            isSetDefaultIO = false;
                        }
                        var URL = CONFIG.SERVER + 'HRTR/GetInternalOrderByOrgUnit/';
                        var oRequestParameter = { InputParameter: { "OrgUnit": Org }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            $scope.objIO = response.data;
                            if ($scope.objIO != null && $scope.objIO.length > 0 && isSetDefaultIO) {
                                // set default
                                if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                                    // none, average
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].IO = $scope.objIO[0].OrderID;
                                } else {
                                    // exactly
                                    $scope.GE.receiptItemDetail.IO = $scope.objIO[0].OrderID;
                                }
                            }
                            console.log('objIO.', $scope.objIO);
                        }, function errorCallback(response) {
                            $scope.objIO = [];
                            console.log('error objIO.', response);
                        });
                    };

                    $scope.changeIsAlternativeCostCenter = function (isAlternativeCostCenter) {
                        if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                            // none, average
                            if (!isAlternativeCostCenter) {
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].CostCenter = $scope.masterData.objCostcenterDistribution[0].CostCenterCode;
                            }
                            //if (!$scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].IsAlternativeIOOrg) {
                            //    $scope.getIOByCostCenter($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].CostCenter, true);
                            //}
                        } else {
                            // exactly
                            if (!isAlternativeCostCenter) {
                                $scope.GE.receiptItemDetail.CostCenter = $scope.masterData.objCostcenterDistribution[0].CostCenterCode;
                            }
                            //if (!$scope.GE.receiptItemDetail.IsAlternativeIOOrg) {
                            //    $scope.getIOByCostCenter($scope.GE.receiptItemDetail.CostCenter, true);
                            //}
                        }
                    };

                    $scope.changeIsAlternativeIOOrg = function (isAlternativeIOOrg) {
                        // change default
                        if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                            // none, average
                            if (!isAlternativeIOOrg) {
                                if ($scope.document.Requestor.OrgUnit != null) {
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].AlternativeIOOrg = $scope.document.Requestor.OrgUnit;
                                }
                                //$scope.getIOByCostCenter($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].CostCenter, true);
                                $scope.getIOByOrganization($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].AlternativeIOOrg, true);
                            } else {
                                $scope.getIOByOrganization($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].AlternativeIOOrg, true);
                            }
                        } else {
                            // exactly
                            if (!isAlternativeIOOrg) {
                                if ($scope.document.Requestor.OrgUnit != null) {
                                    $scope.GE.receiptItemDetail.AlternativeIOOrg = $scope.document.Requestor.OrgUnit;
                                }
                                //$scope.getIOByCostCenter($scope.GE.receiptItemDetail.CostCenter, true);
                                $scope.getIOByOrganization($scope.GE.receiptItemDetail.AlternativeIOOrg, true);
                            } else {
                                $scope.getIOByOrganization($scope.GE.receiptItemDetail.AlternativeIOOrg, true);
                            }
                        }
                    };

                    $scope.onSelectedCostCenter = function (selectedCostCenter) {
                        //if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                        //    // none, average
                        //    if (!$scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].IsAlternativeIOOrg) {
                        //        $scope.getIOByCostCenter(selectedCostCenter.CostCenterCode, true);
                        //    }
                        //} else {
                        //    // exactly
                        //    if (!$scope.GE.receiptItemDetail.IsAlternativeIOOrg) {
                        //        $scope.getIOByCostCenter(selectedCostCenter.CostCenterCode, true);
                        //    }
                        //}
                    };

                    $scope.onSelectedOrganization = function (selectedOrganization) {
                        //if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                        //    // none, average
                        //    if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].IsAlternativeIOOrg) {
                        //        $scope.getIOByOrganization(selectedOrganization.ObjectID, true);
                        //    }
                        //} else {
                        //    // exactly
                        //    if ($scope.GE.receiptItemDetail.IsAlternativeIOOrg) {
                        //        $scope.getIOByOrganization(selectedOrganization.ObjectID, true);
                        //    }
                        //}

                        $scope.getIOByOrganization(selectedOrganization.ObjectID, true);
                    };

                    /* !CostCentet, Organization, IO dropdown */


                    /* Guest */

                    $scope.querySearchGuest = function (query) {
                        var results = query ? $scope.employeeDataList.filter(createGuestFilterFor(query)).filter(checkDuplicateGuest($scope.GE.receiptItem.ExpenseReportReceiptItemGuestList)) : $scope.employeeDataList.filter(checkDuplicateGuest($scope.GE.receiptItem.ExpenseReportReceiptItemGuestList));
                        return results;
                    };

                    function checkDuplicateGuest(arr) {
                        return function (item) {
                            return !(arr.duplicateProp('EmployeeID', item.EmployeeID));
                        };
                    }

                    function createGuestFilterFor(query) {
                        var lowercaseQuery = angular.lowercase(query);
                        return function (item) {
                            var source = angular.lowercase(item.EmployeeID + ' : ' + item.EmployeeName);
                            return (source.indexOf(lowercaseQuery) >= 0);
                        };
                    }

                    $scope.guest = {
                        newGuest: null
                    };

                    var clearNewGuest = function () {
                        $scope.guest.newGuest = null;
                    };

                    var createGuestObjFromEmployee = function (employee) {
                        /*public string RequestNo { get; set; }
                        public int ReceiptID { get; set; }
                        public int ItemID { get; set; }
                        public int GuestID {get;set;}
                        public bool IsEmployee {get;set;}
                        public string EmployeeID {get;set;}
                        public string Name {get;set;}
                        public string OrgUnit {get;set;}
                        public string CompanyCode {get;set;}*/

                        if (angular.isUndefined(employee) || employee == null) {
                            return {
                                RequestNo: '',
                                ReceiptID: $scope.GE.receiptItem.ReceiptID,
                                ItemID: $scope.GE.receiptItem.ItemID,
                                GuestID: 0,
                                IsEmployee: false,
                                EmployeeID: '',
                                EmployeeName: '',
                                OrgUnit: '',
                                OrgUnitName: '',
                                Position: '',
                                PositionName: '',
                                CompanyCode: '',
                                CompanyName: ''
                            };
                        }

                        return {
                            RequestNo: '',
                            ReceiptID: $scope.GE.receiptItem.ReceiptID,
                            ItemID: $scope.GE.receiptItem.ItemID,
                            GuestID: 0,
                            IsEmployee: true,
                            EmployeeID: employee.EmployeeID,
                            EmployeeName: employee.EmployeeName,
                            OrgUnit: employee.OrgUnit,
                            OrgUnitName: employee.OrgUnitName,
                            Position: employee.Position,
                            PositionName: employee.PositionName,
                            CompanyCode: employee.CompanyCode,
                            CompanyName: employee.CompanyName
                        };
                    };

                 

                    $scope.receiptItem_AddGuestEmployee = function () {
                        if ($scope.guest.newGuest != null) {
                            var isExist = false;
                            $.each($scope.GE.receiptItem.ExpenseReportReceiptItemGuestList, function (i, guest) {
                                if (guest.IsEmployee && guest.EmployeeID == $scope.guest.newGuest.EmployeeID) {
                                    isExist = true;
                                    return false;
                                }
                            });
                            if (isExist) {
                                $scope.p_message('Duplicate employee.','w');
                            } else {
                                console.log($scope.guest.newGuest);
                                $scope.GE.receiptItem.ExpenseReportReceiptItemGuestList.push(createGuestObjFromEmployee($scope.guest.newGuest));
                                clearNewGuest();
                            }
                        } else {
                            $scope.p_message($scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['EMPLOYEEID'],'w');
                        }
                    };

                    $scope.receiptItem_AddGuestOther = function () {

                        if ($scope.GE.receiptItemFormData.tmpEmployeeName == '' || $scope.GE.receiptItemFormData.tmpOrgUnitName == '' || $scope.GE.receiptItemFormData.tmpCompanyName == '') {
                            $scope.p_message($scope.Text['EXPENSE']['GUEST_OTHER_EMPTY_NAME'], 'w');
                            return false;
                        }

                        $scope.GE.receiptItem.ExpenseReportReceiptItemGuestList.push(createGuestObjFromEmployee());

                        var iRow = $scope.GE.receiptItem.ExpenseReportReceiptItemGuestList.length;
                        $scope.GE.receiptItem.ExpenseReportReceiptItemGuestList[iRow - 1].EmployeeName = $scope.GE.receiptItemFormData.tmpEmployeeName;
                        $scope.GE.receiptItem.ExpenseReportReceiptItemGuestList[iRow - 1].OrgUnitName = $scope.GE.receiptItemFormData.tmpOrgUnitName;
                        $scope.GE.receiptItem.ExpenseReportReceiptItemGuestList[iRow - 1].CompanyName = $scope.GE.receiptItemFormData.tmpCompanyName;

                        $scope.GE.receiptItemFormData.tmpEmployeeName = '';
                        $scope.GE.receiptItemFormData.tmpOrgUnitName = '';
                        $scope.GE.receiptItemFormData.tmpCompanyName = '';
                    };

                    $scope.receiptItem_DeleteGuestEmployee = function (index) {
                        if (confirm($scope.Text['SYSTEM']['CONFIRM_DELETE'])) {
                            var deleteIndex = 0;
                            $.each($scope.GE.receiptItem.ExpenseReportReceiptItemGuestList, function (i, guest) {
                                if (guest.IsEmployee) {
                                    if (deleteIndex == index) {
                                        deleteIndex = i;
                                        return false;
                                    }
                                    deleteIndex++;
                                }
                            });
                            $scope.GE.receiptItem.ExpenseReportReceiptItemGuestList.splice(deleteIndex, 1);
                        }
                    };

                    $scope.receiptItem_DeleteGuestOther = function (index) {
                        if (confirm($scope.Text['SYSTEM']['CONFIRM_DELETE'])) {
                            var deleteIndex = 0;
                            $.each($scope.GE.receiptItem.ExpenseReportReceiptItemGuestList, function (i, guest) {
                                if (!guest.IsEmployee) {
                                    if (deleteIndex == index) {
                                        deleteIndex = i;
                                        return false;
                                    }
                                    deleteIndex++;
                                }
                            });
                            $scope.GE.receiptItem.ExpenseReportReceiptItemGuestList.splice(deleteIndex, 1);
                        }
                    };

                    /* !Guest */








                    $scope.changeVATBaseAmount = function () {
                        var VATBaseAmount = Number($scope.GE.receipt.VATBaseAmount);
                        VATBaseAmount = isNaN(VATBaseAmount) ? 0 : $scope.MathRounding(VATBaseAmount);
                        var VatPercent = Number($scope.GE.receipt.VATPercent);
                        VatPercent = isNaN(VatPercent) ? 0 : VatPercent;
                        $scope.GE.receipt.VATAmount = $scope.MathRounding(VATBaseAmount * VatPercent / 100);
                    };

                    $scope.onBlurOriginalAmount = function (objDetail) {
                        var valueAmount = Number(objDetail.OriginalAmount);
                        valueAmount = isNaN(valueAmount) ? 0 : $scope.MathRounding(valueAmount);
                        objDetail.OriginalAmount = valueAmount;
                    };

                    $scope.onBlurFormAmount = function (objFormData) {
                        var valueAmount = Number(objFormData.Amount);
                        valueAmount = isNaN(valueAmount) ? 0 : $scope.MathRounding(valueAmount);
                        objFormData.Amount = valueAmount;
                    };

                    $scope.sumWHT = function (WHTAmount1LC, WHTAmount2LC) {
                        return WHTAmount1LC + WHTAmount2LC;
                    };

                    $scope.sumLocalNoVATTotalAmount = function (list, isReceipt) {
                        var total = 0;
                        angular.forEach(list, function (item) {
                            if (item) {
                                if (item.IsReceipt == isReceipt && item.IsVisible) {
                                    var amount = Number(item.LocalNoVATTotalAmount);
                                    total += isNaN(amount) ? 0 : amount;
                                }
                            }
                        });
                        return total;
                    };

                    $scope.sumVATAmount = function (list, isReceipt) {
                        var total = 0;
                        angular.forEach(list, function (item) {
                            if (item.IsReceipt == isReceipt && item.IsVisible) {
                                var amount = Number(item.VATAmount);
                                total += isNaN(amount) ? 0 : amount;
                            }
                        });
                        return total;
                    };

                    $scope.allSum = function () {
                        var sum = $scope.sumLocalNoVATTotalAmount($scope.ExpenseReport.ExpenseReportReceiptList, false);
                        if (angular.isDefined($scope.childSumData)) {
                            $scope.childSumData.ExpenseWithoutReceipt = sum;
                        }
                        return sum;
                    };

                    //$scope.getCarTypes = function () {
                    //    var URL = CONFIG.SERVER + 'HRTR/GetCarTypeAll';
                    //    var oRequestParameter = { InputParameter: {}, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    //    $http({
                    //        method: 'POST',
                    //        url: URL,
                    //        data: oRequestParameter
                    //    }).then(function successCallback(response) {
                    //        $scope.carTypes = response.data;
                    //        console.log('GetCarTypeAll.', response.data);
                    //    }, function errorCallback(response) {
                    //        console.log('error GetCarTypeAll.', response);
                    //    });
                    //};
                    //$scope.getCarTypes();

                    //$scope.getFlatRateLocations = function () {
                    //    var URL = CONFIG.SERVER + 'HRTR/GetFlatRateLocationAll';
                    //    var oRequestParameter = { InputParameter: {}, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    //    $http({
                    //        method: 'POST',
                    //        url: URL,
                    //        data: oRequestParameter
                    //    }).then(function successCallback(response) {
                    //        $scope.flatRateLocations = response.data;
                    //        console.log('GetFlatRateLocationAll.', response.data);
                    //    }, function errorCallback(response) {
                    //        console.log('error GetFlatRateLocationAll.', response);
                    //    });
                    //};
                    //$scope.getFlatRateLocations();

                    var GetFlatRateByLocation = function (SourceLocationID, DestinationLocationID) {
                        var URL = CONFIG.SERVER + 'HRTR/GetFlatRateByLocation';
                        var oRequestParameter = { InputParameter: { 'SourceLocationID': SourceLocationID, 'DestinationLocationID': DestinationLocationID }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                        var promise = $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        });
                        return promise;
                    };

                    $scope.getMileageRate = function (oTranType) {
                        var URL = CONFIG.SERVER + 'HRTR/GetMileageRate';
                        var oRequestParameter = { InputParameter: { 'TranTypeCode': oTranType }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            var amount = 0;
                            if (angular.isDefined(response.data[0])) {
                                amount = Number(response.data[0].FuelRate);
                            }
                            $scope.fuelRate = isNaN(amount) ? 0 : amount;
                            if (angular.isDefined(response.data[0])) {
                                amount = Number(response.data[0].NoCalIncomeRate);
                            }
                            $scope.mileageNoCalIncomeRate = isNaN(amount) ? 0 : amount;

                            $scope.onChangeDistance();

                            console.log('GetMileageRate.', response.data);
                        }, function errorCallback(response) {
                            console.log('error GetMileageRate.', response);
                        });
                    };
                    if ($scope.oTranType != null && $scope.oTranType.length > 0) {
                        $scope.getMileageRate($scope.oTranType[0].TransportationTypeCode);
                    }

                    //Nun Add 25/08/2016
                    $scope.getExpenseTypeAmountInRight = function (expID) {
                        var URL = CONFIG.SERVER + 'HRTR/GetExpenseTypeAmountInRight';
                        var oRequestParameter = { InputParameter: { 'ExpenseTypeID': expID }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {

                            var amount = 9999999999;
                            if (response.data && response.data.length > 0) {
                                amount = Number(response.data[0].AmountInRight);
                            }
                            $scope.amountInRight = isNaN(amount) ? 9999999999 : amount;

                            console.log('GetExpenseTypeAmountInRight.', response.data);
                        }, function errorCallback(response) {
                            console.log('error GetExpenseTypeAmountInRight.', response);
                        });
                    };
                    //

                    $scope.$watch('GE.receipt.ExpenseReportReceiptItemList', function (newObj, oldObj) {
                        // update VatType
                        var listExpenseType = '', listCarType = '';
                        if (angular.isDefined(newObj) && newObj != null) {
                            for (var i = 0; i < newObj.length; i++) {
                                if (newObj[i].ExpenseTypeID != '') {
                                    listExpenseType += newObj[i].ExpenseTypeID + ',';
                                }
                                if (newObj[i].Str2 != '') {
                                    listCarType += newObj[i].Str2 + ',';
                                }
                            }
                        }
                        var URL = CONFIG.SERVER + 'HRTR/GetVATTypeByExpenseTypeCarType';
                        var oRequestParameter = { InputParameter: { 'listExpenseTypeID': listExpenseType, 'listCarTypeID': listCarType }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            $scope.setVatTypeDefault = response.data;
                            if ($scope.setVatTypeDefault != null && $scope.setVatTypeDefault.length > 0) {
                                $scope.ddlDefault.VATCode = $scope.setVatTypeDefault[0].VATCode;
                            }
                            console.log('GetVATTypeByExpenseTypeCarType.', response.data);
                        }, function errorCallback(response) {
                            console.log('error GetVATTypeByExpenseTypeCarType.', response);
                        });
                    }, true);

                    
                    

                    // sum receipt total amount

                    $scope.$watchGroup(['GE.receipt.LocalNoVATTotalAmount', 'GE.receipt.VATAmount', 'GE.receipt.WHTAmount1LC', 'GE.receipt.WHTAmount2LC'], function (newValues, oldValues, scope) {
                        if (angular.isDefined($scope.GE.receipt) && $scope.GE.receipt != null) {
                            //$scope.GE.receipt.LocalTotalAmount = $scope.GE.receipt.LocalNoVATTotalAmount + $scope.GE.receipt.VATAmount + $scope.GE.receipt.WHTAmount1LC + $scope.GE.receipt.WHTAmount2LC;
                            //Nun Add  
                            $scope.GE.receipt.LocalTotalAmount = $scope.GE.receipt.LocalNoVATTotalAmount + $scope.GE.receipt.VATAmount ;
                        }
                    });

                    // !sum receipt total amount

                    /* exchange rate */

                    $scope.changeReceiptCurrencyFromOutside = function (receipt, newExchange) {
                        console.log('newExchange.', newExchange);
                        receipt.ExchangeRate = (newExchange.ExchangeRate == null) ? 0 : Number(newExchange.ExchangeRate);
                        receipt.OriginalCurrencyCode = newExchange.FromCurrency;
                        receipt.ExchangeDate = newExchange.EffectiveDate;
                        receipt.ExchangeTypeID = newExchange.ExchangeTypeID;
                        receipt.FromCurrencyRatio = newExchange.RatioFromCurrencyUnit;
                        receipt.ToCurrencyRatio = newExchange.RatioToCurrencyUnit;
                        var sumLocalAmount = 0;
                        if (angular.isDefined(receipt.ExpenseReportReceiptItemList) && receipt.ExpenseReportReceiptItemList != null) {
                            for (var i = 0; i < receipt.ExpenseReportReceiptItemList.length; i++) {
                                var item = receipt.ExpenseReportReceiptItemList[i];
                                if (angular.isDefined(item.ExpenseReportReceiptItemDetailList) && item.ExpenseReportReceiptItemDetailList != null) {
                                    for (var k = 0; k < item.ExpenseReportReceiptItemDetailList.length; k++) {
                                        //item.ExpenseReportReceiptItemDetailList[k].LocalAmount = item.ExpenseReportReceiptItemDetailList[k].OriginalAmount * receipt.ExchangeRate;
                                        item.ExpenseReportReceiptItemDetailList[k].LocalAmount = $scope.multiplyExchangeRate(item.ExpenseReportReceiptItemDetailList[k].OriginalAmount, receipt.ExchangeRate, receipt.FromCurrencyRatio, receipt.ToCurrencyRatio);
                                        sumLocalAmount += $scope.MathRounding(item.ExpenseReportReceiptItemDetailList[k].LocalAmount);
                                        if (item.ExpenseReportReceiptItemDetailList[k].OriginalAmountInRight != 9999999999) {
                                            item.ExpenseReportReceiptItemDetailList[k].LocalAmountInRight = $scope.multiplyExchangeRate(item.ExpenseReportReceiptItemDetailList[k].OriginalAmountInRight, receipt.ExchangeRate, receipt.FromCurrencyRatio, receipt.ToCurrencyRatio);
                                        }
                                    }
                                }
                            }
                        }
                        receipt.LocalNoVATTotalAmount = sumLocalAmount;
                        receipt.VATBaseAmount = sumLocalAmount;
                        // calculate vat
                        var VATBaseAmount = Number(receipt.VATBaseAmount);
                        VATBaseAmount = isNaN(VATBaseAmount) ? 0 : $scope.MathRounding(VATBaseAmount);
                        var VatPercent = Number(receipt.VATPercent);
                        VatPercent = isNaN(VatPercent) ? 0 : VatPercent;
                        receipt.VATAmount = $scope.MathRounding(VATBaseAmount * VatPercent / 100);
                    };

                    $scope.updateExchangeRate = function (newObj, oldObj) {
                        // find changed
                        console.log('find changed exchangerate.');
                        var oldExchange = null;
                        var newExchange = null;
                        if (oldObj != null && newObj != null && oldObj.length == newObj.length && oldObj != newObj) {
                            // loop find changed
                            for (var i = 0; i < oldObj.length; i++) {
                                if (oldObj[i].EffectiveDate != newObj[i].EffectiveDate
                                    || oldObj[i].FromCurrency != newObj[i].FromCurrency
                                    || (oldObj[i].ExchangeRate != newObj[i].ExchangeRate)
                                    || oldObj[i].ExchangeTypeID != newObj[i].ExchangeTypeID
                                    || oldObj[i].RatioFromCurrencyUnit != newObj[i].RatioFromCurrencyUnit
                                    || oldObj[i].RatioToCurrencyUnit != newObj[i].RatioToCurrencyUnit) {
                                    
                                    oldExchange = oldObj[i];
                                    newExchange = newObj[i];
                                    break;
                                }
                            }
                        }

                        if (oldExchange != null) {
                            if (angular.isDefined($scope.ExpenseReport.ExpenseReportReceiptList) && $scope.ExpenseReport.ExpenseReportReceiptList != null) {
                                for (var i = 0; i < $scope.ExpenseReport.ExpenseReportReceiptList.length; i++) {
                                    var receipt = $scope.ExpenseReport.ExpenseReportReceiptList[i];

                                    //alert(receipt.ExchangeDate + ' & ' + oldExchange.EffectiveDate
                                    //+ '\r\n' +
                                    //receipt.OriginalCurrencyCode + ' & ' + oldExchange.FromCurrency
                                    //+ '\r\n' +
                                    //receipt.ExchangeTypeID + ' & ' + oldExchange.ExchangeTypeID
                                    //+ '\r\n' +
                                    //receipt.ExchangeRate + ' & ' + oldExchange.ExchangeRate
                                    //);
                                    
                                    if (!receipt.IsReceipt && receipt.IsVisible
                                        && receipt.ExchangeDate == oldExchange.EffectiveDate
                                        && receipt.OriginalCurrencyCode == oldExchange.FromCurrency
                                        && receipt.ExchangeTypeID == oldExchange.ExchangeTypeID
                                        && receipt.FromCurrencyRatio == oldExchange.RatioFromCurrencyUnit
                                        && receipt.ToCurrencyRatio == oldExchange.RatioToCurrencyUnit
                                        && (receipt.ExchangeRate == oldExchange.ExchangeRate || (oldExchange.ExchangeRate == null && receipt.ExchangeRate == 0))) {
                                        // found : update exchange rate 

                                        $scope.changeReceiptCurrencyFromOutside(receipt, newExchange);

                                        //receipt.LocalTotalAmount = receipt.LocalNoVATTotalAmount + receipt.VATAmount + receipt.WHTAmount1LC + receipt.WHTAmount2LC;
                                        //Nun Add
                                        receipt.LocalTotalAmount = receipt.LocalNoVATTotalAmount + receipt.VATAmount;
                                        
                                    }
                                }

                                /* fix case when new object $scope.document in root controller */
                                $scope.document.Additional.ExpenseReportReceiptList = $scope.ExpenseReport.ExpenseReportReceiptList;
                                /* !fix case when new object $scope.document in root controller */

                                $scope.expenseItems_WithoutReceipt = $scope.getExpenseItems_WithoutReceipt();
                            }
                        }
                        
                    };

                    $scope.$watch('masterData.travelCurrencyList', function (newObj, oldObj) {
                        $scope.travelCurrencyList = angular.copy(newObj);
                        if ($scope.travelCurrencyList == null) {
                            $scope.travelCurrencyList = [];
                        }
                        if ($scope.personalCurrency != null) {
                            $scope.travelCurrencyList.unshift(angular.copy($scope.personalCurrency));
                        }

                        $scope.updateExchangeRate(newObj, oldObj);

                        console.log('-------- Receipt Update Exchange Rate --------');
                    }, true);

                    /* !exchange rate */

                    /* --- !Method --- */

                    /* ====== wizard form ====== */

                    /* ====== receipt function ====== */

                    $scope.validateReceipt = function () {
                        var oValidate = {
                            isValid: true,
                            message: ''
                        };
                        if ($scope.GE.receipt.ReceiptTextID == null || $scope.GE.receipt.ReceiptTextID == '') {
                            oValidate.isValid = false;
                            oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['TAXINVOICE'];
                            return oValidate;
                        }

                        if (!$scope.GE.receipt.NolimitDate && (myDate > today)) {
                            oValidate.isValid = false;
                            oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['REQUIRED_RECEIPTDATELESSTHANTODAY'];
                            return oValidate;
                        }


                        if ($scope.GE.receipt.VendorCode == null || $scope.GE.receipt.VendorCode == '') {
                            oValidate.isValid = false;
                            oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['VENDORCODE'];
                            return oValidate;
                        }
                        if ($scope.GE.receipt.ExpenseReportReceiptItemList == null || $scope.GE.receipt.ExpenseReportReceiptItemList.length <= 0) {
                            oValidate.isValid = false;
                            oValidate.message = $scope.Text['SYSTEM']['PLEASE_ADD'] + $scope.Text['EXPENSE']['HEADSUBJECT_RECEIPT_ITEM'];
                            return oValidate;
                        }
                        // receipt file
                        if ($scope.GE.receipt.ExpenseReportReceiptFileList == null) {
                            oValidate.isValid = false;
                            oValidate.message = $scope.Text['SYSTEM']['PLEASE_ADD'] + $scope.Text['EXPENSE']['EXPENSE_RECEIPT_FILE'];
                            return oValidate;
                        } else {
                            if ($scope.GE.receipt.ExpenseReportReceiptFileList.length <= 0) {
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['PLEASE_ADD'] + $scope.Text['EXPENSE']['EXPENSE_RECEIPT_FILE'];
                                return oValidate;
                            } else {
                                var countFile = 0;
                                for (var i = 0; i < $scope.GE.receipt.ExpenseReportReceiptFileList.length; i++) {
                                    if (!$scope.GE.receipt.ExpenseReportReceiptFileList[i].IsDelete) {
                                        countFile++;
                                    }
                                }
                                if (countFile <= 0) {
                                    oValidate.isValid = false;
                                    oValidate.message = $scope.Text['SYSTEM']['PLEASE_ADD'] + $scope.Text['EXPENSE']['EXPENSE_RECEIPT_FILE'];
                                    return oValidate;
                                }

                            }
                        }
                        return oValidate;
                    };

                    $scope.AddReceiptFinish = function () {
                        // always validate pass
                        $scope.GoToSubMain_AddGEReceiptFinish();
                    };

                    $scope.setDirectiveFn = function (directiveFn) {
                        $scope.directiveFn = directiveFn;
                    };

                    $scope.setSelectedInvoiceDate = function (selectedDate) {
                        $scope.GE.receipt.ReceiptDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                        console.log('invoice date.', $scope.GE.receipt.ReceiptDate);

                    };
                    $scope.SetLimitDate = function (flgUnlimit, flagChangeReceiptDate) {
                        if (angular.isUndefined(flgUnlimit)) {
                            flgUnlimit = false;
                        }
                        if (angular.isUndefined(flagChangeReceiptDate)) {
                            flagChangeReceiptDate = true;
                        }
                        if (flgUnlimit) {
                            var unlimitDateScope_MINDATE = new Date($scope.savedLimitMinDate);
                            var unlimitDateScope_MAXDATE = new Date($scope.savedLimitMaxDate);
                            if ($scope.settings.ExpenseTypeGroupInfo.prefix == 'TR') {
                                unlimitDateScope_MINDATE.setDate(unlimitDateScope_MINDATE.getDate() + ($scope.receiptLimitDate.RP_LIMIT_MINDATE * -1));
                                unlimitDateScope_MAXDATE.setDate(unlimitDateScope_MAXDATE.getDate() + ($scope.receiptLimitDate.RP_LIMIT_MAXDATE));

                            } else {
                                unlimitDateScope_MINDATE.setDate(unlimitDateScope_MINDATE.getDate() + ($scope.receiptLimitDate.GE_LIMIT_MINDATE * -1));
                                unlimitDateScope_MAXDATE.setDate(unlimitDateScope_MAXDATE.getDate() + ($scope.receiptLimitDate.GE_LIMIT_MAXDATE));
                            }

                            $scope.limitMinDate = $filter('date')(unlimitDateScope_MINDATE, 'yyyy-MM-dd');
                            $scope.limitMaxDate = $filter('date')(unlimitDateScope_MAXDATE, 'yyyy-MM-dd');
                 
                        } else {
                            $scope.limitMinDate = angular.copy($scope.savedLimitMinDate);
                            $scope.limitMaxDate = angular.copy($scope.savedLimitMaxDate);

                            if (angular.isDefined($scope.GE.receipt) && $scope.GE.receipt != null && flagChangeReceiptDate) {
                                // testcomment
                                //$scope.directiveFn($scope.savedLimitMinDate);
                                $scope.GE.receipt.ReceiptDate = $scope.savedLimitMinDate;
                            }
                        }
                    };
                    //$scope.setSelectedExchangeRateDate = function (selectedDate) {
                    //    $scope.GE.receipt.ExchangeDate = $filter('date')(selectedDate, 'yyyy-MM-ddT00:00:00');
                    //    console.log('invoice date.', $scope.GE.receipt.ExchangeDate);

                    //};


                    /* ====== !receipt function ====== */

                    /* ====== receipt item function ====== */

                    $scope.calculateAmountReceiptItem = function () {
                        var sum = 0;
                        angular.forEach($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList, function (item) {
                            var amount = Number(item.OriginalAmount);
                            sum += isNaN(amount) ? 0 : amount;
                        });
                        $scope.GE.receiptItem.Amount = sum;
                    };

                    $scope.validateReceiptItem = function () {
                        var oValidate = {
                            isValid: true,
                            message: ''
                        };
                        if ($scope.GE.receipt.ReceiptDate == null || $scope.GE.receipt.ReceiptDate == '') {
                            oValidate.isValid = false;
                            oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['PAYMENT_DATE'];
                            return oValidate;
                        }
                        if ($scope.GE.receiptItem.ExpenseTypeGroupID == null || $scope.GE.receiptItem.ExpenseTypeGroupID < 1) {
                            oValidate.isValid = false;
                            oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['EXPENSECATEGORY'];
                            return oValidate;
                        }
                        if ($scope.GE.receiptItem.ExpenseTypeID == null || $scope.GE.receiptItem.ExpenseTypeID < 1) {
                            oValidate.isValid = false;
                            oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['EXPENSETYPE'];
                            return oValidate;
                        }
                        if ($scope.GE.receiptItem.Detail == null || $scope.GE.receiptItem.Detail == '') {
                            oValidate.isValid = false;
                            oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['EXPENSE_DETAIL'];
                            return oValidate;
                        }
                        if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                            // none, average


                            if ($scope.settings.ProjectCodeMode != $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                                if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].CostCenter == null || $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].CostCenter == '') {
                                    oValidate.isValid = false;
                                    oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['COST_CENTER'];
                                    return oValidate;
                                }
                                if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].IO == null || $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].IO == '') {
                                    oValidate.isValid = false;
                                    oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['IO'];
                                    return oValidate;
                                }
                            }


                            if ($scope.GE.receiptItemFormData.Amount == null || $scope.GE.receiptItemFormData.Amount.toString() == '') {
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['AMOUNT_NO_VAT'];
                                return oValidate;
                            }
                            if (!isFinite($scope.GE.receiptItemFormData.Amount.toString())) {
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['INVALID_NUMBER'];
                                return oValidate;
                            } else if ($scope.GE.receiptItemFormData.Amount <= 0) {
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['EXPENSE']['AMOUNT_NOT_ZERO'];
                                return oValidate;
                            }
                        } else {
                            // exactly
                            if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList == null || $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList.length < 1) {
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['PLEASE_ADD'] + $scope.Text['EXPENSE']['RECEIPT_ITEM_DETAIL'];
                                return oValidate;
                            }
                        }

                        /* Guest validation */
                        if ($scope.panelName == 'pnlEntertainment') {
                            var isValid = true;

                            $.each($scope.GE.receiptItem.ExpenseReportReceiptItemGuestList, function (i, guest) {
                                if (!guest.IsEmployee && ($.trim(guest.EmployeeName) == '' || $.trim(guest.OrgUnitName) == '' || $.trim(guest.CompanyName) == '')) {
                                    isValid = false;
                                    return false;
                                }
                            });
                            if (!isValid) {
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['EXPENSE']['GUEST_OTHER_EMPTY_NAME'];
                                return oValidate;
                            }

                            else if ($scope.pnlRequireUploadFile) {
                                var countGuest = 0;
                                var countFile = 0;
                                countGuest = $scope.GE.receiptItem.ExpenseReportReceiptItemGuestList.length;
                                countFile = $scope.GE.receiptItem.ExpenseReportReceiptItemGuestFileList.filter(function (x) { return !x.IsDelete }).length;
                                isValid = countGuest > 0 || countFile > 0;
                                if (!isValid) {
                                    oValidate.isValid = false;
                                    oValidate.message = $scope.Text['EXPENSE']['GUEST_ENTERTAINMENT_INCOMPLETE_' + $scope.GE.receiptItem.ExpenseTypeGroupID];
                                    return oValidate;
                                }
                            }
                        }
                            /* !Guest validation */
                        else if ($scope.panelName == 'pnlTransportation') {
                            var isValid = true;
                            if ($scope.pnlRequireUploadFile) {
                                var countFile = 0;
                                countFile = $scope.GE.receiptItem.ExpenseReportReceiptItemGuestFileList.filter(function (x) { return !x.IsDelete }).length;
                                isValid = countFile >= $scope.pnlNoOfFile;
                                if (!isValid) {
                                    oValidate.isValid = false;
                                    oValidate.message = $scope.Text['EXPENSE']['TRANSPORTAION_ATTACHMENT_REQUIRE'];
                                    return oValidate;
                                }
                            }
                        }

                        /* Panel validation */
                        else if ($scope.panelName == 'pnlAccommodation') {
                            // ค่าที่พัก
                            if ($scope.GE.receiptItem.Date1 == null || $scope.GE.receiptItem.Date1 == '') {
                                // วันที่เข้าพัก
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['CHECK_IN_DATE'];
                                return oValidate;
                            }
                            if ($scope.GE.receiptItem.Date2 == null || $scope.GE.receiptItem.Date2 == '') {
                                // วันที่ออก
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['CHECK_OUT_DATE'];
                                return oValidate;
                            }
                            if (new Date($scope.GE.receiptItem.Date1.substr(0, 10)) >= new Date($scope.GE.receiptItem.Date2.substr(0, 10))) {
                                // วันที่
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['EXPENSE']['CHECK_IN_CHECK_OUT_DATE_INVALID'];
                                return oValidate;
                            }
                            if ($scope.GE.receiptItem.Str1 == null || $scope.GE.receiptItem.Str1 == '') {
                                // ชื่อโรงแรม
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['HOTEL_NAME'];
                                return oValidate;
                            }


                            //Nun Add 26082016
                        } else if ($scope.panelName == 'pnlVehicleRent') {
                            // ค่ายานพาหนะ
                            if ($scope.GE.receiptItem.Date1 == null || $scope.GE.receiptItem.Date1 == '') {
                                // วันที่เริ่มต้น
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['START_DATE'];
                                return oValidate;
                            }
                            if ($scope.GE.receiptItem.Date2 == null || $scope.GE.receiptItem.Date2 == '') {
                                // วันที่สิ้นสุด
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['END_DATE'];
                                return oValidate;
                            }
                            if (new Date($scope.GE.receiptItem.Date1.substr(0, 10)) > new Date($scope.GE.receiptItem.Date2.substr(0, 10))) {
                                // วันที่
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['EXPENSE']['START_END_DATE_INVALID'];
                                return oValidate;
                            }
                            if ($scope.GE.receiptItem.Date1 == null || $scope.GE.receiptItem.Date1 == '') {
                                // วันที่เริ่มต้น
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['START_DATE'];
                                return oValidate;
                            }


                        } else if ($scope.panelName == 'pnlVehicleRentAndCarTypeLicense') {
                            // ค่ายานพาหนะ
                            if ($scope.GE.receiptItem.Date1 == null || $scope.GE.receiptItem.Date1 == '') {
                                // วันที่เริ่มต้น
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['START_DATE'];
                                return oValidate;
                            }
                            if ($scope.GE.receiptItem.Date2 == null || $scope.GE.receiptItem.Date2 == '') {
                                // วันที่สิ้นสุด
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['END_DATE'];
                                return oValidate;
                            }
                            if (new Date($scope.GE.receiptItem.Date1.substr(0, 10)) > new Date($scope.GE.receiptItem.Date2.substr(0, 10))) {
                                // วันที่
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['EXPENSE']['START_END_DATE_INVALID'];
                                return oValidate;
                            }
                            if ($scope.GE.receiptItem.Date1 == null || $scope.GE.receiptItem.Date1 == '') {
                                // วันที่เริ่มต้น
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['START_DATE'];
                                return oValidate;
                            }
                            if ($scope.GE.receiptItem.Str2 == null || $scope.GE.receiptItem.Str2 == '') {
                                // Car Type
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['CAR_TYPE'];
                                return oValidate;
                            }
                            if ($scope.GE.receiptItem.Str1 == null || $scope.GE.receiptItem.Str1 == '') {
                                // LICENSE_PLATE
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['LICENSE_PLATE'];
                                return oValidate;
                            }


                        } else if ($scope.panelName == 'pnlTransRegistration') {
                            // ค่าน้ำมันแบบมีใบเสร็จ
                            if ($scope.GE.receiptItem.Bit1 == null || $scope.GE.receiptItem.Bit1 === '' || $scope.GE.receiptItem.Bit2 == null || $scope.GE.receiptItem.Bit2 === '' || $scope.GE.receiptItem.Bit1 == $scope.GE.receiptItem.Bit2) {
                                // การใช้รถยนต์
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['CAR_USAGE'];
                                return oValidate;
                            }
                            if ($scope.GE.receiptItem.Str1 == null || $scope.GE.receiptItem.Str1 == '') {
                                // ทะเบียนรถ
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['LICENSE_PLATE'];
                                return oValidate;
                            }
                            if ($scope.GE.receiptItem.Str2 == null || $scope.GE.receiptItem.Str2 == '') {
                                // ประเภทรถ
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['CAR_TYPE'];
                                return oValidate;
                            }
                        } else if ($scope.panelName == 'pnlMileage') {
                            // ค่าพาหนะ Mileage
                            //if ($scope.GE.receiptItem.Date1 == null || $scope.GE.receiptItem.Date1 == '') {
                            //    // วันที่เดินทาง
                            //    oValidate.isValid = false;
                            //    oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['DATE_DEPART'];
                            //    return oValidate;
                            //}
                            if ($scope.GE.receiptItem.Str1 == null || $scope.GE.receiptItem.Str1 == '') {
                                // สถานที่เริ่มต้น
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['LOCATION_START'];
                                return oValidate;
                            }
                            if ($scope.GE.receiptItem.Str2 == null || $scope.GE.receiptItem.Str2 == '') {
                                // สถานที่สิ้นสุด
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['LOCATION_END'];
                                return oValidate;
                            }
                            if ($scope.GE.receiptItem.Dec1 == null || $scope.GE.receiptItem.Dec1.toString() == '' || $scope.GE.receiptItem.Dec1.toString() == '0') {
                                // ระยะทาง
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['DISTANCE'];
                                return oValidate;
                            }
                            if (!isFinite($scope.GE.receiptItem.Dec1.toString())) {
                                // ระยะทาง
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['INVALID_NUMBER'] + ' (' + $scope.Text['EXPENSE']['DISTANCE'] + ' )';
                                return oValidate;
                            }
                            // Check Sum Amount
                            if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                                // none, average
                            } else {
                                // exactly
                                var sumAmount = 0;
                                if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList != null) {
                                    for (var i = 0; i < $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList.length; i++) {
                                        var amount = Number($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[i].OriginalAmount);
                                        sumAmount += isNaN(amount) ? 0 : amount;
                                    }
                                }
                                if ($scope.GE.receiptItemFormData.Amount != sumAmount) {
                                    oValidate.isValid = false;
                                    oValidate.message = $scope.Text['EXPENSE']['INPUT_AMOUNT_NOT_EQUAL'];
                                    return oValidate;
                                }
                            }
                        } else if ($scope.panelName == 'pnlTransCommutation') {
                            // ค่าพาหนะเหมาจ่าย
                            //if ($scope.GE.receiptItem.Date1 == null || $scope.GE.receiptItem.Date1 == '') {
                            //    // วันที่เดินทาง
                            //    oValidate.isValid = false;
                            //    oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['DATE_DEPART'];
                            //    return oValidate;
                            //}
                            if ($scope.GE.receiptItem.Bit1 == null || $scope.GE.receiptItem.Bit1 === '' || $scope.GE.receiptItem.Bit2 == null || $scope.GE.receiptItem.Bit2 === '' || $scope.GE.receiptItem.Bit3 == null || $scope.GE.receiptItem.Bit3 === ''
                                || ($scope.GE.receiptItem.Bit1 == $scope.GE.receiptItem.Bit2 && $scope.GE.receiptItem.Bit1 == $scope.GE.receiptItem.Bit3)) {
                                // เที่ยวการเดินทาง
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['TRAVEL_ROUND_TYPE'];
                                return oValidate;
                            }
                            if ($scope.GE.receiptItem.Str1 == null || $scope.GE.receiptItem.Str1 == '') {
                                // สถานที่เริ่มต้น
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['LOCATION_START'];
                                return oValidate;
                            }
                            if ($scope.GE.receiptItem.Str2 == null || $scope.GE.receiptItem.Str2 == '') {
                                // สถานที่สิ้นสุด
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['LOCATION_END'];
                                return oValidate;
                            }
                            // Check Sum Amount
                            if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                                // none, average
                            } else {
                                // exactly
                                var sumAmount = 0;
                                if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList != null) {
                                    for (var i = 0; i < $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList.length; i++) {
                                        var amount = Number($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[i].OriginalAmount);
                                        sumAmount += isNaN(amount) ? 0 : amount;
                                    }
                                }
                                if ($scope.GE.receiptItemFormData.Amount != sumAmount) {
                                    oValidate.isValid = false;
                                    oValidate.message = $scope.Text['EXPENSE']['INPUT_AMOUNT_NOT_EQUAL'];
                                    return oValidate;
                                }
                            }
                        }
                        return oValidate;
                    };

                    $scope.getCarTypeDescription = function(arr,id){
                        for (var i = 0; i < arr.length; i++) {
                            if (arr[i].CarTypeID == id) {

                                return arr[i].Description;
                            }
                        }
                        return '';
                    }

			        $scope.sendAddReceiptItemFinish = function () {
                        

                        //Nun Add 26082016
			            if ($scope.panelName == 'pnlVehicleRent' || $scope.panelName == 'pnlVehicleRentAndCarTypeLicense') {
                            $scope.calDiffDateDuration();
                        }
                        //



                        // validate pass
                        if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                            // none, average
                            var exchangeRate = Number($scope.GE.receipt.ExchangeRate);

                            $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].OriginalAmount = $scope.GE.receiptItemFormData.Amount;
                            $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].OriginalAmountInRight = $scope.GE.receiptItemFormData.AmountInRight;
                            $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].LocalAmountInRight = $scope.multiplyExchangeRate($scope.GE.receiptItemFormData.Amount, exchangeRate, $scope.GE.receipt.FromCurrencyRatio, $scope.GE.receipt.ToCurrencyRatio);
                            $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].LocalAmount = $scope.multiplyExchangeRate($scope.GE.receiptItemFormData.Amount, exchangeRate, $scope.GE.receipt.FromCurrencyRatio, $scope.GE.receipt.ToCurrencyRatio);


                            if (Number($scope.GE.receiptItemFormData.AmountInRight) < 9999999999) {
                                var exchangeRate = Number($scope.GE.receipt.ExchangeRate);
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].LocalAmountInRight = $scope.multiplyExchangeRate($scope.GE.receiptItemFormData.AmountInRight, exchangeRate, $scope.GE.receipt.FromCurrencyRatio, $scope.GE.receipt.ToCurrencyRatio);
                            }
                            else
                            {
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].LocalAmountInRight = 9999999999;
                            }


                            $scope.GoToSubMain_AddGEReceiptItemFinish();
                        } else {
                            // exactly
                            
                            // $scope.GE.receiptItemFormData.Amount
                            var exchangeRate = Number($scope.GE.receipt.ExchangeRate);

                            if ($scope.panelName == 'pnlTransCommutation') {
                                // calculate FlatRateNoCalIncome
                                $.each($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList, function (i, itemDetail) {
                                    itemDetail.OriginalAmountInRight = $scope.MathRounding(($scope.FlatRateNoCalIncome * itemDetail.OriginalAmount) / $scope.GE.receiptItemFormData.Amount);
                                    itemDetail.LocalAmountInRight = $scope.multiplyExchangeRate(itemDetail.OriginalAmountInRight, exchangeRate, $scope.GE.receipt.FromCurrencyRatio, $scope.GE.receipt.ToCurrencyRatio);
                                });
                            }
                            if ($scope.panelName == 'pnlMileage') {
                                // calculate mileageNoCalIncomeRate
                                var distance = Number($scope.GE.receiptItem.Dec1);
                                $.each($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList, function (i, itemDetail) {
                                    itemDetail.OriginalAmountInRight = $scope.MathRounding((distance * $scope.mileageNoCalIncomeRate * itemDetail.OriginalAmount) / $scope.GE.receiptItemFormData.Amount);
                                    itemDetail.LocalAmountInRight = $scope.multiplyExchangeRate(itemDetail.OriginalAmountInRight, exchangeRate, $scope.GE.receipt.FromCurrencyRatio, $scope.GE.receipt.ToCurrencyRatio);
                                });
                            }
                            if ($scope.panelName == '') {
                                var distance = Number($scope.GE.receiptItem.Dec1);
                                $.each($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList, function (i, itemDetail) {
                                    itemDetail.OriginalAmountInRight = $scope.GE.receiptItemFormData.AmountInRight;
                                    itemDetail.LocalAmountInRight = $scope.GE.receiptItemFormData.AmountInRight;
                                });
                            }

                            $scope.GoToSubMain_AddGEReceiptItemFinish();
                        }
                        // clear form data


                        /* skip to add receipt finish */
                        $scope.AddReceiptFinish();
                    };



                    $scope.AddReceiptItemFinish = function () {
                        //$scope.GE.receipt.ReceiptDate = $filter('date')($scope.expenseDate, 'yyyy-MM-ddT00:00:00');
                        console.log("$scope.expenseDate", $scope.expenseDate)
                        console.log("$scope.GE.receipt.ReceiptDate", $scope.GE.receipt.ReceiptDate)

                        var oValidate = $scope.validateReceiptItem();
                        if (!oValidate.isValid) {
                            $scope.p_message(oValidate.message,'w');
                            return;
                        }
                       
                        if ($scope.pageClassName == 'GeneralExpenseReport') {
                            validateCreatingRule();
                            return;
                        }

                        if ($scope.document.Additional.TravelSchedulePlaces.length == 0) {
                            $scope.p_message('โปรดเลือกสถานที่ที่จะเดินทางในหน้าหลักก่อนทำรายการในหน้านี้','w');
                            return;
                        }

                        // valid limit of amount
                        validateCreatingRule();
                    };

                    function validateCreatingRule() {

                        var oRequestParameter = { InputParameter: {}, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                        var EXAmountDictionary = [];

                        if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                            EXAmountDictionary.push($scope.GE.receiptItem.ExpenseTypeGroupID + ":" + $scope.GE.receiptItemFormData.Amount);
                        } else {
                            EXAmountDictionary.push($scope.GE.receiptItem.ExpenseTypeGroupID + ":" + $scope.GE.receiptItem.Amount);
                        }

                        var SchedulePlace = [];

                        if ($scope.settings.ExpenseTypeGroupInfo.prefix == "GE_RefTravel")
                        {
                            SchedulePlace = $scope.document.Additional.TravelSchedulePlaces;
                            }
                        else
                        {

                            if ($scope.document.Additional) {
                                SchedulePlace = $scope.document.Additional.TravelSchedulePlaces;

                            } else {
                                SchedulePlace = [];
                            }
                           
                        }

                        oRequestParameter.InputParameter = {
                            RequestNo: $scope.document.RequestNo,
                            EXAmountDictionary: EXAmountDictionary,
                            SchedulePlaceList: SchedulePlace,
                            EmployeeID: $scope.document.Requestor.EmployeeID,
                            ReferRequestNo: $scope.document.ReferRequestNo,
                            OriginalCurrencyCode: $scope.GE.receipt.OriginalCurrencyCode,
                            ExchangeRate: $scope.GE.receipt.ExchangeRate,

                        }
                        var URL = CONFIG.SERVER + 'HRTR/ValidateExpenseTypeGroupCreatingRule';
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            if (response.data == '') {
                                $scope.sendAddReceiptItemFinish();
                            } else {
                                $scope.p_message(response.data, 'w');
                            }
                        }, function errorCallback(response) {
                            console.debug(response);
                            $scope.p_message('Something gone wrong please contract administrator', 'w');
                        });



                    }

                    

                    /* ====== !receipt item function ====== */

                    /* ====== receipt item detail function ====== */

                    $scope.validateReceiptItemDetail = function () {
                        var oValidate = {
                            isValid: true,
                            message: ''
                        };
                        if ($scope.GE.receiptItemDetail.ProjectCode == null || $scope.GE.receiptItemDetail.ProjectCode == '') {
                            oValidate.isValid = false;
                            oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['PROJECT_CODE'];
                            return oValidate;
                        }
                        if ($scope.GE.receiptItemDetail.CostCenter == null || $scope.GE.receiptItemDetail.CostCenter == '') {
                            oValidate.isValid = false;
                            oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['COST_CENTER'];
                            return oValidate;
                        }
                        if ($scope.GE.receiptItemDetail.IO == null || $scope.GE.receiptItemDetail.IO == '') {
                            oValidate.isValid = false;
                            oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['IO'];
                            return oValidate;
                        }
                        if ($scope.GE.receiptItemDetail.OriginalAmount == null || $scope.GE.receiptItemDetail.OriginalAmount.toString() == '') {
                            oValidate.isValid = false;
                            oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['AMOUNT_NO_VAT'];
                            return oValidate;
                        }
                        if (!isFinite($scope.GE.receiptItemDetail.OriginalAmount.toString())) {
                            oValidate.isValid = false;
                            oValidate.message = $scope.Text['SYSTEM']['INVALID_NUMBER'];
                            return oValidate;
                        } else if ($scope.GE.receiptItemDetail.OriginalAmount <= 0) {
                            oValidate.isValid = false;
                            oValidate.message = $scope.Text['EXPENSE']['AMOUNT_NOT_ZERO'];
                            return oValidate;
                        }
                        if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                            // none, average

                        } else {
                            // exactly
                            if ($scope.GE.receiptItemDetailEdit == false && $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList != null) {
                                for (var i = 0; i < $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList.length; i++) {
                                    var tempItemDetail = $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[i];
                                    if (tempItemDetail.ProjectCode == $scope.GE.receiptItemDetail.ProjectCode
                                        && tempItemDetail.IO == $scope.GE.receiptItemDetail.IO
                                        && tempItemDetail.CostCenter == $scope.GE.receiptItemDetail.CostCenter) {
                                        oValidate.isValid = false;
                                        oValidate.message = $scope.Text['EXPENSE']['DUPLICATE_RECEIPT_ITEM_DETAIL'];
                                        return oValidate;
                                    }
                                }
                            }
                        }
                        return oValidate;
                    };

                    $scope.AddReceiptItemDetailFinish = function () {
                        var oValidate = $scope.validateReceiptItemDetail();
                        if (oValidate.isValid) {
                            // validate pass
                            $scope.GoToSubMain_AddGEReceiptItemDetailFinish();
                        } else {
                            $scope.p_message(oValidate.message,'w');
                        }
                    };

                    /* ====== !receipt item detail function ====== */

                    $scope.payToSettings = {
                        ENABLEPAYTOEMPLOYEE: false,
                        ENABLEPAYTOVENDOR: false,
                        ENABLEPAYTOCORPCARD: false,
                    };
                    // call both in receipt and no receipt
                    var getRadioPayTo = function () {
                        if ($scope.document.RequestTypeID == 113) {
                            // TravelReport for Copporate
                            $scope.payToSettings.ENABLEPAYTOEMPLOYEE = false;
                            $scope.payToSettings.ENABLEPAYTOVENDOR = false;
                            $scope.payToSettings.ENABLEPAYTOCORPCARD = true;
                        } else {
                            var TravelRequestNo = '';
                            if ($scope.document.Additional)
                                TravelRequestNo = $scope.document.Additional.TravelRequestNo;
                            var URL = CONFIG.SERVER + 'HRTR/GetRadioPayTo';
                            var oRequestParameter = { InputParameter: { 'TRAVELREQUESTNO': TravelRequestNo, 'PREFIX': $scope.settings.ExpenseTypeGroupInfo.prefix }, CurrentEmployee: $scope.employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                            $http({
                                method: 'POST',
                                url: URL,
                                data: oRequestParameter
                            }).then(function successCallback(response) {

                                $scope.payToSettings.ENABLEPAYTOEMPLOYEE = response.data[0];
                                $scope.payToSettings.ENABLEPAYTOVENDOR = response.data[1];
                                $scope.payToSettings.ENABLEPAYTOCORPCARD = response.data[2];

                                console.log('GetRadioPayTo.', response.data);
                            }, function errorCallback(response) {
                                console.log('error GetRadioPayTo.', response);
                            });
                        }
                    };
                    getRadioPayTo();

                    $scope.GoToSubMain_AddGEReceipt = function (receipt) {
                        console.log('receipt', receipt);
                        isInitialDialogValue = true;
                        if ($scope.settings.ProjectCodeMode != $scope.moduleSetting.PROJECTCODEMODE_EXACTLY || (angular.isDefined($scope.projectCodes) && $scope.projectCodes != null && $scope.projectCodes.length > 0)) {
                            $scope.beginFormWizard();
                            $scope.wizardFormControl.withoutReceiptIsActive = true;
                            $scope.wizardFormFlag.isAddGEReceipt = true;
                            $scope.wizardFormFlag.isAddGEReceiptItem = false;
                            $scope.wizardFormFlag.isAddGEReceiptItemDetail = false;
                            if (angular.isUndefined(receipt)) {
                                // new
                                var ObjReceipt = $scope.model.data.ExpenseReportReceipt[0];
                                $scope.GE.receiptEdit = false;
                                $scope.GE.receipt = angular.copy(ObjReceipt);
                                $scope.GE.receipt.IsReceipt = false;
                                $scope.GE.receipt.RequestNo = $scope.document.RequestNo;
                                $scope.GE.receipt.ReceiptDate = $scope.savedLimitMinDate;
                                $scope.SetLimitDate(false, false);
                                var childLength = ($scope.ExpenseReport.ExpenseReportReceiptList == null) ? 0 : $scope.ExpenseReport.ExpenseReportReceiptList.length;
                                var maxId = 0;
                                if (childLength > 0) {
                                    maxId = $scope.ExpenseReport.ExpenseReportReceiptList[childLength - 1].ReceiptID;
                                }
                                $scope.GE.receipt.ReceiptID = maxId + 1;


                                if ($scope.payToSettings.ENABLEPAYTOCORPCARD == true && $scope.document.RequestTypeID == 113) {
                                    $scope.GE.receipt.PayTo = 'V';
                                } else {
                                    $scope.GE.receipt.PayTo = 'E';
                                }

                                // initial receipt ddl
                                $scope.GE.receipt.VATCode = $scope.ddlDefault.VATCode;
                                //if ($scope.travelCurrencyList != null && $scope.travelCurrencyList.length > 0) {
                                $scope.GE.receipt.OriginalCurrencyCode = $scope.document.Requestor.AreaSetting.CurrencyCode;
                                $scope.GE.receipt.LocalCurrencyCode = $scope.document.Requestor.AreaSetting.CurrencyCode;
                                //}


                                /* skip to add receiptItem */
                                $scope.GoToSubMain_AddGEReceiptItem();



                                console.log('Receipt New.', $scope.GE.receipt);
                            } else {
                                // edit
                                $scope.GE.receiptEdit = true;
                                $scope.GE.receiptEditObj = receipt;
                                $scope.GE.receipt = angular.copy(receipt);
                                $scope.SetLimitDate($scope.GE.receipt.NolimitDate);
                                $scope.GE.receipt.IsReceipt = false;
                                console.log('Recipt Edit.', $scope.GE.receipt);


                                /* skip to edit receiptItem */
                                $scope.GoToSubMain_AddGEReceiptItem($scope.GE.receipt.ExpenseReportReceiptItemList[0]);


                            }
                        } else {
                            $scope.p_message($scope.Text['SYSTEM']['MINIMUM_1'] + $scope.Text['EXPENSE']['PROJECT'],'w');
                        }
                        
                        $scope.GE.receiptItemFormData.tmpEmployeeName = '';
                        $scope.GE.receiptItemFormData.tmpOrgUnitName = '';
                        $scope.GE.receiptItemFormData.tmpCompanyName = '';
                        clearNewGuest();
                        if (!$scope.autocompleteGuest) $scope.autocompleteGuest = {};
                        $scope.autocompleteGuest.searchTextGuest = '';
                    };

                    $scope.GoToSubMain_DeleteGEReceipt = function (receipt) {
                        if (angular.isDefined(receipt)) {

                            if (confirm($scope.Text['SYSTEM']['CONFIRM_DELETE'])) {
                                // delete receipt
                                for (var i = 0; i < $scope.ExpenseReport.ExpenseReportReceiptList.length; i++) {
                                    if ($scope.ExpenseReport.ExpenseReportReceiptList[i] == receipt) {
                                        $scope.ExpenseReport.ExpenseReportReceiptList.splice(i, 1);
                                        break;
                                    }
                                }
                                if ($scope.ExpenseReport.ExpenseReportReceiptList.length == 0) {
                                    $scope.ExpenseReport.ExpenseReportReceiptList = [];
                                }

                                /* fix case when new object $scope.document in root controller */
                                $scope.document.Additional.ExpenseReportReceiptList = $scope.ExpenseReport.ExpenseReportReceiptList;
                                /* !fix case when new object $scope.document in root controller */

                                //--Show Grid
                                $scope.expenseItems_WithoutReceipt = $scope.getExpenseItems_WithoutReceipt();
                                //--Show Grid
                            }
                            console.log('document.', $scope.document);
                        }
                    };

                    $scope.GoToSubMain_AddGEReceiptFinish = function () {
                        if (!$scope.GE.receiptEdit) {
                            // finish from add
                            if ($scope.ExpenseReport.ExpenseReportReceiptList == null) {
                                $scope.ExpenseReport.ExpenseReportReceiptList = [];
                            }
                            $scope.ExpenseReport.ExpenseReportReceiptList.push($scope.GE.receipt);
                        } else {
                            // finish from edit
                            for (var i = 0; i < $scope.ExpenseReport.ExpenseReportReceiptList.length; i++) {
                                if ($scope.ExpenseReport.ExpenseReportReceiptList[i] == $scope.GE.receiptEditObj) {
                                    $scope.ExpenseReport.ExpenseReportReceiptList[i] = $scope.GE.receipt;
                                    break;
                                }
                            }
                        }

                        $scope.GE.receiptEditObj = null;
                        $scope.GE.receipt = null;
                        // clear form data

                        $scope.wizardFormFlag.isAddGEReceipt = false;
                        $scope.wizardFormFlag.isAddGEReceiptItem = false;
                        $scope.wizardFormFlag.isAddGEReceiptItemDetail = false;
                        $scope.wizardFormControl.withoutReceiptIsActive = false;
                        $scope.finishFormWizard();

                        /* fix case when new object $scope.document in root controller */
                        $scope.document.Additional.ExpenseReportReceiptList = $scope.ExpenseReport.ExpenseReportReceiptList;
                        /* !fix case when new object $scope.document in root controller */

                        //--Show Grid
                        $scope.expenseItems_WithoutReceipt = $scope.getExpenseItems_WithoutReceipt();
                        //--Show Grid

                        $timeout(function () {
                            document.getElementById('top-anchor2').scrollIntoView();
                        }, 100);

                        console.log('Recipt Add Receipt Finish.', $scope.ExpenseReport);
                        console.log('document.', $scope.document);
                    };

                    $scope.GoToSubMain_AddGEReceiptCancel = function () {
                        //if (confirm($scope.Text['SYSTEM']['CONFIRM_CANCEL'])) {
                        $scope.GE.receipt = null;
                        // clear form data

                        $scope.wizardFormFlag.isAddGEReceipt = false;
                        $scope.wizardFormFlag.isAddGEReceiptItem = false;
                        $scope.wizardFormFlag.isAddGEReceiptItemDetail = false;
                        $scope.wizardFormControl.withoutReceiptIsActive = false;
                        $scope.finishFormWizard();

                        $timeout(function () {
                            document.getElementById('top-anchor2').scrollIntoView();
                        }, 100);
                        //}
                    };

                    $scope.findObjCostCenter = function (costcenterCode) {
                        for (var i = 0; i < $scope.masterData.objCostcenter.length; i++) {
                            if (costcenterCode == $scope.masterData.objCostcenter[i].CostCenterCode) {
                                return $scope.masterData.objCostcenter[i];
                            }
                        }
                        return null;
                    };

                    $scope.GoToSubMain_AddGEReceiptItem = function (receiptItem) {
                        $scope.wizardFormFlag.isAddGEReceipt = false;
                        $scope.wizardFormFlag.isAddGEReceiptItem = true;
                        $scope.wizardFormFlag.isAddGEReceiptItemDetail = false;
                        if (angular.isUndefined(receiptItem)) {
                            // new
                            var ObjReceiptItem = $scope.model.data.ExpenseReportReceiptItem[0];
                            $scope.GE.receiptItemEdit = false;
                            $scope.GE.receiptItemFormData.Amount = 0;
                            $scope.GE.receiptItemFormData.AmountInRight = 9999999999;
                            $scope.GE.receiptItem = angular.copy(ObjReceiptItem);
                            $scope.GE.receiptItem.Amount = 0;
                            $scope.GE.receiptItem.RequestNo = $scope.document.RequestNo;
                            $scope.GE.receiptItem.ReceiptID = $scope.GE.receipt.ReceiptID;
                            $scope.GE.receiptItem.PayTo = 'E';
                            var childLength = ($scope.GE.receipt.ExpenseReportReceiptItemList == null) ? 0 : $scope.GE.receipt.ExpenseReportReceiptItemList.length;
                            var maxId = 0;
                            if (childLength > 0) {
                                maxId = $scope.GE.receipt.ExpenseReportReceiptItemList[childLength - 1].ItemID;
                            }
                            $scope.GE.receiptItem.ItemID = maxId + 1;
                            $scope.GE.receiptItem.ExpenseTypeGroupID = $scope.ddlDefault.ExpenseTypeGroupID.toString();

                            //--------Nun Add ExpenseTypeGroup Name and Remark 09/06/2016
                            $scope.GE.receiptItem.ExpenseTypeGroupName = $scope.ddlDefault.ExpenseTypeGroupName;
                            $scope.GE.receiptItem.ExpenseTypeGroupRemark = $scope.ddlDefault.ExpenseTypeGroupRemark;
                            //-----------------------------------------------------------

                            //$scope.GE.receiptItem.ExpenseTypeID = $scope.ddlDefault.ExpenseTypeID;

                            //--------Nun Add ExpenseType Name and Remark 09/06/2016
                            //$scope.GE.receiptItem.ExpenseTypeName = $scope.ddlDefault.ExpenseTypeName;
                            //-----------------------------------------------------------

                            $scope.getExpenseTypeByGroupID($scope.ddlDefault.ExpenseTypeGroupID);

                            if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                                // none, average
                                // new 1 itemDetail
                                var ObjReceiptItemDetail = angular.copy($scope.model.data.ExpenseReportReceiptItemDetail[0]);
                                ObjReceiptItemDetail.RequestNo = $scope.document.RequestNo;
                                ObjReceiptItemDetail.ReceiptID = $scope.GE.receipt.ReceiptID;
                                ObjReceiptItemDetail.ItemID = $scope.GE.receiptItem.ItemID;
                                if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList == null) {
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList = [];
                                }

                                // set default CostCenter, IO
                                if (angular.isUndefined($scope.DefaultCostCenterIOByGroup[$scope.GE.receiptItem.ExpenseTypeGroupID])) {
                                    ObjReceiptItemDetail.CostCenter = $scope.DefaultCostCenterIO.CostCenter;
                                    ObjReceiptItemDetail.IsAlternativeCostCenter = $scope.DefaultCostCenterIO.IsAlternativeCostCenter;
                                    ObjReceiptItemDetail.AlternativeIOOrg = $scope.DefaultCostCenterIO.OrgUnitID;
                                    ObjReceiptItemDetail.IsAlternativeIOOrg = $scope.DefaultCostCenterIO.IsAlternativeIOOrg;
                                    ObjReceiptItemDetail.IO = $scope.DefaultCostCenterIO.IO;
                                } else {
                                    var objDefault = $scope.DefaultCostCenterIOByGroup[$scope.GE.receiptItem.ExpenseTypeGroupID];
                                    //ObjReceiptItemDetail.CostCenter = objDefault.CostCenter;
                                    //ObjReceiptItemDetail.IsAlternativeCostCenter = objDefault.IsAlternativeCostCenter;
                                    //Edit by Nipon Supap 13-07-2017
                                    ObjReceiptItemDetail.CostCenter = $scope.document.Additional.CostCenter;
                                    ObjReceiptItemDetail.IsAlternativeCostCenter = $scope.document.Additional.IsAlternativeCostCenter;
                                    if (objDefault.IsAlternativeIOOrg && objDefault.OrgUnitID) {
                                        ObjReceiptItemDetail.AlternativeIOOrg = objDefault.OrgUnitID;
                                    } else {
                                        ObjReceiptItemDetail.AlternativeIOOrg = $scope.DefaultCostCenterIO.OrgUnitID;
                                    }
                                    ObjReceiptItemDetail.IsAlternativeIOOrg = objDefault.IsAlternativeIOOrg;
                                    ObjReceiptItemDetail.IO = objDefault.IO;
                                }
                                //if (ObjReceiptItemDetail.IsAlternativeIOOrg) {
                                    // get default IO by Org
                                    $scope.getIOByOrganization(ObjReceiptItemDetail.AlternativeIOOrg);
                                //} else {
                                //    // get default IO by CostCenter
                                //    $scope.getIOByCostCenter(ObjReceiptItemDetail.CostCenter);
                                //}

                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList.push(ObjReceiptItemDetail);
                            }

                            console.log('ReciptItem New.', $scope.GE.receiptItem);
                        } else {
                            // edit
                            $scope.GE.receiptItemEdit = true;
                            $scope.GE.receiptItemEditObj = receiptItem;
                            $scope.GE.receiptItem = angular.copy(receiptItem);
                            if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                                // none, average
                                $scope.GE.receiptItemFormData.Amount = $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].OriginalAmount;
                                $scope.GE.receiptItemFormData.AmountInRight = $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].OriginalAmountInRight;


                                // set default CostCenter, IO
                                //if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].IsAlternativeIOOrg) {
                                    $scope.getIOByOrganization($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].AlternativeIOOrg);
                                //} else {
                                //    $scope.getIOByCostCenter($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].CostCenter);
                                //}

                            } else {
                                // exactly
                                var sumAmount = 0;
                                if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList != null) {
                                    for (var i = 0; i < $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList.length; i++) {
                                        var amount = Number($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[i].OriginalAmount);
                                        sumAmount += $scope.MathRounding(isNaN(amount) ? 0 : amount);
                                    }
                                }

                                if ($scope.panelName == 'pnlMileage' || $scope.panelName == 'pnlTransCommutation') {
                                    $scope.GE.receiptItemFormData.Amount = sumAmount;
                                } else {
                                    $scope.GE.receiptItem.Amount = sumAmount;
                                }
                            }
                            $scope.GE.receiptItem.ExpenseTypeGroupID = $scope.GE.receiptItem.ExpenseTypeGroupID.toString();
                            //Add by Nipon Supap 08/11/2017 UAT-019 [sp_ExpenseTypeGroupConversionGet]
                            $scope.ExpensetypMergVehicle = true;
                            for (var i = 0; i < $scope.expenseTypeGroup.length; i++) {
                                if ($scope.expenseTypeGroup[i].ExpenseTypeGroupID == $scope.GE.receiptItem.ExpenseTypeGroupID) {
                                    $scope.ExpensetypMergVehicle = false;
                                }
                            }
                            if ($scope.ExpensetypMergVehicle) {
                                $scope.getExpenseTypeGroupConversion($scope.GE.receiptItem.ExpenseTypeGroupID, "LoadEdit");
                            }

                            $scope.getExpenseTypeByGroupID($scope.GE.receiptItem.ExpenseTypeGroupID);
                            $scope.GE.receiptItem.ExpenseTypeID = $scope.GE.receiptItem.ExpenseTypeID.toString();
                            console.log('ReciptItem Edit.', $scope.GE.receiptItem);
                        }


                        $timeout(function () {
                            document.getElementById('top-anchor2').scrollIntoView();
                        }, 100);
                    };


                    //------------- Nun Add ExpenseTypeGroup&ExpenseType -------------

                    //var URL = CONFIG.SERVER + 'HRTR/GetVendorAll';
                    //var oRequestParameter = { InputParameter: {}, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    //$http({
                    //    method: 'POST',
                    //    url: URL,
                    //    data: oRequestParameter
                    //}).then(function successCallback(response) {

                    //    $scope.objVendor = response.data;

                    //    if ($scope.objVendor != null && $scope.objVendor.length > 0) {

                    //        for (var i = 0; i < $scope.objVendor.length; i++) {
                    //            if ($scope.objVendor[i].TaxID != null) {
                    //                $scope.objVendor[i].TaxPersonalID = ' : ' + $scope.objVendor[i].TaxID;
                    //            }
                    //            else {
                    //                if ($scope.objVendor[i].PersonalID != null) {
                    //                    $scope.objVendor[i].TaxPersonalID = ' : ' + $scope.objVendor[i].PersonalID;
                    //                }
                    //                else {
                    //                    $scope.objVendor[i].TaxPersonalID = '';
                    //                }

                    //            }
                    //        }
                    //    }
                    //    //console.log('objVendor.', $scope.objVendor);

                    //}, function errorCallback(response) {
                    //    console.log('error GeneralExpenseEditorController GetVendorAll.', response);
                    //});


                    //var URL = CONFIG.SERVER + 'HRTR/GetAllCountry';
                    //var oRequestParameter = { InputParameter: {}, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    //$http({
                    //    method: 'POST',
                    //    url: URL,
                    //    data: oRequestParameter
                    //}).then(function successCallback(response) {
                    //    $scope.objCountry = response.data;
                    //    //console.log('objCountry.', $scope.objCountry);
                    //}, function errorCallback(response) {
                    //    console.log('error GeneralExpenseEditorController GetAllCountry.', response);
                    //});


                    $scope.onSelectedCountry = function (Country) {
                        var URL = CONFIG.SERVER + 'HRTR/GetProvinceByCountry/';
                        var oRequestParameter = { InputParameter: { "CountryCode": Country }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            $scope.objProvince = response.data;
                            if ($scope.objProvince != null && $scope.objProvince.length > 0) {

                                $scope.GE.receipt.VendorCity = $scope.objProvince[0].ProvinceName;
                                if ($scope.whtSettings.ENABLEWHT) {
                                    $scope.changeShowWhtTax();
                                }
                            }
                            else {
                                $scope.GE.receipt.VendorCity = null;

                            }
                            console.log('objProvince.', $scope.objProvince);
                        }, function errorCallback(response) {
                            console.log('error GeneralExpenseEditorController GetProvinceByCountry.', response);
                        });
                    };

                    //Add by Nipon Supap 08/11/2017 [sp_ExpenseTypeGroupConversionGet]
                    $scope.getExpenseTypeGroupConversion = function (oExpenseTypeGroupID, FlagState) {
                        var URL = CONFIG.SERVER + 'HRTR/GetExpenseTypeGroupConversion/';
                        var oRequestParameter = { InputParameter: { "ExpenseTypeGroupID": oExpenseTypeGroupID, "CompanyCode": $scope.document.Requestor.CompanyCode } };

                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            if (response.data.length > 0) {
                                if (FlagState == "LoadEdit") {
                                    $scope.GE.receiptItem.ExpenseTypeGroupID = response.data[0].NewExpenseTypeGroupID.toString();
                                    $scope.GE.receiptItem.ExpenseTypeGroupName = response.data[0].Name;
                                    $scope.GE.receiptItem.ExpenseTypeGroupRemark = response.data[0].Remark;
                                }
                                else if (FlagState == "LoadView") {
                                    //$scope.expenseItems_ReceiptItem[0]
                                    $scope.GE.receipt.ExpenseReportReceiptItemList[0].ExpenseTypeGroupID = response.data[0].NewExpenseTypeGroupID.toString();
                                    $scope.GE.receipt.ExpenseReportReceiptItemList[0].ExpenseTypeGroupName = response.data[0].Name;
                                    $scope.GE.receipt.ExpenseReportReceiptItemList[0].ExpenseTypeGroupRemark = response.data[0].Remark;

                                    $scope.expenseItems_ReceiptItem[0].ExpenseTypeGroupID = response.data[0].NewExpenseTypeGroupID.toString();
                                    $scope.expenseItems_ReceiptItem[0].ExpenseTypeGroupName = response.data[0].Name;
                                    $scope.expenseItems_ReceiptItem[0].ExpenseTypeGroupRemark = response.data[0].Remark;
                                }
                            }
                        }, function errorCallback(response) {
                            $scope.tempFormData.chkVendorTaxIDDup = 'error';
                            console.log('error GeneralExpenseEditorController checkTaxIDDup.', response);
                        });
                    };
                    //$scope.getCostCenterForLookup = function () {
                    //    var URL = CONFIG.SERVER + 'HRTR/GetCostCenterForLookup';
                    //    var oRequestParameter = { InputParameter: {}, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    //    $http({
                    //        method: 'POST',
                    //        url: URL,
                    //        data: oRequestParameter
                    //    }).then(function successCallback(response) {
                    //        $scope.objCostcenterLookup = response.data;
                    //        console.log('GetCostCenterForLookup.', $scope.objCostcenterLookup);
                    //    }, function errorCallback(response) {
                    //        console.log('error GeneralExpenseEditorController GetCostCenterForLookup.', response);
                    //    });
                    //};
                    //$scope.getCostCenterForLookup();

                    //$scope.getIOForLookup = function () {
                    //    var URL = CONFIG.SERVER + 'HRTR/GetInternalOrderObjForLookup';
                    //    var oRequestParameter = { InputParameter: {}, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    //    $http({
                    //        method: 'POST',
                    //        url: URL,
                    //        data: oRequestParameter
                    //    }).then(function successCallback(response) {
                    //        $scope.objIOLookupObj = response.data;
                    //        console.log('GetInternalOrderObjForLookup.', $scope.objIOLookupObj);
                    //    }, function errorCallback(response) {
                    //        console.log('error GeneralExpenseEditorController GetInternalOrderObjForLookup.', response);
                    //    });
                    //};
                    //$scope.getIOForLookup();

                    //var URL = CONFIG.SERVER + 'HRTR/GetVATTypeDefault';
                    //var oRequestParameter = { InputParameter: {}, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    //$http({
                    //    method: 'POST',
                    //    url: URL,
                    //    data: oRequestParameter
                    //}).then(function successCallback(response) {
                    //    $scope.setVatTypeDefault = response.data;

                    //    if ($scope.setVatTypeDefault != null && $scope.setVatTypeDefault.length > 0) {
                    //        $scope.ddlDefault.VATCode = $scope.setVatTypeDefault[0].VATCode;
                    //    }

                    //    //console.log('GetVATTypeDefault.', $scope.setVatTypeDefault);

                    //}, function errorCallback(response) {
                    //    console.log('error GetVATTypeDefault.', response);
                    //});

                    $scope.getPanelNameByExpenseType = function (expenseTypeID) {
                        var URL = CONFIG.SERVER + 'HRTR/GetEditorPanelByExpenseTypeID';
                        var oRequestParameter = { InputParameter: { "ExpenseTypeID": expenseTypeID }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            if (response.data && response.data.length > 0) {
                                $scope.panelName = response.data[0];
                                $scope.pnlRequireUploadFile = response.data[1] == 'True';
                                $scope.pnlNoOfFile = response.data[2];
                            } else {
                                $scope.panelName = '';
                            }

                            /* set default panel initial value */
                            if ($scope.GE.receiptItemEdit == false && !isInitialDialogValue) {
                                $scope.GE.receiptItemFormData.Amount = 0;
                                $scope.GE.receiptItemFormData.AmountInRight = 9999999999;
                            }
                            if ($scope.panelName == '' && !isInitialDialogValue) {
                                $scope.GE.receiptItemFormData.Amount = 0;
                                $scope.GE.receiptItemFormData.AmountInRight = 9999999999;
                            } else if ($scope.panelName == 'pnlTransRegistration') {
                                if ($scope.GE.receiptItem.Bit1 == $scope.GE.receiptItem.Bit2) {
                                    $scope.GE.receiptItem.Bit1 = true;
                                    $scope.GE.receiptItem.Bit2 = false;
                                }
                            } else if ($scope.panelName == 'pnlMileage') {
                                //if ($scope.GE.receiptItem) {
                                //    var amount = Number($scope.GE.receiptItem.Dec1)
                                //    var distance = isNaN(amount) ? 0 : amount;
                                //    $scope.GE.receiptItemFormData.Amount = distance * $scope.fuelRate;
                                //    $scope.GE.receiptItemFormData.AmountInRight = distance * $scope.mileageNoCalIncomeRate;
                                //} else {
                                //    $scope.GE.receiptItemFormData.Amount = 0;
                                //    $scope.GE.receiptItemFormData.AmountInRight = 0;
                                //}

                                //Nun Add
                                if ($scope.oTranType != null && $scope.oTranType.length > 0 && $scope.GE.receiptItem && $scope.GE.receiptItem.Str3 == '') {
                                    $scope.GE.receiptItem.Str3 = $scope.oTranType[0].TransportationTypeCode;
                                    $scope.getMileageRate($scope.oTranType[0].TransportationTypeCode);
                                }


                            } else if ($scope.panelName == 'pnlTransCommutation') {
                                if ($scope.GE.receiptItemEdit) {
                                    $scope.GE.receiptItemDetailFormData.FlatLocationFilter.Key = '!' + $scope.GE.receiptItem.Str1;
                                } else {
                                    $scope.GE.receiptItemDetailFormData.FlatLocationFilter.Key = '!';
                                }
                                if ($scope.GE.receiptItem) {
                                    if ($scope.GE.receiptItem.Bit1 == $scope.GE.receiptItem.Bit2 && $scope.GE.receiptItem.Bit1 == $scope.GE.receiptItem.Bit3) {
                                        // default
                                        $scope.GE.receiptItem.Bit1 = false;
                                        $scope.GE.receiptItem.Bit2 = true;
                                        $scope.GE.receiptItem.Bit3 = false;
                                    }
                                    calculateAmountFromLocation($scope.GE.receiptItem.Str1, $scope.GE.receiptItem.Str2);
                                } else {
                                }
                            } else if ($scope.panelName == 'pnlAccommodation') {
                                if ($scope.GE.receiptItem && !$scope.GE.receiptItemEdit) {
                                    $scope.GE.receiptItem.Date1 = $scope.savedLimitMinDate;
                                    $scope.GE.receiptItem.Date2 = $scope.savedLimitMaxDate;
                                } else if ($scope.GE.receiptItem && $scope.GE.receiptItemEdit && expenseTypeID != $scope.GE.receiptItemEditObj.ExpenseTypeID) {
                                    $scope.GE.receiptItem.Date1 = $scope.savedLimitMinDate;
                                    $scope.GE.receiptItem.Date2 = $scope.savedLimitMaxDate;
                                }
                            }
                            //add for CR UAT014 
                            else if ($scope.panelName == 'pnlAccommutationForLastNight') {
                                $scope.GE.receiptItem.Date1 = $scope.savedLimitMaxDate;
                                $scope.GE.receiptItem.Date2 = $scope.savedLimitMaxDate;
                            }

                            //Nun Add 25/08/2016
                            else if ($scope.panelName == 'pnlVehicleRent' || $scope.panelName == 'pnlVehicleRentAndCarTypeLicense') {
                                if ($scope.GE.receiptItem && !$scope.GE.receiptItemEdit) {
                                    $scope.GE.receiptItem.Date1 = $scope.savedLimitMinDate;
                                    $scope.GE.receiptItem.Date2 = $scope.savedLimitMaxDate;
                                }
                                $scope.getExpenseTypeAmountInRight(expenseTypeID);
                            }
                            //
                            // reset Amount in right when change Expense Type
                            else {
                                $scope.amountInRight = 9999999999;
                                $scope.GE.receiptItemFormData.AmountInRight = 9999999999;
                            }

                            isInitialDialogValue = false;

                            //
                            /* ! set default panel initial value */

                            console.log('ExpenseReceiptEditorDirective panelName.', $scope.panelName);


                        }, function errorCallback(response) {
                            console.log('error ExpenseReceiptEditorDirective panelName.', response);
                        });
                    };

                    //var URL = CONFIG.SERVER + 'HRTR/GetAllPaymentMethod';
                    //var oRequestParameter = { InputParameter: {}, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    //$http({
                    //    method: 'POST',
                    //    url: URL,
                    //    data: oRequestParameter
                    //}).then(function successCallback(response) {
                    //    $scope.setPaymentMethod = response.data;
                    //    //console.log('GetAllPaymentMethod.', $scope.setPaymentMethod);

                    //}, function errorCallback(response) {
                    //    console.log('error GetAllPaymentMethod.', response);
                    //});

                    //var URL = CONFIG.SERVER + 'HRTR/GetAllPaymentSupplement';
                    //var oRequestParameter = { InputParameter: {}, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    //$http({
                    //    method: 'POST',
                    //    url: URL,
                    //    data: oRequestParameter
                    //}).then(function successCallback(response) {
                    //    $scope.setPaymentSupplement = response.data;
                    //    //console.log('GetAllPaymentSupplement.', $scope.setPaymentSupplement);

                    //}, function errorCallback(response) {
                    //    console.log('error GetAllPaymentSupplement.', response);
                    //});

                    //var URL = CONFIG.SERVER + 'HRTR/GetPaymentTermByLanguage';
                    //var oRequestParameter = { InputParameter: {}, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                    //$http({
                    //    method: 'POST',
                    //    url: URL,
                    //    data: oRequestParameter
                    //}).then(function successCallback(response) {
                    //    $scope.setPaymentTerm = response.data;
                    //    //console.log('GetPaymentTermByLanguage.', $scope.setPaymentTerm);

                    //}, function errorCallback(response) {
                    //    console.log('error GetPaymentTermByLanguage.', response);
                    //});


                    $scope.changeShowWhtTax = function () {
                        if ($scope.whtSettings.ENABLEWHT == true) {        //ถ้าไม่ได้เปิด Panel WHTTax ไม่ต้อง bind ค่า WHTType,WHTCode
                            $scope.fillWHTTax();
                            $scope.changeWHTType1();
                        }   
                    };

                    $scope.fillWHTTax = function () {
                        var oVendor = $scope.GE.receipt.VendorCode;
                        var oVendorCountry = $scope.GE.receipt.VendorCountry;
                        $scope.onBindWHTType(oVendor, oVendorCountry);
                    };

                    $scope.changeWHTType1 = function () {
                        var oVendorCountry = $scope.GE.receipt.VendorCountry;
                        $scope.onBindWHTCode1($scope.GE.receipt.WHTType1, oVendorCountry);
                    };
                    $scope.changeWHTType2 = function () {
                        var oVendorCountry = $scope.GE.receipt.VendorCountry;
                        $scope.onBindWHTCode2($scope.GE.receipt.WHTType2, oVendorCountry);
                    };

                    $scope.onBindWHTType = function (VendorCode, CountryCode) {

                        var URL = CONFIG.SERVER + 'HRTR/GetWHTTypeByVendorCode/';

                        var mapType = ($scope.pageClassName == 'GeneralExpenseReport' || $scope.pageClassName == 'TravelReport') ? 'TE' : 'EX';

                        var oRequestParameter = { InputParameter: { "VendorCode": VendorCode, "CountryCode": CountryCode, "MappingType": mapType }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };

                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {


                            if (response.data != null) {
                                var oAll = { "WithholdingType": "", "Description": "Select or Search" };
                                var oWHTType = response.data;
                                oWHTType.unshift(oAll);
                                $scope.objWHTType = oWHTType;
                            }


                            //$scope.objWHTType = response.data;
                            //if ($scope.objWHTType != null && $scope.objWHTType.length > 0) {
                            //    $scope.GE.receipt.WHTType1 = $scope.objWHTType[0].WithholdingType;
                            //    $scope.GE.receipt.WHTType2 = $scope.objWHTType[0].WithholdingType;

                            //    $scope.onBindWHTCode1($scope.GE.receipt.WHTType1, CountryCode);
                            //    $scope.onBindWHTCode2($scope.GE.receipt.WHTType2, CountryCode);
                            //}


                            console.log('objWHTType.', $scope.objWHTType);
                        }, function errorCallback(response) {
                            console.log('error GeneralExpenseEditorController GetWHTTypeByVendorCode.', response);
                        });
                    };

                    $scope.onBindWHTCode1 = function (WHTType, CountryCode) {
                        var URL = CONFIG.SERVER + 'HRTR/GetWHTCodeByWHTType/';
                        var oRequestParameter = { InputParameter: { "WHTType": WHTType, "CountryCode": CountryCode }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };

                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {

                            if (response.data != null) {
                                var oAll = { "WithholdingCode": "", "Description": "Select or Search" };
                                var oWHTCode = response.data;
                                oWHTCode.unshift(oAll);
                                $scope.objWHTCode = oWHTCode;
                            }

                            
                            console.log('objWHTCode.', $scope.objWHTCode);
                        }, function errorCallback(response) {
                            console.log('error GeneralExpenseEditorController GetWHTCodeByWHTType.', response);
                        });
                    };
                    $scope.onBindWHTCode2 = function (WHTType, CountryCode) {
                        var URL = CONFIG.SERVER + 'HRTR/GetWHTCodeByWHTType/';
                        var oRequestParameter = { InputParameter: { "WHTType": WHTType, "CountryCode": CountryCode }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };

                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {

                            if (response.data != null) {
                                var oAll = { "WithholdingCode": "", "Description": "Select or Search" };
                                var oWHTCode = response.data;
                                oWHTCode.unshift(oAll);
                                $scope.objWHTCode2 = oWHTCode;
                            }

                            
                            console.log('objWHTCode.', $scope.objWHTCode);
                        }, function errorCallback(response) {
                            console.log('error GeneralExpenseEditorController GetWHTCodeByWHTType.', response);
                        });
                    };

                    $scope.onBlurWHTAmount = function () {
                        var whtAmount1 = Number($scope.GE.receipt.WHTAmount1LC);
                        var whtAmount2 = Number($scope.GE.receipt.WHTAmount2LC);
                        var oSumWHT = 0;

                        if ($scope.GE.receipt.WHTType1 != null && $scope.GE.receipt.WHTType1 != '') {
                            if ($scope.tempData.oWHTTypeSubsidise.indexOf($scope.GE.receipt.WHTType1) > -1) {
                                //In the array!
                            } else {
                                oSumWHT = oSumWHT + whtAmount1; //Not in the array 
                            }
                        }

                        if ($scope.GE.receipt.WHTType2 != null && $scope.GE.receipt.WHTType2 != '') {
                            if ($scope.tempData.oWHTTypeSubsidise.indexOf($scope.GE.receipt.WHTType2) > -1) {
                                //In the array!
                            } else {
                                oSumWHT = oSumWHT + whtAmount2; //Not in the array 
                            }
                        }
                        $scope.tempData.sumWHTAmount = oSumWHT;
                    };

                    $scope.isSelectPayToCompany = false;

                    $scope.selectPayTo = function () {
                        var param = $scope.GE.receipt.PayTo;

                        //-------- Nun 29/06/2016 10:18--------
                        //if (param == 'V') {
                        //    $scope.isSelectPayToCompany = true;
                        //}
                        //else {
                        //    $scope.isSelectPayToCompany = false;
                        //}
                        //-------- Nun 29/06/2016 10:18--------
                    }
                    $scope.IsEditVendor = false;
                    $scope.onSelectedVatType = function (oVatPercent) {
                        console.log('oVatPercent', oVatPercent);
                        var VATBaseAmount = Number($scope.GE.receipt.VATBaseAmount);
                        VATBaseAmount = isNaN(VATBaseAmount) ? 0 : $scope.MathRounding(VATBaseAmount);
                        var VatPercent = Number(oVatPercent);
                        VatPercent = isNaN(VatPercent) ? 0 : VatPercent;
                        $scope.GE.receipt.VATPercent = VatPercent;
                        $scope.GE.receipt.VATAmount = $scope.MathRounding(VATBaseAmount * VatPercent / 100);
                    }
                    $scope.personalCurrency = null;
                    $scope.getPersonalCurrency = function () {
                        var URL = CONFIG.SERVER + 'HRTR/GetPersonalExchangeRateCapture';
                        var oRequestParameter = { InputParameter: {}, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            $scope.personalCurrency = response.data[0];

                            if ($scope.settings.ExpenseTypeGroupInfo.prefix.toUpperCase() == 'GE') {
                                $scope.travelCurrencyList = response.data;
                            } else {
                                $scope.travelCurrencyList = angular.copy($scope.masterData.travelCurrencyList);
                                if ($scope.travelCurrencyList == null) {
                                    $scope.travelCurrencyList = [];
                                }
                                if ($scope.personalCurrency != null) {
                                    $scope.travelCurrencyList.unshift(angular.copy($scope.personalCurrency));
                                }


                                /* for test */
                                //$scope.travelCurrencyList = [
                                //    { "FromCurrency": "THB", "ExchangeRate": 1 },
                                //    { "FromCurrency": "USD", "ExchangeRate": 3 }
                                //];
                                /* !for test */
                            }

                            //if ($scope.travelCurrencyList != null && $scope.travelCurrencyList.length > 0) {
                            //    $scope.GE.receipt.OriginalCurrencyCode = $scope.travelCurrencyList[0].FromCurrency;
                            //}

                        }, function errorCallback(response) {
                            console.log('error GeneralExpenseEditorController GetPersonalCurrency.', response);
                        });
                    };
                    $scope.getPersonalCurrency();
                    $scope.onSelectedVendor = function (oVendor) {
                        console.log('selected vendor.', oVendor);

                        $scope.GE.receipt.VendorName = oVendor.VendorName;
                        $scope.GE.receipt.VendorAddress1 = oVendor.VendorAddress1;
                        $scope.GE.receipt.VendorAddress2 = oVendor.VendorAddress2;

                        //Nun Add 28112016
                        $scope.GE.receipt.VendorAddress3 = oVendor.VendorAddress3;
                        $scope.GE.receipt.VendorAddress4 = oVendor.VendorAddress4;
                        //

                        for (var i = 0; i < $scope.objCountry.length; i++) {
                            if ($scope.objCountry[i].CountryCode == oVendor.CountryCode) {
                                $scope.GE.receipt.VendorCountry = $scope.objCountry[i].CountryCode;
                                break;
                            }
                        }

                        $scope.onSelectedCountry(oVendor.CountryCode);

                        $scope.GE.receipt.VendorPostCode = oVendor.PostalCode;
                        $scope.GE.receipt.VendorTaxID = oVendor.TaxID;
                        $scope.GE.receipt.VendorPhone = oVendor.Telephone1;
                        $scope.GE.receipt.VendorFax = oVendor.Fax;
                        $scope.GE.receipt.VendorEmail = oVendor.Mail;

                        for (var i = 0; i < $scope.setPaymentMethod.length; i++) {
                            if ($scope.setPaymentMethod[i].PaymentMethodCodeConcatDesc.indexOf(oVendor.PaymentMethodCode) != -1) {
                                $scope.GE.receipt.PaymentMethodCode = $scope.setPaymentMethod[i].PaymentMethodCodeConcatDesc;
                                break;
                            }
                        }

                        for (var j = 0; j < $scope.setPaymentSupplement.length; j++) {
                            if ($scope.setPaymentSupplement[j].PaymentSupplementCode == oVendor.PaymentSupplementCode) {
                                $scope.GE.receipt.PaymentSupplementCode = $scope.setPaymentSupplement[j].PaymentSupplementCode;
                                break;
                            }
                        }

                        for (var k = 0; k < $scope.setPaymentTerm.length; k++) {
                            if ($scope.setPaymentTerm[k].PaymentTermCodeConcatDayLimit.indexOf(oVendor.PaymentTermCode) != -1) {
                                $scope.GE.receipt.PaymentTermCode = $scope.setPaymentTerm[k].PaymentTermCodeConcatDayLimit;
                                break;
                            }
                        }


                        console.log('oVendorCode', oVendor.VendorCode);


                        if (oVendor.VendorCode.substring(0, 2) == 'PX') {
                            //$scope.isSelectPayToCompany = false;
                            $scope.IsEditVendor = true;
                        }
                        else {
                            //$scope.isSelectPayToCompany = true;
                            $scope.IsEditVendor = false;
                        }
                        if ($scope.whtSettings.ENABLEWHT) {
                            $scope.changeShowWhtTax();
                        }
                    }

                    $scope.changeAlternativePayee = function () {
                        if ($scope.GE.receipt.IsAlternativePayee == true) {
                            $scope.IsEditVendor = true;
                        }
                        else {
                            $scope.IsEditVendor = false;
                        }
                    };

                    $scope.GoToSubMain_DeleteGEReceiptItem = function (receiptItem) {
                        if (angular.isDefined(receiptItem)) {
                            // delete receiptItem
                            if (confirm($scope.Text['SYSTEM']['CONFIRM_DELETE'])) {
                                for (var i = 0; i < $scope.GE.receipt.ExpenseReportReceiptItemList.length; i++) {
                                    if ($scope.GE.receipt.ExpenseReportReceiptItemList[i] == receiptItem) {
                                        $scope.GE.receipt.ExpenseReportReceiptItemList.splice(i, 1);
                                        break;
                                    }
                                }
                                if ($scope.GE.receipt.ExpenseReportReceiptItemList.length == 0) {
                                    $scope.GE.receipt.ExpenseReportReceiptItemList = null;
                                }

                                var oSumOriginalAmountDetailList = 0;
                                var oSumLocalAmountDetailList = 0;
                                if ($scope.GE.receipt.ExpenseReportReceiptItemList != null) {
                                    for (var i = 0; i < $scope.GE.receipt.ExpenseReportReceiptItemList.length; i++) {
                                        if ($scope.GE.receipt.ExpenseReportReceiptItemList[i].ExpenseReportReceiptItemDetailList != null) {
                                            for (var j = 0; j < $scope.GE.receipt.ExpenseReportReceiptItemList[i].ExpenseReportReceiptItemDetailList.length; j++) {
                                                var originalAmount = Number($scope.GE.receipt.ExpenseReportReceiptItemList[i].ExpenseReportReceiptItemDetailList[j].OriginalAmount);
                                                oSumOriginalAmountDetailList += isNaN(originalAmount) ? 0 : originalAmount;

                                                var localAmount = Number($scope.GE.receipt.ExpenseReportReceiptItemList[i].ExpenseReportReceiptItemDetailList[j].LocalAmount);
                                                oSumLocalAmountDetailList += isNaN(localAmount) ? 0 : $scope.MathRounding(localAmount);
                                            }
                                        }
                                    }
                                }
                                oSumOriginalAmountDetailList = $scope.MathRounding(oSumOriginalAmountDetailList);
                                oSumLocalAmountDetailList = $scope.MathRounding(oSumLocalAmountDetailList);
                                $scope.GE.receipt.OriginalTotalAmount = oSumOriginalAmountDetailList;
                                $scope.GE.receipt.LocalNoVATTotalAmount = oSumLocalAmountDetailList;
                                $scope.GE.receipt.VATBaseAmount = oSumLocalAmountDetailList;
                                // calculate vat
                                var VATBaseAmount = Number($scope.GE.receipt.VATBaseAmount);
                                VATBaseAmount = isNaN(VATBaseAmount) ? 0 : $scope.MathRounding(VATBaseAmount);
                                var VatPercent = Number($scope.GE.receipt.VATPercent);
                                VatPercent = isNaN(VatPercent) ? 0 : VatPercent;
                                $scope.GE.receipt.VATAmount = $scope.MathRounding(VATBaseAmount * VatPercent / 100);

                                // -- gen data for table
                                $scope.expenseItems_WithoutReceipt = $scope.getExpenseItems_WithoutReceipt();
                                // -- !gen data for table

                                console.log('oSumLocalAmountDetailList.', oSumLocalAmountDetailList);
                            }
                        }
                    };

                    $scope.GoToSubMain_AddGEReceiptItemFinish = function () {
                        // Nun loop start
                        for (var i = 0; i < $scope.expenseTypeGroup.length ; i++) {
                            if ($scope.expenseTypeGroup[i].ExpenseTypeGroupID.toString() == $scope.GE.receiptItem.ExpenseTypeGroupID.toString()) {
                                $scope.GE.receiptItem.ExpenseTypeGroupName = $scope.expenseTypeGroup[i].Name;
                                $scope.GE.receiptItem.ExpenseTypeGroupRemark = $scope.expenseTypeGroup[i].Remark;
                                break;
                            }
                        }
                        for (var j = 0; j < $scope.expenseType.length ; j++) {
                            if ($scope.expenseType[j].ExpenseTypeID.toString() == $scope.GE.receiptItem.ExpenseTypeID.toString()) {
                                $scope.GE.receiptItem.ExpenseTypeName = $scope.expenseType[j].Name;
                                break;
                            }
                        }
                        for (var k = 0; k < $scope.setVatTypeDefault.length ; k++) {
                            if ($scope.setVatTypeDefault[k].VATCode.toString() == $scope.GE.receipt.VATCode.toString()) {
                                $scope.GE.receipt.VATPercent = $scope.setVatTypeDefault[k].Percent;
                                break;
                            }
                        }
                        // Nun loop end

                        if (!$scope.GE.receiptItemEdit) {
                            // finish from add
                            if ($scope.GE.receipt.ExpenseReportReceiptItemList == null) {
                                $scope.GE.receipt.ExpenseReportReceiptItemList = [];
                            }
                            $scope.GE.receipt.ExpenseReportReceiptItemList.push($scope.GE.receiptItem);
                        } else {
                            // finish from edit
                            for (var i = 0; i < $scope.GE.receipt.ExpenseReportReceiptItemList.length; i++) {
                                if ($scope.GE.receipt.ExpenseReportReceiptItemList[i] == $scope.GE.receiptItemEditObj) {
                                    $scope.GE.receipt.ExpenseReportReceiptItemList[i] = $scope.GE.receiptItem;
                                    break;
                                }
                            }
                        }

                        var oSumOriginalAmountDetailList = 0;
                        var oSumLocalAmountDetailList = 0;
                        if ($scope.GE.receipt.ExpenseReportReceiptItemList != null) {
                            for (var i = 0; i < $scope.GE.receipt.ExpenseReportReceiptItemList.length; i++) {
                                if ($scope.GE.receipt.ExpenseReportReceiptItemList[i].ExpenseReportReceiptItemDetailList != null) {
                                    for (var j = 0; j < $scope.GE.receipt.ExpenseReportReceiptItemList[i].ExpenseReportReceiptItemDetailList.length; j++) {
                                        var originalAmount = Number($scope.GE.receipt.ExpenseReportReceiptItemList[i].ExpenseReportReceiptItemDetailList[j].OriginalAmount);
                                        oSumOriginalAmountDetailList += isNaN(originalAmount) ? 0 : originalAmount;

                                        var localAmount = $scope.multiplyExchangeRate(Number($scope.GE.receipt.ExpenseReportReceiptItemList[i].ExpenseReportReceiptItemDetailList[j].OriginalAmount), $scope.GE.receipt.ExchangeRate, $scope.GE.receipt.FromCurrencyRatio, $scope.GE.receipt.ToCurrencyRatio);
                                        oSumLocalAmountDetailList += isNaN(localAmount) ? 0 : $scope.MathRounding(localAmount);
                                    }
                                }
                            }
                        }
                        oSumOriginalAmountDetailList = $scope.MathRounding(oSumOriginalAmountDetailList);
                        oSumLocalAmountDetailList = $scope.MathRounding(oSumLocalAmountDetailList);
                        $scope.GE.receipt.OriginalTotalAmount = oSumOriginalAmountDetailList;
                        $scope.GE.receipt.LocalNoVATTotalAmount = oSumLocalAmountDetailList;
                        $scope.GE.receipt.VATBaseAmount = oSumLocalAmountDetailList;
                        /* fix no receipt */
                        $scope.GE.receipt.LocalTotalAmount = oSumLocalAmountDetailList;
                        /* !fix no receipt */

                        console.log('oSumLocalAmountDetailList.', oSumLocalAmountDetailList);

                        var VATBaseAmount = Number($scope.GE.receipt.VATBaseAmount);
                        VATBaseAmount = isNaN(VATBaseAmount) ? 0 : $scope.MathRounding(VATBaseAmount);
                        var VatPercent = Number($scope.GE.receipt.VATPercent);
                        VatPercent = isNaN(VatPercent) ? 0 : VatPercent;
                        $scope.GE.receipt.VATAmount = $scope.MathRounding(VATBaseAmount * VatPercent / 100);

                        $scope.GE.receiptItemEditObj = null;
                        $scope.GE.receiptItem = null;
                        $scope.GE.receiptItemFormData.Amount = 0;

                        $scope.wizardFormFlag.isAddGEReceipt = true;
                        $scope.wizardFormFlag.isAddGEReceiptItem = false;
                        $scope.wizardFormFlag.isAddGEReceiptItemDetail = false;
                        console.log('Recipt Add Receipt Item Finish.', $scope.GE.receipt);
                    };

                    $scope.GoToSubMain_AddGEReceiptItemCancel = function () {
                        var confirm = $mdDialog.confirm()
                            .title($scope.Text['SYSTEM']['CONFIRM_REDIRECT_TITLE'])
                            .textContent($scope.Text['SYSTEM']['CONFIRM_CANCEL'])
                            .ok('OK')
                            .cancel('Cancel');
                        $mdDialog.show(confirm).then(function (result) {

                            $scope.GE.receiptItem = null;
                            $scope.GE.receiptItemFormData.Amount = 0;
                            $scope.wizardFormFlag.isAddGEReceipt = true;
                            $scope.wizardFormFlag.isAddGEReceiptItem = false;
                            $scope.wizardFormFlag.isAddGEReceiptItemDetail = false;
                            // clear form data


                            /* skip to cancel receipt */
                            $scope.GoToSubMain_AddGEReceiptCancel();


                        }, function () {
                            event.preventDefault();
                        });
                    };

                    $scope.GoToSubMain_AddGEReceiptItemDetail = function (receiptItemDetail) {
                        $scope.wizardFormFlag.isAddGEReceipt = false;
                        $scope.wizardFormFlag.isAddGEReceiptItem = false;
                        $scope.wizardFormFlag.isAddGEReceiptItemDetail = true;
                        if (angular.isUndefined(receiptItemDetail)) {
                            // new
                            var ObjReceiptItemDetail = $scope.model.data.ExpenseReportReceiptItemDetail[0];
                            $scope.GE.receiptItemDetailEdit = false;
                            $scope.GE.receiptItemDetail = angular.copy(ObjReceiptItemDetail);
                            $scope.GE.receiptItemDetail.RequestNo = $scope.document.RequestNo;
                            $scope.GE.receiptItemDetail.ReceiptID = $scope.GE.receipt.ReceiptID;
                            $scope.GE.receiptItemDetail.ItemID = $scope.GE.receiptItem.ItemID;

                            // set default CostCenter, IO
                            if (angular.isUndefined($scope.DefaultCostCenterIOByGroup[$scope.GE.receiptItem.ExpenseTypeGroupID])) {
                                $scope.GE.receiptItemDetail.CostCenter = $scope.DefaultCostCenterIO.CostCenter;
                                $scope.GE.receiptItemDetail.IsAlternativeCostCenter = $scope.DefaultCostCenterIO.IsAlternativeCostCenter;
                                $scope.GE.receiptItemDetail.AlternativeIOOrg = $scope.DefaultCostCenterIO.OrgUnitID;
                                $scope.GE.receiptItemDetail.IsAlternativeIOOrg = $scope.DefaultCostCenterIO.IsAlternativeIOOrg;
                                $scope.GE.receiptItemDetail.IO = $scope.DefaultCostCenterIO.IO;
                            } else {
                                var objDefault = $scope.DefaultCostCenterIOByGroup[$scope.GE.receiptItem.ExpenseTypeGroupID];
                                $scope.GE.receiptItemDetail.CostCenter = objDefault.CostCenter;
                                $scope.GE.receiptItemDetail.IsAlternativeCostCenter = objDefault.IsAlternativeCostCenter;
                                if (objDefault.IsAlternativeIOOrg && objDefault.OrgUnitID) {
                                    $scope.GE.receiptItemDetail.AlternativeIOOrg = objDefault.OrgUnitID;
                                } else {
                                    $scope.GE.receiptItemDetail.AlternativeIOOrg = $scope.DefaultCostCenterIO.OrgUnitID;
                                }
                                $scope.GE.receiptItemDetail.IsAlternativeIOOrg = objDefault.IsAlternativeIOOrg;
                                $scope.GE.receiptItemDetail.IO = objDefault.IO;
                            }
                            //if ($scope.GE.receiptItemDetail.IsAlternativeIOOrg) {
                                // get default IO by Org
                                $scope.getIOByOrganization($scope.GE.receiptItemDetail.AlternativeIOOrg);
                            //} else {
                            //    // get default IO by CostCenter
                            //    $scope.getIOByCostCenter($scope.GE.receiptItemDetail.CostCenter);
                            //}
			    
			                if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                                // none, average
			                } else {
			    	            // exactly
				                if ($scope.projectCodes.length == 1) {
                                    $scope.GE.receiptItemDetail.ProjectCode = $scope.projectCodes[0].ProjectCode;
                                }
			                }

                            console.log('ReciptItemDetail New.', angular.copy($scope.GE.receiptItemDetail));
                        } else {
                            // edit
                            $scope.GE.receiptItemDetailEdit = true;
                            $scope.GE.receiptItemDetailEditObj = receiptItemDetail;
                            $scope.GE.receiptItemDetail = angular.copy(receiptItemDetail);

                            // set default CostCenter, IO
                            //if ($scope.GE.receiptItemDetail.IsAlternativeIOOrg) {
                                $scope.getIOByOrganization($scope.GE.receiptItemDetail.AlternativeIOOrg);
                            //} else {
                            //    $scope.getIOByCostCenter($scope.GE.receiptItemDetail.CostCenter);
                            //}

                            console.log('ReciptItemDetail Edit.', $scope.GE.receiptItemDetail);
                        }

                        $timeout(function () {
                            document.getElementById('top-anchor2').scrollIntoView();
                        }, 100);
                    };

                    $scope.GoToSubMain_DeleteGEReceiptItemDetail = function (receiptItemDetail) {
                        if (angular.isDefined(receiptItemDetail)) {
                            if (confirm($scope.Text['SYSTEM']['CONFIRM_DELETE'])) {
                                // delete receiptItemDetail
                                for (var i = 0; i < $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList.length; i++) {
                                    if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[i] == receiptItemDetail) {
                                        $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList.splice(i, 1);
                                        break;
                                    }
                                }
                                if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList.length == 0) {
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList = null;
                                }
                            }
                        }
                    };

                    $scope.GoToSubMain_AddGEReceiptItemDetailFinish = function () {
                        console.log('$scope.GE.receiptItem.', $scope.GE.receiptItem);
                        $scope.calculateAmountReceiptItem();
                        if (!$scope.GE.receiptItemDetailEdit) {
                            // finish from add
                            if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList == null) {
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList = [];
                            }
                            $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList.push($scope.GE.receiptItemDetail);
                        } else {
                            // finish from edit
                            for (var i = 0; i < $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList.length; i++) {
                                if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[i] == $scope.GE.receiptItemDetailEditObj) {
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[i] = $scope.GE.receiptItemDetail;
                                    break;
                                }
                            }
                        }


                        $scope.GE.receiptItemDetailEditObj = null;
                        $scope.GE.receiptItemDetail = null;
                        // calculate receiptItem amount
                        $scope.calculateAmountReceiptItem();
                        // clear form data

                        $scope.wizardFormFlag.isAddGEReceipt = false;
                        $scope.wizardFormFlag.isAddGEReceiptItem = true;
                        $scope.wizardFormFlag.isAddGEReceiptItemDetail = false;
                        console.log('Recipt Add Receipt Item Detail Finish.', $scope.GE.receiptItem);
                    };

                    $scope.GoToSubMain_AddGEReceiptItemDetailCancel = function () {
                        if (confirm($scope.Text['SYSTEM']['CONFIRM_CANCEL'])) {
                            $scope.GE.receiptItemDetail = null;
                            $scope.wizardFormFlag.isAddGEReceipt = false;
                            $scope.wizardFormFlag.isAddGEReceiptItem = true;
                            $scope.wizardFormFlag.isAddGEReceiptItemDetail = false;
                            // clear form data

                            $window.scrollTo(0, 0);
                        }
                        
                    };

                    /* ====== !wizard form ====== */

                    $scope.getTemplate = function () {
                        return 'views/hr/tr/data/panel/' + $scope.panelName + '.html?t=' + $scope.runtime;
                        //$scope.panelName = 'pnlMileage'; return 'views/hr/tr/data/panel/pnlMileage.html?t=' + $scope.runtime;
                        //$scope.panelName = 'pnlTransCommutation'; return 'views/hr/tr/data/panel/pnlTransCommutation.html?t=' + $scope.runtime;
                    };

                    var clearPanelVariable = function () {
                        console.log('clear panel.');
                        if (angular.isDefined($scope.GE.receiptItem) && $scope.GE.receiptItem != null) {
                            $scope.GE.receiptItem.Str1 = "";
                            $scope.GE.receiptItem.Str2 = "";
                            $scope.GE.receiptItem.Str3 = "";
                            $scope.GE.receiptItem.Str4 = "";
                            $scope.GE.receiptItem.Str5 = "";
                            $scope.GE.receiptItem.Bit1 = false;
                            $scope.GE.receiptItem.Bit2 = false;
                            $scope.GE.receiptItem.Bit3 = false;
                            $scope.GE.receiptItem.Bit4 = false;
                            $scope.GE.receiptItem.Bit5 = false;
                            $scope.GE.receiptItem.Dec1 = 0;
                            $scope.GE.receiptItem.Dec2 = 0;
                            $scope.GE.receiptItem.Dec3 = 0;
                            $scope.GE.receiptItem.Dec4 = 0;
                            $scope.GE.receiptItem.Dec5 = 0;
                        }
                    };

                    $scope.expenseTypeGroupIDChange = function () {
                        clearPanelVariable();
                        if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                            // none, average
                            if (angular.isUndefined($scope.DefaultCostCenterIOByGroup[$scope.GE.receiptItem.ExpenseTypeGroupID])) {
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].CostCenter = $scope.DefaultCostCenterIO.CostCenter;
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].IsAlternativeCostCenter = $scope.DefaultCostCenterIO.IsAlternativeCostCenter;
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].AlternativeIOOrg = $scope.DefaultCostCenterIO.OrgUnitID;
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].IsAlternativeIOOrg = $scope.DefaultCostCenterIO.IsAlternativeIOOrg;
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].IO = $scope.DefaultCostCenterIO.IO;
                            } else {
                                var objDefault = $scope.DefaultCostCenterIOByGroup[$scope.GE.receiptItem.ExpenseTypeGroupID];
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].CostCenter = objDefault.CostCenter;
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].IsAlternativeCostCenter = objDefault.IsAlternativeCostCenter;
                                if (objDefault.IsAlternativeIOOrg && objDefault.OrgUnitID) {
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].AlternativeIOOrg = objDefault.OrgUnitID;
                                } else {
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].AlternativeIOOrg = $scope.DefaultCostCenterIO.OrgUnitID;
                                }
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].IsAlternativeIOOrg = objDefault.IsAlternativeIOOrg;
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].IO = objDefault.IO;
                            }
                            //if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].IsAlternativeIOOrg) {
                                // get default IO by Org
                                $scope.getIOByOrganization($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].AlternativeIOOrg);
                            //} else {
                            //    // get default IO by CostCenter
                            //    $scope.getIOByCostCenter($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].CostCenter);
                            //}
                        }
                        $scope.getExpenseTypeByGroupID($scope.GE.receiptItem.ExpenseTypeGroupID, true);
                    };

                    $scope.expenseTypeChange = function () {
                        clearPanelVariable();
                        $scope.getPanelNameByExpenseType($scope.GE.receiptItem.ExpenseTypeID);
                    };

                    //$scope.parseDateStringToObj = function (dateString) {
                    //    if (typeof (dateString) != 'undefined' && dateString != '') {
                    //        var from = dateString.split("/");
                    //        var dateObj = new Date(from[2], from[1] - 1, from[0]); // (year,month,date)
                    //        return dateObj;
                    //    }
                    //};

                    $scope.isReadonlySumAmount = function (panelName) {
                        if (panelName == 'pnlMileage' || panelName == 'pnlTransCommutation') {
                            return true;
                        }
                        return false;
                    };

                    $scope.changeOriginalAmount = function (objDetail, originalValue) {
                        if (originalValue) {
                            var exchangeRate = Number($scope.GE.receipt.ExchangeRate);
                            objDetail.OriginalAmount = $scope.MathRounding(Number(originalValue));
                            if (!isNaN(exchangeRate)) {
                                objDetail.LocalAmount = $scope.multiplyExchangeRate(objDetail.OriginalAmount, exchangeRate, $scope.GE.receipt.FromCurrencyRatio, $scope.GE.receipt.ToCurrencyRatio);
                            }
                        }
                    };

                    $scope.changeReceiptCurrency = function (objCurrency) {
                        console.log('objCurrency.', objCurrency);
                        $scope.GE.receipt.ExchangeRate = Number(objCurrency.ExchangeRate);
                        $scope.GE.receipt.OriginalCurrencyCode = objCurrency.FromCurrency;
                        $scope.GE.receipt.ExchangeDate = objCurrency.EffectiveDate;
                        $scope.GE.receipt.ExchangeTypeID = objCurrency.ExchangeTypeID;
                        $scope.GE.receipt.FromCurrencyRatio = objCurrency.RatioFromCurrencyUnit;
                        $scope.GE.receipt.ToCurrencyRatio = objCurrency.RatioToCurrencyUnit;
                        var sumLocalAmount = 0;
                        if (angular.isDefined($scope.GE.receiptItem) && $scope.GE.receiptItem != null) {
                            var item = $scope.GE.receiptItem;
                            if (angular.isDefined(item.ExpenseReportReceiptItemDetailList) && item.ExpenseReportReceiptItemDetailList != null) {
                                for (var k = 0; k < item.ExpenseReportReceiptItemDetailList.length; k++) {
                                    item.ExpenseReportReceiptItemDetailList[k].LocalAmount = $scope.multiplyExchangeRate(item.ExpenseReportReceiptItemDetailList[k].OriginalAmount, $scope.GE.receipt.ExchangeRate, $scope.GE.receipt.FromCurrencyRatio, $scope.GE.receipt.ToCurrencyRatio);
                                    sumLocalAmount += $scope.MathRounding(item.ExpenseReportReceiptItemDetailList[k].LocalAmount);
                                    if (item.ExpenseReportReceiptItemDetailList[k].OriginalAmountInRight != 9999999999) {
                                        item.ExpenseReportReceiptItemDetailList[k].LocalAmountInRight = $scope.multiplyExchangeRate(item.ExpenseReportReceiptItemDetailList[k].OriginalAmountInRight, $scope.GE.receipt.ExchangeRate, $scope.GE.receipt.FromCurrencyRatio, $scope.GE.receipt.ToCurrencyRatio);
                                    }
                                }
                            }
                        }
                        $scope.GE.receipt.LocalNoVATTotalAmount = sumLocalAmount;
                        $scope.GE.receipt.VATBaseAmount = sumLocalAmount;
                        // calculate vat
                        var VATBaseAmount = Number($scope.GE.receipt.VATBaseAmount);
                        VATBaseAmount = isNaN(VATBaseAmount) ? 0 : $scope.MathRounding(VATBaseAmount);
                        var VatPercent = Number($scope.GE.receipt.VATPercent);
                        VatPercent = isNaN(VatPercent) ? 0 : VatPercent;
                        $scope.GE.receipt.VATAmount = $scope.MathRounding(VATBaseAmount * VatPercent / 100);

                        $scope.expenseItems_WithoutReceipt = $scope.getExpenseItems_WithoutReceipt();
                    };

                    /* panel : pnlAccommodation */
                    $scope.setSelectedBeginDate = function (selectedDate) {
                        $scope.GE.receiptItem.Date1 = $filter('date')(selectedDate, 'yyyy-MM-dd');
                        if ($scope.GE.receiptItem.Date1 > $scope.GE.receiptItem.Date2) {
                            $scope.GE.receiptItem.Date2 = $scope.GE.receiptItem.Date1;
                        }
                    };

                    $scope.setSelectedEndDate = function (selectedDate) {
                        $scope.GE.receiptItem.Date2 = $filter('date')(selectedDate, 'yyyy-MM-dd');
                    };
                    /* !panel : pnlAccommodation */



                    //Nun Add 25082016
                    /* panel : pnlVehicleRent */
                    $scope.changeBeginDate = function (selectedDate) {
                        $scope.GE.receiptItem.Date1 = $filter('date')(selectedDate, 'yyyy-MM-dd');
                        if ($scope.GE.receiptItem.Date1 > $scope.GE.receiptItem.Date2) {
                            $scope.GE.receiptItem.Date2 = $scope.GE.receiptItem.Date1;
                        }
                    };
                    $scope.changeEndDate = function (selectedDate) {
                        $scope.GE.receiptItem.Date2 = $filter('date')(selectedDate, 'yyyy-MM-dd');
                    };

                    $scope.calDiffDateDuration = function () {
                        if ($scope.GE.receiptItem.Date1 <= $scope.GE.receiptItem.Date2) {
                            var amountInRight = 0;
                            if ($scope.amountInRight != 9999999999) {
                                var dateStart = moment($scope.GE.receiptItem.Date1);
                                var dateEnd = moment($scope.GE.receiptItem.Date2);

                                var duration = moment.duration(dateEnd.startOf('day').diff(dateStart.startOf('day')));
                                var diff = duration.asDays();

                                if (diff >= 0) {
                                    var days = diff + 1;
                                    amountInRight = $scope.amountInRight * days;
                                }
                            } else {
                                amountInRight = 9999999999;
                            }

                            $scope.GE.receiptItemFormData.AmountInRight = amountInRight;
                            var exchangeRate = Number($scope.GE.receipt.ExchangeRate);
                            if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                                // none, average

                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].OriginalAmount = $scope.GE.receiptItemFormData.Amount;
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].LocalAmount = $scope.multiplyExchangeRate($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].OriginalAmount, exchangeRate, $scope.GE.receipt.FromCurrencyRatio, $scope.GE.receipt.ToCurrencyRatio);
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].OriginalAmountInRight = amountInRight;
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].LocalAmountInRight = $scope.multiplyExchangeRate(amountInRight, exchangeRate, $scope.GE.receipt.FromCurrencyRatio, $scope.GE.receipt.ToCurrencyRatio);
                            }
                            else if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_EXACTLY) {
                                // exactly
                                console.log('oNun1', 'step1');
                                console.log('oNunList1', $scope.GE.receiptItemFormData.Amount);

                                var sumAmount = 0;
                                if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList != null) {
                                    for (var i = 0; i < $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList.length; i++) {
                                        var amt = Number($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[i].OriginalAmount);
                                        sumAmount += isNaN(amt) ? 0 : amt;
                                    }
                                }
                                console.log('oNun2', sumAmount);
                                if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList != null) {
                                    for (var j = 0; j < $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList.length; j++) {
                                        var amount = Number($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[j].OriginalAmount);
                                        $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[j].OriginalAmountInRight = (amountInRight * amount) / sumAmount;
                                        $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[j].LocalAmountInRight = $scope.multiplyExchangeRate(((amountInRight * amount) / sumAmount), exchangeRate, $scope.GE.receipt.FromCurrencyRatio, $scope.GE.receipt.ToCurrencyRatio);
                                    }
                                }
                                console.log('oNun3', (amountInRight * amount) / sumAmount);
                            }
                        }
                    };


                    /* panel : pnlTransRegistration */
                    $scope.GetCarRegistration();

                    $scope.onChangeCarUsage = function (selectedCarUsageCompany) {
                        $scope.GE.receiptItem.Bit2 = !selectedCarUsageCompany;
                        //Nun Add 29052017
                        $scope.GE.receiptItem.Str1 = '';
                        //
                        $scope.GE.receiptItem.Str2 = '';
                        $scope.OnChangeCarRegistration($scope.GE.receiptItem.Str1);
                    };

                    $scope.OnChangeCarRegistration = function (oCarRegistrationDetail) {
                        var oResultValue = $filter('filter')($scope.CarRegistrationList, { CarRegistrationDetail: oCarRegistrationDetail });
                        var oRequestSearch = '0';
                        if (oResultValue && oResultValue.length > 0 && $scope.GE.receiptItem.Bit1) {
                            oRequestSearch = oResultValue[0].CarRegistrationDetail;
                        }
                        var URL = CONFIG.SERVER + 'HRTR/GetCarTypeMappingCarRegistration';
                        var oRequestParameter = { InputParameter: { 'CARREGISTRATIONDETAIL': oRequestSearch, 'ISPERONALCAR': !$scope.GE.receiptItem.Bit1 }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            $scope.carTypes = response.data;


                            //Nun Add 29052017
                            if ($scope.carTypes != null && $scope.carTypes.length > 0) {
                                $scope.ddlDefaultCarType = $scope.carTypes[0].Description;
                                if ($scope.GE.receiptItem.Bit1) {
                                    $scope.GE.receiptItem.Str2 = $scope.ddlDefaultCarType;
                                }
                            }
                            //


                            if (!$scope.carTypes || $scope.carTypes.length == 0) {
                                $mdDialog.show(
                                 $mdDialog.alert()
                                   .clickOutsideToClose(false)
                                   .title($scope.Text['SYSTEM']['INFORMATION'])
                                   .textContent(Text['SYSTEM']['CARREGISTERNOTCONFIG'])
                                   .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                               );
                            }

                        }, function errorCallback(response) {
                            $mdDialog.show(
                                $mdDialog.alert()
                                .clickOutsideToClose(false)
                                .title($scope.Text['SYSTEM']['INFORMATION'])
                                .textContent(response.data)
                                .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                            );
                        });
                    }
                    /* !panel : pnlTransRegistration */

                    /* panel : pnlMileage */
                    //$scope.setSelectedDepartDate = function (selectedDate) {
                    //    $scope.GE.receiptItem.Date1 = $filter('date')(selectedDate, 'yyyy-MM-dd');
                    //};

                    $scope.onChangeDistance = function () {
                        if ($scope.GE.receiptItem != null) {
                            var distance = $scope.GE.receiptItem.Dec1;
                            $scope.GE.receiptItemFormData.Amount = $scope.MathRounding(Number(distance) * $scope.fuelRate);
                            $scope.GE.receiptItemFormData.AmountInRight = $scope.MathRounding(Number(distance) * $scope.mileageNoCalIncomeRate);
                            if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                                // none, average
                                var exchangeRate = Number($scope.GE.receipt.ExchangeRate);
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].OriginalAmount = $scope.GE.receiptItemFormData.Amount;
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].LocalAmount = $scope.multiplyExchangeRate($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].OriginalAmount, exchangeRate, $scope.GE.receipt.FromCurrencyRatio, $scope.GE.receipt.ToCurrencyRatio);
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].OriginalAmountInRight = $scope.GE.receiptItemFormData.AmountInRight;
                                if (Number($scope.GE.receiptItemFormData.AmountInRight) < 9999999999) {
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].LocalAmountInRight = $scope.multiplyExchangeRate($scope.GE.receiptItemFormData.AmountInRight, exchangeRate, $scope.GE.receipt.FromCurrencyRatio, $scope.GE.receipt.ToCurrencyRatio);
                                }
                            }
                        }
                    };

                    /* !panel : pnlMileage */

                    /* panel : pnlTransCommutation */
                    $scope.onChangeTravelRound = function (fieldToSetFlag) {

                        if (fieldToSetFlag == 1) {
                            if ($scope.GE.receiptItem.Bit2 == true || $scope.GE.receiptItem.Bit3 == true) {
                                $scope.GE.receiptItemFormData.Amount = $scope.MathRounding($scope.GE.receiptItemFormData.Amount * 2);
                                $scope.FlatRateNoCalIncome *= 2;
                            }
                        } else if (fieldToSetFlag == 2 || fieldToSetFlag == 3) {
                            if ($scope.GE.receiptItem.Bit1 == true) {
                                $scope.GE.receiptItemFormData.Amount = $scope.MathRounding($scope.GE.receiptItemFormData.Amount / 2);
                                $scope.FlatRateNoCalIncome /= 2;
                            }
                        }

                        $scope.GE.receiptItem.Bit1 = false;
                        $scope.GE.receiptItem.Bit2 = false;
                        $scope.GE.receiptItem.Bit3 = false;
                        if (fieldToSetFlag == 1) {
                            $scope.GE.receiptItem.Bit1 = true;
                        } else if (fieldToSetFlag == 2) {
                            $scope.GE.receiptItem.Bit2 = true;
                        } else if (fieldToSetFlag == 3) {
                            $scope.GE.receiptItem.Bit3 = true;
                        }
                    };

                    $scope.FlatRateNoCalIncome = 0;
                    var calculateAmountFromLocation = function (start, end) {
                        var exchangeRate = Number($scope.GE.receipt.ExchangeRate);
                        if (start != '' && end != '' && start != end) {
                            var promise = GetFlatRateByLocation(start, end);
                            promise.then(function successCallback(response) {

                                //Old code
                                //var amount = response.data[0].Rate;                              

                                var amount = 0;
                                if (response.data && response.data.length > 0) {
                                    amount = response.data[0].Rate;
                                    $scope.FlatRateNoCalIncome = response.data[0].NoCalIncome;
                                    //if ($scope.GE.receiptItem.Bit2 == true || $scope.GE.receiptItem.Bit3 == true) {
                                    //    amount = amount / 2;
                                    //}
                                    if ($scope.GE.receiptItem.Bit1 == true) {
                                        amount *= 2;
                                        $scope.FlatRateNoCalIncome *= 2;
                                    }

                                    $scope.GE.receiptItemFormData.Amount = $scope.MathRounding(Number(amount));

                                    if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                                        // none, average
                                        $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].OriginalAmount = $scope.GE.receiptItemFormData.Amount;
                                        $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].LocalAmount = $scope.multiplyExchangeRate($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].OriginalAmount, exchangeRate, $scope.GE.receipt.FromCurrencyRatio, $scope.GE.receipt.ToCurrencyRatio);
                                    }
                                    console.log('calculateAmountFromLocation.', response.data[0].Rate);

                                } else {
                                    $scope.GE.receiptItemFormData.Amount = 0;

                                    if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                                        // none, average
                                        $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].OriginalAmount = 0;
                                        $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].LocalAmount = 0;
                                        $scope.FlatRateNoCalIncome = 0;
                                    }

                                    $scope.p_message($scope.Text['EXPENSE']['NOFLATRATE'],'w');
                                }

                            }, function errorCallback(response) {
                                console.log('error calculateAmountFromLocation.', response);
                            });
                        } else {
                            $scope.GE.receiptItemFormData.Amount = 0;
                            if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                                // none, average
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].OriginalAmount = $scope.GE.receiptItemFormData.Amount;
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].LocalAmount = $scope.multiplyExchangeRate($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].OriginalAmount, exchangeRate, $scope.GE.receipt.FromCurrencyRatio, $scope.GE.receipt.ToCurrencyRatio);
                            } 
                        }
                    }
                    $scope.onSelectStartLocation = function (selected) {
                        // filter out EndLocation
                        $scope.GE.receiptItemDetailFormData.FlatLocationFilter.Key = '!' + selected.Key;
                        if ($scope.GE.receiptItem.Str2 == selected.Key) {
                            $scope.GE.receiptItem.Str2 = '';
                        }
                        // calculate 
                        calculateAmountFromLocation($scope.GE.receiptItem.Str1, $scope.GE.receiptItem.Str2);
                    };
                    $scope.onSelectEndLocation = function (selected) {
                        calculateAmountFromLocation($scope.GE.receiptItem.Str1, $scope.GE.receiptItem.Str2);
                    };
                    /* !panel : pnlTransCommutation */

                    $scope.onClickReceiptDate = function () {
                        if ($scope.GE.receipt.ReceiptDate) {
                            // testcomment
                            //$scope.directiveFn($scope.GE.receipt.ReceiptDate);
                        } else {
                            // testcomment
                            //$scope.directiveFn($scope.savedLimitMinDate);
                        }
                        
                        document.getElementById('expense-no-receipt-date').click();
                       
                    };

                    $scope.AllowUploadFile = function (fileName) {
                        return CONFIG.FILE_SETTING.ALLOW_FILE.test(fileName);
                    }
                    $scope.AllowUploadFileType = function (fileName) {
                        return CONFIG.FILE_SETTING.ALLOW_FILETYPE.indexOf(fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase()) >= 0;
                    }
                    $scope.AllowFileNameLength = function (fileNameLength) {
                        return CONFIG.FILE_SETTING.ALLOW_FILENAME_LENGTH >= fileNameLength;
                    }
                    $scope.AllowFileSize = function (fileSize) {
                        return CONFIG.FILE_SETTING.ALLOW_FILESIZE >= fileSize;
                    }
                    $scope.AllowFileZeroSize = function (fileSize) {
                        return fileSize > 0;
                    }
                    $scope.expenseDate;

                    $scope.changeDate = function () {
                        $scope.expenseDate = $('#CalendarExpenseDate[name="datefilter"]').val()
                        $scope.GE.receipt.ReceiptDate = $filter('date')(moment($scope.expenseDate, "DD/MM/YYYY").toDate(), 'yyyy-MM-ddT00:00:00');

                    }
                }],
                link: function (scope, element, attrs, controller) {
                    //alert('2. link work.');
                    //alert('2.1 ' + scope.Textcategory);
                    //alert('2.2 ' + scope.controllerPassToLink);
                    //alert('4. Controller found: ' + (controller !== undefined) + ' name is ' + controller);
                    //console.log('cccccccccccccccccccccccc', controller);
                }
            };
        })
        .controller('ReceiptItemGuestAttachmentController', ['$scope', '$http', '$routeParams', '$location', '$filter', '$window', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, $window, CONFIG, $mdDialog) {
            var employeeData = employeeData;
            var InitialValue = function () {
                $scope.formReceiptAttachment = { file: null };
                $scope.objGuestFile = {
                    RequestNo: '',
                    ReceiptID: 0,
                    ItemID: 0,
                    FileID: 0,
                    FilePath: '',
                    FileName: '',
                    FileSize: 0
                };
            };
            InitialValue();
            $scope.p_message = function (message, type) {
                if (type == 'w') {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title('Warning')
                            .textContent(message)
                            .ariaLabel('Warning')
                            .ok('OK')
                    );
                } else {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title($scope.Text['SYSTEM']['INFORMATION'])
                            .textContent(message)
                            .ariaLabel($scope.Text['SYSTEM']['INFORMATION'])
                            .ok('OK')
                    );
                }
            }
            $scope.onAfterValidateFunction = function (event, fileList) {
                //console.log('file validated.', fileList);
                //base64:"VVNFIFtXb3JrZmxvd1JGU10N...EVSIEJZIFNPLk5hbWUNCg=="
                //filename:"stored check text in stored.txt"
                //filesize:811
                //filetype: "text/plain"
                var newFile = angular.copy($scope.objGuestFile);
                newFile.RequestNo = $scope.GE.receipt.RequestNo;
                newFile.ReceiptID = $scope.GE.receipt.ReceiptID;
                newFile.ItemID = $scope.GE.receiptItem.ItemID;
                newFile.FileID = -1;
                newFile.FilePath = '';
                newFile.FileName = fileList[0].filename;
                newFile.FileType = fileList[0].filetype;
                newFile.FileContent = fileList[0].base64;
                newFile.FileSize = fileList[0].filesize;
                newFile.IsDelete = false;
                if ($scope.AllowUploadFile(newFile.FileName))
                {
                    $mdDialog.show(
                       $mdDialog.alert()
                       .clickOutsideToClose(false)
                       .title($scope.Text['SYSTEM']['WARNING'])
                       .textContent($scope.Text['SYSTEM']['VALIDATE_FILENAME'])
                       .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else if (!$scope.AllowUploadFileType(newFile.FileName))
                {
                    $mdDialog.show(
                        $mdDialog.alert()
                        .clickOutsideToClose(false)
                        .title($scope.Text['SYSTEM']['WARNING'])
                        .textContent($scope.Text['SYSTEM']['INVALIDFILETYPE'])
                        .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else if (!$scope.AllowFileNameLength(newFile.FileName.length))
                {
                    $mdDialog.show(
                        $mdDialog.alert()
                        .clickOutsideToClose(false)
                        .title($scope.Text['SYSTEM']['WARNING'])
                        .textContent($scope.Text['SYSTEM']['SAP_FILENAME_LENGTH_INVALID'])
                        .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else if (!$scope.AllowFileSize(newFile.FileSize)) {

                    $mdDialog.show(
                        $mdDialog.alert()
                        .clickOutsideToClose(false)
                        .title($scope.Text['SYSTEM']['WARNING'])
                        .textContent($scope.Text['SYSTEM']['INVALIDFILESIZE'])
                        .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else if (!$scope.AllowFileZeroSize(newFile.FileSize)) {
                    $mdDialog.show(
                        $mdDialog.alert()
                        .clickOutsideToClose(false)
                        .title($scope.Text['SYSTEM']['WARNING'])
                        .textContent($scope.Text['SYSTEM']['INVALIDFILESIZE2'])
                        .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                    );
                }
                else
                {
                    if (angular.isUndefined($scope.GE.receiptItem.ExpenseReportReceiptItemGuestFileList) || $scope.GE.receiptItem.ExpenseReportReceiptItemGuestFileList == null) {
                        $scope.GE.receiptItem.ExpenseReportReceiptItemGuestFileList = [];
                    }
                    $scope.GE.receiptItem.ExpenseReportReceiptItemGuestFileList.push(newFile);
                }
                newFile = null;
                $scope.formReceiptAttachment.file = null;
                angular.element("#expense-receipt-guest-file-input").val(null);
            };

            $scope.deleteFileAttach = function (rowIndex) {
                if (confirm($scope.Text['SYSTEM']['CONFIRM_DELETE'])) {
                    if ($scope.GE.receiptItem.ExpenseReportReceiptItemGuestFileList[rowIndex].FileID != -1) {
                        // delete server attach file (set flag delete)
                        $scope.GE.receiptItem.ExpenseReportReceiptItemGuestFileList[rowIndex].IsDelete = true;
                    } else {
                        // delete new attach file (remove from array)
                        $scope.GE.receiptItem.ExpenseReportReceiptItemGuestFileList.splice(rowIndex, 1);
                    }
                }
            };

            $scope.recoveryFileAttach = function (rowIndex) {
                $scope.GE.receiptItem.ExpenseReportReceiptItemGuestFileList[rowIndex].IsDelete = false;
                //$scope.GE.receipt.HasFileAttached = true;
            };

            $scope.getFileAttach = function (attachment) {
                //var URL = CONFIG.SERVER + 'workflow/GetFile';

                /* via proxy page */
                //var oRequestParameter = { InputParameter: { "RequestNo": attachment.RequestNo, 'ReceiptID': attachment.ReceiptID, "FileID": attachment.FileID } };
                //var MoDule = 'workflow/';
                //var Functional = 'GetFile';
                //var URL = CONFIG.SERVER + MoDule + Functional;
                //// Success
                //$http({
                //    method: 'POST',
                //    url: URL,
                //    data: oRequestParameter
                //}).then(function successCallback(response) {
                //    console.log('ReceiptAttachment getFileAttach.', response.data);
                //    if (typeof cordova != 'undefined') {
                //        cordova.InAppBrowser.open("data:application/octet-stream, " + escape(response.data), '_system', 'location=no');
                //    } else {
                //        window.open("data:application/octet-stream, " + escape(response.data));
                //    }
                //}, function errorCallback(response) {
                //    // Error
                //    console.log('error ReceiptAttachment getFileAttach.', response);
                //});
                /* !via proxy page */

                /* direct */
                if (angular.isDefined(attachment)) {
                    if (typeof cordova != 'undefined') {
                        //cordova.InAppBrowser.open("data:application/octet-stream, " + escape(response.data), '_system', 'location=no');
                    } else {
                        //window.open("data:application/octet-stream, " + escape(response.data));
                        //var path = CONFIG.SERVER + 'Client/files/' + attachment.RequestNo + '/' + ("00000" + attachment.ReceiptID.toString()).substr(-5,5) + '/' + attachment.FileName;
                        var path = CONFIG.SERVER + 'Client/files/' + attachment.FilePath + '/' + attachment.FileName;
                        $window.open(path);
                        //$window.open('http://localhost:15124/Client/index.html#/frmViewRequest/0013160000069-GE000/0013/8150/False/False');
                    }
                }
                /* !direct */
            };

            $scope.AllowUploadFile = function (fileName) {
                return CONFIG.FILE_SETTING.ALLOW_FILE.test(fileName);
            }
            $scope.AllowUploadFileType = function (fileName) {
                return CONFIG.FILE_SETTING.ALLOW_FILETYPE.indexOf(fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase()) >= 0;
            }
            $scope.AllowFileNameLength = function (fileNameLength) {
                return CONFIG.FILE_SETTING.ALLOW_FILENAME_LENGTH >= fileNameLength;
            }
            $scope.AllowFileSize = function (fileSize) {
                return CONFIG.FILE_SETTING.ALLOW_FILESIZE >= fileSize;
            }
            $scope.AllowFileZeroSize = function (fileSize) {
                return fileSize > 0;
            }
        }]);
})();