﻿(function () {
    "use strict";
    angular.module('ESSMobile')
        .directive('essExpenseViewHistoryBlock', function () {
            return {
                restrict: 'AE',
                scope: {
                    Text: '=text',
                    beginFormWizard: '&',
                    finishFormWizard: '&',
                    wizardFormControl: '=',
                    loader: '=',
                    requestNo: '=',
                    requestCompanyCode: '=',
                    keyMaster: '=',
                    isOwner: '=',
                    isDataOwner: '='
                },
                templateUrl: 'views/hr/tr/data/essExpenseViewHistoryBlock.html',
                controller: ['$scope', '$http', '$routeParams', '$location', '$filter', '$window', 'CONFIG', '$timeout', '$q', '$log', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, $window, CONFIG, $timeout, $q, $log, $mdDialog) {
                    
                    $scope.runtime = Date.now();
                    $scope.content = {};
                    $scope.ChildAction = {};
                    $scope.blockAction = {
                        isBLock: false,
                        blockMessage: ''
                    };
                    $scope.getFileFromPath = function (attachment) {
                        /* direct */
                        if (angular.isDefined(attachment)) {
                            if (typeof cordova != 'undefined') {

                            } else if (attachment.FilePath) {
                                var path = CONFIG.SERVER + 'Client/files/' + attachment.FilePath + '/' + attachment.FileName;
                                console.debug(path);
                                $window.open(path);
                            } else {
                                var path = CONFIG.SERVER + 'Client/files/' + attachment;
                                console.debug(path);
                                $window.open(path);

                            }
                        }
                        /* !direct */
                    }
                    $scope.getTextByCompany = function (oRequestor) {
                        //$scope.RequestorText = angular.copy($scope.Text);
                        var oRequestParameter = { InputParameter: {}, Requestor: oRequestor }
                        var URL = CONFIG.SERVER + 'workflow/GetTextDescriptionBySystem/';
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            // Success
                            if (response.data != null) {
                                $scope.RequestorText = response.data;
                            }
                            console.log('----------- TextDescription. -----------', angular.copy($scope.Text));
                        }, function errorCallback(response) {
                            // Error
                            $scope.RequestorText = angular.copy($scope.Text);
                            console.log('error MainController TextDescription.', response);
                        });
                    };
                    $scope.setBlockAction = function (isBLock, blockMessage) {
                        $scope.blockAction.isBLock = isBLock;
                        $scope.blockAction.blockMessage = blockMessage;
                    };
                    $scope.getRequestDocument = function () {
                        var URL = CONFIG.SERVER + 'workflow/GetRequestDocumentByRequestNo';
                  
                        var oRequestParameter = {
                            InputParameter: {
                                "RequestNo": $scope.requestNo,
                                "RequestCompanyCode": $scope.requestCompanyCode,
                                "KeyMaster": $scope.keyMaster,
                                "IsOwnerView": $scope.isOwner,
                                "IsDataOwnerView": $scope.isDataOwner
                            },
                            CurrentEmployee: getToken(CONFIG.USER)
                        };
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            // Success
                            $scope.document = response.data;
                            console.log('history document.', $scope.document);
                            $scope.loader.enable = false;
                        }, function errorCallback(response) {
                            // Error
                            console.log('error history document.', response);
                            $scope.document = null;
                            $scope.loader.enable = false;
                        });
                    };
                    $scope.getRequestDocument();

                    $scope.getRequestViewerTemplate = function (r) {
                   
                        if ($scope.document.Additional) {
                            var viewer = $scope.document.Viewer;
                            return 'views/' + viewer + '.html?t=' + r;
                        }
                       
                    };

                    $scope.zeroPaddingText = function (sourceText, digit) {
                        var str = '';
                        for (var i = 0; i < digit; i++) {
                            str += '0';
                        }
                        sourceText = str + sourceText.toString();
                        return sourceText.substr((-1 * digit), digit);
                    };

                }],
                link: function (scope, element, attrs, controller) {
                    
                }
            };
        });
})();