﻿(function () {
    "use strict";
    angular.module('ESSMobile')
        .directive('essExpenseReceiptEditor', function () {
            return {
                restrict: 'AE',
                scope: {
                    Text: '=text',
                    beginFormWizard: '&',
                    finishFormWizard: '&',
                    wizardFormControl: '=',
                    document: '=',
                    grouptagname: '@',
                    tagname: '@',
                    projectCodes: '=',
                    settings: '=',
                    masterData: '=',
                    isEditor: '=',
                    childSumData: '=',
                    moduleSetting: '=',
                    systemMasterData: '=',
                    receiptLimitDate: '='
                },
                templateUrl: 'views/hr/tr/data/essExpenseReceipt.html',
                controller: ['$scope', '$http', '$routeParams', '$location', '$filter', '$window', 'CONFIG', '$timeout', '$q', '$log', '$mdDialog', '$mdMenu', function ($scope, $http, $routeParams, $location, $filter, $window, CONFIG, $timeout, $q, $log, $mdDialog, $mdMenu) {
                    getProvince();
                    $scope.runtime = Date.now();
                    $scope.employeeData = getToken(CONFIG.USER);
                    var employeeData = getToken(CONFIG.USER);
                    // Map main table
                    var obj = $scope.document;
                    $scope.ExpenseReport = obj.Additional;


                  
                    $scope.p_message = function (message, type) {
                        if (type == 'w') {
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .clickOutsideToClose(true)
                                    .title('Warning')
                                    .textContent(message)
                                    .ariaLabel('Warning')
                                    .ok('OK')
                            );
                        } else {
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .clickOutsideToClose(true)
                                    .title($scope.Text['SYSTEM']['INFORMATION'])
                                    .textContent(message)
                                    .ariaLabel($scope.Text['SYSTEM']['INFORMATION'])
                                    .ok('OK')
                            );
                        }
                    }

                    $scope.isReceipt = true;
                    $scope.Textcategory = 'EXPENSE';

                    /* --- Variable --- */

                    $scope.model = {
                        ready: true,
                        data: $scope.systemMasterData.ExpenseModel
                    };

                    $scope.ddlDefault = {
                        ExpenseTypeGroup: 0,
                        ExpenseType: 0,
                        setVatTypeDefault: 0
                    };

                    $scope.ddlDefaultCarType = '';

                    $scope.showChangeAmountWarning = false;

                    // master
                    $scope.isUserRoleAccounting = false;
                    $scope.checkAccountingRole = function () {
                        var URL = CONFIG.SERVER + 'HRTR/IsUserRoleAccounting';
                        var oRequestParameter = { InputParameter: { "REQUESTNO": $scope.document.RequestNo, 'CompanyCode': $scope.document.Requestor.CompanyCode }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            $scope.isUserRoleAccounting = response.data;
                            console.log('IsAccountingRole.', response.data);
                        }, function errorCallback(response) {
                            console.log('error IsAccountingRole.', response);
                        });
                    };
                    $scope.checkAccountingRole();
                    $scope.projectForLookup = $scope.masterData.projectForLookup;
                    $scope.exchangeType = $scope.masterData.exchangeType;
                    $scope.ExchangeTypeReady = true;
                    $scope.employeeDataList = $scope.masterData.employeeDataList;
                    $scope.employeeDataList_Buffer = angular.copy($scope.employeeDataList);
                    $scope.carTypes = $scope.masterData.carTypes;
                    $scope.flatRateLocations = $scope.masterData.flatRateLocations;
                    $scope.objVendor = $scope.masterData.objVendor;
                    $scope.objVendorOnetime = $scope.masterData.objVendorOnetime;
                    $scope.objCountry = $scope.masterData.objCountry;
                    $scope.objCostcenterLookup = $scope.masterData.objCostcenterLookup;
                    $scope.objIOLookupObj = $scope.masterData.objIOLookupObj;
                    $scope.setVatTypeDefault = $scope.masterData.setVatTypeDefault;

                    if ($scope.setVatTypeDefault != null && $scope.setVatTypeDefault.length > 0) {
                        $scope.ddlDefault.VATCode = $scope.setVatTypeDefault[0].VATCode;
                        if (angular.isDefined($scope.GE) && angular.isDefined($scope.GE.receipt))
                        {
                            $scope.GE.receipt.VATCode = $scope.ddlDefault.VATCode;
                            $scope.onSelectedVatType($scope.setVatTypeDefault[0].Percent);
                        }
                    }

                    $scope.setPaymentMethod = $scope.masterData.setPaymentMethod;
                    $scope.setPaymentSupplement = $scope.masterData.setPaymentSupplement;
                    $scope.setPaymentTerm = $scope.masterData.setPaymentTerm;
                    $scope.oTranType = $scope.masterData.oTranType;

                    $scope.getExpenseTypeByGroupID = function (expenseTypeGroupID, isResetDefault) {
                        if (angular.isUndefined(isResetDefault)) {
                            isResetDefault = false;
                        }
                        var ExpenseTypeGroupInfo = $scope.settings.ExpenseTypeGroupInfo;
                        var URL = CONFIG.SERVER + 'HRTR/GetExpenseTypeByGroupID/';
                        var oRequestParameter = {
                            InputParameter: {
                                "ExpenseTypeGroupID": expenseTypeGroupID,
                                "PrefixTagCode": ExpenseTypeGroupInfo.prefix,
                                "IsReceipt": "True",
                                "TravelTypeID": ExpenseTypeGroupInfo.travelTypeID,
                                "IsDomestic": ExpenseTypeGroupInfo.isDomestic
                            }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor
                        };
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            $scope.expenseType = response.data;
                            if (isResetDefault || !$scope.GE.receiptItemEdit) {
                                if ($scope.expenseType != null && $scope.expenseType.length > 0 && $scope.GE.receiptItem != null) {
                                    $scope.GE.receiptItem.ExpenseTypeID = $scope.expenseType[0].ExpenseTypeID.toString();
                                    $scope.ddlDefault.ExpenseTypeName = $scope.expenseType[0].Name.toString();
                                    if ($scope.GE.receiptItem != null) {
                                        $scope.GE.receiptItem.ExpenseTypeName = $scope.ddlDefault.ExpenseTypeName;
                                    }
                                }
                            }
                            if ($scope.GE.receiptItem != null) {
                                $scope.getPanelNameByExpenseType($scope.GE.receiptItem.ExpenseTypeID);
                            }
                            //console.log('expenseType.', $scope.expenseType);
                        }, function errorCallback(response) {
                            $scope.expenseType = [];
                            console.log('error expenseType.', response);
                        });
                    };

                    $scope.GetExpenseTypeGroupByTag = function (groupTag, tag) {
                        var URL = CONFIG.SERVER + 'HRTR/GetExpenseTypeGroupByTag/';

                        var oRequestParameter = { InputParameter: { "grouptag": groupTag, "tag": tag, "TravelRequestNo": $scope.document.Additional ? $scope.document.Additional.TravelRequestNo : '' }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                        console.log('grouptag', groupTag);
                        console.log('TagName', tag);
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            $scope.expenseTypeGroup = response.data;
                            if ($scope.expenseTypeGroup != null && $scope.expenseTypeGroup.length > 0) {
                                $scope.ddlDefault.ExpenseTypeGroupID = $scope.expenseTypeGroup[0].ExpenseTypeGroupID.toString();

                                //--------Nun Add ExpenseTypeGroup Name and Remark 09/06/2016
                                $scope.ddlDefault.ExpenseTypeGroupName = $scope.expenseTypeGroup[0].Name.toString();
                                $scope.ddlDefault.ExpenseTypeGroupRemark = $scope.expenseTypeGroup[0].Remark.toString();
                                //-----------------------------------------------------------
                                $scope.getExpenseTypeByGroupID($scope.ddlDefault.ExpenseTypeGroupID);

                            }
                            //console.log('expenseTypeGroup.', $scope.expenseTypeGroup);

                            /* get expense type */

                        }, function errorCallback(response) {
                            console.log('error expenseTypeGroup.', response);
                        });
                    }

                    // check page name
                    if ($scope.settings.ExpenseTypeGroupInfo.prefix == 'GE') {
                        $scope.pageClassName = 'GeneralExpenseReport';
                    }else{
                        $scope.pageClassName = 'TravelReport';
                    }
                   
                    $scope.DefaultCostCenterIOObj = {
                        CostCenter: '',
                        OrgUnitID: '',
                        IO: '',
                        IsAlternativeCostCenter: false,
                        IsAlternativeIOOrg: false
                    };
                    $scope.DefaultCostCenterIO = {
                        CostCenter: '',
                        OrgUnitID: '',
                        IO: '',
                        IsAlternativeCostCenter: false,
                        IsAlternativeIOOrg: false
                    };
                    $scope.DefaultCostCenterIOByGroup = [];

                    if ($scope.pageClassName == 'GeneralExpenseReport') {
                        // default costcenter, io
                        $scope.DefaultCostCenterIO.CostCenter = $scope.ExpenseReport.CostCenter;
                        $scope.DefaultCostCenterIO.OrgUnitID = $scope.ExpenseReport.AlternativeIOOrg;
                        $scope.DefaultCostCenterIO.IO = $scope.ExpenseReport.IO;
                        $scope.DefaultCostCenterIO.IsAlternativeCostCenter = $scope.ExpenseReport.IsAlternativeCostCenter;
                        $scope.DefaultCostCenterIO.IsAlternativeIOOrg = $scope.ExpenseReport.IsAlternativeIOOrg;

                        $scope.limitMinDate = formatDateString($scope.ExpenseReport.BeginDate);
                        $scope.limitMaxDate = formatDateString($scope.ExpenseReport.EndDate);
                        $scope.savedLimitMinDate = formatDateString($scope.ExpenseReport.BeginDate);
                        $scope.savedLimitMaxDate = formatDateString($scope.ExpenseReport.EndDate);

                        $scope.$watchGroup(['ExpenseReport.CostCenter', 'ExpenseReport.AlternativeIOOrg', 'ExpenseReport.IO'], function (newValues, oldValues, scp) {
                            scp.DefaultCostCenterIO.CostCenter = scp.ExpenseReport.CostCenter;
                            scp.DefaultCostCenterIO.OrgUnitID = scp.ExpenseReport.AlternativeIOOrg;
                            scp.DefaultCostCenterIO.IO = scp.ExpenseReport.IO;
                            scp.DefaultCostCenterIO.IsAlternativeCostCenter = scp.ExpenseReport.IsAlternativeCostCenter;
                            scp.DefaultCostCenterIO.IsAlternativeIOOrg = scp.ExpenseReport.IsAlternativeIOOrg;
                            console.log('DefaultCostCenterIO.', scp.DefaultCostCenterIO);
                        });

                        $scope.$watchGroup(['ExpenseReport.BeginDate', 'ExpenseReport.EndDate'], function (newValues, oldValues, scp) {
                            scp.limitMinDate = formatDateString(scp.ExpenseReport.BeginDate);
                            scp.limitMaxDate = formatDateString(scp.ExpenseReport.EndDate);
                            scp.savedLimitMinDate = formatDateString(scp.ExpenseReport.BeginDate);
                            scp.savedLimitMaxDate = formatDateString(scp.ExpenseReport.EndDate);
                        });

                        console.log('######################################### 1.1.', $scope.grouptagname);
                        console.log('######################################### 1.2.', $scope.tagname);
                        $scope.GetExpenseTypeGroupByTag($scope.grouptagname, $scope.tagname);

                    } else if ($scope.pageClassName == 'TravelReport') {
                        // default costcenter, io
                        $scope.DefaultCostCenterIO.CostCenter = $scope.document.Additional.CostCenter;
                        $scope.DefaultCostCenterIO.OrgUnitID = $scope.document.Additional.AlternativeIOOrg;
                        $scope.DefaultCostCenterIO.IO = $scope.document.Additional.IO;
                        $scope.DefaultCostCenterIO.IsAlternativeCostCenter = $scope.document.Additional.IsAlternativeCostCenter;
                        $scope.DefaultCostCenterIO.IsAlternativeIOOrg = $scope.document.Additional.IsAlternativeIOOrg;
                        

                        if (angular.isDefined($scope.data) && angular.isDefined($scope.data.TravelSchedulePlaces) && $scope.data.TravelSchedulePlaces != null && $scope.data.TravelSchedulePlaces.length > 0) {
                            $scope.limitMinDate = formatDateString($scope.data.TravelSchedulePlaces[0].BeginDate);
                            $scope.limitMaxDate = formatDateString($scope.data.TravelSchedulePlaces[$scope.data.TravelSchedulePlaces.length - 1].EndDate);
                            $scope.savedLimitMinDate = formatDateString($scope.data.TravelSchedulePlaces[0].BeginDate);
                            $scope.savedLimitMaxDate = formatDateString($scope.data.TravelSchedulePlaces[$scope.data.TravelSchedulePlaces.length - 1].EndDate);
                        } else {
                            $scope.limitMinDate = formatDateString($scope.document.Additional.oTravelGroupRequest.BeginDate);
                            $scope.limitMaxDate = formatDateString($scope.document.Additional.oTravelGroupRequest.EndDate);
                            $scope.savedLimitMinDate = formatDateString($scope.document.Additional.oTravelGroupRequest.BeginDate);
                            $scope.savedLimitMaxDate = formatDateString($scope.document.Additional.oTravelGroupRequest.EndDate);
                        }

                        console.log('######################################### 1.1.', $scope.settings.ExpenseTypeGroupInfo.groupTagName);
                        console.log('######################################### 1.2.', $scope.settings.ExpenseTypeGroupInfo.tagNameReceipt);
                        $scope.GetExpenseTypeGroupByTag($scope.settings.ExpenseTypeGroupInfo.groupTagName, $scope.settings.ExpenseTypeGroupInfo.tagNameReceipt);

                        $scope.$watchGroup(['document.Additional.CostCenter', 'document.Additional.AlternativeIOOrg', 'document.Additional.IO'], function (newValues, oldValues, scp) {
                            scp.DefaultCostCenterIO.CostCenter = scp.document.Additional.CostCenter;
                            scp.DefaultCostCenterIO.OrgUnitID = scp.document.Additional.AlternativeIOOrg;
                            scp.DefaultCostCenterIO.IO = scp.document.Additional.IO;
                            scp.DefaultCostCenterIO.IsAlternativeCostCenter = scp.document.Additional.IsAlternativeCostCenter;
                            scp.DefaultCostCenterIO.IsAlternativeIOOrg = scp.document.Additional.IsAlternativeIOOrg;
                            console.log('DefaultCostCenterIO.', scp.DefaultCostCenterIO);
                        });

                        $scope.$watch('document.Additional.TravelSchedulePlaces', function (newValue, oldValue) {
                            if (angular.isDefined(newValue) && newValue != null && newValue.length > 0) {
                                $scope.limitMinDate = formatDateString(newValue[0].BeginDate);
                                $scope.limitMaxDate = formatDateString(newValue[newValue.length - 1].EndDate);
                                $scope.savedLimitMinDate = formatDateString(newValue[0].BeginDate);
                                $scope.savedLimitMaxDate = formatDateString(newValue[newValue.length - 1].EndDate);
                            } else {
                                $scope.limitMinDate = formatDateString($scope.document.Additional.oTravelGroupRequest.BeginDate);
                                $scope.limitMaxDate = formatDateString($scope.document.Additional.oTravelGroupRequest.EndDate);
                                $scope.savedLimitMinDate = formatDateString($scope.document.Additional.oTravelGroupRequest.BeginDate);
                                $scope.savedLimitMaxDate = formatDateString($scope.document.Additional.oTravelGroupRequest.EndDate);
                            }
                        }, true);

                        $scope.$watch('settings.ExpenseTypeGroupInfo.tagNameReceipt', function (newObj, oldObj) {
                            if (newObj != oldObj) {
                                console.log('######################################### 2.1.', $scope.settings.ExpenseTypeGroupInfo.groupTagName);
                                console.log('######################################### 2.2.', $scope.settings.ExpenseTypeGroupInfo.tagNameReceipt);
                                $scope.GetExpenseTypeGroupByTag($scope.settings.ExpenseTypeGroupInfo.groupTagName, $scope.settings.ExpenseTypeGroupInfo.tagNameReceipt);
                            }
                        });

                        /* default costcenter, io by ExpenseTypeGroupID */

                        $scope.$watch('document.Additional.GroupBudgets', function (newValue, oldValue) {
                            if (newValue != null) {
                                $.each(newValue, function (i, item) {
                                    var obj = angular.copy($scope.DefaultCostCenterIOObj);
                                    obj.CostCenter = item.CostCenter;
                                    obj.OrgUnitID = item.AlternativeIOOrg;
                                    obj.IO = item.IO;
                                    obj.IsAlternativeCostCenter = item.IsAlternativeCostCenter;
                                    obj.IsAlternativeIOOrg = item.IsAlternativeIOOrg;
                                    $scope.DefaultCostCenterIOByGroup[item.ExpenseTypeGroupID] = obj;
                                });
                            }
                            console.log('$$$$$$$$$$$$$$ default costcenter, io by ExpenseTypeGroupID 1 $$$$$$$$$$$$$$.', angular.copy($scope.DefaultCostCenterIOByGroup));
                        });

                        $scope.$watch('document.Additional.Travelers[0].PersonalBudgets', function (newValue, oldValue) {
                            if (newValue != null) {
                                $.each(newValue, function (i, item) {
                                    var obj = angular.copy($scope.DefaultCostCenterIOObj);
                                    obj.CostCenter = item.CostCenter;
                                    obj.OrgUnitID = item.AlternativeIOOrg;
                                    obj.IO = item.IO;
                                    obj.IsAlternativeCostCenter = item.IsAlternativeCostCenter;
                                    obj.IsAlternativeIOOrg = item.IsAlternativeIOOrg;
                                    $scope.DefaultCostCenterIOByGroup[item.ExpenseTypeGroupID] = obj;
                                });
                            }
                            console.log('$$$$$$$$$$$$$$ default costcenter, io by ExpenseTypeGroupID 2 $$$$$$$$$$$$$$.', angular.copy($scope.DefaultCostCenterIOByGroup));
                        });

                        /* !default costcenter, io by ExpenseTypeGroupID */

                    }

                    function formatDateString(value) {
                        if (value.length == 10) {
                            value += 'T00:00:00';
                        } else if (value.length >= 33) {
                            value = value.substr(0, 22);
                        }
                        return value + '+07:00';
                    }

                    $scope.wizardFormFlag = {
                        isAddGEReceipt: false,
                        isAddGEReceiptItem: false,
                        isAddGEReceiptItemDetail: false
                    };

                    $scope.GE = {
                        receipt: null,
                        receiptEdit: false,
                        receiptEditObj: null,

                        receiptItem: null,
                        receiptItemEdit: false,
                        receiptItemEditObj: null,
                        receiptItemFormData: {
                            Amount: 0,
                            AmountInRight: 9999999999,
                            oTransportationType: null

                            ,tmpEmployeeName: ''
                            ,tmpOrgUnitName: ''
                            ,tmpCompanyName: ''

                        },

                        receiptItemDetail: null,
                        receiptItemDetailEdit: false,
                        receiptItemDetailEditObj: null,
                        receiptItemDetailFormData: {
                            ProjectCode: '',
                            FlatLocationFilter: { Key: '!' },
                        }
                    };

                    $scope.panelName = '';

                    $scope.tempLastVendorCode = '';

                    $scope.tempFormData = {
                        chkVendorTaxIDDup: ''
                    };
                    $scope.tempData = {
                        chkWhtTax: false,
                        oWHTTypeSubsidise: null,
                        sumWHTAmount: 0,
                        oVendorSelected: null
                    };

                    
                  

                    $scope.whtSettings = {
                        ENABLEWHT: false,
                        ENABLEROW: false,
                        AUTHORIZEWHT: false
                    };

                    $scope.travelCurrencyList = [];

                    $scope.findExchangeType = function (exchangeTypeID) {
                        if (angular.isDefined($scope.exchangeType)) {
                            for (var i = 0; i < $scope.exchangeType.length; i++) {
                                if ($scope.exchangeType[i].ExchangeTypeID == exchangeTypeID) {
                                    if (employeeData.Language == 'TH') {
                                        return '(' + $scope.exchangeType[i].DescriptionTH + ')';
                                    } else {
                                        return '(' + $scope.exchangeType[i].DescriptionEN + ')';
                                    }

                                }
                            }
                        }
                        return '';
                    };

                    $scope.findDropdownExchangeType = function (exchangeTypeID, exchangeRate) {
                        if (angular.isDefined($scope.exchangeType) && exchangeRate > 1) {
                            for (var i = 0; i < $scope.exchangeType.length; i++) {
                                if ($scope.exchangeType[i].ExchangeTypeID == exchangeTypeID) {
                                    if (employeeData.Language == 'TH') {
                                        return '(' + exchangeRate + ' : ' + $scope.exchangeType[i].DescriptionTH + ')';
                                    } else {
                                        return '(' + exchangeRate + ' : ' + $scope.exchangeType[i].DescriptionEN + ')';
                                    }

                                }
                            }
                        } else {
                            return '(' + exchangeRate + ')';
                        }
                    };



                    /* --- !Variable --- */

                    $scope.MathRounding = function (amount) {
                        var value = Math.round(amount * 100) / 100;
                        value = Number(value.toFixed(2));
                        return value;
                    };

                    $scope.multiplyExchangeRate = function (originalAmount, exchangeRate, fromCurrencyRatio, toCurrencyRatio) {
                        var amount = (originalAmount * exchangeRate * toCurrencyRatio) / fromCurrencyRatio;
                        amount = $scope.MathRounding(amount);
                        return amount;
                    };

                    

                    var getPanelWHT = function () {
                        var sMapType = ($scope.pageClassName == 'GeneralExpenseReport' || $scope.pageClassName == 'TravelReport') ? 'TE_ENABLEWHT' : 'EX_ENABLEWHT';

                        var URL = CONFIG.SERVER + 'HRTR/GetPanelWHT';
                        var oRequestParameter = { InputParameter: { 'MAPPINGTYPE': sMapType }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            $scope.whtSettings.ENABLEWHT = response.data;

                            console.log('GetPanelWHT.', response.data);
                        }, function errorCallback(response) {
                            console.log('error GetPanelWHT.', response);
                        });
                    };
                    getPanelWHT();

                    var checkAuthorizeWHT = function () {
                        $scope.whtSettings.AUTHORIZEWHT = (employeeData.UserRoles.indexOf("JUNIOR_ACCOUNT") != -1) || (employeeData.UserRoles.indexOf("SENIOR_ACCOUNT") != -1);
                    }
                    checkAuthorizeWHT();

                    var GetWHTTypeSubsidiseGetAll = function () {
                        var URL = CONFIG.SERVER + 'HRTR/GetWHTTypeSubsidiseGetAll';
                        var oRequestParameter = { InputParameter: {}, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            $scope.tempData.oWHTTypeSubsidise = response.data;

                            console.log('GetWHTTypeSubsidiseGetAll.', response.data);
                        }, function errorCallback(response) {
                            console.log('error GetWHTTypeSubsidiseGetAll.', response);
                        });
                    };
                    GetWHTTypeSubsidiseGetAll();



                    var getWHTRow = function () {
                        $scope.whtSettings.ENABLEROW = ($scope.pageClassName == 'GeneralExpenseReport' || $scope.pageClassName == 'TravelReport') ? false : true;
                    };
                    getWHTRow();

                    $scope.getExpenseItems_Receipt = function () {
                        var oList = [];
                        if (angular.isDefined($scope.ExpenseReport.ExpenseReportReceiptList) && $scope.ExpenseReport.ExpenseReportReceiptList != null) {

                            var ExpenseReportReceiptList = $scope.ExpenseReport.ExpenseReportReceiptList;
                            var runNo = 1;
                            for (var i = 0; i < ExpenseReportReceiptList.length; i++) {
                                var receipt = angular.copy(ExpenseReportReceiptList[i]);
                                if (receipt.IsReceipt && receipt.IsVisible) {
                                    var countReceiptItem = receipt.ExpenseReportReceiptItemList.length;
                                    receipt.IsFirstRow = false;
                                    var sumAllChild = 0;
                                    for (var j = 0; j < countReceiptItem; j++) {
                                        var receiptItem = receipt.ExpenseReportReceiptItemList[j];
                                        sumAllChild += receiptItem.ExpenseReportReceiptItemDetailList.length;
                                    }
                                    receipt.ItemCount = sumAllChild;
                                    receipt.ExpenseReportReceiptItemList = null;
                                    //receipt.ExpenseReportReceiptFileList = null;
                                    receipt.orderID = runNo++;

                                    for (var j = 0; j < countReceiptItem; j++) {
                                        var receiptItem = ExpenseReportReceiptList[i].ExpenseReportReceiptItemList[j];
                                        var countReceiptItemDetail = receiptItem.ExpenseReportReceiptItemDetailList.length;
                                        receipt.item = {
                                            IsFirstRow: false,
                                            ItemCount: countReceiptItemDetail,
                                            ItemID: receiptItem.ItemID,
                                            ExpenseTypeName: receiptItem.ExpenseTypeName
                                        };

                                        for (var k = 0; k < countReceiptItemDetail; k++) {
                                            var tempReceipt = angular.copy(receipt);
                                            var receiptItemDetail = receiptItem.ExpenseReportReceiptItemDetailList[k];
                                            if (j == 0 && k == 0) {
                                                tempReceipt.IsFirstRow = true;
                                            }
                                            if (k == 0) {
                                                tempReceipt.item.IsFirstRow = true;
                                            }
                                            tempReceipt.itemDetail = {
                                                CostCenter: receiptItemDetail.CostCenter,
                                                IO: receiptItemDetail.IO,
                                                LocalAmount: receiptItemDetail.LocalAmount,
                                                OriginalAmount: receiptItemDetail.OriginalAmount

                                            };

                                            tempReceipt.realItem = ExpenseReportReceiptList[i];


                                            oList.push(tempReceipt);
                                        }
                                    }
                                }
                            }

                        }
                        return oList;
                    };
                    $scope.expenseItems_Receipt = $scope.getExpenseItems_Receipt();

                    $scope.getExpenseItems_ReceiptItem = function () {
                        var oList = [];
                        if ($scope.GE.receipt != null) {
                            if (angular.isDefined($scope.GE.receipt.ExpenseReportReceiptItemList) && $scope.GE.receipt.ExpenseReportReceiptItemList != null) {

                                var ExpenseReportReceiptItemList = $scope.GE.receipt.ExpenseReportReceiptItemList;
                                for (var i = 0; i < ExpenseReportReceiptItemList.length; i++) {
                                    var receiptItem = angular.copy(ExpenseReportReceiptItemList[i]);
                                    var countReceiptItemDetail = receiptItem.ExpenseReportReceiptItemDetailList.length;
                                    receiptItem.IsFirstRow = false;
                                    receiptItem.ItemCount = countReceiptItemDetail;
                                    receiptItem.ExpenseReportReceiptItemDetailList = null;

                                    for (var k = 0; k < countReceiptItemDetail; k++) {
                                        var tempReceiptItem = angular.copy(receiptItem);
                                        var receiptItemDetail = ExpenseReportReceiptItemList[i].ExpenseReportReceiptItemDetailList[k];
                                        if (k == 0) {
                                            tempReceiptItem.IsFirstRow = true;
                                        }
                                        tempReceiptItem.itemDetail = {
                                            CostCenter: receiptItemDetail.CostCenter,
                                            IO: receiptItemDetail.IO,
                                            LocalAmount: receiptItemDetail.LocalAmount,
                                            OriginalAmount: receiptItemDetail.OriginalAmount
                                        };
                                        tempReceiptItem.realItem = ExpenseReportReceiptItemList[i];
                                        tempReceiptItem.orderID = i + 1;
                                        oList.push(tempReceiptItem);
                                    }
                                }

                            }
                        }
                        return oList;
                    };
                    $scope.expenseItems_ReceiptItem = $scope.getExpenseItems_ReceiptItem();

                    

                    $scope.getFileAttach = function (attachment) {
                        /* direct */
                        if (angular.isDefined(attachment) && attachment != '') {
                            if ($.trim(attachment.FilePath) != '') {
                                var path = CONFIG.SERVER + 'Client/files/' + attachment.FilePath + '/' + attachment.FileName;
                                $window.open(path);
                            }
                           
                        }
                        /* !direct */
                    };


                    $scope.getIOByOrganization = function (Org, isSetDefaultIO) {
                        if (angular.isUndefined(isSetDefaultIO)) {
                            isSetDefaultIO = false;
                        }
                        var URL = CONFIG.SERVER + 'HRTR/GetInternalOrderByOrgUnit/';
                        var oRequestParameter = { InputParameter: { "OrgUnit": Org }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            $scope.objIO = response.data;
                            if ($scope.objIO != null && $scope.objIO.length > 0 && isSetDefaultIO) {
                                // set default
                                if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                                    // none, average
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].IO = $scope.objIO[0].OrderID;
                                } else {
                                    // exactly
                                    $scope.GE.receiptItemDetail.IO = $scope.objIO[0].OrderID;
                                }
                            }
                            console.log('objIO.', $scope.objIO);
                        }, function errorCallback(response) {
                            $scope.objIO = [];
                            console.log('error objIO.', response);
                        });
                    };

                    $scope.changeIsAlternativeCostCenter = function (isAlternativeCostCenter) {
                        if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                            // none, average
                            if (!isAlternativeCostCenter) {
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].CostCenter = $scope.masterData.objCostcenterDistribution[0].CostCenterCode;
                            }

                            //if (!$scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].IsAlternativeIOOrg) {
                            //    $scope.getIOByCostCenter($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].CostCenter, true);
                            //}
                        } else {
                            // exactly
                            if (!isAlternativeCostCenter) {
                                $scope.GE.receiptItemDetail.CostCenter = $scope.masterData.objCostcenterDistribution[0].CostCenterCode;
                            }
                            //if (!$scope.GE.receiptItemDetail.IsAlternativeIOOrg) {
                            //    $scope.getIOByCostCenter($scope.GE.receiptItemDetail.CostCenter, true);
                            //}
                        }
                    };

                    $scope.changeIsAlternativeIOOrg = function (isAlternativeIOOrg) {
                        // change default
                        if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                            // none, average
                            if (!isAlternativeIOOrg) {
                                if ($scope.document.Requestor.OrgUnit != null) {
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].AlternativeIOOrg = $scope.document.Requestor.OrgUnit;
                                }
                                //$scope.getIOByCostCenter($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].CostCenter, true);
                                $scope.getIOByOrganization($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].AlternativeIOOrg, true);
                            } else {
                                $scope.getIOByOrganization($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].AlternativeIOOrg, true);
                            }
                        } else {
                            // exactly
                            if (!isAlternativeIOOrg) {
                                if ($scope.document.Requestor.OrgUnit != null) {
                                    $scope.GE.receiptItemDetail.AlternativeIOOrg = $scope.document.Requestor.OrgUnit;
                                }
                                //$scope.getIOByCostCenter($scope.GE.receiptItemDetail.CostCenter, true);
                                $scope.getIOByOrganization($scope.GE.receiptItemDetail.AlternativeIOOrg, true);
                            } else {
                                $scope.getIOByOrganization($scope.GE.receiptItemDetail.AlternativeIOOrg, true);
                            }
                        }
                    };

                    $scope.onSelectedCostCenter = function (selectedCostCenter) {
                        //if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                        //    // none, average
                        //    if (!$scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].IsAlternativeIOOrg) {
                        //        $scope.getIOByCostCenter(selectedCostCenter.CostCenterCode, true);
                        //    }
                        //} else {
                        //    // exactly
                        //    if (!$scope.GE.receiptItemDetail.IsAlternativeIOOrg) {
                        //        $scope.getIOByCostCenter(selectedCostCenter.CostCenterCode, true);
                        //    }
                        //}
                        
                    };

                    $scope.onSelectedOrganization = function (selectedOrganization) {
                        //if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                        //    // none, average
                        //    if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].IsAlternativeIOOrg) {
                        //        $scope.getIOByOrganization(selectedOrganization.ObjectID, true);
                        //    }
                        //} else {
                        //    // exactly
                        //    if ($scope.GE.receiptItemDetail.IsAlternativeIOOrg) {
                        //        $scope.getIOByOrganization(selectedOrganization.ObjectID, true);
                        //    }
                        //}

                        $scope.getIOByOrganization(selectedOrganization.ObjectID, true);
                    };

                    /* !CostCentet, Organization, IO dropdown */


                    /* Guest */
                    $scope.searchTextGuest = 'hello world';
                    $scope.querySearchGuest = function (query) {
                        var results = angular.copy(query ? $scope.employeeDataList.filter(createGuestFilterFor(query)) : $scope.employeeDataList), deferred;
                        results = $scope.limMaxArr(results, 20);
                        $scope.simulateQuery = true;//return results;
                        if ($scope.simulateQuery) {
                            deferred = $q.defer();
                            deferred.resolve(results);//$timeout(function () { deferred.resolve(results); }, Math.random() * 300, false);
                            return deferred.promise;
                        } else {
                            return results;
                        }
                        //if ($scope.simulateQuery) {
                        //    deferred = $q.defer();
                        //    $timeout(function () { deferred.resolve(results); }, Math.random() * 50, false);
                        //    return deferred.promise;
                        //} else {
                        //    return results;
                        //}
                    };
                    $scope.limMaxArr = function (arr, max) {
                        var tempArr = []; var tempArrIn = angular.copy(arr);
                        if (tempArrIn.length > max) { tempArr = tempArrIn.splice(0, max); } else { return arr; }
                        return tempArr
                    }


                    function checkDuplicateGuest(guestList) {
                        return function (item) {
                            if (!item) return false;
                            return !(guestList.duplicateProp('EmployeeID', item.EmployeeID));
                        };
                    }

                    function createGuestFilterFor(query) {
                        var lowercaseQuery = angular.lowercase(query);
                        return function (item) {
                            if (!item || !item.EmployeeID || !item.EmployeeName) return false;
                            var source = angular.lowercase(item.EmployeeID + ' : ' + item.EmployeeName);
                            return (source.indexOf(lowercaseQuery) >= 0);
                        };
                    }

                    function selectedItemGuestChange(item, index) {
                        if (angular.isDefined($scope.autocompleteGuest.searchTextGuest)) {
                            $scope.guest.newGuest = item;
                        } else {
                            $scope.guest.newGuest = null;
                        }
                        
                    }

                    $scope.guest = {
                        newGuest: null
                    };
                    $scope.autocompleteGuest = {
                        selectedItem: null,
                        searchTextGuest:''
                    }
                    var tempResultGuest;
                    $scope.tryToSelectGuest = function (text, $event) {
                        if ($event.keyCode) return;

                        for (var i = 0; i < $scope.employeeDataList.length; i++) {
                            if (($scope.employeeDataList[i].EmployeeID +  " : " + $scope.employeeDataList[i].EmployeeName) == text || $scope.employeeDataList[i].EmployeeName == text) {
                                tempResultGuest = $scope.employeeDataList[i];
                                break;
                            }
                        }
                        $scope.autocompleteGuest.searchTextGuest = '';
                        $scope.autocompleteGuest.selectedItem = null;
                    }
                    $scope.checkTextGuest = function (text) {
                        var result = null;
                        for (var i = 0; i < $scope.employeeDataList.length; i++) {
                            if (($scope.employeeDataList[i].EmployeeID + " : " + $scope.employeeDataList[i].EmployeeName) == text || $scope.employeeDataList[i].EmployeeName == text) {
                                result = $scope.employeeDataList[i];
                                break;
                            }
                        }
                        if (result) {
                            $scope.autocompleteGuest.searchTextGuest = result.EmployeeID + " : " + result.EmployeeName;
                            selectedItemGuestChange(result);
                            $scope.guest.newGuest = angular.copy(result);
                        } else if (tempResultGuest) {
                            $scope.autocompleteGuest.searchTextGuest = tempResultGuest.EmployeeID + " : " + tempResultGuest.EmployeeName;
                            selectedItemGuestChange(tempResultGuest);
                            $scope.guest.newGuest = angular.copy(tempResultGuest);
                        }
                        
                    }
                    var clearNewGuest = function () {
                        $scope.guest.newGuest = null;
                    };

                    var createGuestObjFromEmployee = function (employee) {

                        if (angular.isUndefined(employee) || employee == null) {
                            return {
                                RequestNo: '',
                                ReceiptID: $scope.GE.receiptItem.ReceiptID,
                                ItemID: $scope.GE.receiptItem.ItemID,
                                GuestID: 0,
                                IsEmployee: false,
                                EmployeeID: '',
                                EmployeeName: '',
                                OrgUnit: '',
                                OrgUnitName: '',
                                Position: '',
                                PositionName: '',
                                CompanyCode: '',
                                CompanyName: ''
                            };
                        }

                        return {
                            RequestNo: '',
                            ReceiptID: $scope.GE.receiptItem.ReceiptID,
                            ItemID: $scope.GE.receiptItem.ItemID,
                            GuestID: 0,
                            IsEmployee: true,
                            EmployeeID: employee.EmployeeID,
                            EmployeeName: employee.EmployeeName,
                            OrgUnit: employee.OrgUnit,
                            OrgUnitName: employee.OrgUnitName,
                            Position: employee.Position,
                            PositionName: employee.PositionName,
                            CompanyCode: employee.CompanyCode,
                            CompanyName: employee.CompanyName
                        };
                    };


                    $scope.receiptItem_AddGuestEmployee = function () {
                        if ($scope.guest.newGuest != null) {
                            var isExist = false;
                            $.each($scope.GE.receiptItem.ExpenseReportReceiptItemGuestList, function (i, guest) {
                                if (guest.IsEmployee && guest.EmployeeID == $scope.guest.newGuest.EmployeeID) {
                                    isExist = true;
                                    return false;
                                }
                            });
                            if (isExist) {
                                $scope.p_message('Duplicate employee.','w');
                            } else {
                                console.log($scope.guest.newGuest);
                                $scope.GE.receiptItem.ExpenseReportReceiptItemGuestList.push(createGuestObjFromEmployee($scope.guest.newGuest));
                                clearNewGuest();
                                selectedItemGuestChange(null, 0);
                                $scope.autocompleteGuest.searchTextGuest = '';
                                $scope.employeeDataList = angular.copy($scope.masterData.employeeDataList.filter(checkDuplicateGuest($scope.GE.receiptItem.ExpenseReportReceiptItemGuestList)));
                            }
                        } else {
                            $scope.p_message($scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['EMPLOYEEID'],'w');
                        }
                    };

                    $scope.receiptItem_AddGuestOther = function () {

                        if ($scope.GE.receiptItemFormData.tmpEmployeeName == '' || $scope.GE.receiptItemFormData.tmpOrgUnitName == '' || $scope.GE.receiptItemFormData.tmpCompanyName == '') {
                            $scope.p_message($scope.Text['EXPENSE']['GUEST_OTHER_EMPTY_NAME'], 'w');
                            return false;
                        }

                        $scope.GE.receiptItem.ExpenseReportReceiptItemGuestList.push(createGuestObjFromEmployee());

                        var iRow = $scope.GE.receiptItem.ExpenseReportReceiptItemGuestList.length;
                        $scope.GE.receiptItem.ExpenseReportReceiptItemGuestList[iRow - 1].EmployeeName = $scope.GE.receiptItemFormData.tmpEmployeeName;
                        $scope.GE.receiptItem.ExpenseReportReceiptItemGuestList[iRow - 1].OrgUnitName = $scope.GE.receiptItemFormData.tmpOrgUnitName;
                        $scope.GE.receiptItem.ExpenseReportReceiptItemGuestList[iRow - 1].CompanyName = $scope.GE.receiptItemFormData.tmpCompanyName;

                        $scope.GE.receiptItemFormData.tmpEmployeeName = '';
                        $scope.GE.receiptItemFormData.tmpOrgUnitName = '';
                        $scope.GE.receiptItemFormData.tmpCompanyName = '';
                    };

                    $scope.receiptItem_DeleteGuestEmployee = function (index) {
                        if (confirm($scope.Text['SYSTEM']['CONFIRM_DELETE'])) {
                            var deleteIndex = 0;
                            $.each($scope.GE.receiptItem.ExpenseReportReceiptItemGuestList, function (i, guest) {
                                if (guest.IsEmployee) {
                                    if (deleteIndex == index) {
                                        deleteIndex = i;
                                        return false;
                                    }
                                    deleteIndex++;
                                }
                            });
                            $scope.GE.receiptItem.ExpenseReportReceiptItemGuestList.splice(deleteIndex, 1);
                            $scope.employeeDataList = angular.copy($scope.masterData.employeeDataList.filter(checkDuplicateGuest($scope.GE.receiptItem.ExpenseReportReceiptItemGuestList)));
                        }
                    };

                    $scope.receiptItem_DeleteGuestOther = function (index) {
                        if (confirm($scope.Text['SYSTEM']['CONFIRM_DELETE'])) {
                            var deleteIndex = 0;
                            $.each($scope.GE.receiptItem.ExpenseReportReceiptItemGuestList, function (i, guest) {
                                if (!guest.IsEmployee) {
                                    if (deleteIndex == index) {
                                        deleteIndex = i;
                                        return false;
                                    }
                                    deleteIndex++;
                                }
                            });
                            $scope.GE.receiptItem.ExpenseReportReceiptItemGuestList.splice(deleteIndex, 1);
                        }
                    };
                    $scope.warningAmount = '';
                    /* !Guest */
                    $scope.changeVATBaseAmount = function () {
                        var VATBaseAmount = Number($scope.GE.receipt.VATBaseAmount);
                        VATBaseAmount = isNaN(VATBaseAmount) ? 0 : $scope.MathRounding(VATBaseAmount);
                        var VatPercent = Number($scope.GE.receipt.VATPercent);
                        VatPercent = isNaN(VatPercent) ? 0 : VatPercent;
                        $scope.GE.receipt.VATAmount = $scope.MathRounding(VATBaseAmount * VatPercent / 100);
                    };

                    $scope.isVATBaseAmountValid = function (VATBaseAmount) {
                        var newAmount = isNaN(VATBaseAmount) ? 0 : $scope.MathRounding(VATBaseAmount);
                        var tolerance = Number($scope.settings.ReceiptToleranceAllow);
                        
                        //if (Math.abs(VATBaseAmount - $scope.GE.receipt.LocalNoVATTotalAmount) < tolerance) {
                        //    $scope.showChangeAmountWarning = false;
                        //    return true;
                        //} else {
                        //    $scope.warningAmount = $scope.Text['EXPENSE']['RECEIPT_OVERDUE_TOLERANCE_ALLOW'];
                        //    $scope.warningAmount = $scope.warningAmount.replace("{0}", tolerance);
                        //    $scope.showChangeAmountWarning = true;
                        //    return false;
                        //}
                        //  if (newAmount <= $scope.MathRounding($scope.GE.receipt.LocalNoVATTotalAmount + tolerance)) {


                        if ($scope.settings.ReceiptLessthanBeforevat) {
                            if (newAmount > ($scope.MathRounding($scope.GE.receipt.LocalNoVATTotalAmount)) ) {
                                $scope.showChangeAmountWarning = false;
                                return false;
                            }
                        }


                        var inin = Math.abs($scope.MathRounding($scope.GE.receipt.LocalNoVATTotalAmount) - newAmount);
                        if (tolerance >= Math.abs($scope.MathRounding($scope.GE.receipt.LocalNoVATTotalAmount) - newAmount)) {
                            $scope.showChangeAmountWarning = false;
                            return true;
                        } else {
                            $scope.warningAmount = $scope.Text['EXPENSE']['RECEIPT_OVERDUE_TOLERANCE_ALLOW'];
                            $scope.warningAmount = $scope.warningAmount.replace("{0}", tolerance);
                            $scope.showChangeAmountWarning = true;
                            $scope.showChangeAmountWarning = true;
                            return false;
                        }
                    };

                    $scope.onBlurVATBaseAmount = function () {
                        var VATBaseAmount = Number($scope.GE.receipt.VATBaseAmount);
                        if ($scope.isVATBaseAmountValid(VATBaseAmount)) {
                            $scope.showChangeAmountWarning = false;
                            $scope.GE.receipt.VATBaseAmount = VATBaseAmount;
                        } else {
                            $scope.showChangeAmountWarning = true;
                            $scope.GE.receipt.VATBaseAmount = $scope.GE.receipt.LocalNoVATTotalAmount;
                            for (var i = 0; i < $scope.setVatTypeDefault.length; i++) {
                                if ($scope.GE.receipt.VATCode == $scope.setVatTypeDefault[i].VATCode) {
                                    $scope.onSelectedVatType($scope.setVatTypeDefault[i].Percent);
                                }
                            }
                        }
                    };

                    $scope.onBlurOriginalAmount = function (objDetail) {
                        var valueAmount = Number(objDetail.OriginalAmount);
                        valueAmount = isNaN(valueAmount) ? 0 : $scope.MathRounding(valueAmount);
                        objDetail.OriginalAmount = valueAmount;
                    };

                    $scope.onBlurFormAmount = function (objFormData) {
                        var valueAmount = Number(objFormData.Amount);
                        valueAmount = isNaN(valueAmount) ? 0 : $scope.MathRounding(valueAmount);
                        objFormData.Amount = valueAmount;
                    };

                    $scope.sumWHT = function (WHTAmount1LC, WHTAmount2LC) {
                        return WHTAmount1LC + WHTAmount2LC;
                    };

                    $scope.sumLocalNoVATTotalAmount = function (list, isReceipt) {
                        var total = 0;
                        angular.forEach(list, function (item) {
                            if (item) {
                                if (item.IsReceipt == isReceipt && item.IsVisible) {
                                    var amount = Number(item.LocalNoVATTotalAmount);
                                    total += isNaN(amount) ? 0 : amount;
                                }
                            }
                        });
                        return total;
                    };

                    $scope.sumVATAmount = function (list, isReceipt) {
                        var total = 0;
                        angular.forEach(list, function (item) {
                            if (item.IsReceipt == isReceipt && item.IsVisible) {
                                var amount = Number(item.VATAmount);
                                total += isNaN(amount) ? 0 : amount;
                            }
                        });
                        return $scope.MathRounding(total);
                    };

                    $scope.allSum = function () {
                        var sum = $scope.sumLocalNoVATTotalAmount($scope.ExpenseReport.ExpenseReportReceiptList, true) + $scope.sumVATAmount($scope.ExpenseReport.ExpenseReportReceiptList, true);
                        if (angular.isDefined($scope.childSumData)) {
                            $scope.childSumData.ExpenseReceipt = sum;
                        }
                        return sum;
                    };


                    var GetFlatRateByLocation = function (SourceLocationID, DestinationLocationID) {
                        var URL = CONFIG.SERVER + 'HRTR/GetFlatRateByLocation';
                        var oRequestParameter = { InputParameter: { 'SourceLocationID': SourceLocationID, 'DestinationLocationID': DestinationLocationID }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                        var promise = $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        });
                        return promise;
                    };

                    $scope.getMileageRate = function (oTranType) {
                        var URL = CONFIG.SERVER + 'HRTR/GetMileageRate';
                        var oRequestParameter = { InputParameter: { 'TranTypeCode': oTranType }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            var amount = 0;
                            if (angular.isDefined(response.data[0])) {
                                amount = Number(response.data[0].FuelRate);
                            }
                            $scope.fuelRate = isNaN(amount) ? 0 : amount;
                            if (angular.isDefined(response.data[0])) {
                                amount = Number(response.data[0].NoCalIncomeRate);
                            }
                            $scope.mileageNoCalIncomeRate = isNaN(amount) ? 0 : amount;
                            $scope.onChangeDistance();
                            console.log('GetMileageRate.', response.data);
                        }, function errorCallback(response) {
                            console.log('error GetMileageRate.', response);
                        });
                    };
                    if ($scope.oTranType != null && $scope.oTranType.length > 0) {
                        $scope.getMileageRate($scope.oTranType[0].TransportationTypeCode);
                    }

                    //Nun Add 25/08/2016
                    $scope.getExpenseTypeAmountInRight = function (expID) {
                        var URL = CONFIG.SERVER + 'HRTR/GetExpenseTypeAmountInRight';
                        var oRequestParameter = { InputParameter: { 'ExpenseTypeID': expID }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {

                            var amount = 9999999999;
                            if (response.data && response.data.length > 0) {
                                amount = Number(response.data[0].AmountInRight);
                            }
                            $scope.amountInRight = isNaN(amount) ? 9999999999 : amount;

                            console.log('GetExpenseTypeAmountInRight.', response.data);
                        }, function errorCallback(response) {
                            console.log('error GetExpenseTypeAmountInRight.', response);
                        });
                    };
                    //

                    $scope.$watch('GE.receipt.ExpenseReportReceiptItemList', function (newObj, oldObj) {
                        // update VatType
                        $scope.listExpenseType = "";
                        $scope.listCarType = "";

                        var listExpenseType = '', listCarType = '';
                        if ($scope.panelName == 'pnlTransRegistration' || $scope.panelName == 'pnlVehicleRent') {
                            if (angular.isDefined(newObj) && newObj != null) {
                                for (var i = 0; i < newObj.length; i++) {
                                    if (newObj[i].ExpenseTypeID != '') {
                                        $scope.listExpenseType += newObj[i].ExpenseTypeID + ',';
                                    }
                                    if (newObj[i].Str2 != '') {
                                        for (var n = 0 ; n < $scope.carTypes_RegistrationList.length; n++) {

                                            if (newObj[i].Str2 == $scope.carTypes_RegistrationList[n].Description) {
                                                $scope.listCarType += $scope.carTypes_RegistrationList[n].CarTypeID + ',';
                                            }
                                        }
                                    }
                                }
                                $scope.GetVATTypeByExpenseTypeCarType();
                            }
                        } else {
                            if (angular.isDefined(newObj) && newObj != null) {
                                for (var i = 0; i < newObj.length; i++) {
                                    if (newObj[i].ExpenseTypeID != '') {
                                        $scope.listExpenseType += newObj[i].ExpenseTypeID + ',';
                                    }

                                    //Add new Nipon & Som 14-06-2017
                                    $scope.oParamRequestNo = "";
                                    $scope.oParamReceiptItem = "";
                                    $scope.oParamRequestNo = $scope.GE.receipt.RequestNo;
                                    $scope.oParamReceiptItem = $scope.GE.receipt.ExpenseReportReceiptItemList[0].ExpenseReportReceiptItemDetailList[0].ReceiptID;

                                    var URL = CONFIG.SERVER + 'HRTR/GetCarTypeItem';//requestNo,ReceiptID
                                    var oRequestParameter = { InputParameter: { 'RequestNo': $scope.oParamRequestNo, 'ReceiptItem': $scope.oParamReceiptItem }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                                    $http({
                                        method: 'POST',
                                        url: URL,
                                        data: oRequestParameter
                                    }).then(function successCallback(response) {
                                        $scope.listCarType = response.data;
                                        $scope.GetVATTypeByExpenseTypeCarType();
                                    });
                                }
                            }
                        }
                        
                       
                    }, true);

                    $scope.GetVATTypeByExpenseTypeCarType = function ()
                    {
                        var URL = CONFIG.SERVER + 'HRTR/GetVATTypeByExpenseTypeCarType';
                        var oRequestParameter = { InputParameter: { 'listExpenseTypeID': $scope.listExpenseType, 'listCarTypeID': $scope.listCarType }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            $scope.setVatTypeDefault = response.data;
                            if ($scope.setVatTypeDefault != null && $scope.setVatTypeDefault.length > 0) {
                                if (!$scope.CheckIsCurrentVatInhDropdown())
                                {
                                    if ($scope.GE.receipt != null)
                                    {
                                        $scope.ddlDefault.VATCode = $scope.setVatTypeDefault[0].VATCode;
                                        $scope.GE.receipt.VATCode = $scope.ddlDefault.VATCode;
                                        $scope.onSelectedVatType($scope.setVatTypeDefault[0].Percent);
                                    }
                                }
                            }
                            console.log('GetVATTypeByExpenseTypeCarType.', response.data);
                        }, function errorCallback(response) {
                            console.log('error GetVATTypeByExpenseTypeCarType.', response);
                        });
                    }



                    $scope.CheckIsCurrentVatInhDropdown = function ()
                    {
                        var flg = false;
                        if ($scope.ddlDefault == null || $scope.ddlDefault.VATCode == '') {
                            flg = false;
                        }
                        else
                        {
                            if (angular.isDefined($scope.GE) && ($scope.GE.receipt != null) && angular.isDefined($scope.GE.receipt.VATCode)) {
                                for (var i = 0; i < $scope.setVatTypeDefault.length; i++) {
                                    if ($scope.GE.receipt.VATCode == $scope.setVatTypeDefault[i].VATCode) {
                                        flg = true;
                                        break;
                                    }
                                }
                            }
                        }

                        return flg;
                    }



                    // sum receipt total amount

                    $scope.$watchGroup(['GE.receipt.LocalNoVATTotalAmount', 'GE.receipt.VATAmount', 'GE.receipt.WHTAmount1LC', 'GE.receipt.WHTAmount2LC'], function (newValues, oldValues, scope) {
                        if (angular.isDefined($scope.GE.receipt) && $scope.GE.receipt != null) {
                            //$scope.GE.receipt.LocalTotalAmount = $scope.GE.receipt.LocalNoVATTotalAmount + $scope.GE.receipt.VATAmount + $scope.GE.receipt.WHTAmount1LC + $scope.GE.receipt.WHTAmount2LC;
                            //Nun Add  
                            $scope.GE.receipt.LocalTotalAmount = $scope.GE.receipt.LocalNoVATTotalAmount + $scope.GE.receipt.VATAmount;
                        }
                    });

                    // !sum receipt total amount

                    /* exchange rate */

                    $scope.changeReceiptCurrencyFromOutside = function (receipt, newExchange) {
                        console.log('newExchange.', newExchange);
                        receipt.ExchangeRate = (newExchange.ExchangeRate == null) ? 0 : Number(newExchange.ExchangeRate);
                        receipt.OriginalCurrencyCode = newExchange.FromCurrency;
                        receipt.currency_searchText = newExchange.FromCurrency + ' ' + $scope.findDropdownExchangeType(newExchange.ExchangeTypeID, newExchange.ExchangeRate);
                        receipt.ExchangeDate = newExchange.EffectiveDate;
                        receipt.ExchangeTypeID = newExchange.ExchangeTypeID;
                        receipt.FromCurrencyRatio = newExchange.RatioFromCurrencyUnit;
                        receipt.ToCurrencyRatio = newExchange.RatioToCurrencyUnit;
                        var sumLocalAmount = 0;
                        if (angular.isDefined(receipt.ExpenseReportReceiptItemList) && receipt.ExpenseReportReceiptItemList != null) {
                            for (var i = 0; i < receipt.ExpenseReportReceiptItemList.length; i++) {
                                var item = receipt.ExpenseReportReceiptItemList[i];
                                if (angular.isDefined(item.ExpenseReportReceiptItemDetailList) && item.ExpenseReportReceiptItemDetailList != null) {
                                    for (var k = 0; k < item.ExpenseReportReceiptItemDetailList.length; k++) {
                                        //item.ExpenseReportReceiptItemDetailList[k].LocalAmount = item.ExpenseReportReceiptItemDetailList[k].OriginalAmount * receipt.ExchangeRate;
                                        item.ExpenseReportReceiptItemDetailList[k].LocalAmount = $scope.multiplyExchangeRate(item.ExpenseReportReceiptItemDetailList[k].OriginalAmount, receipt.ExchangeRate, receipt.FromCurrencyRatio, receipt.ToCurrencyRatio);
                                        sumLocalAmount += $scope.MathRounding(item.ExpenseReportReceiptItemDetailList[k].LocalAmount);
                                        if (item.ExpenseReportReceiptItemDetailList[k].OriginalAmountInRight != 9999999999) {
                                            item.ExpenseReportReceiptItemDetailList[k].LocalAmountInRight = $scope.multiplyExchangeRate(item.ExpenseReportReceiptItemDetailList[k].OriginalAmountInRight, receipt.ExchangeRate, receipt.FromCurrencyRatio, receipt.ToCurrencyRatio);
                                        } else {
                                            item.ExpenseReportReceiptItemDetailList[k].LocalAmountInRight = 9999999999;
                                        }
                                    }
                                }
                            }
                        }
                        receipt.LocalNoVATTotalAmount = sumLocalAmount;
                        receipt.VATBaseAmount = sumLocalAmount;
                        // calculate vat
                        var VATBaseAmount = Number(receipt.VATBaseAmount);
                        VATBaseAmount = isNaN(VATBaseAmount) ? 0 : $scope.MathRounding(VATBaseAmount);
                        var VatPercent = Number(receipt.VATPercent);
                        VatPercent = isNaN(VatPercent) ? 0 : VatPercent;
                        receipt.VATAmount = $scope.MathRounding(VATBaseAmount * VatPercent / 100);
                    };

                    $scope.updateExchangeRate = function (newObj, oldObj) {
                        // find changed
                        console.log('find changed exchangerate.');
                        var oldExchange = null;
                        var newExchange = null;
                        if (oldObj != null && newObj != null && oldObj.length == newObj.length && oldObj != newObj) {
                            // loop find changed
                            for (var i = 0; i < oldObj.length; i++) {
                                if (oldObj[i].EffectiveDate != newObj[i].EffectiveDate
                                    || oldObj[i].FromCurrency != newObj[i].FromCurrency
                                    || (oldObj[i].ExchangeRate != newObj[i].ExchangeRate)
                                    || oldObj[i].ExchangeTypeID != newObj[i].ExchangeTypeID
                                    || oldObj[i].RatioFromCurrencyUnit != newObj[i].RatioFromCurrencyUnit
                                    || oldObj[i].RatioToCurrencyUnit != newObj[i].RatioToCurrencyUnit) {

                                    oldExchange = oldObj[i];
                                    newExchange = newObj[i];
                                    break;
                                }
                            }
                        }

                        if (oldExchange != null) {
                            if (angular.isDefined($scope.ExpenseReport.ExpenseReportReceiptList) && $scope.ExpenseReport.ExpenseReportReceiptList != null) {
                                for (var i = 0; i < $scope.ExpenseReport.ExpenseReportReceiptList.length; i++) {
                                    var receipt = $scope.ExpenseReport.ExpenseReportReceiptList[i];

                                    if (receipt.IsReceipt && receipt.IsVisible
                                        && receipt.ExchangeDate == oldExchange.EffectiveDate
                                        && receipt.OriginalCurrencyCode == oldExchange.FromCurrency
                                        && receipt.ExchangeTypeID == oldExchange.ExchangeTypeID
                                        && receipt.FromCurrencyRatio == oldExchange.RatioFromCurrencyUnit
                                        && receipt.ToCurrencyRatio == oldExchange.RatioToCurrencyUnit
                                        && (receipt.ExchangeRate == oldExchange.ExchangeRate || (oldExchange.ExchangeRate == null && receipt.ExchangeRate == 0))) {
                                        // found : update exchange rate 

                                        $scope.changeReceiptCurrencyFromOutside(receipt, newExchange);

                                        //receipt.LocalTotalAmount = receipt.LocalNoVATTotalAmount + receipt.VATAmount + receipt.WHTAmount1LC + receipt.WHTAmount2LC;
                                        //Nun Add
                                        receipt.LocalTotalAmount = receipt.LocalNoVATTotalAmount + receipt.VATAmount;

                                    }
                                }

                                /* fix case when new object $scope.document in root controller */
                                $scope.document.Additional.ExpenseReportReceiptList = $scope.ExpenseReport.ExpenseReportReceiptList;
                                /* !fix case when new object $scope.document in root controller */

                                $scope.expenseItems_Receipt = $scope.getExpenseItems_Receipt();
                            }
                        }

                    };

                    $scope.$watch('masterData.travelCurrencyList', function (newObj, oldObj) {
                        
                        $scope.travelCurrencyList = angular.copy(newObj);
                        if ($scope.travelCurrencyList == null) {
                            $scope.travelCurrencyList = [];
                        }
                        if ($scope.personalCurrency != null) {
                            $scope.travelCurrencyList.unshift(angular.copy($scope.personalCurrency));
                        }

                        $scope.updateExchangeRate(newObj, oldObj);

                        console.log('-------- Receipt Update Exchange Rate --------');
                    }, true);

                    /* !exchange rate */

                    /* --- !Method --- */

                    /* ====== wizard form ====== */

                    /* ====== receipt function ====== */

                    var isAccommodationWorkFlowChangeFromReceipt = function () {
                        console.log('$$$$$$$ check accommodation in receipt $$$$$$$.', $scope.GE.receipt);
                        var isTriggerWF = false;
                        var isUnlimitRight = false;
                        var expenseAmount = 0;
                        var perdiumRight = 0;
                        var VATAmount = $scope.GE.receipt.VATAmount;
                        var IsAccommodation = false;
                        //if ($scope.GE.receipt != null) {
                        //    $.each($scope.GE.receipt.ExpenseReportReceiptItemList, function (index, receipt) {
                        //        if (angular.isDefined(receipt.IsAccommodation) && receipt.IsAccommodation) {
                        //            IsAccommodation = true;
                        //            isUnlimitRight = isUnlimitRight ? isUnlimitRight : receipt.IsUnlimitRight;
                        //            expenseAmount += receipt.LocalAccommodationAmount;
                        //            perdiumRight += receipt.LocalAccommodationRight;
                        //        }
                        //    });
                        //}
                        if ($scope.GE.receipt != null) {
                            $.each($scope.GE.receipt.ExpenseReportReceiptItemList, function (index, receipt) {
                                var receipt_temp = $scope.GE.receipt;
                                if (angular.isDefined(receipt.IsAccommodation) && receipt.IsAccommodation) {
                                    IsAccommodation = true;
                                    isUnlimitRight = isUnlimitRight ? isUnlimitRight : receipt.IsUnlimitRight;
                                    expenseAmount +=  receipt_temp.VATBaseAmount;
                                    perdiumRight += receipt.LocalAccommodationRight;
                                }
                            });
                        }

                        if (IsAccommodation)
                        {
                            isTriggerWF = !isUnlimitRight && ((expenseAmount + VATAmount) > perdiumRight);
                        }
                        var objTrggerWF = {
                            'isTriggerWF': isTriggerWF,
                            'exceedAmount': (expenseAmount + VATAmount) - perdiumRight
                        };
                        console.log('$$$$$$$ isTriggerWF $$$$$$$.', (expenseAmount + VATAmount), perdiumRight, isUnlimitRight);
                        return objTrggerWF;
                    };

                    var isNullOrEmpty = function (str) {
                        return str == null || str.trim() == '';
                    }

                    $scope.validateReceipt = function () {
                        var oValidate = {
                            isValid: true,
                            isWarning: false,
                            message: ''
                        };

                        if ($scope.document.Additional && $scope.document.Additional.length>0 && $scope.GE.receipt && $scope.GE.receipt.ExpenseReportReceiptItemList && $scope.GE.receipt.ExpenseReportReceiptItemList.length > 0)
                        {
                            var oExpenseTypeItem = [];
                            if ($scope.document.Additional == null)
                                var oPerdiumAccommutation = null;
                            else
                                var oPerdiumAccommutation = $filter('filter')($scope.document.Additional.PerdiumList, { EmployeeID : employeeData.EmployeeID, IsCheck: 'True', ExpenseRateTagCodeForPost: 'ACCOMMUTATION' });
                            $scope.GE.receipt.ExpenseReportReceiptItemList.forEach(function (currentExpense) { currentExpense.ExpenseReportReceiptItemRoommateList.forEach(function (currentRoommate) { if (currentRoommate.EmployeeID == employeeData.EmployeeID) { oExpenseTypeItem.push(currentExpense); }}) });
                            if (oPerdiumAccommutation && oPerdiumAccommutation.length > 0 && oExpenseTypeItem && oExpenseTypeItem.length > 0)
                            {
                                var queryResult = [];
                                oPerdiumAccommutation.forEach(function (currentPerdium) { oExpenseTypeItem.forEach(function (currentRoommate) { if (moment(currentPerdium.BeginDate) >= moment(currentRoommate.Date1) && moment(currentPerdium.EndDate) <= moment(currentRoommate.Date2)) { queryResult.push(currentPerdium); return; } }) });
                                if( queryResult && queryResult.length >0)
                                {
                                    oValidate.isValid = false;
                                    oValidate.message = String.format($scope.Text['EXPENSE']['INVALIDACCOMMODATION'], moment(queryResult[0].BeginDate).format('DD/MM/YYYY'));
                                    return oValidate;
                                }
                            }
                        }

                        if (($scope.GE.receipt.ReceiptTextID == null || $scope.GE.receipt.ReceiptTextID == '') && !$scope.GE.receipt.IsAutoGenerateReceiptTextID ) {
                            oValidate.isValid = false;
                            oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['TAXINVOICE'];
                            return oValidate;
                        }

                        if ($scope.GE.receipt.ReceiptDate == null || $scope.GE.receipt.ReceiptDate == '') {
                            oValidate.isValid = false;
                            oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['INVOICEDATE'];
                            return oValidate;
                        }


                        var today = new Date(new Date().setUTCHours(0, 0, 0, 0)).toJSON().slice(0, 10);
                        var myDate = new Date(new Date($scope.GE.receipt.ReceiptDate).setUTCHours(0, 0, 0, 0)).toJSON().slice(0, 10);

                        if (!$scope.GE.receipt.NolimitDate && (myDate > today)) {
                            oValidate.isValid = false;
                            oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['REQUIRED_RECEIPTDATELESSTHANTODAY'];
                            return oValidate;
                        }
                        ////

                        // add by Jirawat Jannet @ 2018-07-19
                        // add condition >>  && !$scope.GE.receipt.IsVendorOnetime
                        // เพราะมีการเพิ่มการเลือก onetime vendor เข้ามา เมื่อเลือก onetime vendor ไม่จำเป็นต้องเลือก vendor code
                        if (($scope.GE.receipt.VendorCode == null || $scope.GE.receipt.VendorCode == '') && !$scope.GE.receipt.IsVendorOnetime) {
                            oValidate.isValid = false;
                            oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['VENDORCODE'];
                            return oValidate;
                        }
                        if ($scope.GE.receipt.VendorBusinessPlace != null && $scope.GE.receipt.VendorBusinessPlace.length > 0 && $scope.GE.receipt.VendorBusinessPlace.length != 5 || isNaN($scope.GE.receipt.VendorBusinessPlace)) {
                            oValidate.isValid = false;
                            oValidate.message = $scope.Text['EXPENSE']['REQUIRED_VENDORBUSINESS_LENGTH'];
                            return oValidate;
                        }
                        if ($scope.GE.receipt.VATPercent > 0) {
                            if ($scope.GE.receipt.VendorBusinessPlace == null || $scope.GE.receipt.VendorBusinessPlace == '') {
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['VENDORBUSINESSPLACE'];
                                return oValidate;
                            }
                            if ($scope.GE.receipt.VendorBusinessPlace.length != 5 || isNaN($scope.GE.receipt.VendorBusinessPlace)) {
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['EXPENSE']['REQUIRED_VENDORBUSINESS_LENGTH'];
                                return oValidate;
                            }
                        }


                        // add by Jirawat Jannet @ 2018-07-25
                        // เพิม condition กรณี onetime vendor
                        if ($scope.GE.receipt.IsVendorOnetime) {
                            if (isNullOrEmpty($scope.GE.receipt.VendorName)) {
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['VENDORNAME'];
                                return oValidate;
                            }

                            if (isNullOrEmpty($scope.GE.receipt.VendorAddress1)) {
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['VENDORADDRESS1'];
                                return oValidate;
                            }

                            if (isNullOrEmpty($scope.GE.receipt.VendorAddress2)) {
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['VENDORADDRESS2'];
                                return oValidate;
                            }

                            if (isNullOrEmpty($scope.GE.receipt.VendorCountry)) {
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['VENDORCOUNTRY'];
                                return oValidate;
                            }

                            if (isNullOrEmpty($scope.GE.receipt.VendorCity)) {
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['VENDORPROVINCE'];
                                return oValidate;
                            }

                            if (isNullOrEmpty($scope.GE.receipt.VendorPostCode) && $scope.GE.receipt.VendorCountry == 'TH') {
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['VENDORPOSTCODE'];
                                return oValidate;
                            }

                            if (isNullOrEmpty($scope.GE.receipt.VendorTaxID) && $scope.GE.receipt.VendorCountry == 'TH') {
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['TAXID'];
                                return oValidate;
                            }
                        }

                        if ($scope.GE.receipt.VendorCode != null && $scope.GE.receipt.VendorCode.substr(0, 2) == 'PX') {
                            if ($scope.GE.receipt.VendorName == null || $scope.GE.receipt.VendorName == '') {
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['VENDORNAME'];
                                return oValidate;
                            }
                            if ($scope.GE.receipt.VendorAddress1 == null || $scope.GE.receipt.VendorAddress1 == '') {
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['VENDORADDRESS1'];
                                return oValidate;
                            }
                            if ($scope.GE.receipt.VendorCountry == null || $scope.GE.receipt.VendorCountry == '') {
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['VENDORCOUNTRY'];
                                return oValidate;
                            }
                            if ($scope.GE.receipt.VendorCity == null || $scope.GE.receipt.VendorCity == '') {
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['VENDORPROVINCE'];
                                return oValidate;
                            }

                            if ($scope.GE.receipt.VendorCountry != null && $scope.GE.receipt.VendorCountry == 'TH') {
                                if ($scope.GE.receipt.VendorTaxID != null && $scope.GE.receipt.VendorTaxID.length > 0 && $scope.GE.receipt.VendorTaxID.length != 13 || isNaN($scope.GE.receipt.VendorTaxID)) {
                                    oValidate.isValid = false;
                                    oValidate.message = $scope.Text['EXPENSE']['REQUIRED_TAXID_LENGTH'];
                                    return oValidate;
                                }
                            }
                            //Nun Add 11052017 Check ถ้าเป็น Vendor onetime TH ให้ Validate TaxID
                            if ($scope.GE.receipt.VendorCountry != null && $scope.GE.receipt.VendorCountry == 'TH') {
                                if($scope.GE.receipt.VendorTaxID == null || $scope.GE.receipt.VendorTaxID == '' || ($scope.GE.receipt.VendorTaxID.length > 0 && $scope.GE.receipt.VendorTaxID.length != 13)){
                                    oValidate.isValid = false;
                                    oValidate.message = $scope.Text['EXPENSE']['REQUIRED_TAXID_LENGTH'];
                                    return oValidate;
                                }
                            }

                            //if ($scope.objProvince && $scope.objProvince.length > 0) {
                            if ($scope.GE.receipt.VendorCountry == 'TH') {
                                if ($scope.GE.receipt.VendorPostCode == null || $scope.GE.receipt.VendorPostCode == '') {
                                    oValidate.isValid = false;
                                    oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['VENDORPOSTCODE'];
                                    return oValidate;
                                }
                                if ($scope.GE.receipt.VendorPostCode.length != 5 || isNaN($scope.GE.receipt.VendorPostCode)) {
                                    oValidate.isValid = false;
                                    oValidate.message = $scope.Text['EXPENSE']['REQUIRED_POSTCODE_LENGTH'];
                                    return oValidate;
                                }

                                //CommentBy: Ratchatawan W. (31 Aug 2017)
                                //สำหรับ Vendor one-time ต่างประเทศ ไม่จำเป็นต้องมี TaxID อยู่แล้ว ไม่จำเป็นต้องตรวจสอบ TaxID ว่าซ้ำกับ Vendor Master หรือไม่
                                //แต่ถ้าเป็น Vendor one-time ในประเทศไทย ห้ามกรอก TaxIO ซ้ำกับ Vendor Master , ห้ามเป็นค่าว่าง และต้องมีความยาว 13 ตัวอักษรเท่านั้น
                                //  if ($scope.GE.receipt.VATPercent > 0) {
                                    if ($scope.GE.receipt.VendorTaxID == null || $scope.GE.receipt.VendorTaxID == '') {
                                        oValidate.isValid = false;
                                        oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['TAXID'];
                                        return oValidate;
                                    }
                                    if ($scope.GE.receipt.VendorTaxID.length != 13 || isNaN($scope.GE.receipt.VendorTaxID)) {
                                        oValidate.isValid = false;
                                        oValidate.message = $scope.Text['EXPENSE']['REQUIRED_TAXID_LENGTH'];
                                        return oValidate;
                                    }
                                    if ($scope.tempFormData.chkVendorTaxIDDup != '') {
                                        oValidate.isValid = false;
                                        oValidate.message = $scope.Text['EXPENSE']['TAXID'] + ' ' + $scope.GE.receipt.VendorTaxID + ' ' +
                                                            $scope.Text['EXPENSE']['DUPLICATEWITH'] + $scope.Text['EXPENSE']['TAXID'] + ' ' +
                                                            $scope.Text['EXPENSE']['VENDORCODE'] + ' ' + $scope.tempFormData.chkVendorTaxIDDup;
                                        return oValidate;
                                    }
                                // } //CommentBy: Ratchatawan W. (31 Aug 2017)
                            }
                        }

                        if ($scope.GE.receipt.ExpenseReportReceiptItemList == null || $scope.GE.receipt.ExpenseReportReceiptItemList.length <= 0) {
                            oValidate.isValid = false;
                            oValidate.message = $scope.Text['SYSTEM']['PLEASE_ADD'] + $scope.Text['EXPENSE']['HEADSUBJECT_RECEIPT_ITEM'];
                            return oValidate;
                        }

                        console.log('$scope.isVATBaseAmountValid($scope.GE.receipt.VATBaseAmount))', $scope.isVATBaseAmountValid($scope.GE.receipt.VATBaseAmount));
                        if (!$scope.isVATBaseAmountValid($scope.GE.receipt.VATBaseAmount)) {
                            oValidate.isValid = false;
                            oValidate.message = $scope.Text['EXPENSE']['RECEIPT_SUM_TOLERANCE_ALLOW'] + ' ' + $filter('number')($scope.settings.ReceiptToleranceAllow, 2) + ' ' + $scope.Text['CURRENCY'][$scope.GE.receipt.LocalCurrencyCode];
                            return oValidate;
                        } else {
                            $scope.showChangeAmountWarning = false;
                        }

                        // receipt file
                        if ($scope.GE.receipt.ExpenseReportReceiptFileList == null) {
                            oValidate.isValid = false;
                            oValidate.message = $scope.Text['SYSTEM']['PLEASE_ADD'] + $scope.Text['EXPENSE']['EXPENSE_RECEIPT_FILE'];
                            return oValidate;
                        } else {
                            if ($scope.GE.receipt.ExpenseReportReceiptFileList.length <= 0) {
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['PLEASE_ADD'] + $scope.Text['EXPENSE']['EXPENSE_RECEIPT_FILE'];
                                return oValidate;
                            } else {
                                var countFile = 0;
                                for (var i = 0; i < $scope.GE.receipt.ExpenseReportReceiptFileList.length; i++) {
                                    if (!$scope.GE.receipt.ExpenseReportReceiptFileList[i].IsDelete) {
                                        countFile++;
                                    }
                                }
                                if (countFile <= 0) {
                                    oValidate.isValid = false;
                                    oValidate.message = $scope.Text['SYSTEM']['PLEASE_ADD'] + $scope.Text['EXPENSE']['EXPENSE_RECEIPT_FILE'];
                                    return oValidate;
                                }

                            }
                        }

                        if ($scope.pageClassName == 'TravelReport') {
                            // เช็ค duplicate วันที่เข้าพัก
                            //

                            // เช็คสิทธิ์ค่าที่พักทำให้ work flow เปลี่ยน
                            var objShowWarning = isAccommodationWorkFlowChangeFromReceipt();
                            if (objShowWarning.isTriggerWF) {
                                oValidate.isValid = true;
                                oValidate.isWarning = true;
                                if (angular.isDefined($scope.settings.MaximumEmpSubGroup) && $scope.settings.MaximumEmpSubGroup == $scope.document.Requestor.EmpSubGroup) {
                                    oValidate.message = $scope.Text['EXPENSE']['CHECK_ACCOMMODATION_WORKFLOW_CHANGE_MD1'] + objShowWarning.exceedAmount.toFixed(2) + $scope.Text['EXPENSE']['CHECK_ACCOMMODATION_WORKFLOW_CHANGE_MD2'];
                                } else {
                                    oValidate.message = $scope.Text['EXPENSE']['CHECK_ACCOMMODATION_WORKFLOW_CHANGE'];
                                }
                                return oValidate;
                            }
                        }

                        return oValidate;
                    };

                    $scope.AddReceiptFinish = function () {

                        var oValidate = $scope.validateReceipt();
                        if (oValidate.isValid) {
                            // validate pass

                            // warning alert
                            if (oValidate.isWarning) {
                                if (!confirm(oValidate.message)) {
                                    return;
                                }
                            }


                            if (!$scope.GE.receipt.IsAutoGenerateReceiptTextID) {

                                var tempReceiptItem = angular.copy($scope.expenseItems_Receipt);
                                if (!$scope.GE.receiptEdit) {
                                    tempReceiptItem.push($scope.GE.receipt);
                                } else {

                                    for (var i = 0; i < tempReceiptItem.length; i++) {
                                        if (tempReceiptItem[i].ReceiptID == $scope.GE.receipt.ReceiptID) {
                                            tempReceiptItem[i] = $scope.GE.receipt;
                                        }
                                    }
                                }
                                var oRequestParameter = { InputParameter: {}, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                                oRequestParameter.InputParameter = {
                                    ReceiptItems: tempReceiptItem,
                                    REQUESTNO: $scope.document.RequestNo
                                }
                                //var isDuplicateReceipt = false;
                                //validate only in this request 
                                    // 
                                for (var i = 0; i < tempReceiptItem.length; i++) {
                                    var compareDate = equalDate(tempReceiptItem[i].ReceiptDate, $scope.GE.receipt.ReceiptDate);
                                    if (tempReceiptItem[i].ReceiptTextID == $scope.GE.receipt.ReceiptTextID
                                        && tempReceiptItem[i].ReceiptID != $scope.GE.receipt.ReceiptID
                                        && tempReceiptItem[i].VendorCode == $scope.GE.receipt.VendorCode
                                        && compareDate//ReceiptDate
                                        //&& tempReceiptItem[i].LocalTotalAmount == $scope.GE.receipt.LocalTotalAmount//LocalTotalAmount
                                    ) {
                                        $scope.p_message($scope.Text['EXPENSE']['DUPLICATED_RECEIPT_TEXT_ID'], 'w');
                                        return; //isDuplicateReceipt = true;
                                    }
                                }
                                //validate in transaction
                                var URL = CONFIG.SERVER + 'HRTR/ValidateReceiptID';
                                $http({
                                    method: 'POST',
                                    url: URL,
                                    data: oRequestParameter
                                }).then(function successCallback(response) {
                                    if (response.data.length > 0) {
                                        $scope.p_message($scope.Text['EXPENSE']['DUPLICATED_RECEIPT_TEXT_ID'], 'w');
                                        return;
                                    }
                                    $scope.GoToSubMain_AddGEReceiptFinish();

                                }, function errorCallback(response) {
                                    console.debug(response);
                                });

                            } else {
                                $scope.GoToSubMain_AddGEReceiptFinish();
                            }
                            
                           
                        } else {
                            $scope.p_message(oValidate.message,'w');
                        }
                    };
                    var equalDate = function (date1, date2) {
                        var dt1 = new Date(date1);
                        var dt2 = new Date(date2);
                        var y1 = 0, y2 = 0, m1 = 0, m2 = 0, d1 = 0, d2 = 0;
                        y1 = dt1.getFullYear();
                        m1 = dt1.getMonth();
                        d1 = dt1.getDate();
                        y2 = dt2.getFullYear();
                        m2 = dt2.getMonth();
                        d2 = dt2.getDate();

                        return y1 === y2 && m1 === m2 && d1 === d2;
                    }
                    $scope.setDirectiveFn = function (directiveFn) {
                        $scope.directiveFn = directiveFn;
                    };

                    $scope.setSelectedInvoiceDate = function (selectedDate) {
                        $scope.GE.receipt.ReceiptDate = $filter('date')($scope.GE.receipt.ReceiptDate, 'yyyy-MM-ddT00:00:00');
                        console.log('invoice date.', $scope.GE.receipt.ReceiptDate);

                    };

                    $scope.SetLimitDate = function (flgUnlimit, flagChangeReceiptDate) {
                        if (angular.isUndefined(flgUnlimit)) {
                            flgUnlimit = false;
                        }
                        if (angular.isUndefined(flagChangeReceiptDate)) {
                            flagChangeReceiptDate = true;
                        }
                        if (flgUnlimit) {
                            var unlimitDateScope_MINDATE = new Date($scope.savedLimitMinDate);
                            var unlimitDateScope_MAXDATE = new Date($scope.savedLimitMaxDate);
                            if ($scope.settings.ExpenseTypeGroupInfo.prefix == 'TR') {
                                unlimitDateScope_MINDATE.setDate(unlimitDateScope_MINDATE.getDate() + ($scope.receiptLimitDate.RP_LIMIT_MINDATE * -1));
                                unlimitDateScope_MAXDATE.setDate(unlimitDateScope_MAXDATE.getDate() + ($scope.receiptLimitDate.RP_LIMIT_MAXDATE));
                             
                            } else {
                                unlimitDateScope_MINDATE.setDate(unlimitDateScope_MINDATE.getDate() + ($scope.receiptLimitDate.GE_LIMIT_MINDATE * -1));
                                unlimitDateScope_MAXDATE.setDate(unlimitDateScope_MAXDATE.getDate() + ($scope.receiptLimitDate.GE_LIMIT_MAXDATE));
                            }
                           
                            $scope.limitMinDate = $filter('date')(unlimitDateScope_MINDATE, 'yyyy-MM-dd');
                            $scope.limitMaxDate = $filter('date')(unlimitDateScope_MAXDATE, 'yyyy-MM-dd');
                        } else {
                            $scope.limitMinDate = angular.copy($scope.savedLimitMinDate);
                            $scope.limitMaxDate = angular.copy($scope.savedLimitMaxDate);

                            if (angular.isDefined($scope.GE.receipt) && $scope.GE.receipt != null && flagChangeReceiptDate) {
                                //$scope.directiveFn($scope.savedLimitMinDate);
                                $scope.GE.receipt.ReceiptDate = $scope.savedLimitMinDate;
                            }
                        }
                    };

                    /* ====== !receipt function ====== */

                    /* ====== receipt item function ====== */

                    $scope.calculateAmountReceiptItem = function () {
                        var sum = 0;
                        angular.forEach($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList, function (item) {
                            var amount = Number(item.OriginalAmount);
                            sum += isNaN(amount) ? 0 : amount;
                        });
                        //angular.forEach($scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList, function (item) {
                        //    sum += Number(item.UsedAmount);
                        //});

                        $scope.GE.receiptItem.Amount = sum;
                    };

                    $scope.validateReceiptItem = function () {
                        var oValidate = {
                            isValid: true,
                            isWarning: false,
                            message: ''
                        };
                        if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE) {
                            if ($scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList && $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.length > 1) {
                                var oRoommateZeroAmount = angular.copy($scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList);
                                for (var i = 0; i < oRoommateZeroAmount.length; i++) {
                                    if (oRoommateZeroAmount[i].UsedAmount <= 0) {
                                        oValidate.isValid = false;
                                        oValidate.message = $scope.Text['EXPENSE']['ROOMMATEZEROAMOUNT'];
                                        break;
                                    }
                                }
                                if (oValidate.isValid == false)
                                    return oValidate;
                            }
                        }
                        if ($scope.GE.receiptItem.ExpenseTypeGroupID == null || $scope.GE.receiptItem.ExpenseTypeGroupID < 1) {
                            oValidate.isValid = false;
                            oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['EXPENSECATEGORY'];
                            return oValidate;
                        }
                        if ($scope.GE.receiptItem.ExpenseTypeID == null || $scope.GE.receiptItem.ExpenseTypeID < 1) {
                            oValidate.isValid = false;
                            oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['EXPENSETYPE'];
                            return oValidate;
                        }
                        if ($scope.GE.receiptItem.Detail == null || $scope.GE.receiptItem.Detail == '') {
                            oValidate.isValid = false;
                            oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['EXPENSE_DETAIL'];
                            return oValidate;
                        }
                        if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {       
                            // none, average

                            if ($scope.settings.ProjectCodeMode != $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                                if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].CostCenter == null || $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].CostCenter == '') {
                                    oValidate.isValid = false;
                                    oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['COST_CENTER'];
                                    return oValidate;
                                }
                                if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].IO == null || $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].IO == '') {
                                    oValidate.isValid = false;
                                    oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['IO'];
                                    return oValidate;
                                }
                            }

                            if ($scope.GE.receiptItemFormData.Amount == null || $scope.GE.receiptItemFormData.Amount.toString() == '') {
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['AMOUNT_NO_VAT'];
                                return oValidate;
                            }
                            if (!isFinite($scope.GE.receiptItemFormData.Amount.toString())) {
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['INVALID_NUMBER'];
                                return oValidate;
                            } else if ($scope.GE.receiptItemFormData.Amount <= 0) {
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['EXPENSE']['AMOUNT_NOT_ZERO'];
                                return oValidate;
                            }
                        } else {
                            // exactly
                            if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList == null || $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList.length < 1) {
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['PLEASE_ADD'] + $scope.Text['EXPENSE']['RECEIPT_ITEM_DETAIL'];
                                return oValidate;
                            }
                        }

                        /* Guest validation */
                        if ($scope.panelName == 'pnlEntertainment') {
                            var isValid = true;

                            $.each($scope.GE.receiptItem.ExpenseReportReceiptItemGuestList, function (i, guest) {
                                if (!guest.IsEmployee && ($.trim(guest.EmployeeName) == '' || $.trim(guest.OrgUnitName) == '' || $.trim(guest.CompanyName) == '')) {
                                    isValid = false;
                                    return false;
                                }
                            });
                            if (!isValid) {
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['EXPENSE']['GUEST_OTHER_EMPTY_NAME'];
                                return oValidate;
                            }

                            else if ($scope.pnlRequireUploadFile) {
                                var countGuest = 0;
                                var countFile = 0;
                                countGuest = $scope.GE.receiptItem.ExpenseReportReceiptItemGuestList.length;
                                countFile = $scope.GE.receiptItem.ExpenseReportReceiptItemGuestFileList.filter(function (x) { return !x.IsDelete }).length;
                                isValid = countGuest > 0 || countFile > 0;
                                if (!isValid) {
                                    oValidate.isValid = false;
                                    //oValidate.message = $scope.Text['EXPENSE'][$scope.GE.receiptItem.ExpenseTypeGroupID == 13 ? 'GUEST_OTHER_DATA_INCOMPLETE' : 'GUEST_OTHER_DATA_INCOMPLETE'];
                                    oValidate.message = $scope.Text['EXPENSE']['GUEST_ENTERTAINMENT_INCOMPLETE_' + $scope.GE.receiptItem.ExpenseTypeGroupID];
                                    return oValidate;
                                }
                            }
                        }
                        /* !Guest validation */
                        else if ($scope.panelName == 'pnlTransportation') {
                            var isValid = true;
                            if ($scope.pnlRequireUploadFile) {
                                var countFile = 0;
                                countFile = $scope.GE.receiptItem.ExpenseReportReceiptItemGuestFileList.filter(function (x) { return !x.IsDelete }).length;
                                isValid = countFile >= $scope.pnlNoOfFile;
                                if (!isValid) {
                                    oValidate.isValid = false;
                                    oValidate.message = $scope.Text['EXPENSE']['TRANSPORTAION_ATTACHMENT_REQUIRE'];
                                    return oValidate;
                                }
                            }
                        }

                        /* Panel validation */
                        else if ($scope.panelName == 'pnlAccommodation') {
                            // ค่าที่พัก
                            if ($scope.GE.receiptItem.Date1 == null || $scope.GE.receiptItem.Date1 == '') {
                                // วันที่เข้าพัก
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['CHECK_IN_DATE'];
                                return oValidate;
                            }
                            if ($scope.GE.receiptItem.Date2 == null || $scope.GE.receiptItem.Date2 == '') {
                                // วันที่ออก
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['CHECK_OUT_DATE'];
                                return oValidate;
                            }
                            if (new Date($scope.GE.receiptItem.Date1.substr(0, 10)) >= new Date($scope.GE.receiptItem.Date2.substr(0, 10))) {
                                // วันที่
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['EXPENSE']['CHECK_IN_CHECK_OUT_DATE_INVALID'];
                                return oValidate;
                            }
                            if ($scope.GE.receiptItem.Str1 == null || $scope.GE.receiptItem.Str1 == '') {
                                // ชื่อโรงแรม
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['HOTEL_NAME'];
                                return oValidate;
                            }

                            var isAccommodationWorkFlowChange = function () {
                                // เช็ครายจ่ายค่าที่พักเกินสิทธิ์
                                // $scope.GE.receiptItem.Date1
                                // $scope.GE.receiptItem.Date2

                                console.log('$$$$$$$ check accommodation self perdium $$$$$$$.', $scope.document.Additional.PerdiumList);
                                console.log('$$$$$$$ check accommodation roommate $$$$$$$.', $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList);

                                var isTriggerWF = false;
                                var expenseAmount = 0;
                                var perdiumRight = 0;
                                var isUnlimitRight = false;
                                if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                                    expenseAmount = $scope.GE.receiptItemFormData.Amount;
                                } else {
                                    expenseAmount = $scope.GE.receiptItem.Amount;
                                }

                                //ถ้าหากผู้มาขอ มีสิทธิ์เป็น IsUnlimit ไม่ต้องตรวจสอบยอดเงินของผู้ร่วมเดินทาง
                                if ($scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList != null) {
                                    $.each($scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList, function (index, roommate) {
                                        if (roommate.IsUnlimit)
                                        {
                                            isUnlimitRight = true;
                                        }
                                        perdiumRight += roommate.AmountLimit;
                                    });
                                }
                                
                                //AddBy: Ratchatawan W. 2016-09-23 Convert currency to THB
                                if ($scope.GE.receipt.OriginalCurrencyCode != $scope.GE.receipt.LocalCurrencyCode) {
                                    //expenseAmount = (expenseAmount * $scope.GE.receipt.ExchangeRate) / $scope.GE.receipt.FromCurrencyRatio;
                                    expenseAmount = $scope.multiplyExchangeRate(expenseAmount, $scope.GE.receipt.ExchangeRate, $scope.GE.receipt.FromCurrencyRatio, $scope.GE.receipt.ToCurrencyRatio);

                                    // add VAT
                                    //expenseAmount += $scope.GE.receipt.VATAmount;
                                }

                                isTriggerWF = (expenseAmount > perdiumRight);
                                var objTrggerWF = {
                                    'isTriggerWF': isTriggerWF,
                                    'exceedAmount': expenseAmount - perdiumRight
                                };
                                // console.log('$$$$$$$ isTriggerWF $$$$$$$.', expenseAmount, perdiumRight, isUnlimitRight);

                                // accommodation right
                                $scope.GE.receiptItem.IsAccommodation = true;
                                $scope.GE.receiptItem.LocalAccommodationAmount = expenseAmount;
                                $scope.GE.receiptItem.Amount = expenseAmount;
                                $scope.GE.receiptItem.LocalAccommodationRight = perdiumRight;
                                $scope.GE.receiptItem.IsUnlimitRight = isUnlimitRight;

                                return objTrggerWF;
                            };

                            if ($scope.pageClassName == 'TravelReport') {
                                // เช็ค duplicate วันที่เข้าพัก
                                //

                                if ($scope.TravelersforAccommodation_Buffer.length == 0) {
                                    $mdDialog.show(
                                     $mdDialog.alert()
                                       .clickOutsideToClose(false)
                                       .title($scope.Text['SYSTEM']['INFORMATION'])
                                       .textContent($scope.Text['EXPENSE']['DUPLICATE_ACCOMMODATION'])
                                       .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                                   );
                                    return null;
                                }
                                else {
                                    // เช็คสิทธิ์ค่าที่พักทำให้ work flow เปลี่ยน
                                    var objShowWarning = isAccommodationWorkFlowChange();
                                    if (objShowWarning.isTriggerWF) {
                                        oValidate.isValid = true;
                                        oValidate.isWarning = true;
                                        if (angular.isDefined($scope.settings.MaximumEmpSubGroup) && $scope.settings.MaximumEmpSubGroup == $scope.document.Requestor.EmpSubGroup) {
                                            oValidate.message = $scope.Text['EXPENSE']['CHECK_ACCOMMODATION_WORKFLOW_CHANGE_MD1'] + objShowWarning.exceedAmount.toFixed(2) + $scope.Text['EXPENSE']['CHECK_ACCOMMODATION_WORKFLOW_CHANGE_MD2'];
                                        } else {
                                            oValidate.message = $scope.Text['EXPENSE']['CHECK_ACCOMMODATION_WORKFLOW_CHANGE'];
                                        }
                                        return oValidate;
                                    }
                                }
                            }

                            //Nun Add 26082016
                        } else if ($scope.panelName == 'pnlVehicleRent') {
                            // ค่ายานพาหนะ
                            if ($scope.GE.receiptItem.Date1 == null || $scope.GE.receiptItem.Date1 == '') {
                                // วันที่เริ่มต้น
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['START_DATE'];
                                return oValidate;
                            }
                            if ($scope.GE.receiptItem.Date2 == null || $scope.GE.receiptItem.Date2 == '') {
                                // วันที่สิ้นสุด
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['END_DATE'];
                                return oValidate;
                            }
                            if (new Date($scope.GE.receiptItem.Date1.substr(0, 10)) > new Date($scope.GE.receiptItem.Date2.substr(0, 10))) {
                                // วันที่
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['EXPENSE']['START_END_DATE_INVALID'];
                                return oValidate;
                            }


                        } else if ($scope.panelName == 'pnlVehicleRentAndCarTypeLicense') {
                            // ค่ายานพาหนะ
                            if ($scope.GE.receiptItem.Date1 == null || $scope.GE.receiptItem.Date1 == '') {
                                // วันที่เริ่มต้น
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['START_DATE'];
                                return oValidate;
                            }
                            if ($scope.GE.receiptItem.Date2 == null || $scope.GE.receiptItem.Date2 == '') {
                                // วันที่สิ้นสุด
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['END_DATE'];
                                return oValidate;
                            }
                            if (new Date($scope.GE.receiptItem.Date1.substr(0, 10)) > new Date($scope.GE.receiptItem.Date2.substr(0, 10))) {
                                // วันที่
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['EXPENSE']['START_END_DATE_INVALID'];
                                return oValidate;
                            }
                            if ($scope.GE.receiptItem.Date1 == null || $scope.GE.receiptItem.Date1 == '') {
                                // วันที่เริ่มต้น
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['START_DATE'];
                                return oValidate;
                            }
                            if ($scope.GE.receiptItem.Str2 == null || $scope.GE.receiptItem.Str2 == '') {
                                // Car Type
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['CAR_TYPE'];
                                return oValidate;
                            }
                            if ($scope.GE.receiptItem.Str1 == null || $scope.GE.receiptItem.Str1 == '') {
                                // LICENSE_PLATE
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['LICENSE_PLATE'];
                                return oValidate;
                            }


                        } else if ($scope.panelName == 'pnlTransRegistration') {
                            // ค่าน้ำมันแบบมีใบเสร็จ
                            if ($scope.GE.receiptItem.Bit1 == null || $scope.GE.receiptItem.Bit1 === '' || $scope.GE.receiptItem.Bit2 == null || $scope.GE.receiptItem.Bit2 === '' || $scope.GE.receiptItem.Bit1 == $scope.GE.receiptItem.Bit2) {
                                // การใช้รถยนต์
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['CAR_USAGE'];
                                return oValidate;
                            }
                            if ($scope.GE.receiptItem.Str1 == null || $scope.GE.receiptItem.Str1 == '') {
                                // ทะเบียนรถ
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['LICENSE_PLATE'];
                                return oValidate;
                            }
                            if ($scope.GE.receiptItem.Str2 == null || $scope.GE.receiptItem.Str2 == '') {
                                // ประเภทรถ
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['CAR_TYPE'];
                                return oValidate;
                            }
                        } else if ($scope.panelName == 'pnlMileage') {
                            // ค่าพาหนะ Mileage

                            if ($scope.GE.receiptItem.Str1 == null || $scope.GE.receiptItem.Str1 == '') {
                                // สถานที่เริ่มต้น
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['LOCATION_START'];
                                return oValidate;
                            }
                            if ($scope.GE.receiptItem.Str2 == null || $scope.GE.receiptItem.Str2 == '') {
                                // สถานที่สิ้นสุด
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['LOCATION_END'];
                                return oValidate;
                            }
                            if ($scope.GE.receiptItem.Dec1 == null || $scope.GE.receiptItem.Dec1.toString() == '' || $scope.GE.receiptItem.Dec1.toString() == '0') {
                                // ระยะทาง
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['DISTANCE'];
                                return oValidate;
                            }
                            if (!isFinite($scope.GE.receiptItem.Dec1.toString())) {
                                // ระยะทาง
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['INVALID_NUMBER'] + ' (' + $scope.Text['EXPENSE']['DISTANCE'] + ' )';
                                return oValidate;
                            }
                            // Check Sum Amount
                            if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                                // none, average
                            } else {
                                // exactly
                                var sumAmount = 0;
                                if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList != null) {
                                    for (var i = 0; i < $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList.length; i++) {
                                        var amount = Number($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[i].OriginalAmount);
                                        sumAmount += isNaN(amount) ? 0 : amount;
                                    }
                                }
                                if ($scope.GE.receiptItemFormData.Amount != sumAmount) {
                                    oValidate.isValid = false;
                                    oValidate.message = $scope.Text['EXPENSE']['INPUT_AMOUNT_NOT_EQUAL'];
                                    return oValidate;
                                }
                            }
                        } else if ($scope.panelName == 'pnlTransCommutation') {
                            // ค่าพาหนะเหมาจ่าย
                            //if ($scope.GE.receiptItem.Date1 == null || $scope.GE.receiptItem.Date1 == '') {
                            //    // วันที่เดินทาง
                            //    oValidate.isValid = false;
                            //    oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['DATE_DEPART'];
                            //    return oValidate;
                            //}
                            if ($scope.GE.receiptItem.Bit1 == null || $scope.GE.receiptItem.Bit1 === '' || $scope.GE.receiptItem.Bit2 == null || $scope.GE.receiptItem.Bit2 === '' || $scope.GE.receiptItem.Bit3 == null || $scope.GE.receiptItem.Bit3 === ''
                                || ($scope.GE.receiptItem.Bit1 == $scope.GE.receiptItem.Bit2 && $scope.GE.receiptItem.Bit1 == $scope.GE.receiptItem.Bit3)) {
                                // เที่ยวการเดินทาง
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['TRAVEL_ROUND_TYPE'];
                                return oValidate;
                            }
                            if ($scope.GE.receiptItem.Str1 == null || $scope.GE.receiptItem.Str1 == '') {
                                // สถานที่เริ่มต้น
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['LOCATION_START'];
                                return oValidate;
                            }
                            if ($scope.GE.receiptItem.Str2 == null || $scope.GE.receiptItem.Str2 == '') {
                                // สถานที่สิ้นสุด
                                oValidate.isValid = false;
                                oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['LOCATION_END'];
                                return oValidate;
                            }
                            // Check Sum Amount
                            if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                                // none, average
                            } else {
                                // exactly
                                var sumAmount = 0;
                                if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList != null) {
                                    for (var i = 0; i < $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList.length; i++) {
                                        var amount = Number($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[i].OriginalAmount);
                                        sumAmount += isNaN(amount) ? 0 : amount;
                                    }
                                }
                                if ($scope.GE.receiptItemFormData.Amount != sumAmount) {
                                    oValidate.isValid = false;
                                    oValidate.message = $scope.Text['EXPENSE']['INPUT_AMOUNT_NOT_EQUAL'];
                                    return oValidate;
                                }
                            }
                        }
                        return oValidate;
                    };

                    $scope.getCarTypeDescription = function (arr, id) {
                        for (var i = 0; i < arr.length; i++) {
                            if (arr[i].CarTypeID == id) {

                                return arr[i].Description;
                            }
                        }
                        return '';
                    }

                    $scope.sendAddReceiptItemFinish2 = function () {

                        // validate pass
                        if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                            $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].OriginalAmount = $scope.GE.receiptItemFormData.Amount;
                            $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].LocalAmount = $scope.GE.receiptItemFormData.Amount;
                            $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].OriginalAmountInRight = $scope.GE.receiptItemFormData.AmountInRight;
                            if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].OriginalAmountInRight != 9999999999) {
                                var exchangeRate = Number($scope.GE.receipt.ExchangeRate);
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].LocalAmountInRight = $scope.multiplyExchangeRate($scope.GE.receiptItemFormData.AmountInRight, exchangeRate, $scope.GE.receipt.FromCurrencyRatio, $scope.GE.receipt.ToCurrencyRatio);
                            } else {
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].LocalAmountInRight = 9999999999;
                            }


                            $scope.GoToSubMain_AddGEReceiptItemFinish();
                        } else {
                            // exactly
                            $scope.GoToSubMain_AddGEReceiptItemFinish();
                        }
                        // clear form data

                        // -- gen data for table
                        $scope.expenseItems_ReceiptItem = $scope.getExpenseItems_ReceiptItem();
                        // -- !gen data for table
                    };

                    $scope.sendAddReceiptItemFinish = function () {

                        //Nun Add 26082016
                        if ($scope.panelName == 'pnlVehicleRent' || $scope.panelName == 'pnlVehicleRentAndCarTypeLicense') {
                            $scope.calDiffDateDuration();
                        }
                        //

                        if ($scope.panelName == 'pnlAccommodation') {
                            // remove empty roommate
                            if (angular.isDefined($scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList)) {
                                for (var i = $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.length - 1; i >= 0; i--) {
                                    if ($scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[i].EmployeeID == '') {
                                        $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.splice(i, 1);
                                    }
                                }
                            }
                            var SchedulePlace = [];

                            if ($scope.settings.ExpenseTypeGroupInfo.prefix == "GE_RefTravel") {
                                SchedulePlace = $scope.document.Additional.TravelSchedulePlaces;
                            }
                            else {
                                if ($scope.document.Additional) {
                                    SchedulePlace = $scope.document.Additional.TravelSchedulePlaces;
                                } else {
                                    SchedulePlace = [];
                                }
                            }
                            var oRequestParameter = { InputParameter: {}, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                            oRequestParameter.InputParameter = {
                                BeginDate: $scope.GE.receiptItem.Date1,
                                EndDate: $scope.GE.receiptItem.Date2,
                                RequestNo: $scope.document.RequestNo,
                                TravelSchedulePlace: SchedulePlace
                            }
                            var URL = CONFIG.SERVER + 'HRTR/ValidateAccommodation';
                            $http({
                                method: 'POST',
                                url: URL,
                                data: oRequestParameter
                            }).then(function successCallback(response) {
                                console.debug(response);
                                if (response.data == 'success') {

                                    // loop this receipt to check accommodation
                                    var accomExpenseType = $scope.GE.receiptItem.ExpenseTypeID;
                                    var isExist = false;
                                    var p_BeginDate = new Date($scope.GE.receiptItem.Date1.substr(0, 10)).getTime();
                                    var p_EndDate = new Date($scope.GE.receiptItem.Date2.substr(0, 10));
                                    p_EndDate.setDate(p_EndDate.getDate() - 1); // add -1 day
                                    p_EndDate = p_EndDate.getTime();
                                    $.each($scope.GE.receipt.ExpenseReportReceiptItemList, function (index, receiptItem) {
                                        var beginDate = new Date(receiptItem.Date1.substr(0, 10)).getTime();
                                        var endDate = new Date(receiptItem.Date2.substr(0, 10)).getTime();
                                        if (receiptItem != $scope.GE.receiptItemEditObj && receiptItem.ExpenseTypeID == accomExpenseType
                                            && ((p_BeginDate <= beginDate && p_EndDate >= endDate)
                                            || (p_BeginDate >= beginDate && p_BeginDate < endDate)
                                            || (p_EndDate >= beginDate && p_EndDate < endDate)
                                            )) {
                                            isExist = true;
                                            return false; // break $.each
                                        }
                                    });

                                    if (isExist) {
                                        $scope.p_message($scope.Text['EXPENSE']['DUPLICATE_ACCOMMODATION'],'w');
                                        return;
                                    }
                                    isExist = false;
                                    console.log('loop self this receipt success.');

                                    // loop self other receipt to check accommodation ($scope.ExpenseReport.ExpenseReportReceiptList)
                                    $.each($scope.ExpenseReport.ExpenseReportReceiptList, function (index, receipt) {
                                        if (receipt.IsReceipt && receipt.IsVisible && receipt != $scope.GE.receiptEditObj) {
                                            $.each(receipt.ExpenseReportReceiptItemList, function (index2, receiptItem) {
                                                var beginDate = new Date(receiptItem.Date1.substr(0, 10)).getTime();
                                                var endDate = new Date(receiptItem.Date2.substr(0, 10)).getTime();
                                                if (receiptItem != $scope.GE.receiptItemEditObj && receiptItem.ExpenseTypeID == accomExpenseType
                                                    && ((p_BeginDate <= beginDate && p_EndDate >= endDate)
                                                    || (p_BeginDate >= beginDate && p_BeginDate < endDate)
                                                    || (p_EndDate >= beginDate && p_EndDate < endDate)
                                                    )) {
                                                    isExist = true;
                                                    return false; // break $.each
                                                }
                                            });
                                            if (isExist) {
                                                return false; // break $.each
                                            }
                                        }
                                    });

                                    

                                    if (!isExist) {
                                        console.log('loop self other receipt success.');
                                        //console.log('loop roommate other receipt success.');
                                        $scope.sendAddReceiptItemFinish2();
                                    } else {
                                        $scope.p_message($scope.Text['EXPENSE']['DUPLICATE_ACCOMMODATION'],'w');
                                    }

                                } else {
                                    var error_text = response.data.split('|')[1];
                                    $scope.p_message($scope.Text['EXPENSE'][error_text], 'w');
                                }
                            }, function errorCallback(response) {
                                console.debug(response);
                                $scope.p_message('Something gone wrong please contract administrator','w');
                            });
                        } else {
                            $scope.sendAddReceiptItemFinish2();
                        }


                    };

                    function filterForAccommodationAndOffshorenight() {
                        return function filterFn(x) {
                            if (!x) return false;
                            return x.EmployeeID == employeeData.EmployeeID && x.IsCheck == true && (x.ExpenseRateTagCodeForPost == 'ACCOMMUTATION' || x.ExpenseRateTagCodeForPost == 'OFFSHORENIGHT');
                        };
                    }

                    $scope.AddReceiptItemFinish = function () {
                        if ($scope.panelName == 'pnlAccommodation') {
                            if ($scope.document.Additional && $scope.document.Additional.length > 0) {
                                var oExpenseTypeItem = [];
                                if ($scope.document.Additional == null)
                                    var oFilteredPerdium = null;//oPerdiumAccommutation = null;
                                else
                                    var oFilteredPerdium = angular.copy($scope.document.Additional.PerdiumList.filter(filterForAccommodationAndOffshorenight()));//oPerdiumAccommutation = $filter('filter')($scope.document.Additional.PerdiumList, { EmployeeID: employeeData.EmployeeID, IsCheck: 'True', ExpenseRateTagCodeForPost: 'ACCOMMUTATION' });
                                var oExpenseTypeItem = $scope.GE.receiptItem;
                                if (oFilteredPerdium && oFilteredPerdium.length > 0 && oExpenseTypeItem) {
                                    var queryResult = [];
                                    //Edit By Nipon Supap 05-02-2018 (BUG069)
                                    //oFilteredPerdium.forEach(function (currentPerdium) { { if (moment(currentPerdium.BeginDate) >= moment(oExpenseTypeItem.Date1) && moment(currentPerdium.EndDate) <= moment(oExpenseTypeItem.Date2)) { queryResult.push(currentPerdium); return; } } });
                                    oFilteredPerdium.forEach(function (currentPerdium) { { if (moment(currentPerdium.BeginDate).format('DD/MM/YYYY') >= moment(oExpenseTypeItem.Date1).format('DD/MM/YYYY') && moment(currentPerdium.EndDate).format('DD/MM/YYYY') <= moment(oExpenseTypeItem.Date2).format('DD/MM/YYYY')) { queryResult.push(currentPerdium); return; } } });
                                    var oValidate = {
                                        isValid: true,
                                        isWarning: false,
                                        message: ''
                                    };
                                    if (queryResult && queryResult.length > 0) {

                                        //oValidate.isValid = false;
                                        //oValidate.message = String.format($scope.Text['EXPENSE']['INVALIDACCOMMODATION'], moment(queryResult[0].BeginDate).format('DD/MM/YYYY'));
                                        //$scope.p_message(oValidate.message + '<br/>', 'w'); return;
                                        var date = String.format(moment(oExpenseTypeItem.Date1).format('DD/MM/YYYY')) + ' - ' + String.format(moment(oExpenseTypeItem.Date2).format('DD/MM/YYYY'));
                                        var perdiumdate = '';
                                        for (var i = 0; i < queryResult.length; i++) {
                                            if (perdiumdate) perdiumdate = perdiumdate + ', ';
                                            perdiumdate = perdiumdate + String.format(moment(queryResult[i].BeginDate).format('DD/MM/YYYY'));
                                        }
                                        var message = String.format($scope.Text['EXPENSE']['INVALIDACCOMMODATIONANDOFFSHORENIGHT'], date, perdiumdate);
                                        $scope.p_message(message, 'w'); return;
                                    }
                                }
                            }
                            validateCreatingRule_before();
                            return;
                        }
                        var oValidate = $scope.validateReceiptItem();
                        if (oValidate == null)
                        {
                            return;
                        }
                        if (!oValidate.isValid) {
                            $scope.p_message(oValidate.message,'w');
                            return;
                        }
                    
                        if ($scope.pageClassName == 'GeneralExpenseReport') {
                            validateCreatingRule();
                            $scope.original_vatAmount();
                            return;
                        }

                        if ($scope.document.Additional.TravelSchedulePlaces.length == 0) {
                            $scope.p_message('โปรดเลือกสถานที่ที่จะเดินทางในหน้าหลักก่อนทำรายการในหน้านี้','w');
                            return;
                        }

                        if (oValidate.isWarning) {
                            if (!confirm(oValidate.message)) {
                                return;
                            }
                        }
                        /* Entertainment */

                        var arrGroupTypeID = [];

                        // this receipt exclude this receiptItem
                        $.each($scope.GE.receipt.ExpenseReportReceiptItemList, function (index, receiptItem) {
                            if (receiptItem != $scope.GE.receiptItemEditObj) {
                                if (arrGroupTypeID.indexOf(receiptItem.ExpenseTypeGroupID.toString()) == -1) {
                                    arrGroupTypeID.push(receiptItem.ExpenseTypeGroupID.toString());
                                }
                            }
                        });

                        // all receipt exclude this receipt
                        $.each($scope.ExpenseReport.ExpenseReportReceiptList, function (index, receipt) {
                            if (receipt.IsVisible && receipt != $scope.GE.receiptEditObj) {
                                $.each(receipt.ExpenseReportReceiptItemList, function (index2, receiptItem) {
                                    if (receiptItem != $scope.GE.receiptItemEditObj) {
                                        if (arrGroupTypeID.indexOf(receiptItem.ExpenseTypeGroupID.toString()) == -1) {
                                            arrGroupTypeID.push(receiptItem.ExpenseTypeGroupID.toString());
                                        }
                                    }
                                });
                            }
                        });

                        var objGroupTypeExpense = {}
                        $.each(arrGroupTypeID, function (ii, groupTypeID) {
                            var sumAmount = 0;

                            // this receipt exclude this receiptItem
                            $.each($scope.GE.receipt.ExpenseReportReceiptItemList, function (index, receiptItem) {
                                if (receiptItem != $scope.GE.receiptItemEditObj && receiptItem.ExpenseTypeGroupID.toString() == groupTypeID.toString()) {
                                    //LocalAmount
                                    $.each(receiptItem.ExpenseReportReceiptItemDetailList, function (index2, receiptItemDetail) {
                                        sumAmount += receiptItemDetail.LocalAmount;
                                    });
                                }
                            });

                            // all receipt exclude this receipt
                            $.each($scope.ExpenseReport.ExpenseReportReceiptList, function (index, receipt) {
                                if (receipt.IsVisible && receipt != $scope.GE.receiptEditObj) {
                                    $.each(receipt.ExpenseReportReceiptItemList, function (index2, receiptItem) {
                                        if (receiptItem != $scope.GE.receiptItemEditObj && receiptItem.ExpenseTypeGroupID.toString() == groupTypeID.toString()) {
                                            //LocalAmount
                                            $.each(receiptItem.ExpenseReportReceiptItemDetailList, function (index3, receiptItemDetail) {
                                                sumAmount += receiptItemDetail.LocalAmount;
                                            });
                                        }
                                    });
                                }
                            });

                            objGroupTypeExpense[groupTypeID.toString()] = sumAmount;
                        });

                        /* !Entertainment */

                        // valid limit of amount
                        validateCreatingRule();


                    };

                    
                    function validateCreatingRule() {

                        var oRequestParameter = { InputParameter: {}, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                        var EXAmountDictionary = [];
                        if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                            EXAmountDictionary.push($scope.GE.receiptItem.ExpenseTypeGroupID + ":" + $scope.GE.receiptItemFormData.Amount);
                        } else {
                            EXAmountDictionary.push($scope.GE.receiptItem.ExpenseTypeGroupID + ":" + $scope.GE.receiptItem.Amount);
                        }
                        var SchedulePlace = [];

                        if ($scope.settings.ExpenseTypeGroupInfo.prefix == "GE_RefTravel") {
                            SchedulePlace = $scope.document.Additional.TravelSchedulePlaces;
                        }
                        else {
                            if ($scope.document.Additional) {
                                SchedulePlace = $scope.document.Additional.TravelSchedulePlaces;

                            } else {
                                SchedulePlace = [];
                            } 
                        }

                        oRequestParameter.InputParameter = {
                            RequestNo: $scope.document.RequestNo,
                            EXAmountDictionary: EXAmountDictionary,
                            SchedulePlaceList: SchedulePlace,
                            EmployeeID: $scope.document.Requestor.EmployeeID,
                            ReferRequestNo: $scope.document.ReferRequestNo,
                            OriginalCurrencyCode: $scope.GE.receipt.OriginalCurrencyCode,
                            ExchangeRate: $scope.GE.receipt.ExchangeRate,
                        }
                        var URL = CONFIG.SERVER + 'HRTR/ValidateExpenseTypeGroupCreatingRule';
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            if (response.data == '') {
                                $scope.sendAddReceiptItemFinish();
                            } else {
                                $scope.p_message(response.data, 'w');
                            }
                        }, function errorCallback(response) {
                            console.debug(response);
                            $scope.p_message('Something gone wrong please contract administrator', 'w');
                        });
                    }

                    function validateCreatingRule_before() {

                        var oRequestParameter = { InputParameter: {}, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                        var EXAmountDictionary = [];
                        if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                            EXAmountDictionary.push($scope.GE.receiptItem.ExpenseTypeGroupID + ":" + $scope.GE.receiptItemFormData.Amount);
                        } else {
                            EXAmountDictionary.push($scope.GE.receiptItem.ExpenseTypeGroupID + ":" + $scope.GE.receiptItem.Amount);
                        }
                        var SchedulePlace = [];

                        if ($scope.settings.ExpenseTypeGroupInfo.prefix == "GE_RefTravel") {
                            SchedulePlace = $scope.document.Additional.TravelSchedulePlaces;
                        }
                        else {
                            if ($scope.document.Additional) {
                                SchedulePlace = $scope.document.Additional.TravelSchedulePlaces;

                            } else {
                                SchedulePlace = [];
                            }
                        }
                        oRequestParameter.InputParameter = {
                            RequestNo: $scope.document.RequestNo,
                            EXAmountDictionary: EXAmountDictionary,
                            SchedulePlaceList: SchedulePlace,
                            ReferRequestNo: $scope.document.ReferRequestNo,
                            EmployeeID: $scope.document.Requestor.EmployeeID,
                            OriginalCurrencyCode: $scope.GE.receipt.OriginalCurrencyCode,
                            ExchangeRate: $scope.GE.receipt.ExchangeRate,
                        }
                        var URL = CONFIG.SERVER + 'HRTR/ValidateExpenseTypeGroupCreatingRule';
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            if (response.data == '') {
                                validateCreatingRule_before_sub_1();
                            } else {
                                $scope.p_message(response.data, 'w');
                            }
                        }, function errorCallback(response) {
                            console.debug(response);
                            $scope.p_message('Something gone wrong please contract administrator', 'w');
                        });
                        
                        function validateCreatingRule_before_sub_1() {

                            var oValidate = $scope.validateReceiptItem();
                            if (oValidate == null) {
                                return;
                            }
                            if (!oValidate.isValid) {
                                $scope.p_message(oValidate.message, 'w');
                                return;
                            }

                            if ($scope.pageClassName == 'GeneralExpenseReport') {
                                validateCreatingRule();
                                $scope.original_vatAmount();
                                return;
                            }

                            if ($scope.document.Additional.TravelSchedulePlaces.length == 0) {
                                $scope.p_message('โปรดเลือกสถานที่ที่จะเดินทางในหน้าหลักก่อนทำรายการในหน้านี้', 'w');
                                return;
                            }

                            if (oValidate.isWarning) {
                                if (!confirm(oValidate.message)) {
                                    return;
                                }
                            }
                           
                            $scope.sendAddReceiptItemFinish();
                        }
                    }

                    /* ====== !receipt item function ====== */

                    /* ====== receipt item detail function ====== */

                    $scope.validateReceiptItemDetail = function () {
                        var oValidate = {
                            isValid: true,
                            isWarning: false,
                            message: ''
                        };

                        if ($scope.GE.receiptItemDetail.ProjectCode == null || $scope.GE.receiptItemDetail.ProjectCode == '') {
                            oValidate.isValid = false;
                            oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['PROJECT_CODE'];
                            return oValidate;
                        }
                        if ($scope.GE.receiptItemDetail.CostCenter == null || $scope.GE.receiptItemDetail.CostCenter == '') {
                            oValidate.isValid = false;
                            oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['COST_CENTER'];
                            return oValidate;
                        }
                        if ($scope.GE.receiptItemDetail.IO == null || $scope.GE.receiptItemDetail.IO == '') {
                            oValidate.isValid = false;
                            oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['IO'];
                            return oValidate;
                        }
                        if ($scope.GE.receiptItemDetail.OriginalAmount == null || $scope.GE.receiptItemDetail.OriginalAmount.toString() == '') {
                            oValidate.isValid = false;
                            oValidate.message = $scope.Text['SYSTEM']['SPECIFY'] + $scope.Text['EXPENSE']['AMOUNT_NO_VAT'];
                            return oValidate;
                        }
                        if (!isFinite($scope.GE.receiptItemDetail.OriginalAmount.toString())) {
                            oValidate.isValid = false;
                            oValidate.message = $scope.Text['SYSTEM']['INVALID_NUMBER'];
                            return oValidate;
                        } else if ($scope.GE.receiptItemDetail.OriginalAmount <= 0) {
                            oValidate.isValid = false;
                            oValidate.message = $scope.Text['EXPENSE']['AMOUNT_NOT_ZERO'];
                            return oValidate;
                        }
                        if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                            // none, average

                        } else {
                            // exactly
                            if ($scope.GE.receiptItemDetailEdit == false && $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList != null) {
                                for (var i = 0; i < $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList.length; i++) {
                                    var tempItemDetail = $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[i];
                                    if (tempItemDetail.ProjectCode == $scope.GE.receiptItemDetail.ProjectCode
                                        && tempItemDetail.IO == $scope.GE.receiptItemDetail.IO
                                        && tempItemDetail.CostCenter == $scope.GE.receiptItemDetail.CostCenter) {
                                        oValidate.isValid = false;
                                        oValidate.message = $scope.Text['EXPENSE']['DUPLICATE_RECEIPT_ITEM_DETAIL'];
                                        return oValidate;
                                    }
                                }
                            }
                        }
                        return oValidate;
                    };

                    $scope.AddReceiptItemDetailFinish = function () {
                        var oValidate = $scope.validateReceiptItemDetail();
                        if (oValidate.isValid) {
                            // validate pass
                            $scope.GoToSubMain_AddGEReceiptItemDetailFinish();
                        } else {
                            $scope.p_message(oValidate.message,'w');
                        }
                    };

                    /* ====== !receipt item detail function ====== */

                    $scope.payToSettings = {
                        ENABLEPAYTOEMPLOYEE: false,
                        ENABLEPAYTOVENDOR: false,
                        ENABLEPAYTOCORPCARD: false
                    };
                    // call both in receipt and no receipt
                    var getRadioPayTo = function () {
                        if ($scope.document.RequestTypeID == 113) {
                            // TravelReport for Copporate
                            $scope.payToSettings.ENABLEPAYTOEMPLOYEE = false;
                            $scope.payToSettings.ENABLEPAYTOVENDOR = false;
                            $scope.payToSettings.ENABLEPAYTOCORPCARD = true;

                        } else {
                            var TravelRequestNo = '';
                            if ($scope.document.Additional)
                                TravelRequestNo = $scope.document.Additional.TravelRequestNo;
                            var URL = CONFIG.SERVER + 'HRTR/GetRadioPayTo';
                            var oRequestParameter = { InputParameter: { 'TRAVELREQUESTNO': TravelRequestNo, 'PREFIX': $scope.settings.ExpenseTypeGroupInfo.prefix }, CurrentEmployee: $scope.employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                            $http({
                                method: 'POST',
                                url: URL,
                                data: oRequestParameter
                            }).then(function successCallback(response) {

                                $scope.payToSettings.ENABLEPAYTOEMPLOYEE = response.data[0];
                                $scope.payToSettings.ENABLEPAYTOVENDOR = response.data[1];
                                $scope.payToSettings.ENABLEPAYTOCORPCARD = response.data[2];

                                console.log('GetRadioPayTo.', response.data);
                            }, function errorCallback(response) {
                                console.log('error GetRadioPayTo.', response);
                            });
                        }
                    };
                    getRadioPayTo();
                    $scope.GoToSubMain_AddGEReceipt = function (receipt) {
                        if ($scope.settings.ProjectCodeMode != $scope.moduleSetting.PROJECTCODEMODE_EXACTLY || (angular.isDefined($scope.projectCodes) && $scope.projectCodes != null && $scope.projectCodes.length > 0)) {
                            $scope.beginFormWizard();
                            $scope.wizardFormControl.receiptIsActive = true;
                            $scope.wizardFormFlag.isAddGEReceipt = true;
                            $scope.wizardFormFlag.isAddGEReceiptItem = false;
                            $scope.wizardFormFlag.isAddGEReceiptItemDetail = false;
                            $scope.IsEditVendor = false;
                            if (angular.isUndefined(receipt)) {
                                // new
                                var ObjReceipt = $scope.model.data.ExpenseReportReceipt[0];
                                $scope.GE.receiptEdit = false;
                                $scope.GE.receipt = angular.copy(ObjReceipt);
                                $scope.searchText = '';
                                $scope.autocomplete.selectedItem = null;
                                $scope.autocomplete_GE_province.selectedItem = '';
                                $scope.searchText_GE_province = null;
                                $scope.GE.receipt.IsReceipt = true;
                                $scope.GE.receipt.RequestNo = $scope.document.RequestNo;
                                $scope.GE.receipt.ReceiptDate = $scope.savedLimitMinDate;
                                $scope.SetLimitDate(false, false);
                                var childLength = ($scope.ExpenseReport.ExpenseReportReceiptList == null) ? 0 : $scope.ExpenseReport.ExpenseReportReceiptList.length;
                                var maxId = 0;
                                if (childLength > 0) {
                                    maxId = $scope.ExpenseReport.ExpenseReportReceiptList[childLength - 1].ReceiptID;
                                }
                                $scope.GE.receipt.ReceiptID = maxId + 1;




                                if ($scope.payToSettings.ENABLEPAYTOCORPCARD == true && $scope.document.RequestTypeID == 113) {
                                    $scope.GE.receipt.PayTo = 'V';
                                } else {
                                    $scope.GE.receipt.PayTo = 'E';
                                }



                                // initial receipt ddl
                                $scope.GE.receipt.VATCode = $scope.ddlDefault.VATCode;
                                if ($scope.travelCurrencyList != null && $scope.travelCurrencyList.length > 0) {
                                    $scope.GE.receipt.OriginalCurrencyCode = $scope.travelCurrencyList[0].FromCurrency;
                                    $scope.GE.receipt.currency_searchText = $scope.travelCurrencyList[0].FromCurrency + ' ' + $scope.findDropdownExchangeType($scope.travelCurrencyList[0].ExchangeTypeID, $scope.travelCurrencyList[0].ExchangeRate);
                                    $scope.GE.receipt.LocalCurrencyCode = $scope.document.Requestor.AreaSetting.CurrencyCode;
                                }

                                console.log('Receipt New.', $scope.GE.receipt);
                            } else {
                                // edit

                                $scope.GE.receiptEdit = true;
                                $scope.GE.receiptEditObj = receipt;
                                $scope.GE.receipt = angular.copy(receipt);
                                //Add by Nipon Supap
                                //$scope.ExpensetypMergVehicle = true;
                                //for (var i = 0; i < $scope.expenseTypeGroup.length; i++) {
                                //    if ($scope.expenseTypeGroup[i].ExpenseTypeGroupID == $scope.GE.receipt.ExpenseReportReceiptItemList[0].ExpenseTypeGroupID) {
                                //        $scope.ExpensetypMergVehicle = false;
                                //    }
                                //}
                                //if ($scope.ExpensetypMergVehicle) {
                                //    $scope.getExpenseTypeGroupConversion($scope.GE.receipt.ExpenseReportReceiptItemList[0].ExpenseTypeGroupID,"LoadView")
                                //}
                                //End

                                var filteredItem = $filter('filter')($scope.objCountry, { CountryCode: receipt.VendorCountry });
                                $scope.searchText = filteredItem[0].CountryName;
                                $scope.autocomplete.selectedItem = filteredItem[0];
                                if (filteredItem[0].CountryCode == 'TH') {
                                    var filteredItem_city = $filter('filter')($scope.GE_provinceList_, { ProvinceName: receipt.VendorCity });
                                    $scope.autocomplete_GE_province.selectedItem = filteredItem_city[0];


                                    //$scope.searchText_GE_province = filteredItem_city[0].ProvinceName;

                                    //15062017
                                    if (filteredItem_city[0] != null) {
                                        $scope.searchText_GE_province = filteredItem_city[0].ProvinceName;
                                    }else
                                    {
                                        $scope.GE.receipt.VendorCity = receipt.VendorCity;
                                        $scope.searchText_GE_province = receipt.VendorCity;
                                    }
                                    //

                                } else {
                                    $scope.GE.receipt.VendorCity = receipt.VendorCity;
                                    $scope.searchText_GE_province = receipt.VendorCity;
                                }

                                $scope.SetLimitDate($scope.GE.receipt.NolimitDate,false);
                                $scope.GE.receipt.IsReceipt = true;
                                console.log('Recipt Edit.', $scope.GE.receipt);


                                $scope.GE.receipt.currency_searchText = $scope.GE.receipt.OriginalCurrencyCode + ' (' + $scope.GE.receipt.ExchangeRate+')';
                                $scope.TravelersforAccommodation = angular.copy($scope.TravelersforAccommodation_Buffer)
                                //Nun Add 16082016 แก้ไขหากเข้ามาแก้ไข Receipt แล้ว VendorCode ขึ้นต้นด้วย PX ต้องสามารถแก้ไขข้อมูล Vendor ได้
                                if ($scope.GE.receipt.VendorCode != '' && $scope.GE.receipt.VendorCode.substring(0, 2) == 'PX') {
                                    $scope.IsEditVendor = true;
                                }
                                //---------------------------------------------
                                //Nun Add 07092016
                                $scope.changeShowWhtTax();
                                //

                                // initial receipt
                                $scope.selectPayTo();

                                // Nun Add 07102016
                                $scope.tempLastVendorCode = $scope.GE.receipt.VendorCode;
                                //
                            }

                            // -- gen data for table
                            $scope.expenseItems_ReceiptItem = $scope.getExpenseItems_ReceiptItem();
                            // -- !gen data for table

                        } else {
                            $scope.p_message($scope.Text['SYSTEM']['MINIMUM_1'] + $scope.Text['EXPENSE']['PROJECT'],'w');
                        }

                        $timeout(function () {
                            document.getElementById('top-anchor1').scrollIntoView();
                            document.getElementById('receipt-text-id').focus();
                        }, 100);
                    };

                    $scope.GoToSubMain_DeleteGEReceipt = function (receipt) {
                        if (angular.isDefined(receipt)) {

                            if (confirm($scope.Text['SYSTEM']['CONFIRM_DELETE'])) {
                                // delete receipt
                                for (var i = 0; i < $scope.ExpenseReport.ExpenseReportReceiptList.length; i++) {
                                    if ($scope.ExpenseReport.ExpenseReportReceiptList[i] == receipt) {
                                        $scope.ExpenseReport.ExpenseReportReceiptList.splice(i, 1);
                                        break;
                                    }
                                }
                                if ($scope.ExpenseReport.ExpenseReportReceiptList.length == 0) {
                                    $scope.ExpenseReport.ExpenseReportReceiptList = [];
                                }

                                /* fix case when new object $scope.document in root controller */
                                $scope.document.Additional.ExpenseReportReceiptList = $scope.ExpenseReport.ExpenseReportReceiptList;
                                /* !fix case when new object $scope.document in root controller */

                                //--Show Grid
                                $scope.expenseItems_Receipt = $scope.getExpenseItems_Receipt();
                                //--Show Grid

                            }
                            console.log('document.', $scope.document);
                        }
                    };

                    $scope.GetCurrentTraveler = function (currentRoommate, index) {
                        var result = $scope.TravelersforAccommodation.filter(createFilterForTravelersforAccommodation(currentRoommate));
                        if (result.length > 0) {
                            $scope.SetRoommate(result[0], index)
                        }
                    }

                    function createFilterForTravelersforAccommodation(currentRoommate) {
                        var lowercaseQuery = angular.lowercase(currentRoommate.EmployeeID);
                        return function filterFn(currentTraveller) {
                            var destination = angular.lowercase(currentTraveller.EmployeeID);
                            return (destination == lowercaseQuery);
                        };
                    }

                    $scope.SetRoommate = function (item, index) {
                        if (angular.isDefined($scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList) && $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.length > 0) {
                            $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].RequestNo = document.RequestNo;
                            $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].ReceiptID = $scope.GE.receipt.ReceiptID;
                            $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].ItemID = index;
                            $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].TravelRequestNo = $scope.document.Additional.TravelRequestNo;
                            $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].EmployeeID = item.EmployeeID;
                            $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].EmployeeName = item.EmployeeName;
                            $scope.CalculateAccommodation(item, index);
                        }
                    }
                    $scope.loader_ = {
                        enable:false
                    }
                    $scope.CalculateAccommodation = function (currentTraveler, index) {
                        $scope.loader_.enable = true;
                        var oSummary = 0.00;
                        var URL = CONFIG.SERVER + 'HRTR/CalculatePerdiumForSpecificTraveler';
                        var oRequestParameter = { InputParameter: { 'TRAVELREPORT': $scope.document.Additional, 'TRAVELER': currentTraveler }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].PerdiumList = response.data;
                            var Perdiums = $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].PerdiumList;
                            $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].AmountLimit = 0.00;
                            for (var i = 0; i < Perdiums.length; i++) {
                                if (Perdiums[i].ExpenseRateTagCodeForPost == 'ACCOMMODATION' && Perdiums[i].NightCount == 1 && $filter('date')(Perdiums[i].BeginDate, 'yyyy-MM-dd') >= $filter('date')($scope.GE.receiptItem.Date1, 'yyyy-MM-dd') && $filter('date')(Perdiums[i].BeginDate, 'yyyy-MM-dd') < $filter('date')($scope.GE.receiptItem.Date2, 'yyyy-MM-dd')) {
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].AmountLimit = $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].AmountLimit + Perdiums[i].LocalAmount;
                                }
                            }
                            assingUnlimit();
                            $scope.loader_.enable = false;
                        }, function errorCallback(response) {
                            $scope.loader_.enable = false;
                            $mdDialog.show(
                                $mdDialog.alert()
                                .clickOutsideToClose(false)
                                .title($scope.Text['SYSTEM']['INFORMATION'])
                                .textContent(response.data)
                                .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                            );

                        });

                    }

                    $scope.getCalculateOwnerAccommodation = function () {
                        var oSummary = 0.00;
                        var currentDate = new Date($scope.GE.receiptItem.Date1);// new Date($scope.GE.receiptItem.Date1.substr(0, 10));
                        var endDate = new Date($scope.GE.receiptItem.Date2);//new Date($scope.GE.receiptItem.Date2.substr(0, 10));
                        //check accom perdium ทั่วไป
                        while (currentDate.getTime() < endDate.getTime()) {
                            var perdiumRight = 0;
                            // loop self perdium
                            if ($scope.document.Additional.PerdiumList != null) {
                                $.each($scope.document.Additional.PerdiumList, function (index, perdium) {
                                    var perdiumDate = new Date(perdium.BeginDate.substr(0, 10));

                                    if (currentDate.getTime() == perdiumDate.getTime() && perdium.ExpenseRateTagCodeForPost == 'ACCOMMODATION' && perdium.IsCheck) {
                                        if (perdium.IsUnlimit) {
                                            perdiumRight = 999999999;
                                        } else {
                                            perdiumRight += perdium.LocalAmount;
                                        }
                                        return false; // break $.each
                                    }
                                });
                            }
                            if (oSummary >= 999999999)
                                return oSummary;
                            oSummary += perdiumRight;

                            
                            currentDate.setDate(currentDate.getDate() + 1); // add 1 day
                        }
                        return oSummary;
                    }

                    $scope.ChangeCheckInCheckOut = function () {
                        $scope.GetTravelers();
                     
                    }

                    function assingUnlimit(){
                        console.debug('Original', $filter('date')($scope.GE.receiptItem.Date1, 'yyyy-MM-dd'));
                        if (angular.isDefined($scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList) && $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.length > 0) {
                            for (var index = 0; index < $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.length; index++) {
                                if (!$scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].IsOwner) {
                                    var Perdiums = $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].PerdiumList;
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].AmountLimit = 0;
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].IsUnlimit = false;
                                    for (var i = 0; i < Perdiums.length; i++) {
                                        if (Perdiums[i].ExpenseRateTagCodeForPost == 'ACCOMMODATION' && Perdiums[i].IsCheck && Perdiums[i].NightCount == 1 && $filter('date')(Perdiums[i].BeginDate, 'yyyy-MM-dd') >= $filter('date')($scope.GE.receiptItem.Date1, 'yyyy-MM-dd') && $filter('date')(Perdiums[i].BeginDate, 'yyyy-MM-dd') < $filter('date')($scope.GE.receiptItem.Date2, 'yyyy-MM-dd')) {
                                            if (Perdiums[i].IsUnlimit) {
                                                $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].AmountLimit = 999999999;
                                                $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].IsUnlimit = true;
                                                break;
                                            }
                                            else
                                                $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].AmountLimit += Perdiums[i].LocalAmount;
                                        }
                                    }
                                } else {
                                    // Owner
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].AmountLimit = $scope.getCalculateOwnerAccommodation();
                                    if ($scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].AmountLimit >= 999999999) {
                                        $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].IsUnlimit = true;
                                    } else {
                                        $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[index].IsUnlimit = false;
                                    }
                                }
                            }
                        }
                    }

                    $scope.GoToSubMain_AddEmployeeRoommate = function () {
                        $scope.CheckPerosonalIsSelectedThenSplice();
                        if ($scope.TravelersforAccommodation && $scope.TravelersforAccommodation.length > 0) {
                            $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.push(angular.copy($scope.model.data.ExpenseReportReceiptItemRoommate[0]));
                            $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[$scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.length - 1].EmployeeID = $scope.TravelersforAccommodation[0].EmployeeID;
                            $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[$scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.length - 1].EmployeeName = $scope.TravelersforAccommodation[0].EmployeeName;
                            $scope.GetCurrentTraveler($scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[$scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.length - 1], $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.length - 1)
                        }
                        else
                        {
                            $mdDialog.show(
                                 $mdDialog.alert()
                                   .clickOutsideToClose(false)
                                   .title($scope.Text['SYSTEM']['INFORMATION'])
                                   .textContent($scope.Text['EXPENSE']['ROOMMATEISEMPTY'])
                                   .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                                );
                        }
                    }

                    $scope.GoToSubMain_DeleteRoommate = function (currentIndex) {
                        if (angular.isDefined($scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[currentIndex]) && !$scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[currentIndex].IsOwner) {
                            $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.splice(currentIndex, 1);
                        }
                        $scope.CheckPerosonalIsSelectedThenSplice();
                    }

                    $scope.GetTravelers = function () {
                        $scope.loader_.enable = true;
                        var URL = CONFIG.SERVER + 'HRTR/GetBuddyforRoommate';
                        var oRequestParameter = { InputParameter: { 'TRAVELREQUESTNO': $scope.document.Additional.TravelRequestNo, 'REQUESTNO': $scope.document.RequestNo, 'CHECKINDATE': $scope.GE.receiptItem.Date1, 'CHECKOUTDATE': $scope.GE.receiptItem.Date2 }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            $scope.TravelersforAccommodation = response.data;
                            if ($scope.TravelersforAccommodation.length == 0) {
                                $mdDialog.show(
                                 $mdDialog.alert()
                                   .clickOutsideToClose(false)
                                   .title($scope.Text['SYSTEM']['INFORMATION'])
                                   .textContent($scope.Text['EXPENSE']['INVALIDADDROOMMATE'])
                                   .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                               );
                            }
                            $scope.TravelersforAccommodation_Buffer = angular.copy($scope.TravelersforAccommodation);
                            $scope.CheckRoommateSelectedThenSpliceOnChangeCheckInCheckOut();
                            $scope.CheckPerosonalIsSelectedThenSplice();
                            assingUnlimit();
                            $scope.loader_.enable = false;

                        }, function errorCallback(response) {
                            $mdDialog.show(
                                $mdDialog.alert()
                                .clickOutsideToClose(false)
                                .title($scope.Text['SYSTEM']['INFORMATION'])
                                .textContent(response.data)
                                .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                            );
                            $scope.loader_.enable = false;
                        });
                    };
                    $scope.ap = {
                        searchCostCenterText: '',
                        searchAlternativeIOOrgText: '',
                        searchIOText: ''

                    }
                    $scope.GetCarRegistration = function()
                    {
                        var URL = CONFIG.SERVER + 'HRTR/GetAllCarRegistration/';
                        var oRequestParameter = { InputParameter: { }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };

                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            // Success
                            $scope.CarRegistrationList = response.data;
                        }, function errorCallback(response) {
                            // Error
                            console.log('error CarRegistrationListController.', response);
                        });

                    }
                    $scope.CheckPerosonalIsSelectedThenSplice = function () {
                        $scope.TravelersforAccommodation = angular.copy($scope.TravelersforAccommodation_Buffer);
                        var CountTravelersforAccommodation = $scope.TravelersforAccommodation.length;
                        for (var i = 0; i < $scope.TravelersforAccommodation.length; i++) {
                            if ($scope.TravelersforAccommodation[i].EmployeeID == $scope.document.Requestor.EmployeeID) {
                                $scope.TravelersforAccommodation.splice(i, 1);
                                i--;
                                continue;
                            }
                            for (var j = 0; j < $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.length; j++) {
                                if (angular.isDefined($scope.TravelersforAccommodation[i]) && $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].EmployeeID == $scope.TravelersforAccommodation[i].EmployeeID) {
                                    $scope.TravelersforAccommodation.splice(i, 1);
                                    i--;
                                    break;
                                }
                            }
                        }
                    }
                    $scope.CheckRoommateSelectedThenSpliceOnChangeCheckInCheckOut = function () {
                        $scope.TravelersforAccommodation = angular.copy($scope.TravelersforAccommodation_Buffer);
                        var ExpenseReportReceiptItemRoommateList_Buffer = [];
                        var CountTravelersforAccommodation = $scope.TravelersforAccommodation.length;
                        for (var i = 0; i < $scope.TravelersforAccommodation.length; i++) {

                            for (var j = 0; j < $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.length; j++) {
                                if (angular.isDefined($scope.TravelersforAccommodation[i]) && $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].EmployeeID == $scope.TravelersforAccommodation[i].EmployeeID) {


                                    //Nun Modified ตัวแปร j >> i เนื่องจาก Assign EmployeeName ไม่ถูกต้อง
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].EmployeeName = $scope.TravelersforAccommodation[i].EmployeeName;
                                    //$scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].EmployeeName = $scope.TravelersforAccommodation[j].EmployeeName;


                                    ////Bind Master for dropdownlist
                                    //$scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].IsAlternativeCostCenter = $scope.TravelersforAccommodation[0].IsAlternativeCostCenter;
                                    if ($scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].CostCenter) {
                                        $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].CostCenterObject = $scope.settings.Master.CostCenterList[$scope.settings.Master.CostCenterList.findIndexWithAttr('CostCenterCode', $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].CostCenter)];
                                        $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].CostCenter = $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].CostCenterObject.CostCenterCode;
                                        $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].CostCenterName = $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].CostCenterObject.LongDesc;
                                    }
                                    ////Get default io/costcenter from select roommate
                                    if ($scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].AlternativeIOOrg) {
                                        //$scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].IsAlternativeIOOrg = $scope.TravelersforAccommodation[0].IsAlternativeIOOrg;
                                        $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].AlternativeIOOrgObject = $scope.settings.Master.OrgUnitList[$scope.settings.Master.OrgUnitList.findIndexWithAttr('ObjectID', $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].AlternativeIOOrg)];
                                        $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].OrgUnitID = $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].AlternativeIOOrgObject.ObjectID;
                                        $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].OrgUnitName = $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].AlternativeIOOrgObject.Text;
                                    }
                                        
                                    if ($scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].IO) {
                                        //$scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].AlternativeIOOrg = $scope.TravelersforAccommodation[0].AlternativeIOOrg;
                                        $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].IOObject = $scope.settings.Master.IOList[$scope.settings.Master.IOList.findIndexWithAttr('OrderID', $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].IO)];
                                        $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].IO = $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].IOObject.OrderID;
                                        $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].IOName = $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].IOObject.Description;
                                    }

                                    //set to autocomplete
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].searchCostCenterText = $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].CostCenter + ' : ' + $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].CostCenterName;
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].searchAlternativeIOOrgText = $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].OrgUnitID + ' : ' + $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].OrgUnitName;
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].searchCostCentesearchIOTextrText = $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].IO + ' : ' + $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j].IOName;

                                    ExpenseReportReceiptItemRoommateList_Buffer.push(angular.copy($scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[j]));

                                    break;
                                }
                            }
                            if ($scope.TravelersforAccommodation[i].EmployeeID == $scope.document.Requestor.EmployeeID) {
                                $scope.TravelersforAccommodation.splice(i, 1);
                                i--;
                                continue;
                            }
                        }
                        if ($scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.length == 1 && $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[0].EmployeeID == $scope.employeeData.RequesterEmployeeID) {

                        }
                        else {
                            $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList = angular.copy(ExpenseReportReceiptItemRoommateList_Buffer);
                        }

                    }

                    /* !Accommodation Roommate */

                    $scope.GoToSubMain_AddGEReceiptFinish = function () {
                        if (!$scope.GE.receiptEdit) {
                            // finish from add
                            if ($scope.ExpenseReport.ExpenseReportReceiptList == null) {
                                $scope.ExpenseReport.ExpenseReportReceiptList = [];
                            }
                            $scope.ExpenseReport.ExpenseReportReceiptList.push($scope.GE.receipt);
                        } else {
                            // finish from edit
                            for (var i = 0; i < $scope.ExpenseReport.ExpenseReportReceiptList.length; i++) {
                                if ($scope.ExpenseReport.ExpenseReportReceiptList[i] == $scope.GE.receiptEditObj) {
                                    $scope.ExpenseReport.ExpenseReportReceiptList[i] = $scope.GE.receipt;
                                    break;
                                }
                            }
                        }

                        $scope.GE.receiptEditObj = null;
                        $scope.GE.receipt = null;
                        //$scope.tempFormData.chkVendorTaxIDDup = '';

                        $scope.wizardFormFlag.isAddGEReceipt = false;
                        $scope.wizardFormFlag.isAddGEReceiptItem = false;
                        $scope.wizardFormFlag.isAddGEReceiptItemDetail = false;
                        $scope.wizardFormControl.receiptIsActive = false;
                        $scope.finishFormWizard();

                        /* fix case when new object $scope.document in root controller */
                        $scope.document.Additional.ExpenseReportReceiptList = $scope.ExpenseReport.ExpenseReportReceiptList;
                        /* !fix case when new object $scope.document in root controller */

                        //--Show Grid
                        $scope.expenseItems_Receipt = $scope.getExpenseItems_Receipt();
                        //--Show Grid

                        $timeout(function () {
                            document.getElementById('top-anchor1').scrollIntoView();
                        }, 100);

                        console.log('Recipt Add Receipt Finish.', $scope.ExpenseReport);
                        console.log('document.', $scope.document);
                    };

                    $scope.GoToSubMain_AddGEReceiptCancel = function () {

                        var confirm = $mdDialog.confirm()
                            .title($scope.Text['SYSTEM']['CONFIRM_REDIRECT_TITLE'])
                            .textContent($scope.Text['SYSTEM']['CONFIRM_CANCEL'])
                            .ok('OK')
                            .cancel('Cancel');
                        $mdDialog.show(confirm).then(function (result) {
                            $scope.GE.receipt = null;
                            //$scope.tempFormData.chkVendorTaxIDDup = '';

                            $scope.wizardFormFlag.isAddGEReceipt = false;
                            $scope.wizardFormFlag.isAddGEReceiptItem = false;
                            $scope.wizardFormFlag.isAddGEReceiptItemDetail = false;
                            $scope.wizardFormControl.receiptIsActive = false;
                            $scope.finishFormWizard();

                            $timeout(function () {
                                document.getElementById('top-anchor1').scrollIntoView();
                            }, 100);
                        }, function () {
                            event.preventDefault();
                        });

                        //if (confirm($scope.Text['SYSTEM']['CONFIRM_CANCEL'])) {
                        //    $scope.GE.receipt = null;
                        //    //$scope.tempFormData.chkVendorTaxIDDup = '';

                        //    $scope.wizardFormFlag.isAddGEReceipt = false;
                        //    $scope.wizardFormFlag.isAddGEReceiptItem = false;
                        //    $scope.wizardFormFlag.isAddGEReceiptItemDetail = false;
                        //    $scope.wizardFormControl.receiptIsActive = false;
                        //    $scope.finishFormWizard();

                        //    $timeout(function () {
                        //        document.getElementById('top-anchor1').scrollIntoView();
                        //    }, 100);
                        //}
                    };

                    $scope.findObjCostCenter = function (costcenterCode) {
                        for (var i = 0; i < $scope.masterData.objCostcenter.length; i++) {
                            if (costcenterCode == $scope.masterData.objCostcenter[i].CostCenterCode) {
                                return $scope.masterData.objCostcenter[i];
                            }
                        }
                        return null;
                    };

                    $scope.GoToSubMain_AddGEReceiptItem = function (receiptItem) {
                        $scope.wizardFormFlag.isAddGEReceipt = false;
                        $scope.wizardFormFlag.isAddGEReceiptItem = true;
                        $scope.wizardFormFlag.isAddGEReceiptItemDetail = false;
                        if (angular.isUndefined(receiptItem)) {
                            // new
                            var ObjReceiptItem = $scope.model.data.ExpenseReportReceiptItem[0];
                            $scope.GE.receiptItemEdit = false;
                            $scope.GE.receiptItemFormData.Amount = 0;
                            $scope.GE.receiptItemFormData.AmountInRight = 9999999999;
                            $scope.GE.receiptItem = angular.copy(ObjReceiptItem);
                            $scope.GE.receiptItem.Amount = 0;
                            $scope.GE.receiptItem.RequestNo = $scope.document.RequestNo;
                            $scope.GE.receiptItem.ReceiptID = $scope.GE.receipt.ReceiptID;
                            var childLength = ($scope.GE.receipt.ExpenseReportReceiptItemList == null) ? 0 : $scope.GE.receipt.ExpenseReportReceiptItemList.length;
                            var maxId = 0;
                            if (childLength > 0) {
                                maxId = $scope.GE.receipt.ExpenseReportReceiptItemList[childLength - 1].ItemID;
                            }
                            $scope.GE.receiptItem.ItemID = maxId + 1;
                            $scope.GE.receiptItem.ExpenseTypeGroupID = $scope.ddlDefault.ExpenseTypeGroupID.toString();

                            //--------Nun Add ExpenseTypeGroup Name and Remark 09/06/2016
                            $scope.GE.receiptItem.ExpenseTypeGroupName = $scope.ddlDefault.ExpenseTypeGroupName;
                            $scope.GE.receiptItem.ExpenseTypeGroupRemark = $scope.ddlDefault.ExpenseTypeGroupRemark;
                            //-----------------------------------------------------------

                            //$scope.GE.receiptItem.ExpenseTypeID = $scope.ddlDefault.ExpenseTypeID;

                            //--------Nun Add ExpenseType Name and Remark 09/06/2016
                            //$scope.GE.receiptItem.ExpenseTypeName = $scope.ddlDefault.ExpenseTypeName;
                            //-----------------------------------------------------------

                            $scope.getExpenseTypeByGroupID($scope.ddlDefault.ExpenseTypeGroupID, true);

                            if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                                // none, average
                                // new 1 itemDetail
                                var ObjReceiptItemDetail = angular.copy($scope.model.data.ExpenseReportReceiptItemDetail[0]);
                                ObjReceiptItemDetail.RequestNo = $scope.document.RequestNo;
                                ObjReceiptItemDetail.ReceiptID = $scope.GE.receipt.ReceiptID;
                                ObjReceiptItemDetail.ItemID = $scope.GE.receiptItem.ItemID;
                                if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList == null) {
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList = [];
                                }

                                // set default CostCenter, IO
                                if (angular.isUndefined($scope.DefaultCostCenterIOByGroup[$scope.GE.receiptItem.ExpenseTypeGroupID])) {
                                    ObjReceiptItemDetail.CostCenter = $scope.DefaultCostCenterIO.CostCenter;
                                    ObjReceiptItemDetail.IsAlternativeCostCenter = $scope.DefaultCostCenterIO.IsAlternativeCostCenter;
                                    ObjReceiptItemDetail.AlternativeIOOrg = $scope.DefaultCostCenterIO.OrgUnitID;
                                    ObjReceiptItemDetail.IsAlternativeIOOrg = $scope.DefaultCostCenterIO.IsAlternativeIOOrg;
                                    ObjReceiptItemDetail.IO = $scope.DefaultCostCenterIO.IO;
                                } else {
                                    var objDefault = $scope.DefaultCostCenterIOByGroup[$scope.GE.receiptItem.ExpenseTypeGroupID];
                                    //ObjReceiptItemDetail.CostCenter = objDefault.CostCenter;
                                    //Edit by Nipon Supap 13-07-2017
                                    ObjReceiptItemDetail.CostCenter = $scope.document.Additional.CostCenter;
                                    ObjReceiptItemDetail.IsAlternativeCostCenter = $scope.document.Additional.IsAlternativeCostCenter;
                                    if (objDefault.IsAlternativeIOOrg && objDefault.OrgUnitID) {
                                        ObjReceiptItemDetail.AlternativeIOOrg = objDefault.OrgUnitID;
                                    } else {
                                        ObjReceiptItemDetail.AlternativeIOOrg = $scope.DefaultCostCenterIO.OrgUnitID;
                                    }
                                    ObjReceiptItemDetail.IsAlternativeIOOrg = objDefault.IsAlternativeIOOrg;
                                    ObjReceiptItemDetail.IO = objDefault.IO;
                                }
                                //if (ObjReceiptItemDetail.IsAlternativeIOOrg) {
                                // get default IO by Org
                                $scope.getIOByOrganization(ObjReceiptItemDetail.AlternativeIOOrg);
                                //} else {
                                //    // get default IO by CostCenter
                                //    $scope.getIOByCostCenter(ObjReceiptItemDetail.CostCenter);
                                //}

                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList.push(ObjReceiptItemDetail);
                            }
                            $scope.employeeDataList = angular.copy($scope.masterData.employeeDataList);
                            console.log('ReciptItem New.', $scope.GE.receiptItem);
                        } else {
                            // edit
                            $scope.GE.receiptItemEdit = true;
                            $scope.GE.receiptItemEditObj = receiptItem;
                            $scope.GE.receiptItem = angular.copy(receiptItem);
                            if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                                // none, average
                                $scope.GE.receiptItemFormData.Amount = $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].OriginalAmount;
                                $scope.GE.receiptItemFormData.AmountInRight = $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].OriginalAmountInRight;

                                // set default CostCenter, IO
                                //if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].IsAlternativeIOOrg) {
                                $scope.getIOByOrganization($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].AlternativeIOOrg);
                                //} else {
                                //    $scope.getIOByCostCenter($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].CostCenter);
                                //}

                            } else {
                                // exactly
                                var sumAmount = 0;
                                if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList != null) {
                                    for (var i = 0; i < $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList.length; i++) {
                                        var amount = Number($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[i].OriginalAmount);
                                        sumAmount += $scope.MathRounding(isNaN(amount) ? 0 : amount);
                                    }
                                }
                                if ($scope.panelName == 'pnlMileage' || $scope.panelName == 'pnlTransCommutation') {
                                    $scope.GE.receiptItemFormData.Amount = sumAmount;
                                } else {
                                    $scope.GE.receiptItem.Amount = sumAmount;
                                }
                            }
                            $scope.GE.receiptItem.ExpenseTypeGroupID = $scope.GE.receiptItem.ExpenseTypeGroupID.toString();
                            $scope.ExpensetypMergVehicle = true;
                            for (var i = 0; i < $scope.expenseTypeGroup.length; i++) {
                                if ($scope.expenseTypeGroup[i].ExpenseTypeGroupID == $scope.GE.receiptItem.ExpenseTypeGroupID) {
                                    $scope.ExpensetypMergVehicle = false;
                                }
                            }
                            if ($scope.ExpensetypMergVehicle) {
                                //Get Stroed [sp_ExpenseTypeGroupConversionGet] by Nipon Supap 08/11/2017
                                $scope.getExpenseTypeGroupConversion($scope.GE.receiptItem.ExpenseTypeGroupID,"LoadEdit");
                                
                            }
                            $scope.getExpenseTypeByGroupID($scope.GE.receiptItem.ExpenseTypeGroupID);
                            $scope.GE.receiptItem.ExpenseTypeID = $scope.GE.receiptItem.ExpenseTypeID.toString();
                            console.log('ReciptItem Edit.', $scope.GE.receiptItem);
                        }

                        $scope.GE.receiptItemFormData.tmpEmployeeName = '';
                        $scope.GE.receiptItemFormData.tmpOrgUnitName = '';
                        $scope.GE.receiptItemFormData.tmpCompanyName = '';
                        clearNewGuest();
                        $scope.autocompleteGuest.searchTextGuest = '';

                        $timeout(function () {
                            document.getElementById('top-anchor1').scrollIntoView();
                        }, 100);
                    };

                    $scope.formatInteger = function (obj, i) {
                        var temp;
                        var isnum = /^\d+$/.test(obj);
                        if (i == 0) {
                        
                            if (!isnum) {
                                $scope.GE.receipt.VendorPostCode = '';
                            }
                        } else if (i == 1) {

                            if (!isnum) {
                                $scope.GE.receipt.VendorTaxID = '';
                            }
                        }

                    }

                    $scope.checkTaxIDDup = function (obj, i) {
                        $scope.formatInteger(obj, i);
                        var URL = CONFIG.SERVER + 'HRTR/CheckIsDupTaxID/';
                        var oRequestParameter = { InputParameter: { "TAXID": $scope.GE.receipt.VendorTaxID }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };

                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            $scope.tempFormData.chkVendorTaxIDDup = response.data;
                            console.log('objVendorDupTaxID.', $scope.tempData.chkVendorTaxIDDup);
                        }, function errorCallback(response) {
                            $scope.tempFormData.chkVendorTaxIDDup = 'error';
                            console.log('error GeneralExpenseEditorController checkTaxIDDup.', response);
                        });
                    };



                    $scope.onSelectedCountry = function (Country) {
                        var URL = CONFIG.SERVER + 'HRTR/GetProvinceByCountry/';
                        var oRequestParameter = { InputParameter: { "CountryCode": Country }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            $scope.objProvince = response.data;
                            if ($scope.objProvince != null && $scope.objProvince.length > 0) {

                                $scope.GE.receipt.VendorCity = $scope.objProvince[0].ProvinceName;
                                if ($scope.whtSettings.ENABLEWHT) {
                                    $scope.changeShowWhtTax();
                                }
                            }
                            else {
                                $scope.GE.receipt.VendorCity = null;

                            }
                            console.log('objProvince.', $scope.objProvince);
                        }, function errorCallback(response) {
                            console.log('error GeneralExpenseEditorController GetProvinceByCountry.', response);
                        });
                    };








                    /********************  auto complete country start ******************* */
                    $scope.simulateQuery = true;
                    $scope.searchText = '';
                    $scope.autocomplete = {
                        selectedItem: null
                    }
                    $scope.querySearchItem = function (query) {
                        if (!$scope.objCountry) return;
                        var results = query ? $scope.objCountry.filter(createFilterFor(query)) : $scope.objCountry, deferred;
                        if (results.length == 0) {
                            $scope.GE.receipt.VendorCity = null;
                            $scope.GE.receipt.VendorCountry = null;
                            $scope.searchText = '';
                        }
                        if ($scope.simulateQuery) {
                            deferred = $q.defer();
                            $timeout(function () { deferred.resolve(results); }, Math.random() * 50, false);
                            return deferred.promise;
                        } else {
                            return results;
                        }
                    }
                    $scope.selectedItemChange = function (item) {
                        if (angular.isDefined(item)) {
                            $scope.autocomplete.selectedItem = item;
                            //$scope.GE.receipt.VendorCity.CountryName = item.CountryName
                            $scope.GE.receipt.VendorCountry = item.CountryCode;
                            if (item.CountryCode == 'TH') {
                                if ($scope.whtSettings.ENABLEWHT) {
                                    $scope.changeShowWhtTax();
                                }

                                if($scope.GE.receipt.VendorPostCode != ''){
                                    if (isNaN($scope.GE.receipt.VendorPostCode) || $scope.GE.receipt.VendorPostCode.length > 5) {
                                        $scope.GE.receipt.VendorPostCode = '';
                                    }
                                }

                                
                            } else {
                                //$scope.GE.receipt.VendorCity = null;
                            }
                            // ควรจะมี flag บอกจาก server ใน country list ว่ามี province ในระบบหรือไม่ เพิ่อทำการเช็คนะจุดนี้ได้เลย TODO
                        }
                    }
                    function createFilterFor(query) {
                        if (!angular.isDefined(query)) return false;
                        var lowercaseQuery = angular.lowercase(query);
                        return function filterFn(item) {
                            //console.debug(item);
                            var textquery = angular.lowercase(item.CountryName);
                            return (textquery.indexOf(lowercaseQuery) >= 0);
                        };
                    }
                    var tempResult;
                    $scope.tryToSelect = function (text) {
                        for (var i = 0; i < $scope.objCountry.length; i++) {
                            if ($scope.objCountry[i].CountryName == text) {
                                tempResult = $scope.objCountry[i];
                                break;
                            }
                        }
                        $scope.searchText = '';
                        $scope.autocomplete.selectedItem = null;
                    }
                    $scope.checkText = function (text) {
                        var result = null;
                        for (var i = 0; i < $scope.objCountry.length; i++) {
                            if ($scope.objCountry[i].CountryName == text) {
                                result = $scope.objCountry[i];
                                break;
                            }
                        }
                        if (result) {
                            $scope.searchText = result.CountryName;
                            $scope.selectedItemChange(result);
                            $scope.autocomplete.selectedItem = result;
                        } else if (tempResult) {
                            $scope.searchText = tempResult.CountryName;
                            $scope.selectedItemChange(tempResult);
                            $scope.autocomplete.selectedItem = tempResult;
                        }
                    }
                    /********************  auto complete country end ******************* */
                    /********************  auto complete province start ******************* */
                    $scope.simulateQuery_GE_province = true;
                    $scope.searchText_GE_province = '';
                    $scope.autocomplete_GE_province = {
                        selectedItem: null
                    }
                    $scope.querySearchItem_GE_province = function (query) {
                        if (!$scope.GE_provinceList_) return;
                        var results = query ? $scope.GE_provinceList_.filter(createFilterFor_GE_province(query)) : $scope.GE_provinceList_, deferred;
                        if (results.length == 0) {
                            $scope.autocomplete_GE_province.selectedItem = null;
                            $scope.GE.receipt.VendorCity = null;
                            $scope.searchText_GE_province = '';
                        }
                        if ($scope.simulateQuery) {
                            deferred = $q.defer();
                            $timeout(function () { deferred.resolve(results); }, 0, false);
                            return deferred.promise;
                        } else {
                            return results;
                        }
                    }
                    $scope.selectedItemChange_GE_province = function (item) {
                        if (angular.isDefined(item)) {
                            $scope.autocomplete_GE_province.selectedItem = item;
                            $scope.GE.receipt.VendorCity = item.ProvinceName;
                        }
                    }
                    function createFilterFor_GE_province(query) {
                        if (!angular.isDefined(query)) return false;
                        var lowercaseQuery = angular.lowercase(query);
                        return function filterFn(item) {
                            //console.debug(item);
                            var textquery = angular.lowercase(item.ProvinceName);
                            return (textquery.indexOf(lowercaseQuery) >= 0);
                        };
                    }
                    var tempResultProvince;
                    $scope.tryToSelectProvince = function (text) {
                        for (var i = 0; i < $scope.GE_provinceList_.length; i++) {
                            if ($scope.GE_provinceList_[i].ProvinceName == text) {
                                tempResultProvince = $scope.GE_provinceList_[i];
                                break;
                            }
                        }
                        $scope.searchText_GE_province = '';
                        $scope.autocomplete_GE_province.selectedItem = null;
                    }
                    $scope.checkTextProvince = function (text) {
                        var result = null;
                        for (var i = 0; i < $scope.GE_provinceList_.length; i++) {
                            if ($scope.GE_provinceList_[i].ProvinceName == text) {
                                result = $scope.GE_provinceList_[i];
                                break;
                            }
                        }
                        if (result) {
                            $scope.searchText_GE_province = result.ProvinceName;
                            $scope.selectedItemChange_GE_province(result);
                            $scope.autocomplete_GE_province.selectedItem = result;
                        } else if (tempResultProvince) {
                            $scope.searchText_GE_province = tempResultProvince.ProvinceName;
                            $scope.selectedItemChange_GE_province(tempResultProvince);
                            $scope.autocomplete_GE_province.selectedItem = tempResultProvince;
                        }
                    }
                    /********************  auto complete province end ******************* */

                    /********************  auto complete currency start ******************* */

                    var travelCurrency_;
                    $scope.querySearchItem_currency = function (query) {
                        console.debug($scope.travelCurrencyList);
                        if (!angular.isDefined($scope.travelCurrencyList) || !angular.isDefined(query)) return [];
                        var results = $filter('filter')($scope.travelCurrencyList, { FromCurrency: query });
                        return results;
                    }
                    $scope.validateCurrency = function (text, $index) {
                        if (!angular.isDefined($scope.travelCurrencyList)) return;
                        var result = null;
                        for (var i = 0; i < $scope.travelCurrencyList.length; i++) {
                            if ($scope.travelCurrencyList[i].FromCurrency + ' ' + $scope.findDropdownExchangeType($scope.travelCurrencyList[i].ExchangeTypeID, $scope.travelCurrencyList[i].ExchangeRate) == text) {
                                result = $scope.travelCurrencyList[i];
                                break;
                            }
                        }
                        if (result) {
                            $scope.GE.receipt.currency_searchText = result.FromCurrency + ' ' + $scope.findDropdownExchangeType(result.ExchangeTypeID, result.ExchangeRate);
                            $scope.GE.receipt.OriginalCurrencyCode = result.FromCurrency;
                        } else if (travelCurrency_) {
                            $scope.GE.receipt.currency_searchText = travelCurrency_.FromCurrency + ' ' + $scope.findDropdownExchangeType(travelCurrency_.ExchangeTypeID, travelCurrency_.ExchangeRate);
                            $scope.GE.receipt.OriginalCurrencyCode = travelCurrency_.FromCurrency;
                        }
                        return;

                    }

                    $scope.tryToSelectCurrency = function (text, $index) {
                        if (!angular.isDefined($scope.travelCurrencyList)) return;
                        for (var i = 0; i < $scope.travelCurrencyList.length; i++) {
                            if ($scope.travelCurrencyList[i].FromCurrency + ' ' + $scope.findDropdownExchangeType($scope.travelCurrencyList[i].ExchangeTypeID, $scope.travelCurrencyList[i].ExchangeRate) == text) {
                                travelCurrency_ = $scope.travelCurrencyList[i];
                                break;
                            }
                        }
                        $scope.GE.receipt.currency_searchText = '';

                    }
                    /********************  auto complete currency end ******************* */



                    $scope.GE_provinceList_ = [];
                    getProvince();
                    //  should be get all posible province from all country TODO
                    function getProvince() {
                        var Country = 'TH';
                        var URL = CONFIG.SERVER + 'HRTR/GetProvinceByCountry/';
                        var oRequestParameter = { InputParameter: { "CountryCode": Country }, CurrentEmployee: getToken(CONFIG.USER), Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            $scope.GE_provinceList_ = response.data;

                        }, function errorCallback(response) {
                            console.log('error onSelectedCountry', response);
                        });
                    }

                    //Add by Nipon Supap 08/11/2017 [sp_ExpenseTypeGroupConversionGet]
                    $scope.getExpenseTypeGroupConversion = function (oExpenseTypeGroupID,FlagState) {
                        var URL = CONFIG.SERVER + 'HRTR/GetExpenseTypeGroupConversion/';
                        var oRequestParameter = { InputParameter: { "ExpenseTypeGroupID": oExpenseTypeGroupID, "CompanyCode": $scope.document.Requestor.CompanyCode } };

                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            if (response.data.length > 0) {
                                if (FlagState == "LoadEdit")
                                {
                                    $scope.GE.receiptItem.ExpenseTypeGroupID = response.data[0].NewExpenseTypeGroupID.toString();
                                    $scope.GE.receiptItem.ExpenseTypeGroupName = response.data[0].Name;
                                    $scope.GE.receiptItem.ExpenseTypeGroupRemark = response.data[0].Remark;
                                }
                                else if(FlagState == "LoadView")
                                {
                                    //$scope.expenseItems_ReceiptItem[0]
                                    $scope.GE.receipt.ExpenseReportReceiptItemList[0].ExpenseTypeGroupID = response.data[0].NewExpenseTypeGroupID.toString();
                                    $scope.GE.receipt.ExpenseReportReceiptItemList[0].ExpenseTypeGroupName = response.data[0].Name;
                                    $scope.GE.receipt.ExpenseReportReceiptItemList[0].ExpenseTypeGroupRemark = response.data[0].Remark;

                                    $scope.expenseItems_ReceiptItem[0].ExpenseTypeGroupID = response.data[0].NewExpenseTypeGroupID.toString();
                                    $scope.expenseItems_ReceiptItem[0].ExpenseTypeGroupName = response.data[0].Name;
                                    $scope.expenseItems_ReceiptItem[0].ExpenseTypeGroupRemark = response.data[0].Remark;
                                }
                            }
                        }, function errorCallback(response) {
                            $scope.tempFormData.chkVendorTaxIDDup = 'error';
                            console.log('error GeneralExpenseEditorController checkTaxIDDup.', response);
                        });
                    };

                    $scope.getPanelNameByExpenseType = function (expenseTypeID) {
                        var URL = CONFIG.SERVER + 'HRTR/GetEditorPanelByExpenseTypeID';
                        var oRequestParameter = { InputParameter: { "ExpenseTypeID": expenseTypeID }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            if (response.data && response.data.length > 0) {
                                $scope.panelName = response.data[0];
                                $scope.pnlRequireUploadFile = response.data[1] == 'True';
                                $scope.pnlNoOfFile = response.data[2];
                            } else {
                                $scope.panelName = '';
                            }

                            /* set default panel initial value */
                            if ($scope.GE.receiptItemEdit == false) {
                                $scope.GE.receiptItemFormData.Amount = 0;
                                $scope.GE.receiptItemFormData.AmountInRight = 9999999999;
                            }
                            if ($scope.panelName == '') {

                            } else if ($scope.panelName == 'pnlTransRegistration') {
                                if ($scope.GE.receiptItem.Bit1 == $scope.GE.receiptItem.Bit2) {
                                    $scope.GE.receiptItem.Bit1 = true;
                                    $scope.GE.receiptItem.Bit2 = false;
                                    $scope.GE.receiptItem.Str2 = "";
                                }
                            } else if ($scope.panelName == 'pnlMileage') {
                                if ($scope.oTranType != null && $scope.oTranType.length > 0 && $scope.GE.receiptItem && $scope.GE.receiptItem.Str3 == '') {
                                    $scope.GE.receiptItem.Str3 = $scope.oTranType[0].TransportationTypeCode;
                                    $scope.getMileageRate($scope.oTranType[0].TransportationTypeCode);
                                }


                            } else if ($scope.panelName == 'pnlTransCommutation') {
                                if ($scope.GE.receiptItemEdit) {
                                    $scope.GE.receiptItemDetailFormData.FlatLocationFilter.Key = '!' + $scope.GE.receiptItem.Str1;
                                } else {
                                    $scope.GE.receiptItemDetailFormData.FlatLocationFilter.Key = '!';
                                }
                                if ($scope.GE.receiptItem) {
                                    if ($scope.GE.receiptItem.Bit1 == $scope.GE.receiptItem.Bit2 && $scope.GE.receiptItem.Bit1 == $scope.GE.receiptItem.Bit3) {
                                        // default
                                        $scope.GE.receiptItem.Bit1 = false;
                                        $scope.GE.receiptItem.Bit2 = true;
                                        $scope.GE.receiptItem.Bit3 = false;
                                    }
                                    calculateAmountFromLocation($scope.GE.receiptItem.Str1, $scope.GE.receiptItem.Str2);
                                } else {
                                }
                            }

                            if ($scope.panelName == 'pnlAccommodation') {
                                if ($scope.GE.receiptItem && !$scope.GE.receiptItemEdit) {
                                    $scope.GE.receiptItem.Date1 = $scope.savedLimitMinDate;
                                    $scope.GE.receiptItem.Date2 = $scope.savedLimitMaxDate;
                                } else if ($scope.GE.receiptItem && $scope.GE.receiptItemEdit && expenseTypeID != $scope.GE.receiptItemEditObj.ExpenseTypeID) {
                                    $scope.GE.receiptItem.Date1 = $scope.savedLimitMinDate;
                                    $scope.GE.receiptItem.Date2 = $scope.savedLimitMaxDate;
                                }
                                if ($scope.pageClassName == 'TravelReport' && $scope.panelName == 'pnlAccommodation') {
                                    $scope.GetTravelers();
                                }
                                // roommate owner
                                if (angular.isDefined($scope.GE.receiptItem) && $scope.GE.receiptItem != null) {
                                    if ($scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList == null) {
                                        $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList = [];
                                    }

                                    var isExist = false;
                                    $.each($scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList, function (index, roommate) {
                                        if (roommate.EmployeeID == $scope.document.Requestor.EmployeeID) {
                                            isExist = true;
                                            return false;
                                        }
                                    });

                                    if (!isExist) {
                                        var objOwnerRoommate = angular.copy($scope.model.data.ExpenseReportReceiptItemRoommate[0]);
                                        objOwnerRoommate.EmployeeID = $scope.document.Requestor.EmployeeID;
                                        objOwnerRoommate.IsOwner = true;
                                   
                                        //objOwnerRoommate.AmountLimit = $scope.getCalculateOwnerAccommodation();
                                        //objOwnerRoommate.IsUnlimit = (objOwnerRoommate.AmountLimit == 999999999) ? true : false;
                                        $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.push(objOwnerRoommate);
                                        var currentIndex = $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.length - 1;
                                        $scope.CalculateAccommodation($scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[currentIndex], currentIndex);
                                        console.log('++++++++++++++ objOwnerRoommate +++++++++++++++', objOwnerRoommate);
                                    }
                                }
                            } else {
                                if (angular.isDefined($scope.GE.receiptItem) && $scope.GE.receiptItem != null) {
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList = [];
                                }
                            }
                            //add for CR UAT014 
                            if ($scope.panelName == 'pnlAccommutationForLastNight') {
                                $scope.GE.receiptItem.Date1 = $scope.savedLimitMaxDate;
                                $scope.GE.receiptItem.Date2 = $scope.savedLimitMaxDate;
                            }

                            //Nun Add 25/08/2016
                            if ($scope.panelName == 'pnlVehicleRent' || $scope.panelName == 'pnlVehicleRentAndCarTypeLicense') {
                                if ($scope.GE.receiptItem && !$scope.GE.receiptItemEdit) {
                                    $scope.GE.receiptItem.Date1 = $scope.savedLimitMinDate;
                                    $scope.GE.receiptItem.Date2 = $scope.savedLimitMaxDate;
                                }
                                $scope.getExpenseTypeAmountInRight(expenseTypeID);
                            }

                            // reset Amount in right when change Expense Type
                            else {
                                $scope.amountInRight = 9999999999;
                                $scope.GE.receiptItemFormData.AmountInRight = 9999999999;
                            }

                            //
                            /* ! set default panel initial value */

                            console.log('ExpenseReceiptEditorDirective panelName.', $scope.panelName);


                        }, function errorCallback(response) {
                            console.log('error ExpenseReceiptEditorDirective panelName.', response);
                        });
                    };

                 

                    $scope.changeShowWhtTax = function () {
                        if ($scope.whtSettings.ENABLEWHT == true) {        //ถ้าไม่ได้เปิด Panel WHTTax ไม่ต้อง bind ค่า WHTType,WHTCode
                            $scope.fillWHTTax();
                            $scope.changeWHTType1();
                        }
                    };

                    $scope.fillWHTTax = function () {
                        var oVendor = $scope.GE.receipt.VendorCode;
                        var oVendorCountry = $scope.GE.receipt.VendorCountry;
                        $scope.onBindWHTType(oVendor, oVendorCountry);
                    };

                    $scope.changeWHTType1 = function () {
                        var oVendorCountry = $scope.GE.receipt.VendorCountry;
                        $scope.onBindWHTCode1($scope.GE.receipt.WHTType1, oVendorCountry);
                        $scope.onBlurWHTAmount();
                    };
                    $scope.changeWHTType2 = function () {
                        var oVendorCountry = $scope.GE.receipt.VendorCountry;
                        $scope.onBindWHTCode2($scope.GE.receipt.WHTType2, oVendorCountry);
                        $scope.onBlurWHTAmount();
                    };

                    $scope.onBindWHTType = function (VendorCode, CountryCode) {

                        var URL = CONFIG.SERVER + 'HRTR/GetWHTTypeByVendorCode/';

                        var mapType = ($scope.pageClassName == 'GeneralExpenseReport' || $scope.pageClassName == 'TravelReport') ? 'TE' : 'EX';

                        var oRequestParameter = { InputParameter: { "VendorCode": VendorCode, "CountryCode": CountryCode, "MappingType": mapType }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };

                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {


                            if (response.data != null) {
                                var oAll = { "WithholdingType": "", "Description": "Select or Search" };
                                var oWHTType = response.data;
                                oWHTType.unshift(oAll);
                                $scope.objWHTType = oWHTType;
                            }

                            console.log('objWHTType.', $scope.objWHTType);
                        }, function errorCallback(response) {
                            console.log('error GeneralExpenseEditorController GetWHTTypeByVendorCode.', response);
                        });
                    };

                    $scope.onBindWHTCode1 = function (WHTType, CountryCode) {
                        var URL = CONFIG.SERVER + 'HRTR/GetWHTCodeByWHTType/';
                        var oRequestParameter = { InputParameter: { "WHTType": WHTType, "CountryCode": CountryCode }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };

                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {

                            if (response.data != null) {
                                var oAll = { "WithholdingCode": "", "Description": "Select or Search" };
                                var oWHTCode = response.data;
                                oWHTCode.unshift(oAll);
                                $scope.objWHTCode = oWHTCode;
                            }


                            console.log('objWHTCode.', $scope.objWHTCode);
                        }, function errorCallback(response) {
                            console.log('error GeneralExpenseEditorController GetWHTCodeByWHTType.', response);
                        });
                    };
                    $scope.onBindWHTCode2 = function (WHTType, CountryCode) {
                        var URL = CONFIG.SERVER + 'HRTR/GetWHTCodeByWHTType/';
                        var oRequestParameter = { InputParameter: { "WHTType": WHTType, "CountryCode": CountryCode }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };

                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {

                            if (response.data != null) {
                                var oAll = { "WithholdingCode": "", "Description": "Select or Search" };
                                var oWHTCode = response.data;
                                oWHTCode.unshift(oAll);
                                $scope.objWHTCode2 = oWHTCode;
                            }


                            console.log('objWHTCode.', $scope.objWHTCode);
                        }, function errorCallback(response) {
                            console.log('error GeneralExpenseEditorController GetWHTCodeByWHTType.', response);
                        });
                    };

                    $scope.onBlurWHTAmount = function () {
                        var whtAmount1 = Number($scope.GE.receipt.WHTAmount1LC);
                        var whtAmount2 = Number($scope.GE.receipt.WHTAmount2LC);
                        var oSumWHT = 0;

                        if ($scope.GE.receipt.WHTType1 != null && $scope.GE.receipt.WHTType1 != '') {
                            if ($scope.tempData.oWHTTypeSubsidise.indexOf($scope.GE.receipt.WHTType1) > -1) {
                                //In the array!
                            } else {
                                oSumWHT = oSumWHT + whtAmount1; //Not in the array 
                            }
                        }

                        if ($scope.GE.receipt.WHTType2 != null && $scope.GE.receipt.WHTType2 != '') {
                            if ($scope.tempData.oWHTTypeSubsidise.indexOf($scope.GE.receipt.WHTType2) > -1) {
                                //In the array!
                            } else {
                                oSumWHT = oSumWHT + whtAmount2; //Not in the array 
                            }
                        }
                        $scope.tempData.sumWHTAmount = oSumWHT;
                    };


                    $scope.isSelectPayToCompany = false;

                    $scope.selectPayTo = function () {
                        var param = $scope.GE.receipt.PayTo;

                        if (param == 'V') {
                            if ($scope.GE.receipt.VendorCode.substring(0, 2) == 'PX') {
                                $scope.isSelectPayToCompany = false;
                            }
                            else {
                                $scope.isSelectPayToCompany = true;
                            }
                        }
                        else {
                            $scope.isSelectPayToCompany = false;
                        }

                    }
                    $scope.IsEditVendor = false;

               
                    $scope.onSelectedVatType = function (oVatPercent) {
                        console.log('oVatPercent', oVatPercent);

                        
                        var VATBaseAmount = Number($scope.GE.receipt.VATBaseAmount);
                        VATBaseAmount = isNaN(VATBaseAmount) ? 0 : $scope.MathRounding(VATBaseAmount);
                        var VatPercent = Number(oVatPercent);
                        VatPercent = isNaN(VatPercent) ? 0 : VatPercent;
                        $scope.GE.receipt.VATPercent = VatPercent;
                        $scope.GE.receipt.VATAmount = $scope.MathRounding(VATBaseAmount * VatPercent / 100);


                       
                    }
                    $scope.document.temp_original_VATAmount = 0;
                    $scope.original_vatAmount = function (VATCode) {
                        var VATCode;
                        if ($scope.pageClassName == 'GeneralExpenseReport') {
                            var VATCode = $scope.GE.receipt.VATCode;
                        } else {

                            var VATCode = $scope.GE.receipt.VATCode;
                        }
                       
                       
                        var item;
                        for (var i = 0; i < $scope.setVatTypeDefault.length; i++) {
                            if ($scope.setVatTypeDefault[i].VATCode == VATCode) {
                                item = $scope.setVatTypeDefault[i];
                                break;
                            }
                        }
                        var VATBaseAmount = Number($scope.GE.receipt.LocalNoVATTotalAmount);
                        VATBaseAmount = isNaN(VATBaseAmount) ? 0 : $scope.MathRounding(VATBaseAmount);
                        var VatPercent = Number(item.Percent);
                        VatPercent = isNaN(VatPercent) ? 0 : VatPercent;
                        $scope.document.temp_original_VATAmount = $scope.MathRounding(VATBaseAmount * VatPercent / 100);
                    }
                    $scope.checkOriginal = function (param1) {

                        if (param1 !== $scope.GE.receipt.VATAmount) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                   
                    


                    $scope.personalCurrency = null;
                    $scope.getPersonalCurrency = function () {
                        var URL = CONFIG.SERVER + 'HRTR/GetPersonalExchangeRateCapture';
                        var oRequestParameter = { InputParameter: {}, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            $scope.personalCurrency = response.data[0];

                            if ($scope.settings.ExpenseTypeGroupInfo.prefix.toUpperCase() == 'GE') {
                                $scope.travelCurrencyList = response.data;
                            } else {
                                $scope.travelCurrencyList = angular.copy($scope.masterData.travelCurrencyList);
                                if ($scope.travelCurrencyList == null) {
                                    $scope.travelCurrencyList = [];
                                }
                                if ($scope.personalCurrency != null) {
                                    $scope.travelCurrencyList.unshift(angular.copy($scope.personalCurrency));
                                }
                            }

                          

                        }, function errorCallback(response) {
                            console.log('error GeneralExpenseEditorController GetPersonalCurrency.', response);
                        });
                    };
                    $scope.getPersonalCurrency();




                    $scope.getVendorByVendorCode = function (VendorCode) {
                        var URL = CONFIG.SERVER + 'HRTR/GetVendorByVendorCode';
                        var oRequestParameter = { InputParameter: { "VENDORCODE": VendorCode }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            if (response.data != null)
                            {
                                $scope.tempData.oVendorSelected = response.data;

                                $scope.autocomplete_GE_province.selectedItem = null;
                                $scope.GE.receipt.VendorCity = '';

                                //Nun Add 07102016
                                if ($scope.tempLastVendorCode == '' ||
                                    ($scope.tempLastVendorCode.substring(0, 2) != 'PX') ||
                                    ($scope.tempLastVendorCode.substring(0, 2) == 'PX' && $scope.tempData.oVendorSelected.VendorCode.substring(0, 2) != 'PX')) {
                                    //

                                    if ($scope.tempData.oVendorSelected.VendorCode.substring(0, 2) != 'PX') {
                                        $scope.GE.receipt.VendorName = $scope.tempData.oVendorSelected.VendorName;
                                    } else {
                                        $scope.GE.receipt.VendorName = '';
                                    }

                                    
                                    $scope.GE.receipt.VendorAddress1 = $scope.tempData.oVendorSelected.VendorAddress1;
                                    $scope.GE.receipt.VendorAddress2 = $scope.tempData.oVendorSelected.VendorAddress2;
                                    $scope.GE.receipt.VendorAddress3 = $scope.tempData.oVendorSelected.VendorAddress3;
                                    $scope.GE.receipt.VendorAddress4 = $scope.tempData.oVendorSelected.VendorAddress4;
                                    $scope.GE.receipt.VendorPostCode = $scope.tempData.oVendorSelected.PostalCode;
                                    $scope.GE.receipt.VendorTaxID = $scope.tempData.oVendorSelected.TaxID;
                                    $scope.GE.receipt.VendorPhone = $scope.tempData.oVendorSelected.Telephone1;
                                    $scope.GE.receipt.VendorFax = $scope.tempData.oVendorSelected.Fax;
                                    $scope.GE.receipt.VendorEmail = $scope.tempData.oVendorSelected.Mail;

                                    //Nun Add 07102016
                                }
                                $scope.tempLastVendorCode = $scope.tempData.oVendorSelected.VendorCode;
                                //

                                // add by ford to default country on second reciept for SIT PTT 21/04/2017 
                                var filteredItem = $filter('filter')($scope.objCountry, { CountryCode: $scope.tempData.oVendorSelected.CountryCode });
                                $scope.GE.receipt.VendorCountry = filteredItem[0].CountryCode;
                                $scope.searchText = filteredItem[0].CountryName;
                                $scope.autocomplete.selectedItem = filteredItem[0];
                                if (filteredItem[0].CountryCode == 'TH') {
                                    if ($scope.tempData.oVendorSelected.City != null && $scope.tempData.oVendorSelected.VendorCode.substring(0, 2) == 'PX') {
                                        var filteredItem_city = $filter('filter')($scope.GE_provinceList_, { ProvinceName: $scope.tempData.oVendorSelected.City });
                                        $scope.GE.receipt.VendorCity = filteredItem_city[0].ProvinceName;
                                        $scope.searchText_GE_province = filteredItem_city[0].ProvinceName;
                                        $scope.autocomplete_GE_province.selectedItem = filteredItem_city[0];

                                    } else {
                                        if ($scope.tempData.oVendorSelected.VendorCode.substring(0, 2) != 'PX') {
                                            $scope.GE.receipt.VendorCity = '';
                                        }
                                        $scope.searchText_GE_province = '';
                                        $scope.GE.receipt.VendorCity = $scope.tempData.oVendorSelected.City;
                                    }

                                } else {
                                    $scope.GE.receipt.VendorCity = $scope.tempData.oVendorSelected.City;
                                }
                                //

                                for (var i = 0; i < $scope.setPaymentMethod.length; i++) {
                                    if ($scope.setPaymentMethod[i].PaymentMethodCodeConcatDesc.indexOf($scope.tempData.oVendorSelected.PaymentMethodCode) != -1) {
                                        $scope.GE.receipt.PaymentMethodCode = $scope.setPaymentMethod[i].PaymentMethodCodeConcatDesc;
                                        break;
                                    }
                                }

                                for (var j = 0; j < $scope.setPaymentSupplement.length; j++) {
                                    if ($scope.setPaymentSupplement[j].PaymentSupplementCode == $scope.tempData.oVendorSelected.PaymentSupplementCode) {
                                        $scope.GE.receipt.PaymentSupplementCode = $scope.setPaymentSupplement[j].PaymentSupplementCode;
                                        break;
                                    }
                                }

                                for (var k = 0; k < $scope.setPaymentTerm.length; k++) {
                                    if ($scope.setPaymentTerm[k].PaymentTermCodeConcatDayLimit.indexOf($scope.tempData.oVendorSelected.PaymentTermCode) != -1) {
                                        $scope.GE.receipt.PaymentTermCode = $scope.setPaymentTerm[k].PaymentTermCodeConcatDayLimit;
                                        break;
                                    }
                                }


                                console.log('oVendorCode', $scope.tempData.oVendorSelected.VendorCode);


                                if ($scope.tempData.oVendorSelected.VendorCode.substring(0, 2) == 'PX') {
                                    $scope.isSelectPayToCompany = false;
                                    $scope.IsEditVendor = true;
                                }
                                else {
                                    if ($scope.GE.receipt.PayTo == 'V') {
                                        $scope.isSelectPayToCompany = true;
                                    }
                                    else {
                                        $scope.isSelectPayToCompany = false;
                                    }
                                    $scope.IsEditVendor = false;
                                }
                                if ($scope.whtSettings.ENABLEWHT) {
                                    $scope.changeShowWhtTax();
                                }

                            }
                          
                        }, function errorCallback(response) {
                            console.log('error GeneralExpenseEditorController GetPersonalCurrency.', response);
                        });
                    };

                    // Add by Jirawat jannet @ 2018-07-19
                    // binding กับ checkbox onetime vendor
                    // ทำการ reset ค่า Vendor เพื่อให้หรอกใหม่ เมื่อมีการเปลี่ยนค่า checkbox
                    $scope.onCheckOneTimeSelect = function (chk) {
                        $scope.GE.receipt.VendorCode = null;
                        $scope.GE.receipt.VendorName = '';
                        $scope.GE.receipt.VendorAddress1 = '';
                        $scope.GE.receipt.VendorAddress2 = '';

                        $scope.autocomplete.selectedItem = null;
                        $scope.GE.receipt.VendorCountry = '';


                        $scope.autocomplete_GE_province.selectedItem = null;
                        $scope.GE.receipt.VendorCity = '';

                        $scope.GE.receipt.VendorPostCode = '';
                        $scope.GE.receipt.VendorTaxID = '';
                        $scope.GE.receipt.VendorPhone = '';
                        $scope.GE.receipt.VendorFax = '';
                    }

                    $scope.onSelectedVendor = function (oVendor) {
                        $scope.getVendorByVendorCode(oVendor.VendorCode);
                    }


                    $scope.changeAlternativePayee = function () {
                        if ($scope.GE.receipt.IsAlternativePayee == true) {
                            $scope.IsEditVendor = true;
                        }
                        else {
                            $scope.IsEditVendor = false;
                        }
                    };

                    $scope.GoToSubMain_DeleteGEReceiptItem = function (receiptItem) {
                        if (angular.isDefined(receiptItem)) {
                            $scope.showChangeAmountWarning = false;
                            // delete receiptItem

                            var confirm = $mdDialog.confirm()
                                .title("INFORMATION")
                                .textContent($scope.Text['SYSTEM']['CONFIRM_DELETE'])
                                .ok('OK')
                                .cancel('Cancel');
                            $mdDialog.show(confirm).then(function (result) {
                                for (var i = 0; i < $scope.GE.receipt.ExpenseReportReceiptItemList.length; i++) {
                                    if ($scope.GE.receipt.ExpenseReportReceiptItemList[i] == receiptItem) {
                                        $scope.GE.receipt.ExpenseReportReceiptItemList.splice(i, 1);
                                        break;
                                    }
                                }
                                if ($scope.GE.receipt.ExpenseReportReceiptItemList.length == 0) {
                                    $scope.GE.receipt.ExpenseReportReceiptItemList = null;
                                }

                                var oSumOriginalAmountDetailList = 0;
                                var oSumLocalAmountDetailList = 0;
                                if ($scope.GE.receipt.ExpenseReportReceiptItemList != null) {
                                    for (var i = 0; i < $scope.GE.receipt.ExpenseReportReceiptItemList.length; i++) {
                                        if ($scope.GE.receipt.ExpenseReportReceiptItemList[i].ExpenseReportReceiptItemDetailList != null) {
                                            for (var j = 0; j < $scope.GE.receipt.ExpenseReportReceiptItemList[i].ExpenseReportReceiptItemDetailList.length; j++) {
                                                var originalAmount = Number($scope.GE.receipt.ExpenseReportReceiptItemList[i].ExpenseReportReceiptItemDetailList[j].OriginalAmount);
                                                oSumOriginalAmountDetailList += isNaN(originalAmount) ? 0 : originalAmount;

                                                var localAmount = Number($scope.GE.receipt.ExpenseReportReceiptItemList[i].ExpenseReportReceiptItemDetailList[j].LocalAmount);
                                                oSumLocalAmountDetailList += isNaN(localAmount) ? 0 : $scope.MathRounding(localAmount);
                                            }
                                        }
                                    }
                                }
                                oSumOriginalAmountDetailList = $scope.MathRounding(oSumOriginalAmountDetailList);
                                oSumLocalAmountDetailList = $scope.MathRounding(oSumLocalAmountDetailList);
                                $scope.GE.receipt.OriginalTotalAmount = oSumOriginalAmountDetailList;
                                $scope.GE.receipt.LocalNoVATTotalAmount = oSumLocalAmountDetailList;
                                $scope.GE.receipt.VATBaseAmount = oSumLocalAmountDetailList;
                                // calculate vat
                                var VATBaseAmount = Number($scope.GE.receipt.VATBaseAmount);
                                VATBaseAmount = isNaN(VATBaseAmount) ? 0 : $scope.MathRounding(VATBaseAmount);
                                var VatPercent = Number($scope.GE.receipt.VATPercent);
                                VatPercent = isNaN(VatPercent) ? 0 : VatPercent;
                                $scope.GE.receipt.VATAmount = $scope.MathRounding(VATBaseAmount * VatPercent / 100);

                                // -- gen data for table
                                $scope.expenseItems_ReceiptItem = $scope.getExpenseItems_ReceiptItem();
                                // -- !gen data for table

                                console.log('oSumLocalAmountDetailList.', oSumLocalAmountDetailList);
                            }, function () {
                                event.preventDefault();
                            });
                        }
                    };

                    $scope.GoToSubMain_AddGEReceiptItemFinish = function () {
                        $scope.showChangeAmountWarning = false;
                        // Nun loop start
                        for (var i = 0; i < $scope.expenseTypeGroup.length ; i++) {
                            if ($scope.expenseTypeGroup[i].ExpenseTypeGroupID.toString() == $scope.GE.receiptItem.ExpenseTypeGroupID.toString()) {
                                $scope.GE.receiptItem.ExpenseTypeGroupName = $scope.expenseTypeGroup[i].Name;
                                $scope.GE.receiptItem.ExpenseTypeGroupRemark = $scope.expenseTypeGroup[i].Remark;
                                break;
                            }
                        }
                        for (var j = 0; j < $scope.expenseType.length ; j++) {
                            if ($scope.expenseType[j].ExpenseTypeID.toString() == $scope.GE.receiptItem.ExpenseTypeID.toString()) {
                                $scope.GE.receiptItem.ExpenseTypeName = $scope.expenseType[j].Name;
                                break;
                            }
                        }
                        for (var k = 0; k < $scope.setVatTypeDefault.length ; k++) {
                            if ($scope.setVatTypeDefault[k].VATCode.toString() == $scope.GE.receipt.VATCode.toString()) {
                                $scope.GE.receipt.VATPercent = $scope.setVatTypeDefault[k].Percent;
                                break;
                            }
                        }
                        // Nun loop end

                        if (!$scope.GE.receiptItemEdit) {
                            // finish from add
                            if ($scope.GE.receipt.ExpenseReportReceiptItemList == null) {
                                $scope.GE.receipt.ExpenseReportReceiptItemList = [];
                            }
                            $scope.GE.receipt.ExpenseReportReceiptItemList.push($scope.GE.receiptItem);
                        } else {
                            // finish from edit
                            for (var i = 0; i < $scope.GE.receipt.ExpenseReportReceiptItemList.length; i++) {
                                if ($scope.GE.receipt.ExpenseReportReceiptItemList[i] == $scope.GE.receiptItemEditObj) {
                                    $scope.GE.receipt.ExpenseReportReceiptItemList[i] = $scope.GE.receiptItem;
                                    break;
                                }
                            }
                        }

                        var oSumOriginalAmountDetailList = 0;
                        var oSumLocalAmountDetailList = 0;
                        if ($scope.GE.receipt.ExpenseReportReceiptItemList != null) {
                            for (var i = 0; i < $scope.GE.receipt.ExpenseReportReceiptItemList.length; i++) {
                                if ($scope.GE.receipt.ExpenseReportReceiptItemList[i].ExpenseReportReceiptItemDetailList != null) {
                                    for (var j = 0; j < $scope.GE.receipt.ExpenseReportReceiptItemList[i].ExpenseReportReceiptItemDetailList.length; j++) {
                                        var originalAmount = Number($scope.GE.receipt.ExpenseReportReceiptItemList[i].ExpenseReportReceiptItemDetailList[j].OriginalAmount);
                                        oSumOriginalAmountDetailList += isNaN(originalAmount) ? 0 : originalAmount;

                                        var localAmount = $scope.multiplyExchangeRate(Number($scope.GE.receipt.ExpenseReportReceiptItemList[i].ExpenseReportReceiptItemDetailList[j].OriginalAmount), $scope.GE.receipt.ExchangeRate, $scope.GE.receipt.FromCurrencyRatio, $scope.GE.receipt.ToCurrencyRatio);
                                        oSumLocalAmountDetailList += isNaN(localAmount) ? 0 : $scope.MathRounding(localAmount);
                                    }
                                }
                            }
                        }
                        oSumOriginalAmountDetailList = $scope.MathRounding(oSumOriginalAmountDetailList);
                        oSumLocalAmountDetailList = $scope.MathRounding(oSumLocalAmountDetailList);
                        $scope.GE.receipt.OriginalTotalAmount = oSumOriginalAmountDetailList;
                        $scope.GE.receipt.LocalNoVATTotalAmount = oSumLocalAmountDetailList;
                        $scope.GE.receipt.VATBaseAmount = oSumLocalAmountDetailList;
                        console.log('oSumLocalAmountDetailList.', oSumLocalAmountDetailList);

                        var VATBaseAmount = Number($scope.GE.receipt.VATBaseAmount);
                        VATBaseAmount = isNaN(VATBaseAmount) ? 0 : $scope.MathRounding(VATBaseAmount);
                        var VatPercent = Number($scope.GE.receipt.VATPercent);
                        VatPercent = isNaN(VatPercent) ? 0 : VatPercent;
                        $scope.GE.receipt.VATAmount = $scope.MathRounding(VATBaseAmount * VatPercent / 100);

                        $scope.GE.receiptItemEditObj = null;
                        $scope.GE.receiptItem = null;
                        $scope.GE.receiptItemFormData.Amount = 0;

                        $scope.wizardFormFlag.isAddGEReceipt = true;
                        $scope.wizardFormFlag.isAddGEReceiptItem = false;
                        $scope.wizardFormFlag.isAddGEReceiptItemDetail = false;
                        console.log('Recipt Add Receipt Item Finish.', $scope.GE.receipt);
                    };

                    $scope.GoToSubMain_AddGEReceiptItemCancel = function () {

                        var confirm = $mdDialog.confirm()
                            .title($scope.Text['SYSTEM']['CONFIRM_REDIRECT_TITLE'])
                            .textContent($scope.Text['SYSTEM']['CONFIRM_CANCEL'])
                            .ok('OK')
                            .cancel('Cancel');
                        $mdDialog.show(confirm).then(function (result) {
                            $scope.GE.receiptItem = null;
                            $scope.GE.receiptItemFormData.Amount = 0;
                            $scope.wizardFormFlag.isAddGEReceipt = true;
                            $scope.wizardFormFlag.isAddGEReceiptItem = false;
                            $scope.wizardFormFlag.isAddGEReceiptItemDetail = false;
                            // clear form data

                            $window.scrollTo(0, 0);
                        }, function () {
                            event.preventDefault();
                        });


                        //if (confirm($scope.Text['SYSTEM']['CONFIRM_CANCEL'])) {
                        //    $scope.GE.receiptItem = null;
                        //    $scope.GE.receiptItemFormData.Amount = 0;
                        //    $scope.wizardFormFlag.isAddGEReceipt = true;
                        //    $scope.wizardFormFlag.isAddGEReceiptItem = false;
                        //    $scope.wizardFormFlag.isAddGEReceiptItemDetail = false;
                        //    // clear form data

                        //    $window.scrollTo(0, 0);
                        //}
                    };

                    $scope.GoToSubMain_AddGEReceiptItemDetail = function (receiptItemDetail) {
                        $scope.wizardFormFlag.isAddGEReceipt = false;
                        $scope.wizardFormFlag.isAddGEReceiptItem = false;
                        $scope.wizardFormFlag.isAddGEReceiptItemDetail = true;
                        if (angular.isUndefined(receiptItemDetail)) {
                            // new
                            var ObjReceiptItemDetail = $scope.model.data.ExpenseReportReceiptItemDetail[0];
                            $scope.GE.receiptItemDetailEdit = false;
                            $scope.GE.receiptItemDetail = angular.copy(ObjReceiptItemDetail);
                            $scope.GE.receiptItemDetail.RequestNo = $scope.document.RequestNo;
                            $scope.GE.receiptItemDetail.ReceiptID = $scope.GE.receipt.ReceiptID;
                            $scope.GE.receiptItemDetail.ItemID = $scope.GE.receiptItem.ItemID;

                            // set default CostCenter, IO
                            if (angular.isUndefined($scope.DefaultCostCenterIOByGroup[$scope.GE.receiptItem.ExpenseTypeGroupID])) {
                                $scope.GE.receiptItemDetail.CostCenter = $scope.DefaultCostCenterIO.CostCenter;
                                $scope.GE.receiptItemDetail.IsAlternativeCostCenter = $scope.DefaultCostCenterIO.IsAlternativeCostCenter;
                                $scope.GE.receiptItemDetail.AlternativeIOOrg = $scope.DefaultCostCenterIO.OrgUnitID;
                                $scope.GE.receiptItemDetail.IsAlternativeIOOrg = $scope.DefaultCostCenterIO.IsAlternativeIOOrg;
                                $scope.GE.receiptItemDetail.IO = $scope.DefaultCostCenterIO.IO;
                            } else {
                                var objDefault = $scope.DefaultCostCenterIOByGroup[$scope.GE.receiptItem.ExpenseTypeGroupID];
                                $scope.GE.receiptItemDetail.CostCenter = objDefault.CostCenter;
                                $scope.GE.receiptItemDetail.IsAlternativeCostCenter = objDefault.IsAlternativeCostCenter;
                                if (objDefault.IsAlternativeIOOrg && objDefault.OrgUnitID) {
                                    $scope.GE.receiptItemDetail.AlternativeIOOrg = objDefault.OrgUnitID;
                                } else {
                                    $scope.GE.receiptItemDetail.AlternativeIOOrg = $scope.DefaultCostCenterIO.OrgUnitID;
                                }
                                $scope.GE.receiptItemDetail.IsAlternativeIOOrg = objDefault.IsAlternativeIOOrg;
                                $scope.GE.receiptItemDetail.IO = objDefault.IO;
                            }
                            //if ($scope.GE.receiptItemDetail.IsAlternativeIOOrg) {
                            // get default IO by Org
                            $scope.getIOByOrganization($scope.GE.receiptItemDetail.AlternativeIOOrg);
                            //} else {
                            //    // get default IO by CostCenter
                            //    $scope.getIOByCostCenter($scope.GE.receiptItemDetail.CostCenter);
                            //}

                            if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                                // none, average
                            } else {
                                // exactly
                                if ($scope.projectCodes.length == 1) {
                                    $scope.GE.receiptItemDetail.ProjectCode = $scope.projectCodes[0].ProjectCode;
                                }
                            }

                            console.log('ReciptItemDetail New.', angular.copy($scope.GE.receiptItemDetail));
                        } else {
                            // edit
                            $scope.GE.receiptItemDetailEdit = true;
                            $scope.GE.receiptItemDetailEditObj = receiptItemDetail;
                            $scope.GE.receiptItemDetail = angular.copy(receiptItemDetail);

                            // set default CostCenter, IO
                            //if ($scope.GE.receiptItemDetail.IsAlternativeIOOrg) {
                            $scope.getIOByOrganization($scope.GE.receiptItemDetail.AlternativeIOOrg);
                            //} else {
                            //    $scope.getIOByCostCenter($scope.GE.receiptItemDetail.CostCenter);
                            //}

                            console.log('ReciptItemDetail Edit.', $scope.GE.receiptItemDetail);
                        }

                        $timeout(function () {
                            document.getElementById('top-anchor1').scrollIntoView();
                        }, 100);
                    };

                    $scope.GoToSubMain_DeleteGEReceiptItemDetail = function (receiptItemDetail) {
                        if (angular.isDefined(receiptItemDetail)) {
                            if (confirm($scope.Text['SYSTEM']['CONFIRM_DELETE'])) {
                                // delete receiptItemDetail
                                for (var i = 0; i < $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList.length; i++) {
                                    if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[i] == receiptItemDetail) {
                                        $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList.splice(i, 1);
                                        break;
                                    }
                                }
                                if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList.length == 0) {
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList = null;
                                }
                            }
                        }
                    };

                    $scope.GoToSubMain_AddGEReceiptItemDetailFinish = function () {
                        console.log('$scope.GE.receiptItem.', $scope.GE.receiptItem);
                        $scope.calculateAmountReceiptItem();
                        if (!$scope.GE.receiptItemDetailEdit) {
                            // finish from add
                            if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList == null) {
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList = [];
                            }
                            $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList.push($scope.GE.receiptItemDetail);
                        } else {
                            // finish from edit
                            for (var i = 0; i < $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList.length; i++) {
                                if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[i] == $scope.GE.receiptItemDetailEditObj) {
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[i] = $scope.GE.receiptItemDetail;
                                    break;
                                }
                            }
                        }


                        $scope.GE.receiptItemDetailEditObj = null;
                        $scope.GE.receiptItemDetail = null;
                        // calculate receiptItem amount
                        $scope.calculateAmountReceiptItem();
                        // clear form data

                        $scope.wizardFormFlag.isAddGEReceipt = false;
                        $scope.wizardFormFlag.isAddGEReceiptItem = true;
                        $scope.wizardFormFlag.isAddGEReceiptItemDetail = false;
                        console.log('Recipt Add Receipt Item Detail Finish.', $scope.GE.receiptItem);
                    };

                    $scope.GoToSubMain_AddGEReceiptItemDetailCancel = function () {
                        if (confirm($scope.Text['SYSTEM']['CONFIRM_CANCEL'])) {
                            $scope.GE.receiptItemDetail = null;
                            $scope.wizardFormFlag.isAddGEReceipt = false;
                            $scope.wizardFormFlag.isAddGEReceiptItem = true;
                            $scope.wizardFormFlag.isAddGEReceiptItemDetail = false;
                            // clear form data

                            $window.scrollTo(0, 0);
                        }

                    };

                    /* ====== !wizard form ====== */

                    $scope.getTemplate = function () {
                        console.log('getTemplate', 'views/hr/tr/data/panel/' + $scope.panelName + '.html?t=' + $scope.runtime);
                        return 'views/hr/tr/data/panel/' + $scope.panelName + '.html?t=' + $scope.runtime;
                        //$scope.panelName = 'pnlMileage'; return 'views/hr/tr/data/panel/pnlMileage.html?t=' + $scope.runtime;
                        //$scope.panelName = 'pnlTransCommutation'; return 'views/hr/tr/data/panel/pnlTransCommutation.html?t=' + $scope.runtime;
                    };

                    var clearPanelVariable = function () {
                        console.log('clear panel.');
                        if (angular.isDefined($scope.GE.receiptItem) && $scope.GE.receiptItem != null) {
                            $scope.GE.receiptItem.Str1 = "";
                            $scope.GE.receiptItem.Str2 = "";
                            $scope.GE.receiptItem.Str3 = "";
                            $scope.GE.receiptItem.Str4 = "";
                            $scope.GE.receiptItem.Str5 = "";
                            $scope.GE.receiptItem.Bit1 = false;
                            $scope.GE.receiptItem.Bit2 = false;
                            $scope.GE.receiptItem.Bit3 = false;
                            $scope.GE.receiptItem.Bit4 = false;
                            $scope.GE.receiptItem.Bit5 = false;
                            $scope.GE.receiptItem.Dec1 = 0;
                            $scope.GE.receiptItem.Dec2 = 0;
                            $scope.GE.receiptItem.Dec3 = 0;
                            $scope.GE.receiptItem.Dec4 = 0;
                            $scope.GE.receiptItem.Dec5 = 0;

                            $scope.GE.receiptItem.IsAccommodation = false;
                            $scope.GE.receiptItem.LocalAccommodationAmount = 0;
                            $scope.GE.receiptItem.LocalAccommodationRight = 0;
                            $scope.GE.receiptItem.IsUnlimitRight = false;
                        }
                    };

                    $scope.expenseTypeGroupIDChange = function () {
                        clearPanelVariable();
                        if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                            // none, average
                            if (angular.isUndefined($scope.DefaultCostCenterIOByGroup[$scope.GE.receiptItem.ExpenseTypeGroupID])) {
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].CostCenter = $scope.DefaultCostCenterIO.CostCenter;
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].IsAlternativeCostCenter = $scope.DefaultCostCenterIO.IsAlternativeCostCenter;
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].AlternativeIOOrg = $scope.DefaultCostCenterIO.OrgUnitID;
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].IsAlternativeIOOrg = $scope.DefaultCostCenterIO.IsAlternativeIOOrg;
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].IO = $scope.DefaultCostCenterIO.IO;
                            } else {
                                var objDefault = $scope.DefaultCostCenterIOByGroup[$scope.GE.receiptItem.ExpenseTypeGroupID];
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].CostCenter = objDefault.CostCenter;
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].IsAlternativeCostCenter = objDefault.IsAlternativeCostCenter;
                                if (objDefault.IsAlternativeIOOrg && objDefault.OrgUnitID) {
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].AlternativeIOOrg = objDefault.OrgUnitID;
                                } else {
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].AlternativeIOOrg = $scope.DefaultCostCenterIO.OrgUnitID;
                                }
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].IsAlternativeIOOrg = objDefault.IsAlternativeIOOrg;
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].IO = objDefault.IO;
                            }
                            //if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].IsAlternativeIOOrg) {
                            // get default IO by Org
                            $scope.getIOByOrganization($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].AlternativeIOOrg);
                            //} else {
                            //    // get default IO by CostCenter
                            //    $scope.getIOByCostCenter($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].CostCenter);
                            //}
                        }
                        $scope.getExpenseTypeByGroupID($scope.GE.receiptItem.ExpenseTypeGroupID, true);
                    };

                    $scope.expenseTypeChange = function () {
                        clearPanelVariable();
                        $scope.getPanelNameByExpenseType($scope.GE.receiptItem.ExpenseTypeID);
                    };
                    $scope.isReadonlySumAmount = function (panelName) {
                        if (panelName == 'pnlMileage' || panelName == 'pnlTransCommutation') {
                            return true;
                        }
                        return false;
                    };

                    $scope.changeOriginalAmount = function (objDetail, originalValue,type) {
                        if ($scope.panelName == "pnlAccommodation" && type == "rm") {
                            var originalValue = 0;
                            for (var i = 0; i < $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList.length; i++) {

                                originalValue += $scope.GE.receiptItem.ExpenseReportReceiptItemRoommateList[i].UsedAmount;
                            }
                            if (originalValue) {
                                var exchangeRate = Number($scope.GE.receipt.ExchangeRate);
                                objDetail.OriginalAmount = $scope.MathRounding(Number(originalValue));
                                if (!isNaN(exchangeRate)) {
                                    objDetail.LocalAmount = $scope.multiplyExchangeRate(objDetail.OriginalAmount, exchangeRate, $scope.GE.receipt.FromCurrencyRatio, $scope.GE.receipt.ToCurrencyRatio);
                                }
                            }
                        } else {
                            if (originalValue) {
                                var exchangeRate = Number($scope.GE.receipt.ExchangeRate);
                                objDetail.OriginalAmount = $scope.MathRounding(Number(originalValue));
                                if (!isNaN(exchangeRate)) {
                                    objDetail.LocalAmount = $scope.multiplyExchangeRate(objDetail.OriginalAmount, exchangeRate, $scope.GE.receipt.FromCurrencyRatio, $scope.GE.receipt.ToCurrencyRatio);
                                }
                            }
                        
                        }
                        
                    };

                    $scope.overided_changeReceiptCurrency = function (objCurrency) {
                        console.log('try to select cur', $scope.GE.receipt, objCurrency)
                        $scope.GE.receipt.OriginalCurrencyCode = objCurrency.FromCurrency;
                        $scope.changeReceiptCurrency(objCurrency);
                    
                    }
                    $scope.changeReceiptCurrency = function (objCurrency) {
                        console.log('objCurrency.', objCurrency);
                        $scope.GE.receipt.ExchangeRate = Number(objCurrency.ExchangeRate);
                        $scope.GE.receipt.OriginalCurrencyCode = objCurrency.FromCurrency;
                        $scope.GE.receipt.currency_searchText = objCurrency.FromCurrency + ' ' + $scope.findDropdownExchangeType(objCurrency.ExchangeTypeID, objCurrency.ExchangeRate);
                        $scope.GE.receipt.ExchangeDate = objCurrency.EffectiveDate;
                        $scope.GE.receipt.ExchangeTypeID = objCurrency.ExchangeTypeID;
                        $scope.GE.receipt.FromCurrencyRatio = objCurrency.RatioFromCurrencyUnit;
                        $scope.GE.receipt.ToCurrencyRatio = objCurrency.RatioToCurrencyUnit;
                        var sumLocalAmount = 0;
                        if (angular.isDefined($scope.GE.receipt.ExpenseReportReceiptItemList) && $scope.GE.receipt.ExpenseReportReceiptItemList != null) {
                            for (var i = 0; i < $scope.GE.receipt.ExpenseReportReceiptItemList.length; i++) {
                                var item = $scope.GE.receipt.ExpenseReportReceiptItemList[i];
                                if (angular.isDefined(item.ExpenseReportReceiptItemDetailList) && item.ExpenseReportReceiptItemDetailList != null) {
                                    for (var k = 0; k < item.ExpenseReportReceiptItemDetailList.length; k++) {
                                        item.ExpenseReportReceiptItemDetailList[k].LocalAmount = $scope.multiplyExchangeRate(item.ExpenseReportReceiptItemDetailList[k].OriginalAmount, $scope.GE.receipt.ExchangeRate, $scope.GE.receipt.FromCurrencyRatio, $scope.GE.receipt.ToCurrencyRatio);
                                        sumLocalAmount += $scope.MathRounding(item.ExpenseReportReceiptItemDetailList[k].LocalAmount);
                                        if (item.ExpenseReportReceiptItemDetailList[k].OriginalAmountInRight != 9999999999) {
                                            item.ExpenseReportReceiptItemDetailList[k].LocalAmountInRight = $scope.multiplyExchangeRate(item.ExpenseReportReceiptItemDetailList[k].OriginalAmountInRight, $scope.GE.receipt.ExchangeRate, $scope.GE.receipt.FromCurrencyRatio, $scope.GE.receipt.ToCurrencyRatio);
                                        } else {
                                            item.ExpenseReportReceiptItemDetailList[k].LocalAmountInRight = 9999999999;
                                        }
                                    }
                                }
                            }
                        }
                        $scope.GE.receipt.LocalNoVATTotalAmount = sumLocalAmount;
                        $scope.GE.receipt.VATBaseAmount = sumLocalAmount;
                        // calculate vat
                        var VATBaseAmount = Number($scope.GE.receipt.VATBaseAmount);
                        VATBaseAmount = isNaN(VATBaseAmount) ? 0 : $scope.MathRounding(VATBaseAmount);
                        var VatPercent = Number($scope.GE.receipt.VATPercent);
                        VatPercent = isNaN(VatPercent) ? 0 : VatPercent;
                        $scope.GE.receipt.VATAmount = $scope.MathRounding(VATBaseAmount * VatPercent / 100);

                        $scope.expenseItems_ReceiptItem = $scope.getExpenseItems_ReceiptItem();
                    };

                    /* panel : pnlAccommodation */
                    $scope.setSelectedBeginDate = function (selectedDate) {
                        //$scope.GE.receiptItem.Date1 = $filter('date')(selectedDate, 'yyyy-MM-dd');
                        if ($scope.GE.receiptItem.Date1 > $scope.GE.receiptItem.Date2) {
                            $scope.GE.receiptItem.Date2 = new Date($scope.GE.receiptItem.Date1);
                        }
                    };

                    $scope.setSelectedEndDate = function (selectedDate) {
                        $scope.GE.receiptItem.Date2 = $filter('date')(selectedDate, 'yyyy-MM-dd');
                    };
                    /* !panel : pnlAccommodation */



                    //Nun Add 26082016
                    /* panel : pnlVehicleRent */
                    $scope.changeBeginDate = function (selectedDate) {
                        $scope.GE.receiptItem.Date1 = $filter('date')(selectedDate, 'yyyy-MM-dd');
                        if ($scope.GE.receiptItem.Date1 > $scope.GE.receiptItem.Date2) {
                            $scope.GE.receiptItem.Date2 = $scope.GE.receiptItem.Date1;
                        }
                    };
                    $scope.changeEndDate = function (selectedDate) {
                        $scope.GE.receiptItem.Date2 = $filter('date')(selectedDate, 'yyyy-MM-dd');
                    };

                    $scope.calDiffDateDuration = function () {
                        var oDate1 = $filter('date')($scope.GE.receiptItem.Date1, 'yyyy-MM-dd');
                        var oDate2 = $filter('date')($scope.GE.receiptItem.Date2, 'yyyy-MM-dd');
                        if (oDate1 <= oDate2) {
                            var amountInRight = 0;
                            if ($scope.amountInRight != 9999999999) {
                                var dateStart = moment($scope.GE.receiptItem.Date1);
                                var dateEnd = moment($scope.GE.receiptItem.Date2);

                                var duration = moment.duration(dateEnd.startOf('day').diff(dateStart.startOf('day')));
                                var diff = duration.asDays();

                                if (diff >= 0) {
                                    var days = diff + 1;
                                    amountInRight = $scope.amountInRight * days;
                                }
                            } else {
                                amountInRight = 9999999999;
                            }

                            $scope.GE.receiptItemFormData.AmountInRight = amountInRight;
                            var exchangeRate = Number($scope.GE.receipt.ExchangeRate);
                            if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                                // none, average

                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].OriginalAmount = $scope.GE.receiptItemFormData.Amount;
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].LocalAmount = $scope.multiplyExchangeRate($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].OriginalAmount, exchangeRate, $scope.GE.receipt.FromCurrencyRatio, $scope.GE.receipt.ToCurrencyRatio);
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].OriginalAmountInRight = amountInRight;
                                if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].OriginalAmountInRight != 9999999999) {
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].LocalAmountInRight = $scope.multiplyExchangeRate(amountInRight, exchangeRate, $scope.GE.receipt.FromCurrencyRatio, $scope.GE.receipt.ToCurrencyRatio);
                                } else {
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].LocalAmountInRight = 9999999999;
                                }

                            }
                            else if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_EXACTLY) {
                                // exactly
                                console.log('oNunList1', $scope.GE.receiptItemFormData.Amount);

                                var sumAmount = 0;
                                if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList != null) {
                                    for (var i = 0; i < $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList.length; i++) {
                                        var amt = Number($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[i].OriginalAmount);
                                        sumAmount += isNaN(amt) ? 0 : amt;
                                    }
                                }
                                console.log('oNun2', sumAmount);
                                if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList != null) {
                                    for (var j = 0; j < $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList.length; j++) {
                                        var amount = Number($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[j].OriginalAmount);
                                        $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[j].OriginalAmountInRight = (amountInRight * amount) / sumAmount;
                                        if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[j].OriginalAmountInRight != 9999999999) {
                                            $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[j].LocalAmountInRight = $scope.multiplyExchangeRate(((amountInRight * amount) / sumAmount), exchangeRate, $scope.GE.receipt.FromCurrencyRatio, $scope.GE.receipt.ToCurrencyRatio);
                                        } else {
                                            $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[j].LocalAmountInRight = 9999999999;
                                        }

                                    }
                                }
                                console.log('oNun3', (amountInRight * amount) / sumAmount);
                            }
                        }
                    };


                    /* panel : pnlTransRegistration */
                    $scope.GetCarRegistration();

                    $scope.onChangeCarUsage = function (selectedCarUsageCompany) {
                        $scope.GE.receiptItem.Bit2 = !selectedCarUsageCompany;
                        //Nun Add 29052017
                        $scope.GE.receiptItem.Str1 = '';
                        //
                        $scope.GE.receiptItem.Str2 = '';
                        $scope.OnChangeCarRegistration($scope.GE.receiptItem.Str1);
                    };
                    $scope.carTypes_RegistrationList = []
                    $scope.OnChangeCarRegistration = function (oCarRegistrationDetail)
                    {
                        var oResultValue = $filter('filter')($scope.CarRegistrationList, { CarRegistrationDetail: oCarRegistrationDetail });
                        var oRequestSearch = '0';
                        if (oResultValue && oResultValue.length > 0 && $scope.GE.receiptItem.Bit1) {
                            oRequestSearch = oResultValue[0].CarRegistrationDetail;
                        }
                        var URL = CONFIG.SERVER + 'HRTR/GetCarTypeMappingCarRegistration';
                        var oRequestParameter = { InputParameter: { 'CARREGISTRATIONDETAIL': oRequestSearch, 'ISPERONALCAR': !$scope.GE.receiptItem.Bit1 }, CurrentEmployee: employeeData, Creator: $scope.document.Creator, Requestor: $scope.document.Requestor };
                        $http({
                            method: 'POST',
                            url: URL,
                            data: oRequestParameter
                        }).then(function successCallback(response) {
                            $scope.carTypes_RegistrationList = response.data;

                            
                            //Nun Add 29052017
                            if ($scope.carTypes_RegistrationList != null && $scope.carTypes_RegistrationList.length > 0) {
                                $scope.ddlDefaultCarType = $scope.carTypes_RegistrationList[0].Description;
                                if ($scope.GE.receiptItem.Bit1 && $scope.GE.receiptItem.Str1) {
                                    $scope.GE.receiptItem.Str2 = $scope.ddlDefaultCarType;
                                }
                            }
                            //


                            if (!$scope.carTypes_RegistrationList || $scope.carTypes_RegistrationList.length == 0) {
                                $mdDialog.show(
                                 $mdDialog.alert()
                                   .clickOutsideToClose(false)
                                   .title($scope.Text['SYSTEM']['INFORMATION'])
                                   .textContent(Text['SYSTEM']['CARREGISTERNOTCONFIG'])
                                   .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                               );
                            }

                        }, function errorCallback(response) {
                            $mdDialog.show(
                                $mdDialog.alert()
                                .clickOutsideToClose(false)
                                .title($scope.Text['SYSTEM']['INFORMATION'])
                                .textContent(response.data)
                                .ok($scope.Text['SYSTEM']['BUTTON_OK'])
                            );
                        });
                    }
                    /* !panel : pnlTransRegistration */

                    /* panel : pnlMileage */
                    $scope.setSelectedDepartDate = function (selectedDate) {
                        $scope.GE.receiptItem.Date1 = $filter('date')(selectedDate, 'yyyy-MM-dd');
                    };

                    $scope.onChangeDistance = function () {
                        if ($scope.GE.receiptItem != null) {
                            var distance = $scope.GE.receiptItem.Dec1;
                            $scope.GE.receiptItemFormData.Amount = $scope.MathRounding(Number(distance) * $scope.fuelRate);
                            $scope.GE.receiptItemFormData.AmountInRight = $scope.MathRounding(Number(distance) * $scope.mileageNoCalIncomeRate);
                            if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                                // none, average
                                var exchangeRate = Number($scope.GE.receipt.ExchangeRate);
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].OriginalAmount = $scope.GE.receiptItemFormData.Amount;
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].LocalAmount = $scope.multiplyExchangeRate($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].OriginalAmount, exchangeRate, $scope.GE.receipt.FromCurrencyRatio, $scope.GE.receipt.ToCurrencyRatio);
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].OriginalAmountInRight = $scope.GE.receiptItemFormData.AmountInRight;
                                if ($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].OriginalAmountInRight != 9999999999) {
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].LocalAmountInRight = $scope.multiplyExchangeRate($scope.GE.receiptItemFormData.AmountInRight, exchangeRate, $scope.GE.receipt.FromCurrencyRatio, $scope.GE.receipt.ToCurrencyRatio);
                                } else {
                                    $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].LocalAmountInRight = 9999999999;
                                }
                            }
                        }
                    };

                    

                    /* !panel : pnlMileage */

                    /* panel : pnlTransCommutation */
                    $scope.onChangeTravelRound = function (fieldToSetFlag) {

                        if (fieldToSetFlag == 1) {
                            if ($scope.GE.receiptItem.Bit2 == true || $scope.GE.receiptItem.Bit3 == true) {
                                $scope.GE.receiptItemFormData.Amount = $scope.MathRounding($scope.GE.receiptItemFormData.Amount * 2);
                            }
                        } else if (fieldToSetFlag == 2 || fieldToSetFlag == 3) {
                            if ($scope.GE.receiptItem.Bit1 == true) {
                                $scope.GE.receiptItemFormData.Amount = $scope.MathRounding($scope.GE.receiptItemFormData.Amount / 2);
                            }
                        }

                        $scope.GE.receiptItem.Bit1 = false;
                        $scope.GE.receiptItem.Bit2 = false;
                        $scope.GE.receiptItem.Bit3 = false;
                        if (fieldToSetFlag == 1) {
                            $scope.GE.receiptItem.Bit1 = true;
                        } else if (fieldToSetFlag == 2) {
                            $scope.GE.receiptItem.Bit2 = true;
                        } else if (fieldToSetFlag == 3) {
                            $scope.GE.receiptItem.Bit3 = true;
                        }
                    };

                    var calculateAmountFromLocation = function (start, end) {
                        var exchangeRate = Number($scope.GE.receipt.ExchangeRate);
                        if (start != '' && end != '' && start != end) {
                            var promise = GetFlatRateByLocation(start, end);
                            promise.then(function successCallback(response) {

                                //Old code
                                //var amount = response.data[0].Rate;                              

                                var amount = 0;
                                if (response.data && response.data.length > 0) {
                                    amount = response.data[0].Rate;

                                    //if ($scope.GE.receiptItem.Bit2 == true || $scope.GE.receiptItem.Bit3 == true) {
                                    //    amount = amount / 2;
                                    //}
                                    if ($scope.GE.receiptItem.Bit1 == true) {
                                        amount *= 2;
                                    }

                                    $scope.GE.receiptItemFormData.Amount = $scope.MathRounding(Number(amount));

                                    if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                                        // none, average
                                        $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].OriginalAmount = $scope.GE.receiptItemFormData.Amount;
                                        $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].LocalAmount = $scope.multiplyExchangeRate($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].OriginalAmount, exchangeRate, $scope.GE.receipt.FromCurrencyRatio, $scope.GE.receipt.ToCurrencyRatio);
                                    }
                                    console.log('calculateAmountFromLocation.', response.data[0].Rate);

                                } else {
                                    if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                                        // none, average
                                        $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].OriginalAmount = 0;
                                        $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].LocalAmount = 0;
                                    }

                                    $scope.p_message($scope.Text['EXPENSE']['NOFLATRATE'],'w');
                                }

                            }, function errorCallback(response) {
                                console.log('error calculateAmountFromLocation.', response);
                            });
                        } else {
                            $scope.GE.receiptItemFormData.Amount = 0;
                            if ($scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_NONE || $scope.settings.ProjectCodeMode == $scope.moduleSetting.PROJECTCODEMODE_AVERAGE) {
                                // none, average
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].OriginalAmount = $scope.GE.receiptItemFormData.Amount;
                                $scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].LocalAmount = $scope.multiplyExchangeRate($scope.GE.receiptItem.ExpenseReportReceiptItemDetailList[0].OriginalAmount, exchangeRate, $scope.GE.receipt.FromCurrencyRatio, $scope.GE.receipt.ToCurrencyRatio);
                            }
                        }
                    }
                    $scope.onSelectStartLocation = function (selected) {
                        // filter out EndLocation
                        $scope.GE.receiptItemDetailFormData.FlatLocationFilter.Key = '!' + selected.Key;
                        if ($scope.GE.receiptItem.Str2 == selected.Key) {
                            $scope.GE.receiptItem.Str2 = '';
                        }
                        // calculate 
                        calculateAmountFromLocation($scope.GE.receiptItem.Str1, $scope.GE.receiptItem.Str2);
                    };
                    $scope.onSelectEndLocation = function (selected) {
                        calculateAmountFromLocation($scope.GE.receiptItem.Str1, $scope.GE.receiptItem.Str2);
                    };
                    /* !panel : pnlTransCommutation */

                    $scope.onClickReceiptDate = function () {
                        if ($scope.GE.receipt.ReceiptDate){
                            $scope.directiveFn($scope.GE.receipt.ReceiptDate);
                        }
                        else {
                            $scope.directiveFn($scope.savedLimitMinDate);
                        }
                        document.getElementById('expense-receipt-date').click();
                    };

                    $scope.onClickRequestReceiptDate = function () {
                        if ($scope.GE.receipt.ReceiptDate) {
                            $scope.directiveFn($scope.GE.receipt.ReceiptDate);
                        }
                        else {
                            $scope.directiveFn($scope.savedLimitMinDate);
                        }
                        document.getElementById('expense-request-receipt-date').click();
                    };
                    $scope.receiptDate;

                    $scope.changeDate = function () {
                        $scope.receiptDate = $('#dateReceipt[name="datefilter"]').val()
                        $scope.GE.receipt.ReceiptDate = $filter('date')(moment($scope.receiptDate, "DD/MM/YYYY").toDate(), 'yyyy-MM-ddT00:00:00');
                        
                    }
                    $scope.initReceiptDateCalendar = function () {
                        $('input[name="datefilter"]').daterangepicker({
                            singleDatePicker: true,
                            autoUpdateInput: true,
                            "startDate": new Date(),
                            "endDate": new Date(),
                            locale: {
                                cancelLabel: 'Clear',
                                format: 'DD/MM/YYYY'
                            }
                        });

                        $('input[name="datefilter"]').on('apply.daterangepicker', function (ev, picker) {
                            $(this).val(picker.startDate.format('DD/MM/YYYY'));
                        });

                        $('input[name="datefilter"]').on('cancel.daterangepicker', function (ev, picker) {
                            $(this).val('');
                        });

                        $scope.receiptDate = moment().format('DD/MM/YYYY');

                        //$scope.changeDate();
                    } 

                }],
                link: function (scope, element, attrs, controller) {
                }
            };
        })





        // other contr
        .controller('ExpenseReceiptAttachmentController', ['$scope', '$http', '$routeParams', '$location', '$filter', '$window', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, $window, CONFIG, $mdDialog) {
            var employeeData = employeeData;
            $scope.p_message = function (message, type) {
                if (type == 'w') {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title('Warning')
                            .textContent(message)
                            .ariaLabel('Warning')
                            .ok('OK')
                    );
                } else {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title($scope.Text['SYSTEM']['INFORMATION'])
                            .textContent(message)
                            .ariaLabel($scope.Text['SYSTEM']['INFORMATION'])
                            .ok('OK')
                    );
                }
            }
            
            var InitialValue = function () {
                $scope.formReceiptAttachment = { file: null };
                $scope.objExpenseReportReceiptFile = $scope.model.data.ExpenseReportReceiptFile[0];
                console.log('ExpenseReceiptAttachmentController InitialValue.');
            };
            InitialValue();

            $scope.onAfterValidateFunction = function (event, fileList) {
                //console.log('file validated.', fileList);
                //base64:"VVNFIFtXb3JrZmxvd1JGU10N...EVSIEJZIFNPLk5hbWUNCg=="
                //filename:"stored check text in stored.txt"
                //filesize:811
                //filetype: "text/plain"
                //var alphaExp = new RegExp(/[!@#$%^&*<>\/:"|?]/);
                var newFile = angular.copy($scope.objExpenseReportReceiptFile);
                newFile.RequestNo = $scope.GE.receipt.RequestNo;
                newFile.ReceiptID = $scope.GE.receipt.ReceiptID;
                newFile.FileID = -1;
                newFile.FilePath = '';
                newFile.FileName = fileList[0].filename;
                newFile.FileType = fileList[0].filetype;
                newFile.FileContent = fileList[0].base64;
                newFile.FileSize = fileList[0].filesize;
                newFile.IsDelete = false;

                if ($scope.AllowUploadFile(newFile.FileName))
                {
                    $scope.p_message($scope.Text['SYSTEM']['VALIDATE_FILENAME'], 'w');
                }
                else if (!$scope.AllowUploadFileType(newFile.FileName))
                {
                    $scope.p_message($scope.Text['SYSTEM']['INVALIDFILETYPE'], 'w');
                }
                else if (!$scope.AllowFileNameLength(newFile.FileName.length))
                {
                    $scope.p_message($scope.Text['SYSTEM']['SAP_FILENAME_LENGTH_INVALID'], 'w');
                }
                else if(!$scope.AllowFileSize(newFile.FileSize))
                {
                    $scope.p_message($scope.Text['SYSTEM']['INVALIDFILESIZE'], 'w');
                }
                else if (!$scope.AllowFileZeroSize(newFile.FileSize)) {
                    $scope.p_message($scope.Text['SYSTEM']['INVALIDFILESIZE2'], 'w');
                }
                else
                {
                    if (angular.isUndefined($scope.GE.receipt.ExpenseReportReceiptFileList) || $scope.GE.receipt.ExpenseReportReceiptFileList == null) {
                        $scope.GE.receipt.ExpenseReportReceiptFileList = [];
                    }
                    $scope.GE.receipt.ExpenseReportReceiptFileList.push(newFile);
                }
                newFile = null;
                $scope.formReceiptAttachment.file = null;
                angular.element("#expense-receipt-file-input").val(null);
            };

            $scope.deleteFileAttach = function (rowIndex) {
                if (confirm($scope.Text['SYSTEM']['CONFIRM_DELETE'])) {
                    if ($scope.GE.receipt.ExpenseReportReceiptFileList[rowIndex].FileID != -1) {
                        // delete server attach file (set flag delete)
                        $scope.GE.receipt.ExpenseReportReceiptFileList[rowIndex].IsDelete = true;
                    } else {
                        // delete new attach file (remove from array)
                        $scope.GE.receipt.ExpenseReportReceiptFileList.splice(rowIndex, 1);
                    }

                    //if ($scope.GE.receipt.ExpenseReportReceiptFileList.length <= 0) {
                    //    // ignore flag isDelete in array
                    //    $scope.GE.receipt.HasFileAttached = false;
                    //}
                }
            };

            $scope.recoveryFileAttach = function (rowIndex) {
                $scope.GE.receipt.ExpenseReportReceiptFileList[rowIndex].IsDelete = false;
                //$scope.GE.receipt.HasFileAttached = true;
            };

            $scope.getFileAttach = function (attachment) {
                //var URL = CONFIG.SERVER + 'workflow/GetFile';

                /* via proxy page */
                //var oRequestParameter = { InputParameter: { "RequestNo": attachment.RequestNo, 'ReceiptID': attachment.ReceiptID, "FileID": attachment.FileID } };
                //var MoDule = 'workflow/';
                //var Functional = 'GetFile';
                //var URL = CONFIG.SERVER + MoDule + Functional;
                //// Success
                //$http({
                //    method: 'POST',
                //    url: URL,
                //    data: oRequestParameter
                //}).then(function successCallback(response) {
                //    console.log('ReceiptAttachment getFileAttach.', response.data);
                //    if (typeof cordova != 'undefined') {
                //        cordova.InAppBrowser.open("data:application/octet-stream, " + escape(response.data), '_system', 'location=no');
                //    } else {
                //        window.open("data:application/octet-stream, " + escape(response.data));
                //    }
                //}, function errorCallback(response) {
                //    // Error
                //    console.log('error ReceiptAttachment getFileAttach.', response);
                //});
                /* !via proxy page */

                /* direct */
                if (angular.isDefined(attachment)) {
                    if (typeof cordova != 'undefined') {
                        //cordova.InAppBrowser.open("data:application/octet-stream, " + escape(response.data), '_system', 'location=no');
                    } else {
                        //window.open("data:application/octet-stream, " + escape(response.data));
                        //var path = CONFIG.SERVER + 'Client/files/' + attachment.RequestNo + '/' + ("00000" + attachment.ReceiptID.toString()).substr(-5,5) + '/' + attachment.FileName;
                        var path = CONFIG.SERVER + 'Client/files/' + attachment.FilePath + '/' + attachment.FileName;
                        $window.open(path);
                        //$window.open('http://localhost:15124/Client/index.html#/frmViewRequest/0013160000069-GE000/0013/8150/False/False');
                    }
                }
                /* !direct */
            };

            $scope.AllowUploadFile = function (fileName) {
                return CONFIG.FILE_SETTING.ALLOW_FILE.test(fileName);
            }
            $scope.AllowUploadFileType = function (fileName) {
                return CONFIG.FILE_SETTING.ALLOW_FILETYPE.indexOf(fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase()) >= 0;
            }
            $scope.AllowFileNameLength = function (fileNameLength) {
                return CONFIG.FILE_SETTING.ALLOW_FILENAME_LENGTH >= fileNameLength;
            }
            $scope.AllowFileSize = function (fileSize) {
                return CONFIG.FILE_SETTING.ALLOW_FILESIZE >= fileSize;
            }
            $scope.AllowFileZeroSize = function (fileSize) {
                return  fileSize > 0;
            }

        }])



        
        // Receipt Item
        .controller('ReceiptItemGuestAttachmentController', ['$scope', '$http', '$routeParams', '$location', '$filter', '$window', 'CONFIG', '$mdDialog', function ($scope, $http, $routeParams, $location, $filter, $window, CONFIG, $mdDialog) {
            var employeeData = employeeData;
            $scope.p_message = function (message, type) {
                if (type == 'w') {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title('Warning')
                            .textContent(message)
                            .ariaLabel('Warning')
                            .ok('OK')
                    );
                } else {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title($scope.Text['SYSTEM']['INFORMATION'])
                            .textContent(message)
                            .ariaLabel($scope.Text['SYSTEM']['INFORMATION'])
                            .ok('OK')
                    );
                }
            }
            
            var InitialValue = function () {
                $scope.formReceiptAttachment = { file: null };
                $scope.objGuestFile = {
                    RequestNo: '',
                    ReceiptID: 0,
                    ItemID: 0,
                    FileID: 0,
                    FilePath: '',
                    FileName: '',
                    FileSize: 0
                };
            };
            InitialValue();

            $scope.onAfterValidateFunction = function (event, fileList) {
                //console.log('file validated.', fileList);
                //base64:"VVNFIFtXb3JrZmxvd1JGU10N...EVSIEJZIFNPLk5hbWUNCg=="
                //filename:"stored check text in stored.txt"
                //filesize:811
                //filetype: "text/plain"
                var newFile = angular.copy($scope.objGuestFile);
                newFile.RequestNo = $scope.GE.receipt.RequestNo;
                newFile.ReceiptID = $scope.GE.receipt.ReceiptID;
                newFile.ItemID = $scope.GE.receiptItem.ItemID;
                newFile.FileID = -1;
                newFile.FilePath = '';
                newFile.FileName = fileList[0].filename;
                newFile.FileType = fileList[0].filetype;
                newFile.FileContent = fileList[0].base64;
                newFile.FileSize = fileList[0].filesize;
                newFile.IsDelete = false;
                if ($scope.AllowUploadFile(newFile.FileName))
                {
                    $scope.p_message($scope.Text['SYSTEM']['VALIDATE_FILENAME'], 'w');
                }
                else if (!$scope.AllowUploadFileType(newFile.FileName))
                {
                    $scope.p_message($scope.Text['SYSTEM']['INVALIDFILETYPE'], 'w');
                }
                else if (!$scope.AllowFileNameLength(newFile.FileName.length))
                {
                    $scope.p_message($scope.Text['SYSTEM']['SAP_FILENAME_LENGTH_INVALID'], 'w');
                }
                else if (!$scope.AllowFileSize(newFile.FileSize))
                {
                    $scope.p_message($scope.Text['SYSTEM']['INVALIDFILESIZE'], 'w');
                }
                else if (!$scope.AllowFileZeroSize(newFile.FileSize)) {
                    $scope.p_message($scope.Text['SYSTEM']['INVALIDFILESIZE2'], 'w');
                }
                else
                {
                    //Validate Complete
                    if (angular.isUndefined($scope.GE.receiptItem.ExpenseReportReceiptItemGuestFileList) || $scope.GE.receiptItem.ExpenseReportReceiptItemGuestFileList == null) {
                        $scope.GE.receiptItem.ExpenseReportReceiptItemGuestFileList = [];
                    }
                    $scope.GE.receiptItem.ExpenseReportReceiptItemGuestFileList.push(newFile);
                }
                newFile = null;
                $scope.formReceiptAttachment.file = null;
                angular.element("#expense-receipt-guest-file-input").val(null);
            };

            $scope.deleteFileAttach = function (rowIndex) {
                if (confirm($scope.Text['SYSTEM']['CONFIRM_DELETE'])) {
                    if ($scope.GE.receiptItem.ExpenseReportReceiptItemGuestFileList[rowIndex].FileID != -1) {
                        // delete server attach file (set flag delete)
                        $scope.GE.receiptItem.ExpenseReportReceiptItemGuestFileList[rowIndex].IsDelete = true;
                    } else {
                        // delete new attach file (remove from array)
                        $scope.GE.receiptItem.ExpenseReportReceiptItemGuestFileList.splice(rowIndex, 1);
                    }

                    //if ($scope.GE.receiptItem.ExpenseReportReceiptItemGuestFileList.length <= 0) {
                    //    // ignore flag isDelete in array
                    //    $scope.GE.receipt.HasFileAttached = false;
                    //}
                }
            };

            $scope.recoveryFileAttach = function (rowIndex) {
                $scope.GE.receiptItem.ExpenseReportReceiptItemGuestFileList[rowIndex].IsDelete = false;
                //$scope.GE.receipt.HasFileAttached = true;
            };

            $scope.getFileAttach = function (attachment) {

                /* direct */
                if (angular.isDefined(attachment)) {
                    if (typeof cordova != 'undefined') {
                        //cordova.InAppBrowser.open("data:application/octet-stream, " + escape(response.data), '_system', 'location=no');
                    } else {
                        //window.open("data:application/octet-stream, " + escape(response.data));
                        //var path = CONFIG.SERVER + 'Client/files/' + attachment.RequestNo + '/' + ("00000" + attachment.ReceiptID.toString()).substr(-5,5) + '/' + attachment.FileName;
                        var path = CONFIG.SERVER + 'Client/files/' + attachment.FilePath + '/' + attachment.FileName;
                        $window.open(path);
                        //$window.open('http://localhost:15124/Client/index.html#/frmViewRequest/0013160000069-GE000/0013/8150/False/False');
                    }
                }
                /* !direct */
            };

            $scope.AllowUploadFile = function (fileName) {
                return CONFIG.FILE_SETTING.ALLOW_FILE.test(fileName);
            }
            $scope.AllowUploadFileType = function (fileName) {
                return CONFIG.FILE_SETTING.ALLOW_FILETYPE.indexOf(fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase()) >= 0;
            }
            $scope.AllowFileNameLength = function (fileNameLength) {
                return CONFIG.FILE_SETTING.ALLOW_FILENAME_LENGTH >= fileNameLength;
            }
            $scope.AllowFileSize = function (fileSize) {
                return CONFIG.FILE_SETTING.ALLOW_FILESIZE >= fileSize;
            }
            $scope.AllowFileZeroSize = function (fileSize) {
                return fileSize > 0;
            }

        }]);
})();
