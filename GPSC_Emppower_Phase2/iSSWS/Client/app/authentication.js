﻿var SERVER = "http://localhost:5555/";
//var SERVER = "http://localhost/iSSWS/";
var adminKey;
function checkAuthorize() {
    if (window.location.hash.indexOf('#/ByPassTicket/') >= 0) {
        return true;
    }
    console.log('checkAuthorize');
    var pageName = location.pathname;
    // if (pageName === '/start.html') {
    if (pageName === '/login.html') {
        setInterval(function () {
            authorize();
        }, 1000);
    } else {
        authorize();
    }

    function authorize() {
        if (checkToken()) {
            // if (pageName === '/start.html') {
            if (pageName === '/login.html') {
                toApplication();
            }
        } else {
            window.localStorage.removeItem('com.pttdigital.ess.authorizeUser');
            window.localStorage.removeItem('com.pttdigital.ess.announcement');
            window.localStorage.removeItem('com.pttdigital.ess.menu');
            toLogin();
        }

        function checkToken() {
            var result = false;
            if (hasToken()) {
                result = checkTokenWithWebService(getEmployeeDataToken());
            }
            return result;

            function checkTokenWithWebService(userData) {
                // check token with webservice
                //
                return true;
            }
        }
    }
}

function login() {
    // add code here test login

    var tryGetAuthen = localStorage.getItem('com.pttdigital.ess.authorizeUser');
    if (tryGetAuthen != null) {
        document.getElementById('ErrorMessage').style.visibility = "visible";
        document.getElementById('ErrorMessage').innerHTML = "Another user is logged in, please check. Otherwise, please contact system administrator.";
        return;
    }


    setLoader(true);
    var inputUsername = document.getElementById('inputAccount').value;
    var inputPassword = document.getElementById('inputPassword').value;
    var x = document.querySelector("select");
    var inputCompanyCode = x.options[x.selectedIndex].value;
    var inputCompanyName = x.options[x.selectedIndex].label;
    // console.log(inputCompanyCode);

    document.getElementById('ErrorMessage').style.visibility = "hidden";
    if (inputUsername != null && inputPassword != null && inputUsername.length > 0 && inputPassword.length > 0
         && inputCompanyCode != null && inputCompanyCode.length > 0) {
        document.getElementById('ErrorMessage').style.visibility = "hidden";
        var currentLocation = window.location;
        var key = gup('key', currentLocation);
        //if (key == adminKey && key != '') {
        //    authenWebByPassAdminService(inputUsername, inputPassword, inputCompanyCode, inputCompanyName);
        //} else {
        //    authenWebService(inputUsername, inputPassword, inputCompanyCode, inputCompanyName);
        //}

        getUserAuthen(inputCompanyCode);

    } else {
        document.getElementById('ErrorMessage').style.visibility = "visible";
        document.getElementById('ErrorMessage').innerHTML = "Please input Username/Password/Company";
        setLoader(false);
    }

    function authenWebService(inputUsername, inputPassword, inputCompanyCode, inputCompanyName) {
        //// check username, password with webservice
        var URL = SERVER + 'workflow/authenticate';
        var oUserLogin = { Username: inputUsername, Password: inputPassword, CompanyCode: inputCompanyCode, CompanyName: inputCompanyName };
        $.ajax({
            type: 'POST',
            data: oUserLogin,
            url: URL,
            //dataType: 'json',
            success:
                function (data, textStatus, XMLHttpRequest) {
                    if (data.ErrorMessage == "") {
                        //window.localStorage.setItem('com.pttdigital.ess.authorizeUser', JSON.stringify(data.EmployeeData));
                        //window.localStorage.setItem('com.pttdigital.ess.announcement', JSON.stringify(data.Announcement));
                        //window.localStorage.setItem('com.pttdigital.ess.menu', JSON.stringify(data.Menu));
                        //toApplication();
                        validateEmployeeData(data);
                    }
                    else {
                        document.getElementById('ErrorMessage').style.visibility = "visible";
                        document.getElementById('ErrorMessage').innerHTML = data.ErrorMessage;
                        setLoader(false);
                    }
                },
            error:
                function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log('error anthenWebService.', XMLHttpRequest, textStatus, errorThrown);
                    document.getElementById('ErrorMessage').style.visibility = "visible";
                    document.getElementById('ErrorMessage').innerHTML = 'Please check connection.';
                    setLoader(false);
                }
        });

        //$.post(URL, { '': JSON.stringify(x) })
        //.done(function (data) {
        //    alert(data);
        //});

    }

    function getUserAuthen(companycode) {
        var URL = SERVER + 'Security/GetUserAuthen';
        var oUserLogin = { Username: '', Password: '', CompanyCode: companycode, CompanyName: '' };
        $.ajax({
            type: 'POST',
            data: oUserLogin,
            url: URL,
            //dataType: 'json',
            success:
                function (data, textStatus, XMLHttpRequest) {
                    if (data != "") {
                        if (key == adminKey && key != '') {
                            authenWebByPassAdminService(inputUsername, inputPassword, inputCompanyCode, inputCompanyName);
                        } else {
                            authenWebService(inputUsername, inputPassword, inputCompanyCode, inputCompanyName);
                        }
                        authenAPIService(data.username, data.password, data.companycode);
                    }
                    else {
                        document.getElementById('ErrorMessage').style.visibility = "visible";
                        document.getElementById('ErrorMessage').innerHTML = data.ErrorMessage;
                        setLoader(false);
                    }
                },
            error:
                function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log('error getUserAuthen.', XMLHttpRequest, textStatus, errorThrown);
                    document.getElementById('ErrorMessage').style.visibility = "visible";
                    document.getElementById('ErrorMessage').innerHTML = 'Please check connection.';
                    setLoader(false);
                }
        });
    }
    function authenAPIService(username, password, companycode) {
        var URL = SERVER + 'Security/AuthenticationUser';
        var oUserLogin = { Username: username, Password: password, CompanyCode: companycode, CompanyName: '' };
        $.ajax({
            type: 'POST',
            data: oUserLogin,
            url: URL,
            //dataType: 'json',
            success:
                function (data, textStatus, XMLHttpRequest) {
                    if (data != "") {
                        window.localStorage.setItem('com.pttdigital.ess.token', data.token);
                    }
                    else {
                        document.getElementById('ErrorMessage').style.visibility = "visible";
                        document.getElementById('ErrorMessage').innerHTML = data.ErrorMessage;
                        setLoader(false);
                    }
                },
            error:
                function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log('error anthenWebService.', XMLHttpRequest, textStatus, errorThrown);
                    document.getElementById('ErrorMessage').style.visibility = "visible";
                    document.getElementById('ErrorMessage').innerHTML = 'Please check connection.';
                    setLoader(false);
                }
        });
    }

    function authenWebByPassAdminService(inputUsername, inputPassword, inputCompanyCode, inputCompanyName) {
        var URL = SERVER + 'workflow/AuthenticateWithOutPassword';
        var oUserLogin = { Username: inputUsername, Password: inputPassword, CompanyCode: inputCompanyCode, CompanyName: inputCompanyName };
        $.ajax({
            type: 'POST',
            data: oUserLogin,
            url: URL,
            success:
                function (data, textStatus, XMLHttpRequest) {
                    if (data.ErrorMessage == "") {
                        validateEmployeeData(data);
                    }
                    else {
                        document.getElementById('ErrorMessage').style.visibility = "visible";
                        document.getElementById('ErrorMessage').innerHTML = data.ErrorMessage;
                        setLoader(false);
                    }
                },
            error:
                function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log('error anthenWebService.', XMLHttpRequest, textStatus, errorThrown);
                    document.getElementById('ErrorMessage').style.visibility = "visible";
                    document.getElementById('ErrorMessage').innerHTML = 'Please check connection.';
                    setLoader(false);
                }
        });
    }


}

function validateEmployeeData(oData) {


    var URL = SERVER + 'Workflow/ValidateData';

    var oRequestParameter = { InputParameter: {}, RequestDocument: {}, CurrentEmployee: {}, Creator: {}, Requestor: oData.EmployeeData };

    $.ajax({
        type: 'POST',
        data: oRequestParameter,
        url: URL,
        success:
            function (data, textStatus, XMLHttpRequest) {
                if (data != null && data.length > 0) {
                    // error config
                    var errorMsg = '';
                    for (var i = 0; i < data.length; i++) {
                        errorMsg += '<b>' + data[i].TableName + '</b> : ' + data[i].ErrorMessage + '<br>';
                    }
                    document.getElementById('ErrorMessage').style.visibility = "visible";
                    document.getElementById('ErrorMessage').innerHTML = errorMsg;
                    setLoader(false);
                }
                else {
                    // config complete

                    window.localStorage.setItem('com.pttdigital.ess.authorizeUser', JSON.stringify(oData.EmployeeData));
                    window.localStorage.setItem('com.pttdigital.ess.announcement', JSON.stringify(oData.Announcement));
                    window.localStorage.setItem('com.pttdigital.ess.menu', JSON.stringify(oData.Menu));
                    toApplication();

                }
            },
        error:
            function (XMLHttpRequest, textStatus, errorThrown) {
                document.getElementById('ErrorMessage').style.visibility = "visible";
                document.getElementById('ErrorMessage').innerHTML = errorThrown;
                setLoader(false);
            }
    });

}

function logout() {
    console.log('logout');
    window.localStorage.removeItem("com.pttdigital.ess.authorizeUser");
    window.localStorage.removeItem("com.pttdigital.ess.announcement");
    window.localStorage.removeItem('com.pttdigital.ess.menu');
    window.localStorage.removeItem('com.pttdigital.ess.token');
    window.location = 'login.html';//'start.html';
    localStorage.clear();
}

function toApplication() {
    window.location.replace("index.html");//'index.html';
}

function toLogin() {
    window.location = 'login.html';
}

function hasToken() {
    var jsonIsValid;
    try {
        var jsonResult = JSON.parse(window.localStorage.getItem('com.pttdigital.ess.authorizeUser'));
        jsonIsValid = true;
    } catch (e) {
        console.log('token error');
        window.localStorage.removeItem("com.pttdigital.ess.authorizeUser");
        jsonIsValid = false;
    };
    return window.localStorage.getItem('com.pttdigital.ess.authorizeUser') != null && jsonIsValid;
}

function getEmployeeDataToken() {
    var objEmployeeData = null;
    try {
        objEmployeeData = JSON.parse(window.localStorage.getItem('com.pttdigital.ess.authorizeUser'));
    } catch (e) {
        console.log('token error');
        window.localStorage.removeItem("com.pttdigital.ess.authorizeUser");
    };
    return objEmployeeData;
}

function getToken(KEY) {
    var obj = null;
    try {
        obj = JSON.parse(window.localStorage.getItem(KEY));
    } catch (e) {
        console.log('token error');
        window.localStorage.removeItem(KEY);
    }
    return obj;
}

function setLoader(enable) {
    if (enable) {
        $('#inputAccount').prop('disabled', true);
        $('#inputPassword').prop('disabled', true);
        $('#login_button').prop('disabled', true);
        $('#inputCompany').prop('disabled', true);
        $('#login_button_icon').hide();
        $('#login_loader').show();
    } else {
        $('#inputAccount').prop('disabled', false);
        $('#inputPassword').prop('disabled', false);
        $('#login_button').prop('disabled', false);
        $('#inputCompany').prop('disabled', false);
        $('#login_loader').hide();
        $('#login_button_icon').show();
    }
}

function gup(name, url) {
    if (!url) url = location.href;
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(url);
    return results == null ? null : results[1];
}


var app = angular.module('LoginApp', []);
app.controller('CompanyController', function ($http, $scope) {

    $scope.event = {
        isEvent: false,
        hiddenVisit: true
    };

    // single signon
    function AuthenticatewithWindowsAuthen() {
        var currentLocation = window.location;
        var KEY_COMPANY_CODE = gup('cc', currentLocation);
        var isFoundCompany = false;
        for (var i = 0; i < $scope.CompanyList.length; i++) {
            if (KEY_COMPANY_CODE == $scope.CompanyList[i].CompanyCode) {
                isFoundCompany = true;
                break;
            }
        }
        if (!isFoundCompany) return;


        console.log("AuthenticateWindows");
        var MoDule = 'workflow/';
        var Functional = 'AuthenticateWindows';
        var URL = SERVER + MoDule + Functional + "?CompanyCode=" + KEY_COMPANY_CODE;
        //var oParams ={
        //    CompanyCode: KEY_COMPANY_CODE
        //};
        console.log(URL);
        $http({
            method: 'GET',
            url: URL,
            //data: oParams
        }).then(function successCallback(response) {
            var data = response.data;
            console.log('success -- authen');
            if (data.ErrorMessage == "") {
                validateEmployeeData(data);
            }
            else {
                document.getElementById('ErrorMessage').style.visibility = "visible";
                document.getElementById('ErrorMessage').innerHTML = data.ErrorMessage + ' AD1 : ' + data.UserAuthenAD1 + ' AD2 : ' + data.UserAuthenAD2;
                setLoader(false);
            }
            //console.log(response.data);
            //$scope.CompanyList = response.data;

        }, function errorCallback(response) {
            // Error
            console.log('error AuthenticatewithWindowsAuthen.', response);
        });
    };


    // external sign on
    function AuthenticatewithExternalUser() {
        var currentLocation = window.location;
        var KEY_TOKEN = gup('ex', currentLocation);
        var KEY_COMPANY = gup('ex_cc', currentLocation);
        if (!KEY_TOKEN) return;
        if (!KEY_COMPANY) return;
        var MoDule = 'workflow/';
        var Functional = 'AuthenticateForExternalUser';
        var URL = SERVER + MoDule + Functional + "?EncryptKey=" + KEY_TOKEN + "|" + KEY_COMPANY;
        console.log(URL);
        $http({
            method: 'GET',
            url: URL,
            //data: oParams
        }).then(function successCallback(response) {
            var data = response.data;
            console.debug(data);
            console.log('success -- AuthenticateForExternalUser');
            if (data.ErrorMessage == "") {
                window.localStorage.setItem('com.pttdigital.ess.authorizeUser', JSON.stringify(data.EmployeeData));
                window.localStorage.setItem('com.pttdigital.ess.announcement', JSON.stringify(data.Announcement));
                window.localStorage.setItem('com.pttdigital.ess.menu', JSON.stringify(data.Menu));
                toApplication();
            }
            else {
                setLoader(false);
            }

        }, function errorCallback(response) {
            // Error
            console.log('error AuthenticateForExternalUser.', response);
        });
    }

    $scope.GetCompanyList = function () {
        console.log("GetCompanyList");
        var MoDule = 'Share/';
        var Functional = 'GetCompanyList';
        var URL = SERVER + MoDule + Functional;
        console.log(URL);
        $http({
            method: 'POST',
            url: URL
            //data: oTravelReportTransfer
        }).then(function successCallback(response) {

            console.log('suAuthenticatewithWindowsAuthenccess');
            //console.log(response.data);
            $scope.CompanyList = response.data;
            AuthenticatewithWindowsAuthen();
            AuthenticatewithExternalUser();

        }, function errorCallback(response) {
            // Error
            console.log('error CompanyController.', response);
        });
    };

    $scope.GetCompanyList();

    $scope.GetEnvironment = function () {
        console.log("GenEnvironment");
        var MoDule = 'Share/';
        var Functional = 'GetEnvironment';
        var URL = SERVER + MoDule + Functional;
        console.log(URL);
        $http({
            method: 'POST',
            url: URL
            //data: oTravelReportTransfer
        }).then(function successCallback(response) {

            console.log('GetEnvironment Success');
            console.log(response.data['DataBaseLocation']);
            $scope.Environment = response.data['DataBaseLocation'];
        }, function errorCallback(response) {
            // Error
            console.log('error GetEnvironment.', response);
        });
    };
    $scope.GetEnvironment();

    function getLockInBlockSetting() {
        var MoDule = 'workflow/';
        var Functional = 'GetLoginBlockSetting';
        var URL = SERVER + MoDule + Functional;
        console.log(URL);
        $http({
            method: 'POST',
            url: URL
        }).then(function successCallback(response) {
            $scope.event.isEvent = (response.data.IsEvent.toLowerCase() != 'false');
            adminKey = response.data.AdminKey;
            if ($scope.event.isEvent) {
                var currentLocation = window.location;
                var key = gup('key', currentLocation);
                if (key == adminKey) {
                    $scope.event.isEvent = false;
                }

            }
        }, function errorCallback(response) {
            // Error
            console.log('error blockseting.', response);
        });
    }
    getLockInBlockSetting();



});