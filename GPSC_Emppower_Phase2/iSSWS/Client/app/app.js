﻿(function () {
    angular.module('ESSMobile'
        , ['ngRoute', 'ngSanitize', '720kb.datepicker', '720kb.datepicker.ayodia', 'naif.base64', 'datatables', 'angular-tour'
            , 'ui.select', 'ngMaterial', 'selectize', 'ngMessages', 'daterangepicker', 'chart.js', 'ckeditor', 'ui.bootstrap','ngFileUpload'])
        .constant('CONFIG', {
            SERVER: 'http://localhost:5555/',
            //SERVER: 'http://localhost/iSSWS/',
            USER: 'com.pttdigital.ess.authorizeUser',
            ANNOUNCEMENT: 'com.pttdigital.ess.announcement',
            MENU: 'com.pttdigital.ess.menu',
            MODULE_SETTING: {
                PROJECTCODEMODE_NONE: 'NONE',
                PROJECTCODEMODE_AVERAGE: 'AVERAGE',
                PROJECTCODEMODE_EXACTLY: 'EXACTLY'
            },
            PAGE_SETTING: {
                TRAVEL_REPORT: 'TRAVEL_REPORT'
            },
            FILE_SETTING: {
                //ALLOW_FILETYPE: new RegExp(/^([a-zA-Z0-9._+-])+.(([gif png jpg jpeg bmp png pdf doc xls ppt zip rar docx xlsx txt pptx]{3}))/i),
                ALLOW_FILETYPE: 'gif png jpg jpeg bmp png pdf doc xls ppt docx xlsx txt pptx',
                ALLOW_FILE: new RegExp(/[!@#$%^&*<>\/:"|?+]/),
                ALLOW_FILENAME_LENGTH: 50,
                ALLOW_FILESIZE: 10485760,
                DIVIDE_FILESIZE: 1048576
                // 1GB = 1073741824
            }
            //PROFILE_SETTING:{
            //    POSITION: '',
            //}
        }).filter("nl2br", function ($filter) {
            return function (data) {
                if (!data) return data;
                return data.replace(/\n\r?/g, '<br />');
            };
        })
        .run(function ($http) {
            //window.localStorage.getItem('com.pttdigital.ess.authorizeUser')
            $http.defaults.headers.common.Authorization = 'Bearer ' + window.localStorage.getItem('com.pttdigital.ess.token');
        })
        /*.directive('fallbackSrc', function () {
           var fallbackSrc = {
               link: function postLink(scope, iElement, iAttrs) {
                   iElement.bind('error', function() {
                       angular.element(this).attr("src", iAttrs.fallbackSrc);
                   });
               }
           }
           return fallbackSrc;
       })*/
        .config(function ($mdThemingProvider) {
            $mdThemingProvider.theme('default').primaryPalette('blue', {
                'default': '400',
                'hue-2': '500'
            });
        })
        .filter('nl2textp', function () {
            return function (text) {
                text = String(text).trim();
                return (text.length > 0 ? '<p>' + text.split("null").join("").replace(/[,]+/g, '</p><p>') + '</p>' : null);
            }
        }).filter('sumByKey', function () {
            return function (data, key) {
                if (typeof (data) === 'undefined' || typeof (key) === 'undefined') {
                    return 0;
                }

                var sum = 0;
                for (var i = data.length - 1; i >= 0; i--) {
                    sum += parseFloat(data[i][key]);
                }

                return sum;
            };
            //});
        }).directive('autoFocus', function ($timeout) {
            return {
                restrict: 'AC',
                link: function (_scope, _element) {
                    $timeout(function () {
                        _element[0].focus();
                    }, 0);
                }
            };
        }).directive('dlEnterKey', function () {
            return function (scope, element, attrs) {
                element.bind("keydown keypress", function (event) {
                    var keyCode = event.which || event.keyCode;

                    // If enter key is pressed 
                    if (keyCode === 13) {
                        scope.$apply(function () {
                            // Evaluate the expression 
                            scope.$eval(attrs.dlEnterKey);
                        });
                        event.preventDefault();
                    }
                });
            };
        });;

})();





var rx = /INPUT|SELECT|TEXTAREA/i;
$(document).bind("keydown keypress", function (e) {
    if (e.which == 8) { // 8 == backspace
        if (!rx.test(e.target.tagName) || e.target.disabled || e.target.readOnly) {
            e.preventDefault();
        }
    }
});

String.format = function () {
    // The string containing the format items (e.g. "{0}")
    // will and always has to be the first argument.
    var theString = arguments[0];

    // start with the second argument (i = 1)
    for (var i = 1; i < arguments.length; i++) {
        // "gm" = RegEx options for Global search (more than one instance)
        // and for Multiline search
        var regEx = new RegExp("\\{" + (i - 1) + "\\}", "gm");
        theString = theString.replace(regEx, arguments[i]);
    }

    return theString;
}
