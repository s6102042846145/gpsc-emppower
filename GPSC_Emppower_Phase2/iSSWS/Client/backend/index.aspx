﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="iSSWS.Client.backend.index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ระบบหลังบ้าน</title>
     <link rel="stylesheet" href="../backend.css" />
    <script src="../backend.js"></script>
</head>
<body>
    <form id="form1" runat="server">
          <div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a href="SearchClaimHistory.aspx">ตรวจสอบค่าเครื่องแต่งกายที่เคยเบิกมาในระบบ</a>
  <a href="ViewAuthorizeModel.aspx">อำนาจอนุมัติสำหรับแต่ละบริษัท</a>
  <a href="CheckAuthorizeRequestForAdmin.aspx">ตรวจสอบอำนาจอนุมัติในเอกสาร</a>
  <a href="../login.html">กลับไปหน้าจอการเข้าระบบ TE</a>
</div>

<!-- Use any element to open the sidenav -->
<span onclick="openNav()">เปิดเมนู</span>
    </form>
</body>
</html>
