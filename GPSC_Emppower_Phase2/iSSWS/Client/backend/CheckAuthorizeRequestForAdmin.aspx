﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CheckAuthorizeRequestForAdmin.aspx.cs" Inherits="iSSWS.Client.backend.CheckAuthorizeRequestForAdmin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ระบบหลังบ้าน</title>
    <link rel="stylesheet" href="../backend.css" />
    <script src="../backend.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="mySidenav" class="sidenav">
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
            <a href="SearchClaimHistory.aspx">ตรวจสอบค่าเครื่องแต่งกายที่เคยเบิกมาในระบบ</a>
            <a href="ViewAuthorizeModel.aspx">อำนาจอนุมัติสำหรับแต่ละบริษัท</a>
            <a href="CheckAuthorizeRequestForAdmin.aspx">ตรวจสอบอำนาจอนุมัติในเอกสาร</a>
            <a href="../login.html">กลับไปหน้าจอการเข้าระบบ TE</a>
        </div>

        <!-- Use any element to open the sidenav -->
        <span onclick="openNav()">เปิดเมนู</span>

        <!-- Add all page content inside this div if you want the side nav to push page content to the right (not used if you only want the sidenav to sit on top of the page -->
        <div id="main">
            <div class="md-12">
                <table border="0" width="50%">
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="Label1" runat="server" Text="ตรวจสอบสายการอนุมัติของเอกสารนั้นๆ" Font-Bold="true" ForeColor="Blue"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td width="25%">บริษัท</td>
                        <td>
                            <asp:DropDownList ID="ddlCompany" runat="server"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td width="25%">Request No.</td>
                        <td>
                            <asp:TextBox ID="txtRequestNo" runat="server" Width="200"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>Emp Subgroup</td>
                        <td>
                            <asp:TextBox ID="txtEmpSubGroup" runat="server" Width="200"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />&nbsp;&nbsp;
                    <asp:Button ID="btnClear" runat="server" Text="Clear" OnClick="btnClear_Click" />
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <br />
            <div>
                <asp:Label ID="lblError" runat="server" ForeColor="Red" Text="ไม่พบข้อมูล" Visible="false"></asp:Label>
            </div>
            <table>
                <tr>
                    <td style="vertical-align: top">
                        <asp:GridView ID="gvShow" runat="server" Font-Size="14px" AutoGenerateColumns="False" CellPadding="3" GridLines="None" BackColor="White" BorderColor="White" BorderStyle="None" BorderWidth="2px" CellSpacing="1">
                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
                            <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                            <RowStyle BackColor="#DEDFDE" ForeColor="Black" />
                            <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#594B9C" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#33276A" />
                            <Columns>
                                <asp:TemplateField HeaderText="Key">
                                    <ItemTemplate>
                                        <asp:Label ID="lblKey" runat="server" Text='<%# Eval("Key") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Value">
                                    <ItemTemplate>
                                        <asp:Label ID="lblValue" runat="server" Text='<%# Eval("Value") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                    <td style="vertical-align: top">
                        <asp:GridView ID="gvShow1" runat="server" AutoGenerateColumns="False" Font-Size="14px" CellPadding="3" GridLines="None" BackColor="White" BorderColor="White" BorderStyle="None" BorderWidth="2px" CellSpacing="1">
                            <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                            <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
                            <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                            <RowStyle BackColor="#DEDFDE" ForeColor="Black" />
                            <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#594B9C" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#33276A" />
                            <Columns>
                                <asp:TemplateField HeaderText="Model ID">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAuthModelID" runat="server" Text='<%# Eval("AuthModelID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Set ID">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAuthModelSetID" runat="server" Text='<%# Eval("AuthModelSetID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Key">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAuthModelKey" runat="server" Text='<%# Eval("AuthModelKey") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Operator">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAuthModelOperator" runat="server" Text='<%# Eval("AuthModelOperator") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ValueInDocument">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAuthModelValueInDocument" runat="server" Text='<%# Eval("AuthModelValueInDocument") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ValueForCheck1">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAuthModelValueForCheck1" runat="server" Text='<%# Eval("AuthModelValueForCheck1") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ValueForCheck2">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAuthModelValueForCheck2" runat="server" Text='<%# Eval("AuthModelValueForCheck2") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="IsPass">
                                    <ItemTemplate>
                                        <asp:Label ID="lblIsPass" runat="server" Text='<%# Eval("IsPass") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>

            <br />
            <div class="row">
            </div>
            <div class="row">
                <div class="md-12">
                    <asp:GridView ID="gvShow3" runat="server" AutoGenerateColumns="False" Font-Size="14px" CellPadding="3" GridLines="None" BackColor="White" BorderColor="White" BorderStyle="None" BorderWidth="2px" CellSpacing="1">
                        <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
                        <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                        <RowStyle BackColor="#DEDFDE" ForeColor="Black" />
                        <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#594B9C" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#33276A" />
                        <Columns>
                            <asp:TemplateField HeaderText="Employee ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmpID" runat="server" Text='<%# Eval("Employee_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Employee Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmpName" runat="server" Text='<%# Eval("Employee_Name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Position">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmpPositionID" runat="server" Text='<%# Eval("Employee_PositionID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Postion Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmpPostionName" runat="server" Text='<%# Eval("Employee_PositionName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Employee EmpGroup">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmpEmpGroup" runat="server" Text='<%# Eval("Employee_EmpGroup") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Manager No">
                                <ItemTemplate>
                                    <asp:Label ID="lblMgrNo" runat="server" Text='<%# Eval("Manager_No") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Manager Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblMgrName" runat="server" Text='<%# Eval("Manager_Name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Position ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblMgrPositionID" runat="server" Text='<%# Eval("Manager_PositionID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Position Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblMgrPositionName" runat="server" Text='<%# Eval("Manager_PositionName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Manager EmpSubGroup">
                                <ItemTemplate>
                                    <asp:Label ID="lblMgrEmpSubGroup" runat="server" Text='<%# Eval("Manager_EmpSubgroup") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Remark">
                                <ItemTemplate>
                                    <asp:Label ID="lblRemark" runat="server" Text='<%# Eval("Remark") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="IsMaximum">
                                <ItemTemplate>
                                    <asp:Label ID="lblIsMaximum" runat="server" Text='<%# Eval("IsMaximum") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>

    </form>
</body>
</html>
