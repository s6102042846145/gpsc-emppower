﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchClaimHistory.aspx.cs" Inherits="iSSWS.Client.backend.SearchClaimHistory" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ระบบหลังบ้าน</title>
     <link rel="stylesheet" href="../backend.css" />
    <script src="../backend.js"></script>
</head>
<body>
    <form id="form1" runat="server">
          <div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a href="SearchClaimHistory.aspx">ตรวจสอบค่าเครื่องแต่งกายที่เคยเบิกมาในระบบ</a>
  <a href="ViewAuthorizeModel.aspx">อำนาจอนุมัติสำหรับแต่ละบริษัท</a>
  <a href="CheckAuthorizeRequestForAdmin.aspx">ตรวจสอบอำนาจอนุมัติในเอกสาร</a>
              <a href="../login.html">กลับไปหน้าจอการเข้าระบบ TE</a>
</div>

<!-- Use any element to open the sidenav -->
<span onclick="openNav()">เปิดเมนู</span>

<!-- Add all page content inside this div if you want the side nav to push page content to the right (not used if you only want the sidenav to sit on top of the page -->
<div id="main">
       <div class="md-12">
        <table border="0" width="50%">
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label1" runat="server" Text="ค้นหา ค่าเครื่องแต่งกาย ที่มีในระบบ" Font-Bold="true" ForeColor="Blue"></asp:Label>
                </td>
            </tr>
            <tr>
                <td width="25%">บริษัท</td>
                <td><asp:DropDownList ID="ddlCompany" runat="server"></asp:DropDownList></td>
            </tr>
            <tr>
                <td width="25%">Employee ID</td>
                <td><asp:TextBox ID="txtEmpID" runat="server" width="200"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Employee Name</td>
                <td><asp:TextBox ID="txtEmpName" runat="server" width="200"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />&nbsp;&nbsp;
                    <asp:Button ID="btnClear" runat="server" Text="Clear" OnClick="btnClear_Click" />
                </td>
            </tr>
        </table>
    </div>
        <br /><br />
    <div>
        <asp:Label ID="lblError" runat="server" ForeColor="Red" Text="ไม่พบการเบิกเครื่องแต่งกาย ใน ClaimHistory" Visible="false"></asp:Label>
    </div>
    <div>
        <asp:GridView ID="gvShow" runat="server" AutoGenerateColumns="False" CellPadding="3" Font-Size="14px" GridLines="None" BackColor="White" BorderColor="White" BorderStyle="None" BorderWidth="2px" CellSpacing="1">
            <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
            <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
            <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
            <RowStyle BackColor="#DEDFDE" ForeColor="Black" />
            <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#594B9C" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#33276A" />
            <Columns>
                <asp:TemplateField HeaderText="รหัสพนักงาน">
                    <ItemTemplate>
                        <asp:Label ID="lblEmpID" runat="server" Text=' <%# ("EmployeeID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ชื่อพนักงาน">
                    <ItemTemplate>
                        <asp:Label ID="lblEmpName" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="วันที่">
                    <ItemTemplate>
                        <asp:Label ID="lblClaimDate" runat="server" Text='<%# Eval("ClaimDate") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="จำนวนเงิน">
                    <ItemTemplate>
                        <asp:Label ID="lblClaimDate" runat="server" Text='<%# Eval("ClaimAmount") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                </asp:TemplateField>
            </Columns>
        </asp:GridView>

    </div>
</div>
 
    </form>
</body>
</html>
