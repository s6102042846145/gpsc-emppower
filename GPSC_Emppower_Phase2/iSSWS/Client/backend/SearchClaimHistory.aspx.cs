﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ESS.HR.TR.ABSTRACT;
using ESS.HR.TR.DATACLASS;
using ESS.SHAREDATASERVICE;
using ESS.UTILITY.EXTENSION;
using System.ComponentModel;
using ESS.WORKFLOW;
using System.IO;
using System.Reflection;
using System.Text;
using ESS.HR.TR.CONFIG;
using ESS.UTILITY.CONVERT;
using ESS.HR.TR;
using System.Data;

namespace iSSWS.Client.backend
{
    public partial class SearchClaimHistory : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCompany();
            }
        }
        void BindCompany()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ID",typeof(String));
            dt.Columns.Add("Name", typeof(String));
            
            dt.Rows.Add("0010","PTT");
            dt.Rows.Add("0013","PTTLNG");
            dt.Rows.Add("0019","PTTTANK");
            dt.Rows.Add("0030","BSA");
            dt.Rows.Add("0031","ENCO");
            dt.Rows.Add("0176","PTTES");

            ddlCompany.DataSource = dt;
            ddlCompany.DataValueField = "ID";
            ddlCompany.DataTextField = "Name";
            ddlCompany.DataBind();
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = HRTRManagement.CreateInstance(ddlCompany.SelectedValue.ToString()).GetClaimHistory(txtEmpID.Text, txtEmpName.Text);

                if (dt.Rows.Count > 0)
                {
                    lblError.Visible = false;
                    gvShow.DataSource = dt;
                    gvShow.DataBind();
                }
                else
                {
                    lblError.Visible = true;
                    gvShow.DataSource = null;
                    gvShow.DataBind();
                }
            }
            catch(Exception ex)
            {

            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            lblError.Visible = false;
            ddlCompany.SelectedIndex = 0;
            txtEmpID.Text = "";
            txtEmpName.Text = "";
            gvShow.DataSource = null;
            gvShow.DataBind();
        }
    }
}