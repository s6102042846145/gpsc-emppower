﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ESS.HR.TR.ABSTRACT;
using ESS.HR.TR.DATACLASS;
using ESS.SHAREDATASERVICE;
using ESS.UTILITY.EXTENSION;
using System.ComponentModel;
using ESS.WORKFLOW;
using System.IO;
using System.Reflection;
using System.Text;
using ESS.HR.TR.CONFIG;
using ESS.UTILITY.CONVERT;
using ESS.HR.TR;
using System.Data;

namespace iSSWS.Client.backend
{
    public partial class CheckAuthorizeRequestForAdmin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCompany();
            }
        }
        void BindCompany()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ID", typeof(String));
            dt.Columns.Add("Name", typeof(String));

            dt.Rows.Add("0010", "PTT");
            dt.Rows.Add("0013", "PTTLNG");
            dt.Rows.Add("0019", "PTTTANK");
            dt.Rows.Add("0030", "BSA");
            dt.Rows.Add("0031", "ENCO");
            dt.Rows.Add("0176", "PTTES");
            dt.Rows.Add("0087", "OR");

            ddlCompany.DataSource = dt;
            ddlCompany.DataValueField = "ID";
            ddlCompany.DataTextField = "Name";
            ddlCompany.DataBind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                //int EmpSubGroup = Convert.ToInt32(txtEmpSubGroup.Text);
                if (txtRequestNo.Text == "")
                    return;
                if (txtEmpSubGroup.Text == "")
                    return;
                
                DataSet ds = HRTRManagement.CreateInstance(ddlCompany.SelectedValue.ToString()).GetCheckAuthorizeRequestForAdmin(txtRequestNo.Text, txtEmpSubGroup.Text);
                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    DataTable dt1 = ds.Tables[1];
                    DataTable dt2 = ds.Tables[2];

                    if (dt.Rows.Count > 0)
                    {
                        gvShow.DataSource = dt;
                        gvShow.DataBind();
                    }
                    else
                    {
                        gvShow.DataSource = null;
                        gvShow.DataBind();
                    }

                    if (dt1.Rows.Count > 0)
                    {
                        gvShow1.DataSource = dt1;
                        gvShow1.DataBind();
                    }
                    else
                    {
                        gvShow1.DataSource = null;
                        gvShow1.DataBind();
                    }

                    if(dt2.Rows.Count > 0)
                    {
                        gvShow3.DataSource = dt2;
                        gvShow3.DataBind();
                    }
                    else
                    {
                        gvShow3.DataSource = null;
                        gvShow3.DataBind();
                    }
                }

                if(ds.Tables.Count == 0)
                {
                    lblError.Text = "ไม่พบข้อมูล";
                }
            }
            catch(Exception ex)
            {
                lblError.Text = ex.Message;
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtRequestNo.Text = "";
            txtEmpSubGroup.Text = "";
            gvShow.DataSource = null;
            gvShow.DataBind();
            gvShow1.DataSource = null;
            gvShow1.DataBind();
        }
    }
}