﻿using ESS.JOB;
using ESS.JOB.DATACLASS;
using ESS.JOB.INTERFACE;
using ESS.WORKFLOW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iSSWS.Client
{
    public partial class runjobs : System.Web.UI.Page
    {
        private static IJobWorker CreateWorker(string CompanyCode, JobDetail oJobDetail)
        {
            IJobWorker worker = null;
            Type oReturn = null;
            Assembly oAssembly;
            oAssembly = Assembly.Load(oJobDetail.JobType.AssemblyName);
            if (oAssembly != null)
            {
                oReturn = oAssembly.GetType(oJobDetail.JobType.ClassName);
                if (oReturn != null)
                {
                    worker = (IJobWorker)Activator.CreateInstance(oReturn);
                }
            }

            worker.Params = JobManagement.CreateInstance(CompanyCode).GetJobParam(oJobDetail.JobID);
            worker.CompanyCode = CompanyCode;

            return worker;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string id = (string)this.Request["id"];
            string CompanyCode = (string)this.Request["cc"];

            JobDetail oJobDetail = JobManagement.CreateInstance(CompanyCode).GetJob(int.Parse(id));
            IJobWorker worker = CreateWorker(CompanyCode, oJobDetail);
            System.IO.TextWriter save = Console.Out;
            Console.SetOut(Response.Output);

            try
            {
                Response.Write("Job " + oJobDetail.JobID + "<br/><br/>");
                List<ITaskWorker> taskList = worker.LoadTasks();
                foreach (ITaskWorker task in taskList)
                {
                    Response.Write("Task " + task.TaskID + "<br/><pre>");
                    task.CompanyCode = CompanyCode;
                    task.Run();
                    Response.Write("</pre>Run task complete.<br/><br/>");
                }
                Response.Write("Run job complete.<br/>");
            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());
            }

            Console.SetOut(save);
        }

    }
}