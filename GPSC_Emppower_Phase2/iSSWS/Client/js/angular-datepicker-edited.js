﻿/*global angular, document, navigator*/
(function withAngular(angular, navigator) {
    'use strict';

    /**
     dateMinLimit = not included in selectable
     dateMaxLimit = max selectatble, included

    -- Sample Settings --
    show-holidays="true"
    date-limit-from-api="true"
    date-limit-from-api-mode="beforeHire"
    date-limit-from-api-mode="afterYear"
    date-limit-from-api-mode="afterHire"
    date-limit-from-api-mode="beforeAndNow"
    date-limit-from-api-mode="afterAndNow"
    date-limit-from-api-mode="betweenNewYearAndNow"
    date-limit-from-api-mode="afterHireAndNewYear"
    date-limit-from-api-mode="normal"

     */


    var A_DAY_IN_MILLISECONDS = 86400000
        , isMobile = (function isMobile() {

            if (navigator.userAgent &&
                (navigator.userAgent.match(/Android/i) ||
                    navigator.userAgent.match(/webOS/i) ||
                    navigator.userAgent.match(/iPhone/i) ||
                    navigator.userAgent.match(/iPad/i) ||
                    navigator.userAgent.match(/iPod/i) ||
                    navigator.userAgent.match(/BlackBerry/i) ||
                    navigator.userAgent.match(/Windows Phone/i))) {

                return false;
            }
            return false;
        }())
        , generateMonthAndYearHeader = function generateMonthAndYearHeader(prevButton, nextButton) {

            if (isMobile) {

                return [
                    '<div class="_720kb-datepicker-calendar-header">',
                    '<div class="_720kb-datepicker-calendar-header-middle _720kb-datepicker-mobile-item _720kb-datepicker-calendar-month">',
                    '<select ng-model="month" title="{{ dateMonthTitle }}" ng-change="selectedMonthHandle(month)">',
                    '<option ng-repeat="item in months" ng-selected="item === month" ng-disabled=\'!isSelectableMaxDate(item + " " + day + ", " + year) || !isSelectableMinDate(item + " " + day + ", " + year)\' ng-value="$index + 1" value="$index + 1">',
                    '{{ item }}',
                    '</option>',
                    '</select>',
                    '</div>',
                    '</div>',
                    '<div class="_720kb-datepicker-calendar-header">',
                    '<div class="_720kb-datepicker-calendar-header-middle _720kb-datepicker-mobile-item _720kb-datepicker-calendar-month">',
                    '<select ng-model="mobileYear" title="{{ dateYearTitle }}" ng-change="setNewYear(mobileYear)">',
                    '<option ng-repeat="item in paginationYears track by $index" ng-selected="year === item" ng-value="item" ng-disabled="!isSelectableMinYear(item) || !isSelectableMaxYear(item)">',
                    '{{ item }}',
                    '</option>',
                    '</select>',
                    '</div>',
                    '</div>'
                ];
            }

            return [
                '<div class="_720kb-datepicker-calendar-header">',
                '<div class="_720kb-datepicker-calendar-header-left">',
                '<a class="_720kb-datepicker-calendar-month-button  {{lockDate}}" href="javascript:void(0)" ng-class="{\'_720kb-datepicker-item-hidden\': !willPrevMonthBeSelectable()}" ng-click="prevMonth()" title="{{ buttonPrevTitle }}">',
                prevButton,
                '</a>',
                '</div>',
                '<div class="_720kb-datepicker-calendar-header-middle _720kb-datepicker-calendar-month {{lockDate}}-w100">',
                '{{month}}&nbsp;<span class="{{lockDate}}-show">{{year}}</span>',

                '<a href="javascript:void(0)"  class="{{lockDate}}" ng-click="paginateYears(year); showYearsPagination = !showYearsPagination;">',
                '<span>',
                '{{year}}',
                '<i ng-class="{\'_720kb-datepicker-calendar-header-closed-pagination\': !showYearsPagination, \'_720kb-datepicker-calendar-header-opened-pagination\': showYearsPagination}"></i>',
                '</span>',
                '</a>',
                '</div>',
                '<div class="_720kb-datepicker-calendar-header-right ">',
                '<a class="_720kb-datepicker-calendar-month-button   {{lockDate}}" ng-class="{\'_720kb-datepicker-item-hidden\': !willNextMonthBeSelectable()}" href="javascript:void(0)" ng-click="nextMonth()" title="{{ buttonNextTitle }}">',
                nextButton,
                '</a>',
                '</div>',
                '</div>'
            ];
        }
        , generateYearsPaginationHeader = function generateYearsPaginationHeader(prevButton, nextButton) {

            return [
                '<div class="_720kb-datepicker-calendar-header" ng-show="showYearsPagination">',
                '<div class="_720kb-datepicker-calendar-years-pagination">',
                '<a ng-class="{\'_720kb-datepicker-active\': y === year, \'_720kb-datepicker-disabled\': !isSelectableMaxYear(y) || !isSelectableMinYear(y)}" ng-disabled="!isSelectableMinYear(y) || !isSelectableMaxYear(y)" href="javascript:void(0)" ng-click="setNewYear(y)" ng-repeat="y in paginationYears track by $index">',
                '{{y}}',
                '</a>',
                '</div>',
                '<div class="_720kb-datepicker-calendar-years-pagination-pages">',
                '<a href="javascript:void(0)" ng-click="paginateYears(paginationYears[0])" ng-class="{\'_720kb-datepicker-item-hidden\': paginationYearsPrevDisabled}">',
                prevButton,
                '</a>',
                '<a href="javascript:void(0)" ng-click="paginateYears(paginationYears[paginationYears.length -1 ])" ng-class="{\'_720kb-datepicker-item-hidden\': paginationYearsNextDisabled}">',
                nextButton,
                '</a>',
                '</div>',
                '</div>'
            ];
        }
        , generateDaysColumns = function generateDaysColumns() {

            return [
                '<div class="_720kb-datepicker-calendar-days-header">',
                '<div ng-repeat="d in daysInString">',
                '{{d}}',
                '</div>',
                '</div>'
            ];
        }
        , generateDays = function generateDays() {

            return [
                '<div class="_720kb-datepicker-calendar-body">',
                '<a href="javascript:void(0)" ng-repeat="px in prevMonthDays" class="_720kb-datepicker-calendar-day _720kb-datepicker-disabled">',
                '{{px}}',
                '</a>',
                '<a href="javascript:void(0)" ng-repeat="item in days" ng-click="setDatepickerDay(item)" ng-class="{\'_720kb-datepicker-active\': day === item, \'_720kb-datepicker-disabled\': !isSelectableMinDate(year + \'-\' + zeroPaddingText(monthNumber) + \'-\' + zeroPaddingText(item) ) || !isSelectableMaxDate(year + \'-\' + zeroPaddingText(monthNumber) + \'-\' + zeroPaddingText(item)) || !isSelectableDate(monthNumber, year, item)}" class="_720kb-datepicker-calendar-day ">',
                '<span  ng-show="!checkIsHoliday(item,monthNumber,year)">{{item}}</span>',
                //<i class="fa fa-suitcase  "style="font-size: 12px;"></i>
                '<span style="color:red;" ng-show="checkIsHoliday(item,monthNumber,year)">{{item}}</span>',
                '</a>',
                '<a href="javascript:void(0)" ng-repeat="nx in nextMonthDays" class="_720kb-datepicker-calendar-day _720kb-datepicker-disabled">',
                '{{nx}}',
                '</a>',
                '</div>'
            ];
        }
        , generateHtmlTemplate = function generateHtmlTemplate(prevButton, nextButton) {

            var toReturn = [
                '<div class="_720kb-datepicker-calendar {{datepickerClass}} {{datepickerID}} {{classForToggle}}" ng-blur="hideCalendar()">',
                '</div>'
            ]
                , monthAndYearHeader = generateMonthAndYearHeader(prevButton, nextButton)
                , yearsPaginationHeader = generateYearsPaginationHeader(prevButton, nextButton)
                , daysColumns = generateDaysColumns()
                , days = generateDays()
                , iterator = function iterator(aRow) {

                    toReturn.splice(toReturn.length - 1, 0, aRow);
                };

            monthAndYearHeader.forEach(iterator);
            yearsPaginationHeader.forEach(iterator);
            daysColumns.forEach(iterator);
            days.forEach(iterator);

            return toReturn.join('');
        }
        , datepickerDirective = function datepickerDirective($window, $compile, $locale, $filter, $interpolate, $http, CONFIG) {

            var linkingFunction = function linkingFunction($scope, element, attr) {
                // default settings
                if (!$scope.showHolidays) {
                    $scope.holidayFromApi = false;
                    if (!$scope.holidayDates) {
                        $scope.showHolidays = true;
                        $scope.holidayDates = [];
                    } else {
                        $scope.showHolidays = true;
                    }
                } else {
                    $scope.holidayFromApi = !$scope.holidayDates;
                }

                if (!$scope.dateMinLimit) {
                    $scope.dateMinLimit = '0001-01-01';
                }
                if (!$scope.dateMinLimit2) {
                    $scope.dateMinLimit2 = '0001-01-01';
                }
                if (!$scope.dateMaxLimit) {
                    $scope.dateMaxLimit = '9999-12-31';
                }
                if (!$scope.dateMaxLimit2) {
                    $scope.dateMaxLimit2 = '9999-12-31';
                }

                $scope.lastHolidayMonth = undefined;
                $scope.lastHolidayYear = undefined;

                $scope.year = new Date().getFullYear();

                $scope.$watch('dateMinLimitForRange', function dateSetWatcher(newValue) {
                    var newDate = new Date(newValue);
                    var currentDate = new Date($scope.dateSet);
                    if (currentDate < newDate) {
                        $scope.updateDatePicker(newDate);
                    }
                });
                //วันหยุด
                //var dataEvent_holiday = [new Date(2020, 3, 25), new Date(2020, 3, 7), new Date(2020, 3, 15), new Date(2020, 3, 21), new Date(2020, 3, 22)];
                $scope.lockDate = $scope.$eval($scope.lockDate);
                $scope.checkIsHoliday = function (d, m, y) {
                    //console.log($scope.holidayDates);
                    if ($scope.holidayDates == null) {
                        return false;
                    }
                    if (!d || !m || !y) return false;

                    var temp_date_n = $scope.year + '-' + $scope.zeroPaddingText(m) + '-' + $scope.zeroPaddingText(d);
                    var temp_list = $scope.holidayDates.filter(function (value) {
                        return new Date(value).getTime() == new Date(m + '/' + d + '/' + y).getTime();
                        //var _date = new Date(value);
                        //return temp_date_n == (_date.getFullYear() + '-' + $scope.zeroPaddingText(_date.getMonth()) + '-' + $scope.zeroPaddingText(_date.getDate()));
                    });
                    return (temp_list.length > 0) ? true : false;
                };
                $scope.checkBlock = function () {
                    if ($scope.lockDate == true) {
                        return true;
                    }
                    return false;
                }
                $scope.zeroPaddingText = function (sourceText) {
                    var digit = 2;
                    var str = '';
                    for (var i = 0; i < digit; i++) {
                        str += '0';
                    }
                    sourceText = str + sourceText.toString();
                    return sourceText.substr((-1 * digit), digit);
                };


                $scope.updateDatePicker = function (input_day) {
                    var unlimitDateScope_MINDATE = new Date(input_day);
                    var day = unlimitDateScope_MINDATE.getDate();
                    var month = unlimitDateScope_MINDATE.getMonth() + 1;
                    $scope.setDatepickerDay(day, month);
                }
                $scope.callFromOutsider({ theDirFn: $scope.updateDatePicker });

                var defaultPrevButton = null,
                    defaultNextButton = null;

                //get child input
                var selector = attr.selector
                    , thisInput = angular.element(selector ? element[0].querySelector('.' + selector) : element[0].children[0])
                    , theCalendar
                    , defaultPrevButton = '<span class="_720kb-datepicker-default-button glyphicon glyphicon-chevron-left"></span>'
                    , defaultNextButton = '<span class="_720kb-datepicker-default-button glyphicon glyphicon-chevron-right"></span>'
                    , prevButton = attr.buttonPrev || defaultPrevButton
                    , nextButton = attr.buttonNext || defaultNextButton
                    , dateFormat = attr.dateFormat
                    //, dateMinLimit
                    //, dateMaxLimit
                    , dateDisabledDates = $scope.$eval($scope.dateDisabledDates)
                    , date = new Date()
                    , isMouseOn = false
                    , isMouseOnInput = false
                    , datetime = $locale.DATETIME_FORMATS
                    , pageDatepickers
                    , hours24h = 86400000
                    , htmlTemplate = generateHtmlTemplate(prevButton, nextButton)
                    , resetToMinDate = function resetToMinDate() {

                        // bowling
                        var minDateTime = $scope.getMinDate() + 'T00:00:00+07:00';
                        var minDateObj = new Date(minDateTime);
                        $scope.month = $filter('date')(minDateObj, 'MMMM');
                        $scope.monthNumber = Number($filter('date')(minDateObj, 'MM'));
                        $scope.day = Number($filter('date')(minDateObj, 'dd'));
                        $scope.year = Number($filter('date')(minDateObj, 'yyyy'));
                    }
                    , resetToMaxDate = function resetToMaxDate() {

                        // bowling

                        var maxDateTime = $scope.getMaxDate() + 'T00:00:00+07:00';
                        var maxDateObj = new Date(maxDateTime);
                        $scope.month = $filter('date')(maxDateObj, 'MMMM');
                        $scope.monthNumber = Number($filter('date')(maxDateObj, 'MM'));
                        $scope.day = Number($filter('date')(maxDateObj, 'dd'));
                        $scope.year = Number($filter('date')(maxDateObj, 'yyyy'));
                    }
                    , prevYear = function prevYear() {

                        $scope.year = Number($scope.year) - 1;
                    }
                    , nextYear = function nextYear() {

                        $scope.year = Number($scope.year) + 1;
                    }
                    , setInputValue = function setInputValue() {


                        if ($scope.isSelectableMinDate($scope.year + '-' + $scope.zeroPaddingText($scope.monthNumber) + '-' + $scope.zeroPaddingText($scope.day)) &&
                            $scope.isSelectableMaxDate($scope.year + '-' + $scope.zeroPaddingText($scope.monthNumber) + '-' + $scope.zeroPaddingText($scope.day))) {

                            var modelDate = new Date($scope.year + '/' + $scope.monthNumber + '/' + $scope.day);

                            if (attr.dateFormat) {

                                thisInput.val($filter('date')(modelDate, dateFormat));
                            } else {

                                thisInput.val(modelDate);
                            }

                            thisInput.triggerHandler('input');
                            thisInput.triggerHandler('change');//just to be sure;
                            return modelDate;
                        } else {

                            return false;
                        }
                    }
                    , classHelper = {
                        'add': function add(ele, klass) {
                            var classes;

                            if (ele.className.indexOf(klass) > -1) {

                                return;
                            }

                            classes = ele.className.split(' ');
                            classes.push(klass);
                            ele.className = classes.join(' ');
                        },
                        'remove': function remove(ele, klass) {
                            var i
                                , classes;

                            if (ele.className.indexOf(klass) === -1) {

                                return;
                            }

                            classes = ele.className.split(' ');
                            for (i = 0; i < classes.length; i += 1) {

                                if (classes[i] === klass) {

                                    classes = classes.slice(0, i).concat(classes.slice(i + 1));
                                    break;
                                }
                            }
                            ele.className = classes.join(' ');
                        }
                    }
                    , updateMonthChanged = function (param) {
                        if ($scope.holidayFromApi || $scope.dateLimitFromApi) {
                            if (!$scope.lastHolidayMonth || !$scope.lastHolidayYear || $scope.lastHolidayYear != param.year || $scope.lastHolidayMonth != param.month) {

                                if (!param.month) param.month = new Date().getMonth() + 1;
                                if (!param.year) param.year = new Date().getFullYear();

                                $scope.lastHolidayMonth = param.month;
                                $scope.lastHolidayYear = param.year;

                                var URL = CONFIG.SERVER + 'HRTM/GetHolidaysByMonthEMployee';
                                var employeeData = getToken(CONFIG.USER);
                                var oRequestParameter = {
                                    InputParameter: {
                                        Year: param.year,
                                        Month: param.month
                                    }
                                    , CurrentEmployee: employeeData
                                    , Requestor: getToken(CONFIG.USER)
                                    , Creator: getToken(CONFIG.USER)
                                };

                                $http.post(URL, JSON.stringify(oRequestParameter)).then(function (response) {

                                    if ($scope.holidayFromApi) {
                                        // update holidays
                                        $scope.holidayDates = [];
                                        if (response.data.Holiday.length > 0) {
                                            angular.forEach(response.data.Holiday, function (item) {
                                                $scope.holidayDates.push(new Date(item.Year, item.Month - 1, item.Day));
                                            });
                                        }
                                    }
                                    if ($scope.dateLimitFromApi) {
                                        if ($scope.dateLimitFromApiMode == 'beforeHire') {
                                            var hireDate = response.data.HireDate ? new Date(response.data.HireDate) : new Date(response.data.MinDate);
                                            $scope.dateMinLimit = "1900-01-01";
                                            $scope.dateMaxLimit = hireDate.toISOString().slice(0, 10);  // -1
                                        } else if ($scope.dateLimitFromApiMode == 'afterHire') {
                                            var hireDate = response.data.HireDate ? new Date(response.data.HireDate) : new Date(response.data.MinDate);
                                            $scope.dateMinLimit = hireDate.toISOString().slice(0, 10); // +1
                                            $scope.dateMaxLimit = '9999-12-31';
                                        } else if ($scope.dateLimitFromApiMode == 'afterYear') {
                                            $scope.dateMinLimit = response.data.MaxDate;
                                            $scope.dateMaxLimit = '9999-12-31';
                                        } else if ($scope.dateLimitFromApiMode == 'beforeAndNow') {
                                            $scope.dateMinLimit = "1900-01-01";
                                            $scope.dateMaxLimit = (new Date()).toISOString().slice(0, 10);
                                        } else if ($scope.dateLimitFromApiMode == 'afterAndNow') {
                                            var yesterday = new Date();
                                            $scope.dateMinLimit = (yesterday).toISOString().slice(0, 10);
                                            $scope.dateMaxLimit = '9999-12-31';
                                        } else if ($scope.dateLimitFromApiMode == 'betweenNewYearAndNow') {
                                            var newYear = new Date(new Date().getFullYear(), 0, 1);
                                            $scope.dateMinLimit = (newYear).toISOString().slice(0, 10);
                                            $scope.dateMaxLimit = (new Date()).toISOString().slice(0, 10);
                                        } else if ($scope.dateLimitFromApiMode == 'afterHireAndNewYear') {
                                            var hireDate = response.data.HireDate ? new Date(response.data.HireDate) : new Date(response.data.MinDate);
                                            var newYear = new Date(new Date().getFullYear(), 0, 1);
                                            var min = newYear > hireDate ? newYear : hireDate; // +1
                                            $scope.dateMinLimit = min.toISOString().slice(0, 10);
                                            $scope.dateMaxLimit = '9999-12-31';
                                        } else {
                                            // update date limit
                                            if (response.data.HireDate) {
                                                var hireDate = new Date(response.data.HireDate);
                                                $scope.dateMinLimit = hireDate.toISOString().slice(0, 10); // +1]
                                                $scope.dateMaxLimit = response.data.MaxDate;
                                            } else {
                                                $scope.dateMinLimit = "1900-01-01";
                                                $scope.dateMaxLimit = '9999-12-31';
                                            }
                                        }
                                    }
                                }, function (response) {
                                    console.log(response);
                                });
                            }
                        }

                    }
                    , showCalendar = function showCalendar() {
                        // reset current date to selected date
                        $scope.monthNumber = $scope.selMonthNumber;
                        $scope.month = $scope.selMonth;
                        $scope.year = $scope.selYear;
                        $scope.day = $scope.selDay;
                        setDaysInMonth($scope.monthNumber, $scope.year);

                        //lets hide all the latest instances of datepicker
                        pageDatepickers = $window.document.getElementsByClassName('_720kb-datepicker-calendar');

                        angular.forEach(pageDatepickers, function forEachDatepickerPages(value, key) {
                            if (pageDatepickers[key].classList) {

                                pageDatepickers[key].classList.remove('_720kb-datepicker-open');
                            } else {

                                classHelper.remove(pageDatepickers[key], '_720kb-datepicker-open');
                            }
                        });

                        if (theCalendar.classList) {

                            theCalendar.classList.add('_720kb-datepicker-open');
                        } else {

                            classHelper.add(theCalendar, '_720kb-datepicker-open');
                        }
                    }
                    , setDaysInMonth = function setDaysInMonth(month, year) {
                        // Ayodia: เพิ่มสำหรับแก้ไข Calendar
                        if ($scope.callbackOnMonthChange) {
                            $scope.callbackOnMonthChange({ month: month, year: year });
                        }
                        // Ayodia: โหลดข้อฒูลวันหยุด
                        updateMonthChanged({ month: month, year: year });

                        var i
                            , limitDate = new Date(year, month, 0).getDate()
                            , firstDayMonthNumber = new Date(year + '/' + month + '/' + 1).getDay()
                            , lastDayMonthNumber = new Date(year + '/' + month + '/' + limitDate).getDay()
                            , prevMonthDays = []
                            , nextMonthDays = []
                            , howManyNextDays
                            , howManyPreviousDays
                            , monthAlias;

                        $scope.days = [];

                        for (i = 1; i <= limitDate; i += 1) {

                            $scope.days.push(i);
                        }

                        //get previous month days is first day in month is not Sunday
                        if (firstDayMonthNumber === 0) {

                            //no need for it
                            $scope.prevMonthDays = [];
                        } else {

                            howManyPreviousDays = firstDayMonthNumber;
                            //get previous month
                            if (Number(month) === 1) {

                                monthAlias = 12;
                            } else {

                                monthAlias = month - 1;
                            }
                            //return previous month days
                            for (i = 1; i <= new Date(year, monthAlias, 0).getDate(); i += 1) {

                                prevMonthDays.push(i);
                            }
                            //attach previous month days
                            $scope.prevMonthDays = prevMonthDays.slice(-howManyPreviousDays);
                        }

                        //get next month days is first day in month is not Sunday
                        if (lastDayMonthNumber < 6) {

                            howManyNextDays = 6 - lastDayMonthNumber;
                            //get previous month

                            //return next month days
                            for (i = 1; i <= howManyNextDays; i += 1) {

                                nextMonthDays.push(i);
                            }
                            //attach previous month days
                            $scope.nextMonthDays = nextMonthDays;
                        } else {
                            //no need for it
                            $scope.nextMonthDays = [];
                        }
                    }
                    , unregisterDataSetWatcher = $scope.$watch('dateSet', function dateSetWatcher(newValue) {

                        if (newValue) {

                            if (newValue.length == 10) {
                                newValue += 'T00:00:00';
                            } else if (newValue.length == 33) {
                                newValue = newValue.substr(0, 22);
                            }

                            var dateObj = null;
                            if (newValue.indexOf('+') == -1) {
                                dateObj = new Date(newValue + '+07:00');
                            } else {
                                dateObj = new Date(newValue);
                            }

                            //console.log('datepicker.', newValue, attr.dateFormat);
                            //console.log('type.', (typeof newValue));
                            date = $filter('date')(dateObj, attr.dateFormat);
                            //console.log('type of result.', (typeof date));

                            //$scope.month = $filter('date')(date, 'MMMM');//december-November like
                            //$scope.monthNumber = Number($filter('date')(date, 'MM')); // 01-12 like
                            //$scope.day = Number($filter('date')(date, 'dd')); //01-31 like
                            //$scope.year = Number($filter('date')(date, 'yyyy'));//2014 like

                            /* fix bug by bowling */
                            $scope.month = $filter('date')(dateObj, 'MMMM');//december-November like
                            $scope.monthNumber = Number($filter('date')(dateObj, 'MM')); // 01-12 like
                            $scope.day = Number($filter('date')(dateObj, 'dd')); //01-31 like
                            $scope.year = Number($filter('date')(dateObj, 'yyyy'));//2014 like
                            $scope.selYear = $scope.year;
                            $scope.selMonth = $scope.month;
                            $scope.selMonthNumber = $scope.monthNumber;
                            $scope.selDay = $scope.day;

                            //console.log('result.', $scope.month, $scope.monthNumber, $scope.day, $scope.year);

                            setDaysInMonth($scope.monthNumber, $scope.year);

                            if ($scope.dateSetHidden !== 'true') {

                                setInputValue();
                            }
                        }
                    });
                $scope.getMinDate = function () {
                    var r = undefined;
                    var min1 = $scope.dateMinLimit.substr(0, 10);
                    var min2 = $scope.dateMinLimit2.substr(0, 10);

                    var min12 = (min1 > min2 ? min1 : min2);
                    if ($scope.dateMinLimitForRange) {
                        var limitRange = new Date($scope.dateMinLimitForRange);
                        limitRange.setDate(limitRange.getDate() - 1);
                        limitRange = limitRange.toISOString().substr(0, 10);
                        r = limitRange > min12 ? limitRange : min12;
                    } else {
                        r = min12;
                    }
                    return r;
                }
                $scope.getMaxDate = function () {
                    var max1 = $scope.dateMaxLimit.substr(0, 10);
                    var max2 = $scope.dateMaxLimit2.substr(0, 10);
                    return (max1 < max2 ? max1 : max2);
                }
                $scope.nextMonth = function nextMonth() {

                    if ($scope.monthNumber === 12) {

                        $scope.monthNumber = 1;
                        //its happy new year
                        nextYear();
                    } else {

                        $scope.monthNumber += 1;
                    }

                    //check if max date is ok
                    if (!$scope.isSelectableMaxDate($scope.year + '-' + $scope.zeroPaddingText($scope.monthNumber) + '-' + $scope.zeroPaddingText($scope.days[0]))) {

                        resetToMaxDate();
                    }

                    //set next month
                    $scope.month = $filter('date')(new Date($scope.year, $scope.monthNumber - 1), 'MMMM');
                    //reinit days
                    setDaysInMonth($scope.monthNumber, $scope.year);
                    //deactivate selected day
                    $scope.day = $scope.selMonth == $scope.month && $scope.selYear == $scope.year ? $scope.selDay : undefined;
                };

                $scope.willPrevMonthBeSelectable = function willPrevMonthBeSelectable() {
                    var monthNumber = $scope.monthNumber
                        , year = $scope.year
                        , prevDay = $filter('date')(new Date(new Date(year + '/' + monthNumber + '/01').getTime() - hours24h), 'dd'); //get last day in previous month

                    if (monthNumber === 1) {

                        monthNumber = 12;
                        year = year - 1;
                    } else {

                        monthNumber -= 1;
                    }

                    if (!$scope.isSelectableMinDate(year + '-' + $scope.zeroPaddingText(monthNumber) + '-' + $scope.zeroPaddingText(prevDay))) {

                        return false;
                    }

                    return true;
                };

                $scope.willNextMonthBeSelectable = function willNextMonthBeSelectable() {
                    var monthNumber = $scope.monthNumber
                        , year = $scope.year;

                    if (monthNumber === 12) {

                        monthNumber = 1;
                        year += 1;
                    } else {

                        monthNumber += 1;
                    }

                    if (!$scope.isSelectableMaxDate(year + '-' + $scope.zeroPaddingText(monthNumber) + '-01')) {

                        return false;
                    }

                    return true;
                };

                $scope.selYear = undefined;
                $scope.selMonth = undefined;
                $scope.selMonthNumber = undefined;
                $scope.selDay = undefined;

                $scope.prevMonth = function managePrevMonth() {

                    if ($scope.monthNumber === 1) {

                        $scope.monthNumber = 12;
                        //its happy new year
                        prevYear();
                    } else {

                        $scope.monthNumber -= 1;
                    }
                    if (!$scope.isSelectableMinDate($scope.year + '-' + $scope.zeroPaddingText($scope.monthNumber) + '-' + $scope.zeroPaddingText($scope.days[$scope.days.length - 1]))) {

                        resetToMinDate();
                    }
                    //set next month
                    $scope.month = $filter('date')(new Date($scope.year, $scope.monthNumber - 1), 'MMMM');
                    //reinit days
                    setDaysInMonth($scope.monthNumber, $scope.year);
                    //deactivate selected day
                    $scope.day = $scope.selMonth == $scope.month && $scope.selYear == $scope.year ? $scope.selDay : undefined;
                };

                $scope.selectedMonthHandle = function manageSelectedMonthHandle(selectedMonthNumber) {

                    $scope.monthNumber = Number($filter('date')(new Date(selectedMonthNumber + '/01/2000'), 'MM'));
                    setDaysInMonth($scope.monthNumber, $scope.year);
                    setInputValue();
                };

                $scope.setNewYear = function setNewYear(year) {

                    //deactivate selected day
                    if (!isMobile) {
                        $scope.day = undefined;
                    }

                    if ($scope.year < Number(year)) {

                        if (!$scope.isSelectableMaxYear(year)) {

                            return;
                        }
                    } else if ($scope.year > Number(year)) {

                        if (!$scope.isSelectableMinYear(year)) {

                            return;
                        }
                    }

                    $scope.year = Number(year);
                    setDaysInMonth($scope.monthNumber, $scope.year);
                    $scope.paginateYears(year);
                    $scope.showYearsPagination = false;
                };

                $scope.hideCalendar = function hideCalendar() {
                    if (theCalendar.classList) {
                        theCalendar.classList.remove('_720kb-datepicker-open');
                    } else {

                        classHelper.remove(theCalendar, '_720kb-datepicker-open');
                    }
                };

                $scope.setDatepickerDay = function setDatepickerDay(day, month) {
                    if (month) {
                        $scope.monthNumber = month;
                    }
                    if ($scope.isSelectableDate($scope.monthNumber, $scope.year, day) &&
                        $scope.isSelectableMaxDate($scope.year + '-' + $scope.zeroPaddingText($scope.monthNumber) + '-' + $scope.zeroPaddingText(day)) &&
                        $scope.isSelectableMinDate($scope.year + '-' + $scope.zeroPaddingText($scope.monthNumber) + '-' + $scope.zeroPaddingText(day))) {

                        $scope.day = Number(day);
                        var date = setInputValue();

                        // add by bowling
                        $scope.callbackOnChange({ newDate: date });

                        $scope.selYear = $scope.year;
                        $scope.selMonth = $scope.month;
                        $scope.selMonthNumber = $scope.monthNumber;
                        $scope.selDay = $scope.day;

                        if (attr.hasOwnProperty('dateRefocus')) {
                            thisInput[0].focus();
                        }

                        $scope.hideCalendar();
                    }
                };

                $scope.paginateYears = function paginateYears(startingYear) {
                    var i
                        , theNewYears = []
                        , daysToPrepend = 10
                        , daysToAppend = 10;

                    $scope.paginationYears = [];
                    if (isMobile) {

                        daysToPrepend = 50;
                        daysToAppend = 50;
                        startingYear = new Date($scope.getMaxDate()).getFullYear();
                        daysToPrepend = startingYear - new Date($scope.getMinDate()).getFullYear();
                        daysToAppend = 1;
                    }

                    for (i = daysToPrepend; i > 0; i -= 1) {

                        theNewYears.push(Number(startingYear) - i);
                    }

                    for (i = 0; i < daysToAppend; i += 1) {

                        theNewYears.push(Number(startingYear) + i);
                    }
                    //date typing in input date-typer
                    if ($scope.dateTyper === 'true') {

                        thisInput.on('keyup blur', function onTyping() {

                            if (thisInput[0].value &&
                                thisInput[0].value.length &&
                                thisInput[0].value.length > 0) {

                                try {

                                    date = new Date(thisInput[0].value.toString());

                                    if (date.getFullYear() &&
                                        date.getDay() &&
                                        !isNaN(date.getMonth()) &&
                                        $scope.isSelectableDate(date) &&
                                        $scope.isSelectableMaxDate(date) &&
                                        $scope.isSelectableMinDate(date)) {

                                        $scope.$apply(function applyTyping() {

                                            $scope.month = $filter('date')(date, 'MMMM');//december-November like
                                            $scope.monthNumber = Number($filter('date')(date, 'MM')); // 01-12 like
                                            $scope.day = Number($filter('date')(date, 'dd')); //01-31 like

                                            if (date.getFullYear().toString().length === 4) {
                                                $scope.year = Number($filter('date')(date, 'yyyy'));//2014 like
                                            }
                                            setDaysInMonth($scope.monthNumber, $scope.year);
                                        });
                                    }
                                } catch (e) {

                                    return e;
                                }
                            }
                        });
                    }
                    //check range dates
                    if (theNewYears &&
                        theNewYears.length &&
                        !$scope.isSelectableMaxYear(Number(theNewYears[theNewYears.length - 1]) + 1)) {

                        $scope.paginationYearsNextDisabled = true;
                    } else {

                        $scope.paginationYearsNextDisabled = false;
                    }

                    if (theNewYears &&
                        theNewYears.length &&
                        !$scope.isSelectableMinYear(Number(theNewYears[0]) - 1)) {

                        $scope.paginationYearsPrevDisabled = true;
                    } else {

                        $scope.paginationYearsPrevDisabled = false;
                    }

                    $scope.paginationYears = theNewYears;
                };

                $scope.isSelectableDate = function isSelectableDate(monthNumber, year, day) {
                    var i = 0;

                    if (dateDisabledDates &&
                        dateDisabledDates.length > 0) {

                        for (i; i <= dateDisabledDates.length; i += 1) {

                            if (new Date(dateDisabledDates[i]).getTime() === new Date(monthNumber + '/' + day + '/' + year).getTime()) {

                                return false;
                            }
                        }
                    }
                    return true;
                };

                $scope.isSelectableMinDate = function isSelectableMinDate(aDate) {
                    var tempInputDate = aDate + 'T00:00:00+07:00';
                    var inputDate = new Date(tempInputDate);
                    var minDate = new Date($scope.getMinDate());

                    if (inputDate < minDate) {
                        return false;
                    }

                    return true;
                };

                $scope.isSelectableMaxDate = function isSelectableMaxDate(aDate) {
                    var tempInputDate = aDate + 'T00:00:00+07:00';
                    var inputDate = new Date(tempInputDate);
                    var maxDate = new Date($scope.getMaxDate());

                    if (inputDate > maxDate) {
                        return false;
                    }
                    return true;
                };

                $scope.isSelectableMaxYear = function isSelectableMaxYear(year) {
                    if (year > new Date($scope.getMaxDate()).getFullYear()) {

                        return false;
                    }

                    return true;
                };

                $scope.isSelectableMinYear = function isSelectableMinYear(year) {
                    if (year < new Date($scope.getMinDate()).getFullYear()) {

                        return false;
                    }

                    return true;
                };

                // respect previously configured interpolation symbols.
                htmlTemplate = htmlTemplate.replace(/{{/g, $interpolate.startSymbol()).replace(/}}/g, $interpolate.endSymbol());
                $scope.dateMonthTitle = $scope.dateMonthTitle || 'Select month';
                $scope.dateYearTitle = $scope.dateYearTitle || 'Select year';
                $scope.buttonNextTitle = $scope.buttonNextTitle || 'Next';
                $scope.buttonPrevTitle = $scope.buttonPrevTitle || 'Prev';
                $scope.month = $filter('date')(date, 'MMMM');//december-November like
                $scope.monthNumber = Number($filter('date')(date, 'MM')); // 01-12 like
                $scope.selMonthNumber = $scope.monthNumber;
                $scope.selMonth = $scope.month;
                $scope.day = Number($filter('date')(date, 'dd')); //01-31 like


                $scope.year = Number($filter('date')(date, 'yyyy'));//2014 like
                $scope.selYear = $scope.year;

                $scope.months = datetime.MONTH;
                $scope.daysInString = ['0', '1', '2', '3', '4', '5', '6'].map(function mappingFunc(el) {

                    return $filter('date')(new Date(new Date('06/08/2014').valueOf() + A_DAY_IN_MILLISECONDS * el), 'EEE');
                });
                //can this toggle  blur/focus?
                if ($scope.datepickerToggle === 'false') {

                    $scope.classForToggle = 'no-toggle';
                }
                //create the calendar holder and append where needed
                if ($scope.datepickerAppendTo &&
                    $scope.datepickerAppendTo.indexOf('.') !== -1) {

                    $scope.datepickerID = 'datepicker-id-' + new Date().getTime() + (Math.floor(Math.random() * 6) + 8);
                    angular.element(document.getElementsByClassName($scope.datepickerAppendTo.replace('.', ''))[0]).append($compile(angular.element(htmlTemplate))($scope, function afterCompile(el) {

                        theCalendar = angular.element(el)[0];
                    }));
                } else if ($scope.datepickerAppendTo &&
                    $scope.datepickerAppendTo.indexOf('#') !== -1) {

                    $scope.datepickerID = 'datepicker-id-' + new Date().getTime() + (Math.floor(Math.random() * 6) + 8);
                    angular.element(document.getElementById($scope.datepickerAppendTo.replace('#', ''))).append($compile(angular.element(htmlTemplate))($scope, function afterCompile(el) {

                        theCalendar = angular.element(el)[0];
                    }));
                } else if ($scope.datepickerAppendTo &&
                    $scope.datepickerAppendTo === 'body') {
                    $scope.datepickerID = 'datepicker-id-' + (new Date().getTime() + (Math.floor(Math.random() * 6) + 8));
                    angular.element(document).find('body').append($compile(angular.element(htmlTemplate))($scope, function afterCompile(el) {

                        theCalendar = angular.element(el)[0];
                    }));
                } else {

                    thisInput.after($compile(angular.element(htmlTemplate))($scope));
                    //get the calendar as element
                    theCalendar = element[0].querySelector('._720kb-datepicker-calendar');
                }
                //some tricky dirty events to fire if click is outside of the calendar and show/hide calendar when needed
                thisInput.on('focus click focusin', function onFocusAndClick() {

                    isMouseOnInput = true;

                    if (!isMouseOn &&
                        !isMouseOnInput && theCalendar) {

                        $scope.hideCalendar();
                    } else {

                        showCalendar();
                    }
                });

                thisInput.on('focusout blur', function onBlurAndFocusOut() {

                    isMouseOnInput = false;
                });

                angular.element(theCalendar).on('mouseenter', function onMouseEnter() {

                    isMouseOn = true;
                });

                angular.element(theCalendar).on('mouseleave', function onMouseLeave() {

                    isMouseOn = false;
                });

                angular.element(theCalendar).on('focusin', function onCalendarFocus() {

                    isMouseOn = true;
                });

                angular.element($window).on('click focus focusin', function onClickOnWindow() {

                    if (!isMouseOn &&
                        !isMouseOnInput && theCalendar) {

                        $scope.hideCalendar();
                    }
                });

                //check always if given range of dates is ok
                if (!$scope.isSelectableMinYear($scope.year) ||
                    !$scope.isSelectableMinDate($scope.year + '-' + $scope.zeroPaddingText($scope.monthNumber) + '-' + $scope.zeroPaddingText($scope.day))) {

                    resetToMinDate();
                }

                if (!$scope.isSelectableMaxYear($scope.year) ||
                    !$scope.isSelectableMaxDate($scope.year + '-' + $scope.zeroPaddingText($scope.monthNumber) + '-' + $scope.zeroPaddingText($scope.day))) {

                    resetToMaxDate();
                }

                $scope.paginateYears($scope.year);
                setDaysInMonth($scope.monthNumber, $scope.year);

                $scope.$on('$destroy', function unregisterListener() {

                    unregisterDataSetWatcher();
                    thisInput.off('focus click focusout blur');
                    angular.element(theCalendar).off('mouseenter mouseleave focusin');
                    angular.element($window).off('click focus');
                });
            };

            return {
                'restrict': 'AEC',
                'scope': {
                    'dateSet': '@',
                    // Ayoia : support limit 2 values , using max
                    'dateMinLimit': '@',
                    'dateMinLimit2': '@',
                    'dateMinLimitForRange': '@',
                    // Ayodai : support limit 2 values, using min
                    'dateMaxLimit': '@',
                    'dateMaxLimit2': '@',
                    // enable query dat limit from api
                    'dateLimitFromApi': '@',
                    // limit mode
                    'dateLimitFromApiMode': '@',
                    'dateMonthTitle': '@',
                    'dateYearTitle': '@',
                    'buttonNextTitle': '@',
                    'buttonPrevTitle': '@',
                    'dateDisabledDates': '@',
                    'showHolidays': '@',
                    'holidayDates': '@',
                    'holidayDates': '=?',
                    'lockDate': '@',
                    'dateSetHidden': '@',
                    'dateTyper': '@',
                    'datepickerAppendTo': '@',
                    'datepickerToggle': '@',
                    'datepickerClass': '@',
                    'callbackOnChange': '&',
                    'callbackOnMonthChange': '&',
                    'callFromOutsider': '&'
                },
                'link': linkingFunction
            };
        };

    angular.module('720kb.datepicker', [])
        .directive('datepicker', ['$window', '$compile', '$locale', '$filter', '$interpolate', '$http', 'CONFIG', datepickerDirective]);
}(angular, navigator));
