﻿using System;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using ESS.UTILITY.CONVERT;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Globalization;
using Microsoft.Reporting.WebForms;
using ESS.UTILITY.EXPORT;
using ESS.UTILITY.FILE;
using iSSWS.Models;
using ESS.PORTALENGINE;
using ESS.SHAREDATASERVICE;
using ESS.EMPLOYEE;
using ESS.HR.PY;
using ESS.HR.PY.DATACLASS;
using ESS.HR.PY.CONFIG;
using ESS.HR.PY.INFOTYPE;
using ESS.HR.PA.INFOTYPE;
using ESS.WORKFLOW;
using ESS.EMPLOYEE.CONFIG.OM;
using ESS.HR.PA;
using System.Web;
using System.IO;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net;
using System.Text;
using System.Net.Http.Headers;

namespace iSSWS.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class HRPYController : ApiController
    {
        private CultureInfo thCL = new CultureInfo("th-TH");
        private CultureInfo enCL = new CultureInfo("en-US");
        string temp1 = "";
        string temp2 = "";
        string temp3 = "";
        string temp4 = "";
        string temp5 = "";
        string temp6 = "";
        string temp7 = "";
        string temp8 = "";

        #region "TaxAllowance"
        [HttpPost]
        public TaxAllowance GetTaxAllowanceData([FromBody] RequestParameter oRequestParameter)
        {
            TaxAllowance oTaxAllowance = new TaxAllowance();
            oTaxAllowance = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetTaxAllowanceData(oRequestParameter.Requestor.EmployeeID, DateTime.Now);

            return oTaxAllowance;
        }

        [HttpPost]
        public List<SpouseAllowance> GetTaxAllowanceEditData([FromBody] RequestParameter oRequestParameter)
        {
            List<SpouseAllowance> oReturn = new List<SpouseAllowance>();
            oReturn = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetAllSpouseAllowanceDropdownData();
            return oReturn;
        }
        #endregion

        #region "ProvidentFund"
        [HttpPost]
        public INFOTYPE0366 GetProvidentFundData([FromBody] RequestParameter oRequestParameter)
        {
            INFOTYPE0366 oResult = new INFOTYPE0366();
            oResult = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetProvidentFund(oRequestParameter.Requestor.EmployeeID, DateTime.Now);

            return oResult;
        }

        [HttpPost]
        public List<PF_ProvidentFundDetail> GetTextProvidentFundDetail([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
                List<PF_ProvidentFundDetail> oResult = new List<PF_ProvidentFundDetail>();
                //oResult = HRPYManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetTextProvidentFundDetail(oRequestParameter.InputParameter["KeyType"].ToString(), oRequestParameter.InputParameter["KeyCode"].ToString(), oRequestParameter.InputParameter["KeyValue"].ToString());
                oResult = HRPYManagement.CreateInstance(sCompanyCode).GetTextProvidentFundDetail(oRequestParameter.InputParameter["KeyType"].ToString(), oRequestParameter.InputParameter["KeyCode"].ToString(), oRequestParameter.InputParameter["KeyValue"].ToString());

                return oResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost]
        public List<PF_PeriodSetting> GetCheckPeriod([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                WorkflowController oWorkflowController = new WorkflowController();

                List<PF_PeriodSetting> TResult = new List<PF_PeriodSetting>();
                TResult = HRPYManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetEffectiveDate();

                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public List<TimeAwareLink> GetAllTimeAwareLink([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
                WorkflowController oWorkflowController = new WorkflowController();

                List<TimeAwareLink> TResult = new List<TimeAwareLink>();
                //TResult = HRPYManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllTimeAwareLink();
                TResult = HRPYManagement.CreateInstance(sCompanyCode).GetAllTimeAwareLink();

                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public void UpdateTimeAwareLink([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
                WorkflowController oWorkflowController = new WorkflowController();
                //HRPYManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).UpdateTimeAwareLink(Convert.ToInt32(oRequestParameter.InputParameter["LinkID"].ToString()), oRequestParameter.InputParameter["BeginDate"].ToString(), oRequestParameter.InputParameter["EndDate"].ToString(), oRequestParameter.Requestor.EmployeeID);
                HRPYManagement.CreateInstance(sCompanyCode).UpdateTimeAwareLink(Convert.ToInt32(oRequestParameter.InputParameter["LinkID"].ToString()), oRequestParameter.InputParameter["BeginDate"].ToString(), oRequestParameter.InputParameter["EndDate"].ToString(), oRequestParameter.Requestor.EmployeeID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public List<PF_Graph> GetFundQuarter([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                WorkflowController oWorkflowController = new WorkflowController();

                string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
                List<PF_Graph> TResult = new List<PF_Graph>();
                //TResult = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode.ToString()).GetFundQuarter(oRequestParameter.CurrentEmployee.EmployeeID.ToString(), oRequestParameter.InputParameter["KeyType"].ToString(), Convert.ToInt16(oRequestParameter.InputParameter["KeyYear"].ToString()), oRequestParameter.InputParameter["KeyValue"].ToString());
                TResult = HRPYManagement.CreateInstance(sCompanyCode).GetFundQuarter(oRequestParameter.CurrentEmployee.EmployeeID.ToString(), oRequestParameter.InputParameter["KeyType"].ToString(), Convert.ToInt16(oRequestParameter.InputParameter["KeyYear"].ToString()), oRequestParameter.InputParameter["KeyValue"].ToString());

                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost]
        public List<INFOTYPE0021> GetProvidentMemberData([FromBody] RequestParameter oRequestParameter)
        {
            List<INFOTYPE0021> oResult = new List<INFOTYPE0021>();
            oResult = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetProvidentFundMember(oRequestParameter.CurrentEmployee.EmployeeID);

            return oResult;
        }

        [HttpPost]
        public List<PF_PeriodSetting> GetPeriodSetting([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                WorkflowController oWorkflowController = new WorkflowController();

                string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
                string sKeyType = oRequestParameter.InputParameter["KeyType"].ToString();

                List<PF_PeriodSetting> TResult = new List<PF_PeriodSetting>();


                if (sKeyType == "SAVEDATA")
                {
                    string sKeyYear = oRequestParameter.InputParameter["KeyYear"].ToString();
                    string sKeyValue = oRequestParameter.InputParameter["KeyValue"].ToString();

                    ConfigurationLog ConfigLog = new ConfigurationLog();
                    ConfigLog.EmployeeID = oRequestParameter.Requestor.EmployeeID;
                    ConfigLog.ConfigTable = "PF_PeriodSetting";
                    ConfigLog.Remark = "Filtered by PeriodYear";
                    //Filter for the period same as Display

                    DataSet oDs = new DataSet();

                    DataTable oDt = new DataTable("PF_PeriodSetting");
                    oDt.Columns.Add("KeyYear", typeof(System.String));
                    oDt.Columns.Add("KeyValue", typeof(System.String));

                    DataRow oDr = oDt.NewRow();
                    oDr["KeyYear"] = sKeyYear;
                    oDr["KeyValue"] = sKeyValue;
                    oDt.Rows.Add(oDr);

                    if (!oDs.Tables.Contains("PF_PeriodSetting"))
                        oDs.Tables.Add(oDt);


                    DataTable tempTable = new DataTable();

                    tempTable = oDs.Tables[0].Clone();
                    foreach (DataRow dr in oDs.Tables[0].Rows)
                        tempTable.ImportRow(dr);
                    ConfigLog.NewData = new DataSet("PF_PeriodSetting");
                    ConfigLog.LoadToNewData(tempTable);
                    HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).SaveConfigurationLog(ConfigLog);
                }

                TResult = HRPYManagement.CreateInstance(sCompanyCode).GetPeriodSetting(oRequestParameter.CurrentEmployee.EmployeeID.ToString(), oRequestParameter.InputParameter["KeyType"].ToString(), Convert.ToInt16(oRequestParameter.InputParameter["KeyYear"].ToString()), oRequestParameter.InputParameter["KeyValue"].ToString());
                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public INFOTYPE0021 GetEmptyMember()
        {
            var EmptyMember = new INFOTYPE0021();
            EmptyMember.BirthDate = DateTime.Now;
            return EmptyMember;
        }

        [HttpPost]
        public string GetLinkTAXDEDUCTIONWEB([FromBody] RequestParameter oRequestParameter)
        {
            string sLinkTAXDEDUCTIONWEB = string.Empty;
            sLinkTAXDEDUCTIONWEB = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).TAXDEDUCTIONWEB;
            return sLinkTAXDEDUCTIONWEB;
        }

        [HttpPost]
        public List<string> GetPaySlipYear([FromBody] RequestParameter oRequestParameter)
        {
            List<string> oListYear = new List<string>();
            oListYear = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetPaySlipYear(oRequestParameter.Requestor.HiringDate);
            return oListYear;
        }

        [HttpPost]
        public DateTime SetEffectiveDateByDefaultPaymentDate([FromBody] RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            //DateTime dEffectiveDate = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).SetEffectiveDateByDefaultPaymentDate(Convert.ToInt32(oRequestParameter.InputParameter["Month"].ToString()), Convert.ToInt32(oRequestParameter.InputParameter["Year"].ToString()));

            DateTime dEffectiveDate = HRPYManagement.CreateInstance(sCompanyCode).SetEffectiveDateByDefaultPaymentDate(Convert.ToInt32(oRequestParameter.InputParameter["Month"].ToString()), Convert.ToInt32(oRequestParameter.InputParameter["Year"].ToString()));
            return dEffectiveDate;
        }


        //แสดง Period ที่ user มีสิทธิ์ในหน้า PaySlip
        [HttpPost]
        public DataTable GetListPayslipPeriodSetting([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                bool isAdmin = false;
                List<string> oListUserRoles = oRequestParameter.Requestor.UserRoles;
                List<PayslipPeriodSetting> oResult = new List<PayslipPeriodSetting>();
                var TResult = ShareDataManagement.LookupCache(oRequestParameter.CurrentEmployee.CompanyCode, "ESS.HR.PY", "ADMIN");
                foreach (string sRole in oListUserRoles)
                {
                    if (TResult.IndexOf(sRole) != -1)
                    {
                        isAdmin = true;
                    }
                }
                if (isAdmin)
                {
                    oResult = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetPaySlipMonthByYearForAdmin(Convert.ToInt32(oRequestParameter.InputParameter["Year"].ToString()), oRequestParameter.Requestor.HiringDate);
                }
                else
                {
                    oResult = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetPaySlipMonthByYear(Convert.ToInt32(oRequestParameter.InputParameter["Year"].ToString()), oRequestParameter.Requestor.HiringDate);
                }

                string strValue = string.Empty;
                List<PaySlipReportWageDataMerge> oPaySlipReportWageDataMerge;
                List<string> oListMonth = new List<string>();


                DataTable oDT = new DataTable();
                oDT.Columns.Add("MONTH_KEY", typeof(System.String));
                oDT.Columns.Add("MONTH_VALUE", typeof(System.String));
                oDT.Columns.Add("MONTH_NAME", typeof(System.String));
                oDT.Columns.Add("MONTH_SELECT", typeof(bool));

                if (oResult.Count > 0)
                {
                    foreach (PayslipPeriodSetting item in oResult)
                    {
                        if (item.IsOffCycle)
                        {
                            oPaySlipReportWageDataMerge = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetPaySlipReportData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.CurrentEmployee.Language, item);
                            if (oPaySlipReportWageDataMerge.Count > 0 && oPaySlipReportWageDataMerge[0].Amount1 > 0)
                            {
                                //strValue = string.Format("{0}|{1}|{2}", item.OffCycleDate.ToString("ddMMyyyy", enCL), (item.IsOffCycle ? "1" : "0"), (item.IsDraft ? "1" : "0")); remove IsDraft by zkathawut
                                strValue = string.Format("{0}|{1}", item.OffCycleDate.ToString("ddMMyyyy", enCL), (item.IsOffCycle ? "1" : "0"));
                                oListMonth.Add(string.Format("{0}|{1}", item.OffCycleDate.Month, strValue));
                                DataRow dRow = oDT.NewRow();
                                dRow["MONTH_KEY"] = item.OffCycleDate.Month;
                                dRow["MONTH_VALUE"] = strValue;
                                dRow["MONTH_NAME"] = "";

                                if (item.OffCycleDate.Year == DateTime.Now.Year && item.OffCycleDate.Month == DateTime.Now.Month)
                                {
                                    if (item.IsOffCycle)
                                        dRow["MONTH_SELECT"] = false;
                                    else
                                        dRow["MONTH_SELECT"] = true;
                                }
                                else
                                    dRow["MONTH_SELECT"] = false;

                                oDT.Rows.Add(dRow);
                            }
                        }
                        else
                        {
                            item.OffCycleDate = DateTime.ParseExact(item.PeriodYear.ToString() + item.PeriodMonth.ToString("00") + "01", "yyyyMMdd", enCL);
                            item.OffCycleDate = item.OffCycleDate.AddMonths(1).AddSeconds(-1);
                            //strValue = string.Format("{0}|{1}|{2}", item.OffCycleDate.ToString("ddMMyyyy", enCL), (item.IsOffCycle ? "1" : "0"), (item.IsDraft ? "1" : "0")); remove IsDraft by zkathawut
                            strValue = string.Format("{0}|{1}", item.OffCycleDate.ToString("ddMMyyyy", enCL), (item.IsOffCycle ? "1" : "0"));
                            oListMonth.Add(string.Format("{0}|{1}", item.OffCycleDate.Month, strValue));
                            DataRow dRow = oDT.NewRow();
                            dRow["MONTH_KEY"] = item.OffCycleDate.Month;
                            dRow["MONTH_VALUE"] = strValue;
                            dRow["MONTH_NAME"] = "";

                            if (item.OffCycleDate.Year == DateTime.Now.Year && item.OffCycleDate.Month == DateTime.Now.Month)
                            {
                                if (item.IsOffCycle)
                                    dRow["MONTH_SELECT"] = false;
                                else
                                    dRow["MONTH_SELECT"] = true;
                            }
                            else
                            {
                                dRow["MONTH_SELECT"] = false;
                            }


                            oDT.Rows.Add(dRow);
                        }
                    }
                }
                return oDT;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //แสดง PaySlip
        [HttpPost]
        public List<PaySlipReportWageDataMerge> GetPaySlipReportData([FromBody] RequestParameter oRequestParameter)
        {
            PayslipPeriodSetting oPeriod = new PayslipPeriodSetting();
            string sPeriodSetting = oRequestParameter.InputParameter["PeriodSetting"].ToString();
            string[] Parameters = sPeriodSetting.Split('|');
            oPeriod.OffCycleDate = DateTime.ParseExact(Parameters[0], "ddMMyyyy", enCL);
            oPeriod.IsOffCycle = (Parameters[1] == "1" ? true : false);
            //oPeriod.IsDraft = (Parameters[2] == "1" ? true : false);
            oPeriod.PeriodMonth = Convert.ToInt32(oPeriod.OffCycleDate.ToString("MM", enCL));
            oPeriod.PeriodYear = Convert.ToInt32(oPeriod.OffCycleDate.ToString("yyyy", enCL));

            bool isVerifyPinCode = false;
            isVerifyPinCode = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).VerifyPinCode(oRequestParameter.Requestor.EmployeeID, oRequestParameter.InputParameter["PinCode"].ToString());

            //oPeriod.OffCycleDate = new DateTime(2019, 8, 31);
            //oPeriod.IsOffCycle = false;
            //oPeriod.IsDraft = false;
            //oPeriod.PeriodMonth = 8;
            //oPeriod.PeriodYear = 2019;

            List<PaySlipReportWageDataMerge> TResult = new List<PaySlipReportWageDataMerge>();
            if (isVerifyPinCode)
            {
                TResult = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetPaySlipReportData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.CurrentEmployee.Language, oPeriod);

                // PayslipResult oPay = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetPayslipByMonthYear(oRequestParameter.Requestor.EmployeeID, oRequestParameter.CurrentEmployee.CompanyCode, oPeriod, "M", "");
            }
            return TResult;
        }

        //แสดงข้อมูลหน้าตั้งค่า PaySlip
        [HttpPost]
        public List<PayslipPeriodSetting> GetPeriodSettingAll([FromBody] RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            List<PayslipPeriodSetting> TResult = new List<PayslipPeriodSetting>();
            //TResult = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetPeriodSettingAll();
            TResult = HRPYManagement.CreateInstance(sCompanyCode).GetPeriodSettingAll();
            return TResult;
        }

        [HttpPost]
        public List<string> GetYearPaySlipPeriodSetting([FromBody] RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            List<string> oListYear = new List<string>();
            //oListYear = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetYearPaySlipPeriodSetting();
            oListYear = HRPYManagement.CreateInstance(sCompanyCode).GetYearPaySlipPeriodSetting();
            return oListYear;
        }

        [HttpPost]
        public DataTable GetMonthPaySlipPeriodSetting([FromBody] RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            DataTable oDT = new DataTable();
            //oDT = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetMonthPaySlipPeriodSetting(oRequestParameter.CurrentEmployee.Language);
            oDT = HRPYManagement.CreateInstance(sCompanyCode).GetMonthPaySlipPeriodSetting(oRequestParameter.CurrentEmployee.Language);
            return oDT;
        }

        [HttpPost]
        public bool HavePinCode([FromBody] RequestParameter oRequestParameter)
        {
            bool isHavePinCode = false;
            isHavePinCode = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).HavePinCode(oRequestParameter.Requestor.EmployeeID);
            return isHavePinCode;
        }

        [HttpPost]
        public bool VerifyPinCode([FromBody] RequestParameter oRequestParameter)
        {
            bool isVerifyPinCode = false;
            isVerifyPinCode = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).VerifyPinCode(oRequestParameter.Requestor.EmployeeID, oRequestParameter.InputParameter["PinCode"].ToString());
            return isVerifyPinCode;
        }

        [HttpPost]
        public DataTable ValidatePinCodeSetting([FromBody] RequestParameter oRequestParameter)
        {
            string ErrorMessage = string.Empty;
            bool isVerifyPinCode = false;
            bool isValidatePolicy = false;
            DataTable oResult = new DataTable();
            oResult.Columns.Add("RESULT", typeof(System.Boolean));
            oResult.Columns.Add("MSG_RESULT", typeof(System.String));
            DataRow dRow = oResult.NewRow();

            isVerifyPinCode = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).VerifyPinCode(oRequestParameter.Requestor.EmployeeID, oRequestParameter.InputParameter["PinCode"].ToString());
            if (isVerifyPinCode)
            {
                if (oRequestParameter.InputParameter["NewPinCode"].ToString() == oRequestParameter.InputParameter["ConfirmNewPinCode"].ToString())
                {
                    isValidatePolicy = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).ValidateNewPasswordPolicy(oRequestParameter.InputParameter["NewPinCode"].ToString(), out ErrorMessage);
                    dRow["RESULT"] = isValidatePolicy;
                    dRow["MSG_RESULT"] = ErrorMessage;
                }
                else
                {
                    dRow["RESULT"] = false;
                    dRow["MSG_RESULT"] = "กรุณากรอก PINCODE ใหม่และยืนยัน PINCODE ใหม่ให้ตรงกัน";
                }
            }
            else
            {
                dRow["RESULT"] = isVerifyPinCode;
                dRow["MSG_RESULT"] = "PINCODE เก่าไม่ถูกต้อง";
            }
            oResult.Rows.Add(dRow);

            return oResult;
        }

        //ส่ง Link ทางเมลเพื่อตั้งค่า PINCode
        [HttpPost]
        public void RequestNEWPin([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);

                EmployeeManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).RequestNEWPin(oEmp);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public void CreateNEWPin([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                EmployeeManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).CreateNEWPin(oRequestParameter.Requestor.EmployeeID, oRequestParameter.InputParameter["TicketID"].ToString(), oRequestParameter.InputParameter["NewPinCode"].ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public bool ValidateTicket([FromBody] RequestParameter oRequestParameter)
        {
            bool isValidTicket = false;
            isValidTicket = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).ValidateTicket(oRequestParameter.Requestor.EmployeeID, oRequestParameter.InputParameter["TicketClass"].ToString(), oRequestParameter.InputParameter["TicketID"].ToString());
            return isValidTicket;
        }

        [HttpPost]
        public void ChangePinCode([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                EmployeeManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).ChangePinCode(oRequestParameter.Requestor.EmployeeID, oRequestParameter.InputParameter["PinCode"].ToString(), oRequestParameter.InputParameter["NewPinCode"].ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public DataTable ValidatePinCodePolicy([FromBody] RequestParameter oRequestParameter)
        {
            string ErrorMessage = string.Empty;
            bool isValidPinCode = false;
            DataTable oResult = new DataTable();
            oResult.Columns.Add("RESULT", typeof(System.Boolean));
            oResult.Columns.Add("MSG_RESULT", typeof(System.String));
            isValidPinCode = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).ValidateNewPasswordPolicy(oRequestParameter.InputParameter["NewPassword"].ToString(), out ErrorMessage);
            DataRow dRow = oResult.NewRow();
            dRow["RESULT"] = isValidPinCode;
            dRow["MSG_RESULT"] = ErrorMessage;
            oResult.Rows.Add(dRow);
            return oResult;
        }

        [HttpPost]
        public DataTable ValidateNewPinCode([FromBody] RequestParameter oRequestParameter)
        {
            string ErrorMessage = string.Empty;
            bool isValidPinCode = false;
            DataTable oResult = new DataTable();
            oResult.Columns.Add("RESULT", typeof(System.Boolean));
            oResult.Columns.Add("MSG_RESULT", typeof(System.String));
            DataRow dRow = oResult.NewRow();
            if (oRequestParameter.InputParameter["NewPinCode"].ToString() == oRequestParameter.InputParameter["ConfirmNewPinCode"].ToString())
            {
                isValidPinCode = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).ValidateNewPasswordPolicy(oRequestParameter.InputParameter["NewPinCode"].ToString(), out ErrorMessage);
                dRow["RESULT"] = isValidPinCode;
                dRow["MSG_RESULT"] = ErrorMessage;
            }
            else
            {
                dRow["RESULT"] = false;
                dRow["MSG_RESULT"] = "กรุณากรอก PINCODE ใหม่และยืนยัน PINCODE ใหม่ให้ตรงกัน";
            }
            oResult.Rows.Add(dRow);
            return oResult;
        }
        #endregion

        [HttpPost]
        public void GetPaySlipExport([FromBody] RequestParameter oRequestParameter)
        {
            #region Old
            //PayslipPeriodSetting oPeriod = new PayslipPeriodSetting();
            //string sPeriodSetting = oRequestParameter.InputParameter["PeriodSetting"].ToString();
            //string[] Parameters = sPeriodSetting.Split('|');
            //oPeriod.OffCycleDate = DateTime.ParseExact(Parameters[0], "ddMMyyyy", enCL);
            //oPeriod.IsOffCycle = (Parameters[1] == "1" ? true : false);
            //oPeriod.IsDraft = (Parameters[2] == "1" ? true : false);
            //oPeriod.PeriodMonth = Convert.ToInt32(oPeriod.OffCycleDate.ToString("MM", enCL));
            //oPeriod.PeriodYear = Convert.ToInt32(oPeriod.OffCycleDate.ToString("yyyy", enCL));

            //List<PaySlipReportWageDataMerge> oResult = new List<PaySlipReportWageDataMerge>();
            //oResult = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetPaySlipReportData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.CurrentEmployee.Language, oPeriod);

            //DataTable oDTPaySlipReportWageDataMerge = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).ToDataTable<PaySlipReportWageDataMerge>(oResult);
            //DataSet ds = new DataSet("DSPaySlip");
            //if (!ds.Tables.Contains("PaySlipReportWageDataMerge"))
            //    ds.Tables.Add(oDTPaySlipReportWageDataMerge);

            //var Type = oRequestParameter.InputParameter["Type"].ToString();
            //var Status = oRequestParameter.InputParameter["Status"].ToString();
            //var ReportName = oRequestParameter.InputParameter["ReportName"].ToString();
            //var LanguageCode = oRequestParameter.InputParameter["LanguageCode"].ToString();
            //var Employee_id = oRequestParameter.InputParameter["Employee_id"].ToString();
            //var ExportType = oRequestParameter.InputParameter["ExportType"].ToString();
            //var ExportName = oRequestParameter.InputParameter["EmployeeName"].ToString();
            //var DateText = oRequestParameter.InputParameter["DateText"].ToString();
            //var TimeText = oRequestParameter.InputParameter["TimeText"].ToString();
            //var ExportDate = DateText + " " + DateTime.Now.ToString("dd/MM/yyyy") + " " + TimeText + " " + DateTime.Now.ToString("HH:mm");

            ////Text Column
            //var T_COLCODE = oRequestParameter.InputParameter["T_COLCODE"].ToString();
            //var T_COLINCOME = oRequestParameter.InputParameter["T_COLINCOME"].ToString();
            //var T_COLAMOUNT = oRequestParameter.InputParameter["T_COLAMOUNT"].ToString();
            //var T_COLOUTCOME = oRequestParameter.InputParameter["T_COLOUTCOME"].ToString();
            //var T_COLREMARK = oRequestParameter.InputParameter["T_COLREMARK"].ToString();


            //var T_COLTOTALINCOME = oRequestParameter.InputParameter["T_COLTOTALINCOME"].ToString();
            //var T_COLTOTALOUTCOME = oRequestParameter.InputParameter["T_COLTOTALOUTCOME"].ToString();
            //var T_COLNET = oRequestParameter.InputParameter["T_COLNET"].ToString();
            //var T_COLINCOMECOLLECTION = oRequestParameter.InputParameter["T_COLINCOMECOLLECTION"].ToString();
            //var T_COLTAXCOLLECTION = oRequestParameter.InputParameter["T_COLTAXCOLLECTION"].ToString();
            //var T_COLPROVIDENTFUND = oRequestParameter.InputParameter["T_COLPROVIDENTFUND"].ToString();

            //string companyLogo = oRequestParameter.CurrentEmployee.CompanyCode;
            //string pathImage = new Uri(System.Web.HttpContext.Current.Server.MapPath("~/Client/images/CompanyLogo/" + companyLogo + ".jpg")).AbsoluteUri;
            //string ExportPeriod = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetCommonText("SYSTEM", oRequestParameter.CurrentEmployee.Language, "D_M" + oPeriod.PeriodMonth) + " " + oPeriod.PeriodYear;
            //string EmpIDHeader = oRequestParameter.Requestor.EmployeeID + " - " + oRequestParameter.Requestor.Name;
            //string PositionName = oRequestParameter.Requestor.Position;
            //string OrgUnitName = oRequestParameter.Requestor.OrgUnitName;

            //ReportObject oReportObject = new ReportObject();
            //oReportObject.ExportType = ExportType;
            //oReportObject.ReportName = System.Web.HttpContext.Current.Server.MapPath("~/Views/Report/HR/PYPaySlipReport.rdlc");
            //oReportObject.ExportFileName = System.Web.HttpContext.Current.Server.MapPath("~/Client/Report/PaySlipReport" + Employee_id);

            //oReportObject.ReportParameters = new List<ReportParameter>()
            //    {
            //         new ReportParameter("ReportName",ReportName)
            //        ,new ReportParameter("ExportDate",ExportDate)
            //        ,new ReportParameter("ExportName",ExportName)
            //        ,new ReportParameter("ExportPeriod",ExportPeriod)
            //        ,new ReportParameter("EmpIDHeader",EmpIDHeader)
            //        ,new ReportParameter("PositionName",PositionName)
            //        ,new ReportParameter("OrgUnitName",OrgUnitName)
            //        ,new ReportParameter("T_COLCODE",T_COLCODE)
            //        ,new ReportParameter("T_COLINCOME",T_COLINCOME)
            //        ,new ReportParameter("T_COLAMOUNT",T_COLAMOUNT)
            //        ,new ReportParameter("T_COLOUTCOME",T_COLOUTCOME)
            //        ,new ReportParameter("T_COLREMARK",T_COLREMARK)
            //        ,new ReportParameter("T_COLTOTALINCOME",T_COLTOTALINCOME)
            //        ,new ReportParameter("T_COLTOTALOUTCOME",T_COLTOTALOUTCOME)
            //        ,new ReportParameter("T_COLNET",T_COLNET)
            //        ,new ReportParameter("T_COLINCOMECOLLECTION",T_COLINCOMECOLLECTION)
            //        ,new ReportParameter("T_COLTAXCOLLECTION",T_COLTAXCOLLECTION)
            //        ,new ReportParameter("T_COLPROVIDENTFUND",T_COLPROVIDENTFUND)
            //        ,new ReportParameter("ImageLogo",pathImage)
            //    };
            //oReportObject.DataSource = ds;

            //ReportViewer oReportViewer = new ReportViewer();
            //oReportViewer.LocalReport.DataSources.Clear();
            //oReportViewer.ProcessingMode = ProcessingMode.Local;
            //oReportViewer.LocalReport.ReportPath = oReportObject.ReportName;
            //oReportViewer.LocalReport.DataSources.Add(new ReportDataSource("DSPaySlip", ds.Tables[0]));
            //oReportViewer.LocalReport.EnableExternalImages = true;

            //if (oReportObject.ReportParameters != null && oReportObject.ReportParameters.Count > 0)
            //{
            //    oReportViewer.LocalReport.SetParameters(oReportObject.ReportParameters);
            //}

            //oReportViewer.ShowRefreshButton = false;
            //ReportPageSettings oReportPageSettings = oReportViewer.LocalReport.GetDefaultPageSettings();
            //Warning[] warnings;
            //string[] streamids;
            //string mimeType;
            //string encoding;
            //string filenameExtension;

            //byte[] bytes = oReportViewer.LocalReport.Render(
            //    oReportObject.ExportType, null, out mimeType, out encoding, out filenameExtension,
            //    out streamids, out warnings);

            //oReportObject.Warnings = warnings;
            //oReportObject.Streamids = streamids;
            //oReportObject.ContentType = mimeType;
            //oReportObject.Encoding = encoding;
            //oReportObject.FilenameExtension = filenameExtension;
            //oReportObject.ExportFileName = string.Format("{0}.{1}", oReportObject.ExportFileName, filenameExtension);
            //FileManager.SaveFile(oReportObject.ExportFileName, bytes, true);
            #endregion

            PayslipPeriodSetting oPeriod = new PayslipPeriodSetting();
            string sPeriodSetting = oRequestParameter.InputParameter["PeriodSetting"].ToString();
            string sEmpPayslip = oRequestParameter.InputParameter["EmpPayslip"].ToString();
            string[] Parameters = sPeriodSetting.Split('|');
            oPeriod.OffCycleDate = DateTime.ParseExact(Parameters[0], "ddMMyyyy", enCL);
            oPeriod.IsOffCycle = (Parameters[1] == "1" ? true : false);
            //oPeriod.IsDraft = (Parameters[2] == "1" ? true : false);
            oPeriod.PeriodMonth = Convert.ToInt32(oPeriod.OffCycleDate.ToString("MM", enCL));
            oPeriod.PeriodYear = Convert.ToInt32(oPeriod.OffCycleDate.ToString("yyyy", enCL));

            List<PaySlipReportWageDataMerge> oResult = new List<PaySlipReportWageDataMerge>();
            oResult = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetPaySlipReportData(sEmpPayslip, oRequestParameter.CurrentEmployee.Language, oPeriod);

            //oResult = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetPaySlipReportData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.CurrentEmployee.Language, oPeriod);


            if (oResult.Count == 0)
            {
                PaySlipReportWageDataMerge oPS = new PaySlipReportWageDataMerge();
                oPS.IncomeCollection = 0;
                oPS.TaxCollection = 0;
                oPS.TotalIncome = 0;
                oPS.TotalOutcome = 0;
                oPS.Net = 0;
                oPS.ProvidentFund = 0;
                oPS.SocialSecurity = 0;
                oPS.TextNote = new DataTable();
                oPS.WageTypeDescription1 = "dummy";
                oPS.WageTypeDescription2 = "dummy";
                oResult.Add(oPS);
            }


            DataTable tblPaySlipReport = new DataTable();
            #region tblPaySlipReportWage
            tblPaySlipReport.TableName = "tblPaySlipReportWage";
            tblPaySlipReport.Columns.Add("PageNo");
            tblPaySlipReport.Columns.Add("EmployeeID");
            tblPaySlipReport.Columns.Add("asofText");
            tblPaySlipReport.Columns.Add("COMPANYNAME");
            tblPaySlipReport.Columns.Add("logoURL");
            tblPaySlipReport.Columns.Add("EmpName");
            tblPaySlipReport.Columns.Add("EmpPosition");
            tblPaySlipReport.Columns.Add("EmpOraganization");

            tblPaySlipReport.Columns.Add("TotalIncome");
            tblPaySlipReport.Columns.Add("TotalOutcome");
            tblPaySlipReport.Columns.Add("IncomeCollection");
            tblPaySlipReport.Columns.Add("TaxCollection");
            tblPaySlipReport.Columns.Add("TotalCollection");
            tblPaySlipReport.Columns.Add("Net");
            tblPaySlipReport.Columns.Add("NetFromSAP");

            tblPaySlipReport.Columns.Add("IsMisMatch");
            tblPaySlipReport.Columns.Add("strPaid");
            tblPaySlipReport.Columns.Add("ProvidentFund");
            tblPaySlipReport.Columns.Add("SocialSecurity");

            tblPaySlipReport.Columns.Add("SortNumber1");
            tblPaySlipReport.Columns.Add("WageTypeCode1");
            tblPaySlipReport.Columns.Add("WageTypeDescription1");
            tblPaySlipReport.Columns.Add("Amount1");
            tblPaySlipReport.Columns.Add("Currency1");

            tblPaySlipReport.Columns.Add("SortNumber2");
            tblPaySlipReport.Columns.Add("WageTypeCode2");
            tblPaySlipReport.Columns.Add("WageTypeDescription2");
            tblPaySlipReport.Columns.Add("Amount2");
            tblPaySlipReport.Columns.Add("Currency2");

            tblPaySlipReport.Columns.Add("Code");
            tblPaySlipReport.Columns.Add("Description");
            tblPaySlipReport.Columns.Add("Value");

            #region CommonText
            tblPaySlipReport.Columns.Add("SUBJECT");
            tblPaySlipReport.Columns.Add("MONTH");
            tblPaySlipReport.Columns.Add("COLCODE");
            tblPaySlipReport.Columns.Add("COLINCOME");
            tblPaySlipReport.Columns.Add("COLOUTCOME");
            tblPaySlipReport.Columns.Add("COLAMOUNT");
            tblPaySlipReport.Columns.Add("COLREMARK");
            tblPaySlipReport.Columns.Add("COLTOTALINCOME");
            tblPaySlipReport.Columns.Add("COLTOTALOUTCOME");
            tblPaySlipReport.Columns.Add("COLNET");
            tblPaySlipReport.Columns.Add("COLINCOMECOLLECTION");
            tblPaySlipReport.Columns.Add("COLTAXCOLLECTION");
            tblPaySlipReport.Columns.Add("COLTOTALCOLLECTION");
            tblPaySlipReport.Columns.Add("COLPROVIDENTFUND");
            tblPaySlipReport.Columns.Add("COLSOCIALSECURITY");
            tblPaySlipReport.Columns.Add("COLPAID");
            tblPaySlipReport.Columns.Add("BANKINTRO");
            tblPaySlipReport.Columns.Add("CONTACT");
            tblPaySlipReport.Columns.Add("PrintedBy");
            #endregion CommonText

            #endregion tblPaySlipReportWage


            int pageNo = 1;

            int RowCount = 0;
            foreach (PaySlipReportWageDataMerge dataWageType in oResult)
            {
                RowCount++;

                DataRow row = tblPaySlipReport.NewRow();
                row["PageNo"] = pageNo.ToString();
                string EmployeeID = sEmpPayslip;
                //string EmployeeID = oRequestParameter.Requestor.EmployeeID;

                DateTime dt = oPeriod.OffCycleDate;

                EmployeeData oEmp = new EmployeeData(EmployeeID.Trim(), dt);

                row["EmployeeID"] = oEmp.EmployeeID;
                if (RowCount == 1)
                {
                    row["asofText"] = string.Format("( as of date : {0} )", oEmp.CheckDate.ToString("dd/MM/yyyy", enCL));
                    row["COMPANYNAME"] = (oRequestParameter.CurrentEmployee.Language == "TH") ? oRequestParameter.Requestor.CompanyDetail.FullNameTH : oRequestParameter.Requestor.CompanyDetail.FullNameEN;

                    row["EmpName"] = oRequestParameter.InputParameter["Employee_id"].ToString() + " - " + oRequestParameter.InputParameter["EmployeeName"].ToString();

                    if (oRequestParameter.InputParameter["PositionName"].ToString() != "")
                    {
                        row["EmpPosition"] = oRequestParameter.InputParameter["PositionName"].ToString();
                    }
                    else
                    {
                        row["EmpPosition"] = oRequestParameter.Requestor.Position;
                    }
                    if (oRequestParameter.InputParameter["OrgUnitName"].ToString() != "")
                    {
                        row["EmpOraganization"] = oRequestParameter.InputParameter["OrgUnitName"].ToString();
                    }
                    else
                    {
                        row["EmpOraganization"] = oRequestParameter.Requestor.OrgUnitName;
                    }

                    //row["EmpPosition"] = oRequestParameter.Requestor.Position;
                    //    row["EmpOraganization"] = oRequestParameter.Requestor.OrgUnitName;

                    row["IncomeCollection"] = (oResult.Count > 0 ? oResult[0].IncomeCollection.ToString("#,0.00") : "0.00");
                    row["TaxCollection"] = (oResult.Count > 0 ? oResult[0].TaxCollection.ToString("#,0.00") : "0.00");
                    row["TotalCollection"] = string.Empty;
                    row["TotalIncome"] = (oResult.Count > 0 ? oResult[0].TotalIncome.ToString("#,0.00") : "0.00");
                    row["TotalOutcome"] = (oResult.Count > 0 ? oResult[0].TotalOutcome.ToString("#,0.00") : "0.00");
                    row["Net"] = (oResult.Count > 0 ? oResult[0].Net.ToString("#,0.00") : "0.00");
                    row["ProvidentFund"] = (oResult.Count > 0 ? oResult[0].ProvidentFund.ToString("#,0.00") : "0.00");
                    row["SocialSecurity"] = (oResult.Count > 0 ? oResult[0].SocialSecurity.ToString("#,0.00") : "0.00");
                    DataTable dtTextNote = (oResult.Count > 0 ? oResult[0].TextNote : new DataTable());
                    string strPaid = string.Empty;
                    if (dtTextNote.Rows.Count > 0)
                        strPaid = string.Format("{0} {1}", dtTextNote.Rows[0][1].ToString(), dtTextNote.Rows[1][1].ToString());
                    row["strPaid"] = strPaid;

                    row["NetFromSAP"] = "";
                    row["IsMisMatch"] = "";

                    row["SUBJECT"] = oRequestParameter.InputParameter["T_SUBJECT"].ToString();
                    row["MONTH"] = string.Format("{0} {1}", HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetCommonText("SYSTEM", oRequestParameter.CurrentEmployee.Language, "D_M" + oPeriod.PeriodMonth.ToString()), oPeriod.OffCycleDate.ToString("yyyy", enCL));

                    row["COLCODE"] = oRequestParameter.InputParameter["T_COLCODE"].ToString();
                    row["COLINCOME"] = oRequestParameter.InputParameter["T_COLINCOME"].ToString();
                    row["COLOUTCOME"] = oRequestParameter.InputParameter["T_COLOUTCOME"].ToString();
                    row["COLAMOUNT"] = oRequestParameter.InputParameter["T_COLAMOUNT"].ToString();
                    row["COLREMARK"] = oRequestParameter.InputParameter["T_COLREMARK"].ToString();
                    row["COLTOTALINCOME"] = String.Format(oRequestParameter.InputParameter["T_COLTOTALINCOME"].ToString(), oPeriod.OffCycleDate.ToString("MMM yy", enCL));
                    row["COLTOTALOUTCOME"] = String.Format(oRequestParameter.InputParameter["T_COLTOTALOUTCOME"].ToString(), oPeriod.OffCycleDate.ToString("MMM yy", enCL));
                    row["COLNET"] = oRequestParameter.InputParameter["T_COLNET"].ToString();
                    row["COLINCOMECOLLECTION"] = oRequestParameter.InputParameter["T_COLINCOMECOLLECTION"].ToString();
                    row["COLTAXCOLLECTION"] = oRequestParameter.InputParameter["T_COLTAXCOLLECTION"].ToString();
                    row["COLTOTALCOLLECTION"] = oRequestParameter.InputParameter["T_COLTOTALCOLLECTION"].ToString();
                    row["COLPROVIDENTFUND"] = oRequestParameter.InputParameter["T_COLPROVIDENTFUND"].ToString();
                    row["COLSOCIALSECURITY"] = oRequestParameter.InputParameter["T_COLSOCIALSECURITY"].ToString();
                    row["COLPAID"] = oRequestParameter.InputParameter["T_COLPAID"].ToString();
                    row["BANKINTRO"] = oRequestParameter.InputParameter["T_BANKINTRO"].ToString();
                    row["CONTACT"] = oRequestParameter.InputParameter["T_CONTACT"].ToString();
                    row["PrintedBy"] = string.Format("{0} - {1}", oRequestParameter.InputParameter["Employee_id"].ToString(), oRequestParameter.InputParameter["EmployeeName"].ToString());
                }

                if (dataWageType.WageTypeDescription1 == "dummy")
                {
                    row["SortNumber1"] = "";
                    row["WageTypeCode1"] = "";
                    row["WageTypeDescription1"] = "";
                    row["Amount1"] = "";
                    row["Currency1"] = "";
                }
                else
                {
                    row["SortNumber1"] = dataWageType.SortNumber1.ToString("000");
                    row["WageTypeCode1"] = dataWageType.WageTypeCode1;
                    row["WageTypeDescription1"] = dataWageType.WageTypeDescription1;
                    row["Amount1"] = dataWageType.Amount1.ToString("#,0.00");
                    row["Currency1"] = dataWageType.Currency1;
                }

                if (dataWageType.WageTypeDescription2 == "dummy")
                {
                    row["SortNumber2"] = "";
                    row["WageTypeCode2"] = "";
                    row["WageTypeDescription2"] = "";
                    row["Amount2"] = "";
                    row["Currency2"] = "";
                }
                else
                {
                    row["SortNumber2"] = dataWageType.SortNumber2.ToString("000");
                    row["WageTypeCode2"] = dataWageType.WageTypeCode2;
                    row["WageTypeDescription2"] = dataWageType.WageTypeDescription2;
                    row["Amount2"] = dataWageType.Amount2.ToString("#,0.00");
                    row["Currency2"] = dataWageType.Currency2;
                }

                if (string.IsNullOrEmpty(dataWageType.Value))
                {
                    row["Code"] = "";
                    row["Description"] = "";
                    row["Value"] = "";
                }
                else
                {
                    row["Code"] = dataWageType.Code;
                    row["Description"] = dataWageType.Description;
                    row["Value"] = dataWageType.Value;
                }

                tblPaySlipReport.Rows.Add(row);
            }
            while (RowCount < 23)
            {
                DataRow row = tblPaySlipReport.NewRow();
                row["EmployeeID"] = sEmpPayslip;
                //row["EmployeeID"] = oRequestParameter.Requestor.EmployeeID;
                tblPaySlipReport.Rows.Add(row);
                RowCount++;
            }
            pageNo++;

            var Employee_id = oRequestParameter.InputParameter["Employee_id"].ToString();
            var ExportType = oRequestParameter.InputParameter["ExportType"].ToString();

            string companyLogo = oRequestParameter.CurrentEmployee.CompanyCode;
            string pathImage = new Uri(System.Web.HttpContext.Current.Server.MapPath("~/Client/images/CompanyLogo/" + companyLogo + ".jpg")).AbsoluteUri;
            string pathSignature = new Uri(System.Web.HttpContext.Current.Server.MapPath("~/Client/images/signature/" + companyLogo + "-Signature.jpg")).AbsoluteUri;

            string ExportPeriod = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetCommonText("SYSTEM", oRequestParameter.CurrentEmployee.Language, "D_M" + oPeriod.PeriodMonth) + " " + oPeriod.PeriodYear;
            string SignRemark = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetCommonText("PAYSLIPREPORT", oRequestParameter.CurrentEmployee.Language, "SIGNREMARK");
            string EmpIDHeader = sEmpPayslip + " - " + oRequestParameter.InputParameter["EmployeeName"].ToString();


            //string EmpIDHeader = oRequestParameter.Requestor.EmployeeID + " - " + oRequestParameter.Requestor.Name;
            //string PositionName = oRequestParameter.Requestor.Position;
            //string OrgUnitName = oRequestParameter.Requestor.OrgUnitName;


            ReportObject oReportObject = new ReportObject();
            oReportObject.ExportType = ExportType;
            oReportObject.ReportName = System.Web.HttpContext.Current.Server.MapPath("~/Views/Report/HR/PayslipReport.rdlc");
            //oReportObject.ExportFileName = System.Web.HttpContext.Current.Server.MapPath("~/Client/Report/PaySlipReport" + Employee_id);

            oReportObject.ExportFileName = System.Web.HttpContext.Current.Server.MapPath("~/Client/Report/PaySlipReport" + Employee_id);

            oReportObject.ReportParameters = new List<ReportParameter>()
                {
                    new ReportParameter("ImageLogo",pathImage),
                    new ReportParameter("Signature",pathSignature),
                    new ReportParameter("SignatureRemark",SignRemark)
                };


            ReportViewer oReportViewer = new ReportViewer();
            oReportViewer.LocalReport.DataSources.Clear();
            oReportViewer.ProcessingMode = ProcessingMode.Local;
            oReportViewer.LocalReport.ReportPath = oReportObject.ReportName;
            oReportViewer.LocalReport.DataSources.Add(new ReportDataSource("PYDataset_Payslip", tblPaySlipReport));
            oReportViewer.LocalReport.EnableExternalImages = true;

            if (oReportObject.ReportParameters != null && oReportObject.ReportParameters.Count > 0)
            {
                oReportViewer.LocalReport.SetParameters(oReportObject.ReportParameters);
            }

            oReportViewer.ShowRefreshButton = false;
            ReportPageSettings oReportPageSettings = oReportViewer.LocalReport.GetDefaultPageSettings();
            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string filenameExtension;

            byte[] bytes = oReportViewer.LocalReport.Render(
                oReportObject.ExportType, null, out mimeType, out encoding, out filenameExtension,
                out streamids, out warnings);

            oReportObject.Warnings = warnings;
            oReportObject.Streamids = streamids;
            oReportObject.ContentType = mimeType;
            oReportObject.Encoding = encoding;
            oReportObject.FilenameExtension = filenameExtension;
            oReportObject.ExportFileName = string.Format("{0}.{1}", oReportObject.ExportFileName, filenameExtension);
            FileManager.SaveFile(oReportObject.ExportFileName, bytes, true);
        }

        [HttpPost]
        public int GetPaySlipExportForAdminExport([FromBody] RequestParameter oRequestParameter)
        {
            int count_data = 0;

            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();

            string sByOrg = oRequestParameter.InputParameter["ByOrg"].ToString();
            string sListOrg = oRequestParameter.InputParameter["ListOrg"].ToString();
            PayslipPeriodSetting oPeriod = new PayslipPeriodSetting();
            string sPeriodSetting = oRequestParameter.InputParameter["PeriodSetting"].ToString();
            string sEmpPayslip = oRequestParameter.InputParameter["EmpPayslip"].ToString();
            string[] Parameters = sPeriodSetting.Split('|');
            oPeriod.OffCycleDate = DateTime.ParseExact(Parameters[0], "ddMMyyyy", enCL);
            oPeriod.IsOffCycle = (Parameters[1] == "1" ? true : false);
            //oPeriod.IsDraft = (Parameters[2] == "1" ? true : false);
            oPeriod.PeriodMonth = Convert.ToInt32(oPeriod.OffCycleDate.ToString("MM", enCL));
            oPeriod.PeriodYear = Convert.ToInt32(oPeriod.OffCycleDate.ToString("yyyy", enCL));


            DataTable tblPaySlipReport = new DataTable();
            #region tblPaySlipReportWage
            tblPaySlipReport.TableName = "tblPaySlipReportWage";
            tblPaySlipReport.Columns.Add("PageNo");
            tblPaySlipReport.Columns.Add("EmployeeID");
            tblPaySlipReport.Columns.Add("asofText");
            tblPaySlipReport.Columns.Add("COMPANYNAME");
            tblPaySlipReport.Columns.Add("logoURL");
            tblPaySlipReport.Columns.Add("EmpName");
            tblPaySlipReport.Columns.Add("EmpPosition");
            tblPaySlipReport.Columns.Add("EmpOraganization");

            tblPaySlipReport.Columns.Add("TotalIncome");
            tblPaySlipReport.Columns.Add("TotalOutcome");
            tblPaySlipReport.Columns.Add("IncomeCollection");
            tblPaySlipReport.Columns.Add("TaxCollection");
            tblPaySlipReport.Columns.Add("TotalCollection");
            tblPaySlipReport.Columns.Add("Net");
            tblPaySlipReport.Columns.Add("NetFromSAP");

            tblPaySlipReport.Columns.Add("IsMisMatch");
            tblPaySlipReport.Columns.Add("strPaid");
            tblPaySlipReport.Columns.Add("ProvidentFund");
            tblPaySlipReport.Columns.Add("SocialSecurity");

            tblPaySlipReport.Columns.Add("SortNumber1");
            tblPaySlipReport.Columns.Add("WageTypeCode1");
            tblPaySlipReport.Columns.Add("WageTypeDescription1");
            tblPaySlipReport.Columns.Add("Amount1");
            tblPaySlipReport.Columns.Add("Currency1");

            tblPaySlipReport.Columns.Add("SortNumber2");
            tblPaySlipReport.Columns.Add("WageTypeCode2");
            tblPaySlipReport.Columns.Add("WageTypeDescription2");
            tblPaySlipReport.Columns.Add("Amount2");
            tblPaySlipReport.Columns.Add("Currency2");

            tblPaySlipReport.Columns.Add("Code");
            tblPaySlipReport.Columns.Add("Description");
            tblPaySlipReport.Columns.Add("Value");

            #region CommonText
            tblPaySlipReport.Columns.Add("SUBJECT");
            tblPaySlipReport.Columns.Add("MONTH");
            tblPaySlipReport.Columns.Add("COLCODE");
            tblPaySlipReport.Columns.Add("COLINCOME");
            tblPaySlipReport.Columns.Add("COLOUTCOME");
            tblPaySlipReport.Columns.Add("COLAMOUNT");
            tblPaySlipReport.Columns.Add("COLREMARK");
            tblPaySlipReport.Columns.Add("COLTOTALINCOME");
            tblPaySlipReport.Columns.Add("COLTOTALOUTCOME");
            tblPaySlipReport.Columns.Add("COLNET");
            tblPaySlipReport.Columns.Add("COLINCOMECOLLECTION");
            tblPaySlipReport.Columns.Add("COLTAXCOLLECTION");
            tblPaySlipReport.Columns.Add("COLTOTALCOLLECTION");
            tblPaySlipReport.Columns.Add("COLPROVIDENTFUND");
            tblPaySlipReport.Columns.Add("COLSOCIALSECURITY");
            tblPaySlipReport.Columns.Add("COLPAID");
            tblPaySlipReport.Columns.Add("BANKINTRO");
            tblPaySlipReport.Columns.Add("CONTACT");
            tblPaySlipReport.Columns.Add("PrintedBy");
            #endregion CommonText

            #endregion tblPaySlipReportWage
            int pageNo = 1;


            if (sByOrg == "2")
            {
                List<PaySlipReportWageDataMerge> oResult = new List<PaySlipReportWageDataMerge>();
                //oResult = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetPaySlipReportData(sEmpPayslip, oRequestParameter.CurrentEmployee.Language, oPeriod);
                oResult = HRPYManagement.CreateInstance(sCompanyCode).GetPaySlipReportData(sEmpPayslip, oRequestParameter.CurrentEmployee.Language, oPeriod);


                if (oResult.Count == 0)
                {
                    PaySlipReportWageDataMerge oPS = new PaySlipReportWageDataMerge();
                    oPS.IncomeCollection = 0;
                    oPS.TaxCollection = 0;
                    oPS.TotalIncome = 0;
                    oPS.TotalOutcome = 0;
                    oPS.Net = 0;
                    oPS.ProvidentFund = 0;
                    oPS.SocialSecurity = 0;
                    oPS.TextNote = new DataTable();
                    oPS.WageTypeDescription1 = "dummy";
                    oPS.WageTypeDescription2 = "dummy";
                    oResult.Add(oPS);
                }
                else
                {
                    // หากไม่มีข้อมูลจะแสดง  popup
                    count_data++;
                }





                int RowCount = 0;
                foreach (PaySlipReportWageDataMerge dataWageType in oResult)
                {
                    RowCount++;

                    DataRow row = tblPaySlipReport.NewRow();
                    row["PageNo"] = pageNo.ToString();
                    string EmployeeID = sEmpPayslip;
                    //string EmployeeID = oRequestParameter.Requestor.EmployeeID;

                    DateTime dt = oPeriod.OffCycleDate;

                    EmployeeData oEmp = new EmployeeData(EmployeeID.Trim(), dt);

                    row["EmployeeID"] = oEmp.EmployeeID;
                    if (RowCount == 1)
                    {
                        row["asofText"] = string.Format("( as of date : {0} )", oEmp.CheckDate.ToString("dd/MM/yyyy", enCL));
                        row["COMPANYNAME"] = (oRequestParameter.CurrentEmployee.Language == "TH") ? oRequestParameter.Requestor.CompanyDetail.FullNameTH : oRequestParameter.Requestor.CompanyDetail.FullNameEN;

                        row["EmpName"] = oRequestParameter.InputParameter["Employee_id"].ToString() + " - " + oRequestParameter.InputParameter["EmployeeName"].ToString();

                        if (oRequestParameter.InputParameter["PositionName"].ToString() != "")
                        {
                            row["EmpPosition"] = oRequestParameter.InputParameter["PositionName"].ToString();
                        }
                        else
                        {
                            row["EmpPosition"] = oRequestParameter.Requestor.Position;
                        }
                        if (oRequestParameter.InputParameter["OrgUnitName"].ToString() != "")
                        {
                            row["EmpOraganization"] = oRequestParameter.InputParameter["OrgUnitName"].ToString();
                        }
                        else
                        {
                            row["EmpOraganization"] = oRequestParameter.Requestor.OrgUnitName;
                        }

                        //row["EmpPosition"] = oRequestParameter.Requestor.Position;
                        //    row["EmpOraganization"] = oRequestParameter.Requestor.OrgUnitName;

                        row["IncomeCollection"] = (oResult.Count > 0 ? oResult[0].IncomeCollection.ToString("#,0.00") : "0.00");
                        row["TaxCollection"] = (oResult.Count > 0 ? oResult[0].TaxCollection.ToString("#,0.00") : "0.00");
                        row["TotalCollection"] = string.Empty;
                        row["TotalIncome"] = (oResult.Count > 0 ? oResult[0].TotalIncome.ToString("#,0.00") : "0.00");
                        row["TotalOutcome"] = (oResult.Count > 0 ? oResult[0].TotalOutcome.ToString("#,0.00") : "0.00");
                        row["Net"] = (oResult.Count > 0 ? oResult[0].Net.ToString("#,0.00") : "0.00");
                        row["ProvidentFund"] = (oResult.Count > 0 ? oResult[0].ProvidentFund.ToString("#,0.00") : "0.00");
                        row["SocialSecurity"] = (oResult.Count > 0 ? oResult[0].SocialSecurity.ToString("#,0.00") : "0.00");
                        DataTable dtTextNote = (oResult.Count > 0 ? oResult[0].TextNote : new DataTable());
                        string strPaid = string.Empty;
                        if (dtTextNote.Rows.Count > 0)
                            strPaid = string.Format("{0} {1}", dtTextNote.Rows[0][1].ToString(), dtTextNote.Rows[1][1].ToString());
                        row["strPaid"] = strPaid;

                        row["NetFromSAP"] = "";
                        row["IsMisMatch"] = "";

                        row["SUBJECT"] = oRequestParameter.InputParameter["T_SUBJECT"].ToString();
                        //row["MONTH"] = string.Format("{0} {1}", HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetCommonText("SYSTEM", oRequestParameter.CurrentEmployee.Language, "D_M" + oPeriod.PeriodMonth.ToString()), oPeriod.OffCycleDate.ToString("yyyy", enCL));
                        row["MONTH"] = string.Format("{0} {1}", HRPYManagement.CreateInstance(sCompanyCode).GetCommonText("SYSTEM", oRequestParameter.CurrentEmployee.Language, "D_M" + oPeriod.PeriodMonth.ToString()), oPeriod.OffCycleDate.ToString("yyyy", enCL));



                        row["COLCODE"] = oRequestParameter.InputParameter["T_COLCODE"].ToString();
                        row["COLINCOME"] = oRequestParameter.InputParameter["T_COLINCOME"].ToString();
                        row["COLOUTCOME"] = oRequestParameter.InputParameter["T_COLOUTCOME"].ToString();
                        row["COLAMOUNT"] = oRequestParameter.InputParameter["T_COLAMOUNT"].ToString();
                        row["COLREMARK"] = oRequestParameter.InputParameter["T_COLREMARK"].ToString();
                        row["COLTOTALINCOME"] = String.Format(oRequestParameter.InputParameter["T_COLTOTALINCOME"].ToString(), oPeriod.OffCycleDate.ToString("MMM yy", enCL));
                        row["COLTOTALOUTCOME"] = String.Format(oRequestParameter.InputParameter["T_COLTOTALOUTCOME"].ToString(), oPeriod.OffCycleDate.ToString("MMM yy", enCL));
                        row["COLNET"] = oRequestParameter.InputParameter["T_COLNET"].ToString();
                        row["COLINCOMECOLLECTION"] = oRequestParameter.InputParameter["T_COLINCOMECOLLECTION"].ToString();
                        row["COLTAXCOLLECTION"] = oRequestParameter.InputParameter["T_COLTAXCOLLECTION"].ToString();
                        row["COLTOTALCOLLECTION"] = oRequestParameter.InputParameter["T_COLTOTALCOLLECTION"].ToString();
                        row["COLPROVIDENTFUND"] = oRequestParameter.InputParameter["T_COLPROVIDENTFUND"].ToString();
                        row["COLSOCIALSECURITY"] = oRequestParameter.InputParameter["T_COLSOCIALSECURITY"].ToString();
                        row["COLPAID"] = oRequestParameter.InputParameter["T_COLPAID"].ToString();
                        row["BANKINTRO"] = oRequestParameter.InputParameter["T_BANKINTRO"].ToString();
                        row["CONTACT"] = oRequestParameter.InputParameter["T_CONTACT"].ToString();
                        row["PrintedBy"] = string.Format("{0} - {1}", oRequestParameter.InputParameter["Employee_id"].ToString(), oRequestParameter.InputParameter["EmployeeName"].ToString());
                    }

                    if (dataWageType.WageTypeDescription1 == "dummy")
                    {
                        row["SortNumber1"] = "";
                        row["WageTypeCode1"] = "";
                        row["WageTypeDescription1"] = "";
                        row["Amount1"] = "";
                        row["Currency1"] = "";
                    }
                    else
                    {
                        row["SortNumber1"] = dataWageType.SortNumber1.ToString("000");
                        row["WageTypeCode1"] = dataWageType.WageTypeCode1;
                        row["WageTypeDescription1"] = dataWageType.WageTypeDescription1;
                        row["Amount1"] = dataWageType.Amount1.ToString("#,0.00");
                        row["Currency1"] = dataWageType.Currency1;
                    }

                    if (dataWageType.WageTypeDescription2 == "dummy")
                    {
                        row["SortNumber2"] = "";
                        row["WageTypeCode2"] = "";
                        row["WageTypeDescription2"] = "";
                        row["Amount2"] = "";
                        row["Currency2"] = "";
                    }
                    else
                    {
                        row["SortNumber2"] = dataWageType.SortNumber2.ToString("000");
                        row["WageTypeCode2"] = dataWageType.WageTypeCode2;
                        row["WageTypeDescription2"] = dataWageType.WageTypeDescription2;
                        row["Amount2"] = dataWageType.Amount2.ToString("#,0.00");
                        row["Currency2"] = dataWageType.Currency2;
                    }

                    if (string.IsNullOrEmpty(dataWageType.Value))
                    {
                        row["Code"] = "";
                        row["Description"] = "";
                        row["Value"] = "";
                    }
                    else
                    {
                        row["Code"] = dataWageType.Code;
                        row["Description"] = dataWageType.Description;
                        row["Value"] = dataWageType.Value;
                    }

                    tblPaySlipReport.Rows.Add(row);
                }
                while (RowCount < 23)
                {
                    DataRow row = tblPaySlipReport.NewRow();
                    row["EmployeeID"] = sEmpPayslip;
                    //row["EmployeeID"] = oRequestParameter.Requestor.EmployeeID;
                    tblPaySlipReport.Rows.Add(row);
                    RowCount++;
                }
                pageNo++;

                var Employee_id = oRequestParameter.InputParameter["Employee_id"].ToString();
                var ExportType = oRequestParameter.InputParameter["ExportType"].ToString();

                //string companyLogo = oRequestParameter.CurrentEmployee.CompanyCode;
                string companyLogo = sCompanyCode;
                string pathImage = new Uri(System.Web.HttpContext.Current.Server.MapPath("~/Client/images/CompanyLogo/" + companyLogo + ".jpg")).AbsoluteUri;
                string pathSignature = new Uri(System.Web.HttpContext.Current.Server.MapPath("~/Client/images/signature/" + companyLogo + "-Signature.jpg")).AbsoluteUri;

                //string ExportPeriod = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetCommonText("SYSTEM", oRequestParameter.CurrentEmployee.Language, "D_M" + oPeriod.PeriodMonth) + " " + oPeriod.PeriodYear;
                //string SignRemark = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetCommonText("PAYSLIPREPORT", oRequestParameter.CurrentEmployee.Language, "SIGNREMARK");
                string ExportPeriod = HRPYManagement.CreateInstance(sCompanyCode).GetCommonText("SYSTEM", oRequestParameter.CurrentEmployee.Language, "D_M" + oPeriod.PeriodMonth) + " " + oPeriod.PeriodYear;
                string SignRemark = HRPYManagement.CreateInstance(sCompanyCode).GetCommonText("PAYSLIPREPORT", oRequestParameter.CurrentEmployee.Language, "SIGNREMARK");

                string EmpIDHeader = sEmpPayslip + " - " + oRequestParameter.InputParameter["EmployeeName"].ToString();

                ReportObject oReportObject = new ReportObject();
                oReportObject.ExportType = ExportType;
                oReportObject.ReportName = System.Web.HttpContext.Current.Server.MapPath("~/Views/Report/HR/PayslipReportForAdmin.rdlc");

                oReportObject.ExportFileName = System.Web.HttpContext.Current.Server.MapPath("~/Client/Report/PaySlipReportForAdmin" + Employee_id);

                oReportObject.ReportParameters = new List<ReportParameter>()
                {
                    new ReportParameter("ImageLogo",pathImage),
                    new ReportParameter("Signature",pathSignature),
                    new ReportParameter("SignatureRemark",SignRemark)
                };

                ReportViewer oReportViewer = new ReportViewer();
                oReportViewer.LocalReport.DataSources.Clear();
                oReportViewer.ProcessingMode = ProcessingMode.Local;
                oReportViewer.LocalReport.ReportPath = oReportObject.ReportName;
                oReportViewer.LocalReport.DataSources.Add(new ReportDataSource("PYDataset_Payslip", tblPaySlipReport));
                oReportViewer.LocalReport.EnableExternalImages = true;

                if (oReportObject.ReportParameters != null && oReportObject.ReportParameters.Count > 0)
                {
                    oReportViewer.LocalReport.SetParameters(oReportObject.ReportParameters);
                }

                oReportViewer.ShowRefreshButton = false;
                ReportPageSettings oReportPageSettings = oReportViewer.LocalReport.GetDefaultPageSettings();
                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string filenameExtension;

                byte[] bytes = oReportViewer.LocalReport.Render(
                    oReportObject.ExportType, null, out mimeType, out encoding, out filenameExtension,
                    out streamids, out warnings);

                oReportObject.Warnings = warnings;
                oReportObject.Streamids = streamids;
                oReportObject.ContentType = mimeType;
                oReportObject.Encoding = encoding;
                oReportObject.FilenameExtension = filenameExtension;
                oReportObject.ExportFileName = string.Format("{0}.{1}", oReportObject.ExportFileName, filenameExtension);
                FileManager.SaveFile(oReportObject.ExportFileName, bytes, true);
            }
            else
            {
                Dictionary<string, string> dicEmployeeID = new Dictionary<string, string>();

                // Test Fix Data
                //dicEmployeeID.Add("23560073", "23560073");
                //dicEmployeeID.Add("23560202", "23560202");
                //dicEmployeeID.Add("23560023", "23560023");
                //dicEmployeeID.Add("23560189", "23560189");

                DateTime dtBeginDate = new DateTime(oPeriod.OffCycleDate.Year, oPeriod.OffCycleDate.Month, 1);
                DateTime dtEndDate = oPeriod.OffCycleDate;

                foreach (string strOrgID in sListOrg.Split(','))
                {
                    //List<EmployeeData> EmployeeList = OMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetEmployeeByOrg(strOrgID, dtBeginDate, dtEndDate);
                    List<EmployeeData> EmployeeList = OMManagement.CreateInstance(sCompanyCode).GetEmployeeByOrg(strOrgID, dtBeginDate, dtEndDate);
                    foreach (EmployeeData oEmployee in EmployeeList)
                    {



                        //if (!ESS.EMPLOYEE.ServiceManager.ACTIVE_EMPGROUP.Contains(oEmployee.EmpGroup))
                        //    continue;
                        //List<PaySlipReportWageDataMerge> oPaySlipReportWageDataMergeList = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetPaySlipReportData(oEmployee.EmployeeID, oRequestParameter.CurrentEmployee.Language, oPeriod);
                        List<PaySlipReportWageDataMerge> oPaySlipReportWageDataMergeList = HRPYManagement.CreateInstance(sCompanyCode).GetPaySlipReportData(oEmployee.EmployeeID, oRequestParameter.CurrentEmployee.Language, oPeriod);

                        if (oPaySlipReportWageDataMergeList.Count > 0 && oPaySlipReportWageDataMergeList[0].Amount1 == 0)
                            continue;

                        //string strEmp = string.Format("{0} - {1}|{2}|{3}|{4}", oEmployee.EmployeeID, oEmployee.Name, oEmployee.PositionData(oRequestParameter.CurrentEmployee.Language).Text, oEmployee.OrgAssignment.OrgUnitData.Text, oEmployee.CheckDate.ToString("ddMMyyyy", DefaultCultureInfo));
                        string strEmp = oEmployee.EmployeeID;

                        if (!dicEmployeeID.ContainsKey(strEmp))
                            dicEmployeeID.Add(strEmp, strEmp);
                    }
                }

                foreach (KeyValuePair<string, string> entry in dicEmployeeID)
                {
                    string emp = entry.Key.ToString();


                    List<PaySlipReportWageDataMerge> oResult = new List<PaySlipReportWageDataMerge>();
                    //oResult = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetPaySlipReportData(emp, oRequestParameter.CurrentEmployee.Language, oPeriod);
                    oResult = HRPYManagement.CreateInstance(sCompanyCode).GetPaySlipReportData(emp, oRequestParameter.CurrentEmployee.Language, oPeriod);


                    if (oResult.Count > 0)
                    {
                        count_data++;

                        if (oResult.Count == 0)
                        {
                            PaySlipReportWageDataMerge oPS = new PaySlipReportWageDataMerge();
                            oPS.IncomeCollection = 0;
                            oPS.TaxCollection = 0;
                            oPS.TotalIncome = 0;
                            oPS.TotalOutcome = 0;
                            oPS.Net = 0;
                            oPS.ProvidentFund = 0;
                            oPS.SocialSecurity = 0;
                            oPS.TextNote = new DataTable();
                            oPS.WageTypeDescription1 = "dummy";
                            oPS.WageTypeDescription2 = "dummy";
                            oResult.Add(oPS);
                        }
                        int RowCount = 0;
                        foreach (PaySlipReportWageDataMerge dataWageType in oResult)
                        {
                            RowCount++;

                            DataRow row = tblPaySlipReport.NewRow();
                            row["PageNo"] = pageNo.ToString();
                            string EmployeeID = emp;

                            //DataTable oDTEmp = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetEmployeeByEmpID(emp.Trim(), DateTime.Now, oRequestParameter.CurrentEmployee.Language);
                            DataTable oDTEmp = EmployeeManagement.CreateInstance(sCompanyCode).GetEmployeeByEmpID(emp.Trim(), dtBeginDate, oRequestParameter.CurrentEmployee.Language);
                            //DataTable oDTEmp = EmployeeManagement.CreateInstance(sCompanyCode).GetEmployeeByEmpID(emp.Trim(), DateTime.Now, oRequestParameter.CurrentEmployee.Language);


                            DateTime dt = oPeriod.OffCycleDate;

                            EmployeeData oEmp = new EmployeeData(emp.Trim(), dt);

                            row["EmployeeID"] = oEmp.EmployeeID;
                            if (RowCount == 1)
                            {
                                row["asofText"] = string.Format("( as of date : {0} )", oEmp.CheckDate.ToString("dd/MM/yyyy", enCL));
                                row["COMPANYNAME"] = (oRequestParameter.CurrentEmployee.Language == "TH") ? oRequestParameter.Requestor.CompanyDetail.FullNameTH : oRequestParameter.Requestor.CompanyDetail.FullNameEN;

                                row["EmpName"] = oDTEmp.Rows[0]["EmployeeID"].ToString() + " - " + oDTEmp.Rows[0]["EmployeeName"].ToString();
                                row["EmpPosition"] = oDTEmp.Rows[0]["PositionName"].ToString();
                                row["EmpOraganization"] = oDTEmp.Rows[0]["OrgUnitName"].ToString();


                                row["IncomeCollection"] = (oResult.Count > 0 ? oResult[0].IncomeCollection.ToString("#,0.00") : "0.00");
                                row["TaxCollection"] = (oResult.Count > 0 ? oResult[0].TaxCollection.ToString("#,0.00") : "0.00");
                                row["TotalCollection"] = string.Empty;
                                row["TotalIncome"] = (oResult.Count > 0 ? oResult[0].TotalIncome.ToString("#,0.00") : "0.00");
                                row["TotalOutcome"] = (oResult.Count > 0 ? oResult[0].TotalOutcome.ToString("#,0.00") : "0.00");
                                row["Net"] = (oResult.Count > 0 ? oResult[0].Net.ToString("#,0.00") : "0.00");
                                row["ProvidentFund"] = (oResult.Count > 0 ? oResult[0].ProvidentFund.ToString("#,0.00") : "0.00");
                                row["SocialSecurity"] = (oResult.Count > 0 ? oResult[0].SocialSecurity.ToString("#,0.00") : "0.00");
                                DataTable dtTextNote = (oResult.Count > 0 ? oResult[0].TextNote : new DataTable());
                                string strPaid = string.Empty;
                                if (dtTextNote.Rows.Count > 0)
                                    strPaid = string.Format("{0} {1}", dtTextNote.Rows[0][1].ToString(), dtTextNote.Rows[1][1].ToString());
                                row["strPaid"] = strPaid;

                                row["NetFromSAP"] = "";
                                row["IsMisMatch"] = "";

                                row["SUBJECT"] = oRequestParameter.InputParameter["T_SUBJECT"].ToString();
                                //row["MONTH"] = string.Format("{0} {1}", HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetCommonText("SYSTEM", oRequestParameter.CurrentEmployee.Language, "D_M" + oPeriod.PeriodMonth.ToString()), oPeriod.OffCycleDate.ToString("yyyy", enCL));
                                row["MONTH"] = string.Format("{0} {1}", HRPYManagement.CreateInstance(sCompanyCode).GetCommonText("SYSTEM", oRequestParameter.CurrentEmployee.Language, "D_M" + oPeriod.PeriodMonth.ToString()), oPeriod.OffCycleDate.ToString("yyyy", enCL));


                                row["COLCODE"] = oRequestParameter.InputParameter["T_COLCODE"].ToString();
                                row["COLINCOME"] = oRequestParameter.InputParameter["T_COLINCOME"].ToString();
                                row["COLOUTCOME"] = oRequestParameter.InputParameter["T_COLOUTCOME"].ToString();
                                row["COLAMOUNT"] = oRequestParameter.InputParameter["T_COLAMOUNT"].ToString();
                                row["COLREMARK"] = oRequestParameter.InputParameter["T_COLREMARK"].ToString();
                                row["COLTOTALINCOME"] = String.Format(oRequestParameter.InputParameter["T_COLTOTALINCOME"].ToString(), oPeriod.OffCycleDate.ToString("MMM yy", enCL));
                                row["COLTOTALOUTCOME"] = String.Format(oRequestParameter.InputParameter["T_COLTOTALOUTCOME"].ToString(), oPeriod.OffCycleDate.ToString("MMM yy", enCL));
                                row["COLNET"] = oRequestParameter.InputParameter["T_COLNET"].ToString();
                                row["COLINCOMECOLLECTION"] = oRequestParameter.InputParameter["T_COLINCOMECOLLECTION"].ToString();
                                row["COLTAXCOLLECTION"] = oRequestParameter.InputParameter["T_COLTAXCOLLECTION"].ToString();
                                row["COLTOTALCOLLECTION"] = oRequestParameter.InputParameter["T_COLTOTALCOLLECTION"].ToString();
                                row["COLPROVIDENTFUND"] = oRequestParameter.InputParameter["T_COLPROVIDENTFUND"].ToString();
                                row["COLSOCIALSECURITY"] = oRequestParameter.InputParameter["T_COLSOCIALSECURITY"].ToString();
                                row["COLPAID"] = oRequestParameter.InputParameter["T_COLPAID"].ToString();
                                row["BANKINTRO"] = oRequestParameter.InputParameter["T_BANKINTRO"].ToString();
                                row["CONTACT"] = oRequestParameter.InputParameter["T_CONTACT"].ToString();
                                row["PrintedBy"] = string.Format("{0} - {1}", oRequestParameter.InputParameter["Employee_id"].ToString(), oRequestParameter.InputParameter["EmployeeName"].ToString());
                            }

                            if (dataWageType.WageTypeDescription1 == "dummy")
                            {
                                row["SortNumber1"] = "";
                                row["WageTypeCode1"] = "";
                                row["WageTypeDescription1"] = "";
                                row["Amount1"] = "";
                                row["Currency1"] = "";
                            }
                            else
                            {
                                row["SortNumber1"] = dataWageType.SortNumber1.ToString("000");
                                row["WageTypeCode1"] = dataWageType.WageTypeCode1;
                                row["WageTypeDescription1"] = dataWageType.WageTypeDescription1;
                                row["Amount1"] = dataWageType.Amount1.ToString("#,0.00");
                                row["Currency1"] = dataWageType.Currency1;
                            }

                            if (dataWageType.WageTypeDescription2 == "dummy")
                            {
                                row["SortNumber2"] = "";
                                row["WageTypeCode2"] = "";
                                row["WageTypeDescription2"] = "";
                                row["Amount2"] = "";
                                row["Currency2"] = "";
                            }
                            else
                            {
                                row["SortNumber2"] = dataWageType.SortNumber2.ToString("000");
                                row["WageTypeCode2"] = dataWageType.WageTypeCode2;
                                row["WageTypeDescription2"] = dataWageType.WageTypeDescription2;
                                row["Amount2"] = dataWageType.Amount2.ToString("#,0.00");
                                row["Currency2"] = dataWageType.Currency2;
                            }

                            if (string.IsNullOrEmpty(dataWageType.Value))
                            {
                                row["Code"] = "";
                                row["Description"] = "";
                                row["Value"] = "";
                            }
                            else
                            {
                                row["Code"] = dataWageType.Code;
                                row["Description"] = dataWageType.Description;
                                row["Value"] = dataWageType.Value;
                            }

                            tblPaySlipReport.Rows.Add(row);
                        }
                        while (RowCount < 23)
                        {
                            DataRow row = tblPaySlipReport.NewRow();
                            row["EmployeeID"] = emp;
                            tblPaySlipReport.Rows.Add(row);
                            RowCount++;
                        }
                        pageNo++;
                    }

                }


                var Employee_id = oRequestParameter.InputParameter["Employee_id"].ToString();
                var ExportType = oRequestParameter.InputParameter["ExportType"].ToString();

                //string companyLogo = oRequestParameter.CurrentEmployee.CompanyCode;
                string companyLogo = sCompanyCode;
                string pathImage = new Uri(System.Web.HttpContext.Current.Server.MapPath("~/Client/images/CompanyLogo/" + companyLogo + ".jpg")).AbsoluteUri;
                string pathSignature = new Uri(System.Web.HttpContext.Current.Server.MapPath("~/Client/images/signature/" + companyLogo + "-Signature.jpg")).AbsoluteUri;

                //string ExportPeriod = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetCommonText("SYSTEM", oRequestParameter.CurrentEmployee.Language, "D_M" + oPeriod.PeriodMonth) + " " + oPeriod.PeriodYear;
                //string SignRemark = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetCommonText("PAYSLIPREPORT", oRequestParameter.CurrentEmployee.Language, "SIGNREMARK");
                string ExportPeriod = HRPYManagement.CreateInstance(sCompanyCode).GetCommonText("SYSTEM", oRequestParameter.CurrentEmployee.Language, "D_M" + oPeriod.PeriodMonth) + " " + oPeriod.PeriodYear;
                string SignRemark = HRPYManagement.CreateInstance(sCompanyCode).GetCommonText("PAYSLIPREPORT", oRequestParameter.CurrentEmployee.Language, "SIGNREMARK");
                string EmpIDHeader = sEmpPayslip + " - " + oRequestParameter.InputParameter["EmployeeName"].ToString();

                ReportObject oReportObject = new ReportObject();
                oReportObject.ExportType = ExportType;
                oReportObject.ReportName = System.Web.HttpContext.Current.Server.MapPath("~/Views/Report/HR/PayslipReportForAdmin.rdlc");
                oReportObject.ExportFileName = System.Web.HttpContext.Current.Server.MapPath("~/Client/Report/PaySlipReportForAdmin" + Employee_id);

                oReportObject.ReportParameters = new List<ReportParameter>()
                {
                    new ReportParameter("ImageLogo",pathImage),
                    new ReportParameter("Signature",pathSignature),
                    new ReportParameter("SignatureRemark",SignRemark)
                };

                ReportViewer oReportViewer = new ReportViewer();
                oReportViewer.LocalReport.DataSources.Clear();
                oReportViewer.ProcessingMode = ProcessingMode.Local;
                oReportViewer.LocalReport.ReportPath = oReportObject.ReportName;
                oReportViewer.LocalReport.DataSources.Add(new ReportDataSource("PYDataset_Payslip", tblPaySlipReport));
                oReportViewer.LocalReport.EnableExternalImages = true;

                if (oReportObject.ReportParameters != null && oReportObject.ReportParameters.Count > 0)
                {
                    oReportViewer.LocalReport.SetParameters(oReportObject.ReportParameters);
                }

                oReportViewer.ShowRefreshButton = false;
                ReportPageSettings oReportPageSettings = oReportViewer.LocalReport.GetDefaultPageSettings();
                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string filenameExtension;

                byte[] bytes = oReportViewer.LocalReport.Render(
                    oReportObject.ExportType, null, out mimeType, out encoding, out filenameExtension,
                    out streamids, out warnings);

                oReportObject.Warnings = warnings;
                oReportObject.Streamids = streamids;
                oReportObject.ContentType = mimeType;
                oReportObject.Encoding = encoding;
                oReportObject.FilenameExtension = filenameExtension;
                oReportObject.ExportFileName = string.Format("{0}.{1}", oReportObject.ExportFileName, filenameExtension);
                FileManager.SaveFile(oReportObject.ExportFileName, bytes, true);
            }

            return count_data;
        }

        [HttpPost]
        public List<string> GetYearTavi50([FromBody] RequestParameter oRequestParameter)
        {
            List<string> oListYear = new List<string>();
            oListYear = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetYearTavi50(oRequestParameter.Requestor.HiringDate, oRequestParameter.CurrentEmployee.Language);
            return oListYear;
        }

        [HttpPost]
        public void SavePeriodSetting([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
                List<PayslipPeriodSetting> oPeriodSetting = JSon.Deserialize<List<PayslipPeriodSetting>>(oRequestParameter.InputParameter["DataPeriodSetting"].ToString());
                List<PayslipPeriodSetting> oPeriodSettingSort = oPeriodSetting.OrderBy(o => o.PeriodYear).ThenBy(o => o.PeriodMonth).ToList();

                //HRPYManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).SavePeriodSetting(oPeriodSettingSort);

                ConfigurationLog ConfigLog = new ConfigurationLog();
                ConfigLog.EmployeeID = oRequestParameter.Requestor.EmployeeID;
                ConfigLog.ConfigTable = "PayslipPeriodSetting";
                ConfigLog.Remark = "Filtered by PeriodYear";
                //Filter for the period same as Display

                DataSet oDs = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).ToDataSet(oPeriodSettingSort);

                DataTable tempTable = new DataTable();

                tempTable = oDs.Tables[0].Clone();
                foreach (DataRow dr in oDs.Tables[0].Rows)
                    tempTable.ImportRow(dr);
                ConfigLog.NewData = new DataSet("PayslipPeriodSetting");
                ConfigLog.LoadToNewData(tempTable);
                HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).SaveConfigurationLog(ConfigLog);

                HRPYManagement.CreateInstance(sCompanyCode).SavePeriodSetting(oPeriodSettingSort);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public List<TaxData> GetTaxDataV2([FromBody] RequestParameter oRequestParameter)
        {
            List<TaxData> TResult = new List<TaxData>();
            bool isVerifyPinCode = false;
            isVerifyPinCode = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).VerifyPinCode(oRequestParameter.Requestor.EmployeeID, oRequestParameter.InputParameter["PinCode"].ToString());

            if (isVerifyPinCode)
                TResult = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetTaxDataV2(oRequestParameter.Requestor.EmployeeID, oRequestParameter.InputParameter["Year"].ToString(), oRequestParameter.InputParameter["PinCode"].ToString());

            return TResult;
        }

        private string FormatNumber(decimal input)
        {
            if (input == 0)
            {
                return "";
            }
            decimal mod = Math.Floor(input); ;
            if (mod == 0)
            {
                return "-";
            }
            return mod.ToString("#,##0");
        }
        private string FormatDecimal(decimal input)
        {
            if (input == 0)
            {
                return "";
            }
            return ((input * 100) % 100).ToString("00");
        }
        private string NumberToText(decimal money485)
        {
            string returnText = "";
            int intValue = (int)decimal.Truncate(money485); //ได้เลขจำนวนเต็ม
            int decimalValue = (int)((money485 % 1) * 100);    //ได้เลขหลังทศนิยม
            string tempIntValue = ModNumber(intValue);
            string tempDecimalValue = "";
            tempDecimalValue = ModNumber(decimalValue);
            if (tempDecimalValue != "")
            {
                returnText = string.Format("{0}บาท {1}สตางค์", tempIntValue, tempDecimalValue);
            }
            else if (tempIntValue == "" && tempDecimalValue == "")
            {
                returnText = "ศูนย์บาทถ้วน";
            }
            else
            {
                returnText = string.Format("{0}บาทถ้วน", tempIntValue);
            }
            return returnText;
        }
        private string ModNumber(int value)
        {
            string textNumber = "";
            int valueLength = 0;
            valueLength = value.ToString().Length;
            int[] textInt = new int[valueLength];

            int tempSubValue = 0;
            int tempSubNumber = (int)value;

            for (int i = 0; i < valueLength; i++)
            {
                tempSubValue = (int)tempSubNumber % 10;
                tempSubNumber = (int)tempSubNumber / 10;
                textInt[i] = tempSubValue;
                if (i == 0) //หน่วย
                {
                    if (textInt[i] == 0)
                    {
                        temp1 = "";
                    }
                    else if (textInt[i] == 1 && valueLength > 1)
                    {
                        temp1 = "เอ็ด";
                    }
                    else
                    {
                        temp1 = Translate(textInt[i]);
                    }
                }
                if (i == 1) //สิบ
                {
                    if (textInt[i] == 0)
                    {
                        temp2 = "";
                    }
                    else if (textInt[i] == 1)
                    {
                        temp2 = "สิบ";
                    }
                    else if (textInt[i] == 2)
                    {
                        temp2 = "ยี่สิบ";
                    }
                    else
                    {
                        temp2 = string.Format("{0}สิบ", Translate(textInt[i]));
                    }
                }
                if (i == 2) //ร้อย
                {
                    if (textInt[i] == 0)
                    {
                        temp3 = "";
                    }
                    else
                    {
                        temp3 = string.Format("{0}ร้อย", Translate(textInt[i]));
                    }
                }
                if (i == 3) //พัน
                {
                    if (textInt[i] == 0)
                    {
                        temp4 = "";
                    }
                    else
                    {
                        temp4 = string.Format("{0}พัน", Translate(textInt[i]));
                    }
                }
                if (i == 4) //หมื่น
                {
                    if (textInt[i] == 0)
                    {
                        temp5 = "";
                    }
                    else
                    {
                        temp5 = string.Format("{0}หมื่น", Translate(textInt[i]));
                    }
                }
                if (i == 5) //แสน
                {
                    if (textInt[i] == 0)
                    {
                        temp6 = "";
                    }
                    else
                    {
                        temp6 = string.Format("{0}แสน", Translate(textInt[i]));
                    }
                }
                if (i == 6) //ล้าน
                {
                    if (textInt[i] == 0)
                    {
                        temp7 = "";
                    }
                    else if (textInt[i] == 1 && valueLength > 7)
                    {
                        temp7 = "เอ็ดล้าน";
                    }
                    else
                    {
                        temp7 = string.Format("{0}ล้าน", Translate(textInt[i]));
                    }
                }
                if (i == 7) //สิบล้าน
                {
                    if (textInt[i] == 1)
                    {
                        temp8 = string.Format("สิบ", Translate(textInt[i]));
                    }
                    else if (textInt[i] == 2)
                    {
                        temp8 = "ยี่สิบ";
                    }
                    else
                    {
                        temp8 = string.Format("{0}ล้าน", Translate(textInt[i]));
                    }
                }
                textNumber = ConvertToString(valueLength);
            }
            return textNumber;
        }
        private string ConvertToString(int valueLength)
        {
            string convertedString = "";
            if (valueLength == 1)   //หน่วย
            {
                convertedString = temp1;
            }
            else if (valueLength == 2)   //สิบ
            {
                convertedString = string.Format(" {0}{1}", temp2, temp1);
            }
            else if (valueLength == 3)   //ร้อย
            {
                convertedString = string.Format(" {0}{1}{2}", temp3, temp2, temp1);
            }
            else if (valueLength == 4)   //พัน
            {
                convertedString = string.Format(" {0}{1}{2}{3}", temp4, temp3, temp2, temp1);
            }
            else if (valueLength == 5)   //หมื่น
            {
                convertedString = string.Format(" {0}{1}{2}{3}{4}", temp5, temp4, temp3, temp2, temp1);
            }
            else if (valueLength == 6)   //แสน
            {
                convertedString = string.Format(" {0}{1}{2}{3}{4}{5}", temp6, temp5, temp4, temp3, temp2, temp1);
            }
            else if (valueLength == 7)   //ล้าน
            {
                convertedString = string.Format(" {0}{1}{2}{3}{4}{5}{6}", temp7, temp6, temp5, temp4, temp3, temp2, temp1);
            }
            else if (valueLength == 8)   //สิบล้าน
            {
                convertedString = string.Format(" {0}{1}{2}{3}{4}{5}{6}{7}", temp8, temp7, temp6, temp5, temp4, temp3, temp2, temp1);
            }
            return convertedString;
        }
        private string Translate(int number)
        {
            string returnString = "";
            switch (number)
            {
                case 1:
                    returnString = "หนึ่ง";
                    break;
                case 2:
                    returnString = "สอง";
                    break;
                case 3:
                    returnString = "สาม";
                    break;
                case 4:
                    returnString = "สี่";
                    break;
                case 5:
                    returnString = "ห้า";
                    break;
                case 6:
                    returnString = "หก";
                    break;
                case 7:
                    returnString = "เจ็ด";
                    break;
                case 8:
                    returnString = "แปด";
                    break;
                case 9:
                    returnString = "เก้า";
                    break;
            }
            return returnString;
        }

        [HttpPost]
        public void Get50TaviReportExport([FromBody] RequestParameter oRequestParameter)
        {
            string sYear = oRequestParameter.InputParameter["Year"].ToString();

            List<TaxData> oResult = new List<TaxData>();
            oResult = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetTaxDataV2(oRequestParameter.Requestor.EmployeeID, oRequestParameter.InputParameter["Year"].ToString(), oRequestParameter.InputParameter["PinCode"].ToString());

            DataTable tbl50TaviReport = new DataTable();
            #region tbl50TaviReport
            tbl50TaviReport.Columns.Add("PageNo");
            tbl50TaviReport.Columns.Add("Employer_TaxID");
            tbl50TaviReport.Columns.Add("Employer_Name");
            tbl50TaviReport.Columns.Add("Employer_Address");
            tbl50TaviReport.Columns.Add("Employee_TaxID");
            tbl50TaviReport.Columns.Add("Employee_Name");
            tbl50TaviReport.Columns.Add("Employee_Address");
            tbl50TaviReport.Columns.Add("Order");
            tbl50TaviReport.Columns.Add("Attach1");
            tbl50TaviReport.Columns.Add("Attach2");
            tbl50TaviReport.Columns.Add("Attach3");
            tbl50TaviReport.Columns.Add("Attach4");
            tbl50TaviReport.Columns.Add("Attach5");
            tbl50TaviReport.Columns.Add("Attach6");
            tbl50TaviReport.Columns.Add("Attach7");

            tbl50TaviReport.Columns.Add("TaxYear1");
            tbl50TaviReport.Columns.Add("TaxYear2");
            tbl50TaviReport.Columns.Add("TaxYear3");

            tbl50TaviReport.Columns.Add("PayAmount1");
            tbl50TaviReport.Columns.Add("PayFraction1");
            tbl50TaviReport.Columns.Add("TaxAmount1");
            tbl50TaviReport.Columns.Add("TaxFraction1");

            tbl50TaviReport.Columns.Add("PayAmount2");
            tbl50TaviReport.Columns.Add("PayFraction2");
            tbl50TaviReport.Columns.Add("TaxAmount2");
            tbl50TaviReport.Columns.Add("TaxFraction2");

            tbl50TaviReport.Columns.Add("PayAmount3");
            tbl50TaviReport.Columns.Add("PayFraction3");
            tbl50TaviReport.Columns.Add("TaxAmount3");
            tbl50TaviReport.Columns.Add("TaxFraction3");
            tbl50TaviReport.Columns.Add("PayReason3");

            tbl50TaviReport.Columns.Add("TotalPayAmount");
            tbl50TaviReport.Columns.Add("TotalPayFraction");
            tbl50TaviReport.Columns.Add("TotalTaxAmount");
            tbl50TaviReport.Columns.Add("TotalTaxFraction");
            tbl50TaviReport.Columns.Add("TotalTaxString");

            tbl50TaviReport.Columns.Add("TaxType");
            tbl50TaviReport.Columns.Add("PFAmount");
            tbl50TaviReport.Columns.Add("SocialAmount");
            tbl50TaviReport.Columns.Add("IssueDate");
            tbl50TaviReport.Columns.Add("Signature");

            #endregion tbl50TaviReport

            int pageNo = 0;
            foreach (TaxData taxData in oResult)
            {
                Company company = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetCompanyData(taxData.Area, taxData.SubArea);
                DateTime dtIssueDate = new DateTime(Convert.ToInt32(taxData.Year), 12, 31);
                if (dtIssueDate.Date > DateTime.Now.Date)
                    dtIssueDate = DateTime.Now;

                DataRow row = tbl50TaviReport.NewRow();
                row["PageNo"] = pageNo.ToString();
                pageNo++;
                row["Employer_TaxID"] = company.CompanyTaxID;
                row["Employer_Name"] = company.CompanyName;

                string address;
                if (company.Floor == "")
                {
                    address = string.Format("{0} เลขที่ {1} ถ.{2} {3} {4} {5} {6} ", company.Building, company.HouseNumber, company.Street, company.City, company.District, company.Province, company.PostCode);
                }
                else if (company.HouseNumber == "")
                {
                    address = string.Format("{0} ชั้น {1} ถ.{2} {3} {4} {5} {6} ", company.Building, company.Floor, company.Street, company.City, company.District, company.Province, company.PostCode);
                }
                else if (company.Floor == "" && company.HouseNumber == "")
                {
                    address = string.Format("{0} ถ.{1} {2} {3} {4} {5} ", company.Building, company.Street, company.City, company.District, company.Province, company.PostCode);
                }
                else
                {
                    address = string.Format("{0} ชั้น {1} เลขที่ {2} ถ.{3} {4} {5} {6} {7} ", company.Building, company.Floor, company.HouseNumber, company.Street, company.City, company.District, company.Province, company.PostCode);
                }
                row["Employer_Address"] = address;

                row["Employee_TaxID"] = taxData.EmployeeTaxNo.Replace("-", "");
                row["Employee_Name"] = oRequestParameter.Requestor.Name;
                row["Employee_Address"] = string.Format("{0} {1} {2} {3} {4}", taxData.EmpAddress, taxData.EmpStreet, taxData.EmpDistrict, taxData.EmpProvince, taxData.EmpPostCode);
                row["Order"] = taxData.Order;
                row["Attach1"] = "X";
                row["Attach2"] = "";
                row["Attach3"] = "";
                row["Attach4"] = "";
                row["Attach5"] = "";
                row["Attach6"] = "";
                row["Attach7"] = "";
                if (taxData.Income > 0)
                {
                    row["TaxYear1"] = taxData.Year;
                    row["PayAmount1"] = FormatNumber(taxData.Income);
                    row["PayFraction1"] = FormatDecimal(taxData.Income);
                    row["TaxAmount1"] = FormatNumber(taxData.Tax);
                    row["TaxFraction1"] = FormatDecimal(taxData.Tax);
                }

                if (taxData.Income2 > 0)
                {
                    row["TaxYear2"] = taxData.Year;
                    row["PayAmount2"] = FormatNumber(taxData.Income2);
                    row["PayFraction2"] = FormatDecimal(taxData.Income2);
                    row["TaxAmount2"] = FormatNumber(taxData.Tax2);
                    row["TaxFraction2"] = FormatDecimal(taxData.Tax2);
                }

                if (taxData.IncomeOther > 0)
                {
                    row["TaxYear3"] = taxData.Year;
                    row["PayAmount3"] = FormatNumber(taxData.IncomeOther);
                    row["PayFraction3"] = FormatDecimal(taxData.IncomeOther);
                    row["TaxAmount3"] = FormatNumber(taxData.TaxOther);
                    row["TaxFraction3"] = FormatDecimal(taxData.TaxOther);
                }

                row["PayReason3"] = "___________";
                if (taxData.Condition == "02" || taxData.Condition == "03")
                {
                    row["PayReason3"] = "บริษัทจ่ายภาษีให้";
                }
                else if (taxData.Condition == "04")
                {
                    row["PayReason3"] = "เงินได้จากการออกจากงาน";
                }

                decimal totalPay = taxData.Income + taxData.Income2 + taxData.IncomeOther;
                decimal totalTax = taxData.Tax + taxData.Tax2 + taxData.TaxOther;
                row["TotalPayAmount"] = FormatNumber(totalPay);
                row["TotalPayFraction"] = FormatDecimal(totalPay);
                row["TotalTaxAmount"] = FormatNumber(totalTax);
                row["TotalTaxFraction"] = FormatDecimal(totalTax);
                row["TotalTaxString"] = NumberToText(totalTax);

                row["TaxType"] = taxData.Condition;
                row["PFAmount"] = taxData.PFAmount.ToString("#,##0.00");
                row["SocialAmount"] = taxData.SSAmount.ToString("#,##0.00");
                row["IssueDate"] = dtIssueDate.ToString("dd MMM yyyy", enCL);
                row["Signature"] = "file:\\" + System.Web.HttpContext.Current.Server.MapPath("~/Client/images/signature/" + sYear + "/" + oRequestParameter.CurrentEmployee.CompanyCode + "_Signature_" + sYear + ".jpeg");
                tbl50TaviReport.Rows.Add(row);
            }




            var Employee_id = oRequestParameter.InputParameter["Employee_id"].ToString();
            var ExportType = oRequestParameter.InputParameter["ExportType"].ToString();

            string companyLogo = oRequestParameter.CurrentEmployee.CompanyCode;
            string pathImage = new Uri(System.Web.HttpContext.Current.Server.MapPath("~/Client/images/CompanyLogo/" + companyLogo + ".jpg")).AbsoluteUri;
            string EmpIDHeader = oRequestParameter.Requestor.EmployeeID + " - " + oRequestParameter.Requestor.Name;
            string PositionName = oRequestParameter.Requestor.Position;
            string OrgUnitName = oRequestParameter.Requestor.OrgUnitName;

            ReportObject oReportObject = new ReportObject();
            oReportObject.ExportType = ExportType;
            oReportObject.ReportName = System.Web.HttpContext.Current.Server.MapPath("~/Views/Report/HR/50TaviReport.rdlc");
            //oReportObject.ExportFileName = System.Web.HttpContext.Current.Server.MapPath("~/Client/Report/PaySlipReport" + Employee_id + "_" + DateTime.Now.ToString("ddMMyyyy"));
            oReportObject.ExportFileName = System.Web.HttpContext.Current.Server.MapPath("~/Client/Report/" + string.Format("ใบรับรองภาษีปี{0}", sYear) + "_" + Employee_id);

            ReportViewer oReportViewer = new ReportViewer();
            oReportViewer.LocalReport.DataSources.Clear();
            oReportViewer.ProcessingMode = ProcessingMode.Local;
            oReportViewer.LocalReport.ReportPath = oReportObject.ReportName;
            oReportViewer.LocalReport.DataSources.Add(new ReportDataSource("PYDataset_50Tavi", tbl50TaviReport));
            oReportViewer.LocalReport.EnableExternalImages = true;

            if (oReportObject.ReportParameters != null && oReportObject.ReportParameters.Count > 0)
            {
                oReportViewer.LocalReport.SetParameters(oReportObject.ReportParameters);
            }

            oReportViewer.ShowRefreshButton = false;
            ReportPageSettings oReportPageSettings = oReportViewer.LocalReport.GetDefaultPageSettings();
            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string filenameExtension;

            byte[] bytes = oReportViewer.LocalReport.Render(
                oReportObject.ExportType, null, out mimeType, out encoding, out filenameExtension,
                out streamids, out warnings);

            oReportObject.Warnings = warnings;
            oReportObject.Streamids = streamids;
            oReportObject.ContentType = mimeType;
            oReportObject.Encoding = encoding;
            oReportObject.FilenameExtension = filenameExtension;
            oReportObject.ExportFileName = string.Format("{0}.{1}", oReportObject.ExportFileName, filenameExtension);
            FileManager.SaveFile(oReportObject.ExportFileName, bytes, true);
        }

        [HttpPost]
        public int Get50TaviReportForAdminExport([FromBody] RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();

            int count_document = 0;

            string sByOrg = oRequestParameter.InputParameter["ByOrg"].ToString();
            string sListOrg = oRequestParameter.InputParameter["ListOrg"].ToString();
            string sEmp50Tavi = oRequestParameter.InputParameter["Emp50Tavi"].ToString();
            string sYear = oRequestParameter.InputParameter["Year"].ToString();


            DataTable tbl50TaviReport = new DataTable();
            #region tbl50TaviReport
            tbl50TaviReport.Columns.Add("PageNo");
            tbl50TaviReport.Columns.Add("Employer_TaxID");
            tbl50TaviReport.Columns.Add("Employer_Name");
            tbl50TaviReport.Columns.Add("Employer_Address");
            tbl50TaviReport.Columns.Add("Employee_TaxID");
            tbl50TaviReport.Columns.Add("Employee_Name");
            tbl50TaviReport.Columns.Add("Employee_Address");
            tbl50TaviReport.Columns.Add("Order");
            tbl50TaviReport.Columns.Add("Attach1");
            tbl50TaviReport.Columns.Add("Attach2");
            tbl50TaviReport.Columns.Add("Attach3");
            tbl50TaviReport.Columns.Add("Attach4");
            tbl50TaviReport.Columns.Add("Attach5");
            tbl50TaviReport.Columns.Add("Attach6");
            tbl50TaviReport.Columns.Add("Attach7");

            tbl50TaviReport.Columns.Add("TaxYear1");
            tbl50TaviReport.Columns.Add("TaxYear2");
            tbl50TaviReport.Columns.Add("TaxYear3");

            tbl50TaviReport.Columns.Add("PayAmount1");
            tbl50TaviReport.Columns.Add("PayFraction1");
            tbl50TaviReport.Columns.Add("TaxAmount1");
            tbl50TaviReport.Columns.Add("TaxFraction1");

            tbl50TaviReport.Columns.Add("PayAmount2");
            tbl50TaviReport.Columns.Add("PayFraction2");
            tbl50TaviReport.Columns.Add("TaxAmount2");
            tbl50TaviReport.Columns.Add("TaxFraction2");

            tbl50TaviReport.Columns.Add("PayAmount3");
            tbl50TaviReport.Columns.Add("PayFraction3");
            tbl50TaviReport.Columns.Add("TaxAmount3");
            tbl50TaviReport.Columns.Add("TaxFraction3");
            tbl50TaviReport.Columns.Add("PayReason3");

            tbl50TaviReport.Columns.Add("TotalPayAmount");
            tbl50TaviReport.Columns.Add("TotalPayFraction");
            tbl50TaviReport.Columns.Add("TotalTaxAmount");
            tbl50TaviReport.Columns.Add("TotalTaxFraction");
            tbl50TaviReport.Columns.Add("TotalTaxString");

            tbl50TaviReport.Columns.Add("TaxType");
            tbl50TaviReport.Columns.Add("PFAmount");
            tbl50TaviReport.Columns.Add("SocialAmount");
            tbl50TaviReport.Columns.Add("IssueDate");
            tbl50TaviReport.Columns.Add("Signature");

            #endregion tbl50TaviReport
            int pageNo = 0;

            if (sByOrg == "2")
            {
                List<TaxData> oResult = new List<TaxData>();
                //oResult = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetTaxDataV2(sEmp50Tavi, oRequestParameter.InputParameter["Year"].ToString(), oRequestParameter.InputParameter["PinCode"].ToString());
                oResult = HRPYManagement.CreateInstance(sCompanyCode).GetTaxDataV2(sEmp50Tavi, oRequestParameter.InputParameter["Year"].ToString(), oRequestParameter.InputParameter["PinCode"].ToString());


                foreach (TaxData taxData in oResult)
                {
                    count_document++;

                    //Company company = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetCompanyData(taxData.Area, taxData.SubArea);
                    Company company = HRPYManagement.CreateInstance(sCompanyCode).GetCompanyData(taxData.Area, taxData.SubArea);

                    DateTime dtIssueDate = new DateTime(Convert.ToInt32(taxData.Year), 12, 31);
                    if (dtIssueDate.Date > DateTime.Now.Date)
                        dtIssueDate = DateTime.Now;

                    DataRow row = tbl50TaviReport.NewRow();
                    row["PageNo"] = pageNo.ToString();
                    pageNo++;
                    row["Employer_TaxID"] = company.CompanyTaxID;
                    row["Employer_Name"] = company.CompanyName;

                    string address;
                    if (company.Floor == "")
                    {
                        address = string.Format("{0} เลขที่ {1} ถ.{2} {3} {4} {5} {6} ", company.Building, company.HouseNumber, company.Street, company.City, company.District, company.Province, company.PostCode);
                    }
                    else if (company.HouseNumber == "")
                    {
                        address = string.Format("{0} ชั้น {1} ถ.{2} {3} {4} {5} {6} ", company.Building, company.Floor, company.Street, company.City, company.District, company.Province, company.PostCode);
                    }
                    else if (company.Floor == "" && company.HouseNumber == "")
                    {
                        address = string.Format("{0} ถ.{1} {2} {3} {4} {5} ", company.Building, company.Street, company.City, company.District, company.Province, company.PostCode);
                    }
                    else
                    {
                        address = string.Format("{0} ชั้น {1} เลขที่ {2} ถ.{3} {4} {5} {6} {7} ", company.Building, company.Floor, company.HouseNumber, company.Street, company.City, company.District, company.Province, company.PostCode);
                    }
                    row["Employer_Address"] = address;

                    row["Employee_TaxID"] = taxData.EmployeeTaxNo.Replace("-", "");
                    row["Employee_Name"] = oRequestParameter.InputParameter["EmployeeName"].ToString();
                    row["Employee_Address"] = string.Format("{0} {1} {2} {3} {4}", taxData.EmpAddress, taxData.EmpStreet, taxData.EmpDistrict, taxData.EmpProvince, taxData.EmpPostCode);
                    row["Order"] = taxData.Order;
                    row["Attach1"] = "X";
                    row["Attach2"] = "";
                    row["Attach3"] = "";
                    row["Attach4"] = "";
                    row["Attach5"] = "";
                    row["Attach6"] = "";
                    row["Attach7"] = "";
                    if (taxData.Income > 0)
                    {
                        row["TaxYear1"] = taxData.Year;
                        row["PayAmount1"] = FormatNumber(taxData.Income);
                        row["PayFraction1"] = FormatDecimal(taxData.Income);
                        row["TaxAmount1"] = FormatNumber(taxData.Tax);
                        row["TaxFraction1"] = FormatDecimal(taxData.Tax);
                    }

                    if (taxData.Income2 > 0)
                    {
                        row["TaxYear2"] = taxData.Year;
                        row["PayAmount2"] = FormatNumber(taxData.Income2);
                        row["PayFraction2"] = FormatDecimal(taxData.Income2);
                        row["TaxAmount2"] = FormatNumber(taxData.Tax2);
                        row["TaxFraction2"] = FormatDecimal(taxData.Tax2);
                    }

                    if (taxData.IncomeOther > 0)
                    {
                        row["TaxYear3"] = taxData.Year;
                        row["PayAmount3"] = FormatNumber(taxData.IncomeOther);
                        row["PayFraction3"] = FormatDecimal(taxData.IncomeOther);
                        row["TaxAmount3"] = FormatNumber(taxData.TaxOther);
                        row["TaxFraction3"] = FormatDecimal(taxData.TaxOther);
                    }

                    row["PayReason3"] = "___________";
                    if (taxData.Condition == "02" || taxData.Condition == "03")
                    {
                        row["PayReason3"] = "บริษัทจ่ายภาษีให้";
                    }
                    else if (taxData.Condition == "04")
                    {
                        row["PayReason3"] = "เงินได้จากการออกจากงาน";
                    }

                    decimal totalPay = taxData.Income + taxData.Income2 + taxData.IncomeOther;
                    decimal totalTax = taxData.Tax + taxData.Tax2 + taxData.TaxOther;
                    row["TotalPayAmount"] = FormatNumber(totalPay);
                    row["TotalPayFraction"] = FormatDecimal(totalPay);
                    row["TotalTaxAmount"] = FormatNumber(totalTax);
                    row["TotalTaxFraction"] = FormatDecimal(totalTax);
                    row["TotalTaxString"] = NumberToText(totalTax);

                    row["TaxType"] = taxData.Condition;
                    row["PFAmount"] = taxData.PFAmount.ToString("#,##0.00");
                    row["SocialAmount"] = taxData.SSAmount.ToString("#,##0.00");
                    row["IssueDate"] = dtIssueDate.ToString("dd MMM yyyy", enCL);
                    //row["Signature"] = "file:\\" + System.Web.HttpContext.Current.Server.MapPath("~/Client/images/signature/" + company.SignatureLogo);
                    row["Signature"] = "file:\\" + System.Web.HttpContext.Current.Server.MapPath("~/Client/images/signature/" + sYear + "/" + sCompanyCode + "_Signature_" + sYear + ".jpeg");

                    tbl50TaviReport.Rows.Add(row);
                }

                var Employee_id = oRequestParameter.InputParameter["Employee_id"].ToString();
                var ExportType = oRequestParameter.InputParameter["ExportType"].ToString();

                //string companyLogo = oRequestParameter.CurrentEmployee.CompanyCode;
                string companyLogo = sCompanyCode;

                string pathImage = new Uri(System.Web.HttpContext.Current.Server.MapPath("~/Client/images/CompanyLogo/" + companyLogo + ".jpg")).AbsoluteUri;

                ReportObject oReportObject = new ReportObject();
                oReportObject.ExportType = ExportType;
                oReportObject.ReportName = System.Web.HttpContext.Current.Server.MapPath("~/Views/Report/HR/50TaviReport.rdlc");
                oReportObject.ExportFileName = System.Web.HttpContext.Current.Server.MapPath("~/Client/Report/" + string.Format("ใบรับรองภาษีปี{0}", sYear) + "_ForAdmin" + Employee_id);

                ReportViewer oReportViewer = new ReportViewer();
                oReportViewer.LocalReport.DataSources.Clear();
                oReportViewer.ProcessingMode = ProcessingMode.Local;
                oReportViewer.LocalReport.ReportPath = oReportObject.ReportName;
                oReportViewer.LocalReport.DataSources.Add(new ReportDataSource("PYDataset_50Tavi", tbl50TaviReport));
                oReportViewer.LocalReport.EnableExternalImages = true;

                if (oReportObject.ReportParameters != null && oReportObject.ReportParameters.Count > 0)
                {
                    oReportViewer.LocalReport.SetParameters(oReportObject.ReportParameters);
                }

                oReportViewer.ShowRefreshButton = false;
                ReportPageSettings oReportPageSettings = oReportViewer.LocalReport.GetDefaultPageSettings();
                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string filenameExtension;

                byte[] bytes = oReportViewer.LocalReport.Render(
                    oReportObject.ExportType, null, out mimeType, out encoding, out filenameExtension,
                    out streamids, out warnings);

                oReportObject.Warnings = warnings;
                oReportObject.Streamids = streamids;
                oReportObject.ContentType = mimeType;
                oReportObject.Encoding = encoding;
                oReportObject.FilenameExtension = filenameExtension;
                oReportObject.ExportFileName = string.Format("{0}.{1}", oReportObject.ExportFileName, filenameExtension);
                FileManager.SaveFile(oReportObject.ExportFileName, bytes, true);
            }
            else
            {

                Dictionary<string, string> dicEmployeeID = new Dictionary<string, string>();

                //dicEmployeeID.Add("23560073", "23560073");
                //dicEmployeeID.Add("23560023", "23560023");

                DateTime oCheckDate = DateTime.ParseExact(string.Format("1/1/{0}", sYear), "d/M/yyyy", enCL);
                DateTime dtEndDate = DateTime.ParseExact(string.Format("31/12/{0}", sYear), "d/M/yyyy", enCL);


                foreach (string strOrgID in sListOrg.Split(','))
                {
                    //List<EmployeeData> EmployeeList = OMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetEmployeeByOrg(strOrgID, oCheckDate, dtEndDate);
                    List<EmployeeData> EmployeeList = OMManagement.CreateInstance(sCompanyCode).GetEmployeeByOrg(strOrgID, oCheckDate, dtEndDate);


                    foreach (EmployeeData oEmployee in EmployeeList)
                    {
                        string strEmp = oEmployee.EmployeeID;

                        if (!dicEmployeeID.ContainsKey(strEmp))
                            dicEmployeeID.Add(strEmp, strEmp);
                    }
                }

                foreach (KeyValuePair<string, string> entry in dicEmployeeID)
                {
                    string emp = entry.Key.ToString();

                    List<TaxData> oResult = new List<TaxData>();
                    //oResult = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetTaxDataV2(emp, oRequestParameter.InputParameter["Year"].ToString(), oRequestParameter.InputParameter["PinCode"].ToString());
                    oResult = HRPYManagement.CreateInstance(sCompanyCode).GetTaxDataV2(emp, oRequestParameter.InputParameter["Year"].ToString(), oRequestParameter.InputParameter["PinCode"].ToString());


                    //DataTable oDTEmp = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetEmployeeByEmpID(emp.Trim(), DateTime.Now, oRequestParameter.CurrentEmployee.Language);
                    DataTable oDTEmp = EmployeeManagement.CreateInstance(sCompanyCode).GetEmployeeByEmpID(emp.Trim(), DateTime.Now, oRequestParameter.CurrentEmployee.Language);


                    foreach (TaxData taxData in oResult)
                    {
                        count_document++;

                        //Company company = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetCompanyData(taxData.Area, taxData.SubArea);
                        Company company = HRPYManagement.CreateInstance(sCompanyCode).GetCompanyData(taxData.Area, taxData.SubArea);

                        DateTime dtIssueDate = new DateTime(Convert.ToInt32(taxData.Year), 12, 31);
                        if (dtIssueDate.Date > DateTime.Now.Date)
                            dtIssueDate = DateTime.Now;

                        DataRow row = tbl50TaviReport.NewRow();
                        row["PageNo"] = pageNo.ToString();
                        pageNo++;
                        row["Employer_TaxID"] = company.CompanyTaxID;
                        row["Employer_Name"] = company.CompanyName;

                        string address;
                        if (company.Floor == "")
                        {
                            address = string.Format("{0} เลขที่ {1} ถ.{2} {3} {4} {5} {6} ", company.Building, company.HouseNumber, company.Street, company.City, company.District, company.Province, company.PostCode);
                        }
                        else if (company.HouseNumber == "")
                        {
                            address = string.Format("{0} ชั้น {1} ถ.{2} {3} {4} {5} {6} ", company.Building, company.Floor, company.Street, company.City, company.District, company.Province, company.PostCode);
                        }
                        else if (company.Floor == "" && company.HouseNumber == "")
                        {
                            address = string.Format("{0} ถ.{1} {2} {3} {4} {5} ", company.Building, company.Street, company.City, company.District, company.Province, company.PostCode);
                        }
                        else
                        {
                            address = string.Format("{0} ชั้น {1} เลขที่ {2} ถ.{3} {4} {5} {6} {7} ", company.Building, company.Floor, company.HouseNumber, company.Street, company.City, company.District, company.Province, company.PostCode);
                        }
                        row["Employer_Address"] = address;

                        row["Employee_TaxID"] = taxData.EmployeeTaxNo.Replace("-", "");
                        row["Employee_Name"] = oDTEmp.Rows[0]["EmployeeName"].ToString();
                        //row["Employee_Name"] = oRequestParameter.InputParameter["EmployeeName"].ToString();
                        row["Employee_Address"] = string.Format("{0} {1} {2} {3}", taxData.EmpAddress, taxData.EmpStreet, taxData.EmpProvince, taxData.EmpPostCode);
                        row["Order"] = taxData.Order;
                        row["Attach1"] = "X";
                        row["Attach2"] = "";
                        row["Attach3"] = "";
                        row["Attach4"] = "";
                        row["Attach5"] = "";
                        row["Attach6"] = "";
                        row["Attach7"] = "";
                        if (taxData.Income > 0)
                        {
                            row["TaxYear1"] = taxData.Year;
                            row["PayAmount1"] = FormatNumber(taxData.Income);
                            row["PayFraction1"] = FormatDecimal(taxData.Income);
                            row["TaxAmount1"] = FormatNumber(taxData.Tax);
                            row["TaxFraction1"] = FormatDecimal(taxData.Tax);
                        }

                        if (taxData.Income2 > 0)
                        {
                            row["TaxYear2"] = taxData.Year;
                            row["PayAmount2"] = FormatNumber(taxData.Income2);
                            row["PayFraction2"] = FormatDecimal(taxData.Income2);
                            row["TaxAmount2"] = FormatNumber(taxData.Tax2);
                            row["TaxFraction2"] = FormatDecimal(taxData.Tax2);
                        }

                        if (taxData.IncomeOther > 0)
                        {
                            row["TaxYear3"] = taxData.Year;
                            row["PayAmount3"] = FormatNumber(taxData.IncomeOther);
                            row["PayFraction3"] = FormatDecimal(taxData.IncomeOther);
                            row["TaxAmount3"] = FormatNumber(taxData.TaxOther);
                            row["TaxFraction3"] = FormatDecimal(taxData.TaxOther);
                        }

                        row["PayReason3"] = "___________";
                        if (taxData.Condition == "02" || taxData.Condition == "03")
                        {
                            row["PayReason3"] = "บริษัทจ่ายภาษีให้";
                        }
                        else if (taxData.Condition == "04")
                        {
                            row["PayReason3"] = "เงินได้จากการออกจากงาน";
                        }

                        decimal totalPay = taxData.Income + taxData.Income2 + taxData.IncomeOther;
                        decimal totalTax = taxData.Tax + taxData.Tax2 + taxData.TaxOther;
                        row["TotalPayAmount"] = FormatNumber(totalPay);
                        row["TotalPayFraction"] = FormatDecimal(totalPay);
                        row["TotalTaxAmount"] = FormatNumber(totalTax);
                        row["TotalTaxFraction"] = FormatDecimal(totalTax);
                        row["TotalTaxString"] = NumberToText(totalTax);

                        row["TaxType"] = taxData.Condition;
                        row["PFAmount"] = taxData.PFAmount.ToString("#,##0.00");
                        row["SocialAmount"] = taxData.SSAmount.ToString("#,##0.00");
                        row["IssueDate"] = dtIssueDate.ToString("dd MMM yyyy", enCL);
                        //row["Signature"] = "file:\\" + System.Web.HttpContext.Current.Server.MapPath("~/Client/images/signature/" + company.SignatureLogo);
                        row["Signature"] = "file:\\" + System.Web.HttpContext.Current.Server.MapPath("~/Client/images/signature/" + sYear + "/" + "Signature_" + sYear + ".jpeg");

                        tbl50TaviReport.Rows.Add(row);
                    }
                }

                var Employee_id = oRequestParameter.InputParameter["Employee_id"].ToString();
                var ExportType = oRequestParameter.InputParameter["ExportType"].ToString();

                //string companyLogo = oRequestParameter.CurrentEmployee.CompanyCode;
                string companyLogo = sCompanyCode;
                string pathImage = new Uri(System.Web.HttpContext.Current.Server.MapPath("~/Client/images/CompanyLogo/" + companyLogo + ".jpg")).AbsoluteUri;

                ReportObject oReportObject = new ReportObject();
                oReportObject.ExportType = ExportType;
                oReportObject.ReportName = System.Web.HttpContext.Current.Server.MapPath("~/Views/Report/HR/50TaviReport.rdlc");
                oReportObject.ExportFileName = System.Web.HttpContext.Current.Server.MapPath("~/Client/Report/" + string.Format("ใบรับรองภาษีปี{0}", sYear) + "_ForAdmin" + Employee_id);

                ReportViewer oReportViewer = new ReportViewer();
                oReportViewer.LocalReport.DataSources.Clear();
                oReportViewer.ProcessingMode = ProcessingMode.Local;
                oReportViewer.LocalReport.ReportPath = oReportObject.ReportName;
                oReportViewer.LocalReport.DataSources.Add(new ReportDataSource("PYDataset_50Tavi", tbl50TaviReport));
                oReportViewer.LocalReport.EnableExternalImages = true;

                if (oReportObject.ReportParameters != null && oReportObject.ReportParameters.Count > 0)
                {
                    oReportViewer.LocalReport.SetParameters(oReportObject.ReportParameters);
                }

                oReportViewer.ShowRefreshButton = false;
                ReportPageSettings oReportPageSettings = oReportViewer.LocalReport.GetDefaultPageSettings();
                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string filenameExtension;

                byte[] bytes = oReportViewer.LocalReport.Render(
                    oReportObject.ExportType, null, out mimeType, out encoding, out filenameExtension,
                    out streamids, out warnings);

                oReportObject.Warnings = warnings;
                oReportObject.Streamids = streamids;
                oReportObject.ContentType = mimeType;
                oReportObject.Encoding = encoding;
                oReportObject.FilenameExtension = filenameExtension;
                oReportObject.ExportFileName = string.Format("{0}.{1}", oReportObject.ExportFileName, filenameExtension);
                FileManager.SaveFile(oReportObject.ExportFileName, bytes, true);

            }

            return count_document;
        }

        [HttpPost]
        public List<Tavi50PeriodSetting> GetTavi50PeriodSetting([FromBody] RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            List<Tavi50PeriodSetting> oListTavi50PeriodSetting = new List<Tavi50PeriodSetting>();
            //oListTavi50PeriodSetting = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetAllTavi50Period(System.Web.HttpContext.Current.Server.MapPath("~/Client/images/signature/"));
            oListTavi50PeriodSetting = HRPYManagement.CreateInstance(sCompanyCode).GetAllTavi50Period(System.Web.HttpContext.Current.Server.MapPath("~/Client/images/signature/"));
            return oListTavi50PeriodSetting;
        }

        [HttpPost]
        public DataTable GetYearTavi50PeriodSetting([FromBody] RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            List<string> oListYear = new List<string>();
            //oListYear = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetYearTavi50PeriodSetting();
            oListYear = HRPYManagement.CreateInstance(sCompanyCode).GetYearTavi50PeriodSetting();

            DataTable oDT = new DataTable();
            oDT.Columns.Add("YearID", typeof(System.Int32));
            oDT.Columns.Add("YearName", typeof(System.Int32));

            if (oListYear != null && oListYear.Count > 0)
            {
                foreach (string sYear in oListYear)
                {
                    DataRow dRow = oDT.NewRow();
                    dRow["YearID"] = sYear;
                    dRow["YearName"] = sYear;
                    oDT.Rows.Add(dRow);
                }
            }
            return oDT;
        }

        [HttpPost]
        public void SaveTavi50PeriodSetting([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
                List<Tavi50PeriodSetting> oPeriodSetting = JSon.Deserialize<List<Tavi50PeriodSetting>>(oRequestParameter.InputParameter["DataPeriodSetting"].ToString());
                //HRPYManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).SaveTavi50PeriodSetting(oPeriodSetting, System.Web.HttpContext.Current.Server.MapPath("~/Client/images/signature/"));

                ConfigurationLog ConfigLog = new ConfigurationLog();
                ConfigLog.EmployeeID = oRequestParameter.Requestor.EmployeeID;
                ConfigLog.ConfigTable = "Tavi50PeriodSetting";
                ConfigLog.Remark = "Filtered by PeriodYear";
                //Filter for the period same as Display

                DataSet oDs = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).ToDataSet(oPeriodSetting);

                DataTable tempTable = new DataTable();

                tempTable = oDs.Tables[0].Clone();
                foreach (DataRow dr in oDs.Tables[0].Rows)
                    tempTable.ImportRow(dr);
                ConfigLog.NewData = new DataSet("Tavi50PeriodSetting");
                ConfigLog.LoadToNewData(tempTable);
                HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).SaveConfigurationLog(ConfigLog);

                HRPYManagement.CreateInstance(sCompanyCode).SaveTavi50PeriodSetting(oPeriodSetting, System.Web.HttpContext.Current.Server.MapPath("~/Client/images/signature/"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public DataTable GetYearPayrollReportAdmin([FromBody] RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            DateTime DateSpecific = DateTime.MinValue;
            DataTable oDT = new DataTable();
            oDT.Columns.Add("YearID", typeof(System.String));
            oDT.Columns.Add("YearName", typeof(System.String));
            bool isByEmp = false;

            if (oRequestParameter.InputParameter["ByOrganization"].ToString() == "1")
            {
                isByEmp = false;
            }
            else
            {
                isByEmp = true;
            }

            if (isByEmp) { DateSpecific = oRequestParameter.Requestor.HiringDate; }
            List<string> oListYear = new List<string>();
            //oListYear = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetPaySlipYear(DateSpecific);
            oListYear = HRPYManagement.CreateInstance(sCompanyCode).GetPaySlipYear(DateSpecific);
            foreach (string sYear in oListYear)
            {
                DataRow dRow = oDT.NewRow();
                dRow["YearID"] = sYear;
                dRow["YearName"] = sYear;
                oDT.Rows.Add(dRow);
            }
            if (oDT.Rows.Count == 0)
            {
                DataRow dRow = oDT.NewRow();
                dRow["YearID"] = "0";
                //dRow["YearName"] = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetCommonText("PAYROLLREPORTFORADMIN", oRequestParameter.CurrentEmployee.Language, "NOPAYSLIPYEAR");
                dRow["YearName"] = HRPYManagement.CreateInstance(sCompanyCode).GetCommonText("PAYROLLREPORTFORADMIN", oRequestParameter.CurrentEmployee.Language, "NOPAYSLIPYEAR");
                oDT.Rows.Add(dRow);
            }
            return oDT;
        }

        [HttpPost]
        public DataTable GetYearPayrollReportAdminByEmpNoSelected([FromBody] RequestParameter oRequestParameter)
        {
            DataTable oDT = new DataTable();
            oDT.Columns.Add("YearID", typeof(System.String));
            oDT.Columns.Add("YearName", typeof(System.String));

            DataRow dRow = oDT.NewRow();
            dRow["YearID"] = string.Empty;
            dRow["YearName"] = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetCommonText("PAYROLLREPORTFORADMIN", oRequestParameter.CurrentEmployee.Language, "NOPERIOD");
            oDT.Rows.Add(dRow);

            return oDT;
        }

        [HttpPost]
        public DataTable GetPeriodPayrollReportAdmin([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();

                List<PayslipPeriodSetting> oResult = new List<PayslipPeriodSetting>();
                //oResult = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetPaySlipMonthByYearForAdmin(Convert.ToInt32(oRequestParameter.InputParameter["Year"].ToString()), oRequestParameter.Requestor.HiringDate);
                oResult = HRPYManagement.CreateInstance(sCompanyCode).GetPaySlipMonthByYearForAdmin(Convert.ToInt32(oRequestParameter.InputParameter["Year"].ToString()), oRequestParameter.Requestor.HiringDate);


                string strValue = string.Empty;
                List<PaySlipReportWageDataMerge> oPaySlipReportWageDataMerge;
                List<string> oListMonth = new List<string>();

                bool isByEmp = false;
                bool isByOrg = false;
                if (oRequestParameter.InputParameter["ByOrganization"].ToString() == "1")
                {
                    isByOrg = true;
                    isByEmp = false;
                }
                else
                {
                    isByOrg = false;
                    isByEmp = true;
                }

                DataTable oDT = new DataTable();
                oDT.Columns.Add("MONTH_KEY", typeof(System.String));
                oDT.Columns.Add("MONTH_VALUE", typeof(System.String));
                oDT.Columns.Add("MONTH_NAME", typeof(System.String));

                if (oResult.Count > 0)
                {
                    foreach (PayslipPeriodSetting item in oResult)
                    {
                        if (item.IsOffCycle && isByEmp)
                        {
                            //oPaySlipReportWageDataMerge = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetPaySlipReportData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.CurrentEmployee.Language, item);
                            oPaySlipReportWageDataMerge = HRPYManagement.CreateInstance(sCompanyCode).GetPaySlipReportData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.CurrentEmployee.Language, item);
                            if (oPaySlipReportWageDataMerge.Count > 0 && oPaySlipReportWageDataMerge[0].Amount1 > 0)
                            {
                                //strValue = string.Format("{0}|{1}|{2}", item.OffCycleDate.ToString("ddMMyyyy", enCL), (item.IsOffCycle ? "1" : "0"), (item.IsDraft ? "1" : "0")); remove is draft by zkathawut
                                strValue = string.Format("{0}|{1}", item.OffCycleDate.ToString("ddMMyyyy", enCL), (item.IsOffCycle ? "1" : "0"));
                                oListMonth.Add(string.Format("{0}|{1}", item.OffCycleDate.Month, strValue));
                                DataRow dRow = oDT.NewRow();
                                dRow["MONTH_KEY"] = item.OffCycleDate.Month;
                                dRow["MONTH_VALUE"] = strValue;
                                dRow["MONTH_NAME"] = "";
                                oDT.Rows.Add(dRow);
                            }
                        }
                        else if (item.IsOffCycle && isByOrg)
                        {
                            //strValue = string.Format("{0}|{1}|{2}", item.OffCycleDate.ToString("ddMMyyyy", enCL), (item.IsOffCycle ? "1" : "0"), (item.IsDraft ? "1" : "0")); remove is draft by zkathawut
                            strValue = string.Format("{0}|{1}", item.OffCycleDate.ToString("ddMMyyyy", enCL), (item.IsOffCycle ? "1" : "0"));
                            oListMonth.Add(string.Format("{0}|{1}", item.OffCycleDate.Month, strValue));
                            DataRow dRow = oDT.NewRow();
                            dRow["MONTH_KEY"] = item.OffCycleDate.Month;
                            dRow["MONTH_VALUE"] = strValue;
                            dRow["MONTH_NAME"] = "";
                            oDT.Rows.Add(dRow);
                        }
                        else
                        {
                            item.OffCycleDate = DateTime.ParseExact(item.PeriodYear.ToString() + item.PeriodMonth.ToString("00") + "01", "yyyyMMdd", enCL);
                            item.OffCycleDate = item.OffCycleDate.AddMonths(1).AddSeconds(-1);
                            //strValue = string.Format("{0}|{1}|{2}", item.OffCycleDate.ToString("ddMMyyyy", enCL), (item.IsOffCycle ? "1" : "0"), (item.IsDraft ? "1" : "0")); remove is draft by zkathawut
                            strValue = string.Format("{0}|{1}", item.OffCycleDate.ToString("ddMMyyyy", enCL), (item.IsOffCycle ? "1" : "0"));
                            oListMonth.Add(string.Format("{0}|{1}", item.OffCycleDate.Month, strValue));
                            DataRow dRow = oDT.NewRow();
                            dRow["MONTH_KEY"] = item.OffCycleDate.Month;
                            dRow["MONTH_VALUE"] = strValue;
                            dRow["MONTH_NAME"] = "";
                            oDT.Rows.Add(dRow);
                        }
                    }
                }
                return oDT;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public DataTable GetReportType([FromBody] RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();

            DataTable oDT = new DataTable();
            oDT.Columns.Add("ReportTypeText", typeof(System.String));
            oDT.Columns.Add("ReportTypeValue", typeof(System.String));

            DataRow dRow = oDT.NewRow();
            //dRow["ReportTypeText"] = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetCommonText("PAYROLLREPORTFORADMIN", oRequestParameter.CurrentEmployee.Language, "PAYSLIPREPORT");
            dRow["ReportTypeText"] = HRPYManagement.CreateInstance(sCompanyCode).GetCommonText("PAYROLLREPORTFORADMIN", oRequestParameter.CurrentEmployee.Language, "PAYSLIPREPORT");
            dRow["ReportTypeValue"] = "PAYSLIP";
            oDT.Rows.Add(dRow);
            DataRow dRow2 = oDT.NewRow();
            //dRow2["ReportTypeText"] = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetCommonText("PAYROLLREPORTFORADMIN", oRequestParameter.CurrentEmployee.Language, "TAXREPORT"); ;
            dRow2["ReportTypeText"] = HRPYManagement.CreateInstance(sCompanyCode).GetCommonText("PAYROLLREPORTFORADMIN", oRequestParameter.CurrentEmployee.Language, "TAXREPORT"); ;
            dRow2["ReportTypeValue"] = "TAX";
            oDT.Rows.Add(dRow2);
            return oDT;
        }

        [HttpPost]
        //public List<string> GetOrganization([FromBody] RequestParameter oRequestParameter)
        public List<OrganizationWithLevel> GetOrganization([FromBody] RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);
            Int32 SubjectID = Convert.ToInt32(oRequestParameter.InputParameter["SubjectID"]);


            //List<UserRoleResponseSetting> lstUserRoleResponseSetting = ApplicationSubject.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).LoadUserResponse(SubjectID);
            //List<string> OrgLists = new List<string>();
            //List<OrganizationWithLevel> lstOM = new List<OrganizationWithLevel>();
            //foreach (UserRoleResponseSetting role in lstUserRoleResponseSetting)
            //   OrgLists.AddRange(HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetResponseCodeByOrganizationOnly(role.UserRole, oEmp.EmployeeID));

            List<string> OrgLists = new List<string>();
            List<OrganizationWithLevel> lstOM = new List<OrganizationWithLevel>();
            //string rootOrg = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).ROOT_ORGANIZATION;
            string rootOrg = HRPYManagement.CreateInstance(sCompanyCode).ROOT_ORGANIZATION;
            OrgLists.Add(rootOrg);
            foreach (string org in OrgLists)
            {
                //lstOM = OMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetAllOrganizationWithLevel(org, oRequestParameter.CurrentEmployee.Language);
                lstOM = OMManagement.CreateInstance(sCompanyCode).GetAllOrganizationWithLevel(org, oRequestParameter.CurrentEmployee.Language);
                break;
            }
            return lstOM;

        }

        [HttpPost]
        public List<OrganizationWithLevel> GetOrganizationForReport([FromBody] RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);
            Int32 SubjectID = Convert.ToInt32(oRequestParameter.InputParameter["SubjectID"]);


            List<string> OrgLists = new List<string>();
            List<OrganizationWithLevel> lstOM = new List<OrganizationWithLevel>();
            //string rootOrg = HRPYManagement.CreateInstance(sCompanyCode).ROOT_ORGANIZATION;
            //OrgLists.Add(rootOrg);

            string rootOrg = HRPYManagement.CreateInstance(sCompanyCode).GetRootOrgForReport(oEmp.EmployeeID,DateTime.Now);
            if (string.IsNullOrEmpty(rootOrg))
            {
                rootOrg = HRPYManagement.CreateInstance(sCompanyCode).ROOT_ORGANIZATION;
            }
            OrgLists.Add(rootOrg);

            foreach (string org in OrgLists)
            {
                lstOM = OMManagement.CreateInstance(sCompanyCode).GetAllOrganizationWithLevel(org, oRequestParameter.CurrentEmployee.Language);
                break;
            }
            return lstOM;

        }

        [HttpPost]
        public DataTable GetPeriodLetter([FromBody] RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            DataTable oDT = new DataTable();
            //oDT = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetPeriodLetter(oRequestParameter.CurrentEmployee.Language);
            oDT = HRPYManagement.CreateInstance(sCompanyCode).GetPeriodLetter(oRequestParameter.CurrentEmployee.Language);
            return oDT;
        }

        [HttpPost]
        public DataTable GetPeriodLetterByEmployee([FromBody] RequestParameter oRequestParameter)
        {
            DataTable oDT = new DataTable();
            oDT = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetPeriodLetter(oRequestParameter.CurrentEmployee.Language);
            return oDT;
        }

        [HttpPost]
        public List<Letter> GetLetterListByPeriod([FromBody] RequestParameter oRequestParameter)
        {
            List<Letter> oListLetter = new List<Letter>();
            string selectedPeriod = oRequestParameter.InputParameter["Period"].ToString();
            int iMonth = int.Parse(selectedPeriod.Substring(4, 2));
            int iYear = int.Parse(selectedPeriod.Substring(0, 4));
            DateTime dtBeginDate, dtEndDate;
            dtBeginDate = new DateTime(iYear, iMonth, 1);
            dtEndDate = dtBeginDate.AddMonths(1).AddSeconds(-1);
            oListLetter = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetLetter(oRequestParameter.Requestor.EmployeeID, dtBeginDate, dtEndDate);
            return oListLetter;
        }

        [HttpPost]
        public object GetLetterSelectData(RequestParameter oRequestParameter)
        {
            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);

            string LanguageCode = oRequestParameter.CurrentEmployee.Language;
            List<string> oLetterTypeList = new List<string>();
            Dictionary<int, LetterType> LetterTypeDict = HRPYManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetLetterType(oEmp.Secondment.SecondmentType);
            if (oEmp.OrgAssignment.EmpGroup == "B")
            {
                if (LetterTypeDict.ContainsKey(9))
                    LetterTypeDict.Remove(9);

                if (LetterTypeDict.ContainsKey(10))
                    LetterTypeDict.Remove(10);
            }
            foreach (LetterType item in LetterTypeDict.Values)
            {
                oLetterTypeList.Add(item.LetterTypeID.ToString());
            }

            DataTable oDT = new DataTable();
            oDT.Columns.Add("EmbassyID", typeof(System.String));
            oDT.Columns.Add("EmbassyName", typeof(System.String));
            DataRow dRow = oDT.NewRow();
            dRow["EmbassyID"] = "0";
            dRow["EmbassyName"] = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetCommonText("HRLE", oRequestParameter.CurrentEmployee.Language, "SELECT");
            oDT.Rows.Add(dRow);

            List<LetterEmbassy> LetterEmbassyList = HRPYManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetLetterEmbassy();
            LetterEmbassyList.Sort(new LetterEmbassySorting());

            foreach (LetterEmbassy item in LetterEmbassyList)
            {
                DataRow dr = oDT.NewRow();
                dr["EmbassyID"] = item.EmbassyID.ToString();
                dr["EmbassyName"] = item.EmbassyName;
                oDT.Rows.Add(dr);
            }

            object oReturn = new
            {
                LetterType = oLetterTypeList,
                LetterEmb = oDT
            };
            return oReturn;
        }

        [HttpPost]
        public DataTable GetSignByManager(RequestParameter oRequestParameter)
        {
            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);

            DataTable oDT = new DataTable();
            string sLetterTypeID = oRequestParameter.InputParameter["LetterTypeID"].ToString();
            oDT = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetSignByManager(oEmp, sLetterTypeID, oRequestParameter.CurrentEmployee.Language);
            return oDT;
        }

        [HttpPost]
        public bool GetEnabledVacation(RequestParameter oRequestParameter)
        {
            bool isEnabledVacation = false;
            string sLetterTypeID = oRequestParameter.InputParameter["LetterTypeID"].ToString();
            LetterType oLetterType = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetSignPositionByLetterType(Convert.ToInt32(sLetterTypeID));
            if (oLetterType != null)
            {
                isEnabledVacation = oLetterType.EnabledVacation;
            }
            return isEnabledVacation;
        }

        [HttpPost]
        public string GetSignName(RequestParameter oRequestParameter)
        {
            string signName = string.Empty;
            string sEmpID = oRequestParameter.InputParameter["EmpID"].ToString();
            DateTime dt = Convert.ToDateTime(oRequestParameter.InputParameter["CreatedDate"]);
            EmployeeData oEmp = new EmployeeData(sEmpID, dt);
            //signName = string.Format("{0} - {1} {2}", oEmp.EmployeeID, oEmp.AlternativeName(oRequestParameter.CurrentEmployee.Language), oEmp.OrgAssignment.PositionData.AlternativeName(oRequestParameter.CurrentEmployee.Language));
            signName = string.Format("{0} {1}", oEmp.AlternativeName(oRequestParameter.CurrentEmployee.Language), oEmp.OrgAssignment.PositionData.AlternativeName(oRequestParameter.CurrentEmployee.Language));


            return signName;
        }

        [HttpPost]
        public RequestDocument GetLetterDocument(RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            RequestDocument doc;
            string reqNo = oRequestParameter.InputParameter["RequestNo"].ToString();
            EmployeeData emp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);
            //doc = RequestDocument.LoadDocumentReadOnly(reqNo, oRequestParameter.Requestor.CompanyCode, emp, true, false);
            doc = RequestDocument.LoadDocumentReadOnly(reqNo, sCompanyCode, emp, true, false);
            return doc;
        }

        public void SetAuthenticate(EmployeeDataForMobile oCurrentEmployee)
        {
            WorkflowIdentity iden;
            if (oCurrentEmployee.IsExternalUser)
                iden = WorkflowIdentity.CreateInstance(oCurrentEmployee.CompanyCode).GetIdentityForExternalUser(oCurrentEmployee.EmployeeID, oCurrentEmployee.Name);
            else
            {
                EmployeeData oEmployeeData = Convert<EmployeeData>.ObjectFrom<EmployeeDataForMobile>(oCurrentEmployee);
                iden = WorkflowIdentity.CreateInstance(oCurrentEmployee.CompanyCode).GetIdentityWithoutPassword(oEmployeeData);
            }
            WorkflowPrinciple Principle = new WorkflowPrinciple(iden);
            WorkflowPrinciple.Current = Principle;
        }

        [HttpPost]
        public bool GetEnabledLetterAdminPrint(RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            string docStatus = oRequestParameter.InputParameter["DocumentStatus"].ToString();

            bool isEnabledPrint = false;
            if (WorkflowPrinciple.Current.UserSetting.Employee.IsInRole("LETTERADMIN") && docStatus == "WAITFORVERIFY")
            {
                isEnabledPrint = true;
            }
            return isEnabledPrint;
        }

        [HttpPost]
        public void UpdateLetterAdminPrint(RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            string sEmployeeID = oRequestParameter.InputParameter["EmployeeID"].ToString();
            string sRequestNo = oRequestParameter.InputParameter["RequestNo"].ToString();

            if (WorkflowPrinciple.Current.UserSetting.Employee.IsInRole("LETTERADMIN"))
            {
                DateTime BeginDate = new DateTime(DateTime.Now.Year, 1, 1);
                DateTime EndDate = new DateTime(DateTime.Now.Year, 12, 31);
                HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).UpdateLetterNo(sEmployeeID, sRequestNo, BeginDate, EndDate, true);
            }
        }

        [HttpPost]
        public void GetLetterExport([FromBody] RequestParameter oRequestParameter)
        {
            //string strSessKey = Request["SessKey"];
            //string Pathexport = System.Configuration.ConfigurationManager.AppSettings["PathExport"];
            string FilePath = System.Web.HttpContext.Current.Server.MapPath("~/Client/Report/Letter.txt");
            string FileContent = "File Name" + System.Web.HttpContext.Current.Server.MapPath("~/Views/Report/HR/PayslipReport.rdlc");
            File.WriteAllText(FilePath, FileContent);

            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            LetterReport oReport = new LetterReport();
            Letter oLetter;

            string ItemKey = oRequestParameter.InputParameter["ItemKey"].ToString();


            string[] arrItemKey = ItemKey.Split('|');
            string sEmployeeID = arrItemKey[0].Trim();
            string sRequestNo = arrItemKey[1].Trim();


            HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).InsertLetterLog(sEmployeeID, sRequestNo);

            oLetter = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetLetter(sRequestNo);

            //if (String.IsNullOrEmpty(strSessKey))
            //    oLetter = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetLetter(Request["RequestNo"]);
            //else
            //    oLetter = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetLetter(Session[strSessKey].ToString());

            LetterType oLetterType = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetLetterTypeByID(oLetter.LetterTypeID);
            EmployeeData oRequestor = new EmployeeData(oLetter.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, oLetter.UpdateTime);
            EmployeeData oSignPerson = null;
            if (string.IsNullOrEmpty(oLetter.SignID))
            {
                oSignPerson = OMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetEmployeeByPositionID(oLetterType.SignPosition, oLetter.UpdateTime);
                //oSignPerson = EmployeeData.GetEmployeeByPositionID(oLetterType.SignPosition, oLetter.UpdateTime);
            }
            else
            {
                oSignPerson = new EmployeeData(oLetter.SignID, oLetter.CreatedDate);
            }
            CultureInfo oCulInfo = new CultureInfo(oLetterType.ReportLanguageCode);
            string strLangCode = oLetterType.ReportLanguageCode.Substring(0, 2).ToUpper();

            oReport.EmployeeID = oLetter.EmployeeID.Substring(2);
            oReport.RequestorName = oRequestor.AlternativeName(strLangCode);
            oReport.RequestorPosition = oRequestor.PositionData(strLangCode).AlternativeName(strLangCode);
            oReport.OrgLevelManager = GetOrganizationType(oRequestor.CurrentOrganization.ObjectID, OrgLevelType.Manager, strLangCode, oRequestParameter.CurrentEmployee.CompanyCode);
            oReport.OrgLevelVP = GetOrganizationType(oRequestor.CurrentOrganization.ObjectID, OrgLevelType.VP, strLangCode, oRequestParameter.CurrentEmployee.CompanyCode);
            oReport.OrgLevelSVP = GetOrganizationType(oRequestor.CurrentOrganization.ObjectID, OrgLevelType.SVP, strLangCode, oRequestParameter.CurrentEmployee.CompanyCode);
            oReport.OrgLevelEVP = GetOrganizationType(oRequestor.CurrentOrganization.ObjectID, OrgLevelType.EVP, strLangCode, oRequestParameter.CurrentEmployee.CompanyCode);

            if (oSignPerson != null)
            {
                oReport.SignName = oSignPerson.AlternativeName(strLangCode);
                oReport.SignPosition = oSignPerson.PositionData(strLangCode).AlternativeName(strLangCode);
            }
            oReport.IssueDate = DateConvertToString(oLetter.UpdateTime, oCulInfo, strLangCode);

            string StartWorkAge = string.Empty;
            List<DateTime> ListStartWorkAge = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetStartWorkAge(oRequestParameter.Requestor.EmployeeID);
            if (ListStartWorkAge != null && ListStartWorkAge.Count > 0)
            {
                oReport.HiringDate = DateConvertToString(Convert.ToDateTime(ListStartWorkAge[0]), oCulInfo, strLangCode);
            }
            else
            {
                oReport.HiringDate = DateConvertToString(oRequestor.DateSpecific.HiringDate, oCulInfo, strLangCode);
            }

            //oReport.HiringDate = DateConvertToString(oRequestor.DateSpecific.HiringDate, oCulInfo, strLangCode);
            oReport.Gender = ((INFOTYPE0002)oRequestor.PersonalData).Gender.ToCharArray()[0] == '1' ? 'M' : 'F';

            if (oLetterType.EnabledVacation)
            {
                List<DateTime> list_begindate = new List<DateTime>();
                List<DateTime> list_enddate = new List<DateTime>();

                // ประเทศที่ 1
                LetterEmbassy oLetterEmbassy = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetLetterEmbassyByID(oLetter.EmbassyID);
                oReport.Embassy = oLetterEmbassy.EmbassyName;
                oReport.EmbassyAddress = oLetterEmbassy.Address;
                oReport.BeginDate = oLetter.VacationBegin.ToString("d MMMM yyyy", oCulInfo);
                oReport.EndDate = oLetter.VacationEnd.ToString("d MMMM yyyy", oCulInfo);
                // oReport.DuringDate = DuringDate(oLetter.VacationBegin, oLetter.VacationEnd, oCulInfo, strLangCode);

                list_begindate.Add(oLetter.VacationBegin);
                list_enddate.Add(oLetter.VacationEnd);

                //ประเทศที่ 2
                if (oLetter.EmbassyID2 > 0)
                {
                    LetterEmbassy oLetterEmbassy2 = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetLetterEmbassyByID(oLetter.EmbassyID2);
                    oReport.Embassy += " and " + oLetterEmbassy2.EmbassyName;
                    oReport.EmbassyAddress += " and " + oLetterEmbassy2.Address;
                    list_begindate.Add(oLetter.VacationBegin2);
                    list_enddate.Add(oLetter.VacationEnd2);
                }

                //ประเทศที่ 3
                if (oLetter.EmbassyID3 > 0)
                {
                    LetterEmbassy oLetterEmbassy3 = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetLetterEmbassyByID(oLetter.EmbassyID3);
                    oReport.Embassy += " and " + oLetterEmbassy3.EmbassyName;
                    oReport.EmbassyAddress += " and " + oLetterEmbassy3.Address;
                    list_begindate.Add(oLetter.VacationBegin3);
                    list_enddate.Add(oLetter.VacationEnd3);
                }

                // หาวันที่เริ่มและสิ้นสุด (แก้ไขให้รองรับเดินทางไปหลายประเทศ)
                DateTime vacationBegin = list_begindate.OrderBy(s => s.Date).FirstOrDefault();
                DateTime vacationEnd = list_enddate.OrderByDescending(s => s.Date).FirstOrDefault();
                oReport.DuringDate = DuringDate(vacationBegin, vacationEnd, oCulInfo, strLangCode);
            }
            DateTime start, end;
            start = oLetter.UpdateTime;
            end = oRequestor.DateSpecific.RetirementDate;
            oReport.DiffissueYear = string.Format("{0}", (end.Year - start.Year - 1) + (((end.Month > start.Month) || ((end.Month == start.Month) && (end.Day >= start.Day))) ? 1 : 0));
            oReport.DiffissueMonth = string.Format("{0}", ((end.Month >= start.Month) && (end.Day >= start.Day)) ? end.Month - start.Month
                                                                                           : (11 + end.Month) + ((end.Day >= start.Day) ? 1 : 0));

            List<ReportParameter> ReportParam = new List<ReportParameter>();
            string strExtFile = string.Empty;

            oReport.LetterRunningNo = oLetter.UpdateTime.ToString(oLetterType.LetterNoFormat.Replace("[LETTERNO]", oLetter.LetterNo.ToString("000")), oCulInfo);

            bool isPrintByEmployee = oLetter.EmployeeID == WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID;
            if (WorkflowPrinciple.Current.UserSetting.Employee.IsInRole("LETTERADMIN") || isPrintByEmployee)
            {
                //oReport.LetterRunningNo = oLetter.UpdateTime.ToString(oLetterType.LetterNoFormat.Replace("[LETTERNO]", oLetter.LetterNo.ToString("000")), oCulInfo);
                DateTime payChkDate = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetPreviousEffectiveDate();
                if (oLetter.UpdateTime < payChkDate)
                    payChkDate = oLetter.UpdateTime;
                oReport.Salary = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetSalary(oLetter.EmployeeID, payChkDate).Salary.ToString("#,##0.00");
                strExtFile = isPrintByEmployee ? "PDF" : "EXCEL";

                if (!String.IsNullOrEmpty(oLetterType.RecurringWageType))
                {
                    Dictionary<string, INFOTYPE0014> oWageDict = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetActiveWageRecuringData(oLetter.EmployeeID, oLetter.UpdateTime, oLetterType.RecurringWageType);
                    oReport.RecurringWageData = oLetterType.RecurringWageType.Replace(',', '|');
                    string strDisplayWageData = string.Empty;
                    string strRecurringWageType = string.Empty;
                    foreach (string strWageType in oLetterType.RecurringWageType.Split(','))
                    {
                        if (oWageDict.ContainsKey(strWageType))
                        {
                            strDisplayWageData += oWageDict[strWageType].Amount.ToString("#,##0.00");
                            strDisplayWageData += "|";
                            strRecurringWageType += strWageType;
                            strRecurringWageType += "|";
                        }
                    }
                    oReport.RecurringWageData = strDisplayWageData;
                    ReportParam.Add(new ReportParameter("RecurringWageType", strRecurringWageType));
                }
                ReportParam.Add(new ReportParameter("Confidential", CacheManager.GetCommonText("LETTERPRINT", strLangCode, "CONFIDENTIAL")));
            }
            else
            {
                oReport.Salary = "XX,XXX";
                ReportParam.Add(new ReportParameter("Example", CacheManager.GetCommonText("LETTERPRINT", strLangCode, "EXAMPLE")));
                strExtFile = "EXCEL";
            }
            ReportParam.Add(new ReportParameter("Tab", "\t"));
            //ReportParam.Add(new ReportParameter("PathImg", ""));

            string imagePath = new Uri(HttpContext.Current.Server.MapPath("~/Client/images/Letter/" + oRequestParameter.Requestor.CompanyCode + "/Header_Company.png")).AbsoluteUri;
            ReportParam.Add(new ReportParameter("ImagePath", imagePath));

            ESS.SHAREDATASERVICE.DATACLASS.Company oCompany = ShareDataManagement.GetCompanyByCompanyCode(oRequestParameter.Requestor.CompanyCode);

            ReportParam.Add(new ReportParameter("CompanyNameLongTH", oCompany.FullNameTH));
            ReportParam.Add(new ReportParameter("CompanyNameLongEN", oCompany.FullNameEN));
            ReportParam.Add(new ReportParameter("CompanyNameShort", oCompany.Name));

            DataSet ds = new DataSet("DS_LetterReport");
            DataTable oTable = new DataTable("LetterReport");
            oTable = oReport.ToADODataTable();
            ds.Tables.Add(oTable);

            ReportObject oReportObject = new ReportObject();
            oReportObject.ExportFileName = System.Web.HttpContext.Current.Server.MapPath("~/Client/Report/LetterReport" + oLetter.EmployeeID);

            ReportViewer rptViewer = new ReportViewer();
            rptViewer.LocalReport.EnableExternalImages = true;
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("DS_LetterReport", ds.Tables[0]));
            rptViewer.LocalReport.ReportPath = System.Web.HttpContext.Current.Server.MapPath("~/Views/Report/HR/LE/" + (isPrintByEmployee ? "PDF_version/" : "") + oLetterType.ReportTemplate);
            rptViewer.LocalReport.SetParameters(ReportParam);
            rptViewer.ShowRefreshButton = false;
            

            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string filenameExtension;

            byte[] bytes = rptViewer.LocalReport.Render(
                strExtFile, null, out mimeType, out encoding, out filenameExtension,
                out streamids, out warnings);

            oReportObject.Warnings = warnings;
            oReportObject.Streamids = streamids;
            oReportObject.ContentType = mimeType;
            oReportObject.Encoding = encoding;
            oReportObject.FilenameExtension = filenameExtension;
            oReportObject.ExportFileName = string.Format("{0}.{1}", oReportObject.ExportFileName, filenameExtension);
            FileManager.SaveFile(oReportObject.ExportFileName, bytes, true);
        }

        [HttpPost]
        public void GetLetterExportForAdmin([FromBody] RequestParameter oRequestParameter)
        {

            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);


            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            LetterReport oReport = new LetterReport();
            Letter oLetter;

            string ItemKey = oRequestParameter.InputParameter["ItemKey"].ToString();
            string TypeDoc = oRequestParameter.InputParameter["TypeDoc"].ToString();

            string[] arrItemKey = ItemKey.Split('|');
            string sEmployeeID = arrItemKey[0].Trim();
            string sRequestNo = arrItemKey[1].Trim();

            //HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).InsertLetterLog(sEmployeeID, sRequestNo);
            HRPYManagement.CreateInstance(sCompanyCode).InsertLetterLog(sEmployeeID, sRequestNo);


            //oLetter = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetLetter(sRequestNo);
            oLetter = HRPYManagement.CreateInstance(sCompanyCode).GetLetter(sRequestNo);



            //if (String.IsNullOrEmpty(strSessKey))
            //    oLetter = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetLetter(Request["RequestNo"]);
            //else
            //    oLetter = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetLetter(Session[strSessKey].ToString());


            //LetterType oLetterType = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetLetterTypeByID(oLetter.LetterTypeID);
            LetterType oLetterType = HRPYManagement.CreateInstance(sCompanyCode).GetLetterTypeByID(oLetter.LetterTypeID);


            //EmployeeData oRequestor = new EmployeeData(oLetter.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, oLetter.UpdateTime);
            EmployeeData oRequestor = new EmployeeData(oLetter.EmployeeID, oLetter.UpdateTime);
            EmployeeData oSignPerson = null;
            if (string.IsNullOrEmpty(oLetter.SignID))
            {
                //oSignPerson = OMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetEmployeeByPositionID(oLetterType.SignPosition, oLetter.UpdateTime);
                oSignPerson = OMManagement.CreateInstance(sCompanyCode).GetEmployeeByPositionID(oLetterType.SignPosition, oLetter.UpdateTime);
            }
            else
            {
                oSignPerson = new EmployeeData(oLetter.SignID, oLetter.CreatedDate);
            }
            CultureInfo oCulInfo = new CultureInfo(oLetterType.ReportLanguageCode);
            string strLangCode = oLetterType.ReportLanguageCode.Substring(0, 2).ToUpper();

            oReport.EmployeeID = oLetter.EmployeeID.Substring(2);
            oReport.RequestorName = oRequestor.AlternativeName(strLangCode);
            oReport.RequestorPosition = oRequestor.PositionData(strLangCode).AlternativeName(strLangCode);

            string tmpOrgLevelManager = string.Empty;
            string tmpOrgLevelVP = string.Empty;
            string tmpOrgLevelSVP = string.Empty;
            string tmpOrgLevelEVP = string.Empty;
            try
            {
                //tmpOrgLevelManager = GetOrganizationType(oRequestor.CurrentOrganization.ObjectID, OrgLevelType.Manager, strLangCode, oRequestParameter.CurrentEmployee.CompanyCode);
                tmpOrgLevelManager = GetOrganizationType(oRequestor.CurrentOrganization.ObjectID, OrgLevelType.Manager, strLangCode, sCompanyCode);
            }
            catch (Exception ex) { }
            finally { oReport.OrgLevelManager = tmpOrgLevelManager; }
            try
            {
                //tmpOrgLevelVP = GetOrganizationType(oRequestor.CurrentOrganization.ObjectID, OrgLevelType.VP, strLangCode, oRequestParameter.CurrentEmployee.CompanyCode);
                tmpOrgLevelVP = GetOrganizationType(oRequestor.CurrentOrganization.ObjectID, OrgLevelType.VP, strLangCode, sCompanyCode);
            }
            catch (Exception ex) { }
            finally { oReport.OrgLevelVP = tmpOrgLevelVP; }
            try
            {
                //tmpOrgLevelSVP = GetOrganizationType(oRequestor.CurrentOrganization.ObjectID, OrgLevelType.SVP, strLangCode, oRequestParameter.CurrentEmployee.CompanyCode);
                tmpOrgLevelSVP = GetOrganizationType(oRequestor.CurrentOrganization.ObjectID, OrgLevelType.SVP, strLangCode, sCompanyCode);
            }
            catch (Exception ex) { }
            finally { oReport.OrgLevelSVP = tmpOrgLevelSVP; }
            try
            {
                //tmpOrgLevelEVP = GetOrganizationType(oRequestor.CurrentOrganization.ObjectID, OrgLevelType.EVP, strLangCode, oRequestParameter.CurrentEmployee.CompanyCode);
                tmpOrgLevelEVP = GetOrganizationType(oRequestor.CurrentOrganization.ObjectID, OrgLevelType.EVP, strLangCode, sCompanyCode);
            }
            catch (Exception ex) { }
            finally { oReport.OrgLevelEVP = tmpOrgLevelEVP; }

            if (oSignPerson != null)
            {
                oReport.SignName = oSignPerson.AlternativeName(strLangCode);
                oReport.SignPosition = oSignPerson.PositionData(strLangCode).AlternativeName(strLangCode);
            }
            oReport.IssueDate = DateConvertToString(oLetter.UpdateTime, oCulInfo, strLangCode);

            string StartWorkAge = string.Empty;
            //List<DateTime> ListStartWorkAge = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetStartWorkAge(oRequestParameter.Requestor.EmployeeID);
            List<DateTime> ListStartWorkAge = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetStartWorkAge(oLetter.EmployeeID);
            if (ListStartWorkAge != null && ListStartWorkAge.Count > 0)
            {
                oReport.HiringDate = DateConvertToString(Convert.ToDateTime(ListStartWorkAge[0]), oCulInfo, strLangCode);
            }
            else
            {
                oReport.HiringDate = DateConvertToString(oRequestor.DateSpecific.HiringDate, oCulInfo, strLangCode);
            }

            //oReport.HiringDate = DateConvertToString(oRequestor.DateSpecific.HiringDate, oCulInfo, strLangCode);
            oReport.Gender = ((INFOTYPE0002)oRequestor.PersonalData).Gender.ToCharArray()[0] == '1' ? 'M' : 'F';

            if (oLetterType.EnabledVacation)
            {
                //LetterEmbassy oLetterEmbassy = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetLetterEmbassyByID(oLetter.EmbassyID);
                LetterEmbassy oLetterEmbassy = HRPYManagement.CreateInstance(sCompanyCode).GetLetterEmbassyByID(oLetter.EmbassyID);
                oReport.Embassy = oLetterEmbassy.EmbassyName;
                oReport.EmbassyAddress = oLetterEmbassy.Address;
                oReport.BeginDate = oLetter.VacationBegin.ToString("d MMMM yyyy", oCulInfo);
                oReport.EndDate = oLetter.VacationEnd.ToString("d MMMM yyyy", oCulInfo);
                oReport.DuringDate = DuringDate(oLetter.VacationBegin, oLetter.VacationEnd, oCulInfo, strLangCode);
            }
            DateTime start, end;
            start = oLetter.UpdateTime;
            end = oRequestor.DateSpecific.RetirementDate;
            oReport.DiffissueYear = string.Format("{0}", (end.Year - start.Year - 1) + (((end.Month > start.Month) || ((end.Month == start.Month) && (end.Day >= start.Day))) ? 1 : 0));
            oReport.DiffissueMonth = string.Format("{0}", ((end.Month >= start.Month) && (end.Day >= start.Day)) ? end.Month - start.Month
                                                                                           : (11 + end.Month) + ((end.Day >= start.Day) ? 1 : 0));

            List<ReportParameter> ReportParam = new List<ReportParameter>();
            string strExtFile = string.Empty;

            oReport.LetterRunningNo = oLetter.UpdateTime.ToString(oLetterType.LetterNoFormat.Replace("[LETTERNO]", oLetter.LetterNo.ToString("000")), oCulInfo);

            bool isPrintByEmployee = oLetter.EmployeeID == WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID;
            if (WorkflowPrinciple.Current.UserSetting.Employee.IsInRole("LETTERADMIN") || isPrintByEmployee)
            {
                //oReport.LetterRunningNo = oLetter.UpdateTime.ToString(oLetterType.LetterNoFormat.Replace("[LETTERNO]", oLetter.LetterNo.ToString("000")), oCulInfo);
                //DateTime payChkDate = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetPreviousEffectiveDate();
                DateTime payChkDate = HRPYManagement.CreateInstance(sCompanyCode).GetPreviousEffectiveDate();

                if (oLetter.UpdateTime < payChkDate)
                    payChkDate = oLetter.UpdateTime;
                //oReport.Salary = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetSalary(oLetter.EmployeeID, payChkDate).Salary.ToString("#,##0.00");
                oReport.Salary = HRPAManagement.CreateInstance(sCompanyCode).GetSalary(oLetter.EmployeeID, payChkDate).Salary.ToString("#,##0.00");

                strExtFile = isPrintByEmployee ? "PDF" : "EXCEL";

                if (!String.IsNullOrEmpty(oLetterType.RecurringWageType))
                {
                    //Dictionary<string, INFOTYPE0014> oWageDict = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetActiveWageRecuringData(oLetter.EmployeeID, oLetter.UpdateTime, oLetterType.RecurringWageType);
                    Dictionary<string, INFOTYPE0014> oWageDict = HRPYManagement.CreateInstance(sCompanyCode).GetActiveWageRecuringData(oLetter.EmployeeID, oLetter.UpdateTime, oLetterType.RecurringWageType);

                    oReport.RecurringWageData = oLetterType.RecurringWageType.Replace(',', '|');
                    string strDisplayWageData = string.Empty;
                    string strRecurringWageType = string.Empty;
                    foreach (string strWageType in oLetterType.RecurringWageType.Split(','))
                    {
                        if (oWageDict.ContainsKey(strWageType))
                        {
                            strDisplayWageData += oWageDict[strWageType].Amount.ToString("#,##0.00");
                            strDisplayWageData += "|";
                            strRecurringWageType += strWageType;
                            strRecurringWageType += "|";
                        }
                    }
                    oReport.RecurringWageData = strDisplayWageData;
                    ReportParam.Add(new ReportParameter("RecurringWageType", strRecurringWageType));
                }
                ReportParam.Add(new ReportParameter("Confidential", CacheManager.GetCommonText("LETTERPRINT", strLangCode, "CONFIDENTIAL")));
            }
            else
            {
                oReport.Salary = "XX,XXX";
                ReportParam.Add(new ReportParameter("Example", CacheManager.GetCommonText("LETTERPRINT", strLangCode, "EXAMPLE")));
                strExtFile = "EXCEL";
            }
            ReportParam.Add(new ReportParameter("Tab", "\t"));

            string imagePath = new Uri(HttpContext.Current.Server.MapPath("~/Client/images/Letter/" + sCompanyCode + "/Header_Company.png")).AbsoluteUri;
            ReportParam.Add(new ReportParameter("ImagePath", imagePath));

            ESS.SHAREDATASERVICE.DATACLASS.Company oCompany = ShareDataManagement.GetCompanyByCompanyCode(sCompanyCode);

            ReportParam.Add(new ReportParameter("CompanyNameLongTH", oCompany.FullNameTH));
            ReportParam.Add(new ReportParameter("CompanyNameLongEN", oCompany.FullNameEN));
            ReportParam.Add(new ReportParameter("CompanyNameShort", oCompany.Name));


            // Fix Export Excel Only
            isPrintByEmployee = false;
            strExtFile = TypeDoc;
            if (TypeDoc == "WORD" || TypeDoc == "EXCEL")
            {
                isPrintByEmployee = false;
            }
            else if (TypeDoc == "PDF")
            {
                isPrintByEmployee = true;
            }

            DataSet ds = new DataSet("DS_LetterReport");
            DataTable oTable = new DataTable("LetterReport");
            oTable = oReport.ToADODataTable();
            ds.Tables.Add(oTable);

            ReportObject oReportObject = new ReportObject();
            oReportObject.ExportFileName = System.Web.HttpContext.Current.Server.MapPath("~/Client/Report/LetterReportForAdmin" + oLetter.EmployeeID);

            
            ReportViewer rptViewer = new ReportViewer();
            rptViewer.LocalReport.EnableExternalImages = true;
            rptViewer.Font.Name = "TH Sarabun New";
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("DS_LetterReport", ds.Tables[0]));
            rptViewer.LocalReport.ReportPath = System.Web.HttpContext.Current.Server.MapPath("~/Views/Report/HR/LE/" + (isPrintByEmployee ? "PDF_version/" : "") + oLetterType.ReportTemplate);
            rptViewer.LocalReport.SetParameters(ReportParam);
            rptViewer.ShowRefreshButton = false;
            

            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string filenameExtension;

            byte[] bytes = rptViewer.LocalReport.Render(
                strExtFile, null, out mimeType, out encoding, out filenameExtension,
                out streamids, out warnings);

            oReportObject.Warnings = warnings;
            oReportObject.Streamids = streamids;
            oReportObject.ContentType = mimeType;
            oReportObject.Encoding = encoding;
            oReportObject.FilenameExtension = filenameExtension;
            oReportObject.ExportFileName = string.Format("{0}.{1}", oReportObject.ExportFileName, filenameExtension);
            FileManager.SaveFile(oReportObject.ExportFileName, bytes, true);

        }

        private string DateConvertToString(DateTime oDate, CultureInfo oCulInfo, string strLangCode)
        {
            string strReturn = "";
            if (strLangCode.ToUpper() == "EN")
            {
                strReturn = string.Format(oDate.ToString("MMMM d{0}, yyyy", oCulInfo),
                        (oDate.Day % 10 == 1 && DateTime.Now.Day != 11) ? "st"
                        : (oDate.Day % 10 == 2 && DateTime.Now.Day != 12) ? "nd"
                        : (oDate.Day % 10 == 3 && DateTime.Now.Day != 13) ? "rd"
                        : "th");
            }
            else
            {
                strReturn = oDate.ToString("d MMMM yyyy", oCulInfo);
            }
            return strReturn;
        }

        private string GetOrganizationType(string OrgID, OrgLevelType orgType, string LanguageCode, string CompCode)
        {
            string _orgid = OrgID;
            INFOTYPE1010 infotype1010 = null;
            string oResult = string.Empty;
            try
            {
                do
                {
                    if ((OrgLevelType)Convert.ToInt16(OMManagement.CreateInstance(CompCode).GetLevelByOrg(_orgid, DateTime.Now).OrgLevel) == orgType)
                    {
                        infotype1010 = OMManagement.CreateInstance(CompCode).GetLevelByOrg(_orgid, DateTime.Now);
                        break;
                    }
                    else
                    {
                        try
                        {
                            _orgid = OMManagement.CreateInstance(CompCode).GetOrgParent(_orgid, DateTime.Now, LanguageCode).ObjectID;
                        }
                        catch
                        {
                            _orgid = string.Empty;
                        }
                    }
                } while (!String.IsNullOrEmpty(_orgid));

                if (infotype1010 != null)
                    oResult = OMManagement.CreateInstance(CompCode).GetObjectData(ObjectType.O, infotype1010.ObjectID, DateTime.Now, LanguageCode).Text;
                //return INFOTYPE1000.GetObjectData(ObjectType.ORGANIZE, infotype1010.ObjectID, DateTime.Now, LanguageCode).Text;
                else
                    oResult = "-";
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
            return oResult;
        }

        private string DayConvertToString(DateTime oDate, CultureInfo oCulInfo, string strLangCode)
        {
            string strReturn = "";
            if (strLangCode.ToUpper() == "EN")
            {
                strReturn = string.Format(oDate.ToString("d{0}", oCulInfo),
                        (oDate.Day % 10 == 1 && DateTime.Now.Day != 11) ? "st"
                        : (oDate.Day % 10 == 2 && DateTime.Now.Day != 12) ? "nd"
                        : (oDate.Day % 10 == 3 && DateTime.Now.Day != 13) ? "rd"
                        : "th");
            }
            else
            {
                strReturn = oDate.ToString("d", oCulInfo);
            }
            return strReturn;
        }

        private string DuringDate(DateTime oBeginDate, DateTime oEndDate, CultureInfo oCulInfo, string strLangCode)
        {
            string strReturn = "";
            if (strLangCode.ToUpper() == "EN")
            {
                if (oBeginDate.Month == oEndDate.Month)
                {
                    strReturn = oBeginDate.ToString("MMMM") + " " + DayConvertToString(oBeginDate, oCulInfo, strLangCode) + " to " + DayConvertToString(oEndDate, oCulInfo, strLangCode) + oEndDate.ToString(", yyyy");
                }
                else
                {
                    strReturn = DateConvertToString(oBeginDate, oCulInfo, strLangCode) + " to " + DateConvertToString(oEndDate, oCulInfo, strLangCode);
                }
            }
            else
            {
                strReturn = DateConvertToString(oBeginDate, oCulInfo, strLangCode) + " ถึง " + DateConvertToString(oEndDate, oCulInfo, strLangCode);
            }
            return strReturn;
        }

        [HttpPost]
        public DataTable GetAllEmployeeInINFOTYPE0001([FromBody] RequestParameter oRequestParameter)
        {

            DataTable oDT = new DataTable();
            oDT.Columns.Add("EmployeeID1", typeof(string));
            oDT.Columns.Add("EmployeeName1", typeof(string));
            //oDT.Columns.Add("EmployeeFullName1", typeof(string));
            oDT.Columns.Add("PositionID1", typeof(string));
            oDT.Columns.Add("EmployeeID2", typeof(string));
            oDT.Columns.Add("EmployeeName2", typeof(string));
            //oDT.Columns.Add("EmployeeFullName2", typeof(string));
            oDT.Columns.Add("PositionID2", typeof(string));

            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();

            //List<EmployeeData> EmployeeDataList = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetAllEmployeeInINFOTYPE0001(DateTime.Now);
            List<EmployeeData> EmployeeDataList = EmployeeManagement.CreateInstance(sCompanyCode).GetAllEmployeeInINFOTYPE0001(DateTime.Now);

            DataRow dtRow;
            if (EmployeeDataList != null && EmployeeDataList.Count > 0)
            {
                for (int i = 0; i < EmployeeDataList.Count; i++)
                {
                    dtRow = oDT.NewRow();
                    dtRow["EmployeeID1"] = EmployeeDataList[i].EmployeeID;
                    dtRow["EmployeeName1"] = EmployeeDataList[i].AlternativeName(oRequestParameter.CurrentEmployee.Language);
                    dtRow["PositionID1"] = EmployeeDataList[i].PositionID;

                    //if (i + 1 < EmployeeDataList.Count)
                    //{
                    //    dtRow["EmployeeID2"] = EmployeeDataList[i + 1].EmployeeID;
                    //    dtRow["EmployeeName2"] = EmployeeDataList[i + 1].AlternativeName(oRequestParameter.CurrentEmployee.Language);
                    //    dtRow["PositionID2"] = EmployeeDataList[i + 1].PositionID;
                    //    i++;
                    //}
                    oDT.Rows.Add(dtRow);
                }

            }
            return oDT;
        }

        [HttpPost]
        public DataTable GetEmpData(RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();

            DataTable oDT = new DataTable();
            oDT.Columns.Add("PositionID", typeof(string));
            oDT.Columns.Add("PositionName", typeof(string));
            oDT.Columns.Add("OrgUnitID", typeof(string));
            oDT.Columns.Add("OrgUnitName", typeof(string));

            EmployeeData oEmp = new EmployeeData();
            //oEmp = new EmployeeData(oRequestParameter.InputParameter["EmployeeID"].ToString(), oRequestParameter.InputParameter["PositionID"].ToString(), oRequestParameter.Requestor.CompanyCode, DateTime.Now);
            oEmp = new EmployeeData(oRequestParameter.InputParameter["EmployeeID"].ToString(), oRequestParameter.InputParameter["PositionID"].ToString(), sCompanyCode, DateTime.Now);

            DataRow dtRow;
            if (oEmp != null)
            {
                dtRow = oDT.NewRow();
                dtRow["PositionID"] = oEmp.PositionID;
                if (oEmp.OrgAssignment != null && oEmp.OrgAssignment.PositionData != null)
                {
                    dtRow["PositionName"] = oEmp.OrgAssignment.PositionData.AlternativeName(oRequestParameter.CurrentEmployee.Language);
                }
                else
                {
                    dtRow["PositionName"] = string.Empty;
                }
                dtRow["OrgUnitID"] = oEmp.OrgAssignment.OrgUnit;
                if (oEmp.OrgAssignment != null && oEmp.OrgAssignment.OrgUnitData != null)
                {
                    dtRow["OrgUnitName"] = oEmp.OrgAssignment.OrgUnitData.AlternativeName(oRequestParameter.CurrentEmployee.Language);
                }
                else
                {
                    dtRow["OrgUnitName"] = string.Empty;
                }
                oDT.Rows.Add(dtRow);
            }
            return oDT;
        }

        [HttpPost]
        public void AlertIncorrectPIN(RequestParameter oRequestParameter)
        {
            //string userIpAddress2 = HttpContext.Current.Request.UserHostAddress;

            string ip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ip))
            {
                ip = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }

            string bodyAlert = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetCommonText("APPLICATION", oRequestParameter.CurrentEmployee.Language, "ALERT_INCORRECT_PINOCDE");
            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);
            EmployeeManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).AlertIncorrectPIN(oEmp, "Alert PINCODE", string.Format(bodyAlert, ip));
        }

        [HttpPost]
        public List<Letter> GetLetterByAdmin(RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();

            List<Letter> oListLetter = new List<Letter>();
            string selectedPeriod = oRequestParameter.InputParameter["Period"].ToString();
            int iMonth = int.Parse(selectedPeriod.Substring(4, 2));
            int iYear = int.Parse(selectedPeriod.Substring(0, 4));
            DateTime dtBeginDate, dtEndDate;
            dtBeginDate = new DateTime(iYear, iMonth, 1);
            dtEndDate = dtBeginDate.AddMonths(1).AddSeconds(-1);
            //oListLetter = HRPYManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetLetterByAdmin(dtBeginDate, dtEndDate);
            oListLetter = HRPYManagement.CreateInstance(sCompanyCode).GetLetterByAdmin(dtBeginDate, dtEndDate);


            foreach (Letter oLet in oListLetter)
            {
                DataTable odt = new DataTable();
                //odt = EmployeeManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetEmployeeByEmpID(oLet.EmployeeID, DateTime.Now, oRequestParameter.CurrentEmployee.Language);
                //odt = EmployeeManagement.CreateInstance(sCompanyCode).GetEmployeeByEmpID(oLet.EmployeeID, DateTime.Now, oRequestParameter.CurrentEmployee.Language);
                odt = EmployeeManagement.CreateInstance(sCompanyCode).GetEmployeeByEmpID(oLet.EmployeeID, oLet.CreatedDate, oRequestParameter.CurrentEmployee.Language);
                if (odt != null && odt.Rows.Count > 0)
                {
                    oLet.EmployeeName = odt.Rows[0]["EmployeeName"].ToString();
                }
            }
            return oListLetter;
        }

        [HttpPost]
        public string GetNameByEmpID(RequestParameter oRequestParameter)
        {
            string sName = string.Empty;
            DataTable odt = new DataTable();

            odt = EmployeeManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetEmployeeByEmpID(oRequestParameter.InputParameter["EmployeeID"].ToString(), DateTime.Now, oRequestParameter.CurrentEmployee.Language);
            if (odt != null && odt.Rows.Count > 0)
            {
                sName = odt.Rows[0]["EmployeeName"].ToString();
            }
            return sName;
        }

        [HttpPost]
        public DataSet GetEmployeeAllSystem(RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            string textSearch = oRequestParameter.InputParameter["SEARCHTEXT"].ToString();

            //DataSet employeeDataTable = EmployeeManagement
            //    .CreateInstance(oRequestParameter.Requestor.CompanyCode)
            //    .GetEmployeeGetAllSearchText(textSearch, DateTime.Now, oCurrentEmployee.Language);

            DataSet employeeDataTable = EmployeeManagement
               .CreateInstance(sCompanyCode)
               .GetEmployeeGetAllSearchText(textSearch, DateTime.Now, oCurrentEmployee.Language);

            return employeeDataTable;
        }

        // แสดงข้อมูลหน้ารายงานสรุปการแก้ไขนโยบายการลงทุนกองทุนสำรองเลี้ยงชีพ
        [HttpPost]
        public List<PFChangePlanReport> GetPFChangePlanList([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();

                var CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;

                var effectiveDate = Convert.ToDateTime(oRequestParameter.InputParameter["EffectiveDate"].ToString());
                int reportType = Convert.ToInt32(oRequestParameter.InputParameter["ReportType"].ToString());

                List<PFChangePlanReport> TResult = new List<PFChangePlanReport>();

                //TResult = HRPYManagement.CreateInstance(CompanyCode).GetPFChangePlanList(effectiveDate, CompanyCode);
                TResult = HRPYManagement.CreateInstance(sCompanyCode).GetPFChangePlanList(effectiveDate, sCompanyCode);
                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Export Report หน้ารายงานสรุปการแก้ไขนโยบายการลงทุนกองทุนสำรองเลี้ยงชีพ
        [HttpPost]
        public void GetPFChangePlanReport([FromBody]RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();

            var effectiveDate = Convert.ToDateTime(oRequestParameter.InputParameter["EffectiveDate"]);
            int reportType = Convert.ToInt32(oRequestParameter.InputParameter["ReportType"].ToString());

            var ReportName = oRequestParameter.InputParameter["ReportName"].ToString();
            var LanguageCode = oRequestParameter.InputParameter["LanguageCode"].ToString();
            var Employee_id = oRequestParameter.InputParameter["Employee_id"].ToString();
            var ExportType = oRequestParameter.InputParameter["ExportType"].ToString();
            var ExportName = oRequestParameter.InputParameter["EmployeeName"].ToString();
            var CompanyCode = oRequestParameter.InputParameter["CompanyCode"].ToString();
            var DateText = oRequestParameter.InputParameter["DateText"].ToString();
            var TimeText = oRequestParameter.InputParameter["TimeText"].ToString();
            var ExportDate = DateText + " " + DateTime.Now.ToString("dd/MM/yyyy") + " " + TimeText + " " + DateTime.Now.ToString("HH:mm");

            // Text Title
            var T_NO = oRequestParameter.InputParameter["T_NO"].ToString();
            var T_EMPLOYEEID = oRequestParameter.InputParameter["T_EMPLOYEEID"].ToString();
            var T_FULLNAME = oRequestParameter.InputParameter["T_FULLNAME"].ToString();
            var T_OLDPLAN = oRequestParameter.InputParameter["T_OLDPLAN"].ToString();
            var T_NEWPLAN = oRequestParameter.InputParameter["T_NEWPLAN"].ToString();
            var T_REQUESTNO = oRequestParameter.InputParameter["T_REQUESTNO"].ToString();
            var T_SUBMITDATE = oRequestParameter.InputParameter["T_SUBMITDATE"].ToString();
            var T_ACTIONDATE = oRequestParameter.InputParameter["T_ACTIONDATE"].ToString();

            //var T_EMPLOYEESAVINGRATE = oRequestParameter.InputParameter["T_EMPLOYEESAVINGRATE"].ToString();
            //var T_COMPANYSAVINGRATE = oRequestParameter.InputParameter["T_COMPANYSAVINGRATE"].ToString();
            //var T_REQUESTDATE = oRequestParameter.InputParameter["T_REQUESTDATE"].ToString();
            //var T_EFFECTIVEDATE = oRequestParameter.InputParameter["T_EFFECTIVEDATE"].ToString();

            string companyLogo = sCompanyCode;
            string pathImage = new Uri(System.Web.HttpContext.Current.Server.MapPath("~/Client/images/CompanyLogo/" + companyLogo + ".jpg")).AbsoluteUri;


            ReportObject oReportObject = new ReportObject();
            oReportObject.ExportType = ExportType;

            bool isPrintByEmployee;
            if (ExportType == "WORD" || ExportType == "EXCEL")
            {
                isPrintByEmployee = false;
            }
            else
            {
                isPrintByEmployee = true;   // Export PDF
            }

            if (reportType == 1)
            {
                oReportObject.ReportName = System.Web.HttpContext.Current.Server.MapPath("~/Views/Report/HR/PY/" + (isPrintByEmployee ? "PDF_version/" : "") + "PFChangePlanReport.rdlc");
                oReportObject.ExportFileName = System.Web.HttpContext.Current.Server.MapPath("~/Client/Report/PFChangePlanReport" + Employee_id);
            }
            else if (reportType == 2)
            {
                oReportObject.ReportName = System.Web.HttpContext.Current.Server.MapPath("~/Views/Report/HR/PFChangeRateAmountPlanReport.rdlc");
                oReportObject.ExportFileName = System.Web.HttpContext.Current.Server.MapPath("~/Client/Report/PFChangeRateAmountPlanReport" + Employee_id);
            }

            oReportObject.ReportParameters = new List<ReportParameter>()
            {
                new ReportParameter("Param_EffectiveDate", effectiveDate.ToString("dd/MM/yyyy")), // 01/02/2019
                new ReportParameter("Param_ExportDate", ExportDate),
                new ReportParameter("Param_ExportName", ExportName),
                new ReportParameter("H_Col1", T_NO),
                new ReportParameter("H_Col2", T_EMPLOYEEID),
                new ReportParameter("H_Col3", T_FULLNAME),
                new ReportParameter("H_Col4", T_OLDPLAN),
                new ReportParameter("H_Col5", T_NEWPLAN),
                new ReportParameter("H_Col6", T_REQUESTNO),
                new ReportParameter("H_Col7", T_SUBMITDATE),
                new ReportParameter("H_Col8", T_ACTIONDATE),
                new ReportParameter("H_Col9", " "),
                new ReportParameter("ImageLogo",pathImage)
            };

            List<PFChangePlanReport> TResult = new List<PFChangePlanReport>();

            //TResult = HRPYManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
            //    .GetPFChangePlanList(effectiveDate, oRequestParameter.Requestor.CompanyCode);

            TResult = HRPYManagement.CreateInstance(sCompanyCode)
                .GetPFChangePlanList(effectiveDate, sCompanyCode);

            var ds = new DataSet();
            var listData = new List<dynamic>();
            int i = 1;
            foreach (var item in TResult)
            {
                listData.Add(new
                {
                    DataColumn1 = i,
                    DataColumn2 = item.EmployeeID,
                    DataColumn3 = item.EmployeeName,
                    DataColumn4 = item.ProvidentFund_OldOption,
                    DataColumn5 = item.ProvidentFund_NewOption,
                    DataColumn6 = item.RequestNo,
                    DataColumn7 = item.SubmitDate.ToString("dd/MM/yyyy"),
                    DataColumn8 = item.ActionDate.ToString("dd/MM/yyyy")
                });
                i++;
            }

            ds.Tables.Add(ToDataTable(listData, "PFChangePlanDataSet"));

            oReportObject.DataSource = ds;
            ReportViewer oReportViewer = new ReportViewer();
            oReportViewer.LocalReport.DataSources.Clear();
            oReportViewer.ProcessingMode = ProcessingMode.Local;
            oReportViewer.LocalReport.ReportPath = oReportObject.ReportName;
            oReportViewer.LocalReport.DataSources.Add(new ReportDataSource("PFChangePlanDataSet", ds.Tables["PFChangePlanDataSet"]));
            oReportViewer.LocalReport.EnableExternalImages = true;

            // Set Parameters Report
            if (oReportObject.ReportParameters != null && oReportObject.ReportParameters.Count > 0)
            {
                oReportViewer.LocalReport.SetParameters(oReportObject.ReportParameters);
            }

            oReportViewer.ShowRefreshButton = false;
            ReportPageSettings oReportPageSettings = oReportViewer.LocalReport.GetDefaultPageSettings();
            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string filenameExtension;

            byte[] bytes = oReportViewer.LocalReport.Render(
                oReportObject.ExportType,
                null,
                out mimeType,
                out encoding,
                out filenameExtension,
                out streamids,
                out warnings
            );

            oReportObject.Warnings = warnings;
            oReportObject.Streamids = streamids;
            oReportObject.ContentType = mimeType;
            oReportObject.Encoding = encoding;
            oReportObject.FilenameExtension = filenameExtension;
            oReportObject.ExportFileName = string.Format("{0}.{1}", oReportObject.ExportFileName, filenameExtension);
            FileManager.SaveFile(oReportObject.ExportFileName, bytes, true);
        }

        // Export Report หน้ารายงานสรุปการแก้ไขอัตราเงินสะสมกองทุนสำรองเลี้ยงชีพ
        [HttpPost]
        public void GetPFChangeRateAmountPlanReport([FromBody] RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            var effectiveDate = Convert.ToDateTime(oRequestParameter.InputParameter["EffectiveDate"]);
            int reportType = Convert.ToInt32(oRequestParameter.InputParameter["ReportType"].ToString());

            var ReportName = oRequestParameter.InputParameter["ReportName"].ToString();
            var LanguageCode = oRequestParameter.InputParameter["LanguageCode"].ToString();
            var Employee_id = oRequestParameter.InputParameter["Employee_id"].ToString();
            var ExportType = oRequestParameter.InputParameter["ExportType"].ToString();
            var ExportName = oRequestParameter.InputParameter["EmployeeName"].ToString();
            var CompanyCode = oRequestParameter.InputParameter["CompanyCode"].ToString();
            var DateText = oRequestParameter.InputParameter["DateText"].ToString();
            var TimeText = oRequestParameter.InputParameter["TimeText"].ToString();
            var ExportDate = DateText + " " + DateTime.Now.ToString("dd/MM/yyyy") + " " + TimeText + " " + DateTime.Now.ToString("HH:mm");

            // Text Title
            var T_NO = oRequestParameter.InputParameter["T_NO"].ToString();
            var T_EMPLOYEEID = oRequestParameter.InputParameter["T_EMPLOYEEID"].ToString();
            var T_FULLNAME = oRequestParameter.InputParameter["T_FULLNAME"].ToString();
            var T_EMPLOYEESAVINGRATEOLD = oRequestParameter.InputParameter["T_EMPLOYEESAVINGRATEOLD"].ToString();
            var T_EMPLOYEESAVINGRATENEW = oRequestParameter.InputParameter["T_EMPLOYEESAVINGRATENEW"].ToString();
            var T_REQUESTNO = oRequestParameter.InputParameter["T_REQUESTNO"].ToString();
            var T_SUBMITDATE = oRequestParameter.InputParameter["T_SUBMITDATE"].ToString();
            var T_ACTIONDATE = oRequestParameter.InputParameter["T_ACTIONDATE"].ToString();

            string companyLogo = sCompanyCode;
            string pathImage = new Uri(System.Web.HttpContext.Current.Server.MapPath("~/Client/images/CompanyLogo/" + companyLogo + ".jpg")).AbsoluteUri;

            ReportObject oReportObject = new ReportObject();
            oReportObject.ExportType = ExportType;

            bool isPrintByEmployee;
            if (ExportType == "WORD" || ExportType == "EXCEL")
            {
                isPrintByEmployee = false;
            }
            else
            {
                isPrintByEmployee = true;   // Export PDF
            }

            oReportObject.ReportName = System.Web.HttpContext.Current.Server.MapPath("~/Views/Report/HR/PY/" + (isPrintByEmployee ? "PDF_version/" : "") + "PFChangeRateAmountPlanReport.rdlc");
            oReportObject.ExportFileName = System.Web.HttpContext.Current.Server.MapPath("~/Client/Report/PFChangeRateAmountPlanReport" + Employee_id);

            oReportObject.ReportParameters = new List<ReportParameter>()
            {
                new ReportParameter("Param_EffectiveDate", effectiveDate.ToString("dd/MM/yyyy")), // 01/02/2019
                new ReportParameter("Param_ExportDate", ExportDate),
                new ReportParameter("Param_ExportName", ExportName),
                new ReportParameter("H_Col1", T_NO),
                new ReportParameter("H_Col2", T_EMPLOYEEID),
                new ReportParameter("H_Col3", T_FULLNAME),
                new ReportParameter("H_Col4", T_EMPLOYEESAVINGRATEOLD),
                new ReportParameter("H_Col5", T_EMPLOYEESAVINGRATENEW),
                new ReportParameter("H_Col6", T_REQUESTNO),
                new ReportParameter("H_Col7", T_SUBMITDATE),
                new ReportParameter("H_Col8", T_ACTIONDATE),
                new ReportParameter("H_Col9", " "),
                new ReportParameter("ImageLogo",pathImage)
            };

            List<PFChangeRateAmountPlanReport> TResult = new List<PFChangeRateAmountPlanReport>();

            // TResult = HRPYManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPFChangeRateAmountPlanList(effectiveDate, oRequestParameter.Requestor.CompanyCode);
            TResult = HRPYManagement.CreateInstance(sCompanyCode).GetPFChangeRateAmountPlanList(effectiveDate, sCompanyCode);
            var ds = new DataSet();
            var listData = new List<dynamic>();
            int i = 1;
            foreach (var item in TResult)
            {
                listData.Add(new
                {
                    DataColumn1 = i,
                    DataColumn2 = item.EmployeeID,
                    DataColumn3 = item.EmployeeName,
                    DataColumn4 = item.EmployeeSavingRateOld,
                    DataColumn5 = item.EmployeeSavingRateNew,
                    DataColumn6 = item.RequestNo,
                    DataColumn7 = item.SubmitDate.ToString("dd/MM/yyyy"),
                    DataColumn8 = item.ActionDate.ToString("dd/MM/yyyy")
                });
                i++;
            }

            ds.Tables.Add(ToDataTable(listData, "PFChangePlanDataSet"));

            oReportObject.DataSource = ds;
            ReportViewer oReportViewer = new ReportViewer();
            oReportViewer.LocalReport.DataSources.Clear();
            oReportViewer.ProcessingMode = ProcessingMode.Local;
            oReportViewer.LocalReport.ReportPath = oReportObject.ReportName;
            oReportViewer.LocalReport.DataSources.Add(new ReportDataSource("PFChangePlanDataSet", ds.Tables["PFChangePlanDataSet"]));
            oReportViewer.LocalReport.EnableExternalImages = true;

            // Set Parameters Report
            if (oReportObject.ReportParameters != null && oReportObject.ReportParameters.Count > 0)
            {
                oReportViewer.LocalReport.SetParameters(oReportObject.ReportParameters);
            }

            oReportViewer.ShowRefreshButton = false;
            ReportPageSettings oReportPageSettings = oReportViewer.LocalReport.GetDefaultPageSettings();
            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string filenameExtension;

            byte[] bytes = oReportViewer.LocalReport.Render(
                oReportObject.ExportType,
                null,
                out mimeType,
                out encoding,
                out filenameExtension,
                out streamids,
                out warnings
            );

            oReportObject.Warnings = warnings;
            oReportObject.Streamids = streamids;
            oReportObject.ContentType = mimeType;
            oReportObject.Encoding = encoding;
            oReportObject.FilenameExtension = filenameExtension;
            oReportObject.ExportFileName = string.Format("{0}.{1}", oReportObject.ExportFileName, filenameExtension);
            FileManager.SaveFile(oReportObject.ExportFileName, bytes, true);
        }

        // Function Convert DataTable To DateSet For Export Report
        public DataTable ToDataTable(List<dynamic> list, string name)
        {
            List<dynamic> dlist = new List<dynamic>();
            var json = JsonConvert.SerializeObject(list);
            var dataTable = (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));
            dataTable.TableName = name;
            return dataTable;
        }

        public class ReusltStatusAllocateBudget
        {
            public bool IsStatus { get; set; }
            public string EncryptString { get; set; }
            public string URL { get; set; }
        }
        [HttpPost]
        public ReusltStatusAllocateBudget GetAllocateBudget([FromBody]RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            ReusltStatusAllocateBudget result = new ReusltStatusAllocateBudget();

            string pincode = oRequestParameter.InputParameter["PinCode"].ToString();
            // 1. verify pincode
            bool isVerifyPinCode = false;
            isVerifyPinCode = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                .VerifyPinCode(oRequestParameter.Requestor.EmployeeID, pincode);

            // 2. encript
            if (isVerifyPinCode)
            {
                string param_Sent = string.Format("app=ess,employee_id={0},date={1}", oCurrentEmployee.EmployeeID, DateTime.Now.ToString("ddMMyyyyHHmmss", System.Globalization.CultureInfo.InvariantCulture));
                var str_data = HRPYManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).EncryptDataString(param_Sent);

                result.IsStatus = true;
                result.EncryptString = str_data.DataEncrypt;
                result.URL = str_data.DataURL;
            }
            else
            {
                result.IsStatus = false;
            }

            return result;
        }

        #region นำเข้ากองทุนสำรองเลี้ยงชีพ By Admin

        [HttpPost]
        public ProvidentFundKtam ImportDataProvidentfund()
        {
            //Create the Directory.
            string path = HttpContext.Current.Server.MapPath("~/Uploads/Providentfund/");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            //Save the Files.
            string fileName = "";
            foreach (string key in HttpContext.Current.Request.Files)
            {
                HttpPostedFile postedFile = HttpContext.Current.Request.Files[key];
                fileName = postedFile.FileName;
                postedFile.SaveAs(path + postedFile.FileName);
            }

            // Read Text File
            ProvidentFundKtam data_provident = new ProvidentFundKtam();
            data_provident.File = fileName;

            string full_path = path + fileName;
            if (File.Exists(full_path))
            {
                string line;
                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Uploads/Providentfund/" + fileName), Encoding.GetEncoding("windows-874")))
                {
                    // \r\n
                    char[] delimiterChars = { '\r', '\n' };
                    line = reader.ReadToEnd();
                    string[] strlist = line.Split(delimiterChars, StringSplitOptions.None);
                    if (strlist.Count() > 0)
                    {
                        try
                        {
                            for (int i = 0; i < strlist.Length; i++)
                            {
                                if (!string.IsNullOrEmpty(strlist[i]))
                                {
                                    char[] delimiterChars_provident = { '|' };
                                    string[] provident_list = strlist[i].Split(delimiterChars_provident, StringSplitOptions.None);

                                    // header
                                    if (i == 0)
                                    {
                                        // หัวเอกสารกองทุน
                                        for (int j = 0; j < provident_list.Length; j++)
                                        {
                                            switch (j)
                                            {
                                                case 0: data_provident.FundId = provident_list[j]; break;
                                                case 1: data_provident.CompanyId = provident_list[j]; break;
                                                case 2:
                                                    int sub_year = int.Parse(provident_list[j].Substring(0, 4));
                                                    int sub_month = int.Parse(provident_list[j].Substring(4, 2));
                                                    int sub_day = int.Parse(provident_list[j].Substring(6, 2));
                                                    ThaiBuddhistCalendar cal = new ThaiBuddhistCalendar();
                                                    data_provident.NavDate = cal.ToDateTime(sub_year, sub_month, sub_day, 0, 0, 0, 0);
                                                    data_provident.Year = data_provident.NavDate.Year;
                                                    data_provident.Month = data_provident.NavDate.Month;
                                                    break;
                                                case 3:
                                                    data_provident.NavValue = Decimal.Parse(provident_list[j]);
                                                    break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ProvidentFundEmployeeDetail provident_detail = new ProvidentFundEmployeeDetail();
                                        provident_detail.Order = data_provident.list_provident_detail.Count + 1;

                                        // รายละเอียดกองทุนสำรองเลี้ยงชีพของพนักงาน
                                        for (int j = 0; j < provident_list.Length; j++)
                                        {
                                            switch (j)
                                            {
                                                case 0: provident_detail.EmployeeId = provident_list[j]; break; // EmployeeId
                                                case 1: provident_detail.TitleName = provident_list[j]; break; // TitleName
                                                case 2: provident_detail.FirstName = provident_list[j]; break; // FirstName
                                                case 3: provident_detail.LastName = provident_list[j]; break; // LastName
                                                case 4: provident_detail.DeptCode = provident_list[j]; break; // DeptCode
                                                case 5: provident_detail.Old_employee_amount = decimal.Parse(provident_list[j]); break; // Old_employee_amount
                                                case 6: provident_detail.Old_employee_benefit_amount = decimal.Parse(provident_list[j]); break; // Old_employee_amount
                                                case 7: provident_detail.Old_employee_unit = decimal.Parse(provident_list[j]); break; // Old_employee_unit
                                                case 8: provident_detail.Old_company_amount = decimal.Parse(provident_list[j]); break; // Old_company_amount
                                                case 9: provident_detail.Old_company_benefit_amount = decimal.Parse(provident_list[j]); break; // Old_company_benefit_amount
                                                case 10: provident_detail.Old_company_unit = decimal.Parse(provident_list[j]); break; // Old_company_unit
                                                case 11: provident_detail.Old_forward_amount_employee = decimal.Parse(provident_list[j]); break; // Old_forward_amount_employee
                                                case 12: provident_detail.Old_forward_benefit_amount_employee = decimal.Parse(provident_list[j]); break; // Old_forward_benefit_amount_employee
                                                case 13: provident_detail.Old_forward_unit_employee = decimal.Parse(provident_list[j]); break; // Old_forward_unit_employee
                                                case 14: provident_detail.Old_forward_amount_company = decimal.Parse(provident_list[j]); break; // Old_forward_amount_company
                                                case 15: provident_detail.Old_forward_benefit_amount_company = decimal.Parse(provident_list[j]); break; // Old_forward_benefit_amount_company
                                                case 16: provident_detail.Old_forward_unit_company = decimal.Parse(provident_list[j]); break; // Old_forward_unit_company
                                                case 17: provident_detail.Employee_amount_transfer_in = decimal.Parse(provident_list[j]); break; // Employee_amount_transfer_in
                                                case 18: provident_detail.Employee_benefit_amount_transfer_in = decimal.Parse(provident_list[j]); break; // Employee_benefit_amount_transfer_in
                                                case 19: provident_detail.Employee_unit_transfer_in = decimal.Parse(provident_list[j]); break; // Employee_unit_transfer_in
                                                case 20: provident_detail.Company_amount_transfer_in = decimal.Parse(provident_list[j]); break; // Company_amount_transfer_in
                                                case 21: provident_detail.Company_benefit_amount_transfer_in = decimal.Parse(provident_list[j]); break; // Company_benefit_amount_transfer_in
                                                case 22: provident_detail.Company_unit_transfer_in = decimal.Parse(provident_list[j]); break; // Company_unit_transfer_in
                                                case 23: provident_detail.Forward_amount_employee_transfer_in = decimal.Parse(provident_list[j]); break; // Forward_amount_employee_transfer_in
                                                case 24: provident_detail.Forward_benefit_amount_employee_transfer_in = decimal.Parse(provident_list[j]); break; // Forward_benefit_amount_employee_transfer_in
                                                case 25: provident_detail.Forward_unit_employee_transfer_in = decimal.Parse(provident_list[j]); break; // Forward_unit_employee_transfer_in
                                                case 26: provident_detail.Forward_amount_company_transfer_in = decimal.Parse(provident_list[j]); break; // Forward_amount_company_transfer_in
                                                case 27: provident_detail.Forward_benefit_amount_company_transfer_in = decimal.Parse(provident_list[j]); break; // Forward_benefit_amount_company_transfer_in
                                                case 28: provident_detail.Forward_unit_company_transfer_in = decimal.Parse(provident_list[j]); break; // Forward_unit_company_transfer_in
                                                case 29: provident_detail.Year_employee_amount = decimal.Parse(provident_list[j]); break; // Year_employee_amount
                                                case 30: provident_detail.Year_employee_benefit_amount = decimal.Parse(provident_list[j]); break; // Year_employee_benefit_amount
                                                case 31: provident_detail.Year_employee_unit = decimal.Parse(provident_list[j]); break; // Year_employee_unit
                                                case 32: provident_detail.Year_company_amount = decimal.Parse(provident_list[j]); break; // Year_company_amount
                                                case 33: provident_detail.Year_company_benefit_amount = decimal.Parse(provident_list[j]); break; // Year_company_benefit_amount
                                                case 34: provident_detail.Year_company_unit = decimal.Parse(provident_list[j]); break; // Year_company_unit
                                                case 35: provident_detail.Year_forward_amount_employee = decimal.Parse(provident_list[j]); break; // Year_forward_amount_employee
                                                case 36: provident_detail.Year_forward_benefit_amount_employee = decimal.Parse(provident_list[j]); break; // Year_forward_benefit_amount_employee
                                                case 37: provident_detail.Year_forward_unit_employee = decimal.Parse(provident_list[j]); break; // Year_forward_unit_employee
                                                case 38: provident_detail.Year_forward_amount_company = decimal.Parse(provident_list[j]); break; // Year_forward_amount_company
                                                case 39: provident_detail.Year_forward_benefit_amount_company = decimal.Parse(provident_list[j]); break; // Year_forward_benefit_amount_company
                                                case 40: provident_detail.Year_forward_unit_company = decimal.Parse(provident_list[j]); break; // Year_forward_unit_company
                                                case 41: provident_detail.Employee_amount_transfer_out = decimal.Parse(provident_list[j]); break; // Employee_amount_transfer_out
                                                case 42: provident_detail.Employee_benfit_amount_transfer_out = decimal.Parse(provident_list[j]); break; // Employee_benfit_amount_transfer_out
                                                case 43: provident_detail.Employee_unit_transfer_out = decimal.Parse(provident_list[j]); break; // Employee_unit_transfer_out
                                                case 44: provident_detail.Company_amount_transfer_out = decimal.Parse(provident_list[j]); break; // Company_amount_transfer_out
                                                case 45: provident_detail.Company_benefit_amount_transfer_out = decimal.Parse(provident_list[j]); break; // Company_benefit_amount_transfer_out
                                                case 46: provident_detail.Company_unit_transfer_out = decimal.Parse(provident_list[j]); break; // Company_unit_transfer_out
                                                case 47: provident_detail.Forward_amount_employee_transfer_out = decimal.Parse(provident_list[j]); break; // Forward_amount_employee_transfer_out
                                                case 48: provident_detail.Forward_benefit_amount_employee_transfer_out = decimal.Parse(provident_list[j]); break; // Forward_benefit_amount_employee_transfer_out
                                                case 49: provident_detail.Forward_unit_employee_transfer_out = decimal.Parse(provident_list[j]); break; // Forward_unit_employee_transfer_out
                                                case 50: provident_detail.Forward_amount_company_transfer_out = decimal.Parse(provident_list[j]); break; // Forward_amount_company_transfer_out
                                                case 51: provident_detail.Forward_benefit_amount_company_transfer_out = decimal.Parse(provident_list[j]); break; // Forward_benefit_amount_company_transfer_out
                                                case 52: provident_detail.Forward_unit_company_transfer_out = decimal.Parse(provident_list[j]); break; // Forward_unit_company_transfer_out
                                                case 53: provident_detail.Total_employee_amount = decimal.Parse(provident_list[j]); break; // Total_employee_amount
                                                case 54: provident_detail.Total_employee_benefit_amount = decimal.Parse(provident_list[j]); break; // Total_employee_benefit_amount
                                                case 55: provident_detail.Total_employee_unit = decimal.Parse(provident_list[j]); break; // Total_employee_unit
                                                case 56: provident_detail.Total_company_amount = decimal.Parse(provident_list[j]); break; // Total_company_amount
                                                case 57: provident_detail.Total_company_benefit_amount = decimal.Parse(provident_list[j]); break; // Total_company_benefit_amount
                                                case 58: provident_detail.Total_company_unit = decimal.Parse(provident_list[j]); break; // Total_company_unit
                                                case 59: provident_detail.Total_forward_amount_employee = decimal.Parse(provident_list[j]); break; // Total_forward_amount_employee
                                                case 60: provident_detail.Total_forward_benefit_amount_employee = decimal.Parse(provident_list[j]); break; // Total_forward_benefit_amount_employee
                                                case 61: provident_detail.Total_forward_unit_employee = decimal.Parse(provident_list[j]); break; // Total_forward_unit_employee
                                                case 62: provident_detail.Total_forward_amount_company = decimal.Parse(provident_list[j]); break; // Total_forward_amount_company
                                                case 63: provident_detail.Total_forward_benefit_amount_company = decimal.Parse(provident_list[j]); break; // Total_forward_benefit_amount_company
                                                case 64: provident_detail.Total_forward_unit_company = decimal.Parse(provident_list[j]); break; // Total_forward_unit_company
                                                case 65: provident_detail.External_trans_emp_amount = decimal.Parse(provident_list[j]); break; // External_trans_emp_amount 
                                                case 66: provident_detail.External_trans_emp_benefit_amount = decimal.Parse(provident_list[j]); break; // External_trans_emp_benefit_amount 
                                                case 67: provident_detail.External_trans_comp_amount = decimal.Parse(provident_list[j]); break; // External_trans_comp_amount 
                                                case 68: provident_detail.External_trans_comp_benefit_amount = decimal.Parse(provident_list[j]); break; // External_trans_comp_benefit_amount 
                                            }
                                        }
                                        data_provident.list_provident_detail.Add(provident_detail);
                                    }
                                }
                            }
                        }
                        catch
                        {
                            data_provident.StirngError = "กรุณาตรวจสอบไฟล์นำเข้า";
                            data_provident.IsError = true;
                        }

                        if(string.IsNullOrEmpty(data_provident.FundId) 
                            || string.IsNullOrEmpty(data_provident.CompanyId) 
                            || data_provident.NavValue <= 0 
                            || data_provident.Month <= 0 || data_provident.Year <= 0)
                        {
                            data_provident.StirngError = "Format Text นำเข้าผิดกรุณาตรวจสอบ";
                            data_provident.IsError = true;
                        }
                    }
                }
            }
            else
            {
                data_provident.StirngError = "ไม่พบไฟล์นำเข้าข้อมูล";
                data_provident.IsError = true;
            }

            if (data_provident.IsError == false)
            {
                // pagination
                data_provident.TotalPage = data_provident.list_provident_detail.Count();  // ข้อมูลทั้งหมด

                int page = 1;
                int itemPerPage = 50;
                var result_pagination = data_provident.list_provident_detail.Skip((page - 1) * itemPerPage).Take(itemPerPage).ToList();

                data_provident.Pagination_data = result_pagination;
            }

            return data_provident;
        }

        public class ListImportTextProvidentfund : RequestParameter
        {
            public ListImportTextProvidentfund()
            {
                Provident_fund_all = new List<ProvidentFundEmployeeDetail>();
            }
            public string FundId { get; set; }
            public string CompanyId { get; set; }
            public DateTime NavDate { get; set; }
            public decimal NavValue { get; set; }
            public string FileName { get; set; }
            public int Year { get; set; }
            public int Month { get; set; }
            public List<ProvidentFundEmployeeDetail> Provident_fund_all { get; set; }
        }
        [HttpPost]
        public List<ProvidentFundEmployeeDetail> PaginationImportDataProvidentfund([FromBody]ListImportTextProvidentfund oRequestParameter)
        {
            int page = Convert.ToInt16(oRequestParameter.InputParameter["Page"]);
            int itemPerPage = Convert.ToInt16(oRequestParameter.InputParameter["ItemPerPage"]);
            return oRequestParameter.Provident_fund_all.Skip((page - 1) * itemPerPage).Take(itemPerPage).ToList();
        }

        [HttpPost]
        public ResponeKeyProvident ImportProvidentfundSaveToDatabase([FromBody]ListImportTextProvidentfund oRequestParameter)
        {
            ResponeKeyProvident result = new ResponeKeyProvident();

            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();

            SaveProvidentFundKtam prodient_save = new SaveProvidentFundKtam()
            {
                CompanyId = oRequestParameter.CompanyId,
                EmployeeId = oRequestParameter.Requestor.EmployeeID,
                File = oRequestParameter.FileName,
                FundId = oRequestParameter.FundId,
                Month = oRequestParameter.Month,
                NavDate = oRequestParameter.NavDate,
                NavValue = oRequestParameter.NavValue,
                ListProvidentfundEmployee = oRequestParameter.Provident_fund_all,
                Year = oRequestParameter.Year
            };

            if (prodient_save.CompanyId != null && prodient_save.FundId != null)
            {
                result = HRPYManagement.CreateInstance(sCompanyCode).SaveImportProvidenfund(prodient_save);
            }

            return result;
        }

        [HttpPost]
        public HttpResponseMessage ExportExcelProvidentFund([FromBody]RequestParameter oRequestParameter)
        {
            try
            {
                string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
                string sYear = oRequestParameter.InputParameter["Year"].ToString();
                string sMonth = oRequestParameter.InputParameter["Month"].ToString();
                string pFundId = oRequestParameter.InputParameter["FundId"].ToString();

                int convert_year = Convert.ToInt16(sYear);
                int convert_month = Convert.ToInt16(sMonth);

                EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
                SetAuthenticate(oCurrentEmployee);

                EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);

                var workbook = HRPYManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).ExportExcelProvidentFund(convert_year, convert_month, pFundId);
                var stream = new MemoryStream();
                workbook.SaveAs(stream);
                stream.Position = 0;

                var result = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new ByteArrayContent(stream.ToArray())
                };

                DateTime date_now = DateTime.Now;
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = "ReportProvidentfund" + date_now.Year + date_now.Month + date_now.Date + date_now.Minute + date_now.Second + ".xlsx"
                };
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

                return result;
            }
            catch
            {
                throw;
            }

        }

        [HttpPost]
        public List<string> GetYearProvidentfund([FromBody]RequestParameter oRequestParameter)
        {
            List<string> list_year = new List<string>();

            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();

            list_year = HRPYManagement.CreateInstance(sCompanyCode).GetYearProvidentfund();

            return list_year;
        }

        [HttpPost]
        public List<string> GetMonthProvidentfund([FromBody]RequestParameter oRequestParameter)
        {
            List<string> list_month_provident = new List<string>();

            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            string sYear = oRequestParameter.InputParameter["YearProvidentfund"].ToString();

            int convert_year;
            if (int.TryParse(sYear, out convert_year))
            {
                list_month_provident = HRPYManagement.CreateInstance(sCompanyCode).GetMonthProvidentfund(convert_year);
            }

            return list_month_provident;
        }

        [HttpPost]
        public List<TypePVD> GetTextProvidentfund([FromBody]RequestParameter oRequestParameter)
        {
            List<TypePVD> list_txt_provident = new List<TypePVD>();

            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            string sYear = oRequestParameter.InputParameter["YearProvidentfund"].ToString();
            string sMonth = oRequestParameter.InputParameter["MonthProvidentfund"].ToString();

            int convert_year;
            int convert_month;
            if (int.TryParse(sYear, out convert_year) && int.TryParse(sMonth, out convert_month))
            {
                list_txt_provident = HRPYManagement.CreateInstance(sCompanyCode).GetTextProvidentfund(convert_year, convert_month);
            }

            return list_txt_provident;
        }
        #endregion

        #region Payroll Employee

        public void LoadPayrollEmployee([FromBody]RequestParameter oRequestParameter)
        {
            HRPYManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).JobLoadPayrollEmployee(0);
        }

        #endregion

        [HttpPost]
        public DataSet GetEmployeeGetAllSearchTextByManager(RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            string textSearch = oRequestParameter.InputParameter["SEARCHTEXT"].ToString();

            DataSet employeeDataTable = EmployeeManagement
               .CreateInstance(sCompanyCode)
               .GetEmployeeGetAllSearchTextByManager(oRequestParameter.Requestor.EmployeeID, textSearch, DateTime.Now, oCurrentEmployee.Language);

            return employeeDataTable;
        }
    }
}