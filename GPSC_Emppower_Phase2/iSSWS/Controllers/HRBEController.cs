﻿using ClosedXML.Excel;
using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.CONFIG.PA;
using ESS.HR.BE;
using ESS.HR.BE.DATACLASS;
using ESS.HR.BE.DATASERVICE;
using ESS.HR.PA;
using ESS.HR.PA.INFOTYPE;
using ESS.SHAREDATASERVICE;
using ESS.UTILITY.EXPORT;
using ESS.UTILITY.FILE;
using iSSWS.Models;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using ESS.HR.FB;
using ESS.EMPLOYEE.DATACLASS;
using ESS.HR.PA.DATACLASS;
using ESS.WORKFLOW;

namespace iSSWS.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class HRBEController : ApiController
    {

        private CultureInfo enCL = new CultureInfo("en-US");
        public void SetAuthenticate(EmployeeDataForMobile oCurrentEmployee)
        {
            WorkflowIdentity iden;
            if (oCurrentEmployee.IsExternalUser)
                iden = WorkflowIdentity.CreateInstance(oCurrentEmployee.CompanyCode).GetIdentityForExternalUser(oCurrentEmployee.EmployeeID, oCurrentEmployee.Name);
            else
            {
                EmployeeData oEmployeeData = Convert<EmployeeData>.ObjectFrom<EmployeeDataForMobile>(oCurrentEmployee);
                iden = WorkflowIdentity.CreateInstance(oCurrentEmployee.CompanyCode).GetIdentityWithoutPassword(oEmployeeData);
            }
            WorkflowPrinciple Principle = new WorkflowPrinciple(iden);
            WorkflowPrinciple.Current = Principle;
        }


        [HttpPost]
        public List<TrainingForDev> GetTraining([FromBody] RequestParameter oRequestParameter)
        {
            List<TrainingForDev> oResult = new List<TrainingForDev>();
            oResult = HRBEManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetBETraining(oRequestParameter.Requestor.EmployeeID);
            return oResult;
        }

        #region EducationAllowance
        [HttpPost]
        public List<BEEducationAllowance> GetEducationAllowance([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                var Employee_ID = "";
                var CompanyCode = "";

                if (oRequestParameter.CurrentEmployee.EmployeeID == oRequestParameter.Requestor.EmployeeID)
                {
                    Employee_ID = oRequestParameter.CurrentEmployee.EmployeeID;
                    CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
                }
                else
                {
                    Employee_ID = oRequestParameter.Requestor.EmployeeID;
                    CompanyCode = oRequestParameter.Requestor.CompanyCode;
                }

                List<BEEducationAllowance> TResult = new List<BEEducationAllowance>();
                TResult = HRBEManagement.CreateInstance(CompanyCode.ToString()).GetEducationAllowance(Employee_ID.ToString(), oRequestParameter.InputParameter["RequestNo"].ToString(), oRequestParameter.InputParameter["KeyYear"].ToString());
                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public List<BEConfig> GetBEConfig([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                List<BEConfig> TResult = new List<BEConfig>();
                TResult = HRBEManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetBEConfig(oRequestParameter.InputParameter["BE_Type"].ToString());
                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public List<BEEducationAllowance> GetQuotaList([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                var Employee_ID = "";
                var CompanyCode = "";

                if (oRequestParameter.CurrentEmployee.EmployeeID == oRequestParameter.Requestor.EmployeeID)
                {
                    Employee_ID = oRequestParameter.CurrentEmployee.EmployeeID;
                    CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
                }
                else
                {
                    Employee_ID = oRequestParameter.Requestor.EmployeeID;
                    CompanyCode = oRequestParameter.Requestor.CompanyCode;
                }
                List<BEEducationAllowance> TResult = new List<BEEducationAllowance>();
                TResult = HRBEManagement.CreateInstance(CompanyCode.ToString()).GetEducationAllowanceQuotaList(Employee_ID.ToString(), oRequestParameter.InputParameter["KeyYear"].ToString());
                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public decimal GetQuotaUse([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                var Employee_ID = "";
                var CompanyCode = "";
                var EducationYear = "";
                var ChildNo = "";
                var EducationLevel = 0;
                var RequestNo = "";
                if (oRequestParameter.CurrentEmployee.EmployeeID == oRequestParameter.Requestor.EmployeeID)
                {
                    Employee_ID = oRequestParameter.CurrentEmployee.EmployeeID;
                    CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
                    EducationYear = oRequestParameter.InputParameter["EducationYear"].ToString();
                    ChildNo = oRequestParameter.InputParameter["ChildNo"].ToString();
                    EducationLevel = Convert.ToInt32(oRequestParameter.InputParameter["EducationLevel"].ToString());
                    RequestNo = oRequestParameter.InputParameter["RequestNo"].ToString();
                }
                else
                {
                    Employee_ID = oRequestParameter.Requestor.EmployeeID;
                    CompanyCode = oRequestParameter.Requestor.CompanyCode;
                    EducationYear = oRequestParameter.InputParameter["EducationYear"].ToString();
                    ChildNo = oRequestParameter.InputParameter["ChildNo"].ToString();
                    EducationLevel = Convert.ToInt32(oRequestParameter.InputParameter["EducationLevel"].ToString());
                    RequestNo = oRequestParameter.InputParameter["RequestNo"].ToString();
                }

                var QuotaUsed = HRBEManagement.CreateInstance(CompanyCode.ToString()).CheckQuota(Employee_ID, EducationYear, ChildNo, EducationLevel, RequestNo);
                return QuotaUsed;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion EducationAllowance

        #region YearsConfig
        [HttpPost]
        public List<YearsConfig> GetYears([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                //string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
                List<YearsConfig> TResult = new List<YearsConfig>();
                TResult = HRBEManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetYears();
                //TResult = HRBEManagement.CreateInstance(sCompanyCode).GetYears();
                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public List<YearsConfig> GetYearsAdminSetting([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
                List<YearsConfig> TResult = new List<YearsConfig>();
                //TResult = HRBEManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetYears();
                TResult = HRBEManagement.CreateInstance(sCompanyCode).GetYears();
                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion YearsConfig



        [HttpPost]
        public List<BERegistorHealthInsuranceConfig> GetRegistorHealthInsuranceConfig([FromBody] RequestParameter oRequestParameter)
        {
            List<BERegistorHealthInsuranceConfig> oResult = new List<BERegistorHealthInsuranceConfig>();
            oResult = HRBEManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetRegistorHealthInsuranceConfig(oRequestParameter.Requestor.EmployeeID);
            return oResult;
        }

        [HttpPost]
        public List<BERegistorHealthInsuranceStatusConfigList> GetRegistorHealthInsuranceStatusConfig([FromBody] RequestParameter oRequestParameter)
        {
            List<BERegistorHealthInsuranceStatusConfigList> oResult = new List<BERegistorHealthInsuranceStatusConfigList>();
            int Year = Convert.ToInt32(oRequestParameter.InputParameter["KeyYear"].ToString());
            oResult = HRBEManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetRegistorHealthInsuranceStatusConfig(oRequestParameter.Requestor.EmployeeID, Year);
            return oResult;
        }


        #region HealthInsAllowance

        [HttpPost]
        public List<BEHealthInsAllowance> GetHealthInsAllowance([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                var Employee_ID = "";
                var CompanyCode = "";

                if (oRequestParameter.CurrentEmployee.EmployeeID == oRequestParameter.Requestor.EmployeeID)
                {
                    Employee_ID = oRequestParameter.CurrentEmployee.EmployeeID;
                    CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
                }
                else
                {
                    Employee_ID = oRequestParameter.Requestor.EmployeeID;
                    CompanyCode = oRequestParameter.Requestor.CompanyCode;
                }

                List<BEHealthInsAllowance> TResult = new List<BEHealthInsAllowance>();
                TResult = HRBEManagement.CreateInstance(CompanyCode.ToString()).GetHealthInsAllowance(Employee_ID.ToString(), oRequestParameter.InputParameter["RequestNo"].ToString(), oRequestParameter.InputParameter["KeyYear"].ToString());
                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public List<BEHealthInsAllowanceItem> GetHealthInsAllowanceItem([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                var Employee_ID = "";
                var CompanyCode = "";

                if (oRequestParameter.CurrentEmployee.EmployeeID == oRequestParameter.Requestor.EmployeeID)
                {
                    Employee_ID = oRequestParameter.CurrentEmployee.EmployeeID;
                    CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
                }
                else
                {
                    Employee_ID = oRequestParameter.Requestor.EmployeeID;
                    CompanyCode = oRequestParameter.Requestor.CompanyCode;
                }

                List<BEHealthInsAllowanceItem> TResult = new List<BEHealthInsAllowanceItem>();
                TResult = HRBEManagement.CreateInstance(CompanyCode.ToString()).GetHealthInsAllowanceItem(Employee_ID.ToString(), oRequestParameter.InputParameter["RequestNo"].ToString());
                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public List<BEHealthInsAllowance> GetHealthInsAllowanceQuota([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                var Employee_ID = "";
                var CompanyCode = "";
                var Years = oRequestParameter.InputParameter["Years"].ToString();
                WorkflowController oWorkflowController = new WorkflowController();

                if (oRequestParameter.CurrentEmployee.EmployeeID == oRequestParameter.Requestor.EmployeeID)
                {
                    Employee_ID = oRequestParameter.CurrentEmployee.EmployeeID;
                    CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
                    oWorkflowController.SetAuthenticate(oRequestParameter.CurrentEmployee);
                }
                else
                {
                    Employee_ID = oRequestParameter.Requestor.EmployeeID;
                    CompanyCode = oRequestParameter.Requestor.CompanyCode;
                    oWorkflowController.SetAuthenticate(oRequestParameter.Requestor);
                }

                //Get Config
                BEHealthInsAllowanceOption oBEHealthInsAllowance = new BEHealthInsAllowanceOption();
                oBEHealthInsAllowance.BEHealthInsAllowanceConfigData = HRBEManagement.CreateInstance(CompanyCode).GetHealthInsAllowanceConfig(Years);

                int HealthInsAllowanceBeginAge = Convert.ToInt32(ShareDataManagement.LookupCache(CompanyCode, "ESS.HR.BE.SAP", "HealthInsAllowanceBeginAge"));
                int HealthInsAllowanceEndAge = Convert.ToInt32(ShareDataManagement.LookupCache(CompanyCode, "ESS.HR.BE.SAP", "HealthInsAllowanceEndAge"));

                //get ข้อมูลครอบครัว
                var sub_type = "";
                var Parameter_FamilyData = "";

                if (oBEHealthInsAllowance.BEHealthInsAllowanceConfigData.Count != 0)
                {
                    foreach (BEHealthInsAllowanceConfig item in oBEHealthInsAllowance.BEHealthInsAllowanceConfigData)
                    {
                        sub_type += item.RelationshipType + ",";
                    }

                    sub_type = sub_type.Remove(sub_type.Length - 1);


                    var FamilyData = HRBEManagement.CreateInstance(CompanyCode).GetFamilyData(Employee_ID, sub_type);


                    if (FamilyData != null)
                    {
                        foreach (var item in FamilyData)
                        {
                            Parameter_FamilyData += item.Name + "|";
                            Parameter_FamilyData += item.Surname + "|";
                            Parameter_FamilyData += item.FamilyMember + "|";
                            Parameter_FamilyData += item.Age0101 + "|";
                            if (!String.IsNullOrEmpty(item.Dead.ToString()))
                            {
                                Parameter_FamilyData += "1|,"; /*ตาย*/
                            }
                            else
                            {
                                Parameter_FamilyData += "0|,"; /*ยังไม่ตาย*/
                            }
                        }
                    }
                }
                else
                {
                    foreach (BEHealthInsAllowanceConfig item in oBEHealthInsAllowance.BEHealthInsAllowanceConfigData)
                    {
                        sub_type += item.RelationshipType + ",";
                    }

                    sub_type = sub_type.Remove(sub_type.Length - 1);


                    var FamilyData = HRBEManagement.CreateInstance(CompanyCode).GetFamilyData(Employee_ID, sub_type);


                    if (FamilyData != null)
                    {
                        foreach (var item in FamilyData)
                        {
                            Parameter_FamilyData += item.Name + "|";
                            Parameter_FamilyData += item.Surname + "|";
                            Parameter_FamilyData += item.FamilyMember + "|";
                            Parameter_FamilyData += item.Age0101 + "|";
                            if (!String.IsNullOrEmpty(item.Dead.ToString()))
                            {
                                Parameter_FamilyData += "1|,"; /*ตาย*/
                            }
                            else
                            {
                                Parameter_FamilyData += "0|,"; /*ยังไม่ตาย*/
                            }
                        }
                    }
                }

                List<BEHealthInsAllowance> TResult = new List<BEHealthInsAllowance>();
                TResult = HRBEManagement.CreateInstance(CompanyCode.ToString()).GetHealthInsAllowanceQuota(Employee_ID.ToString(), oRequestParameter.InputParameter["Years"].ToString(), Parameter_FamilyData);

                var oFamilyData = HRBEManagement.CreateInstance(CompanyCode).GetFamilyData(Employee_ID, sub_type);
                if (oFamilyData != null)
                {
                    foreach (var item in oFamilyData)
                    {
                        int YearChk = ((item.BirthDate.Day == 1 && item.BirthDate.Month == 1) ? 0 : 1);   // 19-01-2021 : เช็คเพิ่มเติม เกิดวันที่ 1 ม.ค. ไม่ต้องบวก 1 
                        DateTime DateChk = new DateTime(item.BirthDate.Year + YearChk, 1, 1);
                        var BirthDateAdd70 = DateChk.AddYears(HealthInsAllowanceBeginAge);
                        var BirthDateAdd80 = DateChk.AddYears(HealthInsAllowanceEndAge);

                        if (Convert.ToInt32(oRequestParameter.InputParameter["Years"]) >= DateTime.Now.Year && (DateTime.Now < BirthDateAdd70 || DateTime.Now >= BirthDateAdd80))
                        {
                            var oHealthInsAllowanceQuota = TResult.FirstOrDefault(a => a.RelationshipType.ToString() == item.FamilyMember);
                            oHealthInsAllowanceQuota.Quota_all = 0;
                            oHealthInsAllowanceQuota.Balance = 0;
                        }
                        else if (Convert.ToInt32(oRequestParameter.InputParameter["Years"]) < DateTime.Now.Year)
                        {
                            // 19-01-2021 : เช็คเพิ่มเติม ปีที่ผ่านมา ถ้าอายุไม่ถึง 70 ปี ให้เคลียร์ยอดโควต้า ปัจจุบันมีโควต้าโชว์อยู่ 
                            DateTime chkYearDate = new DateTime(Convert.ToInt32(oRequestParameter.InputParameter["Years"]), BirthDateAdd70.Month, BirthDateAdd70.Day);
                            if (chkYearDate < BirthDateAdd70)
                            {
                                var oHealthInsAllowanceQuota = TResult.FirstOrDefault(a => a.RelationshipType.ToString() == item.FamilyMember);
                                oHealthInsAllowanceQuota.Quota_all = 0;
                                oHealthInsAllowanceQuota.Balance = 0;
                            }
                        }
                    }
                }

                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public decimal HealthInsAllowanceCheckQuotaUse([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                var Employee_ID = "";
                var CompanyCode = "";
                var Year = "";
                var RequestNo = "";
                var Position_ID = "";
                var RelationshipType = "";
                if (oRequestParameter.CurrentEmployee.EmployeeID == oRequestParameter.Requestor.EmployeeID)
                {
                    Employee_ID = oRequestParameter.CurrentEmployee.EmployeeID;
                    CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
                    Year = oRequestParameter.InputParameter["Year"].ToString();
                    RequestNo = oRequestParameter.InputParameter["RequestNo"].ToString();
                    RelationshipType = oRequestParameter.InputParameter["RelationshipType"].ToString();

                }
                else
                {
                    Employee_ID = oRequestParameter.Requestor.EmployeeID;
                    CompanyCode = oRequestParameter.Requestor.CompanyCode;
                    Year = oRequestParameter.InputParameter["Year"].ToString();
                    RequestNo = oRequestParameter.InputParameter["RequestNo"].ToString();
                    RelationshipType = oRequestParameter.InputParameter["RelationshipType"].ToString();

                }

                var RelationshipTypeInt = Convert.ToInt32(RelationshipType);
                var QuotaUsed = HRBEManagement.CreateInstance(CompanyCode.ToString()).BEHealthInsAllowanceCheckQuota(Employee_ID, Year, RelationshipTypeInt, RequestNo);
                return QuotaUsed;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion HealthInsAllowance

        #region FitnessAllowance

        [HttpPost]
        public List<BEFitnessAllowance> GetFitnessAllowance([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                var Employee_ID = "";
                var CompanyCode = "";

                if (oRequestParameter.CurrentEmployee.EmployeeID == oRequestParameter.Requestor.EmployeeID)
                {
                    Employee_ID = oRequestParameter.CurrentEmployee.EmployeeID;
                    CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
                }
                else
                {
                    Employee_ID = oRequestParameter.Requestor.EmployeeID;
                    CompanyCode = oRequestParameter.Requestor.CompanyCode;
                }

                List<BEFitnessAllowance> TResult = new List<BEFitnessAllowance>();
                TResult = HRBEManagement.CreateInstance(CompanyCode.ToString()).GetBEFitnessAllowance(Employee_ID.ToString(), oRequestParameter.InputParameter["RequestNo"].ToString(), oRequestParameter.InputParameter["KeyYear"].ToString());
                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public List<BEFitnessAllowanceItem> GetFitnessAllowanceItem([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                var Employee_ID = "";
                var CompanyCode = "";

                if (oRequestParameter.CurrentEmployee.EmployeeID == oRequestParameter.Requestor.EmployeeID)
                {
                    Employee_ID = oRequestParameter.CurrentEmployee.EmployeeID;
                    CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
                }
                else
                {
                    Employee_ID = oRequestParameter.Requestor.EmployeeID;
                    CompanyCode = oRequestParameter.Requestor.CompanyCode;
                }

                List<BEFitnessAllowanceItem> TResult = new List<BEFitnessAllowanceItem>();
                TResult = HRBEManagement.CreateInstance(CompanyCode.ToString()).GetBEFitnessAllowanceItem(Employee_ID.ToString(), oRequestParameter.InputParameter["RequestNo"].ToString());
                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public List<BEFitnessAllowance> GetFitnessAllowanceQuota([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                var Employee_ID = "";
                var CompanyCode = "";
                var Position_ID = "";
                WorkflowController oWorkflowController = new WorkflowController();

                if (oRequestParameter.CurrentEmployee.EmployeeID == oRequestParameter.Requestor.EmployeeID)
                {
                    Employee_ID = oRequestParameter.CurrentEmployee.EmployeeID;
                    Position_ID = oRequestParameter.CurrentEmployee.PositionID;
                    CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
                    oWorkflowController.SetAuthenticate(oRequestParameter.CurrentEmployee);
                }
                else
                {
                    Employee_ID = oRequestParameter.Requestor.EmployeeID;
                    Position_ID = oRequestParameter.Requestor.PositionID;
                    CompanyCode = oRequestParameter.Requestor.CompanyCode;
                    oWorkflowController.SetAuthenticate(oRequestParameter.Requestor);
                }
                List<BEFitnessAllowance> TResult = new List<BEFitnessAllowance>();

                //หาวันที่บรรจุ
                EmployeeData oEmpData = new EmployeeData(Employee_ID, Position_ID, CompanyCode, DateTime.Now);

                TResult = HRBEManagement.CreateInstance(CompanyCode.ToString()).GetBEFitnessAllowanceQuota(Employee_ID.ToString(), Convert.ToDateTime(oEmpData.DateSpecific.PassProbation), oRequestParameter.InputParameter["Years"].ToString());

                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public string FitnessAllowanceCheckQuotaUse([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                var Employee_ID = "";
                var CompanyCode = "";
                var Year = "";
                var RequestNo = "";
                var Position_ID = "";
                WorkflowController oWorkflowController = new WorkflowController();
                if (oRequestParameter.CurrentEmployee.EmployeeID == oRequestParameter.Requestor.EmployeeID)
                {
                    Employee_ID = oRequestParameter.CurrentEmployee.EmployeeID;
                    CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
                    Year = oRequestParameter.InputParameter["Year"].ToString();
                    RequestNo = oRequestParameter.InputParameter["RequestNo"].ToString();
                    Position_ID = oRequestParameter.CurrentEmployee.PositionID;
                    oWorkflowController.SetAuthenticate(oRequestParameter.CurrentEmployee);
                }
                else
                {
                    Employee_ID = oRequestParameter.Requestor.EmployeeID;
                    CompanyCode = oRequestParameter.Requestor.CompanyCode;
                    Year = oRequestParameter.InputParameter["EducationYear"].ToString();
                    RequestNo = oRequestParameter.InputParameter["RequestNo"].ToString();
                    Position_ID = oRequestParameter.Requestor.PositionID;
                    oWorkflowController.SetAuthenticate(oRequestParameter.Requestor);
                }

                var QuotaUsed = HRBEManagement.CreateInstance(CompanyCode.ToString()).BEFitnessAllowanceCheckQuota(Employee_ID, Year, RequestNo);

                EmployeeData oEmpData = new EmployeeData(Employee_ID, Position_ID, CompanyCode.ToString(), DateTime.Now);
                var oConfig = HRBEManagement.CreateInstance(CompanyCode.ToString()).GetBEFitnessAllowanceConfig(Employee_ID, oEmpData.DateSpecific.PassProbation, Year);
                var Config = oConfig[0];
                var Quota = Config.Quota;



                var result = Quota + "," + QuotaUsed;

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public string FitnessAllowanceCheckEnroll([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                var Employee_ID = "";
                var CompanyCode = "";

                if (oRequestParameter.CurrentEmployee.EmployeeID == oRequestParameter.Requestor.EmployeeID)
                {
                    Employee_ID = oRequestParameter.CurrentEmployee.EmployeeID;
                    CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
                }
                else
                {
                    Employee_ID = oRequestParameter.Requestor.EmployeeID;
                    CompanyCode = oRequestParameter.Requestor.CompanyCode;
                }

                var TResult = 0;
                TResult = HRBEManagement.CreateInstance(CompanyCode.ToString()).BEFitnessAllowanceCheckEnrollment(Employee_ID.ToString(), oRequestParameter.InputParameter["KeyYear"].ToString());
                return TResult.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion FitnessAllowance

        #region Cremation

        [HttpPost]
        public decimal CremationCompensateAmount([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                var Employee_ID = "";
                var CompanyCode = "";
                var RequestNo = "";

                if (oRequestParameter.CurrentEmployee.EmployeeID == oRequestParameter.Requestor.EmployeeID)
                {
                    Employee_ID = oRequestParameter.CurrentEmployee.EmployeeID;
                    CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
                    RequestNo = oRequestParameter.InputParameter["RequestNo"].ToString();
                }
                else
                {
                    Employee_ID = oRequestParameter.Requestor.EmployeeID;
                    CompanyCode = oRequestParameter.Requestor.CompanyCode;
                    RequestNo = oRequestParameter.InputParameter["RequestNo"].ToString();

                }


                decimal CompensateAmount = 0;
                List<INFOTYPE0008> oSalaryList = HRPAManagement.CreateInstance(CompanyCode.ToString()).GetSalaryList(Employee_ID, DateTime.Now);
                var Salary = oSalaryList[0].Salary;
                CompensateAmount = Salary * 3;

                return CompensateAmount;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public List<BEBereavementInfo> GetBereavementInfoHistory([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                WorkflowController oWorkflowController = new WorkflowController();

                var Employee_ID = "";
                var CompanyCode = "";

                if (oRequestParameter.CurrentEmployee.EmployeeID == oRequestParameter.Requestor.EmployeeID)
                {
                    Employee_ID = oRequestParameter.CurrentEmployee.EmployeeID;
                    CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
                    oWorkflowController.SetAuthenticate(oRequestParameter.CurrentEmployee);
                }
                else
                {
                    Employee_ID = oRequestParameter.Requestor.EmployeeID;
                    CompanyCode = oRequestParameter.Requestor.CompanyCode;
                    oWorkflowController.SetAuthenticate(oRequestParameter.Requestor);
                }

                List<BEBereavementInfo> TResult = new List<BEBereavementInfo>();
                TResult = HRBEManagement.CreateInstance(CompanyCode.ToString()).GetBereavementInfoHistory(Employee_ID.ToString());
                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public List<BEBereavementInfo> GetBereavementItem([FromBody]RequestParameter oRequestParameter)
        {
            try
            {
                WorkflowController oWorkflowController = new WorkflowController();

                var Employee_ID = "";
                var CompanyCode = "";

                if (oRequestParameter.CurrentEmployee.EmployeeID == oRequestParameter.Requestor.EmployeeID)
                {
                    Employee_ID = oRequestParameter.CurrentEmployee.EmployeeID;
                    CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
                    oWorkflowController.SetAuthenticate(oRequestParameter.CurrentEmployee);
                }
                else
                {
                    Employee_ID = oRequestParameter.Requestor.EmployeeID;
                    CompanyCode = oRequestParameter.Requestor.CompanyCode;
                    oWorkflowController.SetAuthenticate(oRequestParameter.Requestor);
                }

                List<BEBereavementInfo> TResult = new List<BEBereavementInfo>();
                TResult = HRBEManagement.CreateInstance(CompanyCode.ToString()).GetBereavementItem(Employee_ID.ToString(), oRequestParameter.InputParameter["RequestNo"].ToString(), oRequestParameter.InputParameter["KeyType"].ToString());
                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost]
        public string GetConfigImageBereavement([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                WorkflowController oWorkflowController = new WorkflowController();

                var Employee_ID = "";
                var CompanyCode = "";

                if (oRequestParameter.CurrentEmployee.EmployeeID == oRequestParameter.Requestor.EmployeeID)
                {
                    Employee_ID = oRequestParameter.CurrentEmployee.EmployeeID;
                    CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
                    oWorkflowController.SetAuthenticate(oRequestParameter.CurrentEmployee);
                }
                else
                {
                    Employee_ID = oRequestParameter.Requestor.EmployeeID;
                    CompanyCode = oRequestParameter.Requestor.CompanyCode;
                    oWorkflowController.SetAuthenticate(oRequestParameter.Requestor);
                }

                string TResult = ShareDataManagement.LookupCache(CompanyCode, "ESS.HR.BE", "CONFIGIMAGEBEREAVEMENT");

                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion Cremation



        #region Report

        [HttpPost]
        public void GetReport([FromBody] RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();

            var UpdateDate_From = DateTime.ParseExact(oRequestParameter.InputParameter["UpdateDate_From"].ToString(), "yyyy-MM-dd", enCL);
            var UpdateDate_To = DateTime.ParseExact(oRequestParameter.InputParameter["UpdateDate_To"].ToString(), "yyyy-MM-dd", enCL);
            var Type = oRequestParameter.InputParameter["Type"].ToString();
            var Status = oRequestParameter.InputParameter["Status"].ToString();
            var ReportName = oRequestParameter.InputParameter["ReportName"].ToString();
            var LanguageCode = oRequestParameter.InputParameter["LanguageCode"].ToString();
            var Employee_id = oRequestParameter.InputParameter["Employee_id"].ToString();
            var ExportType = oRequestParameter.InputParameter["ExportType"].ToString();
            var ExportName = oRequestParameter.InputParameter["EmployeeName"].ToString();
            var DateText = oRequestParameter.InputParameter["DateText"].ToString();
            var TimeText = oRequestParameter.InputParameter["TimeText"].ToString();
            var ExportDate = DateText + " " + DateTime.Now.ToString("dd/MM/yyyy") + " " + TimeText + " " + DateTime.Now.ToString("HH:mm");

            //Text Title
            var T_EMPLOYEEID = oRequestParameter.InputParameter["T_EMPLOYEEID"].ToString();
            var T_FULLNAME = oRequestParameter.InputParameter["T_FULLNAME"].ToString();
            var T_REQUESTNO = oRequestParameter.InputParameter["T_REQUESTNO"].ToString();
            var T_CATEGORYBENEFIT = oRequestParameter.InputParameter["T_CATEGORYBENEFIT"].ToString();
            var T_CREATEDATE = oRequestParameter.InputParameter["T_CREATEDATE"].ToString();
            var T_UPDATE = oRequestParameter.InputParameter["T_UPDATE"].ToString();
            var T_AMOUNT = oRequestParameter.InputParameter["T_AMOUNT"].ToString();
            var T_STATUS = oRequestParameter.InputParameter["T_STATUS"].ToString();
            var T_POSTSAP = oRequestParameter.InputParameter["T_POSTSAP"].ToString();
            var T_POSTMESSAGE = oRequestParameter.InputParameter["T_POSTMESSAGE"].ToString();


            string companyLogo = sCompanyCode;
            string pathImage = new Uri(System.Web.HttpContext.Current.Server.MapPath("~/Client/images/CompanyLogo/" + companyLogo + ".jpg")).AbsoluteUri;


            ReportObject oReportObject = new ReportObject();
            oReportObject.ExportType = ExportType;
            oReportObject.ReportName = System.Web.HttpContext.Current.Server.MapPath("~/Views/Report/HR/BEReport.rdlc");
            oReportObject.ExportFileName = System.Web.HttpContext.Current.Server.MapPath("~/Client/Report/BEReport" + Employee_id);

            DataSet ds = HRBEManagement.CreateInstance(sCompanyCode).GetReportData(UpdateDate_From, UpdateDate_To, Type, Status, LanguageCode);
            oReportObject.ReportParameters = new List<ReportParameter>()
                {
                     new ReportParameter("ReportName",ReportName)
                    ,new ReportParameter("ExportDate",ExportDate)
                    ,new ReportParameter("ExportName",ExportName)

                    ,new ReportParameter("T_EMPLOYEEID",T_EMPLOYEEID)
                    ,new ReportParameter("T_FULLNAME",T_FULLNAME)
                    ,new ReportParameter("T_REQUESTNO",T_REQUESTNO)
                    ,new ReportParameter("T_CATEGORYBENEFIT",T_CATEGORYBENEFIT)
                    ,new ReportParameter("T_CREATEDATE",T_CREATEDATE)
                    ,new ReportParameter("T_UPDATE",T_UPDATE)
                    ,new ReportParameter("T_AMOUNT",T_AMOUNT)
                    ,new ReportParameter("T_STATUS",T_STATUS)
                    ,new ReportParameter("T_POSTSAP",T_POSTSAP)
                    ,new ReportParameter("T_POSTMESSAGE",T_POSTMESSAGE)
                    ,new ReportParameter("ImageLogo",pathImage)
                };

            oReportObject.DataSource = ds;

            ReportViewer oReportViewer = new ReportViewer();
            oReportViewer.LocalReport.DataSources.Clear();
            oReportViewer.ProcessingMode = ProcessingMode.Local;
            oReportViewer.LocalReport.ReportPath = oReportObject.ReportName;
            oReportViewer.LocalReport.DataSources.Add(new ReportDataSource("ReportData", ds.Tables[0]));
            oReportViewer.LocalReport.EnableExternalImages = true;

            //try
            //{
            if (oReportObject.ReportParameters != null && oReportObject.ReportParameters.Count > 0)
            {
                oReportViewer.LocalReport.SetParameters(oReportObject.ReportParameters);
            }



            oReportViewer.ShowRefreshButton = false;
            ReportPageSettings oReportPageSettings = oReportViewer.LocalReport.GetDefaultPageSettings();
            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string filenameExtension;


            byte[] bytes = oReportViewer.LocalReport.Render(
                oReportObject.ExportType, null, out mimeType, out encoding, out filenameExtension,
                out streamids, out warnings);

            oReportObject.Warnings = warnings;
            oReportObject.Streamids = streamids;
            oReportObject.ContentType = mimeType;
            oReportObject.Encoding = encoding;
            oReportObject.FilenameExtension = filenameExtension;
            oReportObject.ExportFileName = string.Format("{0}.{1}", oReportObject.ExportFileName, filenameExtension);
            FileManager.SaveFile(oReportObject.ExportFileName, bytes, true);



            //}
            //catch (Exception ex)
            //{
            //    var Message = ex.Message;
            //    var InnerException = ex.InnerException.Message;
            //    var InnerException2 = ex.InnerException.InnerException.Message;
            //}




            //ReportObject oReportObject = new ReportObject();
            //ReportingServices oReportingServices = new ReportingServices();
            //oReportObject.ExportType = "PDF";
            //oReportObject.ReportName = "Views/Report/HR/BEReport.rdlc";
            //oReportObject.ExportFileName = string.Format("{0}\\{1}", @"D:\Project\GPSC\SourceCode\ESSClient\iSSWS\Client\files", "BEReport");
            //DataSet ds = HRBEManagement.CreateInstance(Requestor.CompanyCode).GetReportData(RequestNo);
            //oReportObject.ReportParameters = new List<ReportParameter>()
            //    {
            //         new ReportParameter("MyParam","HelloWorld Report!!")
            //};
            //oReportObject.DataSource = ds;
            //oReportingServices.ExportToFile(oReportObject);
        }




        #endregion Report




        [HttpPost]
        public List<BenefitReport> GetBenefitReport([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();

                var Employee_ID = oRequestParameter.CurrentEmployee.EmployeeID;
                var CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;

                List<BenefitReport> TResult = new List<BenefitReport>();

                var status_select = oRequestParameter.InputParameter["Benefit_Status"].ToString();
                var category_select = oRequestParameter.InputParameter["Benefit_Type"].ToString();
                var language = oRequestParameter.InputParameter["language"].ToString();

                if (!string.IsNullOrEmpty(status_select))
                {
                    status_select = status_select.Remove(status_select.Length - 1);
                }

                if (!string.IsNullOrEmpty(category_select))
                {
                    category_select = category_select.Remove(category_select.Length - 1);
                }
                //TResult = HRBEManagement.CreateInstance(CompanyCode.ToString()).GetBenefitReport(Convert.ToDateTime(oRequestParameter.InputParameter["UpdateDate_From"].ToString()), Convert.ToDateTime(oRequestParameter.InputParameter["UpdateDate_To"].ToString()), category_select, status_select, language);
                TResult = HRBEManagement.CreateInstance(sCompanyCode).GetBenefitReport(Convert.ToDateTime(oRequestParameter.InputParameter["UpdateDate_From"].ToString()), Convert.ToDateTime(oRequestParameter.InputParameter["UpdateDate_To"].ToString()), category_select, status_select, language);
                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public List<DataReligion> GetReligionAll([FromBody] RequestParameter oRequestParameter)
        {
            List<DataReligion> result = new List<DataReligion>();

            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;

            return result = HRBEManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
               .GetReligionAll();
        }



        #region รายงานการลงทะเบียนประกันสุขภาพ
        [HttpPost]
        public List<HealthInsAllowanceReport> GetHealthInsAllowanceReport([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();

                DateTime stdate = DateTime.Now;
                DateTime endate = DateTime.Now;

                List<HealthInsAllowanceReport> TResult = new List<HealthInsAllowanceReport>();

                //TResult = HRBEManagement.CreateInstance(CompanyCode.ToString()).GetBenefitReport(Convert.ToDateTime(oRequestParameter.InputParameter["UpdateDate_From"].ToString()), Convert.ToDateTime(oRequestParameter.InputParameter["UpdateDate_To"].ToString()), category_select, status_select, language);
                TResult = HRBEManagement.CreateInstance(sCompanyCode).GetHealthInsAllowanceReport(stdate, endate);
                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public HttpResponseMessage ExportReportHealthInsAllowanceByAdmin(RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            string list_str_welfare = oRequestParameter.InputParameter["ListCategoryWelfare"].ToString();
            DateTime begin_date = Convert.ToDateTime(oRequestParameter.InputParameter["BeginDate"].ToString());
            DateTime end_date = Convert.ToDateTime(oRequestParameter.InputParameter["EndDate"].ToString());


            




            // Split the string on line breaks.
            string[] lines = list_str_welfare.Split(new string[] { "," }, StringSplitOptions.None);

            if (lines.Count() <= 0)
            {
                throw new DataServiceException("REPORT_WELFARE_DETAIL", "EXEPTION_WELFARE_NULL");
            }

            XLWorkbook workbook = new XLWorkbook();
            HRBEManagement.CreateInstance(sCompanyCode).GetSheetExcelHealthInsAllowanceByAdmin(workbook, begin_date, end_date, oRequestParameter.Requestor.Language);


            if (lines.Count() > 0)
            {
                foreach (string key_welfare in lines)
                {
                    switch (key_welfare)
                    {
                        case "1": HRBEManagement.CreateInstance(sCompanyCode).GetSheetExcelEducationAllowance(workbook, begin_date, end_date, oRequestParameter.Requestor.Language); break;
                        case "2": HRBEManagement.CreateInstance(sCompanyCode).GetSheetExcelFitnessAllowance(workbook, begin_date, end_date); break;
                        case "3": HRBEManagement.CreateInstance(sCompanyCode).GetSheetExcelHealthInsAllowance(workbook, begin_date, end_date, oRequestParameter.Requestor.Language); break;
                        case "4": HRBEManagement.CreateInstance(sCompanyCode).GetSheetExcelBereavment(workbook, begin_date, end_date, oRequestParameter.Requestor.Language); break;
                        case "5": HRFBManagement.CreateInstance(sCompanyCode).GetSheetExcelReimburse(workbook, begin_date, end_date); break;
                        case "6": HRBEManagement.CreateInstance(sCompanyCode).GetSheetExcelDisbursementOfWelfare(workbook, begin_date, end_date, oRequestParameter.Requestor.Language); break;
                    }
                }
            }

            var stream = new MemoryStream();
            workbook.SaveAs(stream);
            stream.Position = 0;

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(stream.ToArray())
            };

            DateTime date_now = DateTime.Now;
            result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = "ReportOTLogByHour" + date_now.Year + date_now.Month + date_now.Date + date_now.Minute + date_now.Second + ".xlsx"
            };
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

            return result;
        }
        #endregion




        #region รายงานรายละเอียดการเบิกสวัสดิการสำหรับผู้ดูแลระบบ
        public HttpResponseMessage ExportReportDetailWelfareByAdmin(RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            string list_str_welfare = oRequestParameter.InputParameter["ListCategoryWelfare"].ToString();
            DateTime begin_date = Convert.ToDateTime(oRequestParameter.InputParameter["BeginDate"].ToString());
            DateTime end_date = Convert.ToDateTime(oRequestParameter.InputParameter["EndDate"].ToString());

            // Split the string on line breaks.
            string[] lines = list_str_welfare.Split(new string[] { "," }, StringSplitOptions.None);

            if (lines.Count() <= 0)
            {
                throw new DataServiceException("REPORT_WELFARE_DETAIL", "EXEPTION_WELFARE_NULL");
            }

            XLWorkbook workbook = new XLWorkbook();

            if (lines.Count() > 0)
            {
                foreach (string key_welfare in lines)
                {
                    switch (key_welfare)
                    {
                        case "1": HRBEManagement.CreateInstance(sCompanyCode).GetSheetExcelEducationAllowance(workbook, begin_date, end_date, oRequestParameter.Requestor.Language); break;
                        case "2": HRBEManagement.CreateInstance(sCompanyCode).GetSheetExcelFitnessAllowance(workbook, begin_date, end_date); break;
                        case "3": HRBEManagement.CreateInstance(sCompanyCode).GetSheetExcelHealthInsAllowance(workbook, begin_date, end_date, oRequestParameter.Requestor.Language); break;
                        case "4": HRBEManagement.CreateInstance(sCompanyCode).GetSheetExcelBereavment(workbook, begin_date, end_date, oRequestParameter.Requestor.Language); break;
                        case "5": HRFBManagement.CreateInstance(sCompanyCode).GetSheetExcelReimburse(workbook, begin_date, end_date); break;
                        case "6": HRBEManagement.CreateInstance(sCompanyCode).GetSheetExcelDisbursementOfWelfare(workbook, begin_date, end_date, oRequestParameter.Requestor.Language); break;
                    }
                }
            }

            var stream = new MemoryStream();
            workbook.SaveAs(stream);
            stream.Position = 0;

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(stream.ToArray())
            };

            DateTime date_now = DateTime.Now;
            result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = "ReportOTLogByHour" + date_now.Year + date_now.Month + date_now.Date + date_now.Minute + date_now.Second + ".xlsx"
            };
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

            return result;
        }
        #endregion

        #region Dashbaord สวัสดิการพนักงานและสิทธิผลประโยชน์

        [HttpPost]
        public ResponeVerifyPincode VerifyPincdeBenefitsEmployee([FromBody]RequestParameter oRequestParameter)
        {
            string sPincode = oRequestParameter.InputParameter["Pincode"].ToString();
            bool isPincodeVerify = false;

            ResponeVerifyPincode result = new ResponeVerifyPincode();

            isPincodeVerify = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                .VerifyPinCode(oRequestParameter.Requestor.EmployeeID, sPincode);

            if (isPincodeVerify)
            {
                List<int> list_year = HRBEManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).LoadYearEmployeeBenifits();
                result.ListYear.AddRange(list_year);
                result.IsVerifyPincode = true;
            }

            return result;
        }

        [HttpPost]
        public List<BenefitsAbsence> LoadAbsenceByEmployee([FromBody] RequestParameter oRequestParameter)
        {
            string sPincode = oRequestParameter.InputParameter["Pincode"].ToString();
            string sYear = oRequestParameter.InputParameter["Year"].ToString();

            bool isPincodeVerify = false;
            isPincodeVerify = EmployeeManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).VerifyPinCode(oRequestParameter.Requestor.EmployeeID, sPincode);


            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);
            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);
            List<BenefitsAbsence> result = new List<BenefitsAbsence>();
            if (isPincodeVerify)
            {
                string employeeId = oRequestParameter.Requestor.EmployeeID;

                List<BenefitsAbsence> list_absence = HRBEManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
                    .getSumQuotaAbsence(oEmp,sYear, employeeId);
                if (list_absence.Count > 0)
                {
                    
                    foreach (var item in list_absence)
                    {
                        item.TextCodeFull = oEmp.OrgAssignment.SubAreaSetting.AbsAttGrouping + "#" + item.TextCode;
                    }
                }
                result.AddRange(list_absence);
            }

            return result;
        }

        [HttpPost]
        public DashboardProvidentFundByEmployee LoadProvidentfundByEmployee([FromBody] RequestParameter oRequestParameter)
        {
            DashboardProvidentFundByEmployee dashboard_provident = new DashboardProvidentFundByEmployee();

            string sPincode = oRequestParameter.InputParameter["Pincode"].ToString();
            string sYear = oRequestParameter.InputParameter["Year"].ToString();
            bool isPincodeVerify = false;

            isPincodeVerify = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).VerifyPinCode(oRequestParameter.Requestor.EmployeeID, sPincode);

            if (isPincodeVerify)
            {
                dashboard_provident = HRBEManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
                    .GetInformationDashbaordProvidentfundByEmployee(sYear, oRequestParameter.Requestor.EmployeeID);

                if (dashboard_provident.IsHaveProvidentFund)
                {
                    int year_select = Convert.ToInt16(sYear);
                    DateTime date_now = DateTime.Now;

                    DateTime date_select = new DateTime(year_select, 12, 31);

                    DateTime date_working;
                    if (date_select.Date.Year == date_now.Date.Year)
                        date_working = DateTime.Now;
                    else
                        date_working = date_select;

                    // หาอายุงานของพนักงาน
                    RelatedAge EmpRelatedAge = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetRelatedAge(oRequestParameter.Requestor.EmployeeID, date_working);
                    string ageOfCompany = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GenAge(EmpRelatedAge.ageOfCompany, oRequestParameter.CurrentEmployee.Language);
                    dashboard_provident.TextWorkingAge1 = ageOfCompany; // text อายุงาน

                    // หา Rate % อัตราเงินสมทบของบริษัท
                    decimal percentageCompany = 0;
                    decimal working_age = Convert.ToDecimal(EmpRelatedAge.ageOfCompany.Zyear);
                    if(working_age == 0)
                    {
                        // อายุงานยังไม่ถึง 1 ปี
                        decimal start_percentageCompany = (decimal)0.1;
                        percentageCompany = HRBEManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPercentageWorkingAge(start_percentageCompany);
                    }
                    else if( working_age > 0)
                    {
                        // อายุงานมากกว่า 0 ปี
                        percentageCompany = HRBEManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPercentageWorkingAge(working_age);
                    }

                    dashboard_provident.TextContributionCompany1 = percentageCompany.ToString();

                    // หาเงินเดือนพนักงาน
                    decimal salary = HRBEManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAmountSalaryByEmployee(oRequestParameter.Requestor.EmployeeID, sYear);

                    //  ยอดเงินสมทบบริษัท =  เงินเดือน * Rate % อัตราเงินสมทบ ของบริษัท
                    dashboard_provident.AmountEmployee1 = salary * (percentageCompany / 100);

                    // หา Rate % พนักงานนำเงินสมทบกองทุนสำรองเลี้ยงชีพเอง
                    decimal rate_employee_saving = HRBEManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetRateProvidentfundEmployeeSaving(oRequestParameter.Requestor.EmployeeID, sYear);
                    dashboard_provident.TextContributionCompany2 = rate_employee_saving.ToString(); ;
                    dashboard_provident.AmountEmployee2 = salary * (rate_employee_saving / 100);
                }
            }
            return dashboard_provident;
        }

        [HttpPost]
        public TranasctionUseReimburseByEmployee LoadAlternativeWelfareByEmployee([FromBody] RequestParameter oRequestParameter)
        {
            TranasctionUseReimburseByEmployee result = new TranasctionUseReimburseByEmployee();

            string sPincode = oRequestParameter.InputParameter["Pincode"].ToString();
            string sYear = oRequestParameter.InputParameter["Year"].ToString();

            bool isPincodeVerify = false;
            isPincodeVerify = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).VerifyPinCode(oRequestParameter.Requestor.EmployeeID, sPincode);

            if (isPincodeVerify)
            {
                string employeeId = oRequestParameter.Requestor.EmployeeID;
                // 1. หา empsubgroup
                EmployeeINFOTYPE0001 emp_infotype0001 = EmployeeManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetDataEmployeeInfoType0001ByYear(employeeId, sYear);
                if (!string.IsNullOrEmpty(emp_infotype0001.EmpSubGroup))
                {
                    result = HRBEManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
                        .GetInformationDashboardUseReimburseByEmployee(sYear, emp_infotype0001.EmpSubGroup, employeeId);
                }
            }
            return result;
        }

        [HttpPost]
        public DashbaordEducationChildByEmployee LoadChildEducationByEmployee([FromBody] RequestParameter oRequestParameter)
        {
            DashbaordEducationChildByEmployee education = new DashbaordEducationChildByEmployee();
            string sPincode = oRequestParameter.InputParameter["Pincode"].ToString();
            string sYear = oRequestParameter.InputParameter["Year"].ToString();
            bool isPincodeVerify = false;

            isPincodeVerify = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).VerifyPinCode(oRequestParameter.Requestor.EmployeeID, sPincode);

            if (isPincodeVerify)
            {
                education = HRBEManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
                    .GetInformationDashbaordUseEducationChildByEmployee(sYear, oRequestParameter.Requestor.EmployeeID);
            }
            return education;
        }

        [HttpPost]
        public DashboardFitnessByEmployee LoadFitnessByEmployee([FromBody] RequestParameter oRequestParameter)
        {
            DashboardFitnessByEmployee dashbaord_fitness = new DashboardFitnessByEmployee();
            string sPincode = oRequestParameter.InputParameter["Pincode"].ToString();
            string sYear = oRequestParameter.InputParameter["Year"].ToString();
            bool isPincodeVerify = false;

            isPincodeVerify = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).VerifyPinCode(oRequestParameter.Requestor.EmployeeID, sPincode);

            if (isPincodeVerify)
            {
                dashbaord_fitness = HRBEManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
                    .GetInformationDasboardUseFitnessByEmployee(sYear, oRequestParameter.Requestor.EmployeeID);
            }

            return dashbaord_fitness;
        }

        [HttpPost]
        public DashboardHealthInsAllowanceParentByEmployee LoadHealthParentByEmployee([FromBody] RequestParameter oRequestParameter)
        {
            DashboardHealthInsAllowanceParentByEmployee dashboard_healthParent = new DashboardHealthInsAllowanceParentByEmployee();
            string sPincode = oRequestParameter.InputParameter["Pincode"].ToString();
            string sYear = oRequestParameter.InputParameter["Year"].ToString();
            bool isPincodeVerify = false;

            isPincodeVerify = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).VerifyPinCode(oRequestParameter.Requestor.EmployeeID, sPincode);

            if (isPincodeVerify)
            {
                dashboard_healthParent = HRBEManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
                    .GetInformationDashBoardHealthInsAllowanceParentByEmployee(sYear, oRequestParameter.Requestor.EmployeeID);
            }

            return dashboard_healthParent;
        }

        [HttpPost]
        public List<DashbaordOtherWelfare> LoadAnnounceWelfare([FromBody] RequestParameter oRequestParameter)
        {
            List<DashbaordOtherWelfare> list_AnnouncementText = new List<DashbaordOtherWelfare>();

            string sPincode = oRequestParameter.InputParameter["Pincode"].ToString();
            string sYear = oRequestParameter.InputParameter["Year"].ToString();
            bool isPincodeVerify = false;

            isPincodeVerify = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).VerifyPinCode(oRequestParameter.Requestor.EmployeeID, sPincode);

            if (isPincodeVerify)
            {
                list_AnnouncementText = HRBEManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
                    .GetInformationDashboardAnnouncementText();
            }
            return list_AnnouncementText;
        }

        [HttpPost]
        public DashboardIncomeYearByEmployee LoadAmountWorkingByEmployee([FromBody]RequestParameter oRequestParameter)
        {
            DashboardIncomeYearByEmployee income_employee = new DashboardIncomeYearByEmployee();

            string sPincode = oRequestParameter.InputParameter["Pincode"].ToString();
            string sYear = oRequestParameter.InputParameter["Year"].ToString();
            bool isPincodeVerify = false;

            isPincodeVerify = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).VerifyPinCode(oRequestParameter.Requestor.EmployeeID, sPincode);

            int year_convert = Convert.ToInt32(sYear);
            if (isPincodeVerify)
            {
                income_employee = HRBEManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
                    .GetInformationDashboardIncomEmployee(year_convert, oRequestParameter.Requestor.EmployeeID);
            }

            return income_employee;
        }

        [HttpPost]
        public Boolean GetIsActiveFlexibleBenefitDashboard([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                Boolean TResult = true;
                TResult = HRBEManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetIsActiveFlexibleBenefitDashboard();
                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public Boolean GetIsActiveHealthInsuranceDashboard([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                Boolean TResult = true;
                TResult = HRBEManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetIsActiveHealthInsuranceDashboard();
                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public Boolean GetIsActiveFlexibleBenefitDashboard_ByCompany([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                Boolean TResult = true;
                string sCompanyCode = oRequestParameter.InputParameter["SelectCompanyCode"].ToString();
                TResult = HRBEManagement.CreateInstance(sCompanyCode).GetIsActiveFlexibleBenefitDashboard();
                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public Boolean GetIsActiveHealthInsuranceDashboard_ByCompany([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                Boolean TResult = true;
                string sCompanyCode = oRequestParameter.InputParameter["SelectCompanyCode"].ToString();
                TResult = HRBEManagement.CreateInstance(sCompanyCode).GetIsActiveHealthInsuranceDashboard();
                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region HealthInsuranceConfig


        [HttpPost]
        public void SaveHealthInsuranceConfig([FromBody] RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();

            BEHealthInsConfig oHealthConfig = new BEHealthInsConfig();
            try
            {
                oHealthConfig.HealthConfigID = (oRequestParameter.InputParameter["HealthConfigID"] != null) ? Convert.ToInt16(oRequestParameter.InputParameter["HealthConfigID"].ToString()) : 0;
                oHealthConfig.Year = (oRequestParameter.InputParameter["Year"] != null) ? Convert.ToInt16(oRequestParameter.InputParameter["Year"].ToString()) : 0;
                oHealthConfig.Status = oRequestParameter.InputParameter["Status"].ToString();
                oHealthConfig.BeginDate = (oRequestParameter.InputParameter["BeginDate"] != null) ? oRequestParameter.InputParameter["BeginDate"].ToString() : "";
                oHealthConfig.EndDate = (oRequestParameter.InputParameter["EndDate"] != null) ? oRequestParameter.InputParameter["EndDate"].ToString() : "";

                HRBEManagement.CreateInstance(sCompanyCode).SaveHealthInsuranceConfig(oHealthConfig);
            }
            catch (Exception err)
            {
                throw err;
            }
        }
        [HttpPost]
        public List<BEHealthInsConfig> GetHealthInsuranceConfig([FromBody] RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            try
            {
                List<BEHealthInsConfig> oReturnData = new List<BEHealthInsConfig>();
                oReturnData = HRBEManagement.CreateInstance(sCompanyCode).GetHealthInsuranceConfig();
                return oReturnData;
            }
            catch (Exception err)
            {
                throw err;
            }
        }



        #endregion

    }
}