﻿using iSSWS.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using ESS.ANNOUNCEMENT.DATACLASS;
using ESS.ANNOUNCEMENT;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Text;

namespace iSSWS.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AnnouncementController : ApiController
    {
        private CultureInfo oCL = new CultureInfo("en-US");

        [HttpPost]
        public ClientAnnouncementGroupByAdmin GetGroupAnnouncement([FromBody]RequestParameter oRequestParameter)
        {
            ClientAnnouncementGroupByAdmin group_announcement = new ClientAnnouncementGroupByAdmin();

            string str_groupId = oRequestParameter.InputParameter["AnnouncementGroupId"].ToString();
            int groupId = Convert.ToInt16(str_groupId);

            List<AnnouncementGroupByAdmin> result = AnnouncementManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
                .GetGroupAnnoncementList(groupId);

            if (result.Count > 0)
            {
                for (int i = 0; i < result.Count; i++)
                {
                    if (i % 2 == 0)
                        group_announcement.LeftReport.Add(result[i]);
                    else
                        group_announcement.RightReport.Add(result[i]);
                }
            }
            return group_announcement;
        }

        [HttpPost]
        public List<AnnouncementStatus> GetAnnouncementStatus(RequestParameter oRequestParameter)
        {

            string sComapny = oRequestParameter.InputParameter["CompanyCode"].ToString();

            var result = new List<AnnouncementStatus>();

            result = AnnouncementManagement.CreateInstance(sComapny).GetListAnnouncementStatus();

            return result;
        }

        #region ประกาศข้อความ

        [HttpPost]
        public void CreateAnnouncementText([FromBody]RequestParameter oRequestParameter)
        {
            string Subject_TH = oRequestParameter.InputParameter["Subject_TH"].ToString();
            string HtmlEditor_TH = oRequestParameter.InputParameter["HtmlEditor_TH"].ToString();
            string Subject_EN = oRequestParameter.InputParameter["Subject_EN"].ToString();
            string HtmlEditor_EN = oRequestParameter.InputParameter["HtmlEditor_EN"].ToString();
            string EffectiveDate = oRequestParameter.InputParameter["EffectiveDate"].ToString();
            string ExpireDate = oRequestParameter.InputParameter["ExpireDate"].ToString();
            string IsUnlimitTime = oRequestParameter.InputParameter["IsUnlimitTime"].ToString();
            string Sequence = oRequestParameter.InputParameter["Sequence"].ToString();

            DateTime effective_date = DateTime.ParseExact(EffectiveDate, "yyyy-MM-dd", oCL);
            DateTime expire_date = DateTime.ParseExact(ExpireDate, "yyyy-MM-dd", oCL);
            bool isUnlimitTime = Convert.ToBoolean(IsUnlimitTime);
            int sequence = Convert.ToInt16(Sequence);
            string sCompany = oRequestParameter.InputParameter["CompanyCode"].ToString();

            // AnnouncementTypeId  1 = ข่าวหลัก , 2 = ข่าวประชาสัมพันธ์ , 3 = ข่าวข้อความ
            // AnnouncementStatusId  1 = แสดงข่าว , 2 = ซ่อนข่าว
            AnnouncementTextByAdmin model = new AnnouncementTextByAdmin()
            {
                AnnouncementTypeId = 3,
                AnnouncementStatusId = 1,
                Subject_TH = Subject_TH,
                HtmlEditor_TH = HtmlEditor_TH,
                Subject_EN = Subject_EN,
                HtmlEditor_EN = HtmlEditor_EN,
                EffectiveDate = effective_date,
                ExpireDate = expire_date,
                CreateBy = oRequestParameter.Requestor.EmployeeID,
                IsUnlimitTime = isUnlimitTime,
                Sequence = sequence
            };

            AnnouncementManagement.CreateInstance(sCompany).SaveAnnouncementTextByAdmin(model);
        }

        [HttpPost]
        public void UpdateAnnouncementText([FromBody]RequestParameter oRequestParameter)
        {
            string Subject_TH = oRequestParameter.InputParameter["Subject_TH"].ToString();
            string HtmlEditor_TH = oRequestParameter.InputParameter["HtmlEditor_TH"].ToString();
            string Subject_EN = oRequestParameter.InputParameter["Subject_EN"].ToString();
            string HtmlEditor_EN = oRequestParameter.InputParameter["HtmlEditor_EN"].ToString();
            string EffectiveDate = oRequestParameter.InputParameter["EffectiveDate"].ToString();
            string ExpireDate = oRequestParameter.InputParameter["ExpireDate"].ToString();
            string IsUnlimitTime = oRequestParameter.InputParameter["IsUnlimitTime"].ToString();
            string Sequence = oRequestParameter.InputParameter["Sequence"].ToString();
            string AnnouncementId = oRequestParameter.InputParameter["AnnouncementId"].ToString();
            string AnnouncementStatusId = oRequestParameter.InputParameter["AnnouncementStatusId"].ToString();
            string AnnouncementTypeId = oRequestParameter.InputParameter["AnnouncementTypeId"].ToString();

            DateTime effective_date = Convert.ToDateTime(EffectiveDate);
            DateTime expire_date = Convert.ToDateTime(ExpireDate);
            bool isUnlimitTime = Convert.ToBoolean(IsUnlimitTime);
            int sequence = Convert.ToInt16(Sequence);
            int announcementId = Convert.ToInt16(AnnouncementId);
            int announcementStatusId = Convert.ToInt16(AnnouncementStatusId);
            int announcementTypeId = Convert.ToInt16(AnnouncementTypeId);
            string sComapny = oRequestParameter.InputParameter["CompanyCode"].ToString();

            EditAnnouncementTextByAdmin data_edit = new EditAnnouncementTextByAdmin()
            {
                AnnouncementId = announcementId,
                AnnouncementStatusId = announcementStatusId,
                AnnouncementTypeId = announcementTypeId,
                Description_EN = HtmlEditor_EN,
                Description_TH = HtmlEditor_TH,
                EffectiveDate = effective_date,
                ExpireDate = expire_date,
                IsUnlimitedExpire = isUnlimitTime,
                Remark_EN = "",
                Remark_TH = "",
                Sequence = sequence,
                Title_EN = Subject_EN,
                Title_TH = Subject_TH,
                UpdateBy = oRequestParameter.Requestor.EmployeeID
            };
            AnnouncementManagement.CreateInstance(sComapny).EditDataAnnouncementTextByAdmin(data_edit);
        }

        [HttpPost]
        public List<DisplayAnnouncementTextByAdmin> GetAllGetAnnouncementTextByAdmin([FromBody]RequestParameter oRequestParameter)
        {
            string sCompany = oRequestParameter.InputParameter["CompanyCode"].ToString();

            var result = new List<DisplayAnnouncementTextByAdmin>();

            result = AnnouncementManagement.CreateInstance(sCompany).GetAllAnnouncementTextByAdmin();

            return result;
        }

        [HttpPost]
        public EditAnnouncementTextByAdmin GetDataEditAnnouncementTextByAdmin([FromBody]RequestParameter oRequestParameter)
        {
            string announcementId = oRequestParameter.InputParameter["AnnouncementId"].ToString();
            string sComapny = oRequestParameter.InputParameter["CompanyCode"].ToString();
            var data = new EditAnnouncementTextByAdmin();
            int key = 0;
            if (Int32.TryParse(announcementId, out key))
            {
                data = AnnouncementManagement.CreateInstance(sComapny).GetEditAnnouncementTextByAdmin(key);
            }
            return data;
        }

        [HttpPost]
        public void DeleteAnnouncementTextByAdmin([FromBody]RequestParameter oRequestParameter)
        {
            string announcementId = oRequestParameter.InputParameter["AnnouncementId"].ToString();
            string sCompany = oRequestParameter.InputParameter["CompanyCode"].ToString();
            int key = 0;
            if (Int32.TryParse(announcementId, out key))
            {
                AnnouncementManagement.CreateInstance(sCompany).DeleteAnnouncementTextByAdmin(key);
            }
        }

        public class DataSequence : RequestParameter
        {
            public DataSequence()
            {
                list_sequence = new List<ListDataSequence>();
            }
            public List<ListDataSequence> list_sequence { get; set; }
        }
        public class ListDataSequence
        {
            public int AnnouncementId { get; set; }
            public int Sequence { get; set; }
        }

        [HttpPost]
        public void UpdateSequenceAnnouncementTextByAdmin([FromBody]DataSequence oRequestParameter)
        {
            string companyCode = oRequestParameter.InputParameter["CompanyCode"].ToString();
            if (oRequestParameter.list_sequence.Count > 0)
            {
                var list_data = new List<DataEditSequenceAnnouncementTextByAdmin>();
                foreach (var item in oRequestParameter.list_sequence)
                {
                    var data = new DataEditSequenceAnnouncementTextByAdmin()
                    {
                        AnnouncementId = item.AnnouncementId,
                        Sequence = item.Sequence
                    };
                    list_data.Add(data);
                }
                AnnouncementManagement.CreateInstance(companyCode).UpdateSequenceAnnouncementTextByAdmin(list_data);
            }
        }

        #endregion

        #region ประกาศประชาสัมพันธ์
        [HttpPost]
        public HttpResponseMessage SaveAnnouncementPublicRelations([FromBody]RequestParameter oRequestParameter)
        {
            try
            {
                string title_th = oRequestParameter.InputParameter["Title_TH"].ToString();
                string title_en = oRequestParameter.InputParameter["Title_EN"].ToString();
                string link = oRequestParameter.InputParameter["Link"].ToString();
                DateTime effective_date = DateTime.Parse(oRequestParameter.InputParameter["EffectiveDate"].ToString(), oCL);
                DateTime expire_date = DateTime.Parse(oRequestParameter.InputParameter["ExpireDate"].ToString(), oCL);
                bool isUnlimitExpireDate = bool.Parse(oRequestParameter.InputParameter["IsUnlimitExpireDate"].ToString());
                int sequence = int.Parse(oRequestParameter.InputParameter["Sequence"].ToString());
                string emoloyeeId = oRequestParameter.InputParameter["EmployeeId"].ToString();
                string companyCode = oRequestParameter.InputParameter["CompanyCode"].ToString();
                string file_th = oRequestParameter.InputParameter["File_TH"].ToString();
                string file_en = oRequestParameter.InputParameter["File_EN"].ToString();

                AnnouncementDataAll model = new AnnouncementDataAll()
                {
                    Title_TH = title_th,
                    Title_EN = title_en,
                    TextLink = link,
                    EffectiveDate = effective_date,
                    ExpireDate = expire_date,
                    IsUnlimitedExpire = isUnlimitExpireDate,
                    Sequence = sequence,
                    AnnouncementStatusId = 1,
                    AnnouncementTypeId = 2,
                    CreateBy = emoloyeeId,
                    IsDelete = false
                };

                if (!string.IsNullOrEmpty(file_th))
                {
                    AnnouncementFile model_file = new AnnouncementFile()
                    {
                        TextFile = file_th,
                        Sequence = 1,
                        Language = "TH"
                    };
                    model.ListFile.Add(model_file);
                }

                if (!string.IsNullOrEmpty(file_en))
                {
                    AnnouncementFile model_file = new AnnouncementFile()
                    {
                        TextFile = file_en,
                        Sequence = 1,
                        Language = "EN"
                    };
                    model.ListFile.Add(model_file);
                }


                AnnouncementManagement.CreateInstance(companyCode).SaveAnnouncementPublicRelations(model);

                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            catch (System.Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        [HttpPost]
        public List<AnnouncementDataAll> GetAllAnnouncementPublicRelations([FromBody]RequestParameter oRequestParameter)
        {
            string sComapny = oRequestParameter.InputParameter["CompanyCode"].ToString();

            var list_AnnouncementPublicRelations = new List<AnnouncementDataAll>();
            list_AnnouncementPublicRelations = AnnouncementManagement.CreateInstance(sComapny).GetAllAnnouncementPublicRelations();
            return list_AnnouncementPublicRelations;
        }

        [HttpPost]
        public void EditSequenceAnnouncementPublicRelations([FromBody]DataSequence oRequestParameter)
        {
            string sComapny = oRequestParameter.InputParameter["CompanyCode"].ToString();

            if (oRequestParameter.list_sequence.Count > 0)
            {
                var list_data = new List<DataEditSequenceAnnouncementPublicRelations>();
                foreach (var item in oRequestParameter.list_sequence)
                {
                    var data = new DataEditSequenceAnnouncementPublicRelations()
                    {
                        AnnouncementId = item.AnnouncementId,
                        Sequence = item.Sequence
                    };
                    list_data.Add(data);
                }
                AnnouncementManagement.CreateInstance(sComapny).EditSequenceAnnouncementPublicRelations(list_data);
            }
        }

        [HttpPost]
        public void DeleteAnnouncementPublicRelations([FromBody]RequestParameter oRequestParameter)
        {
            string announcementId = oRequestParameter.InputParameter["AnnouncementId"].ToString();
            string sComapany = oRequestParameter.InputParameter["CompanyCode"].ToString();
            int key = 0;
            if (Int32.TryParse(announcementId, out key))
            {
                AnnouncementManagement.CreateInstance(sComapany).DeleteAnnouncementPublicRelations(key);
            }
        }

        [HttpPost]
        public AnnouncementDataAll GetAnnouncementPublicRelationsById([FromBody]RequestParameter oRequestParameter)
        {
            var result = new AnnouncementDataAll();
            string announcementId = oRequestParameter.InputParameter["AnnouncementId"].ToString();
            string sCompany = oRequestParameter.InputParameter["CompanyCode"].ToString();
            int key = 0;
            if (Int32.TryParse(announcementId, out key))
            {
                result = AnnouncementManagement.CreateInstance(sCompany).GetAnnouncementPublicRelationsById(key);
            }
            return result;
        }

        [HttpPost]
        public void EditAnnouncementPublicRelations([FromBody]RequestParameter oRequestParameter)
        {
            string sCompany = oRequestParameter.InputParameter["CompanyCode"].ToString();

            AnnouncementPublicRelations model = new AnnouncementPublicRelations()
            {
                Title_TH = oRequestParameter.InputParameter["Title_TH"].ToString(),
                Title_EN = oRequestParameter.InputParameter["Title_EN"].ToString(),
                TextLink = oRequestParameter.InputParameter["TextLink"].ToString(),
                EffectiveDate = Convert.ToDateTime(oRequestParameter.InputParameter["EffectiveDate"]),
                ExpireDate = Convert.ToDateTime(oRequestParameter.InputParameter["ExpireDate"]),
                IsUnlimitedExpire = Convert.ToBoolean(oRequestParameter.InputParameter["IsUnlimitTime"]),
                Sequence = Convert.ToInt32(oRequestParameter.InputParameter["Sequence"]),
                AnnouncementId = Convert.ToInt32(oRequestParameter.InputParameter["AnnouncementId"]),
                AnnouncementStatusId = Convert.ToInt32(oRequestParameter.InputParameter["AnnouncementStatusId"]),
                AnnouncementTypeId = 2,
                UpdateBy = oRequestParameter.Requestor.EmployeeID,
                AnnouncementFileId = 0,
                TextFile = "",
                File_TH_Id = Convert.ToInt32(oRequestParameter.InputParameter["File_TH_Id"]),
                File_TH = oRequestParameter.InputParameter["File_TH"].ToString(),
                File_EN_Id = Convert.ToInt32(oRequestParameter.InputParameter["File_EN_Id"]),
                File_EN = oRequestParameter.InputParameter["File_EN"].ToString()
            };

            AnnouncementManagement.CreateInstance(sCompany).EditAnnouncementPublicRelations(model);
        }

        [HttpPost]
        public HttpResponseMessage UploadFileAnnouncementPublicRelations()
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);

            //Create the Directory.
            string path = HttpContext.Current.Server.MapPath("~/Uploads/AnnouncementPublicRelations/");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            string data_time = DateTime.Now.ToString("yyyyMMddHHmmss");
            string new_name = "";
            foreach (string key in HttpContext.Current.Request.Files)
            {
                HttpPostedFile postedFile = HttpContext.Current.Request.Files[key];

                postedFile.SaveAs(path + postedFile.FileName);

                string before_name = path + postedFile.FileName;

                // Split String เอา Type File
                string[] str_split = postedFile.FileName.Split('.');
                if (str_split.Count() > 0)
                {
                    string typefile = str_split[str_split.Count() - 1];

                    new_name = "AnnouncementPublic_" + data_time + "." + typefile;
                    string new_name_full_path = path + new_name;

                    //// Rename
                    System.IO.File.Move(before_name, new_name_full_path);
                }
            }

            return new HttpResponseMessage()
            {
                //Content = new StringContent(JsonConvert.SerializeObject(result), Encoding.UTF8, "application/json"),
                Content = new StringContent(new_name),
                StatusCode = HttpStatusCode.OK
            };
        }
        #endregion

        #region ประกาศหลัก

        [HttpPost]
        public HttpResponseMessage UploadFileAnnouncementMain()
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);

            //Create the Directory.
            string path = HttpContext.Current.Server.MapPath("~/Uploads/AnnouncementMain/");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            string data_time = DateTime.Now.ToString("yyyyMMddHHmmss");
            string new_name = "";
            foreach (string key in HttpContext.Current.Request.Files)
            {
                HttpPostedFile postedFile = HttpContext.Current.Request.Files[key];

                postedFile.SaveAs(path + postedFile.FileName);

                string before_name = path + postedFile.FileName;

                // Split String เอา Type File
                string[] str_split = postedFile.FileName.Split('.');
                if (str_split.Count() > 0)
                {
                    string typefile = str_split[str_split.Count() - 1];

                    new_name = "AnnouncementMain_" + data_time + "." + typefile;
                    string new_name_full_path = path + new_name;

                    //// Rename
                    System.IO.File.Move(before_name, new_name_full_path);
                }
            }

            return new HttpResponseMessage()
            {
                //Content = new StringContent(JsonConvert.SerializeObject(result), Encoding.UTF8, "application/json"),
                Content = new StringContent(new_name),
                StatusCode = HttpStatusCode.OK
            };
        }

        public class DataAnnouncementMain : RequestParameter
        {
            public DataAnnouncementMain()
            {
                ListPicture_TH = new List<DataFileAnnouncementMain>();
                ListPicture_EN = new List<DataFileAnnouncementMain>();
            }

            public List<DataFileAnnouncementMain> ListPicture_TH { get; set; }
            public List<DataFileAnnouncementMain> ListPicture_EN { get; set; }
        }
        public class DataFileAnnouncementMain
        {
            public int AnnouncementFileId { get; set; }
            public string FileName { get; set; }
            public int FileSequence { get; set; }
            public string Language { get; set; }
        }
        [HttpPost]
        public void SaveAnnouncementMainByAdmin([FromBody]DataAnnouncementMain oRequestParameter)
        {
            string title_th = oRequestParameter.InputParameter["Title_TH"].ToString();
            string description_TH = oRequestParameter.InputParameter["Description_TH"].ToString();
            string remark_th = oRequestParameter.InputParameter["Remark_TH"].ToString();
            string title_en = oRequestParameter.InputParameter["Title_EN"].ToString();
            string description_en = oRequestParameter.InputParameter["Description_EN"].ToString();
            string remark_en = oRequestParameter.InputParameter["Remark_EN"].ToString();
            DateTime effectiveDate = Convert.ToDateTime(oRequestParameter.InputParameter["EffectiveDate"]);
            DateTime expireDate = Convert.ToDateTime(oRequestParameter.InputParameter["ExpireDate"]);
            bool isUnlimitedExpire = Convert.ToBoolean(oRequestParameter.InputParameter["IsUnlimitedExpire"]);
            string sComapny = oRequestParameter.InputParameter["CompanyCode"].ToString();

            AnnouncementDataAll data = new AnnouncementDataAll()
            {
                Title_TH = title_th,
                Description_TH = description_TH,
                Remark_TH = remark_th,
                Title_EN = title_en,
                Description_EN = description_en,
                Remark_EN = remark_en,
                EffectiveDate = effectiveDate,
                ExpireDate = expireDate,
                IsUnlimitedExpire = isUnlimitedExpire,
                AnnouncementStatusId = 1,
                AnnouncementTypeId = 1,
                CreateBy = oRequestParameter.Requestor.EmployeeID,
            };

            var picture_new_th = oRequestParameter.ListPicture_TH.Where(s => s.FileName != "").ToList();
            var picture_new_en = oRequestParameter.ListPicture_EN.Where(s => s.FileName != "").ToList();

            if (picture_new_th.Count > 0)
            {
                foreach (var item in picture_new_th)
                {
                    AnnouncementFile file = new AnnouncementFile()
                    {
                        TextFile = item.FileName,
                        Sequence = item.FileSequence,
                        Language = item.Language
                    };
                    data.ListFile.Add(file);
                }
            }

            if (picture_new_en.Count > 0)
            {
                foreach (var item in picture_new_en)
                {
                    AnnouncementFile file = new AnnouncementFile()
                    {
                        TextFile = item.FileName,
                        Sequence = item.FileSequence,
                        Language = item.Language
                    };
                    data.ListFile.Add(file);
                }
            }

            AnnouncementManagement.CreateInstance(sComapny).SaveAnnouncementMainByAdmin(data);
        }

        [HttpPost]
        public PaginationAnnouncementMain GetDataPaginationAnnouncementMain([FromBody]RequestParameter oRequestParameter)
        {
            int page = Convert.ToInt32(oRequestParameter.InputParameter["Page"]);
            int itemPerPage = Convert.ToInt32(oRequestParameter.InputParameter["ItemPerPage"]);
            string sComapny = oRequestParameter.InputParameter["CompanyCode"].ToString();

            PaginationAnnouncementMain result = new PaginationAnnouncementMain();

            result = AnnouncementManagement.CreateInstance(sComapny).GetPaginationAnnouncementMain(page, itemPerPage);

            return result;
        }

        [HttpPost]
        public void DeleteAnnouncementMainByAdmin([FromBody]RequestParameter oRequestParameter)
        {
            string announcementId = oRequestParameter.InputParameter["AnnouncementId"].ToString();
            string sCompany = oRequestParameter.InputParameter["CompanyCode"].ToString();
            int key = 0;
            if (Int32.TryParse(announcementId, out key))
            {
                AnnouncementManagement.CreateInstance(sCompany).DeleteAnnouncementMainByAdmin(key);
            }
        }

        [HttpPost]
        public DataAnnouncementMainEdit GetAnnouncementMainById([FromBody]RequestParameter oRequestParameter)
        {
            DataAnnouncementMainEdit result = new DataAnnouncementMainEdit();

            string announcementId = oRequestParameter.InputParameter["AnnouncementId"].ToString();
            string sCompany = oRequestParameter.InputParameter["CompanyCode"].ToString();
            int key = 0;
            if (Int32.TryParse(announcementId, out key))
            {
                result = AnnouncementManagement.CreateInstance(sCompany).GetAnnouncementMainById(key);
            }

            return result;
        }

        [HttpPost]
        public void EditAnnouncementMainByAdmin([FromBody]DataAnnouncementMain oRequestParameter)
        {
            int announcementId = Convert.ToInt32(oRequestParameter.InputParameter["AnnouncementId"]);
            int announcementStatusId = Convert.ToInt32(oRequestParameter.InputParameter["AnnouncementStatusId"]);
            string title_th = oRequestParameter.InputParameter["Title_TH"].ToString();
            string description_TH = oRequestParameter.InputParameter["Description_TH"].ToString();
            string remark_th = oRequestParameter.InputParameter["Remark_TH"].ToString();
            string title_en = oRequestParameter.InputParameter["Title_EN"].ToString();
            string description_en = oRequestParameter.InputParameter["Description_EN"].ToString();
            string remark_en = oRequestParameter.InputParameter["Remark_EN"].ToString();
            DateTime effectiveDate = Convert.ToDateTime(oRequestParameter.InputParameter["EffectiveDate"]);
            DateTime expireDate = Convert.ToDateTime(oRequestParameter.InputParameter["ExpireDate"]);
            bool isUnlimitedExpire = Convert.ToBoolean(oRequestParameter.InputParameter["IsUnlimitedExpire"]);
            string sCompany = oRequestParameter.InputParameter["CompanyCode"].ToString();

            AnnouncementDataAll data = new AnnouncementDataAll()
            {
                AnnouncementId = announcementId,
                Title_TH = title_th,
                Description_TH = description_TH,
                Remark_TH = remark_th,
                Title_EN = title_en,
                Description_EN = description_en,
                Remark_EN = remark_en,
                EffectiveDate = effectiveDate,
                ExpireDate = expireDate,
                IsUnlimitedExpire = isUnlimitedExpire,
                AnnouncementStatusId = announcementStatusId,
                AnnouncementTypeId = 1,  // ประเภทข่าวหลัก
                UpdateBy = oRequestParameter.Requestor.EmployeeID,
                CreateBy = oRequestParameter.Requestor.EmployeeID
            };

            var picture_new_th = oRequestParameter.ListPicture_TH.Where(s => s.FileName != "").ToList();
            var picture_new_en = oRequestParameter.ListPicture_EN.Where(s => s.FileName != "").ToList();

            if (picture_new_th.Count > 0)
            {
                foreach (var item in picture_new_th)
                {
                    AnnouncementFile file = new AnnouncementFile()
                    {
                        AnnouncementFileId = item.AnnouncementFileId,
                        TextFile = item.FileName,
                        Sequence = item.FileSequence,
                        Language = item.Language
                    };
                    data.ListFile.Add(file);
                }
            }

            if (picture_new_en.Count > 0)
            {
                foreach (var item in picture_new_en)
                {
                    AnnouncementFile file = new AnnouncementFile()
                    {
                        AnnouncementFileId = item.AnnouncementFileId,
                        TextFile = item.FileName,
                        Sequence = item.FileSequence,
                        Language = item.Language
                    };
                    data.ListFile.Add(file);
                }
            }

            AnnouncementManagement.CreateInstance(sCompany).EditAnnouncementMainById(data);
        }

        #endregion

        #region ประกาศสวัสดิการอื่นๆ

        [HttpPost]
        public int GetMaxSequence([FromBody]RequestParameter oRequestParameter)
        {
            string sCompany = oRequestParameter.InputParameter["CompanyCode"].ToString();

            int sequence = 0;

            sequence = AnnouncementManagement.CreateInstance(sCompany).GetMaxGetMaxSequenceWelfareAnnouncement();

            return sequence;
        }

        [HttpPost]
        public HttpResponseMessage UploadFileAnnouncementGeneralEmployeeWelfare()
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);

            //Create the Directory.
            string path = HttpContext.Current.Server.MapPath("~/Uploads/EmployeeWelfare/");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            string data_time = DateTime.Now.ToString("yyyyMMddHHmmss");
            string new_name = "";
            foreach (string key in HttpContext.Current.Request.Files)
            {
                HttpPostedFile postedFile = HttpContext.Current.Request.Files[key];

                postedFile.SaveAs(path + postedFile.FileName);

                string before_name = path + postedFile.FileName;

                // Split String เอา Type File
                string[] str_split = postedFile.FileName.Split('.');
                if (str_split.Count() > 0)
                {
                    string typefile = str_split[str_split.Count() - 1];

                    new_name = "EmployeeWelfare_" + data_time + "." + typefile;
                    string new_name_full_path = path + new_name;

                    //// Rename
                    System.IO.File.Move(before_name, new_name_full_path);
                }
            }

            return new HttpResponseMessage()
            {
                //Content = new StringContent(JsonConvert.SerializeObject(result), Encoding.UTF8, "application/json"),
                Content = new StringContent(new_name),
                StatusCode = HttpStatusCode.OK
            };
        }

        [HttpPost]
        public void SaveAnnouncementGeneralEmployeeWelfareByAdmin([FromBody]DataAnnouncementMain oRequestParameter)
        {
            string title_th = oRequestParameter.InputParameter["Title_TH"].ToString();
            string description_TH = oRequestParameter.InputParameter["Description_TH"].ToString();
            string remark_th = oRequestParameter.InputParameter["Remark_TH"].ToString();
            string title_en = oRequestParameter.InputParameter["Title_EN"].ToString();
            string description_en = oRequestParameter.InputParameter["Description_EN"].ToString();
            string remark_en = oRequestParameter.InputParameter["Remark_EN"].ToString();
            DateTime effectiveDate = Convert.ToDateTime(oRequestParameter.InputParameter["EffectiveDate"]);
            DateTime expireDate = Convert.ToDateTime(oRequestParameter.InputParameter["ExpireDate"]);
            bool isUnlimitedExpire = Convert.ToBoolean(oRequestParameter.InputParameter["IsUnlimitedExpire"]);
            int sequence = Convert.ToInt32(oRequestParameter.InputParameter["Sequence"]);
            string sCompany = oRequestParameter.InputParameter["CompanyCode"].ToString();

            AnnouncementDataAll data = new AnnouncementDataAll()
            {
                Sequence = sequence,
                Title_TH = title_th,
                Description_TH = description_TH,
                Remark_TH = remark_th,
                Title_EN = title_en,
                Description_EN = description_en,
                Remark_EN = remark_en,
                EffectiveDate = effectiveDate,
                ExpireDate = expireDate,
                IsUnlimitedExpire = isUnlimitedExpire,
                AnnouncementStatusId = 1,
                AnnouncementTypeId = 1,
                CreateBy = oRequestParameter.Requestor.EmployeeID,
            };

            var picture_welfare_th = oRequestParameter.ListPicture_TH.Where(s => s.FileName != "").ToList();
            var picture_welfare_en = oRequestParameter.ListPicture_EN.Where(s => s.FileName != "").ToList();

            if (picture_welfare_th.Count > 0)
            {
                foreach (var item in picture_welfare_th)
                {
                    AnnouncementFile file = new AnnouncementFile()
                    {
                        TextFile = item.FileName,
                        Sequence = item.FileSequence,
                        Language = item.Language
                    };
                    data.ListFile.Add(file);
                }
            }

            if (picture_welfare_en.Count > 0)
            {
                foreach (var item in picture_welfare_en)
                {
                    AnnouncementFile file = new AnnouncementFile()
                    {
                        TextFile = item.FileName,
                        Sequence = item.FileSequence,
                        Language = item.Language
                    };
                    data.ListFile.Add(file);
                }
            }
            AnnouncementManagement.CreateInstance(sCompany).SaveAnnouncementEmployeeWelfareByAdmin(data);
        }

        [HttpPost]
        public List<AnnouncementDataAll> GetAnnouncementGeneralEmployeeWelfareByAdmin([FromBody]RequestParameter oRequestParameter)
        {
            string sCompany = oRequestParameter.InputParameter["CompanyCode"].ToString();
            var result = new List<AnnouncementDataAll>();

            result = AnnouncementManagement.CreateInstance(sCompany).GetAllAnnouncementWelfareByAdmin();

            return result;
        }

        [HttpPost]
        public void DeleteAnnouncementGeneralEmployeeWelfareByAdmin([FromBody]RequestParameter oRequestParameter)
        {
            string announcementId = oRequestParameter.InputParameter["AnnouncementId"].ToString();
            string sCompany = oRequestParameter.InputParameter["CompanyCode"].ToString();
            int key = 0;
            if (Int32.TryParse(announcementId, out key))
            {
                AnnouncementManagement.CreateInstance(sCompany).DeleteAnnouncementWelfareByAdmin(key);
            }
        }

        [HttpPost]
        public void EditSequenceAnnouncementGeneralEmployeeWelfareByAdmin([FromBody]DataSequence oRequestParameter)
        {
            string sCompany = oRequestParameter.InputParameter["CompanyCode"].ToString();
            if (oRequestParameter.list_sequence.Count > 0)
            {
                var list_data = new List<DataEditSequenceAnnouncementPublicRelations>();
                foreach (var item in oRequestParameter.list_sequence)
                {
                    var data = new DataEditSequenceAnnouncementPublicRelations()
                    {
                        AnnouncementId = item.AnnouncementId,
                        Sequence = item.Sequence
                    };
                    list_data.Add(data);
                }
                AnnouncementManagement.CreateInstance(sCompany).EditSequenceAnnouncementWelfareByAdmin(list_data);
            }
        }

        [HttpPost]
        public DataAnnouncementEmployeeWelfare GetAnnouncementEmployeeWelfareById([FromBody]RequestParameter oRequestParameter)
        {
            DataAnnouncementEmployeeWelfare result = new DataAnnouncementEmployeeWelfare();

            string announcementId = oRequestParameter.InputParameter["AnnouncementId"].ToString();
            string sCompany = oRequestParameter.InputParameter["CompanyCode"].ToString();
            int key = 0;
            if (Int32.TryParse(announcementId, out key))
            {
                result = AnnouncementManagement.CreateInstance(sCompany).GetAnnouncementWelfareById(key);
            }

            return result;
        }

        [HttpPost]
        public void EditAnnouncementEmployeeWelfareByAdmin([FromBody]DataAnnouncementMain oRequestParameter)
        {
            int announcementId = Convert.ToInt32(oRequestParameter.InputParameter["AnnouncementId"]);
            int announcementStatusId = Convert.ToInt32(oRequestParameter.InputParameter["AnnouncementStatusId"]);
            int sequence = Convert.ToInt32(oRequestParameter.InputParameter["Sequence"]);
            string title_th = oRequestParameter.InputParameter["Title_TH"].ToString();
            string description_TH = oRequestParameter.InputParameter["Description_TH"].ToString();
            string remark_th = oRequestParameter.InputParameter["Remark_TH"].ToString();
            string title_en = oRequestParameter.InputParameter["Title_EN"].ToString();
            string description_en = oRequestParameter.InputParameter["Description_EN"].ToString();
            string remark_en = oRequestParameter.InputParameter["Remark_EN"].ToString();
            DateTime effectiveDate = Convert.ToDateTime(oRequestParameter.InputParameter["EffectiveDate"]);
            DateTime expireDate = Convert.ToDateTime(oRequestParameter.InputParameter["ExpireDate"]);
            bool isUnlimitedExpire = Convert.ToBoolean(oRequestParameter.InputParameter["IsUnlimitedExpire"]);
            string sCompany = oRequestParameter.InputParameter["CompanyCode"].ToString();


            AnnouncementDataAll data = new AnnouncementDataAll()
            {
                AnnouncementId = announcementId,
                Title_TH = title_th,
                Description_TH = description_TH,
                Remark_TH = remark_th,
                Title_EN = title_en,
                Description_EN = description_en,
                Remark_EN = remark_en,
                EffectiveDate = effectiveDate,
                ExpireDate = expireDate,
                IsUnlimitedExpire = isUnlimitedExpire,
                AnnouncementStatusId = announcementStatusId,
                AnnouncementTypeId = 4,  // ประเภทประกาศสวัสดิการอื่นๆ
                UpdateBy = oRequestParameter.Requestor.EmployeeID,
                CreateBy = oRequestParameter.Requestor.EmployeeID,
                Sequence = sequence
            };

            var picture_new_th = oRequestParameter.ListPicture_TH.Where(s => s.FileName != "").ToList();
            var picture_new_en = oRequestParameter.ListPicture_EN.Where(s => s.FileName != "").ToList();

            if (picture_new_th.Count > 0)
            {
                foreach (var item in picture_new_th)
                {
                    AnnouncementFile file = new AnnouncementFile()
                    {
                        AnnouncementFileId = item.AnnouncementFileId,
                        TextFile = item.FileName,
                        Sequence = item.FileSequence,
                        Language = item.Language
                    };
                    data.ListFile.Add(file);
                }
            }

            if (picture_new_en.Count > 0)
            {
                foreach (var item in picture_new_en)
                {
                    AnnouncementFile file = new AnnouncementFile()
                    {
                        AnnouncementFileId = item.AnnouncementFileId,
                        TextFile = item.FileName,
                        Sequence = item.FileSequence,
                        Language = item.Language
                    };
                    data.ListFile.Add(file);
                }
            }

            AnnouncementManagement.CreateInstance(sCompany).EditAnnouncementEmployeeWelfareByAdmin(data);
        }
        #endregion

        #region ประกาศหลักทั้งหมด (สำหรับพนักงานดู)
        [HttpPost]
        public PaginationAnnouncementMainNotifyAll GetAllAnnouncementMainNotify([FromBody]RequestParameter oRequestParameter)
        {
            int page = Convert.ToInt32(oRequestParameter.InputParameter["Page"]);
            int itemPerPage = Convert.ToInt32(oRequestParameter.InputParameter["ItemPerPage"]);

            PaginationAnnouncementMainNotifyAll data = new PaginationAnnouncementMainNotifyAll();

            data = AnnouncementManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
                .GetPaginationAnnouncementMainNotify(
                page, 
                itemPerPage, 
                oRequestParameter.Requestor.Language, oRequestParameter.Requestor.EmployeeID);

            return data;
        }

        [HttpPost]
        public AnnouncementNotifyDetail GetAnnouncementMainNotifyDetail([FromBody]RequestParameter oRequestParameter)
        {
            int announcementId = Convert.ToInt32(oRequestParameter.InputParameter["AnnouncementId"]);
            AnnouncementNotifyDetail data = new AnnouncementNotifyDetail();
            data = AnnouncementManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
                .GetAnnouncementMainNotifyDetailById(announcementId, oRequestParameter.Requestor.Language);
            return data;
        }

        [HttpPost]
        public List<DataAnnouncementNotifyAll> GetAllAnnouncementMainNotifyToBell([FromBody]RequestParameter oRequestParameter)
        {
            var list_announcementNotify = new List<DataAnnouncementNotifyAll>();
            try
            {
                list_announcementNotify = AnnouncementManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                    .GetPaginationAnnouncementMainNotifyToBell(oRequestParameter.CurrentEmployee.Language, oRequestParameter.CurrentEmployee.EmployeeID);
            }
            catch
            {
                
            }
            return list_announcementNotify;
        }

        [HttpPost]
        public AlarmNotify CalculateNotifyAnnouncement([FromBody]RequestParameter oRequestParameter)
        {
            var alert = new AlarmNotify();
            try
            {
                alert = AnnouncementManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                    .CalculateNotifyAnnouncement(oRequestParameter.CurrentEmployee.EmployeeID);
            }
            catch
            {

            }
            return alert;

        }

        [HttpPost]
        public void SaveViewNotifyAnnouncementMain([FromBody]RequestParameter oRequestParameter)
        {
            int announcementId = Convert.ToInt32(oRequestParameter.InputParameter["AnnouncementId"]);
            AnnouncementManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
                .SaveOrUpdateViewAnnouncementMain(oRequestParameter.Requestor.EmployeeID, announcementId);
        }

        #endregion

        #region ประกาศหน้า Home (สำหรับพนักงานดู)

        [HttpPost]
        public HomeAnnouncementDisplayEmployee GetAnnouncementEmployeeHome([FromBody]DataAnnouncementMain oRequestParameter)
        {
            var home_announcement = new HomeAnnouncementDisplayEmployee();

            home_announcement = AnnouncementManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
                .GetDataAllAnnouncementDisplayHome();

            return home_announcement;
        }

        #endregion
    }
}