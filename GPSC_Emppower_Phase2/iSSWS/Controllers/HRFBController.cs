﻿using ESS.DATA.ABSTRACT;
using ESS.EMPLOYEE;
using ESS.HR.FB;
using ESS.HR.FB.DATACLASS;
using ESS.SHAREDATASERVICE;
using ESS.UTILITY.EXPORT;
using ESS.UTILITY.FILE;
using iSSWS.Models;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Net.Http;
using System.Net.Http.Headers;
using ESS.WORKFLOW;

namespace iSSWS.Controllers
{
    public class HRFBController : ApiController
    {
        private CultureInfo oCL = new CultureInfo("en-US");
        public void SetAuthenticate(EmployeeDataForMobile oCurrentEmployee)
        {
            WorkflowIdentity iden;
            if (oCurrentEmployee.IsExternalUser)
                iden = WorkflowIdentity.CreateInstance(oCurrentEmployee.CompanyCode).GetIdentityForExternalUser(oCurrentEmployee.EmployeeID, oCurrentEmployee.Name);
            else
            {
                EmployeeData oEmployeeData = Convert<EmployeeData>.ObjectFrom<EmployeeDataForMobile>(oCurrentEmployee);
                iden = WorkflowIdentity.CreateInstance(oCurrentEmployee.CompanyCode).GetIdentityWithoutPassword(oEmployeeData);
            }
            WorkflowPrinciple Principle = new WorkflowPrinciple(iden);
            WorkflowPrinciple.Current = Principle;
        }

        [HttpPost]
        public List<FBEnrollment> GetEnrollmentData([FromBody] RequestParameter oRequestParameter)
        {

            try
            {
                var Employee_ID = "";
                var CompanyCode = "";

                if (oRequestParameter.CurrentEmployee.EmployeeID == oRequestParameter.Requestor.EmployeeID)
                {
                    Employee_ID = oRequestParameter.CurrentEmployee.EmployeeID;
                    CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
                }
                else
                {
                    Employee_ID = oRequestParameter.Requestor.EmployeeID;
                    CompanyCode = oRequestParameter.Requestor.CompanyCode;
                }

                List<FBEnrollment> TResult = new List<FBEnrollment>();
                TResult = HRFBManagement.CreateInstance(CompanyCode.ToString()).GetEnrollmentData(Employee_ID.ToString(), oRequestParameter.InputParameter["EmpSubGroup"].ToString(), oRequestParameter.InputParameter["RequestNo"].ToString(), oRequestParameter.InputParameter["KeyYear"].ToString());
                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpPost]
        public List<FBReimburse> GetReimburseQuota([FromBody] RequestParameter oRequestParameter)
        {

            try
            {
                var Employee_ID = "";
                var CompanyCode = "";

                if (oRequestParameter.CurrentEmployee.EmployeeID == oRequestParameter.Requestor.EmployeeID)
                {
                    Employee_ID = oRequestParameter.CurrentEmployee.EmployeeID;
                    CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
                }
                else
                {
                    Employee_ID = oRequestParameter.Requestor.EmployeeID;
                    CompanyCode = oRequestParameter.Requestor.CompanyCode;
                }

                List<FBReimburse> TResult = new List<FBReimburse>();
                TResult = HRFBManagement.CreateInstance(CompanyCode.ToString()).GetReimburseQuota(Employee_ID.ToString(), oRequestParameter.InputParameter["EmpSubGroup"].ToString(), oRequestParameter.InputParameter["KeyYear"].ToString());
                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpPost]
        public List<FBReimburse> GetReimburseSelected([FromBody] RequestParameter oRequestParameter)
        {

            try
            {
                var Employee_ID = "";
                var CompanyCode = "";

                if (oRequestParameter.CurrentEmployee.EmployeeID == oRequestParameter.Requestor.EmployeeID)
                {
                    Employee_ID = oRequestParameter.CurrentEmployee.EmployeeID;
                    CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
                }
                else
                {
                    Employee_ID = oRequestParameter.Requestor.EmployeeID;
                    CompanyCode = oRequestParameter.Requestor.CompanyCode;
                }

                List<FBReimburse> TResult = new List<FBReimburse>();
                TResult = HRFBManagement.CreateInstance(CompanyCode.ToString()).GetReimburseSelected(Employee_ID.ToString(), oRequestParameter.InputParameter["EmpSubGroup"].ToString(), oRequestParameter.InputParameter["RequestNo"].ToString(), oRequestParameter.InputParameter["KeyYear"].ToString());
                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpPost]
        public List<FBConfiguration> GetTextDetail([FromBody] RequestParameter oRequestParameter)
        {

            try
            {
                var Employee_ID = "";
                var CompanyCode = "";

                if (oRequestParameter.CurrentEmployee.EmployeeID == oRequestParameter.Requestor.EmployeeID)
                {
                    Employee_ID = oRequestParameter.CurrentEmployee.EmployeeID;
                    CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
                }
                else
                {
                    Employee_ID = oRequestParameter.Requestor.EmployeeID;
                    CompanyCode = oRequestParameter.Requestor.CompanyCode;
                }

                List<FBConfiguration> TResult = new List<FBConfiguration>();
                TResult = HRFBManagement.CreateInstance(CompanyCode.ToString()).GetTextDetail(Employee_ID.ToString(), oRequestParameter.InputParameter["KeyCode"].ToString());
                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpPost]
        public List<FBConfiguration> GETFreeText([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                List<FBConfiguration> oResult = new List<FBConfiguration>();

                string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
                //oResult = HRFBManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GETFreeText(oRequestParameter.InputParameter["KeyType"].ToString(), oRequestParameter.InputParameter["KeyCode"].ToString(), oRequestParameter.InputParameter["KeyValue"].ToString());
                oResult = HRFBManagement.CreateInstance(sCompanyCode).GETFreeText(oRequestParameter.InputParameter["KeyType"].ToString(), oRequestParameter.InputParameter["KeyCode"].ToString(), oRequestParameter.InputParameter["KeyValue"].ToString());

                return oResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public List<FBPeriodSetting> GetFlexibleBenefitSetting([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                WorkflowController oWorkflowController = new WorkflowController();

                string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
                List<FBPeriodSetting> TResult = new List<FBPeriodSetting>();
                //TResult = HRFBManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode.ToString()).GetFlexibleBenefitSetting(oRequestParameter.CurrentEmployee.EmployeeID.ToString(), oRequestParameter.InputParameter["KeyType"].ToString(), Convert.ToInt16(oRequestParameter.InputParameter["KeyYear"].ToString()), oRequestParameter.InputParameter["KeyValue"].ToString());
                TResult = HRFBManagement.CreateInstance(sCompanyCode).GetFlexibleBenefitSetting(oRequestParameter.CurrentEmployee.EmployeeID.ToString(), oRequestParameter.InputParameter["KeyType"].ToString(), Convert.ToInt16(oRequestParameter.InputParameter["KeyYear"].ToString()), oRequestParameter.InputParameter["KeyValue"].ToString());

                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public List<YearsConfig> GetNextYears([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();

                List<YearsConfig> TResult = new List<YearsConfig>();
                //TResult = HRFBManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetNextYears();
                TResult = HRFBManagement.CreateInstance(sCompanyCode).GetNextYears();
                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public List<YearsConfig> GetNextYearsBenefitManagement([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                List<YearsConfig> TResult = new List<YearsConfig>();
                TResult = HRFBManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetNextYears();
                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public List<FBPeriodSetting> CheckPeriodSetting([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                List<FBPeriodSetting> TResult = new List<FBPeriodSetting>();
                TResult = HRFBManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).CheckPeriodSetting(Convert.ToInt32(oRequestParameter.InputParameter["KeyYear"].ToString()));
                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost]
        public string CheckAdmin([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                var TResult =  ShareDataManagement.LookupCache(oRequestParameter.CurrentEmployee.CompanyCode, "ESS.HR.FB", "ADMIN");
                return TResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public string GetQuotaUse([FromBody] RequestParameter oRequestParameter)
        {
            try
            {
                var Employee_ID = "";
                var CompanyCode = "";
                var Year = "";
                var RequestNo = "";
                if (oRequestParameter.CurrentEmployee.EmployeeID == oRequestParameter.Requestor.EmployeeID)
                {
                    Employee_ID = oRequestParameter.CurrentEmployee.EmployeeID;
                    CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode;
                    Year = oRequestParameter.InputParameter["Year"].ToString();
                    RequestNo = oRequestParameter.InputParameter["RequestNo"].ToString();
                }
                else
                {
                    Employee_ID = oRequestParameter.Requestor.EmployeeID;
                    CompanyCode = oRequestParameter.Requestor.CompanyCode;
                    Year = oRequestParameter.InputParameter["Year"].ToString();
                    RequestNo = oRequestParameter.InputParameter["RequestNo"].ToString();
                }

                var QuotaUsed = HRFBManagement.CreateInstance(CompanyCode.ToString()).FB_ReimburseCheckQuota(Employee_ID,Year,RequestNo);



                var oConfig = HRFBManagement.CreateInstance(CompanyCode.ToString()).FB_EnrollmentGet(Employee_ID, Year);
                var Config = oConfig[0];
                var Quota = Config.FlexPoints;


                var result = Quota + "," + QuotaUsed;

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public HttpResponseMessage GetTrackingRegisterFlexBenExport([FromBody] RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            string sReportName = oRequestParameter.InputParameter["ReportName"].ToString();
            string sLanguageCode = oRequestParameter.InputParameter["LanguageCode"].ToString();
            string sEmpID = oRequestParameter.InputParameter["Employee_id"].ToString();
            string sEmployeeName = oRequestParameter.InputParameter["EmployeeName"].ToString();
            string sPositionName = oRequestParameter.InputParameter["PositionName"].ToString();
            string sOrgUnitName = oRequestParameter.InputParameter["OrgUnitName"].ToString();
            string sExportType = oRequestParameter.InputParameter["ExportType"].ToString();
            string sByOrg = oRequestParameter.InputParameter["ByOrg"].ToString();
            string sListOrg = oRequestParameter.InputParameter["ListOrg"].ToString();
            string sYear = oRequestParameter.InputParameter["Year"].ToString();
            

            DataTable oTableResult = new DataTable();

            if (sByOrg == "2") //แบบระบุพนักงาน
            {
                oTableResult = HRFBManagement.CreateInstance(sCompanyCode).GetTrackingRegisterFlexBenReport(sYear, sEmpID, "", sCompanyCode, sLanguageCode);
            }
            else //แบบเลือกหน่วยงาน TreeList
            {
                oTableResult = HRFBManagement.CreateInstance(sCompanyCode).GetTrackingRegisterFlexBenReport(sYear, "", sListOrg, sCompanyCode, sLanguageCode);
            }

            var workbook = HRFBManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).ExportExcelTrackingRegisterFlexBenReport(oTableResult, sLanguageCode);
            var stream = new MemoryStream();
            workbook.SaveAs(stream);
            stream.Position = 0;

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(stream.ToArray())
            };

            DateTime date_now = DateTime.Now;
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = "ReportTrackingRegisterFlexBen" + date_now.Year + date_now.Month + date_now.Date + ".xlsx"
            };
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

            return result;
        }

        // Show Report หน้ารายงานติดตามการส่งสรุปค่าล่วงเวลารายเดือน
        [HttpPost]
        public void ShowTrackingRegisterFlexBenReport([FromBody] RequestParameter oRequestParameter)
        {

            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            string sReportName = oRequestParameter.InputParameter["ReportName"].ToString();
            string sLanguageCode = oRequestParameter.InputParameter["LanguageCode"].ToString();
            string sEmpID = oRequestParameter.InputParameter["Employee_id"].ToString();
            string sEmployeeName = oRequestParameter.InputParameter["EmployeeName"].ToString();
            string sPositionName = oRequestParameter.InputParameter["PositionName"].ToString();
            string sOrgUnitName = oRequestParameter.InputParameter["OrgUnitName"].ToString();
            string sExportType = oRequestParameter.InputParameter["ExportType"].ToString();
            string sByOrg = oRequestParameter.InputParameter["ByOrg"].ToString();
            string sListOrg = oRequestParameter.InputParameter["ListOrg"].ToString();
            string sYear = oRequestParameter.InputParameter["Year"].ToString();

            string strExtFile = string.Empty;
            strExtFile = "EXCEL"; // typeReport;

            DataTable oTableResult = new DataTable();
            if (sByOrg == "2") //แบบระบุพนักงาน
            {
                oTableResult = HRFBManagement.CreateInstance(sCompanyCode).GetTrackingRegisterFlexBenReport(sYear, sEmpID, "", sCompanyCode, sLanguageCode);
            }
            else //แบบเลือกหน่วยงาน TreeList
            {
                oTableResult = HRFBManagement.CreateInstance(sCompanyCode).GetTrackingRegisterFlexBenReport(sYear, "", sListOrg, sCompanyCode, sLanguageCode);
            }

            List<ReportParameter> ReportParam = new List<ReportParameter>();

            ReportParam.Add(new ReportParameter("p_ReportTitle", CacheManager.GetCommonText("TRACKINGREGISTERFLEXBENREPORT", oCurrentEmployee.Language, "ReportTitle")));
            ReportParam.Add(new ReportParameter("p_Year", CacheManager.GetCommonText("TRACKINGREGISTERFLEXBENREPORT", oCurrentEmployee.Language, "COL_YEAR")+" "+ sYear));
            ReportParam.Add(new ReportParameter("p_RegisterStatus", CacheManager.GetCommonText("TRACKINGREGISTERFLEXBENREPORT", oCurrentEmployee.Language, "COL_REGISTERSTATUS")));
            ReportParam.Add(new ReportParameter("p_Summary", CacheManager.GetCommonText("TRACKINGREGISTERFLEXBENREPORT", oCurrentEmployee.Language, "SUMMARY")));

            ReportObject oReportObject = new ReportObject();
            oReportObject.ExportFileName = System.Web.HttpContext.Current.Server.MapPath("~/Client/Report/rptTrackingRegisterFlexBenReport" + oRequestParameter.Requestor.EmployeeID);

            ReportViewer rptViewer = new ReportViewer();
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("DS_FlexBenData", oTableResult)); 
            rptViewer.LocalReport.ReportPath = System.Web.HttpContext.Current.Server.MapPath("~/Views/Report/HR/FB/rptTrackingRegisterFlexBenReport.rdlc");
            rptViewer.LocalReport.SetParameters(ReportParam);
            rptViewer.ShowRefreshButton = false;
            rptViewer.LocalReport.EnableExternalImages = true;
            rptViewer.LocalReport.Refresh();

            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string filenameExtension;

            byte[] bytes = rptViewer.LocalReport.Render(
                strExtFile, null, out mimeType, out encoding, out filenameExtension,
                out streamids, out warnings);

            oReportObject.Warnings = warnings;
            oReportObject.Streamids = streamids;
            oReportObject.ContentType = mimeType;
            oReportObject.Encoding = encoding;
            oReportObject.FilenameExtension = filenameExtension;
            oReportObject.ExportFileName = string.Format("{0}.{1}", oReportObject.ExportFileName, filenameExtension);
            FileManager.SaveFile(oReportObject.ExportFileName, bytes, true);

        }

        #region yun-20210905
        [HttpPost]
        public void UploadSizeChart([FromBody] RequestParameter oRequestParameter)
        {
            string sCompanyCode = "0245";// oRequestParameter.InputParameter["SelectCompany"].ToString(); ;// "0245"
            string sImagesChart = oRequestParameter.InputParameter["ImagesChart"].ToString();
            try
            {

                string path = System.Web.HttpContext.Current.Server.MapPath("~/Client/images/uniform/SizeChart/" + sCompanyCode + "/" + DateTime.Now.Year);
                HRFBManagement.CreateInstance(sCompanyCode).UploadSizeChart(path, sImagesChart);

            }
            catch (Exception err)
            {
                throw err;
            }
        }


        [HttpPost]
        public List<FBUniFormType> GetUniFormType([FromBody] RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            try
            {
                List<FBUniFormType> oReturnData = new List<FBUniFormType>();
                oReturnData = HRFBManagement.CreateInstance(sCompanyCode).GetUniFormType();
                return oReturnData;
            }
            catch (Exception err)
            {
                throw err;
            }
        }

        [HttpPost]
        public List<FBUniForm> GetUniForm([FromBody] RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            try
            {
                List<FBUniForm> oReturnData = new List<FBUniForm>();
                string path = System.Web.HttpContext.Current.Server.MapPath("~/Client/images/uniform/Company/" + sCompanyCode + "/" + DateTime.Now.Year);
                oReturnData = HRFBManagement.CreateInstance(sCompanyCode).GetUniForm(path);
                return oReturnData;
            }
            catch (Exception err)
            {
                throw err;
            }
        }

        [HttpPost]
        public List<FBUniFormSize> GetUniFormSize([FromBody] RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            try
            {
                List<FBUniFormSize> oReturnData = new List<FBUniFormSize>();
                oReturnData = HRFBManagement.CreateInstance(sCompanyCode).GetUniFormSize();
                return oReturnData;
            }
            catch (Exception err)
            {
                throw err;
            }
        }

        [HttpPost]
        public void UniFormTypeSave([FromBody] RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();

            FBUniFormType oFbUniformType = new FBUniFormType();
            try
            {
                oFbUniformType.UniFormTypeID = (oRequestParameter.InputParameter["UniFormTypeID"] != null) ? Convert.ToInt16(oRequestParameter.InputParameter["UniFormTypeID"].ToString()) : 0;
                oFbUniformType.UniFormTypeName = oRequestParameter.InputParameter["UniFormTypeName"].ToString();
                oFbUniformType.Status = oRequestParameter.InputParameter["Status"].ToString();
                oFbUniformType.Description = (oRequestParameter.InputParameter["Description"] != null) ? oRequestParameter.InputParameter["Description"].ToString() : "";
                oFbUniformType.StartDate = (oRequestParameter.InputParameter["StartDate"] != null) ? oRequestParameter.InputParameter["StartDate"].ToString() : "";
                oFbUniformType.EndDate = (oRequestParameter.InputParameter["EndDate"] != null) ? oRequestParameter.InputParameter["EndDate"].ToString() : "";
                oFbUniformType.CreateBy = (oRequestParameter.InputParameter["CreateBy"] != null) ? oRequestParameter.InputParameter["CreateBy"].ToString() : "";
                oFbUniformType.UpdateBy = (oRequestParameter.InputParameter["UpdateBy"] != null) ? oRequestParameter.InputParameter["UpdateBy"].ToString() : "";

                HRFBManagement.CreateInstance(sCompanyCode).UniFormTypeSave(oFbUniformType);
            }
            catch (Exception err)
            {
                throw err;
            }
        }

        [HttpPost]
        public void UniFormSave([FromBody] RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();

            FBUniForm oFbUniform = new FBUniForm();
            try
            {
                oFbUniform.UniFormID = (oRequestParameter.InputParameter["UniFormID"] != null) ? Convert.ToInt16(oRequestParameter.InputParameter["UniFormID"].ToString()) : 0;
                oFbUniform.UniFormTypeID = (oRequestParameter.InputParameter["UniFormTypeID"] != null) ? Convert.ToInt16(oRequestParameter.InputParameter["UniFormTypeID"].ToString()) : 0;
                oFbUniform.UniFormName = oRequestParameter.InputParameter["UniFormName"].ToString();
                oFbUniform.Status = oRequestParameter.InputParameter["Status"].ToString();
                oFbUniform.Images = (oRequestParameter.InputParameter["Images"] != null) ? oRequestParameter.InputParameter["Images"].ToString() : "";
                oFbUniform.Description = (oRequestParameter.InputParameter["Description"] != null) ? oRequestParameter.InputParameter["Description"].ToString() : "";
                oFbUniform.StartDate = (oRequestParameter.InputParameter["StartDate"] != null) ? oRequestParameter.InputParameter["StartDate"].ToString() : "";
                oFbUniform.EndDate = (oRequestParameter.InputParameter["EndDate"] != null) ? oRequestParameter.InputParameter["EndDate"].ToString() : "";
                oFbUniform.CreateBy = (oRequestParameter.InputParameter["CreateBy"] != null) ? oRequestParameter.InputParameter["CreateBy"].ToString() : "";
                oFbUniform.UpdateBy = (oRequestParameter.InputParameter["UpdateBy"] != null) ? oRequestParameter.InputParameter["UpdateBy"].ToString() : "";
                string path = System.Web.HttpContext.Current.Server.MapPath("~/Client/images/uniform/Company/" + sCompanyCode + "/" + DateTime.Now.Year);
                //string directoryPath = path + "/" + CompanyCode + "/" + DateTime.Now.Year;
                HRFBManagement.CreateInstance(sCompanyCode).UniFormSave(oFbUniform, path);
            }
            catch (Exception err)
            {
                throw err;
            }
        }


        [HttpPost]
        public void UniFormSizeSave([FromBody] RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();

            FBUniFormSize oFbUniformSize = new FBUniFormSize();
            try
            {
                oFbUniformSize.SizeID = (oRequestParameter.InputParameter["SizeID"] != null) ? Convert.ToInt16(oRequestParameter.InputParameter["SizeID"].ToString()) : 0;
                oFbUniformSize.UniFormID = (oRequestParameter.InputParameter["UniFormID"] != null) ? Convert.ToInt16(oRequestParameter.InputParameter["UniFormID"].ToString()) : 0;
                oFbUniformSize.UniFormTypeID = (oRequestParameter.InputParameter["UniFormTypeID"] != null) ? Convert.ToInt16(oRequestParameter.InputParameter["UniFormTypeID"].ToString()) : 0;
                oFbUniformSize.Size = oRequestParameter.InputParameter["Size"].ToString();
                oFbUniformSize.Status = oRequestParameter.InputParameter["Status"].ToString();
                oFbUniformSize.CWLength = Convert.ToDecimal(oRequestParameter.InputParameter["CWLength"].ToString());
                oFbUniformSize.Length = Convert.ToDecimal(oRequestParameter.InputParameter["Length"].ToString());
                oFbUniformSize.StartDate = (oRequestParameter.InputParameter["StartDate"] != null) ? oRequestParameter.InputParameter["StartDate"].ToString() : "";
                oFbUniformSize.EndDate = (oRequestParameter.InputParameter["EndDate"] != null) ? oRequestParameter.InputParameter["EndDate"].ToString() : "";
                oFbUniformSize.CreateBy = (oRequestParameter.InputParameter["CreateBy"] != null) ? oRequestParameter.InputParameter["CreateBy"].ToString() : "";
                oFbUniformSize.UpdateBy = (oRequestParameter.InputParameter["UpdateBy"] != null) ? oRequestParameter.InputParameter["UpdateBy"].ToString() : "";

                HRFBManagement.CreateInstance(sCompanyCode).UniFormSizeSave(oFbUniformSize);
            }
            catch (Exception err)
            {
                throw err;
            }
        }
        #endregion

    }
}