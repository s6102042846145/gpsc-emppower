﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using ESS.EMPLOYEE;
//using ESS.EMPLOYEE.CONFIG.PA;
using ESS.UTILITY.CONVERT;
using iSSWS.Models;
using ESS.EMPLOYEE.CONFIG.OM;
using ESS.WORKFLOW;
using System.Data;
using ESS.HR.PA.INFOTYPE;
using ESS.HR.PA;
using ESS.HR.PA.DATACLASS;
using System.Globalization;
using ESS.HR.PA.CONFIG;
using ESS.PORTALENGINE;
using ESS.DATA.FILE;
using System.Linq;
using System.Net.Http;
using ClosedXML.Excel;
using System.IO;
using System.Net;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using static iSSWS.Controllers.WorkflowController;
using ESS.SHAREDATASERVICE;
using ESS.API.FILTERS;

namespace iSSWS.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    //Require authorization for all action on the controller
    //[ApiAuthorize]
    public class HRPAController : ApiController
    {
        private CultureInfo thCL = new CultureInfo("th-TH");
        private CultureInfo enCL = new CultureInfo("en-US");

        public enum OrgLevelType
        {
            BelongTo = 010, //สังกัด
            UnderTo = 020,
            LineOfWork = 030, //สายงาน
            SeniorDepartment = 040, //ฝ่ายอาวุโส
            Department = 050,   // ฝ่าย
            Section = 060,   // ส่วน
            Agency = 070,    // หน่วย
            Devision = 080   // แผนก
        }

        #region "PAConfiguration"
        [HttpPost]
        public Dictionary<string, PAConfiguration> GetPAConfigurationList([FromBody] RequestParameter oRequestParameter)
        {
            Dictionary<string, PAConfiguration> oResult = new Dictionary<string, PAConfiguration>();
            if (oRequestParameter.InputParameter.ContainsKey("CategoryName"))
            {
                string[] CategoryName = oRequestParameter.InputParameter["CategoryName"].ToString().TrimEnd(',').Split(',');
                List<PAConfiguration> PAConfig = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPAConfigurationList(CategoryName);
                foreach (PAConfiguration config in PAConfig)
                {
                    if (!oResult.ContainsKey(config.FieldName))
                    {
                        oResult.Add(config.FieldName, config);
                    }
                }
            }
            else
            {
                List<PAConfiguration> PAConfig = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPAConfigurationList();
                foreach (PAConfiguration config in PAConfig)
                {
                    if (!oResult.ContainsKey(config.FieldName))
                    {
                        oResult.Add(config.FieldName, config);
                    }
                }
            }

            return oResult;
        }
        #endregion

        #region "Personal Data"
        [HttpPost]
        public PersonalInformation GetPersonalData([FromBody] RequestParameter oRequestParameter)
        {
            INFOTYPE0002 tmp = new INFOTYPE0002();
            PersonalInformation oResult = new PersonalInformation();
            tmp = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPersonalData(oRequestParameter.Requestor.EmployeeID, DateTime.ParseExact(oRequestParameter.InputParameter["checkDate"].ToString(), "yyyy-MM-dd", enCL));

            oResult.PersonalData = tmp;
            oResult.PersonalData_OLD = tmp;

            return oResult;
        }

        [HttpPost]
        public object GetPersonSelectData(RequestParameter oRequestParameter)
        {
            string LanguageCode = oRequestParameter.CurrentEmployee.Language;
            object oReturn = new
            {
                Language = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllLanguage(),
                Prefix = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllPrefixName(),

                Gender = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllGender(LanguageCode),
                Title = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllTitleName(LanguageCode),
                MaritalStatus = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllMaritalStatus(LanguageCode),
                Nationality = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllNationality(LanguageCode),
                Country = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllCountryList(LanguageCode),
                Religion = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllReligion(LanguageCode)
            };
            return oReturn;
        }

        [HttpPost]
        public object GetProvinceByCountryCode(RequestParameter oRequestParameter)
        {
            string LanguageCode = oRequestParameter.CurrentEmployee.Language;
            object oReturn = new
            {
                Province = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetProvinceByCountryCode(oRequestParameter.InputParameter["Code"].ToString(), LanguageCode)
            };
            return oReturn;
        }

        #endregion

        #region "Personal Account"
        [HttpPost]
        public INFOTYPE0009 GetBankDetail([FromBody] RequestParameter oRequestParameter)
        {
            INFOTYPE0009 oResult = new INFOTYPE0009();
            oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetBankDetail(oRequestParameter.Requestor.EmployeeID);
            oResult.AccountName = oRequestParameter.Requestor.Name;
            return oResult;
        }

        [HttpPost]
        public Bank GetBank([FromBody] RequestParameter oRequestParameter)
        {
            Bank oResult = new Bank();
            oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetBank(oRequestParameter.InputParameter["Code"].ToString());

            return oResult;
        }

        #endregion

        #region "Personal Family"
        [HttpPost]
        public object GetPersonalFamily([FromBody] RequestParameter oRequestParameter)
        {
            Dictionary<string, List<FileAttachmentForMobile>> dictFileAttachmentList = new Dictionary<string, List<FileAttachmentForMobile>>();
            List<INFOTYPE0021> FamilyData = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetFamilyData(oRequestParameter.Requestor.EmployeeID);

            EmployeeDataForMobile oRequestor = oRequestParameter.Requestor;
            SetAuthenticate(oRequestor);
            int requestTypeID = 604;
            foreach (INFOTYPE0021 member in FamilyData)
            {
                PersonalFamily oPersonalFamily = new PersonalFamily();

                string employeeID = oRequestor.EmployeeID;

                string requestSubType = (string.IsNullOrEmpty(member.ChildNo)) ? member.FamilyMember : member.FamilyMember + member.ChildNo;

                //File Attachment
                List<FileAttachmentForMobile> FileAttachmentList = new List<FileAttachmentForMobile>();


                List<PersonalAttachmentFileSet> oAttExport = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetAttachmentExport(employeeID, requestTypeID, requestSubType);
                for (int i = 0; i < oAttExport.Count; i++)
                {
                    FileAttachmentForMobile file = new FileAttachmentForMobile();
                    file.FileID = oAttExport[i].FileID;
                    file.FileName = oAttExport[i].FileName;
                    file.FileSetID = oAttExport[i].FileSetID;
                    file.FilePath = oAttExport[i].FilePath;
                    file.FileType = oAttExport[i].FileType;
                    FileAttachmentList.Add(file);
                }

                if (!dictFileAttachmentList.ContainsKey(requestSubType))
                    dictFileAttachmentList.Add(requestSubType, FileAttachmentList);
            }

            object oReturn = new
            {
                FamilyData,
                dictFileAttachmentList
            };

            foreach (KeyValuePair<string, List<FileAttachmentForMobile>> dictFile in dictFileAttachmentList)
            {
                List<FileAttachmentForMobile> FileAttachmentForMobileList = new List<FileAttachmentForMobile>();
                List<PersonalAttachmentFileSet> oAttFileSet = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetAttachmentFileSet(oRequestor.EmployeeID, requestTypeID, dictFile.Key);
                for (int i = 0; i < oAttFileSet.Count; i++)
                {
                    FileAttachmentForMobile file = new FileAttachmentForMobile();
                    file.FileID = oAttFileSet[i].FileID;
                    file.FileName = oAttFileSet[i].FileName;
                    file.FileSetID = oAttFileSet[i].FileSetID;
                    file.FilePath = oAttFileSet[i].FilePath;
                    file.FileType = oAttFileSet[i].FileType;
                    //FileAttachmentForMobileList.Add(file);
                    dictFile.Value.Add(file);
                }
            }

            return oReturn;
        }




        [HttpPost]
        public object GetPersonalFamilyByBE([FromBody] RequestParameter oRequestParameter)
        {
            INFOTYPE0002 tmp = new INFOTYPE0002();
            PersonalInformation oResult = new PersonalInformation();
            tmp = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPersonalData(oRequestParameter.Requestor.EmployeeID, DateTime.ParseExact(oRequestParameter.InputParameter["checkDate"].ToString(), "yyyy-MM-dd", enCL));
            List<INFOTYPE0021> Son = new List<INFOTYPE0021>();
            List<INFOTYPE0021> FamilyData = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetFamilyData(oRequestParameter.Requestor.EmployeeID);
            DateTime CheckStartWork = new DateTime(2020,4,1);
            EmployeeDataForMobile oRequestor = oRequestParameter.Requestor;
            SetAuthenticate(oRequestor);
            DateTime Datenew = DateTime.Now;
            int Age = Datenew.Year - tmp.DOB.Year;
            INFOTYPE0021 pushtmp = new INFOTYPE0021();
            pushtmp = new INFOTYPE0021
            {
                Ischeck = true,
                FamilyMember = "13",
                ChildNo = null,
                TitleName = tmp.TitleID,
                TitleRank = null,
                Name = tmp.FirstName,
                Surname = tmp.LastName,
                BirthDate = tmp.DOB,
                Sex = tmp.Gender,
                BirthPlace = tmp.BirthPlace,
                CityOfBirth = tmp.BirthCity,
                Nationality = tmp.Nationality,
                FatherID = null,
                MotherID = null,
                SpouseID = null,
                MotherSpouseID = null,
                FatherSpouseID = null,
                JobTitle = null,
                Employer = null,
                Dead = null,
                Remark = null,
                Address = null,
                Street = null,
                District = null,
                City = null,
                Postcode = null,
                Country = null,
                TelephoneNo = null,
                Age = Age
            };
            FamilyData.Insert(0,pushtmp);
            if(tmp.BeginDate > CheckStartWork)
            {
                
                var itemToRemove11 = FamilyData.Single(r => r.FamilyMember == "11");
                FamilyData.Remove(itemToRemove11);
                var itemToRemove12 = FamilyData.Single(r => r.FamilyMember == "12");
                FamilyData.Remove(itemToRemove12);

            }
            FamilyData.RemoveAll(t => t.FamilyMember == "7");
            
            var itemToRemove2 = FamilyData.Where(r => r.FamilyMember == "2");
          

            int Soncount = itemToRemove2.Count();
            if (itemToRemove2.Count() > 0)
            {
                for (int i = 1; i <= Soncount; i++)
                {
                    var itemToRemove2Set = FamilyData.First(r => r.FamilyMember == "2");
                    if (!string.IsNullOrEmpty(itemToRemove2Set.FamilyMember))
                    {
                        Son.Add(itemToRemove2Set);
                        FamilyData.Remove(itemToRemove2Set);
                    }

                }
            }

            var itemToRemoveDead = Son.Where(r => r.Dead == "X");

            if (itemToRemoveDead.Count() > 0)
            {
                for (int i = 1; i <= itemToRemoveDead.Count(); i++)
                {
                    var itemToRemoveDeadSet = Son.Single(r => r.Dead == "X");
                    if (!string.IsNullOrEmpty(itemToRemoveDeadSet.FamilyMember))
                    {
                        Son.Remove(itemToRemoveDeadSet);
                    }

                }
            }


            Son.Sort(delegate (INFOTYPE0021 x, INFOTYPE0021 y)
            {
                if (x.BirthDate == null && y.BirthDate == null) return 0;
                else if (x.BirthDate == null) return -1;
                else if (y.BirthDate == null) return 1;
                else return x.BirthDate.CompareTo(y.BirthDate);
            });

            foreach (INFOTYPE0021 member in Son)
            {
                FamilyData.Add(member); 
            }




            object oReturn = new
            {
                FamilyData,
               
            };

           

            return oReturn;
        }



       



        public void SetAuthenticate(EmployeeDataForMobile oEmployee)
        {
            WorkflowIdentity iden;
            if (oEmployee.IsExternalUser)
                iden = WorkflowIdentity.CreateInstance(oEmployee.CompanyCode).GetIdentityForExternalUser(oEmployee.EmployeeID, oEmployee.Name);
            else
            {
                EmployeeData oEmployeeData = Convert<EmployeeData>.ObjectFrom<EmployeeDataForMobile>(oEmployee);
                iden = WorkflowIdentity.CreateInstance(oEmployee.CompanyCode).GetIdentityWithoutPassword(oEmployeeData);
            }
            WorkflowPrinciple Principle = new WorkflowPrinciple(iden);
            WorkflowPrinciple.Current = Principle;
        }

        [HttpPost]
        public List<INFOTYPE0021> GetFamilyData([FromBody] RequestParameter oRequestParameter)
        {
            List<INFOTYPE0021> oResult = new List<INFOTYPE0021>();
            oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetFamilyData(oRequestParameter.Requestor.EmployeeID);

            return oResult;
        }

        [HttpPost]
        public object GetFamilySelectData(RequestParameter oRequestParameter)
        {
            string LanguageCode = oRequestParameter.CurrentEmployee.Language;
            List<INFOTYPE0021> listINF21 = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetFamilyData(oRequestParameter.Requestor.EmployeeID);
            if (oRequestParameter.InputParameter.ContainsKey("FamilyMember") && !string.IsNullOrEmpty(oRequestParameter.InputParameter["FamilyMember"].ToString()))
            {
                listINF21.RemoveAll(delegate (INFOTYPE0021 inf) { return inf.FamilyMember == oRequestParameter.InputParameter["FamilyMember"].ToString(); });
            }
            List<INFOTYPE0021> listINF21cannotMultiple = listINF21.FindAll(delegate (INFOTYPE0021 inf21) { return inf21.FamilyMember != "2" && inf21.FamilyMember != "7"; });
            List<FamilyMember> listAllFamilyMember = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllFamilyMember();
            foreach (INFOTYPE0021 info21 in listINF21cannotMultiple)
            {
                listAllFamilyMember.RemoveAll(delegate (FamilyMember member) { return member.Key == info21.FamilyMember; });
            }
            object oReturn = new
            {
                FamilyMember = listAllFamilyMember,
                Title = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllTitleName(LanguageCode),
                Country = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllCountryList(LanguageCode),
                Nationality = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllNationality(LanguageCode)
            };
            return oReturn;
        }
        #endregion

        #region "Personal Education(CV)"
        // change to use GetAllPersonalEducationHistory
        //[HttpPost]
        //public List<INFOTYPE0022> GetPersonalEducationHistory([FromBody] RequestParameter oRequestParameter)
        //{
        //    CommonText oCommonText = new CommonText(oRequestParameter.Requestor.CompanyCode);

        //    List<INFOTYPE0022> oEducationData = new List<INFOTYPE0022>();
        //    INFOTYPE0022 tmp = new INFOTYPE0022();
        //    List<INFOTYPE0022> oResult = new List<INFOTYPE0022>();
        //    oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetEducation(oRequestParameter.Requestor.EmployeeID);
        //    foreach (INFOTYPE0022 row in oResult)
        //    {
        //        tmp = new INFOTYPE0022();
        //        tmp.EmployeeID = row.EmployeeID;
        //        //tmp.BeginDate = row.BeginDate;

        //        tmp.Branch1 = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetBranchData(row.Branch1).BranchText;
        //        tmp.Branch2 = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetBranchData(row.Branch2).BranchText;
        //        //tmp.InstituteCode = oCommonText.LoadText("INSTITUTE", oRequestParameter.CurrentEmployee.Language, row.InstituteCode);
        //        oEducationData.Add(tmp);
        //    }
        //    return oEducationData;
        //}
        #endregion

        #region Personal Contact
        [HttpPost]
        public List<ContactData> INFOTYPE0105GetByCategoryCode([FromBody] RequestParameter oRequestParameter)
        {
            List<ContactData> oResult = new List<ContactData>();
            List<INFOTYPE0105> oINF105 = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetInfotype0105ByCategoryCode(oRequestParameter.Requestor.EmployeeID, oRequestParameter.InputParameter["CategoryCode"].ToString());

            foreach (INFOTYPE0105 row in oINF105)
            {
                oResult.Add(Convert<ContactData>.ObjectFrom(row));
            }
            return oResult;
        }

        [HttpPost]
        public CommunicationData GetCommunicationData([FromBody] RequestParameter oRequestParameter)
        {
            CommunicationData oResult = new CommunicationData();
            oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetCommunicationData(oRequestParameter.Requestor.EmployeeID);
            return oResult;
        }

        [HttpPost]
        public bool GetIsViewCommunicationOnly([FromBody] RequestParameter oRequestParameter)
        {
            bool isViewCommunicationOnly = false;
            isViewCommunicationOnly = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).IsViewCommunicationOnly;
            return isViewCommunicationOnly;
        }
        #endregion

        #region Personal Address
        [HttpPost]
        //public List<INFOTYPE0006> GetPersonalAddress([FromBody] RequestParameter oRequestParameter)
        public object GetPersonalAddress([FromBody] RequestParameter oRequestParameter)
        {
            Dictionary<string, List<FileAttachmentForMobile>> dictFileAttachmentList = new Dictionary<string, List<FileAttachmentForMobile>>();
            int requestTypeID = 602;

            List<INFOTYPE0006> oResult = new List<INFOTYPE0006>();
            oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPersonalAddress(oRequestParameter.Requestor.EmployeeID, DateTime.Now);


            for (int i = 0; i < oResult.Count; i++)
            {
                string requestSubType = oResult[i].AddressType;
                List<FileAttachmentForMobile> FileAttachmentList = new List<FileAttachmentForMobile>();


                List<PersonalAttachmentFileSet> oAttExport = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetAttachmentExport(oRequestParameter.Requestor.EmployeeID, requestTypeID, requestSubType);
                for (int j = 0; j < oAttExport.Count; j++)
                {
                    FileAttachmentForMobile file = new FileAttachmentForMobile();
                    file.FileID = oAttExport[j].FileID;
                    file.FileName = oAttExport[j].FileName;
                    file.FileSetID = oAttExport[j].FileSetID;
                    file.FilePath = oAttExport[j].FilePath;
                    file.FileType = oAttExport[j].FileType;
                    FileAttachmentList.Add(file);
                }

                if (!dictFileAttachmentList.ContainsKey(requestSubType))
                    dictFileAttachmentList.Add(requestSubType, FileAttachmentList);

            }
            object oReturn = new
            {
                oResult,
                dictFileAttachmentList
            };

            foreach (KeyValuePair<string, List<FileAttachmentForMobile>> dictFile in dictFileAttachmentList)
            {
                List<FileAttachmentForMobile> FileAttachmentForMobileList = new List<FileAttachmentForMobile>();
                List<PersonalAttachmentFileSet> oAttFileSet = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetAttachmentFileSet(oRequestParameter.Requestor.EmployeeID, requestTypeID, dictFile.Key);
                for (int i = 0; i < oAttFileSet.Count; i++)
                {
                    FileAttachmentForMobile file = new FileAttachmentForMobile();
                    file.FileID = oAttFileSet[i].FileID;
                    file.FileName = oAttFileSet[i].FileName;
                    file.FileSetID = oAttFileSet[i].FileSetID;
                    file.FilePath = oAttFileSet[i].FilePath;
                    file.FileType = oAttFileSet[i].FileType;
                    dictFile.Value.Add(file);
                }
            }

            //return oResult;
            return oReturn;
        }

        [HttpPost]
        public object GetAllCountryData(RequestParameter oRequestParameter)
        {
            string LanguageCode = oRequestParameter.CurrentEmployee.Language;
            object oReturn = new
            {
                Country = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllCountryList(LanguageCode),
            };
            return oReturn;
        }
        #endregion

        #region "Work History"
        [HttpPost]
        public List<WorkPeriod> GetworkHistory([FromBody] RequestParameter oRequestParameter)
        {
            List<WorkPeriod> oResult = new List<WorkPeriod>();
            List<WorkPeriod> workHistory = new List<WorkPeriod>();
            WorkPeriod tmp;
            CultureInfo oCL = new CultureInfo("en-US");
            oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetWorkHistory(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.CompanyCode);

            foreach (WorkPeriod row in oResult)
            {
                string period = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
                    .GetCalculatePeriod(DateTime.ParseExact(row.STARTDATE, "dd/MM/yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(row.ENDDATE, "dd/MM/yyyy", CultureInfo.InvariantCulture), oRequestParameter.CurrentEmployee.Language);
                tmp = new WorkPeriod();
                tmp = row;
                tmp.WorkTime = period;
                workHistory.Add(tmp);
            }
            return workHistory;
        }

        #endregion

        #region "SalaryRate"
        [HttpPost]
        public List<SALARYRATE> GetSalaryRate([FromBody] RequestParameter oRequestParameter)
        {
            List<SALARYRATE> oResult = new List<SALARYRATE>();

            oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetSalaryRate(oRequestParameter.Requestor.EmployeeID);

            var date_now = DateTime.Now;
            // หาเงินเดือนและ % ของเงินเดือน

            decimal salary = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetSalaryByEmployee(oRequestParameter.Requestor.EmployeeID);
            if (oResult.Count > 0)
            {
                foreach (var item in oResult)
                {
                    if (salary > 0)
                    {
                        decimal percen = ((salary - item.Minimum) / (item.Maximum - item.Minimum)) * 100;
                        item.Salary = Math.Round(salary, 2); ;
                        item.PercentageSalary = Math.Round(percen, 2);
                        //item.PercentageSalary = 50;
                    }
                }
            }

            return oResult;
        }
        #endregion

        #region "Personal Education ALL"
        [HttpPost]
        //public List<INFOTYPE0022> GetAllPersonalEducationHistory([FromBody] RequestParameter oRequestParameter)
        public object GetAllPersonalEducationHistory([FromBody] RequestParameter oRequestParameter)
        {

            Dictionary<string, List<FileAttachmentForMobile>> dictFileAttachmentList = new Dictionary<string, List<FileAttachmentForMobile>>();
            int requestTypeID = 607;


            CommonText oCommonText = new CommonText(oRequestParameter.Requestor.CompanyCode);

            List<INFOTYPE0022> oEducationData = new List<INFOTYPE0022>();
            INFOTYPE0022 tmp = new INFOTYPE0022();
            List<INFOTYPE0022> oResult = new List<INFOTYPE0022>();
            oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetEducation(oRequestParameter.Requestor.EmployeeID);

            for (int i = 0; i < oResult.Count; i++)
            {
                //string brcode1 = oResult[i].Branch1;
                //string brcode2 = oResult[i].Branch2;
                //oResult[i].Branch1 = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetBranchData(brcode1).BranchText;
                //oResult[i].Branch2 = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetBranchData(brcode2).BranchText;


                string requestSubType = oResult[i].EducationLevelCode;
                List<FileAttachmentForMobile> FileAttachmentList = new List<FileAttachmentForMobile>();


                List<PersonalAttachmentFileSet> oAttExport = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAttachmentExport(oRequestParameter.Requestor.EmployeeID, requestTypeID, requestSubType);
                for (int j = 0; j < oAttExport.Count; j++)
                {
                    FileAttachmentForMobile file = new FileAttachmentForMobile();
                    file.FileID = oAttExport[j].FileID;
                    file.FileName = oAttExport[j].FileName;
                    file.FileSetID = oAttExport[j].FileSetID;
                    file.FilePath = oAttExport[j].FilePath;
                    file.FileType = oAttExport[j].FileType;
                    FileAttachmentList.Add(file);
                }

                if (!dictFileAttachmentList.ContainsKey(requestSubType))
                    dictFileAttachmentList.Add(requestSubType, FileAttachmentList);

            }


            object oReturn = new
            {
                oResult,
                dictFileAttachmentList
            };

            foreach (KeyValuePair<string, List<FileAttachmentForMobile>> dictFile in dictFileAttachmentList)
            {
                List<FileAttachmentForMobile> FileAttachmentForMobileList = new List<FileAttachmentForMobile>();
                List<PersonalAttachmentFileSet> oAttFileSet = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetAttachmentFileSet(oRequestParameter.Requestor.EmployeeID, requestTypeID, dictFile.Key);
                for (int i = 0; i < oAttFileSet.Count; i++)
                {
                    FileAttachmentForMobile file = new FileAttachmentForMobile();
                    file.FileID = oAttFileSet[i].FileID;
                    file.FileName = oAttFileSet[i].FileName;
                    file.FileSetID = oAttFileSet[i].FileSetID;
                    file.FilePath = oAttFileSet[i].FilePath;
                    file.FileType = oAttFileSet[i].FileType;
                    //FileAttachmentForMobileList.Add(file);
                    dictFile.Value.Add(file);
                }
            }


            //return oResult;
            return oReturn;
        }
        #endregion

        #region "Branch"
        [HttpPost]
        public string GetBranch([FromBody] RequestParameter oRequestParameter)
        {
            string oResult = string.Empty;
            Branch branch = new Branch();
            branch = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetBranchData(oRequestParameter.InputParameter["BranchCode"].ToString());
            if (branch != null)
                oResult = branch.BranchText;
            return oResult;
        }
        #endregion

        #region "Certificate"
        [HttpPost]
        public List<Certificate> GetCertificateByEducationLevel([FromBody] RequestParameter oRequestParameter)
        {
            List<Certificate> listCertificate = new List<Certificate>();
            listCertificate = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetCertificateByEducationLevel(oRequestParameter.InputParameter["EducationLevel"].ToString());
            return listCertificate;
        }
        #endregion

        #region "Education Select Data"
        [HttpPost]
        public object GetEducationSelectData(RequestParameter oRequestParameter)
        {
            string LanguageCode = oRequestParameter.CurrentEmployee.Language;
            List<EducationLevel> oEducationLevel = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllEducationLevel();
            string levelDefault = oRequestParameter.InputParameter["EducationLevelCode"].ToString();
            List<Branch> branchlist = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllBranch();
            //if (!string.IsNullOrEmpty(levelDefault))
            //{
            //    branchlist = branchlist.FindAll(delegate (Branch item) { return item.EducationLevelCode == levelDefault; });
            //}
            //else { branchlist = null; }

            object oReturn = new
            {
                educationlevel = oEducationLevel,
                institute = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllInstitute(),
                country = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllCountryList(LanguageCode),
                certificate = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllCertificate(),
                educationgroup = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAllEducationGroup(),
                branch1 = branchlist


            };
            return oReturn;
        }
        #endregion

        [HttpPost]
        public object GetEmployeeAllData([FromBody] RequestParameter oRequestParameter)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            PersonalData oPersonalData = new PersonalData();
            DateTime oBeginDate = !oRequestParameter.InputParameter.ContainsKey("BeginDate") ? DateTime.MinValue : DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
            Dictionary<string, object> oTemp = EmployeeManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).INFOTYPE0002And0182Get(oRequestParameter.Requestor.EmployeeID, oBeginDate);
            string oINF2Str = JSon.Serialize(oTemp["INFOTYPE0002"]);
            List<INFOTYPE0002> oINF2 = JSon.Deserialize<List<INFOTYPE0002>>(oINF2Str);
            string oINF182Str = JSon.Serialize(oTemp["INFOTYPE0182"]);
            List<ESS.EMPLOYEE.CONFIG.PA.INFOTYPE0182> oINF182 = JSon.Deserialize<List<ESS.EMPLOYEE.CONFIG.PA.INFOTYPE0182>>(oINF182Str);

            ESS.EMPLOYEE.CONFIG.PA.INFOTYPE0001 oINF1 = EmployeeManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).INFOTYPE0001Get(oRequestParameter.Requestor.EmployeeID, oBeginDate, oRequestParameter.CurrentEmployee.Language);
            EmployeeData Requestor = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oINF1.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);
            string sNAME = Requestor.AlternativeName(oRequestParameter.CurrentEmployee.Language);

            DateTime HiringDate = Requestor.DateSpecific.HiringDate;
            DateTime RetirementDate = Requestor.DateSpecific.RetirementDate;

            int i = 0;
            foreach (INFOTYPE0002 row in oINF2)
            {
                oPersonalData = new PersonalData
                {
                    EmployeeID = oRequestParameter.Requestor.EmployeeID,
                    BeginDate = row.BeginDate,
                    EndDate = row.EndDate,
                    TitleID = row.TitleID,
                    FirstName = row.FirstName,
                    LastName = row.LastName,
                    NickName = row.NickName,
                    //FullNameEn = oINF182[i].AlternateName,
                    FullNameEn = sNAME,
                    DOB = row.DOB,
                    Gender = row.Gender,
                    Nationality = row.Nationality,
                    MaritalStatus = row.MaritalStatus,
                    Religion = row.Religion,
                    Language = row.Language
                };
                i++;
            }

            string period = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetCalculatePeriod(oINF2[0].DOB, DateTime.Now, oRequestParameter.CurrentEmployee.Language);

            OrganizationData oResultOraData = Convert<OrganizationData>.ObjectFrom(oINF1);

            //string Suan = GetOrganizationType(Requestor.OrgAssignment.OrgUnit, OrgLevelType.VP, oRequestParameter.Requestor.CompanyCode, oRequestParameter.CurrentEmployee.Language);
            //string Phai = GetOrganizationType(Requestor.OrgAssignment.OrgUnit, OrgLevelType.SVP, oRequestParameter.Requestor.CompanyCode, oRequestParameter.CurrentEmployee.Language);


            string sDevision = GetOrganizationType(Requestor.OrgAssignment.OrgUnit, OrgLevelType.Devision, oRequestParameter.Requestor.CompanyCode, oRequestParameter.CurrentEmployee.Language);
            string sSection = GetOrganizationType(Requestor.OrgAssignment.OrgUnit, OrgLevelType.Section, oRequestParameter.Requestor.CompanyCode, oRequestParameter.CurrentEmployee.Language);
            string sDepartment = GetOrganizationType(Requestor.OrgAssignment.OrgUnit, OrgLevelType.Department, oRequestParameter.Requestor.CompanyCode, oRequestParameter.CurrentEmployee.Language);
            string sSeniorDepartment = GetOrganizationType(Requestor.OrgAssignment.OrgUnit, OrgLevelType.SeniorDepartment, oRequestParameter.Requestor.CompanyCode, oRequestParameter.CurrentEmployee.Language);
            string sLineOfWork = GetOrganizationType(Requestor.OrgAssignment.OrgUnit, OrgLevelType.LineOfWork, oRequestParameter.Requestor.CompanyCode, oRequestParameter.CurrentEmployee.Language);
            string sUnderTo = GetOrganizationType(Requestor.OrgAssignment.OrgUnit, OrgLevelType.UnderTo, oRequestParameter.Requestor.CompanyCode, oRequestParameter.CurrentEmployee.Language);
            string sBelongTo = GetOrganizationType(Requestor.OrgAssignment.OrgUnit, OrgLevelType.BelongTo, oRequestParameter.Requestor.CompanyCode, oRequestParameter.CurrentEmployee.Language);

            string sConcatDepSeniorDep = string.Empty;
            string sConcatLineOfWorkUnderTo = string.Empty;

            //INFOTYPE0105 oINF105 = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).INFOTYPE0105Get(oRequestParameter.InputParameter["EmployeeID"].ToString(), oBeginDate, "");
            //ContactData oResultContData = Convert<ContactData>.ObjectFrom(oINF105);
            //oResultContData.Name = oINF1.Name;

            RelatedAge EmpRelatedAge = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetRelatedAge(oRequestParameter.Requestor.EmployeeID, DateTime.Now);
            string ageOfCompany = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GenAge(EmpRelatedAge.ageOfCompany, oRequestParameter.CurrentEmployee.Language);
            string ageOfLevel = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GenAge(EmpRelatedAge.ageOfLevel, oRequestParameter.CurrentEmployee.Language);

            string sWorkLocationName = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetWorkLocationNameByEmpID(oRequestParameter.Requestor.EmployeeID, DateTime.Now);

            string StartWorkAge = string.Empty;

            List<DateTime> ListStartWorkAge = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetStartWorkAge(oRequestParameter.Requestor.EmployeeID);

            //if(ListStartWorkAge != null && ListStartWorkAge.Count >0)
            //{
            //    ageOfCompany +=  " (" + HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
            //                .GetCommonText("CVREPORT", oRequestParameter.CurrentEmployee.Language, "STARTWORKAGE") + " " + Convert.ToDateTime(ListStartWorkAge[0]).ToString("dd/MM/yyyy", oCL) + ") ";
            //}

            if (ListStartWorkAge != null && ListStartWorkAge.Count > 0)
            {
                StartWorkAge = Convert.ToDateTime(ListStartWorkAge[0]).ToString("dd/MM/yyyy", oCL);
            }

            if (sDepartment != "-")
            {
                if(sSeniorDepartment != "-")
                {
                    sConcatDepSeniorDep = sDepartment + " / " + sSeniorDepartment;
                }
                else
                {
                    sConcatDepSeniorDep = sDepartment;
                }                
            }
            else
            {
                sConcatDepSeniorDep = sSeniorDepartment;
            }

            if (sLineOfWork != "-")
            {
                if (sUnderTo != "-")
                {
                    sConcatLineOfWorkUnderTo = sLineOfWork + " / " + sUnderTo;
                }
                else
                {
                    sConcatLineOfWorkUnderTo = sLineOfWork;
                }
            }
            else
            {
                sConcatLineOfWorkUnderTo = sUnderTo;
            }


            object oResult = new
            {
                PersonData = oPersonalData,
                OrgData = oResultOraData,
                // ContData = oResultContData,
                HiringDate,
                RetirementDate,
                period,
                ageOfCompany,
                ageOfLevel,
                Position = oRequestParameter.CurrentEmployee.Language == "EN" ? Requestor.CurrentPosition.TextEn : Requestor.CurrentPosition.Text,
                //OrgUnitName = oRequestParameter.CurrentEmployee.Language == "EN" ? Requestor.OrgAssignment.OrgUnitData.TextEn : Requestor.OrgAssignment.OrgUnitData.Text,
                OrgUnitName = sDevision,
                Section = sSection,
                Department = sDepartment,
                SeniorDepartment = sSeniorDepartment,
                LineOfWork = sLineOfWork,
                BelongTo = sBelongTo,
                ConcatDepSeniorDep = sConcatDepSeniorDep,
                ConcatLineOfWorkUnderTo = sConcatLineOfWorkUnderTo,
                WorkLocationName = sWorkLocationName,
                AreaCode = oINF1.Area,
                StartWorkAge
            };

            return oResult;
        }

        [HttpPost]
        public List<INFOTYPE0021> GetProvidentMemberData([FromBody] RequestParameter oRequestParameter)
        {
            List<INFOTYPE0021> oResult = new List<INFOTYPE0021>();
            oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetProvidentMemberData(oRequestParameter.Requestor.EmployeeID);

            return oResult;
        }

        [HttpPost]
        public bool ShowTimeAwareLink([FromBody]RequestParameter oRequestParameter)
        {
            bool isShowTimeAwareLink = false;
            isShowTimeAwareLink = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).ShowTimeAwareLink(Convert.ToInt32(oRequestParameter.InputParameter["LinkID"]));
            return isShowTimeAwareLink;
        }

        private string GetOrganizationType(string OrgID, OrgLevelType orgType, string companyCode)
        {
            string _orgid = OrgID;
            INFOTYPE1010 infotype1010 = null;
            do
            {
                if ((OrgLevelType)Convert.ToInt16(OMManagement.CreateInstance(companyCode).GetLevelByOrg(_orgid, DateTime.Now).OrgLevel) == orgType)
                {
                    infotype1010 = OMManagement.CreateInstance(companyCode).GetLevelByOrg(_orgid, DateTime.Now);
                    break;
                }

                else
                {
                    try
                    {
                        _orgid = OMManagement.CreateInstance(companyCode).GetOrgParent(_orgid, DateTime.Now, "TH").ObjectID;

                    }
                    catch
                    {
                        _orgid = string.Empty;
                    }
                }
            } while (!String.IsNullOrEmpty(_orgid));


            if (infotype1010 != null)
                return OMManagement.CreateInstance(companyCode).GetObjectData(ObjectType.O, infotype1010.ObjectID, DateTime.Now, "TH").Text;

            else
                return "-";
        }

        private string GetOrganizationType(string OrgID, OrgLevelType orgType, string companyCode, string language)
        {
            string _orgid = OrgID;
            INFOTYPE1010 infotype1010 = null;
            do
            {
                if ((OrgLevelType)Convert.ToInt16(OMManagement.CreateInstance(companyCode).GetLevelByOrg(_orgid, DateTime.Now).OrgLevel) == orgType)
                {
                    infotype1010 = OMManagement.CreateInstance(companyCode).GetLevelByOrg(_orgid, DateTime.Now);
                    break;
                }

                else
                {
                    try
                    {
                        _orgid = OMManagement.CreateInstance(companyCode).GetOrgParent(_orgid, DateTime.Now, language).ObjectID;

                    }
                    catch
                    {
                        _orgid = string.Empty;
                    }
                }
            } while (!String.IsNullOrEmpty(_orgid));


            if (infotype1010 != null)
                return OMManagement.CreateInstance(companyCode).GetObjectData(ObjectType.O, infotype1010.ObjectID, DateTime.Now, language).Text;

            else
                return "-";
        }

        [HttpPost]
        public string GetIDCardData([FromBody]RequestParameter oRequestParameter)
        {
            List<string> oResult = new List<string>();
            string sIDCardNo = string.Empty;
            oResult = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetIDCardData(oRequestParameter.InputParameter["EmployeeID"].ToString());
            if (oResult != null && oResult.Count > 0)
            {
                sIDCardNo = oResult[0].ToString();
            }
            return sIDCardNo;
        }


        #region Driver License ข้อมูลใบขับขี่

        [HttpPost]
        public PersonalDriverLicenseData GetDriverLicenseByEmployee([FromBody]RequestParameter oRequestParameter)
        {
            PersonalDriverLicenseData driver_License = new PersonalDriverLicenseData();
            List<PersonalDriverLicenseData> list_driverLicense = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
                .GetDriverLicenseByEmployee(oRequestParameter.Requestor.EmployeeID);

            // กรณีมี 2 ใบขับขี่ที่ยังไม่หมดอายุให้เลือกบัตรที่ใกล้หมดอายุมากที่สุด
            if (list_driverLicense.Count > 0)
            {
                driver_License = list_driverLicense.OrderBy(s => s.EffectiveDate).FirstOrDefault();
            }

            return driver_License;
        }

        [HttpPost]
        public PaginationPersonalDriverLicenseData GetDriverLicenseEmployeeAll([FromBody]RequestParameter oRequestParameter)
        {
            PaginationPersonalDriverLicenseData pagination_data = new PaginationPersonalDriverLicenseData();

            int page = Convert.ToInt16(oRequestParameter.InputParameter["Page"]);
            int itemPerPage = Convert.ToInt16(oRequestParameter.InputParameter["ItemPerPage"]);
            string sComapny = oRequestParameter.InputParameter["sCompany"].ToString();

            var list_personal_license = new List<PersonalDriverLicenseData>();
            list_personal_license = HRPAManagement.CreateInstance(sComapny)
                .GetDriverLicenseEmployeeAll();

            var newResult = list_personal_license.Skip((page - 1) * itemPerPage).Take(itemPerPage).ToList();

            pagination_data.ItemPerPage = itemPerPage;
            pagination_data.Page = page;
            pagination_data.Total = list_personal_license.Count;
            pagination_data.ListPersonalDriverLicenseData = newResult;

            return pagination_data;
        }

        [HttpPost]
        public HttpResponseMessage ExportExcelDriverLicenseEmployeeAll([FromBody]RequestParameter oRequestParameter)
        {
            DateTime date_now = DateTime.Now;

            var workbook = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).ExportExcelDriverLicenseEmployee();

            var stream = new MemoryStream();
            workbook.SaveAs(stream);
            stream.Position = 0;

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(stream.ToArray())
            };

            result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = "ReportDriverLicenseEMployee" + date_now.Year + date_now.Month + date_now.Date + date_now.Minute + date_now.Second + ".xlsx"
            };
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

            return result;
        }

        #endregion

        #region รายงานประวัติพนักงาน
        [HttpPost]
        public CVPersonal PerosonalCv([FromBody]RequestParameter oRequestParameter)
        {
            string pincode = oRequestParameter.InputParameter["PinCode"].ToString();
            bool isVerifyPinCode = false;
            isVerifyPinCode = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                .VerifyPinCode(oRequestParameter.CurrentEmployee.EmployeeID, pincode);

            CVPersonal personal_cv = new CVPersonal();

            if (isVerifyPinCode)
            {
                EmployeeDataForMobile oRequestor = oRequestParameter.Requestor;
                SetAuthenticate(oRequestor);

                var data_personal = GetEmployeeAllData(oRequestParameter);
                var data_personal_education = GetAllPersonalEducationHistory(oRequestParameter);
                var data_personal_working = HistoryWorking(oRequestParameter);
                var data_personal_salary = GetSalaryRate(oRequestParameter);
                var data_personal_assessment_history = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
                    .GetAppraisalINFOTYPE0348(oRequestParameter.Requestor.EmployeeID);

                personal_cv.IsPincodeValid = true;
                personal_cv.Personal = data_personal;
                personal_cv.Education = data_personal_education;
                personal_cv.Working = data_personal_working;
                personal_cv.Salary = data_personal_salary;

                //ประวัติผลการประเมิน
                if (data_personal_assessment_history.Count > 0)
                {
                    foreach (var item in data_personal_assessment_history)
                    {
                        var data = new INFOTYPE0348SAP()
                        {
                            AppraisalGrade = item.AppraisalGrade,
                            EmployeeId = item.EmployeeId,
                            IncreaseSalaryPercent = item.IncreaseSalaryPercent,
                            Ranking = item.Ranking,
                            Year = item.Year
                        };
                        personal_cv.AssessmentHistory.Add(data);
                    }
                }

                // ระดับพนักงานตั้งแต่ 11 ขึ้นไปสามารถ Export PPTX ได้
                int level_emp = Convert.ToInt16(oRequestParameter.Requestor.EmpSubGroup);
                int level_export_pptx = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetLevelEmployeeExportPowerPoint();
                if (level_export_pptx > 0)
                {
                    if (level_emp >= level_export_pptx)
                        personal_cv.IsExportPptx = true;
                }
                else
                {
                    // Config จาก db หากเป็น 0 จะ by pass level
                    personal_cv.IsExportPptx = true;
                }


                // เป็น PAADMIN สามารถ Export ได้
                string level_admin_export_pptx = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetLevelLevelAdminExportPowerPoint().ToLower();
                var pa_admin = oRequestParameter.CurrentEmployee.UserRoles.Where(s => s.ToLower() == level_admin_export_pptx).FirstOrDefault();
                //var pa_admin = WorkflowPrinciple.Current.UserSetting.Roles.Where(s => s.ToLower() == level_admin_export_pptx).FirstOrDefault();
                if (pa_admin != null)
                    personal_cv.IsExportPptx = true;
            }
            else
            {
                personal_cv.IsPincodeValid = false;
            }



            return personal_cv;
        }

        [HttpPost]
        public PaginationWorkingPeriod HistoryWorking([FromBody]RequestParameter oRequestParameter)
        {
            PaginationWorkingPeriod pagination_work_period = new PaginationWorkingPeriod();

            string pincode = oRequestParameter.InputParameter["PinCode"].ToString();
            int page = Convert.ToInt16(oRequestParameter.InputParameter["Page"]);
            int itemPerPage = Convert.ToInt16(oRequestParameter.InputParameter["ItemPerPage"]);

            bool isVerifyPinCode = false;

            isVerifyPinCode = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                .VerifyPinCode(oRequestParameter.CurrentEmployee.EmployeeID, pincode);

            if (isVerifyPinCode)
            {
                List<WorkPeriod> oResult = new List<WorkPeriod>();
                List<WorkPeriod> workHistory = new List<WorkPeriod>();
                WorkPeriod tmp;
                CultureInfo oCL = new CultureInfo("en-US");
                oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetWorkHistory(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.CompanyCode);

                oResult.Reverse();

                if (oResult.Count > 0)
                {
                    var newResult = oResult.Skip((page - 1) * itemPerPage).Take(itemPerPage).ToList();

                    if (newResult.Count > 0)
                    {
                        try
                        {
                            foreach (WorkPeriod row in newResult)
                            {
                                string period = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
                                    .GetCalculatePeriod(DateTime.ParseExact(row.STARTDATE, "dd/MM/yyyy", oCL), DateTime.ParseExact(row.ENDDATE, "dd/MM/yyyy", oCL), oRequestParameter.CurrentEmployee.Language);
                                tmp = new WorkPeriod();
                                tmp = row;
                                tmp.WorkTime = period;
                                workHistory.Add(tmp);
                            }
                        }
                        catch
                        {

                        }
                    }

                    pagination_work_period.ItemPerPage = itemPerPage;
                    pagination_work_period.Page = page;
                    pagination_work_period.Total = oResult.Count;
                    pagination_work_period.ListWorkPeriod = newResult;
                }
            }

            return pagination_work_period;
        }

        [HttpPost]
        public string ExportPowerPointEmployee([FromBody]RequestParameter oRequestParameter)
        {
            string pincode = oRequestParameter.InputParameter["PinCode"].ToString();
            bool isVerifyPinCode = false;
            isVerifyPinCode = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                .VerifyPinCode(oRequestParameter.CurrentEmployee.EmployeeID, pincode);

            string naem_file = "";
            if (isVerifyPinCode)
            {
                string path_template_pptx = "";
                if (oRequestParameter.Requestor.Language == "TH")
                {
                    path_template_pptx = System.Web.HttpContext.Current.Server.MapPath("~/templatePPTx/GPSC Leadership_TH.PPTX");
                }

                if (oRequestParameter.Requestor.Language == "EN")
                {
                    path_template_pptx = System.Web.HttpContext.Current.Server.MapPath("~/templatePPTx/GPSC Leadership_EN.PPTX");
                }

                string path_picture = EmployeeManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetPathPictureUrl();
                path_picture = path_picture + oRequestParameter.Requestor.EmployeeID + ".jpg";
                var wc = new WebClient();
                byte[] byte_picture = wc.DownloadData(path_picture);
                
                naem_file = "PPTX_" + oRequestParameter.Requestor.CompanyCode + oRequestParameter.Requestor.EmployeeID.Trim() + ".pptx";
                string path_template_pptx_save = System.Web.HttpContext.Current.Server.MapPath("~/Client/Report/" + naem_file);

                string emp_id = oRequestParameter.Requestor.EmployeeID;
                string language = oRequestParameter.Requestor.Language;

                PowerpointProfileModel model = new PowerpointProfileModel();
                model.ImagePath = path_picture;

                #region ข้อมูลบริษัท

                EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
                SetAuthenticate(oCurrentEmployee);

                var result = new List<CompanyModel>();
                var oMainCompany = new CompanyModel()
                {
                    CompanyCode = oRequestParameter.CurrentEmployee.CompanyCode
                };
                result.Add(oMainCompany);

                DataTable dtCompany = WorkflowManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetUserRoleResponseForOtherCompany(oRequestParameter.Requestor.EmployeeID);
                if (dtCompany != null && dtCompany.Rows.Count > 0)
                {
                    foreach (DataRow dRow in dtCompany.Rows)
                    {
                        var oCompany = new CompanyModel()
                        {
                            CompanyCode = dRow["ResponseCompanyCode"].ToString()
                        };
                        result.Add(oCompany);
                    }
                }
                //ResponseCompanyCode
                var oList = ShareDataManagement.GetCompanyList();

                foreach (CompanyModel oCompany in result)
                {
                    var oMapCompany = (from n in oList
                                       where n.CompanyCode == oCompany.CompanyCode
                                       select n).FirstOrDefault();
                    if (oMapCompany != null)
                    {
                        oCompany.Name = oMapCompany.Name;
                        oCompany.DomainName = oMapCompany.DomainName;
                        oCompany.FullNameTH = oMapCompany.FullNameTH;
                        oCompany.FullNameEN = oMapCompany.FullNameEN;
                    }
                }

                #endregion

                #region  ข้อมูลส่วนบุคคล
                PersonalData oPersonalData = new PersonalData();
                DateTime oBeginDate = !oRequestParameter.InputParameter.ContainsKey("BeginDate") ? DateTime.MinValue : DateTime.ParseExact(oRequestParameter.InputParameter["BeginDate"].ToString(), "yyyy-MM-dd", null);
                Dictionary<string, object> oTemp = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).INFOTYPE0002And0182Get(oRequestParameter.Requestor.EmployeeID, oBeginDate);
                string oINF2Str = JSon.Serialize(oTemp["INFOTYPE0002"]);
                List<INFOTYPE0002> oINF2 = JSon.Deserialize<List<INFOTYPE0002>>(oINF2Str);
                string oINF182Str = JSon.Serialize(oTemp["INFOTYPE0182"]);
                List<ESS.EMPLOYEE.CONFIG.PA.INFOTYPE0182> oINF182 = JSon.Deserialize<List<ESS.EMPLOYEE.CONFIG.PA.INFOTYPE0182>>(oINF182Str);

                ESS.EMPLOYEE.CONFIG.PA.INFOTYPE0001 oINF1 = EmployeeManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).INFOTYPE0001Get(oRequestParameter.Requestor.EmployeeID, oBeginDate, oRequestParameter.CurrentEmployee.Language);
                EmployeeData Requestor = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oINF1.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);
                string sNAME = Requestor.AlternativeName(oRequestParameter.CurrentEmployee.Language);

                DateTime HiringDate = Requestor.DateSpecific.HiringDate;
                DateTime RetirementDate = Requestor.DateSpecific.RetirementDate;

                int i = 0;
                foreach (INFOTYPE0002 row in oINF2)
                {
                    oPersonalData = new PersonalData
                    {
                        EmployeeID = oRequestParameter.Requestor.EmployeeID,
                        BeginDate = row.BeginDate,
                        EndDate = row.EndDate,
                        TitleID = row.TitleID,
                        FirstName = row.FirstName,
                        LastName = row.LastName,
                        NickName = row.NickName,
                        //FullNameEn = oINF182[i].AlternateName,
                        FullNameEn = sNAME,
                        DOB = row.DOB,
                        Gender = row.Gender,
                        Nationality = row.Nationality,
                        MaritalStatus = row.MaritalStatus,
                        Religion = row.Religion,
                        Language = row.Language
                    };
                    i++;
                }

                string period = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetCalculatePeriod(oINF2[0].DOB, DateTime.Now, oRequestParameter.CurrentEmployee.Language);

                OrganizationData oResultOraData = Convert<OrganizationData>.ObjectFrom(oINF1);

                //string Suan = GetOrganizationType(Requestor.OrgAssignment.OrgUnit, OrgLevelType.VP, oRequestParameter.Requestor.CompanyCode);
                //string Phai = GetOrganizationType(Requestor.OrgAssignment.OrgUnit, OrgLevelType.SVP, oRequestParameter.Requestor.CompanyCode);

                string Suan = GetOrganizationType(Requestor.OrgAssignment.OrgUnit, OrgLevelType.Section, oRequestParameter.Requestor.CompanyCode);
                string Phai = GetOrganizationType(Requestor.OrgAssignment.OrgUnit, OrgLevelType.Department, oRequestParameter.Requestor.CompanyCode);


                RelatedAge EmpRelatedAge = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetRelatedAge(oRequestParameter.Requestor.EmployeeID, DateTime.Now);
                string ageOfCompany = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GenAge(EmpRelatedAge.ageOfCompany, oRequestParameter.CurrentEmployee.Language);
                string ageOfLevel = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GenAge(EmpRelatedAge.ageOfLevel, oRequestParameter.CurrentEmployee.Language);

                string company_data = "";
                var data_company = result.Where(s => s.CompanyCode == oRequestParameter.Requestor.CompanyCode).FirstOrDefault();
                if(data_company != null)
                {
                    company_data = data_company.DomainName;
                }

                // Assign ข้อมุลพนักงาน
                model.Title = oPersonalData.TitleID;
                if(oRequestParameter.CurrentEmployee.Language == "EN")
                {
                    model.Title = "";
                    model.Firstname = sNAME;
                    model.Lastname = "";
                }
                else
                {
                    model.Title = oPersonalData.TitleID;
                    model.Firstname = oPersonalData.FirstName;
                    model.Lastname = oPersonalData.LastName;
                }
                
                model.Company = company_data;
                model.EmployeeDate = HiringDate;
                model.PttPositionDate = new DateTime(2014, 4, 3);
                model.RetireYear = RetirementDate.Year;
                model.RetireDate = RetirementDate;
                model.Position = oRequestParameter.CurrentEmployee.Language == "EN" ? Requestor.CurrentPosition.TextEn : Requestor.CurrentPosition.Text;
                model.Unit = oRequestParameter.CurrentEmployee.Language == "EN" ? Requestor.OrgAssignment.OrgUnitData.TextEn : Requestor.OrgAssignment.OrgUnitData.Text;
                model.JobGrade = oResultOraData.EmpSubGroup.ToString();
                model.JobGradeDate = oResultOraData.BeginDate;
                model.Birthdate = oPersonalData.DOB;
                //model.ImagePath = path_picture;
                model.AsOfDate = DateTime.Now;
                model.ageOfCompany = ageOfCompany;
                model.ageOfLevel = ageOfLevel;
                model.agePersonal = period;

                #endregion

                #region ข้อมูลการศึกษา

                List<INFOTYPE0022> oEducationData = new List<INFOTYPE0022>();
                INFOTYPE0022 tmp = new INFOTYPE0022();
                List<INFOTYPE0022> oResult = new List<INFOTYPE0022>();
                oResult = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetEducation(oRequestParameter.Requestor.EmployeeID);

                if (oResult.Count > 0)
                {
                    foreach (var item in oResult)
                    {
                        var Educations = new PowerpointProfileModel.EducationProfile();
                        Educations.Year = item.EndDate.Year;
                        Educations.Grade = item.Grade.ToString();

                        Educations.Level = HRPAManagement
                            .CreateInstance(oRequestParameter.Requestor.CompanyCode)
                            .GetCommonText("EDUCATIONLEVEL", "TH", item.EducationLevelCode); //ระดับการษึกษา

                        Educations.Grade = HRPAManagement
                            .CreateInstance(oRequestParameter.Requestor.CompanyCode)
                            .GetCommonText("CERTIFICATE", "TH", item.CertificateCode); // วุฒิการศึกษา

                        Educations.Field = HRPAManagement
                            .CreateInstance(oRequestParameter.Requestor.CompanyCode)
                            .GetCommonText("BRANCH", "TH", item.Branch1); //สาขาวิชาเอก

                        Educations.Institute = HRPAManagement
                            .CreateInstance(oRequestParameter.Requestor.CompanyCode)
                            .GetCommonText("INSTITUTE", "TH", item.InstituteCode);  // สถานบันการศึกษา

                        Educations.Country = HRPAManagement
                            .CreateInstance(oRequestParameter.Requestor.CompanyCode)
                            .GetCommonText("COUNTRY", "TH", item.CountryCode);  // ประเทศ

                        model.Educations.Add(Educations);
                    }
                }

                #endregion

                #region ประวัติการดำรงตำแหน่ง

                List<WorkPeriod> work_period = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetWorkHistory(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.CompanyCode);
                if (work_period.Count > 0)
                {
                    CultureInfo oCL = new CultureInfo("en-US");

                    string tmpPosition = string.Empty;
                    string tmpUnit = string.Empty;
                    DateTime tmpStart = DateTime.MinValue;
                    DateTime tmpEnd = DateTime.MinValue; ;

                    foreach (var item in work_period)
                    {
                        try
                        {
                            var Employements = new PowerpointProfileModel.EmployementProfile();
                            Employements.Position = item.POS_NAME;
                            Employements.Unit = item.ORG_NAME;
                            Employements.Start = DateTime.ParseExact(item.STARTDATE, "dd/MM/yyyy", oCL);
                            Employements.End = DateTime.ParseExact(item.ENDDATE, "dd/MM/yyyy", oCL);

                            string period_txt_history = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
                                .GetCalculatePeriod(DateTime.ParseExact(item.STARTDATE, "dd/MM/yyyy", oCL), DateTime.ParseExact(item.ENDDATE, "dd/MM/yyyy", oCL), oRequestParameter.CurrentEmployee.Language);

                            Employements.AgeWorkTime = period_txt_history;
                            Employements.Company = company_data;

                            if (Employements.Position != tmpPosition || Employements.Unit != tmpUnit || Employements.Start != tmpStart || Employements.End != tmpEnd)
                            {
                                model.Employements.Add(Employements);

                                tmpPosition = item.POS_NAME;
                                tmpUnit = item.ORG_NAME;
                                tmpStart = DateTime.ParseExact(item.STARTDATE, "dd/MM/yyyy", oCL);
                                tmpEnd = DateTime.ParseExact(item.ENDDATE, "dd/MM/yyyy", oCL);
                            }
                        }
                        catch
                        {

                        }
                    }
                }

                #endregion

                #region ผลการประเมิน

                var data_personal_assessment_history = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetAppraisalINFOTYPE0348(oRequestParameter.Requestor.EmployeeID);

                if (data_personal_assessment_history.Count > 0)
                {
                    foreach (var item in data_personal_assessment_history)
                    {
                        var Performances = new PowerpointProfileModel.PerformanceProfile()
                        {
                            Year = Convert.ToInt16(item.Year),
                            Ranking = item.AppraisalGrade.ToString(),
                            Result = item.Ranking.ToString()
                        };
                        model.Performances.Add(Performances);
                    }
                }

                #endregion

                HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
                    .ExportInformationEmployeePowerPoint(emp_id, path_template_pptx, path_template_pptx_save, language, model, byte_picture);
            }

            return naem_file;
        }

        #endregion

        #region เอกสารของพนักงาน  Home

        [HttpPost]
        public int GetRequestByBoxId([FromBody]RequestParameter oRequestParameter)
        {
            int count = 0;

            string boxId = oRequestParameter.InputParameter["BoxId"].ToString();

            int box_con = Convert.ToInt16(boxId);

            count = HRPAManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
                .GetRequestByBoxId(box_con, oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID);
            return count;
        }

        #endregion
    }
}