﻿using iSSWS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ESS.API;
using System.Net.Http;
using ESS.API.DATACLASS;
using System.Net;
using System.Web.Http;
using System.Web.Http.Cors;
using ESS.SHAREDATASERVICE;
using ESS.API.FILTERS;

namespace iSSWS.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SecurityController : ApiController
    {
        public SecurityController()
        {
        }
        /// <summary>
        /// Service สำหรับ เช็คสิทธิ์ ในการใช้งาน API 
        /// </summary>
        /// <param name="request">ส่ง user/pasword มาเพื่อ check สิทธิ์</param>
        /// <returns>ถ้ามีสิทธิระบบจะคื่นค่า Token เพื่อเรียกใช้ Service อื่นๆ ได้</returns>
        [HttpPost, HttpGet]
        public IHttpActionResult AuthenticationUser([FromBody] UserLogin oRequestParameter)
        {
            IHttpActionResult response = Unauthorized();
            ApiUser user = new ApiUser();
            try
            {
                user = APIManagement.CreateInstance(oRequestParameter.CompanyCode).Authorize(oRequestParameter.Username, oRequestParameter.Password);
                if (!string.IsNullOrEmpty(user.UserName))
                {
                    response = Ok(new { status = true,errormessage = "",token = user.Token });

                }
                else
                {
                    response = Ok(new { status = false, errormessage = "User not found", token = string.Empty });
                }

            }
            catch (Exception ex)
            {
                response = Ok(new { status = false, errormessage = ex.Message, token = string.Empty });
            }
            return response;
        }

        [HttpPost, HttpGet]
        public IHttpActionResult GetUserAuthen([FromBody] UserLogin oRequestParameter)
        {
            string API_USER = ShareDataManagement.LookupCache(oRequestParameter.CompanyCode, "ESS.API", "API_USERNAME");
            string API_PASSWORD = ShareDataManagement.LookupCache(oRequestParameter.CompanyCode, "ESS.API", "API_PASSWORD");
            IHttpActionResult response = Unauthorized();
            ApiUser user = new ApiUser();
            try
            {
                user = APIManagement.CreateInstance(oRequestParameter.CompanyCode).Authorize(API_USER, API_PASSWORD);

                if (!string.IsNullOrEmpty(user.UserName))
                {
                    response = Ok(new { username = API_USER,password = API_PASSWORD, companycode = oRequestParameter.CompanyCode });

                }
                else
                {
                    response = Ok(new { status = false, errormessage = "User not found", token = string.Empty });
                }

            }
            catch (Exception ex)
            {
                response = Ok(new { status = false, errormessage = ex.Message, token = string.Empty });
               
            }
            return response;
        }
    }
}