﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.WORKFLOW;
using iSSWS.Models;
using ESS.HR.TM;
using ESS.HR.TM.INFOTYPE;
using ESS.HR.TM.CONFIG;
using ESS.HR.TM.DATACLASS;
using ESS.SHAREDATASERVICE;
using System.Web.UI.WebControls;
using ESS.TIMESHEET;
using System.Globalization;
using ESS.DATA.EXCEPTION;
using static ESS.HR.TM.HRTMManagement;
using ESS.DATA.INTERFACE;
using static ESS.HR.TM.DATASERVICE.DailyOTLogService;
using ESS.HR.PA;
using Microsoft.Reporting.WebForms;
using ESS.UTILITY.EXPORT;
using ESS.UTILITY.FILE;
using ESS.EMPLOYEE.CONFIG.OM;
using System.IO;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using ESS.HR.BE.DATACLASS;

namespace iSSWS.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class HRTMController : ApiController
    {
        private CultureInfo oCL = new CultureInfo("en-US");
        public void SetAuthenticate(EmployeeDataForMobile oCurrentEmployee)
        {
            WorkflowIdentity iden;
            if (oCurrentEmployee.IsExternalUser)
                iden = WorkflowIdentity.CreateInstance(oCurrentEmployee.CompanyCode).GetIdentityForExternalUser(oCurrentEmployee.EmployeeID, oCurrentEmployee.Name);
            else
            {
                EmployeeData oEmployeeData = Convert<EmployeeData>.ObjectFrom<EmployeeDataForMobile>(oCurrentEmployee);
                iden = WorkflowIdentity.CreateInstance(oCurrentEmployee.CompanyCode).GetIdentityWithoutPassword(oEmployeeData);
            }
            WorkflowPrinciple Principle = new WorkflowPrinciple(iden);
            WorkflowPrinciple.Current = Principle;
        }

        [HttpPost]
        public List<INFOTYPE2006> GetAbsenceQuota([FromBody] RequestParameter oRequestParameter)
        {
            //string sEmployeeID = oRequestParameter.InputParameter["EmployeeID"].ToString();
            List<INFOTYPE2006> oReturn = new List<INFOTYPE2006>();
            oReturn = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetAbsenceQuota(oRequestParameter.Requestor.EmployeeID);
            return oReturn;
        }

        [HttpPost]
        public List<string> GetYearMenuListAbsence([FromBody] RequestParameter oRequestParameter)
        {
            List<string> oListYear = new List<string>();
            int start = DateTime.Now.Month < 10 ? 0 : -1;
            for (int index = start; index <= 1; index++)
            {
                string sYear = string.Empty;
                int year = (DateTime.Now.Year - index);
                sYear = year.ToString("0000");
                oListYear.Add(sYear);
            }
            return oListYear;
        }


        [HttpPost]
        public List<string> GetYearAreaWorkschedule([FromBody] RequestParameter oRequestParameter)
        {
            List<string> oListYear = new List<string>();
            int startYear = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).AreaWorkscheduleOffsetBefore;
            int endYear = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).AreaWorkscheduleOffsetAfter;
            for (int index = DateTime.Now.Year - startYear; index <= DateTime.Now.Year + endYear; index++)
            {
                string sYear = string.Empty;
                sYear = index.ToString("0000");
                oListYear.Add(sYear);
            }

            oListYear = oListYear.OrderByDescending(s => s).ToList();

            return oListYear;
        }


        [HttpPost]
        public object GetTimeManagementList(RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);
            //foreach (TabPage oTP in this.tab1.Tabs)
            //{
            //    oTP.Text = CacheManager.GetCommonText("TIMEMANAGEMENT", this.LanguageCode, oTP.Text);
            //}

            object oResult = new object();

            if (WorkflowPrinciple.Current.UserSetting.Employee.IsInRole("#GROUPEMPLOYEE"))
            {
                oResult = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetTimeManagementList(oEmp, oRequestParameter.CurrentEmployee.CompanyCode);
            }
            return oResult;
        }

        [HttpPost]
        public List<AbsenceType> GetAbsenceTypeList(RequestParameter oRequestParameter)
        {

            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);

            List<AbsenceType> oListAbsenceType = new List<AbsenceType>();
            oListAbsenceType = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetAbsenceTypeList(oEmp, oRequestParameter.CurrentEmployee.Language);
            return oListAbsenceType;
        }

        [HttpPost]
        public object GetAbsenceTime(RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            AbsenceCreatingRule oACR = new AbsenceCreatingRule();
            bool IsRequireAbsToDate = false, IsRequireAbsTime = false;
            List<string> oListAbsTime = new List<string>();
            Dictionary<string, string> dicAbsTime = new Dictionary<string, string>();
            ListItem[] oListItem;
            DateTime AbDate = Convert.ToDateTime(oRequestParameter.InputParameter["AbsenceDate"]);
            DateTime AbDateTo = Convert.ToDateTime(oRequestParameter.InputParameter["AbsenceDateTo"]);
            string AbType = oRequestParameter.InputParameter["AbsenceType"].ToString();

            EmployeeData CurrentRequestor = new EmployeeData(oRequestParameter.Requestor.EmployeeID, AbDate);

            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);


            //oACR = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetAbsenceCreatingRule(oEmp, AbType);
            oACR = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetAbsenceCreatingRule(oEmp, AbType, 0);
            //เงื่อนไขการขอยืนยันการมาปฏิบัติงาน 
            //0 = เต็มวันและลาต่อเนื่องได้
            //1 = เต็มวันและลาต่อเนื่องไม่ได้
            //2 = ครึ่งวันและลาต่อเนื่องไม่ได้
            //3 = เต็มวันและครึ่งวันและลาต่อเนื่องได้
            //4 = เต็มวันและครึ่งวันและลาต่อเนื่องไม่ได้)

            bool isfullday = false;
            switch (oACR.AbsenceInterval)
            {
                case 0:
                    IsRequireAbsToDate = true;
                    IsRequireAbsTime = false;
                    isfullday = true;
                    break;
                case 1:
                    IsRequireAbsToDate = false;
                    IsRequireAbsTime = true;

                    oListItem = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).DivideDailyWorkSchedule(2, false, AbDate, CurrentRequestor, CacheManager.GetCommonText("HRTMABSENCE", oRequestParameter.CurrentEmployee.Language, "FULLDAY"), false);
                    dicAbsTime = oListItem.Cast<ListItem>().ToDictionary(i => i.Value, i => i.Text);

                    break;
                case 2:
                    IsRequireAbsToDate = false;
                    IsRequireAbsTime = true;

                    oListItem = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).DivideDailyWorkSchedule(2, false, AbDate, CurrentRequestor, CacheManager.GetCommonText("HRTMABSENCE", oRequestParameter.CurrentEmployee.Language, "FULLDAY"), false);
                    dicAbsTime = oListItem.Cast<ListItem>().ToDictionary(i => i.Value, i => i.Text);

                    break;
                case 3:
                    IsRequireAbsToDate = true;
                    IsRequireAbsTime = true;

                    oListItem = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).DivideDailyWorkSchedule(2, true, AbDate, CurrentRequestor, CacheManager.GetCommonText("HRTMABSENCE", oRequestParameter.CurrentEmployee.Language, "FULLDAY"), false);
                    dicAbsTime = oListItem.Cast<ListItem>().ToDictionary(i => i.Value, i => i.Text);

                    if (!AbDate.Date.Equals(AbDateTo.Date) && IsRequireAbsTime)
                    {
                        dicAbsTime.Remove(dicAbsTime.Keys.First());
                        dicAbsTime.Remove(dicAbsTime.Keys.First());
                        //this.cboAbsenceTime.Items[0].Enabled = false;
                        //this.cboAbsenceTime.Items[1].Enabled = false;
                        isfullday = true;
                    }
                    break;
                case 4:
                    IsRequireAbsToDate = false;
                    IsRequireAbsTime = true;

                    oListItem = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).DivideDailyWorkSchedule(2, true, AbDate, CurrentRequestor, CacheManager.GetCommonText("HRTMABSENCE", oRequestParameter.CurrentEmployee.Language, "FULLDAY"), false);
                    dicAbsTime = oListItem.Cast<ListItem>().ToDictionary(i => i.Value, i => i.Text);
                    //cboAbsenceTime.Items.AddRange(HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).DivideDailyWorkSchedule(2, true, AbDate, CurrentRequestor, CacheManager.GetCommonText(TextCategory, this.LanguageCode, "FULLDAY"), false));
                    break;
            }

            object oResult = new
            {
                visibleAbsenceDateTo = IsRequireAbsToDate,
                visibleAbsenceTime = IsRequireAbsTime,
                objAbsTime = dicAbsTime,
                stateIsFullday = isfullday
            };
            return oResult;
        }

        public class GetAttendanceTime_params : RequestParameter
        {
            public DateTime AttendanceDate { get; set; }
            public DateTime AttendanceDateTo { get; set; }
            public string AttendanceType { get; set; }
        }

        public class AttTimeModel
        {
            public string AttID { get; set; }
            public string AttName { get; set; }
        }
        [HttpPost]
        public object GetAttendanceTime(GetAttendanceTime_params oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            AttendanceCreatingRule oACR = new AttendanceCreatingRule();
            bool IsRequireAttToDate = false, IsRequireAttTime = false;
            List<string> oListAttTime = new List<string>();
            List<AttTimeModel> dicAttTime = new List<AttTimeModel>();
            ListItem[] oListItem;
            DateTime AttDate = oRequestParameter.AttendanceDate;
            DateTime AttDateTo = oRequestParameter.AttendanceDateTo;
            string AttType = oRequestParameter.AttendanceType;
            EmployeeData CurrentRequestor = new EmployeeData(oRequestParameter.Requestor.EmployeeID, AttDate);

            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);


            oACR = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetCreatingRule(oEmp, AttType, AttDate, AttDateTo);

            //เงื่อนไขการขอยืนยันการมาปฏิบัติงาน 
            //0 = เต็มวันและลาต่อเนื่องได้
            //1 = เต็มวันและลาต่อเนื่องไม่ได้
            //2 = ครึ่งวันและลาต่อเนื่องไม่ได้
            //3 = เต็มวันและครึ่งวันและลาต่อเนื่องได้
            //4 = เต็มวันและครึ่งวันและลาต่อเนื่องไม่ได้)
            switch (oACR.AttendanceInterval)
            {
                case 0:
                    IsRequireAttTime = false;
                    IsRequireAttToDate = true;
                    break;
                case 1:
                    IsRequireAttTime = false;
                    IsRequireAttToDate = false;
                    AttDateTo = AttDate;
                    oListItem = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).DivideDailyWorkSchedule(2, false, AttDate, CurrentRequestor, CacheManager.GetCommonText("HRTMABSENCE", oRequestParameter.CurrentEmployee.Language, "FULLDAY"), true);
                    dicAttTime = oListItem.Select(a => new AttTimeModel() { AttID = a.Value, AttName = a.Text }).ToList();
                    break;
                case 2:
                    IsRequireAttTime = true;
                    IsRequireAttToDate = false;
                    AttDateTo = AttDate;
                    oListItem = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).DivideDailyWorkSchedule(2, false, AttDate, CurrentRequestor, CacheManager.GetCommonText("HRTMABSENCE", oRequestParameter.CurrentEmployee.Language, "FULLDAY"), true);
                    dicAttTime = oListItem.Select(a => new AttTimeModel() { AttID = a.Value, AttName = a.Text }).ToList();
                    break;
                case 3:
                    IsRequireAttTime = true;
                    IsRequireAttToDate = true;
                    oListItem = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).DivideDailyWorkSchedule(2, true, AttDate, CurrentRequestor, CacheManager.GetCommonText("HRTMABSENCE", oRequestParameter.CurrentEmployee.Language, "FULLDAY"), true);
                    dicAttTime = oListItem.Select(a => new AttTimeModel() { AttID = a.Value, AttName = a.Text }).ToList();

                    if (!AttDate.Date.Equals(AttDateTo.Date) && IsRequireAttTime)
                    {
                        int iCount = dicAttTime.Count;
                        dicAttTime.Remove(dicAttTime.First());
                        dicAttTime.Remove(dicAttTime.First());

                        if (iCount > 3)
                        {
                            dicAttTime.Remove(dicAttTime.First());
                            dicAttTime.Remove(dicAttTime.First());
                        }
                    }
                    break;
                case 4:
                    IsRequireAttTime = true;
                    IsRequireAttToDate = false;
                    AttDateTo = AttDate;
                    oListItem = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).DivideDailyWorkSchedule(2, true, AttDate, CurrentRequestor, CacheManager.GetCommonText("HRTMABSENCE", oRequestParameter.CurrentEmployee.Language, "FULLDAY"), true);
                    dicAttTime = oListItem.Select(a => new AttTimeModel() { AttID = a.Value, AttName = a.Text }).ToList();
                    break;
            }

            dicAttTime.RemoveAll(s => s.AttID == "00:00 - 00:00" && s.AttName == "00:00 - 00:00");

            object oResult = new
            {
                visibleAttendanceDateTo = IsRequireAttToDate,
                visibleAttendanceTime = IsRequireAttTime,
                objAttTime = dicAttTime

            };
            return oResult;
        }


        [HttpPost]
        public MonthlyWS GetCalendar(RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            MonthlyWS oMonthlyWS = new MonthlyWS();
            Int32 iYear = 2020;
            Int32 iMonth = 4;
            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);
            oMonthlyWS = MonthlyWS.GetCalendar(oEmp.EmployeeID, iYear, iMonth);
            return oMonthlyWS;
        }

        [HttpPost]
        public GetListTimesheet_Display GetArchiveTimepair(RequestParameter oRequestParameter)
        {
            var result = new GetListTimesheet_Display();

            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);
            bool IsVisibleTimeElement = false, IsVisibleTimePair = false;
            DateTime AbDate = Convert.ToDateTime(oRequestParameter.InputParameter["AbsenceDate"]);
            DateTime AbDateTo = Convert.ToDateTime(oRequestParameter.InputParameter["AbsenceDateTo"]);
            string AbType = oRequestParameter.InputParameter["AbsenceType"].ToString();
            bool IsFullDay = Convert.ToBoolean(oRequestParameter.InputParameter["IsFullDay"]);

            List<TimeElement> oListTimeElement = new List<TimeElement>();
            List<TimePair> oListTimePair = new List<TimePair>();

            AbsenceCreatingRule oACR = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetCreatingRule(oEmp, AbType);
            //if (oACR.OffsetValue > 0)
            if (oACR.AdvanceInform > 0)
            {
                // it is future request--> don't show time event
                IsVisibleTimeElement = false;
                IsVisibleTimePair = false;
                oListTimeElement = null;
                oListTimePair = null;

                result.IsVisibleTimeElement = false;
                result.IsVisibleTimePair = false;
            }
            else
            {
                IsVisibleTimeElement = true;
                IsVisibleTimePair = true;
                result.IsVisibleTimeElement = true;
                result.IsVisibleTimePair = true;
                DateTime date1, date2;
                date1 = AbDate;
                if (IsFullDay)
                {
                    date2 = AbDateTo;
                }
                else
                {
                    date2 = date1;
                }

                List<TimeElement> oTimeElement = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).LoadTimeElement(oEmp.EmployeeID, date1.AddDays(-1), date2.AddDays(2), -1, true, true);
                List<TimeElement> oTimeElementList = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).LoadTimeElement(oEmp.EmployeeID, date1.AddDays(-1), date2.AddDays(2), -1, true, true);
                List<TimeElement> oListTmp = new List<TimeElement>();
                foreach (TimeElement obj in oTimeElementList)
                {
                    TimeElement oTmp = new TimeElement();
                    oTmp.EmployeeID = obj.EmployeeID;
                    oTmp.CardNo = obj.CardNo;
                    oTmp.DoorType = obj.DoorType;
                    oTmp.EventTime = obj.EventTime;
                    oTmp.Location = obj.Location;
                    oTmp.SetElementType(obj.ItemType);
                    oTmp.IsDelete = obj.IsDelete;
                    oTmp.IsSystemDelete = obj.IsSystemDelete;
                    oListTmp.Add(oTmp);
                }

                var listTimePair = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                    .GetListTimesheetAbsence(oEmp, AbDate, AbDateTo, oTimeElement);

                var date_now = DateTime.Now;
                if (listTimePair.TimePairs.Count > 0)
                {
                    foreach (var _timePairs in listTimePair.TimePairs)
                    {
                        var timesheet = new TimesheetModel()
                        {
                            Date = _timePairs.Date,
                            ClockIn = _timePairs.ClockIN == null ? (DateTime?)null : _timePairs.ClockIN.EventTime,
                            ClockOut = _timePairs.ClockOUT == null ? (DateTime?)null : _timePairs.ClockOUT.EventTime,
                            ClockInLate = false,
                            ClockoutEarly = false,
                            ClockNoneWork = false,
                            InvalidClockInOut = false,
                            TextDescription = _timePairs.Remark,
                            IsCorrectPolicy = _timePairs.IsCheckRemark
                        };


                        EmployeeData _EmployeeData = new EmployeeData(_timePairs.EmployeeID, _timePairs.Date);
                        if (_EmployeeData.EmpSubAreaType == EmployeeSubAreaType.Flex)
                        {
                            INFOTYPE0007 oInfotype0007 = _EmployeeData.GetEmployeeWF(_EmployeeData.EmployeeID, _timePairs.Date);
                            if (!string.IsNullOrEmpty(oInfotype0007.WFRule))
                            {
                                TimeSpan oBeginTimeDailyFlexTime;
                                TimeSpan oEndTimeDailyFlexTime;
                                DailyFlexTime oDFX = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetDailyFlexTimeMinForCalculateByFlexCode(oInfotype0007.WFRule);

                                BreakPattern oBreak = new BreakPattern();
                                oBreak = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetBreakPattern(_timePairs.DWS.DailyWorkscheduleGrouping, oDFX.BreakCode);

                                TimeSpan TBegin;
                                if (!TimeSpan.TryParse(oDFX.BeginTime, out TBegin))
                                {
                                    // handle validation error
                                }
                                oBeginTimeDailyFlexTime = new TimeSpan(TBegin.Hours, TBegin.Minutes, 0);

                                TimeSpan TEnd;
                                if (!TimeSpan.TryParse(oDFX.EndTime, out TEnd))
                                {
                                    // handle validation error
                                }
                                oEndTimeDailyFlexTime = new TimeSpan(TEnd.Hours, TEnd.Minutes, 0);
                                _timePairs.DWS.WorkBeginTime = oBeginTimeDailyFlexTime;
                                _timePairs.DWS.WorkEndTime = oEndTimeDailyFlexTime;

                                // เพิ่ม Logic หากเป็นพนักงานที่ไม่ต้องรูดบัตร
                                if (oInfotype0007.TimeEvaluateClass == "0")
                                {
                                    timesheet.IsNoScanCard = true;
                                }
                            }
                        }
                        else
                        {
                            // เพิ่ม Logic หากเป็นพนักงานที่ไม่ต้องรูดบัตร
                            INFOTYPE0007 oInfotype0007 = _EmployeeData.GetEmployeeWF(_EmployeeData.EmployeeID, _timePairs.Date);
                            if (!string.IsNullOrEmpty(oInfotype0007.WFRule))
                            {
                                if (oInfotype0007.TimeEvaluateClass == "0")
                                {
                                    timesheet.IsNoScanCard = true;
                                }
                            }
                        }

                        bool IsWorking = false;
                        if (_timePairs.DWSCode.StartsWith("OFF") || _timePairs.DWSCode.StartsWith("HOL"))
                            timesheet.Type = _timePairs.DWSCode;
                        else
                        {
                            timesheet.Type = string.Format("{0} ({1}-{2})", _timePairs.DWSCode, TimeSpanToString(_timePairs.DWS.WorkBeginTime, oCL), TimeSpanToString(_timePairs.DWS.WorkEndTime, oCL));
                            IsWorking = true;
                        }


                        // เป็นวันทำงานจริง
                        if (IsWorking)
                        {
                            // พนักงานที่ไม่ต้องรูดเวลาเข้างาน 
                            if (timesheet.IsNoScanCard)
                            {
                                // หาก Text ไม่ใช่ค่าว่างแปลว่ามีการลา , รับรองเวลา ไม่ต้องเอาคำว่าไม่ต้องรูดบัตรมาใส่
                                if (string.IsNullOrEmpty(timesheet.TextDescription))
                                {
                                    if (date_now.Date >= timesheet.Date.Date)
                                    {
                                        timesheet.TextDescription = HRTMManagement
                                       .CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                                       .GetCommonText("TIMESHEETMANAGETIME", WorkflowPrinciple.Current.UserSetting.Language, "LELVE_11_STAFF_UP");
                                    }
                                }
                            }
                        }

                        result.TimeSheets.Add(timesheet);
                    }

                }

                result.TimeElements = oTimeElementList;

                #region ของเก่าครับ
                //List<TM_Timesheet> TM_TimesheetList = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetTM_Timesheet(oEmp.EmployeeID, date1, date2, oListTmp);
                ////List<TM_Timesheet> TM_TimesheetList = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetTM_Timesheet(oEmp.EmployeeID, date1, date2, oTimeElementList);
                //INFOTYPE2001 oINFOTYPE2001 = new INFOTYPE2001();
                //INFOTYPE2002 oINFOTYPE2002 = new INFOTYPE2002();
                //List<INFOTYPE2001> oAbsenceList = oINFOTYPE2001.GetData(oEmp, AbDate.AddDays(-1), AbDateTo.AddDays(2));
                //List<INFOTYPE2002> oAttendanceList = oINFOTYPE2002.GetData(oEmp, AbDate.AddDays(-1), AbDateTo.AddDays(2));

                //foreach (var _timesheet in TM_TimesheetList)
                //{
                //    var absence = oAbsenceList.FirstOrDefault(a => a.BeginDate.Date == _timesheet.Date);
                //    if (absence == null)
                //    {
                //        var attendance = oAttendanceList.FirstOrDefault(a => a.BeginDate.Date == _timesheet.Date);
                //        if (attendance != null)
                //        {
                //            _timesheet.TextDescription = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetAttendanceTypeText(attendance.AttendanceType);
                //            if (!attendance.AllDayFlag)
                //            {
                //                _timesheet.TextDescription += " " + attendance.BeginTime.ToString().Substring(0, 5) + "-" + attendance.EndTime.ToString().Substring(0, 5);
                //            }
                //        }
                //    }
                //    else
                //    {
                //        _timesheet.TextDescription = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetAbsenceTypeText(absence.AbsenceType);
                //        if (!absence.AllDayFlag)
                //        {
                //            _timesheet.TextDescription += " " + absence.BeginTime.ToString().Substring(0, 5) + "-" + absence.EndTime.ToString().Substring(0, 5);
                //        }
                //    }
                //    var timesheet = new TimesheetModel()
                //    {
                //        Date = _timesheet.Date,
                //        ClockIn = _timesheet.ClockIn == DateTime.MinValue ? (DateTime?)null : _timesheet.ClockIn,
                //        ClockOut = _timesheet.ClockOut == DateTime.MinValue ? (DateTime?)null : _timesheet.ClockOut,
                //        Type = _timesheet.Type,
                //        ClockInLate = _timesheet.ClockInLate,
                //        ClockoutEarly = _timesheet.ClockoutEarly,
                //        ClockNoneWork = _timesheet.ClockNoneWork,
                //        InvalidClockInOut = _timesheet.InvalidClockInOut,
                //        TextDescription = _timesheet.TextDescription
                //    };
                //    result.TimeSheets.Add(timesheet);
                #endregion ของเก่า
            }

            return result;
        }

        public class GetArchiveTimepairAttendance_param : RequestParameter
        {
            public DateTime AttendanceDate { get; set; }
            public DateTime AttendanceDateTo { get; set; }
            public string AttendanceType { get; set; }
        }
        [HttpPost]
        public GetListTimesheet_Display GetArchiveTimepairAttendance(GetArchiveTimepairAttendance_param oRequestParameter)
        {
            GetListTimesheet_Display result = new GetListTimesheet_Display();

            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);
            bool IsVisibleTimeElement = false, IsVisibleTimePair = false;
            DateTime AttDate = oRequestParameter.AttendanceDate;
            DateTime AttDateTo = oRequestParameter.AttendanceDateTo;
            string AttType = oRequestParameter.AttendanceType;

            List<TimeElement> oListTimeElement = new List<TimeElement>();
            List<TimePair> oListTimePair = new List<TimePair>();

            AttendanceCreatingRule oACR = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetCreatingRule(oEmp, AttType, AttDate, AttDateTo);

            DateTime BeginDate = AttDate;
            DateTime EndDate = AttDateTo.AddDays(1).AddSeconds(-1);

            List<TimeElement> oTimeElement = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).LoadTimeElement(oEmp.EmployeeID, BeginDate.AddDays(-1), EndDate.AddDays(2), -1, true, true);
            List<TimeElement> oTimeElementList = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).LoadTimeElement(oEmp.EmployeeID, BeginDate.AddDays(-1), EndDate.AddDays(2), -1, true, true);
            List<TimeElement> oListTmp = new List<TimeElement>();
            foreach (TimeElement obj in oTimeElementList)
            {
                TimeElement oTmp = new TimeElement();
                oTmp.EmployeeID = obj.EmployeeID;
                oTmp.CardNo = obj.CardNo;
                oTmp.DoorType = obj.DoorType;
                oTmp.EventTime = obj.EventTime;
                oTmp.Location = obj.Location;
                oTmp.SetElementType(obj.ItemType);
                oTmp.IsDelete = obj.IsDelete;
                oTmp.IsSystemDelete = obj.IsSystemDelete;
                oListTmp.Add(oTmp);
            }

            var listTimePair = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                    .GetListTimesheetAttendance(oEmp, BeginDate, EndDate, oTimeElement);

            var date_now = DateTime.Now;
            if (listTimePair.TimePairs.Count > 0)
            {
                foreach (var _timePairs in listTimePair.TimePairs)
                {
                    var timesheet = new TimesheetModel()
                    {
                        Date = _timePairs.Date,
                        ClockIn = _timePairs.ClockIN == null ? (DateTime?)null : _timePairs.ClockIN.EventTime,
                        ClockOut = _timePairs.ClockOUT == null ? (DateTime?)null : _timePairs.ClockOUT.EventTime,
                        ClockInLate = false,
                        ClockoutEarly = false,
                        ClockNoneWork = false,
                        InvalidClockInOut = false,
                        TextDescription = _timePairs.Remark,
                        IsCorrectPolicy = _timePairs.IsCheckRemark
                    };



                    EmployeeData _EmployeeData = new EmployeeData(_timePairs.EmployeeID, _timePairs.Date);
                    if (_EmployeeData.EmpSubAreaType == EmployeeSubAreaType.Flex)
                    {
                        INFOTYPE0007 oInfotype0007 = _EmployeeData.GetEmployeeWF(_EmployeeData.EmployeeID, _timePairs.Date);
                        if (!string.IsNullOrEmpty(oInfotype0007.WFRule))
                        {
                            TimeSpan oBeginTimeDailyFlexTime;
                            TimeSpan oEndTimeDailyFlexTime;
                            DailyFlexTime oDFX = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetDailyFlexTimeMinForCalculateByFlexCode(oInfotype0007.WFRule);

                            BreakPattern oBreak = new BreakPattern();
                            oBreak = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetBreakPattern(_timePairs.DWS.DailyWorkscheduleGrouping, oDFX.BreakCode);

                            TimeSpan TBegin;
                            if (!TimeSpan.TryParse(oDFX.BeginTime, out TBegin))
                            {
                                // handle validation error
                            }
                            oBeginTimeDailyFlexTime = new TimeSpan(TBegin.Hours, TBegin.Minutes, 0);

                            TimeSpan TEnd;
                            if (!TimeSpan.TryParse(oDFX.EndTime, out TEnd))
                            {
                                // handle validation error
                            }
                            oEndTimeDailyFlexTime = new TimeSpan(TEnd.Hours, TEnd.Minutes, 0);
                            _timePairs.DWS.WorkBeginTime = oBeginTimeDailyFlexTime;
                            _timePairs.DWS.WorkEndTime = oEndTimeDailyFlexTime;

                            // เพิ่ม Logic หากเป็นพนักงานที่ไม่ต้องรูดบัตร
                            if (oInfotype0007.TimeEvaluateClass == "0")
                            {
                                timesheet.IsNoScanCard = true;
                            }
                        }
                    }
                    else
                    {
                        // เพิ่ม Logic หากเป็นพนักงานที่ไม่ต้องรูดบัตร
                        INFOTYPE0007 oInfotype0007 = _EmployeeData.GetEmployeeWF(_EmployeeData.EmployeeID, _timePairs.Date);
                        if (!string.IsNullOrEmpty(oInfotype0007.WFRule))
                        {
                            if (oInfotype0007.TimeEvaluateClass == "0")
                            {
                                timesheet.IsNoScanCard = true;
                            }
                        }
                    }


                    bool IsWorking = false;
                    if (_timePairs.DWSCode.StartsWith("OFF") || _timePairs.DWSCode.StartsWith("HOL"))
                        timesheet.Type = _timePairs.DWSCode;
                    else
                    {
                        timesheet.Type = string.Format("{0} ({1}-{2})", _timePairs.DWSCode, TimeSpanToString(_timePairs.DWS.WorkBeginTime, oCL), TimeSpanToString(_timePairs.DWS.WorkEndTime, oCL));
                        IsWorking = true;
                    }

                    // เป็นวันทำงานจริง
                    if (IsWorking)
                    {
                        // พนักงานที่ไม่ต้องรูดเวลาเข้างาน 
                        if (timesheet.IsNoScanCard)
                        {
                            // หาก Text ไม่ใช่ค่าว่างแปลว่ามีการลา , รับรองเวลา ไม่ต้องเอาคำว่าไม่ต้องรูดบัตรมาใส่
                            if (string.IsNullOrEmpty(timesheet.TextDescription))
                            {
                                if (date_now.Date >= timesheet.Date.Date)
                                {
                                    timesheet.TextDescription = HRTMManagement
                                   .CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                                   .GetCommonText("TIMESHEETMANAGETIME", WorkflowPrinciple.Current.UserSetting.Language, "LELVE_11_STAFF_UP");
                                }
                            }
                        }
                    }

                    result.TimeSheets.Add(timesheet);
                }
            }

            result.TimeElements = oTimeElementList;
            result.IsVisibleTimeElement = IsVisibleTimeElement;
            result.IsVisibleTimePair = IsVisibleTimePair;

            #region ของเก่า
            // List<TM_Timesheet> TM_TimesheetList = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetTM_Timesheet(oEmp.EmployeeID, BeginDate, EndDate, oListTmp);
            // List<TM_Timesheet> TM_TimesheetList = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetTM_Timesheet(oEmp.EmployeeID, BeginDate, EndDate, oTimeElementList);

            //INFOTYPE2001 oINFOTYPE2001 = new INFOTYPE2001();
            //INFOTYPE2002 oINFOTYPE2002 = new INFOTYPE2002();
            //List<INFOTYPE2001> oAbsenceList = oINFOTYPE2001.GetData(oEmp, BeginDate.AddDays(-1), EndDate.AddDays(2));
            //List<INFOTYPE2002> oAttendanceList = oINFOTYPE2002.GetData(oEmp, BeginDate.AddDays(-1), EndDate.AddDays(2));


            //foreach (var _timesheet in TM_TimesheetList)
            //{
            //    var absence = oAbsenceList.FirstOrDefault(a => a.BeginDate.Date == _timesheet.Date);
            //    if (absence == null)
            //    {
            //        var attendance = oAttendanceList.FirstOrDefault(a => a.BeginDate.Date == _timesheet.Date);
            //        if (attendance != null)
            //        {
            //            _timesheet.TextDescription = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetAttendanceTypeText(attendance.AttendanceType);
            //            if (!attendance.AllDayFlag)
            //            {
            //                _timesheet.TextDescription += " " + attendance.BeginTime.ToString().Substring(0, 5) + "-" + attendance.EndTime.ToString().Substring(0, 5);
            //            }
            //        }
            //    }
            //    else
            //    {
            //        _timesheet.TextDescription = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetAbsenceTypeText(absence.AbsenceType);
            //        if (!absence.AllDayFlag)
            //        {
            //            _timesheet.TextDescription += " " + absence.BeginTime.ToString().Substring(0, 5) + "-" + absence.EndTime.ToString().Substring(0, 5);
            //        }
            //    }
            //    var timesheet = new TimesheetModel()
            //    {
            //        Date = _timesheet.Date,
            //        ClockIn = _timesheet.ClockIn == DateTime.MinValue ? (DateTime?)null : _timesheet.ClockIn,
            //        ClockOut = _timesheet.ClockOut == DateTime.MinValue ? (DateTime?)null : _timesheet.ClockOut,
            //        Type = _timesheet.Type,
            //        ClockInLate = _timesheet.ClockInLate,
            //        ClockoutEarly = _timesheet.ClockoutEarly,
            //        ClockNoneWork = _timesheet.ClockNoneWork,
            //        InvalidClockInOut = _timesheet.InvalidClockInOut,
            //        TextDescription = _timesheet.TextDescription
            //    };
            //    result.TimeSheets.Add(timesheet);
            //}
            #endregion

            return result;
        }

        public class GetPeriodOTList_Params : RequestParameter
        {
            public string Language { get; set; }
        }
        [HttpPost]
        public DataTable GetPeriodOTList(GetPeriodOTCreate_Params oRequestParameter)
        {
            return HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetPeriodOTList(oRequestParameter.Language);

        }

        public class GetDailyOTList_Params : RequestParameter
        {
            public string SelectedPeriod { get; set; }
        }

        [HttpPost]
        public List<DailyOTLog> GetDailyOTList(GetDailyOTList_Params oRequestParameter)
        {
            var emp = new EmployeeData() { EmployeeID = oRequestParameter.Requestor.EmployeeID };
            var results = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetDailyOTList(emp, oRequestParameter.SelectedPeriod);
            foreach (var item in results)
            {
                item.Concat = item.RequestNo + "|" + item.EmployeeID + "|" +
                            item.BeginDate.ToString("yyyyMMdd HHmmss") + "|" + item.EndDate.ToString("yyyyMMdd HHmmss");
            }
            return results;
        }


        [HttpPost]
        public Int32 GetEmpSubGroup(RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);
            Int32 oEmpSubGroup = 0;
            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);
            oEmpSubGroup = Convert.ToInt32(oEmp.EmpSubGroup);
            return oEmpSubGroup;
        }

        [HttpPost]
        public bool GetIsShowCreateSummaryOT([FromBody] RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            bool isShowCreateSummaryOT = false;
            isShowCreateSummaryOT = HRPAManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).ShowTimeAwareLink(0);
            if (isShowCreateSummaryOT || WorkflowPrinciple.Current.UserSetting.Employee.IsInRole("TIMEADMIN"))
            {
                isShowCreateSummaryOT = true;
            }
            return isShowCreateSummaryOT;
        }

        [HttpPost]
        public bool GetIsCheckUserRole([FromBody] RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            bool isCheckUserRole = false;
            if (WorkflowPrinciple.Current.UserSetting.Employee.IsInRole("SHIFTADMIN") || WorkflowPrinciple.Current.UserSetting.Employee.IsInRole("SHIFTMENU"))
            {
                isCheckUserRole = true;
            }
            return isCheckUserRole;
        }

        public class GetPeriodOTCreate_Params : RequestParameter
        {
            public string Language { get; set; }
        }
        [HttpPost]
        public DataTable GetPeriodOTCreate(GetPeriodOTCreate_Params oRequestParameter)
        {
            return HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetPeriodOTCreate(oRequestParameter.Language);
        }

        public class CalculateOT_Params : RequestParameter
        {
            public CalculateOT_Params()
            {
                OTs = new List<OT_Params>();
            }
            public string CostCenter { get; set; }
            public bool ChkSubstituteOT { get; set; }
            public string SelectedPeriod { get; set; }
            public List<OT_Params> OTs { get; set; }
        }

        public class OT_Params
        {
            public DateTime OTDATE { get; set; }
            public string BegindTimeH { get; set; }
            public string BegindTimeM { get; set; }
            public string EndTimeH { get; set; }
            public string EndTimeM { get; set; }
            public string Description { get; set; }
        }
        [HttpPost]
        public List<DailyOTLog> CalculateOT(CalculateOT_Params oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);
            DateTime period = DateTime.ParseExact(oRequestParameter.SelectedPeriod, "yyyyMM", oCL);
            var results = new List<DailyOTLog>();
            decimal totalOT = 0;

            //MonthlyWS MonthlyCalendar = GetCalendar(EmployeeID, OTYear, OTMonth);

            //validate Date
            //EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);
            var validateDailys = new List<DailyOTLogModel>();
            int index = 1;
            foreach (var ot in oRequestParameter.OTs)
            {
                if (ot.OTDATE == DateTime.MinValue)
                {
                    string Err = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).ShowError("HRTM_EXCEPTION", "NOT_FOUND_ORIGINAL_RECORD", WorkflowPrinciple.Current.UserSetting.Language);
                    throw new Exception(Err);
                }
                validateDailys.Add(new DailyOTLogModel()
                {
                    Id = index,
                    Date = ot.OTDATE,
                    BeginTime = TimeSpan.Parse(String.Format("{0}:{1}", ot.BegindTimeH, ot.BegindTimeM)),
                    EndTime = TimeSpan.Parse(String.Format("{0}:{1}", ot.EndTimeH, ot.EndTimeM))
                });
                index++;
            }



            foreach (var ot in oRequestParameter.OTs)
            {
                if (String.IsNullOrEmpty(ot.BegindTimeH) || String.IsNullOrEmpty(ot.BegindTimeM)
              || String.IsNullOrEmpty(ot.EndTimeH) || String.IsNullOrEmpty(ot.EndTimeM))
                {
                    string Err = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).ShowError("HRTM_EXCEPTION", "INVALID_DATETIME", WorkflowPrinciple.Current.UserSetting.Language);
                    throw new Exception(Err);
                }

                var beginTime = TimeSpan.Parse(String.Format("{0}:{1}", ot.BegindTimeH, ot.BegindTimeM));
                var endTime = TimeSpan.Parse(String.Format("{0}:{1}", ot.EndTimeH, ot.EndTimeM));
                var employeeId = oRequestParameter.Requestor.EmployeeID;

                if (ot.OTDATE == DateTime.MinValue || beginTime == TimeSpan.MinValue || endTime == TimeSpan.MinValue)
                {
                    return null;
                }

                var result = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).CalculateOT(employeeId, ot.OTDATE.Add(beginTime), ot.OTDATE.Add(endTime), ot.Description, oRequestParameter.CostCenter, false, oRequestParameter.ChkSubstituteOT, period.Year, period.Month);

                decimal monthlyOTHours = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetSummaryOTInMonth(oRequestParameter.CurrentEmployee.EmployeeID, oRequestParameter.SelectedPeriod, result.RequestNo);
                totalOT += result.RequestOTHour10 + result.RequestOTHour15 + result.RequestOTHour30;
                result.TOTALAMOUNT = Convert.ToString(Math.Round((totalOT + monthlyOTHours) / 60, 2, MidpointRounding.ToEven));

                results.Add(result);
            }
            return results;
        }


        public class CalClockInClockOutSummaryOT_Params : RequestParameter
        {
            public CalClockInClockOutSummaryOT_Params()
            {
                OTs = new List<ClockInClockOut_Params>();
            }
            public string EmployeeID { get; set; }
            public string SelectedPeriod { get; set; }
            public List<ClockInClockOut_Params> OTs { get; set; }

        }
        public class ClockInClockOut_Params
        {
            public DateTime BeginDate { get; set; }
            public DateTime EndDate { get; set; }
            public DateTime? ClockIn { get; set; }
            public DateTime? ClockOut { get; set; }
        }
        [HttpPost]
        public List<DailyOTLog> CalClockInClockOutSummaryOT(CalClockInClockOutSummaryOT_Params oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);


            DateTime period = DateTime.ParseExact(oRequestParameter.SelectedPeriod, "yyyyMM", oCL);
            var results = new List<DailyOTLog>();

            foreach (var ot in oRequestParameter.OTs)
            {
                if (ot.ClockIn != null && ot.ClockOut != null)
                {
                    if (ot.BeginDate < ot.ClockOut.Value && ot.ClockIn.Value < ot.EndDate)
                    {
                        DateTime oIntersectIn = ot.BeginDate >= ot.ClockIn.Value ? ot.BeginDate : ot.ClockIn.Value;
                        DateTime oIntersectOut = ot.EndDate <= ot.ClockOut.Value ? ot.EndDate : ot.ClockOut.Value;

                        DateTime InUp = RoundUp(oIntersectIn, TimeSpan.FromMinutes(30));
                        //DateTime InDown = RoundDown(ot.ClockIn.Value, TimeSpan.FromMinutes(30));

                        //DateTime OutUp = RoundUp(ot.ClockOut.Value, TimeSpan.FromMinutes(30));
                        DateTime OutDown = RoundDown(oIntersectOut, TimeSpan.FromMinutes(30));

                        TimeSpan oldIn = new TimeSpan(ot.ClockIn.Value.Hour, ot.ClockIn.Value.Minute, ot.ClockIn.Value.Second);
                        TimeSpan oldOut = new TimeSpan(ot.ClockOut.Value.Hour, ot.ClockOut.Value.Minute, ot.ClockOut.Value.Second);

                        if (OutDown > InUp)
                        {
                            var result = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).CalculateOT(oRequestParameter.EmployeeID, InUp, OutDown, string.Empty, string.Empty, false, false, period.Year, period.Month);
                            if (result != null)
                            {
                                results.Add(result);
                            }
                        }
                        else
                        {
                            results.Add(new DailyOTLog());
                        }
                    }
                    else
                    {
                        results.Add(new DailyOTLog());
                    }
                }
                else
                {
                    results.Add(new DailyOTLog());
                }
            }
            return results;
        }
        public DateTime RoundUp(DateTime dt, TimeSpan d)
        {
            return new DateTime((dt.Ticks + d.Ticks - 1) / d.Ticks * d.Ticks, dt.Kind);
        }
        public DateTime RoundDown(DateTime dt, TimeSpan d)
        {
            return new DateTime((dt.Ticks / d.Ticks) * d.Ticks);
        }

        public class GetManagerOTCreate_Params : RequestParameter
        {
            public string Period { get; set; }
            public string Language { get; set; }
        }
        [HttpPost]
        public DataTable GetManagerOTCreate(GetManagerOTCreate_Params oRequestParameter)
        {
            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);
            return HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetManagerOTCreate(oEmp, oRequestParameter.Period, oRequestParameter.Language);
        }

        public class GetOTWorkType_Params : RequestParameter
        {
            public string Language { get; set; }
        }
        [HttpPost]
        public List<OTWorkType> GetOTWorkType(GetOTWorkType_Params oRequestParameter)
        {
            return HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetOTWorkType(oRequestParameter.Language);
        }


        [HttpPost]
        public List<string> GetListYearAttendance(RequestParameter oRequestParameter)
        {
            var result = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetYearAttendance();
            return result;
        }

        public class GetPeriodTimesheet_Params : RequestParameter
        {
            public string Language { get; set; }
        }
        [HttpPost]
        public DataTable GetPeriodTimesheet(GetPeriodTimesheet_Params oRequestParameter)
        {
            var result = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetPeriodTimesheet(oRequestParameter.Language);
            return result;
        }

        [HttpPost]
        public DataTable GetPeriodTimesheetMatching(GetPeriodTimesheet_Params oRequestParameter)
        {
            var result = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetPeriodTimesheetMatching(oRequestParameter.Language);
            return result;
        }

        public class GetClockinLateClockoutEarlyMatching_Params : RequestParameter
        {
            public string PeriodTimesheetMatchingValue { get; set; }
        }
        [HttpPost]
        public object GetClockinLateClockoutEarlyMatching(GetClockinLateClockoutEarlyMatching_Params oRequestParameter)
        {
            var result = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetClockinLateClockoutEarlyMatching(oRequestParameter.CurrentEmployee.EmployeeID, oRequestParameter.PeriodTimesheetMatchingValue);
            return result;
        }


        public class ValidateListAttendance_Params : RequestParameter
        {
            public List<INFOTYPE2002> ListAttendance { get; set; }
            public INFOTYPE2002 NewAttendance { get; set; }
            public int RowId { get; set; }
            public string DocumentState { get; set; }
            public string Language { get; set; }
            public string Type { get; set; }
        }

        public class ValidateListAttendance_Model
        {
            public string Msg { get; set; }
            public bool Valid { get; set; }
        }

        [HttpPost]
        public ValidateListAttendance_Model ValidateListAttendance(ValidateListAttendance_Params oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            var msg = "";
            oRequestParameter.NewAttendance.AttendanceType = oRequestParameter.Type;
            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);
            var result = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).ValidateListAttendance(oRequestParameter.ListAttendance, oRequestParameter.NewAttendance, oEmp, oRequestParameter.RowId, oRequestParameter.DocumentState, oRequestParameter.Language, out msg);

            if (!result)
            {
                throw new Exception(msg);
            }
            return new ValidateListAttendance_Model() { Valid = result, Msg = msg };
        }

        [HttpPost]
        public INFOTYPE2002 GetINFOTYPE2002(RequestParameter oRequestParameter)
        {
            INFOTYPE2002 oItem = new INFOTYPE2002();
            oItem.EmployeeID = oRequestParameter.Requestor.EmployeeID;
            oItem.BeginDate = DateTime.Now.Date;
            oItem.EndDate = DateTime.Now.Date;

            return oItem;
        }


        public class GetListTimesheet_Params : RequestParameter
        {
            public string PeriodValue { get; set; }
        }

        public class GetListTimesheet_Display
        {
            public GetListTimesheet_Display()
            {
                TimeSheets = new List<TimesheetModel>();
                TimeElements = new List<TimeElement>();
            }
            public List<TimesheetModel> TimeSheets { get; set; }
            public List<TimeElement> TimeElements { get; set; }
            public bool IsVisibleTimeElement { get; set; }
            public bool IsVisibleTimePair { get; set; }
        }
        public class TimesheetModel
        {
            public DateTime Date { get; set; }
            public DateTime? ClockIn { get; set; }
            public DateTime? ClockOut { get; set; }
            public string Type { get; set; }
            public bool ClockInLate { get; set; }
            public bool ClockoutEarly { get; set; }
            public bool ClockAbnormal { get; set; }
            public bool ClockNoneWork { get; set; }
            public bool InvalidClockInOut { get; set; }
            public string TextDescription { get; set; }
            public bool IsCorrectPolicy { get; set; }
            public bool IsNoScanCard { get; set; }
            public bool IsFlexChange { get; set; }
            public string BeginTimeFlexChange { get; set; }
            public string EndTimeFlexChange { get; set; }
            public bool OverDailyFlexTime { get; set; }
        }

        public string TimeSpanToString(TimeSpan input, CultureInfo oCL)
        {
            if (input != TimeSpan.MinValue)
            {
                DateTime oDate = DateTime.Now.Date.Add(input);
                return oDate.ToString("HH:mm", oCL);
            }
            else
            {
                return "-";
            }
        }

        [HttpPost]
        public GetListTimesheet_Display GetListTimesheet(GetListTimesheet_Params oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            List<INFOTYPE2001> oAbs = new List<INFOTYPE2001>();
            List<INFOTYPE2002> oAtt = new List<INFOTYPE2002>();

            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);
            //var obj = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetListTimesheet(oRequestParameter.PeriodValue, oEmp);
            var obj = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetListTimesheet(oRequestParameter.PeriodValue, oEmp, out oAbs, out oAtt);


            var result = new GetListTimesheet_Display();
            var date_now = DateTime.Now;

            DateTime FlexLate = OTManagement.FlexLate;  //FLEXLATE
            bool isShift = false;
            bool isNorm = false;
            bool isFlex = false;
            string cDetail = "";
            List<string> buffer;
            Dictionary<DateTime, List<string>> detailDict = new Dictionary<DateTime, List<string>>();
            Dictionary<DateTime, INFOTYPE2001> absenceDict = new Dictionary<DateTime, INFOTYPE2001>();
            Dictionary<DateTime, List<INFOTYPE2002>> attendanceDict = new Dictionary<DateTime, List<INFOTYPE2002>>();

            if (oAbs != null)
            {
                foreach (INFOTYPE2001 absenceItem in oAbs)
                {
                    for (DateTime runDate = absenceItem.BeginDate; runDate <= absenceItem.EndDate; runDate = runDate.AddDays(1))
                    {
                        absenceDict[runDate] = absenceItem;
                        if (detailDict.ContainsKey(runDate))
                        {
                            buffer = detailDict[runDate];
                        }
                        else
                        {
                            buffer = new List<string>();
                            detailDict.Add(runDate, buffer);
                        }
                        buffer.Add(cDetail);
                    }
                }
            }
            if (oAtt != null)
            {
                foreach (INFOTYPE2002 attendanceItem in oAtt)
                {
                    for (DateTime runDate = attendanceItem.BeginDate; runDate <= attendanceItem.EndDate; runDate = runDate.AddDays(1))
                    {
                        if (!attendanceDict.ContainsKey(runDate))
                        {
                            attendanceDict[runDate] = new List<INFOTYPE2002>();
                        }
                        attendanceDict[runDate].Add(attendanceItem);

                        if (detailDict.ContainsKey(runDate))
                        {
                            buffer = detailDict[runDate];
                        }
                        else
                        {
                            buffer = new List<string>();
                            detailDict.Add(runDate, buffer);
                        }
                        buffer.Add(cDetail);
                    }

                }
            }



            foreach (var _timePairs in obj.TimePairs)
            {
                TimePair timePair = _timePairs;
                if (_timePairs.DWS == null)
                {
                    EmployeeData empData = new EmployeeData(_timePairs.EmployeeID, _timePairs.Date);
                    DailyWS dws = empData.GetDailyWorkSchedule(_timePairs.Date);
                    _timePairs.DWS = dws;
                }
                else
                {
                    if (!String.IsNullOrEmpty(_timePairs.DWSCode))
                    {
                        if (_timePairs.DWSCode.StartsWith("S"))
                        {
                            isShift = true;
                            isNorm = isFlex = false;
                        }
                        else if (_timePairs.DWSCode.StartsWith("N"))
                        {
                            isNorm = true;
                            isShift = isFlex = false;
                        }
                        else if (_timePairs.DWSCode.StartsWith("F"))
                        {
                            isFlex = true;
                            isShift = isNorm = false;
                        }
                        else
                        {
                            isShift = isNorm = isFlex = false;
                        }
                    }
                    else
                    {
                        isShift = isNorm = isFlex = false;
                    }
                }

                #region check islate 
                // check islate                
                bool isLate = false;
                bool isClockoutEarly = false;
                bool isClockNoneWork = false;

                if ((timePair.ClockIN != null || timePair.ClockOUT != null) && !timePair.IsDayOff && !timePair.IsHoliday && timePair.Date <= DateTime.Now)
                {
                    List<INFOTYPE2002> attendance = null;
                    if (attendanceDict.ContainsKey(timePair.Date))
                    {
                        attendance = attendanceDict[timePair.Date];
                    }

                    if (attendance != null && attendance.Count > 0)
                    {
                        int i = 0;
                        for (i = 0; i < attendance.Count; i++)
                        {
                            //normalize clockin, clockout
                            if (timePair.ClockIN == null)
                            {
                                timePair.ClockIN = new TimeElement();
                                timePair.ClockIN.EventTime = attendance[i].BeginDate + attendance[i].BeginTime;
                            }
                            if (timePair.ClockOUT == null)
                            {
                                timePair.ClockOUT = new TimeElement();
                                timePair.ClockOUT.EventTime = attendance[i].EndDate + attendance[i].EndTime;
                            }
                        }
                    }
                    else
                    {
                        //normalize clockin, clockout
                        //if (timePair.ClockIN == null)
                        //    timePair.ClockIN = timePair.ClockOUT;
                        //if (timePair.ClockOUT == null)
                        //    timePair.ClockOUT = timePair.ClockIN;
                    }

                    //normalize work schedule time.
                    INFOTYPE0007 infotype7 = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetWorkSchedule(oEmp.EmployeeID, timePair.Date);
                    if (absenceDict.ContainsKey(timePair.Date))    // && timePair.DWS.BreakCode == "NOON")
                    {
                        INFOTYPE2001 absence = absenceDict[timePair.Date];

                        ListItem[] WorkTime = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).DivideDailyWorkSchedule(oEmp.EmployeeID, 2, false, timePair.Date);

                        DateTime MorningTimeBegin = Convert.ToDateTime(WorkTime[0].Text);
                        DateTime MorningTimeEnd = Convert.ToDateTime(WorkTime[1].Text);
                        DateTime NoonTimeBegin = Convert.ToDateTime(WorkTime[2].Text);
                        DateTime NoonTimeEnd = Convert.ToDateTime(WorkTime[3].Text);

                        if (!absence.AllDayFlag)
                        {
                            if (absence.BeginTime == MorningTimeBegin.TimeOfDay)
                            {
                                timePair.DWS.WorkBeginTime = NoonTimeBegin.TimeOfDay;
                                FlexLate = NoonTimeBegin;
                            }
                        }
                        else
                        {
                            timePair.DWS.WorkBeginTime = timePair.DWS.WorkEndTime;
                        }
                    }

                    DateTime workBeginTime = timePair.Date;
                    DateTime workEndTime = timePair.Date;
                    if (!timePair.DWS.IsDayOffOrHoliday)
                    {
                        workBeginTime = workBeginTime.Add(timePair.DWS.WorkBeginTime);
                        workEndTime = workEndTime.Add(timePair.DWS.WorkEndTime);
                    }
                    if (timePair.DWS.WorkBeginTime > timePair.DWS.WorkEndTime)
                    {
                        workEndTime = workEndTime.AddDays(1);
                    }

                    //Employee Type just like Norm,Shift,Flex
                    string Emp_Type = timePair.DWSCode;

                    if (isNorm) //Norm
                    {
                        if (timePair.ClockIN != null)
                        {
                            //check late
                            if (workBeginTime.TimeOfDay < timePair.ClockIN.EventTime.TimeOfDay)// && timePair.ClockIN.EventTime <= workEndTime)
                            {
                                isLate = true;
                                //is attendance fix late
                                if (attendanceDict.ContainsKey(timePair.Date))
                                {
                                    DateTime beginLate = workBeginTime;
                                    // เนื่องจากการรับรองเวลาไม่รองรับหน่วยวินาที จึงต้องลบเวลาที่เป็นหน่วยวินาทีออก
                                    DateTime endLate = timePair.ClockIN.EventTime.AddSeconds(-timePair.ClockIN.EventTime.Second);

                                    foreach (INFOTYPE2002 item1 in attendanceDict[timePair.Date])
                                    {
                                        //normalize attendance time
                                        DateTime beginAttend = timePair.Date.Add(item1.BeginTime);
                                        DateTime endAttend = timePair.Date.Add(item1.EndTime);
                                        if (endAttend <= beginAttend)
                                            endAttend = endAttend.AddDays(1);

                                        if ((beginAttend <= beginLate) && (endLate <= endAttend))
                                        {
                                            //Time attendance cover work schedule, so it fix the late.
                                            isLate = false;
                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                //is attendance fix late
                                if (attendanceDict.ContainsKey(timePair.Date))
                                {
                                    DateTime beginLate = timePair.ClockIN.EventTime;

                                    foreach (INFOTYPE2002 item1 in attendanceDict[timePair.Date])
                                    {
                                        //normalize attendance time
                                        DateTime beginAttend = timePair.Date.Add(item1.BeginTime);
                                        if (timePair.DWS.WorkBeginTime > timePair.DWS.WorkEndTime)
                                        {
                                            beginAttend = beginAttend.AddDays(1);
                                        }
                                        if (!item1.AllDayFlag && beginLate > beginAttend)
                                        {
                                            isLate = true;
                                            break;
                                        }
                                        else
                                        {
                                            isLate = false;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else if (isShift) //Shift
                    {
                        if (timePair.ClockIN != null)
                        {
                            //check late
                            if (workBeginTime.TimeOfDay < timePair.ClockIN.EventTime.TimeOfDay)// && timePair.ClockIN.EventTime <= workEndTime)
                            {
                                isLate = true;
                                //is attendance fix late
                                if (attendanceDict.ContainsKey(timePair.Date))
                                {
                                    DateTime beginLate = workBeginTime;
                                    // เนื่องจากการรับรองเวลาไม่รองรับหน่วยวินาที จึงต้องลบเวลาที่เป็นหน่วยวินาทีออก
                                    DateTime endLate = timePair.ClockIN.EventTime.AddSeconds(-timePair.ClockIN.EventTime.Second);

                                    foreach (INFOTYPE2002 item1 in attendanceDict[timePair.Date])
                                    {
                                        //normalize attendance time
                                        DateTime beginAttend = timePair.Date.Add(item1.BeginTime);
                                        DateTime endAttend = timePair.Date.Add(item1.EndTime);
                                        if (endAttend <= beginAttend)
                                            endAttend = endAttend.AddDays(1);

                                        if ((beginAttend <= beginLate) && (endLate <= endAttend))
                                        {
                                            //Time attendance cover work schedule, so it fix the late.
                                            isLate = false;
                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                //is attendance fix late
                                if (attendanceDict.ContainsKey(timePair.Date))
                                {
                                    DateTime beginLate = timePair.ClockIN.EventTime;

                                    foreach (INFOTYPE2002 item1 in attendanceDict[timePair.Date])
                                    {
                                        //normalize attendance time
                                        DateTime beginAttend = timePair.Date.Add(item1.BeginTime);
                                        if (timePair.DWS.WorkBeginTime > timePair.DWS.WorkEndTime)
                                        {
                                            beginAttend = beginAttend.AddDays(1);
                                        }
                                        if (!item1.AllDayFlag && beginLate > beginAttend)
                                        {
                                            isLate = true;
                                            break;
                                        }
                                        else
                                        {
                                            isLate = false;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else if (isFlex) //Flex
                    {
                        if (timePair.ClockIN != null)
                        {
                            //check late
                            if (FlexLate.TimeOfDay < timePair.ClockIN.EventTime.TimeOfDay)// && timePair.ClockIN.EventTime <= workEndTime)
                            {
                                isLate = true;
                                //is attendance fix late
                                if (attendanceDict.ContainsKey(timePair.Date))
                                {
                                    DateTime beginLate = workBeginTime;
                                    // เนื่องจากการรับรองเวลาไม่รองรับหน่วยวินาที จึงต้องลบเวลาที่เป็นหน่วยวินาทีออก
                                    DateTime endLate = timePair.ClockIN.EventTime.AddSeconds(-timePair.ClockIN.EventTime.Second);

                                    foreach (INFOTYPE2002 item1 in attendanceDict[timePair.Date])
                                    {
                                        //normalize attendance time
                                        DateTime beginAttend = timePair.Date.Add(item1.BeginTime);
                                        DateTime endAttend = timePair.Date.Add(item1.EndTime);
                                        if (endAttend <= beginAttend)
                                            endAttend = endAttend.AddDays(1);

                                        if ((beginAttend <= beginLate) && (endLate <= endAttend))
                                        {
                                            //Time attendance cover work schedule, so it fix the late.
                                            isLate = false;
                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                //is attendance fix late
                                if (attendanceDict.ContainsKey(timePair.Date))
                                {
                                    DateTime beginLate = timePair.ClockIN.EventTime;

                                    foreach (INFOTYPE2002 item1 in attendanceDict[timePair.Date])
                                    {
                                        //normalize attendance time
                                        DateTime beginAttend = timePair.Date.Add(item1.BeginTime);
                                        if (timePair.DWS.WorkBeginTime > timePair.DWS.WorkEndTime)
                                        {
                                            beginAttend = beginAttend.AddDays(1);
                                        }
                                        if (!item1.AllDayFlag && beginLate > beginAttend)
                                        {
                                            isLate = true;
                                            break;
                                        }
                                        else
                                        {
                                            isLate = false;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion


                foreach (var tm in obj.Timesheets.Where(w => w.Date == _timePairs.Date))
                {
                    isClockoutEarly = tm.ClockoutEarly;
                    isClockNoneWork = tm.ClockNoneWork;
                }

                var timesheet = new TimesheetModel()
                {
                    Date = _timePairs.Date,
                    ClockIn = _timePairs.ClockIN == null ? (DateTime?)null : _timePairs.ClockIN.EventTime,
                    ClockOut = _timePairs.ClockOUT == null ? (DateTime?)null : _timePairs.ClockOUT.EventTime,
                    ClockInLate = isLate,
                    ClockoutEarly = isClockoutEarly,
                    ClockNoneWork = isClockNoneWork,
                    //ClockInLate = false,
                    //ClockoutEarly = false,
                    //ClockNoneWork = false,
                    InvalidClockInOut = false,
                    TextDescription = _timePairs.Remark,
                    IsCorrectPolicy = _timePairs.IsCheckRemark
                };


                if (isShift)
                {
                    if ((_timePairs.ClockIN != null && _timePairs.ClockOUT != null))
                    {
                        if (_timePairs.ClockIN.EventTime != null && _timePairs.ClockOUT.EventTime != null && timesheet.ClockNoneWork)
                        {
                            timesheet.ClockNoneWork = false;
                        }
                    }
                }
                if (isFlex)
                {
                    if ((_timePairs.ClockIN != null && _timePairs.ClockOUT != null))
                    {
                        if (_timePairs.ClockIN.EventTime != null && _timePairs.ClockOUT.EventTime != null && timesheet.ClockNoneWork)
                        {
                            timesheet.ClockNoneWork = false;
                        }
                    }
                }

                string chkTextIsAM = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetCommonText("HRTMABSENCE", WorkflowPrinciple.Current.UserSetting.Language, "AMTIME");
                string chkTextIsPM = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetCommonText("HRTMABSENCE", WorkflowPrinciple.Current.UserSetting.Language, "PMTIME");
                string chkTextIsFullDay = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetCommonText("HRTMABSENCE", WorkflowPrinciple.Current.UserSetting.Language, "FULLDAY");

                EmployeeData _EmployeeData = new EmployeeData(_timePairs.EmployeeID, _timePairs.Date);
                if (_EmployeeData.EmpSubAreaType == EmployeeSubAreaType.Flex)
                {
                    INFOTYPE0007 oInfotype0007 = _EmployeeData.GetEmployeeWF(_EmployeeData.EmployeeID, _timePairs.Date);
                    if (!string.IsNullOrEmpty(oInfotype0007.WFRule))
                    {
                        TimeSpan oBeginTimeDailyFlexTime;
                        TimeSpan oEndTimeDailyFlexTime;
                        DailyFlexTime oDFX = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetDailyFlexTimeMinForCalculateByFlexCode(oInfotype0007.WFRule);

                        BreakPattern oBreak = new BreakPattern();
                        oBreak = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetBreakPattern(_timePairs.DWS.DailyWorkscheduleGrouping, oDFX.BreakCode);

                        TimeSpan TBegin;
                        if (!TimeSpan.TryParse(oDFX.BeginTime, out TBegin))
                        {
                            // handle validation error
                        }
                        oBeginTimeDailyFlexTime = new TimeSpan(TBegin.Hours, TBegin.Minutes, 0);

                        TimeSpan TEnd;
                        if (!TimeSpan.TryParse(oDFX.EndTime, out TEnd))
                        {
                            // handle validation error
                        }
                        oEndTimeDailyFlexTime = new TimeSpan(TEnd.Hours, TEnd.Minutes, 0);
                        _timePairs.DWS.WorkBeginTime = oBeginTimeDailyFlexTime;
                        _timePairs.DWS.WorkEndTime = oEndTimeDailyFlexTime;

                        // เพิ่ม Logic หากเป็นพนักงานที่ไม่ต้องรูดบัตร
                        if (oInfotype0007.TimeEvaluateClass == "0")
                        {
                            timesheet.IsNoScanCard = true;
                        }

                        if (_timePairs.ClockIN != null)
                        {
                            string sClockInTime = _timePairs.ClockIN.EventTime.ToString("HH:mm", oCL);
                            DailyFlexTime oDailyFX = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetTimesheetChangeFlexTime(oInfotype0007.WFRule, sClockInTime);

                            if (oDailyFX.BeginTime != null && oDailyFX.EndTime != null)
                            {
                                TimeSpan TBeginFlex;
                                if (!TimeSpan.TryParse(oDailyFX.BeginTime, out TBeginFlex))
                                {
                                    // handle validation error
                                }
                                timesheet.BeginTimeFlexChange = TimeSpanToString(new TimeSpan(TBeginFlex.Hours, TBeginFlex.Minutes, 0), oCL);

                                TimeSpan TEndFlex;
                                if (!TimeSpan.TryParse(oDailyFX.EndTime, out TEndFlex))
                                {
                                    // handle validation error
                                }
                                timesheet.EndTimeFlexChange = TimeSpanToString(new TimeSpan(TEndFlex.Hours, TEndFlex.Minutes, 0), oCL);
                                timesheet.IsFlexChange = true;                      

                                DateTime dCheckOut = new DateTime(timesheet.Date.Year, timesheet.Date.Month, timesheet.Date.Day, TEndFlex.Hours, TEndFlex.Minutes, TEndFlex.Seconds);
                                if (timesheet.ClockOut < dCheckOut)
                                {
                                    if (!timesheet.TextDescription.Contains(chkTextIsFullDay) && !timesheet.TextDescription.Contains(chkTextIsPM))
                                    {
                                        timesheet.ClockoutEarly = true;
                                    }
                                }
                                else
                                {
                                    timesheet.ClockoutEarly = false;
                                }

                                DateTime dCheckIn = new DateTime(timesheet.Date.Year, timesheet.Date.Month, timesheet.Date.Day, TBeginFlex.Hours, TBeginFlex.Minutes, TBeginFlex.Seconds);
                                if (timesheet.ClockIn > dCheckIn)
                                {
                                    timesheet.ClockInLate = true;
                                }
                                else
                                {
                                    timesheet.ClockInLate = false;
                                }
                            }
                            else
                            {
                                timesheet.OverDailyFlexTime = true;
                                DateTime dCheckOut = new DateTime(timesheet.Date.Year, timesheet.Date.Month, timesheet.Date.Day, _timePairs.DWS.WorkEndTime.Hours, _timePairs.DWS.WorkEndTime.Minutes, _timePairs.DWS.WorkEndTime.Seconds);
                                if (timesheet.ClockOut < dCheckOut)
                                {
                                    if (!timesheet.TextDescription.Contains(chkTextIsFullDay) && !timesheet.TextDescription.Contains(chkTextIsPM))
                                    {
                                        timesheet.ClockoutEarly = true;
                                    }
                                }
                                else
                                {
                                    timesheet.ClockoutEarly = false;
                                }

                                DateTime dCheckIn = new DateTime(timesheet.Date.Year, timesheet.Date.Month, timesheet.Date.Day, _timePairs.DWS.WorkBeginTime.Hours, _timePairs.DWS.WorkBeginTime.Minutes, _timePairs.DWS.WorkBeginTime.Seconds);
                                if (timesheet.ClockIn > dCheckIn)
                                {
                                    timesheet.ClockInLate = true;
                                }
                                else
                                {
                                    timesheet.ClockInLate = false;
                                }
                            }
                        }
                    }
                }
                else
                {
                    // เพิ่ม Logic หากเป็นพนักงานที่ไม่ต้องรูดบัตร
                    INFOTYPE0007 oInfotype0007 = _EmployeeData.GetEmployeeWF(_EmployeeData.EmployeeID, _timePairs.Date);
                    if (!string.IsNullOrEmpty(oInfotype0007.WFRule))
                    {
                        if (oInfotype0007.TimeEvaluateClass == "0")
                        {
                            timesheet.IsNoScanCard = true;
                        }
                    }
                }


                if(isShift)
                {
                    if (_timePairs.ClockIN == null && _timePairs.ClockOUT == null && _timePairs.Date < DateTime.Now && string.IsNullOrEmpty(_timePairs.Remark) && !timesheet.IsNoScanCard)
                    {
                        if (!timesheet.ClockNoneWork)
                        {
                            timesheet.ClockNoneWork = true;
                        }
                    }

                    if (_timePairs.ClockOUT != null)
                    {
                        if (_timePairs.ClockOUT.EventTime != null && _timePairs.ClockOUT.EventTime.TimeOfDay >= _timePairs.DWS.WorkEndTime && timesheet.ClockoutEarly)
                        {
                            timesheet.ClockoutEarly = false;
                        }

                        if (_timePairs.ClockOUT.EventTime != null && _timePairs.ClockOUT.EventTime.TimeOfDay < _timePairs.DWS.WorkEndTime && !timesheet.ClockoutEarly && !timesheet.IsNoScanCard)
                        {
                            timesheet.ClockoutEarly = true;
                        }
                    }

                    if ((timesheet.TextDescription.Contains(chkTextIsFullDay) || timesheet.TextDescription.Contains(chkTextIsPM)) && timesheet.ClockoutEarly)
                    {
                        timesheet.ClockoutEarly = false;
                    }

                    if(timesheet.TextDescription.Contains(chkTextIsFullDay) && timesheet.ClockNoneWork)
                    {
                        timesheet.ClockNoneWork = false;
                    }

                    if ((timesheet.TextDescription.Contains(chkTextIsFullDay) || timesheet.TextDescription.Contains(chkTextIsAM)) && timesheet.ClockInLate)
                    {
                        timesheet.ClockInLate = false;
                    }
                }

                if (isFlex)
                {
                    if ((timesheet.TextDescription.Contains(chkTextIsFullDay) || timesheet.TextDescription.Contains(chkTextIsPM)) && timesheet.ClockoutEarly)
                    {
                        timesheet.ClockoutEarly = false;
                    }

                    if (timesheet.TextDescription.Contains(chkTextIsFullDay) && timesheet.ClockNoneWork)
                    {
                        timesheet.ClockNoneWork = false;
                    }

                    if ((timesheet.TextDescription.Contains(chkTextIsFullDay) || timesheet.TextDescription.Contains(chkTextIsAM)) && timesheet.ClockInLate)
                    {
                        timesheet.ClockInLate = false;
                    }
                }

                bool IsWorking = false;
                if (_timePairs.DWSCode.StartsWith("OFF") || _timePairs.DWSCode.StartsWith("HOL"))
                {
                    timesheet.Type = _timePairs.DWSCode;
                    if(timesheet.ClockNoneWork)
                    {
                        timesheet.ClockNoneWork = false;
                    }
                    if (timesheet.ClockInLate)
                    {
                        timesheet.ClockInLate = false;
                    }
                    if (timesheet.ClockoutEarly)
                    {
                        timesheet.ClockoutEarly = false;
                    }
                }
                else
                {
                    if (timesheet.IsFlexChange)
                    {
                        timesheet.Type = string.Format("{0} ({1}-{2})", _timePairs.DWSCode, timesheet.BeginTimeFlexChange, timesheet.EndTimeFlexChange);
                    }
                    else
                    {
                        timesheet.Type = string.Format("{0} ({1}-{2})", _timePairs.DWSCode, TimeSpanToString(_timePairs.DWS.WorkBeginTime, oCL), TimeSpanToString(_timePairs.DWS.WorkEndTime, oCL));
                    }
                    IsWorking = true;
                }

                // เป็นวันทำงานจริง
                if (IsWorking)
                {
                    // พนักงานที่ไม่ต้องรูดเวลาเข้างาน 
                    if (timesheet.IsNoScanCard)
                    {
                        timesheet.ClockInLate = false;
                        timesheet.ClockoutEarly = false;
                        timesheet.ClockNoneWork = false;
                        // หาก Text ไม่ใช่ค่าว่างแปลว่ามีการลา , รับรองเวลา ไม่ต้องเอาคำว่าไม่ต้องรูดบัตรมาใส่
                        if (string.IsNullOrEmpty(timesheet.TextDescription))
                        {
                            if (date_now.Date >= timesheet.Date.Date)
                            {
                                timesheet.TextDescription = HRTMManagement
                               .CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                               .GetCommonText("TIMESHEETMANAGETIME", WorkflowPrinciple.Current.UserSetting.Language, "LELVE_11_STAFF_UP");
                            }
                        }
                    }
                }


                result.TimeSheets.Add(timesheet);
            }
            result.TimeElements = obj.TimeElements;

            return result;
        }


        public class GetAttendanceDocument_Params : RequestParameter
        {
            public string RequestNo { get; set; }
        }
        [HttpPost]
        public RequestDocument GetAttendanceDocument(GetAttendanceDocument_Params oRequestParameter)
        {
            RequestDocument doc;
            string reqNo = oRequestParameter.RequestNo;
            EmployeeData emp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);
            doc = RequestDocument.LoadDocumentReadOnly(reqNo, oRequestParameter.Requestor.CompanyCode, emp, true, false);
            return doc;
        }


        public class GetAttendanceTypeList_Params : RequestParameter
        {
            public string Language { get; set; }
        }
        [HttpPost]
        public List<AttendanceType> GetAttendanceTypeList(GetAttendanceTypeList_Params oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            var result = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetAttendanceTypeList(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Language);

            return result;
        }


        public class CalculateAttendanceEntry_Params : RequestParameter
        {
            public INFOTYPE2002 Info { get; set; }
        }
        [HttpPost]
        public INFOTYPE2002 CalculateAttendanceEntry(CalculateAttendanceEntry_Params model)
        {
            EmployeeData emp = new EmployeeData(model.Requestor.EmployeeID, model.Requestor.PositionID, model.Requestor.CompanyCode, DateTime.Now);
            var temp = HRTMManagement.CreateInstance(model.CurrentEmployee.CompanyCode).CalculateAttendanceEntry(emp, model.Info.SubType, model.Info.BeginDate, model.Info.EndDate, model.Info.BeginTime, model.Info.EndTime, model.Info.AllDayFlag);
            model.Info.AttendanceHours = temp.AbsenceHours;
            model.Info.AttendanceDays = temp.AbsenceDays;
            model.Info.PayrollDays = temp.PayrollDays;
            return model.Info;
        }


        public class GetListAbsence_Parms : RequestParameter
        {
            public int SelectYearAbsence { get; set; }
        }

        [HttpPost]
        public List<AbsenceListData> GetListAbsence(GetListAbsence_Parms oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);

            List<INFOTYPE2001> oResultMapping = new List<INFOTYPE2001>();
            List<AbsenceListData> oAbListData = new List<AbsenceListData>();

            if (WorkflowPrinciple.Current.UserSetting.Employee.IsInRole("#GROUPEMPLOYEE"))
            {
                var result = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetListAbsence(oEmp, oRequestParameter.CurrentEmployee.CompanyCode, oRequestParameter.SelectYearAbsence);
                if (result.Count > 0)
                {
                    foreach (INFOTYPE2001 item in result)
                    {
                        AbsenceListData oAbData = new AbsenceListData();
                        oAbData.RequestNo = item.RequestNo;
                        oAbData.BeginDate = item.BeginDate;
                        oAbData.EndDate = item.EndDate;
                        oAbData.BeginTime = item.BeginTime;
                        oAbData.EndTime = item.EndTime;
                        oAbData.AbsenceDays = item.AbsenceDays;
                        oAbData.AbsenceHours = item.AbsenceHours;
                        oAbData.PayrollDays = item.PayrollDays;
                        oAbData.AllDayFlag = item.AllDayFlag;
                        oAbData.Status = item.Status;
                        oAbData.Remark = item.Remark;
                        oAbData.IsRequireDocument = item.IsRequireDocument;
                        oAbData.IsMark = item.IsMark;
                        oAbData.IsPost = item.IsPost;
                        oAbData.IsSkip = item.IsSkip;
                        oAbData.Absencetype = item.AbsenceType;
                        oAbData.AbsenceConcat =
                            (item.AllDayFlag) ? item.EmployeeID + "|" + item.AbsenceType + "|" +
                            item.BeginDate.ToString("yyyyMMdd") + "|" + item.EndDate.ToString("yyyyMMdd")
                            : item.EmployeeID + "|" + item.AbsenceType + "|" + item.BeginDate.ToString("yyyyMMdd") + "|" + item.EndDate.ToString("yyyyMMdd") + "|" +
                            item.BeginTime.ToString() + "|" + item.EndTime.ToString();
                        oAbData.AbsenceGroupData = oEmp.OrgAssignment.SubAreaSetting.AbsAttGrouping + "#" + item.AbsenceType;

                        oAbListData.Add(oAbData);
                        //oResultMapping.Add(item);
                    }
                }
            }

            return oAbListData;
        }
        [HttpPost]
        public List<AttendanceListData> GetListAttendance(RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);

            List<INFOTYPE2002> oResultMapping = new List<INFOTYPE2002>();
            List<AttendanceListData> oAttListData = new List<AttendanceListData>();

            if (WorkflowPrinciple.Current.UserSetting.Employee.IsInRole("#GROUPEMPLOYEE"))
            {
                var result = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetListAttendance(oEmp, oRequestParameter.CurrentEmployee.CompanyCode);
                if (result.Count > 0)
                {
                    foreach (INFOTYPE2002 item in result)
                    {
                        AttendanceListData oAttData = new AttendanceListData();
                        oAttData.RequestNo = item.RequestNo;
                        oAttData.BeginDate = item.BeginDate;
                        oAttData.EndDate = item.EndDate;
                        oAttData.BeginTime = item.BeginTime;
                        oAttData.EndTime = item.EndTime;
                        oAttData.AttendanceDays = item.AttendanceDays;
                        oAttData.AttendanceHours = item.AttendanceHours;
                        oAttData.PayrollDays = item.PayrollDays;
                        oAttData.AllDayFlag = item.AllDayFlag;
                        oAttData.Status = item.Status;
                        oAttData.Remark = item.Remark;
                        oAttData.IsPrevDay = item.IsPrevDay;
                        oAttData.IsMark = item.IsMark;
                        oAttData.IsPost = item.IsPost;
                        oAttData.AttendanceType = item.AttendanceType;
                        oAttData.AttendanceConcat = item.EmployeeID + "|" + item.AttendanceType + "|" +
                            item.BeginDate.ToString("yyyyMMdd") + "|" + item.EndDate.ToString("yyyyMMdd") + "|" +
                            item.BeginTime.ToString() + "|" + item.EndTime.ToString();

                        oAttListData.Add(oAttData);
                    }
                }
            }

            return oAttListData;
        }

        public class GetCalAbsence_Param : RequestParameter
        {
            public string AbsenceType { get; set; }
            public DateTime BeginDate { get; set; }
            public DateTime EndDate { get; set; }
            public TimeSpan BeginTime { get; set; }
            public TimeSpan EndTime { get; set; }
            public bool AllDayFlag { get; set; }
            public string RequestNo { get; set; }
        }
        [HttpPost]
        public AbsAttCalculateResult GetCalAbsence(GetCalAbsence_Param oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            AbsAttCalculateResult result = HRTMManagement
                .CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                .CalAbsence(oRequestParameter.Requestor.EmployeeID, oRequestParameter.AbsenceType,
                oRequestParameter.BeginDate,
                oRequestParameter.EndDate,
                oRequestParameter.BeginTime,
                oRequestParameter.EndTime,
                oRequestParameter.AllDayFlag,
                oRequestParameter.RequestNo);

            return result;
        }

        public class CalAbsenceIsAllDay_Param : RequestParameter
        {
            public string AbsenceType { get; set; }
            public DateTime BeginDate { get; set; }
            public DateTime EndDate { get; set; }
        }
        [HttpPost]
        public AbsAttCalculateResult CalAbsenceIsAllDay(CalAbsenceIsAllDay_Param oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            AbsAttCalculateResult result = HRTMManagement
                .CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                .CalAbsenceIsAllDay(oRequestParameter.Requestor.EmployeeID, oRequestParameter.AbsenceType,
                oRequestParameter.BeginDate,
                oRequestParameter.EndDate);

            return result;
        }

        public class GetAbsenceDocument_Params : RequestParameter
        {
            public string RequestNoAbsence { get; set; }
        }
        [HttpPost]
        public RequestDocument GetAbsenceDocument(GetAbsenceDocument_Params oRequestParameter)
        {
            RequestDocument doc;
            string reqNo = oRequestParameter.RequestNoAbsence;
            EmployeeData emp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);
            doc = RequestDocument.LoadDocumentReadOnly(reqNo, oRequestParameter.Requestor.CompanyCode, emp, true, false);
            return doc;
        }

        [HttpPost]
        public DataTable GetPeriodDutyPayment(GetAbsenceDocument_Params oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            DataTable result = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetPeriodDutyPaymentCreate(oRequestParameter.CurrentEmployee.Language);

            return result;
        }

        public class DutyPaymentLog_ListContent
        {
            public DutyPaymentLog_ListContent()
            {
                list_duty = new List<DutyPaymentLog>();
                duty_dailyWs = new Dictionary<DateTime, DailyWS>();
            }
            public List<DutyPaymentLog> list_duty { get; set; }
            public Dictionary<DateTime, DailyWS> duty_dailyWs { get; set; }
        }

        public class GetDutyPayment_Params : RequestParameter
        {
            public string SelectedPeriod { get; set; }
        }
        [HttpPost]
        public DutyPaymentLog_ListContent GetListDataPeriodDutyPayment(GetDutyPayment_Params oRequestParameter)
        {
            DutyPaymentLog_ListContent data_result = new DutyPaymentLog_ListContent();

            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);

            int currentYear = int.Parse(oRequestParameter.SelectedPeriod.Substring(0, 4));
            int currentMonth = int.Parse(oRequestParameter.SelectedPeriod.Substring(4, 2));
            DateTime oCurrentDate = new DateTime(currentYear, currentMonth, 1);

            DateTime BeginDate = oCurrentDate;
            DateTime EndDate = BeginDate.AddMonths(1).AddMinutes(-1);

            var result = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetDutyPaymentLog(oRequestParameter.Requestor.EmployeeID, BeginDate, EndDate);

            Dictionary<DateTime, DailyWS> dictDWS = new Dictionary<DateTime, DailyWS>();
            if (result.Count > 0)
            {

                foreach (var item in result)
                {
                    var dailyWs = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetListDetailDailyWs(oEmp, item.DutyDate, item.BeginDate, item.EndDate);

                    data_result.duty_dailyWs.Add(item.DutyDate, dailyWs);
                }
            }

            data_result.list_duty.AddRange(result);

            return data_result;
        }

        public class GetDutyPaymentDocument_Params : RequestParameter
        {
            public string DutyPayment { get; set; }
        }
        [HttpPost]
        public RequestDocument GetDutyPaymentDocument(GetDutyPaymentDocument_Params oRequestParameter)
        {
            RequestDocument doc;
            string reqNo = oRequestParameter.DutyPayment;
            EmployeeData emp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);
            doc = RequestDocument.LoadDocumentReadOnly(reqNo, oRequestParameter.Requestor.CompanyCode, emp, true, false);
            return doc;
        }

        public class GetEditorDutyPaymentDocument_Params : RequestParameter
        {
            public string PeriodDutyPayment { get; set; }
        }
        [HttpPost]
        public object GetListEditorDutyPayment(GetEditorDutyPaymentDocument_Params oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            DateTime dtBeginDate = DateTime.ParseExact(oRequestParameter.PeriodDutyPayment, "yyyyMM", oCL);
            DateTime dtEndDate = dtBeginDate.AddMonths(1).AddDays(-1);

            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, dtEndDate);

            var result = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetListDuty(oEmp, oRequestParameter.PeriodDutyPayment);

            return result;
        }

        public class DataDutyPaymentLog
        {
            public string RequestNo { get; set; }
            public string ItemNo { get; set; }
            public string EmployeeID { get; set; }
            public DateTime DutyDate { get; set; }
            public DateTime BeginDate { get; set; }
            public DateTime EndDate { get; set; }
            public string Status { get; set; }
            public string RefRequestNo { get; set; }
        }
        public class GetViewerDutyPaymentDocument_Params : RequestParameter
        {
            public GetViewerDutyPaymentDocument_Params()
            {
                ListDocumentDutyPayment = new List<DataDutyPaymentLog>();
            }
            public List<DataDutyPaymentLog> ListDocumentDutyPayment { get; set; }
        }

        [HttpPost]
        public DataTable GetPeriodSummaryOTList(GetAbsenceDocument_Params oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            DataTable result = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetPeriodSummaryOTList(oRequestParameter.CurrentEmployee.Language);

            return result;
        }


        public class GetListSummaryOT_Params : RequestParameter
        {
            public string EmployeeID { get; set; }
            public string Period { get; set; }
        }
        [HttpPost]
        public DataSet GetListSummaryOT(GetListSummaryOT_Params oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            DataSet DS = new DataSet("ADDITIONAL");
            DataTable oTable;


            DailyOTLog oItem = new DailyOTLog();
            oTable = oItem.ToADODataTable(true);
            oTable.TableName = "DailyOTLog";

            var result = HRTMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetListSummaryOT(oRequestParameter.EmployeeID, DS, oRequestParameter.Period, "");
            oTable.Rows.Clear();
            foreach (DailyOTLog DailyOTLog in result.DailyOTLog)
            {
                DataRow dr = oTable.NewRow();
                DailyOTLog.LoadDataToTableRow(dr);
                oTable.Rows.Add(dr);
            }

            DS.Tables.Add(oTable);

            oTable = new DataTable();
            oTable = oItem.ToADODataTable(true);
            oTable.TableName = "AllDailyOTLog";
            oTable.Rows.Clear();
            foreach (DailyOTLog DailyOTLog in result.AllDailyOTLog)
            {
                DataRow dr = oTable.NewRow();
                DailyOTLog.LoadDataToTableRow(dr);
                oTable.Rows.Add(dr);
            }
            DS.Tables.Add(oTable);

            return DS;
        }

        public class GetEmployeeByEmpID_Params : RequestParameter
        {
            public string EmployeeID { get; set; }
        }
        [HttpPost]
        public DataTable GetEmployeeByEmpID(GetEmployeeByEmpID_Params oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            DataTable result = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetEmployeeByEmpID(oRequestParameter.EmployeeID, DateTime.Now, oRequestParameter.CurrentEmployee.Language);

            return result;
        }

        public class GetListEmpResponsible_Params : RequestParameter
        {
            public string Period { get; set; }
            public int SubjectID { get; set; }
        }
        public class EmpResponsibleModel
        {
            public string EmployeeId { get; set; }
            public string EmployeeName { get; set; }
            public int SumRequest { get; set; }
            public int SumPayroll { get; set; }
        }
        [HttpPost]
        public List<EmpResponsibleModel> GetListEmpResponsible(GetListEmpResponsible_Params oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);
            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);

            var employees = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetListEmpResponsible(oEmp, oRequestParameter.SubjectID);
            var results = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetListSummaryOTEmpResponsible(employees, oRequestParameter.Period);
            var empResponsibles = new List<EmpResponsibleModel>();
            foreach (var item in results.GroupBy(a => a.EmployeeID))
            {
                var temp = new EmpResponsibleModel()
                {
                    EmployeeId = item.Key,
                    EmployeeName = item.FirstOrDefault().Concat,
                    SumRequest = item.Select(a => a.ApproveRecord).DefaultIfEmpty(0).Sum(),
                    SumPayroll = item.Select(a => a.PayrollRecord).DefaultIfEmpty(0).Sum()
                };
                empResponsibles.Add(temp);
            }

            return empResponsibles;
        }

        [HttpPost]
        public Dictionary<DateTime, DailyWS> GetDataDaliyWs(GetViewerDutyPaymentDocument_Params oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);
            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);


            Dictionary<DateTime, DailyWS> dictDWS = new Dictionary<DateTime, DailyWS>();
            if (oRequestParameter.ListDocumentDutyPayment.Count > 0)
            {
                foreach (var item in oRequestParameter.ListDocumentDutyPayment)
                {
                    var dailyWs = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                       .GetListDetailDailyWs(oEmp, item.DutyDate, item.BeginDate, item.EndDate);

                    dictDWS.Add(item.DutyDate, dailyWs);
                }
            }
            return dictDWS;
        }

        [HttpPost]
        public DataTable GetPeriodChangeWorking(RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);
            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);

            DataTable periodYearWorkingChange = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                       .GetPeriodSubstitutionCreate(oCurrentEmployee.Language);

            return periodYearWorkingChange;
        }

        public class PeriodChangeWorking_Params : RequestParameter
        {
            public string Period { get; set; }
        }
        [HttpPost]
        public List<SubstitutionData> GetLisChangetWorking(PeriodChangeWorking_Params oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);
            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);

            var periodYearWorkingChange = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                       .GetSubstitutionList(oEmp, oRequestParameter.Period);

            //DataTable objEmployee = EmployeeManagement
            //    .CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
            //    .GetEmployeeByEmpID(empId, DateTime.Now, oCurrentEmployee.Language);

            List<SubstitutionData> listSubstitutionData = new List<SubstitutionData>();
            foreach (var item in periodYearWorkingChange)
            {
                if (item.EmpDWSCodeOld != null)
                {


                    var EmpData = new EmployeeData(item.EmployeeID, item.BeginDate);
                    var SubData = new EmployeeData(item.Substitute, item.BeginDate);

                    var EmpDWSOld = EmpData.GetDailyWorkSchedule(item.BeginDate, false);
                    if (EmpDWSOld.WorkBeginTime.Days >= 0)
                        item.WorkingEmpDWSCodeOld = EmpDWSOld.WorkBeginTime.ToString() + " - " + EmpDWSOld.WorkEndTime.ToString();
                    else
                        item.WorkingEmpDWSCodeOld = "";

                    var EmpDWSNew = SubData.GetDailyWorkSchedule(item.BeginDate, false);
                    if (EmpDWSNew.WorkBeginTime.Days >= 0)
                        item.WorkingEmpDWSCodeNew = EmpDWSNew.WorkBeginTime.ToString() + " - " + EmpDWSNew.WorkEndTime.ToString();
                    else
                        item.WorkingEmpDWSCodeNew = "";


                    var SubDWSOld = SubData.GetDailyWorkSchedule(item.BeginDate, false);
                    if (SubDWSOld.WorkBeginTime.Days >= 0)
                        item.WorkingSubDWSCodeOld = SubDWSOld.WorkBeginTime.ToString() + " - " + SubDWSOld.WorkEndTime.ToString();
                    else
                        item.WorkingSubDWSCodeOld = "";

                    var SubDWSNew = EmpData.GetDailyWorkSchedule(item.BeginDate, false);
                    if (SubDWSNew.WorkBeginTime.Days >= 0)
                        item.WorkingSubDWSCodeNew = SubDWSNew.WorkBeginTime.ToString() + " - " + SubDWSNew.WorkEndTime.ToString();
                    else
                        item.WorkingSubDWSCodeNew = "";


                    DataTable dataSubstitute = EmployeeManagement
                        .CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                        .GetEmployeeByEmpID(item.Substitute, DateTime.Now, oCurrentEmployee.Language);


                    if (dataSubstitute.Rows.Count > 0)
                    {

                        foreach (DataRow row in dataSubstitute.Rows)
                        {
                            item.NameSubstitute = row[1].ToString();
                        }
                    }

                    SubstitutionData _SubstitutionData = new SubstitutionData()
                    {
                        BeginDate = item.BeginDate,
                        CompanyCode = item.CompanyCode,
                        DWSCode = item.DWSCode,
                        DWSGroup = item.DWSGroup,
                        EmpDWSCodeNew = item.EmpDWSCodeNew,
                        EmpDWSCodeOld = item.EmpDWSCodeOld,
                        EmployeeID = item.EmployeeID,
                        EmpSubAreaSetting = item.EmpSubAreaSetting,
                        EmpSubGroupSetting = item.EmpSubGroupSetting,
                        EndDate = item.EndDate,
                        HolidayCalendar = item.HolidayCalendar,
                        IsDraft = item.IsDraft,
                        IsOverride = item.IsOverride,
                        NameSubstitute = item.NameSubstitute,
                        RequestNo = item.RequestNo,
                        Status = item.Status,
                        SubDWSCodeNew = item.SubDWSCodeNew,
                        SubDWSCodeOld = item.SubDWSCodeOld,
                        Substitute = item.Substitute,
                        SubstituteBeginDate = item.SubstituteBeginDate,
                        SubstituteBeginTime = item.SubstituteBeginTime,
                        SubstituteEndDate = item.SubstituteEndDate,
                        SubstituteEndTime = item.SubstituteEndTime,
                        WorkingEmpDWSCodeNew = item.WorkingEmpDWSCodeNew,
                        WorkingEmpDWSCodeOld = item.WorkingEmpDWSCodeOld,
                        WorkingSubDWSCodeNew = item.WorkingSubDWSCodeNew,
                        WorkingSubDWSCodeOld = item.WorkingSubDWSCodeOld,
                        WorkingTimeAfter = item.WorkingTimeAfter,
                        WorkingTimeBefore = item.WorkingTimeBefore,
                        WSRule = item.WSRule
                    };

                    DataTable dataEmployee = EmployeeManagement
                        .CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                        .GetEmployeeByEmpID(item.EmployeeID, DateTime.Now, oCurrentEmployee.Language);


                    if (dataEmployee.Rows.Count > 0)
                    {

                        foreach (DataRow row in dataEmployee.Rows)
                        {
                            _SubstitutionData.NameEmoloyee = row[1].ToString();
                        }
                    }

                    listSubstitutionData.Add(_SubstitutionData);
                }
            }

            return listSubstitutionData;
        }

        public class GetRefreshSubstitution_Params : RequestParameter
        {
            public string sEmpIDChange { get; set; }
            public DateTime RequestDate { get; set; }
            public DateTime ChangeDate { get; set; }
            public bool IsTypeSelectDay { get; set; }
        }
        [HttpPost]
        public object GetRefreshSubstitutionList(GetRefreshSubstitution_Params oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);

            var data_Subtitution = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                       .RefreshSubstitutionList(oEmp
                       , oRequestParameter.sEmpIDChange
                       , oRequestParameter.RequestDate
                       , oRequestParameter.ChangeDate
                       , !oRequestParameter.IsTypeSelectDay);
            // เนื่องจาก logic ปรับให้ false = ช่วง , true = รายบุคคล แต่ใน LNG  => false = รายบุคคล , true = ช่วง

            return data_Subtitution;
        }

        [HttpPost]
        public List<Holiday> GetHoliday(GetRefreshSubstitution_Params oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);

            var Holidaies = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                       .GetHoliday(oEmp, DateTime.Now);

            return Holidaies;
        }

        public class DataWorkDaliyWsSubstitution
        {
            public DataWorkDaliyWsSubstitution()
            {
                EmpDWSOld = new DailyWS();
                EmpDWSNew = new DailyWS();
                SubDWSOld = new DailyWS();
                SubDWSNew = new DailyWS();
            }

            public DailyWS EmpDWSOld { get; set; }
            public DailyWS EmpDWSNew { get; set; }
            public DailyWS SubDWSOld { get; set; }
            public DailyWS SubDWSNew { get; set; }
        }
        public class listExchangeWorking_parms
        {
            public bool IsOverride { get; set; }
            public bool IsDraft { get; set; }
            public bool IsMark { get; set; }
            public string RequestNo { get; set; }
            public string Status { get; set; }
            public string Substitute { get; set; }
            public string Remark { get; set; }
            public string CompanyCode { get; set; }
            public string SubDWSCodeOld { get; set; }
            public string SubDWSCodeNew { get; set; }
            public string EmpDWSCodeNew { get; set; }
            public string EmpDWSCodeOld { get; set; }
            public string EmployeeID { get; set; }
            public DateTime BeginDate { get; set; }
            public DateTime EndDate { get; set; }
            public DateTime SubstituteBeginDate { get; set; }
            public DateTime SubstituteEndDate { get; set; }
            public TimeSpan SubstituteBeginTime { get; set; }
            public TimeSpan SubstituteEndTime { get; set; }
            public string DWSGroup { get; set; }
            public string DWSCode { get; set; }
            public string EmpSubGroupSetting { get; set; }
            public string HolidayCalendar { get; set; }
            public string EmpSubAreaSetting { get; set; }
            public string WSRule { get; set; }

            public string WorkingEmpDWSCodeOld { get; set; }
            public string WorkingEmpDWSCodeNew { get; set; }
            public string WorkingSubDWSCodeOld { get; set; }
            public string WorkingSubDWSCodeNew { get; set; }
        }

        public class ExchangeWorking_Params : RequestParameter
        {
            public ExchangeWorking_Params()
            {
                ListExchangeWorkings = new List<listExchangeWorking_parms>();
            }
            public List<listExchangeWorking_parms> ListExchangeWorkings { get; set; }
        }

        [HttpPost]
        public List<listExchangeWorking_parms> GetDataDailyWorkSchedule(ExchangeWorking_Params oRequestParameter)
        {
            List<listExchangeWorking_parms> result = new List<listExchangeWorking_parms>();

            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            if (oRequestParameter.ListExchangeWorkings.Count > 0)
            {
                foreach (var item in oRequestParameter.ListExchangeWorkings)
                {
                    var EmpData = new EmployeeData(item.EmployeeID, item.BeginDate);
                    var SubData = new EmployeeData(item.Substitute, item.BeginDate);

                    var EmpDWSOld = EmpData.GetDailyWorkSchedule(item.BeginDate, false);
                    var EmpDWSNew = SubData.GetDailyWorkSchedule(item.BeginDate, false);

                    var SubDWSOld = SubData.GetDailyWorkSchedule(item.BeginDate, false);
                    var SubDWSNew = EmpData.GetDailyWorkSchedule(item.BeginDate, false);

                    item.EmpDWSCodeOld = EmpDWSOld.DailyWorkscheduleCode;
                    if (EmpDWSOld.WorkBeginTime.Days >= 0)
                        item.WorkingEmpDWSCodeOld = EmpDWSOld.WorkBeginTime.ToString() + " - " + EmpDWSOld.WorkEndTime.ToString();
                    else
                        item.WorkingEmpDWSCodeOld = "";

                    item.EmpDWSCodeNew = EmpDWSNew.DailyWorkscheduleCode;
                    if (EmpDWSNew.WorkBeginTime.Days >= 0)
                        item.WorkingEmpDWSCodeNew = EmpDWSNew.WorkBeginTime.ToString() + " - " + EmpDWSNew.WorkEndTime.ToString();
                    else
                        item.WorkingEmpDWSCodeNew = "";

                    item.SubDWSCodeOld = SubDWSOld.DailyWorkscheduleCode;
                    if (SubDWSOld.WorkBeginTime.Days >= 0)
                        item.WorkingSubDWSCodeOld = SubDWSOld.WorkBeginTime.ToString() + " - " + SubDWSOld.WorkEndTime.ToString();
                    else
                        item.WorkingSubDWSCodeOld = "";


                    item.SubDWSCodeNew = SubDWSNew.DailyWorkscheduleCode;
                    if (SubDWSNew.WorkBeginTime.Days >= 0)
                        item.WorkingSubDWSCodeNew = SubDWSNew.WorkBeginTime.ToString() + " - " + SubDWSNew.WorkEndTime.ToString();
                    else
                        item.WorkingSubDWSCodeNew = "";

                    result.Add(item);
                }
            }

            return result;
        }

        [HttpPost]
        public int GetSubjectIdData(RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            int subjectId = HRTMManagement
                .CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                .GetTimeManagementSubjectID();

            return subjectId;
        }

        [HttpPost]
        public string GetSupervisorPositionByEmpPositionData(RequestParameter oRequestParameter)
        {
            // Id ตำแหน่งหัวหน้าของพนักงานที่ขอแลกกะ
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            string positionId = oRequestParameter.InputParameter["PositionIdEmployee"].ToString();

            string SupervisorPosition = EmployeeManagement
                .CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                .GetSupervisorPositionByEmpPosition(positionId);

            return SupervisorPosition;
        }

        [HttpPost]
        public DataSet SearchEmployeeDataEmppower(RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            string searchText = oRequestParameter.InputParameter["SEARCHTEXT"].ToString();

            DataSet list_employee = EmployeeManagement
                .CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                .GetEmployeeActiveGetAllSearchText(searchText, DateTime.Now, oCurrentEmployee.Language);

            return list_employee;
        }

        // Get ข้อมูลพนักงาน ตำแหน่ง บริษัท ของพนักงาน Empoower
        [HttpPost]
        public DataTable GetObjectEmployeeEmppower(RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            string empId = oRequestParameter.InputParameter["EmployeeId"].ToString();

            DataTable objEmployee = EmployeeManagement
                .CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                .GetEmployeeByEmpID(empId, DateTime.Now, oCurrentEmployee.Language);

            return objEmployee;
        }

        [HttpPost]
        public List<Holiday> GetHolidaysSubstitute(RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            string substituteId = oRequestParameter.InputParameter["SubstituteId"].ToString();
            string substitutePositionId = oRequestParameter.InputParameter["SubstitutePositionId"].ToString();

            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);

            var HolidaiesSubstitute = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                       .GetHoliday(substituteId, substitutePositionId, DateTime.Now);

            return HolidaiesSubstitute;
        }

        [HttpPost]
        public RequestDocument GetDocumentViewSubstitue(RequestParameter oRequestParameter)
        {
            RequestDocument doc;

            string reqNo = oRequestParameter.InputParameter["RequestSubstituteNo"].ToString();

            EmployeeData emp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);
            doc = RequestDocument.LoadDocumentReadOnly(reqNo, oRequestParameter.Requestor.CompanyCode, emp, true, false);
            return doc;
        }

        [HttpPost]
        public List<INFOTYPE2003_LOG> documentCancelSubstitue(RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);

            string reqNo = oRequestParameter.InputParameter["RequestSubstituteNo"].ToString();

            var list_infotype2300log = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).LoadINFOTYPE2003LOG(reqNo);

            if (list_infotype2300log.Count > 0)
            {

                foreach (var item in list_infotype2300log)
                {
                    var EmpData = new EmployeeData(item.EmployeeID, item.BeginDate);
                    var SubData = new EmployeeData(item.Substitute, item.BeginDate);

                    var EmpDWSOld = EmpData.GetDailyWorkSchedule(item.BeginDate, false);
                    var EmpDWSNew = SubData.GetDailyWorkSchedule(item.BeginDate, false);

                    var SubDWSOld = SubData.GetDailyWorkSchedule(item.BeginDate, false);
                    var SubDWSNew = EmpData.GetDailyWorkSchedule(item.BeginDate, false);

                    item.EmpDWSCodeOld = EmpDWSOld.DailyWorkscheduleCode;
                    if (EmpDWSOld.WorkBeginTime.Days >= 0)
                        item.WorkingEmpDWSCodeOld = EmpDWSOld.WorkBeginTime.ToString() + " - " + EmpDWSOld.WorkEndTime.ToString();
                    else
                        item.WorkingEmpDWSCodeOld = "";

                    item.EmpDWSCodeNew = EmpDWSNew.DailyWorkscheduleCode;
                    if (EmpDWSNew.WorkBeginTime.Days >= 0)
                        item.WorkingEmpDWSCodeNew = EmpDWSNew.WorkBeginTime.ToString() + " - " + EmpDWSNew.WorkEndTime.ToString();
                    else
                        item.WorkingEmpDWSCodeNew = "";

                    item.SubDWSCodeOld = SubDWSOld.DailyWorkscheduleCode;
                    if (SubDWSOld.WorkBeginTime.Days >= 0)
                        item.WorkingSubDWSCodeOld = SubDWSOld.WorkBeginTime.ToString() + " - " + SubDWSOld.WorkEndTime.ToString();
                    else
                        item.WorkingSubDWSCodeOld = "";


                    item.SubDWSCodeNew = SubDWSNew.DailyWorkscheduleCode;
                    if (SubDWSNew.WorkBeginTime.Days >= 0)
                        item.WorkingSubDWSCodeNew = SubDWSNew.WorkBeginTime.ToString() + " - " + SubDWSNew.WorkEndTime.ToString();
                    else
                        item.WorkingSubDWSCodeNew = "";
                }
            }


            return list_infotype2300log;
        }

        [HttpPost]
        public bool CheckRoleMenuSubstitue(RequestParameter oRequestParameter)
        {
            /* Check สถานะว่าพนักงานสามารถแลกกะได้หรือไม่ */
            bool isShowMenuSubstitue = false;
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);

            if (oEmp.EmpSubAreaType == EmployeeSubAreaType.Shift
                || oEmp.EmpSubAreaType == EmployeeSubAreaType.Shift12
                || oEmp.EmpSubAreaType == EmployeeSubAreaType.Shift8)
            {
                isShowMenuSubstitue = true;
            }

            return isShowMenuSubstitue;
        }

        public class TimePairOTSummary
        {
            public DateTime Date { get; set; }
            public DateTime? ClockIn { get; set; }
            public DateTime? ClockOut { get; set; }
            public string Type { get; set; }
            public DateTime? WorkBegin { get; set; }
            public DateTime? WorkEnd { get; set; }
            public DateTime? AbsAttBegin { get; set; }
            public DateTime? AbsAttEnd { get; set; }
            public TimeSpan AbsAttBeginTime { get; set; }
            public TimeSpan AbsAttEndTime { get; set; }
            public bool IsAllDayFlag { get; set; }
            public int CountAbsAtt { get; set; }
        }
        [HttpPost]
        public List<TimePairOTSummary> GetTimePairOTSummary(RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            DateTime BeginDate = Convert.ToDateTime(oRequestParameter.InputParameter["BeginDate"]);
            DateTime EndDate = Convert.ToDateTime(oRequestParameter.InputParameter["EndDate"]);

            EmployeeData oEmp = new EmployeeData(oRequestParameter.InputParameter["EmployeeID"].ToString(), BeginDate);

            var obj = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetTimePairOTSummary(BeginDate, EndDate, oEmp);

            var result = new List<TimePairOTSummary>();
            foreach (var item in obj)
            {

                var oTPSummary = new TimePairOTSummary()
                {
                    Date = item.Date,
                    ClockIn = item.ClockIn,
                    ClockOut = item.ClockOut,
                    Type = item.Type,
                    WorkBegin = item.WorkBegin,
                    WorkEnd = item.WorkEnd,
                    AbsAttBegin = item.AbsAttBegin,
                    AbsAttEnd = item.AbsAttEnd,
                    AbsAttBeginTime = item.AbsAttBeginTime,
                    AbsAttEndTime = item.AbsAttEndTime,
                    IsAllDayFlag = item.IsAllDayFlag,
                    CountAbsAtt = item.CountAbsAtt
                };
                result.Add(oTPSummary);
            }

            return result;
        }

        [HttpPost]
        public List<TimePairOTSummary> GetTimePairAllDailyOTLog(RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            DateTime BeginDate = Convert.ToDateTime(oRequestParameter.InputParameter["BeginDate"]);
            DateTime EndDate = Convert.ToDateTime(oRequestParameter.InputParameter["EndDate"]);

            EmployeeData oEmp = new EmployeeData(oRequestParameter.InputParameter["EmployeeID"].ToString(), BeginDate);

            var obj = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode).GetTimePairOTSummary(BeginDate, EndDate, oEmp);

            var result = new List<TimePairOTSummary>();
            foreach (var item in obj)
            {

                var oTPSummary = new TimePairOTSummary()
                {
                    Date = item.Date,
                    ClockIn = item.ClockIn,
                    ClockOut = item.ClockOut,
                    Type = item.Type,
                    WorkBegin = item.WorkBegin,
                    WorkEnd = item.WorkEnd,
                    AbsAttBegin = item.AbsAttBegin,
                    AbsAttEnd = item.AbsAttEnd,
                    AbsAttBeginTime = item.AbsAttBeginTime,
                    AbsAttEndTime = item.AbsAttEndTime,
                    IsAllDayFlag = item.IsAllDayFlag,
                    CountAbsAtt = item.CountAbsAtt
                };
                result.Add(oTPSummary);
            }

            return result;
        }


        [HttpPost]
        public List<GroupAreaWorkscheduleLog> GetContentChangeWorkingByYear(RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            string year_data = oRequestParameter.InputParameter["Year"].ToString();

            List<GroupAreaWorkscheduleLog> ListgroupContentWorking = new List<GroupAreaWorkscheduleLog>();
            int year_convert;
            if (int.TryParse(year_data, out year_convert))
            {
                DateTime oCurrentDate = new DateTime(year_convert, 1, 1);
                DateTime endDate = oCurrentDate.AddYears(1);
                List<AreaWorkscheduleLog> listAreaWorkscheduleLog = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                    .GetAreaWorkscheduleLogByPeriod(oCurrentDate, endDate, oRequestParameter.Requestor.EmployeeID);
                if (listAreaWorkscheduleLog.Count > 0)
                {
                    List<IGrouping<DateTime, AreaWorkscheduleLog>> group_createdDate = listAreaWorkscheduleLog
                        .GroupBy(s => s.CreatedDate.Date)
                        .OrderByDescending(s => s.Key)
                        .ToList();

                    foreach (var group_data in group_createdDate)
                    {
                        GroupAreaWorkscheduleLog groupContent = new GroupAreaWorkscheduleLog();
                        groupContent.CreateDate = group_data.Key;
                        foreach (var item_data in group_data)
                        {
                            // ค้นหาชื่อพนักงาน
                            DataTable objEmployee = EmployeeManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                                .GetEmployeeByEmpID(item_data.EmployeeID, DateTime.Now, oCurrentEmployee.Language);

                            foreach (DataRow row in objEmployee.Rows)
                            {
                                item_data.EmployeeName = row["EmployeeName"].ToString();
                            }

                            // ค้นหา Text Schedule Before
                            string textScheduleBefore = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                                .GetTextNameAreaWorkscheduleByWFRule(item_data.EmployeeID, item_data.EffectiveDate);
                            item_data.BeforeTextName = textScheduleBefore;

                            groupContent.List_AreaWorkscheduleLog.Add(item_data);
                        }
                        ListgroupContentWorking.Add(groupContent);
                    }
                }
            }
            return ListgroupContentWorking;
        }

        [HttpPost]
        public List<AreaWorkschedule> GetListAreaWorkSchedule(RequestParameter oRequestParameter)
        {
            List<AreaWorkschedule> listAreaWorkschedule = new List<AreaWorkschedule>();

            string area = null;
            string subArea = null;

            area = oRequestParameter.InputParameter["Area"].ToString();
            subArea = oRequestParameter.InputParameter["SubArea"].ToString();

            if (!string.IsNullOrEmpty(area) && !string.IsNullOrEmpty(subArea))
            {
                listAreaWorkschedule = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                   .GetAreaWorkscheduleAreaCode(area, subArea);
            }

            return listAreaWorkschedule;
        }

        public class ListDateTimeOt : RequestParameter
        {
            public ListDateTimeOt()
            {
                ListDataDateTime = new List<DateTime>();
            }

            public List<DateTime> ListDataDateTime { get; set; }
        }

        [HttpPost]
        public ListOTOverMsg GetCheckOTOverMsg(ListDateTimeOt DataListDateTimeOt)
        {
            EmployeeDataForMobile oCurrentEmployee = DataListDateTimeOt.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            EmployeeData oEmp = new EmployeeData(DataListDateTimeOt.Requestor.EmployeeID, DataListDateTimeOt.Requestor.PositionID, DataListDateTimeOt.Requestor.CompanyCode, DateTime.Now);

            ListOTOverMsg data = HRTMManagement.CreateInstance(DataListDateTimeOt.CurrentEmployee.CompanyCode).CheckOTOverMsg(oEmp, DataListDateTimeOt.ListDataDateTime);

            return data;
        }

        [HttpPost]
        public DataSet SearchEmployeeChangeWorkingTime(RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            string searchText = oRequestParameter.InputParameter["SEARCHTEXT"].ToString();
            int subjectID = Convert.ToInt32(oRequestParameter.InputParameter["SUBJECTID"]);

            DataSet list_employee = EmployeeManagement
                .CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                .GetUserInResponseForActionOfInstead(oRequestParameter.Requestor.EmployeeID, subjectID, DateTime.Now, oRequestParameter.Requestor.CompanyCode, searchText);

            return list_employee;
        }

        [HttpPost]
        public RequestDocument GetPeriodDocument(RequestParameter oRequestParameter)
        {
            RequestDocument doc;
            string reqNo = oRequestParameter.InputParameter["RequestNo"].ToString();
            EmployeeData emp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);
            doc = RequestDocument.LoadDocumentReadOnly(reqNo, oRequestParameter.Requestor.CompanyCode, emp, true, false);
            return doc;
        }

        public class CreatePeriodEmployee : RequestParameter
        {
            public PeriodData CreatePeriod { get; set; }
        }
        [HttpPost]
        public PeriodData DocumentPeriodSort(CreatePeriodEmployee oRequestParameter)
        {
            PeriodData dataPeriod = oRequestParameter.CreatePeriod;
            PeriodData dataSorting = new PeriodData();
            if (dataPeriod.PeriodGroupType == "1")
            {
                // Sorting Employee
                foreach (var effectiveDate in dataPeriod.PeriodGroupTypeEffectiveDates)
                {
                    var sorting_employee = effectiveDate.PeriodTypeGroupEffectiveDateDetails.OrderBy(s => s.EmployeeId).ToList();

                    effectiveDate.PeriodTypeGroupEffectiveDateDetails = sorting_employee;
                }

                // Sorting Datetime
                var sortingDatetime = dataPeriod.PeriodGroupTypeEffectiveDates.OrderBy(s => s.EffectiveDate).ToList();

                dataPeriod.PeriodGroupTypeEffectiveDates = sortingDatetime;
            }

            if (dataPeriod.PeriodGroupType == "2")
            {
                foreach (var employee in dataPeriod.PeriodGroupTypeEmployees)
                {
                    // Sorting Datetime
                    var sorting_datetime = employee.EmployeeWorkingDetails.OrderBy(s => s.EffectiveDate.HasValue).ToList();
                    employee.EmployeeWorkingDetails = sorting_datetime;
                }
            }

            dataSorting = dataPeriod;

            return dataSorting;
        }

        [HttpPost]
        public string TextNameScheduleBefore(RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            string EmployeeID = oRequestParameter.InputParameter["EmployeeID"].ToString();
            DateTime EffectiveDate = Convert.ToDateTime(oRequestParameter.InputParameter["EffectiveDate"]);

            string TextName = HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                .GetTextNameAreaWorkscheduleByWFRule(EmployeeID, EffectiveDate);

            return TextName;
        }

        [HttpPost]
        public void ExportOtLogReportByAdmin(RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            string list_status = oRequestParameter.InputParameter["ListStatus"].ToString();
            string list_orgtree = oRequestParameter.InputParameter["ListOrgTree"].ToString();
            string startDate = oRequestParameter.InputParameter["StartDate"].ToString();
            string endDate = oRequestParameter.InputParameter["EndDate"].ToString();
            string typeReport = oRequestParameter.InputParameter["ExportTye"].ToString();

            if (string.IsNullOrEmpty(list_status))
            {
                throw new DataServiceException("HRTM_EXCEPTION", "EXEPTION_ORG_NULL");
            }

            if (string.IsNullOrEmpty(list_orgtree))
            {
                throw new DataServiceException("HRTM_EXCEPTION", "EXEPTION_STATUS_NULL");
            }

            List<ReportParameter> ReportParam = new List<ReportParameter>();
            ReportParam.Add(new ReportParameter("ColRequest", CacheManager.GetCommonText("OTLOGREPORT", oCurrentEmployee.Language, "ColRequest")));
            ReportParam.Add(new ReportParameter("ColFinal", CacheManager.GetCommonText("OTLOGREPORT", oCurrentEmployee.Language, "ColFinal")));
            ReportParam.Add(new ReportParameter("ColDate", CacheManager.GetCommonText("OTLOGREPORT", oCurrentEmployee.Language, "ColDate")));
            ReportParam.Add(new ReportParameter("ColStatus", CacheManager.GetCommonText("OTLOGREPORT", oCurrentEmployee.Language, "ColStatus")));
            ReportParam.Add(new ReportParameter("ColNumber10", CacheManager.GetCommonText("OTLOGREPORT", oCurrentEmployee.Language, "ColNumber10")));
            ReportParam.Add(new ReportParameter("ColNumber15", CacheManager.GetCommonText("OTLOGREPORT", oCurrentEmployee.Language, "ColNumber15")));
            ReportParam.Add(new ReportParameter("ColNumber30", CacheManager.GetCommonText("OTLOGREPORT", oCurrentEmployee.Language, "ColNumber30")));
            ReportParam.Add(new ReportParameter("ColNumber", CacheManager.GetCommonText("OTLOGREPORT", oCurrentEmployee.Language, "ColNumber")));
            ReportParam.Add(new ReportParameter("ColNo", CacheManager.GetCommonText("OTLOGREPORT", oCurrentEmployee.Language, "ColNo")));
            ReportParam.Add(new ReportParameter("ColTotal", CacheManager.GetCommonText("OTLOGREPORT", oCurrentEmployee.Language, "ColTotal")));
            ReportParam.Add(new ReportParameter("Title", CacheManager.GetCommonText("OTLOGREPORT", oCurrentEmployee.Language, "Title")));
            ReportParam.Add(new ReportParameter("Subject", String.Format("{0} {1} {2} {3}"
                , CacheManager.GetCommonText("OTLOGREPORT", oCurrentEmployee.Language, "From")
                , DateTime.ParseExact(startDate, "yyyy-MM-dd", oCL).ToString("dd/MM/yyyy", oCL)
                , CacheManager.GetCommonText("OTLOGREPORT", oCurrentEmployee.Language, "To")
                , DateTime.ParseExact(endDate, "yyyy-MM-dd", oCL).ToString("dd/MM/yyyy", oCL))));
            ReportParam.Add(new ReportParameter("Name", CacheManager.GetCommonText("OTLOGREPORT", oCurrentEmployee.Language, "Name")));
            ReportParam.Add(new ReportParameter("Organization", CacheManager.GetCommonText("OTLOGREPORT", oCurrentEmployee.Language, "Organization")));
            ReportParam.Add(new ReportParameter("Description", CacheManager.GetCommonText("OTLOGREPORT", oCurrentEmployee.Language, "Description")));
            ReportParam.Add(new ReportParameter("EmployeeID", CacheManager.GetCommonText("OTLOGREPORT", oCurrentEmployee.Language, "EmployeeID")));
            ReportParam.Add(new ReportParameter("Position", CacheManager.GetCommonText("OTLOGREPORT", oCurrentEmployee.Language, "Position")));
            ReportParam.Add(new ReportParameter("OTWorkType", CacheManager.GetCommonText("OTLOGREPORT", oCurrentEmployee.Language, "OTWORKTYPE")));

            // ข้อมูล Report OT Log
            DataSet ds = new DataSet("DSOTLog");
            DataTable oTable = new DataTable("OTLogTableByAdmin");
            oTable = (new DailyOTLog()).ToADODataTable(true);
            oTable.Columns.Add("StatusText");
            oTable.Columns.Add("PeriodText");
            oTable.Columns.Add("EmployeeName");
            oTable.Columns.Add("EmployeePosition");
            oTable.Columns.Add("EmployeeOrganization");
            oTable.Columns.Add("OTWorkTypeDesc");
            oTable.TableName = "OTLogTableByAdmin";
            DateTime BeginDate = DateTime.ParseExact(startDate, "yyyy-MM-dd", oCL);
            DateTime EndDate = DateTime.ParseExact(endDate, "yyyy-MM-dd", oCL);
            EndDate = EndDate.AddDays(1).AddSeconds(-1);

            string strStatus = list_status; // List สถานะ
            // strStatus = strStatus.Substring(0, strStatus.LastIndexOf(','));
            list_orgtree = list_orgtree.Replace(',', '|');
            list_orgtree = list_orgtree.Substring(0, list_orgtree.LastIndexOf('|')); // List Org

            DataRow dr;
            foreach (string strOrgID in list_orgtree.Split('|'))
            {
                //foreach (EmployeeData emp in OMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                //    .GetEmployeeByOrg(strOrgID, DateTime.Now))
                foreach (EmployeeData emp in OMManagement.CreateInstance(sCompanyCode)
                    .GetEmployeeByOrg(strOrgID, DateTime.Now))
                {
                    //foreach (DailyOTLog data in HRTMManagement.CreateInstance(oRequestParameter.CurrentEmployee.CompanyCode)
                    //    .LoadOTLogByCriteria(emp.EmployeeID, strStatus, BeginDate, EndDate))
                    foreach (DailyOTLog data in HRTMManagement.CreateInstance(sCompanyCode)
                    .LoadOTLogByCriteria(emp.EmployeeID, strStatus, BeginDate, EndDate))
                    {
                        dr = oTable.NewRow();
                        data.LoadDataToTableRow(dr);
                        dr["StatusText"] = CacheManager.GetCommonText("DailyOTLogStatus", oCurrentEmployee.Language, data.Status.ToString());
                        dr["PeriodText"] = string.Format("{0} - {1}", data.BeginDate.ToString("dd/MM/yyyy HH:mm", oCL), data.EndDate.ToString("dd/MM/yyyy HH:mm", oCL));
                        dr["EmployeeName"] = emp.AlternativeName(oCurrentEmployee.Language);
                        dr["EmployeePosition"] = oCurrentEmployee.Language.Equals("EN") ? emp.OrgAssignment.PositionData.TextEn : emp.OrgAssignment.PositionData.Text;
                        dr["EmployeeOrganization"] = emp.OrgAssignment.OrgUnitData.Text;
                        if (!string.IsNullOrEmpty(data.OTWorkTypeID))
                            dr["OTWorkTypeDesc"] = CacheManager.GetCommonText("HRTMOT", oCurrentEmployee.Language, "OTWORKTYPE_" + data.OTWorkTypeID);
                        else
                            dr["OTWorkTypeDesc"] = string.Empty;
                        oTable.Rows.Add(dr);
                    }
                }
            }

            ds.Tables.Add(oTable);

            string strExtFile = string.Empty;
            strExtFile = typeReport;

            ReportObject oReportObject = new ReportObject();
            oReportObject.ExportFileName = System.Web.HttpContext.Current.Server.MapPath("~/Client/Report/OTLogReportForAdmin" + oCurrentEmployee.EmployeeID);

            ReportViewer rptViewer = new ReportViewer();
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("DSOTLog_OTLogTableByAdmin", ds.Tables[0]));
            rptViewer.LocalReport.ReportPath = System.Web.HttpContext.Current.Server.MapPath("~/Views/Report/HR/TM/" + (typeReport == "PDF" ? "VersionPDF/" : "") + "rptOTReportForAdmin.rdlc");
            rptViewer.LocalReport.SetParameters(ReportParam);
            rptViewer.ShowRefreshButton = false;
            rptViewer.LocalReport.EnableExternalImages = true;


            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string filenameExtension;

            byte[] bytes = rptViewer.LocalReport.Render(
                strExtFile, null, out mimeType, out encoding, out filenameExtension,
                out streamids, out warnings);

            oReportObject.Warnings = warnings;
            oReportObject.Streamids = streamids;
            oReportObject.ContentType = mimeType;
            oReportObject.Encoding = encoding;
            oReportObject.FilenameExtension = filenameExtension;
            oReportObject.ExportFileName = string.Format("{0}.{1}", oReportObject.ExportFileName, filenameExtension);
            FileManager.SaveFile(oReportObject.ExportFileName, bytes, true);
        }

        [HttpPost]
        public void ExportReportClockinLateClockoutEarly(RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            string list_orgtree = oRequestParameter.InputParameter["ListOrgTree"].ToString();
            string startDate = oRequestParameter.InputParameter["StartDate"].ToString();
            string endDate = oRequestParameter.InputParameter["EndDate"].ToString();
            string typeReport = oRequestParameter.InputParameter["ExportTye"].ToString();

            DateTime cv_startDate = DateTime.ParseExact(startDate, "yyyy-MM-dd", oCL);
            DateTime cv_endDate = DateTime.ParseExact(endDate, "yyyy-MM-dd", oCL);

            DataTable oTable = new DataTable();
            oTable.Columns.Add(new DataColumn("EmployeeID", Type.GetType("System.String")));
            oTable.Columns.Add(new DataColumn("EmployeeName", Type.GetType("System.String")));
            oTable.Columns.Add(new DataColumn("OrganizationID", Type.GetType("System.String")));
            oTable.Columns.Add(new DataColumn("OrganizationName", Type.GetType("System.String")));
            oTable.Columns.Add(new DataColumn("TextCode", Type.GetType("System.String")));
            oTable.Columns.Add(new DataColumn("Summary", Type.GetType("System.Decimal")));
            oTable.Columns.Add(new DataColumn("Position", Type.GetType("System.String")));
            string strOrgName = string.Empty;
            DataRow oRow;
            DataTable dtData = new DataTable();

            list_orgtree = list_orgtree.Replace(',', '|');
            list_orgtree = list_orgtree.Substring(0, list_orgtree.LastIndexOf('|')); // List Org

            foreach (string strOrgID in list_orgtree.Split('|'))
            {
                try
                {
                    strOrgName = OMManagement.CreateInstance(sCompanyCode).GetObjectData(ObjectType.O, strOrgID, cv_startDate, oCurrentEmployee.Language).Text;
                    List<EmployeeData> EmployeeList = OMManagement.CreateInstance(sCompanyCode).GetEmployeeByOrg(strOrgID, cv_startDate, cv_endDate, sCompanyCode);
                    foreach (EmployeeData oEmployee in EmployeeList)
                    {
                        dtData.Clear();
                        dtData = HRTMManagement.CreateInstance(sCompanyCode).GetClockinLateClockoutEarly(oEmployee.EmployeeID, cv_startDate, cv_endDate);
                        foreach (DataRow row in dtData.Rows)
                        {
                            oRow = oTable.NewRow();
                            oRow["EmployeeID"] = oEmployee.EmployeeID;
                            oRow["EmployeeName"] = oEmployee.Name;
                            oRow["OrganizationID"] = strOrgID;
                            oRow["OrganizationName"] = strOrgName;
                            oRow["TextCode"] = row["TextCode" + oCurrentEmployee.Language].ToString();
                            oRow["Summary"] = Convert.ToDecimal(row["Summary"]);
                            if (oEmployee.ActionOfPosition == null)
                                oRow["Position"] = "";
                            else
                                oRow["Position"] = oEmployee.ActionOfPosition.Text;
                            oTable.Rows.Add(oRow);
                        }
                    }
                }
                catch
                {

                }
                
            }

            List<ReportParameter> ReportParam = new List<ReportParameter>();
            string MonthText = string.Format("{0} {1}", CacheManager.GetCommonText("REPORTATTENDANCE", oCurrentEmployee.Language, "MONTHFROM"), CacheManager.GetCommonText("MONTH", oCurrentEmployee.Language, cv_startDate.Month.ToString("00")));
            if (cv_startDate.Month != cv_endDate.Month)
                MonthText += string.Format(" {0} {1}", CacheManager.GetCommonText("REPORTATTENDANCE", oCurrentEmployee.Language, "MONTHTO"), CacheManager.GetCommonText("MONTH", oCurrentEmployee.Language, cv_endDate.Month.ToString("00")));
            ReportParam.Add(new ReportParameter("ReportTitle", CacheManager.GetCommonText("REPORTATTENDANCE", oCurrentEmployee.Language, "REPORTTITLE")));
            ReportParam.Add(new ReportParameter("MonthText", MonthText));
            ReportParam.Add(new ReportParameter("Col_EmployeeID", CacheManager.GetCommonText("REPORTATTENDANCE", oCurrentEmployee.Language, "COL_EMPLOYEEID")));
            ReportParam.Add(new ReportParameter("Col_EmployeeName", CacheManager.GetCommonText("REPORTATTENDANCE", oCurrentEmployee.Language, "COL_EMPLOYEENAME")));
            ReportParam.Add(new ReportParameter("Col_Position", CacheManager.GetCommonText("REPORTATTENDANCE", oCurrentEmployee.Language, "COL_POSITION")));
            ReportParam.Add(new ReportParameter("Summary", CacheManager.GetCommonText("REPORTATTENDANCE", oCurrentEmployee.Language, "SUMMARY")));
            ReportParam.Add(new ReportParameter("Oraganization", CacheManager.GetCommonText("REPORTATTENDANCE", oCurrentEmployee.Language, "ORGANIZATION")));
            ReportParam.Add(new ReportParameter("Col_Signature", CacheManager.GetCommonText("REPORTATTENDANCE", oCurrentEmployee.Language, "COL_SIGNATURE")));

            string strExtFile = string.Empty;
            strExtFile = typeReport;

            ReportObject oReportObject = new ReportObject();
            oReportObject.ExportFileName = System.Web.HttpContext.Current.Server.MapPath("~/Client/Report/ClockinLateClockoutEarlyReport" + oCurrentEmployee.EmployeeID);

            ReportViewer rptViewer = new ReportViewer();
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("ClockinLateClockoutEarlyReport_ClockinLateClockoutEarlyReport", oTable));
            rptViewer.LocalReport.ReportPath = System.Web.HttpContext.Current.Server.MapPath("~/Views/Report/HR/TM/" + (typeReport == "PDF" ? "VersionPDF/" : "") + "rptClockinLateClockoutEarly.rdlc");
            rptViewer.LocalReport.SetParameters(ReportParam);
            rptViewer.ShowRefreshButton = false;
            rptViewer.LocalReport.EnableExternalImages = true;

            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string filenameExtension;

            byte[] bytes = rptViewer.LocalReport.Render(
                strExtFile, null, out mimeType, out encoding, out filenameExtension,
                out streamids, out warnings);

            oReportObject.Warnings = warnings;
            oReportObject.Streamids = streamids;
            oReportObject.ContentType = mimeType;
            oReportObject.Encoding = encoding;
            oReportObject.FilenameExtension = filenameExtension;
            oReportObject.ExportFileName = string.Format("{0}.{1}", oReportObject.ExportFileName, filenameExtension);
            FileManager.SaveFile(oReportObject.ExportFileName, bytes, true);
        }

        [HttpPost]
        public ClientReportGroupAdmin GetGroupReportAdmin(RequestParameter oRequestParameter)
        {
            ClientReportGroupAdmin group_report = new ClientReportGroupAdmin();

            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            string str_groupId = oRequestParameter.InputParameter["ReportGroupId"].ToString();
            int groupId = Convert.ToInt16(str_groupId);

            List<ReportGroupAdmin> result = HRTMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).LoadReportAdminGroup(groupId);

            if (result.Count > 0)
            {
                for (int i = 0; i < result.Count; i++)
                {
                    if (i % 2 == 0)
                        group_report.LeftReport.Add(result[i]);
                    else
                        group_report.RightReport.Add(result[i]);
                }
            }

            return group_report;
        }

        [HttpPost]
        public GroupSetupSystemForAdmin GetGroupSetupForAdmin(RequestParameter oRequestParameter)
        {
            GroupSetupSystemForAdmin group_setup = new GroupSetupSystemForAdmin();

            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            string str_groupId = oRequestParameter.InputParameter["ReportGroupId"].ToString();
            int groupId = Convert.ToInt16(str_groupId);

            List<ReportGroupAdmin> result = HRTMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).LoadReportAdminGroup(groupId);

            if (result.Count > 0)
            {
                for (int i = 0; i < result.Count; i++)
                {
                    if (i % 2 == 0)
                        group_setup.LeftSystem.Add(result[i]);
                    else
                        group_setup.ReightSystem.Add(result[i]);
                }
            }
            return group_setup;
        }

        [HttpPost]
        public List<AbsenceType> GetAbsenceTypeListAll(RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();

            var list_absence = HRTMManagement.CreateInstance(sCompanyCode)
                .GetAbsenceTypeList(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.Language);

            return list_absence;
        }

        [HttpPost]
        public void ExportAbsenceReportByType(RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            string list_orgtree = oRequestParameter.InputParameter["ListOrgTree"].ToString();
            string absence_all = oRequestParameter.InputParameter["ListAbsence"].ToString();
            string str_begin_date = oRequestParameter.InputParameter["StartDate"].ToString();
            string str_end_date = oRequestParameter.InputParameter["EndDate"].ToString();
            string typeReport = oRequestParameter.InputParameter["ExportTye"].ToString();

            if (string.IsNullOrEmpty(list_orgtree))
            {
                throw new DataServiceException("HRTM_EXCEPTION", "EXEPTION_ORG_NULL");
            }

            if (string.IsNullOrEmpty(absence_all))
            {
                throw new DataServiceException("ABSATTREPORT", "ABSENCE_TYPE_IS_NOT_SELECTED");
            }

            // หา Employee
            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);

            list_orgtree = list_orgtree.Replace(',', '|');
            list_orgtree = list_orgtree.Substring(0, list_orgtree.LastIndexOf('|')); // List Org

            DateTime cv_startDate = DateTime.ParseExact(str_begin_date, "yyyy-MM-dd", oCL);
            DateTime cv_endDate = DateTime.ParseExact(str_end_date, "yyyy-MM-dd", oCL);

            DataSet ds = HRTMManagement.CreateInstance(sCompanyCode).GenerateAbsenceReportDataSet(list_orgtree, absence_all, cv_startDate, cv_endDate, oEmp);

            #region Set parameters to report
            List<ReportParameter> ReportParam = new List<ReportParameter>();
            ReportParam.Add(new ReportParameter("Sequence", CacheManager.GetCommonText("ABSATTREPORT", oCurrentEmployee.Language, "SEQUENCE")));
            ReportParam.Add(new ReportParameter("EmployeeID", CacheManager.GetCommonText("ABSATTREPORT", oCurrentEmployee.Language, "EMPLOYEEID")));
            ReportParam.Add(new ReportParameter("ReportTitle", CacheManager.GetCommonText("ABSATTREPORT", oCurrentEmployee.Language, "RPTABSENCE")));
            ReportParam.Add(new ReportParameter("Duration", CacheManager.GetCommonText("ABSATTREPORT", oCurrentEmployee.Language, "DURATION")));
            ReportParam.Add(
                new ReportParameter("DurationDate"
                , DateTime.ParseExact(str_begin_date, "yyyy-MM-dd", oCL).ToString("dd/MM/yyyy", oCL) + " - " + DateTime.ParseExact(str_end_date, "yyyy-MM-dd", oCL).ToString("dd/MM/yyyy", oCL)));
            ReportParam.Add(new ReportParameter("EmployeeName", CacheManager.GetCommonText("ABSATTREPORT", oCurrentEmployee.Language, "EMPLOYEENAME")));
            ReportParam.Add(new ReportParameter("AType", CacheManager.GetCommonText("ABSATTREPORT", oCurrentEmployee.Language, "ABSENCETYPE")));
            ReportParam.Add(new ReportParameter("AType", CacheManager.GetCommonText("ABSATTREPORT", oCurrentEmployee.Language, "ATTENDANCETYPE")));
            ReportParam.Add(new ReportParameter("Period", CacheManager.GetCommonText("ABSATTREPORT", oCurrentEmployee.Language, "PERIOD")));
            ReportParam.Add(new ReportParameter("Amount", CacheManager.GetCommonText("ABSATTREPORT", oCurrentEmployee.Language, "AMOUNT")));
            ReportParam.Add(new ReportParameter("Org", CacheManager.GetCommonText("ABSATTREPORT", oCurrentEmployee.Language, "ORGANIZATION")));
            #endregion

            string strExtFile = string.Empty;
            strExtFile = typeReport;

            ReportObject oReportObject = new ReportObject();
            oReportObject.ExportFileName = System.Web.HttpContext.Current.Server.MapPath("~/Client/Report/rptAbsence" + oCurrentEmployee.EmployeeID);

            ReportViewer rptViewer = new ReportViewer();
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("DSAbsenceAttendance_tbAbsence", ds.Tables[0]));
            rptViewer.LocalReport.ReportPath = System.Web.HttpContext.Current.Server.MapPath("~/Views/Report/HR/TM/" + (typeReport == "PDF" ? "VersionPDF/" : "") + "rptAbsence.rdlc");
            rptViewer.LocalReport.SetParameters(ReportParam);
            rptViewer.ShowRefreshButton = false;
            rptViewer.LocalReport.EnableExternalImages = true;


            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string filenameExtension;

            byte[] bytes = rptViewer.LocalReport.Render(
                strExtFile, null, out mimeType, out encoding, out filenameExtension,
                out streamids, out warnings);

            oReportObject.Warnings = warnings;
            oReportObject.Streamids = streamids;
            oReportObject.ContentType = mimeType;
            oReportObject.Encoding = encoding;
            oReportObject.FilenameExtension = filenameExtension;
            oReportObject.ExportFileName = string.Format("{0}.{1}", oReportObject.ExportFileName, filenameExtension);
            FileManager.SaveFile(oReportObject.ExportFileName, bytes, true);
        }

        #region รายงานสรุปวันลาป่วยและจำนวนพนักงานที่ลาป่วย ตามเงื่อนไขการเลือกดูข้อมูลคือ ปี และพื้นที่ปฏิบัติงาน

        [HttpPost]
        public List<string> GetListYearConfigSummaryAbsenceWorkLocation(RequestParameter oRequestParameter)
        {
            List<string> list_year = new List<string>();
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            list_year = HRTMManagement.CreateInstance(sCompanyCode).GetListYearConfigSummaryAbsenceWorkLocation();
            return list_year;
        }

        [HttpPost]
        public HttpResponseMessage ExportSummaryAbsenceWorkLocationByAdmin(RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            string sYear = oRequestParameter.InputParameter["Year"].ToString();

            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);

            var workbook = HRTMManagement.CreateInstance(sCompanyCode).ExportReportSummaryAbsenceInLocationByAdmin(sYear,oEmp);
            var stream = new MemoryStream();
            workbook.SaveAs(stream);
            stream.Position = 0;

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(stream.ToArray())
            };

            DateTime date_now = DateTime.Now;
            result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = "ReportOTLogByHour" + date_now.Year + date_now.Month + date_now.Date + date_now.Minute + date_now.Second + ".xlsx"
            };
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

            return result;
        }

        #endregion

        #region รายงานสรุปจำนวนพนักงานที่เบิกค่าล่วงเวลาแบ่งตามจำนวนชั่วโมง

        [HttpPost]
        public List<string> GetListYearOTLogByHour(RequestParameter oRequestParameter)
        {
            List<string> list_year = new List<string>();
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            list_year = HRTMManagement.CreateInstance(sCompanyCode).GetListYearConfigOTLogByHour();
            return list_year;
        }

        [HttpPost]
        public HttpResponseMessage ExportReportOTLogByHour(RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            string sYear = oRequestParameter.InputParameter["Year"].ToString();

            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);

            var workbook = HRTMManagement.CreateInstance(sCompanyCode).CalculateOTLogByHour(sYear);
            var stream = new MemoryStream();
            workbook.SaveAs(stream);
            stream.Position = 0;

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(stream.ToArray())
            };

            DateTime date_now = DateTime.Now;
            result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = "ReportOTLogByHour" + date_now.Year + date_now.Month + date_now.Date + date_now.Minute + date_now.Second + ".xlsx"
            };
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

            return result;
        }
        #endregion

        #region OT Dashbaord

        [HttpPost]
        public List<OrgUnitTree> GetAllOrgUnitByRole(RequestParameter oRequestParameter)
        {
            List<OrgUnitTree> list_org = new List<OrgUnitTree>();

            string sYear = oRequestParameter.InputParameter["sYear"].ToString();
            string eYear = oRequestParameter.InputParameter["eYear"].ToString();

            OrgUnitTree org_all = new OrgUnitTree()
            {
                OrgDisplay = oRequestParameter.Requestor.Language.ToLower() == "th" ? "ทั้งหมด" : "All",
                OrgUnit = "0",
                OrgLevel = "",
                OrgPath = "",
                OrgName = oRequestParameter.Requestor.Language.ToLower() == "th" ? "ทั้งหมด" : "All",
                ParentOrgUnit = "",
                OrgFullName = oRequestParameter.Requestor.Language.ToLower() == "th" ? "ทั้งหมด" : "All",
                OrgUnitPath = ""
            };
            list_org.Add(org_all);


            var data_list_org = HRTMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
                .GetListOTOrgTree(oRequestParameter.Requestor.EmployeeID, Convert.ToInt32(sYear), Convert.ToInt32(eYear));
            list_org.AddRange(data_list_org);


            //string manager = null;
            //string time_admin = null;

            //// 1. ไม่ต้องเอา Role แล้ว
            //manager = oRequestParameter.Requestor.UserRoles.Where(s => s == "#MANAGER").FirstOrDefault();
            //time_admin = oRequestParameter.Requestor.UserRoles.Where(s => s == "TIMEADMIN").FirstOrDefault();

            //if(!string.IsNullOrEmpty(manager) || !string.IsNullOrEmpty(time_admin))
            //{
            //    if (!string.IsNullOrEmpty(time_admin))
            //        list_org = HRTMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
            //            .GetListOrgUnitByUserRole(oRequestParameter.Requestor.EmployeeID, time_admin, oRequestParameter.Requestor.CompanyCode);
            //    else
            //        list_org = HRTMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
            //            .GetListOrgUnitByUserRole(oRequestParameter.Requestor.EmployeeID, manager, oRequestParameter.Requestor.CompanyCode);
            //}

            return list_org;
        }

        [HttpPost]
        public DashbaordYTDOvertimeandSalary GetYTDOvertimeAndSalary(RequestParameter oRequestParameter)
        {
            var dashboard_YTDOvertimeandSalary = new DashbaordYTDOvertimeandSalary();

            string list_org = oRequestParameter.InputParameter["ListOrg"].ToString();
            string[] arr_org = list_org.Split(',');
            if (arr_org.Length > 0)
            {
                var distinct_org = arr_org.Distinct().ToArray();
                dashboard_YTDOvertimeandSalary = HRTMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
                    .GetDashbaordYTDOvertimeandSalary(distinct_org);
            }

            return dashboard_YTDOvertimeandSalary;
        }

        [HttpPost]
        public List<string> GetYearDashboard2(RequestParameter oRequestParameter)
        {
            return HRTMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetListYearConfigDashbaordTab2();
        }

        [HttpPost]
        public DashboardOTReason GetDashboardOTReason(RequestParameter oRequestParameter)
        {
            DashboardOTReason result = new DashboardOTReason();

            string list_org = oRequestParameter.InputParameter["ListOrg"].ToString();
            string sYear = oRequestParameter.InputParameter["sYear"].ToString();
            string sMonth = oRequestParameter.InputParameter["sMonth"].ToString();

            string[] arr_org = list_org.Split(',');

            if(arr_org.Count() > 0)
            {
                var distinct_org = arr_org.Distinct().ToArray();
                result = HRTMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
                    .GetDashboardOTReason(distinct_org, Convert.ToInt16(sYear), Convert.ToInt16(sMonth));
            }

            return result;
        }

        [HttpPost]
        public DashboardOvertimebyBusinessUnitinmonth DashboardOvertimebyBusinessUnitinmonth(RequestParameter oRequestParameter)
        {
            var dashboard = new DashboardOvertimebyBusinessUnitinmonth();

            string list_org = oRequestParameter.InputParameter["ListOrg"].ToString();
            string sYear = oRequestParameter.InputParameter["sYear"].ToString();
            string sMonth = oRequestParameter.InputParameter["sMonth"].ToString();
            string selectOrg = oRequestParameter.InputParameter["OrgSelect"].ToString();
            string[] arr_org = list_org.Split(',');
            if (arr_org.Count() > 0)
            {
                var distinct_org = arr_org.Distinct().ToArray();

                if(selectOrg == "0")
                {
                    if (oRequestParameter.Requestor.UserRoles.Contains("TIMEADMIN"))
                        selectOrg = "23300075";
                    else
                        selectOrg = oRequestParameter.Requestor.OrgUnit;
                }

                dashboard = HRTMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
                    .GetDashboardOvertimebyBusinessUnitinmonth(arr_org
                    , Convert.ToInt16(sYear)
                    , Convert.ToInt16(sMonth)
                    , selectOrg
                    , oRequestParameter.Requestor.EmployeeID
                    );
            }

            return dashboard;
        }

        [HttpPost]
        public DashbaordOvertimeAndSalaryByMonth GetDashbaordOvertimeAndSalaryByMonth(RequestParameter oRequestParameter)
        {
            var dashbaord_OvertimeAndSalaryByMonth = new DashbaordOvertimeAndSalaryByMonth();

            string list_org = oRequestParameter.InputParameter["ListOrg"].ToString();
            string sYear = oRequestParameter.InputParameter["sYear"].ToString();
            string[] arr_org = list_org.Split(',');
            if (arr_org.Count() > 0)
            {
                var distinct_org = arr_org.Distinct().ToArray();
                dashbaord_OvertimeAndSalaryByMonth = HRTMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
                    .GetDashbaordOvertimeAndSalaryByMonth(distinct_org, Convert.ToInt16(sYear));
            }

            return dashbaord_OvertimeAndSalaryByMonth;
        }

        [HttpPost]
        public DashbaordOTSalary6Months GetDashbaordOTSalary6Months(RequestParameter oRequestParameter)
        {
            var dashboard = new DashbaordOTSalary6Months();

            string list_org = oRequestParameter.InputParameter["ListOrg"].ToString();
            string sYear = oRequestParameter.InputParameter["sYear"].ToString();
            string sMonth = oRequestParameter.InputParameter["sMonth"].ToString();
            string[] arr_org = list_org.Split(',');
            if (arr_org.Count() > 0)
            {
                var distinct_org = arr_org.Distinct().ToArray();
                dashboard = HRTMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
                    .GetDashbaordOTSalary6Months(arr_org, Convert.ToInt16(sYear), Convert.ToInt16(sMonth));
            }

            return dashboard;
        }

        [HttpPost]
        public DashbaordOTHours6Months GetDashbaordOTHours6Months(RequestParameter oRequestParameter)
        {
            var dashboard = new DashbaordOTHours6Months();

            string list_org = oRequestParameter.InputParameter["ListOrg"].ToString();
            string sYear = oRequestParameter.InputParameter["sYear"].ToString();
            string sMonth = oRequestParameter.InputParameter["sMonth"].ToString();
            string[] arr_org = list_org.Split(',');
            if (arr_org.Count() > 0)
            {
                var distinct_org = arr_org.Distinct().ToArray();
                dashboard = HRTMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
                    .GetDashbaordOTHours6Months(arr_org, Convert.ToInt16(sYear), Convert.ToInt16(sMonth));
            }

            return dashboard;
        }

        [HttpPost]
        public List<string> GetYearDashboard3(RequestParameter oRequestParameter)
        {
            return HRTMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetListYearConfigDashbaordTab3();
        }

        [HttpPost]
        public DashboardSummarySalaryOvertimePayByYear GetDashboardSummarySalaryOvertimePayByYear(RequestParameter oRequestParameter)
        {
            var dashboard = new DashboardSummarySalaryOvertimePayByYear();

            string list_org = oRequestParameter.InputParameter["ListOrg"].ToString();
            string beginYear = oRequestParameter.InputParameter["beginYear"].ToString();
            string endYear = oRequestParameter.InputParameter["endYear"].ToString();
            string[] arr_org = list_org.Split(',');
            if (arr_org.Count() > 0)
            {
                var distinct_org = arr_org.Distinct().ToArray();
                dashboard = HRTMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetDashboardSummarySalaryOvertimePayByYear(arr_org, Convert.ToInt16(beginYear), Convert.ToInt16(endYear));
            }

            return dashboard;
        }

        [HttpPost]
        public DashboardSummarySalaryOvertimeHourByYear GetDashboardSummarySalaryOvertimeHourByYear(RequestParameter oRequestParameter)
        {
            var dashboard = new DashboardSummarySalaryOvertimeHourByYear();

            string list_org = oRequestParameter.InputParameter["ListOrg"].ToString();
            string beginYear = oRequestParameter.InputParameter["beginYear"].ToString();
            string endYear = oRequestParameter.InputParameter["endYear"].ToString();
            string[] arr_org = list_org.Split(',');
            if (arr_org.Count() > 0)
            {
                var distinct_org = arr_org.Distinct().ToArray();
                dashboard = HRTMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).GetDashboardSummarySalaryOvertimeHourByYear(arr_org, Convert.ToInt16(beginYear), Convert.ToInt16(endYear));
            }

            return dashboard;
        }

        [HttpPost]
        public OTEmployeeByLevel GetDashboardOTEmployeebyLevel(RequestParameter oRequestParameter)
        {
            var dashboard = new OTEmployeeByLevel();

            string list_org = oRequestParameter.InputParameter["ListOrg"].ToString();
            string beginYear = oRequestParameter.InputParameter["beginYear"].ToString();
            string endYear = oRequestParameter.InputParameter["endYear"].ToString();
            string[] arr_org = list_org.Split(',');
            if (arr_org.Count() > 0)
            {
                var distinct_org = arr_org.Distinct().ToArray();
                dashboard = HRTMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
                    .GetDashboardOTEmployeeByLevel(arr_org, Convert.ToInt16(beginYear), Convert.ToInt16(endYear));
            }

            return dashboard;
        }

        [HttpPost]
        public OTEmployeeByBath GetDashboardOTEmployeebyBath(RequestParameter oRequestParameter)
        {
            var dashboard = new OTEmployeeByBath();

            string list_org = oRequestParameter.InputParameter["ListOrg"].ToString();
            string beginYear = oRequestParameter.InputParameter["beginYear"].ToString();
            string endYear = oRequestParameter.InputParameter["endYear"].ToString();
            string[] arr_org = list_org.Split(',');
            if (arr_org.Count() > 0)
            {
                var distinct_org = arr_org.Distinct().ToArray();
                dashboard = HRTMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
                    .GetDashboardOTEmployeeByBath(arr_org, Convert.ToInt16(beginYear), Convert.ToInt16(endYear));
            }

            return dashboard;
        }

        #endregion

        [HttpPost]
        public HolidayMaxMin GetHolidaysByMonthEMployee(RequestParameter oRequestParameter)
        {
            // Holiday รายเดือน
            HolidayMaxMin list_holiday = new HolidayMaxMin();

            int year = Convert.ToInt16(oRequestParameter.InputParameter["Year"]);
            int month = Convert.ToInt16(oRequestParameter.InputParameter["Month"]);

            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            EmployeeData oEmp = new EmployeeData(oRequestParameter.Requestor.EmployeeID, oRequestParameter.Requestor.PositionID, oRequestParameter.Requestor.CompanyCode, DateTime.Now);

            list_holiday = HRTMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
                .GetHolidayByMonthYear(oEmp, DateTime.Now, year, month);

            return list_holiday;
        }

        [HttpPost]
        public HolidayMaxMin GetHolidaysByMonthEmployeeSubstitute(RequestParameter oRequestParameter)
        {
            // Holiday รายเดือน
            HolidayMaxMin list_holiday = new HolidayMaxMin();

            string emp = oRequestParameter.InputParameter["Employee"].ToString();
            int year = Convert.ToInt16(oRequestParameter.InputParameter["Year"]);
            int month = Convert.ToInt16(oRequestParameter.InputParameter["Month"]);

            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            EmployeeData oEmp = new EmployeeData(emp,DateTime.Now);

            list_holiday = HRTMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode)
                .GetHolidayByMonthYear(oEmp, DateTime.Now, year, month);

            return list_holiday;
        }


        #region Code Old
        // [HttpPost]
        // public List<AbsenceQuotaByEmployee> GetAbsenceQuotaByEmployeeID([FromBody] EmployeeDataForMobile CurrentEmployee)
        // {
        //     try
        //     {
        //         WorkflowController wfCtlr = new WorkflowController();
        //         wfCtlr.SetAuthenticate(CurrentEmployee);

        //         ESS.EMPLOYEE.EmployeeData employeeData = new ESS.EMPLOYEE.EmployeeData(CurrentEmployee.RequesterEmployeeID);
        //         List<INFOTYPE2006> _absQuotaList = INFOTYPE2006.GetData(employeeData);

        //         List<AbsenceQuotaByEmployee> absQuotaList = (from row in _absQuotaList.AsQueryable()
        //                                                      select new AbsenceQuotaByEmployee
        //                                                                          {
        //                                                                              AbsenceType = CacheManager.GetCommonText("AbsenceQuotaType", CurrentEmployee.Language, row.AbsenceType),
        //                                                                              DeductionStart = row.DeductionStart,
        //                                                                              DeductionEnd = row.DeductionEnd,
        //                                                                              QuotaAmount = row.QuotaAmount,
        //                                                                              InprocessAmont = row.InprocessAmount,
        //                                                                              UsedAmount = row.UsedAmount,
        //                                                                              Remaining = row.QuotaAmount - row.UsedAmount - row.InprocessAmount
        //                                                                          }).ToList();
        //         return absQuotaList;
        //     }
        //     catch (Exception ex)
        //     {
        //         throw ex;
        //     }
        // }

        // [HttpPost]
        // /*
        // Param1:  EmployeeID
        // Param1:  oLanguage
        // */
        // public List<iSSWS.Models.AbsenceType> GetAbsenceTypeByEmployeeID([FromBody] EmployeeDataForMobile CurrentEmployee)
        // {
        //     try
        //     {
        //         var _absTypeList = ESS.HR.TM.CONFIG.AbsenceType.GetAbsenceTypeList(CurrentEmployee.RequesterEmployeeID, CurrentEmployee.Language);

        //         List<iSSWS.Models.AbsenceType> absTypeList = (from row in _absTypeList.AsQueryable()
        //                                                       select new iSSWS.Models.AbsenceType
        //                                                      {
        //                                                          AbsAttGrouping = row.AbsAttGrouping,
        //                                                          Key = row.Key,
        //                                                          Description = row.Description,
        //                                                          QuotaDayCount = row.QuotaDayCount,
        //                                                          CountingRule = row.CountingRule,
        //                                                          UseQuota = row.IsUseQuota,
        //                                                          MonitorType = row.MonitorType,
        //                                                          AllDayFlag = AllDayFlag(CurrentEmployee, row.Key)
        //                                                      }).ToList();
        //         return absTypeList;
        //     }
        //     catch
        //     {
        //         return null;
        //     }
        // }

        // private bool AllDayFlag(EmployeeDataForMobile CurrentEmployee, string AbsenceType)
        // {
        //     AbsenceCreatingRule oACR = AbsenceCreatingRule.GetCreatingRule(CurrentEmployee.RequesterEmployeeID, AbsenceType, 0);
        //     if (oACR.AbsenceInterval == 1 || oACR.AbsenceInterval == 2)
        //         return false;
        //     else
        //         return true;
        // }

        // [HttpPost]
        // public List<AbsenceListByEmployee> GetAbsenceListByEmployeeID(int Param1, [FromBody] EmployeeDataForMobile CurrentEmployee)
        // {
        //     try
        //     {
        //         WorkflowController wfCtlr = new WorkflowController();
        //         wfCtlr.SetAuthenticate(CurrentEmployee);

        //         DateTime BeginDate = new DateTime(Param1, 1, 1);
        //         DateTime EndDate = BeginDate.AddYears(1).AddDays(-1);

        //         ESS.EMPLOYEE.EmployeeData employeeData = new ESS.EMPLOYEE.EmployeeData(CurrentEmployee.RequesterEmployeeID, CurrentEmployee.RequesterPositionID);//(CurrentEmployee.EmployeeID);
        //         List<INFOTYPE2001> _absenceList = INFOTYPE2001.GetData(employeeData, BeginDate, EndDate);

        //         List<AbsenceListByEmployee> absenceList = (from row in _absenceList.AsQueryable()
        //                                                    select new AbsenceListByEmployee
        //                                                     {
        //                                                         AbsenceType = row.AbsenceType,
        //                                                         AbsenceTypeDesc = CacheManager.GetAbsenceTypeName(CurrentEmployee.RequesterEmployeeID, row.AbsenceType, CurrentEmployee.Language),
        //                                                         RequestNo = row.RequestNo,
        //                                                         BeginDate = row.BeginDate,
        //                                                         EndDate = row.EndDate,
        //                                                         DateStr = row.DateTimeText,
        //                                                         BeginTime = row.BeginTime.ToString(),
        //                                                         EndTime = row.EndTime.ToString(),
        //                                                         AbsenceDays = row.AbsenceDays,
        //                                                         AbsenceHours = row.AbsenceHours,
        //                                                         PayrollDays = row.PayrollDays,
        //                                                         AllDayFlag = row.AllDayFlag,
        //                                                         Status = row.Status,
        //                                                         Remark = row.Remark,
        //                                                         IsRequireDocument = row.IsRequireDocument,
        //                                                         IsMark = row.IsMark,
        //                                                         IsPost = row.IsPost,
        //                                                         IsSkip = row.IsSkip,
        //                                                         Itemkey = string.Format("{0}|{1}|{2}|{3}", CurrentEmployee.RequesterEmployeeID, row.AbsenceType, row.BeginDate.ToString("yyyyMMdd"), row.EndDate.ToString("yyyyMMdd"))
        //                                                     }).ToList();
        //         return absenceList;
        //     }
        //     catch (Exception ex)
        //     {
        //         throw ex;
        //     }
        // }

        // /*
        //Param1:  AbsenceType
        //*/

        // [HttpPost]
        // public string GetAbsenceTypeName(string Param1, [FromBody] EmployeeDataForMobile CurrentEmployee)
        // {
        //     return CacheManager.GetAbsenceTypeName(CurrentEmployee.RequesterEmployeeID, Param1, CurrentEmployee.Language);
        // }

        // [HttpPost]
        // public List<DailyOTByEmployee> GetDailyOTLogByEmployeeID([FromBody] RequestDailyOT oRequestDailyOT)
        // {
        //     List<DailyOTByEmployee> oDailyOTByEmployeeList = new List<DailyOTByEmployee>();
        //     try
        //     {
        //         List<DailyOTLog> oDailyOTLog = HRTMManagement.LoadOTSummaryByRate(oRequestDailyOT.EmployeeId, oRequestDailyOT.BeginDate, oRequestDailyOT.EndDate);
        //         if (oDailyOTLog != null && oDailyOTLog.Count > 0)
        //         {
        //             foreach (DailyOTLog currentItem in oDailyOTLog)
        //             {
        //                 oDailyOTByEmployeeList.Add(Convert<DailyOTByEmployee>.ObjectFrom(currentItem));
        //             }
        //         }
        //     }
        //     catch
        //     {
        //     }
        //     return oDailyOTByEmployeeList;
        // }

        // /*
        // Param1:  AbsenceDate (string Format:yyyy-mm-dd)
        // */

        // [HttpPost]
        // public Dictionary<string, string> DivideDailyWorkSchedule(string Param1, [FromBody] EmployeeDataForMobile CurrentEmployee)
        // {
        //     DateTime AbsenceDate = Convert.ToDateTime(Param1);
        //     EmployeeData oEmployeeData = new EmployeeData(CurrentEmployee.RequesterEmployeeID, CurrentEmployee.PositionID);
        //     return HRTMManagement.DivideDailyWorkScheduleByEmployee(2, false, AbsenceDate, oEmployeeData, CacheManager.GetCommonText("HRTMABSENCE", CurrentEmployee.Language, "FULLDAY"));
        // }

        // /*
        // Param1:  PeriodDate (string Format:yyyy-mm-dd)
        // */

        // [HttpPost]
        // public List<DayTemplate> GetDayTemplateForTimesheet(string Param1, [FromBody] EmployeeDataForMobile CurrentEmployee)
        // {
        //     DateTime PeriodDate = DateTime.Parse(Param1);
        //     WorkflowController wfCtlr = new WorkflowController();
        //     wfCtlr.SetAuthenticate(CurrentEmployee);

        //     Dictionary<string, string> Desc = new Dictionary<string, string>();// PortalEngineManagement.CreateInstance(CurrentEmployee.CompanyCode).GetTextDescription("DAYOFWEEK_ABBV", CurrentEmployee);
        //     Dictionary<string, string> Desc2 = new Dictionary<string, string>();

        //     List<DayTemplate> oReturn = new List<DayTemplate>();
        //     DayTemplate oDayTemplate;

        //     DailyWS dailyWs;
        //     MonthlyWS monthWs = new MonthlyWS();
        //     monthWs = EmployeeManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetCalendar(CurrentEmployee.EmployeeID, PeriodDate.Year, PeriodDate.Month);

        //     DateTime EndDate = PeriodDate;
        //     DateTime Begindate = new DateTime(PeriodDate.Year, PeriodDate.Month, 1);
        //     if (PeriodDate.Day > 15)
        //         Begindate = new DateTime(PeriodDate.Year, PeriodDate.Month, 16);
        //     while (Begindate <= EndDate)
        //     {
        //         oDayTemplate = new DayTemplate();
        //         dailyWs = WorkflowPrinciple.Current.UserSetting.Employee.GetDailyWorkSchedule(Begindate, true);
        //         oDayTemplate.Day_Date = Begindate;
        //         oDayTemplate.Day_Code = Desc2[Begindate.DayOfWeek.ToString().ToUpper()].ToString();
        //         if (dailyWs != null)
        //         {
        //             dailyWs.IsHoliday = monthWs.GetWSCode(Begindate.Date.Day, false) == "1";
        //             oDayTemplate.IsDayOff = dailyWs.IsDayOff;
        //             oDayTemplate.IsHoliday = dailyWs.IsHoliday;
        //         }
        //         oReturn.Add(oDayTemplate);
        //         Begindate = Begindate.AddDays(1);
        //     }

        //     return oReturn;
        // }
        #endregion


        [HttpPost]
        public HttpResponseMessage GetOTTrackingStatusExport([FromBody] RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            string sReportName = oRequestParameter.InputParameter["ReportName"].ToString();
            string sLanguageCode = oRequestParameter.InputParameter["LanguageCode"].ToString();
            string sEmpID = oRequestParameter.InputParameter["Employee_id"].ToString();
            string sEmployeeName = oRequestParameter.InputParameter["EmployeeName"].ToString();
            string sPositionName = oRequestParameter.InputParameter["PositionName"].ToString();
            string sOrgUnitName = oRequestParameter.InputParameter["OrgUnitName"].ToString();
            string sExportType = oRequestParameter.InputParameter["ExportType"].ToString();
            string sByOrg = oRequestParameter.InputParameter["ByOrg"].ToString();
            string sListOrg = oRequestParameter.InputParameter["ListOrg"].ToString();
            DateTime dFromDate = Convert.ToDateTime(oRequestParameter.InputParameter["FromDate"]);
            DateTime dToDate = Convert.ToDateTime(oRequestParameter.InputParameter["ToDate"]);

            DataTable oTableResult = new DataTable();

            if (sByOrg == "2") //แบบระบุพนักงาน
            {
                oTableResult = HRTMManagement.CreateInstance(sCompanyCode).GetOTTrackingStatusReport(dFromDate,dToDate, sEmpID,"", sCompanyCode, sLanguageCode);
            }
            else //แบบเลือกหน่วยงาน TreeList
            {
                oTableResult = HRTMManagement.CreateInstance(sCompanyCode).GetOTTrackingStatusReport(dFromDate, dToDate, "", sListOrg, sCompanyCode, sLanguageCode);             
            }

            var workbook = HRTMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).ExportExcelOTTrackingStatusReport(oTableResult, sLanguageCode);
            var stream = new MemoryStream();
            workbook.SaveAs(stream);
            stream.Position = 0;

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(stream.ToArray())
            };

            DateTime date_now = DateTime.Now;
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = "ReportOTTrackingStatus" + date_now.Year + date_now.Month + date_now.Date + ".xlsx"
            };
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

            return result;
        }

        [HttpPost]
        public HttpResponseMessage GetDutyTrackingStatusExport([FromBody] RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            string sReportName = oRequestParameter.InputParameter["ReportName"].ToString();
            string sLanguageCode = oRequestParameter.InputParameter["LanguageCode"].ToString();
            string sEmpID = oRequestParameter.InputParameter["Employee_id"].ToString();
            string sEmployeeName = oRequestParameter.InputParameter["EmployeeName"].ToString();
            string sPositionName = oRequestParameter.InputParameter["PositionName"].ToString();
            string sOrgUnitName = oRequestParameter.InputParameter["OrgUnitName"].ToString();
            string sExportType = oRequestParameter.InputParameter["ExportType"].ToString();
            string sByOrg = oRequestParameter.InputParameter["ByOrg"].ToString();
            string sListOrg = oRequestParameter.InputParameter["ListOrg"].ToString();
            DateTime dFromDate = Convert.ToDateTime(oRequestParameter.InputParameter["FromDate"]);
            DateTime dToDate = Convert.ToDateTime(oRequestParameter.InputParameter["ToDate"]);

            DataTable oTableResult = new DataTable();

            if (sByOrg == "2") //แบบระบุพนักงาน
            {
                oTableResult = HRTMManagement.CreateInstance(sCompanyCode).GetDutyTrackingStatusReport(dFromDate, dToDate, sEmpID, "", sCompanyCode, sLanguageCode);
            }
            else //แบบเลือกหน่วยงาน TreeList
            {
                oTableResult = HRTMManagement.CreateInstance(sCompanyCode).GetDutyTrackingStatusReport(dFromDate, dToDate, "", sListOrg, sCompanyCode, sLanguageCode);
            }

            var workbook = HRTMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).ExportExcelDutyTrackingStatusReport(oTableResult, sLanguageCode);
            var stream = new MemoryStream();
            workbook.SaveAs(stream);
            stream.Position = 0;

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(stream.ToArray())
            };

            DateTime date_now = DateTime.Now;
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = "ReportDutyTrackingStatus" + date_now.Year + date_now.Month + date_now.Date + ".xlsx"
            };
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

            return result;
        }

        [HttpPost]
        public HttpResponseMessage GetAttendanceMonthlyExport([FromBody] RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            string sReportName = oRequestParameter.InputParameter["ReportName"].ToString();
            string sLanguageCode = oRequestParameter.InputParameter["LanguageCode"].ToString();
            string sEmpID = oRequestParameter.InputParameter["Employee_id"].ToString();
            string sEmployeeName = oRequestParameter.InputParameter["EmployeeName"].ToString();
            string sPositionName = oRequestParameter.InputParameter["PositionName"].ToString();
            string sOrgUnitName = oRequestParameter.InputParameter["OrgUnitName"].ToString();
            string sExportType = oRequestParameter.InputParameter["ExportType"].ToString();
            string sByOrg = oRequestParameter.InputParameter["ByOrg"].ToString();
            string sListOrg = oRequestParameter.InputParameter["ListOrg"].ToString();
            DateTime dFromDate = Convert.ToDateTime(oRequestParameter.InputParameter["FromDate"]);
            DateTime dToDate = Convert.ToDateTime(oRequestParameter.InputParameter["ToDate"]);

            DataTable oTableResult = new DataTable();

            if (sByOrg == "2") //แบบระบุพนักงาน
            {
                oTableResult = HRTMManagement.CreateInstance(sCompanyCode).GetAttendanceMonthlyReport(dFromDate, dToDate, sEmpID, "", sCompanyCode, sLanguageCode);
            }
            else //แบบเลือกหน่วยงาน TreeList
            {
                oTableResult = HRTMManagement.CreateInstance(sCompanyCode).GetAttendanceMonthlyReport(dFromDate, dToDate, "", sListOrg, sCompanyCode, sLanguageCode);
            }

            var workbook = HRTMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).ExportExcelAttendanceMonthlyReport(oTableResult, sLanguageCode);
            var stream = new MemoryStream();
            workbook.SaveAs(stream);
            stream.Position = 0;

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(stream.ToArray())
            };

            DateTime date_now = DateTime.Now;
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = "ReportAttendanceMonthly" + date_now.Year + date_now.Month + date_now.Date + ".xlsx"
            };
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

            return result;
        }

        [HttpPost]
        public HttpResponseMessage GetTimeScanExport([FromBody] RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            string sReportName = oRequestParameter.InputParameter["ReportName"].ToString();
            string sLanguageCode = oRequestParameter.InputParameter["LanguageCode"].ToString();
            string sEmpID = oRequestParameter.InputParameter["Employee_id"].ToString();
            string sEmployeeName = oRequestParameter.InputParameter["EmployeeName"].ToString();
            string sPositionName = oRequestParameter.InputParameter["PositionName"].ToString();
            string sOrgUnitName = oRequestParameter.InputParameter["OrgUnitName"].ToString();
            string sExportType = oRequestParameter.InputParameter["ExportType"].ToString();
            string sByOrg = oRequestParameter.InputParameter["ByOrg"].ToString();
            string sListOrg = oRequestParameter.InputParameter["ListOrg"].ToString();
            DateTime dFromDate = Convert.ToDateTime(oRequestParameter.InputParameter["FromDate"]);
            DateTime dToDate = Convert.ToDateTime(oRequestParameter.InputParameter["ToDate"]);

            DataTable oTableResult = new DataTable();

            if (sByOrg == "2") //แบบระบุพนักงาน
            {
                oTableResult = HRTMManagement.CreateInstance(sCompanyCode).GetTimeScanReport(dFromDate, dToDate, sEmpID, "", sCompanyCode, sLanguageCode);
            }
            else //แบบเลือกหน่วยงาน TreeList
            {
                oTableResult = HRTMManagement.CreateInstance(sCompanyCode).GetTimeScanReport(dFromDate, dToDate, "", sListOrg, sCompanyCode, sLanguageCode);
            }

            var workbook = HRTMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).ExportExcelTimeScanReport(oTableResult, sLanguageCode);
            var stream = new MemoryStream();
            workbook.SaveAs(stream);
            stream.Position = 0;

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(stream.ToArray())
            };

            DateTime date_now = DateTime.Now;
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = "ReportTimeScan" + date_now.Year + date_now.Month + date_now.Date + ".xlsx"
            };
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

            return result;
        }

        [HttpPost]
        public HttpResponseMessage GetWorkingSummaryExport([FromBody] RequestParameter oRequestParameter)
        {
            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            string sReportName = oRequestParameter.InputParameter["ReportName"].ToString();
            string sLanguageCode = oRequestParameter.InputParameter["LanguageCode"].ToString();
            string sEmpID = oRequestParameter.InputParameter["Employee_id"].ToString();
            string sEmployeeName = oRequestParameter.InputParameter["EmployeeName"].ToString();
            string sPositionName = oRequestParameter.InputParameter["PositionName"].ToString();
            string sOrgUnitName = oRequestParameter.InputParameter["OrgUnitName"].ToString();
            string sExportType = oRequestParameter.InputParameter["ExportType"].ToString();
            string sByOrg = oRequestParameter.InputParameter["ByOrg"].ToString();
            string sListOrg = oRequestParameter.InputParameter["ListOrg"].ToString();
            DateTime dFromDate = Convert.ToDateTime(oRequestParameter.InputParameter["FromDate"]);
            DateTime dToDate = Convert.ToDateTime(oRequestParameter.InputParameter["ToDate"]);

            DataTable oTableResult = new DataTable();

            if (sByOrg == "2") //แบบระบุพนักงาน
            {
                oTableResult = HRTMManagement.CreateInstance(sCompanyCode).GetWorkingSummaryReport(dFromDate, dToDate, sEmpID, "", sCompanyCode, sLanguageCode);
            }
            else //แบบเลือกหน่วยงาน TreeList
            {
                oTableResult = HRTMManagement.CreateInstance(sCompanyCode).GetWorkingSummaryReport(dFromDate, dToDate, "", sListOrg, sCompanyCode, sLanguageCode);
            }

            var workbook = HRTMManagement.CreateInstance(oRequestParameter.Requestor.CompanyCode).ExportExcelWorkingSummaryReport(oTableResult);
            var stream = new MemoryStream();
            workbook.SaveAs(stream);
            stream.Position = 0;

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(stream.ToArray())
            };

            DateTime date_now = DateTime.Now;
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = "ReportWorkingSummary" + date_now.Year + date_now.Month + date_now.Date + ".xlsx"
            };
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

            return result;
        }

        // Show Report หน้ารายงานรายงานสรุปการรับรองเวลาปฏิบัติงานของพนักงาน
        [HttpPost]
        public void ShowAttendanceMonthlyReport([FromBody] RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            string sReportName = oRequestParameter.InputParameter["ReportName"].ToString();
            string sLanguageCode = oRequestParameter.InputParameter["LanguageCode"].ToString();
            string sEmpID = oRequestParameter.InputParameter["Employee_id"].ToString();
            string sEmployeeName = oRequestParameter.InputParameter["EmployeeName"].ToString();
            string sPositionName = oRequestParameter.InputParameter["PositionName"].ToString();
            string sOrgUnitName = oRequestParameter.InputParameter["OrgUnitName"].ToString();
            string sExportType = oRequestParameter.InputParameter["ExportType"].ToString();
            string sByOrg = oRequestParameter.InputParameter["ByOrg"].ToString();
            string sListOrg = oRequestParameter.InputParameter["ListOrg"].ToString();
            DateTime dFromDate = Convert.ToDateTime(oRequestParameter.InputParameter["FromDate"]);
            DateTime dToDate = Convert.ToDateTime(oRequestParameter.InputParameter["ToDate"]);

            string strExtFile = string.Empty;
            strExtFile = "EXCEL"; // typeReport;

            DataTable oTableResult = new DataTable();
            if (sByOrg == "2") //แบบระบุพนักงาน
            {
                oTableResult = HRTMManagement.CreateInstance(sCompanyCode).GetAttendanceMonthlyReport(dFromDate, dToDate, sEmpID, "", sCompanyCode, sLanguageCode);
            }
            else //แบบเลือกหน่วยงาน TreeList
            {
                oTableResult = HRTMManagement.CreateInstance(sCompanyCode).GetAttendanceMonthlyReport(dFromDate, dToDate, "", sListOrg, sCompanyCode, sLanguageCode);
            }
            
            List<ReportParameter> ReportParam = new List<ReportParameter>();
            string sDurationDate = string.Format("{0} {1}", CacheManager.GetCommonText("ATTENDANCEMONTHLYREPORT", sLanguageCode, "DATEFROM"), dFromDate.Day.ToString() + " " + CacheManager.GetCommonText("MONTH", sLanguageCode, dFromDate.Month.ToString("00")) + " " + dFromDate.ToString("yyyy", new CultureInfo((sLanguageCode == "TH" ? "th-TH" : "en-US"))));
            if (dFromDate != dToDate)
            {
                sDurationDate += string.Format(" {0} {1}", CacheManager.GetCommonText("ATTENDANCEMONTHLYREPORT", sLanguageCode, "DATETO"), dToDate.Day.ToString() + " " + CacheManager.GetCommonText("MONTH", sLanguageCode, dToDate.Month.ToString("00")) + " " + dToDate.ToString("yyyy", new CultureInfo((sLanguageCode == "TH" ? "th-TH" : "en-US"))));
            }

            ReportParam.Add(new ReportParameter("p_ReportTitle", CacheManager.GetCommonText("ATTENDANCEMONTHLYREPORT", sLanguageCode, "ReportTitle")));
            ReportParam.Add(new ReportParameter("p_MonthText", sDurationDate));
            ReportParam.Add(new ReportParameter("p_EmployeeID", CacheManager.GetCommonText("ATTENDANCEMONTHLYREPORT", sLanguageCode, "COL_EMPLOYEEID")));
            ReportParam.Add(new ReportParameter("p_EmployeeName", CacheManager.GetCommonText("ATTENDANCEMONTHLYREPORT", sLanguageCode, "COL_EMPLOYEENAME")));
            ReportParam.Add(new ReportParameter("p_WorkLocation", CacheManager.GetCommonText("ATTENDANCEMONTHLYREPORT", sLanguageCode, "COL_WORKLOCATION")));
            ReportParam.Add(new ReportParameter("p_Province", CacheManager.GetCommonText("ATTENDANCEMONTHLYREPORT", sLanguageCode, "COL_PROVINCE")));
            ReportParam.Add(new ReportParameter("p_OrgUnit", CacheManager.GetCommonText("ATTENDANCEMONTHLYREPORT", sLanguageCode, "ORGANIZATION")));
            ReportParam.Add(new ReportParameter("p_Summary", CacheManager.GetCommonText("ATTENDANCEMONTHLYREPORT", sLanguageCode, "SUMMARY")));

            ReportObject oReportObject = new ReportObject();
            //oReportObject.ExportFileName = System.Web.HttpContext.Current.Server.MapPath("~/Client/Report/rptAttendanceReport" + sEmpID);
            oReportObject.ExportFileName = System.Web.HttpContext.Current.Server.MapPath("~/Client/Report/rptAttendanceReport" + oRequestParameter.Requestor.EmployeeID);

            ReportViewer rptViewer = new ReportViewer();
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("DS_AttendanceData", oTableResult));
            rptViewer.LocalReport.ReportPath = System.Web.HttpContext.Current.Server.MapPath("~/Views/Report/HR/TM/rptAttendanceReport.rdlc");
            rptViewer.LocalReport.SetParameters(ReportParam);
            rptViewer.ShowRefreshButton = false;
            rptViewer.LocalReport.EnableExternalImages = true;
            rptViewer.LocalReport.Refresh();

            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string filenameExtension;

            byte[] bytes = rptViewer.LocalReport.Render(
                strExtFile, null, out mimeType, out encoding, out filenameExtension,
                out streamids, out warnings);

            oReportObject.Warnings = warnings;
            oReportObject.Streamids = streamids;
            oReportObject.ContentType = mimeType;
            oReportObject.Encoding = encoding;
            oReportObject.FilenameExtension = filenameExtension;
            oReportObject.ExportFileName = string.Format("{0}.{1}", oReportObject.ExportFileName, filenameExtension);
            FileManager.SaveFile(oReportObject.ExportFileName, bytes, true);

        }

        // Show Report หน้ารายงานสรุปการปฏิบัติงานของพนักงาน
        [HttpPost]
        public void ShowWorkingSummaryReport([FromBody] RequestParameter oRequestParameter)
        {

            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            string sReportName = oRequestParameter.InputParameter["ReportName"].ToString();
            string sLanguageCode = oRequestParameter.InputParameter["LanguageCode"].ToString();
            string sEmpID = oRequestParameter.InputParameter["Employee_id"].ToString();
            string sEmployeeName = oRequestParameter.InputParameter["EmployeeName"].ToString();
            string sPositionName = oRequestParameter.InputParameter["PositionName"].ToString();
            string sOrgUnitName = oRequestParameter.InputParameter["OrgUnitName"].ToString();
            string sExportType = oRequestParameter.InputParameter["ExportType"].ToString();
            string sByOrg = oRequestParameter.InputParameter["ByOrg"].ToString();
            string sListOrg = oRequestParameter.InputParameter["ListOrg"].ToString();
            DateTime dFromDate = Convert.ToDateTime(oRequestParameter.InputParameter["FromDate"]);
            DateTime dToDate = Convert.ToDateTime(oRequestParameter.InputParameter["ToDate"]);

            string strExtFile = string.Empty;
            strExtFile = "EXCEL"; // typeReport;

            DataTable oTableResult = new DataTable();
            if (sByOrg == "2") //แบบระบุพนักงาน
            {
                oTableResult = HRTMManagement.CreateInstance(sCompanyCode).GetWorkingSummaryReport(dFromDate, dToDate, sEmpID, "", sCompanyCode, sLanguageCode);
            }
            else //แบบเลือกหน่วยงาน TreeList
            {
                oTableResult = HRTMManagement.CreateInstance(sCompanyCode).GetWorkingSummaryReport(dFromDate, dToDate, "", sListOrg, sCompanyCode, sLanguageCode);
            }

            /* =========== 2. เซ็ตค่า Parameter เพื่อส่งข้อมูลไปยัง Report ===============  */
            List<ReportParameter> ReportParam = new List<ReportParameter>();

            string sDurationDate = string.Format("{0} {1}", CacheManager.GetCommonText("TIMESHEETSUMMARYREPORT", sLanguageCode, "DATEFROM"), dFromDate.Day.ToString() + " " + CacheManager.GetCommonText("MONTH", sLanguageCode, dFromDate.Month.ToString("00")) + " " + dFromDate.ToString("yyyy", new CultureInfo((sLanguageCode == "TH" ? "th-TH" : "en-US"))));
            if (dFromDate != dToDate)
            {
                sDurationDate += string.Format(" {0} {1}", CacheManager.GetCommonText("TIMESHEETSUMMARYREPORT", sLanguageCode, "DATETO"), dToDate.Day.ToString() + " " + CacheManager.GetCommonText("MONTH", sLanguageCode, dToDate.Month.ToString("00")) + " " + dToDate.ToString("yyyy", new CultureInfo((sLanguageCode == "TH" ? "th-TH" : "en-US"))));
            }

            
            ReportParam.Add(new ReportParameter("p_ReportTitle", CacheManager.GetCommonText("TIMESHEETSUMMARYREPORT", sLanguageCode, "REPORTTITLE")));
            ReportParam.Add(new ReportParameter("p_PeriodMonthText", sDurationDate)); 
            ReportParam.Add(new ReportParameter("p_EmployeeID", CacheManager.GetCommonText("TIMESHEETSUMMARYREPORT", sLanguageCode, "COL_EMPLOYEEID")));
            ReportParam.Add(new ReportParameter("p_EmployeeName", CacheManager.GetCommonText("TIMESHEETSUMMARYREPORT", sLanguageCode, "COL_EMPLOYEENAME")));
            ReportParam.Add(new ReportParameter("p_OrgDeptID", CacheManager.GetCommonText("TIMESHEETSUMMARYREPORT", sLanguageCode, "COL_OrgDeptID")));
            ReportParam.Add(new ReportParameter("p_OrgDeptName", CacheManager.GetCommonText("TIMESHEETSUMMARYREPORT", sLanguageCode, "COL_OrgDeptName")));
            ReportParam.Add(new ReportParameter("p_OrgUnitID", CacheManager.GetCommonText("TIMESHEETSUMMARYREPORT", sLanguageCode, "COL_OrgUnitID")));
            ReportParam.Add(new ReportParameter("p_OrgUnitName", CacheManager.GetCommonText("TIMESHEETSUMMARYREPORT", sLanguageCode, "COL_OrgUnitName")));
            ReportParam.Add(new ReportParameter("p_WorkSchedule_TotalDays", CacheManager.GetCommonText("TIMESHEETSUMMARYREPORT", sLanguageCode, "COL_WorkSchedule_TotalDays")));
            ReportParam.Add(new ReportParameter("p_WorkReal_TotalDays", CacheManager.GetCommonText("TIMESHEETSUMMARYREPORT", sLanguageCode, "COL_WorkReal_TotalDays")));
            ReportParam.Add(new ReportParameter("p_WorkReal_TotalHours", CacheManager.GetCommonText("TIMESHEETSUMMARYREPORT", sLanguageCode, "COL_WorkReal_TotalHours")));
            ReportParam.Add(new ReportParameter("p_WorkReal_TotalPercent", CacheManager.GetCommonText("TIMESHEETSUMMARYREPORT", sLanguageCode, "COL_WorkReal_TotalPercent")));
            ReportParam.Add(new ReportParameter("p_Absence_0100", CacheManager.GetCommonText("TIMESHEETSUMMARYREPORT", sLanguageCode, "COL_Absence_0100")));
            ReportParam.Add(new ReportParameter("p_Absence_0200", CacheManager.GetCommonText("TIMESHEETSUMMARYREPORT", sLanguageCode, "COL_Absence_0200")));
            ReportParam.Add(new ReportParameter("p_Absence_0300", CacheManager.GetCommonText("TIMESHEETSUMMARYREPORT", sLanguageCode, "COL_Absence_0300")));
            ReportParam.Add(new ReportParameter("p_Absence_0400", CacheManager.GetCommonText("TIMESHEETSUMMARYREPORT", sLanguageCode, "COL_Absence_0400")));
            ReportParam.Add(new ReportParameter("p_Absence_0500", CacheManager.GetCommonText("TIMESHEETSUMMARYREPORT", sLanguageCode, "COL_Absence_0500")));
            ReportParam.Add(new ReportParameter("p_Absence_0600", CacheManager.GetCommonText("TIMESHEETSUMMARYREPORT", sLanguageCode, "COL_Absence_0600")));
            ReportParam.Add(new ReportParameter("p_Absence_0700", CacheManager.GetCommonText("TIMESHEETSUMMARYREPORT", sLanguageCode, "COL_Absence_0700")));
            ReportParam.Add(new ReportParameter("p_Absence_TotalDays", CacheManager.GetCommonText("TIMESHEETSUMMARYREPORT", sLanguageCode, "COL_Absence_TotalDays")));
            ReportParam.Add(new ReportParameter("p_ClockInLate_QTY", CacheManager.GetCommonText("TIMESHEETSUMMARYREPORT", sLanguageCode, "COL_CLOCKINLATE")));
            ReportParam.Add(new ReportParameter("p_ClockOutEarly_QTY", CacheManager.GetCommonText("TIMESHEETSUMMARYREPORT", sLanguageCode, "COL_CLOCKOUTEARLY")));
            ReportParam.Add(new ReportParameter("p_ClockNoneWork_QTY", CacheManager.GetCommonText("TIMESHEETSUMMARYREPORT", sLanguageCode, "COL_CLOCKNONEWORK")));
            ReportParam.Add(new ReportParameter("p_ConfirmWork_QTY", CacheManager.GetCommonText("TIMESHEETSUMMARYREPORT", sLanguageCode, "COL_CONFIRMWORK")));
            ReportParam.Add(new ReportParameter("p_Workreal_Group", CacheManager.GetCommonText("TIMESHEETSUMMARYREPORT", sLanguageCode, "COL_WORKREAL_GROUP")));
            ReportParam.Add(new ReportParameter("p_WorkAbsence_Group", CacheManager.GetCommonText("TIMESHEETSUMMARYREPORT", sLanguageCode, "COL_WORKABSENCE_GROUP")));
            ReportParam.Add(new ReportParameter("p_WorkAbnormal_Group", CacheManager.GetCommonText("TIMESHEETSUMMARYREPORT", sLanguageCode, "COL_WORKABNORMAL_GROUP")));
            ReportParam.Add(new ReportParameter("p_WorkAbnormal_TotalHours", CacheManager.GetCommonText("TIMESHEETSUMMARYREPORT", sLanguageCode, "COL_WORKABNORMAL_TOTALHOURS")));
            ReportParam.Add(new ReportParameter("p_WorkLocation", CacheManager.GetCommonText("TIMESHEETSUMMARYREPORT", sLanguageCode, "COL_WORKLOCATION")));
            ReportParam.Add(new ReportParameter("p_Province", CacheManager.GetCommonText("TIMESHEETSUMMARYREPORT", sLanguageCode, "COL_PROVINCE")));

            ReportObject oReportObject = new ReportObject();
            oReportObject.ExportFileName = System.Web.HttpContext.Current.Server.MapPath("~/Client/Report/rptWorkingSummaryReport" + oRequestParameter.Requestor.EmployeeID);

            ReportViewer rptViewer = new ReportViewer();
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("DS_TIMESHEETSUMMARY", oTableResult)); 
            rptViewer.LocalReport.ReportPath = System.Web.HttpContext.Current.Server.MapPath("~/Views/Report/HR/TM/rptWorkingSummaryReport.rdlc");
            rptViewer.LocalReport.SetParameters(ReportParam);
            rptViewer.ShowRefreshButton = false;
            rptViewer.LocalReport.EnableExternalImages = true;
            rptViewer.LocalReport.Refresh();

            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string filenameExtension;

            byte[] bytes = rptViewer.LocalReport.Render(
                strExtFile, null, out mimeType, out encoding, out filenameExtension,
                out streamids, out warnings);

            oReportObject.Warnings = warnings;
            oReportObject.Streamids = streamids;
            oReportObject.ContentType = mimeType;
            oReportObject.Encoding = encoding;
            oReportObject.FilenameExtension = filenameExtension;
            oReportObject.ExportFileName = string.Format("{0}.{1}", oReportObject.ExportFileName, filenameExtension);
            FileManager.SaveFile(oReportObject.ExportFileName, bytes, true);

        }

        // Show Report หน้ารายงานติดตามการส่งสรุปค่าล่วงเวลารายเดือน
        [HttpPost]
        public void ShowOTTrackingStatusReport([FromBody] RequestParameter oRequestParameter)
        {
            EmployeeDataForMobile oCurrentEmployee = oRequestParameter.CurrentEmployee;
            SetAuthenticate(oCurrentEmployee);

            string sCompanyCode = oRequestParameter.InputParameter["SelectCompany"].ToString();
            string sReportName = oRequestParameter.InputParameter["ReportName"].ToString();
            string sLanguageCode = oRequestParameter.InputParameter["LanguageCode"].ToString();
            string sEmpID = oRequestParameter.InputParameter["Employee_id"].ToString();
            string sEmployeeName = oRequestParameter.InputParameter["EmployeeName"].ToString();
            string sPositionName = oRequestParameter.InputParameter["PositionName"].ToString();
            string sOrgUnitName = oRequestParameter.InputParameter["OrgUnitName"].ToString();
            string sExportType = oRequestParameter.InputParameter["ExportType"].ToString();
            string sByOrg = oRequestParameter.InputParameter["ByOrg"].ToString();
            string sListOrg = oRequestParameter.InputParameter["ListOrg"].ToString();
            DateTime dFromDate = Convert.ToDateTime(oRequestParameter.InputParameter["FromDate"]);
            DateTime dToDate = Convert.ToDateTime(oRequestParameter.InputParameter["ToDate"]);

            string strExtFile = string.Empty;
            strExtFile = "EXCEL"; // typeReport;

            DataTable oTableResult = new DataTable();
            if (sByOrg == "2") //แบบระบุพนักงาน
            {
                oTableResult = HRTMManagement.CreateInstance(sCompanyCode).GetOTTrackingStatusReport(dFromDate, dToDate, sEmpID, "", sCompanyCode, sLanguageCode);
            }
            else //แบบเลือกหน่วยงาน TreeList
            {
                oTableResult = HRTMManagement.CreateInstance(sCompanyCode).GetOTTrackingStatusReport(dFromDate, dToDate, "", sListOrg, sCompanyCode, sLanguageCode);
            }

            List<ReportParameter> ReportParam = new List<ReportParameter>();
            string sDurationDate = string.Format("{0} {1}", CacheManager.GetCommonText("OTTRACKINGSTATUSREPORT", sLanguageCode, "DATEFROM"), dFromDate.Day.ToString() + " " + CacheManager.GetCommonText("MONTH", sLanguageCode, dFromDate.Month.ToString("00")) + " " + dFromDate.ToString("yyyy", new CultureInfo((sLanguageCode == "TH" ? "th-TH" : "en-US"))));
            if (dFromDate != dToDate)
            {
                sDurationDate += string.Format(" {0} {1}", CacheManager.GetCommonText("OTTRACKINGSTATUSREPORT", sLanguageCode, "DATETO"), dToDate.Day.ToString() + " " + CacheManager.GetCommonText("MONTH", sLanguageCode, dToDate.Month.ToString("00")) + " " + dToDate.ToString("yyyy", new CultureInfo((sLanguageCode == "TH" ? "th-TH" : "en-US"))));
            }

            ReportParam.Add(new ReportParameter("p_ReportTitle", CacheManager.GetCommonText("OTTRACKINGSTATUSREPORT", sLanguageCode, "ReportTitle")));
            ReportParam.Add(new ReportParameter("p_MonthText", sDurationDate));
            ReportParam.Add(new ReportParameter("p_EmployeeID", CacheManager.GetCommonText("OTTRACKINGSTATUSREPORT", sLanguageCode, "COL_EMPLOYEEID")));
            ReportParam.Add(new ReportParameter("p_EmployeeName", CacheManager.GetCommonText("OTTRACKINGSTATUSREPORT", sLanguageCode, "COL_EMPLOYEENAME")));
            ReportParam.Add(new ReportParameter("p_WorkLocation", CacheManager.GetCommonText("OTTRACKINGSTATUSREPORT", sLanguageCode, "COL_WORKLOCATION")));
            ReportParam.Add(new ReportParameter("p_Province", CacheManager.GetCommonText("OTTRACKINGSTATUSREPORT", sLanguageCode, "COL_PROVINCE")));
            ReportParam.Add(new ReportParameter("p_OrgUnit", CacheManager.GetCommonText("OTTRACKINGSTATUSREPORT", sLanguageCode, "ORGANIZATION")));
            ReportParam.Add(new ReportParameter("p_Summary", CacheManager.GetCommonText("OTTRACKINGSTATUSREPORT", sLanguageCode, "SUMMARY")));
            ReportParam.Add(new ReportParameter("p_OTSender", CacheManager.GetCommonText("OTTRACKINGSTATUSREPORT", sLanguageCode, "COL_OTSender")));

            ReportObject oReportObject = new ReportObject();
            oReportObject.ExportFileName = System.Web.HttpContext.Current.Server.MapPath("~/Client/Report/rptOTTrackingStatusReport" + oRequestParameter.Requestor.EmployeeID);

            ReportViewer rptViewer = new ReportViewer();
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.DataSources.Add(new ReportDataSource("DS_OvertimeData", oTableResult)); 
            rptViewer.LocalReport.ReportPath = System.Web.HttpContext.Current.Server.MapPath("~/Views/Report/HR/TM/rptOTTrackingStatusReport.rdlc");
            rptViewer.LocalReport.SetParameters(ReportParam);
            rptViewer.ShowRefreshButton = false;
            rptViewer.LocalReport.EnableExternalImages = true;
            rptViewer.LocalReport.Refresh();

            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string filenameExtension;

            byte[] bytes = rptViewer.LocalReport.Render(
                strExtFile, null, out mimeType, out encoding, out filenameExtension,
                out streamids, out warnings);

            oReportObject.Warnings = warnings;
            oReportObject.Streamids = streamids;
            oReportObject.ContentType = mimeType;
            oReportObject.Encoding = encoding;
            oReportObject.FilenameExtension = filenameExtension;
            oReportObject.ExportFileName = string.Format("{0}.{1}", oReportObject.ExportFileName, filenameExtension);
            FileManager.SaveFile(oReportObject.ExportFileName, bytes, true);

        }
    }
}