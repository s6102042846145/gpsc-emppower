﻿using ESS.DATA.ABSTRACT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE;
using ESS.HR.PY.CONFIG;
using ESS.HR.PY.DATACLASS;
using Newtonsoft.Json;
using System.Data;
using System.Globalization;
using System.Reflection;
using ESS.HR.PY.INFOTYPE;
using ESS.HR.PA;
using ESS.HR.BE;

namespace ESS.HR.PY.DATASERVICE
{
    class LetterService : AbstractDataService
    {
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            DataSet DS = new DataSet("ADDITIONAL");
            DataTable oTable;
            Letter classTemplate = new Letter();
            oTable = classTemplate.ToADODataTable(true);
            oTable.TableName = "Letter";

            DateTime dtNow = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            DataRow dr = oTable.NewRow();
            dr["EmployeeID"] = Requestor.EmployeeID;
            //dr["RequestNo"] = string.Empty;
            dr["LetterID"] = -1;
            dr["LetterTypeID"] = -1;
            dr["SignID"] = string.Empty;
            dr["Reason"] = string.Empty;
            dr["Status"] = string.Empty;
            dr["LetterNo"] = -1;
            dr["EmbassyID"] = -1;
            dr["EmbassyName"] = string.Empty;
            dr["VacationBegin"] = dtNow;
            dr["VacationEnd"] = dtNow;
            dr["EmbassyID2"] = -1;
            dr["EmbassyName2"] = string.Empty;
            dr["VacationBegin2"] = dtNow;
            dr["VacationEnd2"] = dtNow;
            dr["EmbassyID3"] = -1;
            dr["EmbassyName3"] = string.Empty;
            dr["VacationBegin3"] = dtNow;
            dr["VacationEnd3"] = dtNow;
            dr["isUseEmbassy2"] = false;
            dr["isUseEmbassy3"] = false;

            oTable.Rows.Add(dr);

            DS.Tables.Add(oTable);

            oTable = new DataTable("HeaderInfo");
            DataColumn column = new DataColumn();
            column.ColumnName = "LetterTypeName";
            column.DataType = Type.GetType("System.String");
            oTable.Columns.Add(column);

            DataRow dRow = oTable.NewRow();
            dRow["LetterTypeName"] = string.Empty;
            oTable.Rows.Add(dRow);

            DS.Tables.Add(oTable);
            return DS;
        }

        public override void PrepareData(EmployeeData Requestor, object Data)
        {
            DataTable oTable;
            DataSet ds = JsonConvert.DeserializeObject<DataSet>(Data.ToString());
            if (!ds.Tables.Contains("Letter"))
            {
                Letter oItem = new Letter();
                oTable = oItem.ToADODataTable();
                oTable.TableName = "Letter";
                ds.Tables.Add(oTable);
            }
        }

        public override void CalculateInfoData(EmployeeData Requestor, object Data, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
            DataSet ds = JsonConvert.DeserializeObject<DataSet>(Data.ToString());

            Info.Rows.Clear();
            DataRow dr = Info.NewRow();
            dr["LetterTypeName"] = ds.Tables["HeaderInfo"].Rows[0]["LetterTypeName"];
            Info.Rows.Add(dr);
        }

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info)
        {
            string ReturnKey = "";
            if (Creator.IsInRole("PAADMIN"))
            {
                ReturnKey = "ADMIN_CREATE";
            }
            else
            {
                ReturnKey = "EMPLOYEE_CREATE";
            }

            return ReturnKey;
        }

        public override void ValidateData(Object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            DataSet ds = JsonConvert.DeserializeObject<DataSet>(newData.ToString());

            Letter oLetter = new Letter();
            oLetter.ParseToObject(ds.Tables["Letter"]);

            DateTime dtBegin = Convert.ToDateTime(ds.Tables["Letter"].Rows[0]["VacationBegin"]);
            DateTime dtEnd = Convert.ToDateTime(ds.Tables["Letter"].Rows[0]["VacationEnd"]);

            DateTime dtBegin2 = Convert.ToDateTime(ds.Tables["Letter"].Rows[0]["VacationBegin2"]);
            DateTime dtEnd2 = Convert.ToDateTime(ds.Tables["Letter"].Rows[0]["VacationEnd2"]);

            DateTime dtBegin3 = Convert.ToDateTime(ds.Tables["Letter"].Rows[0]["VacationBegin3"]);
            DateTime dtEnd3 = Convert.ToDateTime(ds.Tables["Letter"].Rows[0]["VacationEnd3"]);

            bool isUseEmbassy2 = Convert.ToBoolean(ds.Tables["Letter"].Rows[0]["isUseEmbassy2"]);
            bool isUseEmbassy3 = Convert.ToBoolean(ds.Tables["Letter"].Rows[0]["isUseEmbassy3"]);

            if (Convert.ToInt32(ds.Tables["Letter"].Rows[0]["EmbassyID"]) == 0)
                throw new DataServiceException("HRLE", "PLEASE_SELECT_EMBASSY", "Please select embassy");

            if (Convert.ToInt32(ds.Tables["Letter"].Rows[0]["EmbassyID"]) > -1 && dtBegin > dtEnd)
                throw new DataServiceException("HRLE", "VACATIONDATE_ERROR", "Invalid vacation date");

            if (isUseEmbassy2)
            {
                if (Convert.ToInt32(ds.Tables["Letter"].Rows[0]["EmbassyID2"]) == 0)
                    throw new DataServiceException("HRLE", "PLEASE_SELECT_EMBASSY2", "Please select embassy 2");

                if (Convert.ToInt32(ds.Tables["Letter"].Rows[0]["EmbassyID2"]) > -1 && dtBegin2 > dtEnd2)
                    throw new DataServiceException("HRLE", "VACATIONDATE2_ERROR", "Invalid vacation date of embassy 2");

                if(dtBegin2 < dtEnd)
                {
                    throw new DataServiceException("HRLE", "VACATIONDATE2_ERROR_ERROR", "Invalid vacation date of embassy 2");
                }
            }

            if (isUseEmbassy3)
            {
                if (Convert.ToInt32(ds.Tables["Letter"].Rows[0]["EmbassyID3"]) == 0)
                    throw new DataServiceException("HRLE", "PLEASE_SELECT_EMBASSY3", "Please select embassy 3");

                if (Convert.ToInt32(ds.Tables["Letter"].Rows[0]["EmbassyID3"]) > -1 && dtBegin3 > dtEnd3)
                    throw new DataServiceException("HRLE", "VACATIONDATE3_ERROR", "Invalid vacation date of embassy 3");

                if (dtBegin3 < dtEnd)
                {
                    throw new DataServiceException("HRLE", "VACATIONDATE3_ERROR_ERROR1", "Invalid vacation date of embassy 3");
                }

                if (dtBegin3 < dtEnd2)
                {
                    throw new DataServiceException("HRLE", "VACATIONDATE3_ERROR_ERROR2", "Invalid vacation date of embassy 3");
                }
            }

            if (String.IsNullOrEmpty(oLetter.Reason))
                throw new DataServiceException("HRLE", "PLEASE_INPUT_REASON", "Please input reason");
        }

        public override void ValidateDataInAction(object newData, DataTable Info, EmployeeData Requestor, string RequestNo, string State, string Action, bool HaveComment, bool HaveComment2, DateTime SubmitDate)
        {
            //Call from viewer
        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {

            DataSet ds = JsonConvert.DeserializeObject<DataSet>(Data.ToString());

            Letter oLetter = new Letter();
            oLetter.ParseToObject(ds.Tables["Letter"]);
            DataTable dt = ds.Tables["Letter"];
            oLetter.LetterTypeID = Convert.ToInt32(dt.Rows[0]["LetterTypeID"]);
            oLetter.Status = State;
            oLetter.RequestNo = RequestNo;
            oLetter.EmbassyID = Convert.ToInt32(dt.Rows[0]["EmbassyID"]);
            if (Convert.ToInt32(dt.Rows[0]["EmbassyID"]) != -1)
            {
                oLetter.VacationBegin = Convert.ToDateTime(dt.Rows[0]["VacationBegin"]);
                oLetter.VacationEnd = Convert.ToDateTime(dt.Rows[0]["VacationEnd"]);
            }

            oLetter.EmbassyID2 = Convert.ToInt32(dt.Rows[0]["EmbassyID2"]);
            if (Convert.ToInt32(dt.Rows[0]["EmbassyID2"]) != -1)
            {
                oLetter.VacationBegin2 = Convert.ToDateTime(dt.Rows[0]["VacationBegin2"]);
                oLetter.VacationEnd2 = Convert.ToDateTime(dt.Rows[0]["VacationEnd2"]);
            }

            oLetter.EmbassyID3 = Convert.ToInt32(dt.Rows[0]["EmbassyID3"]);
            if (Convert.ToInt32(dt.Rows[0]["EmbassyID3"]) != -1)
            {
                oLetter.VacationBegin3 = Convert.ToDateTime(dt.Rows[0]["VacationBegin3"]);
                oLetter.VacationEnd3 = Convert.ToDateTime(dt.Rows[0]["VacationEnd3"]);
            }


            ds.Tables["Letter"].Clear();
            oLetter.LoadDataToTable(ds.Tables["Letter"]);

            HRPYManagement.CreateInstance(Requestor.CompanyCode).SaveLetter(oLetter);
            if (State == "COMPLETED")
            {
                try
                {
                    DateTime BeginDate = new DateTime(DateTime.Now.Year, 1, 1);
                    DateTime EndDate = new DateTime(DateTime.Now.Year, 12, 31);
                    HRPYManagement.CreateInstance(Requestor.CompanyCode).UpdateLetterNo(oLetter.EmployeeID, oLetter.RequestNo, BeginDate, EndDate, false);
                }
                catch (Exception ex)
                {
                    throw new SaveExternalDataException("SaveExternalData Error", true, ex);
                }
            }
        }

        public override void PostProcess(EmployeeData Requestor, DataTable Info, object Data, string PreviousState, string State, string RequestNo, string Comment, string ActionCode)
        {
            //CAll after save requestdocument to sql 
        }

        public override string GenerateRequestSubtype(EmployeeData Requestor, object Additional)
        {
            return "";
        }
    }
}
