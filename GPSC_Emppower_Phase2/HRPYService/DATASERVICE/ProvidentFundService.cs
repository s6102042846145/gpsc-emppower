﻿using ESS.DATA.ABSTRACT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE;
using ESS.HR.PY.CONFIG;
using ESS.HR.PY.DATACLASS;
using Newtonsoft.Json;
using System.Data;
using System.Globalization;
using System.Reflection;
using ESS.HR.PY.INFOTYPE;
using ESS.HR.PA;
using ESS.HR.BE;

namespace ESS.HR.PY.DATASERVICE
{
    class ProvidentFundService : AbstractDataService
    {
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            PF_Option PF_Option = new PF_Option();

            DateTime datePF = new DateTime();
            if (!string.IsNullOrEmpty(CreateParam))
            {
                string[] param = CreateParam.Split('|');
                datePF = Convert.ToDateTime(param[1]);
            }

            //Get Provident Fund Data
            //PF_Option.ProvidentFundData = HRPYManagement.CreateInstance(Requestor.CompanyCode).GetProvidentFund(Requestor.EmployeeID, DateTime.Now);
            //PF_Option.OldProvidentFundData = HRPYManagement.CreateInstance(Requestor.CompanyCode).GetProvidentFund(Requestor.EmployeeID, DateTime.Now); 

            //Get Config
            PF_Option.PF_Percentage = HRPYManagement.CreateInstance(Requestor.CompanyCode).GetPF_Percentage();
            PF_Option.PF_Selection = HRPYManagement.CreateInstance(Requestor.CompanyCode).GetPF_Selection();
            //PF_Option.PF_PeriodSetting = HRPYManagement.CreateInstance(Requestor.CompanyCode).GetEffectiveDate();


            List<PF_PeriodSetting> oListPS = HRPYManagement.CreateInstance(Requestor.CompanyCode).GetEffectiveDate();

            PF_PeriodSetting oListPS_Default = oListPS.Where(m => m.EnableOption == 1)
                            .OrderBy(z => z.EffectiveDate).FirstOrDefault();

            List<PF_PeriodSetting> newList = oListPS.Where(a => a.EffectiveDate == datePF)
                     .Select(a => a).ToList();

            PF_Option.PF_PeriodSetting = newList;


            //Get Provident Fund Data
            PF_Option.ProvidentFundData = HRPYManagement.CreateInstance(Requestor.CompanyCode).GetProvidentFund(Requestor.EmployeeID, oListPS_Default.EffectiveDate);
            PF_Option.OldProvidentFundData = HRPYManagement.CreateInstance(Requestor.CompanyCode).GetProvidentFund(Requestor.EmployeeID, oListPS_Default.EffectiveDate);
            //PF_Option.ProvidentFundData = HRPYManagement.CreateInstance(Requestor.CompanyCode).GetProvidentFund(Requestor.EmployeeID, DateTime.Now);
            //PF_Option.OldProvidentFundData = HRPYManagement.CreateInstance(Requestor.CompanyCode).GetProvidentFund(Requestor.EmployeeID, DateTime.Now); 


            PF_Option.PF_ProvidentFundLog.FundID = PF_Option.ProvidentFundData.ProvidentFundNumber;
            PF_Option.PF_ProvidentFundLog.EmployeeSavingText = PF_Option.ProvidentFundData.EmployeePercentage.ToString();
            PF_Option.PF_ProvidentFundLog.CompanySaving = PF_Option.ProvidentFundData.EmployerPercentage;


            return PF_Option;
        }

        public override void PrepareData(EmployeeData Requestor, object Data)
        {
           
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            PF_Option PF_Option = JsonConvert.DeserializeObject<PF_Option>(newData.ToString());

            string dataCategory = "ProvidentFund";
            if (HRPYManagement.CreateInstance(Requestor.CompanyCode).CheckMark(Requestor.EmployeeID, dataCategory, RequestNo))
            {
                throw new DataServiceException("HRPA", "REQUEST_DUPLICATE", "Request duplicate");
            }

            //พนักงานทดลองงาน ไม่สามารถใช้งานได้
            var EmpGroup = PF_Option.PF_ProvidentFundLog.EmpGroup;
            EmployeeData oEmpData = new EmployeeData(Requestor.EmployeeID, Requestor.PositionID, Requestor.CompanyCode, DateTime.Now);

            var Trainee = HRBEManagement.CreateInstance(Requestor.CompanyCode).EmployeeTrainee(EmpGroup, oEmpData.DateSpecific.PassProbation);
            if (Trainee) {
                throw new DataServiceException("PROVIDENTFUND", "EMPLOYEE_TRAINEE");
            }




            //เลือก รูปแบบการลงทุน และ % เงินสะสมของพนักงาน เป็นค่าเดิม
            PF_Option.PF_ProvidentFundLog.EmployeeSaving = Convert.ToDecimal(PF_Option.PF_ProvidentFundLog.EmployeeSavingText);
            if (PF_Option.PF_ProvidentFundLog.FundID == PF_Option.OldProvidentFundData.ProvidentFundNumber && PF_Option.PF_ProvidentFundLog.EmployeeSaving == PF_Option.OldProvidentFundData.EmployeePercentage) {
                throw new DataServiceException("PROVIDENTFUND", "DUPPLICATE_SELECTDATA");
            }
        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            PF_Option PF_Option = JsonConvert.DeserializeObject<PF_Option>(Data.ToString());

            string dataCategory = "ProvidentFund";
            HRPYManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);

            HRPYManagement oHRPYManagement = HRPYManagement.CreateInstance(Requestor.CompanyCode);

            PF_Option.PF_ProvidentFundLog.Type = 1;
            PF_Option.PF_ProvidentFundLog.Status = State;
            PF_Option.PF_ProvidentFundLog.RequestNo = RequestNo;
            PF_Option.PF_ProvidentFundLog.EmployeeID = Requestor.EmployeeID;
            PF_Option.PF_ProvidentFundLog.EmployeeSaving = Convert.ToDecimal(PF_Option.PF_ProvidentFundLog.EmployeeSavingText);

            PF_Option.PF_ProvidentFundLog.OldFundID = PF_Option.OldProvidentFundData.ProvidentFundNumber;
            PF_Option.PF_ProvidentFundLog.OldEmployeeSaving = PF_Option.OldProvidentFundData.EmployeePercentage;

            PF_Option.PF_ProvidentFundLog.EffectiveDate = PF_Option.PF_PeriodSetting[0].EffectiveDate;

            oHRPYManagement.SaveProvidentFundLog(PF_Option.PF_ProvidentFundLog);

            //string dataCategory = "ProvidentFund";
            DataRow dr = Info.NewRow();
            dr["ContentType"] = dataCategory;
            dr["TotalAmount"] = PF_Option.PF_ProvidentFundLog.EmployeeSaving;
            Info.Rows.Add(dr);

            //Save Data To SAP
            if (State == "COMPLETED")
            {
                try
                {
                    //PF_Option.ProvidentFundData.EmployeePercentage = PF_Option.PF_ProvidentFundLog.EmployeeSaving;
                    PF_Option.ProvidentFundData.ProvidentFundNumber = PF_Option.PF_ProvidentFundLog.FundID;
                    PF_Option.ProvidentFundData.EmployeeID = PF_Option.PF_ProvidentFundLog.EmployeeID;
                    PF_Option.ProvidentFundData.BeginDate = PF_Option.PF_ProvidentFundLog.EffectiveDate;

                    var NowrovidentFund = HRPYManagement.CreateInstance(Requestor.CompanyCode).GetProvidentFund(Requestor.EmployeeID, DateTime.Now);
                    PF_Option.ProvidentFundData.EmployeePercentage = NowrovidentFund.EmployeePercentage;

                    HRPYManagement.CreateInstance(Requestor.CompanyCode).SaveProvidentFund(PF_Option.ProvidentFundData);
                }
                catch (Exception ex)
                {
                    HRPYManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, true);
                    throw new SaveExternalDataException("Post data error", true, ex);
                }
            }


        }

        public override void ValidateDataInAction(object newData, DataTable Info, EmployeeData Requestor, string RequestNo, string State, string Action, bool HaveComment, bool HaveComment2, DateTime SubmitDate)
        {
            if (Action != "CANCEL")
            {
                List<PF_PeriodSetting> oListPFPeriodSetting = HRPYManagement.CreateInstance(Requestor.CompanyCode).GetEffectiveDate();

                var chkPass = oListPFPeriodSetting.FirstOrDefault(a => a.EnableOption == 1);
                if (chkPass == null)
                {
                    throw new DataServiceException("HRTM_EXCEPTION", "TIMEOUT_PERIOD_SETTING");
                }
            }
        }
    }
}
