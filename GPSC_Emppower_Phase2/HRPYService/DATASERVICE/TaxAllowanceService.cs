﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.DATA;
using ESS.DATA.EXCEPTION;
using ESS.EMPLOYEE;
using ESS.HR.PY.CONFIG;
using ESS.DATA.ABSTRACT;
using ESS.HR.PY.DATACLASS;
using Newtonsoft.Json;
using System.Data;
using System.Globalization;
using System.Reflection;

namespace ESS.HR.PY.DATASERVICE
{
    public class TaxAllowanceService : AbstractDataService
    {
        CultureInfo oCL = new CultureInfo("en-US");
        public TaxAllowanceService()
        {
        }

        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            TaxAllowanceInfo oTaxAllowanceInfo = new TaxAllowanceInfo();
            TaxAllowance oTax = new TaxAllowance();
            oTax = HRPYManagement.CreateInstance(Requestor.CompanyCode).GetTaxAllowanceData(Requestor.EmployeeID, DateTime.Now.Date);
            oTaxAllowanceInfo.TaxAllowance = oTax;
            oTaxAllowanceInfo.TaxAllowance.TaxAllowance_Effective = DateTime.Now;
            oTaxAllowanceInfo.TaxAllowanceOld = oTax;

            return oTaxAllowanceInfo;
        }

        public override void PrepareData(EmployeeData Requestor, object Data)
        {
           
        }

        public override string GenerateFlowKey(EmployeeData Requestor, EmployeeData Creator, DataTable Info)
        {
            string ReturnKey = "";
            if (Creator.IsInRole("PAADMIN"))
            {
                ReturnKey = "ADMIN_CREATE";
            }
            else
            {
                ReturnKey = "EMPLOYEE_CREATE";
            }

            return ReturnKey;
        }
        public override string GenerateRequestSubtype(EmployeeData Requestor, object Additional)
        {
            return "";
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {

            TaxAllowanceInfo oTaxAllowanceInfo = JsonConvert.DeserializeObject<TaxAllowanceInfo>(newData.ToString());
            if (oTaxAllowanceInfo.TaxAllowance.TaxAllowance_Effective == DateTime.MinValue)
                throw new DataServiceException("HRPY", "RECORD_CONTROL_NOT_FOUND", "Can not find 'RECORDCONTROL' table");

            TaxAllowance data = new TaxAllowance();
            data = oTaxAllowanceInfo.TaxAllowance;
            
            TaxAllowance dataOld = new TaxAllowance();
            dataOld = oTaxAllowanceInfo.TaxAllowanceOld;

            Type oType = data.GetType();
            foreach (PropertyInfo prop in oType.GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                if (prop.PropertyType == typeof(decimal))
                {
                    decimal oValue = (decimal)prop.GetValue(data, null);
                    if (oValue >= 10000000)
                    {
                        throw new DataServiceException("HRPY", "DATA_EXCEED_10M", "DATA_EXCEED_10M");
                    }
                }
            }

            string dataCategory = "TAXALLOWANCEDATA";
            if (HRPYManagement.CreateInstance(Requestor.CompanyCode).CheckMark(Requestor.EmployeeID, dataCategory, RequestNo))
            {
                throw new DataServiceException("HRPA", "REQUEST_DUPLICATE", "Request duplicate");
            }

            //try
            //{
            //    TimesheetManagement.CreateInstance(Requestor.CompanyCode).MarkTimesheet(Requestor.EmployeeID, "TAXALLOWANCE", true, true, RequestNo);
            //}
            //catch (Exception e)
            //{
            //    throw new DataServiceException("TAXALLOWANCE", "COLLISION_OCCUR_WITH_DATA", "", e);
            //}

        }
        
        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            string dataCategory = "TAXALLOWANCEDATA";
            HRPYManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, "COMPLETED,CANCELLED".IndexOf(State.ToUpper()) == -1);
            if (State == "COMPLETED")
            {
                TaxAllowanceInfo oTaxAllowance = JsonConvert.DeserializeObject<TaxAllowanceInfo>(Data.ToString());

                TaxAllowance data = new TaxAllowance();
                TaxAllowance saved = new TaxAllowance();
                data = oTaxAllowance.TaxAllowanceOld;
                saved = oTaxAllowance.TaxAllowance;

                if (string.IsNullOrEmpty(saved.EmployeeID))
                {
                    saved.EmployeeID = Requestor.EmployeeID;
                }

                DateTime beginDate = new DateTime(Convert.ToInt32(DateTime.Now.ToString("yyyy", oCL)), Convert.ToInt32(DateTime.Now.ToString("MM", oCL)), 1);
               
                try
                {
                   HRPYManagement.CreateInstance(Requestor.CompanyCode).SaveTaxallowanceData(saved);
                }
                catch (Exception ex)
                {
                    HRPYManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, dataCategory, RequestNo, true);
                    throw new Exception(ex.Message.ToString());
                    //throw new SaveExternalDataException("SaveExternalData Error", true, ex);
                }
            }
            bool isMark = State == "COMPLETED" || State == "CANCELLED";
            //TimesheetManagement.CreateInstance(Requestor.CompanyCode).MarkTimesheet(Requestor.EmployeeID, "TAXALLOWANCE", !isMark, false, RequestNo);
        }
        public override void ValidateDataInAction(object newData, DataTable Info, EmployeeData Requestor, string RequestNo, string State, string Action, bool HaveComment, bool HaveComment2, DateTime SubmitDate)
        {
            bool isPassValidate = HRPYManagement.CreateInstance(Requestor.CompanyCode).ShowTimeAwareLink(2);
            if (!isPassValidate)
            {
                throw new DataServiceException("HRTM_EXCEPTION", "TIMEOUT_PERIOD_SETTING");
            }
        }
    }
}
