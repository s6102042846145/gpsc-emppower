﻿using ESS.DATA.ABSTRACT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.EMPLOYEE;
using ESS.HR.PY.DATACLASS;
using ESS.HR.PA;
using ESS.HR.PA.INFOTYPE;
using Newtonsoft.Json;
using ESS.DATA.EXCEPTION;
using System.Data;
using ESS.HR.BE;

namespace ESS.HR.PY.DATASERVICE
{
    class ProvidentMemberService : AbstractDataService
    {
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            PF_MemberOption MemberOption = new PF_MemberOption();
            MemberOption.ProvidentFundMember = HRPYManagement.CreateInstance(Requestor.CompanyCode).GetProvidentFundMember(Requestor.EmployeeID);
            MemberOption.ProvidentFundDeleteMember = MemberOption.ProvidentFundMember;

            return MemberOption;
        }

        public override void PrepareData(EmployeeData Requestor, object Data)
        {
            
        }

        public override void ValidateData(object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            PF_MemberOption MemberOption = JsonConvert.DeserializeObject<PF_MemberOption>(newData.ToString());


            //พนักงานทดลองงาน ไม่สามารถใช้งานได้
            var EmpGroup = MemberOption.EmpGroup;
            EmployeeData oEmpData = new EmployeeData(Requestor.EmployeeID, Requestor.PositionID, Requestor.CompanyCode, DateTime.Now);
            var Trainee = HRBEManagement.CreateInstance(Requestor.CompanyCode).EmployeeTrainee(EmpGroup, oEmpData.DateSpecific.PassProbation);
            if (Trainee)
            {
                throw new DataServiceException("PROVIDENTFUND", "EMPLOYEE_TRAINEE");
            }

            decimal TotalEmployer = 0;
            foreach (var item in MemberOption.ProvidentFundMember) {


                //CountryBirth ประเทศที่เกิด
                if (string.IsNullOrEmpty(item.CityOfBirth))
                {
                    throw new DataServiceException("PY_PROVIDENTFUND_MEMBER", "CITYOFBIRTH_CANTBE_NULL");
                }
                //Nationality  สัญชาติ
                if (string.IsNullOrEmpty(item.Nationality))
                {
                    throw new DataServiceException("PY_PROVIDENTFUND_MEMBER", "NATIONALITY_CANTBE_NULL");
                }
                //TitleName	คำนำหน้าชื่อ
                if (string.IsNullOrEmpty(item.TitleName))
                {
                    throw new DataServiceException("PY_PROVIDENTFUND_MEMBER", "TITLENAME_CANTBE_NULL");
                }
                //Name	ชื่อ
                if (string.IsNullOrEmpty(item.Name))
                {
                    throw new DataServiceException("PY_PROVIDENTFUND_MEMBER", "NAME_CANTBE_NULL");
                }
                //Surname นามสกุล
                if (string.IsNullOrEmpty(item.Surname))
                {
                    throw new DataServiceException("PY_PROVIDENTFUND_MEMBER", "SURNAME_CANTBE_NULL");
                }


                //ส่วนแบ่งของผลประโยชน์คิดเป็นร้อยละ
                if (string.IsNullOrEmpty(item.Employer))
                {
                    throw new DataServiceException("PY_PROVIDENTFUND_MEMBER", "EMPLOYER_CANTBE_NULL");
                }
                else
                {
                    TotalEmployer += Convert.ToDecimal(item.Employer);
                }


                //เช็คซ้ำจาก ชื่อ-นามสกุล
                var CountDuplicate = 0;
                foreach (var item_duplicate in MemberOption.ProvidentFundMember)
                {
                    if (item.Name == item_duplicate.Name && item.Surname == item_duplicate.Surname) {
                        CountDuplicate++;
                    }

                }
                if (CountDuplicate > 1) {
                    throw new DataServiceException("PY_PROVIDENTFUND_MEMBER", "MEMBER_DUPLICATE");
                }


            }
            if (TotalEmployer != 100) {
                throw new DataServiceException("PY_PROVIDENTFUND_MEMBER", "OVER_EMPLOYER");
            }

        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            PF_MemberOption MemberOption = JsonConvert.DeserializeObject<PF_MemberOption>(Data.ToString());

            DataRow dr = Info.NewRow();
            dr["ContentType"] = "PF_Member";
            dr["TotalAmount"] = 0;
            Info.Rows.Add(dr);


            //Save Data To SAP
            if (State == "COMPLETED")
            {
                try
                {
                    if (MemberOption.Check_Delete == 1)
                    {
                        List<INFOTYPE0021> PF_MemberDeleteList = new List<INFOTYPE0021>();
                        foreach (var item in MemberOption.ProvidentFundDeleteMember)
                        {
                            item.EndDate = DateTime.Now.AddSeconds(-1);
                            item.FamilyMember = "6";
                            item.EmployeeID = Requestor.EmployeeID;
                            item.Dead = "";

                            PF_MemberDeleteList.Add(item);
                        }
                        if (PF_MemberDeleteList.Count > 0)
                        {
                            HRPYManagement.CreateInstance(Requestor.CompanyCode).DeleteProvidentFundMember(PF_MemberDeleteList);
                        }
                    }



                    List<INFOTYPE0021> FamilyList = new List<INFOTYPE0021>();
                    foreach (var item in MemberOption.ProvidentFundMember)
                    {
                        item.BeginDate = DateTime.Now;
                        item.EndDate = new DateTime(9999, 12, 31);
                        item.FamilyMember = "6";
                        item.EmployeeID = Requestor.EmployeeID;
                        item.Dead = "";
                        FamilyList.Add(item);

                    }
                    if (FamilyList.Count > 0)
                    {
                        HRPYManagement.CreateInstance(Requestor.CompanyCode).SaveProvidentFundMember(FamilyList);
                    }

                }
                catch (Exception ex)
                {
                    HRPYManagement.CreateInstance(Requestor.CompanyCode).MarkUpdate(Requestor.EmployeeID, "PF_Member", RequestNo, true);
                    throw new SaveExternalDataException("Post data error", true, ex);
                }
            }

        }
    }
}
