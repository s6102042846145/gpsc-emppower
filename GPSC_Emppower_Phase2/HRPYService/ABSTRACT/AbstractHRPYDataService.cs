﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;
using ESS.HR.PY.INTERFACE;
using ESS.HR.PY.CONFIG;
using ESS.HR.PY.DATACLASS;
using ESS.HR.PY.INFOTYPE;
using ESS.HR.PA.INFOTYPE;
using ESS.EMPLOYEE.DATACLASS;

namespace ESS.HR.PY.ABSTRACT
{
    public class AbstractHRPYDataService : IHRPYDataService
    {
        #region IHRPYService Members

        public virtual PayslipResult GetPayslip(string EmployeeID, PayslipPeriodSetting Period)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual PayslipResult GetPayslipByMonthYear(string EmployeeID, string CompCode, PayslipPeriodSetting Period, string AccumType, string NotSummarizeRT)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<PaymentDetailRecord> GetPaymentDetail(string EmployeeID, string Period, bool isFindByTimePeriod)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveTaxallowanceData(TaxAllowance saved)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual TaxAllowance GetTaxAllowanceData(string EmployeeID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion
        public virtual void MarkTimesheet(string Employee, string Application, string SubKey1, string SubKey2, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark, bool isCheck, string Detail)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual INFOTYPE0366 GetProvidentFund(string EmployeeID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual void SaveProvidentFund(INFOTYPE0366 data)
        {
            throw new Exception("The method or operation is not implemented.");
        }


        public virtual List<PF_Graph> GetFundQuarter(String EmployeeID, String KeyType, int KeyYear, String KeyValue)
        {
            throw new NotImplementedException();
        }

        public virtual List<PF_PeriodSetting> GetPeriodSetting(String EmployeeID, String KeyType, int KeyYear, String KeyValue)
        {
            throw new NotImplementedException();
        }

        public virtual List<PF_ProvidentFundDetail> GetTextProvidentFundDetail(String KeyType, String KeyCode, String KeyValue)
        {
            throw new NotImplementedException();
        }

        public virtual List<PF_Percentage> GetPF_Percentage()
        {
            throw new NotImplementedException();
        }

        public virtual List<PF_Selection> GetPF_Selection()
        {
            throw new NotImplementedException();
        }

        public virtual void SaveProvidentFundLog(PF_ProvidentFundLog data)
        {
            throw new NotImplementedException();
        }

        public virtual List<PF_PeriodSetting> GetEffectiveDate()
        {
            throw new NotImplementedException();
        }

        public virtual List<TimeAwareLink> GetAllTimeAwareLink()
        {
            throw new NotImplementedException();
        }

        public virtual void UpdateTimeAwareLink(int LinkID, String BeginDate, String EndDate, String EmpID)
        {
            throw new NotImplementedException();
        }

        public virtual List<INFOTYPE0021> GetProvidentFundMember(string EmployeeID)
        {
            throw new NotImplementedException();
        }


        public virtual void DeleteProvidentFundMember(List<INFOTYPE0021> data)
        {
            throw new NotImplementedException();
        }

        public virtual void SaveProvidentFundMember(List<INFOTYPE0021> data)
        {
            throw new NotImplementedException();
        }

        public virtual List<TaxData> GetTaxDataV2(string EmployeeID, string Year, string PINCODE)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<TaxData> GetTaxDataWithoutPinCode(string EmployeeID, string Year)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual Letter GetLetter(string strRequestNo)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<Letter> GetLetter(string strEmployeeID, DateTime dtBeginDate, DateTime dtEndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<Letter> GetLetterByAdmin(DateTime dtBeginDate, DateTime dtEndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void InsertLetterLog(string strEmployeeID, string strRequestNo)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveLetter(Letter oLetter)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void UpdateLetterNo(string strEmployeeID, string strRequestNo, DateTime dtBeginDate, DateTime dtEndDate, bool bUpdateLog)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual Dictionary<string, INFOTYPE0015> GetAssignmentNumberWageTypePA0015(string strEmployeeID, DateTime dtCheckDate, string strWageType)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual Dictionary<string, INFOTYPE0015> GetAssignmentNumberWageTypePA0267(string strEmployeeID, DateTime dtCheckDate, string strWageType)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual Dictionary<string, INFOTYPE0014> GetActiveWageRecuringData(string strEmployeeID, DateTime dtCheckDate, string strWageType)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<PFChangePlanReport> GetPFChangePlanList(DateTime effectiveDate, string companyCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<PFChangeRateAmountPlanReport> GetPFChangeRateAmountPlanList(DateTime effectiveDate, string companyCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual void MarkUpdate(string EmployeeID, string DataCategory, string RequestNo, bool isMarkIn)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual bool CheckMark(string EmployeeID, string DataCategory)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual bool CheckMark(string EmployeeID, string DataCategory, string ReqNo)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #region Import Providentfund (นำเข้าข้อมูลกองทุนสำรองเลี้ยงชีพ)

        public virtual ResponeKeyProvident SaveImportProvidentFund(SaveProvidentFundKtam model)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<string> GetYearProvidentfund()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<string> GetMonthProvidentfund(int year)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<TypePVD> GetTextProvidentfund(int year, int month)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DataProvidentfund GetDataProvidentfund(int year, int month, string fundId)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        #region Job Load Payroll Employee

        public virtual PeriodSettingLatest GetSettingPeriodPayroll()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<SapPayrollEmployee> LoadPayrollSapByEmployee(List<EmployeeAllActive> list_employee, string CompCode, PeriodSettingLatest Period, string AccumType, string NotSummarizeRT, string keySecret)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveOrUpdatePayrollEmployee(List<SapPayrollEmployee> list_payroll_employee)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<PeriodSettingLatest> GetDbPeriodSettingAll()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        public virtual string GetRootOrgForReport(string sEmpID, DateTime dDateTime)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveConfigurationLog(ConfigurationLog oConfigurationLog)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        protected string ParseToXML(DataSet DS)
        {
            RemoveTimezoneForDataSet(DS);
            StringWriter tw = new StringWriter();
            DS.WriteXml(tw, XmlWriteMode.WriteSchema);
            string cData = tw.ToString();
            tw.Close();
            tw.Dispose();
            return cData;
        }

        protected void RemoveTimezoneForDataSet(DataSet DS)
        {
            foreach (DataTable dt in DS.Tables)
            {
                foreach (DataColumn dc in dt.Columns)
                {

                    if (dc.DataType == typeof(DateTime))
                    {
                        dc.DateTimeMode = DataSetDateTime.Unspecified;
                    }
                }
            }
        }
    }
}
