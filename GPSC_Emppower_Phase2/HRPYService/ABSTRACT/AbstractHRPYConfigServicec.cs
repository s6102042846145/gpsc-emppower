﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using ESS.HR.PY.INTERFACE;
using ESS.HR.PY.CONFIG;
using ESS.HR.PY.DATACLASS;

namespace ESS.HR.PY.ABSTRACT
{
    public class AbstractHRPYConfigService : IHRPYConfigService
    {
        #region IHRPYConfig Members

       
        public virtual string GetCommonText(string Category, string Language, string Code)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        #region "Dropdown"
        public virtual List<SpouseAllowance> GetAllSpouseAllowanceDropdownData()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        #endregion

        public virtual List<PayslipPeriodSetting> GetAllPayslipPeriod()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<PayslipPeriodSetting> GetActivePayslipPeriod()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<WageType> GetWageTypeAll()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual PayslipPeriodSetting GetPeriod(int periodID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual WebWageType GetWebWageTypeByCode(int Code)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SavePeriodSetting(List<PayslipPeriodSetting> periods)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<PayslipPeriodSetting> GetPeriodSettingByYear(int year)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<PayslipPeriodSetting> GetPeriodSettingByYearForAdmin(int year)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<SpouseAllowance> GetSpouseAllowanceList()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual SpouseAllowance GetSpouseAllowance(string Code)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<WageType> WageTypeAdditionalGetAll()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveSpouseAllowanceList(List<SpouseAllowance> Data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<Tavi50PeriodSetting> GetActiveTavi50Period()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<Tavi50PeriodSetting> GetAllTavi50Period(string path)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveTavi50PeriodSetting(List<Tavi50PeriodSetting> periods, string path)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual Company GetCompanyData(string Area, string SubArea)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<string> GetResponseCodeByOrganizationOnly(string Role, string EmpID, string RootOrg)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual Dictionary<int, LetterType> GetLetterType(string strPermissionCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<LetterEmbassy> GetLetterEmbassy()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual LetterType GetSignPositionByLetterType(int iLetterType)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual LetterEmbassy GetLetterEmbassyByID(int iEmbassyID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual LetterType GetLetterTypeByID(int iLetterTypeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DateTime GetPreviosEffectiveDatePayslip()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual bool ShowTimeAwareLink(int LinkID)
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }
}
