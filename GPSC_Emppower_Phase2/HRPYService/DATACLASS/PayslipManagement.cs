﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Globalization;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PY.DATACLASS
{
    [Serializable()]
    public class PayslipManagement
    {
        private static CultureInfo oCL = new CultureInfo("en-US");
        private PayslipManagement()
        {
        }

        //public static List<PaymentDetailRecord> GetPayslipDetail(string EmployeeID, string Period, bool isFindByTimePeriod)
        //{
        //    return HR.PY.ServiceManager.HRPYService.GetPaymentDetail(EmployeeID, Period, isFindByTimePeriod);
        //}

        //public static PayslipResult GetPayslip(string EmployeeID, PayslipPeriodSetting Period)
        //{
        //    return HR.PY.ServiceManager.HRPYService.GetPayslip(EmployeeID, Period);
        //}

        public static List<PaySlipReportWageDataMerge> GetPaySlipReportData(string EmployeeID, string LanguageCode, PayslipPeriodSetting oPeriod)
        {
            //1. Get all payslip data from SAP by period
            PayslipResult oResult = GetPayslip(EmployeeID, oPeriod);

            //2. Prepare and seperate data using WageType,WebWageType and others criteria
            List<PaySlipReportWageData> payslipReportDatas = GenData(EmployeeID, oResult, oPeriod, LanguageCode);

            //3. Seperate data by report type
            List<PaySlipReportWageData> CurrentIncome = payslipReportDatas.FindAll(delegate (PaySlipReportWageData ps) { return (ps.ReportType == (int)eReportType.Income); });
            List<PaySlipReportWageData> CurrentOutcome = payslipReportDatas.FindAll(delegate (PaySlipReportWageData ps) { return (ps.ReportType == (int)eReportType.Outcome); });
            CurrentIncome.Sort(new PaySlipReportWageDataComparer());
            CurrentOutcome.Sort(new PaySlipReportWageDataComparer());
            List<PaySlipReportWageData> RetroIncome = payslipReportDatas.FindAll(delegate (PaySlipReportWageData ps) { return (ps.ReportType == (int)eReportType.RetroIncome); });
            List<PaySlipReportWageData> RetroOutcome = payslipReportDatas.FindAll(delegate (PaySlipReportWageData ps) { return (ps.ReportType == (int)eReportType.RetroOutcome); });
            RetroIncome.Sort(new PaySlipReportWageDataComparer());
            RetroOutcome.Sort(new PaySlipReportWageDataComparer());
            decimal IncomeCollection = 0;
            decimal TaxCollection = 0;
            decimal TotalIncome = 0;
            decimal TotalOutcome = 0;
            decimal NetFromSAP = 0;
            decimal ProvidentFund = 0;
            decimal SocialSecurity = 0;
            List<PaySlipReportWageData> Others = payslipReportDatas.FindAll(delegate (PaySlipReportWageData ps) { return (ps.ReportType == (int)eReportType.Others); });
            foreach (PaySlipReportWageData data in Others)
            {
                if (data.WageTypeCode == "IncomeCollection")
                    IncomeCollection = data.Amount;
                else if (data.WageTypeCode == "TaxCollection")
                    TaxCollection = data.Amount;
                else if (data.WageTypeCode == "NetFromSAP")
                    NetFromSAP = data.Amount;
                else if (data.WageTypeCode == "ProvidentFund")
                    ProvidentFund = data.Amount;
                else if (data.WageTypeCode == "SocialSecurity")
                    SocialSecurity = data.Amount;
            }



            //4. Add dummy row to left(Income) and right(Deduction) data
            //to make it have the same number of row
            int cntRow = ((CurrentOutcome.Count + RetroOutcome.Count) > (CurrentIncome.Count + RetroIncome.Count) ? (CurrentOutcome.Count + RetroOutcome.Count) : (CurrentIncome.Count + RetroIncome.Count));


            List<PaySlipReportTaxAllowance> TaxAllowance = GenTaxData(EmployeeID, oPeriod.OffCycleDate, cntRow);
            if (cntRow < TaxAllowance.Count)
                cntRow = TaxAllowance.Count;
            AddDummyData(CurrentIncome, RetroIncome, cntRow);
            AddDummyData(CurrentOutcome, RetroOutcome, cntRow);

            //5. Merge all data to make it easy binding to table
            List<PaySlipReportWageDataMerge> merges = new List<PaySlipReportWageDataMerge>();
            PaySlipReportWageDataMerge merge;
            for (int i = 0; i < cntRow; i++)
            {
                merge = new PaySlipReportWageDataMerge();
                merge.SortNumber1 = CurrentIncome[i].SortNumber;
                merge.WageTypeCode1 = CurrentIncome[i].WageTypeCode;
                merge.WageTypeDescription1 = CurrentIncome[i].WageTypeDescription;
                merge.Amount1 = CurrentIncome[i].Amount;
                TotalIncome += merge.Amount1;
                merge.Currency1 = CurrentIncome[i].Currency;
                merge.SortNumber2 = CurrentOutcome[i].SortNumber;
                merge.WageTypeCode2 = CurrentOutcome[i].WageTypeCode;
                merge.WageTypeDescription2 = CurrentOutcome[i].WageTypeDescription;
                merge.Amount2 = (-1) * CurrentOutcome[i].Amount;
                TotalOutcome += merge.Amount2;
                merge.Currency2 = CurrentOutcome[i].Currency;
                merge.Code = TaxAllowance[i].Code;
                merge.Description = TaxAllowance[i].Description;
                merge.Value = TaxAllowance[i].Value;
                merges.Add(merge);
            }

            //6. Updated summary field to the first row of merge data
            if (merges.Count > 0)
            {
                decimal dNet = (TotalIncome - TotalOutcome) < 0 ? 0 : (TotalIncome - TotalOutcome);
                merges[0].TotalIncome = TotalIncome;
                merges[0].TotalOutcome = TotalOutcome;
                merges[0].Net = dNet;
                merges[0].IncomeCollection = IncomeCollection;
                merges[0].TaxCollection = TaxCollection;
                merges[0].NetFromSAP = NetFromSAP;
                merges[0].IsMisMatch = dNet == NetFromSAP ? false : true;
                merges[0].TextNote = oResult.TextNote;
                merges[0].ProvidentFund = ProvidentFund;
                merges[0].SocialSecurity = SocialSecurity;
            }

            return merges;
        }

        private static List<PaySlipReportTaxAllowance> GenTaxData(string EmployeeID, DateTime CheckDate, int cntRow)
        {
            TaxAllowance tax = ESS.HR.PY.ServiceManager.HRPYService.GetTaxAllowanceData(EmployeeID, CheckDate);
            List<PaySlipReportTaxAllowance> psReports = new List<PaySlipReportTaxAllowance>();
            PaySlipReportTaxAllowance psTax;

            //101 Ê¶Ò¹ÀÒ¾ÊÁÃÊ
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "101";
            SpouseAllowance claimType = SpouseAllowance.GetSpouseAllowance(tax.SpouseAllowance);
            psTax.Value = claimType.Key;
            psReports.Add(psTax);
            //102	ºØµÃ·ÕèÁÕÊÔ·¸ÔìÅ´ËÂèÍ¹ÀÒÉÕ / ¤¹
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "102";
            psTax.Value = (tax.ChdAllow == 0 ? string.Empty : tax.ChdAllow.ToString());
            psReports.Add(psTax);
            //103	ºØµÃ·ÕèÁÕÊÔ·¸ÔìÅ´ËÂèÍ¹¡ÒÃÈÖ¡ÉÒ / ¤¹
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "103";
            psTax.Value = (tax.ChdEduAllow == 0 ? string.Empty : tax.ChdEduAllow.ToString());
            psReports.Add(psTax);
            //104	ºØµÃ¾Ô¡ÒÃ·ÕèÁÕÊÔ·¸ÔìÅ´ËÂèÍ¹ÀÒÉÕ / ¤¹
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "104";
            psTax.Value = (tax.ChdDisAllow == 0 ? string.Empty : tax.ChdDisAllow.ToString());
            psReports.Add(psTax);
            //105	àºÕéÂ»ÃÐ¡Ñ¹ªÕÇÔµ¾¹Ñ¡§Ò¹
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "105";
            psTax.Value = (tax.LifeIns == 0 ? string.Empty : tax.LifeIns.ToString());
            psReports.Add(psTax);
            //106	àºÕéÂ»ÃÐ¡Ñ¹ªÕÇÔµ¤ÙèÊÁÃÊäÁèÁÕÃÒÂä´é
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "106";
            psTax.Value = (tax.SpouseIns == 0 ? string.Empty : tax.SpouseIns.ToString());
            psReports.Add(psTax);
            //107	¡Í§·Ø¹à§Ô¹ºÓ¹Ò­
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "107";
            psTax.Value = (tax.PensionFundInvestment == 0 ? string.Empty : tax.PensionFundInvestment.ToString());
            psReports.Add(psTax);
            //108	¡Í§·Ø¹RMF
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "108";
            psTax.Value = (tax.MFund == 0 ? string.Empty : tax.MFund.ToString());
            psReports.Add(psTax);
            //109	¡Í§·Ø¹LTF
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "109";
            psTax.Value = (tax.LongTerm == 0 ? string.Empty : tax.LongTerm.ToString());
            psReports.Add(psTax);
            //110	´Í¡àºÕéÂà§Ô¹¡Ùé·ÕèÍÂÙèÍÒÈÑÂ
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "110";
            psTax.Value = (tax.Mortgage == 0 ? string.Empty : tax.Mortgage.ToString());
            psReports.Add(psTax);
            //111	Å´ËÂèÍ¹ºÔ´Ò
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "111";
            psTax.Value = (tax.AllowFather == 0 ? string.Empty : tax.AllowFather.ToString());
            psReports.Add(psTax);
            //112	Å´ËÂèÍ¹ÁÒÃ´Ò
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "112";
            psTax.Value = (tax.AllowMother == 0 ? string.Empty : tax.AllowMother.ToString());
            psReports.Add(psTax);
            //113	Å´ËÂèÍ¹ºÔ´Ò¤ÙèÊÁÃÊ
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "113";
            psTax.Value = (tax.SpAllowFather == 0 ? string.Empty : tax.SpAllowFather.ToString());
            psReports.Add(psTax);
            //114	Å´ËÂèÍ¹ÁÒÃ´Ò¤ÙèÊÁÃÊ
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "114";
            psTax.Value = (tax.SpAllowMother == 0 ? string.Empty : tax.SpAllowMother.ToString());
            psReports.Add(psTax);
            //115	àºÕéÂ»ÃÐ¡Ñ¹ªÕÇÔµºÔ´Ò
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "115";
            psTax.Value = (tax.FatherIns == 0 ? string.Empty : tax.FatherIns.ToString());
            psReports.Add(psTax);
            //116	àºÕéÂ»ÃÐ¡Ñ¹ªÕÇÔµÁÒÃ´Ò
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "116";
            psTax.Value = (tax.MotherIns == 0 ? string.Empty : tax.MotherIns.ToString());
            psReports.Add(psTax);
            //117	àºÕéÂ»ÃÐ¡Ñ¹ªÕÇÔµºÔ´Ò¤ÙèÊÁÃÊ
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "117";
            psTax.Value = (tax.SpFatherIns == 0 ? string.Empty : tax.SpFatherIns.ToString());
            psReports.Add(psTax);
            //118	àºÕéÂ»ÃÐ¡Ñ¹ªÕÇÔµÁÒÃ´Ò¤ÙèÊÁÃÊ  
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "118";
            psTax.Value = (tax.SpMotherIns == 0 ? string.Empty : tax.SpMotherIns.ToString());
            psReports.Add(psTax);
            //119	à§Ô¹ºÃÔ¨Ò¤ 
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "119";
            psTax.Value = (tax.Charity == 0 ? string.Empty : tax.Charity.ToString());
            psReports.Add(psTax);
            //120	à§Ô¹Ê¹ÑºÊ¹Ø¹¡ÒÃÈÖ¡ÉÒ
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "120";
            psTax.Value = (tax.CharityEdu == 0 ? string.Empty : tax.CharityEdu.ToString());
            psReports.Add(psTax);
            //121	à§Ô¹Ê¹ÑºÊ¹Ø¹¡ÒÃ¡ÕÌÒ
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "121";
            psTax.Value = (tax.SportContribution == 0 ? string.Empty : tax.SportContribution.ToString());
            psReports.Add(psTax);
            //122	»ÃÐ¡Ñ¹ªÕÇÔµáºººÓ¹Ò­
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "122";
            psTax.Value = (tax.LifeInsurancePension == 0 ? string.Empty : tax.LifeInsurancePension.ToString());
            psReports.Add(psTax);
            //123	·èÍ§à·ÕèÂÇÀÒÂã¹»ÃÐà·È
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "123";
            psTax.Value = (tax.TourismExemption == 0 ? string.Empty : tax.TourismExemption.ToString());
            psReports.Add(psTax);

            List<PaySlipReportTaxAllowance> oReturn = new List<PaySlipReportTaxAllowance>();
            oReturn = psReports.FindAll(delegate (PaySlipReportTaxAllowance pt) { return !string.IsNullOrEmpty(pt.Value.Trim()); });
            oReturn.Sort(new PaySlipReportTaxAllowanceComparer());
            if (oReturn.Count < cntRow)
            {
                int loop = (cntRow - oReturn.Count);
                for (int i = 0; i < loop; i++)
                {
                    psTax = new PaySlipReportTaxAllowance();
                    psTax.Value = string.Empty;
                    psTax.Code = string.Empty;
                    oReturn.Add(psTax);
                }
            }

            return oReturn;
        }

        private static List<PaySlipReportWageData> AddDummyData(List<PaySlipReportWageData> data, List<PaySlipReportWageData> retros, int cntRow)
        {
            if (data.Count < cntRow)
            {
                foreach (PaySlipReportWageData retro in retros)
                    data.Add(retro);
                int loop = (cntRow - data.Count);
                PaySlipReportWageData psData;
                for (int i = 0; i < loop; i++)
                {
                    psData = new PaySlipReportWageData();
                    psData.WageTypeDescription = "dummy";
                    data.Add(psData);
                }
            }

            return data;
        }


        public static void CalculateDetail(List<PaymentDetailRecord> data)
        {
            Dictionary<string, Dictionary<string, PaymentDetailRecord>> dict = new Dictionary<string, Dictionary<string, PaymentDetailRecord>>();
            Dictionary<string, PaymentDetailRecord> innerDict;
            List<PaymentDetailRecord> delList = new List<PaymentDetailRecord>();
            List<string> forPayrollPeriodList = new List<string>();
            foreach (PaymentDetailRecord record in data)
            {
                if (!forPayrollPeriodList.Contains(record.ForPayrollPeriod))
                {
                    forPayrollPeriodList.Add(record.ForPayrollPeriod);
                }
                string key = string.Format("{0}{1}", record.Date.ToString("yyyyMMdd", oCL), record.WageType);
                if (dict.ContainsKey(key))
                {
                    innerDict = dict[key];
                }
                else
                {
                    innerDict = new Dictionary<string, PaymentDetailRecord>();
                    dict.Add(key, innerDict);
                }
                if (innerDict.ContainsKey(record.ForPayrollPeriod))
                {
                    innerDict[record.ForPayrollPeriod] = record;
                }
                else
                {
                    innerDict.Add(record.ForPayrollPeriod, record);
                }
            }
            foreach (string key in dict.Keys)
            {
                innerDict = dict[key];
                foreach (string payrollPeriod in forPayrollPeriodList)
                {
                    if (!innerDict.ContainsKey(payrollPeriod))
                    {
                        PaymentDetailRecord record = new PaymentDetailRecord();
                        string dateString = key.Substring(0, 8);
                        string wageType = key.Substring(8, 4);
                        record.Date = DateTime.ParseExact(dateString, "yyyyMMdd", oCL);
                        record.ForPayrollPeriod = payrollPeriod;
                        record.WageType = wageType;
                        record.Value = 0.0M;
                        innerDict.Add(payrollPeriod, record);
                        data.Add(record);
                    }
                }
            }

            foreach (string key in dict.Keys)
            {
                Decimal calValue;
                innerDict = dict[key];
                List<string> periodList = new List<string>();
                foreach (string period in innerDict.Keys)
                {
                    periodList.Add(period);
                }
                periodList.Sort();
                calValue = 0.0M;
                foreach (string period in periodList)
                {
                    PaymentDetailRecord record = innerDict[period];
                    record.Calculated = record.Value - calValue;
                    calValue = record.Value;
                    if (record.Calculated == 0.0M)
                    {
                        data.Remove(record);
                    }
                }
            }
        }

        public static Decimal CalculateYTD(DataTable Current, DataTable Retro, PayslipLayoutSetting setting)
        {
            DataView oDV = new DataView(Current);
            Decimal oReturn = 0.0M;
            foreach (PayslipLayoutSettingValue value in setting.Values)
            {
                if (value.ItemType.Trim() == "W")
                {
                    oDV.RowFilter = string.Format("WAGETYPE = '{0}' AND ACCUMTYPE = 'Y'", value.ItemCode);
                    foreach (DataRowView drv in oDV)
                    {
                        if (value.ItemFlag == "-")
                        {
                            oReturn -= (Decimal)drv["CURRENCYAMT"];
                        }
                        else
                        {
                            oReturn += (Decimal)drv["CURRENCYAMT"];
                        }
                    }
                }
                else if (value.ItemType == "T")
                {
                    oDV.RowFilter = string.Format("TYPE = '{0}' AND ACCUMTYPE = 'Y'", value.ItemCode);
                    foreach (DataRowView drv in oDV)
                    {
                        if (value.ItemFlag == "-")
                        {
                            decimal _tmp = Math.Abs((Decimal)drv["CURRENCYAMT"]); //CHAT 2011-09-21 á¡é»Ñ­ËÒ SAP Êè§¢éÍÁÙÅÁÒÁÕà¤Ã×èÍ§ËÁÒÂÅººéÒ§ ºÇ¡ºéÒ§
                            oReturn -= _tmp;
                        }
                        else
                        {
                            oReturn += (Decimal)drv["CURRENCYAMT"];
                        }
                    }
                }
            }
            return Math.Abs(oReturn);
        }
        public static Decimal CalculateCurrent(DataTable Current, PayslipLayoutSetting setting)
        {
            DataView oDV = new DataView(Current);
            Decimal oReturn = 0.0M;
            foreach (PayslipLayoutSettingValue value in setting.Values)
            {
                if (value.ItemType.Trim() == "W")
                {
                    oDV.RowFilter = string.Format("WAGETYPE = '{0}' AND ACCUMTYPE = 'M'", value.ItemCode);
                    foreach (DataRowView drv in oDV)
                    {
                        if (value.ItemFlag == "-")
                        {
                            if (string.Compare((string)drv["WAGETYPE"], "/304") != 0)
                            {
                                decimal _tmp = Math.Abs((Decimal)drv["CURRENCYAMT"]);   //CHAT 2011-09-21 á¡é»Ñ­ËÒ SAP Êè§¢éÍÁÙÅÁÒÁÕà¤Ã×èÍ§ËÁÒÂÅººéÒ§ ºÇ¡ºéÒ§
                                oReturn -= _tmp;
                            }
                        }
                        else
                        {
                            oReturn += (Decimal)drv["CURRENCYAMT"];
                        }
                    }
                }
                else if (value.ItemType == "T")
                {
                    oDV.RowFilter = string.Format("TYPE = '{0}' AND ACCUMTYPE = 'M'", value.ItemCode);
                    foreach (DataRowView drv in oDV)
                    {
                        if (value.ItemFlag == "-")
                        {
                            oReturn -= (Decimal)drv["CURRENCYAMT"];
                        }
                        else
                        {
                            oReturn += (Decimal)drv["CURRENCYAMT"];
                        }
                    }
                }
            }
            return Math.Abs(oReturn);
        }

        public static Decimal CalculateRetro(DataTable Retro, string Period, PayslipLayoutSetting setting)
        {
            DataView oDV = new DataView(Retro);
            Decimal oReturn = 0.0M;
            foreach (PayslipLayoutSettingValue value in setting.Values)
            {
                if (value.ItemType.Trim() == "W")
                {
                    oDV.RowFilter = string.Format("WAGETYPE = '{0}' AND ACCUMTYPE = 'M' and PERIOD = '{1}'", value.ItemCode, Period);
                    foreach (DataRowView drv in oDV)
                    {
                        if (value.ItemFlag == "-")
                        {
                            oReturn -= (Decimal)drv["CURRENCYAMT"];
                        }
                        else
                        {
                            oReturn += (Decimal)drv["CURRENCYAMT"];
                        }
                    }
                }
            }
            return oReturn;
        }
        public static DataTable GenerateViewInGroup(DataTable Current, PayslipLayoutSetting setting)
        {
            DataTable oReturn = Current.Clone();
            DataView oDV = new DataView(Current);
            foreach (PayslipLayoutSettingValue value in setting.Values)
            {
                if (value.ItemType == "W")
                {
                    oDV.RowFilter = string.Format("WAGETYPE = '{0}' AND ACCUMTYPE = 'M'", value.ItemCode);
                    if (oDV.Count > 0)
                    {
                        for (int i = oDV.Count - 1; i > 0; i--)
                        {
                            oDV[0].Row["Currencyamt"] = ((decimal)oDV[0].Row["Currencyamt"]) + ((decimal)oDV[i].Row["Currencyamt"]);
                            oDV.Delete(i);
                        }

                        if (!((decimal)oDV[0].Row["Currencyamt"]).Equals(0))
                            oReturn.LoadDataRow(oDV[0].Row.ItemArray, true);
                    }
                }
                else if (value.ItemType == "T")
                {
                    oDV.RowFilter = string.Format("TYPE = '{0}' AND ACCUMTYPE = 'M'", value.ItemCode);
                    foreach (DataRowView drv in oDV)
                    {
                        oReturn.LoadDataRow(drv.Row.ItemArray, true);
                    }
                }
            }
            return oReturn;
        }

        public static PayslipPeriodSetting GetPeriod(int periodID)
        {
            return HR.PY.ServiceManager.HRPYConfig.GetPeriod(periodID);
        }

        //CHAT 2011-09-21 ÊÓËÃÑºÊè§¤èÒ Current ¢Í§¤¹·Ó§Ò¹µèÒ§»ÃÐà·È ÃÑº Parameter DataTable Current, string WageType(¤èÒ WageType)
        public static Decimal GetCurrentInter(DataTable Current, string WageType)
        {
            DataView oDV = new DataView(Current);
            Decimal oReturn = 0.0M;

            oDV.RowFilter = string.Format("WAGETYPE = '{0}' AND ACCUMTYPE = 'M'", WageType);

            foreach (DataRowView drv in oDV)
            {
                oReturn = Math.Abs((Decimal)drv["CURRENCYAMT"]);
            }

            return oReturn;
        }

        private static List<PaySlipReportWageData> GenData(string EmployeeID, PayslipResult psResult, PayslipPeriodSetting psPeriod, string LanguageCode)
        {
            List<WageType> wagetypeList = HRPYManagement.GetWageTypeAll();
            List<PaySlipReportWageData> psReports = new List<PaySlipReportWageData>();
            List<PaySlipReportWageData> psReportsCurrent = new List<PaySlipReportWageData>();
            List<PaySlipReportWageData> psReportsRetro = new List<PaySlipReportWageData>();

            //Current
            psReportsCurrent = GenSubData(EmployeeID, wagetypeList, psResult.Current.Rows, psReportsCurrent, psPeriod, (int)eReportType.Income, (int)eReportType.Outcome, LanguageCode);
            //Retro
            psReportsRetro = GenSubData(EmployeeID, wagetypeList, psResult.Retro.Rows, psReportsRetro, psPeriod, (int)eReportType.RetroIncome, (int)eReportType.RetroOutcome, LanguageCode);

            psReports.AddRange(psReportsCurrent);
            psReports.AddRange(psReportsRetro);

            return psReports;
        }

        private static List<PaySlipReportWageData> GenSubData(string employeeID, List<WageType> wagetypeList, DataRowCollection drs, List<PaySlipReportWageData> psReports, PayslipPeriodSetting psPeriod, int IncomeType, int OutcomeType, string LanguageCode)
        {
            PaySlipReportWageData psData;
            WageType wagetype;
            int SortNumber = 0;

            foreach (DataRow dr in drs)
            {
                //If AccumType of this data is Monthly
                if (dr["AccumType"].ToString() == "M")
                {
                    psData = new PaySlipReportWageData();
                    wagetype = wagetypeList.Find(delegate (WageType wt) { return (wt.Code.ToString() == dr["WageType"].ToString()); });

                    //If this data is existing in table WageType
                    if (wagetype != null)
                    {
                        //This entry is not Retro and is not Gross-up data
                        //If not, keep going
                        //If yes, ignore this entry
                        if (!(IncomeType == (int)eReportType.RetroIncome && OutcomeType == (int)eReportType.RetroOutcome && wagetype.IsGrossUp))
                        {
                            Decimal dec = 0;
                            dec = Convert.ToDecimal(dr["Currencyamt"]);

                            #region Wagetype /401
                            /*
                        ¶éÒäÁèãªè offcyle ãËé assign ¡Ñº WebWageType? 31
                        ¶éÒà»ç¹ offcycle áÅÐ ÍÂÙèã¹ªèÇ§ 1-15 Á¡ÃÒ ãËé assign ¡Ñº WebWageType? 32 
                        */
                            if (wagetype.Code == "/401")
                            {
                                DateTime dtRange1 = DateTime.ParseExact("0101" + psPeriod.PeriodYear.ToString(), "ddMMyyyy", oCL);
                                DateTime dtRange2 = DateTime.ParseExact("1501" + psPeriod.PeriodYear.ToString(), "ddMMyyyy", oCL);
                                SortNumber = 31;

                                if (psPeriod.IsOffCycle && (psPeriod.OffCycleDate >= dtRange1 && psPeriod.OffCycleDate <= dtRange2))
                                    SortNumber = 32;

                                WebWageType wwt = WebWageType.GetWebWageTypeByCode(SortNumber);
                                psData.ReportType = (wwt.DeductionFlag ? OutcomeType : IncomeType);
                                psData.SortNumber = wwt.WebCode;
                                psData.WageTypeCode = "/401";
                                if (psData.ReportType == (int)eReportType.RetroIncome || psData.ReportType == (int)eReportType.RetroOutcome)
                                {
                                    string[] Period = dr["PERIOD"].ToString().Split(':');
                                    if (Period.Length > 1)
                                        psData.WageTypeDescription = string.Format("{0} (Off-Cycle: {1})", GetCommonText("WEBWAGETYPE", LanguageCode, SortNumber.ToString()), DateTime.ParseExact(Period[1], "yyyyMMdd", oCL).ToString("dd/MM/yyyy", oCL));
                                    else
                                        psData.WageTypeDescription = string.Format("{0} ({1})", GetCommonText("WEBWAGETYPE", LanguageCode, SortNumber.ToString()), DateTime.ParseExact(dr["PERIOD"].ToString(), "yyyyMM", oCL).ToString("MM/yyyy", oCL));
                                }
                                else
                                    psData.WageTypeDescription = GetCommonText("WEBWAGETYPE", LanguageCode, SortNumber.ToString());
                                psData.Amount = wagetype.MultiplierAmount * dec;
                                psData.Currency = GetCommonText("CURRENCY", LanguageCode, dr["Currencyunit"].ToString());

                                int MatchIndex = psReports.FindIndex(delegate (PaySlipReportWageData ps) { return (ps.SortNumber == SortNumber && ps.WageTypeDescription == psData.WageTypeDescription); });
                                if (MatchIndex > -1)
                                    psReports[MatchIndex].Amount += (wagetype.MultiplierAmount * dec);
                                else
                                    psReports.Add(psData);
                            }
                            #endregion

                            #region Wagetype /Z02
                            /*
                            ¶éÒà»ç¹à§Ô¹ + ãËé assign ¡Ñº WebWageType? 11
                            ¶éÒà»ç¹à§Ô¹ – ãËé assign ¡Ñº WebWageType? 201
                            */
                            else if (wagetype.Code == "/Z02")
                            {
                                SortNumber = 11;
                                if (Convert.ToDecimal(dr["Currencyamt"]) >= 0)
                                    SortNumber = 11;
                                else
                                    SortNumber = 201;

                                WebWageType wwt = WebWageType.GetWebWageTypeByCode(SortNumber);
                                psData.ReportType = (wwt.DeductionFlag ? OutcomeType : IncomeType);
                                psData.SortNumber = wwt.WebCode;
                                psData.WageTypeCode = "/Z02";
                                if (psData.ReportType == (int)eReportType.RetroIncome || psData.ReportType == (int)eReportType.RetroOutcome)
                                {
                                    string[] Period = dr["PERIOD"].ToString().Split(':');
                                    if (Period.Length > 1)
                                        psData.WageTypeDescription = string.Format("{0} (Off-Cycle: {1})", GetCommonText("WEBWAGETYPE", LanguageCode, SortNumber.ToString()), DateTime.ParseExact(Period[1], "yyyyMMdd", oCL).ToString("dd/MM/yyyy", oCL));
                                    else
                                        psData.WageTypeDescription = string.Format("{0} ({1})", GetCommonText("WEBWAGETYPE", LanguageCode, SortNumber.ToString()), DateTime.ParseExact(dr["PERIOD"].ToString(), "yyyyMM", oCL).ToString("MM/yyyy", oCL));
                                }
                                else
                                    psData.WageTypeDescription = GetCommonText("WEBWAGETYPE", LanguageCode, SortNumber.ToString());
                                psData.Amount = wagetype.MultiplierAmount * dec;
                                psData.Currency = GetCommonText("CURRENCY", LanguageCode, dr["Currencyunit"].ToString());

                                int MatchIndex = psReports.FindIndex(delegate (PaySlipReportWageData ps) { return (ps.SortNumber == SortNumber && ps.WageTypeDescription == psData.WageTypeDescription); });
                                if (MatchIndex > -1)
                                    psReports[MatchIndex].Amount += (wagetype.MultiplierAmount * dec);
                                else
                                    psReports.Add(psData);
                            }
                            #endregion

                            #region Others
                            else if (wagetype.SortNumber > 0)
                            {
                                WebWageType wwt = WebWageType.GetWebWageTypeByCode(wagetype.SortNumber);
                                psData.ReportType = (wwt.DeductionFlag ? OutcomeType : IncomeType);
                                psData.SortNumber = wwt.WebCode;
                                psData.WageTypeCode = wagetype.Code;
                                if (psData.ReportType == (int)eReportType.RetroIncome || psData.ReportType == (int)eReportType.RetroOutcome)
                                {
                                    string[] Period = dr["PERIOD"].ToString().Split(':');
                                    if (Period.Length > 1)
                                        psData.WageTypeDescription = string.Format("{0} (Off-Cycle: {1})", GetCommonText("WEBWAGETYPE", LanguageCode, wagetype.SortNumber.ToString()), DateTime.ParseExact(Period[1], "yyyyMMdd", oCL).ToString("dd/MM/yyyy", oCL));
                                    else
                                        psData.WageTypeDescription = string.Format("{0} ({1})", GetCommonText("WEBWAGETYPE", LanguageCode, wagetype.SortNumber.ToString()), DateTime.ParseExact(dr["PERIOD"].ToString(), "yyyyMM", oCL).ToString("MM/yyyy", oCL));
                                }
                                else
                                    psData.WageTypeDescription = GetCommonText("WEBWAGETYPE", LanguageCode, wagetype.SortNumber.ToString());

                                psData.Amount = (wagetype.MultiplierAmount * dec);
                                psData.Currency = GetCommonText("CURRENCY", LanguageCode, dr["Currencyunit"].ToString());

                                //If this row is have the same SortNumber in existing data in psReports
                                int MatchIndex = psReports.FindIndex(delegate (PaySlipReportWageData ps) { return (ps.SortNumber == wagetype.SortNumber && ps.WageTypeDescription == psData.WageTypeDescription); });
                                if (MatchIndex > -1)
                                {
                                    psReports[MatchIndex].Amount += (wagetype.MultiplierAmount * dec);
                                }
                                else
                                {

                                    psReports.Add(psData);
                                }

                            }
                            #endregion
                        }
                    }
                    //If this data is not existing in table WageType
                    else
                    {
                        //Net total from SAP (TotalIncome - TotalOutcome)
                        if (dr["WageType"].ToString() == "/559")
                        {
                            psData = new PaySlipReportWageData();
                            psData.ReportType = (int)eReportType.Others;
                            psData.WageTypeCode = "NetFromSAP";
                            psData.Amount = Convert.ToDecimal(dr["Currencyamt"]);
                            psReports.Add(psData);
                        }
                    }
                }
                //If AccumType of this data is Yearly
                else
                {
                    //Income Collection
                    if (dr["WageType"].ToString() == "/102" || dr["WageType"].ToString() == "/103")  // (change 04.10.2012) ¹Ó¤èÒ /102+/103 ä»áÊ´§à»ç¹à§Ô¹ä´éÊÐÊÁ
                    {
                        psData = new PaySlipReportWageData();
                        if (psReports.Exists(delegate (PaySlipReportWageData wd) { return wd.WageTypeCode == "IncomeCollection"; })) // ÁÕ data áÅéÇÃÖà»ÅèÒ
                        {
                            psData = psReports.Find(delegate (PaySlipReportWageData wd) { return wd.WageTypeCode == "IncomeCollection"; }); // ¹Ó¤èÒ"IncomeCollection" ¢Öé¹ÁÒ
                            psData.Amount += Convert.ToDecimal(dr["Currencyamt"]); // ºÇ¡¤èÒãËÁèà¢éÒä»
                        }
                        else
                        {
                            psData.ReportType = (int)eReportType.Others;
                            psData.WageTypeCode = "IncomeCollection";
                            psData.Amount = Convert.ToDecimal(dr["Currencyamt"]);
                            psReports.Add(psData);
                        }
                    }

                    #region IncomeCollectionoldversion
                    //if (dr["WageType"].ToString() == "/101")  // (change 20.08.2012 ) /101 ¤èÒ·Õè¨Ðä»áÊ´§ã¹ÃÒÂä´éÊÐÊÁ
                    //{
                    //    psData = new PaySlipReportWageData();
                    //    psData.ReportType = (int)eReportType.Others;
                    //    psData.WageTypeCode = "IncomeCollection";
                    //    psData.Amount = Convert.ToDecimal(dr["Currencyamt"]);
                    //    psReports.Add(psData);
                    //}
                    #endregion
                    //Tax Collection
                    else if (dr["WageType"].ToString() == "/401")
                    // (change 27.07.2012 ) ¤èÒ·Õè¨Ðä»áÊ´§ã¹ÀÒÉÕÊÐÊÁ àÍÒ /401( base+gross ) á·¹ /485 ( base ) 
                    {
                        psData = new PaySlipReportWageData();
                        psData.ReportType = (int)eReportType.Others;
                        psData.WageTypeCode = "TaxCollection";
                        psData.Amount = Convert.ToDecimal(dr["Currencyamt"]);
                        psReports.Add(psData);
                    }
                    //Provident fund
                    else if (dr["WageType"].ToString() == "/321")
                    {
                        psData = new PaySlipReportWageData();
                        psData.ReportType = (int)eReportType.Others;
                        psData.WageTypeCode = "ProvidentFund";
                        psData.Amount = Convert.ToDecimal(dr["Currencyamt"]);
                        psReports.Add(psData);
                    }
                    //Social security
                    else if (dr["WageType"].ToString() == "/301")
                    {
                        psData = new PaySlipReportWageData();
                        psData.ReportType = (int)eReportType.Others;
                        psData.WageTypeCode = "SocialSecurity";
                        psData.Amount = Convert.ToDecimal(dr["Currencyamt"]);
                        psReports.Add(psData);
                    }
                }
            }

            return psReports;
        }

        public static DateTime GetPreviousEffectiveDate()
        {
            return HR.PY.ServiceManager.HRPYConfig.GetPreviosEffectiveDatePayslip();
        }

        public static string GetCommonText(string Category, string Language, string Code)
        {
            return ESS.HR.PY.ServiceManager.HRPYConfig.GetCommonText(Category, Language, Code);
        }

        enum eReportType
        {
            Income = 0,
            Outcome = 1,
            RetroIncome = 2,
            RetroOutcome = 3,
            Others = 4
        }
    }
}
