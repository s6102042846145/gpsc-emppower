﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PY.DATACLASS
{
    [Serializable()]
    public class PaySlipReportTaxAllowanceComparer : IComparer<PaySlipReportTaxAllowance>
    {
        public PaySlipReportTaxAllowanceComparer()
        {
        }

        #region IComparer<PaySlipReportTaxAllowance> Members

        public int Compare(PaySlipReportTaxAllowance x, PaySlipReportTaxAllowance y)
        {
            return x.Code.CompareTo(y.Code);
        }

        #endregion
    }
}
