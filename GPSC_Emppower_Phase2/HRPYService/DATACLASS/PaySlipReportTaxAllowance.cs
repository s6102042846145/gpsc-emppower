﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PY.DATACLASS
{
    [Serializable()]
    public class PaySlipReportTaxAllowance
    {
        private string __code;
        private string __description;
        private string __value;

        public PaySlipReportTaxAllowance()
        {
        }

        public string Code
        {
            get { return __code; }
            set { __code = value; }
        }

        public string Description
        {
            get { return __description; }
            set { __description = value; }
        }

        public string Value
        {
            get { return __value; }
            set { __value = value; }
        }
    }
}
