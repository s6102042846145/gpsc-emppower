﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PY.DATACLASS
{
    public class TaxAllowanceInfo : AbstractObject
    {
        private TaxAllowance __taxAllowance = new TaxAllowance();
        public TaxAllowance TaxAllowance
        {
            get { return __taxAllowance; }
            set { __taxAllowance = value; }
        }

        private TaxAllowance __taxAllowanceOld = new TaxAllowance();
        public TaxAllowance TaxAllowanceOld
        {
            get { return __taxAllowanceOld; }
            set { __taxAllowanceOld = value; }
        }
    }
}
