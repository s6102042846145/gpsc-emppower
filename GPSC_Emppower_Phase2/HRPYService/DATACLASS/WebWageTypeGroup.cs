﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PY.DATACLASS
{
    [Serializable()]
    public class WebWageTypeGroup : AbstractObject
    {
        private int __groupID;
        private string __textCode;
        private int __sortNumber;

        public WebWageTypeGroup()
        {
        }

        public int GroupID
        {
            get { return __groupID; }
            set { __groupID = value; }
        }

        public string TextCode
        {
            get { return __textCode; }
            set { __textCode = value; }
        }

        public int SortNumber
        {
            get { return __sortNumber; }
            set { __sortNumber = value; }
        }
    }
}
