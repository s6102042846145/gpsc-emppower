﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PY.DATACLASS
{
    [Serializable()]
    public class PaySlipReportDescription
    {
        private string __description;
        private bool __isHeader;

        public PaySlipReportDescription()
        {
        }

        public bool IsHeader
        {
            get { return __isHeader; }
            set { __isHeader = value; }
        }

        public string Description
        {
            get { return __description; }
            set { __description = value; }
        }

    }
}
