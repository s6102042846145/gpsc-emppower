﻿using ESS.HR.PY.INFOTYPE;
using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PY.DATACLASS
{
    public class PF_Option : AbstractObject
    {
        

        private PF_ProvidentFundLog __PF_ProvidentFundLog = new PF_ProvidentFundLog();
        public PF_ProvidentFundLog PF_ProvidentFundLog
        {
            get
            {
                return __PF_ProvidentFundLog;
            }
            set
            {
                __PF_ProvidentFundLog = value;
            }
        }

        private List<PF_Percentage> __PF_Percentage = new List<PF_Percentage>();
        public List<PF_Percentage> PF_Percentage
        {
            get
            {
                return __PF_Percentage;
            }
            set
            {
                __PF_Percentage = value;
            }
        }

        private List<PF_Selection> __PF_Selection = new List<PF_Selection>();
        public List<PF_Selection> PF_Selection
        {
            get
            {
                return __PF_Selection;
            }
            set
            {
                __PF_Selection = value;
            }
        }

        private List<PF_PeriodSetting> __PF_PeriodSetting = new List<PF_PeriodSetting>();
        public List<PF_PeriodSetting> PF_PeriodSetting
        {
            get
            {
                return __PF_PeriodSetting;
            }
            set
            {
                __PF_PeriodSetting = value;
            }
        }



        private INFOTYPE0366 __ProvidentFundData = new INFOTYPE0366();
        public INFOTYPE0366 ProvidentFundData
        {
            get
            {
                return __ProvidentFundData;
            }
            set
            {
                __ProvidentFundData = value;
            }
        }


        private INFOTYPE0366 __OldProvidentFundData = new INFOTYPE0366();
        public INFOTYPE0366 OldProvidentFundData
        {
            get
            {
                return __OldProvidentFundData;
            }
            set
            {
                __OldProvidentFundData = value;
            }
        }


    }
}
