﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PY.DATACLASS
{
    [Serializable()]
    public class TaxData : AbstractObject
    {
        private string area = "";
        private string companyCode = "";
        private string subArea = "";
        private string condition = "";
        private string year = "";
        private string order = "";
        private string employeeTaxNo = "";
        private decimal income = 0.00M;
        private decimal tax = 0.00M;
        private decimal income2 = 0.00M;
        private decimal tax2 = 0.00M;
        private decimal income_oth = 0.00M;
        private decimal tax_oth = 0.00M;
        private decimal pfAmount = 0.00M;
        private decimal ssAmount = 0.00M;
        private string empAddress = "";
        private string empStreet = "";
        private string empProvince = "";
        private string empPostCode = "";
        private string employeeID = "";
        private string empDistrict = "";
        public TaxData()
        {
        }

        public string CompanyCode
        {
            get
            {
                return companyCode;
            }
            set
            {
                companyCode = value;
            }
        }

        public string EmployeeID
        {
            get
            {
                return employeeID;
            }
            set
            {
                employeeID = value;
            }
        }

        public string Area
        {
            get
            {
                return area;
            }
            set
            {
                area = value;
            }
        }

        public string SubArea
        {
            get
            {
                return subArea;
            }
            set
            {
                subArea = value;
            }
        }

        /// <summary>
        ///     01 ¾¹Ñ¡§Ò¹¨èÒÂàÍ§
        ///     02 ºÃÔÉÑ·ÍÍ¡ãËé¤ÃÑé§à´ÕÂÇ
        ///     03 ºÃÔÉÑ·ÍÍ¡ãËé·Ñé§ËÁ´
        ///     04 Í×è¹æ
        /// </summary>
        public string Condition
        {
            get
            {
                return condition;
            }
            set
            {
                condition = value;
            }
        }

        public string Order
        {
            get
            {
                return order;
            }
            set
            {
                order = value;
            }
        }
        public string EmployeeTaxNo
        {
            get
            {
                return employeeTaxNo;
            }
            set
            {
                employeeTaxNo = value;
            }
        }
        public decimal Income
        {
            get
            {
                return income;
            }
            set
            {
                income = value;
            }
        }
        public decimal Tax
        {
            get
            {
                return tax;
            }
            set
            {
                tax = value;
            }
        }
        public decimal Income2
        {
            get
            {
                return income2;
            }
            set
            {
                income2 = value;
            }
        }
        public decimal Tax2
        {
            get
            {
                return tax2;
            }
            set
            {
                tax2 = value;
            }
        }
        public decimal TaxOther
        {
            get
            {
                return tax_oth;
            }
            set
            {
                tax_oth = value;
            }
        }
        public decimal IncomeOther
        {
            get
            {
                return income_oth;
            }
            set
            {
                income_oth = value;
            }
        }

        public decimal PFAmount
        {
            get
            {
                return pfAmount;
            }
            set
            {
                pfAmount = value;
            }
        }
        public decimal SSAmount
        {
            get
            {
                return ssAmount;
            }
            set
            {
                ssAmount = value;
            }
        }
        public string EmpAddress
        {
            get
            {
                return empAddress;
            }
            set
            {
                empAddress = value;
            }
        }
        public string EmpStreet
        {
            get
            {
                return empStreet;
            }
            set
            {
                empStreet = value;
            }
        }

        public string EmpDistrict
        {
            get
            {
                return empDistrict;
            }
            set
            {
                empDistrict = value;
            }
        }

        public string EmpProvince
        {
            get
            {
                return empProvince;
            }
            set
            {
                empProvince = value;
            }
        }
        public string EmpPostCode
        {
            get
            {
                return empPostCode;
            }
            set
            {
                empPostCode = value;
            }
        }
        public string Year
        {
            get
            {
                return year;
            }
            set
            {
                year = value;
            }
        }

    }
}
