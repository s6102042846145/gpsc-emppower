﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PY.DATACLASS
{
    public class ProvidentFundKtam
    {
        public ProvidentFundKtam()
        {
            list_provident_detail = new List<ProvidentFundEmployeeDetail>();
        }
        public string FundId { get; set; }
        public string CompanyId { get; set; }
        public DateTime NavDate { get; set; }
        public decimal NavValue { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public string File { get; set; }
        public bool IsError { get; set; }
        public string StirngError { get; set; }
        public int TotalPage { get; set; }
        public List<ProvidentFundEmployeeDetail> list_provident_detail { get; set; }
        public List<ProvidentFundEmployeeDetail> Pagination_data { get; set; }
    }

    public class ProvidentFundEmployeeDetail
    {
        public int Order { get; set; }
        public string EmployeeId { get; set; } // 1
        public string TitleName { get; set; } // 2
        public string FirstName { get; set; } // 3
        public string LastName { get; set; } // 4
        public string DeptCode { get; set; } // 5
        public decimal Old_employee_amount { get; set; } // 6
        public decimal Old_employee_benefit_amount { get; set; } // 7
        public decimal Old_employee_unit { get; set; } // 8
        public decimal Old_company_amount { get; set; } // 9
        public decimal Old_company_benefit_amount { get; set; } // 10
        public decimal Old_company_unit { get; set; } // 11
        public decimal Old_forward_amount_employee { get; set; } // 12
        public decimal Old_forward_benefit_amount_employee { get; set; } // 13
        public decimal Old_forward_unit_employee { get; set; } // 14
        public decimal Old_forward_amount_company { get; set; } // 15
        public decimal Old_forward_benefit_amount_company { get; set; } // 16
        public decimal Old_forward_unit_company { get; set; } // 17

        public decimal Employee_amount_transfer_in { get; set; } // 18
        public decimal Employee_benefit_amount_transfer_in { get; set; } // 19
        public decimal Employee_unit_transfer_in { get; set; } // 20
        public decimal Company_amount_transfer_in { get; set; } // 21
        public decimal Company_benefit_amount_transfer_in { get; set; } // 22
        public decimal Company_unit_transfer_in { get; set; } // 23

        public decimal Forward_amount_employee_transfer_in { get; set; } // 24
        public decimal Forward_benefit_amount_employee_transfer_in { get; set; } // 25
        public decimal Forward_unit_employee_transfer_in { get; set; } // 26
        public decimal Forward_amount_company_transfer_in { get; set; } // 27
        public decimal Forward_benefit_amount_company_transfer_in { get; set; } // 28
        public decimal Forward_unit_company_transfer_in { get; set; } // 29

        public decimal Year_employee_amount { get; set; } // 30
        public decimal Year_employee_benefit_amount { get; set; } // 31
        public decimal Year_employee_unit { get; set; } // 32
        public decimal Year_company_amount { get; set; } // 33
        public decimal Year_company_benefit_amount { get; set; } // 34
        public decimal Year_company_unit { get; set; } // 35

        public decimal Year_forward_amount_employee { get; set; } // 36
        public decimal Year_forward_benefit_amount_employee { get; set; } // 37
        public decimal Year_forward_unit_employee { get; set; } // 38
        public decimal Year_forward_amount_company { get; set; } // 39
        public decimal Year_forward_benefit_amount_company { get; set; } // 40
        public decimal Year_forward_unit_company { get; set; } // 41

        public decimal Employee_amount_transfer_out { get; set; } // 42
        public decimal Employee_benfit_amount_transfer_out { get; set; } // 43
        public decimal Employee_unit_transfer_out { get; set; } // 44

        public decimal Company_amount_transfer_out { get; set; } // 45
        public decimal Company_benefit_amount_transfer_out { get; set; } // 46
        public decimal Company_unit_transfer_out { get; set; } // 47

        public decimal Forward_amount_employee_transfer_out { get; set; } // 48
        public decimal Forward_benefit_amount_employee_transfer_out { get; set; } // 49
        public decimal Forward_unit_employee_transfer_out { get; set; } // 50
        public decimal Forward_amount_company_transfer_out { get; set; } // 51
        public decimal Forward_benefit_amount_company_transfer_out { get; set; } // 52
        public decimal Forward_unit_company_transfer_out { get; set; } // 53

        public decimal Total_employee_amount { get; set; } // 54
        public decimal Total_employee_benefit_amount { get; set; } // 55
        public decimal Total_employee_unit { get; set; } // 56

        public decimal Total_company_amount { get; set; } // 57
        public decimal Total_company_benefit_amount { get; set; } // 58
        public decimal Total_company_unit { get; set; } // 59

        public decimal Total_forward_amount_employee { get; set; } // 60
        public decimal Total_forward_benefit_amount_employee { get; set; } // 61
        public decimal Total_forward_unit_employee { get; set; } // 62
        public decimal Total_forward_amount_company { get; set; } // 63
        public decimal Total_forward_benefit_amount_company { get; set; } // 64
        public decimal Total_forward_unit_company { get; set; } // 65
        public decimal External_trans_emp_amount { get; set; } // 66
        public decimal External_trans_emp_benefit_amount { get; set; } // 67
        public decimal External_trans_comp_amount { get; set; } // 68
        public decimal External_trans_comp_benefit_amount { get; set; } // 69
    }

    public class SaveProvidentFundKtam
    {

        public SaveProvidentFundKtam()
        {
            ListProvidentfundEmployee = new List<ProvidentFundEmployeeDetail>();
        }

        public string FundId { get; set; }
        public string CompanyId { get; set; }
        public DateTime NavDate { get; set; }
        public decimal NavValue { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public string File { get; set; }
        public string EmployeeId { get; set; }
        public List<ProvidentFundEmployeeDetail> ListProvidentfundEmployee { get; set; }
    }

    public class TbHeaderProvidentFundImport : AbstractObject
    {
        public int PF_HeaderID { get; set; }
        public string FundID { get; set; }
        public string CompanyID { get; set; }
        public DateTime NAV_Date { get; set; }
        public decimal NAV_Value { get; set; }
        public int PF_Year { get; set; }
        public int PF_Month { get; set; }
        public DateTime ImportedDate { get; set; }
        public string ImportedBy { get; set; }
        public string ImportedFileName { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateBy { get; set; }
    }

    public class ResponeKeyProvident
    {
        public int PF_HeaderId { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public string FundId { get; set; }
        public string CompanyId { get; set; }
        public string FileName { get; set; }
    }

    public class DataProvidentfund : AbstractObject
    {
        public DataProvidentfund()
        {
            list_detail_provident = new List<DataProvidentDetail>();
        }
        
        public int PF_HeaderID { get; set; }
        public string FundID { get; set; }
        public string CompanyID { get; set; }
        public DateTime NAV_Date { get; set; }
        public decimal NAV_Value { get; set; }
        public int PF_Year { get; set; }
        public int PF_Month { get; set; }
        public List<DataProvidentDetail> list_detail_provident { get; set; }
    }

    public class DataProvidentDetail : AbstractObject
    {
        public int PF_DetailID { get; set; }
        public int PF_HeaderID { get; set; }
        public string EmployeeId { get; set; } // 1
        public string TitleName { get; set; } // 2
        public string FirstName { get; set; } // 3
        public string LastName { get; set; } // 4
        public string DeptCode { get; set; } // 5
        public decimal Old_employee_amount { get; set; } // 6
        public decimal Old_employee_benefit_amount { get; set; } // 7
        public decimal Old_employee_unit { get; set; } // 8
        public decimal Old_company_amount { get; set; } // 9
        public decimal Old_company_benefit_amount { get; set; } // 10
        public decimal Old_company_unit { get; set; } // 11
        public decimal Old_forward_amount_employee { get; set; } // 12
        public decimal Old_forward_benefit_amount_employee { get; set; } // 13
        public decimal Old_forward_unit_employee { get; set; } // 14
        public decimal Old_forward_amount_company { get; set; } // 15
        public decimal Old_forward_benefit_amount_company { get; set; } // 16
        public decimal Old_forward_unit_company { get; set; } // 17

        public decimal Employee_amount_transfer_in { get; set; } // 18
        public decimal Employee_benefit_amount_transfer_in { get; set; } // 19
        public decimal Employee_unit_transfer_in { get; set; } // 20
        public decimal Company_amount_transfer_in { get; set; } // 21
        public decimal Company_benefit_amount_transfer_in { get; set; } // 22
        public decimal Company_unit_transfer_in { get; set; } // 23

        public decimal Forward_amount_employee_transfer_in { get; set; } // 24
        public decimal Forward_benefit_amount_employee_transfer_in { get; set; } // 25
        public decimal Forward_unit_employee_transfer_in { get; set; } // 26
        public decimal Forward_amount_company_transfer_in { get; set; } // 27
        public decimal Forward_benefit_amount_company_transfer_in { get; set; } // 28
        public decimal Forward_unit_company_transfer_in { get; set; } // 29

        public decimal Year_employee_amount { get; set; } // 30
        public decimal Year_employee_benefit_amount { get; set; } // 31
        public decimal Year_employee_unit { get; set; } // 32
        public decimal Year_company_amount { get; set; } // 33
        public decimal Year_company_benefit_amount { get; set; } // 34
        public decimal Year_company_unit { get; set; } // 35

        public decimal Year_forward_amount_employee { get; set; } // 36
        public decimal Year_forward_benefit_amount_employee { get; set; } // 37
        public decimal Year_forward_unit_employee { get; set; } // 38
        public decimal Year_forward_amount_company { get; set; } // 39
        public decimal Year_forward_benefit_amount_company { get; set; } // 40
        public decimal Year_forward_unit_company { get; set; } // 41

        public decimal Employee_amount_transfer_out { get; set; } // 42
        public decimal Employee_benfit_amount_transfer_out { get; set; } // 43
        public decimal Employee_unit_transfer_out { get; set; } // 44

        public decimal Company_amount_transfer_out { get; set; } // 45
        public decimal Company_benefit_amount_transfer_out { get; set; } // 46
        public decimal Company_unit_transfer_out { get; set; } // 47

        public decimal Forward_amount_employee_transfer_out { get; set; } // 48
        public decimal Forward_benefit_amount_employee_transfer_out { get; set; } // 49
        public decimal Forward_unit_employee_transfer_out { get; set; } // 50
        public decimal Forward_amount_company_transfer_out { get; set; } // 51
        public decimal Forward_benefit_amount_company_transfer_out { get; set; } // 52
        public decimal Forward_unit_company_transfer_out { get; set; } // 53

        public decimal Total_employee_amount { get; set; } // 54
        public decimal Total_employee_benefit_amount { get; set; } // 55
        public decimal Total_employee_unit { get; set; } // 56

        public decimal Total_company_amount { get; set; } // 57
        public decimal Total_company_benefit_amount { get; set; } // 58
        public decimal Total_company_unit { get; set; } // 59

        public decimal Total_forward_amount_employee { get; set; } // 60
        public decimal Total_forward_benefit_amount_employee { get; set; } // 61
        public decimal Total_forward_unit_employee { get; set; } // 62
        public decimal Total_forward_amount_company { get; set; } // 63
        public decimal Total_forward_benefit_amount_company { get; set; } // 64
        public decimal Total_forward_unit_company { get; set; } // 65

        public decimal External_trans_emp_amount { get; set; } // 66
        public decimal External_trans_emp_benefit_amount { get; set; } // 67
        public decimal External_trans_comp_amount { get; set; } // 68
        public decimal External_trans_comp_benefit_amount { get; set; } // 69
    }

    public class TypePVD : AbstractObject
    {
        public string FundID { get; set; }
        public string CompanyID { get; set; }
    }
}
