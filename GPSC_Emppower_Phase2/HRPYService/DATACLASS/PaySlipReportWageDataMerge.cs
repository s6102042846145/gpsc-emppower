﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PY.DATACLASS
{
    [Serializable()]
    public class PaySlipReportWageDataMerge : AbstractObject
    {
        //Zone 1
        private int sortNumber1;
        private string wageTypeCode1;
        private string wageTypeDescription1;
        private decimal amount1;
        private string currency1;
        //Zone 2
        private int sortNumber2;
        private string wageTypeCode2;
        private string wageTypeDescription2;
        private decimal amount2;
        private string currency2;
        //Zone 3
        private string code;
        private string description;
        private string value;
        //Others
        private decimal totalIncome;
        private decimal totalOutcome;
        private decimal incomeCollection;
        private decimal taxCollection;
        private decimal net;
        private decimal netFromSAP;
        private bool isMisMatch;
        private DataTable textNote;
        private decimal providentFund;
        private decimal socialSecurity;

        public PaySlipReportWageDataMerge()
        {
        }

        public int SortNumber1
        {
            get { return sortNumber1; }
            set { sortNumber1 = value; }
        }
        public string WageTypeCode1
        {
            get { return wageTypeCode1; }
            set { wageTypeCode1 = value; }
        }
        public string WageTypeDescription1
        {
            get { return wageTypeDescription1; }
            set { wageTypeDescription1 = value; }
        }
        public decimal Amount1
        {
            get { return amount1; }
            set { amount1 = value; }
        }
        public string Currency1
        {
            get { return currency1; }
            set { currency1 = value; }
        }

        public int SortNumber2
        {
            get { return sortNumber2; }
            set { sortNumber2 = value; }
        }
        public string WageTypeCode2
        {
            get { return wageTypeCode2; }
            set { wageTypeCode2 = value; }
        }
        public string WageTypeDescription2
        {
            get { return wageTypeDescription2; }
            set { wageTypeDescription2 = value; }
        }
        public decimal Amount2
        {
            get { return amount2; }
            set { amount2 = value; }
        }
        public string Currency2
        {
            get { return currency2; }
            set { currency2 = value; }
        }

        public string Code
        {
            get { return code; }
            set { code = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        public string Value
        {
            get { return value; }
            set { value = value; }
        }

        public decimal TotalIncome
        {
            get { return totalIncome; }
            set { totalIncome = value; }
        }
        public decimal TotalOutcome
        {
            get { return totalOutcome; }
            set { totalOutcome = value; }
        }
        public decimal IncomeCollection
        {
            get { return incomeCollection; }
            set { incomeCollection = value; }
        }
        public decimal TaxCollection
        {
            get { return taxCollection; }
            set { taxCollection = value; }
        }
        public decimal Net
        {
            get { return net; }
            set { net = value; }
        }
        public decimal NetFromSAP
        {
            get { return netFromSAP; }
            set { netFromSAP = value; }
        }
        public bool IsMisMatch
        {
            get { return isMisMatch; }
            set { isMisMatch = value; }
        }
        public DataTable TextNote
        {
            get { return textNote; }
            set { textNote = value; }
        }

        public decimal ProvidentFund
        {
            get { return providentFund; }
            set { providentFund = value; }
        }
        public decimal SocialSecurity
        {
            get { return socialSecurity; }
            set { socialSecurity = value; }
        }
    }
}
