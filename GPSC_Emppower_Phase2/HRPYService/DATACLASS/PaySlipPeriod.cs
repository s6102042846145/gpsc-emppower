﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PY.DATACLASS
{
    public class PeriodSettingLatest : AbstractObject
    {
        public int PeriodID { get; set; }
        public bool IsOffCycle { get; set; }
        public int PeriodYear { get; set; }
        public int PeriodMonth { get; set; }
        public DateTime? OffCycleDate { get; set; }
        public bool Active { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public bool? IsDraft { get; set; }
        public bool? IsEvaluationHistory { get; set; }
    }

    public class SapPayrollEmployee : AbstractObject
    {
        public SapPayrollEmployee()
        {
            ListPayrollDetail = new List<SapPayrollEmployeeDetail>();
        }
        public int HeaderID { get; set; }
        public string EmployeeID { get; set; }
        public int PeriodYear { get; set; }
        public int PeriodMonth { get; set; }
        public string AccumType { get; set; }
        public string ForPeriod { get; set; }
        public string InPeriod { get; set; }
        public string OffCycleFlag { get; set; }
        public DateTime PYDate { get; set; }
        public int RecordStatus { get; set; }
        public string Remarks { get; set; }
        public DateTime ImportDate { get; set; }
        public int ImportStatus { get; set; }
        public List<SapPayrollEmployeeDetail> ListPayrollDetail { get; set; }
    }

    public class SapPayrollEmployeeDetail : AbstractObject
    {
        public int DetailID { get; set; }
        public int HeaderID { get; set; }
        public string ForPeriod { get; set; }
        public string InPeriod { get; set; }
        public string WageTypeCode { get; set; }
        public string WageTypeName { get; set; }
        public string TimeType { get; set; }
        public decimal TimeAmount { get; set; }
        public string TimeUnit { get; set; }
        public string CurrencyAmount { get; set; }
        public decimal Amount { get; set; } // ยอดเงินที่ทำการ Decription
        public string CurrencyUnit { get; set; }
        public int RecordStatus { get; set; }
        public string Remarks { get; set; }
        public DateTime ImportDate { get; set; }
        public int ImportStatus { get; set; }
    }


}
