﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PY.DATACLASS
{
    [Serializable()]
    public class PaySlipReportWageDataComparer : IComparer<PaySlipReportWageData>
    {
        public PaySlipReportWageDataComparer()
        {
        }

        #region IComparer<PaySlipReportWageDataComparer> Members

        public int Compare(PaySlipReportWageData x, PaySlipReportWageData y)
        {
            if (x.SortNumber > y.SortNumber)
                return 1;
            else if (x.SortNumber == y.SortNumber)
            {
                return x.WageTypeDescription.CompareTo(y.WageTypeDescription);
            }
            else
                return -1;
        }

        #endregion
    }
}
