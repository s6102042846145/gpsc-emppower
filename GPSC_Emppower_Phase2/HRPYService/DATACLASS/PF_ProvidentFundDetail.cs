﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PY.DATACLASS
{
    public class PF_ProvidentFundDetail : AbstractObject
    {
        public int DetailID { get; set; }
        public string Code { get; set; }
        public string Value { get; set; }
    }
}
