﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PY.DATACLASS
{
    public class ConfigurationLog
    {
        public ConfigurationLog()
        {
        }

        private string __employeeID = "";
        private string __configTable = "";
        private DateTime __logDate = new DateTime();
        private DataSet __oldData = new DataSet();
        private DataSet __newData = new DataSet();
        private string __remark = "";

        public string EmployeeID
        {
            get { return __employeeID; }
            set { __employeeID = value; }
        }
        public string ConfigTable
        {
            get { return __configTable; }
            set { __configTable = value; }
        }
        public DateTime LogDate
        {
            get { return __logDate; }
            set { __logDate = value; }
        }
        public DataSet OldData
        {
            get { return __oldData; }
            set { __oldData = value; }
        }
        public DataSet NewData
        {
            get { return __newData; }
            set { __newData = value; }
        }
        public string Remark
        {
            get { return __remark; }
            set { __remark = value; }
        }
        public void LoadToOldData(DataTable dataTable)
        {
            __oldData.Tables.Clear();
            DataTable tmp = dataTable.Copy();
            __oldData.Tables.Add(tmp);
        }
        public void LoadToNewData(DataTable dataTable)
        {
            __newData.Tables.Clear();
            DataTable tmp = dataTable.Copy();
            __newData.Tables.Add(tmp);
        }
        public void LoadToOldData<T>(List<T> items)
        {
            __oldData.Tables.Clear();
            __oldData.Tables.Add(LoadToDataTable(items));
        }
        public void LoadToNewData<T>(List<T> items)
        {
            __newData.Tables.Clear();
            __newData.Tables.Add(LoadToDataTable(items));
        }

        private DataTable LoadToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            return dataTable;
        }
    }
}
