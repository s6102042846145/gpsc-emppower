﻿using ESS.HR.PA.INFOTYPE;
using ESS.HR.PY.INFOTYPE;
using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PY.DATACLASS
{
    public class PF_MemberOption : AbstractObject
    {

        private List<INFOTYPE0021> __ProvidentFundMember = new List<INFOTYPE0021>();
        public List<INFOTYPE0021> ProvidentFundMember
        {
            get
            {
                return __ProvidentFundMember;
            }
            set
            {
                __ProvidentFundMember = value;
            }
        }

        private List<INFOTYPE0021> __ProvidentFundDeleteMember = new List<INFOTYPE0021>();
        public List<INFOTYPE0021> ProvidentFundDeleteMember
        {
            get
            {
                return __ProvidentFundDeleteMember;
            }
            set
            {
                __ProvidentFundDeleteMember = value;
            }
        }

        public int Check_Delete { get; set; }

        public string ErrorText { get; set; }

        public string EmpGroup { get; set; }
    }
}
