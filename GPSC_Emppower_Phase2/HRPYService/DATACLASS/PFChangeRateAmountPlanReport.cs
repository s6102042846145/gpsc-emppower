﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PY.DATACLASS
{
    public class PFChangeRateAmountPlanReport : AbstractObject
    {
        public DateTime EffectiveDate { get; set; }
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public decimal EmployeeSavingRateOld { get; set; }
        public decimal EmployeeSavingRateNew { get; set; }
        public string RequestNo { get; set; }
        public DateTime SubmitDate { get; set; }
        public DateTime ActionDate { get; set; }
    }
}
