﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PY.DATACLASS
{
    [Serializable()]
    public class WebWageTypeComparer : IComparer<WebWageType>
    {
        public WebWageTypeComparer()
        {
        }


        #region IComparer<WebWageType> Members

        public int Compare(WebWageType x, WebWageType y)
        {
            if (x.GroupID > y.GroupID)
                return 1;
            else if (x.GroupID == y.GroupID)
            {
                if (x.WebCode > y.WebCode)
                    return 1;
                else if (x.WebCode == y.WebCode)
                    return x.WageTypeName.CompareTo(y.WageTypeName);
                else
                    return -1;
            }
            else
                return -1;
        }

        #endregion
    }
}
