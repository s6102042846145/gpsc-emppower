﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.DATA;
using ESS.UTILITY.EXTENSION;
using ESS.EMPLOYEE.ABSTRACT;

namespace ESS.HR.PY.DATACLASS
{
    //[Serializable()]
    public class TaxAllowance : AbstractInfoType
    {
        private decimal __spouseIns = 0.0m;
        private decimal __mortgage = 0.0M;
        private decimal __mFund = 0.0M;
        private decimal __charity = 0.0M;
        private decimal __charityEdu = 0.0M;
        private decimal __lifeIns = 0.0M;
        private decimal __longTerm = 0.0M;
        private decimal __allowFather = 0.0M;
        private decimal __allowMother = 0.0M;
        private decimal __spAllowFather = 0.0M;
        private decimal __spAllowMother = 0.0M;
        private decimal __pensionFund = 0.0M;
        private decimal __sportContribution = 0.0M;
        private decimal __fatherIns = 0.0M;
        private decimal __motherIns = 0.0M;
        private decimal __spFatherIns = 0.0M;
        private decimal __spMotherIns = 0.0M;
        private int __chdAllow = 0;
        private int __chdEduAllow = 0;
        private string __taxID = "";
        private string __spouseAllowance = "";
        private string __allowType = "";
        
        private int __chdDisAllow = 0;
        
        private decimal __houseAllowanceHire = 0.0M;
        private decimal __pensionFundInvestment = 0.0M;
        private decimal __expenseimmovableProperty = 0.0M;
        private decimal __lifeInsurancePension = 0.0M;
        private decimal __tourismExemption = 0.0M;

        //Get config from configuration file that these fields below can edit or not
        private bool __spouseInsIsReadonly = false;
        private bool __mortgageIsReadonly = false;
        private bool __mFundIsReadonly = false;
        private bool __charityIsReadonly = false;
        private bool __charityEduIsReadonly = false;
        private bool __lifeInsIsReadonly = false;
        private bool __longTermIsReadonly = false;
        private bool __allowFatherIsReadonly = false;
        private bool __allowMotherIsReadonly = false;
        private bool __spAllowFatherIsReadonly = false;
        private bool __spAllowMotherIsReadonly = false;
        private bool __pensionFundIsReadonly = false;
        private bool __sportContributionIsReadonly = false;
        private bool __fatherInsIsReadonly = false;
        private bool __motherInsIsReadonly = false;
        private bool __spFatherInsIsReadonly = false;
        private bool __spMotherInsIsReadonly = false;
        private bool __chdAllowIsReadonly = false;
        private bool __chdEduAllowIsReadonly = false;
        private bool __taxIDIsReadonly = false;
        private bool __spouseAllowanceIsReadonly = false;
        private bool __allowTypeIsReadonly = false;
        private bool __chdDisAllowIsReadonly = false;

        public TaxAllowance()
        {
        }

        public override string InfoType
        {
            get { return "0364"; }
        }

        public decimal SpouseIns
        {
            get
            {
                return __spouseIns;
            }
            set
            {
                __spouseIns = value;
            }
        }
        public decimal Mortgage
        {
            get
            {
                return __mortgage;
            }
            set
            {
                __mortgage = value;
            }
        }
        public decimal MFund
        {
            get
            {
                return __mFund;
            }
            set
            {
                __mFund = value;
            }
        }
        public decimal Charity
        {
            get
            {
                return __charity;
            }
            set
            {
                __charity = value;
            }
        }
        public decimal CharityEdu
        {
            get
            {
                return __charityEdu;
            }
            set
            {
                __charityEdu = value;
            }
        }
        public decimal LifeIns
        {
            get
            {
                return __lifeIns;
            }
            set
            {
                __lifeIns = value;
            }
        }
        public decimal LongTerm
        {
            get
            {
                return __longTerm;
            }
            set
            {
                __longTerm = value;
            }
        }
        public decimal AllowFather
        {
            get
            {
                return __allowFather;
            }
            set
            {
                __allowFather = value;
            }
        }
        public decimal AllowMother
        {
            get
            {
                return __allowMother;
            }
            set
            {
                __allowMother = value;
            }
        }
        public decimal SpAllowFather
        {
            get
            {
                return __spAllowFather;
            }
            set
            {
                __spAllowFather = value;
            }
        }
        public decimal SpAllowMother
        {
            get
            {
                return __spAllowMother;
            }
            set
            {
                __spAllowMother = value;
            }
        }
        public decimal SportContribution
        {
            get
            {
                return __sportContribution;
            }
            set
            {
                __sportContribution = value;
            }
        }
        public decimal FatherIns
        {
            get
            {
                return __fatherIns;
            }
            set
            {
                __fatherIns = value;
            }
        }
        public decimal MotherIns
        {
            get
            {
                return __motherIns;
            }
            set
            {
                __motherIns = value;
            }
        }
        public decimal SpFatherIns
        {
            get
            {
                return __spFatherIns;
            }
            set
            {
                __spFatherIns = value;
            }
        }
        public decimal SpMotherIns
        {
            get
            {
                return __spMotherIns;
            }
            set
            {
                __spMotherIns = value;
            }
        }
        public int ChdAllow
        {
            get
            {
                return __chdAllow;
            }
            set
            {
                __chdAllow = value;
            }
        }
        public int ChdEduAllow
        {
            get
            {
                return __chdEduAllow;
            }
            set
            {
                __chdEduAllow = value;
            }
        }
        
        public int ChdDisAllow
        {
            get
            {
                return __chdDisAllow;
            }
            set
            {
                __chdDisAllow = value;
            }
        }

        public string TaxID
        {
            get
            {
                return __taxID;
            }
            set
            {
                __taxID = value;
            }
        }
        public string SpouseAllowance
        {
            get
            {
                return __spouseAllowance;
            }
            set
            {
                __spouseAllowance = value;
            }
        }
        public string AllowanceType
        {
            get
            {
                return __allowType;
            }
            set
            {
                __allowType = value;
            }
        }

        public decimal HouseAllowanceHire
        {
            get
            {
                return __houseAllowanceHire;
            }
            set
            {
                __houseAllowanceHire = value;
            }
        }
        public decimal PensionFundInvestment
        {
            get
            {
                return __pensionFundInvestment;
            }
            set
            {
                __pensionFundInvestment = value;
            }
        }
        public decimal ExpenseimmovableProperty
        {
            get
            {
                return __expenseimmovableProperty;
            }
            set
            {
                __expenseimmovableProperty = value;
            }
        }
        public decimal LifeInsurancePension
        {
            get
            {
                return __lifeInsurancePension;
            }
            set
            {
                __lifeInsurancePension = value;
            }
        }
        public decimal TourismExemption
        {
            get
            {
                return __tourismExemption;
            }
            set
            {
                __tourismExemption = value;
            }
        }

        public DateTime TaxAllowance_Effective { get; set; }
            

        //public static TaxAllowance GetData(EmployeeData Requestor)
        //{
        //    return HR.PY.ServiceManager.HRPYService.GetTaxAllowanceData(Requestor.EmployeeID, DateTime.Now);
        //}
    }
}
