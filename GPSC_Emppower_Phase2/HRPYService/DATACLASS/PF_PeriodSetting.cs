﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PY.DATACLASS
{
    public class PF_PeriodSetting : AbstractObject
    {
        public int SettingID { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime EffectiveDate { get; set; }
        public int EnableOption { get; set; }
        public int EnablePercentage { get; set; }
    }
}
