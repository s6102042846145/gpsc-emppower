﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PY.DATACLASS
{
    [Serializable()]
    public class WebWageType : AbstractObject
    {
        private int __webCode;
        private string __wageTypeName;
        private int __groupID;
        private bool __deductionFlag;

        public WebWageType()
        {
        }

        public int WebCode
        {
            get { return __webCode; }
            set { __webCode = value; }
        }
        public string WageTypeName
        {
            get { return __wageTypeName; }
            set { __wageTypeName = value; }
        }
        public int GroupID
        {
            get { return __groupID; }
            set { __groupID = value; }
        }
        public bool DeductionFlag
        {
            get { return __deductionFlag; }
            set { __deductionFlag = value; }
        }

        //public static WebWageType GetWebWageTypeByCode(int Code)
        //{
        //    return ServiceManager.HRPYConfig.GetWebWageTypeByCode(Code);
        //}
    }
}
