﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PY.DATACLASS
{
    public class TimeAwareLink : AbstractObject
    {
        public int LinkID { get; set; }
        public string BeginDate { get; set; }
        public string EndDate { get; set; }
        public string Description { get; set; }
        public string UserRole { get; set; }
        public string LastActionBy { get; set; }
        public DateTime LastActionDate { get; set; }
    }
}
