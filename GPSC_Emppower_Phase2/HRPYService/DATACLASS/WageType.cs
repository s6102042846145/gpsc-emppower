﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PY.DATACLASS
{
    [Serializable()]
    public class WageType : AbstractObject
    {
        private string __code;
        private int __multiplierAmount;
        private string __unitOfMeas;
        private int __inputFlag;
        private decimal __minAmount;
        private decimal __maxAmount;
        private decimal __minNumber;
        private decimal __maxNumber;
        private string __description;
        private bool __forInfotype0014;
        private bool __forInfotype0015;
        private int __sortNumber;
        private int __groupID;
        private bool __isActive;
        private bool __isGrossUp;

        public WageType()
        {
        }

        public string CompanyCode { get; set; }
        public string Code
        {
            get { return __code; }
            set { __code = value; }
        }
        public int MultiplierAmount
        {
            get { return __multiplierAmount; }
            set { __multiplierAmount = value; }
        }
        public string UnitOfMeas
        {
            get { return __unitOfMeas; }
            set { __unitOfMeas = value; }
        }
        public int InputFlag
        {
            get { return __inputFlag; }
            set { __inputFlag = value; }
        }
        public decimal MinAmount
        {
            get { return __minAmount; }
            set { __minAmount = value; }
        }
        public decimal MaxAmount
        {
            get { return __maxAmount; }
            set { __maxAmount = value; }
        }
        public decimal MinNumber
        {
            get { return __minNumber; }
            set { __minNumber = value; }
        }
        public decimal MaxNumber
        {
            get { return __maxNumber; }
            set { __maxNumber = value; }
        }
        public string Description
        {
            get { return __description; }
            set { __description = value; }
        }
        public bool ForInfotype0014
        {
            get { return __forInfotype0014; }
            set { __forInfotype0014 = value; }
        }
        public bool ForInfotype0015
        {
            get { return __forInfotype0015; }
            set { __forInfotype0015 = value; }
        }
        public int SortNumber
        {
            get { return __sortNumber; }
            set { __sortNumber = value; }
        }
        public int GroupID
        {
            get { return __groupID; }
            set { __groupID = value; }
        }
        public bool IsActive
        {
            get { return __isActive; }
            set { __isActive = value; }
        }
        public bool IsGrossUp
        {
            get { return __isGrossUp; }
            set { __isGrossUp = value; }
        }

        public List<WageType> GetAllWageTypeAdditional()
        {
            //return ServiceManager.HRPYConfig.WageTypeAdditionalGetAll();
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.WageTypeAdditionalGetAll();
        }
    }
}
