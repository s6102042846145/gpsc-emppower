﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PY.DATACLASS
{
    [Serializable()]
    public class MeritData
    {
        private string __employeeID = "";
        private decimal __lastSalary = 0.0M;
        private decimal __midPoint = 0.0M;
        private decimal __increasePercent = 0.0M;
        private decimal __increaseAmount = 0.0M;
        private decimal __newSalary = 0.0M;
        private int __empLevel = 0;
        private decimal __minimum = 0.0M;
        private decimal __maximum = 0.0M;
        private decimal __calculationBase = 0.0M;
        public MeritData()
        {
        }
        public string EmployeeID
        {
            get
            {
                return __employeeID;
            }
            set
            {
                __employeeID = value;
            }
        }
        public decimal LastSalary
        {
            get
            {
                return __lastSalary;
            }
            set
            {
                __lastSalary = value;
            }
        }
        public decimal NewSalary
        {
            get
            {
                return __newSalary;
            }
            set
            {
                __newSalary = value;
            }
        }
        public decimal MidPoint
        {
            get
            {
                return __midPoint;
            }
            set
            {
                __midPoint = value;
            }
        }
        public decimal IncreasePercent
        {
            get
            {
                return __increasePercent;
            }
            set
            {
                __increasePercent = value;
            }
        }
        public decimal IncreaseAmount
        {
            get
            {
                return __increaseAmount;
            }
            set
            {
                __increaseAmount = value;
            }
        }

        public int EmpLevel
        {
            get
            {
                return __empLevel;
            }
            set
            {
                __empLevel = value;
            }
        }

        public decimal Minimum
        {
            get
            {
                return __minimum;
            }
            set
            {
                __minimum = value;
            }
        }

        public decimal Maximum
        {
            get
            {
                return __maximum;
            }
            set
            {
                __maximum = value;
            }
        }

        public decimal CalculationBase
        {
            get
            {
                return __calculationBase;
            }
            set
            {
                __calculationBase = value;
            }
        }
    }
}
