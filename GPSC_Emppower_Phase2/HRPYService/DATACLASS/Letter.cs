﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PY.DATACLASS
{
    [Serializable()]
    public class Letter : AbstractObject
    {
        private int letterID = -1;
        private string employeeID = string.Empty;
        private string requestNo = string.Empty;

        private int letterTypeID = -1;
        private string reason = string.Empty;

        private DateTime createdDate = DateTime.MinValue;
        private string status = string.Empty;

        private int letterNo = -1;

        private int embassyID = -1;
        private string embassyName = string.Empty;
        private DateTime vacationBegin = DateTime.MinValue;
        private DateTime vacationEnd = DateTime.MinValue;

        private int printingCount = 0;
        private DateTime updateTime = DateTime.MinValue;
        private string signID = string.Empty;
        private string signName = string.Empty;
        private string employeeName = string.Empty;

        private int embassyID2 = -1;
        private string embassyName2 = string.Empty;
        private bool isUseEmbassy2 = false;
        private DateTime vacationBegin2 = DateTime.MinValue;
        private DateTime vacationEnd2 = DateTime.MinValue;

        private int embassyID3 = -1;
        private string embassyName3 = string.Empty;
        private bool isUseEmbassy3 = false;
        private DateTime vacationBegin3 = DateTime.MinValue;
        private DateTime vacationEnd3 = DateTime.MinValue;

        public Letter()
        {
        }

        #region "Property"
        public int LetterID
        {
            get { return letterID; }
            set { letterID = value; }
        }

        public string EmployeeID
        {
            get { return employeeID; }
            set { employeeID = value; }
        }

        public string RequestNo
        {
            get { return requestNo; }
            set { requestNo = value; }
        }

        public int LetterTypeID
        {
            get { return letterTypeID; }
            set { letterTypeID = value; }
        }

        public string Reason
        {
            get { return reason; }
            set { reason = value; }
        }

        public DateTime CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }

        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        public int LetterNo
        {
            get { return letterNo; }
            set { letterNo = value; }
        }

        public int EmbassyID
        {
            get { return embassyID; }
            set { embassyID = value; }
        }

        public string EmbassyName
        {
            get { return embassyName; }
            set { embassyName = value; }
        }

        public DateTime VacationBegin
        {
            get { return vacationBegin; }
            set { vacationBegin = value; }
        }

        public DateTime VacationEnd
        {
            get { return vacationEnd; }
            set { vacationEnd = value; }
        }

        public int PrintingCount
        {
            get { return printingCount; }
            set { printingCount = value; }
        }

        public DateTime UpdateTime
        {
            get { return updateTime; }
            set { updateTime = value;}
        }

        public string SignID
        {
            get { return signID; }
            set { signID = value; }
        }

        public string SignName
        {
            get { return signName; }
            set { signName = value; }
        }

        public string EmployeeName
        {
            get { return employeeName; }
            set { employeeName = value; }
        }

        public int EmbassyID2
        {
            get { return embassyID2; }
            set { embassyID2 = value; }
        }

        public string EmbassyName2
        {
            get { return embassyName2; }
            set { embassyName2 = value; }
        }

        public bool IsUseEmbassy2
        {
            get { return isUseEmbassy2; }
            set { isUseEmbassy2 = value; }
        }

        public DateTime VacationBegin2
        {
            get { return vacationBegin2; }
            set { vacationBegin2 = value; }
        }

        public DateTime VacationEnd2
        {
            get { return vacationEnd2; }
            set { vacationEnd2 = value; }
        }

        public int EmbassyID3
        {
            get { return embassyID3; }
            set { embassyID3 = value; }
        }

        public string EmbassyName3
        {
            get { return embassyName3; }
            set { embassyName3 = value; }
        }

        public bool IsUseEmbassy3
        {
            get { return isUseEmbassy3; }
            set { isUseEmbassy3 = value; }
        }

        public DateTime VacationBegin3
        {
            get { return vacationBegin3; }
            set { vacationBegin3 = value; }
        }

        public DateTime VacationEnd3
        {
            get { return vacationEnd3; }
            set { vacationEnd3 = value; }
        }

        #endregion
    }
}
