﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PY.DATACLASS
{
    public class PF_Percentage : AbstractObject
    {
        public int PercentageID { get; set; }
        public  decimal Value { get; set; }

        public string ValueText { get; set; }
    }
}
