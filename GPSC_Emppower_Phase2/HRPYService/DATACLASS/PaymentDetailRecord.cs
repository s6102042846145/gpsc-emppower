﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PY.DATACLASS
{
    [Serializable()]
    public class PaymentDetailRecord
    {
        private string __employeeID = "";
        private string __forTimePeriod = "";
        private string __forPayrollPeriod = "";
        private DateTime __date = DateTime.MinValue;
        private string __wageType = "";
        private Decimal __value = 0.0M;
        private Decimal __calculated = 0.0M;
        public PaymentDetailRecord()
        {
        }
        public string EmployeeID
        {
            get
            {
                return __employeeID;
            }
            set
            {
                __employeeID = value;
            }
        }
        public string ForTimePeriod
        {
            get
            {
                return __forTimePeriod;
            }
            set
            {
                __forTimePeriod = value;
            }
        }
        public string ForPayrollPeriod
        {
            get
            {
                return __forPayrollPeriod;
            }
            set
            {
                __forPayrollPeriod = value;
            }
        }
        public DateTime Date
        {
            get
            {
                return __date;
            }
            set
            {
                __date = value;
            }
        }
        public string WageType
        {
            get
            {
                return __wageType;
            }
            set
            {
                __wageType = value;
            }
        }
        public Decimal Value
        {
            get
            {
                return __value;
            }
            set
            {
                __value = value;
            }
        }
        public Decimal Calculated
        {
            get
            {
                return __calculated;
            }
            set
            {
                __calculated = value;
            }
        }
    }
}
