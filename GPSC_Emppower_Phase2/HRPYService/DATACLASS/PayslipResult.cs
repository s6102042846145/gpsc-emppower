﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PY.DATACLASS
{
    [Serializable()]
    public class PayslipResult
    {
        private DataSet __result = new DataSet();
        public PayslipResult()
        {
        }
        public void SetCurrentRT(DataTable table)
        {
            if (__result.Tables.Contains("CURRENT"))
            {
                __result.Tables.Remove("CURRENT");
            }
            table.TableName = "CURRENT";
            __result.Tables.Add(table);
        }
        public void SetRetroRT(DataTable table)
        {
            if (__result.Tables.Contains("RETRO"))
            {
                __result.Tables.Remove("RETRO");
            }
            table.TableName = "RETRO";
            __result.Tables.Add(table);
        }
        public void SetTextNote(DataTable table)
        {
            if (__result.Tables.Contains("TEXTNOTE"))
            {
                __result.Tables.Remove("TEXTNOTE");
            }
            table.TableName = "TEXTNOTE";
            __result.Tables.Add(table);
        }

        //CHAT 2011-09-29 For Inter SAP Return4
        public void SetInterRT(DataTable table)
        {
            if (__result.Tables.Contains("INTER"))
            {
                __result.Tables.Remove("INTER");
            }
            table.TableName = "INTER";
            __result.Tables.Add(table);
        }

        public DataTable Current
        {
            get
            {
                return __result.Tables["CURRENT"];
            }
        }
        public DataTable Retro
        {
            get
            {
                return __result.Tables["RETRO"];
            }
        }
        public DataTable TextNote
        {
            get
            {
                return __result.Tables["TEXTNOTE"];
            }
        }
        //CHAT 2011-09-29 For Inter SAP Return4
        public DataTable Inter
        {
            get
            {
                return __result.Tables["INTER"];
            }
        }

    }
}
