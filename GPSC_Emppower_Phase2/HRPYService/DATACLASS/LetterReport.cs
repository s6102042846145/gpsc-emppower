﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PY.DATACLASS
{
    [Serializable()]
    public class LetterReport : AbstractObject
    {
        #region Private Property
        private string letterRunningNo;
        private string issueDate;
        private string requestorName;
        private string requestorPosition;
        private char gender;
        private string employeeID;
        private string hiringDate;
        private string beginDate;
        private string endDate;
        private string embassy;
        private string embassyAddress;
        private string signName;
        private string signPosition;
        private string salary;
        private string recurringWageData;
        private string orgLevelManager;
        private string orgLevelVP;
        private string orgLevelSVP;
        private string orgLevelEVP;
        private string duringDate;
        private string diffissueYear;
        private string diffissueMonth;
        private string signName2 = "";
        private string signPosition2 = "";
        private string signName3 = "";
        private string signPosition3 = "";
        private string signName4 = "";
        private string signPosition4 = "";
        #endregion

        public string PathFile;


        #region Public Property
        public string LetterRunningNo
        {
            get { return letterRunningNo; }
            set
            {
                letterRunningNo = value;
            }
        }

        public string IssueDate
        {
            get { return issueDate; }
            set
            {
                issueDate = value;
            }
        }

        public string RequestorName
        {
            get { return requestorName; }
            set
            {
                requestorName = value;
            }
        }

        public string RequestorPosition
        {
            get { return requestorPosition; }
            set
            {
                requestorPosition = value;
            }
        }

        public char Gender
        {
            get { return gender; }
            set
            {
                gender = value;
            }
        }

        public string EmployeeID
        {
            get { return employeeID; }
            set
            {
                employeeID = value;
            }
        }

        public string HiringDate
        {
            get { return hiringDate; }
            set
            {
                hiringDate = value;
            }
        }

        public string BeginDate
        {
            get { return beginDate; }
            set
            {
                beginDate = value;
            }
        }

        public string EndDate
        {
            get { return endDate; }
            set
            {
                endDate = value;
            }
        }

        public string DuringDate
        {
            get { return duringDate; }
            set
            {
                duringDate = value;
            }
        }

        public string Embassy
        {
            get { return embassy; }
            set
            {
                embassy = value;
            }
        }

        public string EmbassyAddress
        {
            get { return embassyAddress; }
            set
            {
                embassyAddress = value;
            }
        }

        public string SignName
        {
            get { return signName; }
            set
            {
                signName = value;
            }
        }

        public string SignPosition
        {
            get { return signPosition; }
            set
            {
                signPosition = value;
            }
        }

        public string SignName2
        {
            get { return signName2; }
            set
            {
                signName2 = value;
            }
        }

        public string SignPosition2
        {
            get { return signPosition2; }
            set
            {
                signPosition2 = value;
            }
        }

        public string SignName3
        {
            get { return signName3; }
            set
            {
                signName3 = value;
            }
        }

        public string SignPosition3
        {
            get { return signPosition3; }
            set
            {
                signPosition3 = value;
            }
        }

        public string SignName4
        {
            get { return signName4; }
            set
            {
                signName4 = value;
            }
        }

        public string SignPosition4
        {
            get { return signPosition4; }
            set
            {
                signPosition4 = value;
            }
        }

        public string Salary
        {
            get { return salary; }
            set
            {
                salary = value;
            }
        }

        public string RecurringWageData
        {
            get { return recurringWageData; }
            set
            {
                recurringWageData = value;
            }
        }

        public string OrgLevelManager
        {
            get { return orgLevelManager; }
            set { orgLevelManager = value; }
        }

        public string OrgLevelVP
        {
            get { return orgLevelVP; }
            set { orgLevelVP = value; }
        }

        public string OrgLevelSVP
        {
            get { return orgLevelSVP; }
            set { orgLevelSVP = value; }
        }

        public string OrgLevelEVP
        {
            get { return orgLevelEVP; }
            set { orgLevelEVP = value; }
        }

        public string DiffissueYear
        {
            get { return diffissueYear; }
            set { diffissueYear = value; }
        }

        public string DiffissueMonth
        {
            get { return diffissueMonth; }
            set { diffissueMonth = value; }
        }
        #endregion

    }
}
