﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PY.DATACLASS
{
    public class PF_Graph : AbstractObject
    {
        public int FundGraphID { get; set; }
        public int PeriodYear { get; set; }
        public string FundID { get; set; }
        public virtual decimal Quarter1 { get; set; }
        public virtual decimal Quarter2 { get; set; }
        public virtual decimal Quarter3 { get; set; }
        public virtual decimal Quarter4 { get; set; }
    }
}
