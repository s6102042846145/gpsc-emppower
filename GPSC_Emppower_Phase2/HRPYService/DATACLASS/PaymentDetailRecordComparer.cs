﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PY.DATACLASS
{
    [Serializable()]
    public class PaymentDetailRecordComparer : IComparer<PaymentDetailRecord>
    {
        public PaymentDetailRecordComparer()
        {
        }


        #region IComparer<PaymentDetailRecord> Members

        public int Compare(PaymentDetailRecord x, PaymentDetailRecord y)
        {
            int nReturn = 0;//x.EmployeeID.CompareTo(y.EmployeeID);
            if (nReturn == 0)
            {
                nReturn = x.Date.CompareTo(y.Date);
                if (nReturn == 0)
                {
                    nReturn = x.WageType.CompareTo(y.WageType);
                    if (nReturn == 0)
                    {
                        nReturn = x.ForPayrollPeriod.CompareTo(y.ForPayrollPeriod);
                    }
                }
            }
            return nReturn;
        }

        #endregion
    }
}
