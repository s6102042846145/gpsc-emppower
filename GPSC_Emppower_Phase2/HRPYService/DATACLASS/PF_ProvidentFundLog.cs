﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PY.DATACLASS
{
    public class PF_ProvidentFundLog : AbstractObject
    {
        public string RequestNo { get; set; }
        public string EmployeeID { get; set; }
        public string FundID { get; set; }
        public decimal EmployeeSaving { get; set; }

        public string EmployeeSavingText { get; set; }
        public decimal CompanySaving { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public string Status { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime EffectiveDate { get; set; }

        public string ErrorText { get; set; }

        //add 21/12/2018
        public  int Type { get; set; }

        public string EmpGroup { get; set; }
        public string OldFundID { get; set; }
        public decimal OldEmployeeSaving { get; set; }


    }
}
