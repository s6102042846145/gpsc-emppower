﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Data;
using System.Configuration;
using System.Reflection;
using ESS.DATA;
using ESS.EMPLOYEE;
using ESS.HR.PY.CONFIG;
using ESS.HR.PY.DATACLASS;
using ESS.HR.PY.INFOTYPE;
using ESS.SHAREDATASERVICE;
using SAPInterfaceExt;
using SAP.Connector;
using ESS.PORTALENGINE;
using ESS.HR.PA.INFOTYPE;
using ESS.EMPLOYEE.CONFIG.TM;
using System.Linq;
using ClosedXML.Excel;
using ESS.EMPLOYEE.DATACLASS;

namespace ESS.HR.PY
{
    public class HRPYManagement
    {
        #region Constructor
        private static CultureInfo oCL = new CultureInfo("en-US");
        private HRPYManagement()
        {
        }

        #endregion

        #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        private static Dictionary<string, HRPYManagement> Cache = new Dictionary<string, HRPYManagement>();

        private static string ModuleID = "ESS.HR.PY";
        public string CompanyCode { get; set; }

        public static HRPYManagement CreateInstance(string oCompanyCode)
        {
            HRPYManagement oHRPYManagement = new HRPYManagement()
            {
                CompanyCode = oCompanyCode
            };
            return oHRPYManagement;
        }
        #endregion MultiCompany  Framework

        public string TAXDEDUCTIONWEB
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "TAXDEDUCTIONWEB");
            }
        }

        public string FORWARD_PAYSLIP_YEAR
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "FORWARD_PAYSLIP_YEAR");
            }
        }

        public string BACKWARD_PAYSLIP_YEAROFORIGIN
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BACKWARD_PAYSLIP_YEAROFORIGIN");
            }
        }

        public string DEFAULT_PAYMENT_DATE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "DEFAULT_PAYMENT_DATE");
            }
        }

        public string ROOT_ORGANIZATION
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ROOT_ORGANIZATION");
            }
        }

        public string MONTHLYLETTERBEFORE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "MONTHLYLETTERBEFORE");
            }
        }

        public string LETTERTYPESIGNBYMANAGER
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "LETTERTYPESIGNBYMANAGER");
            }
        }

        public string ALLOCATEBUDGETKEY
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ALLOCATEBUDGET_KEY");
            }
        }

        public string KeySecret
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "GPSC_ENCRYPTION_KEY");
            }
        }

        public string ALLOCATEBUDGET_LINK
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ALLOCATEBUDGET_LINK");
            }
        }

        public TaxAllowance GetTaxAllowanceData(string EmployeeID, DateTime checkDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ERPData.GetTaxAllowanceData(EmployeeID, checkDate);
        }

        public void SaveTaxallowanceData(TaxAllowance saved)
        {
            ServiceManager.CreateInstance(CompanyCode).ERPData.SaveTaxallowanceData(saved);
        }

        public List<SpouseAllowance> GetAllSpouseAllowanceDropdownData()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAllSpouseAllowanceDropdownData();
        }

        public INFOTYPE0366 GetProvidentFund(string EmployeeID, DateTime CheckDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ERPData.GetProvidentFund(EmployeeID, CheckDate);
        }

        public List<PF_Graph> GetFundQuarter(String EmployeeID, String KeyType, int KeyYear, String KeyValue)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetFundQuarter(EmployeeID, KeyType, KeyYear, KeyValue);
        }


        public List<PF_PeriodSetting> GetPeriodSetting(String EmployeeID, String KeyType, int KeyYear, String KeyValue)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetPeriodSetting(EmployeeID, KeyType, KeyYear, KeyValue);
        }

        public List<PF_ProvidentFundDetail> GetTextProvidentFundDetail(String KeyType, String KeyCode, String KeyValue)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetTextProvidentFundDetail(KeyType, KeyCode, KeyValue);
        }

        public void MarkUpdate(string EmployeeID, string DataCategory, string RequestNo, bool isMarkIn)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.MarkUpdate(EmployeeID, DataCategory, RequestNo, isMarkIn);
        }

        public bool CheckMark(string EmployeeID, string DataCategory)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.CheckMark(EmployeeID, DataCategory);
        }

        public bool CheckMark(string EmployeeID, string DataCategory, string ReqNo)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.CheckMark(EmployeeID, DataCategory, ReqNo);
        }

        public List<PF_Percentage> GetPF_Percentage()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetPF_Percentage();
        }

        public List<PF_Selection> GetPF_Selection()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetPF_Selection();
        }


        public void SaveProvidentFundLog(PF_ProvidentFundLog data)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveProvidentFundLog(data);
        }

        public List<PF_PeriodSetting> GetEffectiveDate()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetEffectiveDate();
        }

        public List<TimeAwareLink> GetAllTimeAwareLink()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetAllTimeAwareLink();
        }

        public void UpdateTimeAwareLink(int LinkID, String BeginDate, String EndDate, String EmpID)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.UpdateTimeAwareLink(LinkID, BeginDate, EndDate, EmpID);
        }

        public void SaveProvidentFund(INFOTYPE0366 data)
        {
            ServiceManager.CreateInstance(CompanyCode).ERPData.SaveProvidentFund(data);
        }

        public List<INFOTYPE0021> GetProvidentFundMember(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).ERPData.GetProvidentFundMember(EmployeeID);
        }


        public void DeleteProvidentFundMember(List<INFOTYPE0021> data)
        {
            ServiceManager.CreateInstance(CompanyCode).ERPData.DeleteProvidentFundMember(data);
        }

        public void SaveProvidentFundMember(List<INFOTYPE0021> data)
        {
            ServiceManager.CreateInstance(CompanyCode).ERPData.SaveProvidentFundMember(data);
        }

        private int ComparePeriod(PayslipPeriodSetting x, PayslipPeriodSetting y)
        {
            if (x.PeriodYear > y.PeriodYear)
            {
                return 1;
            }
            else if (x.PeriodYear == y.PeriodYear)
            {
                if (x.PeriodMonth > y.PeriodMonth)
                    return 1;
                else if (x.PeriodMonth == y.PeriodMonth)
                {
                    if (x.OffCycleDate > y.OffCycleDate)
                        return 1;
                    else if (x.OffCycleDate == y.OffCycleDate)
                        return 0;
                    else
                        return -1;
                }
                else
                    return -1;
            }
            else
                return -1;
        }

        public List<string> GetPaySlipYear(DateTime HiringDate)
        {
            List<string> lstYear = new List<string>();

            foreach (PayslipPeriodSetting setting in GetActivePeriod())
            {
                if (!lstYear.Exists(delegate (string year) { return year == setting.PeriodYear.ToString(); }))
                {
                    if (setting.PeriodYear >= Convert.ToInt32(HiringDate.ToString("yyyy", oCL)))
                        lstYear.Add(setting.PeriodYear.ToString());
                }
            }

            return lstYear;
        }


        public List<PayslipPeriodSetting> GetPaySlipMonthByYear(int year, DateTime HiringDate)
        {
            List<PayslipPeriodSetting> oResult = new List<PayslipPeriodSetting>();

            List<PayslipPeriodSetting> periodSettings = GetPeriodSettingByYear(year);

            foreach (PayslipPeriodSetting ps in periodSettings)
            {
                if (ps.Active)
                {
                    //If selected year is in HiringDate
                    if (ps.PeriodYear.ToString() == HiringDate.ToString("yyyy", oCL) && HiringDate != DateTime.MinValue)
                    {
                        if (ps.PeriodMonth >= HiringDate.Month)
                            oResult.Add(ps);
                    }
                    else
                        oResult.Add(ps);
                }
            }

            //oResult.AddRange(periodSettings);

            if (oResult.Count > 0)
                oResult.Sort(ComparePeriod);

            return oResult;
        }

        public List<PayslipPeriodSetting> GetPaySlipMonthByYearForAdmin(int year, DateTime HiringDate)
        {
            List<PayslipPeriodSetting> oResult = new List<PayslipPeriodSetting>();
            List<PayslipPeriodSetting> periodSettings = GetPeriodSettingByYearForAdmin(year);
            oResult.Clear();

            foreach (PayslipPeriodSetting ps in periodSettings)
            {
                if (ps.Active)
                {
                    //If selected year is in HiringDate
                    if (ps.PeriodYear.ToString() == HiringDate.ToString("yyyy", oCL) && HiringDate != DateTime.MinValue)
                    {
                        if (ps.PeriodMonth >= HiringDate.Month)
                            oResult.Add(ps);
                    }
                    else
                        oResult.Add(ps);
                }
            }
            if (oResult.Count > 0)
                oResult.Sort(ComparePeriod);

            return oResult;
        }

        public List<PayslipPeriodSetting> GetActivePeriod()
        {
            //return HR.PY.ServiceManager.HRPYConfig.GetActivePayslipPeriod();
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetActivePayslipPeriod();
        }

        public List<PayslipPeriodSetting> GetPeriodSettingByYearForAdmin(int year)
        {
            //return HR.PY.ServiceManager.HRPYConfig.GetPeriodSettingByYearForAdmin(year);
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetPeriodSettingByYearForAdmin(year);
        }

        public List<PayslipPeriodSetting> GetPeriodSettingByYear(int year)
        {
            //return HR.PY.ServiceManager.HRPYConfig.GetPeriodSettingByYear(year);
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetPeriodSettingByYear(year);
        }

        public List<PaySlipReportWageDataMerge> GetPaySlipReportData(string EmployeeID, string LanguageCode, PayslipPeriodSetting oPeriod)
        {
            //1. Get all payslip data from SAP by period
            PayslipResult oResult = GetPayslip(EmployeeID, oPeriod);

            //2. Prepare and seperate data using WageType,WebWageType and others criteria
            List<PaySlipReportWageData> payslipReportDatas = GenData(EmployeeID, oResult, oPeriod, LanguageCode);

            //3. Seperate data by report type
            List<PaySlipReportWageData> CurrentIncome = payslipReportDatas.FindAll(delegate (PaySlipReportWageData ps) { return (ps.ReportType == (int)eReportType.Income); });
            List<PaySlipReportWageData> CurrentOutcome = payslipReportDatas.FindAll(delegate (PaySlipReportWageData ps) { return (ps.ReportType == (int)eReportType.Outcome); });
            CurrentIncome.Sort(new PaySlipReportWageDataComparer());
            CurrentOutcome.Sort(new PaySlipReportWageDataComparer());
            List<PaySlipReportWageData> RetroIncome = payslipReportDatas.FindAll(delegate (PaySlipReportWageData ps) { return (ps.ReportType == (int)eReportType.RetroIncome); });
            List<PaySlipReportWageData> RetroOutcome = payslipReportDatas.FindAll(delegate (PaySlipReportWageData ps) { return (ps.ReportType == (int)eReportType.RetroOutcome); });
            RetroIncome.Sort(new PaySlipReportWageDataComparer());
            RetroOutcome.Sort(new PaySlipReportWageDataComparer());
            decimal IncomeCollection = 0;
            decimal TaxCollection = 0;
            decimal TotalIncome = 0;
            decimal TotalOutcome = 0;
            decimal NetFromSAP = 0;
            decimal ProvidentFund = 0;
            decimal SocialSecurity = 0;
            List<PaySlipReportWageData> Others = payslipReportDatas.FindAll(delegate (PaySlipReportWageData ps) { return (ps.ReportType == (int)eReportType.Others); });
            foreach (PaySlipReportWageData data in Others)
            {
                if (data.WageTypeCode == "IncomeCollection")
                    IncomeCollection = data.Amount;
                else if (data.WageTypeCode == "TaxCollection")
                    TaxCollection = data.Amount;
                else if (data.WageTypeCode == "NetFromSAP")
                    NetFromSAP = data.Amount;
                else if (data.WageTypeCode == "ProvidentFund")
                    ProvidentFund = data.Amount;
                else if (data.WageTypeCode == "SocialSecurity")
                    SocialSecurity = data.Amount;
            }



            //4. Add dummy row to left(Income) and right(Deduction) data
            //to make it have the same number of row
            int cntRow = ((CurrentOutcome.Count + RetroOutcome.Count) > (CurrentIncome.Count + RetroIncome.Count) ? (CurrentOutcome.Count + RetroOutcome.Count) : (CurrentIncome.Count + RetroIncome.Count));


            List<PaySlipReportTaxAllowance> TaxAllowance = GenTaxData(EmployeeID, oPeriod.OffCycleDate, cntRow);
            if (cntRow < TaxAllowance.Count)
                cntRow = TaxAllowance.Count;
            AddDummyData(CurrentIncome, RetroIncome, cntRow);
            AddDummyData(CurrentOutcome, RetroOutcome, cntRow);

            //5. Merge all data to make it easy binding to table
            List<PaySlipReportWageDataMerge> merges = new List<PaySlipReportWageDataMerge>();
            PaySlipReportWageDataMerge merge;
            for (int i = 0; i < cntRow; i++)
            {
                merge = new PaySlipReportWageDataMerge();
                merge.SortNumber1 = CurrentIncome[i].SortNumber;
                merge.WageTypeCode1 = CurrentIncome[i].WageTypeCode;
                //merge.WageTypeDescription1 = CurrentIncome[i].WageTypeDescription;
                merge.WageTypeDescription1 = CurrentIncome[i].WageTypeDescription + GetWageTypeDescInclude(EmployeeID, CurrentIncome[i].WageTypeCode, oPeriod.OffCycleDate, LanguageCode);
                merge.Amount1 = CurrentIncome[i].Amount;
                TotalIncome += merge.Amount1;
                merge.Currency1 = CurrentIncome[i].Currency;
                merge.SortNumber2 = CurrentOutcome[i].SortNumber;
                merge.WageTypeCode2 = CurrentOutcome[i].WageTypeCode;
                //merge.WageTypeDescription2 = CurrentOutcome[i].WageTypeDescription;
                merge.WageTypeDescription2 = CurrentOutcome[i].WageTypeDescription + GetWageTypeDescInclude(EmployeeID, CurrentOutcome[i].WageTypeCode, oPeriod.OffCycleDate, LanguageCode);
                merge.Amount2 = (-1) * CurrentOutcome[i].Amount;
                TotalOutcome += merge.Amount2;
                merge.Currency2 = CurrentOutcome[i].Currency;
                merge.Code = TaxAllowance[i].Code;
                merge.Description = TaxAllowance[i].Description;
                merge.Value = TaxAllowance[i].Value;
                merges.Add(merge);
            }

            //6. Updated summary field to the first row of merge data
            if (merges.Count > 0)
            {
                decimal dNet = (TotalIncome - TotalOutcome) < 0 ? 0 : (TotalIncome - TotalOutcome);
                merges[0].TotalIncome = TotalIncome;
                merges[0].TotalOutcome = TotalOutcome;
                merges[0].Net = dNet;
                merges[0].IncomeCollection = IncomeCollection;
                merges[0].TaxCollection = TaxCollection;
                merges[0].NetFromSAP = NetFromSAP;
                merges[0].IsMisMatch = dNet == NetFromSAP ? false : true;
                merges[0].TextNote = oResult.TextNote;
                merges[0].ProvidentFund = ProvidentFund;
                merges[0].SocialSecurity = SocialSecurity;
            }

            return merges;
        }

        public string GetWageTypeDescInclude(string EmployeeID, string WageType, DateTime dtPeriod, string LanguageCode)
        {
            string retText = "";
            if (WageType == "5132" || WageType == "5136" || WageType == "5137" || WageType == "5138" || WageType == "5139")
            {
                Dictionary<string, INFOTYPE0015> oWageDictPA0015 = GetAssignmentNumberWageTypePA0015(EmployeeID, dtPeriod, WageType);
                if (oWageDictPA0015.Count > 0 && oWageDictPA0015.ContainsKey(WageType))
                {
                    foreach(var item in oWageDictPA0015)
                    {
                        if(retText == "")
                        {
                            retText = " " + oWageDictPA0015[item.Key].AssignmentNumber;
                        }
                        else
                        {
                            retText += " , " + oWageDictPA0015[item.Key].AssignmentNumber;
                        }
                    }
                    //retText = " " + oWageDictPA0015[WageType].AssignmentNumber;
                }
            }
            else if (WageType == "1010")
            {
                string sYear = (dtPeriod.Year + 543).ToString();
                retText = " " + GetCommonText("SHORTMONTH", LanguageCode, dtPeriod.Month.ToString().PadLeft(2, '0')) + " " + sYear.Substring(sYear.Length - 2);
            }
            else if (WageType == "6001")
            {
                Dictionary<string, INFOTYPE0015> oWageDictPA0267 = GetAssignmentNumberWageTypePA0267(EmployeeID, dtPeriod, WageType);
                if (oWageDictPA0267.Count > 0 && oWageDictPA0267.ContainsKey(WageType))
                {
                    foreach (var item in oWageDictPA0267)
                    {
                        if (retText == "")
                        {
                            retText = " " + oWageDictPA0267[item.Key].AssignmentNumber;
                        }
                        else
                        {
                            retText += " , " + oWageDictPA0267[item.Key].AssignmentNumber;
                        }
                    }
                    //retText = " " + oWageDictPA0267[WageType].AssignmentNumber;
                }
            }
            else
                retText = "";

            return retText;
        }

        public PayslipPeriodSetting GetPeriod(int periodID)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetPeriod(periodID);
        }

        public PayslipResult GetPayslip(string EmployeeID, PayslipPeriodSetting Period)
        {
            return ServiceManager.CreateInstance(CompanyCode).ERPData.GetPayslip(EmployeeID, Period);
        }

        private List<PaySlipReportTaxAllowance> GenTaxData(string EmployeeID, DateTime CheckDate, int cntRow)
        {
            TaxAllowance tax = GetTaxAllowanceData(EmployeeID, CheckDate);
            List<PaySlipReportTaxAllowance> psReports = new List<PaySlipReportTaxAllowance>();
            PaySlipReportTaxAllowance psTax;

            //101 สถานภาพสมรส
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "101";
            SpouseAllowance claimType = GetSpouseAllowance(tax.SpouseAllowance);
            psTax.Value = claimType.Key;
            psReports.Add(psTax);
            //102	บุตรที่มีสิทธิ์ลดหย่อนภาษี / คน
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "102";
            psTax.Value = (tax.ChdAllow == 0 ? string.Empty : tax.ChdAllow.ToString());
            psReports.Add(psTax);
            //103	บุตรที่มีสิทธิ์ลดหย่อนการศึกษา / คน
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "103";
            psTax.Value = (tax.ChdEduAllow == 0 ? string.Empty : tax.ChdEduAllow.ToString());
            psReports.Add(psTax);
            //104	บุตรพิการที่มีสิทธิ์ลดหย่อนภาษี / คน
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "104";
            psTax.Value = (tax.ChdDisAllow == 0 ? string.Empty : tax.ChdDisAllow.ToString());
            psReports.Add(psTax);
            //105	เบี้ยประกันชีวิตพนักงาน
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "105";
            psTax.Value = (tax.LifeIns == 0 ? string.Empty : tax.LifeIns.ToString());
            psReports.Add(psTax);
            //106	เบี้ยประกันชีวิตคู่สมรสไม่มีรายได้
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "106";
            psTax.Value = (tax.SpouseIns == 0 ? string.Empty : tax.SpouseIns.ToString());
            psReports.Add(psTax);
            //107	กองทุนเงินบำนาญ
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "107";
            psTax.Value = (tax.PensionFundInvestment == 0 ? string.Empty : tax.PensionFundInvestment.ToString());
            psReports.Add(psTax);
            //108	กองทุนRMF
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "108";
            psTax.Value = (tax.MFund == 0 ? string.Empty : tax.MFund.ToString());
            psReports.Add(psTax);
            //109	กองทุนLTF
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "109";
            psTax.Value = (tax.LongTerm == 0 ? string.Empty : tax.LongTerm.ToString());
            psReports.Add(psTax);
            //110	ดอกเบี้ยเงินกู้ที่อยู่อาศัย
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "110";
            psTax.Value = (tax.Mortgage == 0 ? string.Empty : tax.Mortgage.ToString());
            psReports.Add(psTax);
            //111	ลดหย่อนบิดา
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "111";
            psTax.Value = (tax.AllowFather == 0 ? string.Empty : tax.AllowFather.ToString());
            psReports.Add(psTax);
            //112	ลดหย่อนมารดา
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "112";
            psTax.Value = (tax.AllowMother == 0 ? string.Empty : tax.AllowMother.ToString());
            psReports.Add(psTax);
            //113	ลดหย่อนบิดาคู่สมรส
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "113";
            psTax.Value = (tax.SpAllowFather == 0 ? string.Empty : tax.SpAllowFather.ToString());
            psReports.Add(psTax);
            //114	ลดหย่อนมารดาคู่สมรส
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "114";
            psTax.Value = (tax.SpAllowMother == 0 ? string.Empty : tax.SpAllowMother.ToString());
            psReports.Add(psTax);
            //115	เบี้ยประกันชีวิตบิดา
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "115";
            psTax.Value = (tax.FatherIns == 0 ? string.Empty : tax.FatherIns.ToString());
            psReports.Add(psTax);
            //116	เบี้ยประกันชีวิตมารดา
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "116";
            psTax.Value = (tax.MotherIns == 0 ? string.Empty : tax.MotherIns.ToString());
            psReports.Add(psTax);
            //117	เบี้ยประกันชีวิตบิดาคู่สมรส
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "117";
            psTax.Value = (tax.SpFatherIns == 0 ? string.Empty : tax.SpFatherIns.ToString());
            psReports.Add(psTax);
            //118	เบี้ยประกันชีวิตมารดาคู่สมรส  
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "118";
            psTax.Value = (tax.SpMotherIns == 0 ? string.Empty : tax.SpMotherIns.ToString());
            psReports.Add(psTax);
            //119	เงินบริจาค 
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "119";
            psTax.Value = (tax.Charity == 0 ? string.Empty : tax.Charity.ToString());
            psReports.Add(psTax);
            //120	เงินสนับสนุนการศึกษา
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "120";
            psTax.Value = (tax.CharityEdu == 0 ? string.Empty : tax.CharityEdu.ToString());
            psReports.Add(psTax);
            //121	เงินสนับสนุนการกีฬา
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "121";
            psTax.Value = (tax.SportContribution == 0 ? string.Empty : tax.SportContribution.ToString());
            psReports.Add(psTax);
            //122	ประกันชีวิตแบบบำนาญ
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "122";
            psTax.Value = (tax.LifeInsurancePension == 0 ? string.Empty : tax.LifeInsurancePension.ToString());
            psReports.Add(psTax);
            //123	ท่องเที่ยวภายในประเทศ
            psTax = new PaySlipReportTaxAllowance();
            psTax.Code = "123";
            psTax.Value = (tax.TourismExemption == 0 ? string.Empty : tax.TourismExemption.ToString());
            psReports.Add(psTax);

            List<PaySlipReportTaxAllowance> oReturn = new List<PaySlipReportTaxAllowance>();
            oReturn = psReports.FindAll(delegate (PaySlipReportTaxAllowance pt) { return !string.IsNullOrEmpty(pt.Value.Trim()); });
            oReturn.Sort(new PaySlipReportTaxAllowanceComparer());
            if (oReturn.Count < cntRow)
            {
                int loop = (cntRow - oReturn.Count);
                for (int i = 0; i < loop; i++)
                {
                    psTax = new PaySlipReportTaxAllowance();
                    psTax.Value = string.Empty;
                    psTax.Code = string.Empty;
                    oReturn.Add(psTax);
                }
            }

            return oReturn;
        }

        private List<PaySlipReportWageData> GenData(string EmployeeID, PayslipResult psResult, PayslipPeriodSetting psPeriod, string LanguageCode)
        {
            List<WageType> wagetypeList = GetWageTypeAll();
            List<PaySlipReportWageData> psReports = new List<PaySlipReportWageData>();
            List<PaySlipReportWageData> psReportsCurrent = new List<PaySlipReportWageData>();
            List<PaySlipReportWageData> psReportsRetro = new List<PaySlipReportWageData>();

            //Current
            psReportsCurrent = GenSubData(EmployeeID, wagetypeList, psResult.Current.Rows, psReportsCurrent, psPeriod, (int)eReportType.Income, (int)eReportType.Outcome, LanguageCode);
            //Retro
            psReportsRetro = GenSubData(EmployeeID, wagetypeList, psResult.Retro.Rows, psReportsRetro, psPeriod, (int)eReportType.RetroIncome, (int)eReportType.RetroOutcome, LanguageCode);

            psReports.AddRange(psReportsCurrent);
            psReports.AddRange(psReportsRetro);

            return psReports;
        }


        private List<PaySlipReportWageData> GenSubData(string employeeID, List<WageType> wagetypeList, DataRowCollection drs, List<PaySlipReportWageData> psReports, PayslipPeriodSetting psPeriod, int IncomeType, int OutcomeType, string LanguageCode)
        {
            PaySlipReportWageData psData;
            WageType wagetype;
            int SortNumber = 0;

            foreach (DataRow dr in drs)
            {
                //If AccumType of this data is Monthly
                if (dr["AccumType"].ToString() == "M")
                {
                    psData = new PaySlipReportWageData();
                    wagetype = wagetypeList.Find(delegate (WageType wt) { return (wt.Code.ToString() == dr["WageType"].ToString()); });

                    //If this data is existing in table WageType
                    if (wagetype != null)
                    {
                        //This entry is not Retro and is not Gross-up data
                        //If not, keep going
                        //If yes, ignore this entry
                        if (!(IncomeType == (int)eReportType.RetroIncome && OutcomeType == (int)eReportType.RetroOutcome && wagetype.IsGrossUp))
                        {
                            Decimal dec = 0;
                            dec = Convert.ToDecimal(dr["Currencyamt"]);

                            #region Wagetype /401
                            /*
                        ถ้าไม่ใช่ offcyle ให้ assign กับ WebWageType? 31
                        ถ้าเป็น offcycle และ อยู่ในช่วง 1-15 มกรา ให้ assign กับ WebWageType? 32 
                        */
                            if (wagetype.Code == "/401")
                            {
                                DateTime dtRange1 = DateTime.ParseExact("0101" + psPeriod.PeriodYear.ToString(), "ddMMyyyy", oCL);
                                DateTime dtRange2 = DateTime.ParseExact("1501" + psPeriod.PeriodYear.ToString(), "ddMMyyyy", oCL);
                                SortNumber = 31;

                                if (psPeriod.IsOffCycle && (psPeriod.OffCycleDate >= dtRange1 && psPeriod.OffCycleDate <= dtRange2))
                                    SortNumber = 32;

                                WebWageType wwt = GetWebWageTypeByCode(SortNumber);
                                psData.ReportType = (wwt.DeductionFlag ? OutcomeType : IncomeType);
                                psData.SortNumber = wwt.WebCode;
                                psData.WageTypeCode = "/401";
                                if (psData.ReportType == (int)eReportType.RetroIncome || psData.ReportType == (int)eReportType.RetroOutcome)
                                {
                                    string[] Period = dr["PERIOD"].ToString().Split(':');
                                    if (Period.Length > 1)
                                        psData.WageTypeDescription = string.Format("{0} (Off-Cycle: {1})", GetCommonText("WEBWAGETYPE", LanguageCode, SortNumber.ToString()), DateTime.ParseExact(Period[1], "yyyyMMdd", oCL).ToString("dd/MM/yyyy", oCL));
                                    else
                                        psData.WageTypeDescription = string.Format("{0} ({1})", GetCommonText("WEBWAGETYPE", LanguageCode, SortNumber.ToString()), DateTime.ParseExact(dr["PERIOD"].ToString(), "yyyyMM", oCL).ToString("MM/yyyy", oCL));
                                }
                                else
                                    psData.WageTypeDescription = GetCommonText("WEBWAGETYPE", LanguageCode, SortNumber.ToString());
                                psData.Amount = wagetype.MultiplierAmount * dec;
                                psData.Currency = GetCommonText("CURRENCY", LanguageCode, dr["Currencyunit"].ToString());

                                int MatchIndex = psReports.FindIndex(delegate (PaySlipReportWageData ps) { return (ps.SortNumber == SortNumber && ps.WageTypeDescription == psData.WageTypeDescription); });
                                if (MatchIndex > -1)
                                    psReports[MatchIndex].Amount += (wagetype.MultiplierAmount * dec);
                                else
                                    psReports.Add(psData);
                            }
                            #endregion

                            #region Wagetype /Z02
                            /*
                            ถ้าเป็นเงิน + ให้ assign กับ WebWageType? 11
                            ถ้าเป็นเงิน – ให้ assign กับ WebWageType? 201
                            */
                            else if (wagetype.Code == "/Z02")
                            {
                                SortNumber = 11;
                                if (Convert.ToDecimal(dr["Currencyamt"]) >= 0)
                                    SortNumber = 11;
                                else
                                    SortNumber = 201;

                                WebWageType wwt = GetWebWageTypeByCode(SortNumber);
                                psData.ReportType = (wwt.DeductionFlag ? OutcomeType : IncomeType);
                                psData.SortNumber = wwt.WebCode;
                                psData.WageTypeCode = "/Z02";
                                if (psData.ReportType == (int)eReportType.RetroIncome || psData.ReportType == (int)eReportType.RetroOutcome)
                                {
                                    string[] Period = dr["PERIOD"].ToString().Split(':');
                                    if (Period.Length > 1)
                                        psData.WageTypeDescription = string.Format("{0} (Off-Cycle: {1})", GetCommonText("WEBWAGETYPE", LanguageCode, SortNumber.ToString()), DateTime.ParseExact(Period[1], "yyyyMMdd", oCL).ToString("dd/MM/yyyy", oCL));
                                    else
                                        psData.WageTypeDescription = string.Format("{0} ({1})", GetCommonText("WEBWAGETYPE", LanguageCode, SortNumber.ToString()), DateTime.ParseExact(dr["PERIOD"].ToString(), "yyyyMM", oCL).ToString("MM/yyyy", oCL));
                                }
                                else
                                    psData.WageTypeDescription = GetCommonText("WEBWAGETYPE", LanguageCode, SortNumber.ToString());
                                psData.Amount = wagetype.MultiplierAmount * dec;
                                psData.Currency = GetCommonText("CURRENCY", LanguageCode, dr["Currencyunit"].ToString());

                                int MatchIndex = psReports.FindIndex(delegate (PaySlipReportWageData ps) { return (ps.SortNumber == SortNumber && ps.WageTypeDescription == psData.WageTypeDescription); });
                                if (MatchIndex > -1)
                                    psReports[MatchIndex].Amount += (wagetype.MultiplierAmount * dec);
                                else
                                    psReports.Add(psData);
                            }
                            #endregion

                            #region Others
                            else if (wagetype.SortNumber > 0)
                            {
                                WebWageType wwt = GetWebWageTypeByCode(wagetype.SortNumber);
                                psData.ReportType = (wwt.DeductionFlag ? OutcomeType : IncomeType);
                                psData.SortNumber = wwt.WebCode;
                                psData.WageTypeCode = wagetype.Code;
                                if (psData.ReportType == (int)eReportType.RetroIncome || psData.ReportType == (int)eReportType.RetroOutcome)
                                {
                                    string[] Period = dr["PERIOD"].ToString().Split(':');
                                    if (Period.Length > 1)
                                        psData.WageTypeDescription = string.Format("{0} (Off-Cycle: {1})", GetCommonText("WEBWAGETYPE", LanguageCode, wagetype.SortNumber.ToString()), DateTime.ParseExact(Period[1], "yyyyMMdd", oCL).ToString("dd/MM/yyyy", oCL));
                                    else
                                        psData.WageTypeDescription = string.Format("{0} ({1})", GetCommonText("WEBWAGETYPE", LanguageCode, wagetype.SortNumber.ToString()), DateTime.ParseExact(dr["PERIOD"].ToString(), "yyyyMM", oCL).ToString("MM/yyyy", oCL));
                                }
                                else
                                    psData.WageTypeDescription = GetCommonText("WEBWAGETYPE", LanguageCode, wagetype.SortNumber.ToString());

                                psData.Amount = (wagetype.MultiplierAmount * dec);
                                psData.Currency = GetCommonText("CURRENCY", LanguageCode, dr["Currencyunit"].ToString());

                                //If this row is have the same SortNumber in existing data in psReports
                                int MatchIndex = psReports.FindIndex(delegate (PaySlipReportWageData ps) { return (ps.SortNumber == wagetype.SortNumber && ps.WageTypeDescription == psData.WageTypeDescription); });
                                if (MatchIndex > -1)
                                {
                                    psReports[MatchIndex].Amount += (wagetype.MultiplierAmount * dec);
                                }
                                else
                                {

                                    psReports.Add(psData);
                                }

                            }
                            #endregion
                        }
                    }
                    //If this data is not existing in table WageType
                    else
                    {
                        //Net total from SAP (TotalIncome - TotalOutcome)
                        if (dr["WageType"].ToString() == "/559")
                        {
                            psData = new PaySlipReportWageData();
                            psData.ReportType = (int)eReportType.Others;
                            psData.WageTypeCode = "NetFromSAP";
                            psData.Amount = Convert.ToDecimal(dr["Currencyamt"]);
                            psReports.Add(psData);
                        }
                    }
                }
                //If AccumType of this data is Yearly
                else
                {
                    //Income Collection
                    if (dr["WageType"].ToString() == "/102" || dr["WageType"].ToString() == "/103")  // (change 04.10.2012) นำค่า /102+/103 ไปแสดงเป็นเงินได้สะสม
                    {
                        psData = new PaySlipReportWageData();
                        if (psReports.Exists(delegate (PaySlipReportWageData wd) { return wd.WageTypeCode == "IncomeCollection"; })) // มี data แล้วรึเปล่า
                        {
                            psData = psReports.Find(delegate (PaySlipReportWageData wd) { return wd.WageTypeCode == "IncomeCollection"; }); // นำค่า"IncomeCollection" ขึ้นมา
                            psData.Amount += Convert.ToDecimal(dr["Currencyamt"]); // บวกค่าใหม่เข้าไป
                        }
                        else
                        {
                            psData.ReportType = (int)eReportType.Others;
                            psData.WageTypeCode = "IncomeCollection";
                            psData.Amount = Convert.ToDecimal(dr["Currencyamt"]);
                            psReports.Add(psData);
                        }
                    }

                    #region IncomeCollectionoldversion
                    //if (dr["WageType"].ToString() == "/101")  // (change 20.08.2012 ) /101 ค่าที่จะไปแสดงในรายได้สะสม
                    //{
                    //    psData = new PaySlipReportWageData();
                    //    psData.ReportType = (int)eReportType.Others;
                    //    psData.WageTypeCode = "IncomeCollection";
                    //    psData.Amount = Convert.ToDecimal(dr["Currencyamt"]);
                    //    psReports.Add(psData);
                    //}
                    #endregion
                    //Tax Collection
                    else if (dr["WageType"].ToString() == "/401")
                    // (change 27.07.2012 ) ค่าที่จะไปแสดงในภาษีสะสม เอา /401( base+gross ) แทน /485 ( base ) 
                    {
                        psData = new PaySlipReportWageData();
                        psData.ReportType = (int)eReportType.Others;
                        psData.WageTypeCode = "TaxCollection";
                        psData.Amount = Convert.ToDecimal(dr["Currencyamt"]);
                        psReports.Add(psData);
                    }
                    //Provident fund
                    else if (dr["WageType"].ToString() == "/321")
                    {
                        psData = new PaySlipReportWageData();
                        psData.ReportType = (int)eReportType.Others;
                        psData.WageTypeCode = "ProvidentFund";
                        psData.Amount = Convert.ToDecimal(dr["Currencyamt"]);
                        psReports.Add(psData);
                    }
                    //Social security
                    else if (dr["WageType"].ToString() == "/301")
                    {
                        psData = new PaySlipReportWageData();
                        psData.ReportType = (int)eReportType.Others;
                        psData.WageTypeCode = "SocialSecurity";
                        psData.Amount = Convert.ToDecimal(dr["Currencyamt"]);
                        psReports.Add(psData);
                    }
                }
            }

            return psReports;
        }


        private List<PaySlipReportWageData> AddDummyData(List<PaySlipReportWageData> data, List<PaySlipReportWageData> retros, int cntRow)
        {
            if (data.Count < cntRow)
            {
                foreach (PaySlipReportWageData retro in retros)
                    data.Add(retro);
                int loop = (cntRow - data.Count);
                PaySlipReportWageData psData;
                for (int i = 0; i < loop; i++)
                {
                    psData = new PaySlipReportWageData();
                    psData.WageTypeDescription = "dummy";
                    data.Add(psData);
                }
            }

            return data;
        }

        public List<WageType> GetWageTypeAll()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetWageTypeAll();
        }

        public string GetCommonText(string Category, string Language, string Code)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetCommonText(Category, Language, Code);
        }

        public WebWageType GetWebWageTypeByCode(int Code)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetWebWageTypeByCode(Code);
        }

        public SpouseAllowance GetSpouseAllowance(string Code)
        {
            //SpouseAllowance oItem = ESS.HR.PY.ServiceManager.HRPYConfig.GetSpouseAllowance(Code);
            SpouseAllowance oItem = ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetSpouseAllowance(Code);
            return oItem;
        }

        public List<PayslipPeriodSetting> GetPeriodSettingAll()
        {
            List<PayslipPeriodSetting> oListPayslipPeriodSetting = ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAllPayslipPeriod();

            foreach (PayslipPeriodSetting oPSetting in oListPayslipPeriodSetting)
            {
                if (oPSetting.EffectiveDate == DateTime.MinValue)
                {
                    oPSetting.EffectiveDate = SetEffectiveDateByDefaultPaymentDate(oPSetting.PeriodMonth, oPSetting.PeriodYear);
                }
            }

            return oListPayslipPeriodSetting;
        }

        public void SavePeriodSetting(List<PayslipPeriodSetting> periods)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSConfig.SavePeriodSetting(periods);
        }

        public bool ValidatePayslipPeriodSetting(PayslipPeriodSetting pSet, bool isNew, string LanguageCode, out bool isError, out string ErrorMessage)
        {
            bool flag = false;
            isError = true;
            ErrorMessage = string.Empty;
            int state = (int)eValidateMessage.Pass;
            DataTable PeriodSettings = new DataTable();
            if (!isNew)
            {
                if (PeriodSettings.Select(string.Format("PeriodYear={0} AND PeriodMonth={1}", pSet.PeriodYear, pSet.PeriodMonth, pSet.PeriodID)).Length > 0)
                {
                    if (pSet.IsOffCycle && PeriodSettings.Select(string.Format("PeriodYear={0} AND PeriodMonth={1} AND OffCycleDate='{2}' AND PeriodID <> {3}", pSet.PeriodYear, pSet.PeriodMonth, pSet.OffCycleDate, pSet.PeriodID)).Length > 0)
                        state = (int)eValidateMessage.DupplicateOffCycle;

                    if (!pSet.IsOffCycle && PeriodSettings.Select(string.Format("PeriodYear={0} AND PeriodMonth={1} AND IsOffCycle=0 AND PeriodID <> {2}", pSet.PeriodYear, pSet.PeriodMonth, pSet.PeriodID)).Length > 0)
                        state = (int)eValidateMessage.Dupplicate;

                    if (pSet.IsOffCycle && pSet.OffCycleDate == DateTime.MinValue)
                        state = (int)eValidateMessage.NoOffCycleDate;

                    if (pSet.EffectiveDate == DateTime.MinValue)
                        state = (int)eValidateMessage.NoEffectiveDate;
                }
            }
            else
            {
                if (PeriodSettings.Select(string.Format("PeriodYear={0} AND PeriodMonth={1}", pSet.PeriodYear, pSet.PeriodMonth)).Length > 0)
                {
                    if (pSet.IsOffCycle && PeriodSettings.Select(string.Format("PeriodYear={0} AND PeriodMonth={1} AND OffCycleDate='{2}'", pSet.PeriodYear, pSet.PeriodMonth, pSet.OffCycleDate)).Length > 0)
                        state = (int)eValidateMessage.DupplicateOffCycle;

                    if (!pSet.IsOffCycle && PeriodSettings.Select(string.Format("PeriodYear={0} AND PeriodMonth={1} AND IsOffCycle=0 ", pSet.PeriodYear, pSet.PeriodMonth)).Length > 0)
                        state = (int)eValidateMessage.Dupplicate;

                    if (pSet.IsOffCycle && pSet.OffCycleDate == DateTime.MinValue)
                        state = (int)eValidateMessage.NoOffCycleDate;

                    if (pSet.EffectiveDate == DateTime.MinValue)
                        state = (int)eValidateMessage.NoEffectiveDate;
                }
            }

            if (state == (int)eValidateMessage.Pass)
            {
                flag = true;
                isError = false;
            }
            else if (state == (int)eValidateMessage.DupplicateOffCycle)
            {
                isError = true;
                ErrorMessage = string.Format("{0} : {1}{2} {3}{4}", GetCommonText("PAYSLIPDATA", LanguageCode, "DUPPLICATEOFFCYCLE"), GetCommonText("PAYSLIPDATA", LanguageCode, "MONTH"), GetCommonText("MONTH", LanguageCode, pSet.PeriodMonth.ToString().PadLeft(2, '0')), GetCommonText("PAYSLIPDATA", LanguageCode, "OFFCYCLEDATE"), pSet.OffCycleDate.ToString("dd/MM/yyyy", oCL));
            }
            else if (state == (int)eValidateMessage.Dupplicate)
            {
                isError = true;
                ErrorMessage = GetCommonText("PAYSLIPDATA", LanguageCode, "DUPPLICATE");
            }
            else if (state == (int)eValidateMessage.NoEffectiveDate)
            {
                isError = true;
                ErrorMessage = GetCommonText("PAYSLIPDATA", LanguageCode, "NOEFFECTIVEDATE");
            }
            else if (state == (int)eValidateMessage.NoOffCycleDate)
            {
                isError = true;
                ErrorMessage = GetCommonText("PAYSLIPDATA", LanguageCode, "NOOFFCYCLEDATE");
            }
            return flag;
        }

        public List<string> GetYearPaySlipPeriodSetting()
        {
            List<string> lstYear = new List<string>();
            int currentyear = Convert.ToInt32(DateTime.Now.ToString("yyyy", oCL));
            int sBACKWARD_PAYSLIP_YEAROFORIGIN = Convert.ToInt32(BACKWARD_PAYSLIP_YEAROFORIGIN);
            int sFORWARD_PAYSLIP_YEAR = Convert.ToInt32(FORWARD_PAYSLIP_YEAR);
            int TotalBackwardYear = 0;

            for (int i = sFORWARD_PAYSLIP_YEAR; i >= 1; i--)
                lstYear.Add(Convert.ToString(currentyear + i));
            lstYear.Add(currentyear.ToString());

            for (int i = currentyear - 1; i >= sBACKWARD_PAYSLIP_YEAROFORIGIN; i--)
            {
                lstYear.Add(Convert.ToString(i));
                TotalBackwardYear++;
            }
            return lstYear;
        }

        public DataTable GetMonthPaySlipPeriodSetting(string Language)
        {
            DataTable dt = new DataTable("ListMonth");
            dt.Columns.Add("MonthID", typeof(System.Int32));
            dt.Columns.Add("MonthName", typeof(System.String));
            for (int i = 1; i <= 12; i++)
            {
                DataRow dr = dt.NewRow();
                //dr["MonthID"] = i.ToString();
                dr["MonthID"] = i;
                dr["MonthName"] = GetCommonText("SYSTEM", Language, "D_M" + i.ToString());
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public List<Tavi50PeriodSetting> GetActiveTavi50Period()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetActiveTavi50Period();
        }

        public List<Tavi50PeriodSetting> GetAllTavi50Period(string path)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAllTavi50Period(path);
        }

        public List<string> GetYearTavi50(DateTime HiringDate, string Language)
        {
            List<string> lstYear = new List<string>();
            List<Tavi50PeriodSetting> oPeriod = GetActiveTavi50Period();
            oPeriod = oPeriod.FindAll(delegate (Tavi50PeriodSetting item) { return item.PeriodYear >= HiringDate.Year; });
            foreach (Tavi50PeriodSetting oitem in oPeriod)
            {
                lstYear.Add(oitem.PeriodYear.ToString());
            }

            if (lstYear.Count <= 0)
            {
                lstYear.Add(GetCommonText("TAXREPORT", Language, "NOTAXYEAR"));
            }
            //else
            //{
            //    lstYear.SelectedValue = Convert.ToString(DateTime.Now.Year - 1);
            //}
            lstYear = lstYear.Distinct().ToList();
            return lstYear;
        }


        public List<TaxData> GetTaxDataV2(string EmployeeID, string Year, string PINCODE)
        {
            List<TaxData> oReturn = ServiceManager.CreateInstance(CompanyCode).ERPData.GetTaxDataV2(EmployeeID, Year, PINCODE);
            return oReturn;
        }

        public List<string> GetYearTavi50PeriodSetting()
        {
            List<string> lstYear = new List<string>();
            int currentyear = Convert.ToInt32(DateTime.Now.ToString("yyyy", oCL));
            int sBACKWARD_PAYSLIP_YEAROFORIGIN = Convert.ToInt32(BACKWARD_PAYSLIP_YEAROFORIGIN);
            int sFORWARD_PAYSLIP_YEAR = Convert.ToInt32(FORWARD_PAYSLIP_YEAR);
            int TotalBackwardYear = 0;

            for (int i = sFORWARD_PAYSLIP_YEAR; i >= 1; i--)
                lstYear.Add(Convert.ToString(currentyear + i));
            lstYear.Add(currentyear.ToString());

            for (int i = currentyear - 1; i >= sBACKWARD_PAYSLIP_YEAROFORIGIN; i--)
            {
                lstYear.Add(Convert.ToString(i));
                TotalBackwardYear++;
            }
            return lstYear;
        }

        public void SaveTavi50PeriodSetting(List<Tavi50PeriodSetting> periods, string path)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSConfig.SaveTavi50PeriodSetting(periods, path);
        }

        public List<string> GetResponseCodeByOrganizationOnly(string Role, string EmpID)
        {
            string RootOrg = string.IsNullOrEmpty(ROOT_ORGANIZATION) ? "999999999" : ROOT_ORGANIZATION;
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetResponseCodeByOrganizationOnly(Role, EmpID, RootOrg);
        }

        public DataTable GetPeriodLetter(string Language)
        {
            DateTime start = DateTime.Now.Date;
            int iMONTHLYLETTERBEFORE = Convert.ToInt32(MONTHLYLETTERBEFORE);
            DateTime rundate;

            DataTable dt = new DataTable("ListPeriod");
            dt.Columns.Add("PeriodValue", typeof(System.String));
            dt.Columns.Add("PeriodText", typeof(System.String));

            for (int index = -iMONTHLYLETTERBEFORE; index <= 0; index++)
            {
                DataRow dr = dt.NewRow();
                rundate = start.AddMonths(index);
                dr["PeriodValue"] = rundate.ToString("yyyyMM", oCL);
                dr["PeriodText"] = String.Format("{0} {1}", GetCommonText("SYSTEM", Language, "D_M" + rundate.Month.ToString()),
                            rundate.ToString("yyyy", oCL));
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public Letter GetLetter(string strRequestNo)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetLetter(strRequestNo);
        }

        public List<Letter> GetLetter(string strEmployeeID, DateTime dtBeginDate, DateTime dtEndDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetLetter(strEmployeeID, dtBeginDate, dtEndDate);
        }

        public List<Letter> GetLetterByAdmin(DateTime dtBeginDate, DateTime dtEndDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetLetterByAdmin(dtBeginDate, dtEndDate);
        }

        public Company GetCompanyData(string Area, string SubArea)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetCompanyData(Area, SubArea);
        }

        public Dictionary<int, LetterType> GetLetterType(string strPermissionCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetLetterType(strPermissionCode);
        }

        public List<LetterEmbassy> GetLetterEmbassy()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetLetterEmbassy();
        }

        public LetterType GetSignPositionByLetterType(int iLetterType)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetSignPositionByLetterType(iLetterType);
        }

        public LetterEmbassy GetLetterEmbassyByID(int iEmbassyID)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetLetterEmbassyByID(iEmbassyID);
        }

        public LetterType GetLetterTypeByID(int iLetterTypeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetLetterTypeByID(iLetterTypeID);
        }


        public DataTable GetSignByManager(EmployeeData oEmp, string sLetterTypeID, string Language)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SignValue", typeof(System.String));
            dt.Columns.Add("SignText", typeof(System.String));

            if (LETTERTYPESIGNBYMANAGER.Contains(sLetterTypeID.ToString()))
            {
                #region "Manager"
                List<EmployeeData> listManager = new List<EmployeeData>();
                EmployeeData emp = oEmp;
                int nLap = 0;
                do
                {
                    if (emp != oEmp)
                    {
                        int intEmpSubGroup = 0;
                        int.TryParse(emp.OrgAssignment.EmpSubGroup, out intEmpSubGroup);
                        //if (intEmpSubGroup >= 14)//EmpSubGroup 14 = VP
                        if (intEmpSubGroup >= 14 && intEmpSubGroup <= 18)//EmpSubGroup 14 = VP
                        {
                            listManager.Add(emp);
                        }
                    }
                    nLap++;
                    emp = emp.FindManager(string.Format("#FIRSTAPPROVER", nLap.ToString("00")));
                } while (emp != null && nLap < 10);
                foreach (EmployeeData item in listManager)
                {
                    DataRow dr = dt.NewRow();
                    dr["SignValue"] = item.EmployeeID;
                    //dr["SignText"] = string.Format("{0} - {1} {2}", item.EmployeeID, item.AlternativeName(Language), item.OrgAssignment.PositionData.AlternativeName(Language));

                    dr["SignText"] = string.Format("{0} {1}", item.AlternativeName(Language), item.OrgAssignment.PositionData.AlternativeName(Language));

                    DataRow[] drFound = dt.Select(string.Format("SignValue='{0}'", item.EmployeeID));
                    if (drFound.Length == 0)
                    {
                        dt.Rows.Add(dr);
                    }
                }
                #endregion
            }

            LetterType oSignPosition = GetSignPositionByLetterType(Convert.ToInt32(sLetterTypeID));

            foreach (string Position in oSignPosition.SignPosition.Split(','))
            {
                EmployeeData oSignPerson = OMManagement.CreateInstance(CompanyCode).GetEmployeeByPositionID(Position, DateTime.Now);
                if (oSignPerson != null)
                {
                    DataRow dr = dt.NewRow();
                    dr["SignValue"] = oSignPerson.EmployeeID;
                    dr["SignText"] = string.Format("{0} {1}", oSignPerson.AlternativeName(Language), oSignPerson.OrgAssignment.PositionData.AlternativeName(Language));
                    //dr["SignText"] = string.Format("{0} - {1} {2}", oSignPerson.EmployeeID, oSignPerson.AlternativeName(Language), oSignPerson.OrgAssignment.PositionData.AlternativeName(Language));
                    if (!chkExistSignByManager(dt, dr["SignValue"].ToString(), dr["SignText"].ToString()))
                    {
                        dt.Rows.Add(dr);
                    }
                }
            }
            return dt;
        }

        public void SaveLetter(Letter oLetter)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveLetter(oLetter);
        }

        public void InsertLetterLog(string strEmployeeID, string strRequestNo)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.InsertLetterLog(strEmployeeID, strRequestNo);
        }

        public void UpdateLetterNo(string strEmployeeID, string strRequestNo, DateTime dtBeginDate, DateTime dtEndDate, bool bUpdateLog)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.UpdateLetterNo(strEmployeeID, strRequestNo, dtBeginDate, dtEndDate, bUpdateLog);
        }

        public DateTime GetPreviousEffectiveDate()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetPreviosEffectiveDatePayslip();
        }

        public Dictionary<string, INFOTYPE0014> GetActiveWageRecuringData(string strEmployeeID, DateTime dtCheckDate, string strWageType)
        {
            return ServiceManager.CreateInstance(CompanyCode).ERPData.GetActiveWageRecuringData(strEmployeeID, dtCheckDate, strWageType);
        }

        public Dictionary<string, INFOTYPE0015> GetAssignmentNumberWageTypePA0015(string strEmployeeID, DateTime dtCheckDate, string strWageType)
        {
            return ServiceManager.CreateInstance(CompanyCode).ERPData.GetAssignmentNumberWageTypePA0015(strEmployeeID, dtCheckDate, strWageType);
        }

        public Dictionary<string, INFOTYPE0015> GetAssignmentNumberWageTypePA0267(string strEmployeeID, DateTime dtCheckDate, string strWageType)
        {
            return ServiceManager.CreateInstance(CompanyCode).ERPData.GetAssignmentNumberWageTypePA0267(strEmployeeID, dtCheckDate, strWageType);
        }

        public bool chkExistSignByManager(DataTable dt, string signValue, string signText)
        {
            bool isExist = false;
            foreach (DataRow dr in dt.Rows)
            {
                if (dr["SignValue"].ToString() == signValue && dr["SignText"].ToString() == signText)
                {
                    isExist = true;
                    break;
                }
            }
            return isExist;
        }

        public DateTime SetEffectiveDateByDefaultPaymentDate(int iMonth, int iYear)
        {
            DateTime dEffectiveDate = new DateTime(iYear, iMonth, 1);
            int i = Convert.ToInt32(HRPYManagement.CreateInstance(CompanyCode).DEFAULT_PAYMENT_DATE);

            MonthlyWS oMonthlyWS = EmployeeManagement.CreateInstance(CompanyCode).GetPublicCalendar(iYear, iMonth);
            while (oMonthlyWS.GetWSCode(i, true).Contains("OFF") || oMonthlyWS.GetWSCode(i, false) == "1")
            {
                i--;
            }
            i--;
            dEffectiveDate = DateTime.ParseExact(string.Format("{0:00}{1:00}{2:0000}", i, iMonth, iYear), "ddMMyyyy", oCL);
            return dEffectiveDate;
        }

        public List<PFChangePlanReport> GetPFChangePlanList(DateTime effectiveDate, string companyCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetPFChangePlanList(effectiveDate, companyCode);
        }

        public List<PFChangeRateAmountPlanReport> GetPFChangeRateAmountPlanList(DateTime effectiveDate, string companyCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetPFChangeRateAmountPlanList(effectiveDate, companyCode);
        }

        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            return dataTable;
        }

        enum eValidateMessage
        {
            Pass = 0,
            Dupplicate = 1,
            DupplicateOffCycle = 2,
            NoOffCycleDate = 3,
            NoEffectiveDate = 4
        }

        enum eReportType
        {
            Income = 0,
            Outcome = 1,
            RetroIncome = 2,
            RetroOutcome = 3,
            Others = 4
        }

        public class Data_ALLOCATEBUDGET_LINK
        {
            public string DataEncrypt { get; set; }
            public string DataURL { get; set; }
        }
        public Data_ALLOCATEBUDGET_LINK EncryptDataString(string param_sent)
        {
            var data = new Data_ALLOCATEBUDGET_LINK();
            string key_data = ALLOCATEBUDGETKEY;
            //string key_data = null;
            if (string.IsNullOrEmpty(key_data))
            {
                key_data = "AnDa10Zzmp=Qso9y";
            }
            string data_encrypt = ParamAESEncrypter.AESEncrypt(key_data, param_sent);

            data.DataEncrypt = data_encrypt;
            data.DataURL = ALLOCATEBUDGET_LINK;
            return data;
        }

        public bool ShowTimeAwareLink(int LinkID)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.ShowTimeAwareLink(LinkID);
        }

        #region Import Providentfund (นำเข้าข้อมูลกองทุนสำรองเลี้ยงชีพ)

        public ResponeKeyProvident SaveImportProvidenfund(SaveProvidentFundKtam model)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.SaveImportProvidentFund(model);
        }

        public XLWorkbook ExportExcelProvidentFund(int year, int month, string fundId)
        {
            var workbook = new XLWorkbook();
            var sheet1 = workbook.Worksheets.Add("กองทุนสำรองเลี้ยงชีพ");

            int header_row = 1;
            int header_column = 1;

            #region Header Excel
            sheet1.Row(header_row).Cell(header_column).Value = "รหัสพนักงาน"; // 1
            sheet1.Column(header_column).Width = 15;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "คำนำหน้าชื่อ"; // 2
            sheet1.Column(header_column).Width = 15;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "ชื่อ"; // 3
            sheet1.Column(header_column).Width = 20;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "นามสกุล"; // 4
            sheet1.Column(header_column).Width = 25;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "รหัสสังกัด"; // 5
            sheet1.Column(header_column).Width = 25;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "สะสมยกมา"; // 6
            sheet1.Column(header_column).Width = 20;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "ผลประโยชน์สะสมยกมา"; // 7
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "หน่วยสะสมยกมา"; // 8
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "สมทบยกมา"; // 9
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "ผลประโยชน์สมทบยกมา"; // 10
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "หน่วยสมทบยกมา"; // 11
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "ประเดิมสมทบยกมา"; // 12
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "ผลประโยชน์ประเดิมสะสมยกมา"; // 13
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "หน่วยประเดิมสะสมยกมา"; // 14
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "ประเดิมสมทบยกมา"; // 15
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "ผลประโยชน์ประเดิมสมทบยกมา"; // 16
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "หน่วยประเดิมสมทบยกมา"; // 17
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "สะสมรับโอนระหว่างปี"; // 18
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "ผลประโยชน์สะสมรับโอนระหว่างปี"; // 19
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "หน่วยสะสมรับโอนระหว่างปี"; // 20
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "สมทบรับโอนระหว่างปี"; // 21
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "ผลประโยชน์สมทบรับโอนระหว่างปี"; // 22
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "หน่วยสมทบรับโอนระหว่างปี"; // 23
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "ประเดิมสะสมรับโอนระหว่างปี"; // 24
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "ผลประโยชน์ประเดิมสะสมรับโอนระหว่างปี"; // 25
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "หน่วยประเดิมสะสมรับโอนระหว่างปี"; // 26
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "ประเดิมสมทบรับโอนระหว่างปี"; // 27
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "ผลประโยชน์ประเดิมสมทบรับโอนระหว่างปี"; // 28
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "หน่วยประเดิมสมทบรับโอนระหว่างปี"; // 29
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "สะสมระหว่างปี"; // 30
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "ผลประโยชน์สะสมระหว่างปี"; // 31
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "หน่วยสะสมระหว่างปี"; // 32
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "สมทบระหว่างปี"; // 33
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "ผลประโยชน์สมทบระหว่างปี"; // 34
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "หน่วยสมทบระหว่างปี"; // 35
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "ประเดิมสะสมระหว่างปี"; // 36
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "ผลประโยชน์ประเดิมสะสมระหว่างปี"; // 37
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "หน่วยประเดิมสะสมระหว่างปี"; // 38
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "ประเดิมสมทบระหว่างปี"; // 39
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "ผลประโยชน์ประเดิมสมทบระหว่างปี"; // 40
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "หน่วยประเดิมสมทบระหว่างปี"; // 41
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "สะสมโอนออกระหว่างปี"; // 42
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "ผลประโยชน์สะสมโอนออกระหว่างปี"; // 43
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "หน่วยสะสมโอนออกระหว่างปี"; // 44
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "สมทบโอนออกระหว่างปี"; // 45
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "ผลประโยชน์สมทบโอนออกระหว่างปี"; // 46
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "หน่วยสมทบโอนออกระหว่างปี"; // 47
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "ประเดิมสะสมโอนออกระหว่างปี"; // 48
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "ผลประโยชน์ประเดิมสะสมโอนออกระหว่างปี"; // 49
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "หน่วยประเดิมสะสมโอนออกระหว่างปี"; // 50
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "ประเดิมสมทบโอนออกระหว่างปี"; // 51
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "ผลประโยชน์ประเดิมสมทบโอนออกระหว่างปี"; // 52
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "หน่วยประเดิมสมทบโอนออกระหว่างปี"; // 53
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "สะสมรวม"; // 54
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "ผลประโยชน์สะสมรวม"; // 55
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "หน่วยสะสมรวม"; // 56
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "สมทบรวม"; // 57
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "ผลประโยชน์สมทบรวม"; // 58
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "หน่วยสมทบรวม"; // 59
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "ประเดิมสะสมรวม"; // 60
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "ผลประโยชน์ประเดิมสะสมรวม"; // 61
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "หน่วยประเดิมสะสมรวม"; // 62
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "ประเดิมสมทบรวม"; // 63
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "ผลประโยชน์ประเดิมสมทบรวม"; // 64
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "หน่วยประเดิมสมทบรวม"; // 65
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "เงินสะสมรับเข้าระหว่างปี จากกองทุนอื่น"; // 66
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "ผลประโยชน์เงินสะสมรับเข้าระหว่างปี จากกองทุนอื่น"; // 67
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "เงินสมทบรับเข้าระหว่างปี จากกองทุนอื่น"; // 68
            sheet1.Column(header_column).Width = 35;
            header_column++;
            sheet1.Row(header_row).Cell(header_column).Value = "ผลประโยชน์เงินสะสมรับเข้าระหว่างปี จากกองทุนอื่น"; // 69
            sheet1.Column(header_column).Width = 45;
            header_column++;

            #endregion


            var range = sheet1.Range("A1:BQ1");
            range.Style.Fill.BackgroundColor = XLColor.Yellow;
            range.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            range.Style.Font.FontName = "Angsana New";
            range.Style.Font.FontSize = 16;
            range.Style.Border.SetOutsideBorder(XLBorderStyleValues.Thick).Border.SetInsideBorder(XLBorderStyleValues.Thick);

            #region Data Excel

            var result = ServiceManager.CreateInstance(CompanyCode).ESSData.GetDataProvidentfund(year, month, fundId);
            if (result.list_detail_provident.Count > 0)
            {
                int column;
                int row = 2;
                int last_column = 0;
                int last_row = 0;
                foreach (var item in result.list_detail_provident)
                {
                    column = 1;

                    sheet1.Row(row).Cell(column).Value = item.EmployeeId; // 1 รหัสพนักงาน
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.TitleName; // 2 คำนำหน้าชื่อ
                    sheet1.Row(row).Cell(column).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.FirstName; // 3 ชื่อ
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.LastName; // 4 นามสกุล
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.DeptCode; // 5 รหัสสังกัด
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Old_employee_amount; // 6 สะสมยกมา
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Old_employee_benefit_amount; // 7 ผลประโยชน์สะสมยกมา
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Old_employee_unit; // 8
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Old_company_amount; // 9 สมทบยกมา
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Old_company_benefit_amount; // 10 ผลประโยชน์สมทบยกมา
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Old_company_unit; // 11 หน่วยสมทบยกมา
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Old_forward_amount_employee; // 12 ประเดิมสมทบยกมา
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Old_forward_benefit_amount_employee; // 13 ผลประโยชน์ประเดิมสะสมยกมา
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Old_forward_unit_employee; // 14 หน่วยประเดิมสะสมยกมา
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Old_forward_amount_company; // 15  ประเดิมสมทบยกมา
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Old_forward_benefit_amount_company; // 16 ผลประโยชน์ประเดิมสมทบยกมา
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Old_forward_unit_company; // 17 หน่วยประเดิมสมทบยกมา
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Employee_amount_transfer_in; // 18 สะสมรับโอนระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Employee_benefit_amount_transfer_in; // 19 ผลประโยชน์สะสมรับโอนระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Employee_unit_transfer_in; // 20  หน่วยสะสมรับโอนระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Company_amount_transfer_in; // 21 สมทบรับโอนระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Company_benefit_amount_transfer_in; // 22 ผลประโยชน์สมทบรับโอนระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Company_unit_transfer_in; // 23 หน่วยสมทบรับโอนระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Forward_amount_employee_transfer_in; // 24 ประเดิมสะสมรับโอนระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Forward_benefit_amount_employee_transfer_in; // 25 ผลประโยชน์ประเดิมสะสมรับโอนระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Forward_unit_employee_transfer_in; // 26 หน่วยประเดิมสะสมรับโอนระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Forward_amount_company_transfer_in; // 27 ประเดิมสมทบรับโอนระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Forward_benefit_amount_company_transfer_in; // 28 ผลประโยชน์ประเดิมสมทบรับโอนระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Forward_unit_company_transfer_in; // 29 หน่วยประเดิมสมทบรับโอนระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Year_employee_amount; // 30 สะสมระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Year_employee_benefit_amount; // 31 ผลประโยชน์สะสมระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Year_employee_unit; // 32 หน่วยสะสมระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Year_company_amount; // 33 สมทบระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Year_company_benefit_amount; // 34 ผลประโยชน์สมทบระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Year_company_unit; // 35 หน่วยสมทบระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Year_forward_amount_employee; // 36 ประเดิมสะสมระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Year_forward_benefit_amount_employee; // 37 ผลประโยชน์ประเดิมสะสมระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Year_forward_unit_employee; // 38 หน่วยประเดิมสะสมระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Year_forward_amount_company; // 39 ประเดิมสมทบระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Year_forward_benefit_amount_company; // 40 ผลประโยชน์ประเดิมสมทบระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Year_forward_unit_company; // 41 หน่วยประเดิมสมทบระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Employee_amount_transfer_out; // 42 สะสมโอนออกระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Employee_benfit_amount_transfer_out; // 43 ผลประโยชน์สะสมโอนออกระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Employee_unit_transfer_out; // 44 หน่วยสะสมโอนออกระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Company_amount_transfer_out; // 45 สมทบโอนออกระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Company_benefit_amount_transfer_out; // 46 ผลประโยชน์สมทบโอนออกระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Company_unit_transfer_out; // 47 หน่วยสมทบโอนออกระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Forward_amount_employee_transfer_out; // 48 ประเดิมสะสมโอนออกระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Forward_benefit_amount_employee_transfer_out; // 49 ผลประโยชน์ประเดิมสะสมโอนออกระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Forward_unit_employee_transfer_out; // 50 หน่วยประเดิมสะสมโอนออกระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Forward_amount_company_transfer_out; // 51 ประเดิมสมทบโอนออกระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Forward_benefit_amount_company_transfer_out; // 52 ผลประโยชน์ประเดิมสมทบโอนออกระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Forward_unit_company_transfer_out; // 53 หน่วยประเดิมสมทบโอนออกระหว่างปี
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Total_employee_amount; // 54 สะสมรวม
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Total_employee_benefit_amount; // 55 ผลประโยชน์สะสมรวม
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Total_employee_unit; // 56 หน่วยสะสมรวม
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Total_company_amount; // 57 สมทบรวม
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Total_company_benefit_amount; // 58 ผลประโยชน์สมทบรวม
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Total_company_unit; // 59 หน่วยสมทบรวม
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Total_forward_amount_employee; // 60 ประเดิมสะสมรวม
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Total_forward_benefit_amount_employee; // 61 ผลประโยชน์ประเดิมสะสมรวม
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Total_forward_unit_employee; // 62 หน่วยประเดิมสะสมรวม
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Total_forward_amount_company; // 63 ประเดิมสมทบรวม
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Total_forward_benefit_amount_company; // 64 ผลประโยชน์ประเดิมสมทบรวม
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.Total_forward_unit_company; // 65 หน่วยประเดิมสมทบรวม
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.External_trans_emp_amount; // 66  เงินสะสมรับเข้าระหว่างปี จากกองทุนอื่น
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.External_trans_emp_benefit_amount; // 67 ผลประโยชน์เงินสะสมรับเข้าระหว่างปี จากกองทุนอื่น
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.External_trans_comp_amount; // 68 เงินสมทบรับเข้าระหว่างปี จากกองทุนอื่น
                    column++;

                    sheet1.Row(row).Cell(column).Value = item.External_trans_comp_benefit_amount; // 69 ผลประโยชน์เงินสะสมรับเข้าระหว่างปี จากกองทุนอื่น

                    if (last_column == 0)
                        last_column = column;

                    row++;
                }

                last_row = row - 1;
                var rang_detail = sheet1.Range(2, 1, last_row, last_column);
                rang_detail.Style.Font.FontName = "Angsana New";
                rang_detail.Style.Font.FontSize = 14;
            }

            #endregion

            return workbook;
        }

        public List<string> GetYearProvidentfund()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetYearProvidentfund();
        }

        public List<string> GetMonthProvidentfund(int year)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetMonthProvidentfund(year);
        }

        public List<TypePVD> GetTextProvidentfund(int year, int month)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetTextProvidentfund(year, month);
        }

        #endregion

        public PayslipResult GetPayslipByMonthYear(string EmployeeID, string CompCode, PayslipPeriodSetting Period, string AccumType, string NotSummarizeRT)
        {
            return ServiceManager.CreateInstance(CompanyCode).ERPData.GetPayslipByMonthYear(EmployeeID, CompCode, Period, AccumType, NotSummarizeRT);
        }

        #region Payroll Result Employee
        public void JobLoadPayrollEmployee(int param)
        {
            DateTime date_now = DateTime.Now;

            // โหลดข้อมูล PaySlip เฉพาะปัจจุบันเท่านั้น
            if (param == 0)
            {
                // 1. หาวันที่โหลด Payroll
                PeriodSettingLatest period_setting = ServiceManager.CreateInstance(CompanyCode).ESSData.GetSettingPeriodPayroll();

                // 2. วันปัจจุบันตรงกับวันที่ตั้งโหลด Payroll (Effective Date)
                if (period_setting.Active &&  date_now.Date >= period_setting.EffectiveDate.Value.Date)
                {
                    // 2.1 หาพนักงานทั้งหมดที่ยัง Active อยู่
                    List<EmployeeAllActive> list_employee_active = EmployeeManagement.CreateInstance(CompanyCode)
                        .GetAllEmployeeActive(date_now, "TH");

                    if (list_employee_active.Count > 0)
                    {
                        // 2.2 Load ข้อมูล Payroll By Employee (SAP) (ของเดือนและปีนั้น)
                        string keySapSecret = KeySecret;
                        List<SapPayrollEmployee> sap_payroll_employee = ServiceManager.CreateInstance(CompanyCode).ERPData
                            .LoadPayrollSapByEmployee(list_employee_active, CompanyCode, period_setting, "M", "", keySapSecret);

                        //2.3 Save TO SQL
                        ServiceManager.CreateInstance(CompanyCode).ESSData.SaveOrUpdatePayrollEmployee(sap_payroll_employee);

                    }
                }

                // 3. สถานะเป็น OffCycle  และตรงกับวันโหลดข้อมูล
                if (period_setting.Active && period_setting.IsOffCycle && period_setting.OffCycleDate.Value.Date == date_now.Date)
                {
                    // 3.1 หาพนักงานทั้งหมดที่ยัง Active อยู่
                    List<EmployeeAllActive> list_employee_active = EmployeeManagement.CreateInstance(CompanyCode)
                        .GetAllEmployeeActive(date_now, "TH");

                    // 3.2 Load ข้อมูล Payroll By Employee (SAP) (ของเดือนและปีนั้น)
                    if(list_employee_active.Count > 0)
                    {
                        // 3.2 Load ข้อมูล Payroll By Employee (SAP) (ของเดือนและปีนั้น)
                        string keySapSecret = KeySecret;
                        List<SapPayrollEmployee> sap_payroll_employee = ServiceManager.CreateInstance(CompanyCode).ERPData
                            .LoadPayrollSapByEmployee(list_employee_active, CompanyCode, period_setting, "M", "", keySapSecret);

                        // 3.3 Save ข้อมูล Payroll
                        ServiceManager.CreateInstance(CompanyCode).ESSData.SaveOrUpdatePayrollEmployee(sap_payroll_employee);
                    }
                }
            }
            else if(param > 0) // โหลดข้อมูล PaySlip ย้อนหลังตาจำนวนที่ระบุ
            {
                // 1. Get Period setting ออกมาทั้งหมด
                List<PeriodSettingLatest> list_period_setting = ServiceManager.CreateInstance(CompanyCode).ESSData.GetDbPeriodSettingAll();
                if(list_period_setting.Count > 0)
                {
                    foreach(var item in list_period_setting)
                    {
                        // จำนวนเดือนที่โหลดย้อนหลังหากน้อยกว่าเท่ากับ 0 จะไม่โหลดแล้ว
                        if (item.Active)
                        {
                            if (param > 0)
                            {
                                if (item.PeriodYear <= date_now.Date.Year)
                                {
                                    bool is_reload_payslip = true;

                                    if (item.PeriodYear == date_now.Year && item.PeriodMonth == date_now.Date.Month)
                                    {
                                        // เดือนตรงกัน หากวันปัจจุบันยังไม่เลย (วันที่โหลด PaySlip ห้ามโหลด)
                                        if (item.EffectiveDate.HasValue && item.EffectiveDate.Value.Date > date_now.Date)
                                            is_reload_payslip = false;
                                    }

                                    // ดักกรณีเซต Period Payslip Setting ไปไกลเช่นเซตทั้งปีแต่ตอนนี้ เดือน 1
                                    if (item.PeriodYear == date_now.Year && item.PeriodMonth > date_now.Date.Month)
                                        is_reload_payslip = false;

                                    if (is_reload_payslip)
                                    {
                                        // Load ข้อมูล Payslip พนักงานภายในปีและเดือนนนั้น
                                        List<EmployeeAllActive> list_employee_active = EmployeeManagement.CreateInstance(CompanyCode)
                                            .GetAllEmployeeActive(date_now, "TH");

                                        if (list_employee_active.Count > 0)
                                        {
                                            // 2.2 Load ข้อมูล Payroll By Employee (SAP) (ของเดือนและปีนั้น)
                                            string keySapSecret = KeySecret;
                                            List<SapPayrollEmployee> sap_payroll_employee = ServiceManager.CreateInstance(CompanyCode).ERPData
                                                .LoadPayrollSapByEmployee(list_employee_active, CompanyCode, item, "M", "", keySapSecret);

                                            //2.3 Save TO SQL
                                            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveOrUpdatePayrollEmployee(sap_payroll_employee);
                                        }

                                        // รอบ Offcycle เป็นรอบพิเศษ ดังนั้นไม่ต้องลบออก
                                        if (!item.IsOffCycle)
                                            param--;
                                    }
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }
        }
        #endregion

        public string GetRootOrgForReport(string sEmpID, DateTime dDateTime)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetRootOrgForReport(sEmpID, dDateTime);
        }

        public void SaveConfigurationLog(ConfigurationLog oConfigurationLog)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveConfigurationLog(oConfigurationLog);
        }

        public DataSet ToDataSet<T>(IList<T> list)
        {
            Type elementType = typeof(T);
            DataSet ds = new DataSet();
            DataTable t = new DataTable();
            ds.Tables.Add(t);

            //add a column to table for each public property on T
            foreach (var propInfo in elementType.GetProperties())
            {
                Type ColType = Nullable.GetUnderlyingType(propInfo.PropertyType) ?? propInfo.PropertyType;

                t.Columns.Add(propInfo.Name, ColType);
            }

            //go through each property on T and add each value to the table
            foreach (T item in list)
            {
                DataRow row = t.NewRow();

                foreach (var propInfo in elementType.GetProperties())
                {
                    row[propInfo.Name] = propInfo.GetValue(item, null) ?? DBNull.Value;
                }

                t.Rows.Add(row);
            }

            return ds;
        }
    }
}
