﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.HR.PY.CONFIG;
using ESS.HR.PY.DATACLASS;
using ESS.HR.PY.INFOTYPE;
using ESS.EMPLOYEE;
using ESS.HR.PA.INFOTYPE;
using ESS.EMPLOYEE.DATACLASS;

namespace ESS.HR.PY.INTERFACE
{
    public interface IHRPYDataService
    {
        PayslipResult GetPayslip(string EmployeeID, PayslipPeriodSetting Period);
        PayslipResult GetPayslipByMonthYear(string EmployeeID, string CompCode, PayslipPeriodSetting Period, string AccumType, string NotSummarizeRT);
        List<PaymentDetailRecord> GetPaymentDetail(string EmployeeID, string Period, bool isFindByTimePeriod);
        TaxAllowance GetTaxAllowanceData(string EmployeeID, DateTime CheckDate);
        void SaveTaxallowanceData(TaxAllowance saved);
        INFOTYPE0366 GetProvidentFund(string EmployeeID, DateTime CheckDate);
        void SaveProvidentFund(INFOTYPE0366 data);

        List<PF_Graph> GetFundQuarter(String EmployeeID, String KeyType, int KeyYear, String KeyValue);
        List<PF_PeriodSetting> GetPeriodSetting(String EmployeeID, String KeyType, int KeyYear, String KeyValue);
        List<PF_ProvidentFundDetail> GetTextProvidentFundDetail(String KeyType, String KeyCode, String KeyValue);

        List<PF_Percentage> GetPF_Percentage();

        List<PF_Selection> GetPF_Selection();

        void SaveProvidentFundLog(PF_ProvidentFundLog data);

        List<PF_PeriodSetting> GetEffectiveDate();

        List<TimeAwareLink> GetAllTimeAwareLink();

        void UpdateTimeAwareLink(int LinkID, String BeginDate, String EndDate, String EmpID);

        List<INFOTYPE0021> GetProvidentFundMember(string EmployeeID);

        void DeleteProvidentFundMember(List<INFOTYPE0021> data);

        void SaveProvidentFundMember(List<INFOTYPE0021> data);

        List<TaxData> GetTaxDataV2(string EmployeeID, string Year, string PINCODE);
        List<TaxData> GetTaxDataWithoutPinCode(string EmployeeID, string Year);
        Letter GetLetter(string strRequestNo);
        List<Letter> GetLetter(string strEmployeeID, DateTime dtBeginDate, DateTime dtEndDate);
        List<Letter> GetLetterByAdmin(DateTime dtBeginDate, DateTime dtEndDate);
        void InsertLetterLog(string strEmployeeID, string strRequestNo);
        void SaveLetter(Letter oLetter);
        void UpdateLetterNo(string strEmployeeID, string strRequestNo, DateTime dtBeginDate, DateTime dtEndDate, bool bUpdateLog);
        Dictionary<string, INFOTYPE0014> GetActiveWageRecuringData(string strEmployeeID, DateTime dtCheckDate, string strWageType);

        Dictionary<string, INFOTYPE0015> GetAssignmentNumberWageTypePA0015(string strEmployeeID, DateTime dtCheckDate, string strWageType);
        Dictionary<string, INFOTYPE0015> GetAssignmentNumberWageTypePA0267(string strEmployeeID, DateTime dtCheckDate, string strWageType);

        List<PFChangePlanReport> GetPFChangePlanList(DateTime effectiveDate, string companyCode);
        List<PFChangeRateAmountPlanReport> GetPFChangeRateAmountPlanList(DateTime effectiveDate,string companyCode);
        void MarkUpdate(string EmployeeID, string DataCategory, string RequestNo, bool isMarkIn);
        bool CheckMark(string EmployeeID, string DataCategory);
        bool CheckMark(string EmployeeID, string DataCategory, string ReqNo);
        ResponeKeyProvident SaveImportProvidentFund(SaveProvidentFundKtam model);
        List<string> GetYearProvidentfund();
        List<string> GetMonthProvidentfund(int year);
        List<TypePVD> GetTextProvidentfund(int year, int month);
        DataProvidentfund GetDataProvidentfund(int year, int month, string fundId);

        #region Load Payroll Employee

        PeriodSettingLatest GetSettingPeriodPayroll();
        List<SapPayrollEmployee> LoadPayrollSapByEmployee(List<EmployeeAllActive> list_employee, string CompCode, PeriodSettingLatest Period, string AccumType, string NotSummarizeRT, string keySecret);
        void SaveOrUpdatePayrollEmployee(List<SapPayrollEmployee> list_payroll_employee);
        List<PeriodSettingLatest> GetDbPeriodSettingAll();

        #endregion

        string GetRootOrgForReport(string sEmpID, DateTime dDateTime);
        void SaveConfigurationLog(ConfigurationLog oConfigurationLog);

    }
}
