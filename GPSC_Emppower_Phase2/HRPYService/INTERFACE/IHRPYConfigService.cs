﻿using System.Collections.Generic;
using System;
using System.Data;
using ESS.HR.PY.CONFIG;
using ESS.HR.PY.DATACLASS;

namespace ESS.HR.PY.INTERFACE
{
    public interface IHRPYConfigService
    {
        
        string GetCommonText(string Category, string Language, string Code);

       
        #region "Dropdown"
        List<SpouseAllowance> GetAllSpouseAllowanceDropdownData();
        #endregion

        List<PayslipPeriodSetting> GetAllPayslipPeriod();
        List<PayslipPeriodSetting> GetActivePayslipPeriod();
        List<WageType> GetWageTypeAll();
        PayslipPeriodSetting GetPeriod(int periodID);
        WebWageType GetWebWageTypeByCode(int Code);
        void SavePeriodSetting(List<PayslipPeriodSetting> periods);
        List<PayslipPeriodSetting> GetPeriodSettingByYear(int year);
        List<PayslipPeriodSetting> GetPeriodSettingByYearForAdmin(int year);
        List<SpouseAllowance> GetSpouseAllowanceList();
        SpouseAllowance GetSpouseAllowance(string Code);
        List<WageType> WageTypeAdditionalGetAll();
        void SaveSpouseAllowanceList(List<SpouseAllowance> Data);
        List<Tavi50PeriodSetting> GetActiveTavi50Period();
        List<Tavi50PeriodSetting> GetAllTavi50Period(string path);
        void SaveTavi50PeriodSetting(List<Tavi50PeriodSetting> periods, string path);
        Company GetCompanyData(string Area, string SubArea);
        List<string> GetResponseCodeByOrganizationOnly(string Role, string EmpID, string RootOrg);
        Dictionary<int, LetterType> GetLetterType(string strPermissionCode);
        List<LetterEmbassy> GetLetterEmbassy();
        LetterType GetSignPositionByLetterType(int iLetterType);
        LetterEmbassy GetLetterEmbassyByID(int iEmbassyID);
        LetterType GetLetterTypeByID(int iLetterTypeID);
        DateTime GetPreviosEffectiveDatePayslip();
        bool ShowTimeAwareLink(int LinkID);
    }
}
