﻿using ESS.JOB.ABSTRACT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PY.JOB
{
    public class JobLoadPayslipDataTask : AbstractTaskWorker
    {
        public int Param1 { get; set; }

        public override void Run()
        {
            HRPYManagement.CreateInstance(CompanyCode).JobLoadPayrollEmployee(Param1);
        }
    }
}
