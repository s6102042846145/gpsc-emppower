﻿using ESS.JOB.ABSTRACT;
using ESS.JOB.INTERFACE;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PY.JOB
{
    public class JobLoadPayslipData : AbstractJobWorker
    {
        private CultureInfo oCL = new CultureInfo("en-US");

        public JobLoadPayslipData()
        {

        }

        public override List<ITaskWorker> LoadTasks()
        {
            List<ITaskWorker> oReturn = new List<ITaskWorker>();

            Console.WriteLine("Load PaySlip Employee GPSC");
            Console.WriteLine("========================================");
            JobLoadPayslipDataTask oTask = new JobLoadPayslipDataTask();

            oTask.Param1 = Convert.ToInt16(Params[0].Value.ToString());
            oTask.TaskID = 0;
            oReturn.Add(oTask);

            return oReturn;
        }
    }
}
