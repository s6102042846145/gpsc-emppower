﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.EMPLOYEE.ABSTRACT;

namespace ESS.HR.PY.INFOTYPE
{
    public class INFOTYPE0366 : AbstractInfoType
    {
        private decimal __employeePercentage = 0;
        private decimal __employerPercentage = 0;
        private string __employeeIndicator = string.Empty;
        private string __employerIndicator = string.Empty;
        private string __providentFundNumber = string.Empty;

        public decimal EmployeePercentage
        {
            get { return __employeePercentage; }
            set
            {
                __employeePercentage = value;
            }
        }

        public decimal EmployerPercentage
        {
            get { return __employerPercentage; }
            set
            {
                __employerPercentage = value;
            }
        }

        public string EmployeeIndicator
        {
            get { return __employeeIndicator; }
            set
            {
                __employeeIndicator = value;
            }
        }

        public string EmployerIndicator
        {
            get { return __employerIndicator; }
            set
            {
                __employerIndicator = value;
            }
        }

        public string ProvidentFundNumber
        {
            get { return __providentFundNumber; }
            set
            {
                __providentFundNumber = value;
            }
        }

        public override string InfoType
        {
            get { return "0366"; }
        }

        //public static INFOTYPE0366 GetData(string EmployeeID, DateTime CheckDate)
        //{
        //    INFOTYPE0366 oReturn = HR.PY.ServiceManager.HRPYService.GetProvidentFund(EmployeeID, CheckDate);
        //    return oReturn;
        //}



        //public static INFOTYPE0366 GetData(string EmployeeID, DateTime CheckDate)
        //{
        //    return ServiceManager.CreateInstance(Requestor.OrgAssignment.CompanyCode).ERPData.GetProvidentFund(Requestor.EmployeeID);


        //    INFOTYPE0366 oReturn = HR.PY.ServiceManager.HRPYService.GetProvidentFund(EmployeeID, CheckDate);
        //    return oReturn;
        //}




    }
}
