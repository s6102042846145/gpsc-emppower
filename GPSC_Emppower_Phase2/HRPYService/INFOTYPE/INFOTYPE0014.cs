﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.EMPLOYEE.ABSTRACT;

namespace ESS.HR.PY.INFOTYPE
{
    public class INFOTYPE0014 : AbstractInfoType
    {
        #region " Variable "
        private int __ID = 0;
        private string __employeeID = string.Empty;
        private DateTime __beginDate = DateTime.MinValue;
        private DateTime __endDate = DateTime.MinValue;
        private string __wageType;
        private decimal __amount;
        private decimal __number;
        private string __unit = string.Empty;
        private string __currency = string.Empty;
        private int __assignmentNumber;
        private string __reasonForChange = string.Empty;
        private string __status = "";
        private string __requestNo = "";
        #endregion

        public INFOTYPE0014()
        {
        }

        #region " Override Method "
        public override string InfoType
        {
            get { return "0014"; }
        }
        #endregion

        #region " Properties "
        public int ID
        {
            get { return __ID; }
            set { __ID = value; }
        }
        public string EmployeeID
        {
            get { return __employeeID; }
            set { __employeeID = value; }
        }
        public DateTime BeginDate
        {
            get { return __beginDate; }
            set { __beginDate = value; }
        }
        public DateTime EndDate
        {
            get { return __endDate; }
            set { __endDate = value; }
        }
        public string WageType
        {
            get { return (__wageType == null ? string.Empty : __wageType); }
            set { __wageType = value; }
        }
        public decimal Amount
        {
            get { return __amount; }
            set { __amount = value; }
        }
        public decimal Number
        {
            get { return __number; }
            set { __number = value; }
        }
        public string Unit
        {
            get { return __unit; }
            set { __unit = value; }
        }
        public string Currency
        {
            get { return __currency; }
            set { __currency = value; }
        }
        public int AssignmentNumber
        {
            get { return __assignmentNumber; }
            set { __assignmentNumber = value; }
        }
        public string ReasonForChange
        {
            get { return __reasonForChange; }
            set { __reasonForChange = value; }
        }
        public string Status
        {
            get { return __status; }
            set { __status = value; }
        }
        public string RequestNo
        {
            get { return __requestNo; }
            set { __requestNo = value; }
        }
        #endregion
    }
}
