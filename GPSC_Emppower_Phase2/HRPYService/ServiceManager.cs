﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using ESS.SHAREDATASERVICE;
using ESS.HR.PY.INTERFACE;

namespace ESS.HR.PY
{
    public class ServiceManager
    {
        #region Constructor
        private ServiceManager()
        {

        }
        #endregion

        #region MultiCompany  Framework

        private static Dictionary<string, ServiceManager> Cache = new Dictionary<string, ServiceManager>();

        private static string ModuleID = "ESS.HR.PY";

        private string CompanyCode { get; set; }

        public static ServiceManager CreateInstance(string oCompanyCode)
        {
            ServiceManager oServiceManager = new ServiceManager()
            {
                CompanyCode = oCompanyCode
            };
            return oServiceManager;
        }

        #endregion MultiCompany  Framework

        #region " privatedata "
        private Type GetService(string Mode, string ClassName)
        {

            Type oReturn = null;
            Assembly oAssembly;
            string  assemblyName = string.Format("{0}.{1}",ModuleID,Mode.ToUpper());
            string typeName = string.Format("{0}.{1}.{2}",ModuleID ,Mode.ToUpper(),ClassName);
            oAssembly = Assembly.Load(assemblyName); // Load assembly (dll)
            oReturn = oAssembly.GetType(typeName);   // Load class
            return oReturn;
        }
        #endregion " privatedata "

        internal IHRPYConfigService GetMirrorConfig(string Mode)
        {
            Type oType = GetService(Mode, "HRPYConfigServiceImpl");
            if (oType == null)
            {
                return null;
            }
            else
            {
                return (IHRPYConfigService)Activator.CreateInstance(oType, CompanyCode);
            }
        }

        internal IHRPYDataService GetMirrorService(string Mode)
        {
            Type oType = GetService(Mode, "HRPYDataServiceImpl");
            if (oType == null)
            {
                return null;
            }
            else
            {
                return (IHRPYDataService)Activator.CreateInstance(oType, CompanyCode);
            }
        }

        private string ESSCONNECTOR
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ESSCONNECTOR");
            }
        }

        private string ERPCONNECTOR
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ERPCONNECTOR");
            }
        }

        public IHRPYDataService ESSData
        {
            get
            {
                Type oType = GetService(ESSCONNECTOR, "HRPYDataServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IHRPYDataService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }

        public IHRPYConfigService ESSConfig
        {
            get
            {
                Type oType = GetService(ESSCONNECTOR, "HRPYConfigServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IHRPYConfigService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }

        public IHRPYDataService ERPData
        {
            get
            {
                Type oType = GetService(ERPCONNECTOR, "HRPYDataServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IHRPYDataService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }

        internal IHRPYConfigService ERPConfig
        {
            get
            {
                Type oType = GetService(ERPCONNECTOR, "HRPYConfigServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IHRPYConfigService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }
    }
}
