﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PY.CONFIG
{
    [Serializable()]
    public class ProvidentFundSelection : AbstractObject
    {
        private string __fundID;
        private string __fundDescription;
        private bool __isActive;

        public ProvidentFundSelection()
        {
        }

        public string FundID
        {
            get
            {
                return __fundID;
            }
            set
            {
                __fundID = value;
            }
        }

        public string FundDescription
        {
            get
            {
                return __fundDescription;
            }
            set
            {
                __fundDescription = value;
            }
        }

        public bool IsActive
        {
            get
            {
                return __isActive;
            }
            set
            {
                __isActive = value;
            }
        }
    }
}
