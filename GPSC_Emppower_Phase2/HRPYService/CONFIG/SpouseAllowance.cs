﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PY.CONFIG
{
    //[Serializable()]
    public class SpouseAllowance : AbstractObject
    {
        private string __key = "";
        private string __description = "";

        public SpouseAllowance()
        { }
        public string CompanyCode { get; set; }
        public string Key
        {
            get
            {
                return __key;
            }
            set
            {
                __key = value;
            }
        }

        public string Description
        {
            get
            {
                return __description;
            }
            set
            {
                __description = value;
            }
        }

        public override int GetHashCode()
        {
            string cCode = string.Format("SPOUSEALLOWANCE_{0}", Key);
            return cCode.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return this.GetHashCode() == obj.GetHashCode();
        }
        public List<SpouseAllowance> GetAllSpouseAllowance()
        {
            //return ESS.HR.PY.ServiceManager.HRPYConfig.GetSpouseAllowanceList();
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetSpouseAllowanceList();
        }

        

        public void SaveTo(List<SpouseAllowance> Data, string Mode)
        {
            //ESS.HR.PY.ServiceManager.GetMirrorConfig(Mode).SaveSpouseAllowanceList(Data);
            ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).SaveSpouseAllowanceList(Data);
        }

    }
}
