﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;
namespace ESS.HR.PY.CONFIG
{
    [Serializable()]
    public class MeritSetting : AbstractObject
    {
        private string __companyCode = "";
        private string __contactPoint = "";
        private string __authorizeName = "";
        private string __authorizePosition = "";
        private string __contactTel = "";
        private string __documentNo = "";
        public MeritSetting()
        {
        }
        public string CompanyCode
        {
            get
            {
                return __companyCode;
            }
            set
            {
                __companyCode = value;
            }
        }
        public string ContactPoint
        {
            get
            {
                return __contactPoint;
            }
            set
            {
                __contactPoint = value;
            }
        }
        public string AuthorizeName
        {
            get
            {
                return __authorizeName;
            }
            set
            {
                __authorizeName = value;
            }
        }
        public string AuthorizePosition
        {
            get
            {
                return __authorizePosition;
            }
            set
            {
                __authorizePosition = value;
            }
        }
        public string ContactTel
        {
            get
            {
                return __contactTel;
            }
            set
            {
                __contactTel = value;
            }
        }
        public string DocumentNo
        {
            get
            {
                return __documentNo;
            }
            set
            {
                __documentNo = value;
            }
        }
    }
}
