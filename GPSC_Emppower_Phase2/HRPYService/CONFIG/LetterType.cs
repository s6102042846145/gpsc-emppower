﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PY.CONFIG
{
    public class LetterType : AbstractObject
    {

        public LetterType()
        {
        }

        private int letterTypeID = -1;
        private string reportTemplate = string.Empty;
        private string reportLanguageCode = string.Empty;
        private string signPosition = string.Empty;
        private bool enabledVacation = false;
        private string letterNoFormat = string.Empty;
        private string recurringWageType = string.Empty;

        #region "Property"
        public int LetterTypeID
        {
            get
            {
                return letterTypeID;
            }
            set
            {
                letterTypeID = value;
            }
        }

        public string ReportTemplate
        {
            get
            {
                return reportTemplate;
            }
            set
            {
                reportTemplate = value;
            }
        }

        public string ReportLanguageCode
        {
            get
            {
                return reportLanguageCode;
            }
            set
            {
                reportLanguageCode = value;
            }
        }

        public string LetterNoFormat
        {
            get { return letterNoFormat; }
            set
            {
                letterNoFormat = value;
            }
        }

        public string SignPosition
        {
            get
            {
                return signPosition;
            }
            set
            {
                signPosition = value;
            }
        }

        public string RecurringWageType
        {
            get
            {
                return recurringWageType;
            }
            set
            {
                recurringWageType = value;
            }
        }

        public bool EnabledVacation
        {
            get
            {
                return enabledVacation;
            }
            set
            {
                enabledVacation = value;
            }
        }
        #endregion
    }
}
