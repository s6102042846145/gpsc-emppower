﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PY.CONFIG
{
    public class LetterEmbassy : AbstractObject
    {
        #region "Constructors"
        public LetterEmbassy()
        {

        }
        #endregion


        #region "Declare"
        private int embassyID = -1;
        private string embassyName = "";
        private string address = "";
        private bool isActive = false;
        #endregion

        #region "Properties"
        public int EmbassyID
        {
            get
            {
                return embassyID;
            }
            set
            {
                embassyID = value;
            }
        }

        public string EmbassyName
        {
            get
            {
                return embassyName;
            }
            set
            {
                embassyName = value;
            }
        }

        public string Address
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
            }
        }

        public bool IsActive
        {
            get
            { return isActive; }
            set
            {
                isActive = value;
            }
        }
        #endregion
    }
}
