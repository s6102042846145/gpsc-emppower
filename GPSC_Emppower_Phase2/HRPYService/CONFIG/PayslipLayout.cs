﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;
namespace ESS.HR.PY.CONFIG
{
    [Serializable()]
    public class PayslipLayout : AbstractObject
    {
        private int __payslipLayoutID = -1;
        private string __payslipLayoutCode = "";
        private List<PayslipLayoutSetting> __settings = null;
        public PayslipLayout()
        {
        }
        public int PayslipLayoutID
        {
            get
            {
                return __payslipLayoutID;
            }
            set
            {
                __payslipLayoutID = value;
            }
        }
        public string PayslipLayoutCode
        {
            get
            {
                return __payslipLayoutCode;
            }
            set
            {
                __payslipLayoutCode = value;
            }
        }
        public List<PayslipLayoutSetting> Setting
        {
            get
            {
                if (__settings == null)
                {
                    __settings = HR.PY.ServiceManager.HRPYConfig.GetPayslipLayoutSetting(this.PayslipLayoutID);
                }
                return __settings;
            }
        }
        public static PayslipLayout GetLayout(int LayoutID)
        {
            return HR.PY.ServiceManager.HRPYConfig.GetPayslipLayout(LayoutID);
        }

    }
}
