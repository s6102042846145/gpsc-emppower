﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PY.CONFIG
{
    [Serializable()]
    public class PayslipLayoutSettingValue : AbstractObject
    {
        private int __payslipLayoutID = -1;
        private int __rowID = -1;
        private int __cellID = -1;
        private int __itemID = -1;
        private string __itemType = "";
        private string __itemCode = "";
        private string __itemFlag = "";
        public PayslipLayoutSettingValue()
        {
        }
        public int PayslipLayoutID
        {
            get
            {
                return __payslipLayoutID;
            }
            set
            {
                __payslipLayoutID = value;
            }
        }
        public int RowID
        {
            get
            {
                return __rowID;
            }
            set
            {
                __rowID = value;
            }
        }
        public int CellID
        {
            get
            {
                return __cellID;
            }
            set
            {
                __cellID = value;
            }
        }
        public int ItemID
        {
            get
            {
                return __itemID;
            }
            set
            {
                __itemID = value;
            }
        }
        public string ItemType
        {
            get
            {
                return __itemType;
            }
            set
            {
                __itemType = value;
            }
        }
        public string ItemCode
        {
            get
            {
                return __itemCode;
            }
            set
            {
                __itemCode = value;
            }
        }
        public string ItemFlag
        {
            get
            {
                return __itemFlag;
            }
            set
            {
                __itemFlag = value;
            }
        }
    }
}
