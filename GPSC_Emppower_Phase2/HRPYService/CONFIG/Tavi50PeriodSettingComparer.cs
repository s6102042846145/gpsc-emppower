﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PY.CONFIG
{
    [Serializable()]
    public class Tavi50PeriodSettingComparer : IComparer<Tavi50PeriodSetting>
    {
        public Tavi50PeriodSettingComparer()
        {
        }

        #region IComparer<Tavi50PeriodSetting> Members

        public int Compare(Tavi50PeriodSetting x, Tavi50PeriodSetting y)
        {
            return y.PeriodYear.CompareTo(x.PeriodYear);
        }

        #endregion
    }
}
