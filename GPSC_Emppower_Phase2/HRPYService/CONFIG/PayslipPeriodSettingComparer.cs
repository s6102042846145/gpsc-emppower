﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PY.CONFIG
{
    [Serializable()]
    public class PayslipPeriodSettingComparer : IComparer<PayslipPeriodSetting>
    {
        public PayslipPeriodSettingComparer()
        {
        }

        #region IComparer<PayslipPeriodSetting> Members

        public int Compare(PayslipPeriodSetting x, PayslipPeriodSetting y)
        {
            int oReturn = 0;
            if (x.PeriodYear > y.PeriodYear)
                oReturn = 1;
            else if (x.PeriodYear == y.PeriodYear)
            {
                if (x.PeriodMonth > y.PeriodMonth)
                    oReturn = 1;
                else if (x.PeriodMonth == y.PeriodMonth)
                {
                    oReturn = 0;
                   
                }
                else
                    oReturn = -1;
            }
            else
                oReturn = -1;

            return oReturn;
        }

        #endregion
    }
}
