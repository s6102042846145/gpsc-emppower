﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PY.CONFIG
{
    [Serializable()]
    public class Tavi50PeriodSetting : AbstractObject
    {
        //Attribute
        private int periodID = -1;
        private bool isActive = false;
        private int periodYear = -1;
        private DateTime effectiveDate = DateTime.MaxValue;
        private string signatureImageBase64 = "";

        public string CompanyCode { get; set; }
        public int PeriodID
        {
            get
            {
                return periodID;
            }
            set
            {
                periodID = value;
            }
        }

        public bool IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                isActive = value;
            }
        }

        public int PeriodYear
        {
            get
            {
                return periodYear;
            }
            set
            {
                periodYear = value;
            }
        }
        public DateTime EffectiveDate
        {
            get
            {
                return effectiveDate;
            }
            set
            {
                effectiveDate = value;
            }
        }
        public string SignatureImageBase64
        {
            get
            {
                return signatureImageBase64;
            }
            set
            {
                signatureImageBase64 = value;
            }
        }

        public List<Tavi50PeriodSetting> GetActiveTavi50Period()
        {
            //return HR.PY.ServiceManager.HRPYConfig.GetActiveTavi50Period();
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetActiveTavi50Period();
        }

        
        public List<Tavi50PeriodSetting> GetPeriodSettingAll(string path)
        {
            //return HR.PY.ServiceManager.HRPYConfig.GetAllTavi50Period();
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAllTavi50Period(path);
        }

        
        public void SaveTavi50PeriodSetting(List<Tavi50PeriodSetting> periods, string path)
        {
            //HR.PY.ServiceManager.HRPYConfig.SaveTavi50PeriodSetting(periods);
            ServiceManager.CreateInstance(CompanyCode).ESSConfig.SaveTavi50PeriodSetting(periods, path);
        }
    }
}
