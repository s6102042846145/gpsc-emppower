﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.PY.CONFIG
{
    public class LetterEmbassySorting : IComparer<LetterEmbassy>
    {
        #region IComparer<LetterEmbassy> Members

        public int Compare(LetterEmbassy x, LetterEmbassy y)
        {
            for (int i = 0; i < (x.EmbassyName.Length < y.EmbassyName.Length ? x.EmbassyName.Length : y.EmbassyName.Length); i++)
            {
                if ((int)x.EmbassyName.ToCharArray()[i] > (int)y.EmbassyName.ToCharArray()[i])
                    return 1;
                else if ((int)x.EmbassyName.ToCharArray()[i] < (int)y.EmbassyName.ToCharArray()[i])
                    return -1;
                else
                    continue;
            }
            return 0;
        }

        #endregion
    }
}
