﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using ESS.UTILITY.EXTENSION;
using ESS.HR.PY.DATACLASS;

namespace ESS.HR.PY.CONFIG
{
    [Serializable()]
    public class PayslipPeriodSetting : AbstractObject
    {
        private int periodID = -1;
        private bool active = false;
        private bool isOffCycle = false;
        private bool isDraft = false;
        private int periodYear = -1;
        private int periodMonth = -1;
        private DateTime offCycleDate = DateTime.MinValue;
        private DateTime effectiveDate = DateTime.MinValue;
        private bool IsEvaluationHistory = false;

        private CultureInfo oCL = new CultureInfo("en-US");

        public PayslipPeriodSetting()
        {

        }

        public string CompanyCode { get; set; }
        public int PeriodID
        {
            get
            {
                return periodID;
            }
            set
            {
                periodID = value;
            }
        }

        public bool Active
        {
            get
            {
                return active;
            }
            set
            {
                active = value;
            }
        }
        public bool IsOffCycle
        {
            get
            {
                return isOffCycle;
            }
            set
            {
                isOffCycle = value;
            }
        }

        public bool IsDraft
        {
            get
            {
                return isDraft;
            }
            set
            {
                isDraft = value;
            }
        }

        public int PeriodYear
        {
            get
            {
                return periodYear;
            }
            set
            {
                periodYear = value;
            }
        }
        public int PeriodMonth
        {
            get
            {
                return periodMonth;
            }
            set
            {
                periodMonth = value;
            }
        }
        public DateTime OffCycleDate
        {
            get
            {
                return offCycleDate;
            }
            set
            {
                offCycleDate = value;
            }
        }
        public DateTime EffectiveDate
        {
            get
            {
                return effectiveDate;
            }
            set
            {
                effectiveDate = value;
            }
        }

        public bool isEvaluationHistory
        {
            get
            {
                return IsEvaluationHistory;
            }
            set
            {
                IsEvaluationHistory = value;
            }
        }

        public string PeriodUniqueCode
        {
            get
            {
                if (IsOffCycle)
                {
                    return string.Format("Y|{0}", OffCycleDate.ToString("yyyyMMdd", oCL));
                }
                else
                {
                    return string.Format("N|{0}|{1}", PeriodYear, PeriodMonth);
                }
            }
        }


        public override int GetHashCode()
        {
            return string.Format("PAYSLIPERIOD_{0}", PeriodUniqueCode).GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return this.GetHashCode() == obj.GetHashCode();
        }

        public PayslipPeriodSetting Parse(string Value)
        {
            int periodID = -1;
            if (!int.TryParse(Value, out periodID))
            {
                periodID = -1;
            }


            return GetPeriod(periodID);
        }

        public List<PayslipPeriodSetting> GetPeriodForSetting()
        {
            //return HR.PY.ServiceManager.HRPYConfig.GetAllPayslipPeriod();
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAllPayslipPeriod();
        }

        public List<PayslipPeriodSetting> GetActivePeriod()
        {
            //return HR.PY.ServiceManager.HRPYConfig.GetActivePayslipPeriod();
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetActivePayslipPeriod();
        }

        public List<PayslipPeriodSetting> GetPeriodSettingAll()
        {
            //return HR.PY.ServiceManager.HRPYConfig.GetAllPayslipPeriod();
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetAllPayslipPeriod();
        }

        public void SavePeriodSetting(List<PayslipPeriodSetting> periods)
        {
            //HR.PY.ServiceManager.HRPYConfig.SavePeriodSetting(periods);
            ServiceManager.CreateInstance(CompanyCode).ESSConfig.SavePeriodSetting(periods);
        }

        
        public List<PayslipPeriodSetting> GetPeriodSettingByYear(int year)
        {
            //return HR.PY.ServiceManager.HRPYConfig.GetPeriodSettingByYear(year);
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetPeriodSettingByYear(year);
        }

        
        

        public List<PaymentDetailRecord> GetPayslipDetail(string EmployeeID, string Period, bool isFindByTimePeriod)
        {
            //return HR.PY.ServiceManager.HRPYService.GetPaymentDetail(EmployeeID, Period, isFindByTimePeriod);
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetPaymentDetail(EmployeeID, Period, isFindByTimePeriod);
        }

        public PayslipResult GetPayslip(string EmployeeID, PayslipPeriodSetting Period)
        {
            //return HR.PY.ServiceManager.HRPYService.GetPayslip(EmployeeID, Period);
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetPayslip(EmployeeID, Period);
        }

        public PayslipPeriodSetting GetPeriod(int periodID)
        {
            //return HR.PY.ServiceManager.HRPYConfig.GetPeriod(periodID);
            return ServiceManager.CreateInstance(CompanyCode).ESSConfig.GetPeriod(periodID);
        }

    }
}
