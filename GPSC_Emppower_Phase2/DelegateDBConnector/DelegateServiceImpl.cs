using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using ESS.DELEGATE.ABSTRACT;
using ESS.DELEGATE.DATACLASS;
using ESS.EMPLOYEE.DATACLASS;
using ESS.SHAREDATASERVICE;

/*
 * CreatedBy: Ratchatawan W. (2011-11-28)
*/

namespace ESS.DELEGATE.DB
{
    public class DelegateServiceImpl : AbstractDelegateService
    {
        public DelegateServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
            oSqlManage["BaseConnStr"] = BaseConnStr;
        }

        #region Member
        //private Dictionary<string, string> Config { get; set; }
        private Dictionary<string, string> oSqlManage = new Dictionary<string, string>();
        private static string ModuleID = "ESS.DELEGATE.DB";

        public string CompanyCode { get; set; }
        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }

        #endregion Member

        #region Variable

        private static Configuration __config;

        #endregion Variable

        #region Properties

        private static Configuration config
        {
            get
            {
                if (__config == null)
                {
                    __config = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().CodeBase.Replace("file:///", "").Replace("/", "\\"));
                }
                return __config;
            }
        }

        private string ConnectionString
        {
            get
            {
                if (config == null || config.AppSettings.Settings["BaseConnStr"] == null)
                {
                    return "";
                }
                else
                {
                    return config.AppSettings.Settings["BaseConnStr"].Value;
                }
            }
        }

        private string PINConnectionString
        {
            get
            {
                if (config == null || config.AppSettings.Settings["PINConnStr"] == null)
                {
                    return "";
                }
                else
                {
                    return config.AppSettings.Settings["PINConnStr"].Value;
                }
            }
        }

        #endregion Properties

        public override DataTable GetRequestTypeForAdmin(string EmployeeID)
        {
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            //Set Storeprocedure Name
            SqlCommand oCommand = new SqlCommand("sp_GetRequestTypeForAdmin", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);

            //Parameters1
            SqlParameter oParam;
            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oConnection.Open();

            DataTable oTable = new DataTable("DELEGATE");
            oAdapter.Fill(oTable);

            return oTable;
        }

        /// <summary>
        /// Used to get delegate data for Edit Page.
        /// </summary>
        /// <param name="EmployeeID">Employee ID</param>
        /// <returns></returns>
        public override DataTable GetDelegateData(string EmployeeID)
        {
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            //Set Storeprocedure Name
            SqlCommand oCommand = new SqlCommand("sp_DelegateDataGet", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);

            //Parameters1
            SqlParameter oParam;
            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oConnection.Open();

            DataTable oTable = new DataTable("DELEGATE");
            oAdapter.Fill(oTable);

            return oTable;
        }

        /// <summary>
        /// Used to get delegate data following user permission in RequestType and it returns data in current month first.
        /// You can pass 'Month' parameter for get data in others month (Used in Content Page)
        /// </summary>
        /// <param name="EmployeeID">Employee ID</param>
        /// <param name="Month">Month that you want to data</param>
        /// <returns></returns>
        public override DataTable GetDelegateData(string EmployeeID, int Month, int Year)
        {
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            //Set Storeprocedure Name
            SqlCommand oCommand = new SqlCommand("sp_DelegateDataGetByMonth", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);

            //Parameters1
            SqlParameter oParam;
            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            //Parameters2
            oParam = new SqlParameter("@p_Month", SqlDbType.Int);
            oParam.Value = Month;
            oCommand.Parameters.Add(oParam);
            //Parameters3
            oParam = new SqlParameter("@p_Year", SqlDbType.Int);
            oParam.Value = Year;
            oCommand.Parameters.Add(oParam);

            oConnection.Open();

            DataTable oTable = new DataTable("DELEGATE");
            oAdapter.Fill(oTable);

            return oTable;
        }

        public override List<DelegateHeader> GetDelegateRequest(string EmployeeID, int Month, int Year)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            //Set Storeprocedure Name
            SqlCommand oCommand = new SqlCommand("sp_DelegateRequestGetByMonth", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);

            //Parameters1
            SqlParameter oParam;
            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            //Parameters2
            oParam = new SqlParameter("@p_Month", SqlDbType.Int);
            oParam.Value = Month;
            oCommand.Parameters.Add(oParam);
            //Parameters3
            oParam = new SqlParameter("@p_Year", SqlDbType.Int);
            oParam.Value = Year;
            oCommand.Parameters.Add(oParam);

            oConnection.Open();

            DataTable oTable = new DataTable("DELEGATE");
            oAdapter.Fill(oTable);

            List<DelegateHeader> oReturn = new List<DelegateHeader>();
            foreach (DataRow dr in oTable.Rows)
            {
                DelegateHeader item = new DelegateHeader();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oTable.Dispose();

            return oReturn;
        }

        public override void UpdateDelegate(DelegateData data)
        {
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            oConnection.Open();
            SqlTransaction oTransaction = oConnection.BeginTransaction();
            SqlCommand oCommand = new SqlCommand("sp_DelegateDataUpdate", oConnection, oTransaction);
            oCommand.CommandType = CommandType.StoredProcedure;

            #region Parameter

            SqlParameter oParam;

            oParam = new SqlParameter("@p_DelegateID", SqlDbType.Int);
            oParam.Value = data.DelegateID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_DelegateFrom", SqlDbType.VarChar);
            oParam.Value = data.DelegateFrom;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_DelegateFromPosition", SqlDbType.VarChar);
            oParam.Value = data.DelegateFromPosition;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_DelegateTo", SqlDbType.VarChar);
            oParam.Value = data.DelegateTo;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_DelegateToPosition", SqlDbType.VarChar);
            oParam.Value = data.DelegateToPosition;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_BeginDate", SqlDbType.DateTime);
            oParam.Value = data.BeginDate;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_EndDate", SqlDbType.DateTime);
            oParam.Value = data.EndDate;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_RequestTypeID", SqlDbType.Int);
            oParam.Value = data.RequestTypeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_Remark", SqlDbType.VarChar);
            oParam.Value = data.Remark;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_UpdatedBy", SqlDbType.VarChar);
            oParam.Value = data.UpdatedBy;
            oCommand.Parameters.Add(oParam);

            #endregion Parameter

            try
            {
                oCommand.ExecuteNonQuery();
                oTransaction.Commit();
            }
            catch (Exception ex)
            {
                oTransaction.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oTransaction.Dispose();
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
            }
        }

        public override void CreateDelegate(DelegateData data)
        {
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            oConnection.Open();
            SqlTransaction oTransaction = oConnection.BeginTransaction();
            SqlCommand oCommand = new SqlCommand("sp_DelegateDataCreate", oConnection, oTransaction);
            oCommand.CommandType = CommandType.StoredProcedure;

            #region Parameter

            SqlParameter oParam;

            oParam = new SqlParameter("@p_DelegateFrom", SqlDbType.VarChar);
            oParam.Value = data.DelegateFrom;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_DelegateFromPosition", SqlDbType.VarChar);
            oParam.Value = data.DelegateFromPosition;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_DelegateTo", SqlDbType.VarChar);
            oParam.Value = data.DelegateTo;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_DelegateToPosition", SqlDbType.VarChar);
            oParam.Value = data.DelegateToPosition;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_BeginDate", SqlDbType.DateTime);
            oParam.Value = data.BeginDate;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_EndDate", SqlDbType.DateTime);
            oParam.Value = data.EndDate;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_RequestTypeID", SqlDbType.Int);
            oParam.Value = data.RequestTypeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_Remark", SqlDbType.VarChar);
            oParam.Value = data.Remark;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_CreatedBy", SqlDbType.VarChar);
            oParam.Value = data.CreatedBy;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_RequestNo", SqlDbType.VarChar);
            oParam.Value = data.RequestNo;
            oCommand.Parameters.Add(oParam);

            #endregion Parameter

            try
            {
                oCommand.ExecuteNonQuery();
                oTransaction.Commit();
            }
            catch (Exception ex)
            {
                oTransaction.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oTransaction.Dispose();
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
            }
        }

        public override void CreateDelegate(DelegateHeader data)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction oTransaction = oConnection.BeginTransaction();
            SqlCommand oCommand = new SqlCommand("sp_DelegateRequestSave", oConnection, oTransaction);
            oCommand.CommandType = CommandType.StoredProcedure;

            #region Parameter

            SqlParameter oParam;

            oParam = new SqlParameter("@p_DelegateFrom", SqlDbType.VarChar);
            oParam.Value = data.DelegateFrom;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_DelegateFromName", SqlDbType.VarChar);
            oParam.Value = data.DelegateFromName;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_DelegateFromPosition", SqlDbType.VarChar);
            oParam.Value = data.DelegateFromPosition;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_DelegateFromPositionName", SqlDbType.VarChar);
            oParam.Value = data.DelegateFromPositionName;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_DelegateFromOrg", SqlDbType.VarChar);
            oParam.Value = data.DelegateFromOrg;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_DelegateFromOrgName", SqlDbType.VarChar);
            oParam.Value = data.DelegateFromOrgName;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_DelegateFromCompanyCode", SqlDbType.VarChar);
            oParam.Value = data.DelegateFromCompanyCode;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_DelegateTo", SqlDbType.VarChar);
            oParam.Value = data.DelegateTo;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_DelegateToName", SqlDbType.VarChar);
            oParam.Value = data.DelegateToName;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_DelegateToPosition", SqlDbType.VarChar);
            oParam.Value = data.DelegateToPosition;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_DelegateToPositionName", SqlDbType.VarChar);
            oParam.Value = data.DelegateToPositionName;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_DelegateToOrg", SqlDbType.VarChar);
            oParam.Value = data.DelegateToOrg;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_DelegateToOrgName", SqlDbType.VarChar);
            oParam.Value = data.DelegateToOrgName;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_DelegateToCompanyCode", SqlDbType.VarChar);
            oParam.Value = data.DelegateToCompanyCode;
            oCommand.Parameters.Add(oParam);
            
            oParam = new SqlParameter("@p_CreatedBy", SqlDbType.VarChar);
            oParam.Value = data.CreatedBy;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_CreatedByName", SqlDbType.VarChar);
            oParam.Value = data.CreatedByName;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_CreatedByPositionID", SqlDbType.VarChar);
            oParam.Value = data.CreatedByPositionID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_CreatedByCompanyCode", SqlDbType.VarChar);
            oParam.Value = data.CreatedByCompanyCode;
            oCommand.Parameters.Add(oParam);
            
            oParam = new SqlParameter("@p_BeginDate", SqlDbType.DateTime);
            oParam.Value = data.BeginDate;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_EndDate", SqlDbType.DateTime);
            oParam.Value = data.EndDate;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_Remark", SqlDbType.VarChar);
            oParam.Value = data.Remark;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_Status", SqlDbType.VarChar);
            oParam.Value = data.Status;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_RequestNo", SqlDbType.VarChar);
            oParam.Value = data.RequestNo;
            oCommand.Parameters.Add(oParam);
            #endregion Parameter

            try
            {
                oCommand.ExecuteNonQuery();
                oTransaction.Commit();
            }
            catch (Exception ex)
            {
                oTransaction.Rollback();
                throw new Exception("����ͧ��ӫ�͹", ex);
            }
            finally
            {
                oTransaction.Dispose();
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
            }
        }

        //Used this method to delete
        public override void DeleteDelegate(List<DelegateData> list)
        {
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            oConnection.Open();
            SqlTransaction oTransaction = oConnection.BeginTransaction();
            SqlCommand oCommand = new SqlCommand("sp_DelegateDataDelete", oConnection, oTransaction);
            oCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                foreach (DelegateData delegateData in list)
                {
                    SqlParameter oParam = new SqlParameter("@p_DelegateID", SqlDbType.Int);
                    oParam.Value = delegateData.DelegateID;
                    oCommand.Parameters.Add(oParam);
                    oCommand.ExecuteNonQuery();
                }

                oTransaction.Commit();
            }
            catch (Exception ex)
            {
                oTransaction.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oTransaction.Dispose();
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
            }
        }

        public override int CheckDupplicate(DelegateData data)
        {
            int result = 0;
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("sp_DelegateDataCheckDupplicate", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            #region Parameter

            SqlParameter oParam;

            oParam = new SqlParameter("@p_DelegateID", SqlDbType.Int);
            oParam.Value = data.DelegateID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_DelegateFrom", SqlDbType.VarChar);
            oParam.Value = data.DelegateFrom;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_DelegateFromPosition", SqlDbType.VarChar);
            oParam.Value = data.DelegateFromPosition;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_DelegateTo", SqlDbType.VarChar);
            oParam.Value = data.DelegateTo;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_DelegateToPosition", SqlDbType.VarChar);
            oParam.Value = data.DelegateToPosition;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_BeginDate", SqlDbType.DateTime);
            oParam.Value = data.BeginDate;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_EndDate", SqlDbType.DateTime);
            oParam.Value = data.EndDate;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_RequestTypeID", SqlDbType.Int);
            oParam.Value = data.RequestTypeID;
            oCommand.Parameters.Add(oParam);

            #endregion Parameter

            oConnection.Open();

            try
            {
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(oCommand);
                da.Fill(dt);
                result = Convert.ToInt32(dt.Rows[0][0].ToString());
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
            }

            return result;
        }

        #region Old Code

        /*
        public List<DelegateData> GetDelegateData(string EmployeeID, DelegateFilter filter)
        {
            SecurityService.SecurityService oService = new ESS.DELEGATE.SecurityService.SecurityService();
            ESS.DELEGATE.SecurityService.DelegateFilter oFilter = new ESS.DELEGATE.SecurityService.DelegateFilter();
            if (filter.DelegateFromMe)
            {
                oFilter.Depositor = EmployeeID;
            }
            if (filter.DelegateToMe)
            {
                oFilter.Receiver = EmployeeID;
            }
            oFilter.IsActive = filter.ActiveOnly;
            //ESS.DELEGATE.SecurityService.DelegateData[] data = oService.GetDelegateData(oFilter);
            ESS.DELEGATE.SecurityService.DelegateData[] data = new ESS.DELEGATE.SecurityService.DelegateData[] { };
            List<DelegateData> retVal = new List<DelegateData>();
            foreach (ESS.DELEGATE.SecurityService.DelegateData item in data)
            {
                DelegateData d = new DelegateData();
                d.BeginDate = item.BeginDate;
                d.DelegateID = item.DelegateID;
                d.DelegateTo = item.DelegateTo;
                d.EmployeeID = item.EmployeeID;
                d.EndDate = item.EndDate;
                d.RequestNo = item.RefDocumentID;
                retVal.Add(d);
            }
            retVal.Sort(new DelegateDataComparer());
            return retVal;
        }
        */

        #endregion Old Code

        #region IDELEGATE Members

        public override DataTable GetRequestType(string EmployeeID)
        {
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            //Set Storeprocedure Name
            SqlCommand oCommand = new SqlCommand("sp_GetDDLRequestType", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);

            //Parameters
            SqlParameter oParam;
            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oConnection.Open();

            DataTable oTable = new DataTable("REQUESTTYPE");
            oAdapter.Fill(oTable);

            return oTable;
        }

        #endregion IDELEGATE Members
    }
}