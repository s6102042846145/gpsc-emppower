using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using ESS.HR.PA.ABSTRACT;
using ESS.HR.PA.DATACLASS;
using ESS.HR.PA.INFOTYPE;
using ESS.SHAREDATASERVICE;
using ESS.UTILITY.EXTENSION;

namespace ESS.HR.PA.DB
{
    public class HRPADataServiceImpl : AbstractHRPADataService
    {
        #region Constructor
        public HRPADataServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
            //Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID);
        }

        #endregion Constructor

        #region Member
        //private Dictionary<string, string> Config { get; set; }

        private static string ModuleID = "ESS.HR.PA.DB";


        public string CompanyCode { get; set; }
        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }

        private string KeySecret
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "ESS.HR.PY", "GPSC_ENCRYPTION_KEY");
            }
        }

        #endregion Member

        #region " private data "

        //private string ConnectionString
        //{
        //    get
        //    {
        //        return config.AppSettings.Settings["BaseConnStr"].Value;
        //    }
        //}

        //private string GetConnectionString(string profile)
        //{
        //    return config.AppSettings.Settings[profile].Value;
        //}

        #endregion " private data "


        #region " GetPersonalData "

        public override INFOTYPE0002 GetPersonalData(string EmployeeID, DateTime CheckDate)
        {
            INFOTYPE0002 returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE0002 Where EmployeeID = @EmpID and @CheckDate between BeginDate and EndDate", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EMPLOYEE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new INFOTYPE0002();
            returnValue.ParseToObject(oTable);
            oTable.Dispose();
            return returnValue;
        }

        #endregion " GetPersonalData "

        #region " SaveInfoType0002 "

        public override void SaveInfotype0002(string EmployeeID1, string EmployeeID2, List<INFOTYPE0002> List, string Profile)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand1 = new SqlCommand("delete from INFOTYPE0002 where EmployeeID between @EmpID1 and @EmpID2", oConnection, tx);
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand1.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand1.Parameters.Add(oParam);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE0002 where EmployeeID between @EmpID1 and @EmpID2", oConnection, tx);
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("INFOTYPE0002");
            try
            {
                oCommand1.ExecuteNonQuery();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                foreach (INFOTYPE0002 item in List)
                {
                    item.LoadDataToTable(oTable);
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand1.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }

        #endregion " SaveInfoType0002 "

        #region " MarkUpdate "

        public override void MarkUpdate(string EmployeeID, string DataCategory, string RequestNo, bool isMarkIn)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select * from MarkData where EmployeeID = @EmpID and DataCategory = @DataCategory ", oConnection, tx);
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@DataCategory", SqlDbType.VarChar);
            oParam.Value = DataCategory;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("MARKDATA");
            try
            {
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                if (oTable.Rows.Count == 0 && isMarkIn)
                {
                    DataRow oRow = oTable.NewRow();
                    oRow["EmployeeID"] = EmployeeID;
                    oRow["DataCategory"] = DataCategory;
                    oRow["RequestNo"] = RequestNo;
                    oTable.Rows.Add(oRow);
                }
                else if (oTable.Rows.Count > 0 && !isMarkIn)
                {
                    foreach (DataRow dr in oTable.Select())
                    {
                        dr.Delete();
                    }
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }

        #endregion " MarkUpdate "

        #region " CheckMark "

        public override bool CheckMark(string EmployeeID, string DataCategory)
        {
            bool oReturn = false;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select Count(*) from MarkData where EmployeeID =" +
                                                 " @EmpID and DataCategory = @DataCategory", oConnection);
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@DataCategory", SqlDbType.VarChar);
            oParam.Value = DataCategory;
            oCommand.Parameters.Add(oParam);
            try
            {
                oConnection.Open();
                oReturn = ((int)oCommand.ExecuteScalar()) > 0;

            }
            catch (Exception ex)
            {
                throw new Exception("CheckMark error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
            }
            return oReturn;
        }

        #endregion " CheckMark "

        #region Check MarkData

        public override bool CheckMark(string EmployeeID, string DataCategory, string ReqNo)
        {
            bool oReturn = false;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select Count(*) from MarkData where EmployeeID =" +
                                                 " @EmpID and DataCategory = @DataCategory and RequestNo <> @ReqNo", oConnection);
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@DataCategory", SqlDbType.VarChar);
            oParam.Value = DataCategory;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@ReqNo", SqlDbType.VarChar);
            oParam.Value = ReqNo;
            oCommand.Parameters.Add(oParam);
            try
            {
                oConnection.Open();
                oReturn = ((int)oCommand.ExecuteScalar()) > 0;

            }
            catch (Exception ex)
            {
                throw new Exception("CheckMark error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
            }
            return oReturn;
        }

        #endregion Check MarkData

        //public override void SavePersonalData(INFOTYPE0002 data)
        //{
        //    SqlConnection oConnection = new SqlConnection(.GetConnectionString("INFOTYPE0002"));
        //    SqlCommand oCommand = new SqlCommand("select * from INFOTYPE0002 Where EmployeeID = @EmpID and @CheckDate between BeginDate and EndDate", oConnection);
        //    SqlParameter oParam;
        //    oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
        //    oParam.Value = EmployeeID;
        //    oCommand.Parameters.Add(oParam);

        //    oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
        //    oParam.Value = CheckDate;
        //    oCommand.Parameters.Add(oParam);

        //    SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
        //    DataTable oTable = new DataTable("EMPLOYEE");
        //    oConnection.Open();
        //    oAdapter.Fill(oTable);
        //    oConnection.Close();
        //    oConnection.Dispose();
        //    oCommand.Dispose();
        //    returnValue = new INFOTYPE0002();
        //    returnValue.ParseToObject(oTable);
        //    oTable.Dispose();
        //}

        public override void SaveWorkPlace(List<WorkPlaceData> list)
        {
            SqlConnection oConnection;
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            SqlCommandBuilder oBuilder;
            DataTable oTable;
            DataRow dRow;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from WorkPlaceData", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oBuilder = new SqlCommandBuilder(oAdapter);
                    oTable = new DataTable("WorkPlaceData");
                    oConnection.Open();
                    oAdapter.FillSchema(oTable, SchemaType.Source);
                    oAdapter.Fill(oTable);

                    foreach (WorkPlaceData item in list)
                    {
                        dRow = oTable.NewRow();
                        item.LoadDataToTableRow(dRow);
                        oTable.LoadDataRow(dRow.ItemArray, false);
                    }
                    oAdapter.Update(oTable);
                    oConnection.Close();
                }
            }
        }

        //no param in getWorkPlace() --> select all, no condition
        public override List<WorkPlaceData> GetWorkPlace()
        {
            List<WorkPlaceData> returnValue = new List<WorkPlaceData>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable;
            SqlCommandBuilder oBuilder;
            WorkPlaceData wData;
            using (oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (oCommand = new SqlCommand("select * from WorkPlaceData", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("WorkPlaceData");
                    oBuilder = new SqlCommandBuilder(oAdapter);

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        wData = new WorkPlaceData();
                        wData.ParseToObject(dr);
                        returnValue.Add(wData);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        public override List<INFOTYPE0105> GetInfotype0105ByCategoryCode(string EmployeeID, string oCategoryCode)
        {

            List<INFOTYPE0105> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_PA_INFOTYPE0105Get", oConnection);

            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_EmployeeID", EmployeeID);
            oCommand.Parameters.AddWithValue("@p_CategoryCode", oCategoryCode);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EMPLOYEE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<INFOTYPE0105>();
            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE0105 item = new INFOTYPE0105();
                item.ParseToObject(dr);
                returnValue.Add(item);
            }
            oTable.Dispose();
            return returnValue;
        }

        public override void UpdateINFOTYPE0105(INFOTYPE0105 oINF105)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            string queryStr = "sp_INFOTYPE0105Update";
            oCommand = new SqlCommand(queryStr, oConnection, tx);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_EmployeeID", oINF105.EmployeeID);
            oCommand.Parameters.AddWithValue("@p_BeginDate", oINF105.BeginDate);
            oCommand.Parameters.AddWithValue("@p_EndDate", oINF105.EndDate == DateTime.MaxValue.AddTicks(-9999999) ? oINF105.EndDate : oINF105.EndDate.TimeOfDay.Seconds == 59 ? oINF105.EndDate : oINF105.EndDate.AddSeconds(-1).AddDays(1));
            oCommand.Parameters.AddWithValue("@p_SubType", oINF105.CategoryCode);
            oCommand.Parameters.AddWithValue("@p_DataText_Long", oINF105.DataText);
            oAdapter = new SqlDataAdapter(oCommand);
            string oAction = "UPDATE INFOTYPE0105 ";
            bool oStatus = false;
            try
            {
                oCommand.ExecuteNonQuery();
                tx.Commit();
                oAction += "Completed";
                oStatus = true;
            }
            catch (Exception ex)
            {
                tx.Rollback();
                oAction += "Failed : " + ex.Message;
                throw new Exception("update data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                //CallActionLog(null, oINF105, oAction, oStatus);
            }
        }

        public override void InsertINFOTYPE0105(INFOTYPE0105 oINF105)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand;
            string queryStr = "sp_INFOTYPE0105Insert";
            oCommand = new SqlCommand(queryStr, oConnection, tx);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_EmployeeID", oINF105.EmployeeID);
            oCommand.Parameters.AddWithValue("@p_BeginDate", oINF105.BeginDate);
            oCommand.Parameters.AddWithValue("@p_EndDate", oINF105.EndDate == DateTime.MaxValue.AddTicks(-9999999) ? oINF105.EndDate : oINF105.EndDate.TimeOfDay.Seconds == 59 ? oINF105.EndDate : oINF105.EndDate.AddSeconds(-1).AddDays(1));
            oCommand.Parameters.AddWithValue("@p_SubType", oINF105.CategoryCode);
            oCommand.Parameters.AddWithValue("@p_DataText_Long", oINF105.DataText);
            string oAction = "INSERT INFOTYPE0105 ";
            bool oStatus = false;
            try
            {
                oCommand.ExecuteNonQuery();
                tx.Commit();
                oAction += "Completed";
                oStatus = true;
            }
            catch (Exception ex)
            {
                tx.Rollback();
                oAction += "Failed : " + ex.Message;
                throw new Exception("insert data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                //CallActionLog(null, oINF105, oAction, oStatus);
            }
        }

        public override void SaveINFOTYPE0105(List<INFOTYPE0105> oListINF105Update, List<INFOTYPE0105> oListINF105Insert)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand, oCommand1;
            string oAction = "UPDATE INFOTYPE0105 ";
            bool oStatus = false;
            oCommand = new SqlCommand("sp_INFOTYPE0105Update", oConnection, tx);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand1 = new SqlCommand("sp_INFOTYPE0105Insert", oConnection, tx);
            oCommand1.CommandType = CommandType.StoredProcedure;
            try
            {
                foreach (INFOTYPE0105 oINF105Update in oListINF105Update)
                {
                    oCommand.Parameters.Clear();
                    oCommand.Parameters.AddWithValue("@p_EmployeeID", oINF105Update.EmployeeID);
                    oCommand.Parameters.AddWithValue("@p_BeginDate", oINF105Update.BeginDate);
                    oCommand.Parameters.AddWithValue("@p_EndDate", oINF105Update.EndDate == DateTime.MaxValue.AddTicks(-9999999) ? oINF105Update.EndDate : oINF105Update.EndDate.TimeOfDay.Seconds == 59 ? oINF105Update.EndDate : oINF105Update.EndDate.AddSeconds(-1).AddDays(1));
                    oCommand.Parameters.AddWithValue("@p_SubType", oINF105Update.CategoryCode);
                    oCommand.Parameters.AddWithValue("@p_DataText_Long", oINF105Update.DataText);
                    oCommand.ExecuteNonQuery();
                }
                foreach (INFOTYPE0105 oINF105Insert in oListINF105Insert)
                {
                    oCommand1.Parameters.Clear();
                    oCommand1.Parameters.AddWithValue("@p_EmployeeID", oINF105Insert.EmployeeID);
                    oCommand1.Parameters.AddWithValue("@p_BeginDate", oINF105Insert.BeginDate);
                    oCommand1.Parameters.AddWithValue("@p_EndDate", oINF105Insert.EndDate == DateTime.MaxValue.AddTicks(-9999999) ? oINF105Insert.EndDate : oINF105Insert.EndDate.TimeOfDay.Seconds == 59 ? oINF105Insert.EndDate : oINF105Insert.EndDate.AddSeconds(-1).AddDays(1));
                    oCommand1.Parameters.AddWithValue("@p_SubType", oINF105Insert.CategoryCode);
                    oCommand1.Parameters.AddWithValue("@p_DataText_Long", oINF105Insert.DataText);
                    oCommand1.ExecuteNonQuery();
                }

                tx.Commit();
                oAction += "Completed";
                oStatus = true;
            }
            catch (Exception ex)
            {
                tx.Rollback();
                oAction += "Failed : " + ex.Message;
                throw new Exception("update data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                oCommand1.Dispose();
            }
        }

        #region PA AttachmentFileSet
        public override List<PersonalAttachmentFileSet> GetAttachmentFileSet(string EmployeeID, int RequestTypeID, string RequestSubType)
        {

            List<PersonalAttachmentFileSet> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_AttachmentFileSetGet", oConnection);

            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_RequestorNo", EmployeeID);
            oCommand.Parameters.AddWithValue("@p_RequestTypeID", RequestTypeID);
            oCommand.Parameters.AddWithValue("@p_RequestSubType", RequestSubType);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("FILESET");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<PersonalAttachmentFileSet>();
            foreach (DataRow dr in oTable.Rows)
            {
                PersonalAttachmentFileSet item = new PersonalAttachmentFileSet();
                item.ParseToObject(dr);
                returnValue.Add(item);
            }
            oTable.Dispose();
            return returnValue;
        }

        public override List<PersonalAttachmentFileSet> GetAttachmentExport(string EmployeeID, int RequestTypeID, string RequestSubType)
        {

            List<PersonalAttachmentFileSet> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_AttachmentExportGet", oConnection);

            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_RequestorNo", EmployeeID);
            oCommand.Parameters.AddWithValue("@p_RequestTypeID", RequestTypeID);
            oCommand.Parameters.AddWithValue("@p_RequestSubType", RequestSubType);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("FILESET");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new List<PersonalAttachmentFileSet>();
            foreach (DataRow dr in oTable.Rows)
            {
                PersonalAttachmentFileSet item = new PersonalAttachmentFileSet();
                item.ParseToObject(dr);
                returnValue.Add(item);
            }
            oTable.Dispose();
            return returnValue;
        }

        public override void FileAttachmentSubTypeSave(string RequestNo, string RequestSubType)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlCommand oCommand;
            string queryStr = "sp_FileAttachmentSubTypeSave";
            oCommand = new SqlCommand(queryStr, oConnection, tx);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.AddWithValue("@p_RequestNo", RequestNo);
            oCommand.Parameters.AddWithValue("@p_RequestSubType", RequestSubType);
            try
            {
                oCommand.ExecuteNonQuery();
                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("insert data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
            }
        }
        #endregion

        public override List<InfotypeAutomaticManagement> GetAllInfotypeAutomaticList()
        {
            List<InfotypeAutomaticManagement> returnValue = new List<InfotypeAutomaticManagement>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand();
            oCommand.CommandText = "select * from InfotypeAutomaticManagement"; ;
            oCommand.Connection = oConnection;
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("InfotypeAutomaticManagement");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                InfotypeAutomaticManagement item = new InfotypeAutomaticManagement();
                item.ParseToObject(dr);
                returnValue.Add(item);
            }
            oTable.Dispose();

            return returnValue;
        }

        #region Common
        private IList<T> ExecuteQuery<T>(string oProcedureName, Dictionary<string, object> oParamRequest)
        {
            IList<T> oResult = new List<T>();
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (SqlCommand oCommand = new SqlCommand(oProcedureName, oConnection))
                {
                    oCommand.CommandType = CommandType.StoredProcedure;
                    if (oParamRequest.Count > 0)
                    {
                        foreach (string oCurrnetKey in oParamRequest.Keys)
                        {
                            oCommand.Parameters.AddWithValue(oCurrnetKey, oParamRequest[oCurrnetKey]);
                        }
                    }
                    SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                    DataTable oTable = new DataTable("TableName");
                    try
                    {
                        oAdapter.Fill(oTable);
                        if (oTable != null && oTable.Rows.Count > 0)
                        {
                            oResult = Convert<T>.ConvertFrom(oTable);
                        }
                    }
                    catch (Exception ex)
                    {
                        //May be log 
                    }
                    finally
                    {
                        oConnection.Close();
                        oConnection.Dispose();
                        oCommand.Dispose();
                    }
                }
            }
            return oResult;
        }
        private bool ExecuteNoneQuery(string oProcedureName, Dictionary<string, object> oParamRequest)
        {
            bool oResult = false;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (SqlCommand oCommand = new SqlCommand(oProcedureName, oConnection))
                {
                    oCommand.CommandType = CommandType.StoredProcedure;
                    if (oParamRequest.Count > 0)
                    {
                        foreach (string oCurrnetKey in oParamRequest.Keys)
                        {
                            oCommand.Parameters.AddWithValue(oCurrnetKey, oParamRequest[oCurrnetKey]);
                        }
                    }
                    //SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                    //DataTable oTable = new DataTable("TableName");
                    try
                    {
                        oConnection.Open();
                        if (oCommand.ExecuteNonQuery() > 0)
                        {
                            oResult = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        //May be log 
                    }
                    finally
                    {
                        oConnection.Close();
                        oConnection.Dispose();
                        oCommand.Dispose();
                    }
                }
            }
            return oResult;
        }
        private string ExecuteScalar(string oProcedureName, Dictionary<string, object> oParamRequest)
        {
            string oResult = string.Empty;
            using (SqlConnection oConnection = new SqlConnection(this.BaseConnStr))
            {
                using (SqlCommand oCommand = new SqlCommand(oProcedureName, oConnection))
                {
                    oCommand.CommandType = CommandType.StoredProcedure;
                    if (oParamRequest.Count > 0)
                    {
                        foreach (string oCurrnetKey in oParamRequest.Keys)
                        {
                            oCommand.Parameters.AddWithValue(oCurrnetKey, oParamRequest[oCurrnetKey]);
                        }
                    }
                    //SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                    //DataTable oTable = new DataTable("TableName");
                    try
                    {
                        oConnection.Open();
                        oResult = Convert.ToString(oCommand.ExecuteScalar());
                    }
                    catch (Exception ex)
                    {
                        //May be log 
                    }
                    finally
                    {
                        oConnection.Close();
                        oConnection.Dispose();
                        oCommand.Dispose();
                    }
                }
            }
            return oResult;
        }
        #endregion

        #region Driver License ������㺢Ѻ���

        public override List<PersonalDriverLicenseData> GetDriverLicenseByEmployee(string EmployeeId)
        {
            List<PersonalDriverLicenseData> list_driverllicense = new List<PersonalDriverLicenseData>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_PA_GetDriverLicenseByEmployee", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;

            oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeId;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EmployeeDriverLicense");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            if (oTable.Rows.Count > 0)
            {
                foreach (DataRow dr in oTable.Rows)
                {
                    PersonalDriverLicenseData item = new PersonalDriverLicenseData();
                    item.ParseToObject(dr);
                    list_driverllicense.Add(item);
                }
            }

            return list_driverllicense;
        }

        public override List<PersonalDriverLicenseData> GetDriverLicenseNotifyExpiredByMonth(int iMonth, int iYear)
        {
            List<PersonalDriverLicenseData> oReturn = new List<PersonalDriverLicenseData>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_PA_GetDriverLicenseNotifyExpiredByMonth", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;

            oParam = new SqlParameter("@p_Month", SqlDbType.Int);
            oParam.Value = iMonth;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_Year", SqlDbType.Int);
            oParam.Value = iYear;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("DriverLicenseNotifyExpired");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            if (oTable.Rows.Count > 0)
            {
                foreach (DataRow dr in oTable.Rows)
                {
                    PersonalDriverLicenseData item = new PersonalDriverLicenseData();
                    item.ParseToObject(dr);
                    oReturn.Add(item);
                }
            }

            return oReturn;
        }

        public override List<DataDriverLicenseByJob> CheckDuplicateDriverLicense(List<DataDriverLicenseByJob> model)
        {
            List<DataDriverLicenseByJob> new_list_driver = new List<DataDriverLicenseByJob>();
            if (model.Count > 0)
            {
                SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
                oConnection.Open();
                foreach (var item in model)
                {
                    SqlCommand oCommand = new SqlCommand("Select * FROM DriverLicense Where EmployeeID = @empId AND EffectiveDate = @effectiveDate", oConnection);
                    oCommand.CommandType = CommandType.Text;

                    SqlParameter oParam;
                    oParam = new SqlParameter("@empId", SqlDbType.VarChar);
                    oParam.Value = "23" + item.EmployeeID;
                    oCommand.Parameters.Add(oParam);

                    oParam = new SqlParameter("@effectiveDate", SqlDbType.DateTime);
                    oParam.Value = item.EffectiveDate.Date;
                    oCommand.Parameters.Add(oParam);

                    SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                    DataTable oTable = new DataTable("TbDriverLicense");

                    oAdapter.Fill(oTable);
                    oCommand.Dispose();

                    if (oTable.Rows.Count == 0)
                    {
                        DataDriverLicenseByJob driver_new = new DataDriverLicenseByJob()
                        {
                            EmployeeID = item.EmployeeID,
                            EffectiveDate = item.EffectiveDate,
                            ExpireDate = item.ExpireDate,
                            FullNameTH = item.FullNameTH,
                            FullNameEN = item.FullNameEN,
                            LicenseID = item.LicenseID
                        };

                        // Add ������ DriverLicense ������ List
                        new_list_driver.Add(driver_new);
                    }
                }

                if (oConnection.State == ConnectionState.Open)
                    oConnection.Close();

                oConnection.Dispose();
            }

            return new_list_driver;
        }

        public override void SaveDriverLicenseByJob(List<DataDriverLicenseByJob> model)
        {
            if (model.Count > 0)
            {
                SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
                oConnection.Open();
                try
                {
                    foreach (var item in model)
                    {
                        SqlCommand oCommand = new SqlCommand("sp_PA_SaveDriverLicense", oConnection);
                        oCommand.CommandType = CommandType.StoredProcedure;

                        SqlParameter param;
                        param = new SqlParameter("@p_LicenseID", SqlDbType.VarChar);
                        param.Value = item.LicenseID;
                        oCommand.Parameters.Add(param);

                        param = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
                        param.Value = "23" + item.EmployeeID;
                        oCommand.Parameters.Add(param);

                        param = new SqlParameter("@p_FullNameTH", SqlDbType.VarChar);
                        param.Value = item.FullNameTH;
                        oCommand.Parameters.Add(param);

                        param = new SqlParameter("@p_FullNameEN", SqlDbType.VarChar);
                        param.Value = item.FullNameEN;
                        oCommand.Parameters.Add(param);

                        param = new SqlParameter("@p_EffectiveDate", SqlDbType.DateTime);
                        param.Value = item.EffectiveDate.Date;
                        oCommand.Parameters.Add(param);

                        param = new SqlParameter("@p_ExpireDate", SqlDbType.DateTime);
                        param.Value = item.ExpireDate.Date;
                        oCommand.Parameters.Add(param);

                        param = new SqlParameter("@p_CreateDate", SqlDbType.DateTime);
                        param.Value = DateTime.Now;
                        oCommand.Parameters.Add(param);

                        param = new SqlParameter("@p_Createby", SqlDbType.VarChar);
                        param.Value = "System";
                        oCommand.Parameters.Add(param);

                        oCommand.ExecuteNonQuery();
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    if (oConnection.State == ConnectionState.Open)
                        oConnection.Close();
                    oConnection.Dispose();
                }
            }
        }

        public override List<PersonalDriverLicenseData> GetDriverLicenseEmployeeAll()
        {
            var list_driverLicnese_all = new List<PersonalDriverLicenseData>();

            DateTime dateNow = DateTime.Now;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            string sql_command = "Select * FROM DriverLicense Where ExpireDate >= @effectiveDate  ORDER BY ExpireDate ASC";
            SqlCommand oCommand = new SqlCommand(sql_command, oConnection);
            oCommand.CommandType = CommandType.Text;

            SqlParameter oParam;
            oParam = new SqlParameter("@effectiveDate", SqlDbType.DateTime);
            oParam.Value = dateNow.Date;
            oCommand.Parameters.Add(oParam);

            oConnection.Open();

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("tbdriverLicense");

            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            if (oTable.Rows.Count > 0)
            {
                foreach (DataRow row in oTable.Rows)
                {
                    PersonalDriverLicenseData item = new PersonalDriverLicenseData();
                    item.ParseToObject(row);
                    list_driverLicnese_all.Add(item);
                }
            }


            return list_driverLicnese_all;
        }

        #endregion

        #region �͡��âͧ��ѡ�ҹ  Home

        public override int GetRequestByBoxId(int boxId, string employeeId, string positionId)
        {
            int count_document = 0;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);

            string sql_cmd = "select count(*) as total from dbo.[GetRequestByBoxID](@p_boxId,@p_employeeId,@p_positionId,@p_positionId,-1,'','','')";
            SqlCommand oCommand = new SqlCommand(sql_cmd, oConnection);
            oCommand.CommandType = CommandType.Text;

            oCommand.Parameters.Add("@p_boxId", SqlDbType.Int).Value = boxId;
            oCommand.Parameters.Add("@p_employeeId", SqlDbType.VarChar).Value = employeeId;
            oCommand.Parameters.Add("@p_positionId", SqlDbType.VarChar).Value = positionId;

            try
            {
                oConnection.Open();
                count_document = (int)oCommand.ExecuteScalar();
                return count_document;

            }
            catch (Exception ex)
            {
                throw new Exception("CheckMark error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
            }

        }
        #endregion

        #region Save Work Location

        public override void SaveOrUpdateEmployeeInWorkLocation(List<EmployeeWorklocationSAP> model)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction trn = oConnection.BeginTransaction();
            try
            {
                foreach (var item in model)
                {
                    SqlCommand oCommand = new SqlCommand("sp_PA_SaveOrUpdateWorkLocationEmployee", oConnection, trn);
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.Parameters.Clear();

                    oCommand.Parameters.Add("@p_EffectiveDate", SqlDbType.DateTime).Value = item.EffectiveDate.Date;
                    oCommand.Parameters.Add("@p_ExpireDate", SqlDbType.DateTime).Value = item.EndDate.Date;
                    oCommand.Parameters.Add("@p_WorkLocationId", SqlDbType.VarChar).Value = item.LocationId;
                    oCommand.Parameters.Add("@p_EmployeeId", SqlDbType.VarChar).Value = item.EmployeeId;
                    oCommand.Parameters.Add("@p_CompanyCode", SqlDbType.VarChar).Value = item.CompanyId;
                    oCommand.Parameters.Add("@p_CreateOrUpdateBy", SqlDbType.VarChar).Value = "System";
                    oCommand.Parameters.Add("@p_CreateOrUpdateDate", SqlDbType.VarChar).Value = DateTime.Now;
                    oCommand.ExecuteNonQuery();
                }
                trn.Commit();
            }
            catch
            {
                trn.Rollback();
                throw;
            }
            finally
            {
                oConnection.Dispose();
                trn.Dispose();
                if (oConnection.State == ConnectionState.Open)
                    oConnection.Close();
            }
        }

        public override void SaveOrUpdateWorkLocationMaster(List<MasterWorkLocationSAP> model)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction trn = oConnection.BeginTransaction();
            try
            {
                foreach (var item in model)
                {
                    SqlCommand oCommand = new SqlCommand("sp_PA_SaveOrUpdateWorkLocation", oConnection, trn);
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.Parameters.Clear();
                    oCommand.Parameters.Add("@p_EffectiveDate", SqlDbType.DateTime).Value = item.EffectiveDate.Date;
                    oCommand.Parameters.Add("@p_ExpireDate", SqlDbType.DateTime).Value = item.EndDate.Date;
                    oCommand.Parameters.Add("@p_WorkLocationId", SqlDbType.VarChar).Value = item.LocationId;
                    oCommand.Parameters.Add("@p_Name", SqlDbType.NVarChar).Value = item.LocationName;
                    oCommand.Parameters.Add("@p_CreateOrUpdateDate", SqlDbType.DateTime).Value = DateTime.Now;
                    oCommand.Parameters.Add("@p_CreateOrUpdateBy", SqlDbType.VarChar).Value = "System";
                    oCommand.ExecuteNonQuery();
                }
                trn.Commit();
            }
            catch
            {
                trn.Rollback();
                throw;
            }
            finally
            {
                oConnection.Dispose();
                trn.Dispose();
                if (oConnection.State == ConnectionState.Open)
                    oConnection.Close();
            }

        }

        #endregion

        public override DbPeriodSettingPaySlipByAdmin GetConfigPeroidSettingPayslipByAdmin()
        {
            var result = new DbPeriodSettingPaySlipByAdmin();

            var date_now = DateTime.Now;

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_PA_PeriodSettingByEvaluationHistory", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            oCommand.Parameters.Add("@p_PeriodYear", SqlDbType.Int).Value = date_now.Year;

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("ConfigPeriodSetting");

            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            if (oTable.Rows.Count > 0)
            {
                result.ParseToObject(oTable);
            }

            return result;
        }

        public override decimal GetSalaryByEmployee(string employeeId)
        {
            decimal salary = 0;

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);

            SqlCommand oCommand = new SqlCommand("sp_PA_GetSalaryByEmployee", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            var date_now = DateTime.Now;
            oCommand.Parameters.Add("@p_EmployeeID", SqlDbType.VarChar).Value = employeeId;
            oCommand.Parameters.Add("@p_PeriodYear", SqlDbType.Int).Value = date_now.Year;

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("PaySlipEmployee");

            oConnection.Open();
            oAdapter.Fill(oTable);
            if (oTable.Rows.Count > 0)
            {
                DbPaySlipByEmployee paySlip = new DbPaySlipByEmployee();

                paySlip.ParseToObject(oTable);

                string data_key = this.KeySecret + paySlip.EmployeeID.Trim();

                decimal amt = 0;
                if (!string.IsNullOrEmpty(paySlip.CurrencyAmount))
                {
                    string amt_decryp = DecryptAes256(paySlip.CurrencyAmount, data_key);
                    amt = Convert.ToDecimal(amt_decryp);
                    salary = amt;
                }
            }
            else
            {
                // ���Թ��͹�ջѨ�غѹ����� ��͹��ѧ �ѡ 3 ���ҡ����͡��

                for (int i = 1; i <= 3; i++)
                {
                    oCommand = new SqlCommand("sp_PA_GetSalaryByEmployee", oConnection);
                    oCommand.CommandType = CommandType.StoredProcedure;

                    DateTime date_diff = DateTime.Now.AddYears(-i);
                    oCommand.Parameters.Add("@p_EmployeeID", SqlDbType.VarChar).Value = employeeId;
                    oCommand.Parameters.Add("@p_PeriodYear", SqlDbType.Int).Value = date_diff.Year;
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("PaySlipEmployee");
                    oAdapter.Fill(oTable);
                    if (oTable.Rows.Count > 0)
                    {
                        DbPaySlipByEmployee paySlip = new DbPaySlipByEmployee();

                        paySlip.ParseToObject(oTable);

                        string data_key = this.KeySecret + paySlip.EmployeeID.Trim();
                        if (!string.IsNullOrEmpty(paySlip.CurrencyAmount))
                        {
                            string amt_decryp = DecryptAes256(paySlip.CurrencyAmount, data_key);
                            decimal amt = Convert.ToDecimal(amt_decryp);

                            salary = amt;
                        }
                        else
                        {
                            salary = 0;
                        }

                        break; // �͢��������� Break; loop �ѹ��
                    }
                }
            }


            if (oConnection.State == ConnectionState.Open)
                oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            oTable.Dispose();


            return salary;
        }

        public string DecryptAes256(string combinedString, string keyString)
        {
            string plainText;
            byte[] combinedData = Convert.FromBase64String(combinedString);
            Aes aes = Aes.Create();
            aes.KeySize = 256;
            aes.BlockSize = 128;
            aes.Key = Encoding.UTF8.GetBytes(keyString);
            byte[] iv = new byte[aes.BlockSize / 8];
            byte[] cipherText = new byte[combinedData.Length - iv.Length];
            Array.Copy(combinedData, iv, iv.Length);
            Array.Copy(combinedData, iv.Length, cipherText, 0, cipherText.Length);
            aes.IV = iv;
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;
            ICryptoTransform decipher = aes.CreateDecryptor(aes.Key, aes.IV);

            using (MemoryStream ms = new MemoryStream(cipherText))
            {
                using (CryptoStream cs = new CryptoStream(ms, decipher, CryptoStreamMode.Read))
                {
                    using (StreamReader sr = new StreamReader(cs))
                    {
                        plainText = sr.ReadToEnd();
                    }
                }

                return plainText;
            }
        }

        public override string GetWorkLocationNameByEmpID(string EmployeeID, DateTime CheckDate)
        {
            string sName = string.Empty;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_WorkLocationGetByEmpID", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter param;

            param = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            param.Value = EmployeeID;
            oCommand.Parameters.Add(param);

            param = new SqlParameter("@p_CheckDate", SqlDbType.DateTime);
            param.Value = CheckDate;
            oCommand.Parameters.Add(param);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("WorkLocation");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();

            if (oTable.Rows.Count > 0)
            {
                sName = oTable.Rows[0]["Name"].ToString();
            }

            oConnection.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();

            return sName;
        }

        public override string GetMailSubjectNotifyExpiredDriverLicense(int MailID, string Language)
        {
            string oReturn = string.Empty;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select dbo.GetMailSubjectByID(@MailID,@LanguageCode) ", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@MailID", SqlDbType.Int);
            oParam.Value = MailID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@LanguageCode", SqlDbType.VarChar);
            oParam.Value = Language;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("MailSubject");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oAdapter.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                oReturn = (string)dr[0];
                break;
            }
            oTable.Dispose();
            return oReturn;
        }

        public override List<DbPeriodSettingPaySlipByAdmin> GetAllPayslipPeriodSetting()
        {
            var payslip_period_all = new List<DbPeriodSettingPaySlipByAdmin>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            string sql_cmd = "SELECT * FROM[ESS_GPSC].[dbo].[PayslipPeriodSetting] pps  WHERE pps.IsEvaluationHistory = 1";
            SqlCommand oCommand = new SqlCommand(sql_cmd, oConnection);
            oCommand.CommandType = CommandType.Text;

            oConnection.Open();

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("MailSubject");

            oAdapter.Fill(oTable);

            if (oTable.Rows.Count > 0)
            {
                foreach(DataRow item in oTable.Rows)
                {
                    DbPeriodSettingPaySlipByAdmin setting_payslip = new DbPeriodSettingPaySlipByAdmin();
                    setting_payslip.ParseToObject(item);
                    payslip_period_all.Add(setting_payslip);
                }
            }

            oAdapter.Dispose();
            oTable.Dispose();
            oConnection.Dispose();

            if (oConnection.State == ConnectionState.Open)
                oConnection.Close();

            return payslip_period_all;
        }
    }
}