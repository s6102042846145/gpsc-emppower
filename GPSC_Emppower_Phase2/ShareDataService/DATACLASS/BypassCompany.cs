﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.SHAREDATASERVICE.DATACLASS
{
    public class BypassCompany
    {
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
    }
}
