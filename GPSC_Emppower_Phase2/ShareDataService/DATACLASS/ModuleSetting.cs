﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.SHAREDATASERVICE.DATACLASS
{
    public class ModuleSetting
    {
        public ModuleSetting()
        {

        }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public string ModuleID { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
    }
}
