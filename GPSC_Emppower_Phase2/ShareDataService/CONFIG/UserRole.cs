﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.SHAREDATASERVICE.CONFIG
{
    public class UserRoleClass
    {
        public string EmployeeID { get; set; }
        public string CompanyCode { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string FullNameEN { get; set; }
    }
}
