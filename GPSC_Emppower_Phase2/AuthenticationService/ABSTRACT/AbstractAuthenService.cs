using System;

namespace ESS.AUTHEN
{
    public class AbstractAuthenService : IAuthenService
    {
        #region IAuthenService Members

        public bool HavePassword(string UserName)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void ChangePassword(string UserName, string NewPassword)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void ChangePassword(string UserName, string OldPassword, string NewPassword)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void ClearPassword(string UserName)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion IAuthenService Members
    }
}