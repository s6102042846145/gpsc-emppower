using System;
using System.Configuration;
using System.Reflection;
using ESS.SECURITY;
using ESS.SHAREDATASERVICE;

namespace ESS.AUTHEN
{
    public class ServiceManager
    {
        #region Constructor
        private ServiceManager()
        {

        }
        #endregion

        #region MultiCompany  Framework
        public string CompanyCode { get; set; }
        public static string ModuleID
        {
            get
            {
                return "ESS.AUTHEN";
            }
        }
        public static ServiceManager CreateInstance(string oCompanyCode)
        {
            ServiceManager oServiceManager = new ServiceManager()
            {
                CompanyCode = oCompanyCode
            };
            return oServiceManager;
        }
        #endregion MultiCompany  Framework

        private string ConfigMode
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ConfigMode");
            }
        }

        private static Type GetService(string Mode, string ClassName)
        {
            Type oReturn = null;
            Assembly oAssembly;
            string assemblyName = string.Format("ESS.AUTHEN.{0}", Mode.ToUpper());
            string typeName = string.Format("ESS.AUTHEN.{0}.{1}", Mode.ToUpper(), ClassName);//CultureInfo.CurrentCulture.TextInfo.ToTitleCase(ClassName.ToLower()));
            oAssembly = Assembly.Load(assemblyName);    // Load assembly (dll)
            oReturn = oAssembly.GetType(typeName);      // Load class
            return oReturn;
        }


        public IAuthenService AuthenService
        {
            get
            {
                IAuthenService service;
                Type oType = GetService(ConfigMode, "AuthenServiceImpl");
                service = (IAuthenService)Activator.CreateInstance(oType, CompanyCode);
                return service;
            }
        }

        public IAuthorizeService AuthorizeService
        {
            get
            {
                IAuthorizeService service;
                Type oType = GetService(ConfigMode, "AuthenServiceImpl");
                service = (IAuthorizeService)Activator.CreateInstance(oType, CompanyCode);
                return service;
            }
        }
    }
}