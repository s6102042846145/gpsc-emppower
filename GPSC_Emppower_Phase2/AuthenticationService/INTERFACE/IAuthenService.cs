using System;

namespace ESS.AUTHEN
{
    public interface IAuthenService
    {
        bool HavePassword(string UserName);

        void ChangePassword(string UserName, String NewPassword);

        void ChangePassword(string UserName, string OldPassword, string NewPassword);

        void ClearPassword(string UserName);
    }
}