﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.SHAREDATASERVICE;

namespace ESS.AUTHEN
{
    public class AuthenticationManagement
    {
        #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        //private static Dictionary<string, AuthenticationManagement> Cache = new Dictionary<string, AuthenticationManagement>();

        private static string AssamblyName = "ESS.AUTHEN.dll";

        public string CompanyCode { get; set; }

        public static AuthenticationManagement CreateInstance(string oCompanyCode)
        {
            //if (!Cache.ContainsKey(oCompanyCode))
            //{
            //    Cache[oCompanyCode] = new AuthenticationManagement()
            //    {
            //        CompanyCode = oCompanyCode
            //        ,
            //        Config = ShareDataManager.GetModuleSettings(oCompanyCode, AssamblyName)
            //    };
            //}
            //return Cache[oCompanyCode];
            AuthenticationManagement oAuthenticationManagement = new AuthenticationManagement()
            {
                CompanyCode = oCompanyCode
            };
            return oAuthenticationManagement;
        }
        #endregion MultiCompany  Framework

    }
}
