using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Reflection;
using System.Text;
using ESS.HR.TM;
using SAPInterface;
using SAP.Connector;

namespace ESS.DATA.SAP
{
    public class HRTMREPORTSERVICE : AbstractHRTMReportService
    {
        private CultureInfo oCLEN = new CultureInfo("en-US");
        private static Configuration __config;

        #region " PrivateData "
        private static Configuration config
        {
            get
            {
                if (__config == null)
                {
                    __config = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().CodeBase.Replace("file:///", "").Replace("/", "\\"));
                }
                return __config;
            }
        }

        private static string BaseConnStr
        {
            get
            {
                if (config == null || config.AppSettings.Settings["BaseConnStr"] == null)
                {
                    return "";
                }
                else
                {
                    return config.AppSettings.Settings["BaseConnStr"].Value;
                }
            }
        }

        #endregion

        //public override List<TimeResult> GetTimeResultFromLastUpdate(DateTime LastUpdate, DateTime UpdateTo)
        //{
        //    CultureInfo oCL = new CultureInfo("en-US");
        //    List<TimeResult> oReturn = new List<TimeResult>();

        //    SAP.Connector.Connection oConnection = SAP.Connector.Connection.GetConnectionFromPool(BaseConnStr);
        //    DOWNLOADB2 oFunction = new DOWNLOADB2();
        //    oFunction.Connection = oConnection;

        //    ZHR_CLUSTER_B2NTable retVal = new ZHR_CLUSTER_B2NTable();

        //    oFunction.Zhri_Download_B2("00000000", "99999999", LastUpdate.ToString("yyyyMMdd", oCL), UpdateTo.ToString("yyyyMMdd", oCL), ref retVal);

        //    SAP.Connector.Connection.ReturnConnection(oConnection);
        //    oFunction.Dispose();

        //    #region " Parse Object "
        //    TimeResult trs;
        //    foreach (ZHR_CLUSTER_B2N item in retVal)
        //    {
        //        trs = new TimeResult();
        //        trs.EmployeeID = item.Empid;
        //        trs.Period = item.Period;
        //        trs.TimeType = item.Timetype;
        //        trs.Amount = decimal.Parse(item.Amount);
        //        oReturn.Add(trs);
        //    }

        //    #endregion


        //    return oReturn;

        //}
    }
}
