﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using ESS.LOG;
using System.Reflection;
using System.Data;
using System.Configuration;

namespace ESS.LOG.DB
{
    public class LogService : AbstractLogService
    {
        #region " Private "
        private static Configuration __config;

        private static Configuration config
        {
            get
            {
                if (__config == null)
                {
                    __config = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().CodeBase.Replace("file:///", "").Replace("/", "\\"));
                }
                return __config;
            }
        }

        private string ConnectionString
        {
            get
            {
                if (config == null || config.AppSettings.Settings["ConnStr"] == null)
                {
                    return "";
                }
                else
                {
                    return config.AppSettings.Settings["ConnStr"].Value;
                }
            }
        }
        #endregion

        public override void SaveLogMessage(LogMessage message)
        {
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();

            SqlCommand oCommand = new SqlCommand("select * from LogMessage where ID = @ID", oConnection, tx);
            oCommand.Parameters.Add(new SqlParameter("@ID", message.ID));

            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);

            DataTable oTable = new DataTable("LogMessage");

            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);

            DataRow dr = oTable.NewRow();
            dr["ID"] = message.ID;
            dr["CreatedDate"] = message.CreatedDate;
            dr["Detail"] = message.Detail;
            dr["Level"] = message.Level;
            dr["Source"] = message.Source;
            dr = oTable.LoadDataRow(dr.ItemArray, false);

            if (message.IsDelete)
            {
                dr.Delete();
            }

            foreach (DataRow dr1 in oTable.Rows)
            {
                if (dr1.RowState == DataRowState.Unchanged)
                {
                    dr1.Delete();
                }
            }

            try
            {
                oAdapter.Update(oTable);
                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }
        public override void InsertActionLog(ActionLog oActionLog)
        {
            using (SqlConnection oConnection = new SqlConnection(this.ConnectionString))
            {
                oConnection.Open();
                //SqlTransaction trn = oConnection.BeginTransaction();
                SqlCommand oCommand = new SqlCommand("sp_ActionLogSet", oConnection);
                try
                {
                    oCommand.CommandType = CommandType.StoredProcedure;
                    SqlParameter oParam;

                    oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
                    oParam.Value = oActionLog.EmployeeID;
                    oCommand.Parameters.Add(oParam);

                    oParam = new SqlParameter("@ActionName", SqlDbType.VarChar);
                    oParam.Value = oActionLog.ActionName;
                    oCommand.Parameters.Add(oParam);

                    oParam = new SqlParameter("@ActionData", SqlDbType.Text);
                    oParam.Value = oActionLog.ActionData;
                    oCommand.Parameters.Add(oParam);

                    oCommand.ExecuteNonQuery();
                    //trn.Commit();
                }
                catch (Exception ex)
                {
                    //trn.Rollback();
                    throw new Exception("Save ActionLog error", ex);
                }
                finally
                {
                    oCommand.Dispose();
                    oConnection.Close();
                    oConnection.Dispose();
                }
            }
        }

        public override void SaveEventLog(EventLog oEventLog)
        {
            using (SqlConnection oConnection = new SqlConnection(this.ConnectionString))
            {
                oConnection.Open();
                SqlCommand oCommand = new SqlCommand("sp_EventLogSave", oConnection);
                try
                {
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.Parameters.AddWithValue("@p_EmployeeID", oEventLog.EmployeeID);
                    oCommand.Parameters.AddWithValue("@p_IP", oEventLog.IP);
                    oCommand.Parameters.AddWithValue("@p_Name", oEventLog.Name);
                    oCommand.Parameters.AddWithValue("@p_Event", oEventLog.Event);
                    oCommand.Parameters.AddWithValue("@p_Description", oEventLog.Description);

                    oCommand.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    //throw new Exception("Save EventLog error", ex);
                }
                finally
                {
                    oCommand.Dispose();
                    oConnection.Close();
                    oConnection.Dispose();
                }
            }
        }
    }
}
