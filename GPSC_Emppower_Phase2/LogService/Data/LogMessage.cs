﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.LOG
{
    public class LogMessage
    {
        private string __strID = Guid.NewGuid().ToString();
        private string __strLevel;
        private DateTime __dtCreatedDate = DateTime.Now;
        private string __strEmployeeID;
        private string __strSource;
        private string __strDetail;
        private bool __bIsDelete = false;

        public string ID
        {
            get
            {
                return __strID;
            }
            set
            {
                __strID = value;
            }
        }

        public string Level
        {
            get
            {
                return __strLevel;
            }
            set
            {
                __strLevel = value;
            }
        }

        public DateTime CreatedDate
        {
            get
            {
                return __dtCreatedDate;
            }
            set
            {
                __dtCreatedDate = value;
            }
        }

        public string EmployeeID
        {
            get
            {
                return __strEmployeeID;
            }
            set
            {
                __strEmployeeID = value;
            }
        }
        public string Source
        {
            get
            {
                return __strSource;
            }
            set
            {
                __strSource = value;
            }
        }
        public string Detail
        {
            get
            {
                return __strDetail;
            }
            set
            {
                __strDetail = value;
            }
        }
        public bool IsDelete
        {
            get
            {
                return __bIsDelete;
            }
            set
            {
                __bIsDelete = value;
            }
        }
    }
}
