using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.LOG
{
    /// <summary>
    /// Summary description for ActionLog
    /// </summary>
    public class ActionLog
    {
        private ActionLog()
        {
        }
        public static ActionLog CreateInstance()
        {
            return new ActionLog();
        }
        #region Member
        private string _ActionId = string.Empty;
        private string _EmployeeID = string.Empty;
        private string _ActionName = string.Empty;
        private DateTime _ActionDate = DateTime.Now;
        private string _ActionData = string.Empty; 
        #endregion

        #region Property
        public string ActionId { get { return _ActionId; } set { _ActionId = value; } }
        public string EmployeeID { get { return _EmployeeID; } set { _EmployeeID = value; } }
        public string ActionName { get { return _ActionName; } set { _ActionName = value; } }
        public DateTime ActionDate { get { return _ActionDate; } set { _ActionDate = value; } }
        public string ActionData { get { return _ActionData; } set { _ActionData = value; } } 
        #endregion
    }
}