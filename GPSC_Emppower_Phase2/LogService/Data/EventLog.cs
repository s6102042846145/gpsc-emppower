﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.LOG
{
    [Serializable()]
    public class EventLog
    {
        public EventLog()
        {
        }

        private DateTime __eventTime = new DateTime();
        private string __employeeID = "";
        private string __ip = "";
        private string __name = "";
        private string __event = "";
        private string __description = "";

        public DateTime EventTime
        {
            get { return __eventTime; }
            set { __eventTime = value; }
        }
        public string EmployeeID
        {
            get { return __employeeID; }
            set { __employeeID = value; }
        }
        public string IP
        {
            get { return __ip; }
            set { __ip = value; }
        }
        public string Name
        {
            get { return __name; }
            set { __name = value; }
        }
        public string Event
        {
            get { return __event; }
            set { __event = value; }
        }
        public string Description
        {
            get { return __description; }
            set { __description = value; }
        }
    }
}
