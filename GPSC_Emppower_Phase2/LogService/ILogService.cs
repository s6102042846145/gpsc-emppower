﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.LOG
{
    interface ILogService
    {
        void SaveLogMessage(LogMessage message);
        void InsertActionLog(ActionLog oActionLog);
        void SaveEventLog(EventLog oEventLog);
    }
}
