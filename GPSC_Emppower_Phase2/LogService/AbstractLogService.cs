﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.LOG
{
    public class AbstractLogService : ILogService
    {
        #region ILogService Members

        public virtual void SaveLogMessage(LogMessage message)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual void InsertActionLog(ActionLog oActionLog)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual void SaveEventLog(EventLog oEventLog)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        #endregion
    }
}
