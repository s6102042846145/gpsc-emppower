﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.LOG
{
    public class Logger
    {
        public static void Debug(string strMessage)
        {
            Log(strMessage, "DEBUG");
        }
        public static void Info(string strMessage)
        {
            Log(strMessage, "INFO");
        }
        public static void Warn(string strMessage)
        {
            Log(strMessage, "WARN");
        }
        public static void Error(string strMessage)
        {
            Log(strMessage, "ERROR");
        }

        private static void Log(string strMessage, string strLevel)
        {
            Dictionary<string, int> lookup = new Dictionary<string, int>();
            lookup["DEBUG"] = 0;
            lookup["INFO"] = 1;
            lookup["WARN"] = 2;
            lookup["ERROR"] = 3;
            lookup["OFF"] = 4;

            if (lookup[strLevel] >= lookup[ServiceManager.LOG_LEVEL])
            {
                LogMessage oMessage = new LogMessage();
                oMessage.Detail = strMessage;
                oMessage.Level = strLevel;
                oMessage.Source = Environment.StackTrace;
                ServiceManager.ESS.SaveLogMessage(oMessage);
            }
        }
        public static void InsertActionLog(ActionLog oReqActionLog)
        {
            ServiceManager.ESS.InsertActionLog(oReqActionLog);
        }
    }
}
