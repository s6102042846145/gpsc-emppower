﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using ESS.LOG;
using System.Reflection;

namespace ESS.LOG
{
    public class ServiceManager
    {
        #region " privatedata "
        private static Configuration __config;
        private ServiceManager()
        {
        }
        private static Configuration config
        {
            get
            {
                if (__config == null)
                {
                    __config = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().CodeBase.Replace("file:///", "").Replace("/", "\\"));
                }
                return __config;
            }
        }

        private static string ESS_CONNECTOR_ASSEMBLY
        {
            get
            {
                if (config == null || config.AppSettings.Settings["ESS_CONNECTOR_ASSEMBLY"] == null)
                {
                    return "";
                }
                else
                {
                    return config.AppSettings.Settings["ESS_CONNECTOR_ASSEMBLY"].Value;
                }
            }
        }

        private static string ESS_CONNECTOR_TYPE
        {
            get
            {
                if (config == null || config.AppSettings.Settings["ESS_CONNECTOR_TYPE"] == null)
                {
                    return "";
                }
                else
                {
                    return config.AppSettings.Settings["ESS_CONNECTOR_TYPE"].Value;
                }
            }
        }

        public static string LOG_LEVEL
        {
            get
            {
                if (config == null || config.AppSettings.Settings["LOG_LEVEL"] == null)
                {
                    return "";
                }
                else
                {
                    return config.AppSettings.Settings["LOG_LEVEL"].Value;
                }
            }
        }

        private static Type GetService(string AssemblyName, string TypeName)
        {
            Type oReturn = null;
            Assembly oAssembly;
            string assemblyName = AssemblyName;
            string typeName = TypeName;
            oAssembly = Assembly.Load(assemblyName);
            oReturn = oAssembly.GetType(typeName);
            return oReturn;
        }
        #endregion

        internal static ILogService ESS
        {
            get
            {
                Type oType = GetService(ESS_CONNECTOR_ASSEMBLY, ESS_CONNECTOR_TYPE);
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (ILogService)Activator.CreateInstance(oType);
                }
            }
        }
    }
}
