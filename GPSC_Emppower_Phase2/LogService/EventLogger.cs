﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace ESS.LOG
{
    public class EventLogger
    {
        public static void SaveEventLog(EventLog oEventLog)
        {
            ServiceManager.ESS.SaveEventLog(oEventLog);
        }

        public static string DetermineCompName(string IP)
        {
            try
            {
                IPAddress myIP = IPAddress.Parse(IP);
                IPHostEntry GetIPHost = Dns.GetHostEntry(myIP);
                List<string> compName = GetIPHost.HostName.ToString().Split('.').ToList();
                return compName.First();
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }
    }
}
