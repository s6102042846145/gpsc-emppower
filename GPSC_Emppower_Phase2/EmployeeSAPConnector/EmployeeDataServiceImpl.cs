using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Reflection;
using ESS.EMPLOYEE.ABSTRACT;
using ESS.EMPLOYEE.CONFIG.PA;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.SHAREDATASERVICE;
using ESS.WORKFLOW;
using SAP.Connector;
using SAPInterface;

namespace ESS.EMPLOYEE.SAP
{
    public class EmployeeDataServiceImpl : AbstractEmployeeDataService
    {
        #region Constructor
        public EmployeeDataServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
        }

        #endregion Constructor

        #region Member
        //private Dictionary<string, string> Config { get; set; }
        public string CompanyCode { get; set; }

        private static string ModuleID = "ESS.EMPLOYEE.SAP";

        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }

        private DateTime MINIMUM_DATE_IN_SAP
        {
            get
            {
                return DateTime.ParseExact(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "MINIMUM_DATE_IN_SAP"), sDateFormat, oCL_ENUS);
            }
        }
        private string INF27_SUBTYPE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "INF27_SUBTYPE", "''");
            }
        }
        
        #endregion Member

        //private CultureInfo oCL_ENUS = WORKFLOW.WorkflowPrinciple.Current.UserSetting.CultureInfo;
        //private string sDateFormat = WORKFLOW.WorkflowPrinciple.Current.UserSetting.DateFormat;

        private CultureInfo oCL_ENUS
        {
            get
            {
                if (WorkflowPrinciple.Current == null)
                {
                    return new CultureInfo("en-US");
                }
                return WorkflowPrinciple.Current.UserSetting.CultureInfo;
            }
        }
        private string sDateFormat
        {
            get
            {
                if (WorkflowPrinciple.Current == null)
                {
                    return ShareDataManagement.LookupCache(CompanyCode, "ESS.EMPLOYEE", "DEFAULTDATEFORMAT");
                }
                return WorkflowPrinciple.Current.UserSetting.DateFormat;
            }
        }
        private static Configuration __config;
        private const int maxAttemp = 3;

        #region " private data "
        private string SAP_VERSION
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAP_VERSION");
            }
        }

        #endregion " private data "

        #region IEmployeeService Members

        #region " GetEmployeeIDFromUserID "

        public override string GetEmployeeIDFromUserID(string UserID)
        {
            DateTime oNow = DateTime.Now;
            Connection oConnection = SAPConnection.GetConnectionFromPool(this.BaseConnStr);
            RFCREADTABLE6 oFunction = null;

            oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_FLD fld;
            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_OPT opt;
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("USRID = '{0}'", UserID.ToUpper());
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = "AND SUBTY = '0001'";
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND BEGDA <= '{0}' AND ENDDA >= '{0}'", oNow.ToString("yyyyMMdd", oCL_ENUS));
            Options.Add(opt);

            if (SAP_VERSION == "6")
            {
                oFunction.Rfc_Read_Table("^", "", "PA0105", 0, 0, ref Data, ref Fields, ref Options);
                oFunction.Dispose();
            }
            else
            {
                oFunction.Rfc_Read_Table("^", "", "PA0105", 0, 0, ref Data, ref Fields, ref Options);
                oFunction.Dispose();
            }

            SAPConnection.ReturnConnection(oConnection);
            if (Data.Count == 0)
            {
                return "";
            }
            return Data[0].Wa;
        }

        #endregion " GetEmployeeIDFromUserID "

        #region " INFOTYPE0000 "

        #region " GetInfoType0000Data "

        private List<INFOTYPE0000> GetInfotype0000Data(string EmployeeID1, string EmployeeID2, DateTime CheckDate, string Profile)
        {
            System.Globalization.CultureInfo oCL = new CultureInfo("en-US");
            List<INFOTYPE0000> list = new List<INFOTYPE0000>();
            Connection oConnection = SAPConnection.GetConnectionFromPool(this.BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;
            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_FLD fld;

            #region " SetField "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "STAT2";
            Fields.Add(fld);

            #endregion " SetField "

            #region " SetOption "

            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_OPT opt;
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR <= '{1}' AND PERNR >= '{0}'", EmployeeID1, EmployeeID2);
            Options.Add(opt);

            if (CheckDate != DateTime.MinValue)
            {
                opt = new RFC_DB_OPT();
                opt.Text = string.Format("AND BEGDA <= '{0}' AND ENDDA >= '{0}'", CheckDate.ToString("yyyyMMdd", oCL_ENUS));
                Options.Add(opt);
            }

            #endregion " SetOption "

            #region " Execute "

            oFunction.Rfc_Read_Table("^", "", "PA0000", 0, 0, ref Data, ref Fields, ref Options);
            SAPConnection.ReturnConnection(oConnection);

            #endregion " Execute "

            #region " ParseToObject "

            foreach (TAB512 item in Data)
            {
                string[] arrTemp = item.Wa.Split('^');
                INFOTYPE0000 inf = new INFOTYPE0000();
                //PERNR
                inf.EmployeeID = arrTemp[0];
                //BEGDA
                inf.BeginDate = DateTime.ParseExact(arrTemp[1], "yyyyMMdd", oCL);
                //ENDDA
                inf.EndDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", oCL);
                //BUKRS
                inf.EmploymentStatus = arrTemp[3].Trim();
                list.Add(inf);
            }

            #endregion " ParseToObject "

            oFunction.Dispose();
            return list;
        }

        #endregion " GetInfoType0000Data "

        public override List<INFOTYPE0000> GetInfotype0000List(string EmployeeID1, string EmployeeID2, DateTime CheckDate, string Profile)
        {
            return GetInfotype0000Data(EmployeeID1, EmployeeID2, DateTime.MinValue, Profile);
        }

        #endregion " INFOTYPE0000 "

        #region " INFOTYPE0001 "

        #region " GetInfoType0001Data "

        private List<INFOTYPE0001> GetInfotype0001Data(string EmployeeID1, string EmployeeID2, DateTime CheckDate, string Profile)
        {
            System.Globalization.CultureInfo oCL = new CultureInfo("en-US");
            List<INFOTYPE0001> list = new List<INFOTYPE0001>();
            Connection oConnection = SAPConnection.GetConnectionFromPool(this.BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;
            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_FLD fld;

            #region " SetField "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BUKRS";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "WERKS";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BTRTL";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERSG";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERSK";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ORGEH";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PLANS";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SACHZ";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENAME";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "KOSTL";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "GSBER";//Business Area
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ABKRS"; //Payroll Area
            Fields.Add(fld);
            #endregion " SetField "

            #region " SetOption "

            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_OPT opt;
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR >= '{0}' AND PERNR <= '{1}'", EmployeeID1, EmployeeID2);
            Options.Add(opt);

            //opt = new RFC_DB_OPT();
            //opt.Text = string.Format("AND PERSG IN ('A','B','I','J')");
            //Options.Add(opt);

            if (CheckDate != DateTime.MinValue)
            {
                opt = new RFC_DB_OPT();
                opt.Text = string.Format("AND BEGDA <= '{0}' AND ENDDA >= '{0}'", CheckDate.ToString("yyyyMMdd", oCL_ENUS));
                Options.Add(opt);
            }
            else
            {
                opt = new RFC_DB_OPT();
                opt.Text = string.Format("AND BEGDA >= '{0}'", MINIMUM_DATE_IN_SAP.ToString("yyyyMMdd", oCL_ENUS));
                Options.Add(opt);
            }
            #endregion " SetOption "

            #region " Execute "

            oFunction.Rfc_Read_Table("^", "", "PA0001", 0, 0, ref Data, ref Fields, ref Options);
            SAPConnection.ReturnConnection(oConnection);

            #endregion " Execute "

            #region " ParseToObject "

            foreach (TAB512 item in Data)
            {
                string[] arrTemp = item.Wa.Split('^');
                INFOTYPE0001 inf = new INFOTYPE0001();
                //PERNR
                inf.EmployeeID = arrTemp[0];
                inf.SubType = "";
                //BEGDA
                inf.BeginDate = DateTime.ParseExact(arrTemp[1], "yyyyMMdd", oCL);
                //ENDDA
                inf.EndDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", oCL);
                //BUKRS
                inf.CompanyCode = arrTemp[3];
                //WERKS
                inf.Area = arrTemp[4];
                //BTRTL
                inf.SubArea = arrTemp[5];
                //PERSG
                inf.EmpGroup = arrTemp[6];
                //PERSK
                inf.EmpSubGroup = arrTemp[7];
                //ORGEH
                inf.OrgUnit = arrTemp[8];
                //PLANS
                inf.Position = arrTemp[9];
                //SACHZ
                inf.AdminGroup = arrTemp[10];
                //ENAME
                inf.Name = arrTemp[11].Trim();
                //ENAME
                inf.CostCenter = arrTemp[12].Trim();
                //Business Area
                inf.BusinessArea = arrTemp[13].Trim();
                //Payroll Area
                inf.PayrollArea = arrTemp[14].Trim();

                list.Add(inf);
            }

            #endregion " ParseToObject "

            oFunction.Dispose();
            return list;
        }

        public override List<INFOTYPE0001> GetAreaWorkscheduleInfotype0001Data(string EmployeeID1, string EmployeeID2, DateTime CheckDate, string Profile)
        {
            System.Globalization.CultureInfo oCL = new CultureInfo("en-US");
            List<INFOTYPE0001> list = new List<INFOTYPE0001>();
            Connection oConnection = SAPConnection.GetConnectionFromPool(this.BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;
            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_FLD fld;

            #region " SetField "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BUKRS";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "WERKS";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BTRTL";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERSG";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERSK";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ORGEH";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PLANS";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SACHZ";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENAME";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "KOSTL";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "GSBER";//Business Area
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ABKRS"; //Payroll Area
            Fields.Add(fld);
            #endregion " SetField "

            #region " SetOption "

            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_OPT opt;
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR >= '{0}' AND PERNR <= '{1}'", EmployeeID1, EmployeeID2);
            Options.Add(opt);

            //opt = new RFC_DB_OPT();
            //opt.Text = string.Format("AND PERSG IN ('A','B','I','J')");
            //Options.Add(opt);

            if (CheckDate != DateTime.MinValue)
            {
                opt = new RFC_DB_OPT();
                opt.Text = string.Format("AND BEGDA <= '{0}' AND ENDDA >= '{0}'", CheckDate.ToString("yyyyMMdd", oCL_ENUS));
                Options.Add(opt);
            }
            else
            {
                opt = new RFC_DB_OPT();
                opt.Text = string.Format("AND BEGDA >= '{0}'", MINIMUM_DATE_IN_SAP.ToString("yyyyMMdd", oCL_ENUS));
                Options.Add(opt);
            }
            #endregion " SetOption "

            #region " Execute "

            oFunction.Rfc_Read_Table("^", "", "PA0001", 0, 0, ref Data, ref Fields, ref Options);
            SAPConnection.ReturnConnection(oConnection);

            #endregion " Execute "

            #region " ParseToObject "

            foreach (TAB512 item in Data)
            {
                string[] arrTemp = item.Wa.Split('^');
                INFOTYPE0001 inf = new INFOTYPE0001();
                //PERNR
                inf.EmployeeID = arrTemp[0];
                inf.SubType = "";
                //BEGDA
                inf.BeginDate = DateTime.ParseExact(arrTemp[1], "yyyyMMdd", oCL);
                //ENDDA
                inf.EndDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", oCL);
                //BUKRS
                inf.CompanyCode = arrTemp[3];
                //WERKS
                inf.Area = arrTemp[4];
                //BTRTL
                inf.SubArea = arrTemp[5];
                //PERSG
                inf.EmpGroup = arrTemp[6];
                //PERSK
                inf.EmpSubGroup = arrTemp[7];
                //ORGEH
                inf.OrgUnit = arrTemp[8];
                //PLANS
                inf.Position = arrTemp[9];
                //SACHZ
                inf.AdminGroup = arrTemp[10];
                //ENAME
                inf.Name = arrTemp[11].Trim();
                //ENAME
                inf.CostCenter = arrTemp[12].Trim();
                //Business Area
                inf.BusinessArea = arrTemp[13].Trim();
                //Payroll Area
                inf.PayrollArea = arrTemp[14].Trim();

                list.Add(inf);
            }

            #endregion " ParseToObject "

            oFunction.Dispose();
            return list;
        }

        #endregion " GetInfoType0001Data "

        public override List<INFOTYPE0001> GetInfotype0001List(string EmployeeID1, string EmployeeID2, DateTime CheckDate, string Profile)
        {
            return GetInfotype0001Data(EmployeeID1, EmployeeID2, DateTime.MinValue, Profile);
        }

        #endregion " INFOTYPE0001 "

        #region " INFOTYPE0002 "
        #region " GetInfotype0002List "

        public override List<INFOTYPE0002> GetInfotype0002List(string EmployeeID1, string EmployeeID2, string Profile)
        {
            return GetInfotype0002Data(EmployeeID1, EmployeeID2, DateTime.Now);
        }
        #endregion
        #region " GetInfotype0002Data "
        private List<INFOTYPE0002> GetInfotype0002Data(string EmployeeID1, string EmployeeID2, DateTime CheckDate)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            List<INFOTYPE0002> oReturn = new List<INFOTYPE0002>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "
            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SUBTY";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ANRED";
            Fields.Add(fld);

            if (this.SAP_VERSION == "6")
            {
                fld = new RFC_DB_FLD();
                fld.Fieldname = "NAMZU";
                Fields.Add(fld);
            }
            else
            {
                fld = new RFC_DB_FLD();
                fld.Fieldname = "TITEL";
                Fields.Add(fld);
            }

            fld = new RFC_DB_FLD();
            fld.Fieldname = "VORNA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "NACHN";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "INITS";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "RUFNM";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "KNZNM";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "GBDAT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "GESCH";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "GBORT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "GBLND";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "NATIO";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FAMST";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FAMDT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ANZKD";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "KONFE";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SPRSL";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "GBDEP";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "GBORT";

            //Add By Kiattiwat 13-02-2012
            //22
            fld = new RFC_DB_FLD();
            fld.Fieldname = "TITEL";
            Fields.Add(fld);

            //23
            fld = new RFC_DB_FLD();
            fld.Fieldname = "TITL2";
            Fields.Add(fld);

            //24
            fld = new RFC_DB_FLD();
            fld.Fieldname = "VORSW";
            Fields.Add(fld);

            //25
            fld = new RFC_DB_FLD();
            fld.Fieldname = "VORS2";
            Fields.Add(fld);

            //26
            fld = new RFC_DB_FLD();
            fld.Fieldname = "NATI2";
            Fields.Add(fld);

            //27
            fld = new RFC_DB_FLD();
            fld.Fieldname = "NATI3";
            Fields.Add(fld);
            #endregion

            #region " Options "
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR >= '{0}' AND PERNR <= '{1}'", EmployeeID1, EmployeeID2);
            Options.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND BEGDA <= '{0}' and ENDDA >= '{0}'", CheckDate.ToString("yyyyMMdd", this.oCL_ENUS));
            Options.Add(opt);
            #endregion

            oFunction.Zrfc_Read_Table("^", "", "PA0002", 0, 0, ref Data, ref Fields, ref Options);
            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            #region " Parse Object "
            INFOTYPE0002 data;
            foreach (TAB512 item in Data)
            {
                data = new INFOTYPE0002();
                string[] arrTemp = item.Wa.Split('^');
                //fld.Fieldname = "PERNR";
                data.EmployeeID = arrTemp[0].Trim();
                //fld.Fieldname = "SUBTY";
                data.SubType = arrTemp[1].Trim();
                //fld.Fieldname = "BEGDA";
                data.BeginDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", oCL);
                //fld.Fieldname = "ENDDA";
                data.EndDate = DateTime.ParseExact(arrTemp[3], "yyyyMMdd", oCL);
                //fld.Fieldname = "ANRED";
                data.TitleID = arrTemp[4].Trim();
                //fld.Fieldname = "TITEL";
                data.Prefix = arrTemp[5].Trim();
                //fld.Fieldname = "VORNA";
                data.FirstName = arrTemp[6].Trim();
                //fld.Fieldname = "NACHN";
                data.LastName = arrTemp[7].Trim();
                //fld.Fieldname = "INITS";
                data.Initial = arrTemp[8].Trim();
                //fld.Fieldname = "RUFNM";
                data.NickName = arrTemp[9].Trim();
                //fld.Fieldname = "KNZNM";
                data.NameFormat = arrTemp[10].Trim();
                //fld.Fieldname = "GBDAT";
                data.DOB = DateTime.ParseExact(arrTemp[11], "yyyyMMdd", oCL);
                //fld.Fieldname = "GESCH";
                data.Gender = arrTemp[12].Trim();
                //fld.Fieldname = "GBORT";
                data.BirthPlace = arrTemp[13].Trim();
                //fld.Fieldname = "GBLND";
                data.BirthCity = arrTemp[14].Trim();
                //fld.Fieldname = "NATIO";
                data.Nationality = arrTemp[15].Trim();
                //fld.Fieldname = "FAMST";
                data.MaritalStatus = arrTemp[16].Trim();
                //fld.Fieldname = "FAMDT";
                if (arrTemp[17].Trim() == "00000000")
                {
                    data.MaritalEffectiveDate = DateTime.MinValue;
                }
                else
                {
                    data.MaritalEffectiveDate = DateTime.ParseExact(arrTemp[17], "yyyyMMdd", oCL);
                }
                //fld.Fieldname = "ANZKD";
                data.NoOfChild = int.Parse(arrTemp[18].Trim());
                //fld.Fieldname = "KONFE";
                data.Religion = arrTemp[19].Trim();
                //fld.Fieldname = "SPRSL";
                data.Language = arrTemp[20].Trim();
                //Add By Kiattiwat 13-02-2012
                //fld.Fieldname = "TITEL";
                //data.militaryTitle = arrTemp[22].Trim();
                //fld.Fieldname = "TITL2";
                //data.secondTitle = arrTemp[23].Trim();
                //fld.Fieldname = "VORSW";
                //data.namePrefix1 = arrTemp[24].Trim();
                //fld.Fieldname = "VORS2";
                //data.namePrefix2 = arrTemp[25].Trim();
                //fld.Fieldname = "NATI2";
                //data.secondNationality = arrTemp[26].Trim();
                //fld.Fieldname = "NATI3";
                //data.thirdNationality = arrTemp[27].Trim();
                oReturn.Add(data);
            }
            #endregion

            return oReturn;
        }
        #endregion
        #endregion
        public override bool ValidateEmployeeID(string EmployeeID, DateTime CheckDate)
        {
            return true;
        }

        #region " INFOTYPE0182 "

        #region " GetInfoType0182Data "

        private List<INFOTYPE0182> GetInfoType0182Data(string EmployeeID1, string EmployeeID2, string Language, DateTime CheckDate, string Profile)
        {
            System.Globalization.CultureInfo oCL = new CultureInfo("en-US");
            List<INFOTYPE0182> list = new List<INFOTYPE0182>();
            Connection oConnection = SAPConnection.GetConnectionFromPool(this.BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;
            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_FLD fld;

            #region " SetField "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SUBTY";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ALNAM";
            Fields.Add(fld);

            #endregion " SetField "

            #region " SetOption "

            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_OPT opt;
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR <= '{1}' AND PERNR >= '{0}'", EmployeeID1, EmployeeID2);
            Options.Add(opt);

            if (Language != "")
            {
                opt = new RFC_DB_OPT();
                opt.Text = string.Format("AND SUBTY = '{0}'", Language);
                Options.Add(opt);
            }

            if (CheckDate != DateTime.MinValue)
            {
                opt = new RFC_DB_OPT();
                opt.Text = string.Format("AND BEGDA <= '{0}' AND ENDDA >= '{0}'", CheckDate.ToString("yyyyMMdd", oCL_ENUS));
                Options.Add(opt);
            }
            else
            {
                opt = new RFC_DB_OPT();
                opt.Text = string.Format("AND BEGDA > '{0}'", MINIMUM_DATE_IN_SAP.ToString("yyyyMMdd", oCL_ENUS));
                Options.Add(opt);
            }

            #endregion " SetOption "

            #region " Execute "

            oFunction.Rfc_Read_Table("^", "", "PA0182", 0, 0, ref Data, ref Fields, ref Options);
            SAPConnection.ReturnConnection(oConnection);

            #endregion " Execute "

            #region " ParseToObject "

            foreach (TAB512 item in Data)
            {
                string[] arrTemp = item.Wa.Split('^');
                INFOTYPE0182 inf = new INFOTYPE0182();
                //PERNR
                inf.EmployeeID = arrTemp[0];
                //BEGDA
                inf.BeginDate = DateTime.ParseExact(arrTemp[1], "yyyyMMdd", oCL);
                //ENDDA
                inf.EndDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", oCL);
                //SUBTY
                inf.SubType = arrTemp[3];
                //SUBTY
                inf.NameType = arrTemp[3];
                //SUBTY
                inf.AlternateName = arrTemp[4];
                list.Add(inf);
            }

            #endregion " ParseToObject "

            oFunction.Dispose();
            return list;
        }

        #endregion " GetInfoType0182Data "

        public override List<INFOTYPE0182> GetInfotype0182List(string EmployeeID1, string EmployeeID2, string Profile)
        {
            return GetInfoType0182Data(EmployeeID1, EmployeeID2, "", DateTime.MinValue, Profile);
        }

        #endregion " INFOTYPE0182 "

        #region " INFOTYPE0105 "

        #region " GetInfotype0105Data "

        private List<INFOTYPE0105> GetInfotype0105Data(string EmployeeID1, string EmployeeID2, DateTime CheckDate, string Subtype, string Profile)
        {
            System.Globalization.CultureInfo oCL = new CultureInfo("en-US");
            List<INFOTYPE0105> list = new List<INFOTYPE0105>();
            Connection oConnection = SAPConnection.GetConnectionFromPool(this.BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;
            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_FLD fld;

            #region " SetField "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SUBTY";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "USRID";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "USRID_LONG";
            Fields.Add(fld);

            #endregion " SetField "

            #region " SetOption "

            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_OPT opt;
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR <= '{1}' AND PERNR >= '{0}'", EmployeeID1, EmployeeID2);
            Options.Add(opt);

            if (CheckDate != DateTime.MinValue)
            {
                opt = new RFC_DB_OPT();
                opt.Text = string.Format("AND BEGDA <= '{0}' AND ENDDA >= '{0}'", CheckDate.ToString("yyyyMMdd", oCL_ENUS));
                Options.Add(opt);
            }
            else
            {
                opt = new RFC_DB_OPT();
                opt.Text = string.Format("AND BEGDA > '{0}'", MINIMUM_DATE_IN_SAP.ToString("yyyyMMdd", oCL_ENUS));
                Options.Add(opt);
            }

            if (Subtype != "")
            {
                opt = new RFC_DB_OPT();
                opt.Text = string.Format("AND SUBTY = '{0}'", Subtype);
                Options.Add(opt);
            }

            #endregion " SetOption "

            #region " Execute "

            oFunction.Rfc_Read_Table("^", "", "PA0105", 0, 0, ref Data, ref Fields, ref Options);
            SAPConnection.ReturnConnection(oConnection);

            #endregion " Execute "

            #region " ParseToObject "

            foreach (TAB512 item in Data)
            {
                string[] arrTemp = item.Wa.Split('^');
                INFOTYPE0105 inf = new INFOTYPE0105();
                //PERNR
                inf.EmployeeID = arrTemp[0];
                //BEGDA
                inf.BeginDate = DateTime.ParseExact(arrTemp[1], "yyyyMMdd", oCL);
                //ENDDA
                inf.EndDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", oCL);
                //SUBTY
                inf.SubType = arrTemp[3].Trim();
                //USRID
                inf.DataText = arrTemp[4].Trim() + arrTemp[5].Trim(); ;
                //USRID_LONG
                list.Add(inf);
            }

            #endregion " ParseToObject "

            oFunction.Dispose();

            // Filter by primary key
            // add by karn 23 March 2011
            Dictionary<string, INFOTYPE0105> buffer = new Dictionary<string, INFOTYPE0105>();
            foreach (INFOTYPE0105 item in list)
            {
                string key = item.EmployeeID + item.SubType + item.BeginDate.ToString() + item.EndDate.ToString();
                if (!buffer.ContainsKey(key))
                {
                    buffer[key] = item;
                }
                else
                {
                    if (item.BeginDate > buffer[key].BeginDate)
                    {
                        buffer[key] = item;
                    }
                }
            }
            list.Clear();
            list = null;

            List<INFOTYPE0105> returnValue = new List<INFOTYPE0105>(buffer.Values);
            return returnValue;
        }

        #endregion " GetInfotype0105Data "

        public override List<INFOTYPE0105> GetInfotype0105List(string EmployeeID1, string EmployeeID2, string Profile)
        {
            return GetInfotype0105Data(EmployeeID1, EmployeeID2, DateTime.MinValue, "", Profile);
        }

        #endregion " INFOTYPE0105 "

        #endregion IEmployeeService Members

        #region " INFOTYPE0007 "

        public override List<INFOTYPE0007> GetInfotype0007List(string EmployeeID1, string EmployeeID2, int Year, int Month, string Profile)
        {
            System.Globalization.CultureInfo oCL = new CultureInfo("en-US");
            List<INFOTYPE0007> list = new List<INFOTYPE0007>();
            Connection oConnection = SAPConnection.GetConnectionFromPool(this.BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;
            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_FLD fld;

            #region " SetField "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SCHKZ";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ZTERF";
            Fields.Add(fld);

            #endregion " SetField "

            #region " SetOption "

            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_OPT opt;
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR >= '{0}' and PERNR <= '{1}'", EmployeeID1, EmployeeID2);
            Options.Add(opt);

            if (Year > -1 && Month > -1)
            {
                DateTime CheckDate;
                DateTime CheckDate1;
                CheckDate = new DateTime(Year, Month, 1);
                CheckDate1 = CheckDate.AddMonths(1).AddSeconds(-1);
                opt = new RFC_DB_OPT();
                opt.Text = string.Format("AND BEGDA < '{1}' AND ENDDA > '{0}'", CheckDate.ToString("yyyyMMdd", oCL_ENUS), CheckDate1.ToString("yyyyMMdd", oCL_ENUS));
                Options.Add(opt);
            }
            else
            {
                opt = new RFC_DB_OPT();
                opt.Text = string.Format("AND BEGDA > '{0}'", MINIMUM_DATE_IN_SAP.ToString("yyyyMMdd", oCL_ENUS));
                Options.Add(opt);
            }

            #endregion " SetOption "

            #region " Execute "

            oFunction.Rfc_Read_Table("^", "", "PA0007", 0, 0, ref Data, ref Fields, ref Options);
            SAPConnection.ReturnConnection(oConnection);

            #endregion " Execute "

            #region " ParseToObject "

            foreach (TAB512 item in Data)
            {
                string[] arrTemp = item.Wa.Split('^');
                INFOTYPE0007 inf = new INFOTYPE0007();
                //PERNR
                inf.EmployeeID = arrTemp[0];
                //BEGDA
                inf.BeginDate = DateTime.ParseExact(arrTemp[1], "yyyyMMdd", oCL);
                //ENDDA
                inf.EndDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", oCL);
                //SCHKZ
                inf.WFRule = arrTemp[3];
                //ZTERF
                inf.TimeEvaluateClass = arrTemp[4];
                list.Add(inf);
            }

            #endregion " ParseToObject "

            oFunction.Dispose();
            return list;
        }

        public override List<INFOTYPE0007> GetInfotype0007List(int Year, int Month, string TimeEvaluationClass)
        {
            System.Globalization.CultureInfo oCL = new CultureInfo("en-US");
            List<INFOTYPE0007> list = new List<INFOTYPE0007>();
            Connection oConnection = SAPConnection.GetConnectionFromPool(this.BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;
            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_FLD fld;

            #region " SetField "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SCHKZ";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ZTERF";
            Fields.Add(fld);

            #endregion " SetField "

            #region " SetOption "

            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_OPT opt;
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("ZTERF = '{0}'", TimeEvaluationClass);
            Options.Add(opt);

            DateTime CheckDate;
            DateTime CheckDate1;
            CheckDate = new DateTime(Year, Month, 1);
            CheckDate1 = CheckDate.AddMonths(1).AddDays(-1);
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND ( ( BEGDA <= '{0}' AND ENDDA >= '{0}' ) AND PERNR >= '23000000' AND PERNR <= '23999999'", CheckDate.ToString("yyyyMMdd", oCL_ENUS));
            Options.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("OR ( BEGDA <= '{0}' AND ENDDA >= '{0}' ) ) AND PERNR >= '23000000' AND PERNR <= '23999999'", CheckDate1.ToString("yyyyMMdd", oCL_ENUS));
            Options.Add(opt);

            #endregion " SetOption "

            #region " Execute "

            oFunction.Rfc_Read_Table("^", "", "PA0007", 0, 0, ref Data, ref Fields, ref Options);
            SAPConnection.ReturnConnection(oConnection);

            #endregion " Execute "

            #region " ParseToObject "

            foreach (TAB512 item in Data)
            {
                string[] arrTemp = item.Wa.Split('^');
                INFOTYPE0007 inf = new INFOTYPE0007();
                //PERNR
                inf.EmployeeID = arrTemp[0];
                //BEGDA
                inf.BeginDate = DateTime.ParseExact(arrTemp[1], "yyyyMMdd", oCL);
                //ENDDA
                inf.EndDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", oCL);
                //SCHKZ
                inf.WFRule = arrTemp[3];
                //ZTERF
                inf.TimeEvaluateClass = arrTemp[4];
                list.Add(inf);
            }

            #endregion " ParseToObject "

            oFunction.Dispose();
            return list;
        }

        public override void SaveInfotype0001(List<INFOTYPE0001> data, string RequestNo)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            Connection oConnection = Connection.GetConnectionFromPool(this.BaseConnStr);
            UPLOADINFOTYPE6 oFunction = new UPLOADINFOTYPE6();
            oFunction.Connection = oConnection;

            ZHRHRS001Table Updatetab;
            ZHRHRS001Table Updatetab_Ret;
            ZHRHRS001 item;
            bool lError;
            int count = 0;
            List<string> lstGuid = new List<string>(data.Count);
            foreach (INFOTYPE0001 infotype1 in data)
            {
                Updatetab = new ZHRHRS001Table();
                Updatetab_Ret = new ZHRHRS001Table();

                item = new ZHRHRS001();
                item.Begda = infotype1.BeginDate.ToString("yyyyMMdd", oCL);
                item.Endda = infotype1.EndDate.ToString("yyyyMMdd", oCL);
                item.Infty = infotype1.InfoType;
                item.Msg = "";
                item.Msgtype = "";
                item.Objps = "";
                item.Operation = "INS";
                item.Pernr = infotype1.EmployeeID;
                item.Reqnr = Guid.NewGuid().ToString();
                item.Seqnr = "";
                item.Status = "N";
                item.Subty = "";
                item.T01 = infotype1.SubAreaSetting.PersonalSubArea;
                count++;
                lstGuid.Add(item.Reqnr);
                Updatetab.Add(item);

                try
                {
                    oFunction.Zhrhri001(ref Updatetab, ref Updatetab_Ret);
                }
                catch (Exception ex)
                {
                    throw new Exception("POST DATA SUBSTITUTION ERROR", ex);
                }
                finally
                {
                    //oFunction.Dispose();
                }
               
                lError = false;
                string errMsg = string.Empty;
                foreach (ZHRHRS001 result in Updatetab_Ret)
                {
                    if (result.Msgtype == "E")
                    {
                        int index = lstGuid.IndexOf(result.Reqnr);
                        data[index].ErrorMessage = result.Msg;
                        errMsg += result.Msg + " (EmployeeID : " + result.Pernr + ", PersonalSubArea : " + result.T01 + " )";
                        lError = true;
                    }
                }

                if (lError)
                {
                    throw new Exception("Post data error : " + errMsg);
                }
            }

            lstGuid.Clear();
            lstGuid = null;

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();
        }

        public override void SaveInfotype0007(List<INFOTYPE0007> data, string RequestNo)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            Connection oConnection = Connection.GetConnectionFromPool(this.BaseConnStr);
            UPLOADINFOTYPE6 oFunction = new UPLOADINFOTYPE6();
            oFunction.Connection = oConnection;

            ZHRHRS001Table Updatetab;
            ZHRHRS001Table Updatetab_Ret;
            ZHRHRS001 item;
            bool lError;
            int count = 0;
            foreach (INFOTYPE0007 infotype7 in data)
            {
                Updatetab = new ZHRHRS001Table();
                Updatetab_Ret = new ZHRHRS001Table();

                item = new ZHRHRS001();
                item.Begda = infotype7.BeginDate.ToString("yyyyMMdd", oCL);
                item.Endda = infotype7.EndDate.ToString("yyyyMMdd", oCL);
                item.Infty = infotype7.InfoType;
                item.Msg = "";
                item.Msgtype = "";
                item.Objps = "";
                item.Operation = "INS";
                item.Pernr = infotype7.EmployeeID;
                item.Reqnr = Guid.NewGuid().ToString();
                item.Seqnr = "";
                item.Status = "N";
                item.Subty = "";
                item.T01 = infotype7.WFRule;
                item.T02 = infotype7.TimeEvaluateClass;
                item.T03 = count.ToString();
                count++;
                Updatetab.Add(item);

                try
                {
                    oFunction.Zhrhri001(ref Updatetab, ref Updatetab_Ret);
                }
                catch (Exception ex)
                {
                    throw new Exception("POST DATA SUBSTITUTION ERROR", ex);
                }
                finally
                {
                    //oFunction.Dispose();
                }
               
                lError = false;
                string errMsg = string.Empty;
                foreach (ZHRHRS001 result in Updatetab_Ret)
                {
                    if (result.Msgtype == "E")
                    {
                        int index = int.Parse(result.T03);
                        data[index].Remark = result.Msg;
                        errMsg += result.Msg + " (EmployeeID : " + result.Pernr + ", WFRule : " + result.T01 + ", TimeEvaluateClass : " + result.T02 + " )";
                        lError = true;
                    }
                }

                if (lError)
                {
                    throw new Exception("Post data error : " + errMsg);
                }
            }

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();
        }

        public override void DeleteInfotype0007(List<INFOTYPE0007> data, string RequestNo)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            Connection oConnection = Connection.GetConnectionFromPool(this.BaseConnStr);
            UPLOADINFOTYPE6 oFunction = new UPLOADINFOTYPE6();
            oFunction.Connection = oConnection;

            ZHRHRS001Table Updatetab;
            ZHRHRS001Table Updatetab_Ret;
            ZHRHRS001 item;
            bool lError;
            int count = 0;
            foreach (INFOTYPE0007 infotype7 in data)
            {
                Updatetab = new ZHRHRS001Table();
                Updatetab_Ret = new ZHRHRS001Table();

                item = new ZHRHRS001();
                item.Begda = infotype7.BeginDate.ToString("yyyyMMdd", oCL);
                item.Endda = infotype7.EndDate.ToString("yyyyMMdd", oCL);
                item.Infty = infotype7.InfoType;
                item.Msg = "";
                item.Msgtype = "";
                item.Objps = "";
                item.Operation = "DEL";
                item.Pernr = infotype7.EmployeeID;
                item.Reqnr = Guid.NewGuid().ToString();
                item.Seqnr = "";
                item.Status = "N";
                item.Subty = "";
                item.T01 = infotype7.WFRule;
                item.T02 = infotype7.TimeEvaluateClass;
                item.T03 = count.ToString();
                count++;
                Updatetab.Add(item);

                try
                {
                    oFunction.Zhrhri001(ref Updatetab, ref Updatetab_Ret);
                }
                catch (Exception ex)
                {
                    throw new Exception("POST DATA SUBSTITUTION ERROR", ex);
                }
                finally
                {
                    //oFunction.Dispose();
                }

                lError = false;
                foreach (ZHRHRS001 result in Updatetab_Ret)
                {
                    if (result.Msgtype == "E")
                    {
                        int index = int.Parse(result.T03);
                        data[index].Remark = result.Msg;
                        lError = true;
                    }
                }

                if (lError)
                {
                    throw new Exception("Post data error");
                }
            }

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();
        }

        #endregion " INFOTYPE0007 "

        #region " INFOTYPE2003 "

        public override List<Substitution> GetInfotype2003(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            List<Substitution> list = null;
            int attemp = 0;
            bool lError = true;
            do
            {
                try
                {
                    list = loadInfotype2003(EmployeeID, BeginDate, EndDate);
                    break;
                }
                catch (Exception ex)
                {
                    if (attemp >= 3)
                    {
                        throw ex;
                    }
                    attemp++;
                }
            } while (lError);
            return list;
        }

        private List<Substitution> loadInfotype2003(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            System.Globalization.CultureInfo oCL = new CultureInfo("en-US");
            List<Substitution> list = new List<Substitution>();
            Connection oConnection = SAPConnection.GetConnectionFromPool(this.BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;
            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_FLD fld;

            #region " SetField "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "MOTPR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "TPROG";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ZEITY";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "MOFID";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "MOSID";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SCHKZ";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGUZ";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDUZ";
            Fields.Add(fld);

            #endregion " SetField "

            #region " SetOption "

            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_OPT opt;
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR = '{0}'", EmployeeID);
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND ( BEGDA <= '{1}' AND ENDDA >= '{0}' )", BeginDate.ToString("yyyyMMdd", oCL_ENUS), EndDate.ToString("yyyyMMdd", oCL_ENUS));
            Options.Add(opt);

            #endregion " SetOption "

            #region " Execute "

            oFunction.Rfc_Read_Table("^", "", "PA2003", 0, 0, ref Data, ref Fields, ref Options);
            SAPConnection.ReturnConnection(oConnection);

            #endregion " Execute "

            #region " ParseToObject "

            foreach (TAB512 item in Data)
            {
                string[] arrTemp = item.Wa.Split('^');
                Substitution inf = new Substitution();
                //PERNR
                inf.EmployeeID = arrTemp[0].Trim();
                //BEGDA
                inf.BeginDate = DateTime.ParseExact(arrTemp[1], "yyyyMMdd", oCL);
                //ENDDA
                inf.EndDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", oCL);
                //MOTPR
                inf.DWSGroup = arrTemp[3].Trim();
                //TPROG
                inf.DWSCode = arrTemp[4].Trim();
                inf.EmpDWSCodeNew = arrTemp[4].Trim();
                //ZEITY
                inf.EmpSubGroupSetting = arrTemp[5].Trim();
                //MOFID
                inf.HolidayCalendar = arrTemp[6].Trim();
                //MOSID
                inf.EmpSubAreaSetting = arrTemp[7].Trim();
                //SHCKZ
                inf.WSRule = arrTemp[8].Trim();
                //BEGUZ"
                if (arrTemp[9].Trim() != "")
                {
                    inf.SubstituteBeginTime = DateTime.ParseExact(arrTemp[9], "HHmmss", oCL).TimeOfDay;
                }
                else
                {
                    inf.SubstituteBeginTime = TimeSpan.Zero;
                }

                //ENDUZ"
                if (arrTemp[10].Trim() != "")
                {
                    inf.SubstituteEndTime = DateTime.ParseExact(arrTemp[10], "HHmmss", oCL).TimeOfDay;
                }
                else
                {
                    inf.SubstituteEndTime = TimeSpan.Zero;
                }
                inf.Status = "COMPLETED";
                list.Add(inf);
            }

            #endregion " ParseToObject "

            oFunction.Dispose();
            return list;
        }

        #endregion " INFOTYPE2003 "

        #region " GetWorkplaceData "

        public override List<WorkPlaceCommunication> GetWorkplaceData(string EmployeeID)
        {
            System.Globalization.CultureInfo oCL = new CultureInfo("en-US");
            List<WorkPlaceCommunication> list = new List<WorkPlaceCommunication>();
            Connection oConnection = SAPConnection.GetConnectionFromPool(this.BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;
            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_FLD fld;

            #region " SetField "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "COM01";
            Fields.Add(fld);

            #endregion " SetField "

            #region " SetOption "

            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_OPT opt;
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("SUBTY = '9'");
            Options.Add(opt);
            if (EmployeeID != "")
            {
                opt = new RFC_DB_OPT();
                opt.Text = string.Format("AND PERNR = '{0}'", EmployeeID);
                Options.Add(opt);
            }

            #endregion " SetOption "

            #region " Execute "

            oFunction.Rfc_Read_Table("^", "", "PA0006", 0, 0, ref Data, ref Fields, ref Options);
            SAPConnection.ReturnConnection(oConnection);

            #endregion " Execute "

            #region " ParseToObject "

            foreach (TAB512 item in Data)
            {
                string[] arrTemp = item.Wa.Split('^');
                WorkPlaceCommunication inf = new WorkPlaceCommunication();
                //PERNR
                inf.EmployeeID = arrTemp[0];
                //BEGDA
                inf.BeginDate = DateTime.ParseExact(arrTemp[1], "yyyyMMdd", oCL);
                //ENDDA
                inf.EndDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", oCL);
                //SUBTY
                inf.WorkPlaceCode = arrTemp[3].Trim();
                list.Add(inf);
            }

            #endregion " ParseToObject "

            oFunction.Dispose();
            return list;
        }

        #endregion " GetWorkplaceData "

        public override List<DateSpecificData> GetDateSpecificList()
        {
            CultureInfo oCL = new CultureInfo("en-US");
            List<DateSpecificData> returnValue = new List<DateSpecificData>();
            Connection oConnection = SAPConnection.GetConnectionFromPool(this.BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;
            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_FLD fld;

            #region " SetField "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            #endregion " SetField "

            #region " SetOption "

            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_OPT opt;
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("MASSN = '01'");
            Options.Add(opt);

            #endregion " SetOption "

            #region " Execute "

            oFunction.Rfc_Read_Table("^", "", "PA0000", 0, 0, ref Data, ref Fields, ref Options);

            #endregion " Execute "

            Dictionary<string, DateSpecificData> dateList = new Dictionary<string, DateSpecificData>();

            #region " ParseToObject "

            foreach (TAB512 item in Data)
            {
                string[] buffer = item.Wa.Split('^');
                DateSpecificData d;
                if (!dateList.ContainsKey(buffer[0]))
                {
                    d = new DateSpecificData();
                    d.EmployeeID = buffer[0];
                    d.HiringDate = DateTime.ParseExact(buffer[1], "yyyyMMdd", oCL_ENUS);
                    dateList.Add(d.EmployeeID, d);
                }
                else
                {
                    d = dateList[buffer[0]];
                    d.HiringDate = DateTime.ParseExact(buffer[1], "yyyyMMdd", oCL_ENUS);
                }
            }

            #endregion " ParseToObject "

            Data = new TAB512Table();
            Fields = new RFC_DB_FLDTable();

            #region " SetField "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "DAR02";
            Fields.Add(fld);
            fld = new RFC_DB_FLD();
            fld.Fieldname = "DAT02";
            Fields.Add(fld);
            fld = new RFC_DB_FLD();
            fld.Fieldname = "DAR03";
            Fields.Add(fld);
            fld = new RFC_DB_FLD();
            fld.Fieldname = "DAT03";
            Fields.Add(fld);
            fld = new RFC_DB_FLD();
            fld.Fieldname = "DAR04";
            Fields.Add(fld);
            fld = new RFC_DB_FLD();
            fld.Fieldname = "DAT04";
            Fields.Add(fld);
            fld = new RFC_DB_FLD();
            fld.Fieldname = "DAR05";
            Fields.Add(fld);
            fld = new RFC_DB_FLD();
            fld.Fieldname = "DAT05";
            Fields.Add(fld);
            fld = new RFC_DB_FLD();
            fld.Fieldname = "DAR06";
            Fields.Add(fld);
            fld = new RFC_DB_FLD();
            fld.Fieldname = "DAT06";
            Fields.Add(fld);
            fld = new RFC_DB_FLD();
            fld.Fieldname = "DAR07";
            Fields.Add(fld);
            fld = new RFC_DB_FLD();
            fld.Fieldname = "DAT07";
            Fields.Add(fld);
            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);

            #endregion " SetField "

            #region " SetOption "

            Options = new RFC_DB_OPTTable();
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("BEGDA <= '{0}' AND ENDDA >= '{0}'", DateTime.Now.ToString("yyyyMMdd", oCL_ENUS));
            Options.Add(opt);

            #endregion " SetOption "

            #region " Execute "

            oFunction.Rfc_Read_Table("^", "", "PA0041", 0, 0, ref Data, ref Fields, ref Options);
            SAPConnection.ReturnConnection(oConnection);

            #endregion " Execute "

            #region " ParseToObject "

            foreach (TAB512 item in Data)
            {
                DateTime temp;
                string[] buffer = item.Wa.Split('^');
                string empid = buffer[6];
                DateSpecificData d;
                if (!dateList.ContainsKey(empid))
                {
                    d = new DateSpecificData();
                    d.EmployeeID = empid;
                    dateList.Add(empid, d);
                }
                else
                {
                    d = dateList[empid];
                }

                for (int i = 0; i < 3; i++)
                {
                    switch (buffer[2 * i])
                    {
                        case "ZB":
                            d.HiringDate = DateTime.ParseExact(buffer[2 * i + 1], "yyyyMMdd", oCL_ENUS);
                            break;

                        case "ZC":
                            d.RetirementDate = DateTime.ParseExact(buffer[2 * i + 1], "yyyyMMdd", oCL_ENUS);
                            break;

                        case "ZD":
                            d.StartWorkingDate = DateTime.ParseExact(buffer[2 * i + 1], "yyyyMMdd", oCL_ENUS);
                            break;

                        case "ZE":
                            d.StartPF = DateTime.ParseExact(buffer[2 * i + 1], "yyyyMMdd", oCL_ENUS);
                            break;

                        case "ZF":
                            d.EndPF = DateTime.ParseExact(buffer[2 * i + 1], "yyyyMMdd", oCL_ENUS);
                            break;

                        case "ZG":
                            d.StartAbsenceQuota = DateTime.ParseExact(buffer[2 * i + 1], "yyyyMMdd", oCL_ENUS);
                            break;
                    }
                }
            }

            #endregion " ParseToObject "

            oFunction.Dispose();

            foreach (string key in dateList.Keys)
            {
                returnValue.Add(dateList[key]);
            }
            return returnValue;
        }

        public override DateSpecificData GetDateSpecific(string EmployeeID)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            DateSpecificData returnValue = new DateSpecificData();
            Connection oConnection = SAPConnection.GetConnectionFromPool(this.BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;
            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_FLD fld;
            RFC_DB_OPTTable Options;
            RFC_DB_OPT opt;

            #region " SetField "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "DAR01";
            Fields.Add(fld);
            fld = new RFC_DB_FLD();
            fld.Fieldname = "DAT01";
            Fields.Add(fld);
            fld = new RFC_DB_FLD();
            fld.Fieldname = "DAR02";
            Fields.Add(fld);
            fld = new RFC_DB_FLD();
            fld.Fieldname = "DAT02";
            Fields.Add(fld);
            fld = new RFC_DB_FLD();
            fld.Fieldname = "DAR03";
            Fields.Add(fld);
            fld = new RFC_DB_FLD();
            fld.Fieldname = "DAT03";
            Fields.Add(fld);
            fld = new RFC_DB_FLD();
            fld.Fieldname = "DAR04";
            Fields.Add(fld);
            fld = new RFC_DB_FLD();
            fld.Fieldname = "DAT04";
            Fields.Add(fld);
            fld = new RFC_DB_FLD();
            fld.Fieldname = "DAR05";
            Fields.Add(fld);
            fld = new RFC_DB_FLD();
            fld.Fieldname = "DAT05";
            Fields.Add(fld);
            fld = new RFC_DB_FLD();
            fld.Fieldname = "DAR06";
            Fields.Add(fld);
            fld = new RFC_DB_FLD();
            fld.Fieldname = "DAT06";
            Fields.Add(fld);
            fld = new RFC_DB_FLD();
            fld.Fieldname = "DAR07";
            Fields.Add(fld);
            fld = new RFC_DB_FLD();
            fld.Fieldname = "DAT07";
            Fields.Add(fld);

            #endregion " SetField "

            #region " SetOption "

            Options = new RFC_DB_OPTTable();
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR = '{0}'", EmployeeID);
            Options.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND BEGDA <= '{0}' AND ENDDA >= '{0}'", DateTime.Now.ToString("yyyyMMdd", oCL_ENUS));
            Options.Add(opt);

            #endregion " SetOption "

            #region " Execute "

            oFunction.Rfc_Read_Table("^", "", "PA0041", 0, 0, ref Data, ref Fields, ref Options);
            SAPConnection.ReturnConnection(oConnection);

            #endregion " Execute "

            #region " ParseToObject "

            foreach (TAB512 item in Data)
            {
                string[] buffer = item.Wa.Split('^');
                for (int i = 0; i < 3; i++)
                {
                    switch (buffer[2 * i])
                    {
                        case "ZA":
                            returnValue.HiringDate = DateTime.ParseExact(buffer[2 * i + 1], "yyyyMMdd", oCL_ENUS);
                            break;
                        //                        case "ZB":
                        //                            returnValue.HiringDate = DateTime.ParseExact(buffer[2 * i + 1], "yyyyMMdd", oCL_ENUS);
                        //                            break;
                        case "ZB":
                            returnValue.PassProbation = DateTime.ParseExact(buffer[2 * i + 1], "yyyyMMdd", oCL_ENUS);
                            break;

                        case "ZC":
                            returnValue.RetirementDate = DateTime.ParseExact(buffer[2 * i + 1], "yyyyMMdd", oCL_ENUS);
                            break;

                        case "ZD":
                            returnValue.StartWorkingDate = DateTime.ParseExact(buffer[2 * i + 1], "yyyyMMdd", oCL_ENUS);
                            break;

                        case "ZE":
                            returnValue.StartPF = DateTime.ParseExact(buffer[2 * i + 1], "yyyyMMdd", oCL_ENUS);
                            break;

                        case "ZF":
                            returnValue.EndPF = DateTime.ParseExact(buffer[2 * i + 1], "yyyyMMdd", oCL_ENUS);
                            break;

                        case "ZG":
                            returnValue.StartAbsenceQuota = DateTime.ParseExact(buffer[2 * i + 1], "yyyyMMdd", oCL_ENUS);
                            break;
                    }
                }
                returnValue.EmployeeID = EmployeeID;
                break;
            }

            #endregion " ParseToObject "

            oFunction.Dispose();
            return returnValue;
        }

        #region "GET INFOTYPE0030"

        public override INFOTYPE0030 GetInfotype0030(string EmployeeID, DateTime CheckDate)
        {
            System.Globalization.CultureInfo oCL = new CultureInfo("en-US");
            Connection oConnection = SAPConnection.GetConnectionFromPool(this.BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;
            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_FLD fld;

            #region " SetField "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "VOLMA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ORGEH";
            Fields.Add(fld);

            #endregion " SetField "

            #region " SetOption "

            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_OPT opt;
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR = '{0}' AND BEGDA <= '{1}' AND ENDDA >= '{1}'", EmployeeID, CheckDate.ToString("yyyyMMdd", oCL));
            Options.Add(opt);

            #endregion " SetOption "

            #region " Execute "

            oFunction.Zrfc_Read_Table("^", "", "PA0030", 0, 0, ref Data, ref Fields, ref Options);
            SAPConnection.ReturnConnection(oConnection);

            #endregion " Execute "

            #region " ParseToObject "

            INFOTYPE0030 oReturn = new INFOTYPE0030();
            oReturn.SecondmentType = "00";
            foreach (TAB512 item in Data)
            {
                string[] arrTemp = item.Wa.Split('^');

                //PERNR
                oReturn.EmployeeID = arrTemp[0];
                //BEGDA
                oReturn.BeginDate = DateTime.ParseExact(arrTemp[1], "yyyyMMdd", oCL);
                //ENDDA
                oReturn.EndDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", oCL);
                //VOLMA
                oReturn.SecondmentType = arrTemp[3].Trim();
                //ORGEH
                oReturn.OraganizationUnit = arrTemp[4];
            }

            #endregion " ParseToObject "

            oFunction.Dispose();
            return oReturn;
        }

        public override List<INFOTYPE0030> GetInfotype0030List(string EmployeeID1, string EmployeeID2, string Profile)
        {
            System.Globalization.CultureInfo oCL = new CultureInfo("en-US");
            Connection oConnection = SAPConnection.GetConnectionFromPool(this.BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;
            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_FLD fld;

            #region " SetField "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "VOLMA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ORGEH";
            Fields.Add(fld);

            #endregion " SetField "

            #region " SetOption "

            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_OPT opt;
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR >= '{0}' AND PERNR <= '{1}'", EmployeeID1, EmployeeID2);
            Options.Add(opt);

            #endregion " SetOption "

            #region " Execute "

            oFunction.Zrfc_Read_Table("^", "", "PA0030", 0, 0, ref Data, ref Fields, ref Options);
            SAPConnection.ReturnConnection(oConnection);

            #endregion " Execute "

            #region " ParseToObject "

            List<INFOTYPE0030> oList = new List<INFOTYPE0030>();
            foreach (TAB512 item in Data)
            {
                INFOTYPE0030 oTemp = new INFOTYPE0030();
                string[] arrTemp = item.Wa.Split('^');
                //PERNR
                oTemp.EmployeeID = arrTemp[0];
                //BEGDA
                oTemp.BeginDate = DateTime.ParseExact(arrTemp[1], "yyyyMMdd", oCL);
                //ENDDA
                oTemp.EndDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", oCL);
                //VOLMA
                oTemp.SecondmentType = arrTemp[3].Trim();
                //ORGEH
                oTemp.OraganizationUnit = arrTemp[4];
                oList.Add(oTemp);
            }

            #endregion " ParseToObject "

            oFunction.Dispose();
            return oList;
        }

        #endregion "GET INFOTYPE0030"

        //AddBy: Ratchatawan W. (2012-12-24)
        public override List<INFOTYPE0032> GetInfotype0032List(string EmployeeID1, string EmployeeID2, DateTime CheckDate, string Profile)
        {
            List<INFOTYPE0032> list = new List<INFOTYPE0032>();
            System.Globalization.CultureInfo oCL = new CultureInfo("en-US");
            Connection oConnection = SAPConnection.GetConnectionFromPool(this.BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;
            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_FLD fld;

            #region " SetField "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PNALT";
            Fields.Add(fld);

            #endregion " SetField "

            #region " SetOption "

            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_OPT opt;
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR <= '{1}' AND PERNR >= '{0}'", EmployeeID1, EmployeeID2);
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND BEGDA <= '{0}' AND ENDDA >= '{0}'", CheckDate.ToString("yyyyMMdd", oCL));
            Options.Add(opt);

            #endregion " SetOption "

            #region " Execute "

            oFunction.Rfc_Read_Table("^", "", "PA0032", 0, 0, ref Data, ref Fields, ref Options);
            SAPConnection.ReturnConnection(oConnection);

            #endregion " Execute "

            #region " ParseToObject "

            foreach (TAB512 item in Data)
            {
                string[] arrTemp = item.Wa.Split('^');
                INFOTYPE0032 inf = new INFOTYPE0032();
                //PERNR
                inf.EmployeeID = arrTemp[0];
                //BEGDA
                inf.BeginDate = DateTime.ParseExact(arrTemp[1], "yyyyMMdd", oCL);
                //ENDDA
                inf.EndDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", oCL);

                inf.OldEmployeeID = arrTemp[3];

                list.Add(inf);
            }

            #endregion " ParseToObject "

            oFunction.Dispose();
            return list;
        }

        public override List<INFOTYPE0185> GetInfotype0185(string EmployeeID, DateTime date)
        {
            List<INFOTYPE0185> oReturn = new List<INFOTYPE0185>();
            System.Globalization.CultureInfo oCL = new CultureInfo("en-US");
            string strDate = date.ToString("yyyyMMdd", oCL);
            Connection oConnection = SAPConnection.GetConnectionFromPool(this.BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;
            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " SetField "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ICNUM";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SUBTY";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            #endregion " SetField "

            #region " SetOption "

            Options = new RFC_DB_OPTTable();
            opt = new RFC_DB_OPT();
            opt.Text = string.Format(" PERNR = '{0}' ", EmployeeID);
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format(" AND BEGDA <= '{0}' AND ENDDA >= '{0}' ", strDate);
            Options.Add(opt);

            //opt = new RFC_DB_OPT();
            //opt.Text = string.Format(" SUBTY = '01' ");
            //Options.Add(opt);

            #endregion " SetOption "

            #region " Execute "

            oFunction.Zrfc_Read_Table("^", "", "PA0185", 0, 0, ref Data, ref Fields, ref Options);
            SAPConnection.ReturnConnection(oConnection);

            #endregion " Execute "

            #region " ParseToObject "

            foreach (TAB512 item in Data)
            {
                string[] arrTemp = item.Wa.Split('^');

                //�ѵû�ЪҪ�
                //oReturn.CardID = arrTemp[0];
                INFOTYPE0185 inf = new INFOTYPE0185();
                inf.CardID = arrTemp[0];
                inf.SubType = arrTemp[1];
                inf.BeginDate = arrTemp[2];
                inf.EndDate = arrTemp[3];
                oReturn.Add(inf);
            }

            #endregion " ParseToObject "

            //Fields = new RFC_DB_FLDTable();

            ////˹ѧ����Թ�ҧ
            //#region " SetField "
            //fld = new RFC_DB_FLD();
            //fld.Fieldname = "ICNUM";
            //Fields.Add(fld);
            //#endregion

            //#region " SetOption "
            //Options = new RFC_DB_OPTTable();
            //opt = new RFC_DB_OPT();
            //opt.Text = string.Format(" PERNR = '{0}' ", EmployeeID);
            //Options.Add(opt);

            //opt = new RFC_DB_OPT();
            //opt.Text = string.Format(" AND BEGDA <= '{0}' AND ENDDA >= '{0}' ", strDate);
            //Options.Add(opt);

            ////opt = new RFC_DB_OPT();
            ////opt.Text = string.Format(" SUBTY = '06' ");
            ////Options.Add(opt);
            //#endregion

            //#region " Execute "
            //oFunction.Zrfc_Read_Table("^", "", "PA0185", 0, 0, ref Data, ref Fields, ref Options);
            //SAPConnection.ReturnConnection(oConnection);
            //#endregion

            //#region " ParseToObject "
            //foreach (TAB512 item in Data)
            //{
            //    string[] arrTemp = item.Wa.Split('^');
            //    //oReturn.PassportID = arrTemp[0];
            //    INFOTYPE0185 inf = new INFOTYPE0185();
            //    inf.PassportID = arrTemp[0];
            //    oReturn.Add(inf);
            //}
            //#endregion

            oFunction.Dispose();
            return oReturn;
        }
        #region "GET INFOTYPE0027"
        public override List<INFOTYPE0027> GetInfotype0027List(string EmployeeID, DateTime CheckDate)
        {
            System.Globalization.CultureInfo oCL = new CultureInfo("en-US");
            Connection oConnection = SAPConnection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;
            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_FLD fld;

            #region " SetField "
            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SUBTY";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            int n = 1;
            while (n < 16)
            {
                string f1 = "KBU" + n.ToString().PadLeft(2, '0');
                string f2 = "KST" + n.ToString().PadLeft(2, '0');
                string f3 = "KPR" + n.ToString().PadLeft(2, '0');

                fld = new RFC_DB_FLD();
                fld.Fieldname = f1;
                Fields.Add(fld);

                fld = new RFC_DB_FLD();
                fld.Fieldname = f2;
                Fields.Add(fld);

                fld = new RFC_DB_FLD();
                fld.Fieldname = f3;
                Fields.Add(fld);
                n++;
            }
            #endregion

            #region " SetOption "
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_OPT opt;
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR = '{0}'", EmployeeID);
            Options.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND SUBTY in ( {0} )", this.INF27_SUBTYPE);
            Options.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND BEGDA > '{0}'", MINIMUM_DATE_IN_SAP.ToString("yyyyMMdd", oCL_ENUS));
            Options.Add(opt);
            #endregion

            #region " Execute "
            oFunction.Zrfc_Read_Table("^", "", "PA0027", 0, 0, ref Data, ref Fields, ref Options);
            SAPConnection.ReturnConnection(oConnection);
            #endregion

            #region " ParseToObject "
            List<INFOTYPE0027> oReturn = new List<INFOTYPE0027>();

            foreach (TAB512 item in Data)
            {
                string[] arrTemp = item.Wa.Split('^');
                int i = 4;
                while (i < 49)
                {
                    INFOTYPE0027 inf27 = new INFOTYPE0027();
                    //PERNR
                    inf27.EmployeeID = arrTemp[0];
                    //SUBTY
                    inf27.SubType = arrTemp[1];
                    //BEGDA
                    inf27.BeginDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", oCL);
                    //ENDDA
                    inf27.EndDate = DateTime.ParseExact(arrTemp[3], "yyyyMMdd", oCL);
                    //KBU
                    inf27.CompanyCode = arrTemp[i].Trim();
                    //KST
                    inf27.CostCenter = arrTemp[i + 1];
                    //KPR
                    inf27.Percentage = arrTemp[i + 2];

                    i = i + 3;
                    oReturn.Add(inf27);
                }
            }
            #endregion

            oFunction.Dispose();
            return oReturn;
        }
        public override List<INFOTYPE0027> GetInfotype0027List(string EmployeeID1, string EmployeeID2, string Profile)
        {

            System.Globalization.CultureInfo oCL = new CultureInfo("en-US");
            Connection oConnection = SAPConnection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;
            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_FLD fld;

            #region " SetField "
            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SUBTY";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            int n = 1;
            while (n < 16)
            {
                string f1 = "KBU" + n.ToString().PadLeft(2, '0');
                string f2 = "KST" + n.ToString().PadLeft(2, '0');
                string f3 = "KPR" + n.ToString().PadLeft(2, '0');

                fld = new RFC_DB_FLD();
                fld.Fieldname = f1;
                Fields.Add(fld);

                fld = new RFC_DB_FLD();
                fld.Fieldname = f2;
                Fields.Add(fld);

                fld = new RFC_DB_FLD();
                fld.Fieldname = f3;
                Fields.Add(fld);
                n++;
            }
            #endregion

            #region " SetOption "
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_OPT opt;
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR >= '{0}' AND PERNR <= '{1}'", EmployeeID1, EmployeeID2);
            Options.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND SUBTY in ( {0} )", this.INF27_SUBTYPE);
            Options.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND BEGDA > '{0}'", MINIMUM_DATE_IN_SAP.ToString("yyyyMMdd", oCL_ENUS));
            Options.Add(opt);
            #endregion

            #region " Execute "
            oFunction.Zrfc_Read_Table("^", "", "PA0027", 0, 0, ref Data, ref Fields, ref Options);
            SAPConnection.ReturnConnection(oConnection);
            #endregion

            #region " ParseToObject "
            List<INFOTYPE0027> oList = new List<INFOTYPE0027>();

            Dictionary<string, int> dict = new Dictionary<string, int>();
            foreach (TAB512 item in Data)
            {
                int i = 4;
                while (i < 49)
                {
                    INFOTYPE0027 oTemp = new INFOTYPE0027();
                    string[] arrTemp = item.Wa.Split('^');

                    if (arrTemp[i].Trim() != "" && arrTemp[i].Trim() != null)
                    {
                        //PERNR
                        oTemp.EmployeeID = arrTemp[0];
                        //SUBTY
                        oTemp.SubType = arrTemp[1];
                        //BEGDA
                        oTemp.BeginDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", oCL);
                        //ENDDA
                        oTemp.EndDate = DateTime.ParseExact(arrTemp[3], "yyyyMMdd", oCL);
                        //KBU
                        oTemp.CompanyCode = arrTemp[i].Trim();
                        //KST
                        oTemp.CostCenter = arrTemp[i + 1].Trim();
                        //KPR
                        oTemp.Percentage = arrTemp[i + 2].Trim();
                        try
                        {
                            Decimal.Parse(oTemp.Percentage);
                        }
                        catch (Exception ex)
                        {
                            oTemp.Percentage = "0.00";
                        }

                        string key = string.Format("{0}#{1}#{2}#{3}#{4}#{5}", oTemp.EmployeeID, oTemp.SubType, oTemp.BeginDate.Ticks, oTemp.EndDate.Ticks, oTemp.CompanyCode, oTemp.CostCenter);
                        if (!dict.ContainsKey(key))
                        {
                            dict[key] = 1;
                            oList.Add(oTemp);
                        }
                    }

                    i = i + 3;
                }


            }
            #endregion
            oFunction.Dispose();
            return oList;
        }
        #endregion
    }
}