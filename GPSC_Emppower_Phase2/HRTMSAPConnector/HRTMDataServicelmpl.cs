using System;
using System.Data;
using System.Collections.Generic;
using System.Globalization;
using ESS.HR.TM.ABSTRACT;
using ESS.HR.TM.DATACLASS;
using ESS.HR.TM.INFOTYPE;
using ESS.HR.TM.CONFIG;
using ESS.SHAREDATASERVICE;
using SAP.Connector;
using SAPInterface;
using AbsAttCalculateInterface;
//using ESS.DATA;
using ESS.HR.TM;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.TIMESHEET;

namespace ESS.HR.TM.SAP
{
    public class HRTMDataServiceImpl : AbstractHRTMDataService
    {
        CultureInfo oCL = new CultureInfo("en-US");
        #region Constructor
        public HRTMDataServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
            //Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID);
        }

        #endregion Constructor

        #region Member
        private CultureInfo DefaultCultureInfo = CultureInfo.GetCultureInfo("en-GB");
        //private Dictionary<string, string> Config { get; set; }

        private static string ModuleID = "ESS.HR.TM.SAP";
        public string CompanyCode { get; set; }
        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }
        private string Sap_Version
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAP_VERSION");


            }
        }

        #endregion Member

        #region " Absence Quota "
        public override List<INFOTYPE2006> GetAbsenceQuota(string EmployeeID)
        {
            return GetAbsenceQuota(EmployeeID, DateTime.Now, true);
        }

        public override List<INFOTYPE2006> GetAbsenceQuota(string EmployeeID, bool IncludeFuture)
        {
            return GetAbsenceQuota(EmployeeID, DateTime.Now, IncludeFuture);
        }

        public override List<INFOTYPE2006> GetAbsenceQuota(string EmployeeID, DateTime CheckDate, bool IncludeFuture)
        {
            List<INFOTYPE2006> list = this.GetInfotype2006Data(EmployeeID, EmployeeID, CheckDate, IncludeFuture);
            return list;
        }

        //AddBy: Ratchatawan W. (2012-09-17)
        public override List<INFOTYPE2006> GetAbsenceQuota(string EmployeeID1, string Employee2, DateTime CheckDate, bool IncludeFuture)
        {
            List<INFOTYPE2006> list = this.GetInfotype2006Data(EmployeeID1, Employee2, CheckDate, IncludeFuture);
            return list;
        }

        public override List<INFOTYPE2006> GetAbsenceQuotaForDeduction(string EmployeeID1, string EmployeeID2, DateTime CheckDate)
        {
            List<INFOTYPE2006> list = this.GetInfotypes2006DataForDeduction(EmployeeID1, EmployeeID2, CheckDate);
            return list;
        }

        //Addby Nuttapon.a 17.01.2013 for getinfotype2006 list condition with begin and end
        public override List<INFOTYPE2006> GetAbsenceQuotaList(string EmployeeID1, string EmployeeID2, DateTime BeginDate, DateTime EndDate)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            List<INFOTYPE2006> oReturn = new List<INFOTYPE2006>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "
            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SUBTY";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SEQNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "DESTA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "DEEND";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ANZHL";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "KVERB";
            Fields.Add(fld);
            #endregion

            #region " Options "
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR >= '{0}' AND PERNR <= '{1}'", EmployeeID1, EmployeeID2);
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND ENDDA >= '{0}' AND BEGDA <= '{1}'", BeginDate.ToString("yyyyMMdd", oCL), EndDate.ToString("yyyyMMdd", oCL));
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND SPRPS <> 'X'");
            Options.Add(opt);
            #endregion

            oFunction.Zrfc_Read_Table("^", "", "PA2006", 0, 0, ref Data, ref Fields, ref Options);

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            #region " Parse Object "
            INFOTYPE2006 data;
            foreach (TAB512 item in Data)
            {
                data = new INFOTYPE2006();
                string[] arrTemp = item.Wa.Split('^');
                //fld.Fieldname = "PERNR";
                data.EmployeeID = arrTemp[0].Trim();
                //fld.Fieldname = "SUBTY";
                data.SubType = arrTemp[1].Trim();
                //fld.Fieldname = "BEGDA";
                data.BeginDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", oCL);
                //fld.Fieldname = "ENDDA";
                data.EndDate = DateTime.ParseExact(arrTemp[3], "yyyyMMdd", oCL);
                //fld.Fieldname = "SEQNR";
                data.SeqNo = arrTemp[4];
                //fld.Fieldname = "DESTA";
                data.DeductionStart = DateTime.ParseExact(arrTemp[5], "yyyyMMdd", oCL);
                //fld.Fieldname = "DEEND";
                data.DeductionEnd = DateTime.ParseExact(arrTemp[6], "yyyyMMdd", oCL);
                //fld.Fieldname = "ANZHL";
                Decimal tempValue = 0.0M;
                if (Decimal.TryParse(arrTemp[7], out tempValue))
                {
                    data.QuotaAmount = tempValue;
                }
                //fld.Fieldname = "KVERB";
                tempValue = 0.0M;
                if (Decimal.TryParse(arrTemp[8], out tempValue))
                {
                    data.UsedAmount = tempValue;
                }
                oReturn.Add(data);
            }
            #endregion

            oReturn.Sort(INFOTYPE2006.Comparer);

            return oReturn;
        }


        #region " GetInfotypes2006DataForDeduction "
        private List<INFOTYPE2006> GetInfotypes2006DataForDeduction(string EmployeeID1, string EmployeeID2, DateTime CheckDate)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            List<INFOTYPE2006> oReturn = new List<INFOTYPE2006>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "
            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SUBTY";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SEQNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "DESTA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "DEEND";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ANZHL";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "KVERB";
            Fields.Add(fld);
            #endregion

            #region " Options "
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR >= '{0}' AND PERNR <= '{1}'", EmployeeID1, EmployeeID2);
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND DESTA <= '{0}' AND DEEND >= '{0}' ", CheckDate.ToString("yyyyMMdd", oCL)); //nuttapon.a 21.09.2012 get valid quota from dedection-from and dedection-to 
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND SPRPS <> 'X'");
            Options.Add(opt);
            #endregion

            oFunction.Zrfc_Read_Table("^", "", "PA2006", 0, 0, ref Data, ref Fields, ref Options);

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            #region " Parse Object "
            INFOTYPE2006 data;
            foreach (TAB512 item in Data)
            {
                data = new INFOTYPE2006();
                string[] arrTemp = item.Wa.Split('^');
                //fld.Fieldname = "PERNR";
                data.EmployeeID = arrTemp[0].Trim();
                //fld.Fieldname = "SUBTY";
                data.SubType = arrTemp[1].Trim();
                //fld.Fieldname = "BEGDA";
                data.BeginDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", oCL);
                //fld.Fieldname = "ENDDA";
                data.EndDate = DateTime.ParseExact(arrTemp[3], "yyyyMMdd", oCL);
                //fld.Fieldname = "SEQNR";
                data.SeqNo = arrTemp[4];
                //fld.Fieldname = "DESTA";
                data.DeductionStart = DateTime.ParseExact(arrTemp[5], "yyyyMMdd", oCL);
                //fld.Fieldname = "DEEND";
                data.DeductionEnd = DateTime.ParseExact(arrTemp[6], "yyyyMMdd", oCL);
                //fld.Fieldname = "ANZHL";
                Decimal tempValue = 0.0M;
                if (Decimal.TryParse(arrTemp[7], out tempValue))
                {
                    data.QuotaAmount = tempValue;
                }
                //fld.Fieldname = "KVERB";
                tempValue = 0.0M;
                if (Decimal.TryParse(arrTemp[8], out tempValue))
                {
                    data.UsedAmount = tempValue;
                }
                oReturn.Add(data);
            }
            #endregion

            oReturn.Sort(INFOTYPE2006.Comparer);

            return oReturn;
        }
        #endregion

        #region " GetInfotype2006Data"
        private List<INFOTYPE2006> GetInfotype2006Data(string EmployeeID1, string EmployeeID2, DateTime CheckDate, bool IncludeFuture)
        { // modified this function  for display all quota by nuttapon 21.09.2012
            CultureInfo oCL = new CultureInfo("en-US");
            List<INFOTYPE2006> oReturn = new List<INFOTYPE2006>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "
            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SUBTY";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SEQNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "DESTA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "DEEND";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ANZHL";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "KVERB";
            Fields.Add(fld);
            #endregion

            #region " Options "
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR >= '{0}' AND PERNR <= '{1}'", EmployeeID1, EmployeeID2);
            Options.Add(opt);
            opt = new RFC_DB_OPT();

            if (IncludeFuture) // 06.12.2012 �Ҩҡ config iService3.sp_GetQuotaPeriodSetting ��˹���Ҩ�����ͧ��� Quota �͹Ҥ��������
            {
                //opt.Text = string.Format("AND ENDDA > '{0}'", CheckDate.AddDays(-16).ToString("yyyyMMdd", oCL)); // modified by nuttapon 21.09.2012
                opt.Text = string.Format("AND ENDDA > '{0}'", CheckDate.ToString("yyyyMMdd", oCL)); // modified by nuttapon 21.09.2012
            }
            else
            {
                opt.Text = string.Format("AND BEGDA <= '{0}' AND ENDDA >= '{0}' ", CheckDate.ToString("yyyyMMdd", oCL));
            }
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND SPRPS <> 'X'");
            Options.Add(opt);
            #endregion

            oFunction.Zrfc_Read_Table("^", "", "PA2006", 0, 0, ref Data, ref Fields, ref Options);

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            #region " Parse Object "
            INFOTYPE2006 data;
            foreach (TAB512 item in Data)
            {
                data = new INFOTYPE2006();
                string[] arrTemp = item.Wa.Split('^');
                //fld.Fieldname = "PERNR";
                data.EmployeeID = arrTemp[0].Trim();
                //fld.Fieldname = "SUBTY";
                data.SubType = arrTemp[1].Trim();
                //fld.Fieldname = "BEGDA";
                data.BeginDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", oCL);
                //fld.Fieldname = "ENDDA";
                data.EndDate = DateTime.ParseExact(arrTemp[3], "yyyyMMdd", oCL);
                //fld.Fieldname = "SEQNR";
                data.SeqNo = arrTemp[4];
                //fld.Fieldname = "DESTA";
                data.DeductionStart = DateTime.ParseExact(arrTemp[5], "yyyyMMdd", oCL);
                //fld.Fieldname = "DEEND";
                data.DeductionEnd = DateTime.ParseExact(arrTemp[6], "yyyyMMdd", oCL);
                //fld.Fieldname = "ANZHL";
                Decimal tempValue = 0.0M;
                if (Decimal.TryParse(arrTemp[7], out tempValue))
                {
                    data.QuotaAmount = tempValue;
                }
                //fld.Fieldname = "KVERB";
                tempValue = 0.0M;
                if (Decimal.TryParse(arrTemp[8], out tempValue))
                {
                    data.UsedAmount = tempValue;
                }
                oReturn.Add(data);
            }
            #endregion

            oReturn.Sort(INFOTYPE2006.Comparer);

            return oReturn;
        }
        #endregion

        public override int GetNextYearAbsenceQuota(string EmployeeID)
        {
            // Get INFOTYPE0041 for Absence quota calculation -- DAT01
            CultureInfo oCL = new CultureInfo("en-US");

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "
            fld = new RFC_DB_FLD();
            fld.Fieldname = "DAR01";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "DAT01";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "DAR02";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "DAT02";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "DAR03";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "DAT03";
            Fields.Add(fld);
            #endregion

            #region " Options "
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR = '{0}'", EmployeeID);
            Options.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND ENDDA >= '{0}' AND BEGDA <= '{1}'", DateTime.Now.ToString("yyyyMMdd", oCL), DateTime.Now.ToString("yyyyMMdd", oCL));
            Options.Add(opt);
            #endregion

            oFunction.Zrfc_Read_Table("^", "", "PA0041", 0, 0, ref Data, ref Fields, ref Options);

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            DateTime startAQ = DateTime.MinValue;

            #region " Parse Object "
            foreach (TAB512 item in Data)
            {
                string[] arrTemp = item.Wa.Split('^');
                for (int i = 1; i <= 3; i++)
                {
                    if (arrTemp[2 * (i - 1)] == "01")
                    {
                        DateTime.TryParseExact(arrTemp[2 * i - 1], "yyyyMMdd", oCL, DateTimeStyles.None, out startAQ);
                        break;
                    }
                }
                break;
            }
            #endregion

            if (startAQ == DateTime.MinValue)
            {
                throw new Exception("Can't find start Absence quota date from INFOTYPE 41");
            }

            int serviceYear = DateTime.Now.Year - startAQ.AddDays(-1).Year;
            int quotaReceive;
            if (serviceYear >= 5)
            {
                quotaReceive = 15;
            }
            else
            {
                quotaReceive = 10;
            }

            return quotaReceive;
        }

        #endregion

        #region " Absence "

        #region " GetAbsenceList "
        public override List<INFOTYPE2001> GetAbsenceList(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            List<INFOTYPE2001> list = this.GetInfotype2001Data(EmployeeID, EmployeeID, BeginDate, EndDate);
            return list;
        }

        public override List<INFOTYPE2001> GetAbsenceList(string EmployeeID1, string EmployeeID2, DateTime BeginDate, DateTime EndDate)
        {
            List<INFOTYPE2001> list = this.GetInfotype2001Data(EmployeeID1, EmployeeID2, BeginDate, EndDate);
            return list;
        }
        #endregion

        #region " GetInfotype2001Data "
        private List<INFOTYPE2001> GetInfotype2001Data(string EmployeeID1, string EmployeeID2, DateTime BeginDate, DateTime EndDate)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            List<INFOTYPE2001> oReturn = new List<INFOTYPE2001>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "
            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SUBTY";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SEQNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGUZ";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDUZ";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ABWTG";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "STDAZ";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ABRTG";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ALLDF";
            Fields.Add(fld);
            #endregion

            #region " Options "
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR >= '{0}' AND PERNR <= '{1}'", EmployeeID1, EmployeeID2);
            Options.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND ENDDA >= '{0}' AND BEGDA <= '{1}'", BeginDate.ToString("yyyyMMdd", oCL), EndDate.ToString("yyyyMMdd", oCL));
            Options.Add(opt);
            #endregion

            oFunction.Zrfc_Read_Table("^", "", "PA2001", 0, 0, ref Data, ref Fields, ref Options);

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            #region " Parse Object "
            INFOTYPE2001 data;
            foreach (TAB512 item in Data)
            {
                data = new INFOTYPE2001();
                string[] arrTemp = item.Wa.Split('^');
                //fld.Fieldname = "PERNR";
                data.EmployeeID = arrTemp[0].Trim();
                //fld.Fieldname = "SUBTY";
                data.SubType = arrTemp[1].Trim();
                //fld.Fieldname = "BEGDA";
                data.BeginDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", oCL);
                //fld.Fieldname = "ENDDA";
                data.EndDate = DateTime.ParseExact(arrTemp[3], "yyyyMMdd", oCL);
                //fld.Fieldname = "SEQNR";
                data.SeqNo = arrTemp[4];
                //fld.Fieldname = "BEGUZ";
                if (arrTemp[5].Trim() != "")
                {
                    data.BeginTime = DateTime.ParseExact(arrTemp[5], "HHmmss", oCL).TimeOfDay;
                }
                else
                {
                    data.BeginTime = TimeSpan.Zero;
                }
                //fld.Fieldname = "ENDUZ";
                if (arrTemp[6].Trim() != "")
                {
                    if (arrTemp[6].Trim() == "240000")
                    {
                        data.EndTime = new TimeSpan(1, 0, 0, 0);
                    }
                    else
                    {
                        data.EndTime = DateTime.ParseExact(arrTemp[6], "HHmmss", oCL).TimeOfDay;
                    }
                }
                else
                {
                    data.EndTime = TimeSpan.Zero;
                }
                //fld.Fieldname = "ABWTG";
                data.AbsenceDays = Decimal.Parse(arrTemp[7]);
                //fld.Fieldname = "STDAZ";
                data.AbsenceHours = Decimal.Parse(arrTemp[8]);
                //fld.Fieldname = "ABRTG";
                data.PayrollDays = Decimal.Parse(arrTemp[9]);
                //fld.Fieldname = "ALLDF";
                data.AllDayFlag = arrTemp[10].Trim() == "X";
                oReturn.Add(data);
            }
            #endregion

            return oReturn;
        }
        #endregion

        #region " CalculateAbsAttEntry "
        private string TimeSpanToString(TimeSpan input)
        {
            if (input == TimeSpan.MinValue)
            {
                return "000000";
            }
            else
            {
                return string.Format("{0}{1}{2}", input.Hours.ToString("00"), input.Minutes.ToString("00"), input.Seconds.ToString("00"));
            }
        }

        private TimeSpan StringToTimeSpan(string input)
        {
            if (input.Trim() == "")
            {
                return TimeSpan.MinValue;
            }
            else
            {
                string cBuffer;
                if (input.Substring(0, 2) == "24")
                {
                    cBuffer = string.Format("{0}:{1}:{2}", "00", input.Substring(2, 2), input.Substring(4, 2));
                }
                else
                {
                    cBuffer = string.Format("{0}:{1}:{2}", input.Substring(0, 2), input.Substring(2, 2), input.Substring(4, 2));
                }
                return TimeSpan.Parse(cBuffer);
            }
        }

        public override AbsAttCalculateResult CalculateAbsAttEntry(string EmployeeID, string SubType, DateTime BeginDate, DateTime EndDate, TimeSpan BeginTime, TimeSpan EndTime, bool FullDay, bool CheckQuota, bool TruncateDayOff)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            AbsAttCalculateResult oReturn = new AbsAttCalculateResult();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            //TMInterface oFunction = new TMInterface();

            AbsAttCalculate oFunction = new AbsAttCalculate();

            oFunction.Connection = oConnection;

            string E_Msg = string.Empty;
            string E_Status = string.Empty;

            decimal absenceHours, absenceDays, payrollDays, calendarDays;
            string fullDay_out, prevDay_out, beginTime_out, endTime_out, begda_out, endda_out;

            string begintime_str = TimeSpanToString(BeginTime);
            string endtime_str = TimeSpanToString(EndTime);

            DateTime tmpBegin = BeginDate;
            DateTime tmpEnd = EndDate;

            if (endtime_str == "000000" && !FullDay)
            {
                endtime_str = "240000";
            }

            DateTime bufferDate;
            if (FullDay)
            {
                bufferDate = tmpEnd;
            }
            else
            {
                bufferDate = tmpBegin;

                EmployeeData oEmployeeData = new EmployeeData(EmployeeID, BeginDate) { CompanyCode = this.CompanyCode };
                DailyWS oDailyWS = oEmployeeData.GetDailyWorkSchedule(BeginDate, this.CompanyCode);

                if (oDailyWS.WorkBeginTime > oDailyWS.WorkEndTime)
                    if (oDailyWS.WorkBeginTime != BeginTime || oDailyWS.WorkEndTime != EndTime)
                        if (EndTime == oDailyWS.WorkEndTime)
                        {
                            tmpBegin = BeginDate.AddDays(1);
                            tmpEnd = EndDate.AddDays(1);
                            bufferDate = tmpBegin;
                        }

            }


            oFunction.Zhrtmi019(tmpBegin.ToString("yyyyMMdd", oCL), begintime_str,
                                                    CheckQuota ? "X" : "", bufferDate.ToString("yyyyMMdd", oCL), endtime_str,
                                                    EmployeeID, SubType,
                                                    TruncateDayOff ? "X" : "",
                                                    out payrollDays, out absenceDays,
                                                    out fullDay_out, out begda_out, out beginTime_out, out endda_out, out endTime_out,
                                                    out calendarDays, out E_Msg, out E_Status, out absenceHours, out prevDay_out);

            //oFunction.Zhrtmi007(tmpBegin.ToString("yyyyMMdd", oCL), begintime_str,
            //                                        CheckQuota ? "X" : "", bufferDate.ToString("yyyyMMdd", oCL), endtime_str,
            //                                        EmployeeID, SubType,
            //                                        TruncateDayOff ? "X" : "",
            //                                        out payrollDays, out absenceDays,
            //                                        out fullDay_out, out begda_out, out beginTime_out, out endda_out, out endTime_out,
            //                                        out calendarDays, out absenceHours, out prevDay_out);

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            if (E_Status == "E")
            {
                throw new Exception(E_Msg);
            }

            #region " Parse Object "
            oReturn.AbsenceDays = absenceDays;
            oReturn.AbsenceHours = absenceHours;
            oReturn.BeginTime = BeginTime;
            oReturn.EndTime = EndTime;
            oReturn.BeginDate = BeginDate;
            oReturn.EndDate = EndDate;
            oReturn.FullDay = fullDay_out.Trim() != "";
            oReturn.PayrollDays = payrollDays;
            oReturn.PrevDay = prevDay_out.Trim() != "";
            #endregion

            return oReturn;
        }

        public override AbsAttCalculateResult CalculateAbsAttEntry(string EmployeeID, string SubType, DateTime BeginDate, DateTime EndDate, TimeSpan BeginTime, TimeSpan EndTime, bool FullDay, bool CheckQuota, bool TruncateDayOff, string CompCode)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            AbsAttCalculateResult oReturn = new AbsAttCalculateResult();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            //TMInterface oFunction = new TMInterface();

            AbsAttCalculate oFunction = new AbsAttCalculate();

            oFunction.Connection = oConnection;

            string E_Msg = string.Empty;
            string E_Status = string.Empty;

            decimal absenceHours, absenceDays, payrollDays, calendarDays;
            string fullDay_out, prevDay_out, beginTime_out, endTime_out, begda_out, endda_out;

            string begintime_str = TimeSpanToString(BeginTime);
            string endtime_str = TimeSpanToString(EndTime);

            DateTime tmpBegin = BeginDate;
            DateTime tmpEnd = EndDate;

            if (endtime_str == "000000" && !FullDay)
            {
                endtime_str = "240000";
            }

            DateTime bufferDate;
            if (FullDay)
            {
                bufferDate = tmpEnd;
            }
            else
            {
                bufferDate = tmpBegin;

                EmployeeData oEmployeeData = new EmployeeData(EmployeeID, BeginDate) { CompanyCode = CompCode };
                DailyWS oDailyWS = oEmployeeData.GetDailyWorkSchedule(BeginDate, CompCode);

                if (oDailyWS.WorkBeginTime > oDailyWS.WorkEndTime)
                    if (oDailyWS.WorkBeginTime != BeginTime || oDailyWS.WorkEndTime != EndTime)
                        if (EndTime == oDailyWS.WorkEndTime)
                        {
                            tmpBegin = BeginDate.AddDays(1);
                            tmpEnd = EndDate.AddDays(1);
                            bufferDate = tmpBegin;
                        }

            }


            oFunction.Zhrtmi019(tmpBegin.ToString("yyyyMMdd", oCL), begintime_str,
                                                    CheckQuota ? "X" : "", bufferDate.ToString("yyyyMMdd", oCL), endtime_str,
                                                    EmployeeID, SubType,
                                                    TruncateDayOff ? "X" : "",
                                                    out payrollDays, out absenceDays,
                                                    out fullDay_out, out begda_out, out beginTime_out, out endda_out, out endTime_out,
                                                    out calendarDays, out E_Msg, out E_Status, out absenceHours, out prevDay_out);

            //oFunction.Zhrtmi007(tmpBegin.ToString("yyyyMMdd", oCL), begintime_str,
            //                                        CheckQuota ? "X" : "", bufferDate.ToString("yyyyMMdd", oCL), endtime_str,
            //                                        EmployeeID, SubType,
            //                                        TruncateDayOff ? "X" : "",
            //                                        out payrollDays, out absenceDays,
            //                                        out fullDay_out, out begda_out, out beginTime_out, out endda_out, out endTime_out,
            //                                        out calendarDays, out absenceHours, out prevDay_out);

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            if (E_Status == "E")
            {
                throw new Exception(E_Msg);
            }

            #region " Parse Object "
            oReturn.AbsenceDays = absenceDays;
            oReturn.AbsenceHours = absenceHours;
            oReturn.BeginTime = BeginTime;
            oReturn.EndTime = EndTime;
            oReturn.BeginDate = BeginDate;
            oReturn.EndDate = EndDate;
            oReturn.FullDay = fullDay_out.Trim() != "";
            oReturn.PayrollDays = payrollDays;
            oReturn.PrevDay = prevDay_out.Trim() != "";
            #endregion

            return oReturn;
        }

        public override AbsAttCalculateResult CalculateAbsAttEntry(string EmployeeID, string SubType, DateTime BeginDate, DateTime EndDate, TimeSpan BeginTime, TimeSpan EndTime, bool FullDay, bool CheckQuota, bool TruncateDayOff, DailyWS oDailyWS)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            AbsAttCalculateResult oReturn = new AbsAttCalculateResult();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            //TMInterface oFunction = new TMInterface();

            AbsAttCalculate oFunction = new AbsAttCalculate();

            oFunction.Connection = oConnection;

            string E_Msg = string.Empty;
            string E_Status = string.Empty;

            decimal absenceHours, absenceDays, payrollDays, calendarDays;
            string fullDay_out, prevDay_out, beginTime_out, endTime_out, begda_out, endda_out;

            string begintime_str = TimeSpanToString(BeginTime);
            string endtime_str = TimeSpanToString(EndTime);

            DateTime tmpBegin = BeginDate;
            DateTime tmpEnd = EndDate;

            if (endtime_str == "000000" && !FullDay)
            {
                endtime_str = "240000";
            }

            DateTime bufferDate;
            if (FullDay)
            {
                bufferDate = tmpEnd;
            }
            else
            {
                bufferDate = tmpBegin;

                EmployeeData oEmployeeData = new EmployeeData(EmployeeID, BeginDate);
                //DailyWS oDailyWS = oEmployeeData.GetDailyWorkSchedule(BeginDate);

                if (oDailyWS.WorkBeginTime > oDailyWS.WorkEndTime)
                    if (oDailyWS.WorkBeginTime != BeginTime || oDailyWS.WorkEndTime != EndTime)
                        if (EndTime == oDailyWS.WorkEndTime)
                        {
                            tmpBegin = BeginDate.AddDays(1);
                            tmpEnd = EndDate.AddDays(1);
                            bufferDate = tmpBegin;
                        }

            }

            //oFunction.Zhrtmi007(tmpBegin.ToString("yyyyMMdd", oCL), begintime_str,
            //                                        CheckQuota ? "X" : "", bufferDate.ToString("yyyyMMdd", oCL), endtime_str,
            //                                        EmployeeID, SubType,
            //                                        TruncateDayOff ? "X" : "",
            //                                        out payrollDays, out absenceDays,
            //                                        out fullDay_out, out begda_out, out beginTime_out, out endda_out, out endTime_out,
            //                                        out calendarDays, out absenceHours, out prevDay_out);

            oFunction.Zhrtmi019(tmpBegin.ToString("yyyyMMdd", oCL), begintime_str,
                                                   CheckQuota ? "X" : "", bufferDate.ToString("yyyyMMdd", oCL), endtime_str,
                                                   EmployeeID, SubType,
                                                   TruncateDayOff ? "X" : "",
                                                   out payrollDays, out absenceDays,
                                                   out fullDay_out, out begda_out, out beginTime_out, out endda_out, out endTime_out,
                                                   out calendarDays, out E_Msg, out E_Status, out absenceHours, out prevDay_out);


            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            if (E_Status == "E")
            {
                throw new Exception(E_Msg);
            }

            #region " Parse Object "
            oReturn.AbsenceDays = absenceDays;
            oReturn.AbsenceHours = absenceHours;
            oReturn.BeginTime = BeginTime;
            oReturn.EndTime = EndTime;
            oReturn.BeginDate = BeginDate;
            oReturn.EndDate = EndDate;
            oReturn.FullDay = fullDay_out.Trim() != "";
            oReturn.PayrollDays = payrollDays;
            oReturn.PrevDay = prevDay_out.Trim() != "";
            #endregion

            return oReturn;
        }
        #endregion

        #region " SaveAbsence "
        public override void SaveAbsence(List<INFOTYPE2001> data)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            UPLOADINFOTYPE6 oFunction = new UPLOADINFOTYPE6();
            oFunction.Connection = oConnection;

            ZHRHRS001Table Updatetab = new ZHRHRS001Table();
            ZHRHRS001Table Updatetab_Ret = new ZHRHRS001Table();

            #region " set item "
            for (int index = 0; index < data.Count; index++)
            {
                INFOTYPE2001 dataitem = data[index];
                dataitem.Remark = "";
                if (dataitem.IsPost)
                {
                    // if record have been post before ---> skip it
                    continue;
                }
                ZHRHRS001 item = new ZHRHRS001();
                item.Begda = dataitem.BeginDate.ToString("yyyyMMdd", oCL);
                item.Endda = dataitem.EndDate.ToString("yyyyMMdd", oCL);

                EmployeeData oEmployeeData = new EmployeeData(dataitem.EmployeeID, dataitem.BeginDate);
                DailyWS oDailyWS = oEmployeeData.GetDailyWorkSchedule(dataitem.BeginDate);

                if (oDailyWS.WorkBeginTime > oDailyWS.WorkEndTime && oDailyWS.WorkEndTime != TimeSpan.MinValue)
                    if ((oDailyWS.WorkBeginTime != dataitem.BeginTime || oDailyWS.WorkEndTime != dataitem.EndTime)
                        && dataitem.BeginTime < dataitem.EndTime)
                        if (dataitem.EndTime == oDailyWS.WorkEndTime)
                        {
                            item.Begda = dataitem.BeginDate.AddDays(1).ToString("yyyyMMdd", oCL);
                            item.Endda = dataitem.EndDate.AddDays(1).ToString("yyyyMMdd", oCL);
                        }


                item.Infty = dataitem.InfoType;
                item.Msg = "";
                item.Msgtype = "";
                item.Objps = "";
                if (dataitem.IsMark)
                {
                    item.Operation = "INS";
                }
                else
                {
                    item.Operation = "DEL";
                }
                item.Pernr = dataitem.EmployeeID;
                item.Reqnr = Guid.NewGuid().ToString();
                item.Seqnr = "";
                item.Status = "N";
                item.Subty = dataitem.SubType;
                if (!dataitem.AllDayFlag)
                {
                    DateTime oTemp;
                    oTemp = DateTime.MinValue.Add(dataitem.BeginTime);
                    // BEGUZ
                    item.T01 = oTemp.ToString("HHmm00");
                    // ENDUZ
                    oTemp = DateTime.MinValue.Add(dataitem.EndTime);
                    item.T02 = oTemp.ToString("HHmm00");
                }
                item.T03 = index.ToString();
                dataitem.IsPost = false;
                Updatetab.Add(item);
            }
            #endregion

            try
            {
                oFunction.Zhrhri001(ref Updatetab, ref Updatetab_Ret);
            }
            catch (Exception ex)
            {
                throw new Exception("POST DATA ABSENCE ERROR", ex);
            }
            finally
            {
                Connection.ReturnConnection(oConnection);
                oFunction.Dispose();
            }

            string errMsg = string.Empty;
            bool lError = false;
            foreach (ZHRHRS001 result in Updatetab_Ret)
            {
                int index = int.Parse(result.T03);
                if (result.Msgtype == "E")
                {
                    if (result.T20.Trim() == "009")
                    {
                        // create record in non working time set post = true and skip = true
                        lError = false;
                        data[index].IsPost = true;
                        data[index].IsSkip = true;
                    }
                    else if (result.T20.Trim() == "XXX" && data[index].IsMark == false)
                    {
                        // try to delete but not found, set post = false and skip = false
                        lError = false;
                        data[index].IsPost = true;
                        data[index].IsSkip = false;
                    }

                    //usedfor fix collision error only do not use in normally case
                    //----------------------------------------------------------------
                    //else if (result.Msg == "Insert cannot be made due to collision")
                    //{
                    //    lError = false;
                    //    data[index].IsPost = true;
                    //}
                    //----------------------------------------------------------------
                    else
                    {
                        lError = true;
                        data[index].IsPost = false;
                        data[index].IsSkip = false;
                    }
                    data[index].Remark = result.Msg;
                    errMsg += result.Msg;
                }
                else
                {
                    data[index].IsPost = true;
                    data[index].IsSkip = false;
                }
            }

            if (lError)
            {
                throw new Exception("Post data error " + errMsg);
            }

        }

        public override void SaveAbsence(List<INFOTYPE2001> data, string CompCode)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            UPLOADINFOTYPE6 oFunction = new UPLOADINFOTYPE6();
            oFunction.Connection = oConnection;

            ZHRHRS001Table Updatetab = new ZHRHRS001Table();
            ZHRHRS001Table Updatetab_Ret = new ZHRHRS001Table();

            #region " set item "
            for (int index = 0; index < data.Count; index++)
            {
                INFOTYPE2001 dataitem = data[index];
                dataitem.Remark = "";
                if (dataitem.IsPost)
                {
                    // if record have been post before ---> skip it
                    continue;
                }
                ZHRHRS001 item = new ZHRHRS001();
                item.Begda = dataitem.BeginDate.ToString("yyyyMMdd", oCL);
                item.Endda = dataitem.EndDate.ToString("yyyyMMdd", oCL);

                EmployeeData oEmployeeData = new EmployeeData(dataitem.EmployeeID, dataitem.BeginDate) { CompanyCode = CompCode };
                DailyWS oDailyWS = oEmployeeData.GetDailyWorkSchedule(dataitem.BeginDate, CompCode);

                if (oDailyWS.WorkBeginTime > oDailyWS.WorkEndTime && oDailyWS.WorkEndTime != TimeSpan.MinValue)
                    if ((oDailyWS.WorkBeginTime != dataitem.BeginTime || oDailyWS.WorkEndTime != dataitem.EndTime)
                        && dataitem.BeginTime < dataitem.EndTime)
                        if (dataitem.EndTime == oDailyWS.WorkEndTime)
                        {
                            item.Begda = dataitem.BeginDate.AddDays(1).ToString("yyyyMMdd", oCL);
                            item.Endda = dataitem.EndDate.AddDays(1).ToString("yyyyMMdd", oCL);
                        }


                item.Infty = dataitem.InfoType;
                item.Msg = "";
                item.Msgtype = "";
                item.Objps = "";
                if (dataitem.IsMark)
                {
                    item.Operation = "INS";
                }
                else
                {
                    item.Operation = "DEL";
                }
                item.Pernr = dataitem.EmployeeID;
                item.Reqnr = Guid.NewGuid().ToString();
                item.Seqnr = "";
                item.Status = "N";
                item.Subty = dataitem.SubType;
                if (!dataitem.AllDayFlag)
                {
                    DateTime oTemp;
                    oTemp = DateTime.MinValue.Add(dataitem.BeginTime);
                    // BEGUZ
                    item.T01 = oTemp.ToString("HHmm00");
                    // ENDUZ
                    oTemp = DateTime.MinValue.Add(dataitem.EndTime);
                    item.T02 = oTemp.ToString("HHmm00");
                }
                item.T03 = index.ToString();
                dataitem.IsPost = false;
                Updatetab.Add(item);
            }
            #endregion

            try
            {
                oFunction.Zhrhri001(ref Updatetab, ref Updatetab_Ret);
            }
            catch (Exception ex)
            {
                throw new Exception("POST DATA ABSENCE ERROR", ex);
            }
            finally
            {
                Connection.ReturnConnection(oConnection);
                oFunction.Dispose();
            }

            string errMsg = string.Empty;
            bool lError = false;
            foreach (ZHRHRS001 result in Updatetab_Ret)
            {
                int index = int.Parse(result.T03);
                if (result.Msgtype == "E")
                {
                    if (result.T20.Trim() == "009")
                    {
                        // create record in non working time set post = true and skip = true
                        lError = false;
                        data[index].IsPost = true;
                        data[index].IsSkip = true;
                    }
                    else if (result.T20.Trim() == "XXX" && data[index].IsMark == false)
                    {
                        // try to delete but not found, set post = false and skip = false
                        lError = false;
                        data[index].IsPost = true;
                        data[index].IsSkip = false;
                    }

                    //usedfor fix collision error only do not use in normally case
                    //----------------------------------------------------------------
                    //else if (result.Msg == "Insert cannot be made due to collision")
                    //{
                    //    lError = false;
                    //    data[index].IsPost = true;
                    //}
                    //----------------------------------------------------------------
                    else
                    {
                        lError = true;
                        data[index].IsPost = false;
                        data[index].IsSkip = false;
                    }
                    data[index].Remark = result.Msg;
                    errMsg += result.Msg;
                }
                else
                {
                    data[index].IsPost = true;
                    data[index].IsSkip = false;
                }
            }

            if (lError)
            {
                throw new Exception("Post data error " + errMsg);
            }

        }
        #endregion

        #endregion

        #region " Attendance "

        #region " GetAttendanceList "
        public override List<INFOTYPE2002> GetAttendanceList(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            List<INFOTYPE2002> list = this.GetInfotype2002Data(EmployeeID, EmployeeID, BeginDate, EndDate, false);
            return list;
        }
        #endregion

        #region " GetInfotype2002Data "
        private List<INFOTYPE2002> GetInfotype2002Data(string EmployeeID1, string EmployeeID2, DateTime BeginDate, DateTime EndDate, bool IsLoadOT)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            List<INFOTYPE2002> oReturn = new List<INFOTYPE2002>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;

            #region " Fields "
            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SUBTY";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SEQNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGUZ";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDUZ";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ABWTG";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "STDAZ";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ABRTG";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ALLDF";
            Fields.Add(fld);
            #endregion

            #region " Options "
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR >= '{0}' AND PERNR <= '{1}'", EmployeeID1, EmployeeID2);
            Options.Add(opt);
            if (IsLoadOT)
            {
                opt = new RFC_DB_OPT();
                opt.Text = string.Format("AND SUBTY = '5000'");
                Options.Add(opt);
            }
            else
            {
                opt = new RFC_DB_OPT();
                opt.Text = string.Format("AND SUBTY <> '5000'");
                Options.Add(opt);
            }
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND ENDDA >= '{0}' AND BEGDA <= '{1}'", BeginDate.ToString("yyyyMMdd", oCL), EndDate.ToString("yyyyMMdd", oCL));
            Options.Add(opt);
            #endregion

            oFunction.Zrfc_Read_Table("^", "", "PA2002", 0, 0, ref Data, ref Fields, ref Options);

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            #region " Parse Object "
            INFOTYPE2002 data;
            foreach (TAB512 item in Data)
            {
                data = new INFOTYPE2002();
                string[] arrTemp = item.Wa.Split('^');
                //fld.Fieldname = "PERNR";
                data.EmployeeID = arrTemp[0].Trim();
                //fld.Fieldname = "SUBTY";
                data.SubType = arrTemp[1].Trim();
                //fld.Fieldname = "BEGDA";
                data.BeginDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", oCL);
                //fld.Fieldname = "ENDDA";
                data.EndDate = DateTime.ParseExact(arrTemp[3], "yyyyMMdd", oCL);
                //fld.Fieldname = "SEQNR";
                data.SeqNo = arrTemp[4];
                //fld.Fieldname = "BEGUZ";
                if (arrTemp[5].Trim() != "")
                {
                    data.BeginTime = DateTime.ParseExact(arrTemp[5], "HHmmss", oCL).TimeOfDay;
                }
                else
                {
                    data.BeginTime = TimeSpan.Zero;
                }
                //fld.Fieldname = "ENDUZ";
                if (arrTemp[6].Trim() != "")
                {
                    if (arrTemp[6].Trim() == "240000")
                    {
                        data.EndTime = new TimeSpan(1, 0, 0, 0);
                    }
                    else
                    {
                        data.EndTime = DateTime.ParseExact(arrTemp[6], "HHmmss", oCL).TimeOfDay;
                    }
                }
                else
                {
                    data.EndTime = TimeSpan.Zero;
                }
                //fld.Fieldname = "ABWTG";
                data.AttendanceDays = Decimal.Parse(arrTemp[7]);
                //fld.Fieldname = "STDAZ";
                data.AttendanceHours = Decimal.Parse(arrTemp[8]);
                //fld.Fieldname = "ABRTG";
                data.PayrollDays = Decimal.Parse(arrTemp[9]);
                //fld.Fieldname = "ALLDF";
                data.AllDayFlag = arrTemp[10].Trim() == "X";
                oReturn.Add(data);
            }
            #endregion

            return oReturn;
        }
        #endregion

        #region " SaveAttendance "
        public override void SaveAttendance(List<INFOTYPE2002> data)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            UPLOADINFOTYPE6 oFunction = new UPLOADINFOTYPE6();
            oFunction.Connection = oConnection;

            ZHRHRS001Table Updatetab = new ZHRHRS001Table();
            ZHRHRS001Table Updatetab_Ret = new ZHRHRS001Table();

            #region " set item "
            for (int index = 0; index < data.Count; index++)
            {
                INFOTYPE2002 dataitem = data[index];
                if (dataitem.IsPost)
                {
                    continue;
                }
                ZHRHRS001 item = new ZHRHRS001();
                item.Begda = dataitem.BeginDate.ToString("yyyyMMdd", oCL);
                item.Endda = dataitem.EndDate.ToString("yyyyMMdd", oCL);

                EmployeeData oEmployeeData = new EmployeeData(dataitem.EmployeeID, dataitem.BeginDate);
                DailyWS oDailyWS = oEmployeeData.GetDailyWorkSchedule(dataitem.BeginDate);

                if (oDailyWS.WorkBeginTime > oDailyWS.WorkEndTime && oDailyWS.WorkEndTime != TimeSpan.MinValue)
                    if ((oDailyWS.WorkBeginTime != dataitem.BeginTime || oDailyWS.WorkEndTime != dataitem.EndTime)
                        && dataitem.BeginTime < dataitem.EndTime)
                        if (dataitem.EndTime == oDailyWS.WorkEndTime)
                        {
                            item.Begda = dataitem.BeginDate.AddDays(1).ToString("yyyyMMdd", oCL);
                            item.Endda = dataitem.EndDate.AddDays(1).ToString("yyyyMMdd", oCL);
                        }

                item.Infty = dataitem.InfoType;
                item.Msg = "";
                item.Msgtype = "";
                item.Objps = "";
                if (dataitem.IsMark)
                {
                    item.Operation = "INS";
                }
                else
                {
                    item.Operation = "DEL";
                }
                item.Pernr = dataitem.EmployeeID;
                item.Reqnr = Guid.NewGuid().ToString();
                item.Seqnr = "";
                item.Status = "N";
                item.Subty = dataitem.SubType;
                if (!dataitem.AllDayFlag)
                {
                    if (item.Operation == "DEL" && dataitem.BeginTime == TimeSpan.Zero && dataitem.EndTime == TimeSpan.Zero)
                    {
                        // BEGUZ
                        item.T01 = "";
                        // ENDUZ
                        item.T02 = "";
                    }
                    else
                    {
                        DateTime oTemp;
                        oTemp = DateTime.MinValue.Add(dataitem.BeginTime);
                        // BEGUZ
                        item.T01 = oTemp.ToString("HHmm00");
                        // ENDUZ
                        oTemp = DateTime.MinValue.Add(dataitem.EndTime);
                        item.T02 = oTemp.ToString("HHmm00");
                        if (item.T02 == "000000")
                        {
                            item.T02 = "240000";
                        }
                    }
                    item.T03 = dataitem.IsPrevDay ? "X" : "";
                }
                else
                {
                    // BEGUZ
                    item.T01 = "";
                    // ENDUZ
                    item.T02 = "";
                }
                item.T04 = dataitem.IONumber;
                item.T05 = dataitem.ActivityCode;
                item.T06 = index.ToString();
                dataitem.IsPost = true;
                Updatetab.Add(item);
            }
            #endregion

            try
            {
                oFunction.Zhrhri001(ref Updatetab, ref Updatetab_Ret);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            bool lError = false;
            string errorMessage = "";
            foreach (ZHRHRS001 result in Updatetab_Ret)
            {
                if (result.Msgtype == "E" || result.Msgtype == "W")
                {
                    int index = int.Parse(result.T06);
                    if (result.T20.Trim() == "XXX" && data[index].IsMark == false)
                    {
                        // try to delete but not found, set post = false and skip = false
                        lError = false;
                        data[index].IsPost = true;
                    }
                    else if (data[index].IsMark == false && result.Msg == "RECORD NOT FOUND" && result.Msgtype == "W")
                    {
                        // try to delete but not found, set post = false and skip = false
                        lError = false;
                        data[index].IsPost = true;
                    }

                    //usedfor fix collision error only do not use in normally case
                    //----------------------------------------------------------------
                    //else if (result.Msg == " Insert cannot be made due to collision")
                    //{
                    //    lError = false;
                    //    data[index].IsPost = true;
                    //}
                    //----------------------------------------------------------------
                    else
                    {
                        lError = true;
                        data[index].IsPost = false;
                    }
                    data[index].Remark = result.Msg;
                    errorMessage += string.Format("{0}\r\n", result.Msg);
                }
            }

            Exception exceptionForThrow = null;

            if (lError)
            {
                // try manual rollback;
                Updatetab = new ZHRHRS001Table();
                Updatetab_Ret = new ZHRHRS001Table();

                for (int index = 0; index < data.Count; index++)
                {
                    INFOTYPE2002 dataitem = data[index];
                    if (!dataitem.IsPost)
                    {
                        continue;
                    }
                    ZHRHRS001 item = new ZHRHRS001();
                    item.Begda = dataitem.BeginDate.ToString("yyyyMMdd", oCL);
                    item.Endda = dataitem.EndDate.ToString("yyyyMMdd", oCL);
                    item.Infty = dataitem.InfoType;
                    item.Msg = "";
                    item.Msgtype = "";
                    item.Objps = "";
                    if (dataitem.IsMark)
                    {
                        item.Operation = "DEL";
                    }
                    else
                    {
                        item.Operation = "INS";
                    }
                    item.Pernr = dataitem.EmployeeID;
                    item.Reqnr = Guid.NewGuid().ToString();
                    item.Seqnr = "";
                    item.Status = "N";
                    item.Subty = dataitem.SubType;
                    if (!dataitem.AllDayFlag)
                    {
                        DateTime oTemp;
                        oTemp = DateTime.MinValue.Add(dataitem.BeginTime);
                        // BEGUZ
                        item.T01 = oTemp.ToString("HHmm00");
                        // ENDUZ
                        oTemp = DateTime.MinValue.Add(dataitem.EndTime);
                        item.T02 = oTemp.ToString("HHmm00");
                        item.T03 = dataitem.IsPrevDay ? "X" : "";
                    }
                    item.T04 = index.ToString();
                    Updatetab.Add(item);
                }
                try
                {
                    oFunction.Zhrhri001(ref Updatetab, ref Updatetab_Ret);
                }
                catch (Exception ex)
                {
                    exceptionForThrow = new Exception("Post data error and can't rollback please contact admin to check data", ex);
                }
                exceptionForThrow = new Exception("Post data error and rollback completed : " + errorMessage + "\r\n");
            }

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            if (exceptionForThrow != null)
            {
                throw exceptionForThrow;
            }

        }

        public override void SaveAttendance(List<INFOTYPE2002> data, string CompCode)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            UPLOADINFOTYPE6 oFunction = new UPLOADINFOTYPE6();
            oFunction.Connection = oConnection;

            ZHRHRS001Table Updatetab = new ZHRHRS001Table();
            ZHRHRS001Table Updatetab_Ret = new ZHRHRS001Table();

            #region " set item "
            for (int index = 0; index < data.Count; index++)
            {
                INFOTYPE2002 dataitem = data[index];
                if (dataitem.IsPost)
                {
                    continue;
                }
                ZHRHRS001 item = new ZHRHRS001();
                item.Begda = dataitem.BeginDate.ToString("yyyyMMdd", oCL);
                item.Endda = dataitem.EndDate.ToString("yyyyMMdd", oCL);

                EmployeeData oEmployeeData = new EmployeeData(dataitem.EmployeeID, dataitem.BeginDate) { CompanyCode = CompCode };
                DailyWS oDailyWS = oEmployeeData.GetDailyWorkSchedule(dataitem.BeginDate, CompCode);

                if (oDailyWS.WorkBeginTime > oDailyWS.WorkEndTime && oDailyWS.WorkEndTime != TimeSpan.MinValue)
                    if ((oDailyWS.WorkBeginTime != dataitem.BeginTime || oDailyWS.WorkEndTime != dataitem.EndTime)
                        && dataitem.BeginTime < dataitem.EndTime)
                        if (dataitem.EndTime == oDailyWS.WorkEndTime)
                        {
                            item.Begda = dataitem.BeginDate.AddDays(1).ToString("yyyyMMdd", oCL);
                            item.Endda = dataitem.EndDate.AddDays(1).ToString("yyyyMMdd", oCL);
                        }

                item.Infty = dataitem.InfoType;
                item.Msg = "";
                item.Msgtype = "";
                item.Objps = "";
                if (dataitem.IsMark)
                {
                    item.Operation = "INS";
                }
                else
                {
                    item.Operation = "DEL";
                }
                item.Pernr = dataitem.EmployeeID;
                item.Reqnr = Guid.NewGuid().ToString();
                item.Seqnr = "";
                item.Status = "N";
                item.Subty = dataitem.SubType;
                if (!dataitem.AllDayFlag)
                {
                    if (item.Operation == "DEL" && dataitem.BeginTime == TimeSpan.Zero && dataitem.EndTime == TimeSpan.Zero)
                    {
                        // BEGUZ
                        item.T01 = "";
                        // ENDUZ
                        item.T02 = "";
                    }
                    else
                    {
                        DateTime oTemp;
                        oTemp = DateTime.MinValue.Add(dataitem.BeginTime);
                        // BEGUZ
                        item.T01 = oTemp.ToString("HHmm00");
                        // ENDUZ
                        oTemp = DateTime.MinValue.Add(dataitem.EndTime);
                        item.T02 = oTemp.ToString("HHmm00");
                        if (item.T02 == "000000")
                        {
                            item.T02 = "240000";
                        }
                    }
                    item.T03 = dataitem.IsPrevDay ? "X" : "";
                }
                else
                {
                    // BEGUZ
                    item.T01 = "";
                    // ENDUZ
                    item.T02 = "";
                }
                item.T04 = dataitem.IONumber;
                item.T05 = dataitem.ActivityCode;
                item.T06 = index.ToString();
                dataitem.IsPost = true;
                Updatetab.Add(item);
            }
            #endregion

            try
            {
                oFunction.Zhrhri001(ref Updatetab, ref Updatetab_Ret);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            bool lError = false;
            string errorMessage = "";
            foreach (ZHRHRS001 result in Updatetab_Ret)
            {
                if (result.Msgtype == "E" || result.Msgtype == "W")
                {
                    int index = int.Parse(result.T06);
                    if (result.T20.Trim() == "XXX" && data[index].IsMark == false)
                    {
                        // try to delete but not found, set post = false and skip = false
                        lError = false;
                        data[index].IsPost = true;
                    }
                    else if (data[index].IsMark == false && result.Msg == "RECORD NOT FOUND" && result.Msgtype == "W")
                    {
                        // try to delete but not found, set post = false and skip = false
                        lError = false;
                        data[index].IsPost = true;
                    }

                    //usedfor fix collision error only do not use in normally case
                    //----------------------------------------------------------------
                    //else if (result.Msg == " Insert cannot be made due to collision")
                    //{
                    //    lError = false;
                    //    data[index].IsPost = true;
                    //}
                    //----------------------------------------------------------------
                    else
                    {
                        lError = true;
                        data[index].IsPost = false;
                    }
                    data[index].Remark = result.Msg;
                    errorMessage += string.Format("{0}\r\n", result.Msg);
                }
            }

            Exception exceptionForThrow = null;

            if (lError)
            {
                // try manual rollback;
                Updatetab = new ZHRHRS001Table();
                Updatetab_Ret = new ZHRHRS001Table();

                for (int index = 0; index < data.Count; index++)
                {
                    INFOTYPE2002 dataitem = data[index];
                    if (!dataitem.IsPost)
                    {
                        continue;
                    }
                    ZHRHRS001 item = new ZHRHRS001();
                    item.Begda = dataitem.BeginDate.ToString("yyyyMMdd", oCL);
                    item.Endda = dataitem.EndDate.ToString("yyyyMMdd", oCL);
                    item.Infty = dataitem.InfoType;
                    item.Msg = "";
                    item.Msgtype = "";
                    item.Objps = "";
                    if (dataitem.IsMark)
                    {
                        item.Operation = "DEL";
                    }
                    else
                    {
                        item.Operation = "INS";
                    }
                    item.Pernr = dataitem.EmployeeID;
                    item.Reqnr = Guid.NewGuid().ToString();
                    item.Seqnr = "";
                    item.Status = "N";
                    item.Subty = dataitem.SubType;
                    if (!dataitem.AllDayFlag)
                    {
                        DateTime oTemp;
                        oTemp = DateTime.MinValue.Add(dataitem.BeginTime);
                        // BEGUZ
                        item.T01 = oTemp.ToString("HHmm00");
                        // ENDUZ
                        oTemp = DateTime.MinValue.Add(dataitem.EndTime);
                        item.T02 = oTemp.ToString("HHmm00");
                        item.T03 = dataitem.IsPrevDay ? "X" : "";
                    }
                    item.T04 = index.ToString();
                    Updatetab.Add(item);
                }
                try
                {
                    oFunction.Zhrhri001(ref Updatetab, ref Updatetab_Ret);
                }
                catch (Exception ex)
                {
                    exceptionForThrow = new Exception("Post data error and can't rollback please contact admin to check data", ex);
                }
                exceptionForThrow = new Exception("Post data error and rollback completed : " + errorMessage + "\r\n");
            }

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            if (exceptionForThrow != null)
            {
                throw exceptionForThrow;
            }

        }
        #endregion

        #endregion

        #region " Substitution "
        public override void SaveSubstitution(List<INFOTYPE2003> data)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            UPLOADINFOTYPE6 oFunction = new UPLOADINFOTYPE6();
            oFunction.Connection = oConnection;

            ZHRHRS001Table Updatetab = new ZHRHRS001Table();
            ZHRHRS001Table Updatetab_Ret = new ZHRHRS001Table();
            List<string> lstGuid = new List<string>(data.Count);
            #region " set item "
            for (int index = 0; index < data.Count; index++)
            {
                INFOTYPE2003 dataitem = data[index];
                dataitem.Remark = "";
                ZHRHRS001 item = new ZHRHRS001();
                item.Begda = dataitem.BeginDate.ToString("yyyyMMdd", oCL);
                item.Endda = dataitem.EndDate.ToString("yyyyMMdd", oCL);
                item.Infty = "2003";
                item.Msg = "";
                item.Msgtype = "";
                item.Objps = "";
                if (dataitem.IsMark)
                {
                    item.Operation = "INS";
                }
                else
                {
                    item.Operation = "DEL";
                }
                item.Pernr = dataitem.EmployeeID;
                item.Reqnr = Guid.NewGuid().ToString();
                item.Seqnr = "";
                item.Status = "N";
                item.Subty = "";
                item.T01 = dataitem.DWSCode;
                item.T02 = dataitem.DWSGroup;
                //item.T03 = index.ToString();
                //item.VPERN = dataitem.Substitute;  PersonalData
                lstGuid.Add(item.Reqnr);
                Updatetab.Add(item);
            }
            #endregion

            try
            {
                oFunction.Zhrhri001(ref Updatetab, ref Updatetab_Ret);
            }
            catch (Exception ex)
            {
                throw new Exception("POST DATA SUBSTITUTION ERROR", ex);
            }
            finally
            {
                Connection.ReturnConnection(oConnection);
                oFunction.Dispose();
            }

            bool lError = false;
            foreach (ZHRHRS001 result in Updatetab_Ret)
            {
                if (result.Msgtype == "E")
                {
                    int index = lstGuid.IndexOf(result.Reqnr);
                    data[index].Remark = result.Msg;
                    lError = true;
                }
            }

            lstGuid.Clear();
            lstGuid = null;

            if (lError)
            {
                throw new Exception("Post data error");
            }
        }
        public override void SaveSubstitutionCustomTime(INFOTYPE2003 data)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            UPLOADINFOTYPE6 oFunction = new UPLOADINFOTYPE6();
            oFunction.Connection = oConnection;

            ZHRHRS001Table Updatetab = new ZHRHRS001Table();
            ZHRHRS001Table Updatetab_Ret = new ZHRHRS001Table();

            #region " set item "
            data.Remark = "";
            ZHRHRS001 item = new ZHRHRS001();
            item.Begda = data.BeginDate.ToString("yyyyMMdd", oCL);
            item.Endda = data.EndDate.ToString("yyyyMMdd", oCL);
            item.Infty = "2003";
            item.Msg = "";
            item.Msgtype = "";
            item.Objps = "";
            if (data.IsMark)
            {
                item.Operation = "INS";
            }
            else
            {
                item.Operation = "DEL";
            }
            item.Pernr = data.EmployeeID;
            item.Reqnr = Guid.NewGuid().ToString();
            item.Seqnr = "";
            item.Status = "N";
            item.Subty = "";
            DateTime oTemp;
            oTemp = DateTime.MinValue.Add(data.SubstituteBeginTime);
            item.T03 = oTemp.ToString("HHmm00");

            oTemp = DateTime.MinValue.Add(data.SubstituteEndTime);
            item.T04 = oTemp.ToString("HHmm00");
            Updatetab.Add(item);

            #endregion

            try
            {
                oFunction.Zhrhri001(ref Updatetab, ref Updatetab_Ret);
            }
            catch (Exception ex)
            {
                throw new Exception("POST DATA SUBSTITUTION ERROR", ex);
            }
            finally
            {
                Connection.ReturnConnection(oConnection);
                oFunction.Dispose();
            }

            bool lError = false;
            foreach (ZHRHRS001 result in Updatetab_Ret)
            {
                if (result.Msgtype == "E")
                {
                    data.Remark = result.Msg;
                    lError = true;
                }
            }

            if (lError)
            {
                throw new Exception("Post data error");
            }
        }

        //AddBy: Ratchatawan W. (2012-11-16)
        public override void DeleteSubstitution(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            List<INFOTYPE2003> list = new List<INFOTYPE2003>();
            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;
            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_FLD fld;

            #region " SetField "
            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SUBTY";
            Fields.Add(fld);
            #endregion

            #region " SetOption "
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_OPT opt;
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR = '{0}'", EmployeeID);
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND ( BEGDA <= '{1}' AND ENDDA >= '{0}' )", BeginDate.ToString("yyyyMMdd", oCL), EndDate.ToString("yyyyMMdd", oCL));
            Options.Add(opt);
            #endregion

            #region " Execute "
            oFunction.Rfc_Read_Table("^", "", "PA2003", 0, 0, ref Data, ref Fields, ref Options);
            SAPConnection.ReturnConnection(oConnection);
            #endregion

            oFunction.Dispose();

            //Delete from SAP with get data
            UPLOADINFOTYPE6 oFunctionUPLOAD = new UPLOADINFOTYPE6();
            oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            oFunctionUPLOAD.Connection = oConnection;

            ZHRHRS001Table Updatetab = new ZHRHRS001Table();
            ZHRHRS001Table Updatetab_Ret = new ZHRHRS001Table();

            #region " set item "
            foreach (TAB512 itemTAB512 in Data)
            {
                string[] arrTemp = itemTAB512.Wa.Split('^');

                ZHRHRS001 item = new ZHRHRS001();
                item.Pernr = arrTemp[0].Trim();
                item.Begda = arrTemp[1];
                item.Endda = arrTemp[2];
                item.Infty = "2003";
                item.Subty = arrTemp[3];
                item.Msg = "";
                item.Msgtype = "";
                item.Objps = "";
                item.Operation = "DEL";
                Updatetab.Add(item);
            }
            #endregion

            try
            {
                oFunctionUPLOAD.Zhrhri001(ref Updatetab, ref Updatetab_Ret);
            }
            catch (Exception ex)
            {
                throw new Exception("POST DATA SUBSTITUTION ERROR", ex);
            }
            finally
            {
                Connection.ReturnConnection(oConnection);
                oFunctionUPLOAD.Dispose();
            }

            bool lError = false;
            string ErrorMsg = string.Empty;
            foreach (ZHRHRS001 result in Updatetab_Ret)
            {
                if (result.Msgtype == "E")
                {
                    ErrorMsg = result.Msg;
                    lError = true;
                }
            }

            if (lError)
            {
                throw new Exception("Post data error", new Exception(ErrorMsg));
            }
        }
        #endregion

        #region " OT "

        #region " LoadOTData "
        public override List<SaveOTData> LoadOTData(string EmployeeID, DateTime BeginDate, DateTime EndDate, string OnlyType, bool OnlyCompleted, bool OnlyNotSummary)
        {
            List<SaveOTData> oReturn = new List<SaveOTData>();
            if (OnlyType != "P")
            {
                List<INFOTYPE2002> oAtten = GetInfotype2002Data(EmployeeID, EmployeeID, BeginDate, EndDate, true);
                foreach (INFOTYPE2002 attendance in oAtten)
                {
                    SaveOTData item = new SaveOTData();
                    item.BeginTime = attendance.BeginTime;
                    item.EmployeeID = attendance.EmployeeID;
                    item.EndTime = attendance.EndTime;
                    item.IsCompleted = true;
                    item.OTDate = attendance.BeginDate;
                    item.OTHours = attendance.AttendanceHours;
                    item.RequestNo = "SAP Recorded";
                    oReturn.Add(item);
                }
            }
            return oReturn;
        }
        #endregion

        #region " SaveOTList "
        public override void SaveOTList(List<SaveOTData> data, bool IsMark, List<string> requestNoList, bool deleteItemNotInList)
        {
            List<INFOTYPE2002> list = new List<INFOTYPE2002>();
            MonthlyWS MWS = null;

            for (int index = 0; index < data.Count; index++)
            {
                SaveOTData item = data[index];
                if (!item.IsSummary)
                {
                    continue;
                }
                if (MWS == null)
                {
                    MWS = MonthlyWS.GetCalendar(item.EmployeeID, item.OTDate.Year, item.OTDate.Month);
                }
                INFOTYPE2002 inf = new INFOTYPE2002();
                inf.AllDayFlag = false;
                inf.AttendanceType = "5000";
                inf.EmployeeID = item.EmployeeID;
                inf.BeginDate = item.OTDate;
                inf.EndDate = item.OTDate;
                inf.BeginTime = item.BeginTime;
                inf.EndTime = item.EndTime;
                inf.IsMark = IsMark;
                inf.IsPost = false;
                inf.IsPrevDay = item.IsPrevDay;
                inf.SeqNo = index.ToString();
                inf.IONumber = item.IONumber;
                inf.ActivityCode = item.ActivityCode;
                list.Add(inf);
                string dwsCode = MWS.GetValuationClassExactly(item.OTDate.Day);
                if (item.DoNotPayShiftAllowance && !(PlanTimeManagement.IsNORM(dwsCode) || PlanTimeManagement.IsTNOM(dwsCode)))
                {
                    inf = new INFOTYPE2002();
                    inf.AllDayFlag = false;
                    inf.AttendanceType = "5500";
                    inf.EmployeeID = item.EmployeeID;
                    inf.BeginDate = item.OTDate;
                    inf.EndDate = item.OTDate;
                    inf.BeginTime = item.BeginTime;
                    inf.EndTime = item.EndTime;
                    inf.IONumber = item.IONumber;
                    inf.ActivityCode = item.ActivityCode;
                    inf.IsMark = IsMark;
                    inf.IsPost = false;
                    inf.IsPrevDay = item.IsPrevDay;
                    inf.SeqNo = index.ToString();
                    list.Add(inf);
                }
            }
            try
            {
                SaveAttendance(list);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                for (int index = 0; index < list.Count; index++)
                {
                    if (list[index].SeqNo != "")
                    {
                        int myIndex = int.Parse(list[index].SeqNo);
                        if (!list[index].IsPost)
                        {
                            data[myIndex].IsPost = false;
                            data[myIndex].Remark = list[index].Remark;
                            break;
                        }
                        else
                        {
                            data[myIndex].IsPost = true;
                        }
                    }
                }
            }
        }
        #endregion


        public override void PostDutyPaymentDayOff(string strEmployeeID, Dictionary<DateTime, decimal> dictData)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            UPLOADINFOTYPE6 oFunction = new UPLOADINFOTYPE6();
            oFunction.Connection = oConnection;

            ZHRHRS001Table Updatetab = new ZHRHRS001Table();
            ZHRHRS001Table Updatetab_Ret = new ZHRHRS001Table();
            ZHRHRS001 item;
            #region " set item "
            DateTime dtKey = DateTime.MinValue;
            decimal dValue;
            DateTime[] arrKey = new DateTime[dictData.Count];
            dictData.Keys.CopyTo(arrKey, 0);
            decimal[] arrValue = new decimal[dictData.Count];
            dictData.Values.CopyTo(arrValue, 0);
            for (int index = 0; index < dictData.Count; index++)
            {
                dtKey = arrKey[index];
                dValue = arrValue[index];

                item = new ZHRHRS001();
                item.Begda = dtKey.ToString("yyyyMMdd", oCL);
                item.Endda = dtKey.ToString("yyyyMMdd", oCL);
                item.Infty = "2010";
                item.Msg = "";
                item.Msgtype = "";
                item.Objps = "";
                item.Operation = "INS";
                item.Pernr = strEmployeeID;
                item.Reqnr = Guid.NewGuid().ToString();
                item.Seqnr = "";
                item.Status = "N";
                item.Subty = "2040";
                item.T03 = dValue.ToString("0.00");
                item.T04 = "010";
                Updatetab.Add(item);
            }
            #endregion

            try
            {
                oFunction.Zhrhri001(ref Updatetab, ref Updatetab_Ret);
            }
            catch (Exception ex)
            {
                throw new Exception("POST DATA OT ERROR", ex);
            }
            finally
            {
                Connection.ReturnConnection(oConnection);
                oFunction.Dispose();
            }

            bool lError = false;
            ZHRHRS001 result;
            for (int i = 0; i < Updatetab_Ret.Count; i++)
            {
                result = Updatetab_Ret[i];
                if (result.Msgtype == "E")
                {
                    lError = true;
                }
            }

            if (lError)
            {
                throw new Exception("Post data error");
            }
        }

        public override void PostDutyPaymentDayWork(string strEmployeeID, Dictionary<DateTime, decimal> dictData)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            UPLOADINFOTYPE6 oFunction = new UPLOADINFOTYPE6();
            oFunction.Connection = oConnection;

            ZHRHRS001Table Updatetab = new ZHRHRS001Table();
            ZHRHRS001Table Updatetab_Ret = new ZHRHRS001Table();
            ZHRHRS001 item;
            #region " set item "
            DateTime dtKey = DateTime.MinValue;
            decimal dValue;
            DateTime[] arrKey = new DateTime[dictData.Count];
            dictData.Keys.CopyTo(arrKey, 0);
            decimal[] arrValue = new decimal[dictData.Count];
            dictData.Values.CopyTo(arrValue, 0);
            for (int index = 0; index < dictData.Count; index++)
            {
                dtKey = arrKey[index];
                dValue = arrValue[index];

                item = new ZHRHRS001();
                item.Begda = dtKey.ToString("yyyyMMdd", oCL);
                item.Endda = dtKey.ToString("yyyyMMdd", oCL);
                item.Infty = "2010";
                item.Msg = "";
                item.Msgtype = "";
                item.Objps = "";
                item.Operation = "INS";
                item.Pernr = strEmployeeID;
                item.Reqnr = Guid.NewGuid().ToString();
                item.Seqnr = "";
                item.Status = "N";
                item.Subty = "2035";
                item.T03 = dValue.ToString("0.00");
                item.T04 = "010";
                Updatetab.Add(item);
            }
            #endregion

            try
            {
                oFunction.Zhrhri001(ref Updatetab, ref Updatetab_Ret);
            }
            catch (Exception ex)
            {
                throw new Exception("POST DATA OT ERROR", ex);
            }
            finally
            {
                Connection.ReturnConnection(oConnection);
                oFunction.Dispose();
                arrKey = null;
                arrValue = null;
            }

            bool lError = false;
            ZHRHRS001 result;
            for (int i = 0; i < Updatetab_Ret.Count; i++)
            {
                result = Updatetab_Ret[i];
                if (result.Msgtype == "E")
                {
                    lError = true;
                }
            }

            if (lError)
            {
                throw new Exception("Post data error");
            }
        }

        public override List<TimeEval> LoadTimeEval(DateTime BeginDate, DateTime EndDate)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            TMInterface oFunction = new TMInterface();
            oFunction.Connection = oConnection;

            int Record = 0;
            SAPInterface.BAPIRET2 bAPIRET2 = new SAPInterface.BAPIRET2();
            ZHRTMS001Table zHRTMS001Table = new ZHRTMS001Table();
            ZHRTMS002Table zHRTMS002Table = new ZHRTMS002Table();
            ZHRTMS003Table zHRTMS003Table = new ZHRTMS003Table();

            oFunction.Zhrtmi014(BeginDate.ToString("yyyyMMdd", oCL), EndDate.ToString("yyyyMMdd", oCL), out Record, out bAPIRET2, ref zHRTMS002Table, ref zHRTMS001Table, ref zHRTMS003Table);

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            DataTable dtResult = zHRTMS003Table.ToADODataTable();
            DateTime dtTemp = new DateTime();
            List<TimeEval> Result = new List<TimeEval>();
            foreach (DataRow dr in dtResult.Rows)
            {
                TimeEval item = new TimeEval();
                item.EmployeeID = dr[0].ToString();

                dtTemp = DateTime.ParseExact(dr[1].ToString(), "yyyyMMdd", oCL);
                dtTemp = dtTemp.Add(new TimeSpan(int.Parse(dr[2].ToString().Substring(0, 2)), int.Parse(dr[2].ToString().Substring(2, 2)), int.Parse(dr[2].ToString().Substring(4, 2))));
                item.BeginDate = dtTemp;

                dtTemp = DateTime.ParseExact(dr[1].ToString(), "yyyyMMdd", oCL);
                dtTemp = dtTemp.Add(new TimeSpan(int.Parse(dr[3].ToString().Substring(0, 2)), int.Parse(dr[3].ToString().Substring(2, 2)), int.Parse(dr[3].ToString().Substring(4, 2))));
                item.EndDate = dtTemp;

                item.WageType = dr[4].ToString();
                item.WageHour = Decimal.Parse(dr[5].ToString());
                Result.Add(item);
            }

            return Result;
        }

        public override void PostTimePair(List<TimePair> Data)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            UPLOADINFOTYPE6 oFunction = new UPLOADINFOTYPE6();
            oFunction.Connection = oConnection;

            ZHRHRS001Table Updatetab = new ZHRHRS001Table();
            ZHRHRS001Table Updatetab_Ret = new ZHRHRS001Table();
            TimePair pair;
            String DayAssignment = String.Empty;
            for (int index = 0; index < Data.Count; index++)
            {
                DayAssignment = String.Empty;
                pair = Data[index];
                pair.Remark = "";
                if (pair.ClockIN != null && !pair.PostIN)
                {
                    #region " CLOCKIN "
                    ZHRHRS001 item = new ZHRHRS001();
                    item.Begda = pair.Date.ToString("yyyyMMdd", oCL);//date
                    item.Endda = pair.Date.ToString("yyyyMMdd", oCL); //date

                    item.Infty = "2011";
                    item.Msg = "";
                    item.Msgtype = "";
                    item.Objps = "";
                    item.Operation = "INS";
                    item.Pernr = pair.EmployeeID;
                    item.Reqnr = Guid.NewGuid().ToString();
                    item.Seqnr = "";
                    item.Status = "N";
                    item.Subty = "";
                    item.T01 = pair.ClockIN.EventTime.TimeOfDay == new TimeSpan() ? "240000" : pair.ClockIN.EventTime.ToString("HHmmss");  //time
                    item.T02 = "P10"; //time event type
                    item.T03 = DayAssignment = pair.ClockIN.EventTime.ToString("ddMMyyyy") != pair.Date.ToString("ddMMyyyy") ? "-" : string.Empty;//day assignment    
                    item.T04 = ""; //pair.ClockIN.DoorType == DoorType.IN_OT ? "8" : String.Empty;
                    //mark seqno
                    item.T05 = pair.ClockIN.DoorType == DoorType.IN_OT ? "8" : String.Empty;
                    item.T06 = index.ToString();

                    Updatetab.Add(item);
                    #endregion
                }
                if (pair.ClockOUT != null && !pair.PostOUT)
                {
                    #region " ClockOUT "
                    ZHRHRS001 item = new ZHRHRS001();
                    item.Begda = pair.ClockOUT.EventTime.ToString("yyyyMMdd", oCL);
                    item.Endda = pair.ClockOUT.EventTime.ToString("yyyyMMdd", oCL);
                    item.Infty = "2011";
                    item.Msg = "";
                    item.Msgtype = "";
                    item.Objps = "";
                    item.Operation = "INS";
                    item.Pernr = pair.EmployeeID;
                    item.Reqnr = Guid.NewGuid().ToString();
                    item.Seqnr = "";
                    item.Status = "N";
                    item.Subty = "";
                    item.T01 = pair.ClockOUT.EventTime.TimeOfDay == new TimeSpan() ? "240000" : pair.ClockOUT.EventTime.ToString("HHmmss");
                    item.T02 = "P20";
                    item.T03 = DayAssignment;
                    item.T04 = "";// pair.ClockOUT.DoorType == DoorType.OUT_OT ? "9" : String.Empty;
                    //mark seqno
                    item.T05 = pair.ClockOUT.DoorType == DoorType.OUT_OT ? "9" : String.Empty;
                    item.T06 = index.ToString();

                    Updatetab.Add(item);
                    #endregion
                }
                pair.PostIN = true;
                pair.PostOUT = true;
            }

            oFunction.Zhrhri001(ref Updatetab, ref Updatetab_Ret);

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            DataTable oResult = Updatetab_Ret.ToADODataTable();
            DataView oDV = new DataView(oResult);
            bool lError;
            oDV.RowFilter = "MSGTYPE = 'E'";
            lError = oDV.Count > 0;
            foreach (DataRowView drv in oDV)
            {
                int index = int.Parse((string)drv["T06"]);
                bool isClockIN = ((string)drv["T02"]).Trim() == "P10";
                if (isClockIN)
                {
                    Data[index].PostIN = false;
                }
                else
                {
                    Data[index].PostOUT = false;
                }
                Data[index].Remark += ((string)drv["MSG"]).Trim() + ", ";
            }
            oDV.Dispose();
            oResult.Dispose();
            if (lError)
            {
                string strError = string.Empty;
                foreach (TimePair tp in Data)
                    if (!string.IsNullOrEmpty(tp.Remark))
                        strError += string.Format("{0} {1}\r\n", tp.EmployeeID, tp.Remark);
                throw new TimePairPostingException("Can't post time event\r\n" + strError);
            }
        }

        public override void PostOTLog(List<DailyOTLog> oDailyOTLog)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            //DateTime PostingDate = DateTime.ParseExact("20120701", "yyyyMMdd", oCL);
            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            UPLOADINFOTYPE6 oFunction = new UPLOADINFOTYPE6();
            oFunction.Connection = oConnection;

            ZHRHRS001Table Updatetab = new ZHRHRS001Table();
            ZHRHRS001Table Updatetab_Ret = new ZHRHRS001Table();
            ZHRHRS001 item;
            List<int> map = new List<int>();
            #region " set item "
            DailyOTLog dataitem;
            for (int index = 0; index < oDailyOTLog.Count; index++)
            {
                dataitem = oDailyOTLog[index];
                if (!dataitem.IsPayroll) continue;
                dataitem.Remark = "";

                // OT x1
                if (dataitem.FinalOTHour10 > 0)
                {
                    item = new ZHRHRS001();
                    item.Begda = dataitem.BeginDate.ToString("yyyyMMdd", oCL);
                    item.Endda = dataitem.EndDate.ToString("yyyyMMdd", oCL);
                    item.Infty = "2010";
                    item.Msg = "";
                    item.Msgtype = "";
                    item.Objps = "";
                    item.Operation = "INS";
                    item.Pernr = dataitem.EmployeeID;
                    item.Reqnr = Guid.NewGuid().ToString();
                    item.Seqnr = "";
                    item.Status = "N";
                    item.Subty = "2010";
                    item.T03 = TimeSpan.FromMinutes(Convert.ToDouble(dataitem.FinalOTHour10)).TotalHours.ToString("0.00");
                    item.T04 = "001";  
                    if (!String.IsNullOrEmpty(dataitem.OTWorkTypeID))
                        item.T05 = dataitem.OTWorkTypeID;
                    Updatetab.Add(item);
                    map.Add(new int());
                    map[map.Count - 1] = index;
                }

                // OTx1.5
                if (dataitem.FinalOTHour15 > 0)
                {
                    item = new ZHRHRS001();
                    item.Begda = dataitem.BeginDate.ToString("yyyyMMdd", oCL);
                    item.Endda = dataitem.EndDate.ToString("yyyyMMdd", oCL);
                    item.Infty = "2010";
                    item.Msg = "";
                    item.Msgtype = "";
                    item.Objps = "";
                    item.Operation = "INS";
                    item.Pernr = dataitem.EmployeeID;
                    item.Reqnr = Guid.NewGuid().ToString();
                    item.Seqnr = "";
                    item.Status = "N";
                    item.Subty = "2015";
                    item.T03 = TimeSpan.FromMinutes(Convert.ToDouble(dataitem.FinalOTHour15)).TotalHours.ToString("0.00");
                    item.T04 = "001";  
                    if (!String.IsNullOrEmpty(dataitem.OTWorkTypeID))
                        item.T05 = dataitem.OTWorkTypeID;
                    Updatetab.Add(item);
                    map.Add(new int());
                    map[map.Count - 1] = index;
                }

                // OT x3
                if (dataitem.FinalOTHour30 > 0)
                {
                    item = new ZHRHRS001();
                    item.Begda = dataitem.BeginDate.ToString("yyyyMMdd", oCL);
                    item.Endda = dataitem.EndDate.ToString("yyyyMMdd", oCL);
                    item.Infty = "2010";
                    item.Msg = "";
                    item.Msgtype = "";
                    item.Objps = "";
                    item.Operation = "INS";
                    item.Pernr = dataitem.EmployeeID;
                    item.Reqnr = Guid.NewGuid().ToString();
                    item.Seqnr = "";
                    item.Status = "N";
                    item.Subty = "2030";
                    item.T03 = TimeSpan.FromMinutes(Convert.ToDouble(dataitem.FinalOTHour30)).TotalHours.ToString("0.00");
                    item.T04 = "001";  
                    if (!String.IsNullOrEmpty(dataitem.OTWorkTypeID))
                        item.T05 = dataitem.OTWorkTypeID;
                    Updatetab.Add(item);
                    map.Add(new int());
                    map[map.Count - 1] = index;
                }
            }
            #endregion

            try
            {
                oFunction.Zhrhri001(ref Updatetab, ref Updatetab_Ret);
            }
            catch (Exception ex)
            {
                throw new Exception("POST DATA OT ERROR", ex);
            }
            finally
            {
                Connection.ReturnConnection(oConnection);
                oFunction.Dispose();
            }

            bool lError = false;
            string errMsg = string.Empty;
            ZHRHRS001 result;
            for (int i = 0; i < Updatetab_Ret.Count; i++)
            {
                result = Updatetab_Ret[i];
                if (result.Msgtype == "E")
                {
                    oDailyOTLog[map[i]].Remark = result.Msg;
                    errMsg += result.Msg;
                    lError = true;
                }
            }

            if (lError)
            {
                throw new Exception("Post data error : " + errMsg);
            }
        }

        #endregion     
    }
}
