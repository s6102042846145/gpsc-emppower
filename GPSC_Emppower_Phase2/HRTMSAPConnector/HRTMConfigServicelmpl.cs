using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.HR.TM.SAP
{
    class HRTMConfigServiceImpl
    {

        //#region " GetAllAbsenceTypeList "
        //public override List<AbsenceType> GetAllAbsenceTypeList()
        //{
        //    List<AbsenceType> oReturn = new List<AbsenceType>();

        //    Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
        //    RFCREADTABLE6 oFunction = new RFCREADTABLE6();
        //    oFunction.Connection = oConnection;

        //    TAB512Table Data = new TAB512Table();
        //    RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
        //    RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

        //    RFC_DB_FLD fld;
        //    RFC_DB_OPT opt;

        //    #region " Fields "
        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "MOABW";
        //    Fields.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "SUBTY";
        //    Fields.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "MUNIT";
        //    Fields.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "DEDQU";
        //    Fields.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "CRULE";
        //    Fields.Add(fld);
        //    #endregion

        //    #region " Options "
        //    opt = new RFC_DB_OPT();
        //    opt.Text = string.Format("KLZBI = 2");
        //    Options.Add(opt);
        //    #endregion

        //    oFunction.Zrfc_Read_Table("^", "", "T554S", 0, 0, ref Data, ref Fields, ref Options);

        //    TAB512Table Data1 = new TAB512Table();
        //    RFC_DB_FLDTable Fields1 = new RFC_DB_FLDTable();
        //    RFC_DB_OPTTable Options1 = new RFC_DB_OPTTable();

        //    #region " Fields "
        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "MOABW";
        //    Fields1.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "AWART";
        //    Fields1.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "ATEXT";
        //    Fields1.Add(fld);
        //    #endregion

        //    #region " Options "
        //    opt = new RFC_DB_OPT();
        //    opt.Text = string.Format("SPRSL = 2");
        //    Options1.Add(opt);
        //    #endregion

        //    oFunction.Zrfc_Read_Table("^", "", "T554T", 0, 0, ref Data1, ref Fields1, ref Options1);

        //    Dictionary<string, string> listText = new Dictionary<string, string>();
        //    foreach (TAB512 data in Data1)
        //    {
        //        string[] arrTemp = data.Wa.Split('^');
        //        listText.Add(string.Format("{0}#{1}", arrTemp[0].Trim(), arrTemp[1].Trim()), arrTemp[2].Trim());
        //    }

        //    #region " ParseObject "
        //    foreach (TAB512 data in Data)
        //    {
        //        string[] arrTemp = data.Wa.Split('^');
        //        AbsenceType Item = new AbsenceType();
        //        Item.AbsAttGrouping = arrTemp[0].Trim();
        //        Item.Key = arrTemp[1].Trim();
        //        Item.QuotaDayCount = arrTemp[2].Trim();
        //        Item.IsUseQuota = arrTemp[3].Trim() == "X";
        //        Item.CountingRule = arrTemp[4].Trim();
        //        if (listText.ContainsKey(string.Format("{0}#{1}", Item.AbsAttGrouping, Item.Key)))
        //        {
        //            Item.Description = listText[string.Format("{0}#{1}", Item.AbsAttGrouping, Item.Key)];
        //        }
        //        oReturn.Add(Item);
        //    }
        //    #endregion

        //    Connection.ReturnConnection(oConnection);
        //    oFunction.Dispose();

        //    return oReturn;
        //}
        //#endregion

        //#region " GetAbsenceQuotaTypeList "
        //public override List<AbsenceQuotaType> GetAbsenceQuotaTypeList(string LanguageCode)
        //{
        //    List<AbsenceQuotaType> oReturn = new List<AbsenceQuotaType>();

        //    Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
        //    RFCREADTABLE6 oFunction = new RFCREADTABLE6();
        //    oFunction.Connection = oConnection;

        //    TAB512Table Data = new TAB512Table();
        //    RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
        //    RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

        //    RFC_DB_FLD fld;
        //    RFC_DB_OPT opt;

        //    #region " Fields "
        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "MOPGK";
        //    Fields.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "MOZKO";
        //    Fields.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "KTART";
        //    Fields.Add(fld);
        //    #endregion

        //    #region " Options "
        //    opt = new RFC_DB_OPT();
        //    opt.Text = string.Format("BEGDA <= '{0}' AND ENDDA >= '{0}'", DateTime.Now.ToString("yyyyMMdd", oCL_ENUS));
        //    Options.Add(opt);
        //    #endregion

        //    oFunction.Zrfc_Read_Table("^", "", "T556A", 0, 0, ref Data, ref Fields, ref Options);

        //    TAB512Table Data1 = new TAB512Table();
        //    RFC_DB_FLDTable Fields1 = new RFC_DB_FLDTable();
        //    RFC_DB_OPTTable Options1 = new RFC_DB_OPTTable();

        //    #region " Fields "
        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "MOPGK";
        //    Fields1.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "MOZKO";
        //    Fields1.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "KTART";
        //    Fields1.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "KTEXT";
        //    Fields1.Add(fld);
        //    #endregion

        //    #region " Options "
        //    opt = new RFC_DB_OPT();
        //    opt.Text = string.Format("MOZKO = 90");
        //    Options1.Add(opt);
        //    #endregion

        //    oFunction.Zrfc_Read_Table("^", "", "T556B", 0, 0, ref Data1, ref Fields1, ref Options1);

        //    Dictionary<string, string> listText = new Dictionary<string, string>();
        //    foreach (TAB512 data in Data1)
        //    {
        //        string[] arrTemp = data.Wa.Split('^');
        //        listText.Add(string.Format("{0}#{1}#{2}", arrTemp[0].Trim(), arrTemp[1].Trim(), arrTemp[2].Trim()), arrTemp[3].Trim());
        //    }

        //    #region " ParseObject "
        //    foreach (TAB512 data in Data)
        //    {
        //        string[] arrTemp = data.Wa.Split('^');
        //        AbsenceQuotaType Item = new AbsenceQuotaType();
        //        Item.SubGroupSetting = arrTemp[0].Trim();
        //        Item.SubAreaSetting = arrTemp[1].Trim();
        //        Item.Key = arrTemp[2].Trim();
        //        if (listText.ContainsKey(string.Format("{0}#{1}#{2}", Item.SubGroupSetting, Item.SubAreaSetting, Item.Key)))
        //        {
        //            Item.Description = listText[string.Format("{0}#{1}#{2}", Item.SubGroupSetting, Item.SubAreaSetting, Item.Key)];
        //        }
        //        oReturn.Add(Item);
        //    }
        //    #endregion

        //    Connection.ReturnConnection(oConnection);
        //    oFunction.Dispose();

        //    return oReturn;
        //}
        //#endregion

        //#region " GetAllAttendanceTypeList "
        //public override List<AttendanceType> GetAllAttendanceTypeList()
        //{
        //    List<AttendanceType> oReturn = new List<AttendanceType>();

        //    Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
        //    RFCREADTABLE6 oFunction = new RFCREADTABLE6();
        //    oFunction.Connection = oConnection;

        //    TAB512Table Data = new TAB512Table();
        //    RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
        //    RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

        //    RFC_DB_FLD fld;
        //    RFC_DB_OPT opt;

        //    #region " Fields "
        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "MOABW";
        //    Fields.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "SUBTY";
        //    Fields.Add(fld);
        //    #endregion

        //    #region " Options "
        //    opt = new RFC_DB_OPT();
        //    //opt.Text = string.Format("MOAW == '01' AND ENDDA >= '{0}'", DateTime.Now.ToString("yyyyMMdd",oCL_ENUS));
        //    opt.Text = string.Format("KLZBI = 1");
        //    Options.Add(opt);
        //    #endregion

        //    oFunction.Zrfc_Read_Table("^", "", "T554S", 0, 0, ref Data, ref Fields, ref Options);

        //    TAB512Table Data1 = new TAB512Table();
        //    RFC_DB_FLDTable Fields1 = new RFC_DB_FLDTable();
        //    RFC_DB_OPTTable Options1 = new RFC_DB_OPTTable();

        //    #region " Fields "
        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "MOABW";
        //    Fields1.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "AWART";
        //    Fields1.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "ATEXT";
        //    Fields1.Add(fld);
        //    #endregion

        //    #region " Options "
        //    opt = new RFC_DB_OPT();
        //    opt.Text = string.Format("SPRSL = 2");
        //    Options1.Add(opt);
        //    #endregion

        //    oFunction.Zrfc_Read_Table("^", "", "T554T", 0, 0, ref Data1, ref Fields1, ref Options1);

        //    Dictionary<string, string> listText = new Dictionary<string, string>();
        //    foreach (TAB512 data in Data1)
        //    {
        //        string[] arrTemp = data.Wa.Split('^');
        //        listText.Add(string.Format("{0}#{1}", arrTemp[0].Trim(), arrTemp[1].Trim()), arrTemp[2].Trim());
        //    }

        //    #region " ParseObject "
        //    foreach (TAB512 data in Data)
        //    {
        //        string[] arrTemp = data.Wa.Split('^');
        //        AttendanceType Item = new AttendanceType();
        //        Item.AbsAttGrouping = arrTemp[0].Trim();
        //        Item.Key = arrTemp[1].Trim();
        //        if (listText.ContainsKey(string.Format("{0}#{1}", Item.AbsAttGrouping, Item.Key)))
        //        {
        //            Item.Description = listText[string.Format("{0}#{1}", Item.AbsAttGrouping, Item.Key)];
        //        }
        //        oReturn.Add(Item);
        //    }
        //    #endregion

        //    Connection.ReturnConnection(oConnection);
        //    oFunction.Dispose();

        //    return oReturn;
        //}
        //#endregion

        //#region " GetAllCountingRuleList "
        //public override List<CountingRule> GetAllCountingRuleList()
        //{
        //    List<CountingRule> oReturn = new List<CountingRule>();
        //    Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
        //    RFCREADTABLE6 oFunction = new RFCREADTABLE6();
        //    oFunction.Connection = oConnection;

        //    TAB512Table Data = new TAB512Table();
        //    RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
        //    RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

        //    RFC_DB_FLD fld;
        //    RFC_DB_OPT opt;

        //    #region " Fields "
        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "MOPGK"; // Employee Subgroup Grouping for Time Quota Types
        //    Fields.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "MOZKO"; // Grouping of Personnel Subareas for Time Quota Types
        //    Fields.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "CRULE"; // Rule for attendance and absence counting
        //    Fields.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "DEABP"; // Rule for deduction sequence of quotas
        //    Fields.Add(fld);
        //    #endregion

        //    #region " Options "
        //    opt = new RFC_DB_OPT();
        //    opt.Text = string.Format("MOZKO = 90");
        //    Options.Add(opt);
        //    #endregion

        //    oFunction.Zrfc_Read_Table("^", "", "T556C", 0, 0, ref Data, ref Fields, ref Options);

        //    #region " ParseObject "
        //    foreach (TAB512 data in Data)
        //    {
        //        string[] arrTemp = data.Wa.Split('^');
        //        CountingRule Item = new CountingRule();
        //        Item.SubGroupSetting = arrTemp[0].Trim();
        //        Item.SubAreaSetting = arrTemp[1].Trim();
        //        Item.CountingCode = arrTemp[2].Trim();
        //        Item.DeductionRule = arrTemp[3].Trim();
        //        oReturn.Add(Item);
        //    }
        //    #endregion

        //    Connection.ReturnConnection(oConnection);
        //    oFunction.Dispose();

        //    return oReturn;
        //}
        //#endregion

        //#region " GetAllDeductionRule "
        //public override List<DeductionRule> GetAllDeductionRule()
        //{
        //    List<DeductionRule> oReturn = new List<DeductionRule>();

        //    Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
        //    RFCREADTABLE6 oFunction = new RFCREADTABLE6();
        //    oFunction.Connection = oConnection;

        //    TAB512Table Data = new TAB512Table();
        //    RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
        //    RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

        //    RFC_DB_FLD fld;
        //    RFC_DB_OPT opt;

        //    #region " Fields "
        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "MOPGK";
        //    Fields.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "MOZKO";
        //    Fields.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "DEDRG";
        //    Fields.Add(fld);

        //    fld = new RFC_DB_FLD();
        //    fld.Fieldname = "QTTPS";
        //    Fields.Add(fld);
        //    #endregion

        //    #region " Options "
        //    opt = new RFC_DB_OPT();
        //    opt.Text = string.Format("QTYPE = 'A' and ENDDA > '{0}' and BEGDA < '{0}'", DateTime.Now.ToString("yyyyMMdd"));
        //    Options.Add(opt);
        //    #endregion

        //    oFunction.Zrfc_Read_Table("^", "", "T556R", 0, 0, ref Data, ref Fields, ref Options);

        //    #region " ParseObject "
        //    foreach (TAB512 data in Data)
        //    {
        //        string[] arrTemp = data.Wa.Split('^');
        //        DeductionRule Item = new DeductionRule();
        //        Item.SubGroupSetting = arrTemp[0].Trim();
        //        Item.SubAreaSetting = arrTemp[1].Trim();
        //        Item.DeductionCode = arrTemp[2].Trim();
        //        string cTemp = arrTemp[3].Trim().PadRight(6, ' ');
        //        Item.Quota1 = cTemp.Substring(0, 2).Trim();
        //        Item.Quota2 = cTemp.Substring(2, 2).Trim();
        //        Item.Quota3 = cTemp.Substring(4, 2).Trim();
        //        oReturn.Add(Item);
        //    }
        //    #endregion

        //    Connection.ReturnConnection(oConnection);
        //    oFunction.Dispose();

        //    return oReturn;
        //}
        //#endregion
    }
}
