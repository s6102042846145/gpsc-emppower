using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.DELEGATE
{
    public class DelegateFilter 
    {
        private bool __activeOnly = true;
        private bool __delegateFromMe = true;
        private bool __delegateToMe = true;
        public DelegateFilter()
        { 
        }

        public bool ActiveOnly
        {
            get
            {
                return __activeOnly;
            }
            set
            {
                __activeOnly = value;
            }
        }
        public bool DelegateFromMe
        {
            get
            {
                return __delegateFromMe;
            }
            set
            {
                __delegateFromMe = value;
            }
        }
        public bool DelegateToMe
        {
            get
            {
                return __delegateToMe;
            }
            set
            {
                __delegateToMe = value;
            }
        }
    }
}
