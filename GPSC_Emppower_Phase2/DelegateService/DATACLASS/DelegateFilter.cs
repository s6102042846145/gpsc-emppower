using System;
using ESS.UTILITY.EXTENSION;

namespace ESS.DELEGATE.DATACLASS
{
    [Serializable()]
    public class DelegateFilter : AbstractObject
    {
        public DelegateFilter()
        {
        }

        public bool ActiveOnly { get; set; }
        public bool DelegateFromMe { get; set; }
        public bool DelegateToMe { get; set; }
    }
}