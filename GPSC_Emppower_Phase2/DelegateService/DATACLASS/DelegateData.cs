using System;
using System.Collections.Generic;
using System.ComponentModel;
using ESS.EMPLOYEE.CONFIG.OM;
using ESS.UTILITY.EXTENSION;

/*
 * Modified By: Ratchatawan W.
 * Modified Date: 29 Sep 2011
 */

namespace ESS.DELEGATE.DATACLASS
{
    [Serializable()]
    public class DelegateData : AbstractObject
    {
        #region Constructor

        public DelegateData()
        {
            #region Assign Default Value

            foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(this))
            {
                if (property.PropertyType == typeof(bool))
                {
                    property.SetValue(this, false);
                }
                else if (property.PropertyType == typeof(byte))
                {
                    property.SetValue(this, false);
                }
                else if (property.PropertyType == typeof(char))
                {
                    property.SetValue(this, '\0');
                }
                else if (property.PropertyType == typeof(decimal))
                {
                    property.SetValue(this, 0.0M);
                }
                else if (property.PropertyType == typeof(double))
                {
                    property.SetValue(this, 0.0D);
                }
                else if (property.PropertyType == typeof(float))
                {
                    property.SetValue(this, 0.0F);
                }
                else if (property.PropertyType == typeof(int))
                {
                    property.SetValue(this, 0);
                    if (property.Name.IndexOf("ID", StringComparison.CurrentCultureIgnoreCase) >= 0)
                    {
                        property.SetValue(this, -1);
                    }
                }
                else if (property.PropertyType == typeof(long))
                {
                    property.SetValue(this, 0L);
                }
                else if (property.PropertyType == typeof(sbyte))
                {
                    property.SetValue(this, 0);
                }
                else if (property.PropertyType == typeof(short))
                {
                    property.SetValue(this, 0);
                }
                else if (property.PropertyType == typeof(uint))
                {
                    property.SetValue(this, 0);
                }
                else if (property.PropertyType == typeof(ulong))
                {
                    property.SetValue(this, 0);
                }
                else if (property.PropertyType == typeof(ushort))
                {
                    property.SetValue(this, 0);
                }
                else if (property.PropertyType == typeof(string))
                {
                    property.SetValue(this, string.Empty);
                }
                else if (property.PropertyType == typeof(DateTime))
                {
                    property.SetValue(this, DateTime.MinValue);
                }
            }

            #endregion Assign Default Value
        }

        #endregion Constructor

        #region Properties

        public int DelegateID { get; set; }

        public string DelegateFrom { get; set; }

        public string DelegateFromPosition { get; set; }

        public string DelegateTo { get; set; }

        public string DelegateToPosition { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public int RequestTypeID { get; set; }
        public string Remark { get; set; }

        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public bool IsAdmin { get; set; }
        public string RequestNo { get; set; }

        #endregion Properties
    }

    public class EmployeeDataForMobile
    {
        #region Constructor
        public EmployeeDataForMobile()
        {
        }
        #endregion

        #region Member

        public string EmployeeID { get; set; }
        public string PositionID { get; set; }

        public bool IsExternalUser { get; set; }

        public string Name { get; set; }
        private string oCompanyCode = string.Empty;
        public string CompanyCode
        {
            get
            {
                return oCompanyCode.Trim().PadLeft(4, '0');
            }
            set
            {
                oCompanyCode = value.Trim().PadLeft(4, '0');
            }
        }
        public string ImageUrl { get; set; }
        public string Area { get; set; }
        public string SubArea { get; set; }
        public string EmpGroup { get; set; }
        public string EmpSubGroup { get; set; }
        public string OrgUnit { get; set; }
        public string OrgUnitName { get; set; }
        public string Position { get; set; }

        public string AdminGroup { get; set; }
        public string CostCenter { get; set; }
        public string Language { get; set; }
        public bool ReceiveMail { get; set; }
        public string RequesterEmployeeID { get; set; }
        public string RequesterPositionID { get; set; }
        public string RequesterCompanyCode { get; set; } //AddBy: Ratchatawan W. (9 jan 2016) - Work across company
        public string EmailAddress { get; set; }
        public string OfficeTelephoneNo { get; set; }
        public string MobileNo { get; set; }
        public string HomeTelephoneNo { get; set; }
        public bool IsViewPost { get; set; }
        public bool IsViewEmployeeLevel { get; set; }
        public List<INFOTYPE1000> AllPosition { get; set; }

        public DateTime HiringDate { get; set; }
        public DateTime RetirementDate { get; set; }
        public DateTime StartWorkingDate { get; set; }
        
        #endregion
    }
}