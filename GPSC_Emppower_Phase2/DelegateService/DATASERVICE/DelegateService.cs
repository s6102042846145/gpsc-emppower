using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using ESS.DELEGATE.DATACLASS;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.CONFIG.OM;
using ESS.EMPLOYEE.DATACLASS;
using ESS.PORTALENGINE;
using ESS.WORKFLOW;
using Newtonsoft.Json;

/*
 * Modified By: Ratchatawan W.
 * Modified Date: 29 Sep 2011
 */

namespace ESS.DELEGATE.DATASERVICE
{
    public class DelegateService : AbstractDataService
    {
        public DelegateService()
        {
        }

        public override Object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            DataSet ds = new DataSet("ADDITIONAL");
            DataTable oTable;

            //ESS.DELEGATE.DATACLASS.DelegateData item = new DATACLASS.DelegateData();
            //item.RequestTypeID = -1;
            //item.DelegateFrom = Requestor.EmployeeID;
            //item.DelegateFromPosition = Requestor.OrgAssignment.Position;
            //item.BeginDate = DateTime.Now;
            //item.EndDate = DateTime.Now;

            DelegateHeader oDelegateHeader = new DelegateHeader();

            Dictionary<string, Object> oAllEmployeePossibleToDelegated = new Dictionary<string, object>();
            foreach (INFOTYPE1000 info10 in Requestor.GetAllPositions(WorkflowPrinciple.Current.UserSetting.Language))
            {
                oAllEmployeePossibleToDelegated.Add(info10.ObjectID, GetDelegateToByEmployeePosition(Requestor.EmployeeID, info10.ObjectID, Requestor.CompanyCode));
            }


            oDelegateHeader.DelegateFrom = Requestor.EmployeeID;
            oDelegateHeader.DelegateFromName = Requestor.Name;
            oDelegateHeader.DelegateFromCompanyCode = Requestor.CompanyCode;
            oDelegateHeader.DelegateFromOrg = Requestor.CurrentOrganization.ObjectID;
            oDelegateHeader.DelegateFromOrgName = Requestor.CurrentOrganization.Text;
            oDelegateHeader.DelegateFromPosition = Requestor.PositionID;
            oDelegateHeader.DelegateFromPositionName = Requestor.CurrentPosition.Text;

            oDelegateHeader.CreatedBy = Requestor.EmployeeID;
            oDelegateHeader.CreatedByPositionID = Requestor.PositionID;
            oDelegateHeader.CreatedByCompanyCode = Requestor.CompanyCode;
            oDelegateHeader.CreatedByName = Requestor.Name;

            oDelegateHeader.AllEmployeePossibleToDelegated = oAllEmployeePossibleToDelegated;

            List<EmployeeDataForMobile> DelegatedList = (List<EmployeeDataForMobile>)oAllEmployeePossibleToDelegated[Requestor.PositionID];
            if (DelegatedList.Count > 0)
            {
                oDelegateHeader.DelegateTo = DelegatedList[0].EmployeeID;
                oDelegateHeader.DelegateToName = DelegatedList[0].Name;
                oDelegateHeader.DelegateToPosition = DelegatedList[0].PositionID;
                oDelegateHeader.DelegateToPositionName = DelegatedList[0].Position;
                oDelegateHeader.DelegateToOrg = DelegatedList[0].OrgUnit;
                oDelegateHeader.DelegateToOrgName = DelegatedList[0].OrgUnitName;
                oDelegateHeader.DelegateToCompanyCode = Requestor.CompanyCode;
            }
            oDelegateHeader.BeginDate = DateTime.Now.Date;
            oDelegateHeader.EndDate = DateTime.Now.Date;

            List<DbDelegateGroupping> list_dbgroup_delegate = EmployeeManagement.CreateInstance(Requestor.CompanyCode).GetListDelegateGroupping();

            List<DelegateGroupping> list_group_detail = new List<DelegateGroupping>();

            // Check list Request Type ������
            oDelegateHeader.DetailList = new List<DelegateDetail>();
            foreach (DataRow dr in WorkflowManagement.CreateInstance(Requestor.CompanyCode).GetAllRequestType(WorkflowPrinciple.Current.UserSetting.Language).Rows)
            {
                DelegateDetail oDetail = new DelegateDetail();
                oDetail.DelegateID = oDelegateHeader.DelegateID;
                oDetail.RequestTypeID = int.Parse(dr["RequestTypeID"].ToString());
                oDetail.RequestTypeName = PortalEngineManagement.CreateInstance(Requestor.CompanyCode).GetTextDescription("REQUESTTYPE", WorkflowPrinciple.Current.UserSetting.Language, dr["RequestTypeCode"].ToString());
                oDelegateHeader.DetailList.Add(oDetail);

                var check_request_type = list_dbgroup_delegate.FirstOrDefault(s => s.RequestTypeId == oDetail.RequestTypeID);
                if (check_request_type != null)
                {
                    var dup = list_group_detail.FirstOrDefault(s => s.TextCodeGroupping == check_request_type.TextCode);
                    if (dup == null)
                    {
                        DelegateGroupping new_group = new DelegateGroupping()
                        {
                            IsCheckGroupping = false,
                            TextCodeGroupping = check_request_type.TextCode,
                            Sequence = check_request_type.Sequence
                        };

                        DelegateGrouppingDetail new_group_detail = new DelegateGrouppingDetail()
                        {
                            IsCheckDetailGroupping = false,
                            RequestTypeID = check_request_type.RequestTypeId,
                            RequestTypeName = check_request_type.RequestTypeName,
                            SequenceDetail = check_request_type.SequenceDetail
                        };

                        new_group.GroupDetail.Add(new_group_detail);
                        list_group_detail.Add(new_group);
                    }
                    else
                    {
                        DelegateGrouppingDetail new_group_detail = new DelegateGrouppingDetail()
                        {
                            IsCheckDetailGroupping = false,
                            RequestTypeID = check_request_type.RequestTypeId,
                            RequestTypeName = check_request_type.RequestTypeName,
                            SequenceDetail = check_request_type.SequenceDetail
                        };
                        dup.GroupDetail.Add(new_group_detail);
                    }
                }
            }

            if (list_group_detail.Count > 0)
            {
                List<DelegateGroupping> order_list_group = list_group_detail.OrderBy(s => s.Sequence).ToList();
                foreach(var data in order_list_group)
                {
                    var order =  data.GroupDetail.OrderBy(s => s.SequenceDetail).ToList();
                    data.GroupDetail = order;
                }

                oDelegateHeader.DetailGroup.AddRange(order_list_group);
            }

            var list_position_all = EmployeeManagement.CreateInstance(Requestor.CompanyCode).GetAllDelegatePosition(Requestor.EmployeeID);
            oDelegateHeader.DetailPosition.AddRange(list_position_all);


            oTable = oDelegateHeader.ToADODataTable();
            oTable.TableName = "DELEGATE";
            ds.Tables.Add(oTable);
            return ds;
        }

        public List<EmployeeDataForMobile> GetDelegateToByEmployeePosition(string EmployeeID, string PositionID, string CompanyCode)
        {
            List<EmployeeDataForMobile> oReturn = new List<EmployeeDataForMobile>();
            List<EmployeeData> EmployeeList = WorkflowManagement.CreateInstance(CompanyCode).GetDelegateToByEmployeePosition(EmployeeID, PositionID);
            foreach (EmployeeData oEmployeeData in EmployeeList)
            {
                EmployeeDataForMobile oInf1Mobile = Convert<EmployeeDataForMobile>.ObjectFrom(oEmployeeData.OrgAssignment);
                oInf1Mobile.Name = oEmployeeData.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                oInf1Mobile.OrgUnitName = oEmployeeData.OrgAssignment.OrgUnitData.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                oInf1Mobile.EmployeeID = oEmployeeData.EmployeeID;
                try
                {
                    oInf1Mobile.PositionID = oEmployeeData.OrgAssignment.PositionData.ObjectID;
                    oInf1Mobile.Position = oEmployeeData.OrgAssignment.PositionData.AlternativeName(WorkflowPrinciple.Current.UserSetting.Language);
                }
                catch
                {
                    throw new Exception("EXCEPTION_USER_POSITION_NOT_FOUND");
                }
                oInf1Mobile.AllPosition = oEmployeeData.GetAllPositions(WorkflowPrinciple.Current.UserSetting.Language);
                //Modify By Morakot.t 2018-10-03
                //string ImageFile = string.Format("{0}{1}.jpg", EmployeeManagement.CreateInstance(CompanyCode).PHOTO_PATH, oInf1Mobile.EmployeeID.Substring(2));
                string ImageFile = string.Format("{0}{1}.jpg", EmployeeManagement.CreateInstance(CompanyCode).PHOTO_PATH, oInf1Mobile.EmployeeID);
                oInf1Mobile.ImageUrl = ImageFile;
                oInf1Mobile.RequesterEmployeeID = oEmployeeData.EmployeeID;
                oInf1Mobile.RequesterPositionID = oEmployeeData.OrgAssignment.PositionData.ObjectID;
                oInf1Mobile.CompanyCode = oEmployeeData.CompanyCode;//AddBy: Ratchatawan W. (9 jan 2016) - Work across company
                oReturn.Add(oInf1Mobile);
            }
            return oReturn;
        }

        public override void CalculateInfoData(EmployeeData Requestor, Object Data, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
            Info.Rows.Clear();
            DataRow oRow = Info.NewRow();

            DelegateHeader oDelegateHeader = new DelegateHeader();
            Temp oHeader = JsonConvert.DeserializeObject<Temp>(Data.ToString());

            oRow["DELEGATETO"] = oHeader.DELEGATE[0].DelegateTo;
            Info.Rows.Add(oRow);
        }

        public override void PrepareData(EmployeeData Requestor, Object Data)
        {
        }

        /// <summary>
        /// Used to validate data before insert or update Delegate Data to database
        /// </summary>
        /// <param name="newData">You can set this data in method 'LoadData' on Editor page</param>
        /// <param name="Requestor"></param>
        /// <param name="RequestNo"></param>
        /// <param name="DocumentDate"></param>
        /// 
        public override void ValidateData(Object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int iUpload)
        {
            #region Code Before
            //EmployeeData emp = new EmployeeData() { CompanyCode = Requestor.CompanyCode };
            //ESS.DELEGATE.DATACLASS.DelegateData delegateData = new DATACLASS.DelegateData();
            //delegateData.ParseToObject(newData.Tables[0].Rows[0]);
            //if (string.IsNullOrEmpty(delegateData.DelegateFrom))
            //    throw new Exception("INVALID_DELEGATEFROM_NULL");
            //else if (string.IsNullOrEmpty(delegateData.DelegateTo))
            //    throw new Exception("INVALID_DELEGATETO_NULL");
            //else if (!emp.ValidateEmployeeID(delegateData.DelegateFrom))
            //    throw new Exception("INVALID_DELEGATEFROM_EXISTING");
            //else if (!emp.ValidateEmployeeID(delegateData.DelegateTo))
            //    throw new Exception("INVALID_DELEGATETO_EXISTING");
            //else if (delegateData.DelegateFrom == delegateData.DelegateTo)
            //    throw new Exception("INVALID_DELEGATE_SAMEPERSON");
            ////BeginDate and EndDate couldn't be null (this application use DateTime.MinValue to check null)
            //else if (delegateData.BeginDate.Date < DateTime.MinValue.Date || delegateData.EndDate.Date < DateTime.MinValue.Date)
            //    throw new Exception("INVALID_INTERVAL_NULL");
            ////BeginDate must more than EndDate
            //else if (delegateData.BeginDate > delegateData.EndDate)
            //    throw new Exception("INVALID_INTERVAL_BEGINLESSTHANEND");
            //else
            //{
            //    try
            //    {
            //        //This Delegate data must not dupplicate with the existing in database
            //        if (DelegateManagement.CheckDupplicate(delegateData) > 0)
            //            throw new Exception("INVALID_DUPPLICATE");
            //    }
            //    catch (SqlException ex)
            //    {
            //        throw new Exception(ex.Message);
            //    }
            //}
            #endregion

            Temp oHeader = JsonConvert.DeserializeObject<Temp>(newData.ToString());
            bool isCheckGrant = false;
            if (oHeader.DELEGATE.Count > 0)
            {
                foreach (var item in oHeader.DELEGATE)
                {
                    if (string.IsNullOrEmpty(item.Remark))
                    {
                        throw new DataServiceException("DELEGATE", "REMARK_NOT_NULL");
                        //item.Remark = "";
                    }
                }

                var count_check = oHeader.DELEGATE.SelectMany(s => s.DetailList).Where(s => s.IsCheck == true).Count();
                if (count_check > 0)
                {
                    isCheckGrant = true;
                }
            }

            if (isCheckGrant == false)
            {
                throw new DataServiceException("DELEGATE", "NOT_CHECK");
            }



        }


        public class Temp
        {
            public Temp()
            {
                DELEGATE = new List<DelegateHeader>();
            }
            public List<DelegateHeader> DELEGATE { get; set; }
        }
        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, Object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            DelegateHeader oDelegateHeader = new DelegateHeader();
            Temp oHeader = JsonConvert.DeserializeObject<Temp>(Data.ToString());

            if (State.ToUpper() == "DRAFT")
            {
                oHeader.DELEGATE[0].Status = "DRAFT";
            }
            else if (State.ToUpper().Contains("WAITFORACCEPT"))
            {
                oHeader.DELEGATE[0].Status = "WAITFORACCEPT";
            }
            else if (State.ToUpper() == "CANCELLED")
            {
                oHeader.DELEGATE[0].Status = "CANCELLED";
            }
            else if (State.ToUpper() == "COMPLETED")
            {
                oHeader.DELEGATE[0].Status = "COMPLETED";
                WorkflowManagement.CreateInstance(Requestor.CompanyCode).SaveDelegateHeader(oHeader.DELEGATE[0]);
            }
            oHeader.DELEGATE[0].RequestNo = RequestNo;
            DelegateManagement.CreateInstance(Requestor.CompanyCode).CreateDelegate(oHeader.DELEGATE[0]);


            //if (State == "COMPLETED")
            //{
            ////SecurityService.SecurityService oService = new SecurityService.SecurityService();
            //ESS.DELEGATE.DATACLASS.DelegateData delegateData = new DATACLASS.DelegateData();
            //delegateData.ParseToObject(Data.Tables[0].Rows[0]);
            ////SecurityService.DelegateData oData = new SecurityService.DelegateData();
            ////oData.BeginDate = delegateData.BeginDate;
            ////oData.DelegateID = delegateData.DelegateID;
            ////oData.DelegateTo = delegateData.DelegateTo;
            ////oData.EmployeeID = delegateData.EmployeeID;
            ////oData.EndDate = delegateData.EndDate;
            //delegateData.RequestNo = RequestNo;
            ////if (delegateData.DelegateID > 0)
            ////    ServiceManager.DelegateService.UpdateDelegate(delegateData);
            ////else
            //ServiceManager.DelegateService.CreateDelegate(delegateData);
            //}
            //else
            //{

            //}
        }
    }
}