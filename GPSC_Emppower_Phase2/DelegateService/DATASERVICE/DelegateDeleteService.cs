using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ESS.DATA.ABSTRACT;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.DATACLASS;
using ESS.WORKFLOW;
using Newtonsoft.Json;

/*
 * Modified By: Ratchatawan W.
 * Modified Date: 29 Sep 2011
 */

namespace ESS.DELEGATE.DATASERVICE
{
    public class DelegateDeleteService : AbstractDataService
    {
        public DelegateDeleteService()
        {
        }

        public override Object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            DataSet ds = new DataSet("ADDITIONAL");
            DataTable oTable = new DataTable();
            DelegateHeader item = new DelegateHeader();
            oTable = item.ToADODataTable(true);
           

            if (!string.IsNullOrEmpty(CreateParam))
            {
                oTable.Rows.Clear();
                string requestNo = CreateParam.Split('|')[0];
                int year = Convert.ToInt32(CreateParam.Split('|')[2]);
                int month = Convert.ToInt32(CreateParam.Split('|')[1]);
                var requests = DelegateManagement.CreateInstance(Requestor.CompanyCode).GetDelegateRequest(Requestor.EmployeeID, month, year);

                var request = requests.FirstOrDefault(a => a.RequestNo == requestNo);
                oTable = request.ToADODataTable();
            }
            oTable.TableName = "DELEGATE";


            ds.Tables.Add(oTable);

            return ds;
        }

        public override void PrepareData(EmployeeData Requestor, Object Data)
        {
        }

        public override void ValidateData(Object newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate,int NoOfFileAttached)
        {
        }

        public class Temp
        {
            public Temp()
            {
                DELEGATE = new List<DelegateHeader>();
            }
            public List<DelegateHeader> DELEGATE { get; set; }
        }
        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, Object Data, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            if (State == "COMPLETED")
            {
                DelegateHeader oDelegateHeader = new DelegateHeader();
                Temp oHeader = JsonConvert.DeserializeObject<Temp>(Data.ToString());

                var requests = WorkflowManagement.CreateInstance(Requestor.CompanyCode).GetDelegateDataByCriteria(Requestor.EmployeeID, oHeader.DELEGATE[0].BeginDate.Month, oHeader.DELEGATE[0].BeginDate.Year);

                var request = requests.FirstOrDefault(a => a.DelegateFrom == oHeader.DELEGATE[0].DelegateFrom && a.DelegateTo == oHeader.DELEGATE[0].DelegateTo && a.BeginDate == oHeader.DELEGATE[0].BeginDate);

                oHeader.DELEGATE[0].DelegateID = request.DelegateID;
                oHeader.DELEGATE[0].Status = "CANCELLED";
                WorkflowManagement.CreateInstance(Requestor.CompanyCode).DeleteDelegateHeader(oHeader.DELEGATE[0]);
                DelegateManagement.CreateInstance(Requestor.CompanyCode).CreateDelegate(oHeader.DELEGATE[0]);

            }
        }
    }
}