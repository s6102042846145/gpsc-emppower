using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using ESS.DATA;
using ESS.EMPLOYEE;

/*
 * Modified By: Ratchatawan W.
 * Modified Date: 29 Sep 2011 
 */
namespace ESS.DELEGATE
{

    public class DelegateDeleteService : AbstractDataService
    {
        public DelegateDeleteService()
        {
        }

        public override DataSet GenerateAdditionalData(EmployeeData Requestor)
        {
            DataSet ds = new DataSet("DELEGATE");
            ds.Tables.Add(DelegateManagement.GetDelegateData(Requestor.EmployeeID, 0, 0));
            return ds;
        }

        public override void PrepareData(EmployeeData Requestor, DataSet Data)
        {

        }

        public override void ValidateData(DataSet newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate)
        {

        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, DataSet Data, string State, string RequestNo)
        {
            if (State == "COMPLETED")
            {
                List<DelegateData> delegateDataList = new List<DelegateData>();
                DelegateData delegateData;
                foreach (DataRow dr in Data.Tables[0].Rows)
                {
                    delegateData = new DelegateData();
                    delegateData.ParseToObject(Data.Tables[0].Rows[0]);
                    delegateDataList.Add(delegateData);
                }

                ServiceManager.DelegateService.DeleteDelegate(delegateDataList);
            }
        }
    }
}
