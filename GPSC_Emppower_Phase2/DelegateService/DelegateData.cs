using System;
using System.Collections.Generic;
using System.Text;
using ESS.DATA;

/*
 * Modified By: Ratchatawan W.
 * Modified Date: 29 Sep 2011 
 */
namespace ESS.DELEGATE
{
    [Serializable()]
    public class DelegateData : AbstractObject
    {
        public DelegateData()
        {
        }

        #region Variable
        private int __delegateID = 0;
        private string __delegateFrom = string.Empty;
        private string __delegateFromPosition = string.Empty;
        private string __delegateTo = string.Empty;
        private string __delegateToPosition = string.Empty;
        private DateTime __beginDate = DateTime.MinValue;
        private DateTime __endDate = DateTime.MinValue;
        private int __requestTypeID = 0;
        private string __remark = string.Empty;
        private string __createdBy = string.Empty;
        private DateTime __createdDate = DateTime.MinValue;
        private string __updatedBy = string.Empty;
        private DateTime __updatedDate = DateTime.MinValue;
        private bool __isAdmin = false;
        private string __requestNo = string.Empty;
        #endregion

        #region Properties
        public int DelegateID
        {
            get
            {
                return __delegateID;
            }
            set
            {
                __delegateID = value;
            }
        }

        public string DelegateFrom
        {
            get
            {
                return __delegateFrom;
            }
            set
            {
                __delegateFrom = value;
            }
        }

        public string DelegateFromPosition
        {
            get
            {
                return __delegateFromPosition;
            }
            set
            {
                __delegateFromPosition = value;
            }
        }

        public string DelegateTo
        {
            get
            {
                return __delegateTo;
            }
            set
            {
                __delegateTo = value;
            }
        }

        public string DelegateToPosition
        {
            get
            {
                return __delegateToPosition;
            }
            set
            {
                __delegateToPosition = value;
            }
        }

        public DateTime BeginDate
        {
            get
            {
                return __beginDate;
            }
            set
            {
                __beginDate = value;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return __endDate;
            }
            set
            {
                __endDate = value;
            }
        }

        public int RequestTypeID
        {
            get
            {
                return __requestTypeID;
            }
            set
            {
                __requestTypeID = value;
            }
        }

        public string Remark
        {
            get
            {
                return __remark;
            }
            set
            {
                __remark = value;
            }
        }

        public string CreatedBy
        {
            get
            {
                return __createdBy;
            }
            set
            {
                __createdBy = value;
            }
        }

        public DateTime CreatedDate
        {
            get
            {
                return __createdDate;
            }
            set
            {
                __createdDate = value;
            }
        }

        public string UpdatedBy
        {
            get
            {
                return __updatedBy;
            }
            set
            {
                __updatedBy = value;
            }
        }

        public DateTime UpdatedDate
        {
            get
            {
                return __updatedDate;
            }
            set
            {
                __updatedDate = value;
            }
        }

        public bool IsAdmin
        {
            get
            {
                return __isAdmin;
            }
            set
            {
                __isAdmin = value;
            }
        }

        public string RequestNo
        {
            get
            {
                return __requestNo;
            }
            set
            {
                __requestNo = value;
            }
        }
        #endregion
    }
}
