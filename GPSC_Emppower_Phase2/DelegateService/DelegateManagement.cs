using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Reflection;
using ESS.DELEGATE.DATACLASS;
using ESS.EMPLOYEE.DATACLASS;

/*
 * Modified By: Ratchatawan W.
 * Modified Date: 29 Sep 2011
 */

namespace ESS.DELEGATE
{
    public class DelegateManagement
    {
        private static Configuration __config;

        private static Configuration config
        {
            get
            {
                if (__config == null)
                {
                    __config = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().CodeBase.Replace("file:///", "").Replace("/", "\\"));
                }
                return __config;
            }
        }

        public DelegateManagement()
        {
        }
        public string CompanyCode { get; set; }

        public static DelegateManagement CreateInstance(string oCompanyCode)
        {

            DelegateManagement oDelegateManagement = new DelegateManagement()
            {
                CompanyCode = oCompanyCode
            };
            return oDelegateManagement;
        }
        public static DataTable GetRequestTypeForAdmin(string EmployeeID)
        {
            return DELEGATE.ServiceManager.DelegateService.GetRequestTypeForAdmin(EmployeeID);
        }

        public static DataTable GetDelegateData(string EmployeeID)
        {
            return DELEGATE.ServiceManager.DelegateService.GetDelegateData(EmployeeID);
        }

        public List<DelegateHeader> GetDelegateRequest(string EmployeeID, int Month, int Year)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetDelegateRequest(EmployeeID, Month, Year);
        }

        public static DataTable GetDelegateData(string EmployeeID, int Month, int Year)
        {
            return DELEGATE.ServiceManager.DelegateService.GetDelegateData(EmployeeID, Month, Year);

            #region
            //SecurityService.SecurityService oService = new ESS.DELEGATE.SecurityService.SecurityService();
            //ESS.DELEGATE.SecurityService.DelegateFilter oFilter = new ESS.DELEGATE.SecurityService.DelegateFilter();
            //if (filter.DelegateFromMe)
            //{
            //    oFilter.Depositor = EmployeeID;
            //}
            //if (filter.DelegateToMe)
            //{
            //    oFilter.Receiver = EmployeeID;
            //}
            //oFilter.IsActive = filter.ActiveOnly;
            ////ESS.DELEGATE.SecurityService.DelegateData[] data = oService.GetDelegateData(oFilter);
            //ESS.DELEGATE.SecurityService.DelegateData[] data = new ESS.DELEGATE.SecurityService.DelegateData[] { };
            //List<DelegateData> retVal = new List<DelegateData>();
            //foreach (ESS.DELEGATE.SecurityService.DelegateData item in data)
            //{
            //    DelegateData d = new DelegateData();
            //    d.BeginDate = item.BeginDate;
            //    d.DelegateID = item.DelegateID;
            //    d.DelegateTo = item.DelegateTo;
            //    d.EmployeeID = item.EmployeeID;
            //    d.EndDate = item.EndDate;
            //    d.RequestNo = item.RefDocumentID;
            //    retVal.Add(d);
            //}
            //retVal.Sort(new DelegateDataComparer());
            //return retVal;
            #endregion
        }

        public static DataTable GetRequestType(string EmployeeID)
        {
            return ServiceManager.DelegateService.GetRequestType(EmployeeID);
        }

        public static int CheckDupplicate(DelegateData data)
        {
            return ServiceManager.DelegateService.CheckDupplicate(data);
        }

        public  void CreateDelegate(DelegateHeader data)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.CreateDelegate(data);
        }
    }
}