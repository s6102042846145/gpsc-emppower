using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace ESS.DELEGATE
{
    public class AbstractDelegateService : IDELEGATE
    {
        #region IDELEGATE Members

        public virtual DataTable GetDelegateData(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DataTable GetDelegateData(string EmployeeID, int Month, int Year)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DataTable GetRequestTypeForAdmin(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DataTable GetRequestType(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void UpdateDelegate(DelegateData data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void CreateDelegate(DelegateData data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual int CheckDupplicate(DelegateData data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void DeleteDelegate(List<DelegateData> list)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion
    }
}
