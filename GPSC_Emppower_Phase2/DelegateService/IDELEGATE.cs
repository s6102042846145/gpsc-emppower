using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

/*
 * Modified By: Ratchatawan W.
 * Modified Date: 28 Sep 2011 
 */
namespace ESS.DELEGATE
{
    public interface IDELEGATE
    {
        DataTable GetDelegateData(string EmployeeID);
        DataTable GetDelegateData(string EmployeeID, int Month, int Year);
        DataTable GetRequestTypeForAdmin(string EmployeeID);
        DataTable GetRequestType(string EmployeeID);
        void UpdateDelegate(DelegateData data);
        void CreateDelegate(DelegateData data);
        int CheckDupplicate(DelegateData data);
        void DeleteDelegate(List<DelegateData> list);
    }
}
