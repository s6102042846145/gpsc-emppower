using System.Collections.Generic;
using System.Data;
using ESS.DELEGATE.DATACLASS;
using ESS.EMPLOYEE.DATACLASS;

/*
 * Modified By: Ratchatawan W.
 * Modified Date: 28 Sep 2011
 */

namespace ESS.DELEGATE.INTERFACE
{
    public interface IDelegateService
    {
        DataTable GetDelegateData(string EmployeeID);

        DataTable GetDelegateData(string EmployeeID, int Month, int Year);

        List<DelegateHeader> GetDelegateRequest(string EmployeeID, int Month, int Year);

        DataTable GetRequestTypeForAdmin(string EmployeeID);

        DataTable GetRequestType(string EmployeeID);

        void UpdateDelegate(DelegateData data);

        void CreateDelegate(DelegateData data);

        void CreateDelegate(DelegateHeader data);

        int CheckDupplicate(DelegateData data);

        void DeleteDelegate(List<DelegateData> list);
    }
}