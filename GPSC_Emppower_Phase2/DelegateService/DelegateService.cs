using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using ESS.DATA;
using ESS.EMPLOYEE;
using System.Data.SqlClient;

/*
 * Modified By: Ratchatawan W.
 * Modified Date: 29 Sep 2011 
 */
namespace ESS.DELEGATE
{
    public class DelegateService : AbstractDataService
    {
        public DelegateService()
        {
        }

        public override DataSet GenerateAdditionalData(EmployeeData Requestor)
        {
            DataSet ds = new DataSet("ADDITIONAL");
            DataTable oTable;
            DelegateData item = new DelegateData();
            item.RequestTypeID = -1;
            item.DelegateFrom = Requestor.EmployeeID;
            item.DelegateFromPosition = Requestor.OrgAssignment.Position;//Requestor.CurrentPosition.ObjectID;
            item.BeginDate = DateTime.Now;
            item.EndDate = DateTime.Now;
            oTable = item.ToADODataTable();
            oTable.TableName = "DELEGATE";
            ds.Tables.Add(oTable);
            return ds;
        }

        public override void CalculateInfoData(EmployeeData Requestor, DataSet Data, DataTable Info)
        {
            Info.Rows.Clear();
            DataRow oRow = Info.NewRow();
            DelegateData item = new DelegateData();
           
            item.ParseToObject(Data.Tables["DELEGATE"].Rows[0]);
            oRow["DELEGATETO"] = item.DelegateTo;
            Info.Rows.Add(oRow);
        }

        public override void PrepareData(EmployeeData Requestor, DataSet Data)
        {
        }

        /// <summary>
        /// Used to validate data before insert or update Delegate Data to database
        /// </summary>
        /// <param name="newData">You can set this data in method 'LoadData' on Editor page</param>
        /// <param name="Requestor"></param>
        /// <param name="RequestNo"></param>
        /// <param name="DocumentDate"></param>
        public override void ValidateData(DataSet newData, EmployeeData Requestor, string RequestNo, DateTime DocumentDate)
        {
            DelegateData delegateData = new DelegateData();
            delegateData.ParseToObject(newData.Tables[0].Rows[0]);
            if (string.IsNullOrEmpty(delegateData.DelegateFrom))
                throw new Exception("INVALID_DELEGATEFROM_NULL");
            else if (string.IsNullOrEmpty(delegateData.DelegateTo))
                throw new Exception("INVALID_DELEGATETO_NULL");
            else if (!EmployeeData.ValidateEmployeeID(delegateData.DelegateFrom))
                throw new Exception("INVALID_DELEGATEFROM_EXISTING");
            else if (!EmployeeData.ValidateEmployeeID(delegateData.DelegateTo))
                throw new Exception("INVALID_DELEGATETO_EXISTING");
            else if (delegateData.DelegateFrom == delegateData.DelegateTo)
                throw new Exception("INVALID_DELEGATE_SAMEPERSON");
            //BeginDate and EndDate couldn't be null (this application use DateTime.MinValue to check null)
            else if (delegateData.BeginDate.Date < DateTime.MinValue.Date || delegateData.EndDate.Date < DateTime.MinValue.Date)
                throw new Exception("INVALID_INTERVAL_NULL");
            //BeginDate must more than EndDate
            else if (delegateData.BeginDate > delegateData.EndDate)
                throw new Exception("INVALID_INTERVAL_BEGINLESSTHANEND");
            else
            {
                try
                {
                    //This Delegate data must not dupplicate with the existing in database
                    if (DelegateManagement.CheckDupplicate(delegateData) > 0)
                        throw new Exception("INVALID_DUPPLICATE");
                }
                catch (SqlException ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }
        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, DataSet Data, string State, string RequestNo)
        {

            if (State == "COMPLETED")
            {
                //SecurityService.SecurityService oService = new SecurityService.SecurityService();
                DelegateData delegateData = new DelegateData();
                delegateData.ParseToObject(Data.Tables[0].Rows[0]);
                //SecurityService.DelegateData oData = new SecurityService.DelegateData();
                //oData.BeginDate = delegateData.BeginDate;
                //oData.DelegateID = delegateData.DelegateID;
                //oData.DelegateTo = delegateData.DelegateTo;
                //oData.EmployeeID = delegateData.EmployeeID;
                //oData.EndDate = delegateData.EndDate;
                delegateData.RequestNo = RequestNo;
                //if (delegateData.DelegateID > 0)
                //    ServiceManager.DelegateService.UpdateDelegate(delegateData);
                //else
                ServiceManager.DelegateService.CreateDelegate(delegateData);
            }
        }


    }
}
