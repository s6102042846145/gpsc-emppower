using System;
using System.Collections.Generic;
using System.Text;

/*
 * Modified By: Ratchatawan W.
 * Modified Date: 28 Sep 2011 
 */
namespace ESS.DELEGATE
{
    public class DelegateDataComparer : IComparer<DelegateData>
    {
        #region IComparer<DelegateData> Members

        public int Compare(DelegateData x, DelegateData y)
        {
            int res = x.BeginDate.CompareTo(y.BeginDate);
            //if (res == 0)
            //{
            //    res = x.EndDate.CompareTo(y.EndDate);
            //    if (res == 0)
            //    {
            //        res = x.EmployeeID.CompareTo(y.EmployeeID);
            //    }
            //}
            return res;
        }

        #endregion
    }
}
