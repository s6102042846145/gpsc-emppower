using System;
using System.Configuration;
using System.Reflection;
using ESS.DELEGATE.INTERFACE;

namespace ESS.DELEGATE
{
    public class ServiceManager
    {
        private static Configuration __config;

        private ServiceManager()
        {
        }

        private static Configuration config
        {
            get
            {
                if (__config == null)
                {
                    __config = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().CodeBase.Replace("file:///", "").Replace("/", "\\"));
                }
                return __config;
            }
        }

        private static string ESSCONNECTOR
        {
            get
            {
                if (config == null || config.AppSettings.Settings["ESSCONNECTOR"] == null)
                {
                    return "DB";
                }
                else
                {
                    return config.AppSettings.Settings["ESSCONNECTOR"].Value;
                }
            }
        }
        public string CompanyCode { get; set; }
        public static ServiceManager CreateInstance(string oCompanyCode)
        {

            ServiceManager oServiceManager = new ServiceManager()
            {
                CompanyCode = oCompanyCode
            };
            return oServiceManager;
        }
        internal static IDelegateService DelegateService
        {
            get
            {
                Type oType = GetService(ESSCONNECTOR, "DelegateServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IDelegateService)Activator.CreateInstance(oType);
                }
            }
        }

        public IDelegateService ESSData
        {
            get
            {
                Type oType = GetService(ESSCONNECTOR, "DelegateServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IDelegateService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }

        private static Type GetService(string Mode, string ClassName)
        {
            Type oReturn = null;
            Assembly oAssembly;
            string assemblyName = string.Format("ESS.DELEGATE.{0}", Mode.ToUpper());
            string typeName = string.Format("ESS.DELEGATE.{0}.{1}", Mode.ToUpper(), ClassName);// CultureInfo.CurrentCulture.TextInfo.ToTitleCase(ClassName.ToLower()));
            oAssembly = Assembly.Load(assemblyName);    // Load assembly (dll)
            oReturn = oAssembly.GetType(typeName);      // Load class
            return oReturn;
        }
    }
}