using System;
using System.Collections.Generic;
using System.Data;
using ESS.DELEGATE.DATACLASS;
using ESS.DELEGATE.INTERFACE;
using ESS.EMPLOYEE.DATACLASS;

namespace ESS.DELEGATE.ABSTRACT
{
    public class AbstractDelegateService : IDelegateService
    {
        #region IDELEGATE Members

        public virtual DataTable GetDelegateData(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DataTable GetDelegateData(string EmployeeID, int Month, int Year)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<DelegateHeader> GetDelegateRequest(string EmployeeID, int Month, int Year)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DataTable GetRequestTypeForAdmin(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DataTable GetRequestType(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void UpdateDelegate(DelegateData data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void CreateDelegate(DelegateData data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void CreateDelegate(DelegateHeader data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual int CheckDupplicate(DelegateData data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void DeleteDelegate(List<DelegateData> list)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion IDELEGATE Members
    }
}