using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Reflection;
using System.Text;
using ESS.TIMESHEET;
using SAPInterface;
using SAP.Connector;
using ESS.SHAREDATASERVICE;

namespace ESS.TIMESHEET.SAP
{
    public class TimesheetServicelmpl : AbstractERPTimesheetService
    {
        private CultureInfo oCLEN = new CultureInfo("en-US");
        //private static Configuration __config;
        public TimesheetServicelmpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
        }

        private static string ModuleID = "ESS.HR.TM.SAP";
        public string CompanyCode { get; set; }
        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }
        private string Sap_Version
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAP_VERSION");


            }
        }

        //#region " PrivateData "
        //private static Configuration config
        //{
        //    get
        //    {
        //        if (__config == null)
        //        {
        //            __config = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().CodeBase.Replace("file:///", "").Replace("/", "\\"));
        //        }
        //        return __config;
        //    }
        //}

        //private static string BaseConnStr
        //{
        //    get
        //    {
        //        if (config == null || config.AppSettings.Settings["BaseConnStr"] == null)
        //        {
        //            return "BaseConnStr";
        //        }
        //        else
        //        {
        //            return config.AppSettings.Settings["BaseConnStr"].Value;
        //        }
        //    }
        //}

        //private static string Sap_Version
        //{
        //    get
        //    {
        //        if (config == null || config.AppSettings.Settings["SAP_VERSION"] == null)
        //        {
        //            return "6";
        //        }
        //        else
        //        {
        //            return config.AppSettings.Settings["SAP_VERSION"].Value;
        //        }
        //    }
        //}

        //#endregion

        #region " PostTimePair "
        public override void PostTimePair(List<TimePair> Data)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            UPLOADINFOTYPE6 oFunction = new UPLOADINFOTYPE6();
            oFunction.Connection = oConnection;

            ZHRHRS001Table Updatetab = new ZHRHRS001Table();
            ZHRHRS001Table Updatetab_Ret = new ZHRHRS001Table();
            TimePair pair;
            String DayAssignment = String.Empty;
            for (int index = 0; index < Data.Count; index++)
            {
                DayAssignment = String.Empty;
                pair = Data[index];
                pair.Remark = "";
                if (pair.ClockIN != null && !pair.PostIN)
                {
                    #region " CLOCKIN "
                    ZHRHRS001 item = new ZHRHRS001();
                    item.Begda = pair.Date.ToString("yyyyMMdd", oCL);//date
                    item.Endda = pair.Date.ToString("yyyyMMdd", oCL); //date

                    item.Infty = "2011";
                    item.Msg = "";
                    item.Msgtype = "";
                    item.Objps = "";
                    item.Operation = "INS";
                    item.Pernr = pair.EmployeeID;
                    item.Reqnr = Guid.NewGuid().ToString();
                    item.Seqnr = "";
                    item.Status = "N";
                    item.Subty = "";
                    item.T01 = pair.ClockIN.EventTime.TimeOfDay == new TimeSpan() ? "240000" : pair.ClockIN.EventTime.ToString("HHmmss");  //time
                    item.T02 = "P10"; //time event type
                    item.T03 = DayAssignment = pair.ClockIN.EventTime.ToString("ddMMyyyy") != pair.Date.ToString("ddMMyyyy") ? "-" : string.Empty;//day assignment    
                    item.T04 = ""; //pair.ClockIN.DoorType == DoorType.IN_OT ? "8" : String.Empty;
                    //mark seqno
                    item.T05 = pair.ClockIN.DoorType == DoorType.IN_OT ? "8" : String.Empty;
                    item.T06 = index.ToString();

                    Updatetab.Add(item);
                    #endregion
                }
                if (pair.ClockOUT != null && !pair.PostOUT)
                {
                    #region " ClockOUT "
                    ZHRHRS001 item = new ZHRHRS001();
                    item.Begda = pair.ClockOUT.EventTime.ToString("yyyyMMdd", oCL);
                    item.Endda = pair.ClockOUT.EventTime.ToString("yyyyMMdd", oCL);
                    item.Infty = "2011";
                    item.Msg = "";
                    item.Msgtype = "";
                    item.Objps = "";
                    item.Operation = "INS";
                    item.Pernr = pair.EmployeeID;
                    item.Reqnr = Guid.NewGuid().ToString();
                    item.Seqnr = "";
                    item.Status = "N";
                    item.Subty = "";
                    item.T01 = pair.ClockOUT.EventTime.TimeOfDay == new TimeSpan() ? "240000" : pair.ClockOUT.EventTime.ToString("HHmmss");
                    item.T02 = "P20";
                    item.T03 = DayAssignment;
                    item.T04 = "";// pair.ClockOUT.DoorType == DoorType.OUT_OT ? "9" : String.Empty;
                    //mark seqno
                    item.T05 = pair.ClockOUT.DoorType == DoorType.OUT_OT ? "9" : String.Empty;
                    item.T06 = index.ToString();

                    Updatetab.Add(item);
                    #endregion
                }
                pair.PostIN = true;
                pair.PostOUT = true;
            }

            oFunction.Zhrhri001(ref Updatetab, ref Updatetab_Ret);

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            DataTable oResult = Updatetab_Ret.ToADODataTable();
            DataView oDV = new DataView(oResult);
            bool lError;
            oDV.RowFilter = "MSGTYPE = 'E'";
            lError = oDV.Count > 0;
            foreach (DataRowView drv in oDV)
            {
                int index = int.Parse((string)drv["T06"]);
                bool isClockIN = ((string)drv["T02"]).Trim() == "P10";
                if (isClockIN)
                {
                    Data[index].PostIN = false;
                }
                else
                {
                    Data[index].PostOUT = false;
                }
                Data[index].Remark += ((string)drv["MSG"]).Trim() + ", ";
            }
            oDV.Dispose();
            oResult.Dispose();
            if (lError)
            {
                string strError = string.Empty;
                foreach (TimePair tp in Data)
                    if (!string.IsNullOrEmpty(tp.Remark))
                        strError += string.Format("{0} {1}\r\n", tp.EmployeeID, tp.Remark);
                throw new TimePairPostingException("Can't post time event\r\n" + strError);
            }
        }
        #endregion

        //Add By Apirat.c
        #region " CARDSETTING "

        public override List<CardSetting> LoadCardSetting(DateTime BeginDate)
        {
            System.Globalization.CultureInfo oCL = new CultureInfo("en-US");

            List<CardSetting> oReturn = new List<CardSetting>();
            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnection;

            TAB512Table Data = new TAB512Table();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();

            RFC_DB_FLD fld;
            RFC_DB_OPT opt;


            #region "Fields"
            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ZAUSW";
            Fields.Add(fld);


            #endregion

            #region " Options "
            opt = new RFC_DB_OPT();
            opt.Text = "PERNR >= '23000000' AND PERNR <= '23999999'";
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND BEGDA <= '{0}' AND ENDDA >= '{0}'", BeginDate.ToString("yyyyMMdd", oCL));
            Options.Add(opt);
            #endregion

            oFunction.Rfc_Read_Table("^", "", "PA0050", 0, 0, ref Data, ref Fields, ref Options);



            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            #region " Parse Object "
            CardSetting data;

            foreach (TAB512 item in Data)
            {
                data = new CardSetting();
                string[] arrTemp = item.Wa.Split('^');
                //fld.Fieldname = "PERNR";
                data.EmployeeID = arrTemp[0].Trim();
                //fld.Fieldname = "BEGDA";
                data.BeginDate = DateTime.ParseExact(arrTemp[1], "yyyyMMdd", oCL);
                //fld.Fieldname = "ENDDA";
                data.EndDate = DateTime.ParseExact(arrTemp[2], "yyyyMMdd", oCL);
                //fld.Fieldname = "ZAUSW";
                data.CardNo = arrTemp[3].Substring(2);

                oReturn.Add(data);
            }

            #endregion

            return oReturn;
        }

        #endregion

    }
}
