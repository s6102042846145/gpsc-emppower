﻿using ESS.API.UTILITY;
using ESS.API.DATACLASS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Filters;
using System.Web.Script.Serialization;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using System.Json;
using ESS.WORKFLOW;
using ESS.SHAREDATASERVICE;

namespace ESS.API.FILTERS
{
    public class ApiLogRequestAttribute : System.Web.Http.Filters.ActionFilterAttribute
    {
        private DateTime startExecute;
        private static string ModuleID = "ESS.API";
        private string CompanyCode { get; set; }
        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }
        /// <summary>
        /// For Web API controllers
        /// </summary>
        /// <param name="actionExecutedContext"></param>
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            base.OnActionExecuted(actionExecutedContext);
            try
            {
                APIUtility util = new APIUtility();
                ApiLog log = new ApiLog();
                var serializer = new JavaScriptSerializer();
                serializer.MaxJsonLength = Int32.MaxValue;
                base.OnActionExecuted(actionExecutedContext);

                var request = actionExecutedContext.Request;
                var response = actionExecutedContext.Response;
                var actionContext = actionExecutedContext.ActionContext;

                var obj = actionContext.ActionArguments.Values.ToList();
                //string jsonReq = obj.Count>0? new JavaScriptSerializer().Serialize(obj[0]):"";
                string jsonReq = obj.Count > 0 ? serializer.Serialize(obj[0]) : "";
                var objRes = (response != null) ? ((System.Net.Http.ObjectContent)response.Content).Value : null;


                // For simplicity just use Int32's max value.
                // You could always read the value from the config section mentioned above.

                var jsonRes = serializer.Serialize(objRes);


                var splitUri = request.RequestUri.AbsoluteUri.Split('/');
                var splitPara = request.RequestUri.AbsoluteUri.Split('?');
                int countUrl = splitUri.Length;
                log.ResponseDateTime = DateTime.Now;
                log.RequestDateTime = startExecute;
                log.ResponseText = util.ReplaceJsonDataFile(jsonRes, "filedata", "");
                log.RequestUrl = request.RequestUri.AbsoluteUri;
                log.GroupAPI = splitUri.Length > 1 ? splitUri[countUrl - 2] : "";
                log.APIName = splitUri[countUrl - 1].Split('?')[0];
                log.MethodType = request.Method.ToString();

                if (log.MethodType == "POST")
                {
                    log.RequestParameter = util.ReplaceJsonDataFile(jsonReq, "filedata", "");
                    log.RequestParameter = util.ReplaceJsonDataFile(log.RequestParameter, "password", "****");
                    log.EmployeeID = WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID;

                }
                else
                {
                    log.RequestParameter = splitPara.Length > 1 ? splitPara[1].Replace('&', ',') : "";
                    log.EmployeeID = WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID;
                }
                APIManagement.CreateInstance(CompanyCode).InsertAPILog(log);
            }
            catch (Exception ex)
            {
                string message = ex.ToString();
            }

        }

        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            base.OnActionExecuting(actionContext);
            startExecute = DateTime.Now;
        }

    }
}
