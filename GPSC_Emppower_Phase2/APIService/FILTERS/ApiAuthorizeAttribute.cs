﻿using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;
using System.Web.Http.Results;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using ESS.API.DATACLASS;
using ESS.SHAREDATASERVICE;
using ESS.API.UTILITY;

namespace ESS.API.FILTERS
{

    /// <summary>
    /// 
    /// </summary>
    public class ApiAuthorizeAttribute : Attribute, IAuthenticationFilter
    {
        private static string ModuleID = "ESS.API";
        private string CompanyCode { get; set; }
        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }

        private static Dictionary<string, string> allowedApps = new Dictionary<string, string>();
        private readonly string authenticationScheme = "Bearer";
        /// <summary>
        /// 
        /// </summary>
        public ApiAuthorizeAttribute()
        {
            if (allowedApps.Count == 0)
            {
                allowedApps.Add("4d53bce03ec34c0a911182d4c228ee6c", "A93reRTUJHsCuQSHR+L3GxqOJyDmQpCgps102ciuabc=");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            var req = context.Request;
            DateTime startDate = DateTime.Now;
            APIUtility util = new APIUtility();
            ApiLog log = new ApiLog();
            string APIControllerName = HttpContext.Current.Request.RequestContext.RouteData.Values["controller"].ToString();
            string APIActionName = HttpContext.Current.Request.RequestContext.RouteData.Values["action"].ToString();
            var splitUri = req.RequestUri.AbsoluteUri.Split('/');
            var splitPara = req.RequestUri.AbsoluteUri.Split('?');
            int countUrl = splitUri.Length;
            log.EmployeeID = "";
            log.ResponseDateTime = DateTime.Now;
            log.RequestDateTime = startDate;
            log.RequestUrl = req.RequestUri.AbsoluteUri;
            log.GroupAPI = APIControllerName;
            log.APIName = APIActionName;
            log.MethodType = req.Method.ToString();
            var requestBody = context.ActionContext.Request.Content.ReadAsStringAsync();
            string VALIDATE_API_TOKEN = string.Empty;
            string VALIDATE_API_ACCESS = string.Empty;
            if (log.MethodType == "POST")
            {
                log.RequestParameter = requestBody.Result;
                Employee employee = new Employee();
                if (!string.IsNullOrEmpty(log.RequestParameter))
                {
                    employee = util.GetEmployeeDataFromJson(log.RequestParameter);
                    log.EmployeeID = employee.EmployeeID;
                    CompanyCode = employee.CompanyCode;
                    VALIDATE_API_TOKEN = ShareDataManagement.LookupCache(CompanyCode, "ESS.API", "VALIDATE_API_TOKEN");
                    VALIDATE_API_ACCESS = ShareDataManagement.LookupCache(CompanyCode, "ESS.API", "VALIDATE_API_ACCESS");
                }
            }
            else
            {

                //if (!string.IsNullOrEmpty(log.RequestParameter))
                //{
                //    log.RequestParameter = splitPara.Length > 1 ? splitPara[1].Replace('&', ',') : "";

                //    log.EmployeeID = util.GetEmployeeFromParameter(log.RequestParameter);
                //}
            }

            if (!string.IsNullOrEmpty(VALIDATE_API_TOKEN) && VALIDATE_API_TOKEN.ToLower() == "true")
            {
                if (req.Headers.Authorization == null)
                {
                    context.ErrorResult = new UnauthorizedResult(new AuthenticationHeaderValue[0], context.Request);
                    log.ResponseText = "401-Unauthorized : Authorization header is null ";
                    APIManagement.CreateInstance(CompanyCode).InsertAPILog(log);

                }
                else
                {
                    if (req.Headers.Authorization != null && authenticationScheme.Equals(req.Headers.Authorization.Scheme, StringComparison.OrdinalIgnoreCase))
                    {
                        var rawAuthzHeader = req.Headers.Authorization.Parameter;
                        var autherizationHeaderArray = GetAutherizationHeaderValues(rawAuthzHeader);
                        if (autherizationHeaderArray != null)
                        {
                            var appId = autherizationHeaderArray[0];
                            var appKey = autherizationHeaderArray[1];
                            ApiUser user = APIManagement.CreateInstance(CompanyCode).AuthorizeCheckToken(appId + ":" + appKey);
                            if (!string.IsNullOrEmpty(user.Token) && user.Active)
                            {
                                var indentity = new GenericIdentity(user.UserName, "Bearer");
                                var principal = new GenericPrincipal(indentity, null);
                                HttpContext.Current.User = principal;
                                string dbtoken = user.Token.Split(':')[0];
                                byte[] data = Convert.FromBase64String(dbtoken);
                                DateTime when = DateTime.FromBinary(BitConverter.ToInt64(data, 0));

                                if (when < DateTime.UtcNow.AddMinutes(-1 * user.TimeLimit))
                                {
                                    context.ErrorResult = new UnauthorizedResult(new AuthenticationHeaderValue[0], context.Request);
                                    log.ResponseText = "401-Unauthorized : Token timeout ";
                                    APIManagement.CreateInstance(CompanyCode).InsertAPILog(log);
                                }
                                else
                                {
                                    var currentPrincipal = new GenericPrincipal(new GenericIdentity(appId), null);
                                    context.Principal = currentPrincipal;
                                }

                            }
                            else
                            {
                                context.ErrorResult = new UnauthorizedResult(new AuthenticationHeaderValue[0], context.Request);
                                log.ResponseText = "401-Unauthorized : Invalid token ";
                                APIManagement.CreateInstance(CompanyCode).InsertAPILog(log);

                            }

                            //Validate User access api service
                            ApiService oApiService = new ApiService();
                            ApiAccess oApiAccess = new ApiAccess();

                            //check api service name is active?
                            if (!string.IsNullOrEmpty(VALIDATE_API_ACCESS) && VALIDATE_API_ACCESS.ToLower() == "true")
                            {
                                APIActionName = HttpContext.Current.Request.RequestContext.RouteData.Values["action"].ToString();
                                oApiService = APIManagement.CreateInstance(CompanyCode).GetAPIServiceByName(APIActionName);
                                if (!string.IsNullOrEmpty(oApiService.APIName) && oApiService.Active)  // check authen api from DB
                                {
                                    oApiAccess = APIManagement.CreateInstance(CompanyCode).GetAPIAccess(user.UserID, oApiService.API_ID);
                                    if (!oApiAccess.Active)
                                    {
                                        context.ErrorResult = new UnauthorizedResult(new AuthenticationHeaderValue[0], context.Request);
                                        log.ResponseText = "401-Unauthorized : User dose not access api, Please contact staft";
                                        APIManagement.CreateInstance(CompanyCode).InsertAPILog(log);
                                    }

                                }
                            }
                        }
                        else
                        {
                            context.ErrorResult = new UnauthorizedResult(new AuthenticationHeaderValue[0], context.Request);
                            log.ResponseText = "401-Unauthorized : Token is requied ";
                            APIManagement.CreateInstance(CompanyCode).InsertAPILog(log);

                        }
                    }
                }
            }
            else if (string.IsNullOrEmpty(VALIDATE_API_TOKEN))
            {
                context.ErrorResult = new UnauthorizedResult(new AuthenticationHeaderValue[0], context.Request);
                log.ResponseText = "401-Unauthorized : Token is requied ";
                APIManagement.CreateInstance(CompanyCode).InsertAPILog(log);
            }
            return Task.FromResult(0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            context.Result = new ResultWithChallenge(context.Result);
            return Task.FromResult(0);
        }

        /// <summary>
        /// 
        /// </summary>
        public bool AllowMultiple
        {
            get { return false; }
        }

        private string[] GetAutherizationHeaderValues(string rawAuthzHeader)
        {

            var credArray = rawAuthzHeader.Split(':');

            if (credArray.Length == 2)
            {
                return credArray;
            }
            else
            {
                return null;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ResultWithChallenge : IHttpActionResult
    {
        private readonly string authenticationScheme = "apiKey";
        private readonly IHttpActionResult next;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="next"></param>
        public ResultWithChallenge(IHttpActionResult next)
        {
            this.next = next;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            var response = await next.ExecuteAsync(cancellationToken);

            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                response.Headers.WwwAuthenticate.Add(new AuthenticationHeaderValue(authenticationScheme));
            }

            return response;
        }

    }
}
