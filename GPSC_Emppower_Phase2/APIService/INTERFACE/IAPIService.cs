﻿using ESS.API.DATACLASS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.API.INTERFACE
{
  public  interface IAPIService
    {
        List<ApiService> GetAPIServiceAll();
        ApiService GetAPIServiceByName(string apiName);
        ApiAccess GetAPIAccess(int userID, int apiID);
        void InsertAPILog(ApiLog model);

        ApiConfig GetAPIConfig(string KeyName);

        ApiUser Authorize(string username, string password);
        ApiUser AuthorizeCheckToken(string token);
        ApiUser GetAPIUser(string username);
        ApiUser GetRSAPublicKey(string userName);
        void UpdateAPIUser(ApiUser model);
        void UpdateUserRsaPublicKey(ApiUser model);

        //string JWTGenerator();
        //string SimpleToken();
    }
}
