﻿using ESS.API;
using ESS.API.DATACLASS;
using ESS.API.INTERFACE;
using ESS.SHAREDATASERVICE;
using log4net;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IdentityModel.Tokens.Jwt;
using System.Json;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace ESS.API.UTILITY
{
    public class APIUtility
    {
        public string GetEmployeeFromParameter(string data)
        {
            string result = "";
            var splitParameter = data.Split(',');
            foreach (var item in splitParameter)
            {
                if (item.ToLower().IndexOf("employeeid") > -1)
                {
                    var para = item.Split('=');
                    result = para.Length > 1 ? para[1] : "";
                    break;
                }
            }

            return result;
        }

        public string GetValueFromParameter(string data, string key)
        {
            string result = "";
            var splitParameter = data.Split(',');
            foreach (var item in splitParameter)
            {
                if (item.ToLower().IndexOf(key) > -1)
                {
                    var para = item.Split('=');
                    result = para.Length > 1 ? para[1] : "";
                    break;
                }
            }

            return result;
        }
        public string ReplaceValueFromParameter(string data, string key, string NewValue)
        {
            string result = "";
            var splitParameter = data.Split(',');
            foreach (var item in splitParameter)
            {
                if (item.ToLower().IndexOf(key) > -1)
                {
                    var para = item.Split('=');
                    if (result == "")
                        result = para.Length > 1 ? para[0] + "=" + NewValue : item;
                    else
                        result = result + "," + (para.Length > 1 ? para[0] + "=" + NewValue : item);
                }
                else
                {
                    if (result == "")
                        result = item;
                    else
                        result = result + "," + item;
                }
            }

            return result;
        }
        public string GetEmployeeFromJson(string json)
        {
            string result = "";
            var jso = (JsonObject)JsonObject.Parse(json);
            //  jso.Where(a=>a.)
            foreach (var item in jso)
            {
                if (item.Key.ToLower() == "currentemployee")
                {
                    result = item.Value["EmployeeID"];
                    break;
                }
            }
            return result;
        }
        public Employee GetEmployeeDataFromJson(string json)
        {
            Employee result = new Employee();
            var jso = (JsonObject)JsonObject.Parse(json);
            foreach (var item in jso)
            {
                if (item.Key.ToLower() == "currentemployee")
                {
                    result.EmployeeID = item.Value["EmployeeID"];
                    result.CompanyCode = item.Value["CompanyCode"];
                    break;
                }
            }
            return result;
        }
        public string GetEmployeeCompanyFromJson(string json)
        {
            string result = "";
            var jso = (JsonObject)JsonObject.Parse(json);
            //  jso.Where(a=>a.)
            foreach (var item in jso)
            {
                if (item.Key.ToLower() == "currentemployee")
                {
                    result = item.Value["EmployeeID"];
                    break;
                }
            }
            return result;
        }
        public string ReplaceJsonDataFile(string json, string key, string newValue)
        {
            var jso = (JsonObject)JsonObject.Parse(json);

            foreach (var item in jso)
            {
                if (item.Key.ToLower() == key)
                {
                    json = json.Replace(item.Value.ToString().Replace(" ", ""), newValue);
                }
                else
                {
                    if (item.Value != null && item.Value.GetType().Name == "JsonObject")
                    {
                        json = ReplaceJsonstring(item.Value, json, key, newValue);
                    }
                    else
                    {
                        if (item.Key.ToLower() == key)
                        {
                            json = json.Replace(item.Value.ToString().Replace(" ", ""), newValue);
                        }
                    }
                }
            }
            return json;
        }
        private string ReplaceJsonstring(object jsonObjec, string json, string key, string newValue)
        {

            var obj = (JsonObject)jsonObjec;
            foreach (var item in obj)
            {
                if (item.Key.ToLower() == key)
                {
                    string oldValue = item.Value.ToString().Replace(" ", "");
                    json = json.Replace(oldValue, newValue);
                }
                else
                {
                    if (item.Value != null && item.Value.GetType().Name == "JsonObject")
                    {
                        json = ReplaceJsonstring(item.Value, json, key, newValue);
                    }
                    else
                    {
                        if (item.Key.ToLower() == key)
                        {
                            json = json.Replace(item.Value, newValue);
                        }
                    }
                }
            }

            return json;
        }
        public string JWTGenerator(ApiUser user)
        {
            try
            {
                // authentication successful so generate jwt token
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes("GPSCEMPOWWERAPIS-rKE8ZV3mQWKT2i97uP2K3e233rWucG"); //_appSettings.Secret);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim(ClaimTypes.Name, user.UserName)    // user.UserName)
                    }),
                    IssuedAt = DateTime.UtcNow,
                    Expires = DateTime.UtcNow.AddMinutes(user.TimeLimit),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);
                return tokenHandler.WriteToken(token);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public string SimpleToken()
        {
            //string token = "";
            byte[] time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
            byte[] key = Guid.NewGuid().ToByteArray();
            string token = Convert.ToBase64String(time.Concat(key).ToArray());
            return token;
        }
        public string EncryptData(string userID, string data)
        {
            var enData = Encoding.UTF8.GetBytes(data);
            var user = APIManagement.CreateInstance("").GetAPIUser(userID);
            using (var rsa = new RSACryptoServiceProvider(512))
            {

                try
                {
                    rsa.FromXmlString(user.RSAPublicKey);
                    var encryptedData = rsa.Encrypt(enData, true);

                    var base64Encrypted = Convert.ToBase64String(encryptedData);
                    return base64Encrypted;

                }
                finally
                {
                    rsa.PersistKeyInCsp = false;
                }

            }
        }
        public string DecriptData(string data, string pKey)
        {
            using (var rsa = new RSACryptoServiceProvider(512))
            {
                try
                {

                    rsa.FromXmlString(pKey.Replace(@"\r\n", @"").Replace(@"\", @""));

                    var resultBytes = Convert.FromBase64String(data);
                    var decryptedBytes = rsa.Decrypt(resultBytes, true);
                    var decryptedData = Encoding.UTF8.GetString(decryptedBytes);

                    return decryptedData;

                }
                finally
                {
                    rsa.PersistKeyInCsp = false;
                }

            }
        }



    }
}
