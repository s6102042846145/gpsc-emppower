﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using ESS.API.INTERFACE;
using ESS.SHAREDATASERVICE;

namespace ESS.API
{
    public class ServiceManager
    {
        #region Constructor
        private ServiceManager()
        {

        }
        #endregion

        #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        private static Dictionary<string, ServiceManager> Cache = new Dictionary<string, ServiceManager>();

        private static string ModuleID = "ESS.API";

        public string CompanyCode { get; set; }
        private string ESSCONNECTOR
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ESSCONNECTOR");
            }
        }
        public static ServiceManager CreateInstance(string oCompanyCode)
        {
            ServiceManager oServiceManager = new ServiceManager()
            {
                CompanyCode = oCompanyCode
            };
            return oServiceManager;
        }


        #endregion MultiCompany  Framework

        public IAPIService ESSData
        {
            get
            {
                Type oType = GetService(ESSCONNECTOR, "APIServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IAPIService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }


        #region " privatedata "

        private Type GetService(string Mode, string ClassName)
        {
            Type oReturn = null;
            Assembly oAssembly;
            string assemblyName = string.Format("{0}.{1}", ModuleID, Mode.ToUpper());
            string typeName = string.Format("{0}.{1}.{2}", ModuleID, Mode.ToUpper(), ClassName);// CultureInfo.CurrentCulture.TextInfo.ToTitleCase(ClassName.ToLower()));
            oAssembly = Assembly.Load(assemblyName);    // Load assembly (dll)
            oReturn = oAssembly.GetType(typeName);      // Load class
            return oReturn;
        }
        #endregion " privatedata "
    }
    //public class ServiceManager
    //{
    //    #region Constructor
    //    private ServiceManager()
    //    {

    //    }
    //    #endregion 

    //    #region MultiCompany  Framework
    //    private static string ModuleID = "ESS.API";


    //    public string CompanyCode { get; set; }

    //    public static ServiceManager CreateInstance(string oCompanyCode)
    //    {
    //        ServiceManager oServiceManager = new ServiceManager()
    //        {
    //            CompanyCode = oCompanyCode
    //        };
    //        return oServiceManager;
    //    }
    //    #endregion MultiCompany  Framework

    //    #region Member
    //    private string ESSCONNECTOR
    //    {
    //        get
    //        {
    //            return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ESSCONNECTOR");
    //        }
    //    }

    //    private string ERPCONNECTOR
    //    {
    //        get
    //        {
    //            return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ERPCONNECTOR");
    //        }
    //    }

    //    #endregion Member

    //    #region Properties

    //    #endregion Properties

    //    #region Function

    //    private Type GetService(string Mode)
    //    {
    //        Type oReturn = null;
    //        Assembly oAssembly;
    //        string assemblyName = string.Format("{0}.{1}", ModuleID, Mode.ToUpper());
    //        string typeName = string.Format("{0}.{1}.APIServiceImpl", ModuleID, Mode.ToUpper());
    //        oAssembly = Assembly.Load(assemblyName);
    //        oReturn = oAssembly.GetType(typeName);
    //        return oReturn;
    //    }

    //    private Type GetService(string Mode, string ClassName)
    //    {
    //        Type oReturn = null;
    //        Assembly oAssembly;
    //        string assemblyName = string.Format("{0}.{1}", ModuleID, Mode.ToUpper());
    //        string typeName = string.Format("{0}.{1}.{2}", ModuleID, Mode.ToUpper(), ClassName); //
    //        oAssembly = Assembly.Load(assemblyName);    // Load assembly (dll)
    //        oReturn = oAssembly.GetType(typeName);      // Load class
    //        return oReturn;
    //    }

    //    public IAPIService ESSData
    //    {
    //        get
    //        {
    //            IAPIService service;
    //            Type oType = GetService(ESSCONNECTOR);
    //            service = (IAPIService)Activator.CreateInstance(oType, CompanyCode);
    //            //service.CompanyCode = CompanyCode;
    //            return service;
    //        }
    //    }

    //    public IAPIService ESSConfig
    //    {
    //        get
    //        {
    //            IAPIService service;
    //            Type oType = GetService(ESSCONNECTOR);
    //            service = (IAPIService)Activator.CreateInstance(oType, CompanyCode);
    //            //service.CompanyCode = CompanyCode;
    //            return service;
    //        }
    //    }

    //    #endregion Function
    //}
}
