﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.API.DATACLASS
{
   public class Employee
    {
        public string CompanyCode { get; set; }
        public string EmployeeID { get; set; }

        public string UserRoles { get; set; }

    }
}
