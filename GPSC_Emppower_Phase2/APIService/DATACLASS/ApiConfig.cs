﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.API.DATACLASS
{
  public  class ApiConfig
    {
        public int ConfigID { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public bool Active { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}
