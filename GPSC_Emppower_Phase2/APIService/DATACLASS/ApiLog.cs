﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.API.DATACLASS
{
   public class ApiLog
    {
        public int LogID { get; set; }
        public string EmployeeID { get; set; }
        public DateTime RequestDateTime { get; set; }
        public DateTime ResponseDateTime { get; set; }
        public string GroupAPI { get; set; }
        public string APIName { get; set; }
        public string MethodType { get; set; }
        public string RequestParameter { get; set; }
        public string RequestUrl { get; set; }
        public string ResponseText { get; set; }
    }
}
