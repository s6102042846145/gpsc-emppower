﻿using ESS.API.DATACLASS;
using ESS.SHAREDATASERVICE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.API
{

    public class APIManagement
    {
        #region Constructor
        private APIManagement()
        {

        }
        #endregion

        #region MultiCompany  Framework
        // private static string ModuleID = "ESS.HR.API";

        public string CompanyCode { get; set; }

        public static APIManagement CreateInstance(string oCompanyCode)
        {
            APIManagement oAPIManagement = new APIManagement()
            {
                CompanyCode = oCompanyCode
            };

            return oAPIManagement;
        }

        #endregion MultiCompany  Framework
        public ApiUser Authorize(string username, string password)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.Authorize(username, password);
        }
        public ApiUser AuthorizeCheckToken(string token)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.AuthorizeCheckToken(token);
        }
        public List<ApiService> GetAPIServiceAll()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetAPIServiceAll();
        }
        public ApiService GetAPIServiceByName(string apiName)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetAPIServiceByName(apiName);
        }

        public ApiAccess GetAPIAccess(int userID, int apiID)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetAPIAccess(userID, apiID);
        }
        public ApiUser GetAPIUser(string username)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetAPIUser(username);
        }
        public ApiUser GetRSAPublicKey(string userName)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetRSAPublicKey(userName);
        }
        public ApiConfig GetAPIConfig(string keyName)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.GetAPIConfig(keyName);
        }
        public void InsertAPILog(ApiLog model)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.InsertAPILog(model);
        }
        public void UpdateAPIUser(ApiUser model)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.UpdateAPIUser(model);
        }

        public void UpdateUserRsaPublicKey(ApiUser model)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.UpdateUserRsaPublicKey(model);
        }


    }
}
