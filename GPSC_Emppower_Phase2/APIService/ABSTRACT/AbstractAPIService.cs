﻿using ESS.API.DATACLASS;
using ESS.API.INTERFACE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.API.ABSTRACT
{
    public class AbstractAPIService : IAPIService
    {
        public virtual List<ApiService> GetAPIServiceAll()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual ApiService GetAPIServiceByName(string apiName)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual ApiAccess GetAPIAccess(int userID, int apiID)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual void InsertAPILog(ApiLog model)
        {
            throw new Exception("The method or operation is not implemented.");
        } 
        public virtual ApiUser Authorize(string username, string password)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual ApiUser AuthorizeCheckToken(string token)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual ApiUser GetAPIUser(string username)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual ApiUser GetRSAPublicKey(string userName)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual ApiConfig GetAPIConfig(string keyName)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual void UpdateAPIUser(ApiUser model)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual void UpdateUserRsaPublicKey(ApiUser model)
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }
}
