using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Reflection;
using PTTCHEM.HR;
using PTTCHEM.EMPLOYEE;

namespace PTTCHEM.DATA.DB
{
    public class HRPADATA:AbstractHRPAService
    {
        #region " private data "
        private static Configuration __config;
        private static Configuration config
        {
            get
            {
                if (__config == null)
                {
                    __config = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().CodeBase.Replace("file:///", "").Replace("/", "\\"));
                }
                return __config;
            }
        }
        private string ConnectionString
        {
            get
            {
                return config.AppSettings.Settings["BaseConnStr"].Value;
            }
        }
        private string GetConnectionString(string profile)
        {
            return config.AppSettings.Settings[profile].Value;
        }
        #endregion

        public HRPADATA()
        {
        }

        #region " GetPersonalData "
        public override INFOTYPE0002 GetPersonalData(string EmployeeID, DateTime CheckDate)
        {
            INFOTYPE0002 returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString("WORKFLOW"));
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE0002 Where EmployeeID = @EmpID and @CheckDate between BeginDate and EndDate", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("EMPLOYEE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            returnValue = new INFOTYPE0002();
            returnValue.ParseToObject(oTable);
            oTable.Dispose();
            return returnValue;
        }
        #endregion


        #region " SaveInfoType0002 "
        public override void SaveInfotype0002(string EmployeeID1, string EmployeeID2, List<INFOTYPE0002> List, string Profile)
        {
            SqlConnection oConnection = new SqlConnection(this.GetConnectionString(Profile));
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand1 = new SqlCommand("delete from INFOTYPE0002 where EmployeeID between @EmpID1 and @EmpID2", oConnection, tx);
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand1.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand1.Parameters.Add(oParam);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE0002 where EmployeeID between @EmpID1 and @EmpID2", oConnection, tx);
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("INFOTYPE0002");
            try
            {
                oCommand1.ExecuteNonQuery();
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                foreach (INFOTYPE0002 item in List)
                {
                    item.LoadDataToTable(oTable);
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand1.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }
        #endregion

        #region " MarkUpdate "
        public override void MarkUpdate(string EmployeeID, string DataCategory, string RequestNo, bool isMarkIn)
        {
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select * from MarkData where EmployeeID = @EmpID and DataCategory = @DataCategory ", oConnection, tx);
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@DataCategory", SqlDbType.VarChar);
            oParam.Value = DataCategory;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("MARKDATA");
            try
            {
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                if (oTable.Rows.Count == 0 && isMarkIn)
                {
                    DataRow oRow = oTable.NewRow();
                    oRow["EmployeeID"] = EmployeeID;
                    oRow["DataCategory"] = DataCategory;
                    oRow["RequestNo"] = RequestNo;
                    oTable.Rows.Add(oRow);
                }
                else if (oTable.Rows.Count > 0 && !isMarkIn)
                {
                    foreach (DataRow dr in oTable.Select())
                    {
                        dr.Delete();
                    }
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }
        #endregion

        #region " CheckMark "
        public override bool CheckMark(string EmployeeID, string DataCategory)
        {
            bool oReturn = false;
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select Count(*) from MarkData where EmployeeID = @EmpID and DataCategory = @DataCategory", oConnection);
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@DataCategory", SqlDbType.VarChar);
            oParam.Value = DataCategory;
            oCommand.Parameters.Add(oParam);
            try
            {
                oConnection.Open();
                oReturn = ((int)oCommand.ExecuteScalar()) > 0;
                oConnection.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("CheckMark error",ex);
            }
            finally
            {
                oConnection.Dispose();
                oCommand.Dispose();
            }
            return oReturn;
        }
        #endregion

        //public override void SavePersonalData(INFOTYPE0002 data)
        //{
        //    SqlConnection oConnection = new SqlConnection(this.GetConnectionString("INFOTYPE0002"));
        //    SqlCommand oCommand = new SqlCommand("select * from INFOTYPE0002 Where EmployeeID = @EmpID and @CheckDate between BeginDate and EndDate", oConnection);
        //    SqlParameter oParam;
        //    oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
        //    oParam.Value = EmployeeID;
        //    oCommand.Parameters.Add(oParam);

        //    oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
        //    oParam.Value = CheckDate;
        //    oCommand.Parameters.Add(oParam);

        //    SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
        //    DataTable oTable = new DataTable("EMPLOYEE");
        //    oConnection.Open();
        //    oAdapter.Fill(oTable);
        //    oConnection.Close();
        //    oConnection.Dispose();
        //    oCommand.Dispose();
        //    returnValue = new INFOTYPE0002();
        //    returnValue.ParseToObject(oTable);
        //    oTable.Dispose();
        //}
    }
}
