using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Reflection;
using PTTCHEM.HR;
using PTTCHEM.HR.CONFIG;

namespace PTTCHEM.DATA.DB
{
    public class HRPACONFIG:AbstractHRPAConfig
    {
        #region " SaveConfig "
        private delegate void GetData<T>(DataTable Table, List<T> Data);
        private delegate List<T> ParseObject<T>(DataTable dt);

        private void SaveConfig<T>(string ConfigName, List<T> Data, GetData<T> Method)
        {
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand;
            SqlTransaction tx;
            SqlDataAdapter oAdapter;
            SqlCommandBuilder oCB;
            DataTable oTable = new DataTable(ConfigName.ToUpper());
            oConnection.Open();
            tx = oConnection.BeginTransaction();
            oCommand = new SqlCommand(string.Format("Delete from {0}",ConfigName), oConnection);
            try
            {
                oCommand.Transaction = tx;
                oCommand.ExecuteNonQuery();
                oCommand.CommandText = string.Format("select * from {0}",ConfigName);
                oCommand.Transaction = tx;
                oAdapter = new SqlDataAdapter(oCommand);
                oCB = new SqlCommandBuilder(oAdapter);
                oAdapter.FillSchema(oTable, SchemaType.Source);
                Method(oTable, Data); 
                oAdapter.Update(oTable);
                tx.Commit();
                oConnection.Close();
            }
            catch (Exception e)
            {
                tx.Rollback();
                throw new Exception(string.Format("Save {0} Error",ConfigName), e);
            }
            finally
            {
                oConnection.Dispose();
                oCommand.Dispose();
                oTable.Dispose();
            }
        }
        private List<T> LoadConfig<T>(string ConfigName, ParseObject<T> Method)
        {
            return LoadConfig<T>(ConfigName, Method, false);
        }
        private List<T> LoadConfig<T>(string ConfigName, ParseObject<T> Method, bool CaseSensitive)
        {
            return LoadConfig<T>(ConfigName, Method, CaseSensitive, "");
        }
        private List<T> LoadConfig<T>(string ConfigName, ParseObject<T> Method, bool CaseSensitive,string LanguageCode)
        {
            bool MultiLanguage = LanguageCode != "";
            List<T> oReturn = new List<T>();
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable(ConfigName.ToUpper());
            oTable.CaseSensitive = CaseSensitive;
            if (MultiLanguage)
            {
                oCommand = new SqlCommand(string.Format("select *,dbo.GetCommonTextItem('{0}',{0}Key,'{1}') as MultiLanguageName from {0}", ConfigName, LanguageCode), oConnection);
            }
            else
            {
                oCommand = new SqlCommand(string.Format("select * from {0}", ConfigName), oConnection);
            }
            oAdapter = new SqlDataAdapter(oCommand);
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            oReturn.AddRange(Method(oTable));
            oTable.Dispose();
            return oReturn;
        }
        private T LoadConfigSingle<T>(string ConfigName, ParseObject<T> Method, string Code)
        {
            return LoadConfigSingle<T>(ConfigName, Method, Code, "");
        }
        private T LoadConfigSingle<T>(string ConfigName,ParseObject<T> Method,string Code,string LanguageCode)
        {
            bool MultiLanguage = LanguageCode != "";
            T oReturn = default(T);
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable(ConfigName);
            oTable.CaseSensitive = false;
            if (MultiLanguage)
            {
                oCommand = new SqlCommand(string.Format("select *,dbo.GetCommonTextItem('{0}',{0}Key,'{1}') as MultiLanguageName from {0} Where {0}Key = @Key", ConfigName, LanguageCode), oConnection);
            }
            else
            {
                oCommand = new SqlCommand(string.Format("select * from {0} Where {0}Key = @Key", ConfigName), oConnection);
            }
            SqlParameter oParam = new SqlParameter("@Key", SqlDbType.VarChar);
            oParam.Value = Code;
            oCommand.Parameters.Add(oParam);
            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            List<T> list = Method(oTable);
            if (list.Count > 0)
            {
                oReturn = list[0];
            }
            oTable.Dispose();
            return oReturn;
        }
        #endregion

        private static Configuration __config;

        #region " Private Data "
        private Configuration config
        {
            get
            {
                if (__config == null)
                {
                    __config = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().CodeBase.Replace("file:///", "").Replace("/", "\\"));
                }
                return __config;
            }
        }
        private string ConnectionString
        {
            get
            {
                if (config == null || config.AppSettings.Settings["BaseConnStr"] == null)
                {
                    return "";
                }
                else
                {
                    return config.AppSettings.Settings["BaseConnStr"].Value;
                }
            }
        }

        #endregion

        #region " Object Parser "

        #region " TitleName "
        private List<TitleName> ParseToTitleName(DataTable dt)
        {
            List<TitleName> oReturn = new List<TitleName>();
            foreach (DataRow dr in dt.Rows)
            {
                TitleName Item = new TitleName();
                Item.Key = (string)dr["TitleNameKey"];
                Item.Description = (string)dr["TitleName"];
                Item.Abbreviation = (string)dr["TitleAbbreviation"];
                oReturn.Add(Item);
            }
            return oReturn;
        }
        #endregion

        #region " PrefixName "
        private List<PrefixName> ParseToPrefixName(DataTable dt)
        {
            List<PrefixName> oReturn = new List<PrefixName>();
            foreach (DataRow dr in dt.Rows)
            {
                PrefixName Item = new PrefixName();
                Item.Key = (string)dr["PrefixNameKey"];
                Item.Description = (string)dr["PrefixName"];
                oReturn.Add(Item);
            }
            return oReturn;
        }
        #endregion

        #region " NameFormat "
        private List<NameFormat> ParseToNameFormat(DataTable dt)
        {
            List<NameFormat> oReturn = new List<NameFormat>();
            Dictionary<string, NameFormat> oDic = new Dictionary<string, NameFormat>();
            NameFormat Item;
            foreach (DataRow dr in dt.Rows)
            {
                string cKey = ((string)dr["NameFormatKey"]).Trim();
                if (!oDic.ContainsKey(cKey))
                {
                    Item = new NameFormat();
                    Item.Key = (string)dr["NameFormatKey"];
                    oDic.Add(Item.Key, Item);
                }
                else
                {
                    Item = oDic[cKey];
                }
                Item.Fields.Add((string)dr["NameFormatField"]);
            }
            oReturn.AddRange(oDic.Values);
            return oReturn;
        }
        #endregion

        #region " Gender "
        private List<Gender> ParseToGender(DataTable dt)
        {
            List<Gender> oReturn = new List<Gender>();
            foreach (DataRow dr in dt.Rows)
            {
                Gender Item = new Gender();
                Item.Key = (string)dr["GenderKey"];
                Item.Description = (string)dr["GenderName"];
                oReturn.Add(Item);
            }
            return oReturn;
        }
        #endregion

        #region " Country "
        private List<Country> ParseToCountry(DataTable dt)
        {
            List<Country> oReturn = new List<Country>();
            foreach (DataRow dr in dt.Rows)
            {
                Country Item = new Country();
                Item.Key = (string)dr["CountryKey"];
                Item.Name = (string)dr["CountryName"];
                Item.Nationality = (string)dr["Nationality"];
                oReturn.Add(Item);
            }
            return oReturn;
        }
        #endregion

        #region " MaritalStatus "
        private List<MaritalStatus> ParseToMaritalStatus(DataTable dt)
        {
            List<MaritalStatus> oReturn = new List<MaritalStatus>();
            foreach (DataRow dr in dt.Rows)
            {
                MaritalStatus Item = new MaritalStatus();
                Item.Key = (string)dr["MaritalStatusKey"];
                if (dr.Table.Columns.Contains("MultiLanguageName"))
                {
                    Item.Description = (string)dr["MultiLanguageName"];
                }
                else
                {
                    Item.Description = (string)dr["MaritalStatusName"];
                }
                oReturn.Add(Item);
            }
            return oReturn;
        }
        #endregion

        #region " Religion "
        private List<Religion> ParseToReligion(DataTable dt)
        {
            List<Religion> oReturn = new List<Religion>();
            foreach (DataRow dr in dt.Rows)
            {
                Religion Item = new Religion();
                Item.Key = (string)dr["ReligionKey"];
                if (dr.Table.Columns.Contains("MultiLanguageName"))
                {
                    Item.Description = (string)dr["MultiLanguageName"];
                }
                else
                {
                    Item.Description = (string)dr["ReligionName"];
                }
                oReturn.Add(Item);
            }
            return oReturn;
        }
        #endregion

        #region " Language "
        private List<Language> ParseToLanguage(DataTable dt)
        {
            List<Language> oReturn = new List<Language>();
            foreach (DataRow dr in dt.Rows)
            {
                Language Item = new Language();
                Item.Key = (string)dr["LanguageKey"];
                Item.Code = (string)dr["LanguageCode"];
                Item.Description = (string)dr["LanguageName"];
                oReturn.Add(Item);
            }
            return oReturn;
        }
        #endregion

        #region " AddressType "
        private List<AddressType> ParseToAddressType(DataTable dt)
        {
            List<AddressType> oReturn = new List<AddressType>();
            foreach (DataRow dr in dt.Rows)
            {
                AddressType Item = new AddressType();
                Item.Key = (string)dr["AddressTypeKey"];
                if (dr.Table.Columns.Contains("MultiLanguageName"))
                {
                    Item.Description = (string)dr["MultiLanguageName"];
                }
                else
                {
                    Item.Description = (string)dr["AddressTypeName"];
                }
                oReturn.Add(Item);
            }
            return oReturn;
        }
        #endregion

        #region " Bank "
        private List<Bank> ParseToBank(DataTable dt)
        {
            List<Bank> oReturn = new List<Bank>();
            foreach (DataRow dr in dt.Rows)
            {
                Bank Item = new Bank();
                Item.Key = (string)dr["BankKey"];
                Item.Description = (string)dr["BankName"];
                oReturn.Add(Item);
            }
            return oReturn;
        }
        #endregion

        #endregion

        #region IHRPAConfig Members

        #region " TitleList "
        public override List<TitleName> GetTitleList()
        {
            return this.LoadConfig<TitleName>("TitleName", new ParseObject<TitleName>(ParseToTitleName));
        }

        public override TitleName GetTitle(string Code)
        {
            return this.LoadConfigSingle<TitleName>("TitleName", new ParseObject<TitleName>(ParseToTitleName), Code);
        }

        public override void SaveTitleList(List<TitleName> data)
        {
            this.SaveConfig<TitleName>("TitleName", data, new GetData<TitleName>(ParseTitleToTable));
        }

        private void ParseTitleToTable(DataTable oTable, List<TitleName> Data)
        {
            foreach (TitleName item in Data)
            {
                DataRow oNewRow = oTable.NewRow();
                oNewRow["TitleNameKey"] = item.Key;
                oNewRow["TitleName"] = item.Description;
                oNewRow["TitleAbbreviation"] = item.Abbreviation;
                oTable.LoadDataRow(oNewRow.ItemArray, false);
            }
        }
        #endregion

        #region " PrefixName "
        public override List<PrefixName> GetPrefixNameList()
        {
            return this.LoadConfig<PrefixName>("PrefixName", new ParseObject<PrefixName>(ParseToPrefixName));
        }

        public override PrefixName GetPrefixName(string Code)
        {
            return this.LoadConfigSingle<PrefixName>("PrefixName", new ParseObject<PrefixName>(ParseToPrefixName), Code);
        }

        public override void SavePrefixNameList(List<PrefixName> data)
        {
            this.SaveConfig<PrefixName>("PrefixName", data, new GetData<PrefixName>(ParsePrefixNameToTable));
        }

        private void ParsePrefixNameToTable(DataTable oTable, List<PrefixName> Data)
        {
            foreach (PrefixName item in Data)
            {
                DataRow oNewRow = oTable.NewRow();
                oNewRow["PrefixNameKey"] = item.Key;
                oNewRow["PrefixName"] = item.Description;
                oTable.LoadDataRow(oNewRow.ItemArray, false);
            }
        }
        #endregion

        #region " NameFormat "
        public override List<NameFormat> GetNameFormatList()
        {
            return this.LoadConfig<NameFormat>("NameFormat", new ParseObject<NameFormat>(ParseToNameFormat));
        }

        public override NameFormat GetNameFormat(string Code)
        {
            return this.LoadConfigSingle<NameFormat>("NameFormat", new ParseObject<NameFormat>(ParseToNameFormat), Code);
        }

        public override void SaveNameFormatList(List<NameFormat> data)
        {
            this.SaveConfig<NameFormat>("NameFormat", data, new GetData<NameFormat>(ParseNameFormatToTable));
        }

        private void ParseNameFormatToTable(DataTable oTable, List<NameFormat> Data)
        {
            foreach (NameFormat item in Data)
            {
                for (int index = 0; index < item.Fields.Count; index++)
                {
                    DataRow oNewRow = oTable.NewRow();
                    oNewRow["NameFormatKey"] = item.Key;
                    oNewRow["NameFormatID"] = index;
                    oNewRow["NameFormatField"] = item.Fields[index];
                    oTable.LoadDataRow(oNewRow.ItemArray, false);
                }
            }
        }
        #endregion

        #region " Gender "
        public override List<Gender> GetGenderList()
        {
            return this.LoadConfig<Gender>("Gender", new ParseObject<Gender>(ParseToGender));
        }

        public override Gender GetGender(string Code)
        {
            return this.LoadConfigSingle<Gender>("Gender", new ParseObject<Gender>(ParseToGender), Code);
        }

        public override void SaveGenderList(List<Gender> data)
        {
            this.SaveConfig<Gender>("Gender", data, new GetData<Gender>(ParseGenderToTable));
        }

        private void ParseGenderToTable(DataTable oTable, List<Gender> Data)
        {
            foreach (Gender item in Data)
            {
                DataRow oNewRow = oTable.NewRow();
                oNewRow["GenderKey"] = item.Key;
                oNewRow["GenderName"] = item.Description;
                oTable.LoadDataRow(oNewRow.ItemArray, false);
            }
        }
        #endregion

        #region " Country "
        public override List<Country> GetCountryList()
        {
            return this.LoadConfig<Country>("Country", new ParseObject<Country>(ParseToCountry));
        }

        public override Country GetCountry(string Code)
        {
            return this.LoadConfigSingle<Country>("Country", new ParseObject<Country>(ParseToCountry), Code);
        }

        public override void SaveCountryList(List<Country> data)
        {
            this.SaveConfig<Country>("Country", data, new GetData<Country>(ParseCountryToTable));
        }

        private void ParseCountryToTable(DataTable oTable,List<Country> Data)
        {
            foreach (Country item in Data)
            {
                DataRow oNewRow = oTable.NewRow();
                oNewRow["CountryKey"] = item.Key;
                oNewRow["CountryName"] = item.Name;
                oNewRow["Nationality"] = item.Nationality;
                oTable.LoadDataRow(oNewRow.ItemArray, false);
            }
        }
        #endregion

        #region " MaritalStatus "
        public override List<MaritalStatus> GetMaritalStatusList(string LanguageCode)
        {
            return this.LoadConfig<MaritalStatus>("MaritalStatus", new ParseObject<MaritalStatus>(ParseToMaritalStatus),false,LanguageCode);
        }

        public override MaritalStatus GetMaritalStatus(string Code)
        {
            return this.LoadConfigSingle<MaritalStatus>("MaritalStatus", new ParseObject<MaritalStatus>(ParseToMaritalStatus), Code);
        }

        public override MaritalStatus GetMaritalStatus(string Code,string LanguageCode)
        {
            return this.LoadConfigSingle<MaritalStatus>("MaritalStatus", new ParseObject<MaritalStatus>(ParseToMaritalStatus), Code, LanguageCode);
        }

        public override void SaveMaritalStatusList(List<MaritalStatus> Data)
        {
            this.SaveConfig<MaritalStatus>("MaritalStatus", Data, new GetData<MaritalStatus>(ParseToTable));
        }

        private void ParseToTable(DataTable oTable, List<MaritalStatus> Data)
        {
            foreach (MaritalStatus item in Data)
            {
                DataRow oNewRow = oTable.NewRow();
                oNewRow["MaritalStatusKey"] = item.Key;
                oNewRow["MaritalStatusName"] = item.Description;
                oTable.LoadDataRow(oNewRow.ItemArray, false);
            }
        }
        #endregion

        #region " Religion "
        public override List<Religion> GetReligionList(string LanguageCode)
        {
            return this.LoadConfig<Religion>("Religion", new ParseObject<Religion>(ParseToReligion), false, LanguageCode);
        }
        public override Religion GetReligion(string Code)
        {
            return this.LoadConfigSingle<Religion>("Religion", new ParseObject<Religion>(ParseToReligion), Code);
        }
        public override Religion GetReligion(string Code,string LanguageCode)
        {
            return this.LoadConfigSingle<Religion>("Religion", new ParseObject<Religion>(ParseToReligion), Code, LanguageCode);
        }

        public override void SaveReligionList(List<Religion> Data)
        {
            this.SaveConfig<Religion>("Religion", Data, new GetData<Religion>(ParseToTable));
        }

        private void ParseToTable(DataTable oTable, List<Religion> Data)
        {
            foreach (Religion item in Data)
            {
                DataRow oNewRow = oTable.NewRow();
                oNewRow["ReligionKey"] = item.Key;
                oNewRow["ReligionName"] = item.Description;
                oTable.LoadDataRow(oNewRow.ItemArray, false);
            }
        }
        #endregion

        #region " Language "

        public override List<Language> GetLanguageList()
        {
            return this.LoadConfig<Language>("Language", new ParseObject<Language>(ParseToLanguage),true);
        }

        public override Language GetLanguage(string Code)
        {
            return this.LoadConfigSingle<Language>("Language", new ParseObject<Language>(ParseToLanguage), Code);
        }

        public override void SaveLanguageList(List<Language> Data)
        {
            this.SaveConfig<Language>("Language", Data, new GetData<Language>(ParseToTable));
        }

        private void ParseToTable(DataTable oTable, List<Language> Data)
        {
            oTable.CaseSensitive = true;
            foreach (Language item in Data)
            {
                DataRow oNewRow = oTable.NewRow();
                oNewRow["LanguageKey"] = item.Key;
                oNewRow["LanguageCode"] = item.Code;
                oNewRow["LanguageName"] = item.Description;
                oTable.LoadDataRow(oNewRow.ItemArray, false);
            }
        }
        #endregion

        #region " AddressType "
        public override List<AddressType> GetAddressTypeList(string LanguageCode)
        {
            return this.LoadConfig<AddressType>("AddressType", new ParseObject<AddressType>(ParseToAddressType), false,LanguageCode);
        }

        //public override MaritalStatus GetMaritalStatus(string Code)
        //{
        //    return this.LoadConfigSingle<MaritalStatus>("MaritalStatus", new ParseObject<MaritalStatus>(ParseToMaritalStatus), Code);
        //}

        //public override MaritalStatus GetMaritalStatus(string Code, string LanguageCode)
        //{
        //    return this.LoadConfigSingle<MaritalStatus>("MaritalStatus", new ParseObject<MaritalStatus>(ParseToMaritalStatus), Code, LanguageCode);
        //}

        public override void SaveAddressTypeList(List<AddressType> Data)
        {
            this.SaveConfig<AddressType>("AddressType", Data, new GetData<AddressType>(ParseToTable));
        }

        private void ParseToTable(DataTable oTable, List<AddressType> Data)
        {
            foreach (AddressType item in Data)
            {
                DataRow oNewRow = oTable.NewRow();
                oNewRow["AddressTypeKey"] = item.Key;
                oNewRow["AddressTypeName"] = item.Description;
                oTable.LoadDataRow(oNewRow.ItemArray, false);
            }
        }
        #endregion

        #region " Bank "
        public override List<Bank> GetBankList()
        {
            return this.LoadConfig<Bank>("Bank", new ParseObject<Bank>(ParseToBank));
        }

        public override Bank GetBank(string Code)
        {
            return this.LoadConfigSingle<Bank>("Bank", new ParseObject<Bank>(ParseToBank), Code);
        }

        public override void SaveBankList(List<Bank> data)
        {
            this.SaveConfig<Bank>("Bank", data, new GetData<Bank>(ParseBankToTable));
        }

        private void ParseBankToTable(DataTable oTable, List<Bank> Data)
        {
            foreach (Bank item in Data)
            {
                DataRow oNewRow = oTable.NewRow();
                oNewRow["BankKey"] = item.Key;
                oNewRow["BankName"] = item.Description;
                oTable.LoadDataRow(oNewRow.ItemArray, false);
            }
        }
        #endregion

        #region " GetFamilyMember "
        public override FamilyMember GetFamilyMember(string Code)
        {
            FamilyMember returnValue = new FamilyMember();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            SqlCommandBuilder oBuilder;
            using (oConnection = new SqlConnection(this.ConnectionString))
            {
                using (oCommand = new SqlCommand("select * from FamilyMember Where [Key] = @key", oConnection))
                {
                    SqlParameter oParam;
                    oParam = new SqlParameter("@key", SqlDbType.VarChar);
                    oParam.Value = Code;
                    oCommand.Parameters.Add(oParam);

                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("FamilyMember");
                    oBuilder = new SqlCommandBuilder(oAdapter);

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        returnValue.ParseToObject(dr);
                        break;
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }
        #endregion

        #region " GetFamilyMemberList "
        public override List<FamilyMember> GetFamilyMemberList()
        {
            List<FamilyMember> returnValue = new List<FamilyMember>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            SqlCommandBuilder oBuilder;
            using (oConnection = new SqlConnection(this.ConnectionString))
            {
                using (oCommand = new SqlCommand("select * from FamilyMember", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("FamilyMember");
                    oBuilder = new SqlCommandBuilder(oAdapter);

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        FamilyMember item = new FamilyMember();
                        item.ParseToObject(dr);
                        returnValue.Add(item);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }
        #endregion

        #region " SaveFamilyMemberList "
        public override void SaveFamilyMemberList(List<FamilyMember> Data)
        {
            SqlConnection oConnection;
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable;
            SqlCommandBuilder oBuilder;
            DataRow dRow;
            using (oConnection = new SqlConnection(this.ConnectionString))
            {
                using(oCommand = new SqlCommand("select * from FamilyMember", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("FamilyMember");
                    oBuilder = new SqlCommandBuilder(oAdapter);
                    oConnection.Open();
                    oAdapter.FillSchema(oTable, SchemaType.Source);
                    oAdapter.Fill(oTable);

                    foreach (FamilyMember item in Data)
                    {

                        dRow = oTable.NewRow();
                        item.LoadDataToTableRow(dRow);
                        oTable.LoadDataRow(dRow.ItemArray, false);
                    }
                    oAdapter.Update(oTable);
                    oConnection.Close();
                }
            }
        }
        #endregion

        #region " GetProvinceList "
        public override List<Province> GetProvinceList()
        {
            List<Province> returnValue = new List<Province>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            SqlCommandBuilder oBuilder;
            using (oConnection = new SqlConnection(this.ConnectionString))
            {
                using (oCommand = new SqlCommand("select * from Province", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("Province");
                    oBuilder = new SqlCommandBuilder(oAdapter);

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        //Province item = new Province();
                        //item.ParseToObject(dr);
                        //returnValue.Add(item);

                        Province Item = new Province();
                        Item.CountryCode = (string)dr["CountryCode"];
                        Item.Key = (string)dr["ProvinceCode"];
                        Item.Description = (string)dr["ProvinceName"];
                        returnValue.Add(Item);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }
        #endregion

        #region " GetProvince "
        public override Province GetProvince(string Code)
        {
            Province returnValue = new Province();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            SqlCommandBuilder oBuilder;
            SqlParameter oParam;
            string param = "";
            if (Code.Length > 2)
            {
                param = "select * from Province Where ProvinceName = @ProvinceName";
                oParam = new SqlParameter("@ProvinceName", SqlDbType.VarChar);
            }
            else
            {
                param = "select * from Province Where ProvinceCode = @ProvinceCode";
                oParam = new SqlParameter("@ProvinceCode", SqlDbType.VarChar);
            }
            using (oConnection = new SqlConnection(this.ConnectionString))
            {
                using (oCommand = new SqlCommand(string.Format("{0}", param), oConnection))
                {
                    oParam.Value = Code;
                    oCommand.Parameters.Add(oParam);

                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("Province");
                    oBuilder = new SqlCommandBuilder(oAdapter);

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        returnValue.CountryCode = (string)dr["CountryCode"];
                        returnValue.Key = (string)dr["ProvinceCode"];
                        returnValue.Description = (string)dr["ProvinceName"];

                        //Province Item = new Province();
                        //Item.CountryCode = (string)dr["CountryCode"];
                        //Item.Key = (string)dr["ProvinceCode"];
                        //Item.Description = (string)dr["ProvinceName"];
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }
        #endregion

        public override Branch GetBranchData(string BranchCode)
        {
            Branch returnValue = new Branch();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            SqlParameter oParam;
            SqlCommandBuilder oBuilder;
            using (oConnection = new SqlConnection(this.ConnectionString))
            {
                using (oCommand = new SqlCommand("select * from Branch where BranchCode = @BranchCode", oConnection))
                {
                    oParam = new SqlParameter("@BranchCode", SqlDbType.VarChar);
                    oParam.Value = BranchCode;
                    oCommand.Parameters.Add(oParam);

                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("Branch");
                    oBuilder = new SqlCommandBuilder(oAdapter);

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        returnValue.ParseToObject(dr);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        #region " GetAllEducationLevel "
        public override List<EducationLevel> GetAllEducationLevel()
        {
            List<EducationLevel> returnValue = new List<EducationLevel>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            using (oConnection = new SqlConnection(this.ConnectionString))
            {
                using (oCommand = new SqlCommand("select * from EducationLevel", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("EducationLevel");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        EducationLevel item = new EducationLevel();
                        item.ParseToObject(dr);
                        returnValue.Add(item);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }
        #endregion

        #region " GetAllCertificate "
        public override List<Certificate> GetAllCertificate()
        {
            List<Certificate> returnValue = new List<Certificate>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            using (oConnection = new SqlConnection(this.ConnectionString))
            {
                using (oCommand = new SqlCommand("select * from Certificate", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("Certificate");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        Certificate item = new Certificate();
                        item.ParseToObject(dr);
                        returnValue.Add(item);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }
        #endregion

        #endregion
        #region " SaveAllEducationLevel "
        public override void SaveAllEducationLevel(List<EducationLevel> list)
        {
            SqlConnection oConnection;
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable;
            SqlCommandBuilder oBuilder;
            DataRow dRow;
            using (oConnection = new SqlConnection(this.ConnectionString))
            {
                using (oCommand = new SqlCommand("select * from EducationLevel", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("EducationLevel");
                    oBuilder = new SqlCommandBuilder(oAdapter);
                    oConnection.Open();
                    oAdapter.FillSchema(oTable, SchemaType.Source);
                    oAdapter.Fill(oTable);

                    foreach (EducationLevel item in list)
                    {
                        dRow = oTable.NewRow();
                        item.LoadDataToTableRow(dRow);
                        oTable.LoadDataRow(dRow.ItemArray, false);
                    }
                    oAdapter.Update(oTable);
                    oConnection.Close();
                }
            }
        }
        #endregion

        #region "SaveAllCertificate"
        public override void SaveAllCertificate(List<Certificate> list)
        {
            SqlConnection oConnection;
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable;
            SqlCommandBuilder oBuilder;
            DataRow dRow;
            using (oConnection = new SqlConnection(this.ConnectionString))
            {
                using (oCommand = new SqlCommand("select * from Certificate", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("Certificate");
                    oBuilder = new SqlCommandBuilder(oAdapter);
                    oConnection.Open();
                    oAdapter.FillSchema(oTable, SchemaType.Source);
                    oAdapter.Fill(oTable);

                    foreach (Certificate item in list)
                    {
                        dRow = oTable.NewRow();
                        item.LoadDataToTableRow(dRow);
                        oTable.LoadDataRow(dRow.ItemArray, false);
                    }
                    oAdapter.Update(oTable);
                    oConnection.Close();
                }
            }
        }
        #endregion

        #region " GetAllVocation "
        public override List<Vocation> GetAllVocation()
        {
            List<Vocation> returnValue = new List<Vocation>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            SqlCommandBuilder oBuilder;
            using (oConnection = new SqlConnection(this.ConnectionString))
            {
                using (oCommand = new SqlCommand("select * from Vocation", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("Vocation");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        Vocation item = new Vocation();
                        item.ParseToObject(dr);
                        returnValue.Add(item);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }
        #endregion

        #region " SaveAllVocation "
        public override void SaveAllVocation(List<Vocation> list)
        {
            SqlConnection oConnection;
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable;
            SqlCommandBuilder oBuilder;
            DataRow dRow;
            using (oConnection = new SqlConnection(this.ConnectionString))
            {
                using (oCommand = new SqlCommand("select * from Vocation", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("Vocation");
                    oBuilder = new SqlCommandBuilder(oAdapter);
                    oConnection.Open();
                    oAdapter.FillSchema(oTable, SchemaType.Source);
                    oAdapter.Fill(oTable);

                    foreach (Vocation item in list)
                    {
                        dRow = oTable.NewRow();
                        item.LoadDataToTableRow(dRow);
                        oTable.LoadDataRow(dRow.ItemArray, false);
                    }
                    oAdapter.Update(oTable);
                    oConnection.Close();
                }
            }
        }
        #endregion

        public override Institute GetInstitute(string InstituteCode)
        {
            Institute returnValue = new Institute();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            using (oConnection = new SqlConnection(this.ConnectionString))
            {
                using (oCommand = new SqlCommand("select * from Institute where InstituteCode = @InstituteCode", oConnection))
                {
                    SqlParameter oParam = new SqlParameter("@InstituteCode",SqlDbType.VarChar);
                    oParam.Value = InstituteCode;
                    oCommand.Parameters.Add(oParam);                

                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("Institute");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        returnValue.ParseToObject(dr);
                        break;
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        #region " GetAllInstitute "
        public override List<Institute> GetAllInstitute()
        {
            List<Institute> returnValue = new List<Institute>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            using (oConnection = new SqlConnection(this.ConnectionString))
            {
                using (oCommand = new SqlCommand("select * from Institute", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("Institute");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        Institute item = new Institute();
                        item.ParseToObject(dr);
                        returnValue.Add(item);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }
        #endregion

        #region " SaveAllInstitute "
        public override void SaveAllInstitute(List<Institute> list)
        {
            SqlConnection oConnection;
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable;
            SqlCommandBuilder oBuilder;
            DataRow dRow;
            using (oConnection = new SqlConnection(this.ConnectionString))
            {
                using (oCommand = new SqlCommand("select * from Institute", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("Institute");
                    oBuilder = new SqlCommandBuilder(oAdapter);
                    oConnection.Open();
                    oAdapter.FillSchema(oTable, SchemaType.Source);
                    oAdapter.Fill(oTable);

                    foreach (Institute item in list)
                    {
                        dRow = oTable.NewRow();
                        item.LoadDataToTableRow(dRow);
                        oTable.LoadDataRow(dRow.ItemArray, false);
                    }
                    oAdapter.Update(oTable);
                    oConnection.Close();
                }
            }
        }
        #endregion

        #region " GetAllHonor "
        public override List<Honor> GetAllHonor()
        {
            List<Honor> returnValue = new List<Honor>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            using (oConnection = new SqlConnection(this.ConnectionString))
            {
                using (oCommand = new SqlCommand("select * from Honor", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("Honor");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        Honor item = new Honor();
                        item.ParseToObject(dr);
                        returnValue.Add(item);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }
        #endregion

        #region " SaveAllHonor "
        public override void SaveAllHonor(List<Honor> list)
        {
            SqlConnection oConnection;
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable;
            SqlCommandBuilder oBuilder;
            DataRow dRow;
            using (oConnection = new SqlConnection(this.ConnectionString))
            {
                using (oCommand = new SqlCommand("select * from Honor", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("Honor");
                    oBuilder = new SqlCommandBuilder(oAdapter);
                    oConnection.Open();
                    oAdapter.FillSchema(oTable, SchemaType.Source);
                    oAdapter.Fill(oTable);

                    foreach (Honor item in list)
                    {
                        dRow = oTable.NewRow();
                        item.LoadDataToTableRow(dRow);
                        oTable.LoadDataRow(dRow.ItemArray, false);
                    }
                    oAdapter.Update(oTable);
                    oConnection.Close();
                }
            }
        }
        #endregion

        #region " GetAllBranch "
        public override List<Branch> GetAllBranch()
        {
            List<Branch> returnValue = new List<Branch>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            using (oConnection = new SqlConnection(this.ConnectionString))
            {
                using (oCommand = new SqlCommand("select * from Branch", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("Branch");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        Branch item = new Branch();
                        item.ParseToObject(dr);
                        returnValue.Add(item);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }
        #endregion

        #region " SaveAllBranch "
        public override void SaveAllBranch(List<Branch> list)
        {
            SqlConnection oConnection;
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable;
            SqlCommandBuilder oBuilder;
            DataRow dRow;
            using (oConnection = new SqlConnection(this.ConnectionString))
            {
                using (oCommand = new SqlCommand("select * from Branch", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("Branch");
                    oBuilder = new SqlCommandBuilder(oAdapter);
                    oConnection.Open();
                    oAdapter.FillSchema(oTable, SchemaType.Source);
                    oAdapter.Fill(oTable);

                    foreach (Branch item in list)
                    {
                        dRow = oTable.NewRow();
                        item.LoadDataToTableRow(dRow);
                        oTable.LoadDataRow(dRow.ItemArray, false);
                    }
                    oAdapter.Update(oTable);
                    oConnection.Close();
                }
            }
        }
        #endregion

        #region " GetAllCompanyBusiness "
        public override List<CompanyBusiness> GetAllCompanyBusiness()
        {
            List<CompanyBusiness> returnValue = new List<CompanyBusiness>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            using (oConnection = new SqlConnection(this.ConnectionString))
            {
                using (oCommand = new SqlCommand("select * from CompanyBusiness", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("CompanyBusiness");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        CompanyBusiness item = new CompanyBusiness();
                        item.ParseToObject(dr);
                        returnValue.Add(item);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }
        #endregion

        #region " SaveAllCompanyBusiness "
        public override void SaveAllCompanyBusiness(List<CompanyBusiness> list)
        {
            SqlConnection oConnection;
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable;
            SqlCommandBuilder oBuilder;
            DataRow dRow;
            using (oConnection = new SqlConnection(this.ConnectionString))
            {
                using (oCommand = new SqlCommand("select * from CompanyBusiness", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("CompanyBusiness");
                    oBuilder = new SqlCommandBuilder(oAdapter);
                    oConnection.Open();
                    oAdapter.FillSchema(oTable, SchemaType.Source);
                    oAdapter.Fill(oTable);

                    foreach (CompanyBusiness item in list)
                    {
                        dRow = oTable.NewRow();
                        item.LoadDataToTableRow(dRow);
                        oTable.LoadDataRow(dRow.ItemArray, false);
                    }
                    oAdapter.Update(oTable);
                    oConnection.Close();
                }
            }
        }
        #endregion

        #region " GetAllJobType "
        public override List<JobType> GetAllJobType()
        {
            List<JobType> returnValue = new List<JobType>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            using (oConnection = new SqlConnection(this.ConnectionString))
            {
                using (oCommand = new SqlCommand("select * from JobType", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("JobType");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        JobType item = new JobType();
                        item.ParseToObject(dr);
                        returnValue.Add(item);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }
        #endregion

        #region " SaveAllJobType "
        public override void SaveAllJobType(List<JobType> list)
        {
            SqlConnection oConnection;
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable;
            SqlCommandBuilder oBuilder;
            DataRow dRow;
            using (oConnection = new SqlConnection(this.ConnectionString))
            {
                using (oCommand = new SqlCommand("select * from JobType", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("JobType");
                    oBuilder = new SqlCommandBuilder(oAdapter);
                    oConnection.Open();
                    oAdapter.FillSchema(oTable, SchemaType.Source);
                    oAdapter.Fill(oTable);

                    foreach (JobType item in list)
                    {
                        dRow = oTable.NewRow();
                        item.LoadDataToTableRow(dRow);
                        oTable.LoadDataRow(dRow.ItemArray, false);
                    }
                    oAdapter.Update(oTable);
                    oConnection.Close();
                }
            }
        }
        #endregion

        #region " GetEducationLevel "
        public override EducationLevel GetEducationLevel(string EducationLevelCode)
        {
            EducationLevel returnValue = new EducationLevel();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            SqlParameter oParam;
            using (oConnection = new SqlConnection(this.ConnectionString))
            {
                using (oCommand = new SqlCommand("select * from EducationLevel where EducationLevelCode = @EducationLevelCode", oConnection))
                {
                    oParam = new SqlParameter("EDUCATIONLEVEL", SqlDbType.VarChar);
                    oParam.Value = EducationLevelCode;
                    oCommand.Parameters.Add(oParam);

                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("EducationLevel");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        returnValue.ParseToObject(dr);
                        break;
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }
        #endregion

        #region " GetCertificateCode "
        public override Certificate GetCertificateCode(string CertificateCode)
        {
            Certificate returnValue = new Certificate();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            SqlParameter oParam;
            using (oConnection = new SqlConnection(this.ConnectionString))
            {
                using (oCommand = new SqlCommand("select * from Certificate where CertificateCode = @CertificateCode", oConnection))
                {
                    oParam = new SqlParameter("@CertificateCode", SqlDbType.VarChar);
                    oParam.Value = CertificateCode;
                    oCommand.Parameters.Add(oParam);

                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("Certificate");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        returnValue.ParseToObject(dr);
                        break;
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }
        #endregion

        #region " GetVocation "
        public override Vocation GetVocation(string VocationCode)
        {
            Vocation returnValue = new Vocation();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            SqlParameter oParam;
            using (oConnection = new SqlConnection(this.ConnectionString))
            {
                using (oCommand = new SqlCommand("select * from Vocation where VocationCode = @VocationCode", oConnection))
                {
                    oParam = new SqlParameter("@VocationCode", SqlDbType.VarChar);
                    oParam.Value = VocationCode;
                    oCommand.Parameters.Add(oParam);

                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("VOCATIONCODE");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        returnValue.ParseToObject(dr);
                        break;
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }
        #endregion

        #region " GetCompanyBusiness "
        public override CompanyBusiness GetCompanyBusiness(string CompanyBusinessCode)
        {
            CompanyBusiness returnValue = new CompanyBusiness();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            SqlParameter oParam;
            using (oConnection = new SqlConnection(this.ConnectionString))
            {
                using (oCommand = new SqlCommand("select * from CompanyBusiness where BusinessCode = @BusinessCode", oConnection))
                {
                    oParam = new SqlParameter("@BusinessCode", SqlDbType.VarChar);
                    oParam.Value = CompanyBusinessCode;
                    oCommand.Parameters.Add(oParam);

                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("COMPANYBUSINESS");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        returnValue.ParseToObject(dr);
                        break;
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }
        #endregion

        public override JobType GetJobType(string JobCode)
        {
            JobType returnValue = new JobType();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            SqlParameter oParam;
            using (oConnection = new SqlConnection(this.ConnectionString))
            {
                using (oCommand = new SqlCommand("select * from JobType where JobCode = @JobCode", oConnection))
                {
                    oParam = new SqlParameter("@JobCode", SqlDbType.VarChar);
                    oParam.Value = JobCode;
                    oCommand.Parameters.Add(oParam);

                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("JOBTYPE");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        returnValue.ParseToObject(dr);
                        break;
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        private List<string> GetAvailableLanguage(int LetterTypeID)
        {
            List<string> returnValue = new List<string>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            using (oConnection = new SqlConnection(this.ConnectionString))
            {
                using (oCommand = new SqlCommand("select * from LetterLanguage where LetterTypeID = @LetterTypeID", oConnection))
                {
                    SqlParameter oParam = new SqlParameter("@LetterTypeID", SqlDbType.Int);
                    oParam.Value = LetterTypeID;
                    oCommand.Parameters.Add(oParam);

                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("LetterType");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        string item = (string)dr["LanguageCode"];
                        returnValue.Add(item);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        private List<LetterTypeField> GetLetterField(int LetterTypeID)
        {
            List<LetterTypeField> returnValue = new List<LetterTypeField>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            using (oConnection = new SqlConnection(this.ConnectionString))
            {
                using (oCommand = new SqlCommand("select * from LetterField where LetterTypeID = @LetterTypeID", oConnection))
                {
                    SqlParameter oParam = new SqlParameter("@LetterTypeID", SqlDbType.Int);
                    oParam.Value = LetterTypeID;
                    oCommand.Parameters.Add(oParam);

                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("LetterField");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        LetterTypeField item = new LetterTypeField();
                        item.ParseToObject(dr);
                        returnValue.Add(item);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }

        #region " GetAllLetterTypes "
        public override List<LetterType> GetAllLetterTypes()
        {
            List<LetterType> returnValue = new List<LetterType>();
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            using (oConnection = new SqlConnection(this.ConnectionString))
            {
                using (oCommand = new SqlCommand("select * from LetterType", oConnection))
                {
                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("LetterType");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        LetterType item = new LetterType();
                        item.ParseToObject(dr);
                        item.LanguageList.AddRange(GetAvailableLanguage(item.LetterTypeID));
                        item.FieldList.AddRange(GetLetterField(item.LetterTypeID));
                        returnValue.Add(item);
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }
        #endregion

        public override LetterType GetLetterType(int LetterTypeID)
        {
            LetterType returnValue = null;
            SqlConnection oConnection;
            SqlCommand oCommand;
            DataTable oTable;
            SqlDataAdapter oAdapter;
            using (oConnection = new SqlConnection(this.ConnectionString))
            {
                using (oCommand = new SqlCommand("select * from LetterType where LetterTypeID = @LetterTypeID", oConnection))
                {
                    SqlParameter oParam = new SqlParameter("@LetterTypeID", SqlDbType.Int);
                    oParam.Value = LetterTypeID;
                    oCommand.Parameters.Add(oParam);

                    oAdapter = new SqlDataAdapter(oCommand);
                    oTable = new DataTable("LetterType");

                    oConnection.Open();
                    oAdapter.Fill(oTable);

                    foreach (DataRow dr in oTable.Rows)
                    {
                        returnValue = new LetterType();
                        returnValue.ParseToObject(dr);
                        returnValue.LanguageList.AddRange(GetAvailableLanguage(returnValue.LetterTypeID));
                        returnValue.FieldList.AddRange(GetLetterField(returnValue.LetterTypeID));
                    }
                    oConnection.Close();
                }
            }
            return returnValue;
        }
    }
}
