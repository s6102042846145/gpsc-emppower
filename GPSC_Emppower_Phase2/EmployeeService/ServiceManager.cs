using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using ESS.EMPLOYEE.INTERFACE;
using ESS.SHAREDATASERVICE;

namespace ESS.EMPLOYEE
{
    public class ServiceManager
    {

        #region Constructor
        private ServiceManager()
        {

        }
        #endregion 
	    #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        //private static Dictionary<string, ServiceManager> Cache = new Dictionary<string, ServiceManager>();
        public string CompanyCode { get; set; }
        public static string ModuleID
        {
            get
            {
                return "ESS.EMPLOYEE";
            }
        }
        public static ServiceManager CreateInstance(string oCompanyCode)
        {
            //if (!Cache.ContainsKey(oCompanyCode))
            //{
            //    Cache[oCompanyCode] = new ServiceManager()
            //    {
            //        CompanyCode = oCompanyCode
            //        ,
            //        Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID)
            //    };
            //}
            //return Cache[oCompanyCode];
            ServiceManager oServiceManager = new ServiceManager()
            {
                CompanyCode = oCompanyCode
            };
            return oServiceManager;
        }
        #endregion MultiCompany  Framework

        public string SOURCEMODE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SOURCEMODE");
            }
        }

        public string SOURCEPROFILE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SOURCEPROFILE");
            }
        }

        public string TARGETMODE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "TARGETMODE");
            }
        }

        public string TARGETPROFILE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "TARGETPROFILE");
            }
        }

        private string ESSCONNECTOR
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ESSCONNECTOR");

            }
        }

        private string INFOTYPE0007DATASOURCE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "INFOTYPE0007DATASOURCE");

            }
        }

        private string ERPCONNECTOR
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ERPCONNECTOR");
            }
        }

        private string OMDATASOURCE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "OMDATASOURCE");
            }
        }

        private string COMPAT
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "COMPAT");
            }
        }

        public Boolean CALENDAR_USE_SUBSTITUTE
        {
            get
            {
                return Boolean.Parse(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "CALENDAR_USE_SUBSTITUTE"));
            }
        }

        private Type GetService(string Mode, string ClassName)
        {
            Type oReturn = null;
            Assembly oAssembly;
            string assemblyName = string.Format("ESS.EMPLOYEE.{0}", Mode.ToUpper());
            string typeName = string.Format("ESS.EMPLOYEE.{0}.{1}", Mode.ToUpper(), ClassName);
            oAssembly = Assembly.Load(assemblyName);    // Load assembly (dll)
            oReturn = oAssembly.GetType(typeName);      // Load class
            return oReturn;
        }

        //internal string NameLanguageCode(string Language)
        //{
        //    string code = string.Format("NAMELANG_{0}", Language.ToUpper());
        //    if ( ShareDataManagement.LookupCache(CompanyCode, ModuleID, code) == null)
        //    {
        //        return "0";
        //    }
        //    else
        //    {
        //        return ShareDataManagement.LookupCache(CompanyCode, ModuleID, code);
        //    }
        //}

        internal IEmployeeDataService EmployeeService
        {
            get
            {
                Type oType = GetService(ESSCONNECTOR, "EmployeeDataServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IEmployeeDataService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }

        internal IEmployeeDataService EmployeeINFOTYPE0007Service
        {
            get
            {
                Type oType = GetService(INFOTYPE0007DATASOURCE, "EmployeeDataServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IEmployeeDataService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }

        internal IEmployeeDataService ERPEmployeeService
        {
            get
            {
                Type oType = GetService(ERPCONNECTOR, "EmployeeDataServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IEmployeeDataService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }

        public IEmployeeConfigService EmployeeConfig
        {
            get
            {
                Type oType = GetService(ESSCONNECTOR, "EmployeeConfigServiceImpl");
                //Type oType = GetConfigService(EMPDATASOURCE, "EmployeeConfigServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IEmployeeConfigService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }

        internal IOMService OMService
        {
            get
            {
                Type oType = GetService(OMDATASOURCE, "OmServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IOMService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }

        internal IOMServiceHana OMServiceHana
        {
            get
            {
                Type oType = GetService(OMDATASOURCE, "OmServiceHanaImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IOMServiceHana)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }

        internal IEmployeeDataService GetMirrorService(string Mode)
        {
            string mytest = Mode;
            if (Mode == "WS")
            {
                mytest = COMPAT;
            }
            Type oType = GetService(mytest, "EmployeeDataServiceImpl");
            if (oType == null)
            {
                return null;
            }
            else
            {
                return (IEmployeeDataService)Activator.CreateInstance(oType,CompanyCode);
            }
        }

        internal IOMService GetMirrorOMService(string Mode)
        {
            Type oType = GetService(Mode, "OmServiceImpl");
            //Type oType = GetOMService(Mode);
            if (oType == null)
            {
                return null;
            }
            else
            {
                return (IOMService)Activator.CreateInstance(oType, CompanyCode);
            }
        }

        internal IOMServiceHana GetMirrorOMServiceHana(string Mode)
        {
            Type oType = GetService(Mode, "OmServiceHanaImpl");
            //Type oType = GetOMService(Mode);
            if (oType == null)
            {
                return null;
            }
            else
            {
                return (IOMServiceHana)Activator.CreateInstance(oType, CompanyCode);
            }
        }

        internal Type GetInfotype(string Code)
        {
            string __AssemblyName;
            string __ClassName;
            if (ShareDataManagement.LookupCache(CompanyCode, ModuleID, string.Format("{0}_ASSEMBLY", Code)) == null)
            {
                return null;
            }
            else
            {
                __AssemblyName = ShareDataManagement.LookupCache(CompanyCode, ModuleID, string.Format("{0}_ASSEMBLY", Code));
            }
            if (ShareDataManagement.LookupCache(CompanyCode, ModuleID, string.Format("{0}_CLASS", Code)) == null)
            {
                return null;
            }
            else
            {
                __ClassName = ShareDataManagement.LookupCache(CompanyCode, ModuleID, string.Format("{0}_CLASS", Code));
            }

            Assembly oAssembly = Assembly.Load(__AssemblyName);
            Type oType = oAssembly.GetType(__ClassName);
            return oType;
        }

        internal IEmployeeConfigService GetMirrorConfig(string Mode)
        {
            Type oType = GetService(Mode, "EmployeeConfigServiceImpl");
            if (oType == null)
            {
                return null;
            }
            else
            {
                return (IEmployeeConfigService)Activator.CreateInstance(oType, CompanyCode);
            }
        }


    }
}