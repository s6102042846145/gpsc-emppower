using System;

namespace ESS.EMPLOYEE.EXCEPTION
{
    public class DailyWSException : Exception
    {
        public DailyWSException(string message)
            : base(message)
        {
        }
    }
}