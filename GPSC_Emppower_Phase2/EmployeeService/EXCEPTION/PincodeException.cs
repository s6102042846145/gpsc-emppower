using System;

namespace ESS.EMPLOYEE.EXCEPTION
{
    public class PincodeException : Exception
    {
        public PincodeException()
            : base("PINCODE_NOT_CORRECT")
        {
        }

        public PincodeException(Exception innerException)
            : base("PINCODE_NOT_CORRECT", innerException)
        {
        }
    }
}