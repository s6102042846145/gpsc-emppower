using System;
using System.Collections.Generic;
using System.Text;
using ESS.EMPLOYEE.OM;

namespace ESS.EMPLOYEE
{
    public class AbstractOMService : IOMService
    {
        #region IOMService Members

        public virtual INFOTYPE1000 GetObjectData(ObjectType objectType, string objectID, DateTime CheckDate, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public List<INFOTYPE1000> GetAllObject(List<ObjectType> objectTypes)
        {
            return GetAllObject(objectTypes, "DEFAULT");
        }
        public virtual List<INFOTYPE1000> GetAllObject(List<ObjectType> objectTypes, string Mode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetAllObject(string objId1, string objId2, List<ObjectType> objectTypes, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveAllObject(List<ObjectType> objectTypes, List<INFOTYPE1000> data, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public List<INFOTYPE1001> GetAllRelation(List<ObjectType> objectTypes, List<string> relationCodes)
        {
            return GetAllRelation(objectTypes, relationCodes, "DEFAULT");
        }

        public virtual List<INFOTYPE1001> GetAllRelation(List<ObjectType> objectTypes, List<string> relationCodes, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public List<INFOTYPE1001> GetAllRelation(string objId1, string objId2, List<ObjectType> objectTypes, List<string> relationCodes)
        {
            return GetAllRelation(objId1, objId2, objectTypes, relationCodes, "DEFAULT");
        }

        public virtual List<INFOTYPE1001> GetAllRelation(string objId1, string objId2, List<ObjectType> objectTypes, List<string> relationCodes, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveAllRelation(List<ObjectType> objectTypes, List<string> relationCodes, List<INFOTYPE1001> data, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetRelationObjects(string ObjectID, ObjectType type, string relation, ObjectType nextObjectType, bool isReverse, string relation1, ObjectType nextObjectType1, bool isReverse1, DateTime CheckDate, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual bool IsHaveSubOrdinate(string PositionID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE1000 GetOrgRoot(string OrgID, int Level, DateTime CheckDate, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<EmployeeData> GetEmployeeUnder(string OrgID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE1000 GetJobByPositionID(string PositionID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public List<INFOTYPE1013> GetAllPositionBandLevel()
        {
            return GetAllPositionBandLevel("DEFAULT");
        }

        public virtual List<INFOTYPE1013> GetAllPositionBandLevel(string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1013> GetAllPositionBandLevel(string objId1, string objId2, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveAllPositionBandLevel(List<INFOTYPE1013> data, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE1013 GetPositionBandLevel(string PositionID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual EmployeeData GetEmployeeByPositionID(string PositionID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public List<INFOTYPE1003> GetAllApprovalData()
        {
            return GetAllApprovalData("DEFAULT");
        }

        public virtual List<INFOTYPE1003> GetAllApprovalData(string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1003> GetAllApprovalData(string objId1, string objId2, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveAllApprovalData(List<INFOTYPE1003> data, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE1003 GetApprovalData(string PositionID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetOrgUnder(string OrgID, DateTime CheckDate, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetAllPosition(string EmployeeID, DateTime CheckDate, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE1000 GetOrgUnit(string PositionID, DateTime CheckDate, string Language)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetAllOrg(List<ObjectType> list, DateTime dateTime, string CompanyCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetAllOrg(List<ObjectType> list, string CompanyCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE1000 FindNextPosition(string CurrentPositionID, DateTime CheckDate, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE1000 GetOrgParent(string OrgID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<EmployeeData> GetEmployeeByOrg(String OrgID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE1000 GetOraganizationByPositionID(string PositionID, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        #region IOMService Members


        public virtual List<OrganizationWithLevel> GetAllOrganizationWithLevel(string Orgunit, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        //AddBy: Ratchatawan W. (2012-11-07)
        public virtual List<EmployeeData> GetEmployeeByOrg(string OrgID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<INFOTYPE1010> GetAllOrgUnitLevel()
        {
            return GetAllOrgUnitLevel("DEFAULT");
        }
        public virtual List<INFOTYPE1010> GetAllOrgUnitLevel(string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveOrgUnitLevel(List<INFOTYPE1010> data, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual INFOTYPE1010 GetLevelByOrg(string ObjectID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }
}
