using System;
using System.Collections.Generic;
using System.Text;
using ESS.EMPLOYEE.OM;

namespace ESS.EMPLOYEE
{
    public interface IOMService
    {
        INFOTYPE1000 GetObjectData(ObjectType objectType, string objectID, DateTime CheckDate, string LanguageCode);
        List<INFOTYPE1000> GetAllObject(List<ObjectType> objectTypes);
        List<INFOTYPE1000> GetAllObject(List<ObjectType> objectTypes, string Profile);
        List<INFOTYPE1000> GetAllObject(string objId1, string objId2, List<ObjectType> objectTypes, string Profile);
        void SaveAllObject(List<ObjectType> objectTypes, List<INFOTYPE1000> data, string Profile);

        List<INFOTYPE1001> GetAllRelation(List<ObjectType> objectTypes, List<string> relationCodes);
        List<INFOTYPE1001> GetAllRelation(List<ObjectType> objectTypes, List<string> relationCodes, string Profile);
        List<INFOTYPE1001> GetAllRelation(string objId1, string objId2, List<ObjectType> objectTypes, List<string> relationCodes);
        List<INFOTYPE1001> GetAllRelation(string objId1, string objId2, List<ObjectType> objectTypes, List<string> relationCodes, string Profile);
        void SaveAllRelation(List<ObjectType> objectTypes, List<string> relationCodes, List<INFOTYPE1001> data, string Profile);
        List<INFOTYPE1000> GetOrgUnder(string OrgID, DateTime CheckDate, string LanguageCode);
        //List<INFOTYPE1000> GetRelationObjects(string objectID, ObjectType type, string relation, ObjectType nextObjectType, bool isReverse, string relation1, ObjectType nextObjectType1, bool isReverse1, DateTime CheckDate, string LanguageCode);
        INFOTYPE1000 GetOrgRoot(string OrgID, int Level, DateTime CheckDate, string LanguageCode);
        List<EmployeeData> GetEmployeeUnder(string OrgID, DateTime CheckDate);
        bool IsHaveSubOrdinate(string PositionID, DateTime CheckDate);
        List<INFOTYPE1000> GetAllPosition(string EmployeeID, DateTime CheckDate, string LanguageCode);
        INFOTYPE1000 GetOrgUnit(string PositionID, DateTime CheckDate, string Language);
        INFOTYPE1000 GetOrgParent(string OrgID, DateTime CheckDate);

        List<INFOTYPE1013> GetAllPositionBandLevel();
        List<INFOTYPE1013> GetAllPositionBandLevel(string Profile);
        List<INFOTYPE1013> GetAllPositionBandLevel(string objId1, string objId2, string Profile);
        void SaveAllPositionBandLevel(List<INFOTYPE1013> data, string Profile);
        INFOTYPE1013 GetPositionBandLevel(string PositionID, DateTime CheckDate);

        EmployeeData GetEmployeeByPositionID(string PositionID, DateTime CheckDate);


        List<INFOTYPE1003> GetAllApprovalData();
        List<INFOTYPE1003> GetAllApprovalData(string Profile);
        List<INFOTYPE1003> GetAllApprovalData(string objId1, string objId2, string Profile);
        void SaveAllApprovalData(List<INFOTYPE1003> data, string Profile);
        INFOTYPE1003 GetApprovalData(string PositionID, DateTime CheckDate);
        List<INFOTYPE1000> GetAllOrg(List<ObjectType> list, DateTime dateTime, string CompanyCode);
        List<INFOTYPE1000> GetAllOrg(List<ObjectType> list, string CompanyCode);

        INFOTYPE1000 FindNextPosition(string CurrentPositionID, DateTime CheckDate, string LanguageCode);
        List<EmployeeData> GetEmployeeByOrg(String OrgID, DateTime CheckDate);

        INFOTYPE1000 GetOraganizationByPositionID(string PositionID, string LanguageCode);

        //AddBy: Ratchatawan W. (2012-02-22)
        List<OrganizationWithLevel> GetAllOrganizationWithLevel(string Orgunit, string LanguageCode);
        //AddBy: Ratchatawan W. (2012-11-07)
        List<EmployeeData> GetEmployeeByOrg(String OrgID, DateTime BeginDate, DateTime EndDate);

        List<INFOTYPE1010> GetAllOrgUnitLevel();
        List<INFOTYPE1010> GetAllOrgUnitLevel(string Profile);
        void SaveOrgUnitLevel(List<INFOTYPE1010> data, string Profile);
        INFOTYPE1010 GetLevelByOrg(string ObjectID, DateTime CheckDate);


        INFOTYPE1000 GetJobByPositionID(string PositionID, DateTime CheckDate);
    }
}
