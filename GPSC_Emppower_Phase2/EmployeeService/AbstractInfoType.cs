using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Text;

namespace ESS.DATA
{
    [Serializable()]
    public abstract class AbstractInfoType : AbstractObject, IInfoType
    {
        private string __employeeID = string.Empty;
        private string __subtype = string.Empty;
        private string __seqno = string.Empty;
        private DateTime __begindate;
        private DateTime __enddate;
        private string __errorMessage = string.Empty;

        public string EmployeeID
        {
            get
            {
                return __employeeID;
            }
            set
            {
                __employeeID = value;
            }
        }

        public string SubType
        {
            get
            {
                return __subtype;
            }
            set
            {
                __subtype = value;
            }
        }

        public string SeqNo
        {
            get
            {
                return __seqno;
            }
            set
            {
                __seqno = value;
            }
        }

        public DateTime BeginDate
        {
            get
            {
                return __begindate;
            }
            set
            {
                __begindate = value;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return __enddate;
            }
            set
            {
                __enddate = value;
            }
        }

        public string ErrorMessage
        {
            get
            {
                return __errorMessage;                
            }
            set
            {
                __errorMessage = value;
            }
        }

        public abstract string InfoType
        {
            get;
        }

        public virtual object LoadData(string EmployeeID, string SubType, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("method LoadData is not implement");
        }

        #region IInfoType Members

        public virtual List<IInfoType> GetList(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<IInfoType> GetList(string EmployeeID1, string EmployeeID2, string Mode, string Profile, params object[] args)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveTo(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<IInfoType> Data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveTo(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<IInfoType> Data, params object[] args)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion
    }
}
