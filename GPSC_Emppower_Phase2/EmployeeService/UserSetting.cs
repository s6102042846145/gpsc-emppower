using System;
using System.Collections.Generic;
using ESS.WORKFLOW;
using System.ComponentModel;
using ESS.SHAREDATASERVICE;
using System.Globalization;

namespace ESS.EMPLOYEE
{
    [Serializable]
    public class UserSetting
    {
        public static string ModuleID
        {
            get
            {
                return "ESS.EMPLOYEE";
            }
        }
        public UserSetting(string EmployeeID)
            : this(EmployeeID, WorkflowPrinciple.CurrentIdentity.CompanyCode)
        {
        }

        public UserSetting(string EmployeeID, string CompanyCode)
        {
            Employee = new EmployeeData(EmployeeID,"",CompanyCode,DateTime.Now);
            this.CompanyCode = CompanyCode;
            Theme = ShareDataManagement.LookupCache(CompanyCode, ModuleID, "DEFAULTTHEME");
            CultureInfo = new CultureInfo(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "DEFAULTCULTUREINFO"));
            DateFormat = ShareDataManagement.LookupCache(CompanyCode, ModuleID, "DEFAULTDATEFORMAT");
            NormalDecimalFormat = ShareDataManagement.LookupCache(CompanyCode, ModuleID, "DEFAULTNORMALDECIMALFORMAT");
            PrecisionDecimalFormat = ShareDataManagement.LookupCache(CompanyCode, ModuleID, "DEFAULTPRECISIONDECIMALFORMAT");
            Language = ShareDataManagement.LookupCache(CompanyCode, ModuleID, "DEFAULTLANGUAGE");
            ReceiveMail = true;
        }
        
        public List<string> Roles
        {
            get
            {
                return ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).EmployeeService.GetUserRole(Employee.EmployeeID);
            }
        }

        public EmployeeData Employee { get; set; }
        public string Language { get; set; }
        public string Theme { get; set; }
        public CultureInfo CultureInfo { get; set; }
        public string DateFormat { get; set; }
        public string NormalDecimalFormat { get; set; }
        public string PrecisionDecimalFormat { get; set; }
        public string CompanyCode { get; set; }
        public bool ReceiveMail { get; set; }

    }
}