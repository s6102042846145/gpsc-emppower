using System;
using System.Collections.Generic;
using System.Text;
using ESS.WORKFLOW;
using System.Data;

namespace ESS.EMPLOYEE
{
    public class AbstractEmployeeService : IEmployeeService
    {
        #region IEmployeeService Members

        public virtual List<EmployeeData> GetUserInResponse(UserRoleResponseSetting setting, DateTime oCheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual string GetEmployeeIDFromUserID(string UserID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool ValidateEmployeeID(string EmployeeID)
        {
            return this.ValidateEmployeeID(EmployeeID, DateTime.Now);
        }

        public virtual bool ValidateEmployeeID(string EmployeeID,DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }


        public List<INFOTYPE0000> GetInfotype0000List(string EmployeeID)
        {
            return GetInfotype0000List(EmployeeID, EmployeeID);
        }

        public List<INFOTYPE0000> GetInfotype0000List(string EmployeeID1, string EmployeeID2)
        {
            return GetInfotype0000List(EmployeeID1, EmployeeID2, "DEFAULT");
        }

        public List<INFOTYPE0000> GetInfotype0000List(string EmployeeID1, string EmployeeID2, string Profile)
        {
            return GetInfotype0000List(EmployeeID1, EmployeeID2, DateTime.MinValue, Profile);
        }

        public virtual List<INFOTYPE0000> GetInfotype0000List(string EmployeeID1, string EmployeeID2, DateTime CheckDate, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public INFOTYPE0001 GetInfotype0001(string EmployeeID)
        {
            return this.GetInfotype0001(EmployeeID, DateTime.Now);
        }

        public INFOTYPE0001 GetInfotype0001(string EmployeeID, DateTime CheckDate)
        {
            INFOTYPE0001 returnValue = null;
            List<INFOTYPE0001> list = GetInfotype0001List(EmployeeID, EmployeeID, CheckDate, "DEFAULT");
            if (list.Count > 0)
            {
                returnValue = list[0];
            }
            return returnValue;
        }

        public CARDSETTING GetCardSetting(string EmployeeID)
        {
            return this.GetCardSetting(EmployeeID, DateTime.Now);
        }

        public CARDSETTING GetCardSetting(string EmployeeID, DateTime CheckDate)
        {
            CARDSETTING returnValue = null;
            List<CARDSETTING> list = GetCardSettingList(EmployeeID, CheckDate,"DEVTIMESHEET");
            if (list.Count > 0)
            {
                returnValue = list[0];
            }
            return returnValue;
        }

        public virtual List<CARDSETTING> GetCardSettingList(string EmployeeID, DateTime CheckDate,string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public List<INFOTYPE0001> GetInfotype0001List(string EmployeeID)
        {
            return GetInfotype0001List(EmployeeID, EmployeeID);
        }

        public List<INFOTYPE0001> GetInfotype0001List(string EmployeeID1, string EmployeeID2)
        {
            return GetInfotype0001List(EmployeeID1, EmployeeID2, "DEFAULT");
        }

        public List<INFOTYPE0001> GetInfotype0001List(string EmployeeID, DateTime CheckDate)
        {
            return GetInfotype0001List(EmployeeID, EmployeeID,CheckDate, "DEFAULT");
        }

        public List<INFOTYPE0001> GetInfotype0001List(string EmployeeID1, string EmployeeID2, string Profile)
        {
            return GetInfotype0001List(EmployeeID1, EmployeeID2, DateTime.Now, Profile);
        }

        public List<INFOTYPE0001> GetInfotype0001AllList(string EmployeeID)
        {
            return GetInfotype0001List(EmployeeID, EmployeeID, DateTime.MinValue, "DEFAULT");
        }
        public virtual List<INFOTYPE0001> GetInfotype0001List(string EmployeeID1, string EmployeeID2, DateTime CheckDate, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveInfotype0001(string EmployeeID1, string EmployeeID2, List<INFOTYPE0001> data, string profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE0182 GetInfotype0182(string EmployeeID, string Language)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void UpdateInfotype0182(string EmployeeID, string Language, INFOTYPE0182 item)
        {
            throw new Exception("The method 'UpdateInfotype0182(string EmployeeID, string Language, INFOTYPE0182 item)' is not implemented.");
        }

        public virtual void SaveInfotype0182(string EmployeeID1, string EmployeeID2, List<INFOTYPE0182> data, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public List<INFOTYPE0182> GetInfotype0182List(string EmployeeID)
        {
            return GetInfotype0182List(EmployeeID, EmployeeID);
        }

        public List<INFOTYPE0182> GetInfotype0182List(string EmployeeID1, string EmployeeID2)
        {
            return GetInfotype0182List(EmployeeID1, EmployeeID2, "DEFAULT");
        }

        public virtual List<INFOTYPE0182> GetInfotype0182List(string EmployeeID1, string EmployeeID2, string Profile)
        {
            throw new Exception("The method 'List<INFOTYPE0182> GetInfotype0182List(string EmployeeID1, string EmployeeID2, string Profile)' is not implemented.");
        }

        public virtual INFOTYPE0105 GetInfotype0105(string EmployeeID, string SubType)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public List<INFOTYPE0105> GetInfotype0105List(string EmployeeID)
        {
            return GetInfotype0105List(EmployeeID, EmployeeID);
        }

        public List<INFOTYPE0105> GetInfotype0105List(string EmployeeID1, string EmployeeID2)
        {
            return GetInfotype0105List(EmployeeID1, EmployeeID2, "DEFAULT");
        }

        public virtual List<INFOTYPE0105> GetInfotype0105List(string EmployeeID1, string EmployeeID2, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveInfotype0105(string EmployeeID1, string EmployeeID2, List<INFOTYPE0105> data, string profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public List<INFOTYPE0007> GetInfotype0007(string EmployeeID, int Year, int Month)
        {
            return GetInfotype0007List(EmployeeID, EmployeeID, Year, Month, "DEFAULT");
        }

        public List<INFOTYPE0007> GetInfotype0007List(string EmployeeID1, string EmployeeID2)
        {
            return GetInfotype0007List(EmployeeID1, EmployeeID2, -1, -1, "DEFAULT");
        }

        public List<INFOTYPE0007> GetInfotype0007List(string EmployeeID1, string EmployeeID2, string Profile)
        {
            return GetInfotype0007List(EmployeeID1, EmployeeID2, -1, -1, Profile);
        }

        public virtual List<INFOTYPE0007> GetInfotype0007List(DateTime BeginDate, DateTime EndDate, string TimeEvaluateClass)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0007> GetInfotype0007List(string EmployeeID1, string EmployeeID2, int Year, int Month, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0007> GetInfotype0007List(int Year, int Month, string TimeEvaluationClass)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveInfotype0007(string EmployeeID1, string EmployeeID2, List<INFOTYPE0007> data, string profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual UserSetting GetUserSetting(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual  List<string> GetUserRole(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual UserRoleSetting GetUserRoleSetting(string UserRole)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<EmployeeData> GetUserResponse(string Role, string AdminGroup)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<EmployeeData> GetUserInResponse(UserRoleResponseSetting setting)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<EmployeeData> GetUserInResponse(UserRoleResponseSetting setting, string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual bool IsUserInResponse(UserRoleResponseSetting role, string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<Substitution> GetInfotype2003(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<Substitution> GetInfotype2003_Log(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual EmployeeData FindManager(string EmployeeID, string ManagerCode, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveUserSetting(UserSetting userSetting)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<EmployeeData> GetDelegatePersons(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<TaskCopyEmployeeConfig> GetTasks()
        {
            throw new Exception("Tge method 'List<TaskCopyEmployeeConfig> GetTasks()' is not implemented.");
        }

        public List<WorkPlaceCommunication> GetWorkplaceData()
        {
            return GetWorkplaceData("");
        }

        public virtual List<WorkPlaceCommunication> GetWorkplaceData(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveWorkPlaceData(string EmployeeID, List<WorkPlaceCommunication> workplaceList, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SaveWorkPlaceData(List<WorkPlaceCommunication> workplaceList, string Profile)
        {
            SaveWorkPlaceData("", workplaceList, Profile);
        }

        public DateSpecificData GetDateSpecific(string EmployeeID)
        {
            return GetDateSpecificData(EmployeeID);
            //EmployeeData currentLogon = WorkflowPrinciple.Current.UserSetting.Employee;
            //List<UserRoleResponseSetting> permitRoles = new List<UserRoleResponseSetting>();
            //permitRoles.Add(new UserRoleResponseSetting("PAADMIN"));
            //permitRoles.Add(new UserRoleResponseSetting("PYADMIN"));
            //permitRoles.Add(new UserRoleResponseSetting("PAYROLLADMIN"));
            //permitRoles.Add(new UserRoleResponseSetting("MANAGER", true));
            //permitRoles.Add(new UserRoleResponseSetting("SUPERVISOR", true));
            //if (currentLogon.EmployeeID == EmployeeID || currentLogon.IsUserInResponse(permitRoles, EmployeeID))
            //{
            //    return GetDateSpecificData(EmployeeID);
            //}
            //return null;
        }

        protected virtual DateSpecificData GetDateSpecificData(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }


        #region " PIN CODE "

        public virtual bool HavePinCode(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual bool VerifyPinCode(string Employeeid, string pincode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void RequestNewPIN(string Employeeid)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void CreateNewPIN(string EmployeeID, string TicketID, string NewPINcode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual bool ValidateTicket(string EmployeeID, string TicketClass, string TicketID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void ChangePINCode(string EmployeeID, string pincode, string NewPINcode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<TicketMaster> GetActiveTicket(string TicketClass)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        #endregion


        public virtual List<INFOTYPE0001> GetMeEmpList(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual string CreateTicket(string TicketClass, TimeSpan LifeTime, string PINcode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<DateSpecificData> GetDateSpecificList()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<string> GetUserInRole(string UserRole)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //CHAT 2011-10-05 For MassPayslip & MassTaxReport
        //CHAT 2011-10-05 ����Ѻ�֧���������ʾ�ѡ�ҹ��������˹��§ҹ�������
        public virtual DataTable GetEmployeeIDByOrgUnit(string OrgUnit)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //CHAT 2011-10-05 ����Ѻ�֧������ ResponseType �ͧ��������͵�Ǩ�ͺ�Է�������ҹ��� Type
        public virtual List<string> GetUserResponseType(string EmpID, string UserRole)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //CHAT 2011-10-10 ����Ѻ�֧������ Performance �ͧ��ѡ�ҹ
        public virtual DataTable GetRequestorPerformance(string EmpID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //CHAT 2011-10-10 ����Ѻ�֧������ Competency �ͧ��ѡ�ҹ
        public virtual DataTable GetRequestorCompetency(string EmpID)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        #endregion

        #region IEmployeeService Members


        public INFOTYPE0007 GetInfotype0007(string EmployeeID, DateTime CheckDate)
        {
            return GetInfotype0007(EmployeeID, CheckDate, "DEFAULT");
        }

        public virtual INFOTYPE0007 GetInfotype0007(string EmployeeID, DateTime CheckDate, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        //AddBy: Ratchatawan W. (2012-04-23)
        public virtual bool ValidateManager(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        //AddBy: Ratchatawan W. (2012-04-23)
        public virtual List<EmployeeData> GetDelegateEmployeeOMByPosition(string EmployeeID, string PositionID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<EmployeeData> GetDelegateEmployeeForSentMail(string DelegateFromID, string DelegateFromPositionID, int RequestTypeID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<EmployeeData> GetAllActiveEmployeeInINFOTYPE0001(DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<EmployeeData> GetManagerInSameOraganizationAndSameEmpSubGroup(string EmployeeID, string PositionID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE0032 GetInfotype0032ByEmployeeID(string EmployeeID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0032> GetInfotype0032List(string EmployeeID1, string EmployeeID2, DateTime CheckDate, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveInfotype0032(string EmployeeID1, string EmployeeID2, List<INFOTYPE0032> data, string profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<EmployeeData> GetUserInResponse(UserRoleResponseSetting setting, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<EmployeeData> GetAllEmployeeInINFOTYPE0001(DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        
        public virtual DataTable GetUserInResponseForActionOfInstead(string EmployeeID, int SubjectID, DateTime CheckDate, int FilterEmployee)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void DeleteInfotype0007(List<INFOTYPE0007> data, string RequestNo)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveInfotype0007(List<INFOTYPE0007> data, string RequestNo)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #region IEmployeeService Members


        public virtual List<EmployeeData> GetOTSummaryPermissionByEmployeeID(string EmployeeID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual bool IsHaveOTSummaryPermissionByEmployeeID(string EmployeeID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveInfotype0001(List<INFOTYPE0001> data, string RequestNo)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0030> GetInfotype0030List(string EmployeeID1, string EmployeeID2)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0030> GetInfotype0030List(string EmployeeID1, string EmployeeID2, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveInfotype0030(string EmployeeID1, string EmployeeID2, List<INFOTYPE0030> data, string profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE0030 GetInfotype0030(string EmployeeID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        #endregion

        public virtual List<INFOTYPE0185> GetInfotype0185(string EmployeeID, DateTime date)
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }
}
