using System.Security.Principal;
using ESS.EMPLOYEE;
using ESS.WORKFLOW;

namespace ESS.WORKFLOW
{
    public class WorkflowPrinciple : IPrincipal
    {
        private WorkflowIdentity __identity;
        private UserSetting __setting = null;
        private string __theme = "";

        public WorkflowPrinciple(WorkflowIdentity identity)
        {
            __identity = identity;
        }

        public WorkflowPrinciple(WorkflowIdentity identity, string theme)
        {
            __identity = identity;
            __theme = theme;
        }

        #region IPrincipal Members

        public IIdentity Identity
        {
            get { return __identity; }
        }

        public bool IsInRole(string role)
        {
            return UserSetting.Roles.Contains(role.ToUpper());
        }

        #endregion IPrincipal Members

        public static void SetCurrentPrincipal(EmployeeData oEmployeeData)
        {
            WorkflowIdentity iden = WorkflowIdentity.CreateInstance(oEmployeeData.CompanyCode).GetIdentityWithoutPassword(oEmployeeData);
            WorkflowPrinciple Principle = new WorkflowPrinciple(iden);
            WorkflowPrinciple.Current = Principle;
        }
        public static WorkflowPrinciple Current
        {
            get
            {
                if (!(System.Threading.Thread.CurrentPrincipal is WorkflowPrinciple))
                {
                    return null;
                }
                return (WorkflowPrinciple)System.Threading.Thread.CurrentPrincipal;
            }
            set
            {
                System.Threading.Thread.CurrentPrincipal = value;
            }
        }

        public static WorkflowIdentity CurrentIdentity
        {
            get
            {
                return (WorkflowIdentity)Current.Identity;
            }
        }

        public UserSetting UserSetting
        {
            get
            {
                if (__setting == null)
                {
                    __setting =new EmployeeData().GetUserSetting(__identity.EmployeeID);
                    __setting.Employee.CompanyCode = CurrentIdentity.CompanyCode;
                    __setting.Employee.IsSystem = CurrentIdentity.IsSystem;
                    //__setting.Theme = __theme;
                }
                return __setting;
            }
        }
    }
}