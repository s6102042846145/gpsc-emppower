using System;
using System.Collections.Generic;
using System.Text;
using ESS.DATA;
using ESS.EMPLOYEE.OM;
using ESS.EMPLOYEE.CONFIG;
using ESS.WORKFLOW;

namespace ESS.EMPLOYEE
{
    public class INFOTYPE0032 : AbstractInfoType
    {
        private string __employeeID;
        private DateTime __beginDate;
        private DateTime __endDate;
        private string __oldEmployeeID;

         public INFOTYPE0032()
        {
        }

        public override string InfoType
        {
            get { return "0032"; }
        }

        public string EmployeeID
        {
            get { return __employeeID; }
            set { __employeeID = value; }
        }
        public DateTime BeginDate
        {
            get { return __beginDate; }
            set { __beginDate = value; }
        }
        public DateTime EndDate
        {
            get { return __endDate; }
            set { __endDate = value; }
        }
        public string OldEmployeeID
        {
            get { return __oldEmployeeID; }
            set { __oldEmployeeID = value; }
        }

        public static INFOTYPE0032 GetInfotype0032ByEmployeeID(string EmployeeID, DateTime CheckDate)
        {
            return ServiceManager.EmployeeService.GetInfotype0032ByEmployeeID(EmployeeID, CheckDate);
        }

        public override List<IInfoType> GetList(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            List<IInfoType> oReturn = new List<IInfoType>();
            oReturn.AddRange(EMPLOYEE.ServiceManager.GetMirrorService(Mode).GetInfotype0032List(EmployeeID1, EmployeeID2, DateTime.Now, Profile).ToArray());
            return oReturn;
        }

        public override void SaveTo(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<IInfoType> Data)
        {
            List<INFOTYPE0032> List = new List<INFOTYPE0032>();
            foreach (IInfoType item in Data)
            {
                List.Add((INFOTYPE0032)item);
            }
            ServiceManager.GetMirrorService(Mode).SaveInfotype0032(EmployeeID1, EmployeeID2, List, Profile);
        }
    }
}
