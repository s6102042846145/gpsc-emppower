using System;
using System.Collections.Generic;
using System.Text;
using ESS.DATA;

namespace ESS.EMPLOYEE
{
    public class INFOTYPE0182 : AbstractInfoType
    {
        private string __nameType;
        private string __alternateName;
        public INFOTYPE0182()
        { 
        }
        public string NameType
        {
            get
            {
                return __nameType;
            }
            set
            {
                __nameType = value;
            }
        }
        public string AlternateName
        {
            get
            {
                return __alternateName;
            }
            set
            {
                __alternateName = value;
            }
        }

        public override string InfoType
        {
            get { return "0182"; }
        }

        public override void SaveTo(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<IInfoType> Data)
        {
            List<INFOTYPE0182> List = new List<INFOTYPE0182>();
            foreach (IInfoType item in Data)
            {
                List.Add((INFOTYPE0182)item);
            }
            ServiceManager.GetMirrorService(Mode).SaveInfotype0182(EmployeeID1, EmployeeID2, List, Profile);
        }

        public static List<INFOTYPE0001> GetList(string EmployeeID)
        {
            throw new Exception("ttt");
            //return ServiceManager.EmployeeService.GetInfotype0182List(EmployeeID);
        }
        public override List<IInfoType> GetList(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            List<IInfoType> oReturn = new List<IInfoType>();
            oReturn.AddRange(ServiceManager.GetMirrorService(Mode).GetInfotype0182List(EmployeeID1, EmployeeID2, Profile).ToArray());
            return oReturn;
        }
    }
}
