using System;
using System.Collections.Generic;
using System.Text;
using ESS.DATA;
using ESS.EMPLOYEE.OM;
using ESS.EMPLOYEE.CONFIG;
using ESS.WORKFLOW;

namespace ESS.EMPLOYEE
{
    [Serializable()]
    public class CARDSETTING : AbstractInfoType
    {
        #region " Members "
        private string _employeeID_card;
        private DateTime _beginDate_card;
        private DateTime _endDate_card;
        private string _cardNo;
        private string _location;
        #endregion

        public string EmployeeID_Card
        {
            get
            {
                return _employeeID_card;
            }
            set
            {
                _employeeID_card = value;
            }
        }

        public DateTime BeginDate_Card
        {
            get
            {
                return _beginDate_card;
            }
            set
            {
                _beginDate_card = value;
            }
        }

        public DateTime EndDate_Card
        {
            get
            {
                return _endDate_card;
            }
            set
            {
                _endDate_card = value;
            }
        }

        public string CardNo
        {
            get
            {
                return _cardNo;
            }
            set
            {
                _cardNo = value;
            }
        }

        public string Location
        {
            get
            {
                return _location;
            }
            set
            {
                _location = value;
            }
        }
        public override string InfoType
        {
            get { return "0001"; }
        }

        public static CARDSETTING GetCardSettingByEmpID(string EmployeeID)
        {
            return ServiceManager.EmployeeService.GetCardSetting(EmployeeID);
        }
    }
}
