using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using ESS.EMPLOYEE;

namespace ESS.HR
{
    public class INFOTYPE0007_Comparer: IComparer<INFOTYPE0007>
    {
        public int Compare(INFOTYPE0007 obj1, INFOTYPE0007 obj2)
        {
            return obj1.BeginDate.CompareTo(obj2.BeginDate);
        }
    }
}
