using System;
using System.Collections.Generic;
using System.Text;
using ESS.DATA;
using ESS.EMPLOYEE.OM;
using ESS.EMPLOYEE.CONFIG;
using ESS.WORKFLOW;

namespace ESS.EMPLOYEE
{
    [Serializable()]
    public class INFOTYPE0030 : AbstractInfoType
    {
        private string __employeeID;
        private DateTime __beginDate;
        private DateTime __endDate;
        private string __secondmentType = "00";
        private string __oraganizationUnit;

        public override string InfoType
        {
            get { return "0030"; }
        }

        public string EmployeeID
        {
            get { return __employeeID; }
            set { __employeeID = value; }
        }
        public DateTime BeginDate
        {
            get { return __beginDate; }
            set { __beginDate = value; }
        }
        public DateTime EndDate
        {
            get { return __endDate; }
            set { __endDate = value; }
        }
        public string SecondmentType
        {
            get { return __secondmentType; }
            set { __secondmentType = value; }
        }
        public string OraganizationUnit
        {
            get { return __oraganizationUnit; }
            set { __oraganizationUnit = value; }
        }

        public static INFOTYPE0030 GetList(string EmployeeID, DateTime CheckDate)
        {
            return ServiceManager.EmployeeService.GetInfotype0030(EmployeeID, CheckDate);
        }

        public override List<IInfoType> GetList(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            List<IInfoType> oReturn = new List<IInfoType>();
            oReturn.AddRange(ServiceManager.GetMirrorService(Mode).GetInfotype0030List(EmployeeID1, EmployeeID2, Profile).ToArray());
            return oReturn;
        }

        public override void SaveTo(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<IInfoType> Data)
        {
            List<INFOTYPE0030> List = new List<INFOTYPE0030>();
            foreach (IInfoType item in Data)
            {
                List.Add((INFOTYPE0030)item);
            }
            ServiceManager.GetMirrorService(Mode).SaveInfotype0030(EmployeeID1, EmployeeID2, List, Profile);
        }
    }
}
