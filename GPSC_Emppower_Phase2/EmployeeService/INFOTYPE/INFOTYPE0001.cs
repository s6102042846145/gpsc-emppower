using System;
using System.Collections.Generic;
using System.Text;
using ESS.DATA;
using ESS.EMPLOYEE.OM;
using ESS.EMPLOYEE.CONFIG;
using ESS.WORKFLOW;
using ESS.HR;

namespace ESS.EMPLOYEE
{
    [Serializable()]
    public class INFOTYPE0001:AbstractInfoType
    {
        #region " Members "
        private string __compcode;
        private string __area;
        private string __subarea;
        private string __empgroup;
        private string __empsubgroup;
        private string __orgunit = "";
        private string __position = "";
        private string __costcenter = "";
        private string __admingroup;
        private string __name;
        private PersonalSubAreaSetting __setting;
        private PersonalSubGroupSetting __groupSetting;
        #endregion

        INFOTYPE1000 __orgUnitData = null;
        INFOTYPE1000 __positionData = null;

        public INFOTYPE0001()
        {
        }

        private DateTime CheckDate
        {
            get
            {
                if (this.EndDate > DateTime.Now)
                {
                    return DateTime.Now;
                }
                else
                {
                    return this.EndDate;
                }
            }
        }

        private List<UserRoleResponseSetting> permitRoles
        {
            get
            {
                List<UserRoleResponseSetting> roles = new List<UserRoleResponseSetting>();
                roles.Add(new UserRoleResponseSetting("PAADMIN"));
                roles.Add(new UserRoleResponseSetting("TIMEADMIN"));
                roles.Add(new UserRoleResponseSetting("PAYROLLADMIN"));
                roles.Add(new UserRoleResponseSetting("MANAGER", true));
                roles.Add(new UserRoleResponseSetting("SUPERVISOR"));
                return roles;
            }
        }

        #region " Base Properties "

        #region " CompanyCode "
        public string CompanyCode
        {
            get
            {
                return __compcode;
            }
            set
            {
                __compcode = value;
            }
        } 
        #endregion

        #region " Name "
        public string Name
        {
            get
            {
                return __name;
            }
            set
            {
                __name = value;
            }
        }
        #endregion

        #region " Area "
        public string Area
        {
            get 
            { 
                return __area; 
            }
            set 
            { 
                __area = value; 
            }
        } 
        #endregion

        #region " SubArea "
        public string SubArea
        {
            get
            {
                return __subarea;
            }
            set
            {
                __subarea = value;
            }
        }
        #endregion

        #region " EmpGroup "
        public string EmpGroup
        {
            get
            {
                return __empgroup;
            }
            set
            {
                __empgroup = value;
            }
        }
        #endregion

        #region " EmpSubGroup "
        public string EmpSubGroup
        {
            get
            {
                return __empsubgroup;
            }
            set
            {
                __empsubgroup = value;
            }
        }
        #endregion

        #region " OrgUnit "
        public string OrgUnit
        {
            get
            {
                return __orgunit;
            }
            set
            {
                __orgunit = value;
                __orgUnitData = null;
            }
        }
        #endregion

        #region " Position "
        public string Position
        {
            get
            {
                return __position;
            }
            set
            {
                __position = value;
            }
        }
        #endregion

        #region " AdminGroup "
        public string AdminGroup
        {
            get
            {
                return __admingroup;
            }
            set
            {
                __admingroup = value;
            }
        }
        #endregion

        #region " CostCenter "
        public string CostCenter
        {
            get
            {
                return __costcenter;
            }
            set
            {
                __costcenter = value;
            }
        }
        #endregion

        public override string InfoType
        {
            get { return "0001"; }
        }

        public PersonalSubAreaSetting SubAreaSetting
        {
            get
            {
                if (__setting == null || __setting.PersonalArea != this.Area || __setting.PersonalSubArea != this.SubArea)
                {
                    __setting = PersonalSubAreaSetting.GetSetting(this.Area, this.SubArea);
                }
                return __setting;
            }
        }
        public PersonalSubGroupSetting SubGroupSetting
        {
            get
            {
                if (__groupSetting == null || __groupSetting.EmpGroup != this.EmpGroup || __groupSetting.EmpSubGroup != this.EmpSubGroup)
                {
                    __groupSetting = PersonalSubGroupSetting.GetSetting(this.EmpGroup, this.EmpSubGroup);
                }
                return __groupSetting;
            }
        }
        #endregion

        #region " Methods "
        public override void SaveTo(string EmployeeID1,string EmployeeID2,string Mode, string Profile,List<IInfoType> Data)
        {
            List<INFOTYPE0001> List = new List<INFOTYPE0001>();
            foreach (IInfoType item in Data)
            {
                List.Add((INFOTYPE0001)item);
            }
            ServiceManager.GetMirrorService(Mode).SaveInfotype0001(EmployeeID1, EmployeeID2, List, Profile);
        }

        public static List<INFOTYPE0001> GetList(string EmployeeID)
        {
            return ServiceManager.EmployeeService.GetInfotype0001List(EmployeeID);
        }

        public static List<INFOTYPE0001> GetAllList(string EmployeeID)
        {
            return ServiceManager.EmployeeService.GetInfotype0001AllList(EmployeeID);
        }

        public static List<INFOTYPE0001> GetList(string EmployeeID,DateTime dtCheckdate)
        {
            return ServiceManager.EmployeeService.GetInfotype0001List(EmployeeID, dtCheckdate);
        }

        public static List<INFOTYPE0001> GetListFromSAP(string EmployeeID)
        {
            return ServiceManager.ERPEmployeeService.GetInfotype0001List(EmployeeID);
        }

        public static INFOTYPE0001 GetInfotype0001ByEmpID(string EmployeeID)
        {
            return ServiceManager.EmployeeService.GetInfotype0001(EmployeeID);
        }

        public static INFOTYPE0001 GetInfotype0001ByEmpID(string EmployeeID,DateTime CheckDate)
        {
            return ServiceManager.EmployeeService.GetInfotype0001(EmployeeID,CheckDate);
        }

        public override List<IInfoType> GetList(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            List<IInfoType> oReturn = new List<IInfoType>();
            oReturn.AddRange(ServiceManager.GetMirrorService(Mode).GetInfotype0001List(EmployeeID1, EmployeeID2, Profile).ToArray());
            return oReturn;
        }

        public static List<INFOTYPE0001> MergeEndDate(List<INFOTYPE0001> lstInfo)
        {
            lstInfo.Sort(new INFOTYPE0001_Comparer());
            List<INFOTYPE0001> lstReturn = new List<INFOTYPE0001>();
            string subArea = string.Empty;
            foreach (INFOTYPE0001 item in lstInfo)
            {
                if (string.IsNullOrEmpty(subArea) || !subArea.Equals(item.SubArea))
                {
                    subArea = item.SubArea;
                    lstReturn.Add(item);
                }
                else
                {
                    lstReturn[lstReturn.Count - 1].EndDate = item.EndDate;
                }
            }

            return lstReturn;
        }

        public static void SaveList(string EmployeeID1, string EmployeeID2, List<INFOTYPE0001> data, string profile)
        {
            EMPLOYEE.ServiceManager.EmployeeService.SaveInfotype0001(EmployeeID1, EmployeeID2, data, profile);
        }

        public static void SaveInfotype0001(List<INFOTYPE0001> data, string RequestNo)
        {
            EMPLOYEE.ServiceManager.ERPEmployeeService.SaveInfotype0001(data, RequestNo);
        }
        #endregion

        public INFOTYPE1000 OrgUnitData
        {
            get
            {
                if (__orgUnitData == null)
                {
                    __orgUnitData = INFOTYPE1000.GetObjectData(ObjectType.ORGANIZE, this.OrgUnit, this.CheckDate, currentSetting.Language);
                }
                return __orgUnitData;
            }
        }

        public INFOTYPE1000 PositionData
        {
            get
            {
                if (__positionData == null)
                {
                    __positionData = INFOTYPE1000.GetObjectData(ObjectType.POSITION, this.Position, CheckDate, currentSetting.Language);
                }
                return __positionData;
            }
        }
    }
}
