using System;
using System.Collections.Generic;
using System.Text;
using ESS.DATA;

namespace ESS.EMPLOYEE
{
    [Serializable()]
    public class INFOTYPE0007:AbstractInfoType
    {
        private string __WSRule;
        private string __timeEvaClass;
        private string __remark;

        public INFOTYPE0007()
        {
        }

        public string WFRule
        {
            get
            {
                return __WSRule;
            }
            set
            {
                __WSRule = value;
            }
        }

        public string TimeEvaluateClass
        {
            get
            {
                return __timeEvaClass;
            }
            set
            {
                __timeEvaClass = value;
            }
        }

        public String Remark
        {
            get { return __remark; }
            set
            {
                __remark = value;
            }
        }

        public override string InfoType
        {
            get { return "0007"; }
        }

        public override List<IInfoType> GetList(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            List<IInfoType> oReturn = new List<IInfoType>();
            oReturn.AddRange(EMPLOYEE.ServiceManager.GetMirrorService(Mode).GetInfotype0007List(EmployeeID1, EmployeeID2, Profile).ToArray());
            return oReturn;
        }

        public override void SaveTo(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<IInfoType> Data)
        {
            List<INFOTYPE0007> List = new List<INFOTYPE0007>();
            foreach (IInfoType item in Data)
            {
                List.Add((INFOTYPE0007)item);
            }
            ServiceManager.GetMirrorService(Mode).SaveInfotype0007(EmployeeID1, EmployeeID2, List, Profile);
        }

        public static List<INFOTYPE0007> GetFromSAP(string EmployeeID)
        {
            return EMPLOYEE.ServiceManager.ERPEmployeeService.GetInfotype0007List(EmployeeID, EmployeeID);
        }

        public static List<INFOTYPE0007> GetWorkSchedule(string Employee, int Year, int Month)
        {
            return EMPLOYEE.ServiceManager.EmployeeINFOTYPE0007Service.GetInfotype0007(Employee, Year, Month);
        }

        public static List<INFOTYPE0007> GetWorkSchedule(string EmployeeID)
        {
            return EMPLOYEE.ServiceManager.EmployeeINFOTYPE0007Service.GetInfotype0007(EmployeeID, -1, 0);
        }

        public static INFOTYPE0007 GetWorkSchedule(string Employee, DateTime Date)
        {
            INFOTYPE0007 oReturn = null;
            List<INFOTYPE0007> list = EMPLOYEE.ServiceManager.EmployeeINFOTYPE0007Service.GetInfotype0007(Employee, Date.Year, Date.Month);
            foreach (INFOTYPE0007 item in list)
            {
                if (item.BeginDate <= Date.Date && item.EndDate >= Date.Date)
                {
                    oReturn = item;
                    break;
                }
            }
            return oReturn;
        }

        public static List<INFOTYPE0007> GetWorkScheduleWithEvalClass(int Year, int Month, string TimeEvaluationClass)
        {
            return EMPLOYEE.ServiceManager.EmployeeINFOTYPE0007Service.GetInfotype0007List(Year, Month, TimeEvaluationClass);
        }

        public static List<INFOTYPE0007> GetWorkScheduleWithEvalClass(DateTime BeginDate, DateTime EndDate, string TimeEvaluationClass)
        {
            return EMPLOYEE.ServiceManager.EmployeeINFOTYPE0007Service.GetInfotype0007List(BeginDate, EndDate, TimeEvaluationClass);
        }

        public static void SavePeriod(string EmployeeID1, string EmployeeID2, List<INFOTYPE0007> data, string profile)
        {
            EMPLOYEE.ServiceManager.EmployeeService.SaveInfotype0007(EmployeeID1, EmployeeID2, data, profile);
        }

        public static void SaveInfotype0007(List<INFOTYPE0007> data, string RequestNo)
        {
            EMPLOYEE.ServiceManager.ERPEmployeeService.SaveInfotype0007(data, RequestNo);
        }

        public static void DeleteInfotype0007(List<INFOTYPE0007> data, string RequestNo)
        {
            EMPLOYEE.ServiceManager.ERPEmployeeService.DeleteInfotype0007(data, RequestNo);
        }
    }
}
