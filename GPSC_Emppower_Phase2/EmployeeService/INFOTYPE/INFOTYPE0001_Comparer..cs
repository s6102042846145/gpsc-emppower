using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using ESS.EMPLOYEE;

namespace ESS.HR
{
    public class INFOTYPE0001_Comparer: IComparer<INFOTYPE0001>
    {
        public int Compare(INFOTYPE0001 obj1, INFOTYPE0001 obj2)
        {
            return obj1.BeginDate.CompareTo(obj2.BeginDate);
        }
    }
}
