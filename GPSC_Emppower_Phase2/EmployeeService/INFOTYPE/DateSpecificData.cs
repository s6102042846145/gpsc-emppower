using System;
using System.Collections.Generic;
using System.Text;
using ESS.DATA;
using ESS.WORKFLOW;

namespace ESS.EMPLOYEE
{
    [Serializable()]
    public class DateSpecificData : AbstractObject
    {
        private DateTime __startAbsenceQuota = DateTime.MinValue;
        private DateTime __startPF = DateTime.MinValue;
        private DateTime __endPF = DateTime.MinValue;
        private DateTime __hiringDate = DateTime.MinValue;
        private DateTime __retirementDate = DateTime.MinValue;
        private DateTime __startWorkingDate = DateTime.MinValue;
        public string __employeeID = "";
        public DateSpecificData()
        { 
        }
        public string EmployeeID
        {
            get
            {
                return __employeeID;
            }
            set
            {
                __employeeID = value;
            }
        }
        public DateTime HiringDate
        {
            get
            {
                return __hiringDate;
            }
            set
            {
                __hiringDate = value;
            }
        }
        public DateTime StartAbsenceQuota
        {
            get
            {
                return __startAbsenceQuota;
            }
            set
            {
                __startAbsenceQuota = value;
            }
        }
        public DateTime StartPF
        {
            get
            {
                return __startPF;
            }
            set
            {
                __startPF = value;
            }
        }

        public DateTime EndPF
        {
            get
            {
                return __endPF;
            }
            set
            {
                __endPF = value;
            }
        }

        public DateTime RetirementDate
        {
            get
            {
                return __retirementDate;
            }
            set
            {
                __retirementDate = value;
            }
        }

        public DateTime StartWorkingDate
        {
            get
            {
                return __startWorkingDate;
            }
            set
            {
                __startWorkingDate = value;
            }
        }

        public static DateSpecificData GetDateSpecificData(string EmployeeID)
        {
            return EMPLOYEE.ServiceManager.ERPEmployeeService.GetDateSpecific(EmployeeID);
        }
        public static List<DateSpecificData> GetDateSpecificeList()
        {
            return EMPLOYEE.ServiceManager.ERPEmployeeService.GetDateSpecificList();
        }
    }
}
