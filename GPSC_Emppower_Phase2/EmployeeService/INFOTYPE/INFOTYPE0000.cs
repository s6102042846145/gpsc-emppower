using System;
using System.Collections.Generic;
using System.Text;
using ESS.DATA;
using ESS.EMPLOYEE.OM;
using ESS.EMPLOYEE.CONFIG;
using ESS.WORKFLOW;

namespace ESS.EMPLOYEE
{
    public class INFOTYPE0000 : AbstractInfoType
    {
        private string __employmentStatus = "";

        public INFOTYPE0000()
        {
        }

        public string EmploymentStatus
        {
            get
            {
                return __employmentStatus;
            }
            set
            {
                __employmentStatus = value;
            }
        }

        public override List<IInfoType> GetList(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            List<IInfoType> oReturn = new List<IInfoType>();
            oReturn.AddRange(ServiceManager.GetMirrorService(Mode).GetInfotype0000List(EmployeeID1, EmployeeID2, DateTime.Now.Date, Profile).ToArray());
            return oReturn;
        }


        public override string InfoType
        {
            get { return "0000"; }
        }
    }
}
