using System;
using System.Collections.Generic;
using System.Text;
using ESS.DATA;
using ESS.EMPLOYEE.OM;
using ESS.EMPLOYEE.CONFIG;
using ESS.WORKFLOW;

namespace ESS.EMPLOYEE
{
    [Serializable]
    public class INFOTYPE0027 : AbstractInfoType
    {
        private string __employeeID;
        private string __subType;
        private DateTime __beginDate;
        private DateTime __endDate;
        private string __companyCode;
        private string __costCenter;
        private string __percentage;

        public override string InfoType
        {
            get { return "0027"; }
        }

        public string EmployeeID
        {
            get { return __employeeID; }
            set { __employeeID = value; }
        }
        public string SubType
        {
            get { return __subType; }
            set { __subType = value; }
        }
        public DateTime BeginDate
        {
            get { return __beginDate; }
            set { __beginDate = value; }
        }
        public DateTime EndDate
        {
            get { return __endDate; }
            set { __endDate = value; }
        }
        public string CompanyCode
        {
            get { return __companyCode; }
            set { __companyCode = value; }
        }
        public string CostCenter
        {
            get { return __costCenter; }
            set { __costCenter = value; }
        }
        public string Percentage
        {
            get { return __percentage; }
            set { __percentage = value; }
        }
        public static List<INFOTYPE0027> GetInfotype27List(string EmployeeID)
        {
            //return ServiceManager.EmployeeService.GetInfotype0027List(EmployeeID);
            return new List<INFOTYPE0027>();
        }
        public static List<INFOTYPE0027> GetInfotype27List(string EmployeeID, DateTime CheckDate)
        {
            //return ServiceManager.EmployeeService.GetInfotype0027List(EmployeeID, EmployeeID, CheckDate);
            return new List<INFOTYPE0027>();
        }
        public override List<IInfoType> GetList(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            //List<IInfoType> oReturn = new List<IInfoType>();
            //oReturn.AddRange(ServiceManager.GetMirrorService(Mode).GetInfotype0027List(EmployeeID1, EmployeeID2, DateTime.MinValue, Profile).ToArray());
            //return oReturn;
            return new List<IInfoType>();
        }

        public override void SaveTo(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<IInfoType> Data)
        {
            //List<INFOTYPE0027> List = new List<INFOTYPE0027>();
            //foreach (IInfoType item in Data)
            //{
            //    List.Add((INFOTYPE0027)item);
            //}
            //ServiceManager.GetMirrorService(Mode).SaveInfotype0027(EmployeeID1, EmployeeID2, List, Profile);
            return;
        }

        public static List<INFOTYPE0027> GetListByCCPattern(string strCCPattern, DateTime dtCheckDate)
        {
            //return ServiceManager.EmployeeService.GetInfotype0027ListByCCPattern(strCCPattern, dtCheckDate);
            return new List<INFOTYPE0027>();
        }
    }
}