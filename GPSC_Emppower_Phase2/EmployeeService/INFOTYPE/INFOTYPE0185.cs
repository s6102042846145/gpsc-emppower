﻿using ESS.DATA;
using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.EMPLOYEE
{
    public class INFOTYPE0185 : AbstractInfoType
    {
        public enum IDType
        {
            IDENTIFICATION_CARD_NO = 1,
            DRIVING_LICENSE_NO = 2,
            MOTORCYCLE_DRIVING_LICENSE_NO = 3,
            MARRIAGE_CERT_NO = 4,
            SEQUENCE_NO = 5
        }

        public string CardID
        {
            get;
            set;
        }
        public string Subtype
        {
            get;
            set;
        }
        public string BeginDate
        {
            get;
            set;
        }
        public string EndDate
        {
            get;
            set;
        }

        public INFOTYPE0185()
        {
        }

        public override string InfoType
        {
            get { return "0185"; }
        }

        public static List<INFOTYPE0185> GetInfotype0185ByEmpID(string EmployeeID)
        {
            return GetInfotype0185ByEmpID(EmployeeID, DateTime.Now);
        }

        public static List<INFOTYPE0185> GetInfotype0185ByEmpID(string EmployeeID, DateTime date)
        {
            return ServiceManager.ERPEmployeeService.GetInfotype0185(EmployeeID, date);
        }
    }
}
