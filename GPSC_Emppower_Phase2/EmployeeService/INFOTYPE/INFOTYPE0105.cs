using System;
using System.Collections.Generic;
using System.Text;
using ESS.DATA;

namespace ESS.EMPLOYEE
{
    public class INFOTYPE0105 : AbstractInfoType
    {
        #region " Members "
        private string __dataText = "";
        private string __dataText_long = "";
        #endregion

        public INFOTYPE0105()
        {
        }

        public string DataText
        {
            get
            {
                return __dataText;
            }
            set
            {
                __dataText = value;
            }
        }

        public string DataText_Long
        {
            get
            {
                return __dataText_long;
            }
            set
            {
                __dataText_long = value;
            }
        }

        public override string InfoType
        {
            get { return "0105"; }
        }
        public override void SaveTo(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<IInfoType> Data)
        {
            List<INFOTYPE0105> List = new List<INFOTYPE0105>();
            foreach (IInfoType item in Data)
            {
                List.Add((INFOTYPE0105)item);
            }
            ServiceManager.GetMirrorService(Mode).SaveInfotype0105(EmployeeID1, EmployeeID2, List, Profile);
        }

        public static List<INFOTYPE0105> GetList(string EmployeeID)
        {
            return ServiceManager.EmployeeService.GetInfotype0105List(EmployeeID);
        }
        public override List<IInfoType> GetList(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            List<IInfoType> oReturn = new List<IInfoType>();
            oReturn.AddRange(ServiceManager.GetMirrorService(Mode).GetInfotype0105List(EmployeeID1, EmployeeID2, Profile).ToArray());
            return oReturn;
        }
    }
}
