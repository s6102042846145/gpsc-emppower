using System;
using System.Collections.Generic;
using System.Text;
using ESS.EMPLOYEE.CONFIG;

namespace ESS.EMPLOYEE
{
    public interface IEmployeeConfig
    {
        List<PersonalSubAreaSetting> GetPersonalSubAreaSettingList(string Profile);
        PersonalSubAreaSetting GetPersonalSubAreaSetting(string PersonalArea, string PersonalSubArea);
        void SavePersonalSubAreaSettingList(List<PersonalSubAreaSetting> Data, string Profile);
        List<PersonalSubGroupSetting> GetPersonalSubGroupSettingList(string SourceProfile);
        void SavePersonalSubGroupSettingList(List<PersonalSubGroupSetting> Data, string TargetProfile);
        PersonalSubGroupSetting GetPersonalSubGroupSetting(string EmpGroup, string EmpSubGroup);
        List<MonthlyWS> GetMonthlyWorkscheduleList(int Year, string SourceProfile);
        MonthlyWS GetMonthlyWorkschedule(string EmpSubGroupForWorkSchedule, string EmpSubAreaForWorkSchedule, string PublicHolidayCalendar, string WorkScheduleRule, int Year, int Month);
        void SaveMonthlyWorkscheduleList(int Year, List<MonthlyWS> Data, string Profile);
        List<DailyWS> GetDailyWorkscheduleList(string Profile);
        void SaveDailyWorkscheduleList(List<DailyWS> Data, string Profile);
        DailyWS GetDailyWorkschedule(string DailyGroup, string DailyCode, DateTime CheckDate);
        BreakPattern GetBreakPattern(string DWSGroup, string BreakCode);
        WFRuleSetting GetWFRuleSetting(string WFRule);
        List<MonthlyWS> GetMonthlyWorkscheduleGroup(string EmpSubGroupForWorkSchedule, string EmpSubAreaForWorkSchedule, string PublicHolidayCalendar);
        List<MonthlyWS> GetMonthlyWorkscheduleGroup(string EmpSubGroupForWorkSchedule, string EmpSubAreaForWorkSchedule, string PublicHolidayCalendar, bool IncludeMonth, bool IncludeYear);
        List<PersonalSubAreaSetting> GetPersonalSubAreaByArea(string strArea);
        List<MonthlyWS> SimulateMonthlyWorkschedule(string EmpSubGroupForWorkSchedule, string PublicHolidayCalendar, string EmpSubAreaForWorkSchedule, int Year, int Month, Dictionary<string, string> DayOption);
        List<DailyWS> GetDailyWSByWorkscheduleGrouping(string WorkScheduleGrouping, DateTime CheckDate);

        List<string> GetPersonalSubAreaWSRMapping(string strSubArea);
    }
}
