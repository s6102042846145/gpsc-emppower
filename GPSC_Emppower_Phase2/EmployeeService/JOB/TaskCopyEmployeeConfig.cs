using System;
using System.Collections.Generic;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.HR.PA;
using ESS.HR.PA.CONFIG;
using ESS.JOB.ABSTRACT;
using ESS.WORKFLOW;

namespace ESS.EMPLOYEE.JOB
{
    public class TaskCopyEmployeeConfig : AbstractTaskWorker
    {
        public TaskCopyEmployeeConfig()
        {
        }

        public string ConfigName { get; set; }


        public string SourceMode { get; set; }


        public string SourceProfile { get; set; }

        public string TargetMode { get; set; }

        public string TargetProfile { get; set; }


        public string Param1 { get; set; }


        public string Param2 { get; set; }


        public override void Run()
        {
            if (string.IsNullOrEmpty(CompanyCode))
            {
                CompanyCode = WorkflowPrinciple.CurrentIdentity.CompanyCode;
            }
            string oWorkAction = string.Format("Try to copy employee config '{0}' from {1} to {3}", ConfigName, SourceMode, SourceProfile, TargetMode, TargetProfile);
            Console.WriteLine(oWorkAction);
            switch (ConfigName.ToUpper())
            {
                case "PERSONALSUBAREASETTING":
                    List<PersonalSubAreaSetting> personalSubAreaSettingList = EmployeeManagement.CreateInstance(CompanyCode).GetPersonalSubAreaSettingList(SourceMode, SourceProfile);
                    EmployeeManagement.CreateInstance(CompanyCode).SaveTo(personalSubAreaSettingList, TargetMode, TargetProfile);
                    //EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null,personalSubAreaSettingList, oWorkAction, true);
                    Console.WriteLine("Copy {0} record(s) complete", personalSubAreaSettingList.Count);
                    break;

                case "PERSONALSUBGROUPSETTING":
                    List<PersonalSubGroupSetting> personalSubGroupSettingList = EmployeeManagement.CreateInstance(CompanyCode).GetPersonalSubGroupSettingList(SourceMode, SourceProfile);
                    EmployeeManagement.CreateInstance(CompanyCode).SaveTo(personalSubGroupSettingList, TargetMode, TargetProfile);
                    //EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null,personalSubGroupSettingList, oWorkAction, true);
                    Console.WriteLine("Copy {0} record(s) complete", personalSubGroupSettingList.Count);
                    break;

                case "MONTHLYWS":
                    List<MonthlyWS> monthlyWSList = EmployeeManagement.CreateInstance(CompanyCode).GetAll(MonthlyWSYear, SourceMode, SourceProfile);
                    EmployeeManagement.CreateInstance(CompanyCode).SaveTo(MonthlyWSYear, monthlyWSList, TargetMode, TargetProfile);
                    //EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, monthlyWSList, oWorkAction, true);
                    Console.WriteLine("Copy {0} record(s) complete", monthlyWSList.Count);
                    break;

                case "DAILYWS":
                    List<DailyWS> dailyWSList = EmployeeManagement.CreateInstance(CompanyCode).GetAll(SourceMode, SourceProfile);
                    EmployeeManagement.CreateInstance(CompanyCode).SaveTo(dailyWSList, TargetMode, TargetProfile);
                    //EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, dailyWSList, oWorkAction, true);
                    Console.WriteLine("Copy {0} record(s) complete", dailyWSList.Count);
                    break;

                case "BANK":
                    List<Bank> bankList = HRPAManagement.CreateInstance(CompanyCode).GetAllBank(SourceMode);
                    HRPAManagement.CreateInstance(CompanyCode).SaveTo(bankList, TargetMode);
                    //EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, bankList, oWorkAction, true);
                    Console.WriteLine("Copy {0} record(s) complete", bankList.Count);
                    break;

                case "INSTITUTE":
                    List<Institute> instituteList = HRPAManagement.CreateInstance(CompanyCode).GetAllInstitute(SourceMode);
                    HRPAManagement.CreateInstance(CompanyCode).SaveTo(instituteList, TargetMode);
                    //EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, instituteList, oWorkAction, true);
                    Console.WriteLine("Copy {0} record(s) complete", instituteList.Count);
                    break;
            }
        }

        private int MonthlyWSYear
        {
            get
            {
                int nReturn;
                if (!int.TryParse(Param1, out nReturn))
                {
                    nReturn = DateTime.Now.Year;
                }
                return nReturn;
            }
        }
    }
}