using System;
using System.Collections.Generic;
using ESS.JOB.ABSTRACT;
using ESS.JOB.INTERFACE;
using ESS.WORKFLOW;

namespace ESS.EMPLOYEE.JOB
{
    public class JobCopyEmployeeConfig : AbstractJobWorker
    {
        public JobCopyEmployeeConfig()
        {
        }
        public override List<ITaskWorker> LoadTasks()
        {
            if (string.IsNullOrEmpty(CompanyCode))
            {
                CompanyCode = WorkflowPrinciple.CurrentIdentity.CompanyCode;
            }

            int taskID = 0;
            List<ITaskWorker> oReturn = new List<ITaskWorker>();
            TaskCopyEmployeeConfig task;
            task = new TaskCopyEmployeeConfig();
            task.SourceMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEMODE;
            task.SourceProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEPROFILE;
            task.TargetMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETMODE;
            task.TargetProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETPROFILE;
            task.TaskID = taskID++;
            task.ConfigName = "PERSONALSUBAREASETTING";
            oReturn.Add(task);

            task = new TaskCopyEmployeeConfig();
            task.SourceMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEMODE;
            task.SourceProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEPROFILE;
            task.TargetMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETMODE;
            task.TargetProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETPROFILE;
            task.TaskID = taskID++;
            task.ConfigName = "PERSONALSUBGROUPSETTING";
            oReturn.Add(task);

            task = new TaskCopyEmployeeConfig();
            task.SourceMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEMODE;
            task.SourceProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEPROFILE;
            task.TargetMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETMODE;
            task.TargetProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETPROFILE;
            task.TaskID = taskID++;
            task.ConfigName = "MONTHLYWS";
            task.Param1 = DateTime.Now.Year.ToString();
            oReturn.Add(task);

            task = new TaskCopyEmployeeConfig();
            task.SourceMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEMODE;
            task.SourceProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEPROFILE;
            task.TargetMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETMODE;
            task.TargetProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETPROFILE;
            task.TaskID = taskID++;
            task.ConfigName = "DAILYWS";
            oReturn.Add(task);

            if (DateTime.Now.Month >= 9)
            {
                task = new TaskCopyEmployeeConfig();
                task.SourceMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEMODE;
                task.SourceProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEPROFILE;
                task.TargetMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETMODE;
                task.TargetProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETPROFILE;
                task.TaskID = taskID++;
                task.ConfigName = "MONTHLYWS";
                task.Param1 = (DateTime.Now.Year + 1).ToString();
                oReturn.Add(task);
            }

            task = new TaskCopyEmployeeConfig();//Move BANK from JobCopyPAConfig by Koissares 20200417
            task.SourceMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEMODE;
            task.SourceProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEPROFILE;
            task.TargetMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETMODE;
            task.TargetProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETPROFILE;
            task.TaskID = taskID++;
            task.ConfigName = "BANK";
            oReturn.Add(task);

            task = new TaskCopyEmployeeConfig();//Move INSTITUTE from JobCopyPAConfig by Koissares 20200417
            task.SourceMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEMODE;
            task.SourceProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEPROFILE;
            task.TargetMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETMODE;
            task.TargetProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETPROFILE;
            task.TaskID = taskID++;
            task.ConfigName = "INSTITUTE";
            oReturn.Add(task);

            return oReturn;
        }

    }
}