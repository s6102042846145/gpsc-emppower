using System;
using ESS.JOB.ABSTRACT;
using ESS.WORKFLOW;
using System.Collections.Generic;
using ESS.EMPLOYEE.CONFIG.OM;
using ESS.SHAREDATASERVICE;

namespace ESS.EMPLOYEE.JOB
{
    public class TaskCopyOMData : AbstractTaskWorker
    {

        private string __objID1 = "00000000";
        private string __objID2 = "99999999";

        public TaskCopyOMData()
        {
        }

        public string ConfigName { get; set; }

        public string SourceMode { get; set; }


        public string SourceProfile { get; set; }

        public string TargetMode { get; set; }

        public string TargetProfile { get; set; }


        public string ObjectID1 { get; set; }


        public string ObjectID2 { get; set; }

        public string SAP_VERSION { get; set; }

        public override void Run()
        {
            if (string.IsNullOrEmpty(CompanyCode))
            {
                CompanyCode = WorkflowPrinciple.CurrentIdentity.CompanyCode;
            }

            string oWorkAction = string.Format("Try to copy OM data '{0}' from {1} to {3}", ConfigName, SourceMode, SourceProfile, TargetMode, TargetProfile);
            Console.WriteLine(oWorkAction);
            OMManagement oMan = OMManagement.CreateInstance(CompanyCode);
            switch (ConfigName.ToUpper())
            {
                case "OBJECT":
                    List<INFOTYPE1000> lstInf = oMan.GetAllObject(SourceMode, SourceProfile);
                    Console.WriteLine("Load {0} record(s) completed ", lstInf.Count);
                    oMan.SaveAllObject(lstInf, TargetMode, TargetProfile);
                    EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null,string.Format("Copy {0} records", lstInf.Count), oWorkAction, true);
                    Console.WriteLine("Save {0} record(s) completed.", lstInf.Count);
                    break;

                case "RELATION":
                    List<INFOTYPE1001> lstInf1001 = oMan.GetAllRelation(ObjectID1, ObjectID2, SourceMode, SourceProfile);
                    Console.WriteLine("Load {0} record(s) completed", lstInf1001.Count);
                    oMan.SaveAllRelation(lstInf1001, TargetMode, TargetProfile);
                    EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format("Copy {0} records", lstInf1001.Count), oWorkAction, true);
                    Console.WriteLine("Save {0} record(s) completed.", lstInf1001.Count);
                    break;

                case "USERROLERESPONSESNAPSHOT":
                    Console.WriteLine("Gen UserRoleResponseSnapshot Start");
                    oMan.GenUserRoleResponseSnapshot();
                    EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, null, oWorkAction, true);
                    Console.WriteLine("Gen UserRoleResponseSnapshot Completed");
                    break;

                //case "APPROVALDATA":
                //    List<INFOTYPE1003> lstInf1003 = oMan.GetAllApprovalData(ObjectID1, ObjectID2, SourceMode, SourceProfile);
                //    Console.WriteLine("Load {0} record(s) completed", lstInf1003.Count);
                //    oMan.SaveAllApprovalData(lstInf1003, TargetMode, TargetProfile);
                //    EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format("Copy {0} records", lstInf1003.Count), oWorkAction, true);
                //    Console.WriteLine("Save {0} record(s) completed.", lstInf1003.Count);
                //    break;

                case "ORGUNITLEVEL":
                    List<INFOTYPE1010> lstInf1010 = OMManagement.CreateInstance(CompanyCode).GetAllOrgUnitLevel(ObjectID1, ObjectID2, SourceMode, SourceProfile);
                    Console.WriteLine("Load {0} record(s) completed", lstInf1010.Count);
                    oMan.SaveAllOrgUnitLevel(lstInf1010, TargetMode, TargetProfile);
                    EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format("Copy {0} records", lstInf1010.Count), oWorkAction, true);
                    Console.WriteLine("Save {0} record(s) completed.", lstInf1010.Count);
                    break;
                case "POSITIONBANDLEVEL":
                    List<INFOTYPE1013> lstInf1013 = oMan.GetAllPositionBandLevel(ObjectID1, ObjectID2, SourceMode, SourceProfile);
                    Console.WriteLine("Load {0} record(s) completed", lstInf1013.Count);
                    oMan.SaveAllPositionBandLevel(lstInf1013, TargetMode, TargetProfile);
                    EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format("Copy {0} records", lstInf1013.Count), oWorkAction, true);
                    Console.WriteLine("Save {0} record(s) completed.", lstInf1013.Count);
                    break;
                //case "JOBINDEX":
                //    List<INFOTYPE1513> lstInf1513 = oMan.GetAllJobIndex(ObjectID1, ObjectID2, SourceMode, SourceProfile);
                //    Console.WriteLine("Load {0} record(s) completed", lstInf1513.Count);
                //    oMan.SaveAllJobIndex(lstInf1513, TargetMode, TargetProfile);
                //    EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format("Copy {0} records", lstInf1513.Count), oWorkAction, true);
                //    Console.WriteLine("Save {0} record(s) completed.", lstInf1513.Count);
                //    break;
                case "MAPEMPSUBGROUP":
                    oMan.MapEmpSubGroup();
                    EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, null, oWorkAction, true);
                    Console.WriteLine("Map EmpSubGroup completed.");
                    break;
            }
        }
    }
}