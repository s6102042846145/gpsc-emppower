using System.Collections.Generic;
using ESS.JOB.ABSTRACT;
using ESS.JOB.INTERFACE;
using ESS.WORKFLOW;

namespace ESS.EMPLOYEE.JOB
{
    public class JobCopyEmployeeData : AbstractJobWorker
    {
        public JobCopyEmployeeData()
        {
        }

        public override List<ITaskWorker> LoadTasks()
        {
            if (string.IsNullOrEmpty(CompanyCode))
            {
                CompanyCode = WorkflowPrinciple.CurrentIdentity.CompanyCode;
            }

            List<ITaskWorker> returnValue = new List<ITaskWorker>();
            TaskCopyEmployeeData task;

            task = new TaskCopyEmployeeData();
            task.InfoType = "INFOTYPE0001";
            task.EmployeeID1 = Params[0].Value;
            task.EmployeeID2 = Params[1].Value;
            task.SourceMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEMODE;
            task.SourceProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEPROFILE;
            task.TargetMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETMODE;
            task.TargetProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETPROFILE;
            task.TaskID = 0;
            returnValue.Add(task);

            task = new TaskCopyEmployeeData();
            task.InfoType = "INFOTYPE0002";
            task.EmployeeID1 = Params[0].Value;
            task.EmployeeID2 = Params[1].Value;
            task.SourceMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEMODE;
            task.SourceProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEPROFILE;
            task.TargetMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETMODE;
            task.TargetProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETPROFILE;
            task.TaskID = 1;
            returnValue.Add(task);

            task = new TaskCopyEmployeeData();
            task.InfoType = "INFOTYPE0007";
            task.EmployeeID1 = Params[0].Value;
            task.EmployeeID2 = Params[1].Value;
            task.SourceMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEMODE;
            task.SourceProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEPROFILE;
            task.TargetMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETMODE;
            task.TargetProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETPROFILE;
            task.TaskID = 2;
            returnValue.Add(task);

            task = new TaskCopyEmployeeData();
            task.InfoType = "INFOTYPE0027";
            task.EmployeeID1 = Params[0].Value;
            task.EmployeeID2 = Params[1].Value;
            task.SourceMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEMODE;
            task.SourceProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEPROFILE;
            task.TargetMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETMODE;
            task.TargetProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETPROFILE;
            task.TaskID = 2;
            returnValue.Add(task);

            task = new TaskCopyEmployeeData();
            task.InfoType = "INFOTYPE0030";
            task.EmployeeID1 = Params[0].Value;
            task.EmployeeID2 = Params[1].Value;
            task.SourceMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEMODE;
            task.SourceProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEPROFILE;
            task.TargetMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETMODE;
            task.TargetProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETPROFILE;
            task.TaskID = 3;
            returnValue.Add(task);

            task = new TaskCopyEmployeeData();
            task.InfoType = "INFOTYPE0105";
            task.EmployeeID1 = Params[0].Value;
            task.EmployeeID2 = Params[1].Value;
            task.SourceMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEMODE;
            task.SourceProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEPROFILE;
            task.TargetMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETMODE;
            task.TargetProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETPROFILE;
            task.TaskID = 3;
            returnValue.Add(task);

            task = new TaskCopyEmployeeData();
            task.InfoType = "INFOTYPE0182";
            task.EmployeeID1 = Params[0].Value;
            task.EmployeeID2 = Params[1].Value;
            task.SourceMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEMODE;
            task.SourceProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEPROFILE;
            task.TargetMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETMODE;
            task.TargetProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETPROFILE;
            task.TaskID = 4;
            returnValue.Add(task);

            return returnValue;
        }
    }
}