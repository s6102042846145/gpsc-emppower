using System.Collections.Generic;
using ESS.JOB.ABSTRACT;
using ESS.JOB.INTERFACE;
using ESS.WORKFLOW;

namespace ESS.EMPLOYEE.JOB
{
    public class JobGenUserRoleResponseSnapShot : AbstractJobWorker
    {
        public JobGenUserRoleResponseSnapShot()
        {
        }

        public override List<ITaskWorker> LoadTasks()
        {
            if (string.IsNullOrEmpty(CompanyCode))
            {
                CompanyCode = WorkflowPrinciple.CurrentIdentity.CompanyCode;
            }
            List<ITaskWorker> returnValue = new List<ITaskWorker>();
            TaskCopyOMData task;

            task = new TaskCopyOMData();
            task.ConfigName = "USERROLERESPONSESNAPSHOT";
            task.SourceMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEMODE;
            task.SourceProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).SOURCEPROFILE;
            task.TargetMode = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETMODE;
            task.TargetProfile = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).TARGETPROFILE;
            task.ObjectID1 = Params[0].Value;
            task.ObjectID2 = Params[1].Value;
            returnValue.Add(task);

            return returnValue;
        }
    }
}