using System;
using System.Collections.Generic;
using ESS.EMPLOYEE.INTERFACE;
using ESS.JOB.ABSTRACT;
using ESS.EMPLOYEE.CONFIG.PA;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.WORKFLOW;

namespace ESS.EMPLOYEE.JOB
{
    public class TaskCopyEmployeeData : AbstractTaskWorker
    {
        private string __infoType = "";
        private string __empID1 = "00000000";
        private string __empID2 = "99999999";
        private string __sourceMode = "";
        private string __sourceProfile = "";
        private string __targetMode = "";
        private string __targetProfile = "";

        public TaskCopyEmployeeData()
        {
        }

        public string InfoType { get; set; }
        public string EmployeeID1 { get; set; }
        public string EmployeeID2 { get; set; }
        public string SourceMode { get; set; }

        public string SourceProfile { get; set; }

        public string TargetMode { get; set; }

        public string TargetProfile { get; set; }

        public override void Run()
        {
            if (string.IsNullOrEmpty(CompanyCode))
            {
                CompanyCode = WorkflowPrinciple.CurrentIdentity.CompanyCode;
            }
            string oWorkAction = string.Format("Try to copy data {0} from {1} to {3}", InfoType, SourceMode, SourceProfile, TargetMode, TargetProfile);
            Console.WriteLine(oWorkAction);
            if (InfoType == "WORKPLACE")
            {
                EmployeeManagement.CreateInstance(CompanyCode).CopyAllWorkPlaceData();
                Console.WriteLine("Copy completed.");
            }
            else if (InfoType == "INFOTYPE0001")
            {
                EmployeeManagement oMan = EmployeeManagement.CreateInstance(CompanyCode);
                List<INFOTYPE0001> lstInf = oMan.GetINFOTYPE0001List(EmployeeID1, EmployeeID2, SourceMode, SourceProfile);
                Console.WriteLine("Load {0} record(s) completed", lstInf.Count);
                oMan.SaveINFOTYPE0001To(EmployeeID1, EmployeeID2, TargetMode, TargetProfile, lstInf);
                EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format("Copy {0} records", lstInf.Count), oWorkAction, true);
                Console.WriteLine("Save {0} record(s) completed.", lstInf.Count);
            }
            else if (InfoType == "INFOTYPE0002")
            {
                EmployeeManagement oMan = EmployeeManagement.CreateInstance(CompanyCode);
                List<INFOTYPE0002> lstInf = oMan.GetINFOTYPE0002List(EmployeeID1, EmployeeID2, SourceMode, SourceProfile);
                Console.WriteLine("Load {0} record(s) completed", lstInf.Count);
                oMan.SaveINFOTYPE0002To(EmployeeID1, EmployeeID2, TargetMode, TargetProfile, lstInf);
                EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format("Copy {0} records", lstInf.Count), oWorkAction, true);
                Console.WriteLine("Save {0} record(s) completed.", lstInf.Count);
            }
            else if (InfoType == "INFOTYPE0007")
            {
                EmployeeManagement oMan = EmployeeManagement.CreateInstance(CompanyCode);
                List<INFOTYPE0007> lstInf = oMan.GetINFOTYPE0007List(EmployeeID1, EmployeeID2, SourceMode, SourceProfile);
                Console.WriteLine("Load {0} record(s) completed", lstInf.Count);
                oMan.SaveINFOTYPE0007To(EmployeeID1, EmployeeID2, TargetMode, TargetProfile, lstInf);
                EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format("Copy {0} records", lstInf.Count), oWorkAction, true);
                Console.WriteLine("Save {0} record(s) completed.", lstInf.Count);
            }
            else if (InfoType == "INFOTYPE0027")
            {
                EmployeeManagement oMan = EmployeeManagement.CreateInstance(CompanyCode);
                List<INFOTYPE0027> lstInf = oMan.GetINFOTYPE0027List(EmployeeID1, EmployeeID2, SourceMode, SourceProfile);
                Console.WriteLine("Load {0} record(s) completed", lstInf.Count);
                oMan.SaveINFOTYPE0027To(EmployeeID1, EmployeeID2, TargetMode, TargetProfile, lstInf);
                EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format("Copy {0} records", lstInf.Count), oWorkAction, true);
                Console.WriteLine("Save {0} record(s) completed.", lstInf.Count);
            }
            else if (InfoType == "INFOTYPE0030")
            {
                EmployeeManagement oMan = EmployeeManagement.CreateInstance(CompanyCode);
                List<INFOTYPE0030> lstInf = oMan.GetINFOTYPE0030List(EmployeeID1, EmployeeID2, SourceMode, SourceProfile);
                Console.WriteLine("Load {0} record(s) completed", lstInf.Count);
                oMan.SaveINFOTYPE0030To(EmployeeID1, EmployeeID2, TargetMode, TargetProfile, lstInf);
                EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format("Copy {0} records", lstInf.Count), oWorkAction, true);
                Console.WriteLine("Save {0} record(s) completed.", lstInf.Count);
            }
            else if (InfoType == "INFOTYPE0105")
            {
                EmployeeManagement oMan = EmployeeManagement.CreateInstance(CompanyCode);
                List<INFOTYPE0105> lstInf = oMan.GetINFOTYPE0105List(EmployeeID1, EmployeeID2, SourceMode, SourceProfile);
                Console.WriteLine("Load {0} record(s) completed", lstInf.Count);
                oMan.SaveINFOTYPE0105To(EmployeeID1, EmployeeID2, TargetMode, TargetProfile, lstInf);
                EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format("Copy {0} records", lstInf.Count), oWorkAction, true);
                Console.WriteLine("Save {0} record(s) completed.", lstInf.Count);
            }
            else if (InfoType == "INFOTYPE0182")
            {
                EmployeeManagement oMan = EmployeeManagement.CreateInstance(CompanyCode);
                List<INFOTYPE0182> lstInf = oMan.GetINFOTYPE0182List(EmployeeID1, EmployeeID2, SourceMode, SourceProfile);
                Console.WriteLine("Load {0} record(s) completed", lstInf.Count);
                oMan.SaveINFOTYPE0182To(EmployeeID1, EmployeeID2, TargetMode, TargetProfile, lstInf);
                EmployeeManagement.CreateInstance(CompanyCode).InsertActionLog(null, string.Format("Copy {0} records", lstInf.Count), oWorkAction, true);
                Console.WriteLine("Save {0} record(s) completed.", lstInf.Count);
            }
        }
    }
}