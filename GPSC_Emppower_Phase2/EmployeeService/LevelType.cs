﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.EMPLOYEE
{
    public enum OrgLevelType
    {
        Manager = 080,
        VP = 060,
        SVP = 050,
        EVP = 040,
        President = 030,
    }
}
