using System;
using System.Collections.Generic;
using System.Text;
using ESS.UTILITY.EXTENSION;

namespace ESS.EMPLOYEE
{
    public class DelegateData:AbstractObject
    {
        private string __employeeID = String.Empty;
        private string __delegateTo = String.Empty;
        private DateTime __beginDate = DateTime.MinValue;
        private DateTime __endDate = DateTime.MinValue;
        private string __requestNo = String.Empty;
        public DelegateData()
        { 
        }
        public string EmployeeID
        {
            get
            {
                return __employeeID;
            }
            set
            {
                __employeeID = value;
            }
        }
        public string DelegateTo
        {
            get
            {
                return __delegateTo;
            }
            set
            {
                __delegateTo = value;
            }
        }
        public DateTime BeginDate
        {
            get
            {
                return __beginDate;
            }
            set
            {
                __beginDate = value;
            }
        }
        public DateTime EndDate
        {
            get
            {
                return __endDate;
            }
            set
            {
                __endDate = value;
            }
        }

        public string RequestNo
        {
            get
            {
                return __requestNo;
            }
            set
            {
                __requestNo = value;
            }
        }
    }
}
