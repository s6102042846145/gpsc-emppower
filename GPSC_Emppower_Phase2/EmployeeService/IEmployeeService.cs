using System;
using System.Collections.Generic;
using System.Text;
using ESS.WORKFLOW;
using System.Data;

namespace ESS.EMPLOYEE
{
    public interface IEmployeeService
    {
        #region " User "
        UserSetting GetUserSetting(string EmployeeID);
        List<string> GetUserRole(string EmployeeID);
        List<string> GetUserInRole(string UserRole);
        UserRoleSetting GetUserRoleSetting(string UserRole);
        List<EmployeeData> GetUserResponse(string Role, string AdminGroup);
        List<EmployeeData> GetUserInResponse(UserRoleResponseSetting setting);
        List<EmployeeData> GetUserInResponse(UserRoleResponseSetting setting, string EmployeeID);
        List<EmployeeData> GetUserInResponse(UserRoleResponseSetting setting,DateTime oCheckDate);
        List<EmployeeData> GetUserInResponse(UserRoleResponseSetting setting, DateTime BeginDate,DateTime EndDate);
        bool IsUserInResponse(UserRoleResponseSetting role, string EmployeeID);
        void SaveUserSetting(UserSetting userSetting);
        #endregion

        List<EmployeeData> GetManagerInSameOraganizationAndSameEmpSubGroup(string EmployeeID,string PositionID,DateTime CheckDate);
        string GetEmployeeIDFromUserID(string UserID);
        bool ValidateEmployeeID(string EmployeeID);
        bool ValidateEmployeeID(string EmployeeID,DateTime CheckDate);
        EmployeeData FindManager(string EmployeeID, string ManagerCode, DateTime CheckDate);

        List<INFOTYPE0000> GetInfotype0000List(string EmployeeID);
        List<INFOTYPE0000> GetInfotype0000List(string EmployeeID1, string EmployeeID2);
        List<INFOTYPE0000> GetInfotype0000List(string EmployeeID1, string EmployeeID2, string Profile);
        List<INFOTYPE0000> GetInfotype0000List(string EmployeeID1, string EmployeeID2, DateTime CheckDate, string Profile);

        CARDSETTING GetCardSetting(string EmployeeID);
        CARDSETTING GetCardSetting(string EmployeeID, DateTime CheckDate);
        List<CARDSETTING> GetCardSettingList(string EmployeeID, DateTime CheckDate, string Profile);

        INFOTYPE0001 GetInfotype0001(string EmployeeID);
        INFOTYPE0001 GetInfotype0001(string EmployeeID, DateTime CheckDate);
        List<INFOTYPE0001> GetInfotype0001List(string EmployeeID);
        List<INFOTYPE0001> GetInfotype0001List(string EmployeeID,DateTime CheckDate);
        List<INFOTYPE0001> GetInfotype0001List(string EmployeeID1, string EmployeeID2);
        List<INFOTYPE0001> GetInfotype0001List(string EmployeeID1, string EmployeeID2, string Profile);
        List<INFOTYPE0001> GetInfotype0001List(string EmployeeID1, string EmployeeID2, DateTime CheckDate, string Profile);
        void SaveInfotype0001(string EmployeeID1, string EmployeeID2, List<INFOTYPE0001> data, string profile);
        void SaveInfotype0001(List<INFOTYPE0001> data, string RequestNo);

        INFOTYPE0182 GetInfotype0182(string EmployeeID, string Language);
        void UpdateInfotype0182(string EmployeeID, string Language, INFOTYPE0182 item);
        void SaveInfotype0182(string EmployeeID1, string EmployeeID2, List<INFOTYPE0182> data, string Profile);
        List<INFOTYPE0182> GetInfotype0182List(string EmployeeID);
        List<INFOTYPE0182> GetInfotype0182List(string EmployeeID1, string EmployeeID2);
        List<INFOTYPE0182> GetInfotype0182List(string EmployeeID1, string EmployeeID2, string Profile);
        INFOTYPE0105 GetInfotype0105(string EmployeeID,string SubType);
        List<INFOTYPE0105> GetInfotype0105List(string EmployeeID);
        List<INFOTYPE0105> GetInfotype0105List(string EmployeeID1, string EmployeeID2);
        List<INFOTYPE0105> GetInfotype0105List(string EmployeeID1, string EmployeeID2, string Profile);
        void SaveInfotype0105(string EmployeeID1, string EmployeeID2, List<INFOTYPE0105> data, string profile);

        //AddBy: Ratchatawan W. (2012-02-22)
        INFOTYPE0007 GetInfotype0007(string EmployeeID, DateTime CheckDate);
        INFOTYPE0007 GetInfotype0007(string EmployeeID, DateTime CheckDate, string Profile);

        List<INFOTYPE0007> GetInfotype0007(string EmployeeID, int Year, int Month);
        List<INFOTYPE0007> GetInfotype0007List(string EmployeeID1, string EmployeeID2);
        List<INFOTYPE0007> GetInfotype0007List(string EmployeeID1, string EmployeeID2, string Profile);
        List<INFOTYPE0007> GetInfotype0007List(string EmployeeID1, string EmployeeID2, int Year, int Month, string Profile);
        List<INFOTYPE0007> GetInfotype0007List(int Year, int Month, string TimeEvaluateClass);
        List<INFOTYPE0007> GetInfotype0007List(DateTime BeginDate, DateTime EndDate, string TimeEvaluateClass);
        void SaveInfotype0007(string EmployeeID1, string EmployeeID2, List<INFOTYPE0007> data, string profile);
        void SaveInfotype0007(List<INFOTYPE0007> data, string RequestNo);
        void DeleteInfotype0007(List<INFOTYPE0007> data, string RequestNo);

        List<Substitution> GetInfotype2003(string EmployeeID, DateTime BeginDate, DateTime EndDate);
        List<Substitution> GetInfotype2003_Log(string EmployeeID, DateTime BeginDate, DateTime EndDate);

        List<EmployeeData> GetDelegatePersons(string EmployeeID);
        List<EmployeeData> GetAllEmployeeInINFOTYPE0001(DateTime CheckDate);

        List<TaskCopyEmployeeConfig> GetTasks();

        List<WorkPlaceCommunication> GetWorkplaceData(string EmployeeID);
        void SaveWorkPlaceData(string EmployeeID, List<WorkPlaceCommunication> workplaceList, string Profile);
        List<WorkPlaceCommunication> GetWorkplaceData();
        void SaveWorkPlaceData(List<WorkPlaceCommunication> workplaceList, string Profile);

        DateSpecificData GetDateSpecific(string EmployeeID);
        List<DateSpecificData> GetDateSpecificList();

        bool HavePinCode(string EmployeeID);
        bool VerifyPinCode(string Employeeid, string pincode);
        void RequestNewPIN(string Employeeid);
        void CreateNewPIN(string EmployeeID, string TicketID, string NewPINcode);
        string CreateTicket(string TicketClass, TimeSpan LifeTime, string PINcode);
        bool ValidateTicket(string EmployeeID, string TicketClass, string TicketID);
        void ChangePINCode(string EmployeeID, string pincode, string NewPINcode);
        List<TicketMaster> GetActiveTicket(string TicketClass);
        List<INFOTYPE0001> GetMeEmpList(string EmployeeID);
        //CHAT 2011-10-05 For MassPayslip & MassTaxReport
        DataTable GetEmployeeIDByOrgUnit(string OrgUnit);
        List<string> GetUserResponseType(string EmpID, string UserRole);
        DataTable GetRequestorPerformance(string EmpID);
        DataTable GetRequestorCompetency(string EmpID);

        //AddBy: Ratchatawan W. (2012-04-23)
        bool ValidateManager(string EmployeeID);
        //AddBy: Ratchatawan W. (2012-04-23)
        List<EmployeeData> GetDelegateEmployeeOMByPosition(string EmployeeID, string PositionID);

        //AddBy: Ratchatawan W. (2012-08-08)
        List<EmployeeData> GetDelegateEmployeeForSentMail(string DelegateFromID, string DelegateFromPositionID,int RequestTypeID,DateTime CheckDate);

        //AddBy: Ratchatawan W. (2012-11-07)
        List<EmployeeData> GetAllActiveEmployeeInINFOTYPE0001(DateTime CheckDate);

        //AddBy: Ratchatawan W. (2012-12-24)
        INFOTYPE0032 GetInfotype0032ByEmployeeID(string EmployeeID, DateTime CheckDate);
        List<INFOTYPE0032> GetInfotype0032List(string EmployeeID1, string EmployeeID2, DateTime CheckDate, string Profile);
        void SaveInfotype0032(string EmployeeID1, string EmployeeID2, List<INFOTYPE0032> data, string profile);

        //AddBy: Ratchatawan W. (2013-04-12)
        List<EmployeeData> GetOTSummaryPermissionByEmployeeID(string EmployeeID, DateTime CheckDate);
        bool IsHaveOTSummaryPermissionByEmployeeID(string EmployeeID, DateTime CheckDate);
        DataTable GetUserInResponseForActionOfInstead(string EmployeeID, int SubjectID, DateTime CheckDate, int FilterEmployee);

        void SaveInfotype0030(string EmployeeID1, string EmployeeID2, List<INFOTYPE0030> data, string profile);
        INFOTYPE0030 GetInfotype0030(string EmployeeID, DateTime CheckDate);
        List<INFOTYPE0030> GetInfotype0030List(string EmployeeID1, string EmployeeID2, string Profile);

        List<INFOTYPE0185> GetInfotype0185(string EmployeeID, DateTime date);

        List<INFOTYPE0001> GetInfotype0001AllList(string EmployeeID);
    }
}
