using System;
using System.Collections.Generic;
using ESS.EMPLOYEE.ABSTRACT;
using ESS.EMPLOYEE.CONFIG.OM;
using ESS.EMPLOYEE.CONFIG.PA;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.DATA;
using ESS.WORKFLOW;
using System.Data;
using System.Reflection;

namespace ESS.EMPLOYEE
{
    [Serializable()]
    public class EmployeeData : AbstractObject
    {
        #region " Constructors "
        public EmployeeData()
            : this("", DateTime.Now)
        {
        }

        public EmployeeData(string employeeid)
            : this(employeeid, "", DateTime.Now)
        {
        }
       

        public EmployeeData(string employeeid, string positionid)
            : this(employeeid, positionid, DateTime.Now)
        {
        }

        public EmployeeData(string employeeid, string positionid,string companycode)
            : this(employeeid, positionid,companycode, DateTime.Now)
        {
        }


        public EmployeeData(string employeeid, DateTime checkDate)
            : this(employeeid, "", checkDate)
        {
        }

        public EmployeeData(string employeeid, string positionid, DateTime checkDate)
        {
            this.EmployeeID = employeeid;
            this.CheckDate = checkDate;
            this.PositionID = positionid;
            if (WorkflowPrinciple.Current != null)
            {
                this.CompanyCode = WorkflowPrinciple.CurrentIdentity.CompanyCode;
            }
            if (!string.IsNullOrEmpty(positionid))
            {
                this.ActionOfPosition = OMManagement.CreateInstance(CompanyCode).GetObjectData(ObjectType.S, positionid, checkDate, currentSetting.Language);
            }
        }

        public EmployeeData(string employeeid, string positionid,string companycode, DateTime checkDate)
        {
            this.EmployeeID = employeeid;
            this.CheckDate = checkDate;
            this.CompanyCode = companycode;
            if (!string.IsNullOrEmpty(positionid))
            {
                this.ActionOfPosition = OMManagement.CreateInstance(CompanyCode).GetObjectData(ObjectType.S, positionid, checkDate, currentSetting.Language);
                this.PositionID = positionid;
            }
        }

        #endregion " Constructors "

        #region " Basic Property "

        public string IdentityCardNO
        {
            get
            {
                string oIdentityCardNo = string.Empty;
                List<INFOTYPE0185> oINFOTYPE0185 = EmployeeManagement.CreateInstance(CompanyCode).GetInfotype0185ByEmpID(WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID);
                if (oINFOTYPE0185 != null)
                {
                    oIdentityCardNo = oINFOTYPE0185.Find(all => all.SubType == "01" || all.SubType == "06").CardID;
                }
                return oIdentityCardNo;
            }
        }

        /*public EmployeeSubAreaType EmpSubAreaType
        {
            //Each Corporate not same
            get
            {
                switch (this.OrgAssignment.SubArea.Substring(this.OrgAssignment.SubArea.Length - 2, 2))
                {
                    case "01":
                        return EmployeeSubAreaType.Flex;

                    case "02":
                        return EmployeeSubAreaType.Norm;

                    case "03":
                        return EmployeeSubAreaType.Turn;

                    case "04":
                        return EmployeeSubAreaType.Shift12;

                    default:
                        return new EmployeeSubAreaType();
                }
            }
        }*/

        public EmployeeSubAreaType EmpSubAreaType
        {
            get
            {
                switch (this.OrgAssignment.SubArea.Substring(this.OrgAssignment.SubArea.Length - 2, 2))
                {
                    case "03":
                        return EmployeeSubAreaType.Flex;
                    case "04":
                        return EmployeeSubAreaType.Norm;
                    case "05":
                        return EmployeeSubAreaType.Shift12;
                    default:
                        return new EmployeeSubAreaType();
                }
            }

        }

        public string EmployeeID { get; set; }

        public DateSpecificData __dateSpecificData { get; set; }

        public string CompanyCode { get; set; }

        public bool IsExternalUser
        {
            get
            {
                return false;
                // return ServiceManager.CreateInstance(CompanyCode).EmployeeService.IsExternalUser(this.EmployeeID);
            }
        }

        public DateTime CheckDate { get; set; }

        private string __PositionID;
        public string PositionID { get{
            if (string.IsNullOrEmpty(__PositionID))
                return this.OrgAssignment.Position;
            else
                return __PositionID;
        }
            set { __PositionID = value; }
        }
        private DataTable __monthlyWorkSchedule = null;
        public INFOTYPE0001 OrgAssignment
        {
            get
            {
                return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetInfotype0001(this.EmployeeID, this.CheckDate); 
            }
        }

        public INFOTYPE0030 Secondment
        {
            get
            {
                return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetInfotype0030(this.EmployeeID, this.CheckDate); ;
            }
        }

        private INFOTYPE1000 __actionOfPostion = null;
        public INFOTYPE1000 ActionOfPosition
        {
            get
            {
                if (__actionOfPostion == null)
                    __actionOfPostion = OMManagement.CreateInstance(CompanyCode).GetObjectData(ObjectType.S, this.OrgAssignment.Position, this.CheckDate.Date, currentSetting.Language); 
                return __actionOfPostion;
            }
            set
            {
                __actionOfPostion = value;
            }
        }

        public INFOTYPE1000 CurrentPosition
        {
            get
            {
                return ServiceManager.CreateInstance(CompanyCode).OMService.GetObjectData(ObjectType.S, this.PositionID, this.CheckDate.Date, currentSetting.Language);
            }
        }

        public INFOTYPE1000 Job
        {
            get
            {
                return ServiceManager.CreateInstance(CompanyCode).OMService.GetJobByPositionID(this.OrgAssignment.Position, CheckDate); ;
            }
        }

        public int JobLevel
        {
            get
            {
                int level = 0;
                try
                {
                    int length = Job.ShortText.Length;
                    if (length >= 2)
                        level = int.Parse(Job.ShortText.Substring(length - 2, 2));
                }
                catch (Exception e)
                {
                    throw new Exception("Job Object has a invalid job level in short text", e);
                }
                return level;
            }
        }

        public INFOTYPE1000 CurrentOrganization
        {
            get
            {
                if ((WorkflowPrinciple.CurrentIdentity.EmployeeID == this.EmployeeID))
                {
                    return OMManagement.CreateInstance(CompanyCode).GetOrganizationByPositionID(WorkflowPrinciple.CurrentIdentity.CurrentPosition, WorkflowPrinciple.Current.UserSetting.Language);
                }
                else
                {
                    return OMManagement.CreateInstance(CompanyCode).GetOrganizationByPositionID(this.ActionOfPosition == null ? this.PositionID : this.ActionOfPosition.ObjectID, WorkflowPrinciple.Current.UserSetting.Language);
                }
            }
        }

        public INFOTYPE1013 PositionBandLevel
        {
            get
            {
                //if (__infotype1013 == null && this.ActionOfPosition != null)
                //{
                //    __infotype1013 = ServiceManager.CreateInstance(CompanyCode).OMService.GetPositionBandLevel(ActionOfPosition.ObjectID, this.CheckDate);
                //}
                return ServiceManager.CreateInstance(CompanyCode).OMService.GetPositionBandLevel(ActionOfPosition.ObjectID, this.CheckDate); ;
            }
        }

        public INFOTYPE1003 ApprovalData
        {
            get
            {
                //if (__infotype1003 == null)
                //{
                //    __infotype1003 = ServiceManager.CreateInstance(CompanyCode).OMService.GetApprovalData(ActionOfPosition.ObjectID, this.CheckDate);
                //}
                return ServiceManager.CreateInstance(CompanyCode).OMService.GetApprovalData(ActionOfPosition.ObjectID, this.CheckDate); ;
            }
        }

        public DataTable MonthlyWorkSchedule
        {
            get
            {
                if(__monthlyWorkSchedule == null)
                {
                    __monthlyWorkSchedule = ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetMonthlyWorkSchedule(this.EmployeeID);
                }
                return __monthlyWorkSchedule;
            }
            set
            {
                __monthlyWorkSchedule = value;
            }
        }

        public List<ESS.EMPLOYEE.CONFIG.TM.INFOTYPE0007> GetWorkSchedule(int Year, int Month)
        {
            return EmployeeManagement.CreateInstance(CompanyCode).GetWorkSchedule(this.EmployeeID, Year, Month);
        }

        #endregion " Basic Property "

        #region " Method "

        #region " GetInfoType "

        public Type GetInfotype(string code)
        {
            Type oType = ServiceManager.CreateInstance(CompanyCode).GetInfotype(code);
            return oType;
        }

        #endregion " GetInfoType "

        public List<EmployeeData> GetDelegatePersons()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetDelegatePersons(EmployeeID);
        }

        public UserSetting GetUserSetting(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetUserSetting(EmployeeID);
        }

        public INFOTYPE0007 GetEmployeeWF(string EmployeeID, DateTime CheckDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetInfotype0007(EmployeeID, CheckDate);
        }

        public string GetEmployeeIDFromUserID(string UserID)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetEmployeeIDFromUserID(UserID);
        }
        public string GetEmployeeIDFromUserID(string UserID,string oCompanyCode)
        {
            return ServiceManager.CreateInstance(oCompanyCode).EmployeeService.GetEmployeeIDFromUserID(UserID);
        }
        public bool ValidateEmployeeID(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.ValidateEmployeeID(EmployeeID);
        }

        //AddBy: Ratchatawan W. (2012-04-23)
        public bool ValidateManager(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.ValidateManager(EmployeeID);
        }

        public INFOTYPE1000 GetFirstRoot(string LanguageCode)
        {
            return OMManagement.CreateInstance(CompanyCode).GetOrgRoot(this.OrgAssignment.OrgUnit, 1, this.CheckDate, LanguageCode);
        }

        public EmployeeData GetEmployeeByPositionID(string PositionID)
        {
            return GetEmployeeByPositionID(PositionID, DateTime.Now);
        }

        public EmployeeData GetEmployeeByPositionID(string PositionID, string LanguageCode)
        {
            return GetEmployeeByPositionID(PositionID, DateTime.Now, LanguageCode);
        }

        public EmployeeData GetEmployeeByPositionID(string PositionID, DateTime CheckDate)
        {
            return GetEmployeeByPositionID(PositionID, CheckDate, "");
        }

        public EmployeeData GetEmployeeByPositionID(string PositionID, DateTime CheckDate, string LanguageCode)
        {
            EmployeeData oReturn;
            INFOTYPE1000 oPosition = ServiceManager.CreateInstance(CompanyCode).OMService.GetObjectData(ObjectType.S, PositionID, CheckDate, LanguageCode);
            oReturn = OMManagement.CreateInstance(CompanyCode).GetEmployeeByPositionID(PositionID, CheckDate);
            if (oReturn != null)
                oReturn.ActionOfPosition = oPosition;
            return oReturn;
        }


        public EmployeeData FindManager(string ManagerCode)
        {
            //return new EmployeeData();
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.FindManager(this.EmployeeID,this.PositionID, ManagerCode, this.CheckDate, currentSetting.Language);
        }

        public EmployeeData FindManager()
        {
            return FindManager("#FIRSTAPPROVER");
        }
       

        #region " GetUserRoles "

        public List<string> GetUserRoles()
        {
            if (currentSetting == null)
            {
                currentSetting = new EmployeeData().GetUserSetting(this.EmployeeID);
            }
            return currentSetting.Roles;
        }

        public List<string> GetUserRoles2()
        {
            if (currentSetting == null)
            {
                currentSetting = new EmployeeData().GetUserSetting(this.EmployeeID);
            }
            return currentSetting.Roles;
        }

        #endregion " GetUserRoles "

        #region " AlternativeName "

        public INFOTYPE1000 PositionData(string Language)
        {
            return ServiceManager.CreateInstance(CompanyCode).OMService.GetObjectData(ObjectType.S, this.OrgAssignment.Position, this.CheckDate, Language);
        }

        #endregion " AlternativeName "

        #region " Role action "

        public bool IsInRole(string role)
        {
            return GetUserRoles().Contains(role.ToUpper());
        }

        public bool IsInRole(List<string> roles)
        {
            foreach (string role in roles)
            {
                if (IsInRole(role))
                {
                    return true;
                }
            }
            return false;
        }

        public List<EmployeeData> LoadUserResponsible(List<UserRoleResponseSetting> roles)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();
            List<string> empList = new List<string>();
            foreach (UserRoleResponseSetting role in roles)
            {
                if (IsInRole(role.UserRole))
                {
                    List<EmployeeData> buffer = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetUserInResponse(role);
                    foreach (EmployeeData emp in buffer)
                    {
                        if (!empList.Contains(emp.EmployeeID))
                        {
                            empList.Add(emp.EmployeeID);
                            oReturn.Add(emp);
                        }
                    }
                }
            }
            return oReturn;
        }

        //AddBy: Ratchatawan W. (2013-03-05)
        public List<EmployeeData> LoadUserResponsible(List<UserRoleResponseSetting> roles, DateTime oCheckDate)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();
            List<string> empList = new List<string>();
            foreach (UserRoleResponseSetting role in roles)
            {
                if (IsInRole(role.UserRole))
                {
                    List<EmployeeData> buffer = EMPLOYEE.ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetUserInResponse(role, oCheckDate);
                    foreach (EmployeeData emp in buffer)
                    {
                        if (!empList.Contains(emp.EmployeeID))
                        {
                            empList.Add(emp.EmployeeID);
                            oReturn.Add(emp);
                        }
                    }
                }
            }
            return oReturn;
        }

        //AddBy: Ratchatawan W. (2013-03-0)
        public List<EmployeeData> LoadUserResponsible(List<UserRoleResponseSetting> roles, DateTime BeginDate, DateTime EndDate)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();
            List<string> empList = new List<string>();
            foreach (UserRoleResponseSetting role in roles)
            {
                if (IsInRole(role.UserRole))
                {
                    List<EmployeeData> buffer = ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetUserInResponse(role, BeginDate, EndDate);
                    foreach (EmployeeData emp in buffer)
                    {
                        if (!empList.Contains(emp.EmployeeID))
                        {
                            empList.Add(emp.EmployeeID);
                            oReturn.Add(emp);
                        }
                    }
                }
            }
            return oReturn;
        }

        public List<EmployeeData> LoadUserResponsible(List<UserRoleResponseSetting> roles, string EmployeeID)
        {
            List<EmployeeData> oReturn = new List<EmployeeData>();
            List<string> empList = new List<string>();
            foreach (UserRoleResponseSetting role in roles)
            {
                if (IsInRole(role.UserRole))
                {
                    List<EmployeeData> buffer = ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetUserInResponse(role, EmployeeID);
                    foreach (EmployeeData emp in buffer)
                    {
                        if (!empList.Contains(emp.EmployeeID))
                        {
                            empList.Add(emp.EmployeeID);
                            oReturn.Add(emp);
                        }
                    }
                }
            }
            return oReturn;
        }

        public bool IsUserInResponse(List<UserRoleResponseSetting> roles, string EmployeeID)
        {
            foreach (UserRoleResponseSetting role in roles)
            {
                if (IsInRole(role.UserRole))
                {
                    if (ServiceManager.CreateInstance(CompanyCode).EmployeeService.IsUserInResponse(role, EmployeeID))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        #endregion " Role action "

        #endregion " Method "

        #region " Property for Flow "

        #region " Name "

        public string Name
        {
            get
            {
                if (OrgAssignment == null)
                {
                    if (EmployeeID.StartsWith("#"))
                    {
                        if (EmployeeID == "#SYSTEM")
                        {
                            return "WORKFLOW SYSTEM";
                        }
                        else
                        {
                            return EmployeeID;
                        }
                    }
                    else
                    {
                        return "";
                    }
                }
                else
                {
                    return OrgAssignment.Name;
                }
            }
        }

        #endregion " Name "

        #region " AlternativeName "

        public string AlternativeName(string Language)
        {
            if (Language.ToUpper() == "TH")
            {
                return Name;
            }
            else
            {
                INFOTYPE0182 name = ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetInfotype0182(this.EmployeeID, Language);
                return name.AlternateName;
            }
        }

        public string AlternativeNameForMail(string Language)
        {
            if (this.IsExternalUser)
            {
                return this.Name;
            }
            else
            {
                if (Language.ToUpper() == "TH")
                {
                    return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetNameFromInfotype0002Name(this.EmployeeID);
                }
                else
                {
                    INFOTYPE0182 name = ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetInfotype0182(this.EmployeeID, Language);
                    return name.AlternateName;
                }
            }
        }

        #endregion " AlternativeName "

        #region Email
		public string EmailAddress
        {
            get
            {
                INFOTYPE0105 oResult = ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetInfotype0105(this.EmployeeID, "MAIL");
                return oResult.DataText; // change from email.DataText_Long to email.DataText fixed null email address in request PIN of pay slip.
            }
        } 
        #endregion

        #region MobileNo
        public string MobileNo
        {
            get
            {
                //CELLPHONE
                INFOTYPE0105 oResult = ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetInfotype0105(this.EmployeeID, "CELLPHONE");
                return oResult.DataText; // change from email.DataText_Long to email.DataText fixed null email address in request PIN of pay slip.
            }
        } 
        #endregion

        #region FaxNumber
        public string FaxNumber
        {
            get
            {
                //FAXNUMBER
                INFOTYPE0105 oResult = ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetInfotype0105(this.EmployeeID, "FAXNUMBER");
                return oResult.DataText; // change from email.DataText_Long to email.DataText fixed null email address in request PIN of pay slip.
            }
        }
        
        #endregion

        #region HomeNumber
        public string HomeNumber
        {
            get
            {
                //HOMENUMBER
                INFOTYPE0105 oResult = ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetInfotype0105(this.EmployeeID, "HOMENUMBER");
                return oResult.DataText; // change from email.DataText_Long to email.DataText fixed null email address in request PIN of pay slip.
            }
        } 
        #endregion
        public string OfficeNumber
        {
            get
            {
                //OFFICENUMBER
                INFOTYPE0105 oResult = ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetInfotype0105(this.EmployeeID, "OFFICENUMBER");
                return oResult.DataText; // change from email.DataText_Long to email.DataText fixed null email address in request PIN of pay slip.
            }
        }


        #region " EmpGroup "
        public string EmpGroup
        {
            get
            {
                if (this.PositionBandLevel == null)
                {
                    return "";
                }
                return this.PositionBandLevel.EmpGroup;
            }
        }

        #endregion " EmpGroup "


        #region " EmpSubGroup "

        public decimal EmpSubGroup
        {
            get
            {
                if (EmpSubGroupForDelegate > 0)
                    return EmpSubGroupForDelegate;
                Decimal retVal = 0;
                if (this.PositionBandLevel != null)
                    Decimal.TryParse(this.PositionBandLevel.EmpSubGroup, out retVal);
                return retVal;
            }
        }

        public int BrandNo
        {
            get
            {
                int retVal = 0;
                if (this.PositionBandLevel != null)
                    retVal = this.PositionBandLevel.BrandNo;
                return retVal;
            }
        }

        public decimal EmpSubGroupForDelegate { get; set; }

        #endregion " EmpSubGroup "

        #region " NextEmpGroup "

        public string NextEmpGroup
        {
            get
            {
                //if (__nextEmpGroup == null)
                //{
                //    INFOTYPE1000 nextPos = OMManagement.CreateInstance(CompanyCode).FindNextPosition(this.ActionOfPosition.ObjectID, this.CheckDate, "EN");
                //    __nextEmpGroup = ServiceManager.CreateInstance(CompanyCode).OMService.GetPositionBandLevel(nextPos.ObjectID, this.CheckDate);
                //    return __nextEmpGroup.EmpGroup;
                //}
                INFOTYPE1000 nextPos = OMManagement.CreateInstance(CompanyCode).FindNextPosition(this.ActionOfPosition.ObjectID, this.CheckDate, "EN");
                return ServiceManager.CreateInstance(CompanyCode).OMService.GetPositionBandLevel(nextPos.ObjectID, this.CheckDate).EmpGroup;
            }
        }

        #endregion " NextEmpGroup "

        #region " IsManager "

        public bool IsManager
        {
            get
            {
                return this.IsHaveSubOrdinate && !this.ApprovalData.IsStaff;
            }
        }

        public bool IsSystem { get; set; }


        #endregion " IsManager "

        #region " IsHaveSubOrdinate "

        public bool IsHaveSubOrdinate
        {
            get
            {
                return ServiceManager.CreateInstance(CompanyCode).OMService.IsHaveSubOrdinate(ActionOfPosition.ObjectID, this.CheckDate);
            }
        }

        #endregion " IsHaveSubOrdinate "

        #region " UserRoles "

        public string UserRoles
        {
            get
            {
                string returnValue = "";
                foreach (string roleName in GetUserRoles())
                {
                    returnValue += string.Format("{0},", roleName);
                }
                return returnValue;
            }
        }

        #endregion " UserRoles "

        #endregion " Property for Flow "

        #region " Other properties "

        protected EmployeeData currentLogon
        {
            get
            {
                return currentSetting.Employee;
            }
        }

        protected UserSetting currentSetting
        {
            get
            {
                if (WorkflowPrinciple.Current == null)
                {
                    return new UserSetting("", "");
                }
                return WorkflowPrinciple.Current.UserSetting;
            }
            set
            {

            }
        }

        public string MultiLanguageName
        {
            get
            {
                if (currentSetting.Language == "TH")
                {
                    return OrgAssignment.Name;
                }
                else
                {
                    return AlternativeName(currentSetting.Language);
                }
            }
        }

        public DateSpecificData DateSpecific
        {
            get
            {
                if (__dateSpecificData == null)
                {
                    __dateSpecificData = EmployeeManagement.CreateInstance(CompanyCode).GetDateSpecificData(this.EmployeeID);
                }
                return __dateSpecificData;//EmployeeManagement.CreateInstance(CompanyCode).GetDateSpecificData(this.EmployeeID);
            }
        }

        public object PersonalData
        {
            get
            {
                Type type = GetInfotype("INFOTYPE0002");
                AbstractInfoType objectdata = (AbstractInfoType)Activator.CreateInstance(type);
                return objectdata.LoadData(EmployeeID, "", CheckDate, CheckDate);
            }
        }

        public INFOTYPE1000 DepartmentData
        {
            get
            {
                return OMManagement.CreateInstance(CompanyCode).FindDepartment(this.OrgAssignment.OrgUnitData, currentSetting.Language);
            }
        }

        public List<INFOTYPE1000> GetAllPositions(string LanguageCode)
        {
            return OMManagement.CreateInstance(CompanyCode).GetAllPosition(this.EmployeeID, this.CheckDate, LanguageCode);
        }

        #endregion " Other properties "

        public List<EmployeeData> GetDelegateEmployeeForSentMail(string DelegateFromID, string DelegateFromPositionID, int RequestTypeID, DateTime CheckDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetDelegateEmployeeForSentMail(DelegateFromID, DelegateFromPositionID, RequestTypeID, CheckDate);
        }

        public DataTable ValidateData()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.ValidateData(this.EmployeeID);
        }

        #region GetDailyWorkSchedule
        public DailyWS GetDailyWorkSchedule(DateTime date)
        {
            MonthlyWS oMWS = MonthlyWS.GetCalendar(this.EmployeeID, date.Year, date.Month);
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetDailyWorkschedule(this.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, oMWS.GetWSCode(date.Day, true), date);
        }
        public DailyWS GetDailyWorkSchedule(DateTime date, string CompCode)
        {
            MonthlyWS oMWS = MonthlyWS.GetCalendar(this.EmployeeID, date.Year, date.Month, CompCode);
            return ServiceManager.CreateInstance(CompCode).EmployeeConfig.GetDailyWorkschedule(this.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, oMWS.GetWSCode(date.Day, true), date);
        }
        public DailyWS GetDailyWorkSchedule1(DateTime date)
        {
            try
            {
                MonthlyWS oMWS = MonthlyWS.GetCalendar(this.EmployeeID, date.Year, date.Month);
                DailyWS dws = ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetDailyWorkschedule(this.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, oMWS.GetWSCode(date.Day, true), date);
                dws.IsHoliday = oMWS.GetWSCode(date.Day, false) != "";
                return dws;
            }
            catch (Exception e)
            {
                DailyWS dws = new DailyWS();
                dws.DailyWorkscheduleCode = "UNKNOWN";
                dws.BeginDate = date;
                dws.WorkscheduleClass = "UNKNOWN";
                dws.IsHoliday = false;
                dws.WorkBeginTime = new TimeSpan(0, 0, 0);
                dws.WorkEndTime = new TimeSpan(0, 0, 0);
                return dws;
            }
        }
        public DailyWS GetDailyWorkSchedule(DateTime date, bool includeSubstitute)
        {
            MonthlyWS oMWS = MonthlyWS.GetCalendar(this.EmployeeID, date, date.Year, date.Month, includeSubstitute);
            DailyWS dws = ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetDailyWorkschedule(this.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, oMWS.GetWSCode(date.Day, true), date);
            dws.IsHoliday = oMWS.GetWSCode(date.Day, false) != "";
            return dws;
        }
        #endregion


        #region MonthlyWorkschedule

        public MonthlyWS GetSimulateCalendar(PersonalSubAreaSetting SubAreaSetting, PersonalSubGroupSetting SubGroupSetting, int Year, int Month, string WFRule)
        {
            string cSubGroup = SubGroupSetting.WorkScheduleGrouping;
            string cSubArea = SubAreaSetting.WorkScheduleGrouping;
            string cHoliday = SubAreaSetting.HolidayCalendar;
            MonthlyWS oReturn = new MonthlyWS();
            oReturn.WS_Year = Year;
            oReturn.WS_Month = Month;
            oReturn.EmpSubGroupForWorkSchedule = cSubGroup;
            oReturn.EmpSubAreaForWorkSchedule = cSubArea;
            oReturn.PublicHolidayCalendar = cHoliday;

            bool lFirst = true;
            DateTime dTemp = new DateTime(Year, Month, 1);
            //MonthlyWS temp = EMPLOYEE.ServiceManager.EmployeeConfig.GetMonthlyWorkschedule(SubGroupSetting.WorkScheduleGrouping, SubAreaSetting.WorkScheduleGrouping, SubAreaSetting.HolidayCalendar, WFRule, Year, Month);

            MonthlyWS temp = ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetMonthlyWorkschedule(SubGroupSetting.WorkScheduleGrouping, SubAreaSetting.WorkScheduleGrouping, SubAreaSetting.HolidayCalendar, WFRule, Year, Month);

            oReturn.WorkScheduleRule = temp.WorkScheduleRule;
            if (lFirst)
            {
                oReturn.ValuationClass = temp.ValuationClass;
            }
            lFirst = false;
            int nStart = dTemp.Day;
            int nEnd = dTemp.AddMonths(1).AddDays(-1).Day;
            oReturn.SetValuationClassExactly(nEnd, temp.ValuationClass);
            PropertyInfo oProp;
            Type oType = oReturn.GetType();
            for (int index = nStart; index <= nEnd; index++)
            {
                string cDayNo = index.ToString("00");
                string cDWSCode;
                oProp = oType.GetProperty(string.Format("Day{0}", cDayNo));

                cDWSCode = (string)oProp.GetValue(temp, null);

                oProp.SetValue(oReturn, cDWSCode, null);

                oProp = oType.GetProperty(string.Format("Day{0}_H", cDayNo));
                oProp.SetValue(oReturn, oProp.GetValue(temp, null), null);
            }

            return oReturn;
        }
        public List<MonthlyWS> SimulateMonthlyWorkschedule(string EmpSubGroupForWorkSchedule, string PublicHolidayCalendar, string EmpSubAreaForWorkSchedule, int Year, int Month, Dictionary<string, string> DayOption)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.SimulateMonthlyWorkschedule(EmpSubGroupForWorkSchedule, PublicHolidayCalendar, EmpSubAreaForWorkSchedule, Year, Month, DayOption);
        }

        #endregion
        //AddBy: Ratchatawan W. (2013-03-07)
        public DailyWS GetDailyWorkSchedule(string WSCODE, DateTime date)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetDailyWorkschedule(this.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, WSCODE, date);
            //return DailyWS.GetDailyWorkschedule(this.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, WSCODE, date);
        }  
    }
}