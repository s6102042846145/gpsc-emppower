#region Using

using System;
using System.Collections.Generic;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.EMPLOYEE.INTERFACE;
using ESS.EMPLOYEE.CONFIG.PA;
using ESS.EMPLOYEE.CONFIG.OM;
using System.Data;
using ESS.UTILITY.DATACLASS;

#endregion Using

namespace ESS.EMPLOYEE.ABSTRACT
{
    public class AbstractEmployeeConfigService : IEmployeeConfigService
    {
        #region IEmployeeConfig Members
        
        public virtual PersonalArea GetPersonalAreaSetting(string PersonalArea)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual PersonalSubAreaSetting GetPersonalSubAreaSetting(string PersonalArea, string PersonalSubArea)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<PersonalSubAreaSetting> GetPersonalSubAreaSettingList(string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SavePersonalSubAreaSettingList(List<PersonalSubAreaSetting> Data, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<PersonalSubGroupSetting> GetPersonalSubGroupSettingList(string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SavePersonalSubGroupSettingList(List<PersonalSubGroupSetting> Data, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual PersonalSubGroupSetting GetPersonalSubGroupSetting(string EmpGroup, string EmpSubGroup)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<MonthlyWS> GetMonthlyWorkscheduleList(int Year, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual MonthlyWS GetMonthlyWorkschedule(string EmpSubGroupForWorkSchedule, string EmpSubAreaForWorkSchedule, string PublicHolidayCalendar, string WorkScheduleRule, int Year, int Month)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveMonthlyWorkscheduleList(int Year, List<MonthlyWS> Data, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<DailyWS> GetDailyWorkscheduleList(string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveDailyWorkscheduleList(List<DailyWS> Data, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DailyWS GetDailyWorkschedule(string DailyGroup, string DailyCode, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual BreakPattern GetBreakPattern(string DWSGroup, string BreakCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual WFRuleSetting GetWFRuleSetting(string WFRule)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<MonthlyWS> GetMonthlyWorkscheduleGroup(string EmpSubGroupForWorkSchedule, string EmpSubAreaForWorkSchedule, string PublicHolidayCalendar)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<MonthlyWS> GetMonthlyWorkscheduleGroup(string EmpSubGroupForWorkSchedule, string EmpSubAreaForWorkSchedule, string PublicHolidayCalendar, bool IncludeMonth, bool IncludeYear)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<PersonalSubAreaSetting> GetPersonalSubAreaByArea(string strArea)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<MonthlyWS> SimulateMonthlyWorkschedule(string EmpSubGroupForWorkSchedule, string PublicHolidayCalendar, string EmpSubAreaForWorkSchedule, int Year, int Month, Dictionary<string, string> DayOption)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<DailyWS> GetDailyWSByWorkscheduleGrouping(string WorkScheduleGrouping, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<string> GetPersonalSubAreaWSRMapping(string strSubArea)
        {
            throw new Exception("The method or operation is not implemented.");
        }


        public virtual List<INFOTYPE1000> GetGenderCheckBoxData()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetPrefixDropdownData()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetMaritalStatusDropdownData()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetNationalityDropdownData()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetLanguageDropdownData()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetReligionDropdownData()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetEmpGroupDropdownData()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetEmpSubGroupDropdownData()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetSubTypeDropdownData()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetCostCenterDropdownData(DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetCostCenterByOrganize(string Organize)
        {
            throw new Exception("The method or operation is not impliemented.");
        }

        public virtual List<INFOTYPE1000> GetAreaDropdownData()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetSubAreaDropdownData()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> GetRelationSelectData()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<string> GetRelationValidationByRelation(string relation)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<INFOTYPE1000> GetRelationValidationSelectData()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion IEmployeeConfig Members

        public virtual DataTable GetMonthlyWorkscheduleGetByPeriod(string EmployeeID, int iYear, int iMonth)
        {
            throw new NotImplementedException();
        }
        public virtual bool InsertActionLog(ActionLog oActionLog)
        {
            throw new NotImplementedException();
        }

        public virtual string GetBusinessPlace(string oBusinessArea)
        {
            throw new NotImplementedException();
        }

        public virtual List<MonthlyWS> GetMonthlyWS(string EmployeeID, int CheckYear)
        {
            throw new NotImplementedException();
        }

        public virtual string GetSupervisorPositionByEmpPosition(string EmpPosition)
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }
}