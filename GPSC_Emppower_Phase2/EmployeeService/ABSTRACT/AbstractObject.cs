using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Text;
using ESS.EMPLOYEE;
using ESS.WORKFLOW;

namespace ESS.DATA
{
    [Serializable()]
    public class AbstractObject
    {
        public AbstractObject()
        { 
        }

        protected EmployeeData currentLogon
        {
            get
            {
                return currentSetting.Employee;
            }
        }

        protected UserSetting currentSetting
        {
            get
            {
                if (WorkflowPrinciple.Current == null)
                {
                    return new UserSetting("");
                }
                return WorkflowPrinciple.Current.UserSetting;
            }
        }

        public DataTable ToADODataTable()
        {
            return ToADODataTable(false);
        }

        protected virtual string GetEnumString(PropertyInfo prop)
        {
            return prop.GetValue(this, null).ToString();
        }

        protected virtual DataColumn CreateComplexTypeColumn(PropertyInfo prop)
        {
            //DataColumn oDC;
            DataColumn oDC = new DataColumn(prop.Name, prop.PropertyType);
            return oDC;
        }

        public object[] FindKeys(DataColumn[] primaryKeys)
        {
            DataTable oTable = new DataTable("TEMPTABLE");
            foreach (DataColumn oDC in primaryKeys)
            {
                oTable.Columns.Add(oDC);
            }
            this.LoadDataToTable(oTable);
            return oTable.Rows[0].ItemArray;
        }

        public DataTable ToADODataTable(bool OnlySchema)
        {
            DataTable oTable = new DataTable();
            Type oType = this.GetType();
            DataColumn oDC;
            foreach (PropertyInfo prop in oType.GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                if (prop.CanWrite && prop.CanRead)
                {
                    if (prop.PropertyType.IsEnum)
                    {
                        oDC = oTable.Columns.Add(prop.Name, typeof(string));
                    }
                    else if (prop.PropertyType.IsValueType)
                    {
                        oDC = oTable.Columns.Add(prop.Name, prop.PropertyType);
                    }
                    else
                    {
                        oDC = this.CreateComplexTypeColumn(prop);
                        oTable.Columns.Add(oDC);
                    }
                }
            }
            if (!OnlySchema)
            {
                LoadDataToTable(oTable);
            }
            return oTable;
        }

        protected virtual void SetDataRowToObject(PropertyInfo prop, DataRow dr)
        {
            Type oType = this.GetType();
            DataColumn oDC;
            oDC = dr.Table.Columns[prop.Name];
            if (oDC != null)
            {
                if (!dr.IsNull(oDC))
                {
                    if (prop.PropertyType.IsEnum)
                    {
                        if (!dr.IsNull(oDC) && Enum.IsDefined(prop.PropertyType, dr[oDC]))
                        {
                            prop.SetValue(this, Enum.Parse(prop.PropertyType, (string)dr[oDC], true), null);
                        }
                        else
                        {
                            if (oDC.DataType == typeof(string))
                            {
                                prop.SetValue(this, Enum.ToObject(prop.PropertyType, Convert.ChangeType(Char.ConvertToUtf32((string)dr[oDC], 0), Enum.GetUnderlyingType(prop.PropertyType))), null);
                            }
                            else
                            {
                                prop.SetValue(this, Enum.ToObject(prop.PropertyType, Convert.ChangeType(dr[oDC], Enum.GetUnderlyingType(prop.PropertyType))), null);
                            }
                        }
                    }
                    else if (prop.PropertyType == typeof(TimeSpan))
                    {
                        TimeSpan buffer = TimeSpan.MinValue;
                        if (oDC.DataType == typeof(string))
                        {
                            string value = ((string)dr[oDC]).Trim();
                            if (value != "")
                            {
                                buffer = TimeSpan.Parse(value);
                            }
                        }
                        else if (oDC.DataType == typeof(TimeSpan))
                        {
                            buffer = (TimeSpan)dr[oDC];
                        }
                        else if (oDC.DataType == typeof(DateTime))
                        {
                            buffer = ((DateTime)dr[oDC]).TimeOfDay;
                        }
                        prop.SetValue(this, buffer, null);
                    }
                    else if (prop.PropertyType == typeof(Int32))
                    {
                        prop.SetValue(this, Convert.ToInt32(dr[oDC]), null);
                    }
                    else if (prop.PropertyType == typeof(Decimal))
                    {
                        prop.SetValue(this, Convert.ToDecimal(dr[oDC]), null);
                    }
                    else if (prop.PropertyType == typeof(string))
                    {
                        prop.SetValue(this, dr[oDC].ToString(), null);
                    }
                    else
                    {
                        try
                        {
                            prop.SetValue(this, dr[oDC], null);
                        }
                        catch
                        { 
                        }
                    }
                }
                else if (prop.PropertyType == typeof(DateTime))
                {
                    prop.SetValue(this, DateTime.MinValue, null);
                }
                else if (prop.PropertyType == typeof(TimeSpan))
                {
                    prop.SetValue(this, TimeSpan.MinValue, null);
                }
            }
        }

        protected virtual void LoadObjectToDataRow(PropertyInfo prop, DataRow oNewRow, DataColumn oDC)
        {
            if (oDC == null)
            {
                return;
            }
            if (prop.PropertyType == typeof(DateTime) && (DateTime)prop.GetValue(this, null) == DateTime.MinValue)
            {
                oNewRow[oDC] = DBNull.Value;
            }
            else if (prop.PropertyType == typeof(TimeSpan))
            {
                TimeSpan oValue = (TimeSpan)prop.GetValue(this, null);
                if (oDC.DataType == typeof(TimeSpan))
                {
                    oNewRow[oDC] = oValue.ToString();
                }
                else if (oDC.DataType == typeof(string))
                {
                    if (oValue == TimeSpan.MinValue)
                    {
                        oNewRow[oDC] = "";
                    }
                    else
                    {
                        //oNewRow[oDC] = string.Format("{0}:{1}:{2}",oValue.Hours.ToString("00"),oValue.Minutes.ToString("00"),oValue.Seconds.ToString("00"));
                        oNewRow[oDC] = oValue.ToString();
                    }
                }
                else if (oDC.DataType == typeof(DateTime))
                {
                    oNewRow[oDC] = new DateTime(1900, 1, 1).Add(oValue);
                }
            }
            else
            {
                if (prop.PropertyType.IsEnum)
                {
                    if (oDC.DataType == typeof(int))
                    {
                        oNewRow[oDC] = (int)prop.GetValue(this, null);
                    }
                    else
                    {
                        oNewRow[oDC] = GetEnumString(prop);
                    }
                }
                else
                {
                    oNewRow[oDC] = prop.GetValue(this, null);
                }
            }
        }

        public virtual void ParseToObject(DataRow dr)
        {
            Type oType = this.GetType();
            foreach (PropertyInfo prop in oType.GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                if (prop.CanWrite && prop.CanRead)
                {
                    this.SetDataRowToObject(prop, dr);
                }
            }
        }

        public void ParseToObject(DataTable Data)
        {
            if (Data.Rows.Count > 0)
            {
                DataRow dr = Data.Rows[0];
                ParseToObject(dr);
            }
        }

        public void LoadDataToTableRow(DataRow DataRow)
        {
            DataTable oTable = DataRow.Table.Clone();
            this.LoadDataToTable(oTable);
            DataRow.ItemArray = oTable.Rows[0].ItemArray;
        }

        public virtual void LoadDataToTable(DataTable Data)
        {
            Type oType = this.GetType();
            DataRow oNewRow = Data.NewRow();
            DataColumn oDC;
            foreach (PropertyInfo prop in oType.GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                if (prop.CanWrite && prop.CanRead)
                {
                    oDC = Data.Columns[prop.Name];
                    this.LoadObjectToDataRow(prop, oNewRow, oDC);
                }
            }
            Data.Rows.Add(oNewRow);
        }

    }
}
