#region Using

using System;
using System.Collections.Generic;
using System.Data;
using ESS.EMPLOYEE.CONFIG.PA;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.EMPLOYEE.INTERFACE;
using ESS.EMPLOYEE.JOB;
using ESS.EMPLOYEE.CONFIG.OM;
using ESS.UTILITY.DATACLASS;
using ESS.EMPLOYEE.DATACLASS;

#endregion Using

namespace ESS.EMPLOYEE.ABSTRACT
{
    public class AbstractEmployeeDataService : IEmployeeDataService
    {
        #region IEmployeeService Members

        public virtual List<EmployeeData> GetUserInResponse(UserRoleResponseSetting setting, DateTime oCheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual bool ValidateActiveEmployeeForTravel(string EmployeeID, DateTime BeginTravelDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual string GetEmployeeIDFromUserID(string UserID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool ValidateEmployeeID(string EmployeeID)
        {
            return this.ValidateEmployeeID(EmployeeID, DateTime.Now);
        }

        public virtual bool ValidateEmployeeID(string EmployeeID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public List<INFOTYPE0000> GetInfotype0000List(string EmployeeID)
        {
            return GetInfotype0000List(EmployeeID, EmployeeID);
        }

        public List<INFOTYPE0000> GetInfotype0000List(string EmployeeID1, string EmployeeID2)
        {
            return GetInfotype0000List(EmployeeID1, EmployeeID2, "DEFAULT");
        }

        public List<INFOTYPE0000> GetInfotype0000List(string EmployeeID1, string EmployeeID2, string Profile)
        {
            return GetInfotype0000List(EmployeeID1, EmployeeID2, DateTime.MinValue, Profile);
        }

        public virtual List<INFOTYPE0000> GetInfotype0000List(string EmployeeID1, string EmployeeID2, DateTime CheckDate, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public INFOTYPE0001 GetInfotype0001(string EmployeeID)
        {
            return this.GetInfotype0001(EmployeeID, DateTime.Now);
        }

        public INFOTYPE0001 GetInfotype0001(string EmployeeID, DateTime CheckDate)
        {
            INFOTYPE0001 returnValue = null;
            List<INFOTYPE0001> list = GetInfotype0001List(EmployeeID, EmployeeID, CheckDate, "DEFAULT");
            if (list.Count > 0)
            {
                returnValue = list[0];
            }
            return returnValue;
        }

        public CARDSETTING GetCardSetting(string EmployeeID)
        {
            return this.GetCardSetting(EmployeeID, DateTime.Now);
        }

        public CARDSETTING GetCardSetting(string EmployeeID, DateTime CheckDate)
        {
            CARDSETTING returnValue = null;
            List<CARDSETTING> list = GetCardSettingList(EmployeeID, CheckDate, "DEVTIMESHEET");
            if (list.Count > 0)
            {
                returnValue = list[0];
            }
            return returnValue;
        }

        public virtual List<CARDSETTING> GetCardSettingList(string EmployeeID, DateTime CheckDate, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public List<INFOTYPE0001> GetInfotype0001List(string EmployeeID)
        {
            return GetInfotype0001List(EmployeeID, EmployeeID);
        }

        public List<INFOTYPE0001> GetInfotype0001List(string EmployeeID1, string EmployeeID2)
        {
            return GetInfotype0001List(EmployeeID1, EmployeeID2, "DEFAULT");
        }

        public List<INFOTYPE0001> GetInfotype0001List(string EmployeeID, DateTime CheckDate)
        {
            return GetInfotype0001List(EmployeeID, EmployeeID, CheckDate, "DEFAULT");
        }

        public List<INFOTYPE0001> GetInfotype0001List(string EmployeeID1, string EmployeeID2, string Profile)
        {
            return GetInfotype0001List(EmployeeID1, EmployeeID2, DateTime.Now, Profile);
        }
        public virtual List<INFOTYPE0002> GetInfotype0002List(string EmployeeID1, string EmployeeID2, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public List<INFOTYPE0001> GetInfotype0001AllList(string EmployeeID)
        {
            return GetInfotype0001List(EmployeeID, EmployeeID, DateTime.MinValue, "DEFAULT");
        }

        public virtual List<INFOTYPE0001> GetInfotype0001List(string EmployeeID1, string EmployeeID2, DateTime CheckDate, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0002> GetInfotype0002List(string EmployeeID1, string EmployeeID2, DateTime CheckDate, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveInfotype0001(string EmployeeID1, string EmployeeID2, List<INFOTYPE0001> data, string profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveInfotype0002(string EmployeeID1, string EmployeeID2, List<INFOTYPE0002> data, string profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE0182 GetInfotype0182(string EmployeeID, string Language)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void UpdateInfotype0182(string EmployeeID, string Language, INFOTYPE0182 item)
        {
            throw new Exception("The method 'UpdateInfotype0182(string EmployeeID, string Language, INFOTYPE0182 item)' is not implemented.");
        }

        public virtual void SaveInfotype0182(string EmployeeID1, string EmployeeID2, List<INFOTYPE0182> data, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0182> GetInfotype0182List(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
            //return GetInfotype0182List(EmployeeID, EmployeeID);
        }

        public List<INFOTYPE0182> GetInfotype0182List(string EmployeeID1, string EmployeeID2)
        {
            return GetInfotype0182List(EmployeeID1, EmployeeID2, "DEFAULT");
        }

        public virtual List<INFOTYPE0182> GetInfotype0182List(string EmployeeID1, string EmployeeID2, string Profile)
        {
            throw new Exception("The method 'List<INFOTYPE0182> GetInfotype0182List(string EmployeeID1, string EmployeeID2, string Profile)' is not implemented.");
        }

        public virtual INFOTYPE0105 GetInfotype0105(string EmployeeID, string oCategoryCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0105> GetInfotype0105ByCategoryCode(string EmployeeID, string oCategoryCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public List<INFOTYPE0105> GetInfotype0105List(string EmployeeID)
        {
            return GetInfotype0105List(EmployeeID, EmployeeID);
        }

        public List<INFOTYPE0105> GetInfotype0105List(string EmployeeID1, string EmployeeID2)
        {
            return GetInfotype0105List(EmployeeID1, EmployeeID2, "DEFAULT");
        }

        public virtual List<INFOTYPE0105> GetInfotype0105List(string EmployeeID1, string EmployeeID2, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveInfotype0105(string EmployeeID1, string EmployeeID2, List<INFOTYPE0105> data, string profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public List<INFOTYPE0007> GetInfotype0007(string EmployeeID, int Year, int Month)
        {
            return GetInfotype0007List(EmployeeID, EmployeeID, Year, Month, "DEFAULT");
        }

        public List<INFOTYPE0007> GetInfotype0007List(string EmployeeID1, string EmployeeID2)
        {
            return GetInfotype0007List(EmployeeID1, EmployeeID2, -1, -1, "DEFAULT");
        }

        public List<INFOTYPE0007> GetInfotype0007List(string EmployeeID1, string EmployeeID2, string Profile)
        {
            return GetInfotype0007List(EmployeeID1, EmployeeID2, -1, -1, Profile);
        }

        public virtual List<INFOTYPE0007> GetInfotype0007List(DateTime BeginDate, DateTime EndDate, string TimeEvaluateClass)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0007> GetInfotype0007List(string EmployeeID1, string EmployeeID2, int Year, int Month, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0007> GetAreaWorkscheduleInfotype0007List(string EmployeeID1, int Year, int Month)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0007> GetInfotype0007List(int Year, int Month, string TimeEvaluationClass)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveInfotype0007(string EmployeeID1, string EmployeeID2, List<INFOTYPE0007> data, string profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<INFOTYPE0027> GetInfotype0027List(string EmployeeID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual List<INFOTYPE0027> GetInfotype0027List(string EmployeeID1, string EmployeeID2, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual void SaveInfotype0027(string EmployeeID1, string EmployeeID2, List<INFOTYPE0027> data, string profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual UserSetting GetUserSetting(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<string> GetUserRole(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual UserRoleSetting GetUserRoleSetting(string UserRole)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<EmployeeData> GetUserResponse(string Role, string AdminGroup)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<EmployeeData> GetUserInResponse(UserRoleResponseSetting setting)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<EmployeeData> GetUserInResponse(UserRoleResponseSetting setting, string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual bool IsUserInResponse(UserRoleResponseSetting role, string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual bool IsUserInResponse(UserRoleResponseSetting role, string EmployeeID, DateTime checkDate)
        {
            throw new NotImplementedException();
        }
        public virtual List<ESS.EMPLOYEE.CONFIG.TM.Substitution> GetInfotype2003(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<ESS.EMPLOYEE.CONFIG.TM.Substitution> GetInfotype2003_Log(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<Substitution> GetInfotype2003(string EmployeeID, DateTime BeginDate, DateTime EndDate, string ExcludeReqNo)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual EmployeeData FindManager(string EmployeeID, string PositionID, string ManagerCode, DateTime CheckDate, string LanguageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveUserSetting(UserSetting userSetting)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<EmployeeData> GetDelegatePersons(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<TaskCopyEmployeeConfig> GetTasks()
        {
            throw new Exception("Tge method 'List<TaskCopyEmployeeConfig> GetTasks()' is not implemented.");
        }

        public List<WorkPlaceCommunication> GetWorkplaceData()
        {
            return GetWorkplaceData("");
        }

        public virtual List<WorkPlaceCommunication> GetWorkplaceData(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveWorkPlaceData(string EmployeeID, List<WorkPlaceCommunication> workplaceList, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SaveWorkPlaceData(List<WorkPlaceCommunication> workplaceList, string Profile)
        {
            SaveWorkPlaceData("", workplaceList, Profile);
        }


        public virtual List<INFOTYPE0001> GetMeEmpList(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual bool HavePinCode(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual bool VerifyPinCode(string Employeeid, string pincode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void RequestNewPIN(EmployeeData oEmp)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void AlertIncorrectPIN(EmployeeData oEmp, string subjectAlert, string bodyAlert)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void CreateNewPIN(string EmployeeID, string TicketID, string NewPINcode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void ChangePINCode(string EmployeeID, string pincode, string NewPINcode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual string CreateTicket(string TicketClass, TimeSpan LifeTime, string PINcode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual bool ValidateTicket(string EmployeeID, string TicketClass, string TicketID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<DateSpecificData> GetDateSpecificList()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<string> GetUserInRole(string UserRole)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<string> GetEmployeeIDByRole(string UserRole)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //CHAT 2011-10-05 For MassPayslip & MassTaxReport
        //CHAT 2011-10-05 ����Ѻ�֧���������ʾ�ѡ�ҹ��������˹��§ҹ�������
        public virtual DataTable GetEmployeeIDByOrgUnit(string OrgUnit)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //CHAT 2011-10-05 ����Ѻ�֧������ ResponseType �ͧ��������͵�Ǩ�ͺ�Է�������ҹ��� Type
        public virtual List<string> GetUserResponseType(string EmpID, string UserRole)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //CHAT 2011-10-10 ����Ѻ�֧������ Performance �ͧ��ѡ�ҹ
        public virtual DataTable GetRequestorPerformance(string EmpID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //CHAT 2011-10-10 ����Ѻ�֧������ Competency �ͧ��ѡ�ҹ
        public virtual DataTable GetRequestorCompetency(string EmpID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion IEmployeeService Members

        #region IEmployeeService Members

        public INFOTYPE0007 GetInfotype0007(string EmployeeID, DateTime CheckDate)
        {
            return GetInfotype0007(EmployeeID, CheckDate, "DEFAULT");
        }

        public virtual INFOTYPE0007 GetInfotype0007(string EmployeeID, DateTime CheckDate, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion IEmployeeService Members

        public virtual string GetNameFromInfotype0002Name(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //AddBy: Ratchatawan W. (2012-04-23)
        public virtual bool ValidateManager(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //AddBy: Ratchatawan W. (2012-04-23)
        public virtual List<EmployeeData> GetDelegateEmployeeOMByPosition(string EmployeeID, string PositionID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<EmployeeData> GetDelegateEmployeeForSentMail(string DelegateFromID, string DelegateFromPositionID, int RequestTypeID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<EmployeeData> GetAllActiveEmployeeInINFOTYPE0001(DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<EmployeeData> GetManagerInSameOraganizationAndSameEmpSubGroup(string EmployeeID, string PositionID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE0032 GetInfotype0032ByEmployeeID(string EmployeeID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0032> GetInfotype0032List(string EmployeeID1, string EmployeeID2, DateTime CheckDate, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveInfotype0032(string EmployeeID1, string EmployeeID2, List<INFOTYPE0032> data, string profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<EmployeeData> GetUserInResponse(UserRoleResponseSetting setting, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0001> GetAllINFOTYPE0001(DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0001> GetAllEmployeeName(string Language)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0001> GetAllEmployeeNameForUserRole(string Language)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DataSet GetUserInResponseForActionOfInstead(string EmployeeID, int SubjectID, DateTime CheckDate, string ResponseCompanyCode, string SearchText)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void DeleteInfotype0007(List<INFOTYPE0007> data, string RequestNo)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveInfotype0007(List<INFOTYPE0007> data, string RequestNo)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveAreaWorkscheduleInfotype0007(List<INFOTYPE0007> data, string RequestNo)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #region IEmployeeService Members

        public virtual List<EmployeeData> GetOTSummaryPermissionByEmployeeID(string EmployeeID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual bool IsHaveOTSummaryPermissionByEmployeeID(string EmployeeID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveInfotype0001(List<INFOTYPE0001> data, string RequestNo)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0030> GetInfotype0030List(string EmployeeID1, string EmployeeID2)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0030> GetInfotype0030List(string EmployeeID1, string EmployeeID2, string Profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveInfotype0030(string EmployeeID1, string EmployeeID2, List<INFOTYPE0030> data, string profile)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE0030 GetInfotype0030(string EmployeeID, DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion IEmployeeService Members

        public virtual List<INFOTYPE0185> GetInfotype0185(string EmployeeID, DateTime date)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual string InsertINFOTYPE0002And0182(INFOTYPE0002 oINF2, INFOTYPE0182 oINF182)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void UpdateINFOTYPE0002And0182(INFOTYPE0002 oINF2, INFOTYPE0182 oINF182)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void InsertINFOTYPE0001(INFOTYPE0001 oINF1)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void UpdateINFOTYPE0001(INFOTYPE0001 oINF1)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void InsertINFOTYPE0105(INFOTYPE0105 oINF105)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void UpdateINFOTYPE0105(INFOTYPE0105 oINF105)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual string InsertINFOTYPE0002And0182And0001And0105(INFOTYPE0002 oINF2, INFOTYPE0182 oINF182, INFOTYPE0001 oINF1, INFOTYPE0105 oINF105)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void DeleteINFOTYPE0002And0182And0001And0105And1001(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void DeleteINFOTYPE0002And0182(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void DeleteINFOTYPE0001(string EmployeeID, DateTime BeginDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void DeleteINFOTYPE0105(string EmployeeID, DateTime BeginDate, string SubType)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void Resign(string EmployeeID, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0001> INFOTYPE0001GetAllEmployeeHistory(string Language)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual Dictionary<string, object> INFOTYPE0002And0182GetAllHistory(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0001> INFOTYPE0001GetAllHistory(string EmployeeID, string Language)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE0105> INFOTYPE0105GetAllHistory(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual Dictionary<string, object> INFOTYPE0002And0182Get(string EmployeeID, DateTime BeginDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE0001 INFOTYPE0001Get(string EmployeeID, DateTime BeginDate, string Language)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE0105 INFOTYPE0105Get(string EmployeeID, DateTime BeginDate, string CategoryCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1000> INFOTYPE1000GetAllHistory()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void DeleteINFOTYPE1000(string ObjectType, string ObjectID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void InsertINFOTYPE1000(INFOTYPE1000 oINF1000)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void UpdateINFOTYPE1000(INFOTYPE1000 oINF1000)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE1000 INFOTYPE1000Get(string ObjectType, string ObjectID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1001> INFOTYPE1001GetAllHistory()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE1000 INFOTYPE1000GetByObjectID(string ObjectType, string ObjectID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1001> GetPositionForPerson(string NextObjectID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1001> GetBelongToRelation()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE1001 INFOTYPE1001Get(string ObjectType, string ObjectID, string NextObjectType, string NextObjectID, string Relation, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void DeleteINFOTYPE1001(INFOTYPE1001 oINF1001)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void InsertINFOTYPE1001(INFOTYPE1001 oINF1001, INFOTYPE1001 oOldINF1001)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void InsertINFOTYPEAllForEmployee(List<INFOTYPE0001> INF0001Lst, List<INFOTYPE0002> INF0002Lst, List<INFOTYPE0182> INF0182Lst, List<INFOTYPE0105> INF0105Lst, List<INFOTYPE1001> INF1001Lst)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void UpdateINFOTYPE1001(INFOTYPE1001 oINF1001)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<INFOTYPE1013> INFOTYPE1013GetAllHistory()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void DeleteINFOTYPE1013(INFOTYPE1013 oINF1013)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void InsertINFOTYPE1013(INFOTYPE1013 oINF1013)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void UpdateINFOTYPE1013(INFOTYPE1013 oINF1013)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual INFOTYPE1013 INFOTYPE1013Get(string ObjectID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<EmployeeData> GetAllEmployeeInINFOTYPE0001(DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DataTable GetEmployeeByEmpID(string EmployeeID, DateTime CheckDate, string Language)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DataTable GetUserInResponseForActionOfInsteadByResponseType(string ResponseType, string ResponseCode, string ResponseCompanyCode, bool IncludeSub, DateTime CheckDate, string EmployeeID)
        {
            throw new NotImplementedException();
        }


        public virtual DataTable GetOrganizationStructure(string EmployeeID, string PositionID)
        {
            throw new NotImplementedException();
        }

        public virtual DataTable ValidateData(string EmployeeID)
        {
            throw new NotImplementedException();
        }

        public virtual bool InsertActionLog(ActionLog oActionLog)
        {
            throw new NotImplementedException();
        }

        public virtual DataTable GetMonthlyWorkSchedule(string EmployeeID)
        {
            throw new NotImplementedException();
        }

        public virtual List<INFOTYPE0001> GetAllInfotype0001ForReport(DateTime oCheckDate, string oLanguage)
        {
            throw new NotImplementedException();
        }


        public virtual DataTable GetExternalUserbyID(string UserID, string oLanguage)
        {
            throw new NotImplementedException();
        }

        public virtual DataTable GetPettyCustodianGetByCode(string PettyCode)
        {
            throw new NotImplementedException();
        }


        public virtual DataSet GetContractDetailByEmployeeID(string EmployeeID, string CostCenter, DateTime CheckDate)
        {
            throw new NotImplementedException();
        }

        public virtual bool IsContractUser(string EmployeeID)
        {
            throw new NotImplementedException();
        }

        public virtual bool IsExternalUser(string EmployeeID)
        {
            throw new NotImplementedException();
        }


        public virtual DateSpecificData GetDateSpecific(string EmployeeID)
        {
            throw new NotImplementedException();
        }

        public virtual DateSpecificData GetERPDateSpecific(string EmployeeID)
        {
            throw new NotImplementedException();
        }

        public virtual DataTable GetExternalUserSnapshotbyRequestID(int RequestID, int ApproverID)
        {
            throw new NotImplementedException();
        }

        public virtual DataSet GetEmployeeActiveGetAllSearchText(string SearhText, DateTime CheckDate, string LanguageCode)
        {
            throw new NotImplementedException();
        }

        public virtual DataSet GetEmployeeGetAllSearchText(string SearhText, DateTime CheckDate, string LanguageCode)
        {
            throw new NotImplementedException();
        }

        public virtual DateTime GetCalendarEndDate(string EmployeeID)
        {
            throw new NotImplementedException();
        }

        public virtual List<INFOTYPE0001> GetAreaWorkscheduleInfotype0001Data(string EmployeeID1, string EmployeeID2, DateTime EffectiveDate, string Profile)
        {
            throw new NotImplementedException();
        }

        public virtual List<DelegatePosition> GetAllPositionByEmployee(string EmployeeId)
        {
            throw new NotImplementedException();
        }

        public virtual DataTable GetEmployeeListByOrganizationXML(string XML, DateTime CheckDate)
        {
            throw new NotImplementedException();
        }

        public virtual EmployeeINFOTYPE0001 GetDataEmployeeINFOTYPE001(string employeeId, DateTime begindate)
        {
            throw new NotImplementedException();
        }

        public virtual EmployeeINFOTYPE0001 GetDataEmployeeINFOTYPE001(string employeeId, string year)
        {
            throw new NotImplementedException();
        }

        public virtual List<EmployeeAllActive> GetAllEmployeeActive(DateTime date_time,string language)
        {
            throw new NotImplementedException();
        }

        public virtual List<DbDelegateGroupping> GetListDelegateGroupping()
        {
            throw new NotImplementedException();
        }

        public virtual DataEmployeeInfotype0002 GetDateOfBirthByEmployee(string employeeId)
        {
            throw new NotImplementedException();
        }

        public virtual NotifyBirthDay GetNotifyBirthDayCard(string employeeId, int year)
        {
            throw new NotImplementedException();
        }
        
        public virtual void SaveAlertNotifyBirthDayCardByEmployee(string employeeId, int year, int month, int day)
        {
            throw new NotImplementedException();
        }

        public virtual DataSet GetEmployeeGetAllSearchTextByManager(string ManagerID, string SearhText, DateTime CheckDate, string LanguageCode)
        {
            throw new NotImplementedException();
        }
    }
}