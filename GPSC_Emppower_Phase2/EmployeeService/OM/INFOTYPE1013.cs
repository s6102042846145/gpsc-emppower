using System;
using System.Collections.Generic;
using System.Text;
using ESS.DATA;

namespace ESS.EMPLOYEE.OM
{
    [Serializable()]
    public class INFOTYPE1013 : AbstractObject
    {
        private string __empGroup = "";
        private string __empSubGroup = "";
        private string __objectID = "";
        private DateTime __beginDate;
        private DateTime __endDate;

        public INFOTYPE1013()
        { 
        }

        public string ObjectID
        {
            get
            {
                return __objectID;
            }
            set
            {
                __objectID = value;
            }
        }

        public DateTime BeginDate
        {
            get
            {
                return __beginDate;
            }
            set
            {
                __beginDate = value;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return __endDate;
            }
            set
            {
                __endDate = value;
            }
        }

        public string EmpGroup
        {
            get
            {
                return __empGroup;
            }
            set
            {
                __empGroup = value;
            }
        }
        public string EmpSubGroup
        {
            get
            {
                return __empSubGroup;
            }
            set
            {
                __empSubGroup = value;
            }
        }

        public static List<INFOTYPE1013> GetAll()
        {
            return ServiceManager.OMService.GetAllPositionBandLevel();
        }

        public static List<INFOTYPE1013> GetAll(string Mode, string Profile)
        {
            return ServiceManager.GetMirrorOMService(Mode).GetAllPositionBandLevel(Profile);
        }

        public static List<INFOTYPE1013> GetAll(string objID1,string objID2,string Mode, string Profile)
        {
            return ServiceManager.GetMirrorOMService(Mode).GetAllPositionBandLevel(objID1,objID2,Profile);
        }

        public static void SaveAll(List<INFOTYPE1013> data, string Mode, string Profile)
        {
            ServiceManager.GetMirrorOMService(Mode).SaveAllPositionBandLevel(data, Profile);
        }

    }
}
