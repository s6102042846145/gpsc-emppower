﻿using System;
using System.Collections.Generic;
using System.Text;
using ESS.DATA;

namespace ESS.EMPLOYEE.OM
{
    public class INFOTYPE1010 : AbstractObject
    {
        private string __objectID;
        private DateTime __beginDate;
        private DateTime __endDate;
        private string __orgLevel;

        public INFOTYPE1010()
        {
        }

        public string ObjectID
        {
            get
            {
                return __objectID;
            }
            set
            {
                __objectID = value;
            }
        }
        public DateTime BeginDate
        {
            get
            {
                return __beginDate;
            }
            set
            {
                __beginDate = value;
            }
        }
        public DateTime EndDate
        {
            get
            {
                return __endDate;
            }
            set
            {
                __endDate = value;
            }
        }
        public string OrgLevel
        {
            get
            {
                return __orgLevel;
            }
            set
            {
                __orgLevel = value;
            }
        }

        public static List<INFOTYPE1010> GetAll()
        {
            return ServiceManager.OMService.GetAllOrgUnitLevel();
        }

        public static List<INFOTYPE1010> GetAll(string Mode, string Profile)
        {
            return ServiceManager.GetMirrorOMService(Mode).GetAllOrgUnitLevel(Profile);
        }

        public static void SaveAll(List<INFOTYPE1010> data, string Mode, string Profile)
        {
            ServiceManager.GetMirrorOMService(Mode).SaveOrgUnitLevel(data, Profile);
        }
    }
}
