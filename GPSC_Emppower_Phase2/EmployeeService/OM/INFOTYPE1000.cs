using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using ESS.DATA;

namespace ESS.EMPLOYEE.OM
{
    [Serializable()]
    public class INFOTYPE1000 : AbstractObject
    {
        private ObjectType __objectType;
        private string __objectID = "";
        private DateTime __beginDate;
        private DateTime __endDate;
        private string __shortText = "";
        private string __text = "";
        private string __shortTextEn = "";
        private string __textEn = "";
        private int __level = 0;
        private bool __isHaveChild;
        public INFOTYPE1000()
        { 
        }

        public INFOTYPE1000 FindDivisionData(string LanguageCode)
        {
            if (ObjectType != ObjectType.ORGANIZE)
                return null;
            return OrgStructure.FindDivision(this, LanguageCode);
        }

        public INFOTYPE1000 FindDepartmentData(string LanguageCode)
        {
            if (ObjectType != ObjectType.ORGANIZE)
                return null;
            return OrgStructure.FindDepartment(this, LanguageCode);
        }
        public INFOTYPE1000 FindVCData(string LanguageCode)
        {
            if (ObjectType != ObjectType.ORGANIZE)
                return null;
            return OrgStructure.FindVC(this, LanguageCode);
        }
        public INFOTYPE1000 FindOrgUnit(string LanguageCode)
        {
            if (ObjectType != ObjectType.POSITION)
                return null;
            return OrgStructure.FinOrgUnit(this, LanguageCode);
        }

        protected override string GetEnumString(PropertyInfo prop)
        {
            if (prop.Name == "ObjectType")
            {
                return Convert.ToChar((int)prop.GetValue(this, null)).ToString();
            }
            else
            {
                return base.GetEnumString(prop);
            }
        }

        public ObjectType ObjectType
        {
            get
            {
                return __objectType;
            }
            set
            {
                __objectType = value;
            }
        }

        public string ObjectID
        {
            get
            {
                return __objectID;
            }
            set
            {
                __objectID = value;
            }
        }

        public DateTime BeginDate
        {
            get
            {
                return __beginDate;
            }
            set
            {
                __beginDate = value;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return __endDate;
            }
            set
            {
                __endDate = value;
            }
        }

        public string ShortText
        {
            get
            {
                return __shortText;
            }
            set
            {
                __shortText = value;
            }
        }

        public string Text
        {
            get
            {
                return __text;
            }
            set
            {
                __text = value;
            }
        }

        public string ShortTextEn
        {
            get
            {
                return __shortTextEn;
            }
            set
            {
                __shortTextEn = value;
            }
        }

        public string TextEn
        {
            get
            {
                return __textEn;
            }
            set
            {
                __textEn = value;
            }
        }

        public int Level
        {
            get
            {
                INFOTYPE1010 orgLevel = ServiceManager.GetMirrorOMService("DB").GetLevelByOrg(this.ObjectID, this.BeginDate);
               
                if (orgLevel == null)
                    __level = 0;
                else
                    __level = int.Parse(orgLevel.OrgLevel);

                return __level;
            }
            set
            {
                __level = value;
            }
        }

        public string AlternativeName(string Language)
        {
            if (Language.ToUpper() == "TH")
            {
                return this.Text;
            }
            else
            {
                return this.TextEn;
            }
        }

        private static List<ObjectType> ObjectTypes
        {
            get
            {
                List<ObjectType> types = new List<ObjectType>();
                types.Add(ObjectType.ORGANIZE);
                types.Add(ObjectType.POSITION);
                types.Add(ObjectType.JOB);
                return types;
            }
        }

        public static INFOTYPE1000 GetObjectData(ObjectType ObjectType, string ObjectID, DateTime CheckDate, string Language)
        {
            return ServiceManager.OMService.GetObjectData(ObjectType, ObjectID, CheckDate, Language);
        }

        public static INFOTYPE1000 GetJobByPositionID(string PositionID, DateTime CheckDate)
        {
            return ServiceManager.OMService.GetJobByPositionID(PositionID, CheckDate);
        }
        public static INFOTYPE1000 GetOragnization(string PositionID, string Language)
        {
            return ServiceManager.OMService.GetOraganizationByPositionID(PositionID, Language);
        }

        public static List<INFOTYPE1000> GetAll()
        {
            return ServiceManager.OMService.GetAllObject(INFOTYPE1000.ObjectTypes);
        }

        public static List<INFOTYPE1000> GetAll(string Mode, string Profile)
        {
            return ServiceManager.GetMirrorOMService(Mode).GetAllObject(INFOTYPE1000.ObjectTypes, Profile);
        }

        public static List<INFOTYPE1000> GetAll(string objID1, string objID2, string Mode, string Profile)
        {
            return ServiceManager.GetMirrorOMService(Mode).GetAllObject(objID1, objID2, INFOTYPE1000.ObjectTypes, Profile);
        }

        public static List<INFOTYPE1000> GetMEOrg()
        {
            string CompanyCode = "20";
            return ServiceManager.OMService.GetAllOrg(INFOTYPE1000.ObjectTypes, CompanyCode);
        }

        public static void SaveAll(List<INFOTYPE1000> data, string Mode, string Profile)
        {
            ServiceManager.GetMirrorOMService(Mode).SaveAllObject(INFOTYPE1000.ObjectTypes, data, Profile);
        }

        public bool HaveChild
        {
            get
            {
                return __isHaveChild;
            }
            set
            {
                __isHaveChild = value;
            }
        }
        public INFOTYPE1000 GetParent(string LangeuageCode)
        {
            if (ObjectType != ObjectType.ORGANIZE)
                return null;
            return OrgStructure.GetOrgParent(this.ObjectID, this.BeginDate);
        }
    }
}
