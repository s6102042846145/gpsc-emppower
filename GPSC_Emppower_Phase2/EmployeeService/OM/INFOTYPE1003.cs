using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using ESS.DATA;

namespace ESS.EMPLOYEE.OM
{
    [Serializable()]
    public class INFOTYPE1003 : AbstractObject
    {
        private ObjectType __objectType;
        private string __objectID;
        private DateTime __beginDate;
        private DateTime __endDate;
        private bool __isStaff = false;
        public INFOTYPE1003()
        {
 
        }
        public ObjectType ObjectType
        {
            get
            {
                return __objectType;
            }
            set
            {
                __objectType = value;
            }
        }

        public string ObjectID
        {
            get
            {
                return __objectID;
            }
            set
            {
                __objectID = value;
            }
        }

        public DateTime BeginDate
        {
            get
            {
                return __beginDate;
            }
            set
            {
                __beginDate = value;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return __endDate;
            }
            set
            {
                __endDate = value;
            }
        }

        public bool IsStaff
        {
            get
            {
                return __isStaff;
            }
            set
            {
                __isStaff = value;
            }
        }

        protected override string GetEnumString(PropertyInfo prop)
        {
            if (prop.PropertyType == typeof(ObjectType))
            {
                return Convert.ToChar(prop.GetValue(this, null)).ToString();
            }
            return base.GetEnumString(prop);
        }

        public static List<INFOTYPE1003> GetAll()
        {
            return ServiceManager.GetMirrorOMService("SAP").GetAllApprovalData();
        }

        public static List<INFOTYPE1003> GetAll(string Mode, string Profile)
        {
            return ServiceManager.GetMirrorOMService(Mode).GetAllApprovalData(Profile);
        }

        public static List<INFOTYPE1003> GetAll(string objID1, string objID2, string Mode, string Profile)
        {
            return ServiceManager.GetMirrorOMService(Mode).GetAllApprovalData(objID1, objID2, Profile);
        }

        public static void SaveAll(List<INFOTYPE1003> data, string Mode, string Profile)
        {
            ServiceManager.GetMirrorOMService(Mode).SaveAllApprovalData(data, Profile);
        }

    }
}
