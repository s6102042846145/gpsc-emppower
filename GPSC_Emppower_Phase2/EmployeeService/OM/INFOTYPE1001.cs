using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using ESS.DATA;

namespace ESS.EMPLOYEE.OM
{
    [Serializable()]
    public class INFOTYPE1001 : AbstractObject
    {
        private ObjectType __objectType;
        private string __objectID;
        private string __nextObjectType;
        private string __nextObjectID;
        private string __releation;
        private string __orderKey;
        private DateTime __beginDate;
        private DateTime __endDate;
        private decimal __percentValue;
        public INFOTYPE1001()
        { 
        }

        protected override string GetEnumString(PropertyInfo prop)
        {
            if (prop.PropertyType == typeof(ObjectType))
            {
                return Convert.ToChar((int)prop.GetValue(this, null)).ToString();
            }
            else
            {
                return base.GetEnumString(prop);
            }
        }

        public ObjectType ObjectType
        {
            get
            {
                return __objectType;
            }
            set
            {
                __objectType = value;
            }
        }
        public string ObjectID
        {
            get
            {
                return __objectID;
            }
            set
            {
                __objectID = value;
            }
        }
        public string NextObjectType
        {
            get
            {
                return __nextObjectType;
            }
            set
            {
                __nextObjectType = value;
            }
        }
        public string NextObjectID
        {
            get
            {
                return __nextObjectID;
            }
            set
            {
                __nextObjectID = value;
            }
        }
        public string Relation
        {
            get
            {
                return __releation;
            }
            set
            {
                __releation = value;
            }
        }
        public string OrderKey
        {
            get
            {
                return __orderKey;
            }
            set
            {
                __orderKey = value;
            }
        }
        public DateTime BeginDate
        {
            get
            {
                return __beginDate;
            }
            set
            {
                __beginDate = value;
            }
        }
        public DateTime EndDate
        {
            get
            {
                return __endDate;
            }
            set
            {
                __endDate = value;
            }
        }
        public decimal PercentValue
        {
            get
            {
                return __percentValue;
            }
            set
            {
                __percentValue = value;
            }
        }

        private static List<ObjectType> ObjectTypes
        {
            get
            {
                List<ObjectType> types = new List<ObjectType>();
                types.Add(ObjectType.ORGANIZE);
                types.Add(ObjectType.POSITION);
                types.Add(ObjectType.EMPLOYEE);
                types.Add(ObjectType.JOB);
                return types;
            }
        }

        private static List<string> RelationCodes
        {
            get
            {
                List<string> types = new List<string>();
                types.Add("A002");
                types.Add("A003");
                types.Add("A008");
                types.Add("A012");
                types.Add("A007");
                return types;
            }
        }

        //public static List<INFOTYPE1000> GetRelationObjects(string objectID, ObjectType type, string relation, ObjectType nextObjectType, bool isReverse, DateTime CheckDate, string LanguageCode)
        //{
        //    return ServiceManager.OMService.GetRelationObjects(objectID, type, relation, nextObjectType, isReverse, "", ObjectType.ORGANIZE, false, CheckDate, LanguageCode);
        //}

        //public static List<INFOTYPE1000> GetRelationObjects(string objectID, ObjectType type, string relation, ObjectType nextObjectType, bool isReverse, string relation1, ObjectType nextObjectType1, bool isReverse1, DateTime CheckDate, string LanguageCode)
        //{
        //    return ServiceManager.OMService.GetRelationObjects(objectID, type, relation, nextObjectType, isReverse,relation1, nextObjectType1, isReverse1, CheckDate, LanguageCode);
        //}

        public static List<INFOTYPE1001> GetAll()
        {
            return ServiceManager.OMService.GetAllRelation(INFOTYPE1001.ObjectTypes, INFOTYPE1001.RelationCodes);
        }
        public static List<INFOTYPE1001> GetAll(string Mode, string Profile)
        {
            return ServiceManager.GetMirrorOMService(Mode).GetAllRelation(INFOTYPE1001.ObjectTypes, INFOTYPE1001.RelationCodes, Profile);
        }

        public static List<INFOTYPE1001> GetAll(string objID1, string objID2, string Mode, string Profile)
        {
            return ServiceManager.GetMirrorOMService(Mode).GetAllRelation(objID1, objID2, INFOTYPE1001.ObjectTypes, INFOTYPE1001.RelationCodes, Profile);
        }
        
        public static void SaveAll(List<INFOTYPE1001> data, string Mode, string Profile)
        {
            ServiceManager.GetMirrorOMService(Mode).SaveAllRelation(INFOTYPE1001.ObjectTypes, INFOTYPE1001.RelationCodes, data, Profile);
        }

        public static List<EmployeeData> GetEmployeeUnder(string OrgID, DateTime CheckDate)
        {
            return ServiceManager.OMService.GetEmployeeUnder(OrgID, CheckDate);
        }

        public static EmployeeData GetEmployeeByPositionID(string PositionID, DateTime CheckDate)
        {
            return ServiceManager.OMService.GetEmployeeByPositionID(PositionID, CheckDate);
        }
    }
}
