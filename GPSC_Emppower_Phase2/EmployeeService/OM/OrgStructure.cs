using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.EMPLOYEE.OM
{
    public class OrgStructure
    {
        public OrgStructure()
        { 
        }
        public static List<INFOTYPE1000> GetAllPosition(string EmployeeID, DateTime CheckDate, string LanguageCode)
        {
            return ESS.EMPLOYEE.ServiceManager.OMService.GetAllPosition(EmployeeID, CheckDate, LanguageCode);
        }

        public static INFOTYPE1000 FindNextPosition(string CurrentPositionID, DateTime CheckDate, string LanguageCode)
        {
            return ESS.EMPLOYEE.ServiceManager.OMService.FindNextPosition(CurrentPositionID, CheckDate, LanguageCode);
        }

        public static List<INFOTYPE1000> GetOrgUnder(string OrgID, DateTime CheckDate, string LanguageCode)
        {
            return ESS.EMPLOYEE.ServiceManager.OMService.GetOrgUnder(OrgID, CheckDate, LanguageCode);
            //throw new Exception("function 'List<INFOTYPE1000> GetOrgUnder(string OrgID, DateTime CheckDate, string LanguageCode)' is obsolete");
            //return INFOTYPE1001.GetRelationObjects(OrgID, ObjectType.ORGANIZE, "A002", ObjectType.ORGANIZE, true, "A002", ObjectType.ORGANIZE, true, CheckDate, LanguageCode);
        }
        public static List<EmployeeData> GetEmpUnder(string OrgID, DateTime CheckDate)
        {
            return INFOTYPE1001.GetEmployeeUnder(OrgID, CheckDate);
        }
        public static INFOTYPE1000 GetOrgRoot(string OrgID, int Level,DateTime CheckDate, string LanguageCode)
        {
            return ESS.EMPLOYEE.ServiceManager.OMService.GetOrgRoot(OrgID, Level,CheckDate, LanguageCode);
        }

        public static INFOTYPE1000 GetOrgParent(string OrgID, DateTime CheckDate)
        {
            return ESS.EMPLOYEE.ServiceManager.OMService.GetOrgParent(OrgID, CheckDate);
        }

        public static int SectionLevel  // ˹���
        {
            get
            {
                return 70;
            }
        }

        public static int DivisionLevel
        {
            get
            {
                return 5;
            }
        }

        public static int DepartmentLevel
        {
            get
            {
                return 4;
            }
        }
        public static int VCLevel
        {
            get
            {
                return 2;
            }
        }
       
        public static INFOTYPE1000 FindDivision(INFOTYPE1000 orgUnit, string LanguageCode)
        {
            if (orgUnit.Level <= DivisionLevel)
            {
                return orgUnit;
            }
            return GetOrgRoot(orgUnit.ObjectID, DivisionLevel, orgUnit.EndDate, LanguageCode);
        }

        public static INFOTYPE1000 FindDepartment(INFOTYPE1000 orgUnit, string LanguageCode)
        {
            if (orgUnit.Level <= DepartmentLevel)
            {
                return orgUnit;
            }
            return GetOrgRoot(orgUnit.ObjectID, DepartmentLevel, orgUnit.EndDate, LanguageCode);
        }
        public static INFOTYPE1000 FindVC(INFOTYPE1000 orgUnit, string LanguageCode)
        {
            if (orgUnit.Level <= VCLevel)
            {
                return orgUnit;
            }
            return GetOrgRoot(orgUnit.ObjectID, VCLevel, orgUnit.EndDate, LanguageCode);
        }

        public static INFOTYPE1000 FinOrgUnit(INFOTYPE1000 item, string LanguageCode)
        {
            return ESS.EMPLOYEE.ServiceManager.OMService.GetOrgUnit(item.ObjectID, item.EndDate, LanguageCode);
        }
    }
}
