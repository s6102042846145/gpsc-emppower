﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using ESS.EMPLOYEE.CONFIG.OM;
using ESS.EMPLOYEE.CONFIG.PA;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.EMPLOYEE.DATACLASS;
using ESS.EMPLOYEE.INTERFACE;
using ESS.SHAREDATASERVICE;
using ESS.UTILITY.DATACLASS;
using ESS.UTILITY.LOG;


namespace ESS.EMPLOYEE
{
    public class EmployeeManagement
    {
        #region Constructor
        private EmployeeManagement()
        {

        }
        #endregion
        #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        private static Dictionary<string, EmployeeManagement> Cache = new Dictionary<string, EmployeeManagement>();
        public string CompanyCode { get; set; }
        public static string ModuleID
        {
            get
            {
                return "ESS.EMPLOYEE";
            }
        }

        public static EmployeeManagement CreateInstance(string oCompanyCode)
        {
            EmployeeManagement oEmployeeManagement = new EmployeeManagement()
            {
                CompanyCode = oCompanyCode
            };
            return oEmployeeManagement;
        }
        #endregion MultiCompany  Framework

        public int MAXIMUM_EMPSUBGROUP
        {
            get
            {
                int EmpSubGroup = 0;
                int.TryParse(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "MAXIMUM_EMPSUBGROUP"), out EmpSubGroup);
                return EmpSubGroup;
            }
        }

        public string ROOT_ORGANIZATION
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ROOT_ORGANIZATION");
            }
        }

        public string PHOTO_PATH
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "PHOTO_PATH");
            }
        }

        public bool VISIBLE_EMPLOYEE_LEVEL
        {
            get
            {
                return Boolean.Parse(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "VISIBLE_EMPLOYEE_LEVEL"));
            }
        }

        public int MINPINCODE
        {
            get
            {
                return int.Parse(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "MINPINCODE"));
            }
        }

        public bool BYPASSPINCODE
        {
            get
            {
                return Boolean.Parse(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BYPASSPINCODE"));
            }
        }

        public string P_EMPSUBGROUPFORWORKSCHEDULE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "P_EMPSUBGROUPFORWORKSCHEDULE");
            }
        }

        public string P_PUBLICHOLIDAYCALENDAR
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "P_PUBLICHOLIDAYCALENDAR");
            }
        }

        public string P_EMPSUBAREAFORWORKSCHEDULE
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "P_EMPSUBAREAFORWORKSCHEDULE");
            }
        }

        public string P_WORKSCHEDULERULES
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "P_WORKSCHEDULERULES");
            }
        }

        public int ActivateMultiCompany
        {
            get
            {
                return int.Parse(ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ACTIVATE_MULTICOMPANY"));
            }
        }

        private string HostEmail
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "ESS.MAIL.EXCHANGE", "EXCHANGESERVER");
            }
        }

        private string SYSTEMEMAIL
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "ESS.MAIL.EXCHANGE", "SYSTEMEMAIL");
            }
        }

        public void CopyAllWorkPlaceData()
        {
            List<WorkPlaceCommunication> workplaceList;
            workplaceList = ServiceManager.CreateInstance(CompanyCode).ERPEmployeeService.GetWorkplaceData();
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.SaveWorkPlaceData(workplaceList, "WORKFLOW");
        }


        public EmployeeData FindManager(string EmployeeID, string PositionID, string ManagerCode, DateTime CheckDate, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.FindManager(EmployeeID, PositionID, ManagerCode, CheckDate, LanguageCode);
        }


        public List<INFOTYPE0001> GetAllINFOTYPE0001(DateTime CheckDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetAllINFOTYPE0001(CheckDate);
        }

        public List<INFOTYPE0001> GetAllEmployeeName(string Language)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetAllEmployeeName(Language);
        }

        public List<INFOTYPE0001> GetAllEmployeeNameForUserRole(string Language)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetAllEmployeeNameForUserRole(Language);
        }

        public DataSet GetUserInResponseForActionOfInstead(string EmployeeID, int SubjectID, DateTime CheckDate, string ResponseCompanyCode, string SearchText)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetUserInResponseForActionOfInstead(EmployeeID, SubjectID, CheckDate, ResponseCompanyCode, SearchText);
        }

        public List<PersonalSubGroupSetting> GetPersonalSubGroupSettingList(string SourceMode, string SourceProfile)
        {
            return ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(SourceMode).GetPersonalSubGroupSettingList(SourceProfile);
        }

        public void SaveTo(List<PersonalSubGroupSetting> Data, string TargetMode, string TargetProfile)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(TargetMode).SavePersonalSubGroupSettingList(Data, TargetProfile);
        }
        public void SaveList(string EmployeeID1, string EmployeeID2, List<INFOTYPE0001> data, string profile)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.SaveInfotype0001(EmployeeID1, EmployeeID2, data, profile);
        }

        public void SavePeriod(string EmployeeID1, string EmployeeID2, List<INFOTYPE0007> data, string profile)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.SaveInfotype0007(EmployeeID1, EmployeeID2, data, profile);
        }

        public void SaveINFOTYPE0001To(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<INFOTYPE0001> Data)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorService(Mode).SaveInfotype0001(EmployeeID1, EmployeeID2, Data, Profile);
        }
        public void SaveINFOTYPE0001To(List<INFOTYPE0001> data, string RequestNo)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.SaveInfotype0001(data, RequestNo);
        }

        public void SaveINFOTYPE0002To(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<INFOTYPE0002> Data)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorService(Mode).SaveInfotype0002(EmployeeID1, EmployeeID2, Data, Profile);
        }

        public void SaveINFOTYPE0007To(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<INFOTYPE0007> Data)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorService(Mode).SaveInfotype0007(EmployeeID1, EmployeeID2, Data, Profile);
        }

        public void SaveAreaWorkscheduleINFOTYPE0001(List<INFOTYPE0001> data, string RequestNo)
        {
            ServiceManager.CreateInstance(CompanyCode).ERPEmployeeService.SaveInfotype0001(data, RequestNo);
        }

        public void SaveAreaWorkscheduleInfotype0007(List<INFOTYPE0007> data, string RequestNo)
        {
            ServiceManager.CreateInstance(CompanyCode).ERPEmployeeService.SaveInfotype0007(data, RequestNo);
        }

        public void SaveINFOTYPE0007To(List<INFOTYPE0007> data, string RequestNo)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.SaveInfotype0007(data, RequestNo);
        }

        public void SaveINFOTYPE0027To(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<INFOTYPE0027> Data)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorService(Mode).SaveInfotype0027(EmployeeID1, EmployeeID2, Data, Profile);
        }

        public void SaveINFOTYPE0030To(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<INFOTYPE0030> Data)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorService(Mode).SaveInfotype0030(EmployeeID1, EmployeeID2, Data, Profile);
        }

        public void SaveINFOTYPE0105To(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<INFOTYPE0105> Data)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorService(Mode).SaveInfotype0105(EmployeeID1, EmployeeID2, Data, Profile);
        }

        public void SaveINFOTYPE0182To(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<INFOTYPE0182> Data)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorService(Mode).SaveInfotype0182(EmployeeID1, EmployeeID2, Data, Profile);
        }

        public PersonalSubGroupSetting GetSettingFromEmpSubGroup(string EmpGroup, string EmpSubGroup)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetPersonalSubGroupSetting(EmpGroup, EmpSubGroup);
        }

        public List<EmployeeData> GetUserResponse(string Role, string AdminGroup)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetUserResponse(Role, AdminGroup);
        }

        public List<string> GetEmployeeIDByRole(string Role)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetEmployeeIDByRole(Role);
        }

        public bool IsHaveOTSummaryPermissionByEmployeeID(String EmployeeID, DateTime CheckDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.IsHaveOTSummaryPermissionByEmployeeID(EmployeeID, CheckDate);
        }

        public List<EmployeeData> GetOTSummaryPermissionByEmployeeID(String EmployeeID, DateTime CheckDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetOTSummaryPermissionByEmployeeID(EmployeeID, CheckDate);
        }

        public List<INFOTYPE0007> GetWorkSchedule(string Employee, int Year, int Month)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeINFOTYPE0007Service.GetInfotype0007(Employee, Year, Month);
        }
        public List<INFOTYPE0007> GetWorkSchedule(string EmployeeID)
        {
            //return EMPLOYEE.ServiceManager.EmployeeINFOTYPE0007Service.GetInfotype0007(EmployeeID, -1, 0);
            return ServiceManager.CreateInstance(CompanyCode).EmployeeINFOTYPE0007Service.GetInfotype0007(EmployeeID, -1, 0);
        }

        public INFOTYPE0007 GetWorkSchedule(string Employee, DateTime Date)
        {
            INFOTYPE0007 oReturn = null;
            //List<INFOTYPE0007> list = EMPLOYEE.ServiceManager.EmployeeINFOTYPE0007Service.GetInfotype0007(Employee, Date.Year, Date.Month);

            List<INFOTYPE0007> list = ServiceManager.CreateInstance(CompanyCode).EmployeeINFOTYPE0007Service.GetInfotype0007(Employee, Date.Year, Date.Month);
            foreach (INFOTYPE0007 item in list)
            {
                if (item.BeginDate <= Date.Date && item.EndDate >= Date.Date)
                {
                    oReturn = item;
                    break;
                }
            }
            return oReturn;
        }

        public BreakPattern GetBreakPattern(string DWSGroup, string BreakCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetBreakPattern(DWSGroup, BreakCode);
        }

        public INFOTYPE0001 GetInfotype0001ByEmpID(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetInfotype0001(EmployeeID);
        }

        public INFOTYPE0001 GetInfotype0001ByEmpID(string EmployeeID, DateTime CheckDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetInfotype0001(EmployeeID, CheckDate);
        }

        public List<INFOTYPE0001> GetINFOTYPE0001List(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            return ServiceManager.CreateInstance(CompanyCode).GetMirrorService(Mode).GetInfotype0001List(EmployeeID1, EmployeeID2, Profile);
        }

        public List<INFOTYPE0002> GetINFOTYPE0002List(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            return ServiceManager.CreateInstance(CompanyCode).GetMirrorService(Mode).GetInfotype0002List(EmployeeID1, EmployeeID2, Profile);
        }

        public INFOTYPE0001 GetAreaWorkscheduleInfotype0001(string EmployeeID1, DateTime EffectiveDate)
        {
            INFOTYPE0001 returnValue = null;
            List<INFOTYPE0001> list = ServiceManager.CreateInstance(CompanyCode).ERPEmployeeService.GetAreaWorkscheduleInfotype0001Data(EmployeeID1, EmployeeID1, EffectiveDate, "DEFAULT");
            if (list.Count > 0)
            {
                returnValue = list[0];
            }
            return returnValue;
        }

        public List<INFOTYPE0007> GetAreaWorkscheduleInfotype0007List(string EmployeeID1, int Year, int Month)
        {
            return ServiceManager.CreateInstance(CompanyCode).ERPEmployeeService.GetInfotype0007List(EmployeeID1, EmployeeID1, Year, Month, "DEFAULT");
        }

        public List<INFOTYPE0007> GetINFOTYPE0007List(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            return ServiceManager.CreateInstance(CompanyCode).GetMirrorService(Mode).GetInfotype0007List(EmployeeID1, EmployeeID2, Profile);
        }
        public List<INFOTYPE0027> GetINFOTYPE0027List(string EmployeeID, DateTime CheckDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetInfotype0027List(EmployeeID, CheckDate);
        }
        public List<INFOTYPE0027> GetINFOTYPE0027List(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            return ServiceManager.CreateInstance(CompanyCode).GetMirrorService(Mode).GetInfotype0027List(EmployeeID1, EmployeeID2, Profile);
        }

        public List<INFOTYPE0030> GetINFOTYPE0030List(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            return ServiceManager.CreateInstance(CompanyCode).GetMirrorService(Mode).GetInfotype0030List(EmployeeID1, EmployeeID2, Profile);
        }

        public List<INFOTYPE0105> GetINFOTYPE0105List(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            return ServiceManager.CreateInstance(CompanyCode).GetMirrorService(Mode).GetInfotype0105List(EmployeeID1, EmployeeID2, Profile);
        }

        public List<INFOTYPE0182> GetINFOTYPE0182List(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            return ServiceManager.CreateInstance(CompanyCode).GetMirrorService(Mode).GetInfotype0182List(EmployeeID1, EmployeeID2, Profile);
        }

        public List<Substitution> GetSubstitutionList(string EmployeeID, int Year, int Month, bool OnlyEffective)
        {
            DateTime date1, date2;
            date1 = new DateTime(Year, Month, 1);
            date2 = date1.AddMonths(1).AddDays(-1);
            return GetSubstitutionList(EmployeeID, date1, date2, OnlyEffective);
        }

        public List<Substitution> GetSubstitutionList(string EmployeeID, DateTime BeginDate, DateTime EndDate, bool OnlyEffective)
        {
            List<Substitution> buffer = new List<Substitution>();
            Dictionary<DateTime, Substitution> oDictSubstition = new Dictionary<DateTime, Substitution>();
            if (!OnlyEffective)
            {
                buffer.AddRange(ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetInfotype2003(EmployeeID, BeginDate, EndDate));
            }
            buffer.AddRange(ServiceManager.CreateInstance(CompanyCode).ERPEmployeeService.GetInfotype2003(EmployeeID, BeginDate, EndDate));

            foreach (Substitution item in buffer)
            {
                oDictSubstition.Add(item.BeginDate.Date, item);
            }

            List<Substitution> oSubstitutionListFromDB = ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetInfotype2003(EmployeeID, BeginDate, EndDate);
            foreach (Substitution oSubstitution in oSubstitutionListFromDB)
            {
                if (oDictSubstition.ContainsKey(oSubstitution.BeginDate.Date) && oDictSubstition[oSubstitution.BeginDate.Date].Status.Equals(oSubstitution.Status))
                {
                    buffer.Remove(oDictSubstition[oSubstitution.BeginDate.Date]);
                }
                buffer.Add(oSubstitution);
            }
            oDictSubstition.Clear();
            oDictSubstition = null;
            return buffer;
        }

        public List<INFOTYPE0185> GetInfotype0185ByEmpID(string EmployeeID)
        {
            return GetInfotype0185ByEmpID(EmployeeID, DateTime.Now);
        }

        public List<INFOTYPE0185> GetInfotype0185ByEmpID(string EmployeeID, DateTime date)
        {
            return ServiceManager.CreateInstance(CompanyCode).ERPEmployeeService.GetInfotype0185(EmployeeID, date);
        }


        public List<PersonalSubAreaSetting> GetPersonalSubAreaSettingList(string Mode, string Profile = "DEFAULT")
        {
            return ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).GetPersonalSubAreaSettingList(Profile);
        }

        public void SaveTo(List<PersonalSubAreaSetting> Data, string Mode, string Profile)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).SavePersonalSubAreaSettingList(Data, Profile);
        }

        public PersonalArea GetSettingFromEmpArea(string PersonalArea)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetPersonalAreaSetting(PersonalArea);
        }

        public PersonalSubAreaSetting GetSettingFromEmpSubArea(string PersonalArea, string PersonalSubArea)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetPersonalSubAreaSetting(PersonalArea, PersonalSubArea);
        }

        public DateSpecificData GetDateSpecificData(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).ERPEmployeeService.GetDateSpecific(EmployeeID);
            //return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetDateSpecific(EmployeeID);
        }

        public DateSpecificData GetERPDateSpecificData(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).ERPEmployeeService.GetDateSpecific(EmployeeID);
        }

        public List<MonthlyWS> GetAll(int Year, string SourceMode, string SourceProfile)
        {
            return ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(SourceMode).GetMonthlyWorkscheduleList(Year, SourceProfile);
        }

        public MonthlyWS GetCalendar(string EmployeeID, DateTime CheckDate, int Year, int Month, bool includeSubstitute)
        {
            EmployeeData oEmp = new EmployeeData(EmployeeID, CheckDate);

            string cSubGroup = "";
            string cSubArea = "";
            string cHoliday = "";

            if (oEmp.OrgAssignment != null)
            {
                cSubGroup = oEmp.OrgAssignment.SubGroupSetting.WorkScheduleGrouping;
                cSubArea = oEmp.OrgAssignment.SubAreaSetting.WorkScheduleGrouping;
                cHoliday = oEmp.OrgAssignment.SubAreaSetting.HolidayCalendar;
            }

            MonthlyWS oReturn = new MonthlyWS();
            oReturn.WS_Year = Year;
            oReturn.WS_Month = Month;
            oReturn.EmpSubGroupForWorkSchedule = cSubGroup;
            oReturn.EmpSubAreaForWorkSchedule = cSubArea;
            oReturn.PublicHolidayCalendar = cHoliday;

            //ModifiedBy: Ratchatawan W. (2013-04-05)
            List<Substitution> substitutionList = new List<Substitution>();
            if (includeSubstitute)
                substitutionList = EmployeeManagement.CreateInstance(CompanyCode).GetSubstitutionList(EmployeeID, Year, Month, true);
            //Dictionary<int, Substitution> substitutionDict = new Dictionary<int, Substitution>();
            //if (includeSubstitute)
            //{
            //    List<Substitution> substitutionList = Substitution.GetSubstitutionList(EmployeeID, Year, Month, true);

            //    foreach (Substitution item in substitutionList.FindAll(delegate(Substitution oSub) { return oSub.Status == "COMPLETED"; }))
            //    {
            //        for (DateTime rundate = item.BeginDate; rundate <= item.EndDate; rundate = rundate.AddDays(1))
            //        {
            //            if (rundate.Year == Year && rundate.Month == Month)
            //            {
            //                substitutionDict.Add(rundate.Day, item);
            //            }
            //        }
            //    }
            //}
            bool lFirst = true;
            foreach (INFOTYPE0007 item in oEmp.GetWorkSchedule(Year, Month))
            {
                DateTime dTemp = new DateTime(Year, Month, 1);
                dTemp = dTemp.AddMonths(1).AddDays(-1);
                if (dTemp < item.BeginDate)
                    break;
                EmployeeData oEmp1 = new EmployeeData(EmployeeID, item.BeginDate);
                MonthlyWS temp = ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetMonthlyWorkschedule(oEmp1.OrgAssignment.SubGroupSetting.WorkScheduleGrouping, oEmp1.OrgAssignment.SubAreaSetting.WorkScheduleGrouping, oEmp1.OrgAssignment.SubAreaSetting.HolidayCalendar, item.WFRule, Year, Month);
                oReturn.WorkScheduleRule = temp.WorkScheduleRule;
                if (lFirst)
                {
                    oReturn.ValuationClass = temp.ValuationClass;
                }
                lFirst = false;
                int nStart = 1;
                if (item.BeginDate.Year == Year && item.BeginDate.Month == Month)
                {
                    nStart = item.BeginDate.Day;
                }
                int nEnd = dTemp.Day;
                if (item.EndDate.Year == Year && item.EndDate.Month == Month)
                {
                    nEnd = item.EndDate.Day;
                }
                oReturn.SetValuationClassExactly(nEnd, temp.ValuationClass);
                PropertyInfo oProp;
                Type oType = oReturn.GetType();
                for (int index = nStart; index <= nEnd; index++)
                {
                    string cDayNo = index.ToString("00");
                    string cDWSCode;
                    oProp = oType.GetProperty(string.Format("Day{0}", cDayNo));

                    //Modified By: Ratchatawan W. (2013-4-5)
                    DateTime oCheckDate = new DateTime(Year, Month, index);
                    Substitution oSubstitution = substitutionList.Find(delegate (Substitution oSub) { return oSub.Status == "COMPLETED" && oSub.BeginDate <= oCheckDate && oCheckDate <= oSub.EndDate; });
                    if (oSubstitution != null)
                    {
                        //                    if (substitutionList.Exists(delegate(Substitution oSub) { return oSub.Status == "COMPLETED" && oSub.BeginDate.Day == index && oSub.BeginDate.Month == Month && oSub.BeginDate.Year == Year; }))
                        //                    {
                        //                        Substitution oSubstitution = substitutionList.Find(delegate(Substitution oSub) { return oSub.BeginDate.Day == index; });
                        if (String.IsNullOrEmpty(oSubstitution.EmpDWSCodeNew))
                        {
                            cDWSCode = oSubstitution.SubstituteBeginTime.ToString() + "-" + oSubstitution.SubstituteEndTime.ToString();
                        }
                        else
                        {
                            if (oEmp.EmployeeID.Trim() == oSubstitution.EmployeeID.Trim())
                                cDWSCode = oSubstitution.EmpDWSCodeNew;
                            else
                                cDWSCode = oSubstitution.SubDWSCodeNew;
                        }
                    }
                    else
                    {
                        cDWSCode = (string)oProp.GetValue(temp, null);
                    }

                    //if (substitutionDict.ContainsKey(index))
                    //{
                    //    //CommentBy: Ratchatawan W. (2012-11-16)
                    //    //cDWSCode = substitutionDict[index].DWSCodeView;
                    //    if(oEmp.EmployeeID == substitutionDict[index].EmployeeID)
                    //        cDWSCode = substitutionDict[index].EmpDWSCodeNew;
                    //    else
                    //        cDWSCode = substitutionDict[index].SubDWSCodeNew;
                    //}
                    //else
                    //{
                    //    cDWSCode = (string)oProp.GetValue(temp, null);
                    //}

                    oProp.SetValue(oReturn, cDWSCode, null);

                    oProp = oType.GetProperty(string.Format("Day{0}_H", cDayNo));
                    oProp.SetValue(oReturn, oProp.GetValue(temp, null), null);
                }
            }
            return oReturn;
        }

        public void SaveTo(int Year, List<MonthlyWS> list, string Mode, string Profile)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).SaveMonthlyWorkscheduleList(Year, list, Profile);
        }

        public DailyWS GetDailyWorkschedule(string DailyGroup, string WSCode, DateTime CheckDate)
        {
            if (WSCode == null)
            {
                throw new Exception("WSCode can't be null.May be check workschedule for this employee.");
            }
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetDailyWorkschedule(DailyGroup, WSCode, CheckDate);
        }

        public List<DailyWS> GetAll(string Mode, string Profile)
        {
            return ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).GetDailyWorkscheduleList(Profile);
        }

        public void SaveTo(List<DailyWS> list, string Mode, string Profile)
        {
            ServiceManager.CreateInstance(CompanyCode).GetMirrorConfig(Mode).SaveDailyWorkscheduleList(list, Profile);
        }
        public void SaveUserSetting(UserSetting oUserSetting)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.SaveUserSetting(oUserSetting);
        }

        #region " PA "

        #region " Insert And Update "
        public string InsertINFOTYPE0002And0182(INFOTYPE0002 oINF2, INFOTYPE0182 oINF182)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.InsertINFOTYPE0002And0182(oINF2, oINF182);
        }

        public void UpdateINFOTYPE0002And0182(INFOTYPE0002 oINF2, INFOTYPE0182 oINF182)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.UpdateINFOTYPE0002And0182(oINF2, oINF182);
        }

        public void InsertINFOTYPE0001(INFOTYPE0001 oINF1)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.InsertINFOTYPE0001(oINF1);
        }

        public void UpdateINFOTYPE0001(INFOTYPE0001 oINF1)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.UpdateINFOTYPE0001(oINF1);
        }

        public void InsertINFOTYPE0105(INFOTYPE0105 oINF105)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.InsertINFOTYPE0105(oINF105);
        }

        public void UpdateINFOTYPE0105(INFOTYPE0105 oINF105)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.UpdateINFOTYPE0105(oINF105);
        }

        public string InsertINFOTYPE0002And0182And0001And0105(INFOTYPE0002 oINF2, INFOTYPE0182 oINF182, INFOTYPE0001 oINF1, INFOTYPE0105 oINF105)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.InsertINFOTYPE0002And0182And0001And0105(oINF2, oINF182, oINF1, oINF105);
        }
        #endregion

        #region " Delete Employee Data "
        public void DeleteINFOTYPE0002And0182And0001And0105And1001(string EmployeeID)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.DeleteINFOTYPE0002And0182And0001And0105And1001(EmployeeID);
        }

        public void DeleteINFOTYPE0002And0182(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.DeleteINFOTYPE0002And0182(EmployeeID, BeginDate, EndDate);
        }

        public void DeleteINFOTYPE0001(string EmployeeID, DateTime BeginDate)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.DeleteINFOTYPE0001(EmployeeID, BeginDate);
        }

        public void DeleteINFOTYPE0105(string EmployeeID, DateTime BeginDate, string SubType)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.DeleteINFOTYPE0105(EmployeeID, BeginDate, SubType);
        }

        public void Resign(string EmployeeID, DateTime EndDate)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.Resign(EmployeeID, EndDate);
        }
        #endregion

        #region " Get Employee Data "
        public List<INFOTYPE0001> INFOTYPE0001GetAllEmployeeHistory(string Language)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.INFOTYPE0001GetAllEmployeeHistory(Language);
        }

        public DataTable GetOrganizationStructure(string EmployeeID, string PositionID)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetOrganizationStructure(EmployeeID, PositionID);
        }

        public Dictionary<string, object> INFOTYPE0002And0182GetAllHistory(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.INFOTYPE0002And0182GetAllHistory(EmployeeID);
        }

        public List<INFOTYPE0001> INFOTYPE0001GetAllHistory(string EmployeeID, string Language)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.INFOTYPE0001GetAllHistory(EmployeeID, Language);
        }

        public List<INFOTYPE0105> INFOTYPE0105GetAllHistory(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.INFOTYPE0105GetAllHistory(EmployeeID);
        }

        public Dictionary<string, object> INFOTYPE0002And0182Get(string EmployeeID, DateTime BeginDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.INFOTYPE0002And0182Get(EmployeeID, BeginDate);
        }

        public INFOTYPE0001 INFOTYPE0001Get(string EmployeeID, DateTime BeginDate, string Language)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.INFOTYPE0001Get(EmployeeID, BeginDate, Language);
        }

        public INFOTYPE0105 INFOTYPE0105Get(string EmployeeID, DateTime BeginDate, string CategoryCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.INFOTYPE0105Get(EmployeeID, BeginDate, CategoryCode);
        }
        #endregion  

        #region " Get Employee Data Selector "
        public List<INFOTYPE1000> GetGenderCheckBoxData()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetGenderCheckBoxData();
        }

        public List<INFOTYPE1000> GetPrefixDropdownData()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetPrefixDropdownData();
        }

        public List<INFOTYPE1000> GetMaritalStatusDropdownData()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetMaritalStatusDropdownData();
        }

        public List<INFOTYPE1000> GetNationalityDropdownData()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetNationalityDropdownData();
        }

        public List<INFOTYPE1000> GetLanguageDropdownData()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetLanguageDropdownData();
        }

        public List<INFOTYPE1000> GetReligionDropdownData()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetReligionDropdownData();
        }

        public List<INFOTYPE1000> GetEmpGroupDropdownData()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetEmpGroupDropdownData();
        }

        public List<INFOTYPE1000> GetEmpSubGroupDropdownData()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetEmpSubGroupDropdownData();
        }

        public List<INFOTYPE1000> GetSubTypeDropdownData()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetSubTypeDropdownData();
        }

        public List<INFOTYPE1000> GetCostCenterDropdownData(DateTime CheckDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetCostCenterDropdownData(CheckDate);
        }

        public List<INFOTYPE1000> GetCostCenterByOrganize(string Organize)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetCostCenterByOrganize(Organize);
        }

        public List<INFOTYPE1000> GetAreaDropdownData()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetAreaDropdownData();
        }

        public List<INFOTYPE1000> GetSubAreaDropdownData()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetSubAreaDropdownData();
        }
        #endregion

        #endregion

        #region " OM "

        #region " Get Data "
        public List<INFOTYPE1000> INFOTYPE1000GetAllHistory()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.INFOTYPE1000GetAllHistory();
        }

        public INFOTYPE1000 INFOTYPE1000Get(string ObjectType, string ObjectID, DateTime BeginDate, DateTime EndDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.INFOTYPE1000Get(ObjectType, ObjectID, BeginDate, EndDate);
        }

        public List<INFOTYPE1001> INFOTYPE1001GetAllHistory()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.INFOTYPE1001GetAllHistory();
        }

        public INFOTYPE1000 INFOTYPE1000GetByObjectID(string ObjectType, string ObjectID)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.INFOTYPE1000GetByObjectID(ObjectType, ObjectID);
        }

        public List<INFOTYPE1001> GetPositionForPerson(string NextObjectID)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetPositionForPerson(NextObjectID);
        }

        public List<INFOTYPE1001> GetBelongToRelation()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetBelongToRelation();
        }

        public INFOTYPE1001 INFOTYPE1001Get(string ObjectType, string ObjectID, string NextObjectType, string NextObjectID, string Relation, DateTime BeginDate, DateTime EndDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.INFOTYPE1001Get(ObjectType, ObjectID, NextObjectType, NextObjectID, Relation, BeginDate, EndDate);
        }

        public List<INFOTYPE1013> INFOTYPE1013GetAllHistory()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.INFOTYPE1013GetAllHistory();
        }

        public INFOTYPE1013 INFOTYPE1013Get(string ObjectID, DateTime BeginDate, DateTime EndDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.INFOTYPE1013Get(ObjectID, BeginDate, EndDate);
        }
        #endregion

        #region " Delete "
        public void DeleteINFOTYPE1000(string ObjectType, string ObjectID, DateTime BeginDate, DateTime EndDate)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.DeleteINFOTYPE1000(ObjectType, ObjectID, BeginDate, EndDate);
        }

        public void DeleteINFOTYPE1001(INFOTYPE1001 oINF1001)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.DeleteINFOTYPE1001(oINF1001);
        }

        public void DeleteINFOTYPE1013(INFOTYPE1013 oINF1013)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.DeleteINFOTYPE1013(oINF1013);
        }
        #endregion

        #region " Insert and Update "
        public void InsertINFOTYPE1000(INFOTYPE1000 oINF1000)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.InsertINFOTYPE1000(oINF1000);
        }

        public void UpdateINFOTYPE1000(INFOTYPE1000 oINF1000)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.UpdateINFOTYPE1000(oINF1000);
        }

        public void InsertINFOTYPE1001(INFOTYPE1001 oINF1001, INFOTYPE1001 oOldINF1001)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.InsertINFOTYPE1001(oINF1001, oOldINF1001);
        }

        public void InsertINFOTYPEAllForEmployee(List<INFOTYPE0001> INF0001Lst, List<INFOTYPE0002> INF0002Lst, List<INFOTYPE0182> INF0182Lst, List<INFOTYPE0105> INF0105Lst, List<INFOTYPE1001> INF1001Lst)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.InsertINFOTYPEAllForEmployee(INF0001Lst, INF0002Lst, INF0182Lst, INF0105Lst, INF1001Lst);
        }

        public void UpdateINFOTYPE1001(INFOTYPE1001 oINF1001)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.UpdateINFOTYPE1001(oINF1001);
        }

        public void InsertINFOTYPE1013(INFOTYPE1013 oINF1013)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.InsertINFOTYPE1013(oINF1013);
        }

        public void UpdateINFOTYPE1013(INFOTYPE1013 oINF1013)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.UpdateINFOTYPE1013(oINF1013);
        }
        #endregion

        public List<INFOTYPE1000> GetRelationSelectData()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetRelationSelectData();
        }

        public List<INFOTYPE1000> GetRelationValidationSelectData()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetRelationValidationSelectData();
        }

        public List<string> GetRelationValidationByRelation(string relation)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetRelationValidationByRelation(relation);
        }
        #endregion

        public INFOTYPE0105 GetInfotype0105(string oEmployeeID, string oCategoryCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetInfotype0105(oEmployeeID, oCategoryCode);
        }

        public List<INFOTYPE0105> GetInfotype0105ByCategoryCode(string EmployeeID, string CategoryCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetInfotype0105ByCategoryCode(EmployeeID, CategoryCode);
        }

        public DataTable GetUserInResponseForActionOfInsteadByResponseType(string ResponseType, string ResponseCode, string ResponseCompanyCode, bool IncludeSub, DateTime CheckDate, string SearchText)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetUserInResponseForActionOfInsteadByResponseType(ResponseType, ResponseCode, ResponseCompanyCode, IncludeSub, CheckDate, SearchText);
        }

        public EmployeeData GetEmployeeDataByPosition(string PositionID, DateTime CheckDate, string LanguageCode)
        {
            EmployeeData oReturn;
            INFOTYPE1000 oPosition = ServiceManager.CreateInstance(CompanyCode).OMService.GetObjectData(ObjectType.S, PositionID, CheckDate, LanguageCode);
            oReturn = OMManagement.CreateInstance(CompanyCode).GetEmployeeByPositionID(PositionID, CheckDate);
            if (oReturn != null)
                oReturn.ActionOfPosition = oPosition;
            return oReturn;
        }
        public bool InsertActionLog(object objOld, object objNew, string oAction, bool oStatus)
        {
            bool flg = false;
            ActionLog oActionLog = new ActionLog();
            oActionLog.LogAction = oAction;
            oActionLog.LogActionBy = string.Format("{0}", "SYSTEM");
            oActionLog.LogData = string.Format("OLD : {0}\r\n,NEW : {1}", (objOld != null) ? LogMgr.GetSerialize(objOld) : "", (objNew != null) ? LogMgr.GetSerialize(objNew) : "");
            oActionLog.LogActionDate = DateTime.Now; //  generate from db
            oActionLog.LogID = Guid.NewGuid().ToString();//  generate from db
            oActionLog.LogStatus = oStatus;

            flg = ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.InsertActionLog(oActionLog);
            return flg;
        }

        public DataTable GetMonthlyWorkSchedule(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetMonthlyWorkSchedule(EmployeeID);
        }

        public MonthlyWS GetMonthlyWorkschedule(string EmpSubGroupForWorkSchedule, string EmpSubAreaForWorkSchedule, string PublicHolidayCalendar, string WorkScheduleRule, int Year, int Month)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetMonthlyWorkschedule(EmpSubGroupForWorkSchedule, EmpSubAreaForWorkSchedule, PublicHolidayCalendar, WorkScheduleRule, Year, Month);
        }

        public MonthlyWS GetPublicCalendar(int Year, int Month)
        {
            string EmpSubGroupForWorkSchedule = P_EMPSUBGROUPFORWORKSCHEDULE;
            string EmpSubAreaForWorkSchedule = P_EMPSUBAREAFORWORKSCHEDULE;
            string PublicHolidayCalendar = P_PUBLICHOLIDAYCALENDAR;
            string WorkScheduleRule = P_WORKSCHEDULERULES;

            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetMonthlyWorkschedule(EmpSubGroupForWorkSchedule, EmpSubAreaForWorkSchedule, PublicHolidayCalendar, WorkScheduleRule, Year, Month);
        }

        public List<INFOTYPE0182> GetINFOTYPE0182List(string oEmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetInfotype0182List(oEmployeeID);
        }

        public List<INFOTYPE0001> GetAllInfotype0001ForReport(DateTime oCheckDate, string oLanguage)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetAllInfotype0001ForReport(oCheckDate, oLanguage);
        }

        public DataTable GetExternalUserbyID(string UserID, string oLanguage)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetExternalUserbyID(UserID, oLanguage);
        }

        public DataTable GetExternalUserSnapshotbyRequestID(int RequestID, int ApproverID)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetExternalUserSnapshotbyRequestID(RequestID, ApproverID);
        }

        public EmployeeData GetPettyCustodianGetByCode(string PettyCode)
        {
            EmployeeData oEmp = new EmployeeData();
            DataTable dt = ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetPettyCustodianGetByCode(PettyCode);
            if (dt.Rows.Count > 0)
            {
                oEmp = new EmployeeData(dt.Rows[0]["EmployeeID"].ToString(), DateTime.Now.Date);
            }
            return oEmp;
        }

        public DataSet GetContractDetailByEmployeeID(string EmployeeID, string CostCenter, DateTime CheckDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetContractDetailByEmployeeID(EmployeeID, CostCenter, CheckDate);
        }

        public bool IsContractUser(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.IsContractUser(EmployeeID);
        }

        public bool ValidateActiveEmployeeForTravel(string EmployeeID, DateTime BeginTravelDate)
        {
            return ServiceManager.CreateInstance(this.CompanyCode).EmployeeService.ValidateActiveEmployeeForTravel(EmployeeID, BeginTravelDate);
        }

        public bool HavePinCode(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.HavePinCode(EmployeeID);
        }
        public bool VerifyPinCode(string Employeeid, string pincode)
        {
            bool oReturn = false;
            bool verify = ServiceManager.CreateInstance(CompanyCode).EmployeeService.VerifyPinCode(Employeeid, pincode);
            if (verify || BYPASSPINCODE)
            {
                oReturn = true;
            }
            return oReturn;
        }
        public void RequestNEWPin(EmployeeData oEmp)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.RequestNewPIN(oEmp);
        }
        public void AlertIncorrectPIN(EmployeeData oEmp, string subjectAlert, string bodyAlert)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.AlertIncorrectPIN(oEmp, subjectAlert, bodyAlert);
        }
        public void CreateNEWPin(string EmployeeID, string TicketID, string NewPINcode)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.CreateNewPIN(EmployeeID, TicketID, NewPINcode);
        }
        public bool ValidateTicket(string EmployeeID, string TicketClass, string TicketID)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.ValidateTicket(EmployeeID, TicketClass, TicketID);
        }
        public void ChangePinCode(string EmployeeID, string pincode, string NewPINcode)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.ChangePINCode(EmployeeID, pincode, NewPINcode);
        }

        public List<EmployeeData> GetAllEmployeeInINFOTYPE0001(DateTime CheckDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetAllEmployeeInINFOTYPE0001(CheckDate);
        }

        public DataTable GetEmployeeByEmpID(string EmployeeID, DateTime CheckDate, string Language)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetEmployeeByEmpID(EmployeeID, CheckDate, Language);
        }

        public List<EmployeeData> GetAllActiveEmployeeInINFOTYPE0001(DateTime Checkdate)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetAllActiveEmployeeInINFOTYPE0001(Checkdate);
        }

        public List<EmployeeData> GetManagerInSameOraganizationAndSameEmpSubGroup(string EmployeeID, string PositionID, DateTime CheckDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetManagerInSameOraganizationAndSameEmpSubGroup(EmployeeID, PositionID, CheckDate);
        }

        public List<Substitution> GetInfotype2003(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetInfotype2003(EmployeeID, BeginDate, EndDate);
        }

        public List<Substitution> GetInfotype2003(string EmployeeID, DateTime BeginDate, DateTime EndDate, string ExcludeReqNo)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetInfotype2003(EmployeeID, BeginDate, EndDate, ExcludeReqNo);
        }

        public List<Substitution> GetInfotype2003_ERP(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ERPEmployeeService.GetInfotype2003(EmployeeID, BeginDate, EndDate);
        }

        public List<Substitution> GetInfotype2003_Log(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetInfotype2003_Log(EmployeeID, BeginDate, EndDate);
        }

        public bool ValidateNewPasswordPolicy(string NewPassword, out string ErrorMessage)
        {
            //bool inputTest1 = EmployeeManagement.CreateInstance(CompanyCode).ValidatePinCode("HELLO%12247", out ErrorMessage);
            //bool inputTest2 = EmployeeManagement.CreateInstance(CompanyCode).ValidatePinCode("HEL556677", out ErrorMessage);
            //bool inputTest3 = EmployeeManagement.CreateInstance(CompanyCode).ValidatePinCode("hel..low", out ErrorMessage);
            //bool inputTest4 = EmployeeManagement.CreateInstance(CompanyCode).ValidatePinCode("h#l-2Wk", out ErrorMessage);

            bool isPass = false;
            ErrorMessage = string.Empty;
            int iCondition = 0;
            if (NewPassword.Length < MINPINCODE)
            {
                ErrorMessage = "PINCODE must be at least " + MINPINCODE.ToString() + " characters in length";
            }
            else
            {
                if (Regex.IsMatch(NewPassword, "[A-Z]")) { iCondition += 1; }
                if (Regex.IsMatch(NewPassword, "[a-z]")) { iCondition += 1; }
                if (Regex.IsMatch(NewPassword, "[0-9]")) { iCondition += 1; }
                if (Regex.IsMatch(NewPassword, @"[!@#$%^&*()_+=\[{\]};:<>|./?,-]")) { iCondition += 1; }
                if (iCondition >= 3)
                {
                    isPass = true;
                }
                else
                {
                    ErrorMessage = "PINCODE must contain at least 3 of the following 4 types of characters: " + "\n" +
                        "-UPPER case letters (i.e. A-Z) " + "\n" +
                        "-lower case letters (i.e. a-z) " + "\n" +
                        "-numbers (i.e. 0-9) " + "\n" +
                        "-special characters (e.g. !@#$%^&*()_+=\\[{\\]};:<>|./?,-)";
                }
            }
            return isPass;
        }
        public List<DailyWS> GetDailyWSByWorkscheduleGrouping(string WorkScheduleGrouping, DateTime CheckDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetDailyWSByWorkscheduleGrouping(WorkScheduleGrouping, CheckDate);
            //EMPLOYEE.ServiceManager.EmployeeConfig.GetDailyWSByWorkscheduleGrouping(WorkScheduleGrouping, CheckDate);
        }


        public List<MonthlyWS> GetMonthlyWS(EmployeeData oEmp, int CheckYear)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetMonthlyWS(oEmp.EmployeeID, CheckYear);
        }

        public string GetSupervisorPositionByEmpPosition(string EmpPosition)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetSupervisorPositionByEmpPosition(EmpPosition);
        }

        public DataSet GetEmployeeActiveGetAllSearchText(string SearchText, DateTime CheckDate, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetEmployeeActiveGetAllSearchText(SearchText, CheckDate, LanguageCode);
        }

        // ใช้เฉพาะ Admin ออกรายงานแทนเท่านั้นเพราะต้อง ออกรายงานสำหรับคนที่ลาออกแล้วด้วย
        public DataSet GetEmployeeGetAllSearchText(string SearchText, DateTime CheckDate, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetEmployeeGetAllSearchText(SearchText, CheckDate, LanguageCode);
        }

        public DataSet GetEmployeeGetAllSearchTextByManager(string ManagerID, string SearhText, DateTime CheckDate, string LanguageCode)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetEmployeeGetAllSearchTextByManager(ManagerID, SearhText, CheckDate, LanguageCode);
        }

        public DateTime GetCalendarEndDate(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetCalendarEndDate(EmployeeID);
        }

        public List<DelegatePosition> GetAllDelegatePosition(string EmployeeId)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetAllPositionByEmployee(EmployeeId);
        }

        public DataTable GetEmployeeListByOrganizationXML(string XML, DateTime CheckDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetEmployeeListByOrganizationXML(XML, CheckDate);
        }

        public EmployeeINFOTYPE0001 GetDataEmployeeInfoType0001ByYear(string employeeId,string year)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetDataEmployeeINFOTYPE001(employeeId, year);
        }

        public List<EmployeeAllActive> GetAllEmployeeActive(DateTime date_time, string language)
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetAllEmployeeActive(date_time, language);
        }

        public string GetPathPictureUrl()
        {
            return PHOTO_PATH;
        }
        
        public int GetConfingIsUseMultiCompany()
        {
            // 0 = ไม่ใช้งาน , 1 = ใช้งาน
            return ActivateMultiCompany;
        }

        public List<DbDelegateGroupping> GetListDelegateGroupping()
        {
            return ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetListDelegateGroupping();
        }

        public ResponeBirthDayCard BirthdayCardByEmployee(string employeeId)
        {
            var data = new ResponeBirthDayCard();

            DateTime date_now = DateTime.Now;
            // 1.หาวันเกิดพนักงาน
            DataEmployeeInfotype0002 employee = ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetDateOfBirthByEmployee(employeeId);

            // 2.แจ้งวันเกิดในปีนั้น
            if (!string.IsNullOrEmpty(employee.EmployeeID))
            {
                if(date_now.Month >= employee.DOB.Month)
                {
                    NotifyBirthDay notify_birthday = ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetNotifyBirthDayCard(employeeId, date_now.Year);
                    if (!string.IsNullOrEmpty(notify_birthday.EmployeeID))
                    {
                        // 3. มีการ Notify แจ้งเตือนแล้ว
                        data.FlagBirthDay = false;
                    }
                    else
                    {
                        // 4. ยังไม่มีการแจ้งเตือน
                        if(date_now.Month > employee.DOB.Month)
                        {
                            data.FlagBirthDay = true;
                            data.Year = date_now.Year;
                            data.Month = employee.DOB.Month;
                            data.Day = employee.DOB.Day;
                            data.FullName = employee.Fullname;
                            data.EmployeeId = employee.EmployeeID;
                        }
                        else
                        {
                            if(date_now.Day >= employee.DOB.Day)
                            {
                                data.FlagBirthDay = true;
                                data.Year = date_now.Year;
                                data.Month = employee.DOB.Month;
                                data.Day = employee.DOB.Day;
                                data.FullName = employee.Fullname;
                                data.EmployeeId = employee.EmployeeID;
                            }
                                
                        }
                    }
                }
            }

            return data;
        }

        public void SaveAlertNotifyBirthDayCardByEmployee(string employeeId,int year,int month,int day)
        {
            ServiceManager.CreateInstance(CompanyCode).EmployeeService.SaveAlertNotifyBirthDayCardByEmployee(employeeId, year, month, day);
        }

        //  Birth Day Card Sent Email
        public void SentEmailBirthDayCard()
        {
            string sMailBody = string.Empty;

            #region Render Table html


            sMailBody = "<table style='{font - family: Arial, Helvetica, sans - serif; border - collapse: collapse; width: 100%; " +
                "td, th { border: 1px solid #ddd; padding: 8px;}" +
                "tr:nth-child(even){background-color: #f2f2f2;}" +
                "tr:hover {background-color: #ddd;}" +
                "th { padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #4CAF50;color: white;}'>";
            sMailBody += "<tr><th>Company</th><th>Contact</th><th>Country</th></tr>";
            sMailBody += "<tr><td>Alfreds Futterkiste</td><td>Maria Anders</td><td>Germany</td></tr>";
            sMailBody += "<tr><td>Berglunds snabbköp</td><td>Christina Berglund</td><td>Sweden</td></tr>";
            sMailBody += "<tr><td>Centro comercial Moctezuma</td> <td>Francisco Chang</td><td>Mexico</td></tr>";
            sMailBody += "</table>";

            #endregion

            #region Send Email

            System.Net.Mail.SmtpClient oClient = new System.Net.Mail.SmtpClient("app2smtp.pttgrp.corp"); // EXCHANGESERVER
            System.Net.Mail.MailAddress oSender = new System.Net.Mail.MailAddress(SYSTEMEMAIL, "WORKFLOW SYSTEM");
            System.Net.Mail.MailMessage oMessage = new System.Net.Mail.MailMessage();
            System.Net.Mail.MailAddress oTo = new System.Net.Mail.MailAddress("tara.l@ayodiacompany.com");

            try
            {
                oMessage.From = oSender;
                oMessage.Sender = oSender;
                oMessage.To.Add(oTo);
                oMessage.Subject = "ทดสอบส่ง Email";
                oMessage.Body = sMailBody;
                oMessage.BodyEncoding = Encoding.UTF8;
                oMessage.IsBodyHtml = true;
                oClient.Send(oMessage);
                oMessage.Dispose();
            }
            catch
            {
                throw;
            }
            

            #endregion
        }
    }
}