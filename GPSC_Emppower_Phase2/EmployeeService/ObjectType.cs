using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.EMPLOYEE
{
    public enum ObjectType
    {
        EMPLOYEE = 'P',
        POSITION = 'S',
        ORGANIZE = 'O',
        JOB = 'C'
    }
}
