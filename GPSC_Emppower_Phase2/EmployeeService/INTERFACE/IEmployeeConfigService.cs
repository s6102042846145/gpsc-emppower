using System;
using System.Collections.Generic;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.EMPLOYEE.CONFIG.PA;
using ESS.EMPLOYEE.CONFIG.OM;
using System.Data;

namespace ESS.EMPLOYEE.INTERFACE
{
    public interface IEmployeeConfigService
    {
        List<PersonalSubAreaSetting> GetPersonalSubAreaSettingList(string Profile);

        PersonalArea GetPersonalAreaSetting(string PersonalArea);

        PersonalSubAreaSetting GetPersonalSubAreaSetting(string PersonalArea, string PersonalSubArea);

        void SavePersonalSubAreaSettingList(List<PersonalSubAreaSetting> Data, string Profile);

        List<PersonalSubGroupSetting> GetPersonalSubGroupSettingList(string SourceProfile);

        void SavePersonalSubGroupSettingList(List<PersonalSubGroupSetting> Data, string TargetProfile);

        PersonalSubGroupSetting GetPersonalSubGroupSetting(string EmpGroup, string EmpSubGroup);

        List<MonthlyWS> GetMonthlyWorkscheduleList(int Year, string SourceProfile);

        MonthlyWS GetMonthlyWorkschedule(string EmpSubGroupForWorkSchedule, string EmpSubAreaForWorkSchedule, string PublicHolidayCalendar, string WorkScheduleRule, int Year, int Month);

        void SaveMonthlyWorkscheduleList(int Year, List<MonthlyWS> Data, string Profile);

        List<DailyWS> GetDailyWorkscheduleList(string Profile);

        void SaveDailyWorkscheduleList(List<DailyWS> Data, string Profile);

        DailyWS GetDailyWorkschedule(string DailyGroup, string DailyCode, DateTime CheckDate);

        BreakPattern GetBreakPattern(string DWSGroup, string BreakCode);

        WFRuleSetting GetWFRuleSetting(string WFRule);

        List<MonthlyWS> GetMonthlyWorkscheduleGroup(string EmpSubGroupForWorkSchedule, string EmpSubAreaForWorkSchedule, string PublicHolidayCalendar);

        List<MonthlyWS> GetMonthlyWorkscheduleGroup(string EmpSubGroupForWorkSchedule, string EmpSubAreaForWorkSchedule, string PublicHolidayCalendar, bool IncludeMonth, bool IncludeYear);

        List<PersonalSubAreaSetting> GetPersonalSubAreaByArea(string strArea);

        List<DailyWS> GetDailyWSByWorkscheduleGrouping(string WorkScheduleGrouping, DateTime CheckDate);

        List<string> GetPersonalSubAreaWSRMapping(string strSubArea);

        List<INFOTYPE1000> GetGenderCheckBoxData();

        List<INFOTYPE1000> GetPrefixDropdownData();

        List<INFOTYPE1000> GetMaritalStatusDropdownData();

        List<INFOTYPE1000> GetNationalityDropdownData();

        List<INFOTYPE1000> GetLanguageDropdownData();

        List<INFOTYPE1000> GetReligionDropdownData();

        List<INFOTYPE1000> GetEmpGroupDropdownData();

        List<INFOTYPE1000> GetEmpSubGroupDropdownData();

        List<INFOTYPE1000> GetSubTypeDropdownData();

        List<INFOTYPE1000> GetCostCenterDropdownData(DateTime CheckDate);

        List<INFOTYPE1000> GetCostCenterByOrganize(string Organize);

        List<INFOTYPE1000> GetAreaDropdownData();

        List<INFOTYPE1000> GetSubAreaDropdownData();

        List<INFOTYPE1000> GetRelationSelectData();
        List<string> GetRelationValidationByRelation(string relation);

        List<INFOTYPE1000> GetRelationValidationSelectData();

        DataTable GetMonthlyWorkscheduleGetByPeriod(string EmployeeID,int iYear,int iMonth);

        bool InsertActionLog(UTILITY.DATACLASS.ActionLog oActionLog);

        string GetBusinessPlace(string oBusinessArea);


        #region MonthlyWS
        List<MonthlyWS> SimulateMonthlyWorkschedule(string EmpSubGroupForWorkSchedule, string PublicHolidayCalendar, string EmpSubAreaForWorkSchedule, int Year, int Month, Dictionary<string, string> DayOption);

        List<MonthlyWS> GetMonthlyWS(string EmployeeID, int CheckYear);
        string GetSupervisorPositionByEmpPosition(string EmpPosition);
        #endregion
    }
}