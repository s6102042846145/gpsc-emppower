using System;
using System.Collections.Generic;
using ESS.EMPLOYEE.CONFIG.OM;

namespace ESS.EMPLOYEE.INTERFACE
{
    public interface IOMServiceHana
    {
        List<INFOTYPE1000> GetAllObject(string objId1, string objId2, List<ObjectType> objectTypes, string Profile);
        List<INFOTYPE1001> GetAllRelation(string objId1, string objId2, List<ObjectType> objectTypes, List<string> relationCodes, string Profile);

        List<INFOTYPE1003> GetAllApprovalData(string objId1, string objId2, string Profile);

        List<INFOTYPE1013> GetAllPositionBandLevel(string objId1, string objId2, string Profile);
        List<INFOTYPE1513> GetAllJobIndex(string objId1, string objId2, string Profile);

        List<INFOTYPE1010> GetAllOrgUnitLevel(string ObjectID1, string ObjectID2, string Profile);
    }
}