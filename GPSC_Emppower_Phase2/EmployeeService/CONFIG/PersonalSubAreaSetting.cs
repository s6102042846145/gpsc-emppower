using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.EMPLOYEE.CONFIG
{
    [Serializable()]
    public class PersonalSubAreaSetting
    {
        #region " Members "
        private string __personalArea;
        private string __personalSubArea;
        private string __description;
        private string __countryGrouping;
        private string __absAttGrouping;
        private string __timeQuotaGrouping;
        private string __holidayCalendar;
        private string __workscheduleGrouping;
        private string __dailyWorkscheduleGrouping;
        #endregion

        public PersonalSubAreaSetting()
        {
        }

        #region " PersonalArea "
        public string PersonalArea
        {
            get
            {
                return __personalArea;
            }
            set
            {
                __personalArea = value;
            }
        }
        #endregion

        #region " PersonalSubArea "
        public string PersonalSubArea
        {
            get
            {
                return __personalSubArea;
            }
            set
            {
                __personalSubArea = value;
            }
        }
        #endregion

        #region " Description "
        public string Description
        {
            get
            {
                return __description;
            }
            set
            {
                __description = value;
            }
        }
        #endregion

        #region " CountryGrouping "
        public string CountryGrouping
        {
            get
            {
                return __countryGrouping;
            }
            set
            {
                __countryGrouping = value;
            }
        }
        #endregion

        #region " AbsAttGrouping "
        public string AbsAttGrouping
        {
            get
            {
                return __absAttGrouping;
            }
            set
            {
                __absAttGrouping = value;
            }
        }
        #endregion

        #region " TimeQuotaGrouping "
        public string TimeQuotaGrouping
        {
            get
            {
                return __timeQuotaGrouping;
            }
            set
            {
                __timeQuotaGrouping = value;
            }
        }
        #endregion

        #region " HolidayCalendar "
        public string HolidayCalendar
        {
            get
            {
                return __holidayCalendar;
            }
            set
            {
                __holidayCalendar = value;
            }
        }
        #endregion

        #region " WorkScheduleGrouping "
        public string WorkScheduleGrouping
        {
            get
            {
                return __workscheduleGrouping;
            }
            set
            {
                __workscheduleGrouping = value;
            }
        }
        #endregion

        #region " DailyWorkScheduleGrouping "
        public string DailyWorkScheduleGrouping
        {
            get
            {
                return __dailyWorkscheduleGrouping;
            }
            set
            {
                __dailyWorkscheduleGrouping = value;
            }
        }
        #endregion

        public override int GetHashCode()
        {
            string cCode = string.Format("PERSONALSUBAREASETTING_{0}_{1}", PersonalArea, PersonalSubArea);
            return cCode.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return this.GetHashCode() == obj.GetHashCode();
        }

        //public static List<PersonalSubAreaSetting> GetAllSetting()
        //{
        //    return ServiceManager.EmployeeConfig.GetPersonalSubAreaSettingList("DEFAULT");
        //}

        public static List<PersonalSubAreaSetting> GetAllSetting(string Mode)
        {
            return ServiceManager.GetMirrorConfig(Mode).GetPersonalSubAreaSettingList("DEFAULT");
        }

        public static List<PersonalSubAreaSetting> GetAllSetting(string Mode, string Profile)
        {
            return ServiceManager.GetMirrorConfig(Mode).GetPersonalSubAreaSettingList(Profile);
        }

        public static void SaveTo(List<PersonalSubAreaSetting> Data, string Mode)
        {
            ServiceManager.GetMirrorConfig(Mode).SavePersonalSubAreaSettingList(Data, "DEFAULT");
        }

        public static void SaveTo(List<PersonalSubAreaSetting> Data, string Mode, string Profile)
        {
            ServiceManager.GetMirrorConfig(Mode).SavePersonalSubAreaSettingList(Data,Profile);
        }

        #region " GetSetting "
        public static PersonalSubAreaSetting GetSetting(string PersonalArea, string PersonalSubArea)
        {
            return ServiceManager.EmployeeConfig.GetPersonalSubAreaSetting(PersonalArea,PersonalSubArea);
        }
        #endregion

    }
}
