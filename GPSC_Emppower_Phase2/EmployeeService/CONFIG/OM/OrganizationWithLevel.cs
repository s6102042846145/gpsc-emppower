using System;
using ESS.UTILITY.EXTENSION;

namespace ESS.EMPLOYEE.CONFIG.OM
{
    //[Serializable()]
    public class OrganizationWithLevel : AbstractObject
    {
        public OrganizationWithLevel()
        {
        }
        public string OrgUnit { get; set; }
        public string OrgUnitShortName { get; set; }
        public string OrgUnitName { get; set; }
        public string OrgParent { get; set; }
        public int OrgLevel { get; set; }
        public bool IsUsed { get; set; }
    }
}