using System;
using System.Collections.Generic;
using System.Reflection;
using ESS.UTILITY.EXTENSION;

namespace ESS.EMPLOYEE.CONFIG.OM
{
    [Serializable()]
    public class INFOTYPE1001 : AbstractObject
    {
        public INFOTYPE1001()
        {

        }

        //protected override string GetEnumString(PropertyInfo prop)
        //{
        //    if (prop.PropertyType == typeof(ObjectType))
        //    {
        //        return Convert.ToChar((int)prop.GetValue(this, null)).ToString();
        //    }
        //    else
        //    {
        //        return base.GetEnumString(prop);
        //    }
        //}

        public ObjectType ObjectType { get; set; }

        public string ObjectID { get; set; }
        public ObjectType NextObjectType { get; set; }
        public string NextObjectID { get; set; }
        public string Relation { get; set; }
        public string OrderKey { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal PercentValue { get; set; }
    }
}