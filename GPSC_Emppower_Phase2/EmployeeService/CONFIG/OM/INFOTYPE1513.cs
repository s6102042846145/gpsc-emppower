using System;
using System.Collections.Generic;
using System.Reflection;
using ESS.UTILITY.EXTENSION;
using ESS.WORKFLOW;

namespace ESS.EMPLOYEE.CONFIG.OM
{
    
    public class INFOTYPE1513 : AbstractObject
    {
        public INFOTYPE1513()
        {
        }

        public string ObjectID { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public string MainGroup { get; set; }
    }
}