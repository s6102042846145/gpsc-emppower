﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.EMPLOYEE.CONFIG.OM
{
    public class UserRoleResponse
    {
        public string EmployeeID { get; set; }
        public string CompanyCode { get; set; }
        public string UserRole { get; set; }
        public string ResponseType { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseCompanyCode { get; set; }
        public bool AllowDuplicate { get; set; }
        public bool? IncludeSub { get; set; }
    }
}
