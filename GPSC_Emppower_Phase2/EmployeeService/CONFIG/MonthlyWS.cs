using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Configuration;

namespace ESS.EMPLOYEE.CONFIG
{
    [Serializable()]
    public class MonthlyWS : ESS.DATA.AbstractObject
    {
        #region "config"
        private static Configuration __config;
        private static Configuration config
        {
            get
            {
                if (__config == null)
                {
                    __config = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().CodeBase.Replace("file:///", "").Replace("/", "\\"));
                }
                return __config;
            }
        }

        private static String P_EMPSUBGROUPFORWORKSCHEDULE
        {
            get
            {
                if (config == null || config.AppSettings.Settings["P_EMPSUBGROUPFORWORKSCHEDULE"] == null)
                {
                    throw new Exception("can't find configuration for P_EMPSUBGROUPFORWORKSCHEDULE");
                }
                else
                {
                    return config.AppSettings.Settings["P_EMPSUBGROUPFORWORKSCHEDULE"].Value;
                }
            }
        }

        private static String P_PUBLICHOLIDAYCALENDAR
        {
            get
            {
                if (config == null || config.AppSettings.Settings["P_PUBLICHOLIDAYCALENDAR"] == null)
                {
                    throw new Exception("can't find configuration for P_PUBLICHOLIDAYCALENDAR");
                }
                else
                {
                    return config.AppSettings.Settings["P_PUBLICHOLIDAYCALENDAR"].Value;
                }
            }
        }

        private static String P_EMPSUBAREAFORWORKSCHEDULE
        {
            get
            {
                if (config == null || config.AppSettings.Settings["P_EMPSUBAREAFORWORKSCHEDULE"] == null)
                {
                    throw new Exception("can't find configuration for P_EMPSUBAREAFORWORKSCHEDULE");
                }
                else
                {
                    return config.AppSettings.Settings["P_EMPSUBAREAFORWORKSCHEDULE"].Value;
                }
            }
        }

        private static String P_WORKSCHEDULERULES
        {
            get
            {
                if (config == null || config.AppSettings.Settings["P_WORKSCHEDULERULES"] == null)
                {
                    throw new Exception("can't find configuration for P_WORKSCHEDULERULES");
                }
                else
                {
                    return config.AppSettings.Settings["P_WORKSCHEDULERULES"].Value;
                }
            }
        }

        #endregion

        #region " Members "
        private string __empSubGroupForWorkSchedule;
        private string __publicHolidayCalendar;
        private string __empSubAreaForWorkSchedule;
        private string __workScheduleCode;
        private string __valuationClass = "";
        private int __year;
        private int __month;
        private string __day01;
        private string __day01_h;
        private string __day02;
        private string __day02_h;
        private string __day03;
        private string __day03_h;
        private string __day04;
        private string __day04_h;
        private string __day05;
        private string __day05_h;
        private string __day06;
        private string __day06_h;
        private string __day07;
        private string __day07_h;
        private string __day08;
        private string __day08_h;
        private string __day09;
        private string __day09_h;
        private string __day10;
        private string __day10_h;
        private string __day11;
        private string __day11_h;
        private string __day12;
        private string __day12_h;
        private string __day13;
        private string __day13_h;
        private string __day14;
        private string __day14_h;
        private string __day15;
        private string __day15_h;
        private string __day16;
        private string __day16_h;
        private string __day17;
        private string __day17_h;
        private string __day18;
        private string __day18_h;
        private string __day19;
        private string __day19_h;
        private string __day20;
        private string __day20_h;
        private string __day21;
        private string __day21_h;
        private string __day22;
        private string __day22_h;
        private string __day23;
        private string __day23_h;
        private string __day24;
        private string __day24_h;
        private string __day25;
        private string __day25_h;
        private string __day26;
        private string __day26_h;
        private string __day27;
        private string __day27_h;
        private string __day28;
        private string __day28_h;
        private string __day29;
        private string __day29_h;
        private string __day30;
        private string __day30_h;
        private string __day31;
        private string __day31_h;
        Dictionary<int, string> __valuationClassList = new Dictionary<int, string>();
        #endregion

        public MonthlyWS()
        {
        }

        #region " Headers Properties "
        public string EmpSubGroupForWorkSchedule
        {
            get
            {
                return __empSubGroupForWorkSchedule;
            }
            set
            {
                __empSubGroupForWorkSchedule = value;
            }
        }
        public string PublicHolidayCalendar
        {
            get
            {
                return __publicHolidayCalendar;
            }
            set
            {
                __publicHolidayCalendar = value;
            }
        }

        public string EmpSubAreaForWorkSchedule
        {
            get
            {
                return __empSubAreaForWorkSchedule;
            }
            set
            {
                __empSubAreaForWorkSchedule = value;
            }
        }

        public string WorkScheduleRule
        {
            get
            {
                return __workScheduleCode;
            }
            set
            {
                __workScheduleCode = value;
            }
        }

        public int WS_Year
        {
            get
            {
                return __year;
            }
            set
            {
                __year = value;
            }
        }

        public int WS_Month
        {
            get
            {
                return __month;
            }
            set
            {
                __month = value;
            }
        }

        private void SetValuationClassExactly(int EndDay, string ValuationClass)
        {
            if (!__valuationClassList.ContainsKey(EndDay))
            {
                __valuationClassList.Add(EndDay, ValuationClass);
            }
            else
            {
                __valuationClassList[EndDay] = ValuationClass;
            }
        }

        public string GetValuationClassExactly(int EndDay)
        {
            foreach (int key in __valuationClassList.Keys)
            {
                if (key >= EndDay)
                {
                    return __valuationClassList[key];
                }
            }
            return ValuationClass;
        }

        public string ValuationClass
        {
            get
            {
                return __valuationClass;
            }
            set
            {
                __valuationClass = value;
            }
        }
        #endregion

        #region " Properties "
        public string Day01
        {
            get
            {
                return __day01;
            }
            set
            {
                __day01 = value;
            }
        }

        public string Day01_H
        {
            get
            {
                return __day01_h;
            }
            set
            {
                __day01_h = value;
            }
        }

        public string Day02
        {
            get
            {
                return __day02;
            }
            set
            {
                __day02 = value;
            }
        }

        public string Day02_H
        {
            get
            {
                return __day02_h;
            }
            set
            {
                __day02_h = value;
            }
        }

        public string Day03
        {
            get
            {
                return __day03;
            }
            set
            {
                __day03 = value;
            }
        }

        public string Day03_H
        {
            get
            {
                return __day03_h;
            }
            set
            {
                __day03_h = value;
            }
        }

        public string Day04
        {
            get
            {
                return __day04;
            }
            set
            {
                __day04 = value;
            }
        }

        public string Day04_H
        {
            get
            {
                return __day04_h;
            }
            set
            {
                __day04_h = value;
            }
        }

        public string Day05
        {
            get
            {
                return __day05;
            }
            set
            {
                __day05 = value;
            }
        }

        public string Day05_H
        {
            get
            {
                return __day05_h;
            }
            set
            {
                __day05_h = value;
            }
        }

        public string Day06
        {
            get
            {
                return __day06;
            }
            set
            {
                __day06 = value;
            }
        }

        public string Day06_H
        {
            get
            {
                return __day06_h;
            }
            set
            {
                __day06_h = value;
            }
        }

        public string Day07
        {
            get
            {
                return __day07;
            }
            set
            {
                __day07 = value;
            }
        }

        public string Day07_H
        {
            get
            {
                return __day07_h;
            }
            set
            {
                __day07_h = value;
            }
        }

        public string Day08
        {
            get
            {
                return __day08;
            }
            set
            {
                __day08 = value;
            }
        }

        public string Day08_H
        {
            get
            {
                return __day08_h;
            }
            set
            {
                __day08_h = value;
            }
        }

        public string Day09
        {
            get
            {
                return __day09;
            }
            set
            {
                __day09 = value;
            }
        }

        public string Day09_H
        {
            get
            {
                return __day09_h;
            }
            set
            {
                __day09_h = value;
            }
        }

        public string Day10
        {
            get
            {
                return __day10;
            }
            set
            {
                __day10 = value;
            }
        }

        public string Day10_H
        {
            get
            {
                return __day10_h;
            }
            set
            {
                __day10_h = value;
            }
        }

        public string Day11
        {
            get
            {
                return __day11;
            }
            set
            {
                __day11 = value;
            }
        }

        public string Day11_H
        {
            get
            {
                return __day11_h;
            }
            set
            {
                __day11_h = value;
            }
        }

        public string Day12
        {
            get
            {
                return __day12;
            }
            set
            {
                __day12 = value;
            }
        }

        public string Day12_H
        {
            get
            {
                return __day12_h;
            }
            set
            {
                __day12_h = value;
            }
        }

        public string Day13
        {
            get
            {
                return __day13;
            }
            set
            {
                __day13 = value;
            }
        }

        public string Day13_H
        {
            get
            {
                return __day13_h;
            }
            set
            {
                __day13_h = value;
            }
        }

        public string Day14
        {
            get
            {
                return __day14;
            }
            set
            {
                __day14 = value;
            }
        }

        public string Day14_H
        {
            get
            {
                return __day14_h;
            }
            set
            {
                __day14_h = value;
            }
        }

        public string Day15
        {
            get
            {
                return __day15;
            }
            set
            {
                __day15 = value;
            }
        }

        public string Day15_H
        {
            get
            {
                return __day15_h;
            }
            set
            {
                __day15_h = value;
            }
        }

        public string Day16
        {
            get
            {
                return __day16;
            }
            set
            {
                __day16 = value;
            }
        }

        public string Day16_H
        {
            get
            {
                return __day16_h;
            }
            set
            {
                __day16_h = value;
            }
        }

        public string Day17
        {
            get
            {
                return __day17;
            }
            set
            {
                __day17 = value;
            }
        }

        public string Day17_H
        {
            get
            {
                return __day17_h;
            }
            set
            {
                __day17_h = value;
            }
        }

        public string Day18
        {
            get
            {
                return __day18;
            }
            set
            {
                __day18 = value;
            }
        }

        public string Day18_H
        {
            get
            {
                return __day18_h;
            }
            set
            {
                __day18_h = value;
            }
        }

        public string Day19
        {
            get
            {
                return __day19;
            }
            set
            {
                __day19 = value;
            }
        }

        public string Day19_H
        {
            get
            {
                return __day19_h;
            }
            set
            {
                __day19_h = value;
            }
        }

        public string Day20
        {
            get
            {
                return __day20;
            }
            set
            {
                __day20 = value;
            }
        }

        public string Day20_H
        {
            get
            {
                return __day20_h;
            }
            set
            {
                __day20_h = value;
            }
        }

        public string Day21
        {
            get
            {
                return __day21;
            }
            set
            {
                __day21 = value;
            }
        }

        public string Day21_H
        {
            get
            {
                return __day21_h;
            }
            set
            {
                __day21_h = value;
            }
        }

        public string Day22
        {
            get
            {
                return __day22;
            }
            set
            {
                __day22 = value;
            }
        }

        public string Day22_H
        {
            get
            {
                return __day22_h;
            }
            set
            {
                __day22_h = value;
            }
        }

        public string Day23
        {
            get
            {
                return __day23;
            }
            set
            {
                __day23 = value;
            }
        }

        public string Day23_H
        {
            get
            {
                return __day23_h;
            }
            set
            {
                __day23_h = value;
            }
        }

        public string Day24
        {
            get
            {
                return __day24;
            }
            set
            {
                __day24 = value;
            }
        }

        public string Day24_H
        {
            get
            {
                return __day24_h;
            }
            set
            {
                __day24_h = value;
            }
        }

        public string Day25
        {
            get
            {
                return __day25;
            }
            set
            {
                __day25 = value;
            }
        }

        public string Day25_H
        {
            get
            {
                return __day25_h;
            }
            set
            {
                __day25_h = value;
            }
        }

        public string Day26
        {
            get
            {
                return __day26;
            }
            set
            {
                __day26 = value;
            }
        }

        public string Day26_H
        {
            get
            {
                return __day26_h;
            }
            set
            {
                __day26_h = value;
            }
        }

        public string Day27
        {
            get
            {
                return __day27;
            }
            set
            {
                __day27 = value;
            }
        }

        public string Day27_H
        {
            get
            {
                return __day27_h;
            }
            set
            {
                __day27_h = value;
            }
        }

        public string Day28
        {
            get
            {
                return __day28;
            }
            set
            {
                __day28 = value;
            }
        }

        public string Day28_H
        {
            get
            {
                return __day28_h;
            }
            set
            {
                __day28_h = value;
            }
        }

        public string Day29
        {
            get
            {
                return __day29;
            }
            set
            {
                __day29 = value;
            }
        }

        public string Day29_H
        {
            get
            {
                return __day29_h;
            }
            set
            {
                __day29_h = value;
            }
        }

        public string Day30
        {
            get
            {
                return __day30;
            }
            set
            {
                __day30 = value;
            }
        }

        public string Day30_H
        {
            get
            {
                return __day30_h;
            }
            set
            {
                __day30_h = value;
            }
        }

        public string Day31
        {
            get
            {
                return __day31;
            }
            set
            {
                __day31 = value;
            }
        }

        public string Day31_H
        {
            get
            {
                return __day31_h;
            }
            set
            {
                __day31_h = value;
            }
        }
        #endregion

        public static List<MonthlyWS> GetAll(int Year, string SourceMode, string SourceProfile)
        {
            return ESS.EMPLOYEE.ServiceManager.GetMirrorConfig(SourceMode).GetMonthlyWorkscheduleList(Year, SourceProfile);
        }

        public static MonthlyWS GetCalendar(string EmployeeID, int Year, int Month)
        {
            return GetCalendar(EmployeeID,DateTime.Now, Year, Month, ServiceManager.CALENDAR_USE_SUBSTITUTE);
        }

        public static MonthlyWS GetCalendar(string EmployeeID, DateTime CheckDate)
        {
            return GetCalendar(EmployeeID, CheckDate, CheckDate.Year, CheckDate.Month, ServiceManager.CALENDAR_USE_SUBSTITUTE);
        }

        public static MonthlyWS GetCalendar(string EmployeeID, DateTime CheckDate, bool includeSubstitute)
        {
            return GetCalendar(EmployeeID, CheckDate, CheckDate.Year, CheckDate.Month, includeSubstitute);
        }
            
        public static MonthlyWS GetCalendar(string EmployeeID, DateTime CheckDate, int Year, int Month, bool includeSubstitute)
        {
            EmployeeData oEmp = new EmployeeData(EmployeeID,CheckDate);

            string cSubGroup = "";
            string cSubArea = "";
            string cHoliday = "";

            if (oEmp.OrgAssignment != null) 
            {
                cSubGroup = oEmp.OrgAssignment.SubGroupSetting.WorkScheduleGrouping;
                cSubArea = oEmp.OrgAssignment.SubAreaSetting.WorkScheduleGrouping;
                cHoliday = oEmp.OrgAssignment.SubAreaSetting.HolidayCalendar;
            }

            MonthlyWS oReturn = new MonthlyWS();
            oReturn.WS_Year = Year;
            oReturn.WS_Month = Month;
            oReturn.EmpSubGroupForWorkSchedule = cSubGroup;
            oReturn.EmpSubAreaForWorkSchedule = cSubArea;
            oReturn.PublicHolidayCalendar = cHoliday;

            //ModifiedBy: Ratchatawan W. (2013-04-05)
            List<Substitution> substitutionList = new List<Substitution>();
            if (includeSubstitute)
                substitutionList = Substitution.GetSubstitutionList(EmployeeID, Year, Month, true);
            //Dictionary<int, Substitution> substitutionDict = new Dictionary<int, Substitution>();
            //if (includeSubstitute)
            //{
            //    List<Substitution> substitutionList = Substitution.GetSubstitutionList(EmployeeID, Year, Month, true);

            //    foreach (Substitution item in substitutionList.FindAll(delegate(Substitution oSub) { return oSub.Status == "COMPLETED"; }))
            //    {
            //        for (DateTime rundate = item.BeginDate; rundate <= item.EndDate; rundate = rundate.AddDays(1))
            //        {
            //            if (rundate.Year == Year && rundate.Month == Month)
            //            {
            //                substitutionDict.Add(rundate.Day, item);
            //            }
            //        }
            //    }
            //}
            bool lFirst = true;
            foreach (INFOTYPE0007 item in oEmp.GetWorkSchedule(Year, Month))
            {
                DateTime dTemp = new DateTime(Year, Month, 1);
                dTemp = dTemp.AddMonths(1).AddDays(-1);
                if (dTemp < item.BeginDate)
                    break;
                EmployeeData oEmp1 = new EmployeeData(EmployeeID,item.BeginDate);
                MonthlyWS temp = EMPLOYEE.ServiceManager.EmployeeConfig.GetMonthlyWorkschedule(oEmp1.OrgAssignment.SubGroupSetting.WorkScheduleGrouping, oEmp1.OrgAssignment.SubAreaSetting.WorkScheduleGrouping, oEmp1.OrgAssignment.SubAreaSetting.HolidayCalendar, item.WFRule, Year, Month);
                oReturn.WorkScheduleRule = temp.WorkScheduleRule;
                if (lFirst)
                {
                    oReturn.ValuationClass = temp.ValuationClass;
                }
                lFirst = false;
                int nStart = 1;
                if (item.BeginDate.Year == Year && item.BeginDate.Month == Month)
                {
                    nStart = item.BeginDate.Day;
                }
                int nEnd = dTemp.Day;
                if (item.EndDate.Year == Year && item.EndDate.Month == Month)
                {
                    nEnd = item.EndDate.Day;
                }
                oReturn.SetValuationClassExactly(nEnd, temp.ValuationClass);
                PropertyInfo oProp;
                Type oType = oReturn.GetType();
                for (int index = nStart; index <= nEnd; index++)
                {
                    string cDayNo = index.ToString("00");
                    string cDWSCode;
                    oProp = oType.GetProperty(string.Format("Day{0}", cDayNo));

                    //Modified By: Ratchatawan W. (2013-4-5)
                    DateTime oCheckDate = new DateTime(Year, Month, index);
                    Substitution oSubstitution = substitutionList.Find(delegate(Substitution oSub) { return oSub.Status == "COMPLETED" && oSub.BeginDate <= oCheckDate && oCheckDate <= oSub.EndDate; });
                    if (oSubstitution != null)
                    {
//                    if (substitutionList.Exists(delegate(Substitution oSub) { return oSub.Status == "COMPLETED" && oSub.BeginDate.Day == index && oSub.BeginDate.Month == Month && oSub.BeginDate.Year == Year; }))
//                    {
//                        Substitution oSubstitution = substitutionList.Find(delegate(Substitution oSub) { return oSub.BeginDate.Day == index; });
                        if (String.IsNullOrEmpty(oSubstitution.EmpDWSCodeNew))
                        {
                            cDWSCode = oSubstitution.SubstituteBeginTime.ToString() + "-" + oSubstitution.SubstituteEndTime.ToString();
                        }
                        else
                        {
                            if (oEmp.EmployeeID.Trim() == oSubstitution.EmployeeID.Trim())
                                cDWSCode = oSubstitution.EmpDWSCodeNew;
                            else
                                cDWSCode = oSubstitution.SubDWSCodeNew;
                        }
                    }
                    else
                    {
                        cDWSCode = (string)oProp.GetValue(temp, null);
                    }

                    //if (substitutionDict.ContainsKey(index))
                    //{
                    //    //CommentBy: Ratchatawan W. (2012-11-16)
                    //    //cDWSCode = substitutionDict[index].DWSCodeView;
                    //    if(oEmp.EmployeeID == substitutionDict[index].EmployeeID)
                    //        cDWSCode = substitutionDict[index].EmpDWSCodeNew;
                    //    else
                    //        cDWSCode = substitutionDict[index].SubDWSCodeNew;
                    //}
                    //else
                    //{
                    //    cDWSCode = (string)oProp.GetValue(temp, null);
                    //}

                    oProp.SetValue(oReturn, cDWSCode, null);

                    oProp = oType.GetProperty(string.Format("Day{0}_H", cDayNo));
                    oProp.SetValue(oReturn, oProp.GetValue(temp, null), null);
                }
            }
            return oReturn;
        }

        public static MonthlyWS GetSimulateCalendar(PersonalSubAreaSetting SubAreaSetting,PersonalSubGroupSetting SubGroupSetting, int Year, int Month, string WFRule)
        {
            string cSubGroup = SubGroupSetting.WorkScheduleGrouping;
            string cSubArea = SubAreaSetting.WorkScheduleGrouping;
            string cHoliday = SubAreaSetting.HolidayCalendar;
            MonthlyWS oReturn = new MonthlyWS();
            oReturn.WS_Year = Year;
            oReturn.WS_Month = Month;
            oReturn.EmpSubGroupForWorkSchedule = cSubGroup;
            oReturn.EmpSubAreaForWorkSchedule = cSubArea;
            oReturn.PublicHolidayCalendar = cHoliday;

            bool lFirst = true;
            DateTime dTemp = new DateTime(Year, Month, 1);
            MonthlyWS temp = EMPLOYEE.ServiceManager.EmployeeConfig.GetMonthlyWorkschedule(SubGroupSetting.WorkScheduleGrouping, SubAreaSetting.WorkScheduleGrouping, SubAreaSetting.HolidayCalendar, WFRule, Year, Month);
            oReturn.WorkScheduleRule = temp.WorkScheduleRule;
            if (lFirst)
            {
                oReturn.ValuationClass = temp.ValuationClass;
            }
            lFirst = false;
            int nStart = dTemp.Day;
            int nEnd = dTemp.AddMonths(1).AddDays(-1).Day;
            oReturn.SetValuationClassExactly(nEnd, temp.ValuationClass);
            PropertyInfo oProp;
            Type oType = oReturn.GetType();
            for (int index = nStart; index <= nEnd; index++)
            {
                string cDayNo = index.ToString("00");
                string cDWSCode;
                oProp = oType.GetProperty(string.Format("Day{0}", cDayNo));

                cDWSCode = (string)oProp.GetValue(temp, null);

                oProp.SetValue(oReturn, cDWSCode, null);

                oProp = oType.GetProperty(string.Format("Day{0}_H", cDayNo));
                oProp.SetValue(oReturn, oProp.GetValue(temp, null), null);
            }

            return oReturn;
        }

        public static List<MonthlyWS> SimulateMonthlyWorkschedule(string EmpSubGroupForWorkSchedule, string PublicHolidayCalendar, string EmpSubAreaForWorkSchedule, int Year, int Month, Dictionary<string, string> DayOption)
        {
            return ServiceManager.EmployeeConfig.SimulateMonthlyWorkschedule(EmpSubGroupForWorkSchedule, PublicHolidayCalendar, EmpSubAreaForWorkSchedule, Year, Month, DayOption);
        }

        public static MonthlyWS GetPublicCalendar(int Year, int Month)
        {
            return EMPLOYEE.ServiceManager.EmployeeConfig.GetMonthlyWorkschedule(P_EMPSUBGROUPFORWORKSCHEDULE, P_EMPSUBAREAFORWORKSCHEDULE, P_PUBLICHOLIDAYCALENDAR, P_WORKSCHEDULERULES, Year, Month);
        }

        public string GetWSCode(int dayNo, bool isCode)
        {
            PropertyInfo oProp;
            Type oType = this.GetType();
            string cDayNo = dayNo.ToString("00");
            string cReturn = "";
            if (isCode)
            {
                oProp = oType.GetProperty(string.Format("Day{0}", cDayNo));
            }
            else
            {
                oProp = oType.GetProperty(string.Format("Day{0}_H", cDayNo));
            }
            if (oProp != null)
            {
                cReturn = (string)oProp.GetValue(this, null);
            }
            return cReturn;
        }

        public static void SaveTo(int Year, List<MonthlyWS> list, string Mode, string Profile)
        {
            ESS.EMPLOYEE.ServiceManager.GetMirrorConfig(Mode).SaveMonthlyWorkscheduleList(Year, list, Profile);
        }


    }
}
