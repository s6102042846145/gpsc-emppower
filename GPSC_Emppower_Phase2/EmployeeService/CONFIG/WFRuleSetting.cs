using System;
using System.Collections.Generic;
using System.Text;
using ESS.DATA;

namespace ESS.EMPLOYEE.CONFIG
{
    public class WFRuleSetting : AbstractObject
    {
        private string __wfrule = "";
        private TimeSpan __normalBeginTime = TimeSpan.MinValue;
        private TimeSpan __normalEndTime = TimeSpan.MinValue;
        private TimeSpan __cutoffTime = TimeSpan.MinValue;
        public WFRuleSetting()
        {
 
        }

        public string WFRule
        {
            get
            {
                return __wfrule;
            }
            set
            {
                __wfrule = value;
            }
        }

        public TimeSpan NormalBeginTime
        {
            get
            {
                return __normalBeginTime;
            }
            set
            {
                __normalBeginTime = value;
            }
        }

        public TimeSpan NormalEndTime
        {
            get
            {
                return __normalEndTime;
            }
            set
            {
                __normalEndTime = value;
            }
        }

        public TimeSpan CutoffTime
        {
            get
            {
                return __cutoffTime;
            }
            set
            {
                __cutoffTime = value;
            }
        }

        public static WFRuleSetting GetWFRuleSetting(string WFRule)
        {
            return EmployeeManagement.GetWFRuleSetting(WFRule);
        }
    }
}
