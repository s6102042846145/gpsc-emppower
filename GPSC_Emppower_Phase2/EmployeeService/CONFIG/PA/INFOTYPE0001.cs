using System;
using System.Collections.Generic;
using ESS.EMPLOYEE.ABSTRACT;
using ESS.EMPLOYEE.CONFIG.OM;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.EMPLOYEE.INTERFACE;
using ESS.WORKFLOW;

namespace ESS.EMPLOYEE.CONFIG.PA
{
    //[Serializable()]
    public class INFOTYPE0001 : AbstractInfoType
    {
        public INFOTYPE0001()
        {
        }

        private DateTime CheckDate { get; set; }
        private List<UserRoleResponseSetting> permitRoles
        {
            get
            {
                List<UserRoleResponseSetting> roles = new List<UserRoleResponseSetting>();
                roles.Add(new UserRoleResponseSetting("PAADMIN"));
                roles.Add(new UserRoleResponseSetting("TIMEADMIN"));
                roles.Add(new UserRoleResponseSetting("PAYROLLADMIN"));
                roles.Add(new UserRoleResponseSetting("MANAGER", true));
                roles.Add(new UserRoleResponseSetting("SUPERVISOR"));
                return roles;
            }
        }
        private string oCompanyCode = string.Empty;
        public string CompanyCode 
        { 
            get 
            { 
                return oCompanyCode.Trim().PadLeft(4, '0'); 
            } 
            set 
            {
                oCompanyCode = value.Trim().PadLeft(4, '0'); 
            } 
        }
        public string Name { get; set; }
        public string Area { get; set; }
        public string SubArea{ get; set; }
        public string EmpGroup{ get; set; }

        public string PositionID { get; set; }

        public string EmpSubGroup{ get; set; }

        public string OrgUnit { get; set; }
        public string Position{ get; set; }
        public string AdminGroup{ get; set; }
        
        public string CostCenter{ get; set; }
        public string CostCenterText { get; set; }
        protected EmployeeData currentLogon
        {
            get
            {
                return currentSetting.Employee;
            }
        }

        protected UserSetting currentSetting
        {
            get
            {
                if (WorkflowPrinciple.Current == null)
                {
                    return new UserSetting("", "");
                }
                return WorkflowPrinciple.Current.UserSetting;
            }
        }
        public override string InfoType
        {
            get { return "0001"; }
        }

        public PersonalArea AreaSetting
        {
            get
            {
                return EmployeeManagement.CreateInstance(CompanyCode).GetSettingFromEmpArea(this.Area);
            }
        }
        public PersonalSubAreaSetting SubAreaSetting
        {
            get
            {
                //if (__setting == null || __setting.PersonalArea != this.Area || __setting.PersonalSubArea != this.SubArea)
                //{
                return EmployeeManagement.CreateInstance(CompanyCode).GetSettingFromEmpSubArea(this.Area, this.SubArea);
                //}
                //return __setting;
            }
        }

        public PersonalSubGroupSetting SubGroupSetting
        {
            get
            {
                //if (__groupSetting == null || __groupSetting.EmpGroup != this.EmpGroup || __groupSetting.EmpSubGroup != this.EmpSubGroup)
                //{
                return EmployeeManagement.CreateInstance(CompanyCode).GetSettingFromEmpSubGroup(this.EmpGroup, this.EmpSubGroup);
                //}
                //return __groupSetting;
            }
        }

        public override void SaveTo(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<IInfoType> Data)
        {
            List<INFOTYPE0001> List = new List<INFOTYPE0001>();
            foreach (IInfoType item in Data)
            {
                List.Add((INFOTYPE0001)item);
            }
            ServiceManager.CreateInstance(CompanyCode).GetMirrorService(Mode).SaveInfotype0001(EmployeeID1, EmployeeID2, List, Profile);
        }
        public INFOTYPE1000 OrgUnitData
        {
            get
            {
                //if (__orgUnitData == null)
                //{
                if (this.CheckDate == DateTime.MinValue || this.CheckDate == DateTime.MaxValue || this.CheckDate < DateTime.MinValue)
                {
                    this.CheckDate = DateTime.Now;
                }
                return ServiceManager.CreateInstance(CompanyCode).OMService.GetObjectData(ObjectType.O, this.OrgUnit, this.CheckDate, currentSetting.Language);
                //}
                //return __orgUnitData;
            }
            set 
            { 
            }
        }

        public INFOTYPE1000 PositionData
        {
            get
            {
                //if (__positionData == null)
                //{
                if (this.CheckDate == DateTime.MinValue || this.CheckDate == DateTime.MaxValue || this.CheckDate < DateTime.MinValue)
                {
                    this.CheckDate = DateTime.Now;
                }
                return ServiceManager.CreateInstance(CompanyCode).OMService.GetObjectData(ObjectType.S, this.Position, CheckDate, currentSetting.Language);
                //}
                //return __positionData;
            }
        }

        public string BusinessArea { get; set; }

        public string PayrollArea { get; set; }

        public List<INFOTYPE0001> GetList(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).ERPEmployeeService.GetInfotype0001List(EmployeeID);
        }

        public  List<INFOTYPE0001> GetList(string EmployeeID, DateTime dtCheckdate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ERPEmployeeService.GetInfotype0001List(EmployeeID, dtCheckdate);
        }

        public List<INFOTYPE0001> GetListFromSAP(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).ERPEmployeeService.GetInfotype0001List(EmployeeID);
        }

        public List<INFOTYPE0001> MergeEndDate(List<INFOTYPE0001> lstInfo)
        {
            lstInfo.Sort(new INFOTYPE0001_Comparer());
            List<INFOTYPE0001> lstReturn = new List<INFOTYPE0001>();
            string subArea = string.Empty;
            foreach (INFOTYPE0001 item in lstInfo)
            {
                if (string.IsNullOrEmpty(subArea) || !subArea.Equals(item.SubArea))
                {
                    subArea = item.SubArea;
                    lstReturn.Add(item);
                }
                else
                {
                    lstReturn[lstReturn.Count - 1].EndDate = item.EndDate;
                }
            }

            return lstReturn;
        }

    }
}