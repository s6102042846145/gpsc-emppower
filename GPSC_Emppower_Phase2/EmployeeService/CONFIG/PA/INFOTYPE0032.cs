using System;
using System.Collections.Generic;
using ESS.EMPLOYEE.ABSTRACT;
using ESS.EMPLOYEE.INTERFACE;
using ESS.WORKFLOW;

namespace ESS.EMPLOYEE.CONFIG.PA
{
    [Serializable()]
    public class INFOTYPE0032 : AbstractInfoType
    {

        public INFOTYPE0032()
        {
        }

        public override string InfoType
        {
            get { return "0032"; }
        }

        public string EmployeeID { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public string OldEmployeeID { get; set; }

        public override List<IInfoType> GetList(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            List<IInfoType> oReturn = new List<IInfoType>();
            oReturn.AddRange(ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetMirrorService(Mode).GetInfotype0032List(EmployeeID1, EmployeeID2, DateTime.Now, Profile).ToArray());
            return oReturn;
        }

        public override void SaveTo(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<IInfoType> Data)
        {
            List<INFOTYPE0032> List = new List<INFOTYPE0032>();
            foreach (IInfoType item in Data)
            {
                List.Add((INFOTYPE0032)item);
            }
            ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetMirrorService(Mode).SaveInfotype0032(EmployeeID1, EmployeeID2, List, Profile);
        }
    }
}