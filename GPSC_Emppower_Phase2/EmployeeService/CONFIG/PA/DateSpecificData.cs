using System;
using System.Collections.Generic;
using ESS.UTILITY.EXTENSION;

namespace ESS.EMPLOYEE.CONFIG.PA
{
    [Serializable()]
    public class DateSpecificData : AbstractObject
    {
        public DateSpecificData()
        {
        }
        public string EmployeeID { get; set; }
        public DateTime HiringDate { get; set; }
        public DateTime StartAbsenceQuota { get; set; }
        public DateTime StartPF { get; set; }
        public DateTime EndPF { get; set; }
        public DateTime RetirementDate { get; set; }
        public DateTime StartWorkingDate { get; set; }
        public DateTime PassProbation { get; set; }
    }
}