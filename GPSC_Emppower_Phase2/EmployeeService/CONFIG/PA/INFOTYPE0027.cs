using System;
using System.Collections.Generic;
using System.Text;
using ESS.EMPLOYEE.CONFIG;
using ESS.WORKFLOW;
using ESS.EMPLOYEE.ABSTRACT;

namespace ESS.EMPLOYEE.CONFIG.PA
{
    [Serializable()]
    public class INFOTYPE0027 : AbstractInfoType
    {
        public override string InfoType
        {
            get { return "0027"; }
        }

        public string EmployeeID { get; set; }

        public string SubType { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }


        public string CompanyCode { get; set; }


        public string CostCenter { get; set; }


        public string Percentage { get; set; }

    }
}