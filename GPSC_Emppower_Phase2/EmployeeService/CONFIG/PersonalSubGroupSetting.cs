using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.EMPLOYEE.CONFIG
{
    [Serializable()]
    public class PersonalSubGroupSetting
    {
        private string __empGroup;
        private string __empSubGroup;
        private string __workScheduleGrouping;
        private string __timeQuotaTypeGrouping;
        public PersonalSubGroupSetting()
        {
        }
        public string EmpGroup
        {
            get
            {
                return __empGroup;
            }
            set
            {
                __empGroup = value;
            }
        }
        public string EmpSubGroup
        {
            get
            {
                return __empSubGroup;
            }
            set
            {
                __empSubGroup = value;
            }
        }
        public string WorkScheduleGrouping
        {
            get
            {
                return __workScheduleGrouping;
            }
            set
            {
                __workScheduleGrouping = value;
            }
        }
        public string TimeQuotaTypeGrouping
        {
            get
            {
                return __timeQuotaTypeGrouping;
            }
            set
            {
                __timeQuotaTypeGrouping = value;
            }
        }

        public override int GetHashCode()
        {
            string cCode = string.Format("PERSONALSUBGROUPSETTING_{0}_{1}", EmpGroup, EmpSubGroup);
            return cCode.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return this.GetHashCode() == obj.GetHashCode();
        }

        public static List<PersonalSubGroupSetting> GetAllSetting(string Mode)
        {
            return ServiceManager.GetMirrorConfig(Mode).GetPersonalSubGroupSettingList("DEFAULT");
        }

        public static List<PersonalSubGroupSetting> GetAllSetting(string SourceMode, string SourceProfile)
        {
            return ServiceManager.GetMirrorConfig(SourceMode).GetPersonalSubGroupSettingList(SourceProfile);
        }

        public static void SaveTo(List<PersonalSubGroupSetting> Data, string TargetMode)
        {
            ServiceManager.GetMirrorConfig(TargetMode).SavePersonalSubGroupSettingList(Data, "DEFAULT");
        }
        public static void SaveTo(List<PersonalSubGroupSetting> Data, string TargetMode, string TargetProfile)
        {
            ServiceManager.GetMirrorConfig(TargetMode).SavePersonalSubGroupSettingList(Data, TargetProfile);
        }

        #region " GetSetting "
        public static PersonalSubGroupSetting GetSetting(string EmpGroup, string EmpSubGroup)
        {
            return ServiceManager.EmployeeConfig.GetPersonalSubGroupSetting(EmpGroup, EmpSubGroup);
        }
        #endregion



    }
}
