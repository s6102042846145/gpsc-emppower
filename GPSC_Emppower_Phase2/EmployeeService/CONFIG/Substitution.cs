using System;
using System.Collections.Generic;
using System.Text;
using ESS.DATA;
using ESS.EMPLOYEE.CONFIG;

namespace ESS.EMPLOYEE
{
    [Serializable()]
    public class Substitution : AbstractObject
    {
        private bool _IsDraft;
        private string _SubDWSCodeOld = string.Empty;
        private string _SubDWSCodeNew = string.Empty;
        private string _EmpDWSCodeNew = string.Empty;
        private string _EmpDWSCodeOld = string.Empty;
        private bool _IsOverride;
        private string __employeeID = "";
        private DateTime __beginDate = DateTime.MinValue;
        private DateTime __endDate = DateTime.MinValue;
        private DateTime __substituteBeginDate = DateTime.MinValue;
        private DateTime __substituteEndDate = DateTime.MinValue;
        private TimeSpan __substituteBeginTime = TimeSpan.MinValue;
        private TimeSpan __substituteEndTime = TimeSpan.MinValue;
        private string __DWSGroup = "";
        private string __DWSCode = "";
        private string __empSubGroupSetting = "";
        private string __holidayCalendar = "";
        private string __empSubAreaSetting = "";
        private string __WSRule = "";
        private string __Substitute = string.Empty;
        private string __requestNo = "";
        private string __status = "";

        public bool IsDraft
        {
            get { return _IsDraft; }
            set
            {
                _IsDraft = value;
            }
        }

        public string SubDWSCodeOld
        {
            get { return _SubDWSCodeOld; }
            set
            {
                _SubDWSCodeOld = value;
            }
        }

        public string SubDWSCodeNew
        {
            get { return _SubDWSCodeNew; }
            set
            {
                _SubDWSCodeNew = value;
            }
        }

        public string EmpDWSCodeNew
        {
            get { return _EmpDWSCodeNew; }
            set
            {
                _EmpDWSCodeNew = value;
            }
        }

        public string EmpDWSCodeOld
        {
            get { return _EmpDWSCodeOld; }
            set
            {
                _EmpDWSCodeOld = value;
            }
        }
        public bool IsOverride
        {
            get { return _IsOverride; }
            set
            {
                _IsOverride = value;
            }
        }

        public Substitution()
        {
        }

        public string EmployeeID
        {
            get
            {
                return __employeeID;
            }
            set
            {
                __employeeID = value;
            }
        }

        public DateTime BeginDate
        {
            get
            {
                return __beginDate;
            }
            set
            {
                __beginDate = value;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return __endDate;
            }
            set
            {
                __endDate = value;
            }
        }

        public DateTime SubstituteBeginDate
        {
            get
            {
                return __substituteBeginDate;
            }
            set
            {
                __substituteBeginDate = value;
            }
        }

        public DateTime SubstituteEndDate
        {
            get
            {
                return __substituteEndDate;
            }
            set
            {
                __substituteEndDate = value;
            }
        }

        public TimeSpan SubstituteBeginTime
        {
            get
            {
                return __substituteBeginTime;
            }
            set
            {
                __substituteBeginTime = value;
            }
        }

        public TimeSpan SubstituteEndTime
        {
            get
            {
                return __substituteEndTime;
            }
            set
            {
                __substituteEndTime = value;
            }
        }

        public string DWSGroup
        {
            get
            {
                return __DWSGroup;
            }
            set
            {
                __DWSGroup = value;
            }
        }

        public string DWSCode
        {
            get
            {
                return __DWSCode;
            }
            set
            {
                __DWSCode = value;
            }
        }

        public string EmpSubGroupSetting
        {
            get
            {
                return __empSubGroupSetting;
            }
            set
            {
                __empSubGroupSetting = value;
            }
        }

        public string HolidayCalendar
        {
            get
            {
                return __holidayCalendar;
            }
            set
            {
                __holidayCalendar = value;
            }
        }

        public string EmpSubAreaSetting
        {
            get
            {
                return __empSubAreaSetting;
            }
            set
            {
                __empSubAreaSetting = value;
            }
        }

        public string Status
        {
            get
            {
                return __status;
            }
            set
            {
                __status = value;
            }
        }

        public string WSRule
        {
            get
            {
                return __WSRule;
            }
            set
            {
                __WSRule = value;
            }
        }

        public string DWSCodeView
        {
            get
            {
                if (DWSCode == "" && WSRule != "")
                {
                    MonthlyWS temp = EMPLOYEE.ServiceManager.EmployeeConfig.GetMonthlyWorkschedule(EmpSubGroupSetting, EmpSubAreaSetting, HolidayCalendar, WSRule, BeginDate.Year, BeginDate.Month);
                    if (temp != null)
                    {
                        return temp.GetWSCode(BeginDate.Day, true);
                    }
                    else
                    {
                        return DWSCode;
                    }
                }
                else
                {
                    return DWSCode;
                }
            }
        }

        public string Substitute
        {
            get
            {
                return __Substitute;
            }
            set
            {
                __Substitute = value;
            }
        }

        public string RequestNo
        {
            get
            {
                return __requestNo;
            }
            set
            {
                __requestNo = value;
            }
        }

        public static List<Substitution> GetSubstitutionList(string EmployeeID, int Year, int Month)
        {
            return GetSubstitutionList(EmployeeID, Year, Month, true);
        }

        public static List<Substitution> GetSubstitutionList(string EmployeeID, int Year, int Month, bool OnlyEffective)
        {
            DateTime date1, date2;
            date1 = new DateTime(Year, Month, 1);
            date2 = date1.AddMonths(1).AddDays(-1);
            return GetSubstitutionList(EmployeeID, date1, date2, OnlyEffective);
        }

        public static List<Substitution> GetSubstitutionList(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            return GetSubstitutionList(EmployeeID, BeginDate, EndDate, true);
        }

        public static List<Substitution> GetSubstitutionList(string EmployeeID, DateTime BeginDate, DateTime EndDate, bool OnlyEffective)
        {
            List<Substitution> buffer = new List<Substitution>();
            Dictionary<DateTime, Substitution> oDictSubstition = new Dictionary<DateTime, Substitution>();
            if (!OnlyEffective)
            {
                buffer.AddRange(EMPLOYEE.ServiceManager.EmployeeService.GetInfotype2003(EmployeeID, BeginDate, EndDate));
            }
            buffer.AddRange(EMPLOYEE.ServiceManager.ERPEmployeeService.GetInfotype2003(EmployeeID, BeginDate, EndDate));

            foreach (Substitution item in buffer)
            {
                oDictSubstition.Add(item.BeginDate.Date, item);
            }

            List<Substitution> oSubstitutionListFromDB = EMPLOYEE.ServiceManager.EmployeeService.GetInfotype2003(EmployeeID, BeginDate, EndDate);
            foreach (Substitution oSubstitution in oSubstitutionListFromDB)
            {
                if (oDictSubstition.ContainsKey(oSubstitution.BeginDate.Date) && oDictSubstition[oSubstitution.BeginDate.Date].Status.Equals(oSubstitution.Status))
                {
                    buffer.Remove(oDictSubstition[oSubstitution.BeginDate.Date]);
                }
                buffer.Add(oSubstitution);
            }
            oDictSubstition.Clear();
            oDictSubstition = null;
            return buffer;
        }

        public static List<Substitution> GetSubstitutionListLog(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            List<Substitution> buffer = new List<Substitution>();
            buffer.AddRange(EMPLOYEE.ServiceManager.EmployeeService.GetInfotype2003_Log(EmployeeID, BeginDate, EndDate));
            return buffer;
        }
    }
}
