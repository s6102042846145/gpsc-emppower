using System;
using System.Collections.Generic;
using System.Text;
using ESS.DATA;

namespace ESS.EMPLOYEE.CONFIG
{
    [Serializable()]
    public class DailyWS : AbstractObject
    {
        private bool _IsAttendance0192;
        private string __dailyWorkscheduleGrouping;
        private string __dailyWorkscheduleCode;
        private string __workscheduleClass;
        private DateTime __beginDate;
        private DateTime __endDate;
        private decimal __workHours;
        private TimeSpan __beginTime;
        private TimeSpan __endTime;
        private TimeSpan __normalBeginTime;
        private TimeSpan __normalEndTime;
        private bool __isHoliday;
        private string __breakCode = "";
        public DailyWS()
        {
        }
        public string DailyWorkscheduleGrouping
        {
            get
            {
                return __dailyWorkscheduleGrouping;
            }
            set
            {
                __dailyWorkscheduleGrouping = value;
            }
        }
        public string DailyWorkscheduleCode
        {
            get
            {
                return this.__isHoliday?"HOL":__dailyWorkscheduleCode;
            }
            set
            {
                __dailyWorkscheduleCode = value;
            }
        }
        public string WorkscheduleClass
        {
            get
            {
                if (__workscheduleClass==null)
                    throw new ESS.EMPLOYEE.EXCEPTION.DailyWSException("NOT_FOUND_WORKSCHEDULECLASS");
                else
                    return __workscheduleClass;
            }
            set
            {
                __workscheduleClass = value;
            }
        }
        public DateTime BeginDate
        {
            get
            {
                return __beginDate;
            }
            set
            {
                __beginDate = value;
            }
        }
        public DateTime EndDate
        {
            get
            {
                return __endDate;
            }
            set
            {
                __endDate = value;
            }
        }
        public decimal WorkHours
        {
            get
            {
                return __workHours;
            }
            set
            {
                __workHours = value;
            }
        }
        public TimeSpan WorkBeginTime
        {
            get
            {
                return __beginTime;
            }
            set
            {
                __beginTime = value;
            }
        }
        public long WorkBeginTimeTicks
        {
            get { return __beginTime.Ticks; }
        }
        public TimeSpan WorkEndTime
        {
            get
            {
                return __endTime;
            }
            set
            {
                __endTime = value;
            }
        }
        public long WorkEndTimeTicks
        {
            get { return __endTime.Ticks; }
        }
        public TimeSpan NormalBeginTime
        {
            get
            {
                return __normalBeginTime;
            }
            set
            {
                __normalBeginTime = value;
            }
        }
        public TimeSpan NormalEndTime
        {
            get
            {
                return __normalEndTime;
            }
            set
            {
                __normalEndTime = value;
            }
        }

        public string BreakCode
        {
            get
            {
                return __breakCode;
            }
            set
            {
                __breakCode = value;
            }
        }

        public bool IsDayOff
        {
            get
            {
                return WorkHours == 0.00m;
            }
        }

        public bool IsHoliday
        {
            get
            {
                return __isHoliday;
            }
            set
            {
                __isHoliday = value;
            }
        }

        public bool IsDayOffOrHoliday
        {
            get
            {
                return this.IsDayOff || this.IsHoliday;
            }
        }


        public bool IsAttendance0192
        {
            get { return _IsAttendance0192; }
            set
            {
                _IsAttendance0192 = value;
            }
        }
        

        public static bool IsNormOrTNorm1(string ValuationClass)
        {
            return ",1,2,".IndexOf(ValuationClass.Trim()) > -1;
        }


        public static DailyWS GetDailyWorkschedule(string DailyGroup, string WSCode, DateTime CheckDate)
        {
            if (WSCode == null)
            {
                throw new Exception("WSCode can't be null.May be check workschedule for this employee.");
            }
            return ESS.EMPLOYEE.ServiceManager.EmployeeConfig.GetDailyWorkschedule(DailyGroup, WSCode, CheckDate);
        }

        public static List<DailyWS> GetAll(string Mode, string Profile)
        {
            return ESS.EMPLOYEE.ServiceManager.GetMirrorConfig(Mode).GetDailyWorkscheduleList(Profile);
        }

        public static void SaveTo(List<DailyWS> list, string Mode,string Profile)
        {
            ESS.EMPLOYEE.ServiceManager.GetMirrorConfig(Mode).SaveDailyWorkscheduleList(list, Profile);
        }

    }
}
