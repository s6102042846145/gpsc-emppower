using System;
using ESS.EMPLOYEE.ABSTRACT;

namespace ESS.EMPLOYEE.CONFIG.TM
{
    [Serializable()]
    public class CARDSETTING : AbstractInfoType
    {
        //#region " Members "

        //private string _employeeID_card;
        //private DateTime _beginDate_card;
        //private DateTime _endDate_card;
        //private string _cardNo;
        //private string _location;

        //#endregion " Members "

        public string EmployeeID_Card { get; set; }
        public DateTime BeginDate_Card { get; set; }
        public DateTime EndDate_Card { get; set; }
        public string CardNo { get; set; }
        public string Location { get; set; }
        public override string InfoType
        {
            get { return "0001"; }
        }

    }
}