using System;
using ESS.UTILITY.EXTENSION;

namespace ESS.EMPLOYEE.CONFIG.TM
{
    [Serializable()]
    public class WFRuleSetting : AbstractObject
    {
        //private string __wfrule = "";
        //private TimeSpan __normalBeginTime = TimeSpan.MinValue;
        //private TimeSpan __normalEndTime = TimeSpan.MinValue;
        //private TimeSpan __cutoffTime = TimeSpan.MinValue;

        public WFRuleSetting()
        {
        }

        public string WFRule { get; set; }

        public TimeSpan NormalBeginTime { get; set; }
        public TimeSpan NormalEndTime { get; set; }

        public TimeSpan CutoffTime { get; set; }

        //public static WFRuleSetting GetWFRuleSetting(string WFRule)
        //{
        //    return EmployeeManagement.CreateInstance("").GetWFRuleSetting(WFRule);
        //}
    }
}