using System;
using System.ComponentModel;
using System.Collections.Generic;
using ESS.UTILITY.EXTENSION;
using ESS.WORKFLOW;

namespace ESS.EMPLOYEE.CONFIG.TM
{

    public class Substitution : AbstractObject
    {
        public Substitution()
        {
        }
        #region MultiCompany  Framework
        public string CompanyCode { get; set; }
        public static string ModuleID
        {
            get
            {
                return "ESS.EMPLOYEE";
            }
        }
        public static Substitution CreateInstance(string oCompanyCode)
        {
            //if (!Cache.ContainsKey(oCompanyCode))
            //{
            //    Cache[oCompanyCode] = new ServiceManager()
            //    {
            //        CompanyCode = oCompanyCode
            //        ,
            //        Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID)
            //    };
            //}
            //return Cache[oCompanyCode];
            Substitution oSubstitution = new Substitution()
            {
                CompanyCode = oCompanyCode
            };
            return oSubstitution;
        }
        #endregion MultiCompany  Framework


        public bool IsDraft { get; set; }

        public string SubDWSCodeOld { get; set; }

        public string SubDWSCodeNew { get; set; }

        public string EmpDWSCodeNew { get; set; }

        public string EmpDWSCodeOld { get; set; }

        public bool IsOverride { get; set; }

        public string EmployeeID { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public DateTime SubstituteBeginDate { get; set; }

        public DateTime SubstituteEndDate { get; set; }

        public TimeSpan SubstituteBeginTime { get; set; }

        public TimeSpan SubstituteEndTime { get; set; }

        public string DWSGroup { get; set; }

        public string DWSCode { get; set; }

        public string EmpSubGroupSetting { get; set; }

        public string HolidayCalendar { get; set; }
        public string EmpSubAreaSetting { get; set; }

        public string Status { get; set; }
        public string WSRule { get; set; }

        public string Substitute { get; set; }
        public string RequestNo { get; set; }
        
        public string WorkingTimeBefore { get; set; }
        public string WorkingTimeAfter { get; set; }

        public string DWSCodeView
        {
            get
            {
                if (DWSCode == "" && WSRule != "")
                {                
                    MonthlyWS temp = EMPLOYEE.ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).EmployeeConfig.GetMonthlyWorkschedule(EmpSubGroupSetting, EmpSubAreaSetting, HolidayCalendar, WSRule, BeginDate.Year, BeginDate.Month);
                    if (temp != null)
                    {
                        return temp.GetWSCode(BeginDate.Day, true);
                    }
                    else
                    {
                        return DWSCode;
                    }
                }
                else
                {
                    return DWSCode;
                }
            }
        }
        public string NameSubstitute { get; set; }
        public string WorkingEmpDWSCodeOld { get; set; }
        public string WorkingEmpDWSCodeNew { get; set; }
        public string WorkingSubDWSCodeOld { get; set; }
        public string WorkingSubDWSCodeNew { get; set; }

        public List<Substitution> GetSubstitutionList(string EmployeeID, int Year, int Month)
        {
            return GetSubstitutionList(EmployeeID, Year, Month, true);
        }

        public List<Substitution> GetSubstitutionList(string EmployeeID, int Year, int Month, bool OnlyEffective)
        {
            DateTime date1, date2;
            date1 = new DateTime(Year, Month, 1);
            date2 = date1.AddMonths(1).AddDays(-1);
            return GetSubstitutionList(EmployeeID, date1, date2, OnlyEffective);
        }

        public List<Substitution> GetSubstitutionList(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            return GetSubstitutionList(EmployeeID, BeginDate, EndDate, true);
        }

        public List<Substitution> GetSubstitutionList(string EmployeeID, DateTime BeginDate, DateTime EndDate, bool OnlyEffective)
        {
            List<Substitution> buffer = new List<Substitution>();
            Dictionary<DateTime, Substitution> oDictSubstition = new Dictionary<DateTime, Substitution>();
            if (!OnlyEffective)
            {
                //buffer.AddRange(EMPLOYEE.ServiceManager.EmployeeService.GetInfotype2003(EmployeeID, BeginDate, EndDate));
                buffer.AddRange(ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetInfotype2003(EmployeeID, BeginDate, EndDate));
            }
            //buffer.AddRange(EMPLOYEE.ServiceManager.ERPEmployeeService.GetInfotype2003(EmployeeID, BeginDate, EndDate));
            //buffer.AddRange(ServiceManager.CreateInstance(CompanyCode).ERPEmployeeService.GetInfotype2003(EmployeeID, BeginDate, EndDate));

            foreach (Substitution item in buffer)
            {
                if (!oDictSubstition.ContainsKey(item.BeginDate.Date))  //Modify by Morakot.t 2017-12-22
                    oDictSubstition.Add(item.BeginDate.Date, item);
            }

            //List<Substitution> oSubstitutionListFromDB = EMPLOYEE.ServiceManager.EmployeeService.GetInfotype2003(EmployeeID, BeginDate, EndDate);

            List<Substitution> oSubstitutionListFromDB = ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetInfotype2003(EmployeeID, BeginDate, EndDate);
            foreach (Substitution oSubstitution in oSubstitutionListFromDB)
            {
                if (oDictSubstition.ContainsKey(oSubstitution.BeginDate.Date) && oDictSubstition[oSubstitution.BeginDate.Date].Status.Equals(oSubstitution.Status))
                {
                    buffer.Remove(oDictSubstition[oSubstitution.BeginDate.Date]);
                }
                buffer.Add(oSubstitution);
            }
            oDictSubstition.Clear();
            oDictSubstition = null;
            return buffer;
        }

        public List<Substitution> GetSubstitutionListLog(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            List<Substitution> buffer = new List<Substitution>();
            //buffer.AddRange(EMPLOYEE.ServiceManager.EmployeeService.GetInfotype2003_Log(EmployeeID, BeginDate, EndDate));
            buffer.AddRange(ServiceManager.CreateInstance(CompanyCode).EmployeeService.GetInfotype2003_Log(EmployeeID, BeginDate, EndDate));
            return buffer;
        }
    }
}