using System;
using System.Collections.Generic;

namespace ESS.EMPLOYEE.CONFIG.TM
{
    [Serializable()]
    public class PersonalSubGroupSetting
    {
        public PersonalSubGroupSetting()
        {
        }

        public string EmpGroup { get; set; }
        public string EmpSubGroup { get; set; }
        public string WorkScheduleGrouping { get; set; }
        public string TimeQuotaTypeGrouping { get; set; }
        public override int GetHashCode()
        {
            string cCode = string.Format("PERSONALSUBGROUPSETTING_{0}_{1}", EmpGroup, EmpSubGroup);
            return cCode.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return this.GetHashCode() == obj.GetHashCode();
        }


    }
}