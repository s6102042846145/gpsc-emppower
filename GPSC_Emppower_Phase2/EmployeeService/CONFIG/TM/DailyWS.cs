using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ESS.UTILITY.EXTENSION;

namespace ESS.EMPLOYEE.CONFIG.TM
{
    public class DailyWS : AbstractObject
    {
        public DailyWS()
        {
        }

        private string __dailyWorkscheduleGrouping;
        private string __dailyWorkscheduleCode;
        private string __workscheduleClass;
        private DateTime __beginDate;
        private DateTime __endDate;
        private decimal __workHours = 0;
        private TimeSpan __workBeginTime = TimeSpan.Zero;
        private TimeSpan __workEndTime = TimeSpan.Zero;
        private TimeSpan __normalBeginTime = TimeSpan.Zero;
        private TimeSpan __normalEndTime = TimeSpan.Zero;
        private string __breakCode;
        private bool __isHoliday;
        private bool __isAttendance0192;

        public string DailyWorkscheduleGrouping
        {
            get { return __dailyWorkscheduleGrouping; }
            set { __dailyWorkscheduleGrouping = value; }
        }

        public string DailyWorkscheduleCode
        {
            get
            {
                return this.IsHoliday ? "HOL" : __dailyWorkscheduleCode;
            }
            set
            {
                __dailyWorkscheduleCode = value;
            }
        }

        public string WorkscheduleClass
        {
            get { return __workscheduleClass; }
            set { __workscheduleClass = value; }
        }

        public DateTime BeginDate
        {
            get { return __beginDate; }
            set { __beginDate = value; }
        }

        public DateTime EndDate
        {
            get { return __endDate; }
            set { __endDate = value; }
        }
        
        public decimal WorkHours
        {
            get { return __workHours; }
            set { __workHours = value; }
        }
        
        public TimeSpan WorkBeginTime
        {
            get { return __workBeginTime; }
            set { __workBeginTime = value; }
        }

        public long WorkBeginTimeTicks
        {
            get { return WorkBeginTime.Ticks; }
        }

        public TimeSpan WorkEndTime
        {
            get { return __workEndTime; }
            set { __workEndTime = value; }
        }

        public long WorkEndTimeTicks
        {
            get { return WorkEndTime.Ticks; }
        }

        public TimeSpan NormalBeginTime
        {
            get { return __normalBeginTime; }
            set { __normalBeginTime = value; }
        }

        public TimeSpan NormalEndTime
        {
            get { return __normalEndTime; }
            set { __normalEndTime = value; }
        }

        public string BreakCode
        {
            get { return __breakCode; }
            set { __breakCode = value; }
        }


        public bool IsDayOff
        {
            get
            {
                return WorkHours == 0.00m;
            }
        }

        public bool IsHoliday
        {
            get { return __isHoliday; }
            set { __isHoliday = value; }
        }

        public bool IsDayOffOrHoliday
        {
            get
            {
                return this.IsDayOff || this.IsHoliday;
            }
        }

        public bool IsAttendance0192
        {
            get { return __isAttendance0192; }
            set { __isAttendance0192 = value; }
        }

        public bool IsNormOrTNorm1(string ValuationClass)
        {
            return ",1,2,".IndexOf(ValuationClass.Trim()) > -1;
        }

        public static DailyWS GetDailyWorkschedule(string DailyGroup, string WSCode, DateTime CheckDate)
        {
            if (WSCode == null)
            {
                throw new Exception("WSCODE_CANT_BE_NULL");
            }
            return EmployeeManagement.CreateInstance(WORKFLOW.WorkflowPrinciple.CurrentIdentity.CompanyCode).GetDailyWorkschedule(DailyGroup, WSCode, CheckDate);
        }
    }
}