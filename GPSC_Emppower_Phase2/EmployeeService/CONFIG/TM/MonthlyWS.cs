using System;
using System.Collections.Generic;
using System.Reflection;
using ESS.SHAREDATASERVICE;
using ESS.DATA;
using ESS.WORKFLOW;

namespace ESS.EMPLOYEE.CONFIG.TM
{
    [Serializable()]
    public class MonthlyWS : AbstractObject
    {
        #region Constructor

        public MonthlyWS()
        {
        }

        #endregion Constructor

        #region MultiCompany  Framework  
        public string CompanyCode { get; set; }
        //private Dictionary<string, string> Config { get; set; }

        //private static Dictionary<string, MonthlyWS> Cache = new Dictionary<string, MonthlyWS>();

        //private static string ModuleID = "ESS";

        //public string CompanyCode { get; set; }

        //public static MonthlyWS CreateInstance(string oCompanyCode)
        //{
        //    if (!Cache.ContainsKey(oCompanyCode))
        //    {
        //        Cache[oCompanyCode] = new MonthlyWS()
        //        {
        //            CompanyCode = oCompanyCode
        //            ,
        //            Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID)
        //        };
        //    }
        //    return Cache[oCompanyCode];
        //}

        #endregion MultiCompany  Framework

        private Dictionary<int, string> ValuationClassList = new Dictionary<int, string>();

        #region " Headers Properties "

        public string EmpSubGroupForWorkSchedule { get; set; }

        public string PublicHolidayCalendar { get; set; }

        public string EmpSubAreaForWorkSchedule { get; set; }

        public string WorkScheduleRule { get; set; }

        public int WS_Year { get; set; }

        public int WS_Month { get; set; }

        public void SetValuationClassExactly(int EndDay, string ValuationClass)
        {
            if (!ValuationClassList.ContainsKey(EndDay))
            {
                ValuationClassList.Add(EndDay, ValuationClass);
            }
            else
            {
                ValuationClassList[EndDay] = ValuationClass;
            }
        }

        public string GetValuationClassExactly(int EndDay)
        {
            foreach (int key in ValuationClassList.Keys)
            {
                if (key >= EndDay)
                {
                    return ValuationClassList[key];
                }
            }
            return ValuationClass;
        }

        public string ValuationClass { get; set; }
        public string GetWSCode(int dayNo, bool isCode)
        {
            PropertyInfo oProp;
            Type oType = this.GetType();
            string cDayNo = dayNo.ToString("00");
            string cReturn = "";
            if (isCode)
            {
                oProp = oType.GetProperty(string.Format("Day{0}", cDayNo));
            }
            else
            {
                oProp = oType.GetProperty(string.Format("Day{0}_H", cDayNo));
            }
            if (oProp != null)
            {
                cReturn = (string)oProp.GetValue(this, null);
            }
            return cReturn;
        }

        #endregion " Headers Properties "

        #region " Properties "

        public string Day01 { get; set; }

        public string Day01_H { get; set; }

        public string Day02 { get; set; }

        public string Day02_H { get; set; }

        public string Day03 { get; set; }

        public string Day03_H { get; set; }

        public string Day04 { get; set; }

        public string Day04_H { get; set; }

        public string Day05 { get; set; }

        public string Day05_H { get; set; }

        public string Day06 { get; set; }

        public string Day06_H { get; set; }

        public string Day07 { get; set; }

        public string Day07_H { get; set; }

        public string Day08 { get; set; }

        public string Day08_H { get; set; }

        public string Day09 { get; set; }

        public string Day09_H { get; set; }

        public string Day10 { get; set; }

        public string Day10_H { get; set; }

        public string Day11 { get; set; }

        public string Day11_H { get; set; }

        public string Day12 { get; set; }

        public string Day12_H { get; set; }

        public string Day13 { get; set; }

        public string Day13_H { get; set; }

        public string Day14 { get; set; }

        public string Day14_H { get; set; }

        public string Day15 { get; set; }

        public string Day15_H { get; set; }

        public string Day16 { get; set; }

        public string Day16_H { get; set; }

        public string Day17 { get; set; }

        public string Day17_H { get; set; }

        public string Day18 { get; set; }

        public string Day18_H { get; set; }

        public string Day19 { get; set; }

        public string Day19_H { get; set; }

        public string Day20 { get; set; }

        public string Day20_H { get; set; }

        public string Day21 { get; set; }

        public string Day21_H { get; set; }

        public string Day22 { get; set; }

        public string Day22_H { get; set; }

        public string Day23 { get; set; }

        public string Day23_H { get; set; }

        public string Day24 { get; set; }

        public string Day24_H { get; set; }

        public string Day25 { get; set; }

        public string Day25_H { get; set; }

        public string Day26 { get; set; }

        public string Day26_H { get; set; }

        public string Day27 { get; set; }

        public string Day27_H { get; set; }

        public string Day28 { get; set; }

        public string Day28_H { get; set; }

        public string Day29 { get; set; }

        public string Day29_H { get; set; }

        public string Day30 { get; set; }

        public string Day30_H { get; set; }
        public string Day31 { get; set; }
        public string Day31_H { get; set; }

        #endregion " Properties "

        #region Old Code
        public static MonthlyWS GetCalendar(string EmployeeID, int Year, int Month)
        {
            return GetCalendar(EmployeeID, DateTime.Now, Year, Month, ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).CALENDAR_USE_SUBSTITUTE);
        }

        public static MonthlyWS GetCalendar(string EmployeeID, DateTime CheckDate)
        {
            return GetCalendar(EmployeeID, CheckDate, CheckDate.Year, CheckDate.Month, ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).CALENDAR_USE_SUBSTITUTE);
        }

        public static MonthlyWS GetCalendar(string EmployeeID, DateTime CheckDate, bool includeSubstitute)
        {
            return GetCalendar(EmployeeID, CheckDate, CheckDate.Year, CheckDate.Month, includeSubstitute);
        }

        public static MonthlyWS GetCalendar(string EmployeeID, DateTime CheckDate, int Year, int Month, bool includeSubstitute)
        {
            EmployeeData oEmp = new EmployeeData(EmployeeID, CheckDate);

            string cSubGroup = "";
            string cSubArea = "";
            string cHoliday = "";

            if (oEmp.OrgAssignment != null)
            {
                cSubGroup = oEmp.OrgAssignment.SubGroupSetting.WorkScheduleGrouping;
                cSubArea = oEmp.OrgAssignment.SubAreaSetting.WorkScheduleGrouping;
                cHoliday = oEmp.OrgAssignment.SubAreaSetting.HolidayCalendar;
            }

            MonthlyWS oReturn = new MonthlyWS();
            oReturn.WS_Year = Year;
            oReturn.WS_Month = Month;
            oReturn.EmpSubGroupForWorkSchedule = cSubGroup;
            oReturn.EmpSubAreaForWorkSchedule = cSubArea;
            oReturn.PublicHolidayCalendar = cHoliday;

            //ModifiedBy: Ratchatawan W. (2013-04-05)
            List<Substitution> substitutionList = new List<Substitution>();
            if (includeSubstitute)
                substitutionList = Substitution.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetSubstitutionList(EmployeeID, Year, Month, true);
            //Dictionary<int, Substitution> substitutionDict = new Dictionary<int, Substitution>();
            //if (includeSubstitute)
            //{
            //    List<Substitution> substitutionList = Substitution.GetSubstitutionList(EmployeeID, Year, Month, true);

            //    foreach (Substitution item in substitutionList.FindAll(delegate(Substitution oSub) { return oSub.Status == "COMPLETED"; }))
            //    {
            //        for (DateTime rundate = item.BeginDate; rundate <= item.EndDate; rundate = rundate.AddDays(1))
            //        {
            //            if (rundate.Year == Year && rundate.Month == Month)
            //            {
            //                substitutionDict.Add(rundate.Day, item);
            //            }
            //        }
            //    }
            //}
            bool lFirst = true;
            foreach (INFOTYPE0007 item in oEmp.GetWorkSchedule(Year, Month))
            {
                DateTime dTemp = new DateTime(Year, Month, 1);
                dTemp = dTemp.AddMonths(1).AddDays(-1);
                if (dTemp < item.BeginDate)
                    break;
                EmployeeData oEmp1 = new EmployeeData(EmployeeID, item.BeginDate);
                //MonthlyWS temp = EMPLOYEE.ServiceManager.EmployeeConfig.GetMonthlyWorkschedule(oEmp1.OrgAssignment.SubGroupSetting.WorkScheduleGrouping, oEmp1.OrgAssignment.SubAreaSetting.WorkScheduleGrouping, oEmp1.OrgAssignment.SubAreaSetting.HolidayCalendar, item.WFRule, Year, Month);

                MonthlyWS temp = ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).EmployeeConfig.GetMonthlyWorkschedule(oEmp1.OrgAssignment.SubGroupSetting.WorkScheduleGrouping, oEmp1.OrgAssignment.SubAreaSetting.WorkScheduleGrouping, oEmp1.OrgAssignment.SubAreaSetting.HolidayCalendar, item.WFRule, Year, Month);
                oReturn.WorkScheduleRule = temp.WorkScheduleRule;
                if (lFirst)
                {
                    oReturn.ValuationClass = temp.ValuationClass;
                }
                lFirst = false;
                int nStart = 1;
                if (item.BeginDate.Year == Year && item.BeginDate.Month == Month)
                {
                    nStart = item.BeginDate.Day;
                }
                int nEnd = dTemp.Day;
                if (item.EndDate.Year == Year && item.EndDate.Month == Month)
                {
                    nEnd = item.EndDate.Day;
                }
                oReturn.SetValuationClassExactly(nEnd, temp.ValuationClass);
                PropertyInfo oProp;
                Type oType = oReturn.GetType();
                for (int index = nStart; index <= nEnd; index++)
                {
                    string cDayNo = index.ToString("00");
                    string cDWSCode;
                    oProp = oType.GetProperty(string.Format("Day{0}", cDayNo));

                    //Modified By: Ratchatawan W. (2013-4-5)
                    if (substitutionList.Exists(delegate (Substitution oSub) { return oSub.Status == "COMPLETED" && oSub.BeginDate.Day == index && oSub.BeginDate.Month == Month && oSub.BeginDate.Year == Year; }))
                    {
                        //Edit by Koissares 20161007
                        Substitution oSubstitution = substitutionList.Find(delegate (Substitution oSub) { return oSub.Status == "COMPLETED" && oSub.BeginDate.Day == index && oSub.BeginDate.Month == Month && oSub.BeginDate.Year == Year; });
                        //Substitution oSubstitution = substitutionList.Find(delegate(Substitution oSub) { return oSub.BeginDate.Day == index; });
                        if (String.IsNullOrEmpty(oSubstitution.EmpDWSCodeNew))
                        {
                            cDWSCode = oSubstitution.SubstituteBeginTime.ToString() + "-" + oSubstitution.SubstituteEndTime.ToString();
                        }
                        else
                        {
                            if (oEmp.EmployeeID.Trim() == oSubstitution.EmployeeID.Trim())
                                cDWSCode = oSubstitution.EmpDWSCodeNew;
                            else
                                cDWSCode = oSubstitution.SubDWSCodeNew;
                        }
                    }
                    else
                    {
                        cDWSCode = (string)oProp.GetValue(temp, null);
                    }

                    //if (substitutionDict.ContainsKey(index))
                    //{
                    //    //CommentBy: Ratchatawan W. (2012-11-16)
                    //    //cDWSCode = substitutionDict[index].DWSCodeView;
                    //    if(oEmp.EmployeeID == substitutionDict[index].EmployeeID)
                    //        cDWSCode = substitutionDict[index].EmpDWSCodeNew;
                    //    else
                    //        cDWSCode = substitutionDict[index].SubDWSCodeNew;
                    //}
                    //else
                    //{
                    //    cDWSCode = (string)oProp.GetValue(temp, null);
                    //}

                    oProp.SetValue(oReturn, cDWSCode, null);

                    oProp = oType.GetProperty(string.Format("Day{0}_H", cDayNo));
                    oProp.SetValue(oReturn, oProp.GetValue(temp, null), null);
                }
            }
            return oReturn;
        }

        //public MonthlyWS GetSimulateCalendar(PersonalSubAreaSetting SubAreaSetting, PersonalSubGroupSetting SubGroupSetting, int Year, int Month, string WFRule)
        //{
        //    string cSubGroup = SubGroupSetting.WorkScheduleGrouping;
        //    string cSubArea = SubAreaSetting.WorkScheduleGrouping;
        //    string cHoliday = SubAreaSetting.HolidayCalendar;
        //    MonthlyWS oReturn = new MonthlyWS();
        //    oReturn.WS_Year = Year;
        //    oReturn.WS_Month = Month;
        //    oReturn.EmpSubGroupForWorkSchedule = cSubGroup;
        //    oReturn.EmpSubAreaForWorkSchedule = cSubArea;
        //    oReturn.PublicHolidayCalendar = cHoliday;

        //    bool lFirst = true;
        //    DateTime dTemp = new DateTime(Year, Month, 1);
        //    //MonthlyWS temp = EMPLOYEE.ServiceManager.EmployeeConfig.GetMonthlyWorkschedule(SubGroupSetting.WorkScheduleGrouping, SubAreaSetting.WorkScheduleGrouping, SubAreaSetting.HolidayCalendar, WFRule, Year, Month);

        //    MonthlyWS temp = ServiceManager.CreateInstance(CompanyCode).EmployeeConfig.GetMonthlyWorkschedule(SubGroupSetting.WorkScheduleGrouping, SubAreaSetting.WorkScheduleGrouping, SubAreaSetting.HolidayCalendar, WFRule, Year, Month);

        //    oReturn.WorkScheduleRule = temp.WorkScheduleRule;
        //    if (lFirst)
        //    {
        //        oReturn.ValuationClass = temp.ValuationClass;
        //    }
        //    lFirst = false;
        //    int nStart = dTemp.Day;
        //    int nEnd = dTemp.AddMonths(1).AddDays(-1).Day;
        //    oReturn.SetValuationClassExactly(nEnd, temp.ValuationClass);
        //    PropertyInfo oProp;
        //    Type oType = oReturn.GetType();
        //    for (int index = nStart; index <= nEnd; index++)
        //    {
        //        string cDayNo = index.ToString("00");
        //        string cDWSCode;
        //        oProp = oType.GetProperty(string.Format("Day{0}", cDayNo));

        //        cDWSCode = (string)oProp.GetValue(temp, null);

        //        oProp.SetValue(oReturn, cDWSCode, null);

        //        oProp = oType.GetProperty(string.Format("Day{0}_H", cDayNo));
        //        oProp.SetValue(oReturn, oProp.GetValue(temp, null), null);
        //    }

        //    return oReturn;
        //}


        //public static List<MonthlyWS> SimulateMonthlyWorkschedule(string EmpSubGroupForWorkSchedule, string PublicHolidayCalendar, string EmpSubAreaForWorkSchedule, int Year, int Month, Dictionary<string, string> DayOption)
        //{
        //    return ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).EmployeeConfig.SimulateMonthlyWorkschedule(EmpSubGroupForWorkSchedule, PublicHolidayCalendar, EmpSubAreaForWorkSchedule, Year, Month, DayOption);
        //}
        public static MonthlyWS GetCalendar(string EmployeeID, int Year, int Month, string CompCode)
        {
            return GetCalendar(EmployeeID, DateTime.Now, Year, Month, ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).CALENDAR_USE_SUBSTITUTE, CompCode);
        }

        public static MonthlyWS GetCalendar(string EmployeeID, DateTime CheckDate, string CompCode)
        {
            return GetCalendar(EmployeeID, CheckDate, CheckDate.Year, CheckDate.Month, ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).CALENDAR_USE_SUBSTITUTE, CompCode);
        }

        public static MonthlyWS GetCalendar(string EmployeeID, DateTime CheckDate, int Year, int Month, bool includeSubstitute, string CompCode)
        {
            EmployeeData oEmp = new EmployeeData(EmployeeID, CheckDate) { CompanyCode = CompCode };

            string cSubGroup = "";
            string cSubArea = "";
            string cHoliday = "";

            if (oEmp.OrgAssignment != null)
            {
                cSubGroup = oEmp.OrgAssignment.SubGroupSetting.WorkScheduleGrouping;
                cSubArea = oEmp.OrgAssignment.SubAreaSetting.WorkScheduleGrouping;
                cHoliday = oEmp.OrgAssignment.SubAreaSetting.HolidayCalendar;
            }

            MonthlyWS oReturn = new MonthlyWS();
            oReturn.WS_Year = Year;
            oReturn.WS_Month = Month;
            oReturn.EmpSubGroupForWorkSchedule = cSubGroup;
            oReturn.EmpSubAreaForWorkSchedule = cSubArea;
            oReturn.PublicHolidayCalendar = cHoliday;

            //ModifiedBy: Ratchatawan W. (2013-04-05)
            List<Substitution> substitutionList = new List<Substitution>();
            if (includeSubstitute)
                substitutionList = Substitution.CreateInstance(CompCode).GetSubstitutionList(EmployeeID, Year, Month, true);
            //Dictionary<int, Substitution> substitutionDict = new Dictionary<int, Substitution>();
            //if (includeSubstitute)
            //{
            //    List<Substitution> substitutionList = Substitution.GetSubstitutionList(EmployeeID, Year, Month, true);

            //    foreach (Substitution item in substitutionList.FindAll(delegate(Substitution oSub) { return oSub.Status == "COMPLETED"; }))
            //    {
            //        for (DateTime rundate = item.BeginDate; rundate <= item.EndDate; rundate = rundate.AddDays(1))
            //        {
            //            if (rundate.Year == Year && rundate.Month == Month)
            //            {
            //                substitutionDict.Add(rundate.Day, item);
            //            }
            //        }
            //    }
            //}
            bool lFirst = true;
            foreach (INFOTYPE0007 item in oEmp.GetWorkSchedule(Year, Month))
            {
                DateTime dTemp = new DateTime(Year, Month, 1);
                dTemp = dTemp.AddMonths(1).AddDays(-1);
                if (dTemp < item.BeginDate)
                    break;
                EmployeeData oEmp1 = new EmployeeData(EmployeeID, item.BeginDate) { CompanyCode = CompCode };
                //MonthlyWS temp = EMPLOYEE.ServiceManager.EmployeeConfig.GetMonthlyWorkschedule(oEmp1.OrgAssignment.SubGroupSetting.WorkScheduleGrouping, oEmp1.OrgAssignment.SubAreaSetting.WorkScheduleGrouping, oEmp1.OrgAssignment.SubAreaSetting.HolidayCalendar, item.WFRule, Year, Month);

                MonthlyWS temp = ServiceManager.CreateInstance(CompCode).EmployeeConfig.GetMonthlyWorkschedule(oEmp1.OrgAssignment.SubGroupSetting.WorkScheduleGrouping, oEmp1.OrgAssignment.SubAreaSetting.WorkScheduleGrouping, oEmp1.OrgAssignment.SubAreaSetting.HolidayCalendar, item.WFRule, Year, Month);
                oReturn.WorkScheduleRule = temp.WorkScheduleRule;
                if (lFirst)
                {
                    oReturn.ValuationClass = temp.ValuationClass;
                }
                lFirst = false;
                int nStart = 1;
                if (item.BeginDate.Year == Year && item.BeginDate.Month == Month)
                {
                    nStart = item.BeginDate.Day;
                }
                int nEnd = dTemp.Day;
                if (item.EndDate.Year == Year && item.EndDate.Month == Month)
                {
                    nEnd = item.EndDate.Day;
                }
                oReturn.SetValuationClassExactly(nEnd, temp.ValuationClass);
                PropertyInfo oProp;
                Type oType = oReturn.GetType();
                for (int index = nStart; index <= nEnd; index++)
                {
                    string cDayNo = index.ToString("00");
                    string cDWSCode;
                    oProp = oType.GetProperty(string.Format("Day{0}", cDayNo));

                    //Modified By: Ratchatawan W. (2013-4-5)
                    if (substitutionList.Exists(delegate (Substitution oSub) { return oSub.Status == "COMPLETED" && oSub.BeginDate.Day == index && oSub.BeginDate.Month == Month && oSub.BeginDate.Year == Year; }))
                    {
                        //Edit by Koissares 20161007
                        Substitution oSubstitution = substitutionList.Find(delegate (Substitution oSub) { return oSub.Status == "COMPLETED" && oSub.BeginDate.Day == index && oSub.BeginDate.Month == Month && oSub.BeginDate.Year == Year; });
                        //Substitution oSubstitution = substitutionList.Find(delegate(Substitution oSub) { return oSub.BeginDate.Day == index; });
                        if (String.IsNullOrEmpty(oSubstitution.EmpDWSCodeNew))
                        {
                            cDWSCode = oSubstitution.SubstituteBeginTime.ToString() + "-" + oSubstitution.SubstituteEndTime.ToString();
                        }
                        else
                        {
                            if (oEmp.EmployeeID.Trim() == oSubstitution.EmployeeID.Trim())
                                cDWSCode = oSubstitution.EmpDWSCodeNew;
                            else
                                cDWSCode = oSubstitution.SubDWSCodeNew;
                        }
                    }
                    else
                    {
                        cDWSCode = (string)oProp.GetValue(temp, null);
                    }

                    //if (substitutionDict.ContainsKey(index))
                    //{
                    //    //CommentBy: Ratchatawan W. (2012-11-16)
                    //    //cDWSCode = substitutionDict[index].DWSCodeView;
                    //    if(oEmp.EmployeeID == substitutionDict[index].EmployeeID)
                    //        cDWSCode = substitutionDict[index].EmpDWSCodeNew;
                    //    else
                    //        cDWSCode = substitutionDict[index].SubDWSCodeNew;
                    //}
                    //else
                    //{
                    //    cDWSCode = (string)oProp.GetValue(temp, null);
                    //}

                    oProp.SetValue(oReturn, cDWSCode, null);

                    oProp = oType.GetProperty(string.Format("Day{0}_H", cDayNo));
                    oProp.SetValue(oReturn, oProp.GetValue(temp, null), null);
                }
            }
            return oReturn;
        }
        #endregion

    }
}