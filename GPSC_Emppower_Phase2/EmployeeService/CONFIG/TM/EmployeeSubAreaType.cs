namespace ESS.EMPLOYEE.CONFIG.TM
{
    public enum EmployeeSubAreaType
    {
        Flex = 0,
        Norm = 1,
        Shift8 = 2,
        Shift12 = 3,
        Shift = 4,
        Turn = 5
    }

    public enum EmployeeFilter
    {
        Flex = 0,
        FlexAndOnlyManager = 1,
        Norm = 2,
        NormAndOnlyManager = 3,
        Shift8 = 4,
        Shift8AndOnlyManager = 5,
        Shift12 = 6,
        Shift12AndOnlyManager = 7,
        NormFlex = 8,
        NormFlexAndOnlyManager = 9,
        Shift8Shift12 = 10,
        Shift8Shift12AndOnlyManager = 11,
        NormFlexAndShift8Shift12 = 12,
        NormFlexAndShift8Shift12AndOnlyManager = 13
    }
}