using System;
using ESS.UTILITY.EXTENSION;

namespace ESS.EMPLOYEE.CONFIG.TM
{
    [Serializable()]
    public class DateSplit : AbstractObject
    {
        private TimeSpan __beginTime;
        private TimeSpan __endTime;
        private bool __isPrevDay;

        public DateSplit()
        {
        }

        public TimeSpan BeginTime
        {
            get
            {
                return __beginTime;
            }
            set
            {
                __beginTime = value;
            }
        }

        public TimeSpan EndTime
        {
            get
            {
                return __endTime;
            }
            set
            {
                __endTime = value;
            }
        }

        public bool IsPrevDay
        {
            get
            {
                return __isPrevDay;
            }
            set
            {
                __isPrevDay = value;
            }
        }
    }
}