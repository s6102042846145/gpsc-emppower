using System;
using System.Collections.Generic;
using System.Text;
using ESS.DATA;

namespace ESS.EMPLOYEE
{
    public class TicketMaster : AbstractObject
    {
        private string __employeeID = "";
        private string __ticketClass = "";
        private string __ticketID = "";
        private DateTime __ticketExpired = DateTime.MinValue;
        public TicketMaster()
        { 
        }
        public string EmployeeID
        {
            get
            {
                return __employeeID;
            }
            set
            {
                __employeeID = value;
            }
        }
        public string TicketClass
        {
            get
            {
                return __ticketClass;
            }
            set
            {
                __ticketClass = value;
            }
        }
        public string TicketID
        {
            get
            {
                return __ticketID;
            }
            set
            {
                __ticketID = value;
            }
        }
        public DateTime TicketExpired
        {
            get
            {
                return __ticketExpired;
            }
            set
            {
                __ticketExpired = value;
            }
        }
        public List<TicketMaster> GetActiveTicket(string TicketClass)
        {
            return EMPLOYEE.ServiceManager.EmployeeService.GetActiveTicket(TicketClass);
        }
    }
}
