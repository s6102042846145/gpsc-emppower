using System;
using System.Collections.Generic;
using System.Text;
using ESS.EMPLOYEE.CONFIG;
using ESS.EMPLOYEE.OM;

using System.Configuration;
using System.Reflection;

namespace ESS.EMPLOYEE
{
    public class OMManagement
    {
        private static Configuration __config;

        private static Configuration config
        {
            get
            {
                if (__config == null)
                {
                    __config = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().CodeBase.Replace("file:///", "").Replace("/", "\\"));
                }
                return __config;
            }
        }

        public static List<OrganizationWithLevel> GetAllOrganizationWithLevel(string Orgunit, string LanguageCode)
        {
            return ESS.EMPLOYEE.ServiceManager.OMService.GetAllOrganizationWithLevel(Orgunit, LanguageCode);
        }

        //Add By Kiattiwat K. (10-02-2012)
        public static INFOTYPE1000 GetOrgParent(string OrgID, DateTime CheckDate, string LanguageCode)
        {
            return ESS.EMPLOYEE.ServiceManager.OMService.GetOrgParent(OrgID, CheckDate);
        }
        public static INFOTYPE1010 GetLevelByOrg(string OrgID, DateTime CheckDate)
        {
            return EMPLOYEE.ServiceManager.OMService.GetLevelByOrg(OrgID, CheckDate);
        }

    }
}
