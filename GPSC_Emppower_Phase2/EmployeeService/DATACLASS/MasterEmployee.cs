﻿using ESS.UTILITY.EXTENSION;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.EMPLOYEE.DATACLASS
{
    public class EmployeeINFOTYPE0001 : AbstractObject
    {
        public string EmployeeID { get; set; }
        public string SubType { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public string CompanyCode { get; set; }
        public string Name { get; set; }
        public string Area { get; set; }
        public string SubArea { get; set; }
        public string EmpGroup { get; set; }
        public string EmpSubGroup { get; set; }
        public string OrgUnit { get; set; }
        public string Position { get; set; }
        public string AdminGroup { get; set; }
        public string CostCenter { get; set; }
        public string BusinessArea { get; set; }
        public string PayrollArea { get; set; }
    }

    public class EmployeeAllActive : AbstractObject
    {
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string OrgUnit { get; set; }
        public string OrgUnitName { get; set; }
        public string Position { get; set; }
        public string PositionName { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
    }

    
    public class DataEmployeeInfotype0002 : AbstractObject
    {
        public string   EmployeeID { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public string TitleID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public string Fullname { get; set; }
    }

    public class NotifyBirthDay : AbstractObject
    {
        public int BirthDayCardID { get; set; }
        public string EmployeeID { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }
        public DateTime NotifyDate { get; set; }
    }

    public class ResponeBirthDayCard
    {
        public string EmployeeId { get; set; }
        public bool FlagBirthDay { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }
        public string FullName { get; set; }
    }
}
