using System;
using System.Collections.Generic;
using System.Text;
using ESS.EMPLOYEE.CONFIG;
using System.Data;
using ESS.EMPLOYEE.OM;
using System.Configuration;
using System.Reflection;

namespace ESS.EMPLOYEE
{
    public class EmployeeManagement
    {
        private static Configuration __config;
        private static Configuration config
        {
            get
            {
                if (__config == null)
                {
                    __config = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().CodeBase.Replace("file:///", "").Replace("/", "\\"));
                }
                return __config;
            }
        }

        public static string ROOT_ORGANIZATION
        {
            get
            {
                if (config == null || config.AppSettings.Settings["ROOT_ORGANIZATION"] == null)
                {
                    return string.Empty;
                }
                else
                {
                    return config.AppSettings.Settings["ROOT_ORGANIZATION"].Value;
                }
            }
        }

        public static void SaveCommunicationData(List<INFOTYPE0105> commData)
        {

        }
        public static void UpdateCommunicationData(string EmployeeID)
        { 
            List<INFOTYPE0105> communicationList;
            communicationList = EMPLOYEE.ServiceManager.ERPEmployeeService.GetInfotype0105List(EmployeeID);
            EMPLOYEE.ServiceManager.EmployeeService.SaveInfotype0105(EmployeeID, EmployeeID, communicationList, "WORKFLOW");
            List<WorkPlaceCommunication> workplaceList = new List<WorkPlaceCommunication>();
            workplaceList = EMPLOYEE.ServiceManager.ERPEmployeeService.GetWorkplaceData(EmployeeID);
            EMPLOYEE.ServiceManager.EmployeeService.SaveWorkPlaceData(EmployeeID, workplaceList, "WORKFLOW");
        }
        public static void CopyAllWorkPlaceData()
        {
            List<WorkPlaceCommunication> workplaceList;
            workplaceList = EMPLOYEE.ServiceManager.ERPEmployeeService.GetWorkplaceData();
            EMPLOYEE.ServiceManager.EmployeeService.SaveWorkPlaceData(workplaceList, "WORKFLOW");
        }
        public static List<WorkPlaceCommunication> GetAllWorkPlaceData()
        {
            List<WorkPlaceCommunication> workplaceList;
            workplaceList = EMPLOYEE.ServiceManager.EmployeeService.GetWorkplaceData();
            return workplaceList;
        }

        public static bool HavePinCode(string EmployeeID)
        {
            return EMPLOYEE.ServiceManager.EmployeeService.HavePinCode(EmployeeID);
        }
        public static bool VerifyPinCode(string Employeeid, string pincode)
        {
            return EMPLOYEE.ServiceManager.EmployeeService.VerifyPinCode(Employeeid, pincode);
        }
        public static void RequestNEWPin(string Employeeid)
        {
            EMPLOYEE.ServiceManager.EmployeeService.RequestNewPIN(Employeeid);
        }
        public static void CreateNEWPin(string EmployeeID, string TicketID, string NewPINcode)
        {
            EMPLOYEE.ServiceManager.EmployeeService.CreateNewPIN(EmployeeID, TicketID, NewPINcode);
        }
        public static bool ValidateTicket(string EmployeeID, string TicketClass, string TicketID)
        {
            return EMPLOYEE.ServiceManager.EmployeeService.ValidateTicket(EmployeeID, TicketClass, TicketID);
        }
        public static void ChangePinCode(string EmployeeID, string pincode, string NewPINcode)
        {
            EMPLOYEE.ServiceManager.EmployeeService.ChangePINCode(EmployeeID, pincode, NewPINcode);
        }

        public static List<TicketMaster> GetActiveTicket(string TicketClass)
        {
            return EMPLOYEE.ServiceManager.EmployeeService.GetActiveTicket(TicketClass);
        }

        public static BreakPattern GetBreakPattern(string DWSGroup, string BreakCode)
        {
            return EMPLOYEE.ServiceManager.EmployeeConfig.GetBreakPattern(DWSGroup, BreakCode);
        }

        public static WFRuleSetting GetWFRuleSetting(string WFRule)
        {
            return EMPLOYEE.ServiceManager.EmployeeConfig.GetWFRuleSetting(WFRule);
        }

        public static List<DateSplit> SplitTime(string EmployeeID, DateTime inputDate, TimeSpan beginTime, TimeSpan endTime)
        {
            List<DateSplit> retVal = new List<DateSplit>();
            DateSplit item;
            INFOTYPE0007 info7 = INFOTYPE0007.GetWorkSchedule(EmployeeID, inputDate);
            INFOTYPE0007 info7_1 = INFOTYPE0007.GetWorkSchedule(EmployeeID, inputDate.AddDays(1));
            string wfRule = info7.WFRule;
            string wfRule1 = info7_1.WFRule;
            WFRuleSetting oSetting = GetWFRuleSetting(wfRule);
            TimeSpan time1, time2;
            TimeSpan timeCutOff, timeBegin, timeEnd;
            TimeSpan timeCutOff1, timeBegin1, timeEnd1;
            time1 = beginTime;
            time2 = endTime > beginTime ? endTime : endTime.Add(new TimeSpan(1, 0, 0, 0));
            timeBegin = oSetting.NormalBeginTime;
            if (oSetting.CutoffTime < timeBegin)
            {
                timeCutOff = oSetting.CutoffTime.Add(new TimeSpan(1, 0, 0, 0));
            }
            else
            {
                timeCutOff = oSetting.CutoffTime;
            }
            if (oSetting.NormalEndTime < timeBegin)
            {
                timeEnd = oSetting.NormalEndTime.Add(new TimeSpan(1, 0, 0, 0));
            }
            else
            {
                timeEnd = oSetting.NormalEndTime;
            }
            if (wfRule != wfRule1)
            {
                oSetting = GetWFRuleSetting(wfRule1);
                timeBegin1 = oSetting.NormalBeginTime;
                if (oSetting.CutoffTime < timeBegin1)
                {
                    timeCutOff1 = oSetting.CutoffTime.Add(new TimeSpan(1, 0, 0, 0));
                }
                else
                {
                    timeCutOff1 = oSetting.CutoffTime;
                }
                if (oSetting.NormalEndTime < timeBegin1)
                {
                    timeEnd1 = oSetting.NormalEndTime.Add(new TimeSpan(1, 0, 0, 0));
                }
                else
                {
                    timeEnd1 = oSetting.NormalEndTime;
                }
            }
            else
            {
                timeCutOff1 = timeCutOff;
                timeBegin1 = timeBegin;
                timeEnd1 = timeEnd;
            }
            timeBegin1 = timeBegin1.Add(new TimeSpan(1, 0, 0, 0));
            timeEnd1 = timeEnd1.Add(new TimeSpan(1, 0, 0, 0));
            timeCutOff1 = timeCutOff1.Add(new TimeSpan(1, 0, 0, 0));
            if (time2 > timeBegin && time1 < timeBegin)
            {
                item = new DateSplit();
                item.BeginTime = time1;
                item.EndTime = timeBegin;
                item.IsPrevDay = true;
                retVal.Add(item);
                time1 = timeBegin;
            }
            if (time2 > timeEnd && time1 < timeEnd)
            {
                item = new DateSplit();
                item.BeginTime = time1;
                item.EndTime = timeEnd;
                item.IsPrevDay = false;
                retVal.Add(item);
                time1 = timeEnd;
            }
            if (time2 > timeCutOff && time1 < timeCutOff)
            {
                item = new DateSplit();
                item.BeginTime = time1;
                item.EndTime = timeCutOff;
                item.IsPrevDay = false;
                retVal.Add(item);
                time1 = timeCutOff;
            }
            if (time2 <= timeBegin1)
            {
                item = new DateSplit();
                item.BeginTime = time1;
                item.EndTime = time2;
                item.IsPrevDay = true;
                retVal.Add(item);
            }
            else
            {
                if (time2 > timeBegin1 && time1 < timeBegin1)
                {
                    item = new DateSplit();
                    item.BeginTime = time1;
                    item.EndTime = timeBegin1;
                    item.IsPrevDay = true;
                    retVal.Add(item);
                    time1 = timeBegin1;
                }
                if (time2 > timeEnd1 && time1 < timeEnd1)
                {
                    item = new DateSplit();
                    item.BeginTime = time1;
                    item.EndTime = timeEnd1;
                    item.IsPrevDay = false;
                    retVal.Add(item);
                    time1 = timeEnd1;
                }
                if (time2 > timeCutOff1 && time1 < timeCutOff1)
                {
                    item = new DateSplit();
                    item.BeginTime = time1;
                    item.EndTime = timeCutOff1;
                    item.IsPrevDay = false;
                    retVal.Add(item);
                    time1 = timeCutOff1;
                }
                item = new DateSplit();
                item.BeginTime = time1;
                item.EndTime = time2;
                item.IsPrevDay = false;
                retVal.Add(item);
            }
            return retVal;
        }

        public static string CreateTicket(string TicketClass, TimeSpan LifeTime, string PINcode)
        {
            return EMPLOYEE.ServiceManager.EmployeeService.CreateTicket(TicketClass, LifeTime, PINcode);
        }

        public static List<string> GetUserInRoles(string UserRole)
        {
            return EMPLOYEE.ServiceManager.EmployeeService.GetUserInRole(UserRole);
        }

        //CHAT 2011-10-05 For MassPayslip & MassTaxReport
        //CHAT 2011-10-05 ����Ѻ�֧���������ʾ�ѡ�ҹ��������˹��§ҹ�������
        public static DataTable GetEmployeeIDByOrgUnit(string OrgUnit)
        {
            return EMPLOYEE.ServiceManager.EmployeeService.GetEmployeeIDByOrgUnit(OrgUnit);
        }

        //CHAT 2011-10-05 ����Ѻ�֧������ ResponseType �ͧ��������͵�Ǩ�ͺ�Է�������ҹ��� Type
        public static List<string> GetUserResponseType(string EmpID, string UserRole)
        {
            return EMPLOYEE.ServiceManager.EmployeeService.GetUserResponseType(EmpID, UserRole);
        }

        //CHAT 2011-10-10 ����Ѻ�֧������ Performance �ͧ��ѡ�ҹ
        public static DataTable GetRequestorPerformance(string EmpID)
        {
            return EMPLOYEE.ServiceManager.EmployeeService.GetRequestorPerformance(EmpID);
        }

        //CHAT 2011-10-10 ����Ѻ�֧������ Competency �ͧ��ѡ�ҹ
        public static DataTable GetRequestorCompetency(string EmpID)
        {
            return EMPLOYEE.ServiceManager.EmployeeService.GetRequestorCompetency(EmpID);
        }

        public static List<EmployeeData> GetEmployeeByOrg(String OrgID, DateTime CheckDate)
        {
            return EMPLOYEE.ServiceManager.OMService.GetEmployeeByOrg(OrgID, CheckDate);
        }

        public static List<INFOTYPE1000> GetOrgUnder(string OrgID, DateTime CheckDate, string LanguageCode)
        {
            return EMPLOYEE.ServiceManager.OMService.GetOrgUnder(OrgID, CheckDate, LanguageCode);
        }


        //AddBy: Ratchatawan W. (2012-11-07)
        public static List<EmployeeData> GetAllActiveEmployeeInINFOTYPE0001(DateTime Checkdate)
        {
            return EMPLOYEE.ServiceManager.EmployeeService.GetAllActiveEmployeeInINFOTYPE0001(Checkdate);
        }
        public static List<EmployeeData> GetEmployeeByOrg(String OrgID, DateTime BeginDate, DateTime EndDate)
        {
            return EMPLOYEE.ServiceManager.OMService.GetEmployeeByOrg(OrgID, BeginDate, EndDate);
        }
        //AddBy: Ratchatawan W. (2012-11-09)
        public static List<EmployeeData> GetManagerInSameOraganizationAndSameEmpSubGroup(String EmployeeID, string PositionID, DateTime CheckDate)
        {
            return EMPLOYEE.ServiceManager.EmployeeService.GetManagerInSameOraganizationAndSameEmpSubGroup(EmployeeID, PositionID, CheckDate);
        }

        //AddBy: Ratchatawan W. (2013-04-12)
        public static List<EmployeeData> GetOTSummaryPermissionByEmployeeID(String EmployeeID, DateTime CheckDate)
        {
            return EMPLOYEE.ServiceManager.EmployeeService.GetOTSummaryPermissionByEmployeeID(EmployeeID,CheckDate);
        }
        public static bool IsHaveOTSummaryPermissionByEmployeeID(String EmployeeID, DateTime CheckDate)
        {
            return EMPLOYEE.ServiceManager.EmployeeService.IsHaveOTSummaryPermissionByEmployeeID(EmployeeID, CheckDate);
        }

        public static List<EmployeeData> GetAllEmployeeInINFOTYPE0001(DateTime CheckDate)
        {
            return EMPLOYEE.ServiceManager.EmployeeService.GetAllEmployeeInINFOTYPE0001(CheckDate);
        }

        public static DataTable GetUserInResponseForActionOfInstead(string EmployeeID, int SubjectID, DateTime CheckDate, int FilterEmployee)
        {
            return EMPLOYEE.ServiceManager.EmployeeService.GetUserInResponseForActionOfInstead(EmployeeID, SubjectID, CheckDate, FilterEmployee);
        }

        public static List<PersonalSubAreaSetting> GetPersonalSubAreaByArea(string strArea)
        {
            return EMPLOYEE.ServiceManager.EmployeeConfig.GetPersonalSubAreaByArea(strArea);
        }

        public static List<DailyWS> GetDailyWSByWorkscheduleGrouping(string WorkScheduleGrouping, DateTime CheckDate)
        {
            return EMPLOYEE.ServiceManager.EmployeeConfig.GetDailyWSByWorkscheduleGrouping(WorkScheduleGrouping, CheckDate);
        }

        public static void SaveInfotype0105(DataTable oOldValue, DataTable oNewValue)
        {
            throw new NotImplementedException();
        }
    }
}
