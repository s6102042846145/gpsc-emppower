using System.Collections.Generic;
using ESS.EMPLOYEE.ABSTRACT;
using ESS.EMPLOYEE.INTERFACE;
using ESS.WORKFLOW;

namespace ESS.EMPLOYEE
{
    public class WorkPlaceCommunication : AbstractInfoType
    {
        private string __workplaceCode = "";

        public WorkPlaceCommunication()
        {
        }

        public string WorkPlaceCode { get; set; }
        public override string InfoType
        {
            get { return "WORKPLACE"; }
        }
        public override void SaveTo(string EmployeeID1, string EmployeeID2, string Mode, string Profile, List<IInfoType> Data)
        {
            List<WorkPlaceCommunication> List = new List<WorkPlaceCommunication>();
            foreach (IInfoType item in Data)
            {
                List.Add((WorkPlaceCommunication)item);
            }
            ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetMirrorService(Mode).SaveWorkPlaceData(List, Profile);
        }

        public override List<IInfoType> GetList(string EmployeeID1, string EmployeeID2, string Mode, string Profile)
        {
            List<IInfoType> oReturn = new List<IInfoType>();
            oReturn.AddRange(ServiceManager.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetMirrorService(Mode).GetWorkplaceData().ToArray());
            return oReturn;
        }
    }
}