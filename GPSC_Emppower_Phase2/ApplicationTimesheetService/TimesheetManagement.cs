﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.TIMESHEET.DATACLASS;
using ESS.TIMESHEET.INTERFACE;


namespace ESS.TIMESHEET
{
    public class TimesheetManagement
    {
        #region Constructor
        private TimesheetManagement()
        {
        }

        #endregion

        #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        private static Dictionary<string, TimesheetManagement> Cache = new Dictionary<string, TimesheetManagement>();

        private static string ModuleID = "ESS.TIMESHEET";
        public string CompanyCode { get; set; }

        public static TimesheetManagement CreateInstance(string oCompanyCode)
        {
            TimesheetManagement oTimesheetManagement = new TimesheetManagement()
            {
                CompanyCode = oCompanyCode
            };
            return oTimesheetManagement;
        }
        #endregion MultiCompany  Framework

        #region " privatedata "        
        private Type GetService(string Mode, string ClassName)
        {
            Type oReturn = null;
            Assembly oAssembly;
            string assemblyName = string.Format("{0}.{1}", ModuleID, Mode.ToUpper());
            string typeName = string.Format("{0}.{1}.{2}", ModuleID, Mode.ToUpper(), ClassName);// CultureInfo.CurrentCulture.TextInfo.ToTitleCase(ClassName.ToLower()));
            oAssembly = Assembly.Load(assemblyName);    // Load assembly (dll)
            oReturn = oAssembly.GetType(typeName);      // Load class
            return oReturn;
        }

        #endregion " privatedata "

        internal ITimeSheetConfigService GetMirrorConfig(string Mode)
        {
            Type oType = GetService(Mode, "HRPAConfigServiceImpl");
            if (oType == null)
            {
                return null;
            }
            else
            {
                return (ITimeSheetConfigService)Activator.CreateInstance(oType, CompanyCode);
            }
        }

        #region " MarkTimesheet "
        public void MarkTimesheet(string Employee, string Application, bool isMark)
        {
            MarkTimesheet(Employee, Application, isMark, false, "");
        }
        public void MarkTimesheet(string Employee, string Application, bool isMark, string Detail)
        {
            MarkTimesheet(Employee, Application, isMark, false, Detail);
        }
        public void MarkTimesheet(string Employee, string Application, bool isMark, bool isCheck)
        {
            MarkTimesheet(Employee, Application, isMark, isCheck, "");
        }
        public void MarkTimesheet(string Employee, string Application, bool isMark, bool isCheck, string Detail)
        {
            DateTime dateTime1, dateTime2;
            dateTime1 = new DateTime(1900, 1, 1);
            dateTime2 = new DateTime(9999, 12, 31);
            MarkTimesheet(Employee, Application, "", "", false, dateTime1, dateTime2, isMark, isCheck, Detail);
        }
        public void MarkTimesheet(string Employee, string Application, DateTime BeginDate, bool isMark)
        {
            MarkTimesheet(Employee, Application, "", "", true, BeginDate, BeginDate, isMark, false, "");
        }
        public void MarkTimesheet(string Employee, string Application, DateTime BeginDate, bool isMark, bool isCheck)
        {
            MarkTimesheet(Employee, Application, "", "", true, BeginDate, BeginDate, isMark, isCheck, "");
        }
        public void MarkTimesheet(string Employee, string Application, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark)
        {
            MarkTimesheet(Employee, Application, "", "", isFullDay, BeginDate, EndDate, isMark, false, "");
        }
        public void MarkTimesheet(string Employee, string Application, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark, bool isCheck)
        {
            MarkTimesheet(Employee, Application, "", "", isFullDay, BeginDate, EndDate, isMark, isCheck, "");
        }
        public void MarkTimesheet(string Employee, string Application, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark, string Detail)
        {
            MarkTimesheet(Employee, Application, "", "", isFullDay, BeginDate, EndDate, isMark, false, Detail);
        }
        public void MarkTimesheet(string Employee, string Application, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark, bool isCheck, string Detail)
        {
            MarkTimesheet(Employee, Application, "", "", isFullDay, BeginDate, EndDate, isMark, isCheck, Detail);
        }
        public void MarkTimesheet(string Employee, string Application, string SubKey, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark)
        {
            MarkTimesheet(Employee, Application, SubKey, "", isFullDay, BeginDate, EndDate, isMark, false, "");
        }
        public void MarkTimesheet(string Employee, string Application, string SubKey, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark, bool isCheck)
        {
            MarkTimesheet(Employee, Application, SubKey, "", isFullDay, BeginDate, EndDate, isMark, isCheck, "");
        }
        public void MarkTimesheet(string Employee, string Application, string SubKey, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark, string Detail)
        {
            MarkTimesheet(Employee, Application, SubKey, "", isFullDay, BeginDate, EndDate, isMark, false, Detail);
        }
        public void MarkTimesheet(string Employee, string Application, string SubKey, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark, bool isCheck, string Detail)
        {
            MarkTimesheet(Employee, Application, SubKey, "", isFullDay, BeginDate, EndDate, isMark, isCheck, Detail);
        }
        public void MarkTimesheet(string Employee, string Application, string SubKey1, string SubKey2, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark)
        {
            MarkTimesheet(Employee, Application, SubKey1, SubKey2, isFullDay, BeginDate, EndDate, isMark, false, "");
        }
        public void MarkTimesheet(string Employee, string Application, string SubKey1, string SubKey2, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark, bool isCheck)
        {
            MarkTimesheet(Employee, Application, SubKey1, SubKey2, isFullDay, BeginDate, EndDate, isMark, isCheck, "");
        }
        public void MarkTimesheet(string Employee, string Application, string SubKey1, string SubKey2, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark, string Detail)
        {
            MarkTimesheet(Employee, Application, SubKey1, SubKey2, isFullDay, BeginDate, EndDate, isMark, false, Detail);
        }
        public void MarkTimesheet(string Employee, string Application, string SubKey1, string SubKey2, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark, bool isCheck, string Detail)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.MarkTimesheet(Employee, Application, SubKey1, SubKey2, isFullDay, BeginDate, EndDate, isMark, isCheck, Detail);
        }
        #endregion
    }
}
