﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.SHAREDATASERVICE;
using ESS.TIMESHEET.INTERFACE;

namespace ESS.TIMESHEET
{
    public class ServiceManager
    {
        #region Constructor
        private ServiceManager()
        {

        }
        #endregion

        #region MultiCompany  Framework
        //private Dictionary<string, string> Config { get; set; }

        private static Dictionary<string, ServiceManager> Cache = new Dictionary<string, ServiceManager>();

        private static string ModuleID = "ESS.TIMESHEET";

        public string CompanyCode { get; set; }

        public static ServiceManager CreateInstance(string oCompanyCode)
        {
            
            ServiceManager oServiceManager = new ServiceManager()
            {
                CompanyCode = oCompanyCode
            };
            return oServiceManager;
        }
        #endregion MultiCompany  Framework

        #region " privatedata "        
        private Type GetService(string Mode, string ClassName)
        {
            Type oReturn = null;
            Assembly oAssembly;
            string assemblyName = string.Format("{0}.{1}", ModuleID, Mode.ToUpper());
            string typeName = string.Format("{0}.{1}.{2}", ModuleID, Mode.ToUpper(), ClassName);// CultureInfo.CurrentCulture.TextInfo.ToTitleCase(ClassName.ToLower()));
            oAssembly = Assembly.Load(assemblyName);    // Load assembly (dll)
            oReturn = oAssembly.GetType(typeName);      // Load class
            return oReturn;
        }

        #endregion " privatedata "

        internal ITimeSheetConfigService GetMirrorConfig(string Mode)
        {
            Type oType = GetService(Mode, "TimeSheetConfigServiceImpl");
            if (oType == null)
            {
                return null;
            }
            else
            {
                return (ITimeSheetConfigService)Activator.CreateInstance(oType, CompanyCode);
            }
        }

        internal ITimeSheetConfigService GetMirrorService(string Mode)
        {
            Type oType = GetService(Mode, "TimeSheetDataServiceImpl");
            if (oType == null)
            {
                return null;
            }
            else
            {
                return (ITimeSheetConfigService)Activator.CreateInstance(oType, CompanyCode);
            }
        }

       
        private string ESSCONNECTOR
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ESSCONNECTOR");
            }
        }

        private string ERPCONNECTOR
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ERPCONNECTOR");
            }
        }

        public ITimeSheetDataService ESSData
        {
            get
            {
                Type oType = GetService(ESSCONNECTOR, "TimeSheetDataServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (ITimeSheetDataService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }

        public ITimeSheetConfigService ESSConfig
        {
            get
            {
                Type oType = GetService(ESSCONNECTOR, "TimeSheetConfigServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (ITimeSheetConfigService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }

        public ITimeSheetDataService ERPData
        {
            get
            {
                Type oType = GetService(ERPCONNECTOR, "TimeSheetDataServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (ITimeSheetDataService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }

        internal ITimeSheetConfigService ERPConfig
        {
            get
            {
                Type oType = GetService(ERPCONNECTOR, "TimeSheetConfigServiceImpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (ITimeSheetConfigService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }
    }
}
