﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESS.TIMESHEET.INTERFACE
{
    public interface ITimeSheetDataService
    {
        void MarkTimesheet(string Employee, string Application, string SubKey1, string SubKey2, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark, bool isCheck, string Detail);
    }
}
