﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.TIMESHEET.DATACLASS;

namespace ESS.TIMESHEET.INTERFACE
{
    public interface ICollisionable
    {
        List<TimesheetData> LoadCollision(string EmployeeID, DateTime BeginDate, DateTime EndDate, string SubKey1, string SubKey2);
    }
}
