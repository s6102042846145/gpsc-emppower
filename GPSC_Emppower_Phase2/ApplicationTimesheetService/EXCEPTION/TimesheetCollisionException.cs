﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESS.TIMESHEET.DATACLASS;

namespace ESS.TIMESHEET.EXCEPTION
{
    public class TimesheetCollisionException : Exception
    {
        private List<TimesheetData> __list;
        public TimesheetCollisionException(List<TimesheetData> List)
            : base("Collission occur with data")
        {
            __list = List;
        }
        public List<TimesheetData> CollissionData
        {
            get
            {
                return __list;
            }
        }
    }
}
