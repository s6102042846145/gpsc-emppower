using System;
using System.Collections.Generic;
using System.Text;
using ESS.UTILITY.EXTENSION;

namespace ESS.TIMESHEET
{
    public class TimePairArchive : AbstractObject
    {
        // Fields...
        private String _Remark;
        private bool _PostOUT;
        private bool _PostIN;
        private String _ReferenceDocument;
        private DateTime _ClockOUT;
        private DateTime _ClockIN;
        private int _SeqNo;
        private DateTime _Date;
        private String _EmployeeID;

        public String Remark
        {
            get { return _Remark; }
            set
            {
                _Remark = value;
            }
        }
        
        public bool PostOUT
        {
            get { return _PostOUT; }
            set
            {
                _PostOUT = value;
            }
        }
        
        public bool PostIN
        {
            get { return _PostIN; }
            set
            {
                _PostIN = value;
            }
        }
        
        public String ReferenceDocument
        {
            get { return _ReferenceDocument; }
            set
            {
                _ReferenceDocument = value;
            }
        }
        
        public DateTime ClockOUT
        {
            get { return _ClockOUT; }
            set
            {
                _ClockOUT = value;
            }
        }
        
        public DateTime ClockIN
        {
            get { return _ClockIN; }
            set
            {
                _ClockIN = value;
            }
        }
        
        public int SeqNo
        {
            get { return _SeqNo; }
            set
            {
                _SeqNo = value;
            }
        }
        
        public DateTime Date
        {
            get { return _Date; }
            set
            {
                _Date = value;
            }
        }
        
        public String EmployeeID
        {
            get { return _EmployeeID; }
            set
            {
                _EmployeeID = value;
            }
        }
        
    }
}
