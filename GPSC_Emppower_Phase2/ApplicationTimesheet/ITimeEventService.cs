using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.TIMESHEET
{
    public interface ITimeEventService
    {
        List<TimeElement> GetTimeElement(string CardNo, DateTime BeginDate, DateTime EndDate, int RecordCount);
    }
}
