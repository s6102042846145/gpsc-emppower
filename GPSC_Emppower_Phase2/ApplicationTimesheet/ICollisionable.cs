using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.TIMESHEET
{
    public interface ICollisionable
    {
        List<TimesheetData> LoadCollision(string EmployeeID, DateTime BeginDate, DateTime EndDate, string SubKey1, string SubKey2);
    }
}
