using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.TIMESHEET
{
    public interface IERPTimesheetService
    {
        void PostTimePair(List<TimePair> Data);
        List<TimeElement> LoadArchiveEvent(string EmployeeID, DateTime BeginDate, DateTime EndDate, int RecordCount);
        List<CardSetting> LoadCardSetting(DateTime BeginDate);
    
    }
}
