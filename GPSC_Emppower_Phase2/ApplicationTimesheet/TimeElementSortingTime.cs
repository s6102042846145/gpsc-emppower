using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.TIMESHEET
{
    public class TimeElementSortingTime : IComparer<TimeElement>
    {
        private bool __isAsc;
        public TimeElementSortingTime()
            : this(true)
        {
        }

        public TimeElementSortingTime(bool isAsc)
        {
            __isAsc = isAsc;
        }

        #region IComparer<TimeElement> Members

        public int Compare(TimeElement x, TimeElement y)
        {
            if (x.EventTime > y.EventTime)
            {
                return __isAsc ? 1 : -1;
            }
            else if (x.EventTime == y.EventTime)
            {
                return 0;
            }
            else
            {
                return __isAsc ? -1 : 1;
            }
        }

        #endregion
    }
}
