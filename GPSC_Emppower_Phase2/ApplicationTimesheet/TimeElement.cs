using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using ESS.UTILITY.EXTENSION;

namespace ESS.TIMESHEET
{
    //[Serializable()]
    public class TimeElement : AbstractObject
    {
        private string __employeeID = "";
        private string __cardNo = "";
        private DoorType __doorType = DoorType.IN;
        private DateTime __eventTime;
        private string __location = "";
        private TimeElementType __type = TimeElementType.REALTIME;
        private bool __isDelete = false;
        private bool __isSystemDelete = false;
        public TimeElement()
        { 
        }
        public string EmployeeID
        {
            get
            {
                return __employeeID;
            }
            set
            {
                __employeeID = value;
            }
        }
        public string CardNo
        {
            get
            {
                return __cardNo;
            }
            set
            {
                __cardNo = value;
            }
        }
        public DoorType DoorType
        {
            get
            {
                return __doorType;
            }
            set
            {
                __doorType = value;
            }
        }
        public DateTime EventTime
        {
            get
            {
                return __eventTime;
            }
            set
            {
                __eventTime = value;
            }
        }
        public string Location
        {
            get
            {
                return __location;
            }
            set
            {
                __location = value;
            }
        }
        public TimeElementType ItemType
        {
            get
            {
                return __type;
            }
        }

        //internal void SetElementType(TimeElementType type)
        public void SetElementType(TimeElementType type)
        {
            __type = type;
        }


        public bool IsDelete
        {
            get
            {
                return __isDelete;
            }
            set
            {
                __isDelete = value;
            }
        }

        public bool IsSystemDelete
        {
            get
            {
                return __isSystemDelete;
            }
            set
            //internal set
            {
                __isSystemDelete = value;
            }
        }

        //public static List<TimeElement> GetLastData(string EmployeeID, int RecordCount)
        //{
        //    return TimesheetManagement.LoadTimeElement(EmployeeID,RecordCount);
        //}

        public static bool operator >(TimeElement c1, TimeElement c2)
        {
            return (c1 == null || c2 == null || c1.EventTime == DateTime.MinValue || c2.EventTime == DateTime.MinValue) ? false : c1.EventTime > c2.EventTime;
        }

        public static bool operator <(TimeElement c1, TimeElement c2)
        {
            return (c1 == null || c2 == null || c1.EventTime == DateTime.MinValue || c2.EventTime == DateTime.MinValue) ? false : c1.EventTime < c2.EventTime;
        }

        public static bool operator >=(TimeElement c1, TimeElement c2)
        {
            return (c1 == null || c2 == null || c1.EventTime == DateTime.MinValue || c2.EventTime == DateTime.MinValue) ? false : c1.EventTime >= c2.EventTime;
        }

        public static bool operator <=(TimeElement c1, TimeElement c2)
        {
            return (c1 == null || c2 == null || c1.EventTime == DateTime.MinValue || c2.EventTime == DateTime.MinValue) ? false : c1.EventTime <= c2.EventTime;
        }
    }
}
