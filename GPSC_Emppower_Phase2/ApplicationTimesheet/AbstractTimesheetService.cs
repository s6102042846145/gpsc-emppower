using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.TIMESHEET
{
    public class AbstractTimesheetService : ITimesheetService
    {
        public AbstractTimesheetService()
        { 
        }

        #region ITimesheetService Members

        public virtual void MarkTimesheet(string Employee, string Application, string SubKey1, string SubKey2, bool isFullDay,  DateTime BeginDate, DateTime EndDate, bool isMark, bool isCheck, string Detail)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual DateTime LoadArchiveDate(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<TimeElement> LoadArchiveEvent(string EmployeeID, DateTime BeginDate, DateTime EndDate, int RecordCount)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveArchiveEvent(string EmployeeID, DateTime BeginDate, DateTime EndDate, List<TimeElement> Data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<CardSetting> LoadCardSetting(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<CardSetting> LoadCardSetting(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<CardSetting> LoadCardSetting(string EmployeeID,DateTime CheckDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual string LoadArchivePeriod()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual string LoadTextData(string Code, string Language)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveArchiveTimePair(string EmployeeID, DateTime BeginDate, DateTime EndDate, List<TimePair> list)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<TimePairArchive> LoadTimePairArchive(DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<TimePairArchive> LoadTimePairArchive(String EmpID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public List<TimePair> LoadArchiveTimePair(string EmployeeID, DateTime BeginDate, DateTime EndDate, bool OnlyFirstRecord)
        {
            DateTime date1, date2;
            date1 = BeginDate.AddDays(-2);
            date2 = EndDate.AddDays(7);
            List<TimePair> oReturn = new List<TimePair>();
            List<TimeElement> buffer = LoadArchiveEvent(EmployeeID, date1, date2, -1);
            buffer.Sort(new TimeElementSortingTime());
            List<TimePair> pairs = TimesheetManagement.CreateInstance("").MatchingClockNew(EmployeeID, buffer, true, BeginDate, EndDate, new Dictionary<DateTime, string>());
            //List<TimePair> pairs = TimesheetManagement.MatchingClock1(EmployeeID, buffer);
            pairs.Sort(new TimePairSortingDate());


            foreach (TimePair pair in pairs)
            {
                if (pair.Date >= BeginDate && pair.Date <= EndDate)
                {
                    oReturn.Add(pair);
                    if (OnlyFirstRecord && oReturn.Count > 1)
                    {
                        break;
                    }
                }
                /*
                else if (pair.Date == EndDate.AddDays(1) && pair.ClockIN == null)
                {
                    oReturn.Add(pair);
                    if (OnlyFirstRecord && oReturn.Count > 1)
                    {
                        break;
                    }
                }
                 */
            }

            return oReturn;
        }

        public List<TimePair> LoadArchiveTimePair(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            return LoadArchiveTimePair(EmployeeID, BeginDate, EndDate, false);
        }

        public List<TimeElement> LoadArchiveEventRelative(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            List<TimeElement> oReturn = new List<TimeElement>();
            DateTime date1, date2;
            List<TimePair> nextPair = LoadArchiveTimePair(EmployeeID, BeginDate.AddDays(1), EndDate.AddMonths(1), true);
            date1 = BeginDate;
            date2 = EndDate;
            if (nextPair.Count > 0)
            {
                if (nextPair[0].ClockIN != null)
                {
                    if (nextPair[0].ClockOUT != null)
                    {
                        date2 = nextPair[0].ClockIN.EventTime.AddMinutes(-1);
                    }
                    else if (nextPair.Count > 1)
                    {
                        if (nextPair[1].ClockIN != null)
                        {
                            date2 = nextPair[1].ClockIN.EventTime.AddMinutes(-1);
                        }
                        else if (nextPair[1].ClockOUT != null)
                        {
                            date2 = nextPair[1].ClockOUT.EventTime.AddMinutes(-1);
                        }
                        else
                        {
                            date2 = EndDate.AddDays(1);
                        }
                    }
                    else
                    {
                        date2 = EndDate.AddDays(1);
                    }
                }
                else if (nextPair[0].ClockOUT != null)
                {
                    date2 = nextPair[0].ClockOUT.EventTime.AddMinutes(-1);
                    nextPair = LoadArchiveTimePair(EmployeeID, BeginDate.AddDays(1), EndDate.AddMonths(1));
                    if (nextPair.Count > 1)
                    {
                        if (nextPair[1].ClockIN != null)
                        {
                            date2 = nextPair[1].ClockIN.EventTime.AddMinutes(-1);
                        }
                        else if (nextPair[1].ClockOUT != null)
                        {
                            date2 = nextPair[1].ClockOUT.EventTime.AddMinutes(-1);
                        }
                    }
                    else
                    {
                        date2 = EndDate;
                    }
                }
                else
                {
                    date2 = EndDate;
                }
            }
            else
            {
                List<TimePair> currentPair = LoadArchiveTimePair(EmployeeID, BeginDate, EndDate.AddDays(1), true);
                if (currentPair.Count == 0)
                {
                    date2 = EndDate.AddDays(1);
                }
                else if (currentPair[0].ClockOUT != null)
                {
                    date1 = currentPair[0].ClockOUT.EventTime;
                    date2 = EndDate.AddDays(2);
                }
                else
                {
                    date2 = EndDate.AddDays(2);
                }
            }
            oReturn = LoadArchiveEvent(EmployeeID,date1,date2,-1);
            return oReturn;

        }

        public virtual List<TimesheetData> LoadTimesheet(string Employee, string Application, string SubKey1, string SubKey2, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveCardSetting(string EmployeeID, List<CardSetting> cardList)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveCardSetting(List<CardSetting> cardList)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<TimePairArchive> LoadTimePairArchiveOverlap(String EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        public virtual void SaveTimePairArchiveMappingByUser(List<TimePair> TimePairList)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<TimePair> LoadTimePairArchiveMappingByUser(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }
}
