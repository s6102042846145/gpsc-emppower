using System;
using System.Collections.Generic;
using System.Text;
//using ESS.UTILITY.EXTENSION;
using ESS.DATA;

namespace ESS.TIMESHEET
{
    public class TimesheetData : AbstractObject
    {
        private string __employeeID;
        private string __applicationKey;
        private string __subKey1;
        private string __subKey2;
        private DateTime __beginDate;
        private DateTime __endDate;
        private bool __fullDay;
        private string __detail;

        public TimesheetData()
        { 
        }

        public string EmployeeID
        {
            get
            {
                return __employeeID;
            }
            set
            {
                __employeeID = value;
            }
        }

        public string ApplicationKey
        {
            get
            {
                return __applicationKey;
            }
            set
            {
                __applicationKey = value;
            }
        }

        public string SubKey1
        {
            get
            {
                return __subKey1;
            }
            set
            {
                __subKey1 = value;
            }
        }

        public string SubKey2
        {
            get
            {
                return __subKey2;
            }
            set
            {
                __subKey2 = value;
            }
        }

        public DateTime BeginDate
        {
            get
            {
                return __beginDate;
            }
            set
            {
                __beginDate = value;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return __endDate;
            }
            set
            {
                __endDate = value;
            }
        }

        public bool FullDay
        {
            get
            {
                return __fullDay;
            }
            set
            {
                __fullDay = value;
            }
        }

        public string Detail
        {
            get
            {
                return __detail;
            }
            set
            {
                __detail = value;
            }
        }

    }
}
