using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Reflection;
using ESS.UTILITY.EXTENSION;
using ESS.EMPLOYEE.CONFIG.TM;

namespace ESS.TIMESHEET
{
    public class TimePair : AbstractObject
    {
        private string __employeeID = "";
        private TimeElement __clockIN = null;
        private TimeElement __clockOUT = null;
        private DateTime __date = DateTime.MinValue;
        private string __dwsCode = "";
        private DailyWS __dws;
        private string __referenceDocument = "";
        private int __seqno = 0;
        private bool __postIN = false;
        private bool __postOUT = false;
        private bool __isDayOff = false;
        private bool __isHoliday = false;
        private string __remark = "";
        private string __detail = "";
        private bool _IsCheckRemark = false; // �����������Ѻ����Ҿ�ѡ�ҹ�ա�� �Ѻ�ͧ���� �����Ҥú���͹� Timesheet �е�ͧ����ʴ���ᴧ��ѹ���ӧҹ
        private bool _IsNoScan = false;


        private static CultureInfo oCL = new CultureInfo("en-US");
        const string __dateFormat = "yyyyMMddHHmmss";
        public TimePair()
        { 
        }
        public TimePair(TimeElement cin,TimeElement cout)
        {
            __clockIN = cin;
            __clockOUT = cout;
        }

        public bool IsNoScan
        {
            get
            {
                return _IsNoScan;
            }
            set
            {
                _IsNoScan = value;
            }
        }

        public bool IsCheckRemark
        {
            get
            {
                return _IsCheckRemark;
            }
            set
            {
                _IsCheckRemark = value;
            }
        }

        public string EmployeeID
        {
            get
            {
                return __employeeID;
            }
            set
            {
                __employeeID = value;
            }
        }

        public DateTime Date
        {
            get
            {
                return __date;
            }
            set
            {
                __date = value.Date;
            }
        }

        public int SeqNo
        {
            get
            {
                return __seqno;
            }
            set
            {
                __seqno = value;
            }
        }

        public string DWSCode
        {
            get
            {
                return __dwsCode;
            }
            set
            {
                __dwsCode = value;
            }
        }

        public DailyWS DWS
        {
            get
            {
                return __dws;
            }
            set
            {
                __dws = value;
            }
        }

        public TimeElement ClockIN
        {
            get
            {
                return __clockIN;
            }
            set
            {
                __clockIN = value;
            }
        }

        public bool PostIN
        {
            get
            {
                return __postIN;
            }
            set
            {
                __postIN = value;
            }
        }

        public TimeElement ClockOUT
        {
            get
            {
                return __clockOUT;
            }
            set
            {
                __clockOUT = value;
            }
        }

        public bool PostOUT
        {
            get
            {
                return __postOUT;
            }
            set
            {
                __postOUT = value;
            }
        }

        public string Remark
        {
            get
            {
                return __remark;
            }
            set
            {
                __remark = value;
            }
        }

        public string Detail
        {
            get
            {
                return __detail;
            }
            set
            {
                __detail = value;
            }
        }

        public bool IsDayOff
        {
            get
            {
                return __isDayOff;
            }
            set
            {
                __isDayOff = value;
            }
        }

        public bool IsHoliday
        {
            get
            {
                return __isHoliday;
            }
            set
            {
                __isHoliday = value;
            }
        }

        public string ReferenceDocument
        {
            get
            {
                return __referenceDocument;
            }
            set
            {
                __referenceDocument = value;
            }
        }

        protected override DataColumn CreateComplexTypeColumn(PropertyInfo prop)
        {
            if (prop.PropertyType == typeof(TimeElement))
            {
                DataColumn oDC = new DataColumn(prop.Name, typeof(DateTime));
                return oDC;
            }
            return base.CreateComplexTypeColumn(prop);
        }

        public override void LoadDataToTable(DataTable Data)
        {
            DataRow dr = Data.NewRow();
            dr["EmployeeID"] = EmployeeID;
            dr["Date"] = Date;
            dr["SeqNo"] = SeqNo;
            if (ClockIN == null)
            {
                dr["ClockIN"] = DBNull.Value;
            }
            else
            {
                if (dr.Table.Columns["ClockIN"].DataType == typeof(TimeElement))
                {
                    dr["ClockIN"] = ClockIN;
                }
                else
                {
                    dr["ClockIN"] = ClockIN.EventTime;
                }
            }
            if (ClockOUT == null)
            {
                dr["ClockOUT"] = DBNull.Value;
            }
            else
            {
                if (dr.Table.Columns["ClockOUT"].DataType == typeof(TimeElement))
                {
                    dr["ClockOUT"] = ClockOUT;
                }
                else
                {
                    dr["ClockOUT"] = ClockOUT.EventTime;
                }
            }
            dr["ReferenceDocument"] = ReferenceDocument;
            dr["PostIN"] = PostIN;
            dr["PostOUT"] = PostOUT;
            dr["Remark"] = Remark;
            Data.Rows.Add(dr);
        }

        public override void ParseToObject(DataRow dr)
        {
            EmployeeID = (string)dr["EmployeeID"];
            Date = (DateTime)dr["Date"];
            PostIN = (bool)dr["PostIN"];
            PostOUT = (bool)dr["PostOUT"];
            TimeElement buffer;
            if (!dr.IsNull("ClockIN"))
            {
                buffer = new TimeElement();
                buffer.SetElementType(TimeElementType.ACCEPT);
                buffer.DoorType = DoorType.IN;
                buffer.EventTime = (DateTime)dr["ClockIN"];
                buffer.EmployeeID = dr["EmployeeID"].ToString();
                ClockIN = buffer;
            }
            if (!dr.IsNull("ClockOUT"))
            {
                buffer = new TimeElement();
                buffer.SetElementType(TimeElementType.ACCEPT);
                buffer.DoorType = DoorType.OUT;
                buffer.EventTime = (DateTime)dr["ClockOUT"];
                buffer.EmployeeID = dr["EmployeeID"].ToString();
                ClockOUT = buffer;
            }
            ReferenceDocument = (string)dr["ReferenceDocument"];
        }

        private string GetTimeString(TimeElement obj)
        {
            if (obj == null || obj.EventTime == null)
            {
                //return "  :  :  ";
                return "-";
            }
            return obj.EventTime.ToString("HH:mm:ss");
        }

        private string GetTimeStringWithFlag(TimeElement obj)
        {
            if (obj == null || obj.EventTime == null)
            {
                return "";
            }
            return string.Format("{0}|{1}", obj.DoorType.ToString(), obj.EventTime.ToString(__dateFormat, oCL));
        }

        private string GetTimeStringWithFlag_UT(DoorType dt ,TimeElement obj)
        {
            if (obj == null || obj.EventTime == null)
            {
                return "";
            }
            return string.Format("{0}|{1}", dt, obj.EventTime.ToString(__dateFormat, oCL));
        }

        public override string ToString()
        {
            //return string.Format("{0}#{1}", GetTimeStringWithFlag(this.ClockIN), GetTimeStringWithFlag(this.ClockOUT));
            return string.Format("{0}#{1}", GetTimeStringWithFlag_UT(DoorType.IN, this.ClockIN), GetTimeStringWithFlag_UT(DoorType.OUT, this.ClockOUT));
        }

        public static TimePair Parse(string Value)
        {
            Regex regex = new Regex(@"((?<FLAG1>IN|OUT|in|out)\|(?<DATE1>[0-9]{14}))?#((?<FLAG2>IN|OUT|in|out)\|(?<DATE2>[0-9]{14}))?");
            if (!regex.IsMatch(Value))
            {
                throw new TimePairParsingException();
            }
            Match match = regex.Match(Value);
            TimeElement item,item1;
            DateTime buffer = DateTime.MinValue;
            item = new TimeElement();
            item.DoorType = match.Groups["FLAG1"].Value == "IN" ? DoorType.IN : DoorType.OUT;
            DateTime.TryParseExact(match.Groups["DATE1"].Value, __dateFormat, oCL, DateTimeStyles.None, out buffer);
            item.EventTime = buffer;
            item1 = new TimeElement();
            item1.DoorType = match.Groups["FLAG2"].Value == "IN" ? DoorType.IN : DoorType.OUT;
            DateTime.TryParseExact(match.Groups["DATE2"].Value, __dateFormat, oCL, DateTimeStyles.None, out buffer);
            item1.EventTime = buffer;

            return new TimePair(item, item1);
        }
    }
}
