using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using System.Text;
using ESS.SHAREDATASERVICE;

namespace ESS.TIMESHEET
{
    public class ServiceManager 
    {
        //private static Configuration __config;
        private ServiceManager()
        {
        }

        private static string ModuleID = "ESS.HR.TM";
        public string CompanyCode { get; set; }

        public static ServiceManager CreateInstance(string oCompanyCode)
        {

            ServiceManager oServiceManager = new ServiceManager()
            {
                CompanyCode = oCompanyCode
            };
            return oServiceManager;
        }

        #region " privatedata "        
        private Type GetService(string Mode, string ClassName)
        {
            Type oReturn = null;
            Assembly oAssembly;
            string assemblyName = string.Format("{0}.{1}", ModuleID, Mode.ToUpper());
            string typeName = string.Format("{0}.{1}.{2}", ModuleID, Mode.ToUpper(), ClassName);// CultureInfo.CurrentCulture.TextInfo.ToTitleCase(ClassName.ToLower()));
            oAssembly = Assembly.Load(assemblyName);    // Load assembly (dll)
            oReturn = oAssembly.GetType(typeName);      // Load class
            return oReturn;
        }

        #endregion " privatedata "


        

        internal ITimesheetService GetMirrorService(string Mode)
        {
            Type oType = GetService(Mode, "TIMESHEETSERVICE");
            if (oType == null)
            {
                return null;
            }
            else
            {
                return (ITimesheetService)Activator.CreateInstance(oType, CompanyCode);
            }
        }


        #region " private data "
        //private static Configuration config
        //{
        //    get
        //    {
        //        if (__config == null)
        //        {
        //            __config = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().CodeBase.Replace("file:///", "").Replace("/", "\\"));
        //        }
        //        return __config;
        //    }
        //}

        private string ESSCONNECTOR
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ESSCONNECTOR");
            }
        }

        private string ERPCONNECTOR
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "ERPCONNECTOR");
            }
        }

        //private static string MODE
        //{
        //    get
        //    {
        //        if (config == null || config.AppSettings.Settings["MODE"] == null)
        //        {
        //            return "DB";
        //        }
        //        else
        //        {
        //            return config.AppSettings.Settings["MODE"].Value;
        //        }
        //    }
        //}

        //private static string ERPMODE
        //{
        //    get
        //    {
        //        if (config == null || config.AppSettings.Settings["ERPMODE"] == null)
        //        {
        //            return "DB";
        //        }
        //        else
        //        {
        //            return config.AppSettings.Settings["ERPMODE"].Value;
        //        }
        //    }
        //}

        //private static Type GetService(string Mode)
        //{
        //    Type oReturn = null;
        //    Assembly oAssembly;
        //    string assemblyName = string.Format("ESS.TIMESHEET.{0}", Mode.ToUpper());
        //    string typeName = string.Format("ESS.TIMESHEET.{0}.TIMESHEETSERVICE", Mode.ToUpper());
        //    oAssembly = Assembly.Load(assemblyName);
        //    oReturn = oAssembly.GetType(typeName);
        //    return oReturn;
        //}

        //private static Type GetERPService(string Mode)
        //{
        //    Type oReturn = null;
        //    Assembly oAssembly;
        //    string assemblyName = string.Format("ESS.TIMESHEET.{0}", Mode.ToUpper());
        //    string typeName = string.Format("ESS.TIMESHEET.{0}.ERPTIMESHEETSERVICE", Mode.ToUpper());
        //    oAssembly = Assembly.Load(assemblyName);
        //    oReturn = oAssembly.GetType(typeName);
        //    return oReturn;
        //}

        #endregion

        //internal static ITimesheetService TimesheetService
        //{
        //    get
        //    {
        //        Type oType = GetService(MODE);
        //        if (oType == null)
        //        {
        //            return null;
        //        }
        //        else
        //        {
        //            return (ITimesheetService)Activator.CreateInstance(oType);
        //        }
        //    }
        //}

        //internal static IERPTimesheetService ERPTimesheetService
        //{
        //    get
        //    {
        //        Type oType = GetERPService(ERPMODE);
        //        if (oType == null)
        //        {
        //            return null;
        //        }
        //        else
        //        {
        //            return (IERPTimesheetService)Activator.CreateInstance(oType);
        //        }
        //    }
        //}

        public ITimesheetService ESSData
        {
            get
            {
                Type oType = GetService(ESSCONNECTOR, "TimesheetServicelmpl");
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (ITimesheetService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }

        public IERPTimesheetService ERPData
        {
            get
            {
                Type oType = GetService(ERPCONNECTOR, "TimesheetServicelmpl");
                if (oType == null)
                {
                    return null;
                }
                else 
                {
                    return (IERPTimesheetService)Activator.CreateInstance(oType, CompanyCode);
                }
            }
        }

        

        internal static ITimeEventService GetTimeEventService(string ServiceCode)
        {
            ITimeEventService oService = null;
            Type oReturn = null;
            Assembly oAssembly;
            string assemblyName = "ESS.TIMESHEET.DB"; //string.Format("ESS.TIMESHEET.{0}", ServiceCode.Trim().ToUpper());
            string typeName = "ESS.TIMESHEET.TIMEEVENT.DB.TIMEEVENTSERVICE"; // string.Format("ESS.TIMESHEET.TIMEEVENT.{0}.TIMEEVENTSERVICE", ServiceCode.Trim().ToUpper());
            oAssembly = Assembly.Load(assemblyName);
            oReturn = oAssembly.GetType(typeName);
            if (oReturn != null)
            {
                oService = (ITimeEventService)Activator.CreateInstance(oReturn);
            }
            return oService;
        }
    }
}
