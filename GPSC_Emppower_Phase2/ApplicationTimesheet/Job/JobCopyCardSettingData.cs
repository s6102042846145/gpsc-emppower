using System;
using System.Collections.Generic;
using System.Text;

using ESS.JOB;

namespace ESS.TIMESHEET
{
    public class JobCopyCardSettingData : AbstractJobWorker
    {
        public JobCopyCardSettingData()
        {
        }

        public override List<ITaskWorker> LoadTasks()
        {
            List<ITaskWorker> returnValue = new List<ITaskWorker>();
            TaskCopyCardSettingData task;
            task = new TaskCopyCardSettingData();
            task.InfoType = "CARDSETTING";
            task.TaskID = 0;
            returnValue.Add(task);

            return returnValue;
        }


    }
}
