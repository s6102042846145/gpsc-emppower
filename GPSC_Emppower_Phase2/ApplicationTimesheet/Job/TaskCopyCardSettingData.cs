using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

using ESS.DATA;
using ESS.EMPLOYEE.CONFIG;



namespace ESS.TIMESHEET
{
    public class TaskCopyCardSettingData : AbstractTaskWorker
    {
        private string _infoType = "";
        private string __empID1 = "00000000";
        private string __empID2 = "99999999";
        private DateTime _beginDate;
        private DateTime _endDate;
        private string _cardNo = "";
        private string _location = "";

        public TaskCopyCardSettingData()
        {

        }

        public string InfoType
        {
            get
            {
                return _infoType;
            }
            set
            {
                _infoType = value;
            }
        }

        public string EmployeeID1
        {
            get
            {
                return __empID1;
            }
            set
            {
                __empID1 = value;
            }
        }

        public string EmployeeID2
        {
            get
            {
                return __empID2;
            }
            set
            {
                __empID2 = value;
            }
        }

        public DateTime BeginDate
        {
            get
            {
                return _beginDate;
            }
            set
            {
                _beginDate = value;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return _endDate;
            }
            set
            {
                _endDate = value;
            }
        }

        public string CardNo
        {
            get
            {
                return _cardNo;
            }
            set
            {
                _cardNo = value;
            }
        }

        public string Location
        {
            get
            {
                return _location;
            }
            set
            {
                _location = value;
            }
        }

        public override void Run()
        {
            Console.WriteLine("Begin Save Data");

            CultureInfo oCL = new CultureInfo("en-US");

            List<CardSetting> cdSettingList=new List<CardSetting>();
            
            //DateTime rDate = new DateTime(2011, 8, 4);

            

            cdSettingList = ServiceManager.ERPTimesheetService.LoadCardSetting(DateTime.Now);
            ServiceManager.TimesheetService.SaveCardSetting(cdSettingList);
 
            //Type typeData = EmployeeData.GetInfotype(InfoType);
            //IInfoType prototype = (IInfoType)Activator.CreateInstance(typeData, null);

            //List<IInfoType> _list = new List<IInfoType>();
            //_list.AddRange(prototype.GetList(EmployeeID1, EmployeeID2, DateTime.Now.Date.ToString("yyyyMMdd", oCL), "DEFAULT"));

            //prototype.SaveTo(EmployeeID1, EmployeeID2, "","DEFAULT", _list);         


        }

    }
}
