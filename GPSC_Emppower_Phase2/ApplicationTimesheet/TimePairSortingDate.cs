using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.TIMESHEET
{
    public class TimePairSortingDate : IComparer<TimePair>
    {
        public TimePairSortingDate()
        {
        }

        #region IComparer<TimePair> Members

        public int Compare(TimePair x, TimePair y)
        {
            if (x.Date > y.Date)
            {
                return 1;
            }
            else if (x.Date == y.Date)
            {
                if (x.ClockIN != null && y.ClockIN != null)
                {
                    if (x.ClockIN > y.ClockIN)
                    {
                        return 1;
                    }
                    else if (x.ClockIN < y.ClockIN)
                    {
                        return -1;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else if (x.ClockIN != null && y.ClockOUT != null)
                {
                    if (x.ClockIN > y.ClockOUT)
                    {
                        return 1;
                    }
                    else if (x.ClockIN < y.ClockOUT)
                    {
                        return -1;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else if (x.ClockOUT != null && y.ClockIN != null)
                {
                    if (x.ClockOUT > y.ClockIN)
                    {
                        return 1;
                    }
                    else if (x.ClockOUT < y.ClockIN)
                    {
                        return -1;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else if (x.ClockOUT != null && y.ClockOUT != null)
                {
                    if (x.ClockOUT > y.ClockOUT)
                    {
                        return 1;
                    }
                    else if (x.ClockOUT < y.ClockOUT)
                    {
                        return -1;
                    }
                    else
                    {
                        return 0;
                    }
                } 
                return 0;
            }
            else
            {
                return -1;
            }
        }

        #endregion
    }
}
