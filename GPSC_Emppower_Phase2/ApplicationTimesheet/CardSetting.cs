using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using ESS.UTILITY.EXTENSION;

namespace ESS.TIMESHEET
{
    public class CardSetting : AbstractObject
    {
        private string __employeeID;
        private DateTime __beginDate;
        private DateTime __endDate;
        private string __cardno;
        private string __location = "PTTCH";
        public CardSetting()
        { 
        }
        public string EmployeeID
        {
            get
            {
                return __employeeID;
            }
            set
            {
                __employeeID = value;
            }
        }
        public DateTime BeginDate
        {
            get
            {
                return __beginDate;
            }
            set
            {
                __beginDate = value;
            }
        }
        public DateTime EndDate
        {
            get
            {
                return __endDate;
            }
            set
            {

                __endDate = value;
            }
        }
        public string CardNo
        {
            get
            {
                return __cardno;
            }
            set
            {
                __cardno = value;
            }
        }
        public string Location
        {
            get
            {
                return __location;
            }
            set
            {
                __location = value;
            }
        }

        public void ParseToObject(DataRow dr)
        {
            throw new NotImplementedException();
        }
    }
}
