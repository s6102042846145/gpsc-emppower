using System;
using System.Collections.Generic;
using System.Text;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.WORKFLOW;

namespace ESS.TIMESHEET
{
    public class TimesheetManagement
    {
        private TimesheetManagement()
        {
        }

        private static Dictionary<string, EmployeeManagement> Cache = new Dictionary<string, EmployeeManagement>();
        private static string ModuleID = "ESS.HR.TM";
        public string CompanyCode { get; set; }
        public static TimesheetManagement CreateInstance(string oCompanyCode)
        {

            TimesheetManagement oTimesheetManagement = new TimesheetManagement()
            {
                CompanyCode = oCompanyCode
            };
            return oTimesheetManagement;
        }

        #region " MarkTimesheet "
        public void MarkTimesheet(string Employee, string Application, bool isMark)
        {
            MarkTimesheet(Employee, Application, isMark, false, "");
        }
        public void MarkTimesheet(string Employee, string Application, bool isMark, string Detail)
        {
            MarkTimesheet(Employee, Application, isMark, false, Detail);
        }
        public void MarkTimesheet(string Employee, string Application, bool isMark, bool isCheck)
        {
            MarkTimesheet(Employee, Application, isMark, isCheck, "");
        }
        public void MarkTimesheet(string Employee, string Application, bool isMark, bool isCheck, string Detail)
        {
            DateTime dateTime1, dateTime2;
            dateTime1 = new DateTime(1900, 1, 1);
            dateTime2 = new DateTime(9999, 12, 31);
            ServiceManager.CreateInstance(CompanyCode).ESSData.MarkTimesheet(Employee, Application, "", "", false, dateTime1, dateTime2, isMark, isCheck, Detail);
        }
        public void MarkTimesheet(string Employee, string Application, DateTime BeginDate, bool isMark)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.MarkTimesheet(Employee, Application, "", "", true, BeginDate, BeginDate, isMark, false, "");
        }
        public void MarkTimesheet(string Employee, string Application, DateTime BeginDate, bool isMark, bool isCheck)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.MarkTimesheet(Employee, Application, "", "", true, BeginDate, BeginDate, isMark, isCheck, "");
        }
        public void MarkTimesheet(string Employee, string Application, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.MarkTimesheet(Employee, Application, "", "", isFullDay, BeginDate, EndDate, isMark, false, "");
        }
        public void MarkTimesheet(string Employee, string Application, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark, bool isCheck)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.MarkTimesheet(Employee, Application, "", "", isFullDay, BeginDate, EndDate, isMark, isCheck, "");
        }
        public void MarkTimesheet(string Employee, string Application, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark, string Detail)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.MarkTimesheet(Employee, Application, "", "", isFullDay, BeginDate, EndDate, isMark, false, Detail);
        }
        public void MarkTimesheet(string Employee, string Application, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark, bool isCheck, string Detail)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.MarkTimesheet(Employee, Application, "", "", isFullDay, BeginDate, EndDate, isMark, isCheck, Detail);
        }
        public void MarkTimesheet(string Employee, string Application, string SubKey, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.MarkTimesheet(Employee, Application, SubKey, "", isFullDay, BeginDate, EndDate, isMark, false, "");
        }
        public void MarkTimesheet(string Employee, string Application, string SubKey, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark, bool isCheck)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.MarkTimesheet(Employee, Application, SubKey, "", isFullDay, BeginDate, EndDate, isMark, isCheck, "");
        }
        public void MarkTimesheet(string Employee, string Application, string SubKey, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark, string Detail)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.MarkTimesheet(Employee, Application, SubKey, "", isFullDay, BeginDate, EndDate, isMark, false, Detail);
        }
        public void MarkTimesheet(string Employee, string Application, string SubKey, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark, bool isCheck, string Detail)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.MarkTimesheet(Employee, Application, SubKey, "", isFullDay, BeginDate, EndDate, isMark, isCheck, Detail);
        }
        public void MarkTimesheet(string Employee, string Application, string SubKey1, string SubKey2, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.MarkTimesheet(Employee, Application, SubKey1, SubKey2, isFullDay, BeginDate, EndDate, isMark, false, "");
        }
        public void MarkTimesheet(string Employee, string Application, string SubKey1, string SubKey2, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark, bool isCheck)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.MarkTimesheet(Employee, Application, SubKey1, SubKey2, isFullDay, BeginDate, EndDate, isMark, isCheck, "");
        }
        public void MarkTimesheet(string Employee, string Application, string SubKey1, string SubKey2, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark, string Detail)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.MarkTimesheet(Employee, Application, SubKey1, SubKey2, isFullDay, BeginDate, EndDate, isMark, false, Detail);
        }
        public void MarkTimesheet(string Employee, string Application, string SubKey1, string SubKey2, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark, bool isCheck, string Detail)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.MarkTimesheet(Employee, Application, SubKey1, SubKey2, isFullDay, BeginDate, EndDate, isMark, isCheck, Detail);
            //ESS.TIMESHEET.ServiceManager.TimesheetService.MarkTimesheet(Employee, Application, SubKey1, SubKey2, isFullDay, BeginDate, EndDate, isMark, isCheck, Detail);
        }
        #endregion

        #region " TimeElement "

        public List<TimeElement> GetLastData(string EmployeeID, int RecordCount)
        {
            return LoadTimeElement(EmployeeID, RecordCount);
        }

        public List<TimeElement> LoadTimeElement(string EmployeeID, int recordCount)
        {
            DateTime date1, date2;
            date2 = DateTime.Now;
            date1 = date2.AddMonths(-1);
            return LoadTimeElement(EmployeeID, date1, date2.AddDays(1), recordCount);
        }

        public List<TimeElement> LoadTimeElement(string EmployeeID, int Year, int Month)
        {
            DateTime date1, date2;
            date1 = new DateTime(Year, Month, 1);
            date2 = date1.AddMonths(1).AddDays(-1);
            return LoadTimeElement(EmployeeID, date1.AddDays(-2), date2.AddDays(1));
        }

        public List<TimeElement> LoadTimeElement(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            return LoadTimeElement(EmployeeID, BeginDate, EndDate, -1, true, true);
        }

        public List<TimeElement> LoadTimeElement(string EmployeeID, DateTime BeginDate, DateTime EndDate, int RecordCount, bool LoadRealtime)
        {
            return LoadTimeElement(EmployeeID, BeginDate, EndDate, RecordCount, LoadRealtime, false);
        }

        public List<TimeElement> LoadTimeElement(string EmployeeID, DateTime BeginDate, DateTime EndDate, int RecordCount, bool LoadRealtime, bool LoadArchive)
        {
            List<TimeElement> oReturn = new List<TimeElement>();
            // load last archive date
            DateTime lastArchive = ServiceManager.CreateInstance(CompanyCode).ESSData.LoadArchiveDate(EmployeeID);
            DateTime date1, date2;

            //����ѹ����ش ���¡��� �ѹ LastArchive �����Ŵ�����Ũҡ Table Timesheet.TimeElementArchive
            if (EndDate < lastArchive)
            {
                date1 = BeginDate;
                date2 = EndDate;
                // load archive;
                if (LoadArchive)
                {
                    List<TimeElement> buffer = LoadTimeElementArchive(EmployeeID, date1, date2, RecordCount);
                    foreach (TimeElement item in buffer)
                    {
                        item.SetElementType(TimeElementType.ACCEPT);
                        oReturn.Add(item);
                    }
                }
            }
            //����ѹ������� ���¡��� �ѹ LastArchive 
            else if (BeginDate < lastArchive)
            {
                date1 = lastArchive;
                date2 = EndDate;
                //load realtime
                if (LoadRealtime)
                {
                    oReturn.AddRange(LoadTimeElementRealtime(EmployeeID, date1, date2, RecordCount));
                }
                if ((oReturn.Count < RecordCount && RecordCount > 0 && LoadArchive) || RecordCount == -1)
                {
                    date1 = BeginDate;
                    date2 = lastArchive;
                    // load archive;
                    List<TimeElement> buffer = LoadTimeElementArchive(EmployeeID, date1, date2, RecordCount);
                    foreach (TimeElement item in buffer)
                    {
                        item.SetElementType(TimeElementType.ACCEPT);
                        oReturn.Add(item);
                    }
                }
            }
            else
            {
                date1 = BeginDate;
                date2 = EndDate;
                //load realtime
                if (LoadRealtime)
                {
                    oReturn.AddRange(LoadTimeElementRealtime(EmployeeID, date1, date2, RecordCount));
                }
            }
            return oReturn;
        }

        public List<TimeElement> LoadTimeElement(string EmployeeID, DateTime BeginDate, DateTime EndDate, int RecordCount)
        {
            return LoadTimeElement(EmployeeID, BeginDate, EndDate, RecordCount, true, true);
        }

        private List<TimeElement> LoadTimeElementArchive(string EmployeeID, DateTime BeginDate, DateTime EndDate, int RecordCount)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.LoadArchiveEvent(EmployeeID, BeginDate, EndDate, RecordCount);
        }

        public List<TimeElement> LoadTimeElementRealTime(string EmployeeID, int Year, int Month)
        {
            DateTime date1, date2;
            date1 = new DateTime(Year, Month, 1);
            date2 = date1.AddMonths(1).AddDays(-1);
            return LoadTimeElement(EmployeeID, date1.AddDays(-2), date2.AddDays(2), -1, true, false);
        }

        public List<TimeElement> ForceLoadTimeElementRealtime(string EmployeeID, DateTime BeginDate, DateTime EndDate, int RecordCount)
        {
            return LoadTimeElementRealtime(EmployeeID, BeginDate, EndDate, RecordCount);
        }

        public List<TimeElement> LoadTimeElementRealtime(string EmployeeID, DateTime BeginDate, DateTime EndDate, int RecordCount)
        {
            List<TimeElement> oReturn = new List<TimeElement>();
            foreach (CardSetting CS in ServiceManager.CreateInstance(CompanyCode).ESSData.LoadCardSetting(EmployeeID, BeginDate, EndDate))
            {
                DateTime date1, date2;
                //date1 = BeginDate > CS.BeginDate ? CS.BeginDate : BeginDate;
                date1 = BeginDate > CS.BeginDate ? BeginDate : CS.BeginDate;
                date2 = EndDate > CS.EndDate ? CS.EndDate : EndDate;
                
                ITimeEventService oService = ESS.TIMESHEET.ServiceManager.GetTimeEventService(CS.Location);
                oReturn.AddRange(oService.GetTimeElement(CS.CardNo, date1, date2, RecordCount));
            }
            foreach (TimeElement item in oReturn)
            {
                item.EmployeeID = EmployeeID;
            }
            return oReturn;
        }

        public List<CardSetting> GetCardSetting(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.LoadCardSetting(EmployeeID);
        }

        public List<CardSetting> GetCardSetting(string EmployeeID, DateTime CheckDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.LoadCardSetting(EmployeeID, CheckDate);
        }

        public void SaveCardSetting(string EmployeeID, List<CardSetting> cardList)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveCardSetting(EmployeeID, cardList);
        }

        #endregion

        #region " LoadArchiveDate "
        public DateTime LoadArchiveDate(string EmployeeID)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.LoadArchiveDate(EmployeeID);
        }
        #endregion

        #region " LoadArchivePeriod "
        public string LoadArchivePeriod()
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.LoadArchivePeriod();
        }
        #endregion

        #region " LoadTextData "
        public string LoadTextData(string Code, string Language)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.LoadTextData(Code, Language);
        }
        #endregion

        #region " CollisionText "
        public string CollisionText(string Language)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.LoadTextData("COLLISION", Language);
        }
        #endregion

        #region " MatchingClock "
        public List<TimePair> MatchingClock1(string EmployeeID, List<TimeElement> clock)
        {
            return MatchingClock1(EmployeeID, clock, false);
        }
        public List<TimePair> MatchingClock1(string EmployeeID, List<TimeElement> clock, bool FillBlankPair)
        {
            return MatchingClock1(EmployeeID, clock, FillBlankPair, DateTime.MinValue, DateTime.MinValue);
        }

        private static List<TimeElement> GetClock(List<TimeElement> clocks, string door, DateTime begin, DateTime end)
        {
            List<TimeElement> returnValue = new List<TimeElement>();
            List<DoorType> doors = new List<DoorType>();
            if (door.Contains("IN"))
            {
                doors.Add(DoorType.IN);
            }
            if (door.Contains("OUT"))
            {
                doors.Add(DoorType.OUT);
            }

            foreach (TimeElement te in clocks)
            {
                if (doors.Contains(te.DoorType))
                {
                    if (te.EventTime < begin)
                    {
                        continue;
                    }
                    else if (begin <= te.EventTime && te.EventTime < end)
                    {
                        returnValue.Add(te);
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    if (te.EventTime >= end)
                        break;
                }
            }
            return returnValue;
        }

        private static List<TimeElement> GetClock(List<TimeElement> clocks, DateTime begin, DateTime end)
        {
            // CHAT 2011-11-25
            // PTT UT ������ͧ�᡹���� ��������ú͡���� IN/OUT �������ͧ�Ѵ����ͧ�����͡�˹���赡ŧ���
            List<TimeElement> returnValue = new List<TimeElement>();

            foreach (TimeElement te in clocks)
            {
                if (te.EventTime < begin)
                {
                    continue;
                }
                else if (begin <= te.EventTime && te.EventTime < end)
                {
                    returnValue.Add(te);
                }
                else
                {
                    break;
                }
            }
            return returnValue;
        }

        private static void RemoveClock(List<TimeElement> clocks, DateTime last)
        {
            List<TimeElement> delList = new List<TimeElement>();
            foreach (TimeElement te in clocks)
            {
                if (te.EventTime <= last)
                {
                    delList.Add(te);
                }
                else
                    break;
            }
            foreach (TimeElement item in delList)
            {
                clocks.Remove(item);
            }
        }

        private static List<TimePair> MatchStepByStep(List<TimeElement> clocks)
        {
            List<TimePair> pairs = new List<TimePair>();
            List<TimeElement> queue = new List<TimeElement>();

            foreach (TimeElement te in clocks)
            {
                if (te.DoorType == DoorType.IN)
                {
                    queue.Add(te);
                }
                else if (te.DoorType == DoorType.OUT)
                {
                    if (queue.Count == 0)
                    {
                        continue;
                    }
                    TimePair tp = new TimePair();
                    tp.Date = queue[0].EventTime.Date;
                    tp.ClockIN = queue[0];
                    tp.ClockOUT = te;
                    pairs.Add(tp);
                    queue.Clear();
                }
            }

            /*
            if (pairs.Count == 0)
            {
                TimePair tp = new TimePair();
                tp.Date = dws.BeginDate;
                tp.ClockIN = null;
                tp.ClockOUT = null;
                tp.DWSCode = dws.DailyWorkscheduleCode;
                tp.DWS = dws;
                tp.IsDayOff = dws.IsDayOff;
                tp.IsHoliday = dws.IsHoliday;

                pairs.Add(tp);
            }
            */
            return pairs;
        }

        private static List<TimePair> MatchStepByStep_UT(List<TimeElement> clocks, DailyWS curr, bool IsShift, DateTime prev, DateTime next, Dictionary<DateTime, DailyWS> dictDWS)
        {
            List<TimePair> pairs = new List<TimePair>();
            List<TimeElement> queue = new List<TimeElement>();
            int count = 1;
            int elementcount = 1;
            TimePair tp;
            DateTime NRFLStart = curr.BeginDate;
            DateTime NRFLEnd = curr.BeginDate.AddDays(1);
            DateTime SHStart = new DateTime();
            DateTime SHEnd = new DateTime();
            TimeSpan tsBegin = new TimeSpan(4, 0, 0);
            TimeSpan tsEnd = new TimeSpan(3, 59, 0);
            DateTime currentDate = curr.BeginDate;

            DailyWS dws_prev = dictDWS[currentDate.AddDays(-1).Date];
            DailyWS dws_next = dictDWS[currentDate.AddDays(1).Date];

            NRFLStart = NRFLStart.Add(tsBegin);
            NRFLEnd = NRFLEnd.Add(tsEnd);

            if (clocks.Count > 0)
            {
                if (IsShift)  // Shift
                {
                    SHStart = prev;
                    SHEnd = next;

                    #region Case OFF OFF OFF
                    //Case Off Off Off
                    if (dws_prev.IsDayOffOrHoliday && dws_next.IsDayOffOrHoliday)
                    {
                        foreach (TimeElement te in clocks)
                        {
                            if (te.EventTime.Date == currentDate.Date)
                            {
                                if ((count % 2) != 0)    // IN
                                {
                                    queue.Add(te);
                                }
                                else
                                {
                                    tp = new TimePair();
                                    tp.Date = queue[0].EventTime.Date;

                                    if (queue[0].EventTime >= SHStart)
                                        tp.ClockIN = queue[0];
                                    else
                                        tp.ClockIN = null;

                                    if (te.EventTime <= SHEnd)
                                        tp.ClockOUT = te;
                                    else
                                        tp.ClockOUT = null;
                                    if (tp.Date.Date == tp.ClockIN.EventTime.Date)
                                    pairs.Add(tp);
                                    queue.Clear();
                                }
                            }
                            else
                            {
                                if ((count % 2) != 0)    // IN
                                {
                                    queue.Add(te);
                                }
                                else
                                {
                                    tp = new TimePair();
                                    tp.Date = queue[0].EventTime.Date;

                                    if (queue[0].EventTime >= SHStart)
                                        tp.ClockIN = queue[0];
                                    else
                                        tp.ClockIN = null;

                                    if (te.EventTime <= SHEnd)
                                        tp.ClockOUT = te;
                                    else
                                        tp.ClockOUT = null;
                                    if (tp.Date.Date == tp.ClockIN.EventTime.Date)
                                    pairs.Add(tp);
                                    queue.Clear();

                                    count++;
                                    elementcount++;
                                }

                                if (clocks.Count - (elementcount - 1) == 1)
                                    continue;
                                else
                                    break;
                            }
                            count++;
                            elementcount++;
                        }

                        if (queue.Count > 0)
                        {
                            if (queue[0].EventTime.Date <= currentDate.Date)
                            {
                                tp = new TimePair();
                                tp.Date = queue[0].EventTime.Date;

                                if (queue[0].EventTime >= SHStart)
                                    tp.ClockIN = queue[0];
                                else
                                    tp.ClockIN = null;

                                tp.ClockOUT = null;
                                if (tp.Date.Date == tp.ClockIN.EventTime.Date)
                                pairs.Add(tp);
                                queue.Clear();
                            }
                        }
                    }
                    #endregion

                    #region Case OFF OFF WORK
                    //Case Off Off Work
                    if (dws_prev.IsDayOffOrHoliday && !dws_next.IsDayOffOrHoliday)
                    {
                        foreach (TimeElement te in clocks)
                        {
                            if (te.EventTime.Date == currentDate.Date)
                            {
                                if ((count % 2) != 0)    // IN
                                {
                                    queue.Add(te);
                                }
                                else
                                {
                                    tp = new TimePair();
                                    tp.Date = queue[0].EventTime.Date;

                                    if (queue[0].EventTime >= SHStart)
                                        tp.ClockIN = queue[0];
                                    else
                                        tp.ClockIN = null;

                                    if (te.EventTime <= SHEnd)
                                        tp.ClockOUT = te;
                                    else
                                        tp.ClockOUT = null;
                                    if (tp.Date.Date == tp.ClockIN.EventTime.Date)
                                    pairs.Add(tp);
                                    queue.Clear();
                                }

                            }
                            else
                            {
                                if ((count % 2) != 0)    // IN
                                {
                                    queue.Add(te);
                                    count++;
                                }
                                else
                                {
                                    tp = new TimePair();
                                    tp.Date = queue[0].EventTime.Date;

                                    if (queue[0].EventTime >= SHStart)
                                        tp.ClockIN = queue[0];
                                    else
                                        tp.ClockIN = null;

                                    if (te.EventTime <= SHEnd)
                                        tp.ClockOUT = te;
                                    else
                                        tp.ClockOUT = null;
                                    if (tp.Date.Date == tp.ClockIN.EventTime.Date)
                                    pairs.Add(tp);
                                    queue.Clear();

                                    count++;
                                    elementcount++;
                                }

                                if (clocks.Count - (elementcount - 1) == 1 ||
                                    clocks.Exists(delegate(TimeElement _te) { return _te.EventTime.Day == 1; })
                                    )
                                    continue;
                                else
                                    break;
                            }
                            count++;
                            elementcount++;
                        }

                        if (queue.Count > 0)
                        {
                            if (queue[0].EventTime.Date <= currentDate.Date)
                            {
                                if (!clocks.Exists(delegate(TimeElement _te) { return _te.EventTime.Day == 1; }))
                                {
                                    tp = new TimePair();
                                    tp.Date = queue[0].EventTime.Date;

                                    if (queue[0].EventTime >= SHStart)
                                        tp.ClockIN = queue[0];
                                    else
                                        tp.ClockIN = null;

                                    tp.ClockOUT = null;
                                    if (tp.Date.Date == tp.ClockIN.EventTime.Date)
                                    pairs.Add(tp);
                                    queue.Clear();
                                }
                                else
                                {
                                    for (int i = 0; i < queue.Count; i++)
                                    {
                                        tp = new TimePair();
                                        tp.Date = queue[i].EventTime.Date;
                                        tp.ClockIN = queue[i];
                                        if (i + 1 <= queue.Count - 1)
                                            tp.ClockOUT = queue[i + 1];
                                        if (tp.Date.Date == tp.ClockIN.EventTime.Date)
                                        pairs.Add(tp);

                                    }
                                }
                            }
                            else
                            {
                                tp = new TimePair();
                                tp.Date = queue[0].EventTime.AddDays(-1).Date;

                                if (queue[0].EventTime >= SHStart)
                                    tp.ClockIN = queue[0];
                                else
                                    tp.ClockIN = null;

                                tp.ClockOUT = null;
                                if (tp.Date.Date == tp.ClockIN.EventTime.Date)
                                pairs.Add(tp);
                                queue.Clear();
                            }
                        }
                    }
                    #endregion

                    #region Case WORK OFF WORK
                    //Case Work Off Work
                    if (!dws_prev.IsDayOffOrHoliday && !dws_next.IsDayOffOrHoliday)
                    {
                        foreach (TimeElement te in clocks)
                        {
                            if (te.EventTime.Date == currentDate.Date)
                            {
                                if ((count % 2) != 0)    // IN
                                {
                                    queue.Add(te);
                                }
                                else
                                {
                                    tp = new TimePair();
                                    tp.Date = queue[0].EventTime.Date;

                                    if (queue[0].EventTime >= SHStart)
                                        tp.ClockIN = queue[0];
                                    else
                                        tp.ClockIN = null;

                                    if (te.EventTime <= SHEnd)
                                        tp.ClockOUT = te;
                                    else
                                        tp.ClockOUT = null;
                                    if (tp.Date.Date == tp.ClockIN.EventTime.Date)
                                    pairs.Add(tp);
                                    queue.Clear();
                                }
                            }
                            else
                            {
                                if ((count % 2) != 0)    // IN
                                {
                                    queue.Add(te);
                                }
                                else
                                {
                                    tp = new TimePair();
                                    tp.Date = queue[0].EventTime.Date;

                                    if (queue[0].EventTime >= SHStart)
                                        tp.ClockIN = queue[0];
                                    else
                                        tp.ClockIN = null;

                                    if (te.EventTime <= SHEnd)
                                        tp.ClockOUT = te;
                                    else
                                        tp.ClockOUT = null;
                                    if (tp.Date.Date == tp.ClockIN.EventTime.Date)
                                    pairs.Add(tp);
                                    queue.Clear();

                                    count++;
                                    elementcount++;
                                }

                                if (clocks.Count - (elementcount - 1) == 1)
                                    continue;
                                else
                                    break;

                            }
                            count++;
                            elementcount++;
                        }

                        if (queue.Count > 0)
                        {
                            if (queue[0].EventTime.Date <= currentDate.Date)
                            {
                                tp = new TimePair();
                                tp.Date = queue[0].EventTime.Date;

                                if (queue[0].EventTime >= SHStart)
                                    tp.ClockIN = queue[0];
                                else
                                    tp.ClockIN = null;

                                tp.ClockOUT = null;
                                if (tp.Date.Date == tp.ClockIN.EventTime.Date)
                                pairs.Add(tp);
                                queue.Clear();
                            }
                        }
                    }
                    #endregion

                    #region Case WORK OFF OFF
                    //Case Work Off Off
                    if (!dws_prev.IsDayOffOrHoliday && dws_next.IsDayOffOrHoliday)
                    {
                        foreach (TimeElement te in clocks)
                        {
                            if (te.EventTime.Date == currentDate.Date)
                            {
                                if ((count % 2) != 0)    // IN
                                {
                                    queue.Add(te);
                                }
                                else
                                {
                                    tp = new TimePair();
                                    tp.Date = queue[0].EventTime.Date;

                                    if (queue[0].EventTime >= SHStart)
                                        tp.ClockIN = queue[0];
                                    else
                                        tp.ClockIN = null;

                                    if (te.EventTime <= SHEnd)
                                        tp.ClockOUT = te;
                                    else
                                        tp.ClockOUT = null;
                                    if (tp.Date.Date == tp.ClockIN.EventTime.Date)
                                    pairs.Add(tp);
                                    queue.Clear();
                                }
                            }
                            else
                            {
                                if ((count % 2) != 0)    // IN
                                {
                                    queue.Add(te);
                                }
                                else
                                {
                                    tp = new TimePair();
                                    tp.Date = queue[0].EventTime.Date;

                                    if (queue[0].EventTime >= SHStart)
                                        tp.ClockIN = queue[0];
                                    else
                                        tp.ClockIN = null;

                                    if (te.EventTime <= SHEnd)
                                        tp.ClockOUT = te;
                                    else
                                        tp.ClockOUT = null;
                                    if (tp.Date.Date == tp.ClockIN.EventTime.Date)
                                    pairs.Add(tp);
                                    queue.Clear();

                                    count++;
                                    elementcount++;
                                }

                                if (clocks.Count - (elementcount - 1) == 1)
                                    continue;
                                else
                                    break;
                            
                            }
                            count++;
                            elementcount++;
                        }

                        if (queue.Count > 0)
                        {
                            if (queue[0].EventTime.Date <= currentDate.Date)
                            {
                                tp = new TimePair();
                                tp.Date = queue[0].EventTime.Date;

                                if (queue[0].EventTime >= SHStart)
                                    tp.ClockIN = queue[0];
                                else
                                    tp.ClockIN = null;

                                tp.ClockOUT = null;
                                if (tp.Date.Date == tp.ClockIN.EventTime.Date)
                                pairs.Add(tp);
                                queue.Clear();
                            }
                        }
                    }
                    #endregion
                    
                }
                else  // Norm, Flex
                {
                    tp = new TimePair();

                    if (clocks.Count != 0)
                    {
                        if (clocks[0].EventTime >= NRFLStart)
                            tp.ClockIN = clocks[0];
                    }
                    else
                        tp.ClockIN = null;

                    if (clocks.Count >= 2)
                    {
                        if (clocks[clocks.Count - 1].EventTime <= NRFLEnd)
                            tp.ClockOUT = clocks[clocks.Count - 1];
                    }
                    else
                        tp.ClockOUT = null;

                    tp.Date = curr.BeginDate;
                    tp.DWSCode = curr.DailyWorkscheduleCode;
                    tp.DWS = curr;
                    tp.IsDayOff = curr.IsDayOff;
                    tp.IsHoliday = curr.IsHoliday;
                    if (tp.Date.Date == tp.ClockIN.EventTime.Date)
                    pairs.Add(tp);
                }
            }

            return pairs;
        }

        //private static List<TimePair> MatchStepByStep_UT(List<TimeElement> clocks, DailyWS curr, bool IsShift, DateTime prev, DateTime next, Dictionary<DateTime, DailyWS> dictDWS)
        //{
        //    List<TimePair> pairs = new List<TimePair>();
        //    List<TimeElement> queue = new List<TimeElement>();
        //    int count = 1;
        //    int elementcount = 1;
        //    TimePair tp;
        //    DateTime NRFLStart = curr.BeginDate;
        //    DateTime NRFLEnd = curr.BeginDate.AddDays(1);
        //    DateTime SHStart = new DateTime();
        //    DateTime SHEnd = new DateTime();
        //    TimeSpan tsBegin = new TimeSpan(4, 0, 0);
        //    TimeSpan tsEnd = new TimeSpan(3, 59, 0);
        //    DateTime currentDate = curr.BeginDate;

        //    DailyWS dws_prev = dictDWS[currentDate.AddDays(-1).Date];
        //    DailyWS dws_next = dictDWS[currentDate.AddDays(1).Date];

        //    NRFLStart = NRFLStart.Add(tsBegin);
        //    NRFLEnd = NRFLEnd.Add(tsEnd);

        //    if (clocks.Count > 0)
        //    {
        //        if (IsShift)  // Shift
        //        {
        //            SHStart = prev;
        //            SHEnd = next;

        //            #region Case OFF OFF OFF
        //            //Case Off Off Off
        //            if (dws_prev.IsDayOffOrHoliday && dws_next.IsDayOffOrHoliday)
        //            {
        //                foreach (TimeElement te in clocks)
        //                {
        //                    if (te.EventTime.Date == currentDate.Date)
        //                    {
        //                        if ((count % 2) != 0)    // IN
        //                        {
        //                            queue.Add(te);
        //                        }
        //                        else
        //                        {
        //                            tp = new TimePair();
        //                            tp.Date = queue[0].EventTime.Date;

        //                            if (queue[0].EventTime >= SHStart)
        //                                tp.ClockIN = queue[0];
        //                            else
        //                                tp.ClockIN = null;

        //                            if (te.EventTime <= SHEnd)
        //                                tp.ClockOUT = te;
        //                            else
        //                                tp.ClockOUT = null;
        //                            if (tp.Date.Date == tp.ClockIN.EventTime.Date)
        //                                pairs.Add(tp);
        //                            queue.Clear();
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if ((count % 2) != 0)    // IN
        //                        {
        //                            queue.Add(te);
        //                        }
        //                        else
        //                        {
        //                            tp = new TimePair();
        //                            tp.Date = queue[0].EventTime.Date;

        //                            if (queue[0].EventTime >= SHStart)
        //                                tp.ClockIN = queue[0];
        //                            else
        //                                tp.ClockIN = null;

        //                            if (te.EventTime <= SHEnd)
        //                                tp.ClockOUT = te;
        //                            else
        //                                tp.ClockOUT = null;
        //                            if (tp.Date.Date == tp.ClockIN.EventTime.Date)
        //                                pairs.Add(tp);
        //                            queue.Clear();

        //                            count++;
        //                            elementcount++;
        //                        }

        //                        if (clocks.Count - (elementcount - 1) == 1)
        //                            continue;
        //                        else
        //                            break;
        //                    }
        //                    count++;
        //                    elementcount++;
        //                }

        //                if (queue.Count > 0)
        //                {
        //                    if (queue[0].EventTime.Date <= currentDate.Date)
        //                    {
        //                        tp = new TimePair();
        //                        tp.Date = queue[0].EventTime.Date;

        //                        if (queue[0].EventTime >= SHStart)
        //                            tp.ClockIN = queue[0];
        //                        else
        //                            tp.ClockIN = null;

        //                        tp.ClockOUT = null;
        //                        if (tp.Date.Date == tp.ClockIN.EventTime.Date)
        //                            pairs.Add(tp);
        //                        queue.Clear();
        //                    }
        //                }
        //            }
        //            #endregion

        //            #region Case OFF OFF WORK
        //            //Case Off Off Work
        //            if (dws_prev.IsDayOffOrHoliday && !dws_next.IsDayOffOrHoliday)
        //            {
        //                foreach (TimeElement te in clocks)
        //                {
        //                    if (te.EventTime.Date == currentDate.Date)
        //                    {
        //                        if ((count % 2) != 0)    // IN
        //                        {
        //                            queue.Add(te);
        //                        }
        //                        else
        //                        {
        //                            tp = new TimePair();
        //                            tp.Date = queue[0].EventTime.Date;

        //                            if (queue[0].EventTime >= SHStart)
        //                                tp.ClockIN = queue[0];
        //                            else
        //                                tp.ClockIN = null;

        //                            if (te.EventTime <= SHEnd)
        //                                tp.ClockOUT = te;
        //                            else
        //                                tp.ClockOUT = null;
        //                            if (tp.Date.Date == tp.ClockIN.EventTime.Date)
        //                                pairs.Add(tp);
        //                            queue.Clear();
        //                        }

        //                    }
        //                    else
        //                    {
        //                        if ((count % 2) != 0)    // IN
        //                        {
        //                            queue.Add(te);
        //                        }
        //                        else
        //                        {
        //                            tp = new TimePair();
        //                            tp.Date = queue[0].EventTime.Date;

        //                            if (queue[0].EventTime >= SHStart)
        //                                tp.ClockIN = queue[0];
        //                            else
        //                                tp.ClockIN = null;

        //                            if (te.EventTime <= SHEnd)
        //                                tp.ClockOUT = te;
        //                            else
        //                                tp.ClockOUT = null;
        //                            if (tp.Date.Date == tp.ClockIN.EventTime.Date)
        //                                pairs.Add(tp);
        //                            queue.Clear();

        //                            count++;
        //                            elementcount++;
        //                        }

        //                        if (clocks.Count - (elementcount - 1) == 1 ||
        //                            clocks.Exists(delegate(TimeElement _te) { return _te.EventTime.Day == 1; })
        //                            )
        //                            continue;
        //                        else
        //                            break;
        //                    }
        //                    count++;
        //                    elementcount++;
        //                }

        //                if (queue.Count > 0)
        //                {
        //                    if (queue[0].EventTime.Date <= currentDate.Date)
        //                    {
        //                        if (!clocks.Exists(delegate(TimeElement _te) { return _te.EventTime.Day == 1; }))
        //                        {
        //                            tp = new TimePair();
        //                            tp.Date = queue[0].EventTime.Date;

        //                            if (queue[0].EventTime >= SHStart)
        //                                tp.ClockIN = queue[0];
        //                            else
        //                                tp.ClockIN = null;

        //                            tp.ClockOUT = null;
        //                            if (tp.Date.Date == tp.ClockIN.EventTime.Date)
        //                                pairs.Add(tp);
        //                            queue.Clear();
        //                        }
        //                        else
        //                        {
        //                            for (int i = 0; i < queue.Count; i++)
        //                            {
        //                                tp = new TimePair();
        //                                tp.Date = queue[i].EventTime.Date;
        //                                tp.ClockIN = queue[i];
        //                                if (i + 1 <= queue.Count - 1)
        //                                    tp.ClockOUT = queue[i + 1];
        //                                if (tp.Date.Date == tp.ClockIN.EventTime.Date)
        //                                    pairs.Add(tp);

        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        tp = new TimePair();
        //                        tp.Date = queue[0].EventTime.AddDays(-1).Date;

        //                        if (queue[0].EventTime >= SHStart)
        //                            tp.ClockIN = queue[0];
        //                        else
        //                            tp.ClockIN = null;

        //                        tp.ClockOUT = null;
        //                        if (tp.Date.Date == tp.ClockIN.EventTime.Date)
        //                            pairs.Add(tp);
        //                        queue.Clear();
        //                    }
        //                }
        //            }
        //            #endregion

        //            #region Case WORK OFF WORK
        //            //Case Work Off Work
        //            if (!dws_prev.IsDayOffOrHoliday && !dws_next.IsDayOffOrHoliday)
        //            {
        //                foreach (TimeElement te in clocks)
        //                {
        //                    if (te.EventTime.Date == currentDate.Date)
        //                    {
        //                        if ((count % 2) != 0)    // IN
        //                        {
        //                            queue.Add(te);
        //                        }
        //                        else
        //                        {
        //                            tp = new TimePair();
        //                            tp.Date = queue[0].EventTime.Date;

        //                            if (queue[0].EventTime >= SHStart)
        //                                tp.ClockIN = queue[0];
        //                            else
        //                                tp.ClockIN = null;

        //                            if (te.EventTime <= SHEnd)
        //                                tp.ClockOUT = te;
        //                            else
        //                                tp.ClockOUT = null;
        //                            if (tp.Date.Date == tp.ClockIN.EventTime.Date)
        //                                pairs.Add(tp);
        //                            queue.Clear();
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if ((count % 2) != 0)    // IN
        //                        {
        //                            queue.Add(te);
        //                        }
        //                        else
        //                        {
        //                            tp = new TimePair();
        //                            tp.Date = queue[0].EventTime.Date;

        //                            if (queue[0].EventTime >= SHStart)
        //                                tp.ClockIN = queue[0];
        //                            else
        //                                tp.ClockIN = null;

        //                            if (te.EventTime <= SHEnd)
        //                                tp.ClockOUT = te;
        //                            else
        //                                tp.ClockOUT = null;
        //                            if (tp.Date.Date == tp.ClockIN.EventTime.Date)
        //                                pairs.Add(tp);
        //                            queue.Clear();

        //                            count++;
        //                            elementcount++;
        //                        }

        //                        if (clocks.Count - (elementcount - 1) == 1)
        //                            continue;
        //                        else
        //                            break;

        //                    }
        //                    count++;
        //                    elementcount++;
        //                }

        //                if (queue.Count > 0)
        //                {
        //                    if (queue[0].EventTime.Date <= currentDate.Date)
        //                    {
        //                        tp = new TimePair();
        //                        tp.Date = queue[0].EventTime.Date;

        //                        if (queue[0].EventTime >= SHStart)
        //                            tp.ClockIN = queue[0];
        //                        else
        //                            tp.ClockIN = null;

        //                        tp.ClockOUT = null;
        //                        if (tp.Date.Date == tp.ClockIN.EventTime.Date)
        //                            pairs.Add(tp);
        //                        queue.Clear();
        //                    }
        //                }
        //            }
        //            #endregion

        //            #region Case WORK OFF OFF
        //            //Case Work Off Off
        //            if (!dws_prev.IsDayOffOrHoliday && dws_next.IsDayOffOrHoliday)
        //            {
        //                foreach (TimeElement te in clocks)
        //                {
        //                    if (te.EventTime.Date == currentDate.Date)
        //                    {
        //                        if ((count % 2) != 0)    // IN
        //                        {
        //                            queue.Add(te);
        //                        }
        //                        else
        //                        {
        //                            tp = new TimePair();
        //                            tp.Date = queue[0].EventTime.Date;

        //                            if (queue[0].EventTime >= SHStart)
        //                                tp.ClockIN = queue[0];
        //                            else
        //                                tp.ClockIN = null;

        //                            if (te.EventTime <= SHEnd)
        //                                tp.ClockOUT = te;
        //                            else
        //                                tp.ClockOUT = null;
        //                            if (tp.Date.Date == tp.ClockIN.EventTime.Date)
        //                                pairs.Add(tp);
        //                            queue.Clear();
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if ((count % 2) != 0)    // IN
        //                        {
        //                            queue.Add(te);
        //                        }
        //                        else
        //                        {
        //                            tp = new TimePair();
        //                            tp.Date = queue[0].EventTime.Date;

        //                            if (queue[0].EventTime >= SHStart)
        //                                tp.ClockIN = queue[0];
        //                            else
        //                                tp.ClockIN = null;

        //                            if (te.EventTime <= SHEnd)
        //                                tp.ClockOUT = te;
        //                            else
        //                                tp.ClockOUT = null;
        //                            if (tp.Date.Date == tp.ClockIN.EventTime.Date)
        //                                pairs.Add(tp);
        //                            queue.Clear();

        //                            count++;
        //                            elementcount++;
        //                        }

        //                        if (clocks.Count - (elementcount - 1) == 1)
        //                            continue;
        //                        else
        //                            break;

        //                    }
        //                    count++;
        //                    elementcount++;
        //                }

        //                if (queue.Count > 0)
        //                {
        //                    if (queue[0].EventTime.Date <= currentDate.Date)
        //                    {
        //                        tp = new TimePair();
        //                        tp.Date = queue[0].EventTime.Date;

        //                        if (queue[0].EventTime >= SHStart)
        //                            tp.ClockIN = queue[0];
        //                        else
        //                            tp.ClockIN = null;

        //                        tp.ClockOUT = null;
        //                        if (tp.Date.Date == tp.ClockIN.EventTime.Date)
        //                            pairs.Add(tp);
        //                        queue.Clear();
        //                    }
        //                }
        //            }
        //            #endregion

        //        }
        //        else  // Norm, Flex
        //        {
        //            tp = new TimePair();

        //            if (clocks.Count != 0)
        //            {
        //                if (clocks[0].EventTime >= NRFLStart)
        //                    tp.ClockIN = clocks[0];
        //            }
        //            else
        //                tp.ClockIN = null;

        //            if (clocks.Count >= 2)
        //            {
        //                if (clocks[clocks.Count - 1].EventTime <= NRFLEnd)
        //                    tp.ClockOUT = clocks[clocks.Count - 1];
        //            }
        //            else
        //                tp.ClockOUT = null;

        //            tp.Date = curr.BeginDate;
        //            tp.DWSCode = curr.DailyWorkscheduleCode;
        //            tp.DWS = curr;
        //            tp.IsDayOff = curr.IsDayOff;
        //            tp.IsHoliday = curr.IsHoliday;
        //            if (tp.Date.Date == tp.ClockIN.EventTime.Date)
        //                pairs.Add(tp);
        //        }
        //    }

        //    return pairs;
        //}

        private static List<TimePair> MatchFILO(List<TimeElement> ins, List<TimeElement> outs, DailyWS dws)
        {
            List<TimePair> pairs = new List<TimePair>();
            TimePair tp = new TimePair();
            tp.Date = dws.BeginDate;
            tp.ClockIN = ins.Count == 0 ? null : ins[0];
            tp.ClockOUT = outs.Count == 0 ? null : outs[outs.Count - 1];
            tp.DWSCode = dws.DailyWorkscheduleCode;
            tp.DWS = dws;
            tp.IsDayOff = dws.IsDayOff;
            tp.IsHoliday = dws.IsHoliday;
            pairs.Add(tp);
            return pairs;
        }

        private static List<TimePair> MatchFILO_UT(List<TimeElement> time_elment, DailyWS prev, DailyWS curr, DailyWS next)
        {
            List<TimePair> pairs = new List<TimePair>();
            TimePair tp;
            DateTime NRFLStart = curr.BeginDate;
            DateTime NRFLEnd = next.BeginDate;
            DateTime SHStart = new DateTime();
            DateTime SHEnd = new DateTime();
            TimeSpan tsBegin = new TimeSpan(4, 0, 0);
            TimeSpan tsEnd = new TimeSpan(3, 59, 0);

            NRFLStart = NRFLStart.Add(tsBegin);
            NRFLEnd = NRFLEnd.Add(tsEnd);

            if (time_elment.Count > 0)
            {
                if (!curr.DailyWorkscheduleCode.StartsWith("S"))     // Case NORM / FLEX
                {
                    tp = new TimePair();
                    tp.Date = curr.BeginDate;

                    if (time_elment.Count != 0)
                    {
                        if (time_elment[0].EventTime >= NRFLStart)
                            tp.ClockIN = time_elment[0];
                        else
                            tp.ClockIN = null;
                    }
                    else
                        tp.ClockIN = null;

                    if (time_elment.Count >= 2)
                    {
                        if (time_elment[time_elment.Count - 1].EventTime <= NRFLEnd)
                            tp.ClockOUT = time_elment[time_elment.Count - 1];
                        else
                            tp.ClockOUT = null;
                    }
                    else
                        tp.ClockOUT = null;

                    tp.DWSCode = curr.DailyWorkscheduleCode;
                    tp.DWS = curr;
                    tp.IsDayOff = curr.IsDayOff;
                    tp.IsHoliday = curr.IsHoliday;
                    if (tp.Date.Date == tp.ClockIN.EventTime.Date)
                        pairs.Add(tp);
                }
                else
                {
                    // Shift
                    // Find Rang of Shift Time Archive
                    if (!curr.IsDayOffOrHoliday) // current is work
                    {
                        TimeSpan ts = new TimeSpan(6, 0, 0);
                        DateTime c_ts, p_ts, n_ts;
                        double min = 0.0;
                        int counter = 0;

                        if (!prev.IsDayOffOrHoliday && !next.IsDayOffOrHoliday) // Case work work work
                        {
                            //SHStart =     Begin time(cur) � ((Begin time (cur) � End time(prv))  / 2) 
                            //SHEnd =     End time(cur)+ ((Begin time (next) � End time(cur) ) / 2)
                            c_ts = new DateTime();
                            p_ts = new DateTime();
                            c_ts = curr.BeginDate;
                            c_ts = c_ts.Add(curr.WorkBeginTime);
                            p_ts = prev.EndDate;
                            p_ts = p_ts.Add(prev.WorkEndTime);

                            SHStart = curr.BeginDate;
                            SHStart = SHStart.Add(c_ts.Subtract(p_ts));
                            if (SHStart.TimeOfDay.TotalHours > 0)
                            {
                                min = (SHStart.TimeOfDay.TotalHours / 2);
                                SHStart = SHStart.Subtract(SHStart.TimeOfDay);
                            }
                            else
                            {
                                min = (c_ts.Subtract(p_ts).TotalHours / 2);
                                SHStart = SHStart.Subtract(c_ts.Subtract(p_ts));
                            }
                            
                            SHStart = SHStart.Add(curr.WorkBeginTime.Subtract(TimeSpan.FromHours(min)));

                            c_ts = new DateTime();
                            n_ts = new DateTime();
                            c_ts = curr.EndDate;
                            c_ts = c_ts.Add(curr.WorkEndTime);
                            n_ts = next.BeginDate;
                            n_ts = n_ts.Add(next.WorkBeginTime);

                            SHEnd = curr.EndDate;
                            SHEnd = SHEnd.Add(n_ts.Subtract(c_ts));
                            min = 0.0;
                            if (SHEnd.TimeOfDay.TotalHours > 0)
                            {
                                min = (SHEnd.TimeOfDay.TotalHours / 2);
                                SHEnd = SHEnd.Subtract(SHEnd.TimeOfDay);
                            }
                            else
                            {
                                min = (n_ts.Subtract(c_ts).TotalHours / 2);
                                SHEnd = SHEnd.Subtract(n_ts.Subtract(c_ts));
                            }
                            SHEnd = SHEnd.Subtract(SHEnd.TimeOfDay);
                            SHEnd = SHEnd.Add(curr.WorkEndTime.Add(TimeSpan.FromHours(min)));
                        }
                        else if (prev.IsDayOffOrHoliday && !next.IsDayOffOrHoliday) // Case off work work
                        {
                            //SHStart = Begin time(cur) � 6
                            //SHEnd =   End time(cur) + ((Begin time (next) � End time(cur) ) / 2)

                            c_ts = new DateTime();
                            c_ts = curr.BeginDate;
                            c_ts = c_ts.Add(curr.WorkBeginTime);
                            c_ts = c_ts.Subtract(ts);

                            SHStart = curr.BeginDate;
                            SHStart = SHStart.Add(c_ts.TimeOfDay);

                            c_ts = new DateTime();
                            n_ts = new DateTime();
                            c_ts = curr.EndDate;
                            c_ts = c_ts.Add(curr.WorkEndTime);
                            n_ts = next.BeginDate;
                            n_ts = n_ts.Add(next.WorkBeginTime);

                            SHEnd = curr.EndDate;
                            SHEnd = SHEnd.Add(n_ts.Subtract(c_ts));
                            min = 0.0;
                            if (SHEnd.TimeOfDay.TotalHours > 0)
                            {
                                min = (SHEnd.TimeOfDay.TotalHours / 2);
                                SHEnd = SHEnd.Subtract(SHEnd.TimeOfDay);
                            }
                            else
                            {
                                min = (n_ts.Subtract(c_ts).TotalHours / 2);
                                SHEnd = SHEnd.Subtract(n_ts.Subtract(c_ts));
                            }
                            SHEnd = SHEnd.Add(curr.WorkEndTime.Add(TimeSpan.FromHours(min)));
                        }
                        else if (!prev.IsDayOffOrHoliday && next.IsDayOffOrHoliday) // Case work work off
                        {
                            //SHStart =  Begin time(cur) � ((Begin time (cur.) � End time(prv))  / 2) 
                            //SHEnd =  End time(cur) + 6 

                            c_ts = new DateTime();
                            p_ts = new DateTime();
                            c_ts = curr.BeginDate;
                            c_ts = c_ts.Add(curr.WorkBeginTime);
                            p_ts = prev.EndDate;
                            p_ts = p_ts.Add(prev.WorkEndTime);

                            SHStart = c_ts.Date;
                            SHStart = SHStart.Add(c_ts.Subtract(p_ts));
                            if (SHStart.TimeOfDay.TotalHours > 0)
                            {
                                min = (SHStart.TimeOfDay.TotalHours / 2);
                                SHStart = SHStart.Subtract(SHStart.TimeOfDay);
                            }
                            else
                            {
                                min = (c_ts.Subtract(p_ts).TotalHours / 2);
                                SHStart = SHStart.Subtract(c_ts.Subtract(p_ts));
                            }
                            SHStart = SHStart.Add(curr.WorkBeginTime.Subtract(TimeSpan.FromHours(min)));

                            SHEnd = curr.EndDate;
                            SHEnd = SHEnd.Add(curr.WorkEndTime + ts);
                        }
                        else if (prev.IsDayOffOrHoliday && next.IsDayOffOrHoliday)  // Case off work off       
                        {
                            //SHStart =  Begin time(cur) � 6                                                                    
                            //SHEnd =  End time(cur) + 6 

                            SHStart = curr.BeginDate;
                            SHStart = SHStart.Add(curr.WorkBeginTime - ts);

                            SHEnd = curr.EndDate;
                            SHEnd = SHEnd.Add(curr.WorkEndTime + ts);
                        }

                        tp = new TimePair();
                        tp.Date = curr.BeginDate;

                        if (time_elment.Count != 0)
                        {
                            foreach (TimeElement te in time_elment)
                            {
                                if (te.EventTime >= SHStart)
                                {
                                    tp.ClockIN = te;
                                    break;
                                }

                                counter++;
                            }

                            for (int i = counter + 1; i < time_elment.Count; i++)
                            {
                                if (time_elment[time_elment.Count - 1].EventTime <= SHEnd)
                                {
                                    tp.ClockOUT = time_elment[time_elment.Count - 1];
                                    break;
                                }
                            }

                            tp.DWSCode = curr.DailyWorkscheduleCode;
                            tp.DWS = curr;
                            tp.IsDayOff = curr.IsDayOff;
                            tp.IsHoliday = curr.IsHoliday;
                            if (tp.Date.Date == tp.ClockIN.EventTime.Date)
                            pairs.Add(tp);
                        }

                    }
                }
            }

            return pairs;
        }

        public List<TimePair> MatchingClock1(string EmployeeID, List<TimeElement> clocks, bool FillBlankPair, DateTime BeginDate, DateTime EndDate)
        {
            return MatchingClock1(EmployeeID, clocks, FillBlankPair, BeginDate, EndDate, new Dictionary<DateTime, string>());
        }

        public List<TimePair> MatchingClock1(string EmployeeID, List<TimeElement> clocks, bool FillBlankPair, DateTime BeginDate, DateTime EndDate,Dictionary<DateTime,string> optionAttendance)
        {
            List<TimePair> matches = new List<TimePair>();
            const double ATLEAST_FREETIME = 4.0;
            Dictionary<DateTime, DailyWS> dictDWS = new Dictionary<DateTime, DailyWS>();

            //Dictionary<string, MonthlyWS> mwsCache = new Dictionary<string, MonthlyWS>();

            // Not allow empty clock
            if (clocks.Count == 0)
            {
                return new List<TimePair>();
            }

            // Sort 
            TimeElementSortingTime oSortingCriteria = new TimeElementSortingTime();
            clocks.Sort(oSortingCriteria);

            // Clone to new list. the clocks parameter not effect
            clocks = new List<TimeElement>(clocks);

            DateTime beginDate = BeginDate != DateTime.MinValue ? BeginDate : clocks[0].EventTime.Date;
            DateTime endDate = EndDate != DateTime.MinValue ? EndDate : clocks[clocks.Count - 1].EventTime.Date;

            // find Daily Work Schedule in this period
            for (DateTime runningDate = beginDate.AddDays(-1); runningDate <= endDate.AddDays(1); runningDate = runningDate.AddDays(1))
            {
                EmployeeData empData = new EmployeeData(EmployeeID, runningDate.Date);
                DailyWS dws = empData.GetDailyWorkSchedule(runningDate.Date,true);

                if (optionAttendance.ContainsKey(runningDate.Date))
                {
                    dws.WorkscheduleClass = optionAttendance[runningDate.Date];
                }

                dws.BeginDate = runningDate;

                // fill BeginDate and EndDate.
                // EndDate is after BeginDate one day, if DWS is over midnight.
                if (dws.IsDayOffOrHoliday)
                {
                    dws.EndDate = runningDate;
                }
                else
                {
                    if (dws.WorkBeginTime <= dws.WorkEndTime)
                    {
                        dws.EndDate = runningDate;
                    }
                    else
                    {
                        dws.EndDate = runningDate.AddDays(1);
                    }
                }
                dictDWS[runningDate] = dws;
            }

            List<DailyWS> offs = new List<DailyWS>();

            // process daily day
            DailyWS beforeOff = null;
            DailyWS afterOff = null;
            for (DateTime cdate = beginDate; cdate <= endDate; cdate = cdate.AddDays(1))
            {
                DailyWS previous = dictDWS[cdate.AddDays(-1)];
                DailyWS current = dictDWS[cdate];
                DailyWS next = dictDWS[cdate.AddDays(1)];

                if (optionAttendance.ContainsKey(cdate.AddDays(-1).Date))
                    previous.WorkscheduleClass = optionAttendance[cdate.AddDays(-1).Date];
                if (optionAttendance.ContainsKey(cdate.AddDays(1).Date))
                    next.WorkscheduleClass = optionAttendance[cdate.AddDays(1).Date];

                bool IsShift = false;

                List<TimeElement> ins, outs, inouts, DailyArchive;

                for (DateTime c_date = beginDate; c_date <= endDate; c_date = c_date.AddDays(1))
                {
                    if (dictDWS[c_date].DailyWorkscheduleCode == "OFF")
                        continue;

                    if (dictDWS[c_date].DailyWorkscheduleCode.StartsWith("S"))
                    {
                        IsShift = true;
                        break;
                    }
                }

                if (current.IsDayOffOrHoliday)
                {
                    beforeOff = beforeOff == null ? previous : beforeOff;

                    // match clock in day off
                    DateTime begin, end;

                    afterOff = next;

                    if (IsShift)
                    {
                        //Shift

                        //Find previous work
                        for (DateTime p_date = beforeOff.EndDate; p_date >= beginDate; p_date = p_date.AddDays(-1))
                        {
                            if (dictDWS[p_date].IsDayOffOrHoliday)
                                continue;
                            else
                            {
                                beforeOff = dictDWS[p_date];
                                break;
                            }
                        }

                        //Find next work
                        for (DateTime n_date = afterOff.BeginDate; n_date <= endDate; n_date = n_date.AddDays(1))
                        {
                            if (dictDWS[n_date].IsDayOffOrHoliday)
                                continue;
                            else
                            {
                                afterOff = dictDWS[n_date];
                                break;
                            }
                        }

                        if (beforeOff.IsDayOffOrHoliday)
                            begin = beforeOff.EndDate;
                        else
                            begin = beforeOff.EndDate.Add(beforeOff.WorkEndTime.Add(new TimeSpan(6, 0, 0)));

                        if (afterOff.IsDayOffOrHoliday)
                            end = afterOff.BeginDate;
                        else
                            end = afterOff.BeginDate.Add(afterOff.WorkBeginTime.Subtract(new TimeSpan(6, 0, 0)));

                        //begin = current.BeginDate.Subtract(new TimeSpan(6, 0, 0));
                        //end = current.EndDate.AddDays(1).Add(new TimeSpan(6, 0, 0));
                    }
                    else 
                    {
                        //Norm and Flex
                        begin = current.BeginDate.Add(new TimeSpan(4,0,0));
                        end = afterOff.BeginDate.Add(new TimeSpan(3, 59, 0));
                    }

                    inouts = GetClock(clocks, begin, end);

                    matches.AddRange(MatchStepByStep_UT(inouts, current, IsShift, begin, end, dictDWS));
                    beforeOff = null;
                    afterOff = null;
                    //}
                }
                else
                {
                    DateTime begin = new DateTime();
                    DateTime end = new DateTime(); 
                    TimeSpan ts = new TimeSpan(6, 0, 0);
                    DateTime c_ts, p_ts, n_ts;
                    double min = 0.0;

                    if (IsShift)
                    {
                        // find list of in out For UT
                        if (!previous.IsDayOffOrHoliday && !next.IsDayOffOrHoliday) // Case work work work
                        {
                            //SHStart =     Begin time(cur) � ((Begin time (cur) � End time(prv))  / 2) 
                            //SHEnd =     End time(cur)+ ((Begin time (next) � End time(cur) ) / 2)
                            c_ts = new DateTime();
                            p_ts = new DateTime();
                            c_ts = current.BeginDate;
                            c_ts = c_ts.Add(current.WorkBeginTime);
                            p_ts = previous.EndDate;
                            p_ts = p_ts.Add(previous.WorkEndTime);

                            begin = current.BeginDate;
                            begin = begin.Add(c_ts.Subtract(p_ts));
                            if (begin.TimeOfDay.TotalHours > 0)
                            {
                                min = (begin.TimeOfDay.TotalHours / 2);
                                begin = begin.Subtract(begin.TimeOfDay);
                            }
                            else
                            {
                                min = (c_ts.Subtract(p_ts).TotalHours / 2);
                                begin = begin.Subtract(c_ts.Subtract(p_ts));
                            }
                            
                            begin = begin.Add(current.WorkBeginTime.Subtract(TimeSpan.FromHours(min)));

                            c_ts = new DateTime();
                            n_ts = new DateTime();
                            c_ts = current.EndDate;
                            c_ts = c_ts.Add(current.WorkEndTime);
                            n_ts = next.BeginDate;
                            n_ts = n_ts.Add(next.WorkBeginTime);

                            end = current.EndDate;
                            end = end.Add(n_ts.Subtract(c_ts));
                            min = 0.0;
                            if (end.TimeOfDay.TotalHours > 0)
                            {
                                min = (end.TimeOfDay.TotalHours / 2);
                                end = end.Subtract(end.TimeOfDay);
                            }
                            else
                            {
                                min = (n_ts.Subtract(c_ts).TotalHours / 2);
                                end = end.Subtract(n_ts.Subtract(c_ts));
                            }
                            
                            end = end.Add(current.WorkEndTime.Add(TimeSpan.FromHours(min)));
                        }
                        else if (previous.IsDayOffOrHoliday && !next.IsDayOffOrHoliday) // Case off work work
                        {
                            //SHStart = Begin time(cur) � 6
                            //SHEnd =   End time(cur) + ((Begin time (next) � End time(cur) ) / 2)

                            c_ts = new DateTime();
                            c_ts = current.BeginDate;
                            c_ts = c_ts.Add(current.WorkBeginTime);
                            c_ts = c_ts.Subtract(ts);

                            begin = current.BeginDate;
                            begin = begin.Add(c_ts.TimeOfDay);

                            c_ts = new DateTime();
                            n_ts = new DateTime();
                            c_ts = current.EndDate;
                            c_ts = c_ts.Add(current.WorkEndTime);
                            n_ts = next.BeginDate;
                            n_ts = n_ts.Add(next.WorkBeginTime);

                            end = current.EndDate;
                            end = end.Add(n_ts.Subtract(c_ts));
                            min = 0.0;
                            if (end.TimeOfDay.TotalHours > 0)
                            {
                                min = (end.TimeOfDay.TotalHours / 2);
                                end = end.Subtract(end.TimeOfDay);
                            }
                            else
                            {
                                min = (n_ts.Subtract(c_ts).TotalHours / 2);
                                end = end.Subtract(n_ts.Subtract(c_ts));
                            }
                            
                            end = end.Add(current.WorkEndTime.Add(TimeSpan.FromHours(min)));
                        }
                        else if (!previous.IsDayOffOrHoliday && next.IsDayOffOrHoliday) // Case work work off
                        {
                            //SHStart =  Begin time(cur) � ((Begin time (cur.) � End time(prv))  / 2) 
                            //SHEnd =  End time(cur) + 6 

                            c_ts = new DateTime();
                            p_ts = new DateTime();
                            c_ts = current.BeginDate;
                            c_ts = c_ts.Add(current.WorkBeginTime);
                            p_ts = previous.EndDate;
                            p_ts = p_ts.Add(previous.WorkEndTime);

                            begin = c_ts.Date;
                            begin = begin.Add(c_ts.Subtract(p_ts));
                            if (begin.TimeOfDay.TotalHours > 0)
                            {
                                min = (begin.TimeOfDay.TotalHours / 2);
                                begin = begin.Subtract(begin.TimeOfDay);
                            }
                            else
                            {
                                min = (c_ts.Subtract(p_ts).TotalHours / 2);
                                begin = begin.Subtract(c_ts.Subtract(p_ts));
                            }
                            begin = begin.Add(current.WorkBeginTime.Subtract(TimeSpan.FromHours(min)));

                            end = current.EndDate;
                            end = end.Add(current.WorkEndTime + ts);
                        }
                        else if (previous.IsDayOffOrHoliday && next.IsDayOffOrHoliday)  // Case off work off       
                        {
                            //SHStart =  Begin time(cur) � 6                                                                    
                            //SHEnd =  End time(cur) + 6 

                            begin = current.BeginDate;
                            begin = begin.Add(current.WorkBeginTime - ts);

                            end = current.EndDate;
                            end = end.Add(current.WorkEndTime + ts);
                        }
                    }
                    else
                    {
                        begin = current.BeginDate.Add(new TimeSpan(4, 0, 0));
                        end = next.BeginDate.Add(new TimeSpan(3, 59, 0));
                    }

                    DailyArchive = new List<TimeElement>();
                    DailyArchive = GetClock(clocks,begin,end);

                    matches.AddRange(MatchFILO_UT(DailyArchive, previous, current, next));
                }
                if (matches.Count > 0)
                {
                    if (matches[matches.Count - 1].ClockOUT != null)
                    {
                        DateTime last = matches[matches.Count - 1].ClockOUT.EventTime;
                        RemoveClock(clocks, last);
                    }
                    else if (matches[matches.Count - 1].ClockIN != null)
                    {
                        DateTime last = matches[matches.Count - 1].ClockIN.EventTime;
                        RemoveClock(clocks, last);
                    }
                }
            }

            // Fill dws and group by date
            Dictionary<DateTime, List<TimePair>> grouppair = new Dictionary<DateTime, List<TimePair>>();
            foreach (TimePair tp in matches)
            {
                DailyWS dws = dictDWS[tp.Date];
                tp.DWSCode = dws.DailyWorkscheduleCode;
                tp.DWS = dws;
                tp.IsDayOff = dws.IsDayOff;
                tp.IsHoliday = dws.IsHoliday;
                tp.EmployeeID = EmployeeID;
                if (!grouppair.ContainsKey(tp.Date))
                {
                    grouppair[tp.Date] = new List<TimePair>();
                }
                grouppair[tp.Date].Add(tp);
            }

            // Rebuild a list and insert dummy timepair 
            matches.Clear();
            for (DateTime cdate = beginDate; cdate < endDate.AddDays(1); cdate = cdate.AddDays(1))
            {
                if (!grouppair.ContainsKey(cdate))
                {
                    grouppair[cdate] = new List<TimePair>();
                    DailyWS dws = dictDWS[cdate];
                    TimePair tp = new TimePair();
                    tp.Date = dws.BeginDate;
                    tp.ClockIN = null;
                    tp.ClockOUT = null;
                    tp.DWSCode = dws.DailyWorkscheduleCode;
                    tp.DWS = dws;
                    tp.IsDayOff = dws.IsDayOff;
                    tp.IsHoliday = dws.IsHoliday;
                    tp.EmployeeID = EmployeeID;
                    grouppair[cdate].Add(tp);
                }
                matches.AddRange(grouppair[cdate]);
            }

            //AddBy: Ratchatawan W. (2012-11-20)
            //Merge between MatchingClockRealTime with Table TimePairArchiveMappingByUser
            List<TimePair> oTimePairList = ServiceManager.CreateInstance(CompanyCode).ESSData.LoadTimePairArchiveMappingByUser(EmployeeID, BeginDate, EndDate);
            TimePair MatchTimePair;
            foreach (TimePair oTimePair in oTimePairList)
            {
                if (matches.Exists(delegate(TimePair otp) { return otp.Date == oTimePair.Date; }))
                {
                    MatchTimePair = matches.Find(delegate(TimePair otp) { return otp.Date == oTimePair.Date; });
                    if (oTimePair.ClockIN != null)
                        MatchTimePair.ClockIN.EventTime = oTimePair.ClockIN.EventTime;
                    if (oTimePair.ClockOUT != null)
                        MatchTimePair.ClockOUT.EventTime = oTimePair.ClockOUT.EventTime;

                }
            }
            return matches;
        }

        //AddBy: Ratchatawan W. (2012-11-23)
        public List<TimePair> MatchingClockNew(string EmployeeID, List<TimeElement> TimeElementList, bool FillBlankPair, DateTime BeginDate, DateTime EndDate, Dictionary<DateTime, string> optionAttendance)
        {
            List<TimePair> MatchTimePair = new List<TimePair>();

            ////��� TimeElement �� 0 �� Return List Timepair ��ҧ��Ѻ�
            //if (TimeElementList.Count == 0)
            //    return new List<TimePair>();

            
            //���§�ӴѺ TimeElement ���� 
            TimeElementList.Sort(new TimeElementSortingTime());

            //�ѹ�����������������ش��èѺ�������
            //�ҡ BeginDate �դ����ҡѺ DateTime.MinValue �����ѹ����á� TimeElementList ᷹
            //�ҡ EndDate �դ����ҡѺ DateTime.MinValue �����ѹ����ش����� TimeElementList ᷹
            DateTime oBeginDate = BeginDate != DateTime.MinValue ? BeginDate : TimeElementList[0].EventTime.Date;
            DateTime oEndDate = EndDate != DateTime.MinValue ? EndDate : TimeElementList[TimeElementList.Count - 1].EventTime.Date;

            DateTime oRunningDate = TimeElementList.Count == 0 || oBeginDate < TimeElementList[0].EventTime.Date ? oBeginDate : TimeElementList[0].EventTime.Date, oCheckDateBegin, oCheckDateEnd;
            EmployeeData oEmployeeData;
            DailyWS oDWS;
            TimePair oTimePair;
            List<TimeElement> TimeElementForShift = new List<TimeElement>();
            TimeElementForShift = TimeElementList;

            //- TimeElement ���١ Map �� User ���Ǩ����ӡ�Ѻ�����ա
            List<TimePair> oTimePairList = ServiceManager.CreateInstance(CompanyCode).ESSData.LoadTimePairArchiveMappingByUser(EmployeeID, BeginDate, EndDate);
            if (oTimePairList.Count > 0)
            {
                List<DateTime> oUsedTimeElement = new List<DateTime>();
                foreach (TimePair item in oTimePairList)
                {
                    if (item.ClockIN != null && item.ClockOUT != null)
                    {
                        foreach (TimeElement element in TimeElementForShift.FindAll(delegate(TimeElement TE) { return TE.EventTime >= item.ClockIN.EventTime && TE.EventTime <= item.ClockOUT.EventTime; })) 
                        {
                            oUsedTimeElement.Add(element.EventTime);
                        }
                    }
                    else
                    {
                        if (item.ClockIN != null)
                            oUsedTimeElement.Add(item.ClockIN.EventTime);
                        if (item.ClockOUT != null)
                            oUsedTimeElement.Add(item.ClockOUT.EventTime);
                    }
                }

                if (oUsedTimeElement.Count > 0)
                    TimeElementForShift = TimeElementForShift.FindAll(delegate(TimeElement TE) { return !oUsedTimeElement.Contains(TE.EventTime); });

                oUsedTimeElement.Clear();
            }

            List<TimeElement> TimeElementMatchRange = new List<TimeElement>();
            DailyWS oDWSBeforeDayOff = new DailyWS();
            DailyWS oDWSOriginal = new DailyWS();

            while (oRunningDate <= oEndDate)
            {
                DateTime tmpDate = oRunningDate;
                try
                {
                    oTimePair = new TimePair();
                    oTimePair.Date = tmpDate;
                    oTimePair.EmployeeID = EmployeeID;

                    oEmployeeData = new EmployeeData(EmployeeID, tmpDate);
                    if (oEmployeeData.DateSpecific.HiringDate <= tmpDate.Date)
                    {
                        oDWS = oEmployeeData.GetDailyWorkSchedule(tmpDate.Date, true);
                        oDWSOriginal = oDWS;
                        oTimePair.DWS = oDWS;
                        oTimePair.DWSCode = oDWS.DailyWorkscheduleCode;

                        //����ѹ������ѹ OFF ����� DailyWS �ѹ�ӧҹ��͹˹�ҹ�鹷������ش
                        if (oDWS.DailyWorkscheduleCode == "OFF")
                        {
                            oTimePair.IsDayOff = true;
                            oTimePair.DWS.WorkBeginTime = new TimeSpan(0, 0, 0);
                            oTimePair.DWS.WorkEndTime = new TimeSpan(0, 0, 0);
                            DateTime oBackwardDate = tmpDate.AddDays(-1);

                            while (oDWS.DailyWorkscheduleCode == "OFF")
                            {
                                try
                                {
                                    EmployeeData tmpEmployeeData = new EmployeeData(EmployeeID, oBackwardDate);
                                    oDWS = tmpEmployeeData.GetDailyWorkSchedule(oBackwardDate, true);
                                }
                                catch
                                {
                                    EmployeeData tmpEmployeeData = new EmployeeData(EmployeeID);
                                    oDWS.DailyWorkscheduleCode = tmpEmployeeData.OrgAssignment.SubAreaSetting.Description.ToUpper();
                                    break;
                                }
                                oBackwardDate = oBackwardDate.AddDays(-1);
                            }

                        }

                        //��Ҿ�ѡ�ҹ�� Norm/Flex
                        //�� TimeElement 㹪�ǧ 04:00 - 03:59(�ͧ�ѹ�Ѵ�) ���� Record �á�� Clock-In ��� Record �ش������ Clock-Out
                        if (!String.IsNullOrEmpty(oDWS.DailyWorkscheduleCode) && !oDWS.DailyWorkscheduleCode.StartsWith("S"))
                        {
                            oCheckDateBegin = tmpDate.Add(new TimeSpan(4, 0, 0));
                            oCheckDateEnd = oCheckDateBegin.AddDays(1).AddSeconds(-1);
                            TimeElementMatchRange = TimeElementForShift.FindAll(delegate(TimeElement TE) { return TE.EventTime >= oCheckDateBegin && TE.EventTime <= oCheckDateEnd && !TE.IsSystemDelete; });
                            if (TimeElementMatchRange.Count > 0)
                            {
                                if (TimeElementMatchRange.Count > 0)
                                {
                                    oTimePair.ClockIN = TimeElementMatchRange[0];
                                    //�Ӥ�� Element ���١�������� Timepair �͡�ҡ TimeElementForShift
                                    TimeElementForShift.Remove(TimeElementMatchRange[0]);
                                }
                                if (TimeElementMatchRange.Count > 1)
                                {
                                    oTimePair.ClockOUT = TimeElementMatchRange[TimeElementMatchRange.Count - 1];
                                    //�Ӥ�� Element ���١�������� Timepair �͡�ҡ TimeElementForShift
                                    TimeElementForShift.Remove(TimeElementMatchRange[TimeElementMatchRange.Count - 1]);
                                }
                            }

                            MatchTimePair.Add(oTimePair);
                        }
                        //��Ҿ�ѡ�ҹ�� Shift ������� TimeElement � TimeElementForShift
                        else
                        {
                            List<int> lstRemoveIndex = new List<int>();
                            //���͡�ѹ���ç�Ѻ RunningDate �ѹ�á���� ClockIn
                            if (TimeElementForShift.Exists(delegate(TimeElement TE) { return TE.EventTime.Date == tmpDate.Date && !TE.IsSystemDelete; }))
                            {
                                int index = TimeElementForShift.FindIndex(delegate(TimeElement TE) { return TE.EventTime.Date == tmpDate.Date && !TE.IsSystemDelete; });

                                while (index > -1 && TimeElementForShift[index] != null && TimeElementForShift[index].EventTime != null && TimeElementForShift[index].EventTime.Date == tmpDate.Date)
                                {
                                    oTimePair = new TimePair();
                                    oTimePair.Date = tmpDate;
                                    oTimePair.EmployeeID = EmployeeID;
                                    oTimePair.DWS = oDWSOriginal;
                                    oTimePair.DWSCode = oDWSOriginal.DailyWorkscheduleCode;
                                    if (oTimePair.DWSCode == "OFF")
                                        oTimePair.IsDayOff = true;

                                    lstRemoveIndex.Add(index);
                                    oTimePair.ClockIN = TimeElementForShift[index];
                                    TimeElementForShift[index].IsSystemDelete = true;
                                    index++;
                                    if (index <= TimeElementForShift.Count - 1)
                                    {
                                        while (TimeElementForShift[index].IsSystemDelete)
                                            index++;

                                        //�ѹ���Դ�ѹ��ͧ����Թ���§�׹�ͧ oRunningdate.Date+2
                                        if ((index <= TimeElementForShift.Count - 1) && (TimeElementForShift[index].EventTime <= tmpDate.Date.AddDays(2)))
                                        {
                                            lstRemoveIndex.Add(index);
                                            oTimePair.ClockOUT = TimeElementForShift[index];
                                            TimeElementForShift[index].IsSystemDelete = true;
                                            index++;
                                        }
                                    }
                                    MatchTimePair.Add(oTimePair);

                                    if (index > TimeElementForShift.Count - 1)
                                        break;
                                }
                            }
                            else
                            {
                                MatchTimePair.Add(oTimePair);
                            }
                        }
                    }
                }
                catch (Exception ex)
                { 
                
                }
                oRunningDate = oRunningDate.AddDays(1);
            }

            //Merge between MatchingClockRealTime with Table TimePairArchiveMappingByUser
            //List<TimePair> oTimePairList = TimesheetManagement.LoadTimePairArchiveMappingByUser(EmployeeID, BeginDate, EndDate);
            foreach (TimePair obj in oTimePairList)
            {
                if (MatchTimePair.Exists(delegate(TimePair otp) { return otp.Date == obj.Date; }))
                {
                    oTimePair = MatchTimePair.Find(delegate(TimePair otp) { return otp.Date == obj.Date; });

                    if (obj.ClockIN != null)
                        oTimePair.ClockIN = obj.ClockIN;//.EventTime = obj.ClockIN.EventTime;
                    else
                        oTimePair.ClockIN = null;
                    if (obj.ClockOUT != null)
                        oTimePair.ClockOUT = obj.ClockOUT; //.EventTime = obj.ClockOUT.EventTime;
                    else
                        oTimePair.ClockOUT = null;

                }
            }

            return MatchTimePair.FindAll(delegate(TimePair tp) { return tp.Date >= BeginDate && tp.Date <= EndDate; });
        }

        
        //Add by Koissares P. 20181206
        #region Tuned up TM
        public List<TimePair> TM_TimePairing(string EmployeeID, List<TimeElement> TimeElementList, bool FillBlankPair, DateTime BeginDate, DateTime EndDate, Dictionary<DateTime, string> optionAttendance)
        {
            Dictionary<string, MonthlyWS> dictMWS = new Dictionary<string, MonthlyWS>();
            List<TimePair> oReturn = TM_TimePairing(EmployeeID, TimeElementList, FillBlankPair, BeginDate, EndDate, optionAttendance, ref dictMWS);
            dictMWS.Clear();
            dictMWS = null;
            return oReturn;
        }

        public List<TimePair> TM_TimePairing(string EmployeeID, List<TimeElement> TimeElementList, bool FillBlankPair, DateTime BeginDate, DateTime EndDate, Dictionary<DateTime, string> optionAttendance, ref Dictionary<string, MonthlyWS> dictMWS)
        {
            List<TimePair> MatchTimePair = new List<TimePair>();

            //���§�ӴѺ TimeElement ���� 
            TimeElementList.Sort(new TimeElementSortingTime());

            //�ѹ�����������������ش��èѺ�������
            //�ҡ BeginDate �դ����ҡѺ DateTime.MinValue �����ѹ����á� TimeElementList ᷹
            //�ҡ EndDate �դ����ҡѺ DateTime.MinValue �����ѹ����ش����� TimeElementList ᷹
            DateTime oBeginDate = BeginDate != DateTime.MinValue ? BeginDate : TimeElementList[0].EventTime.Date;
            DateTime oEndDate = EndDate != DateTime.MinValue ? EndDate : TimeElementList[TimeElementList.Count - 1].EventTime.Date;

            DateTime oRunningDate, oCheckDateBegin, oCheckDateEnd;
            if (TimeElementList.Count == 0 || oBeginDate < TimeElementList[0].EventTime.Date)
                oRunningDate = oBeginDate;
            else
                oRunningDate = TimeElementList[0].EventTime.Date;


            EmployeeData oEmployeeData = null;
            DailyWS oDWS;
            TimePair oTimePair;
            List<TimeElement> TimeElementForShift = new List<TimeElement>();
            TimeElementForShift = TimeElementList;

            //- TimeElement ���١ Map �� User ���Ǩ����ӡ�Ѻ�����ա
            List<TimePair> oTimePairList = LoadTimePairArchiveMappingByUser(EmployeeID, BeginDate, EndDate);
            if (oTimePairList.Count > 0)
            {
                List<DateTime> oUsedTimeElement = new List<DateTime>();
                foreach (TimePair item in oTimePairList)
                {
                    if (item.ClockIN != null && item.ClockOUT != null)
                    {
                        foreach (TimeElement element in TimeElementForShift.FindAll(delegate(TimeElement TE) { return TE.EventTime >= item.ClockIN.EventTime && TE.EventTime <= item.ClockOUT.EventTime; }))
                        {
                            oUsedTimeElement.Add(element.EventTime);
                        }
                    }
                    else
                    {
                        if (item.ClockIN != null)
                            oUsedTimeElement.Add(item.ClockIN.EventTime);
                        if (item.ClockOUT != null)
                            oUsedTimeElement.Add(item.ClockOUT.EventTime);
                    }
                }

                if (oUsedTimeElement.Count > 0)
                    TimeElementForShift = TimeElementForShift.FindAll(delegate(TimeElement TE) { return !oUsedTimeElement.Contains(TE.EventTime); });

                oUsedTimeElement.Clear();
                oUsedTimeElement = null;
            }

            DailyWS oDWSBeforeDayOff = new DailyWS();
            DailyWS oDWSOriginal = new DailyWS();

            List<TimeElement> TimeElementMatchRange = new List<TimeElement>();
            List<DailyWS> listDWS = new List<DailyWS>();
            Dictionary<string, List<ESS.EMPLOYEE.CONFIG.TM.Substitution>> dictSubstitution = new Dictionary<string, List<ESS.EMPLOYEE.CONFIG.TM.Substitution>>();
            INFOTYPE0007 info0007 = null;

            while (oRunningDate <= oEndDate)
            {
                DateTime tmpDate = oRunningDate;
                try
                {
                    oTimePair = new TimePair();
                    oTimePair.Date = tmpDate;
                    oTimePair.EmployeeID = EmployeeID;

                    if (oEmployeeData == null || oEmployeeData.OrgAssignment == null || tmpDate.Date > oEmployeeData.OrgAssignment.EndDate)
                    {
                        oEmployeeData = new EmployeeData(EmployeeID, tmpDate);
                    }
                    if (info0007 == null || info0007.BeginDate > tmpDate || info0007.EndDate < tmpDate)
                    {
                        //info0007 = INFOTYPE0007.GetWorkSchedule(EmployeeID, tmpDate);
                        info0007 = EmployeeManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetWorkSchedule(EmployeeID, tmpDate);
                    }
                    if (oEmployeeData.DateSpecific != null && oEmployeeData.DateSpecific.HiringDate != null)
                    {
                        if (oEmployeeData.DateSpecific.HiringDate <= tmpDate.Date)
                        {
                            if (info0007 != null && oEmployeeData.OrgAssignment != null)
                            {
                                //oDWS = oEmployeeData.GetDailyWorkSchedule(tmpDate.Date, true);
                                oDWS = GetDailyWSFromCollectedData(tmpDate, oEmployeeData, info0007, ref dictMWS, ref dictSubstitution, ref listDWS);
                                if (oDWS != null)
                                {
                                    oDWSOriginal = oDWS;
                                    oTimePair.DWS = oDWS;
                                    oTimePair.DWSCode = oDWS.DailyWorkscheduleCode;

                                    //����ѹ������ѹ OFF ����� DailyWS �ѹ�ӧҹ��͹˹�ҹ�鹷������ش
                                    if (oDWS.DailyWorkscheduleCode == "OFF")
                                    {
                                        oTimePair.IsDayOff = true;
                                        oTimePair.DWS.WorkBeginTime = new TimeSpan(0, 0, 0);
                                        oTimePair.DWS.WorkEndTime = new TimeSpan(0, 0, 0);
                                        DateTime oBackwardDate = tmpDate.AddDays(-1);
                                        while (oDWS.DailyWorkscheduleCode.StartsWith("OFF"))
                                        {
                                            try
                                            {
                                                oDWSBeforeDayOff = GetDailyWSFromCollectedData(oBackwardDate, oEmployeeData, info0007, ref dictMWS, ref dictSubstitution, ref listDWS);
                                                if (oDWSBeforeDayOff == null)
                                                    break;
                                                else
                                                    oDWS = oDWSBeforeDayOff;
                                            }
                                            catch
                                            {
                                                oDWS.DailyWorkscheduleCode = oEmployeeData.OrgAssignment.SubAreaSetting.Description.ToUpper();
                                                break;
                                            }
                                            oBackwardDate = oBackwardDate.AddDays(-1);
                                        }
                                    }

                                    //��Ҿ�ѡ�ҹ�� Norm/Flex
                                    //�� TimeElement 㹪�ǧ 04:00 - 03:59(�ͧ�ѹ�Ѵ�) ���� Record �á�� Clock-In ��� Record �ش������ Clock-Out
                                    if (!String.IsNullOrEmpty(oDWS.DailyWorkscheduleCode) && !oDWS.DailyWorkscheduleCode.StartsWith("S"))
                                    {
                                        oCheckDateBegin = tmpDate.Add(new TimeSpan(4, 0, 0));
                                        oCheckDateEnd = oCheckDateBegin.AddDays(1).AddSeconds(-1);
                                        TimeElementMatchRange = TimeElementForShift.FindAll(delegate(TimeElement TE) { return TE.EventTime >= oCheckDateBegin && TE.EventTime <= oCheckDateEnd && !TE.IsSystemDelete; });
                                        if (TimeElementMatchRange.Count > 0)
                                        {
                                            if (TimeElementMatchRange.Count > 0)
                                            {
                                                oTimePair.ClockIN = TimeElementMatchRange[0];
                                                //�Ӥ�� Element ���١�������� Timepair �͡�ҡ TimeElementForShift
                                                TimeElementForShift.Remove(TimeElementMatchRange[0]);
                                            }
                                            if (TimeElementMatchRange.Count > 1)
                                            {
                                                oTimePair.ClockOUT = TimeElementMatchRange[TimeElementMatchRange.Count - 1];
                                                //�Ӥ�� Element ���١�������� Timepair �͡�ҡ TimeElementForShift
                                                TimeElementForShift.Remove(TimeElementMatchRange[TimeElementMatchRange.Count - 1]);
                                            }
                                        }

                                        MatchTimePair.Add(oTimePair);
                                    }
                                    //��Ҿ�ѡ�ҹ�� Shift ������� TimeElement � TimeElementForShift
                                    else
                                    {
                                        List<int> lstRemoveIndex = new List<int>();
                                        //���͡�ѹ���ç�Ѻ RunningDate �ѹ�á���� ClockIn
                                        if (TimeElementForShift.Exists(delegate(TimeElement TE) { return TE.EventTime.Date == tmpDate.Date && !TE.IsSystemDelete; }))
                                        {
                                            int index = TimeElementForShift.FindIndex(delegate(TimeElement TE) { return TE.EventTime.Date == tmpDate.Date && !TE.IsSystemDelete; });

                                            while (index > -1 && TimeElementForShift[index] != null && TimeElementForShift[index].EventTime != null && TimeElementForShift[index].EventTime.Date == tmpDate.Date)
                                            {
                                                oTimePair = new TimePair();
                                                oTimePair.Date = tmpDate;
                                                oTimePair.EmployeeID = EmployeeID;
                                                oTimePair.DWS = oDWSOriginal;
                                                oTimePair.DWSCode = oDWSOriginal.DailyWorkscheduleCode;
                                                if (oTimePair.DWSCode == "OFF")
                                                    oTimePair.IsDayOff = true;

                                                lstRemoveIndex.Add(index);
                                                oTimePair.ClockIN = TimeElementForShift[index];
                                                TimeElementForShift[index].IsSystemDelete = true;
                                                index++;
                                                if (index <= TimeElementForShift.Count - 1)
                                                {
                                                    while (TimeElementForShift[index].IsSystemDelete)
                                                        index++;

                                                    //�ѹ���Դ�ѹ��ͧ����Թ���§�׹�ͧ oRunningdate.Date+2
                                                    if ((index <= TimeElementForShift.Count - 1) && (TimeElementForShift[index].EventTime <= tmpDate.Date.AddDays(2)))
                                                    {
                                                        lstRemoveIndex.Add(index);
                                                        oTimePair.ClockOUT = TimeElementForShift[index];
                                                        TimeElementForShift[index].IsSystemDelete = true;
                                                        index++;
                                                    }
                                                }
                                                MatchTimePair.Add(oTimePair);

                                                if (index > TimeElementForShift.Count - 1)
                                                    break;
                                            }
                                        }
                                        else
                                        {
                                            MatchTimePair.Add(oTimePair);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
                oRunningDate = oRunningDate.AddDays(1);
            }//End of while


            listDWS.Clear();
            dictSubstitution.Clear();
            listDWS = null;
            dictSubstitution = null;

            //Merge between MatchingClockRealTime with Table TimePairArchiveMappingByUser
            foreach (TimePair obj in oTimePairList)
            {
                if (MatchTimePair.Exists(delegate(TimePair otp) { return otp.Date == obj.Date; }))
                {
                    oTimePair = MatchTimePair.Find(delegate(TimePair otp) { return otp.Date == obj.Date; });

                    if (obj.ClockIN != null)
                        oTimePair.ClockIN = obj.ClockIN;//.EventTime = obj.ClockIN.EventTime;
                    else
                        oTimePair.ClockIN = null;
                    if (obj.ClockOUT != null)
                        oTimePair.ClockOUT = obj.ClockOUT; //.EventTime = obj.ClockOUT.EventTime;
                    else
                        oTimePair.ClockOUT = null;
                }
            }

            return MatchTimePair.FindAll(delegate(TimePair tp) { return tp.Date >= BeginDate && tp.Date <= EndDate; });
        }

        public static DailyWS GetDailyWSFromCollectedData(DateTime chkDate, EmployeeData _EmployeeData, INFOTYPE0007 info0007, ref Dictionary<string, MonthlyWS> dictMWS, ref Dictionary<string, List<Substitution>> dictSubstitution, ref List<DailyWS> listDWS)
        {
            try
            {
                System.Data.DataTable dtDWS = new System.Data.DataTable();
                DailyWS oDWS = null;
                string cSubGroup = _EmployeeData.OrgAssignment.SubGroupSetting.WorkScheduleGrouping;
                string cSubArea = _EmployeeData.OrgAssignment.SubAreaSetting.WorkScheduleGrouping;
                string cHoliday = _EmployeeData.OrgAssignment.SubAreaSetting.HolidayCalendar;
                string keyMWS = "";
                string keySubstitution = "";

                //Dictionary MonthlyWS
                keyMWS = string.Format("y{0}m{1}|Group{2}Area{3}Hol{4}WF{5}", chkDate.Year, chkDate.Month, cSubGroup, cSubArea, cHoliday, info0007.WFRule);
                if (!dictMWS.ContainsKey(keyMWS))
                {
                    dictMWS.Add(keyMWS, EmployeeManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetCalendar(_EmployeeData.EmployeeID, chkDate.Date, chkDate.Year, chkDate.Month, false));
                }

                //Dictionary Substitution
                keySubstitution = string.Format("{0}y{1}m{2}", _EmployeeData.EmployeeID, chkDate.Year, chkDate.Month);
                if (!dictSubstitution.ContainsKey(keySubstitution))
                {
                    dictSubstitution.Add(keySubstitution, Substitution.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetSubstitutionList(_EmployeeData.EmployeeID, chkDate.Year, chkDate.Month, true));
                }

                string _WSCode = GetDWSCodeFormMWSwithSubstitute(_EmployeeData.EmployeeID, chkDate, dictMWS[keyMWS], dictSubstitution[keySubstitution]);

                if (listDWS == null || listDWS.Count == 0 || listDWS[0].DailyWorkscheduleGrouping != _EmployeeData.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping)
                {
                    listDWS = EmployeeManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetDailyWSByWorkscheduleGrouping(_EmployeeData.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, chkDate.Date);
                }
                else
                {
                    if (!listDWS.Exists(delegate(DailyWS ws) { return ws.DailyWorkscheduleCode == _WSCode && ws.BeginDate <= chkDate.Date && chkDate.Date <= ws.EndDate; }))
                    {
                        listDWS = EmployeeManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetDailyWSByWorkscheduleGrouping(_EmployeeData.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, chkDate.Date);
                    }
                    else
                    {
                        foreach (DailyWS dws in listDWS)
                        {
                            if (chkDate.Date > dws.EndDate || chkDate.Date < dws.BeginDate)
                            {
                                listDWS = EmployeeManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetDailyWSByWorkscheduleGrouping(_EmployeeData.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, chkDate.Date);
                            }
                        }
                    }
                }

                if (listDWS.Exists(delegate(DailyWS ws) { return ws.DailyWorkscheduleCode == _WSCode && ws.BeginDate <= chkDate.Date && chkDate.Date <= ws.EndDate; }))
                {
                    dtDWS = listDWS.Find(delegate(DailyWS ws) { return ws.DailyWorkscheduleCode == _WSCode && ws.BeginDate <= chkDate.Date && chkDate.Date <= ws.EndDate; }).ToADODataTable();
                    if (dtDWS.Rows.Count > 0)
                    {
                        oDWS = new DailyWS();
                        oDWS.ParseToObject(dtDWS);
                        oDWS.IsHoliday = dictMWS[keyMWS].GetWSCode(chkDate.Day, false) != "";
                        //oDWS.IsDayOff = oDWS.WorkHours == 0.00m;
                    }
                }

                cSubGroup = null;
                cSubArea = null;
                cHoliday = null;
                keyMWS = null;
                keySubstitution = null;

                return oDWS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string GetDWSCodeFormMWSwithSubstitute(string EmployeeID, DateTime chkDate, MonthlyWS _MWS, List<ESS.EMPLOYEE.CONFIG.TM.Substitution> _SubstitutionList)
        {
            string cDWSCode = "";

            ESS.EMPLOYEE.CONFIG.TM.Substitution oSubstitution = _SubstitutionList.Find(delegate(ESS.EMPLOYEE.CONFIG.TM.Substitution oSub) { return oSub.Status == "COMPLETED" && oSub.BeginDate.Day == chkDate.Day && oSub.BeginDate.Month == chkDate.Month && oSub.BeginDate.Year == chkDate.Year; });
            if (oSubstitution != null)
            {
                if (String.IsNullOrEmpty(oSubstitution.EmpDWSCodeNew))
                {
                    cDWSCode = oSubstitution.SubstituteBeginTime.ToString() + "-" + oSubstitution.SubstituteEndTime.ToString();
                }
                else
                {
                    if (EmployeeID.Trim() == oSubstitution.EmployeeID.Trim())
                        cDWSCode = oSubstitution.EmpDWSCodeNew;
                    else
                        cDWSCode = oSubstitution.SubDWSCodeNew;
                }
            }
            else
            {
                cDWSCode = _MWS.GetWSCode(chkDate.Day, true);
            }
            return cDWSCode;
        }

        #endregion


        public static List<TimePair> MatchingClock2(string EmployeeID, List<TimeElement> clock, bool FillBlankPair, DateTime BeginDate, DateTime EndDate)
        {
            if (clock.Count == 0)
            {
                return new List<TimePair>();
            }

            List<TimePair> matches = new List<TimePair>();
            // Sort 
            TimeElementSortingTime oSortingCriteria = new TimeElementSortingTime();
            clock.Sort(oSortingCriteria);

            // Group TimeElement by date into clockInDay and clockOutDay
            Dictionary<DateTime, List<TimeElement>> clockInDay = new Dictionary<DateTime, List<TimeElement>>();
            Dictionary<DateTime, List<TimeElement>> clockOutDay = new Dictionary<DateTime, List<TimeElement>>();
            foreach (TimeElement te in clock)
            {
                if (te.DoorType == DoorType.IN)
                {
                    if (!clockInDay.ContainsKey(te.EventTime.Date))
                    {
                        clockInDay[te.EventTime.Date] = new List<TimeElement>();
                    }
                    clockInDay[te.EventTime.Date].Add(te);
                }
                else
                {
                    if (!clockOutDay.ContainsKey(te.EventTime.Date))
                    {
                        clockOutDay[te.EventTime.Date] = new List<TimeElement>();
                    }
                    clockOutDay[te.EventTime.Date].Add(te);
                }
            }
            DateTime beginInterval = BeginDate == DateTime.MinValue ? clock[0].EventTime.Date : BeginDate.Date;
            DateTime endInterval = EndDate == DateTime.MinValue ? clock[clock.Count - 1].EventTime.Date : EndDate.Date;

            for (DateTime runningDate = beginInterval; runningDate < endInterval.AddDays(1); runningDate = runningDate.AddDays(1))
            {
                EmployeeData empData = new EmployeeData(EmployeeID, runningDate.Date);
                MonthlyWS mws = MonthlyWS.GetCalendar(EmployeeID, runningDate.Date.Year, runningDate.Date.Month);
                DailyWS dws = empData.GetDailyWorkSchedule(runningDate.Date);

                TimePair tp = new TimePair();
                List<TimeElement> clockinRunningDate = clockInDay.ContainsKey(runningDate) ? clockInDay[runningDate] : null;
                List<TimeElement> clockoutRunningDate = clockOutDay.ContainsKey(runningDate) ? clockOutDay[runningDate] : null;
                tp.Date = runningDate;
                tp.ClockIN = clockinRunningDate != null ? clockinRunningDate[0] : null;
                tp.ClockOUT = clockoutRunningDate != null ? clockoutRunningDate[clockoutRunningDate.Count - 1] : null;
                tp.DWSCode = dws.DailyWorkscheduleCode;
                tp.DWS = dws;
                tp.IsDayOff = dws.IsDayOff;
                tp.IsHoliday = mws.GetWSCode(runningDate.Day, false) != "";
                matches.Add(tp);
            }
            return matches;
        }

        public static List<TimePair> MatchingClock(string EmployeeID, List<TimeElement> clock)
        {
            return MatchingClock(EmployeeID, clock, false);
        }
        public static List<TimePair> MatchingClock(string EmployeeID, List<TimeElement> clock, bool FillBlankPair)
        {
            return MatchingClock(EmployeeID, clock, FillBlankPair, DateTime.MinValue, DateTime.MinValue);
        }
        public static List<TimePair> MatchingClock(string EmployeeID, List<TimeElement> clock, bool FillBlankPair, DateTime BeginDate, DateTime EndDate)
        {
            List<TimePair> matches = new List<TimePair>();
            // Sort 
            TimeElementSortingTime oSortingCriteria = new TimeElementSortingTime();
            clock.Sort(oSortingCriteria);

            // loop
            TimePair pair = new TimePair();
            TimeElement oFirstOut = null;
            TimeElement oCheckDate = null;// --out ����� in �ش���µ�ҧ�ѹ���֧ INTENTIONALLY_GAP �ҷ� [out(this) in]
            TimeElement oCheckDate1 = null;// --in ����� in �ش������ҧ�ѹ�Թ POSSIBLE_DISTINCT_HOURS �� [in(this) in]
            TimeElement oCheckDate2 = null;// --out ����� out �Ѵ���ҧ�Ѻ in ��͹˹���Թ POSSIBLE_DISTINCT_HOURS ��  [in out(this) out]
            const int INTENTIONALLY_GAP = 5;
            const int POSSIBLE_DISTINCT_HOURS = 10;
            const int MUST_DISTINCT_HOURS = 20;

            #region " main loop "
            foreach (TimeElement item in clock)
            {
                if (item.IsSystemDelete)
                {
                    continue;
                }

                if (item.DoorType == DoorType.IN)
                {
                    #region " flag in "
                    if (pair.ClockIN == null && pair.ClockOUT == null)
                    {
                        #region " new pair "
                        pair.ClockIN = item;
                        #endregion
                    }
                    else if (pair.ClockIN == null && pair.ClockOUT != null && pair.ClockOUT <= item)
                    {
                        #region " case [null,out] item(in) "
                        if (datediff(pair.ClockOUT, item).TotalMinutes >= INTENTIONALLY_GAP)
                        {
                            // different from 2 element that gap size more than INTENTIONALLY_GAP ---> assume it is intentionally element
                            #region " item(in) - out >= INTENTIONALLY_GAP ----> create pair (null , out)"
                            matches.Add(pair);
                            #endregion
                        }
                        else if (oFirstOut != null && pair.ClockOUT != oFirstOut)
                        {
                            #region " ( item(in) - out < INTENTIONALLY_GAP ) and ( is case out1 out2 in ) ----> create pair (null, out1) "
                            matches.Add(new TimePair(null, oFirstOut));
                            #endregion
                        }
                        pair = new TimePair(item, null);
                        oFirstOut = null;
                        #endregion
                    }
                    else if (pair.ClockIN != null && pair.ClockOUT != null && pair.ClockOUT > pair.ClockIN)
                    {
                        #region " case [in,out] item(in) "
                        if (datediff(pair.ClockIN, pair.ClockOUT).TotalMinutes < INTENTIONALLY_GAP)
                        {
                            if ((datediff(pair.ClockIN, item).TotalHours >= POSSIBLE_DISTINCT_HOURS) && oCheckDate1 == null)
                            {
                                #region " in <- POSSIBLE_DISTINCT_HOURS -> item(in) --> assume both in & out element incorrect. remove all and set ClockIN = item "
                                pair.ClockIN = item;
                                #endregion
                            }
                            #region " in <- INTENTIONALLY_GAP -> out ----> assume out element incorrect. remove out"
                            pair.ClockOUT = null;
                            #endregion
                        }
                        else if (datediff(pair.ClockOUT, item).TotalMinutes < INTENTIONALLY_GAP)
                        {
                            #region " case [in,out] <- INTENTIONALLY_GAP -> item(in) "
                            if (oCheckDate2 != null)
                            {
                                #region " case in out1 <- POSSIBLE_DISTINCT_HOURS -> out2 <- INTENTIONALLY_GAP -> item(in), create pair (in, out1) and assume out2 is incorrect. remove out2"
                                pair.ClockOUT = oCheckDate2;
                                matches.Add(pair);
                                oCheckDate = null;
                                oCheckDate2 = null;
                                pair = new TimePair(item, null);
                                #endregion
                            }
                            else
                            {
                                #region " case in out <- INTENTIONALLY_GAP -> item(in), mark CheckDate = out and clear both out and item(in)"
                                oCheckDate = pair.ClockOUT;
                                pair.ClockOUT = null;
                                #endregion
                            }
                            #endregion
                        }
                        else
                        {
                            if (datediff(pair.ClockIN, pair.ClockOUT).TotalHours <= MUST_DISTINCT_HOURS)
                            {
                                #region " case normal "
                                matches.Add(pair);
                                #endregion
                            }
                            else
                            {
                                #region " in <- MUST_DISTINCT_HOUR -> out ---> assume it not match. create 1 record (in, null) remove out "
                                matches.Add(new TimePair(pair.ClockIN, null));
                                //matches.Add(new TimePair(null, pair.ClockOUT));
                                #endregion
                            }
                            pair = new TimePair(item, null);
                            oCheckDate1 = null;
                            oCheckDate2 = null;
                            oFirstOut = null;
                        }
                        #endregion
                    }
                    else if (pair.ClockIN != null && pair.ClockOUT == null && (datediff(pair.ClockIN, item).TotalHours) >= MUST_DISTINCT_HOURS)
                    {
                        #region " case [in,null] and ( in <- MUST_DISTINCT_HOURS -> item(in) )"
                        if (oCheckDate != null)
                        {
                            #region " case in1 out <- INTENTIONALLY_GAP -> in2 item(in). assume in2 incorrect. remove in2. set in = item "
                            pair.ClockOUT = oCheckDate;
                            matches.Add(pair);
                            pair = new TimePair(item, null);
                            #endregion

                            if (oCheckDate1 != null)
                            {
                                // may be not occur
                                #region " case in1 out <- INTENTIONALLY_GAP -> in2 in3 item(in) and ( in1 <- POSSIBLE_DISTINCT_HOURS -> in3 ). set in = in3 "
                                if (oCheckDate < oCheckDate1)
                                {
                                    pair.ClockIN = oCheckDate1;
                                }
                                #endregion
                            }
                            oCheckDate = null;
                            oCheckDate1 = null;
                            oFirstOut = null;
                        }
                        else if (oCheckDate1 != null)
                        {
                            #region " case in1 <- POSSIBLE_DISTINCT_HOURS -> in2 item(in)"
                            matches.Add(pair);

                            #region "  OLD VERSION "
                            //pair = new TimePair();
                            //if (datediff(oCheckDate1, item).TotalHours >= 10)
                            //{
                            //    if (datediff(oCheckDate1, item).TotalHours >= 20)
                            //    {
                            //        matches.Add(new TimePair(oCheckDate1, null));
                            //    }
                            //    pair.ClockIN = item;
                            //    //may be clockin = oCheckDate1 and oCheckDate1 = item
                            //}
                            //else
                            //{
                            //    pair.ClockIN = oCheckDate1;
                            //}
                            //oCheckDate1 = null;
                            #endregion

                            pair = new TimePair(oCheckDate1, null);
                            if (datediff(oCheckDate1, item).TotalHours >= POSSIBLE_DISTINCT_HOURS)
                            {
                                if (datediff(oCheckDate1, item).TotalHours >= MUST_DISTINCT_HOURS)
                                {
                                    #region " in1 <- POSSIBLE_DISTINCT_HOURS -> in2 <- MUST_DISTINCT_HOURS -> item(in). create (in1,null) (in2,null) and set in = item(in) "
                                    matches.Add(pair);
                                    pair = new TimePair(item, null);
                                    oCheckDate1 = null;
                                    #endregion
                                }
                                else
                                {
                                    #region " in1 <- POSSIBLE_DISTINCT_HOURS -> in2 <- POSSIBLE_DISTINCT_HOURS -> item(in). create (in1,null) set in = in2 and mark CheckDate1 = in3 "
                                    oCheckDate1 = item;
                                    #endregion
                                }
                            }

                            oFirstOut = null;
                            #endregion
                        }
                        else
                        {
                            #region " in <- MUST_DISTINCT_HOURS -> item(in). create (in,null) set in = item "
                            matches.Add(pair);
                            pair = new TimePair(item, null);
                            oFirstOut = null;
                            #endregion
                        }
                        #endregion
                    }
                    else if (pair.ClockIN != null && pair.ClockOUT == null && (datediff(pair.ClockIN, item).TotalHours >= POSSIBLE_DISTINCT_HOURS) && oCheckDate1 == null)
                    {
                        #region " in <- POSSIBLE_DISTINCT_HOURS -> item(in). mark CheckDate1 = item "
                        oCheckDate1 = item;
                        #endregion
                    }
                    #endregion
                }
                else if (item.DoorType == DoorType.OUT)
                {
                    #region " flag out "
                    if (oCheckDate != null)
                    {
                        if (pair.ClockIN != null)
                        {
                            if (datediff(pair.ClockIN, item).TotalHours > MUST_DISTINCT_HOURS)
                            {
                                #region " in1 out1 <- INTENTIONALLY_GAP -> in2 item(out) and in1 <- MUST_DISTINCT_HOURS -> item(out). assum in1 and out1 is match and in2 is incorrect. create (in1,out1) , clear in2 "
                                pair.ClockOUT = oCheckDate;
                                matches.Add(pair);
                                pair = new TimePair();
                                #endregion
                            }
                        }
                        #region " out1 <- INTENTIONALLY_GAP -> in2 item(out). assume out1 , in2 are incorrect. remove both. "
                        pair.ClockOUT = item;
                        oCheckDate = null;
                        #endregion
                    }
                    else if (pair.ClockOUT != null && datediff(pair.ClockOUT, item).TotalHours > POSSIBLE_DISTINCT_HOURS)
                    {
                        if (pair.ClockIN != null && datediff(pair.ClockIN, pair.ClockOUT).TotalHours > MUST_DISTINCT_HOURS)
                        {
                            #region " in <- MUST_DISTINCT_HOURS -> out <- POSSIBLE_DISTINCT_HOURS -> item(out). remove CLOCKOUT."
                            pair.ClockOUT = null;
                            #endregion
                        }
                        #region " in out <- POSSIBLE_DISTINCT_HOURS -> item(out). assume first pair complete. create pair , set out = item "
                        matches.Add(pair);
                        pair = new TimePair(null, item);
                        oFirstOut = null;
                        #endregion
                    }
                    else if (pair.ClockIN != null)
                    {
                        #region " case [in,*] item(out) "
                        if (datediff(pair.ClockIN, item).TotalHours > MUST_DISTINCT_HOURS)
                        {
                            #region " case in <- MUST_DISTINCT_HOURS -> item(out) "
                            if (oCheckDate1 != null)
                            {
                                #region " in1 <- POSSIBLE_DISTINCT_HOURS -> in2 item(out) and in1 <- MUST_DISTINCT_HOURS -> item(out). assume first pair correct. create pair. set in = in2 exception case in1 in2 out item(out). clear in "
                                matches.Add(pair);
                                if (pair.ClockOUT != null && pair.ClockOUT > oCheckDate1)
                                {
                                    #region " exception case in1 in2(CheckDate1) out1(ClockOUT) item(out). assume in2 incorrect. remove in2"
                                    pair = new TimePair();
                                    #endregion
                                }
                                else
                                {
                                    pair = new TimePair(oCheckDate1, null);
                                }
                                #endregion

                                if (pair.ClockIN != null && datediff(pair.ClockIN, item).TotalHours > MUST_DISTINCT_HOURS)
                                {
                                    #region " case in1 out1 in2 <- MUST_DISTINCT_HOURS -> item(out). create (in2, null)"
                                    matches.Add(pair);
                                    pair = new TimePair();
                                    #endregion
                                }
                                pair.ClockOUT = item;
                                oCheckDate1 = null;
                            }
                            else
                            {
                                #region " [in,*] <- MUST_DISTINCT_HOURS -> item(out) . first pair complete, create pair."
                                matches.Add(pair);
                                pair = new TimePair();
                                oFirstOut = null;
                                #endregion
                            }
                            #endregion
                        }
                        else if (datediff(pair.ClockIN, item).TotalMinutes > INTENTIONALLY_GAP)
                        {
                            if (pair.ClockOUT != null)
                            {
                                if (pair.ClockIN < pair.ClockOUT && datediff(pair.ClockOUT, item).TotalMinutes > INTENTIONALLY_GAP && datediff(pair.ClockIN, item).TotalHours > POSSIBLE_DISTINCT_HOURS)
                                {
                                    #region " case (in out1 item(out)) and in <- POSSIBLE_DISTINCT_HOURS -> item(out). mark checkdate2"
                                    oCheckDate2 = pair.ClockOUT;
                                    #endregion
                                }
                            }
                            #region " case [in,*] item(out) "
                            pair.ClockOUT = item;
                            #endregion
                        }
                        #endregion
                    }
                    else if (oFirstOut != null)
                    {
                        if (datediff(oFirstOut, item).TotalHours > MUST_DISTINCT_HOURS)
                        {
                            #region " case [null,*] out <- MUST_DISTINCT_HOURS -> item(out). assume out match with no-ClockIN. create (null,out) "
                            pair.ClockOUT = oFirstOut;
                            matches.Add(pair);
                            pair = new TimePair();
                            oFirstOut = null;
                            #endregion
                        }
                    }
                    if (oFirstOut == null)
                    {
                        #region " [pair] item(out). mark FIRSTOUT"
                        oFirstOut = item;
                        #endregion
                    }
                    if (pair.ClockIN != null)
                    {
                        if (datediff(pair.ClockIN, item).TotalMinutes > INTENTIONALLY_GAP)
                        {
                            pair.ClockOUT = item;
                        }
                    }
                    else
                    {
                        pair.ClockOUT = item;
                    }

                    #endregion
                }
            }
            #endregion

            #region " check last record "
            if (pair.ClockIN != null || pair.ClockOUT != null)
            {
                if (pair.ClockIN != null && pair.ClockOUT != null && pair.ClockOUT < pair.ClockIN)
                {
                    pair.ClockOUT = null;
                }
                if (oCheckDate != null)
                {
                    #region " case [A,B] out <- INTENTIONALLY_GAP -> in. assume out is correct remove B and in "
                    pair.ClockOUT = oCheckDate;
                    matches.Add(pair);
                    pair = new TimePair();
                    #endregion
                }
                else
                {
                    #region " normal case : append last record "
                    matches.Add(pair);
                    #endregion
                }
            }
            #endregion

            string cEmpID = null;
            if (clock.Count > 0)
            {
                cEmpID = clock[0].EmployeeID;
            }

            foreach (TimePair item in matches)
            {
                item.EmployeeID = cEmpID;
            }

            FillDWS(EmployeeID, matches, clock, FillBlankPair, true, BeginDate, EndDate);

            #region " SystemDelete "
            foreach (TimeElement item in clock)
            {
                item.IsSystemDelete = true;
            }
            foreach (TimePair pairitem in matches)
            {
                int index;
                if (pairitem.ClockIN != null)
                {
                    index = clock.IndexOf(pairitem.ClockIN);
                    if (index > -1)
                    {
                        clock[index].IsSystemDelete = false;
                    }
                }
                if (pairitem.ClockOUT != null)
                {
                    index = clock.IndexOf(pairitem.ClockOUT);
                    if (index > -1)
                    {
                        clock[index].IsSystemDelete = false;
                    }
                }
            }
            #endregion

            return matches;
        }

        private static TimeSpan datediff(TimeElement time1, TimeElement time2)
        {
            return time2.EventTime.Subtract(time1.EventTime);
        }
        #endregion

        #region " ArchiveData "
        public void ArchiveData(string EmployeeID, DateTime BeginDate, DateTime EndDate, List<TimeElement> Data)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveArchiveEvent(EmployeeID, BeginDate, EndDate, Data);
        }
        public void ArchiveData(string EmployeeID, DateTime BeginDate, DateTime EndDate, List<TimePair> Data)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveArchiveTimePair(EmployeeID, BeginDate, EndDate, Data);
        }
        public void ArchiveData(string EmployeeID, List<TimeElement> Data)
        {
            if (Data.Count > 0)
            {
                ArchiveData(EmployeeID, Data[0].EventTime, Data[Data.Count - 1].EventTime, Data);
            }
        }
        #endregion

        #region " PostTimePair "
        public void PostTimePair(List<TimePair> Data)
        {
            ServiceManager.CreateInstance(CompanyCode).ERPData.PostTimePair(Data);
        }
        #endregion

        #region " LoadArchiveTimepair "
        public List<TimePair> LoadArchiveTimepair(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.LoadArchiveTimePair(EmployeeID, BeginDate, EndDate);
        }
        #endregion

        #region " LoadArchiveTimeElementRelative "
        public List<TimeElement> LoadArchiveTimeElementRelative(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            List<TimeElement> buffer = ServiceManager.CreateInstance(CompanyCode).ESSData.LoadArchiveEventRelative(EmployeeID, BeginDate, EndDate);
            foreach (TimeElement item in buffer)
            {
                item.SetElementType(TimeElementType.ACCEPT);
            }
            return buffer;
        }
        #endregion

        #region " FillDWS "
        public static void FillDWS(string EmployeeID, List<TimePair> pairs, bool FillBlankPair, bool DeleteFutureDate, DateTime BeginDate, DateTime EndDate)
        {
            FillDWS(EmployeeID, pairs, FillBlankPair, DeleteFutureDate, BeginDate, EndDate, true);
        }

        public static void FillDWS(string EmployeeID, List<TimePair> pairs, bool FillBlankPair, bool DeleteFutureDate, DateTime BeginDate, DateTime EndDate, bool DeleteOutSide)
        {
            FillDWS(EmployeeID, pairs, new List<TimeElement>(), FillBlankPair, DeleteFutureDate, BeginDate, EndDate, DeleteOutSide);
        }

        private static void FillDWS(string EmployeeID, List<TimePair> pairs, List<TimeElement> clock, bool FillBlankPair)
        {
            FillDWS(EmployeeID, pairs, clock, FillBlankPair, true, DateTime.MinValue, DateTime.MinValue);
        }
        private static void FillDWS(string EmployeeID, List<TimePair> pairs, List<TimeElement> clock, bool FillBlankPair, bool DeleteFutureDate, DateTime BeginDate, DateTime EndDate)
        {
            FillDWS(EmployeeID, pairs, clock, FillBlankPair, true, BeginDate, EndDate, true);
        }

        private static void FillDWS(string EmployeeID, List<TimePair> pairs, List<TimeElement> clock, bool FillBlankPair, bool DeleteFutureDate, DateTime BeginDate, DateTime EndDate, bool DeleteOutSide)
        {
            List<DateTime> dateList = new List<DateTime>();
            Dictionary<string, MonthlyWS> listMWS = new Dictionary<string, MonthlyWS>();
            Dictionary<string, Dictionary<string, DailyWS>> listDWS = new Dictionary<string, Dictionary<string, DailyWS>>();
            EmployeeData oEmp = null;
            string cEmpID = EmployeeID;
            string cYYYYMM;
            MonthlyWS oMWS;
            DailyWS oDWS;
            string cDWSCode = "";
            bool isHoliday = false;
            string cDailyGroup;

            if (pairs.Count > 0)
            {
                //cEmpID = pairs[0].EmployeeID;
                if (pairs[0].ClockIN != null)
                {
                    oEmp = new EmployeeData(cEmpID, pairs[0].ClockIN.EventTime);
                }
                else if (pairs[0].ClockOUT != null)
                {
                    oEmp = new EmployeeData(cEmpID, pairs[0].ClockOUT.EventTime);
                }
                else if (BeginDate != DateTime.MinValue)
                {
                    oEmp = new EmployeeData(cEmpID, BeginDate);
                }
                List<TimePair> delList = new List<TimePair>();
                List<TimePair> delList1 = new List<TimePair>();

                #region " find Date, DWS "
                foreach (TimePair pairitem in pairs)
                {
                    if (pairitem.ClockIN != null)
                    {
                        #region " CLOCKIN "
                        cYYYYMM = pairitem.ClockIN.EventTime.ToString("yyyyMM");
                        if (!listMWS.ContainsKey(cYYYYMM))
                        {
                            oMWS = MonthlyWS.GetCalendar(cEmpID, pairitem.ClockIN.EventTime.Year, pairitem.ClockIN.EventTime.Month);
                            listMWS.Add(cYYYYMM, oMWS);
                        }
                        else
                        {
                            oMWS = listMWS[cYYYYMM];
                        }

                        cDWSCode = oMWS.GetWSCode(pairitem.ClockIN.EventTime.Day, true);
                        if (cDWSCode == null)
                        {
                            cDWSCode = "";
                        }
                        isHoliday = oMWS.GetWSCode(pairitem.ClockIN.EventTime.Day, false) != "";

                        // OrgAssignment not support --- > Load new
                        if (oEmp.OrgAssignment == null || !(oEmp.OrgAssignment.BeginDate <= pairitem.ClockIN.EventTime && oEmp.OrgAssignment.EndDate >= pairitem.ClockIN.EventTime))
                        {
                            oEmp = new EmployeeData(cEmpID, pairitem.ClockIN.EventTime);
                        }

                        if (oEmp.OrgAssignment != null)
                        {
                            cDailyGroup = oEmp.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping;
                        }
                        else
                        {
                            cDailyGroup = "";
                        }

                        Dictionary<string, DailyWS> subList;
                        if (listDWS.ContainsKey(cDailyGroup))
                        {
                            subList = listDWS[cDailyGroup];
                        }
                        else
                        {
                            subList = new Dictionary<string, DailyWS>();
                            listDWS.Add(cDailyGroup, subList);
                        }

                        if (subList.ContainsKey(cDWSCode))
                        {
                            oDWS = subList[cDWSCode];
                        }
                        else
                        {
                            oDWS = EmployeeManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetDailyWorkschedule(cDailyGroup, cDWSCode, oEmp.CheckDate);
                            subList.Add(cDWSCode, oDWS);
                        }
                        if (oDWS == null || oDWS.WorkscheduleClass == null || "0,9,".IndexOf(oDWS.WorkscheduleClass.Trim() + ",") > -1)
                        {
                            // if DWS is off
                            pairitem.DWSCode = cDWSCode;
                            pairitem.Date = pairitem.ClockIN.EventTime.Date;
                            pairitem.IsDayOff = true;
                            pairitem.IsHoliday = false;
                        }
                        else
                        {
                            // if DWS is work day
                            pairitem.IsDayOff = oDWS.DailyWorkscheduleCode == "OFF";
                            pairitem.DWSCode = oDWS.DailyWorkscheduleCode;
                            pairitem.IsHoliday = isHoliday;

                            bool lFound = false;
                            if (pairitem.ClockOUT != null)
                            {
                                DateTime dateCal;
                                DateTime dateBegin, dateEnd;
                                dateCal = pairitem.ClockIN.EventTime.Date;
                                if (oDWS.NormalBeginTime != TimeSpan.MinValue)
                                {
                                    // case FlexTime
                                    dateBegin = dateCal.Add(oDWS.NormalBeginTime);
                                    dateEnd = dateCal.Add(oDWS.NormalEndTime);
                                }
                                else
                                {
                                    // case StandardTime
                                    dateBegin = dateCal.Add(oDWS.WorkBeginTime);
                                    dateEnd = dateCal.Add(oDWS.WorkEndTime);
                                }
                                if (dateBegin >= dateEnd)
                                {
                                    dateEnd = dateEnd.AddDays(1);
                                }
                                if (pairitem.ClockIN.EventTime <= dateEnd && pairitem.ClockOUT.EventTime >= dateBegin)
                                {
                                    lFound = true;
                                    pairitem.Date = dateCal;
                                    pairitem.DWSCode = cDWSCode;
                                }
                            }
                            if (pairitem.ClockOUT == null || !lFound)
                            {
                                #region " case (in,null)"
                                // case (in,null)
                                if ("1,2,".IndexOf(oMWS.ValuationClass.Trim() + ",") > -1)
                                {
                                    // if (norm or tnom)
                                    pairitem.DWSCode = cDWSCode;
                                    pairitem.Date = pairitem.ClockIN.EventTime.Date;
                                }
                                else
                                {
                                    // if shift cutoff at 4:30
                                    if (pairitem.ClockIN.EventTime.TimeOfDay < new TimeSpan(4, 30, 0) && pairitem.ClockIN.EventTime.Date < DateTime.Now.Date)
                                    {
                                        DateTime oYesterday = pairitem.ClockIN.EventTime.AddDays(-1);

                                        string cYYYYMM_temp = oYesterday.ToString("yyyyMM");
                                        MonthlyWS oMWS_temp;
                                        DailyWS oDWS_temp;
                                        if (!listMWS.ContainsKey(cYYYYMM_temp))
                                        {
                                            oMWS_temp = MonthlyWS.GetCalendar(pairitem.ClockIN.EmployeeID, oYesterday.Year, oYesterday.Month);
                                            listMWS.Add(cYYYYMM_temp, oMWS);
                                        }
                                        else
                                        {
                                            oMWS_temp = listMWS[cYYYYMM_temp];
                                        }
                                        string cDWSCode_temp = oMWS_temp.GetWSCode(oYesterday.Day, true);
                                        bool isHoliday_temp = oMWS_temp.GetWSCode(oYesterday.Day, false) != "";

                                        // OrgAssignment not support --- > Load new
                                        if (!(oEmp.OrgAssignment.BeginDate <= pairitem.ClockIN.EventTime && oEmp.OrgAssignment.EndDate >= pairitem.ClockIN.EventTime))
                                        {
                                            oEmp = new EmployeeData(cEmpID, pairitem.ClockIN.EventTime);
                                        }

                                        cDailyGroup = oEmp.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping;

                                        Dictionary<string, DailyWS> subList_temp;

                                        if (listDWS.ContainsKey(cDailyGroup))
                                        {
                                            subList_temp = listDWS[cDailyGroup];
                                        }
                                        else
                                        {
                                            subList_temp = new Dictionary<string, DailyWS>();
                                            listDWS.Add(cDailyGroup, subList_temp);
                                        }
                                        if (subList_temp.ContainsKey(cDWSCode_temp))
                                        {
                                            oDWS_temp = subList_temp[cDWSCode_temp];
                                        }
                                        else
                                        {
                                            oDWS_temp = EmployeeManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetDailyWorkschedule(cDailyGroup, cDWSCode_temp, oYesterday);
                                            subList_temp.Add(cDWSCode_temp, oDWS_temp);
                                        }
                                        if ("0,9,".IndexOf(oDWS_temp.WorkscheduleClass.Trim() + ",") > -1)
                                        {
                                            // if DWS is off
                                            pairitem.IsDayOff = true;
                                        }
                                        else
                                        {
                                            pairitem.IsDayOff = false;
                                        }
                                        pairitem.IsHoliday = isHoliday_temp;
                                        pairitem.DWSCode = cDWSCode_temp;
                                        pairitem.Date = oYesterday.Date;
                                    }
                                    else
                                    {
                                        pairitem.DWSCode = cDWSCode;
                                        pairitem.Date = pairitem.ClockIN.EventTime.Date;
                                    }
                                }
                                #endregion
                            }
                        }
                        #endregion
                    }
                    else if (pairitem.ClockOUT != null)
                    {
                        #region " CLOCKOUT "
                        cYYYYMM = pairitem.ClockOUT.EventTime.ToString("yyyyMM");
                        if (!listMWS.ContainsKey(cYYYYMM))
                        {
                            oMWS = MonthlyWS.GetCalendar(cEmpID, pairitem.ClockOUT.EventTime.Year, pairitem.ClockOUT.EventTime.Month);
                            listMWS.Add(cYYYYMM, oMWS);
                        }
                        else
                        {
                            oMWS = listMWS[cYYYYMM];
                        }

                        cDWSCode = oMWS.GetWSCode(pairitem.ClockOUT.EventTime.Day, true);
                        isHoliday = oMWS.GetWSCode(pairitem.ClockOUT.EventTime.Day, false) != "";

                        // OrgAssignment not support --- > Load new
                        if (oEmp == null || oEmp.OrgAssignment == null || !(oEmp.OrgAssignment.BeginDate <= pairitem.ClockOUT.EventTime && oEmp.OrgAssignment.EndDate >= pairitem.ClockOUT.EventTime))
                        {
                            oEmp = new EmployeeData(cEmpID, pairitem.ClockOUT.EventTime);
                        }

                        cDailyGroup = oEmp.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping;

                        Dictionary<string, DailyWS> subList;
                        if (listDWS.ContainsKey(cDailyGroup))
                        {
                            subList = listDWS[cDailyGroup];
                        }
                        else
                        {
                            subList = new Dictionary<string, DailyWS>();
                            listDWS.Add(cDailyGroup, subList);
                        }

                        if (subList.ContainsKey(cDWSCode))
                        {
                            oDWS = subList[cDWSCode];
                        }
                        else
                        {
                            oDWS = EmployeeManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetDailyWorkschedule(cDailyGroup, cDWSCode, oEmp.CheckDate);
                            subList.Add(cDWSCode, oDWS);
                        }

                        pairitem.IsHoliday = isHoliday;

                        if ("0,9,".IndexOf(oDWS.WorkscheduleClass.Trim() + ",") > -1)
                        {
                            // if DWS is off
                            pairitem.IsDayOff = true;
                        }
                        else
                        {
                            pairitem.IsDayOff = false;
                        }

                        #region " case (null,out)"
                        if ("1,2,".IndexOf(oMWS.ValuationClass.Trim() + ",") > -1)
                        {
                            // if (norm or tnom)
                            pairitem.DWSCode = cDWSCode;
                            pairitem.Date = pairitem.ClockOUT.EventTime.Date;
                        }
                        else
                        {
                            // if shift cutoff at 7:30
                            if (pairitem.ClockOUT.EventTime.TimeOfDay < new TimeSpan(8, 30, 0))
                            {
                                DateTime oYesterday = pairitem.ClockOUT.EventTime.AddDays(-1);
                                string cYYYYMM_temp = oYesterday.ToString("yyyyMM");
                                MonthlyWS oMWS_temp;
                                DailyWS oDWS_temp;
                                if (!listMWS.ContainsKey(cYYYYMM_temp))
                                {
                                    oMWS_temp = MonthlyWS.GetCalendar(pairitem.ClockOUT.EmployeeID, oYesterday);
                                    listMWS.Add(cYYYYMM_temp, oMWS);
                                }
                                else
                                {
                                    oMWS_temp = listMWS[cYYYYMM_temp];
                                }
                                string cDWSCode_temp = oMWS_temp.GetWSCode(oYesterday.Day, true);
                                bool isHoliday_temp = oMWS_temp.GetWSCode(oYesterday.Day, false) != "";

                                // OrgAssignment not support --- > Load new
                                if (!(oEmp.OrgAssignment.BeginDate <= pairitem.ClockOUT.EventTime && oEmp.OrgAssignment.EndDate >= pairitem.ClockOUT.EventTime))
                                {
                                    oEmp = new EmployeeData(cEmpID, pairitem.ClockOUT.EventTime);
                                }

                                cDailyGroup = oEmp.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping;

                                Dictionary<string, DailyWS> subList_temp;

                                if (listDWS.ContainsKey(cDailyGroup))
                                {
                                    subList_temp = listDWS[cDailyGroup];
                                }
                                else
                                {
                                    subList_temp = new Dictionary<string, DailyWS>();
                                    listDWS.Add(cDailyGroup, subList_temp);
                                }
                                if (subList_temp.ContainsKey(cDWSCode_temp))
                                {
                                    oDWS_temp = subList_temp[cDWSCode_temp];
                                }
                                else
                                {
                                    oDWS_temp = EmployeeManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetDailyWorkschedule(cDailyGroup, cDWSCode_temp, oYesterday);
                                    subList_temp.Add(cDWSCode_temp, oDWS_temp);
                                }
                                if ("0,9,".IndexOf(oDWS_temp.WorkscheduleClass.Trim() + ",") > -1)
                                {
                                    // if DWS is off
                                    pairitem.IsDayOff = true;
                                }
                                else
                                {
                                    pairitem.IsDayOff = false;
                                }
                                pairitem.IsHoliday = isHoliday_temp;

                                pairitem.DWSCode = cDWSCode_temp;
                                pairitem.Date = oYesterday.Date;
                            }
                            else
                            {
                                pairitem.DWSCode = cDWSCode;
                                pairitem.Date = pairitem.ClockOUT.EventTime.Date;
                            }
                        }
                        #endregion
                        #endregion
                    }
                    if (pairitem.Date == DateTime.MinValue || (pairitem.Date > DateTime.Now.Date && DeleteFutureDate))
                    {
                        if (delList.Count > 0)
                        {
                            TimePair oLastPair = delList[delList.Count - 1];
                            if (oLastPair.Date == DateTime.Now.Date.AddDays(-1) && oLastPair.ClockOUT == null)
                            {
                                delList.Remove(oLastPair);
                            }
                        }
                        delList.Add(pairitem);
                    }
                    //else if (pairitem.Date >= DateTime.Now.Date.AddDays(-1) && DeleteFutureDate && pairitem.ClockOUT == null)
                    //{
                    //    delList.Add(pairitem);
                    //}
                    else if (DeleteOutSide && ((BeginDate != DateTime.MinValue && pairitem.Date < BeginDate) || (EndDate != DateTime.MinValue && pairitem.Date > EndDate)))
                    {
                        delList1.Add(pairitem);
                    }
                    else if (!dateList.Contains(pairitem.Date))
                    {
                        dateList.Add(pairitem.Date);
                    }
                }

                TimeElement lastEvent = null;
                foreach (TimePair pairitem in delList)
                {
                    if (lastEvent == null)
                    {
                        if (pairitem.ClockIN != null)
                        {
                            lastEvent = pairitem.ClockIN;
                        }
                        else if (pairitem.ClockOUT != null)
                        {
                            lastEvent = pairitem.ClockOUT;
                        }
                    }
                    pairs.Remove(pairitem);
                }

                foreach (TimePair pairitem in delList1)
                {
                    pairs.Remove(pairitem);
                }


                if (lastEvent != null)
                {
                    for (int index = clock.Count - 1; index >= 0; index--)
                    {
                        if (clock[index] >= lastEvent)
                        {
                            clock.RemoveAt(index);
                        }
                        else
                        {
                            break;
                        }
                    }
                }

                #endregion
            }
            else
            {
                oEmp = new EmployeeData(cEmpID, BeginDate);
            }

            #region " FillBlankPair "
            if (FillBlankPair)
            {
                DateTime dateMin, dateMax;
                if (dateList.Count > 0 || (BeginDate != DateTime.MinValue && EndDate != DateTime.MinValue))
                {
                    if (BeginDate != DateTime.MinValue && EndDate != DateTime.MinValue)
                    {
                        dateMin = BeginDate;
                        dateMax = EndDate;
                    }
                    else
                    {
                        dateMin = dateList[0];
                        dateMax = dateList[dateList.Count - 1];
                    }
                    for (DateTime rundate = dateMin.Date; rundate <= dateMax.Date; rundate = rundate.AddDays(1))
                    {
                        if (!dateList.Contains(rundate))
                        {
                            cYYYYMM = rundate.ToString("yyyyMM");
                            if (!listMWS.ContainsKey(cYYYYMM))
                            {
                                oMWS = MonthlyWS.GetCalendar(cEmpID, rundate.Year, rundate.Month);
                                listMWS.Add(cYYYYMM, oMWS);
                            }
                            else
                            {
                                oMWS = listMWS[cYYYYMM];
                            }

                            cDWSCode = oMWS.GetWSCode(rundate.Day, true);
                            if (cDWSCode == null)
                            {
                                cDWSCode = "";
                            }
                            isHoliday = oMWS.GetWSCode(rundate.Day, false) != "";
                            // OrgAssignment not support --- > Load new
                            if (oEmp.OrgAssignment == null || !(oEmp.OrgAssignment.BeginDate <= rundate && oEmp.OrgAssignment.EndDate >= rundate))
                            {
                                oEmp = new EmployeeData(cEmpID, rundate);
                            }

                            if (oEmp.OrgAssignment != null && oEmp.OrgAssignment.SubAreaSetting != null)
                            {
                                cDailyGroup = oEmp.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping;

                                Dictionary<string, DailyWS> subList;
                                if (listDWS.ContainsKey(cDailyGroup))
                                {
                                    subList = listDWS[cDailyGroup];
                                }
                                else
                                {
                                    subList = new Dictionary<string, DailyWS>();
                                    listDWS.Add(cDailyGroup, subList);
                                }

                                if (subList.ContainsKey(cDWSCode))
                                {
                                    oDWS = subList[cDWSCode];
                                }
                                else
                                {
                                    oDWS = EmployeeManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetDailyWorkschedule(cDailyGroup, cDWSCode, oEmp.CheckDate);
                                    subList.Add(cDWSCode, oDWS);
                                }
                                TimePair oBlankPair = new TimePair();
                                oBlankPair.EmployeeID = cEmpID;
                                oBlankPair.Date = rundate;
                                oBlankPair.DWSCode = cDWSCode;
                                pairs.Add(oBlankPair);
                                oBlankPair.IsHoliday = isHoliday;

                                if (oDWS != null && oDWS.WorkscheduleClass != null)
                                {
                                    if ("0,9,".IndexOf(oDWS.WorkscheduleClass.Trim() + ",") > -1)
                                    {
                                        // if DWS is off
                                        oBlankPair.IsDayOff = true;
                                    }
                                    else
                                    {
                                        oBlankPair.IsDayOff = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            #endregion
            pairs.Sort(new TimePairSortingDate());
            clock.Sort(new TimeElementSortingTime());
        }
        #endregion

        public List<TimesheetData> LoadTimesheet(string EmployeeID, string ApplicationKey, string SubKey1, string SubKey2, DateTime BeginDate, DateTime EndDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.LoadTimesheet(EmployeeID, ApplicationKey, SubKey1, SubKey2, BeginDate, EndDate);
        }

        public List<TimePairArchive> LoadTimePairArchive(DateTime BeginDate, DateTime EndDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.LoadTimePairArchive(BeginDate, EndDate);
        }

        public List<TimePairArchive> LoadTimePairArchiveOverlap(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.LoadTimePairArchiveOverlap(EmployeeID, BeginDate, EndDate);
        }


        //AddBy: Ratchatawan W. (2012-11-20)
        public void SaveTimePairArchiveMappingByUser(List<TimePair> TimePairList)
        {
            ServiceManager.CreateInstance(CompanyCode).ESSData.SaveTimePairArchiveMappingByUser(TimePairList);
        }
        public List<TimePair> LoadTimePairArchiveMappingByUser(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            return ServiceManager.CreateInstance(CompanyCode).ESSData.LoadTimePairArchiveMappingByUser(EmployeeID,BeginDate,EndDate);
        }
    }
}
