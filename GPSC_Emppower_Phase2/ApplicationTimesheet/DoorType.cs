using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.TIMESHEET
{
    public enum DoorType
    {
        IN = 0,
        OUT = 1,
        INOUT = 2,
        INOUT_OT = 4,
        IN_OT = 8,
        OUT_OT = 16
    }
}
