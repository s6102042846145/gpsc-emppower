using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.TIMESHEET
{
    public class AbstractERPTimesheetService : IERPTimesheetService
    {
        #region IERPTimesheetService Members

        public virtual void PostTimePair(List<TimePair> Data)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<TimeElement> LoadArchiveEvent(string EmployeeID, DateTime BeginDate, DateTime EndDate, int RecordCount)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<CardSetting> LoadCardSetting(DateTime BeginDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion
    }
}
