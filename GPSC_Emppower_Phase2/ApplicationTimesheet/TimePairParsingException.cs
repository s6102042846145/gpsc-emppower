using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.TIMESHEET
{
    public class TimePairParsingException : Exception
    {
        public TimePairParsingException() : base("Parsing TimePair must be format [IN|OUT]|yyyyMMddHHmmss#[IN|OUT]|yyyyMMddHHmmss")
        {
        }
    }
}
