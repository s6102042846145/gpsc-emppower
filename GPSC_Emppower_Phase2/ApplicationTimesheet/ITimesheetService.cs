using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.TIMESHEET
{
    public interface ITimesheetService
    {
        void MarkTimesheet(string EmployeeID, string ApplicationKey, string SubKey1, string SubKey2, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark, bool isCheck, string Detail);
        List<TimesheetData> LoadTimesheet(string EmployeeID, string ApplicationKey, string SubKey1, string SubKey2, DateTime BeginDate, DateTime EndDate);
        DateTime LoadArchiveDate(string EmployeeID);
        string LoadArchivePeriod();
        List<TimeElement> LoadArchiveEvent(string EmployeeID, DateTime BeginDate, DateTime EndDate, int RecordCount);
        void SaveArchiveEvent(string EmployeeID, DateTime BeginDate, DateTime EndDate, List<TimeElement> Data);
        List<CardSetting> LoadCardSetting(string EmployeeID, DateTime BeginDate, DateTime EndDate);
        List<CardSetting> LoadCardSetting(string EmployeeID);
        List<CardSetting> LoadCardSetting(string EmployeeID, DateTime CheckDate);
        string LoadTextData(string Code, string Language);
        void SaveArchiveTimePair(string EmployeeID, DateTime BeginDate, DateTime EndDate, List<TimePair> list);
        List<TimePair> LoadArchiveTimePair(string EmployeeID, DateTime BeginDate, DateTime EndDate);
        List<TimePair> LoadArchiveTimePair(string EmployeeID, DateTime BeginDate, DateTime EndDate,bool OnlyFirstRecord);
        List<TimeElement> LoadArchiveEventRelative(string EmployeeID, DateTime BeginDate, DateTime EndDate);
        void SaveCardSetting(string EmployeeID, List<CardSetting> cardList);
        void SaveCardSetting(List<CardSetting> cardList);
        List<TimePairArchive> LoadTimePairArchive(String EmpID, DateTime BeginDate, DateTime EndDate);
        List<TimePairArchive> LoadTimePairArchive(DateTime BeginDate, DateTime EndDate);
        List<TimePairArchive> LoadTimePairArchiveOverlap(String EmployeeID, DateTime BeginDate, DateTime EndDate);
        //AddBy: Ratchatawan W. (2012-11-20)
        void SaveTimePairArchiveMappingByUser(List<TimePair> TimePairList);
        List<TimePair> LoadTimePairArchiveMappingByUser(string EmployeeID, DateTime BeginDate, DateTime EndDate);
    }
}
