using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.TIMESHEET
{
    public class TimePairPostingException : Exception
    {
        public TimePairPostingException(string message)
            : base(message)
        { 
        }
        public TimePairPostingException(string message, Exception innerException)
            : base (message,innerException)
        { 
        }
    }
}
