using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ESS.TIMESHEET
{
    public class TimesheetCollisionException : Exception
    {
        private List<TimesheetData> __list;
        public TimesheetCollisionException(List<TimesheetData> List)
            : base("Collission occur with data")
        {
            __list = List;
        }
        public List<TimesheetData> CollissionData
        {
            get
            {
                return __list;
            }
        }
    }
}
