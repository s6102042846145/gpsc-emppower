using System.Configuration;
using System.Reflection;
using ESS.HR.TM.ABSTRACT;
using ESS.HR.TM.DATACLASS;
using ESS.SHAREDATASERVICE;
using System;
using System.Collections.Generic;
using ESS.UTILITY.EXTENSION;
using System.Data;
using System.Data.SqlClient;
using ESS.HR.TM.INFOTYPE;
using ESS.EMPLOYEE;
using ESS.HR.TM.CONFIG;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.WORKFLOW;
using ESS.TIMESHEET;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using System.IO;

namespace ESS.HR.TM.DB
{
    public class HRTMDataServiceImpl : AbstractHRTMDataService
    {

        #region Constructor
        public HRTMDataServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
            oSqlManage["BaseConnStr"] = BaseConnStr;
        }

        #endregion Constructor

        #region Member
        //private Dictionary<string, string> Config { get; set; }
        private Dictionary<string, string> oSqlManage = new Dictionary<string, string>();
        private static string ModuleID = "ESS.HR.TM.DB";

        public string CompanyCode { get; set; }
        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }

        private string KeySecret
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, "ESS.HR.PY", "GPSC_ENCRYPTION_KEY");
            }
        }

        #endregion Member


        #region " MarkUpdate "

        public override void MarkUpdate(string EmployeeID, string DataCategory, string RequestNo, bool isMarkIn)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select * from MarkData where EmployeeID = @EmpID and DataCategory = @DataCategory ", oConnection, tx);
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@DataCategory", SqlDbType.VarChar);
            oParam.Value = DataCategory;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("MARKDATA");
            try
            {
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                if (oTable.Rows.Count == 0 && isMarkIn)
                {
                    DataRow oRow = oTable.NewRow();
                    oRow["EmployeeID"] = EmployeeID;
                    oRow["DataCategory"] = DataCategory;
                    oRow["RequestNo"] = RequestNo;
                    oTable.Rows.Add(oRow);
                }
                else if (oTable.Rows.Count > 0 && !isMarkIn)
                {
                    foreach (DataRow dr in oTable.Select())
                    {
                        dr.Delete();
                    }
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }

        #endregion " MarkUpdate "

        #region " CheckMark "

        public override bool CheckMark(string EmployeeID, string DataCategory)
        {
            bool oReturn = false;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select Count(*) from MarkData where EmployeeID =" +
                                                 " @EmpID and DataCategory = @DataCategory", oConnection);
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@DataCategory", SqlDbType.VarChar);
            oParam.Value = DataCategory;
            oCommand.Parameters.Add(oParam);
            try
            {
                oConnection.Open();
                oReturn = ((int)oCommand.ExecuteScalar()) > 0;

            }
            catch (Exception ex)
            {
                throw new Exception("CheckMark error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
            }
            return oReturn;
        }

        #endregion " CheckMark "

        #region Check MarkData

        public override bool CheckMark(string EmployeeID, string DataCategory, string ReqNo)
        {
            bool oReturn = false;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select Count(*) from MarkData where EmployeeID =" +
                                                 " @EmpID and DataCategory = @DataCategory and RequestNo <> @ReqNo", oConnection);
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@DataCategory", SqlDbType.VarChar);
            oParam.Value = DataCategory;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@ReqNo", SqlDbType.VarChar);
            oParam.Value = ReqNo;
            oCommand.Parameters.Add(oParam);
            try
            {
                oConnection.Open();
                oReturn = ((int)oCommand.ExecuteScalar()) > 0;

            }
            catch (Exception ex)
            {
                throw new Exception("CheckMark error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
            }
            return oReturn;
        }

        #endregion Check MarkData

        #region " GetAbsenceList "
        public override List<INFOTYPE2001> GetAbsenceList(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            List<INFOTYPE2001> oReturn = new List<INFOTYPE2001>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            if (BeginDate.TimeOfDay <= new TimeSpan(0, 0, 0))
                oCommand = new SqlCommand("select * from INFOTYPE2001 Where EmployeeID = @EmpID and (BeginDate <= @EndDate and EndDate >= @BeginDate)", oConnection);
            else
                oCommand = new SqlCommand("select * from INFOTYPE2001 Where EmployeeID = @EmpID and (BeginDate <= @EndDate and EndDate >= @BeginDate) AND (BeginTime >= @BeginTime AND EndTime <= @EndTime)", oConnection);

            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate.Date;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate.Date;
            oCommand.Parameters.Add(oParam);

            if (BeginDate.TimeOfDay > new TimeSpan(0, 0, 0))
            {
                oParam = new SqlParameter("@BeginTime", SqlDbType.VarChar);
                oParam.Value = BeginDate.TimeOfDay.ToString();
                oCommand.Parameters.Add(oParam);
            }
            if (EndDate.TimeOfDay > new TimeSpan(0, 0, 0))
            {
                oParam = new SqlParameter("@EndTime", SqlDbType.VarChar);
                oParam.Value = EndDate.TimeOfDay.ToString();
                oCommand.Parameters.Add(oParam);
            }

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE2001");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE2001 item = new INFOTYPE2001();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oTable.Dispose();
            return oReturn;
        }

        public override List<INFOTYPE2001> GetAbsenceList(string EmployeeID1, string EmployeeID2, DateTime BeginDate, DateTime EndDate)
        {
            List<INFOTYPE2001> oReturn = new List<INFOTYPE2001>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            if (BeginDate.TimeOfDay <= new TimeSpan(0, 0, 0))
                oCommand = new SqlCommand("select * from INFOTYPE2001 Where EmployeeID between @EmpID1 and @EmpID2 and (BeginDate <= @EndDate and EndDate >= @BeginDate)", oConnection);
            else
                oCommand = new SqlCommand("select * from INFOTYPE2001 Where EmployeeID between @EmpID1 and @EmpID2 and (BeginDate <= @EndDate and EndDate >= @BeginDate) AND (BeginTime >= @BeginTime AND EndTime <= @EndTime)", oConnection);

            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID1;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EmpID2", SqlDbType.VarChar);
            oParam.Value = EmployeeID2;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate.Date;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate.Date;
            oCommand.Parameters.Add(oParam);

            if (BeginDate.TimeOfDay > new TimeSpan(0, 0, 0))
            {
                oParam = new SqlParameter("@BeginTime", SqlDbType.VarChar);
                oParam.Value = BeginDate.TimeOfDay.ToString();
                oCommand.Parameters.Add(oParam);
            }
            if (EndDate.TimeOfDay > new TimeSpan(0, 0, 0))
            {
                oParam = new SqlParameter("@EndTime", SqlDbType.VarChar);
                oParam.Value = EndDate.TimeOfDay.ToString();
                oCommand.Parameters.Add(oParam);
            }

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE2001");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE2001 item = new INFOTYPE2001();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oTable.Dispose();
            return oReturn;
        }

        public override List<INFOTYPE2001> GetAbsenceListForCheckData(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            List<INFOTYPE2001> oReturn = new List<INFOTYPE2001>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE2001 Where EmployeeID = @EmpID " +
                                                 "AND " +
                                                 "( " +
                                                     "BeginDate between @BeginDate and @EndDate " +
                                                     "OR " +
                                                     "EndDate between @BeginDate and @EndDate " +
                                                 ")", oConnection);

            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE2001");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE2001 item = new INFOTYPE2001();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oTable.Dispose();
            return oReturn;
        }
        #endregion

        #region " SaveAbsence "
        public override void SaveAbsence(List<INFOTYPE2001> data)
        {
            if (data.Count == 0)
            {
                return;
            }
            data.Sort(new INFOTYPE2001_Comparer());
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE2001 where RequestNo = @RequestNo", oConnection, tx);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);

            DataTable oTable = new DataTable("INFOTYPE2001");

            oParam = new SqlParameter("@RequestNo", SqlDbType.VarChar);
            oCommand.Parameters.Add(oParam);
            oAdapter.FillSchema(oTable, SchemaType.Source);

            List<string> requestList = new List<string>();

            foreach (INFOTYPE2001 item in data)
            {
                if (!requestList.Contains(item.RequestNo))
                {
                    requestList.Add(item.RequestNo);
                    oParam.Value = item.RequestNo;
                    oAdapter.Fill(oTable);
                }
            }

            try
            {
                foreach (INFOTYPE2001 item in data)
                {
                    DataRow dr = oTable.NewRow();
                    item.LoadDataToTableRow(dr);
                    dr = oTable.LoadDataRow(dr.ItemArray, false);
                    if (!item.IsMark)
                    {
                        dr.Delete();
                    }
                }

                foreach (DataRow dr in oTable.Rows)
                {
                    if (dr.RowState == DataRowState.Unchanged)
                    {
                        dr.Delete();
                    }
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }
        #endregion

        #region " GetAttendanceList "
        public override List<INFOTYPE2002> GetAttendanceList(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            List<INFOTYPE2002> oReturn = new List<INFOTYPE2002>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE2002 Where EmployeeID = @EmpID and (BeginDate <= @EndDate and EndDate >= @BeginDate) AND (BeginTime >= @BeginTime AND EndTime <= @EndTime)", oConnection);
            if (BeginDate.TimeOfDay <= new TimeSpan(0, 0, 0))
                oCommand = new SqlCommand("select * from INFOTYPE2002 Where EmployeeID = @EmpID and (BeginDate <= @EndDate and EndDate >= @BeginDate)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate.Date;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate.Date;
            oCommand.Parameters.Add(oParam);

            if (BeginDate.TimeOfDay > new TimeSpan(0, 0, 0))
            {
                oParam = new SqlParameter("@BeginTime", SqlDbType.VarChar);
                oParam.Value = BeginDate.TimeOfDay.ToString();
                oCommand.Parameters.Add(oParam);
            }
            if (EndDate.TimeOfDay > new TimeSpan(0, 0, 0))
            {
                oParam = new SqlParameter("@EndTime", SqlDbType.VarChar);
                oParam.Value = EndDate.TimeOfDay.ToString();
                oCommand.Parameters.Add(oParam);
            }

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE2002");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE2002 item = new INFOTYPE2002();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oTable.Dispose();
            return oReturn;
        }
        #endregion

        #region " SaveAttendance "
        public override void SaveAttendance(List<INFOTYPE2002> data)
        {
            if (data.Count == 0)
            {
                return;
            }
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE2002 where RequestNo = @RequestNo", oConnection, tx);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);

            DataTable oTable = new DataTable("INFOTYPE2002");

            oParam = new SqlParameter("@RequestNo", SqlDbType.VarChar);
            oCommand.Parameters.Add(oParam);
            oAdapter.FillSchema(oTable, SchemaType.Source);

            List<string> requestList = new List<string>();

            foreach (INFOTYPE2002 item in data)
            {
                if (!requestList.Contains(item.RequestNo))
                {
                    requestList.Add(item.RequestNo);
                    oParam.Value = item.RequestNo;
                    oAdapter.Fill(oTable);
                }
            }

            try
            {
                foreach (INFOTYPE2002 item in data)
                {
                    DataRow dr = oTable.NewRow();
                    //CommentBy: Ratchatawan W. (2012-02-20)
                    //if (item.BeginTime == TimeSpan.MinValue)
                    //{
                    //    throw new Exception("BEGINTIME_CAN_NOT_BE_NULL");
                    //}
                    item.LoadDataToTableRow(dr);
                    dr = oTable.LoadDataRow(dr.ItemArray, false);
                    if (!item.IsMark)
                    {
                        dr.Delete();
                    }
                }

                foreach (DataRow dr in oTable.Rows)
                {
                    if (dr.RowState == DataRowState.Unchanged)
                    {
                        dr.Delete();
                    }
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }
        #endregion

        #region " SaveSubstitution "
        public override void SaveSubstitution(List<INFOTYPE2003> data)
        {
            if (data.Count == 0)
            {
                return;
            }
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE2003 where RequestNo = @RequestNo", oConnection, tx);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);

            DataTable oTable = new DataTable("INFOTYPE2003");

            oParam = new SqlParameter("@RequestNo", SqlDbType.VarChar);
            oCommand.Parameters.Add(oParam);
            oAdapter.FillSchema(oTable, SchemaType.Source);

            List<string> requestList = new List<string>();

            foreach (INFOTYPE2003 item in data)
            {
                if (!requestList.Contains(item.RequestNo))
                {
                    requestList.Add(item.RequestNo);
                    oParam.Value = item.RequestNo;
                    oAdapter.Fill(oTable);
                }
            }

            try
            {
                foreach (INFOTYPE2003 item in data)
                {
                    DataRow dr = oTable.NewRow();
                    item.LoadDataToTableRow(dr);
                    dr = oTable.LoadDataRow(dr.ItemArray, false);
                    if (!item.IsMark)
                    {
                        dr.Delete();
                    }
                }

                foreach (DataRow dr in oTable.Rows)
                {
                    if (dr.RowState == DataRowState.Unchanged)
                    {
                        dr.Delete();
                    }
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }
        #endregion

        #region "SaveSubstitutionLog"
        public override void SaveSubstitutionLog(System.Data.DataRow data)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE2003_Log", oConnection, tx);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("INFOTYPE2003_Log");
            oAdapter.FillSchema(oTable, SchemaType.Source);

            List<string> requestList = new List<string>();
            DeleteSubStitution(data);

            try
            {
                if ((bool)data["IsMark"])
                {
                    DataRow NewRow = oTable.NewRow();
                    foreach (DataColumn col in oTable.Columns)
                        NewRow[col.ColumnName] = data[col.ColumnName];
                    oTable.Rows.Add(NewRow);
                    oAdapter.Update(oTable);
                    tx.Commit();
                }
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }

        public override void SaveSubstitutionLog(List<INFOTYPE2003> data)
        {
            if (data.Count == 0)
            {
                return;
            }
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select * from INFOTYPE2003_Log where RequestNo = @RequestNo", oConnection, tx);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);

            DataTable oTable = new DataTable("INFOTYPE2003_Log");

            oParam = new SqlParameter("@RequestNo", SqlDbType.VarChar);
            oCommand.Parameters.Add(oParam);
            oAdapter.FillSchema(oTable, SchemaType.Source);

            List<string> requestList = new List<string>();

            foreach (INFOTYPE2003 item in data)
            {
                if (!requestList.Contains(item.RequestNo))
                {
                    requestList.Add(item.RequestNo);
                    oParam.Value = item.RequestNo;
                    oAdapter.Fill(oTable);
                }
            }

            try
            {
                foreach (INFOTYPE2003 item in data)
                {
                    DataRow dr = oTable.NewRow();
                    item.LoadDataToTableRow(dr);
                    dr = oTable.LoadDataRow(dr.ItemArray, false);
                    //Edit by Koissares 20161025 ���ź INFOTYPE2003_Log ��ʶҹ�᷹
                    //if (!item.IsMark)
                    //{
                    //    dr.Delete();
                    //}
                }

                foreach (DataRow dr in oTable.Rows)
                {
                    if (dr.RowState == DataRowState.Unchanged)
                    {
                        dr.Delete();
                    }
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }

        public override void CancelSubstitutionLog(DataRow data)
        {
            if (!DeleteSubStitution(data))
                throw new Exception("cannot cancel substitution.");

        }

        bool DeleteSubStitution(DataRow item)
        {
            bool result = false;
            if (item == null)
            {
                return true;
            }
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand;

            oCommand = new SqlCommand("delete from INFOTYPE2003_Log where RequestNo = @RequestNo and EmployeeID=@EmployeeID and BeginDate=@BeginDate and EndDate=@EndDate and Substitute=@Substitute", oConnection, tx);
            oParam = new SqlParameter("@RequestNo", SqlDbType.VarChar);
            oParam.Value = item["RequestNo"];
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            oParam.Value = item["EmployeeID"];
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = item["BeginDate"];
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = item["EndDate"];
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Substitute", SqlDbType.VarChar);
            oParam.Value = item["Substitute"];
            oCommand.Parameters.Add(oParam);

            try
            {
                oCommand.ExecuteNonQuery();
                result = true;
            }
            catch (Exception e)
            {
                tx.Rollback();
                oCommand.Dispose();
                oConnection.Close();
                oConnection.Dispose();
                return result;
            }
            tx.Commit();
            oCommand.Dispose();
            oConnection.Close();
            oConnection.Dispose();
            return result;
        }
        #endregion

        #region SaveOTList1
        public override void SaveOTList1(List<SaveOTData> data, bool IsMark, List<string> RequestNoList, bool deleteItemNotInList)
        {
            if (data.Count == 0)
            {
                return;
            }

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select * from OTData where RequestNo = @RequestNo", oConnection, tx);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            DataTable oTable = new DataTable();

            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);



            oParam = new SqlParameter("@RequestNo", SqlDbType.VarChar);
            oParam.Value = data[0].RequestNo;

            oCommand.Parameters.Add(oParam);
            oAdapter.FillSchema(oTable, SchemaType.Source);

            try
            {
                foreach (SaveOTData item in data)
                {
                    DataRow dr = oTable.NewRow();
                    item.LoadDataToTableRow(dr);
                    oTable.LoadDataRow(dr.ItemArray, false);
                }
                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();

            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }
        #endregion

        #region " SaveOTList "
        public override void SaveOTList(List<SaveOTData> data, bool IsMark, List<string> RequestNoList, bool deleteItemNotInList)
        {
            if (data.Count == 0 && IsMark)
            {
                return;
            }
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select * from OTData where RequestNo = @RequestNo", oConnection, tx);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);

            DataTable oTable = new DataTable("PLANNENDOT");

            oParam = new SqlParameter("@RequestNo", SqlDbType.VarChar);
            oCommand.Parameters.Add(oParam);
            oAdapter.FillSchema(oTable, SchemaType.Source);

            List<string> requestList = new List<string>();

            if (RequestNoList == null)
            {
                foreach (SaveOTData item in data)
                {
                    if (!requestList.Contains(item.RequestNo))
                    {
                        requestList.Add(item.RequestNo);
                        oParam.Value = item.RequestNo;
                        oAdapter.Fill(oTable);
                    }
                }
            }
            else
            {
                foreach (string requestNo in RequestNoList)
                {
                    requestList.Add(requestNo);
                    oParam.Value = requestNo;
                    oAdapter.Fill(oTable);
                }
            }

            try
            {
                foreach (SaveOTData item in data)
                {
                    if (IsMark || (!item.IsSummary && item.IsCompleted))
                    {
                        item.BeginTime = new TimeSpan(item.BeginTime.Hours, item.BeginTime.Minutes, item.BeginTime.Seconds);
                        item.EndTime = new TimeSpan(item.EndTime.Hours, item.EndTime.Minutes, item.EndTime.Seconds);
                        DataRow dr = oTable.NewRow();
                        item.LoadDataToTableRow(dr);
                        oTable.LoadDataRow(dr.ItemArray, false);
                    }
                    //if (IsMark || !item.IsSummary)
                    //{
                    //    item.BeginTime = new TimeSpan(item.BeginTime.Hours, item.BeginTime.Minutes, item.BeginTime.Seconds);
                    //    item.EndTime = new TimeSpan(item.EndTime.Hours, item.EndTime.Minutes, item.EndTime.Seconds);
                    //    DataRow dr = oTable.NewRow();
                    //    item.LoadDataToTableRow(dr);
                    //    oTable.LoadDataRow(dr.ItemArray, false);
                    //}
                }

                if (deleteItemNotInList && (!IsMark || RequestNoList == null))
                {
                    foreach (DataRow dr in oTable.Rows)
                    {
                        if (dr.RowState == DataRowState.Unchanged)
                        {
                            dr.Delete();
                        }
                    }

                }
                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();

            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }
        #endregion

        private string TimeSpanToString(TimeSpan input)
        {
            DateTime buffer = DateTime.Now.Date;
            buffer = buffer.Add(input);
            return buffer.ToString("HH:mm:ss");
        }

        public override void DeleteDailyOT(DailyOT daily)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlCommand oCommand = new SqlCommand("delete from OTData where EmployeeID = @EmployeeID and OTType = 'D' and OTDate = @OTDate and BeginTime = @BeginTime and EndTime = @EndTime", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            oParam.Value = daily.EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@OTDate", SqlDbType.DateTime);
            oParam.Value = daily.OTDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BeginTime", SqlDbType.VarChar);
            oParam.Value = TimeSpanToString(daily.BeginTime);
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EndTime", SqlDbType.VarChar);
            oParam.Value = TimeSpanToString(daily.EndTime);
            oCommand.Parameters.Add(oParam);

            try
            {
                int nResult = oCommand.ExecuteNonQuery();
                if (nResult == 0)
                {
                    throw new Exception("NO_ROWS");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
            }
        }

        #region " LoadPlannedOT "
        public override List<PlannedOT> LoadPlannedOT(string EmployeeID, DateTime BeginDate, DateTime EndDate, bool OnlyOneRecord, bool OnlyCompleteRecord)
        {
            List<PlannedOT> oReturn = new List<PlannedOT>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            string subQry = OnlyCompleteRecord ? " And IsCompleted = 1 " : "";
            if (OnlyOneRecord)
            {
                oCommand = new SqlCommand("select top 1 * from V_OTData Where EmployeeID = @EmpID and BeginDate <= @EndDate and EndDate > @BeginDate and OTType = 'P'" + subQry, oConnection);
            }
            else
            {
                oCommand = new SqlCommand("select * from V_OTData Where EmployeeID = @EmpID and BeginDate <= @EndDate and EndDate > @BeginDate and OTType = 'P'" + subQry, oConnection);
            }
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OTDATA");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                PlannedOT item = new PlannedOT();
                item = new PlannedOT();
                item.ParseToObject(dr);
                oReturn.Add(item);
                if (OnlyOneRecord)
                    break;
            }

            oTable.Dispose();
            return oReturn;
        }
        #endregion

        #region "LoadDailyOTLog"
        public override List<DailyOTLog> LoadDailyOTLog(int Month, int Year)
        {
            List<DailyOTLog> oReturn = new List<DailyOTLog>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_GetDailyOTLog", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter param = new SqlParameter("@p_Month", SqlDbType.Int);
            param.Value = Month;
            oCommand.Parameters.Add(param);

            param = new SqlParameter("@p_Year", SqlDbType.Int);
            param.Value = Year;
            oCommand.Parameters.Add(param);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OTLOG");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();

            foreach (DataRow dr in oTable.Rows)
            {
                DailyOTLog item = new DailyOTLog();

                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oConnection.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();
            oTable.Dispose();

            return oReturn;
        }

        public override void SaveInLateOutEarly(List<ClockinLateClockoutEarly> List, string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select * from TM_InLateOutEarly where EmployeeID = @EmployeeID and [Date] between @BeginDate and @EndDate", oConnection, tx);
            oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = null;
            DataTable oTable = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);
            oTable = new DataTable("TM_InLateOutEarly");
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);

            DataRow dr;
            try
            {
                //Update table
                foreach (ClockinLateClockoutEarly item in List.FindAll(delegate (ClockinLateClockoutEarly clock) { return (clock.Date >= BeginDate && clock.Date <= EndDate); }))
                {
                    DataRow[] Rows = oTable.Select(string.Format("EmployeeID='{0}' AND Date='#{1}#'", item.EmployeeID, item.Date.ToString()));
                    if (Rows.Length > 0)
                    {
                        Rows[0]["Type"] = item.Type;
                        Rows[0]["ClockInLate"] = item.ClockInLate;
                        Rows[0]["ClockoutEarly"] = item.ClockoutEarly;
                        Rows[0]["ClockAbnormal"] = item.ClockAbnormal;
                        Rows[0]["ClockNoneWork"] = item.ClockNoneWork;
                        Rows[0]["IsRemark"] = item.IsRemark;
                        for (int r = 1; r < Rows.Length; r++)
                        {
                            Rows[r].Delete();
                        }
                    }
                    else
                    {
                        dr = oTable.NewRow();
                        dr["EmployeeID"] = item.EmployeeID;
                        dr["Date"] = item.Date;
                        dr["Type"] = item.Type;
                        dr["ClockInLate"] = item.ClockInLate;
                        dr["ClockoutEarly"] = item.ClockoutEarly;
                        dr["ClockAbnormal"] = item.ClockAbnormal;
                        dr["ClockNoneWork"] = item.ClockNoneWork;
                        dr["IsRemark"] = item.IsRemark;
                        oTable.LoadDataRow(dr.ItemArray, false);
                    }
                }

                oAdapter.Update(oTable);
                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("Save data error:", ex);
            }
            finally
            {
                if (oConnection.State == ConnectionState.Open)
                    oConnection.Close();

                oConnection.Dispose();
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                if (oCommand != null)
                {
                    oCommand.Dispose();
                }
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oTable != null)
                {
                    oTable.Dispose();
                }
            }
        }

        public override void SaveTimeEval(List<TimeEval> TimeEvalList)
        {

            // Remove duplicate
            Dictionary<string, TimeEval> dict = new Dictionary<string, TimeEval>();
            List<TimeEval> UniqueTimeEvalList = new List<TimeEval>();
            foreach (TimeEval tv in TimeEvalList)
            {
                string key = string.Format("{0}|{1}|{2}", tv.EmployeeID, tv.BeginDate, tv.EndDate);
                if (!dict.ContainsKey(key))
                {
                    dict[key] = tv;
                    UniqueTimeEvalList.Add(tv);
                }
            }

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlParameter param;
            SqlTransaction oTransaction = null;
            try
            {
                oConnection.Open();
                oTransaction = oConnection.BeginTransaction();

                #region Delete all time eval that have date in Month/Year

                //Find min begindate and max enddate
                DateTime minBeginDate = TimeEvalList[0].BeginDate;
                DateTime maxEndDate = TimeEvalList[0].EndDate;
                foreach (TimeEval item in TimeEvalList)
                {
                    if (item.BeginDate < minBeginDate)
                        minBeginDate = item.BeginDate;
                    if (item.EndDate > maxEndDate)
                        maxEndDate = item.EndDate;
                }

                oCommand = new SqlCommand("sp_TimeEvalDelete", oConnection, oTransaction);
                oCommand.CommandType = CommandType.StoredProcedure;

                param = new SqlParameter("@p_BeginDate", SqlDbType.DateTime);
                param.Value = minBeginDate;
                oCommand.Parameters.Add(param);

                param = new SqlParameter("@p_EndDate", SqlDbType.DateTime);
                param.Value = maxEndDate;
                oCommand.Parameters.Add(param);

                oCommand.ExecuteNonQuery();
                #endregion

                #region Insert new time eval
                oCommand = new SqlCommand("sp_TimeEvalSave", oConnection, oTransaction);
                oCommand.CommandType = CommandType.StoredProcedure;

                foreach (TimeEval timeEval in UniqueTimeEvalList)
                {
                    oCommand.Parameters.Clear();
                    param = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
                    param.Value = timeEval.EmployeeID;
                    oCommand.Parameters.Add(param);
                    param = new SqlParameter("@p_BeginDate", SqlDbType.DateTime);
                    param.Value = timeEval.BeginDate;
                    oCommand.Parameters.Add(param);
                    param = new SqlParameter("@p_EndDate", SqlDbType.DateTime);
                    param.Value = timeEval.EndDate;
                    oCommand.Parameters.Add(param);
                    param = new SqlParameter("@p_WageType", SqlDbType.VarChar);
                    param.Value = timeEval.WageType;
                    oCommand.Parameters.Add(param);
                    param = new SqlParameter("@p_WageHour", SqlDbType.Decimal);
                    param.Value = timeEval.WageHour;
                    oCommand.Parameters.Add(param);

                    oCommand.ExecuteNonQuery();
                }
                #endregion

                oTransaction.Commit();
            }
            catch (Exception ex)
            {
                oTransaction.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                if (oConnection.State == ConnectionState.Open)
                    oConnection.Close();
                oConnection.Dispose();
                oTransaction.Dispose();
            }
        }
        #endregion

        #region " LoadDailyOT "
        public override List<DailyOT> LoadDailyOT(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            List<DailyOT> oReturn = new List<DailyOT>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;

            oCommand = new SqlCommand("select * from V_OTData Where EmployeeID = @EmpID and BeginDate <= @EndDate and EndDate > @BeginDate and OTType = 'D'", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OTDATA");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                DailyOT item = new DailyOT();
                item = new DailyOT();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oTable.Dispose();
            return oReturn;
        }

        public override List<DailyOTLog> LoadDailyOTLog(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            return this.LoadDailyOTLog(string.Empty, EmployeeID, BeginDate, EndDate);
        }

        public override bool CheckDailyOTLogIsView(string RequestNo)
        {
            bool oReturn = false;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;

            oCommand = new SqlCommand("SELECT RequestNo FROM RequestDocument WHERE RequestNo = @RequestNo ", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@RequestNo", SqlDbType.VarChar);
            oParam.Value = RequestNo;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OTDATA");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            if (oTable != null && oTable.Rows.Count > 0)
            {
                oReturn = true;
            }

            oTable.Dispose();
            return oReturn;
        }


        public override List<DailyOTLog> LoadDailyOTLog(string RequestNo, string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            List<DailyOTLog> oReturn = new List<DailyOTLog>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            string sql = string.Empty;
            if (string.IsNullOrEmpty(RequestNo))
                sql = "select * from OTLog Where EmployeeID = @EmpID and BeginDate <= @EndDate and EndDate >= @BeginDate and BeginDate >= @BeginDate and Status <> 5 order by BeginDate";
            else
                sql = "select * from OTLog Where EmployeeID = @EmpID and BeginDate <= @EndDate and EndDate >= @BeginDate and BeginDate >= @BeginDate and RequestNo = @RequestNo and Status <> 5 order by BeginDate";

            oCommand = new SqlCommand(sql, oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);

            if (!string.IsNullOrEmpty(RequestNo))
            {
                oParam = new SqlParameter("@RequestNo", SqlDbType.VarChar);
                oParam.Value = RequestNo;
                oCommand.Parameters.Add(oParam);
            }

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OTDATA");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                DailyOTLog item = new DailyOTLog();
                item = new DailyOTLog();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oTable.Dispose();
            return oReturn;
        }

        public override List<DailyOTLog> LoadDailyOTLog(string EmployeeID, string BeginDate, string EndDate)
        {
            List<DailyOTLog> oReturn = new List<DailyOTLog>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            string sql = string.Empty;
            sql = "select * from OTLog Where EmployeeID = @EmpID and convert(varchar, BeginDate, 103) = @BeginDate and convert(varchar, EndDate, 103) = @EndDate order by BeginDate";

            oCommand = new SqlCommand(sql, oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BeginDate", SqlDbType.VarChar);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EndDate", SqlDbType.VarChar);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OTDATA");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                DailyOTLog item = new DailyOTLog();
                item = new DailyOTLog();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oTable.Dispose();
            return oReturn;
        }

        public override List<DailyOTLog> LoadDailyOTLog(string EmployeeID, string BeginDate)
        {
            List<DailyOTLog> oReturn = new List<DailyOTLog>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            string sql = string.Empty;
            sql = "select * from OTLog Where EmployeeID = @EmpID and convert(varchar, BeginDate, 103) = @BeginDate order by BeginDate";

            oCommand = new SqlCommand(sql, oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BeginDate", SqlDbType.VarChar);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OTDATA");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                DailyOTLog item = new DailyOTLog();
                item = new DailyOTLog();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oTable.Dispose();
            return oReturn;
        }

        public override bool OverlapDialyOTLog(string EmployeeID, DateTime BeginDate, DateTime EndDate, String RequestNo)
        {
            bool Result;
            List<DailyOTLog> oReturn = new List<DailyOTLog>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlCommand oCommand;
            string sql = "select Count(*) from OTLog Where EmployeeID = @EmpID and Status not in (4,5) and " +
                        "BeginDate < @EndDate and EndDate > @BeginDate " +
                        "and RequestNo != @RequestNo";

            oCommand = new SqlCommand(sql, oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@RequestNo", SqlDbType.VarChar);
            oParam.Value = RequestNo;
            oCommand.Parameters.Add(oParam);
            try
            {
                if ((int)oCommand.ExecuteScalar() > 0)
                    Result = !IsCancleDailyOT(EmployeeID, BeginDate, EndDate, RequestNo);
                else
                    Result = false;
            }
            catch
            {
                Result = false;
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
            }
            return Result;

        }

        public override bool IsCompleteCancleDailyOT(String EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            bool Result;
            List<DailyOTLog> oReturn = new List<DailyOTLog>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlCommand oCommand;
            string sql = "select Count(*) from OTLog Where EmployeeID = @EmpID and Status = 4 and " +
                        "((BeginDate < @BeginDate and @BeginDate < EndDate) or (BeginDate < @EndDate and @EndDate < EndDate) or " +
                        "(@BeginDate < BeginDate and BeginDate < @EndDate) or (@BeginDate < EndDate and EndDate < @EndDate)) ";

            oCommand = new SqlCommand(sql, oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);

            try
            {
                if ((int)oCommand.ExecuteScalar() > 0)
                    Result = false;
                else
                    Result = true;
            }
            catch
            {
                Result = false;
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
            }
            return Result;
        }

        bool IsCancleDailyOT(string EmployeeID, DateTime BeginDate, DateTime EndDate, String RequestNo)
        {
            bool Result;
            List<DailyOTLog> oReturn = new List<DailyOTLog>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlCommand oCommand;
            string sql = "select count(*) from OTLog where refrequestno in (" +
                        "select requestno from OTLog Where EmployeeID = @EmpID and Status not in (4,5) and " +
                        "((BeginDate < @BeginDate and @BeginDate < EndDate) or (BeginDate < @EndDate and @EndDate < EndDate) or " +
                        "(@BeginDate < BeginDate and BeginDate < @EndDate) or (@BeginDate < EndDate and EndDate < @EndDate)) " +
                        "and RequestNo != @RequestNo )";

            oCommand = new SqlCommand(sql, oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@RequestNo", SqlDbType.VarChar);
            oParam.Value = RequestNo;
            oCommand.Parameters.Add(oParam);
            try
            {
                if ((int)oCommand.ExecuteScalar() > 0)
                    Result = true;
                else
                    Result = false;
            }
            catch
            {
                Result = false;
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
            }
            return Result;
        }

        public override List<DailyOTLog> LoadDailyOTLog(string EmployeeID, DateTime BeginDate, DateTime EndDate, DailyOTLogStatus _DailyOTLogStatus)
        {
            List<DailyOTLog> oReturn = new List<DailyOTLog>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            string sql = "select * from OTLog Where EmployeeID = @EmpID and BeginDate <= @EndDate and EndDate >= @BeginDate and Status = @Status order by BeginDate";

            oCommand = new SqlCommand(sql, oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Status", SqlDbType.TinyInt);
            oParam.Value = (int)_DailyOTLogStatus;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OTDATA");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                DailyOTLog item = new DailyOTLog();
                item = new DailyOTLog();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oTable.Dispose();
            return oReturn;
        }

        public override List<DailyOTLog> LoadDailyOTLog(string EmployeeID)
        {
            List<DailyOTLog> oReturn = new List<DailyOTLog>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            string sql = "select * from OTLog Where EmployeeID = @EmpID";

            oCommand = new SqlCommand(sql, oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OTDATA");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                DailyOTLog item = new DailyOTLog();
                item = new DailyOTLog();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oTable.Dispose();
            return oReturn;
        }

        public override INFOTYPE2001 GetINFOTYPE2001ByRequestNo(string RequestNo)
        {
            INFOTYPE2001 oReturn = new INFOTYPE2001();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            string sql = "select * from INFOTYPE2001 Where RequestNo = @RequestNo";

            oCommand = new SqlCommand(sql, oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@RequestNo", SqlDbType.VarChar);
            oParam.Value = RequestNo;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE2001");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            if (oTable != null && oTable.Rows.Count > 0)
            {
                oReturn.ParseToObject(oTable.Rows[0]);
            }

            oTable.Dispose();
            return oReturn;
        }

        #endregion

        public override List<TimeEval> LoadTimeEval(DateTime BeginDate, DateTime EndDate)
        {
            List<TimeEval> oReturn = new List<TimeEval>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from TimeEval", oConnection);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("TIMEEVAL");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();

            foreach (DataRow dr in oTable.Rows)
            {
                TimeEval item = new TimeEval();

                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oConnection.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();
            oTable.Dispose();

            return oReturn;
        }

        public override List<TimeEval> LoadTimeEval(string EmployeeID, DateTime ClockIN, DateTime ClockOUT)
        {
            List<TimeEval> oReturn = new List<TimeEval>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from TimeEval where EmployeeID = @EmpID and @ClockIN <= BeginDate and EndDate <= @ClockOUT", oConnection);

            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@ClockIN", SqlDbType.DateTime);
            oParam.Value = ClockIN;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@ClockOUT", SqlDbType.DateTime);
            oParam.Value = ClockOUT;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("TIMEEVAL");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();

            foreach (DataRow dr in oTable.Rows)
            {
                TimeEval item = new TimeEval();

                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oConnection.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();
            oTable.Dispose();

            return oReturn;
        }

        #region " LoadOTData "
        public override List<SaveOTData> LoadOTData(string EmployeeID, DateTime BeginDate, DateTime EndDate, string OnlyType, bool OnlyCompleted, bool OnlyNotSummary)
        {
            List<SaveOTData> oReturn = new List<SaveOTData>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            string subQry = OnlyType.Trim() == "" ? "" : string.Format(" And OTType = '{0}'", OnlyType.Trim());
            string subQry1 = OnlyCompleted ? " And IsCompleted = 1" : "";
            string subQry2 = OnlyNotSummary ? " And IsSummary = 0" : "";

            oCommand = new SqlCommand("select * from V_OTData Where EmployeeID = @EmpID and BeginDate <= @EndDate and EndDate >= @BeginDate" + subQry + subQry1 + subQry2, oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OTDATA");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                SaveOTData item = new SaveOTData();
                item = new SaveOTData();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oTable.Dispose();
            return oReturn;
        }
        #endregion

        #region " LoadOTDataByAssigner "
        public override List<SaveOTData> LoadOTDataByAssigner(string AssignerID, DateTime BeginDate, DateTime EndDate, string OnlyType, bool OnlyCompleted, bool OnlyNotSummary)
        {
            List<SaveOTData> oReturn = new List<SaveOTData>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            string subQry = OnlyType.Trim() == "" ? "" : string.Format(" And OTType = '{0}'", OnlyType.Trim());
            string subQry1 = OnlyCompleted ? " And IsCompleted = 1" : "";
            string subQry2 = OnlyNotSummary ? " And IsSummary = 0" : "";

            oCommand = new SqlCommand("select * from V_OTData Where AssignFrom = @EmpID and BeginDate <= @EndDate and EndDate >= @BeginDate" + subQry + subQry1 + subQry2, oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = AssignerID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OTDATA");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                SaveOTData item = new SaveOTData();
                item = new SaveOTData();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oTable.Dispose();
            return oReturn;
        }
        #endregion

        #region " GetLeavePlanList "

        public override List<LeavePlan> GetLeavePlanList(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            List<LeavePlan> oReturn = new List<LeavePlan>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;

            oCommand = new SqlCommand("select * from LeavePlan where EmployeeID = @EmployeeID and PlanDate <= @EndDate and PlanDate >= @BeginDate", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("LeavePlan");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                LeavePlan item = new LeavePlan();
                item = new LeavePlan();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oTable.Dispose();
            return oReturn;
        }

        #endregion

        #region " GetLeavePlanList "
        public override void SaveLeavePlanList(string EmployeeID, int Year, List<LeavePlan> leavePlanList)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlCommand oCommand1;
            SqlTransaction tx;
            SqlParameter oParam;
            DataTable oTable = null;
            oConnection.Open();
            tx = oConnection.BeginTransaction();

            try
            {
                oCommand1 = new SqlCommand("delete from LeavePlan where EmployeeID = @EmployeeID and Year(PlanDate) = @Year", oConnection, tx);
                oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
                oParam.Value = EmployeeID;
                oCommand1.Parameters.Add(oParam);

                oParam = new SqlParameter("@Year", SqlDbType.Int);
                oParam.Value = Year;
                oCommand1.Parameters.Add(oParam);

                oCommand1.ExecuteNonQuery();

                oCommand = new SqlCommand("select * from LeavePlan where EmployeeID = @EmployeeID and Year(PlanDate) = @Year", oConnection, tx);
                oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
                oParam.Value = EmployeeID;
                oCommand.Parameters.Add(oParam);

                oParam = new SqlParameter("@Year", SqlDbType.Int);
                oParam.Value = Year;
                oCommand.Parameters.Add(oParam);

                SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                SqlCommandBuilder oCB = new SqlCommandBuilder(oAdapter);
                oTable = new DataTable("LeavePlan");
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                foreach (LeavePlan lp in leavePlanList)
                {
                    DataRow newRow = oTable.NewRow();
                    lp.LoadDataToTableRow(newRow);
                    oTable.LoadDataRow(newRow.ItemArray, false);
                }

                oAdapter.Update(oTable);
                oTable.Dispose();
                oCommand.Dispose();
                oCommand1.Dispose();

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw ex;
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
            }
        }
        #endregion

        #region "SaveOTLog"
        public override void SaveOTLog(DataTable data, bool IsMark)
        {
            this.SaveOTLog(data);
            if (!IsMark) { return; }
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction trn = oConnection.BeginTransaction();
            SqlCommand oCommand;
            SqlDataAdapter oSqlDataAdapter;

            oCommand = new SqlCommand("SELECT RequestNo,EmployeeID,BeginDate,EndDate," +
                        "RequestOTHour10,RequestOTHour15,RequestOTHour30,Status," +
                        "EvaOTHour10,EvaOTHour15,EvaOTHour30," +
                        "FinalOTHour10,FinalOTHour15,FinalOTHour30,IsPayroll, " +
                        "OTWorkTypeID,Description,RefRequestNo,ClockIn,ClockOut " +
                        "From OTLog", oConnection, trn);
            oSqlDataAdapter = new SqlDataAdapter(oCommand);
            oSqlDataAdapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;

            oCommand = new SqlCommand("INSERT INTO OTLog (RequestNo,EmployeeID,BeginDate,EndDate," +
                        "RequestOTHour10,RequestOTHour15,RequestOTHour30,Status," +
                        "EvaOTHour10,EvaOTHour15,EvaOTHour30," +
                        "FinalOTHour10,FinalOTHour15,FinalOTHour30,IsPayroll," +
                        "OTWorkTypeID,Description,RefRequestNo,ClockIn,ClockOut) " +
                        "VALUES(@RequestNo,@EmployeeID,@BeginDate,@EndDate," +
                        "@RequestOTHour10,@RequestOTHour15,@RequestOTHour30,@Status," +
                        "@EvaOTHour10,@EvaOTHour15,@EvaOTHour30," +
                        "@FinalOTHour10,@FinalOTHour15,@FinalOTHour30,@IsPayroll," +
                        "@OTWorkTypeID,@Description,@RefRequestNo,@ClockIn,@ClockOut)", oConnection, trn);

            oCommand.Parameters.Add("@RequestNo", SqlDbType.VarChar, 50, "RequestNo");
            oCommand.Parameters.Add("@EmployeeID", SqlDbType.VarChar, 8, "EmployeeID");
            oCommand.Parameters.Add("@BeginDate", SqlDbType.DateTime, 8, "BeginDate");
            oCommand.Parameters.Add("@EndDate", SqlDbType.DateTime, 8, "EndDate");
            oCommand.Parameters.Add("@RequestOTHour10", SqlDbType.Decimal, 5, "RequestOTHour10");
            oCommand.Parameters.Add("@RequestOTHour15", SqlDbType.Decimal, 5, "RequestOTHour15");
            oCommand.Parameters.Add("@RequestOTHour30", SqlDbType.Decimal, 5, "RequestOTHour30");
            oCommand.Parameters.Add("@Status", SqlDbType.TinyInt, 1, "Status");
            oCommand.Parameters.Add("@EvaOTHour10", SqlDbType.Decimal, 5, "EvaOTHour10");
            oCommand.Parameters.Add("@EvaOTHour15", SqlDbType.Decimal, 5, "EvaOTHour15");
            oCommand.Parameters.Add("@EvaOTHour30", SqlDbType.Decimal, 5, "EvaOTHour30");
            oCommand.Parameters.Add("@FinalOTHour10", SqlDbType.Decimal, 5, "FinalOTHour10");
            oCommand.Parameters.Add("@FinalOTHour15", SqlDbType.Decimal, 5, "FinalOTHour15");
            oCommand.Parameters.Add("@FinalOTHour30", SqlDbType.Decimal, 5, "FinalOTHour30");
            oCommand.Parameters.Add("@IsPayroll", SqlDbType.Bit, 1, "IsPayroll");
            oCommand.Parameters.Add("@Description", SqlDbType.Text, 0, "Description");
            oCommand.Parameters.Add("@RefRequestNo", SqlDbType.VarChar, 50, "RefRequestNo");
            //oCommand.Parameters.Add("@EvaTimePairs", SqlDbType.VarChar, 100, "EvaTimePairs");
            //oCommand.Parameters.Add("@FinalTimePairs", SqlDbType.VarChar, 100, "FinalTimePairs");
            oCommand.Parameters.Add("@OTWorkTypeID", SqlDbType.VarChar, 2, "OTWorkTypeID");
            oCommand.Parameters.Add("@ClockIn", SqlDbType.DateTime, 8, "ClockIn");
            oCommand.Parameters.Add("@ClockOut", SqlDbType.DateTime, 8, "ClockOut");

            oSqlDataAdapter.InsertCommand = oCommand;


            oCommand = new SqlCommand("UPDATE OTLog Set RequestNo=@RequestNo,EmployeeID=@EmployeeID,BeginDate=@BeginDate,EndDate=@EndDate," +
                        "RequestOTHour10=@RequestOTHour10,RequestOTHour15=@RequestOTHour15,RequestOTHour30=@RequestOTHour30,Status=@Status," +
                        "EvaOTHour10=@EvaOTHour10,EvaOTHour15=@EvaOTHour15,EvaOTHour30=@EvaOTHour30," +
                        "FinalOTHour10=@FinalOTHour10,FinalOTHour15=@FinalOTHour15,FinalOTHour30=@FinalOTHour30,IsPayroll=@IsPayroll," +
                        "OTWorkTypeID=@OTWorkTypeID,Description=@Description,RefRequestNo=@RefRequestNo,ClockIn=@ClockIn,ClockOut=@ClockOut " +
                        "WHERE RequestNo=@OldRequestNo and EmployeeID=@OldEmployeeID and BeginDate=@OldBeginDate and EndDate=@OldEndDate", oConnection, trn);

            oCommand.Parameters.Add("@RequestNo", SqlDbType.VarChar, 50, "RequestNo");
            oCommand.Parameters.Add("@EmployeeID", SqlDbType.VarChar, 8, "EmployeeID");
            oCommand.Parameters.Add("@BeginDate", SqlDbType.DateTime, 8, "BeginDate");
            oCommand.Parameters.Add("@EndDate", SqlDbType.DateTime, 8, "EndDate");
            oCommand.Parameters.Add("@RequestOTHour10", SqlDbType.Decimal, 5, "RequestOTHour10");
            oCommand.Parameters.Add("@RequestOTHour15", SqlDbType.Decimal, 5, "RequestOTHour15");
            oCommand.Parameters.Add("@RequestOTHour30", SqlDbType.Decimal, 5, "RequestOTHour30");
            oCommand.Parameters.Add("@Status", SqlDbType.TinyInt, 1, "Status");
            oCommand.Parameters.Add("@EvaOTHour10", SqlDbType.Decimal, 5, "EvaOTHour10");
            oCommand.Parameters.Add("@EvaOTHour15", SqlDbType.Decimal, 5, "EvaOTHour15");
            oCommand.Parameters.Add("@EvaOTHour30", SqlDbType.Decimal, 5, "EvaOTHour30");
            oCommand.Parameters.Add("@FinalOTHour10", SqlDbType.Decimal, 5, "FinalOTHour10");
            oCommand.Parameters.Add("@FinalOTHour15", SqlDbType.Decimal, 5, "FinalOTHour15");
            oCommand.Parameters.Add("@FinalOTHour30", SqlDbType.Decimal, 5, "FinalOTHour30");
            oCommand.Parameters.Add("@IsPayroll", SqlDbType.Bit, 1, "IsPayroll");
            oCommand.Parameters.Add("@Description", SqlDbType.Text, 0, "Description");
            oCommand.Parameters.Add("@RefRequestNo", SqlDbType.VarChar, 50, "RefRequestNo");
            //oCommand.Parameters.Add("@EvaTimePairs", SqlDbType.VarChar, 100, "EvaTimePairs");
            //oCommand.Parameters.Add("@FinalTimePairs", SqlDbType.VarChar, 100, "FinalTimePairs");
            oCommand.Parameters.Add("@OTWorkTypeID", SqlDbType.VarChar, 2, "OTWorkTypeID");
            oCommand.Parameters.Add("@ClockIn", SqlDbType.DateTime, 8, "ClockIn");
            oCommand.Parameters.Add("@ClockOut", SqlDbType.DateTime, 8, "ClockOut");


            oCommand.Parameters.Add("@OldRequestNo", SqlDbType.VarChar, 50, "RequestNo").SourceVersion = DataRowVersion.Original;
            oCommand.Parameters.Add("@OldEmployeeID", SqlDbType.VarChar, 8, "EmployeeID").SourceVersion = DataRowVersion.Original;
            oCommand.Parameters.Add("@oldBeginDate", SqlDbType.DateTime, 8, "BeginDate").SourceVersion = DataRowVersion.Original;
            oCommand.Parameters.Add("@oldEndDate", SqlDbType.DateTime, 8, "EndDate").SourceVersion = DataRowVersion.Original;
            oSqlDataAdapter.UpdateCommand = oCommand;

            oCommand = new SqlCommand("DELETE OTLog FROM " +
                                        "WHERE RequestNo=@OldRequestNo and EmployeeID=@OldEmployeeID and BeginDate=@OldBeginDate and EndDate=@OldEndDate", oConnection, trn);
            oCommand.Parameters.Add("@OldRequestNo", SqlDbType.VarChar, 50, "RequestNo").SourceVersion = DataRowVersion.Original;
            oCommand.Parameters.Add("@OldEmployeeID", SqlDbType.VarChar, 8, "EmployeeID").SourceVersion = DataRowVersion.Original;
            oCommand.Parameters.Add("@oldBeginDate", SqlDbType.DateTime, 8, "BeginDate").SourceVersion = DataRowVersion.Original;
            oCommand.Parameters.Add("@oldEndDate", SqlDbType.DateTime, 8, "EndDate").SourceVersion = DataRowVersion.Original;
            oSqlDataAdapter.DeleteCommand = oCommand;

            try
            {
                oSqlDataAdapter.Update(data);
                trn.Commit();
            }
            catch (Exception e)
            {
                trn.Rollback();
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                oSqlDataAdapter.Dispose();
            }
        }


        public override void SaveDailyOTLogService(DataTable data)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction trn = oConnection.BeginTransaction();
            SqlCommand oCommand;
            SqlCommand oCmd;
            SqlParameter oParam;
            SqlDataAdapter oSqlDataAdapter;

            string ReqNo = data.Rows[0]["RequestNo"].ToString();

            oCmd = new SqlCommand("DELETE FROM OTLog WHERE RequestNo=@RequestNo", oConnection, trn);

            oParam = new SqlParameter("@RequestNo", SqlDbType.VarChar);
            oParam.Value = ReqNo;
            oCmd.Parameters.Add(oParam);

            //oCommand.Parameters.Add("@RequestNo", SqlDbType.VarChar, 50, "RequestNo");
            //oSqlDataAdapter = new SqlDataAdapter(oCommand);
            //oSqlDataAdapter.DeleteCommand = oCommand;

            oCommand = new SqlCommand("INSERT INTO OTLog (RequestNo,EmployeeID,BeginDate,EndDate," +
                        "RequestOTHour10,RequestOTHour15,RequestOTHour30,Status," +
                        "EvaOTHour10,EvaOTHour15,EvaOTHour30," +
                        "FinalOTHour10,FinalOTHour15,FinalOTHour30,IsPayroll," +
                        "OTWorkTypeID,Description,RefRequestNo) " +
                        "VALUES(@RequestNo,@EmployeeID,@BeginDate,@EndDate," +
                        "@RequestOTHour10,@RequestOTHour15,@RequestOTHour30,@Status," +
                        "@EvaOTHour10,@EvaOTHour15,@EvaOTHour30," +
                        "@FinalOTHour10,@FinalOTHour15,@FinalOTHour30,@IsPayroll," +
                        "@OTWorkTypeID,@Description,@RefRequestNo)", oConnection, trn);

            oCommand.Parameters.Add("@RequestNo", SqlDbType.VarChar, 50, "RequestNo");
            oCommand.Parameters.Add("@EmployeeID", SqlDbType.VarChar, 8, "EmployeeID");
            oCommand.Parameters.Add("@BeginDate", SqlDbType.DateTime, 8, "BeginDate");
            oCommand.Parameters.Add("@EndDate", SqlDbType.DateTime, 8, "EndDate");
            oCommand.Parameters.Add("@RequestOTHour10", SqlDbType.Decimal, 5, "RequestOTHour10");
            oCommand.Parameters.Add("@RequestOTHour15", SqlDbType.Decimal, 5, "RequestOTHour15");
            oCommand.Parameters.Add("@RequestOTHour30", SqlDbType.Decimal, 5, "RequestOTHour30");
            oCommand.Parameters.Add("@Status", SqlDbType.TinyInt, 1, "Status");
            oCommand.Parameters.Add("@EvaOTHour10", SqlDbType.Decimal, 5, "EvaOTHour10");
            oCommand.Parameters.Add("@EvaOTHour15", SqlDbType.Decimal, 5, "EvaOTHour15");
            oCommand.Parameters.Add("@EvaOTHour30", SqlDbType.Decimal, 5, "EvaOTHour30");
            oCommand.Parameters.Add("@FinalOTHour10", SqlDbType.Decimal, 5, "FinalOTHour10");
            oCommand.Parameters.Add("@FinalOTHour15", SqlDbType.Decimal, 5, "FinalOTHour15");
            oCommand.Parameters.Add("@FinalOTHour30", SqlDbType.Decimal, 5, "FinalOTHour30");
            oCommand.Parameters.Add("@IsPayroll", SqlDbType.Bit, 1, "IsPayroll");
            oCommand.Parameters.Add("@Description", SqlDbType.Text, 0, "Description");
            oCommand.Parameters.Add("@RefRequestNo", SqlDbType.VarChar, 50, "RefRequestNo");
            oCommand.Parameters.Add("@OTWorkTypeID", SqlDbType.VarChar, 2, "OTWorkTypeID");

            oSqlDataAdapter = new SqlDataAdapter(oCommand);

            oSqlDataAdapter.InsertCommand = oCommand;

            try
            {
                oCmd.ExecuteNonQuery();
                oSqlDataAdapter.Update(data);
                trn.Commit();
            }
            catch (Exception e)
            {
                trn.Rollback();
                throw new Exception("save data error : " + e.Message.ToString());
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                oSqlDataAdapter.Dispose();
            }
        }


        void SaveOTLog(DataTable data)
        {
            List<string> sqlcommand = new List<string>();

            foreach (DataRow row in data.Rows)
            {
                // sqlcommand.Add(String.Format("DELETE FROM OTLog WHERE RequestNo = '{0}' and BeginDate <='{2}' and EndDate >='{1}' and BeginDate >= '{1}' ", row["RequestNo"], row["BeginDate"], row["EndDate"]));
                sqlcommand.Add(String.Format("DELETE FROM OTLog WHERE RequestNo = '{0}'", row["RequestNo"]));
            }
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction trn = oConnection.BeginTransaction();
            SqlCommand oCommand = null;

            foreach (string str in sqlcommand)
            {
                oCommand = new SqlCommand(str, oConnection, trn);
                try
                {
                    oCommand.ExecuteNonQuery();
                }
                catch
                {
                    trn.Rollback();
                    oConnection.Close();
                    oConnection.Dispose();
                    oCommand.Dispose();
                    return;
                }
            }

            trn.Commit();
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
        }

        public override void DeleteOTLogByRefRequestNo(DataTable data)
        {
            List<string> sqlcommand = new List<string>();

            foreach (DataRow row in data.Rows)
                sqlcommand.Add(String.Format("DELETE FROM OTLog WHERE RefRequestNo = '{0}'", row["RequestNo"]));

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction trn = oConnection.BeginTransaction();
            SqlCommand oCommand = null;

            foreach (string str in sqlcommand)
            {
                oCommand = new SqlCommand(str, oConnection, trn);
                try
                {
                    oCommand.ExecuteNonQuery();
                }
                catch
                {
                    trn.Rollback();
                    oConnection.Close();
                    oConnection.Dispose();
                    oCommand.Dispose();
                    return;
                }
            }

            trn.Commit();
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
        }

        public override void SaveOTLogStatus(DataTable data)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction trn = oConnection.BeginTransaction();
            SqlCommand oCommand = null;
            SqlParameter oParam = null;

            try
            {
                foreach (DataRow row in data.Rows)
                {
                    oCommand = new SqlCommand("UPDATE OTLog SET Status = @Status,FinalOTHour10=@FinalOTHour10,FinalOTHour15=@FinalOTHour15,FinalOTHour30=@FinalOTHour30 WHERE RequestNo = @RequestNo and EmployeeID = @EmployeeID and BeginDate = @BeginDate and EndDate = @EndDate", oConnection, trn);

                    oParam = new SqlParameter("@RequestNo", SqlDbType.VarChar);
                    oParam.Value = row["RequestNo"];
                    oCommand.Parameters.Add(oParam);

                    oParam = new SqlParameter("@Status", SqlDbType.TinyInt);
                    oParam.Value = row["Status"];
                    oCommand.Parameters.Add(oParam);

                    oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
                    oParam.Value = row["EmployeeID"];
                    oCommand.Parameters.Add(oParam);

                    oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
                    oParam.Value = row["BeginDate"];
                    oCommand.Parameters.Add(oParam);

                    oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
                    oParam.Value = row["EndDate"];
                    oCommand.Parameters.Add(oParam);

                    oParam = new SqlParameter("@FinalOTHour10", SqlDbType.Decimal);
                    oParam.Value = row["FinalOTHour10"];
                    oCommand.Parameters.Add(oParam);

                    oParam = new SqlParameter("@FinalOTHour15", SqlDbType.Decimal);
                    oParam.Value = row["FinalOTHour15"];
                    oCommand.Parameters.Add(oParam);

                    oParam = new SqlParameter("@FinalOTHour30", SqlDbType.Decimal);
                    oParam.Value = row["FinalOTHour30"];
                    oCommand.Parameters.Add(oParam);

                    oCommand.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                trn.Rollback();
                oCommand.Dispose();
                oConnection.Close();
                oConnection.Dispose();
                throw e;
            }
            trn.Commit();
            oCommand.Dispose();
            oConnection.Close();
            oConnection.Dispose();
        }

        public override void UpdateDailyOTLog(List<DailyOTLog> DailyOTLogList)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlParameter param;
            SqlTransaction oTransaction = null;

            try
            {
                oConnection.Open();
                oTransaction = oConnection.BeginTransaction();

                oCommand = new SqlCommand("sp_OTLogUpdate", oConnection, oTransaction);
                oCommand.CommandType = CommandType.StoredProcedure;

                foreach (DailyOTLog dailyOTLog in DailyOTLogList)
                {
                    oCommand.Parameters.Clear();
                    param = new SqlParameter("@p_RequestNo", SqlDbType.VarChar);
                    param.Value = dailyOTLog.RequestNo;
                    oCommand.Parameters.Add(param);
                    param = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
                    param.Value = dailyOTLog.EmployeeID;
                    oCommand.Parameters.Add(param);
                    param = new SqlParameter("@p_BeginDate", SqlDbType.DateTime);
                    param.Value = dailyOTLog.BeginDate;
                    oCommand.Parameters.Add(param);
                    param = new SqlParameter("@p_EndDate", SqlDbType.DateTime);
                    param.Value = dailyOTLog.EndDate;
                    oCommand.Parameters.Add(param);
                    param = new SqlParameter("@p_Description", SqlDbType.Text);
                    param.Value = dailyOTLog.Description;
                    oCommand.Parameters.Add(param);
                    param = new SqlParameter("@p_RequestOTHour10", SqlDbType.Decimal);
                    param.Value = dailyOTLog.RequestOTHour10;
                    oCommand.Parameters.Add(param);
                    param = new SqlParameter("@p_RequestOTHour15", SqlDbType.Decimal);
                    param.Value = dailyOTLog.RequestOTHour15;
                    oCommand.Parameters.Add(param);
                    param = new SqlParameter("@p_RequestOTHour30", SqlDbType.Decimal);
                    param.Value = dailyOTLog.RequestOTHour30;
                    oCommand.Parameters.Add(param);
                    param = new SqlParameter("@p_Status", SqlDbType.TinyInt);
                    param.Value = dailyOTLog.Status;
                    oCommand.Parameters.Add(param);
                    //param = new SqlParameter("@p_EvaTimePairs", SqlDbType.VarChar);
                    //param.Value = dailyOTLog.EvaTimePairs;
                    //oCommand.Parameters.Add(param);
                    param = new SqlParameter("@p_EvaOTHour10", SqlDbType.Decimal);
                    param.Value = dailyOTLog.EvaOTHour10;
                    oCommand.Parameters.Add(param);
                    param = new SqlParameter("@p_EvaOTHour15", SqlDbType.Decimal);
                    param.Value = dailyOTLog.EvaOTHour15;
                    oCommand.Parameters.Add(param);
                    param = new SqlParameter("@p_EvaOTHour30", SqlDbType.Decimal);
                    param.Value = dailyOTLog.EvaOTHour30;
                    oCommand.Parameters.Add(param);
                    //param = new SqlParameter("@p_FinalTimePairs", SqlDbType.VarChar);
                    //param.Value = dailyOTLog.FinalTimePairs;
                    //oCommand.Parameters.Add(param);
                    param = new SqlParameter("@p_FinalOTHour10", SqlDbType.Decimal);
                    param.Value = dailyOTLog.FinalOTHour10;
                    oCommand.Parameters.Add(param);
                    param = new SqlParameter("@p_FinalOTHour15", SqlDbType.Decimal);
                    param.Value = dailyOTLog.FinalOTHour15;
                    oCommand.Parameters.Add(param);
                    param = new SqlParameter("@p_FinalOTHour30", SqlDbType.Decimal);
                    param.Value = dailyOTLog.FinalOTHour30;
                    oCommand.Parameters.Add(param);
                    //param = new SqlParameter("@p_ClockTimePairs", SqlDbType.VarChar);
                    //param.Value = dailyOTLog.ClockTimePairs;
                    //oCommand.Parameters.Add(param);
                    param = new SqlParameter("@p_OTWorkTypeID", SqlDbType.VarChar);
                    param.Value = dailyOTLog.OTWorkTypeID;
                    oCommand.Parameters.Add(param);

                    oCommand.ExecuteNonQuery();
                }

                oTransaction.Commit();
            }
            catch (Exception ex)
            {
                oTransaction.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                if (oConnection.State == ConnectionState.Open)
                    oConnection.Close();
                oConnection.Dispose();
                oTransaction.Dispose();
            }
        }
        #endregion

        #region " SaveDutyPaymentLog"
        public override void SaveDutyPaymentLog(DataTable data, bool IsDeleteOnly)
        {
            this.DeleteDutyPaymentLog(data);
            if (IsDeleteOnly)
                return;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction trn = oConnection.BeginTransaction();
            SqlCommand oCommand;
            SqlDataAdapter oSqlDataAdapter;

            oCommand = new SqlCommand("INSERT INTO DutyPaymentLog (RequestNo,ItemNo,EmployeeID,DutyDate,BeginDate,EndDate," +
                        "Status,RefRequestNo,UpdateTime) " +
                        "VALUES(@RequestNo,@ItemNo,@EmployeeID,@DutyDate,@BeginDate,@EndDate," +
                        "@Status,@RefRequestNo,GETDATE())", oConnection, trn);

            oCommand.Parameters.Add("@RequestNo", SqlDbType.VarChar, 50, "RequestNo");
            oCommand.Parameters.Add("@ItemNo", SqlDbType.VarChar, 50, "ItemNo");
            oCommand.Parameters.Add("@EmployeeID", SqlDbType.VarChar, 8, "EmployeeID");
            oCommand.Parameters.Add("@DutyDate", SqlDbType.DateTime, 8, "DutyDate");
            oCommand.Parameters.Add("@BeginDate", SqlDbType.DateTime, 8, "BeginDate");
            oCommand.Parameters.Add("@EndDate", SqlDbType.DateTime, 8, "EndDate");
            oCommand.Parameters.Add("@Status", SqlDbType.VarChar, 20, "Status");
            oCommand.Parameters.Add("@RefRequestNo", SqlDbType.VarChar, 50, "RefRequestNo");
            oSqlDataAdapter = new SqlDataAdapter(oCommand);
            oSqlDataAdapter.InsertCommand = oCommand;


            oCommand = new SqlCommand("UPDATE DutyPaymentLog SET RequestNo=@RequestNo,ItemNo=@ItemNo,EmployeeID=@EmployeeID,DutyDate=@DutyDate,BeginDate=@BeginDate,EndDate=@EndDate," +
                        "Status=@Status,RefRequestNo=@RefRequestNo,UpdateTime=GETDATE() " +
                        "WHERE RequestNo=@OldRequestNo and ItemNo=@OldItemNo", oConnection, trn);

            oCommand.Parameters.Add("@RequestNo", SqlDbType.VarChar, 50, "RequestNo");
            oCommand.Parameters.Add("@ItemNo", SqlDbType.VarChar, 50, "ItemNo");
            oCommand.Parameters.Add("@EmployeeID", SqlDbType.VarChar, 8, "EmployeeID");
            oCommand.Parameters.Add("@DutyDate", SqlDbType.DateTime, 8, "DutyDate");
            oCommand.Parameters.Add("@BeginDate", SqlDbType.DateTime, 8, "BeginDate");
            oCommand.Parameters.Add("@EndDate", SqlDbType.DateTime, 8, "EndDate");
            oCommand.Parameters.Add("@Status", SqlDbType.VarChar, 20, "Status");
            oCommand.Parameters.Add("@RefRequestNo", SqlDbType.VarChar, 50, "RefRequestNo");

            oCommand.Parameters.Add("@OldRequestNo", SqlDbType.VarChar, 50, "RequestNo").SourceVersion = DataRowVersion.Original;
            oCommand.Parameters.Add("@OldItemNo", SqlDbType.VarChar, 50, "ItemNo").SourceVersion = DataRowVersion.Original;
            oSqlDataAdapter.UpdateCommand = oCommand;

            try
            {
                oSqlDataAdapter.Update(data);
                trn.Commit();
            }
            catch (Exception e)
            {
                trn.Rollback();
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                oSqlDataAdapter.Dispose();
            }

        }

        void DeleteDutyPaymentLog(DataTable data)
        {
            List<string> sqlcommand = new List<string>();

            foreach (DataRow row in data.Rows)
            {
                sqlcommand.Add(String.Format("DELETE FROM DutyPaymentLog WHERE RequestNo = '{0}'", row["RequestNo"]));
                break;
            }
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction trn = oConnection.BeginTransaction();
            SqlCommand oCommand = null;

            foreach (string str in sqlcommand)
            {
                oCommand = new SqlCommand(str, oConnection, trn);
                try
                {
                    oCommand.ExecuteNonQuery();
                }
                catch
                {
                    trn.Rollback();
                    oConnection.Close();
                    oConnection.Dispose();
                    oCommand.Dispose();
                    return;
                }
            }

            trn.Commit();
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
        }
        #endregion

        #region " GetDutyPaymentLog "
        public override List<DutyPaymentLog> GetDutyPaymentLog(string strEmployeeID, DateTime dtBeginDate, DateTime dtEndDate)
        {
            List<DutyPaymentLog> oReturn = new List<DutyPaymentLog>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_GetDutyPaymentLogGet", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter param;
            param = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            param.Value = strEmployeeID;
            oCommand.Parameters.Add(param);

            param = new SqlParameter("@p_BeginDate", SqlDbType.DateTime);
            param.Value = dtBeginDate;
            oCommand.Parameters.Add(param);

            param = new SqlParameter("@p_EndDate", SqlDbType.DateTime);
            param.Value = dtEndDate;
            oCommand.Parameters.Add(param);


            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("DutyPaymentTable");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();

            foreach (DataRow dr in oTable.Rows)
            {
                DutyPaymentLog item = new DutyPaymentLog();

                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oConnection.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();

            return oReturn;
        }
        #endregion

        //AddBy: Ratchatawan W. (2012-02-23)
        public override List<DailyOTLog> LoadOTLogByCriteria(string EmployeeList, string StatusList, DateTime BeginDate, DateTime EndDate)
        {
            List<DailyOTLog> oReturn = new List<DailyOTLog>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_GetOTLogByCriteria", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter param;

            param = new SqlParameter("@p_BeginDate", SqlDbType.DateTime);
            param.Value = BeginDate;
            oCommand.Parameters.Add(param);

            param = new SqlParameter("@p_EndDate", SqlDbType.DateTime);
            param.Value = EndDate;
            oCommand.Parameters.Add(param);

            param = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            param.Value = EmployeeList;
            oCommand.Parameters.Add(param);

            param = new SqlParameter("@p_StatusList", SqlDbType.VarChar);
            param.Value = StatusList;
            oCommand.Parameters.Add(param);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("OTLogTable");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();

            foreach (DataRow dr in oTable.Rows)
            {
                DailyOTLog item = new DailyOTLog();

                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oConnection.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();

            return oReturn;
        }

        public override DataTable GetSummaryOTInMonth(string EmployeeID, DateTime BeginDate, DateTime EndDate, string RequestNo)
        {
            DataTable oReturn = new DataTable("UserTimesheet");
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_OTLogGetSummaryOTInMonth", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;

            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_RequestNo", SqlDbType.VarChar);
            oParam.Value = RequestNo;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            oAdapter.Fill(oReturn);
            oConnection.Close();
            oConnection.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();

            return oReturn;
        }

        public override List<TimeElement> GetTimeElement(string CardNo, DateTime BeginDate, DateTime EndDate, int RecordCount)
        {
            // recordCount = -1 mean get all record
            List<TimeElement> oReturn = new List<TimeElement>();
            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("TIMEEVENT");
            oTable.CaseSensitive = false;
            if (RecordCount == -1)
            {
                oCommand = new SqlCommand("select * from TimeEvents_BOSCH Where CardNo = @CardNo and EventTime <= @EndDate and EventTime > @BeginDate ", oConnection);
            }
            else
            {
                oCommand = new SqlCommand(string.Format("select Top {0} * from TimeEvents_BOSCH Where CardNo = @CardNo and EventTime <= @EndDate and EventTime > @BeginDate order by EventTime Desc", RecordCount), oConnection);
            }
            SqlParameter oParam;
            oParam = new SqlParameter("@CardNo", SqlDbType.VarChar);
            oParam.Value = CardNo;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                TimeElement item = new TimeElement();
                item.CardNo = dr["CardNo"].ToString();
                item.EventTime = Convert.ToDateTime(dr["EventTime"]);
                item.Location = dr["Location"].ToString();
                //item.ParseToObject(dr);
                oReturn.Add(item);
            }
            oTable.Dispose();
            return oReturn;
        }

        #region " GetOTOverLimitPerWeek "
        /*
         * Add By Han : 11/03/2019
         * Add Detail : Retrieve data of OT List Over limit per week (over 24, 36 Hours)
         */
        public override DataTable GetOTOverLimitPerWeek(string Employeeid, DateTime BeginDate)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_GetOTOverLimitPerWeek", oConnection);
            oCommand.CommandType = System.Data.CommandType.StoredProcedure;
            oCommand.Parameters.Add("@p_EmployeeId", SqlDbType.VarChar).Value = Employeeid;
            oCommand.Parameters.Add("@p_BeginDate", SqlDbType.DateTime).Value = BeginDate;
            oConnection.Open();

            DataTable dt = new DataTable();
            dt.Load(oCommand.ExecuteReader());
            oConnection.Close();
            oConnection.Dispose();

            return dt;
        }
        #endregion

        #region " GetOTOverLimitPerMonth "
        /*
         * Add By Han : 10/05/2019
         * Add Detail : Retrieve data of OT List Over limit per month (over 120 Hours)
         */
        public override DataTable GetOTOverLimitPerMonth(string Employeeid, DateTime BeginDate)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_GetOTOverLimitPerMonth", oConnection);
            oCommand.CommandType = System.Data.CommandType.StoredProcedure;
            oCommand.Parameters.Add("@p_EmployeeId", SqlDbType.VarChar).Value = Employeeid;
            oCommand.Parameters.Add("@p_BeginDate", SqlDbType.DateTime).Value = BeginDate;
            oConnection.Open();

            DataTable dt = new DataTable();
            dt.Load(oCommand.ExecuteReader());
            oConnection.Close();
            oConnection.Dispose();

            return dt;
        }
        #endregion

        public override DataTable GetOTOnHoliday(string Employeeid, DateTime BeginDate)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_GetOTOnHoliday", oConnection);
            oCommand.CommandType = System.Data.CommandType.StoredProcedure;
            oCommand.Parameters.Add("@p_EmployeeId", SqlDbType.VarChar).Value = Employeeid;
            oCommand.Parameters.Add("@p_BeginDate", SqlDbType.DateTime).Value = BeginDate;
            oConnection.Open();

            DataTable dt = new DataTable();
            dt.Load(oCommand.ExecuteReader());
            oConnection.Close();
            oConnection.Dispose();

            return dt;
        }

        #region " SaveDelegateByAbsence "
        public override void SaveDelegateByAbsence(DataTable data)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction trn = oConnection.BeginTransaction();
            SqlCommand oCommand = null;
            SqlParameter oParam = null;

            try
            {
                foreach (DataRow row in data.Rows)
                {
                    oCommand = new SqlCommand("sp_AbsenceWithDelegateSave", oConnection, trn);
                    oCommand.CommandType = CommandType.StoredProcedure;

                    oParam = new SqlParameter("@p_RequestNo", SqlDbType.VarChar);
                    oParam.Value = row["RequestNo"];
                    oCommand.Parameters.Add(oParam);


                    oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
                    oParam.Value = row["EmployeeID"];
                    oCommand.Parameters.Add(oParam);

                    oParam = new SqlParameter("@p_BeginDate", SqlDbType.DateTime);
                    oParam.Value = row["BeginDate"];
                    oCommand.Parameters.Add(oParam);

                    oParam = new SqlParameter("@p_EndDate", SqlDbType.DateTime);
                    oParam.Value = row["EndDate"];
                    oCommand.Parameters.Add(oParam);

                    oParam = new SqlParameter("@p_DelegateTo", SqlDbType.VarChar);
                    oParam.Value = row["DelegateTo"];
                    oCommand.Parameters.Add(oParam);

                    oCommand.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                trn.Rollback();
                oCommand.Dispose();
                oConnection.Close();
                oConnection.Dispose();
                throw e;
            }
            trn.Commit();
            oCommand.Dispose();
            oConnection.Close();
            oConnection.Dispose();
        }
        #endregion

        #region " DeleteDelegateByAbsence "
        public override void DeleteDelegateByAbsence(INFOTYPE2001 data)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction trn = oConnection.BeginTransaction();
            SqlCommand oCommand = null;
            SqlParameter oParam = null;

            try
            {
                oCommand = new SqlCommand("sp_AbsenceWithDelegateDelete", oConnection, trn);
                oCommand.CommandType = CommandType.StoredProcedure;

                oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
                oParam.Value = data.EmployeeID;
                oCommand.Parameters.Add(oParam);

                oParam = new SqlParameter("@p_BeginDate", SqlDbType.DateTime);
                oParam.Value = data.BeginDate;
                oCommand.Parameters.Add(oParam);

                oParam = new SqlParameter("@p_EndDate", SqlDbType.DateTime);
                oParam.Value = data.EndDate;
                oCommand.Parameters.Add(oParam);

                oCommand.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                trn.Rollback();
                oCommand.Dispose();
                oConnection.Close();
                oConnection.Dispose();
                throw e;
            }
            trn.Commit();
            oCommand.Dispose();
            oConnection.Close();
            oConnection.Dispose();
        }
        #endregion

        #region " GetDelegateByAbsence "
        public override List<DelegateData> GetDelegateByAbsence(string EmployeeId, string DelegateToId, DateTime BeginDate, DateTime EndDate)
        {
            List<DelegateData> oReturn = new List<DelegateData>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_AbsenceWithDelegateGet", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter oParam;

            if (!String.IsNullOrEmpty(EmployeeId))
            {
                oParam = new SqlParameter("@p_EmployeeId", SqlDbType.VarChar);
                oParam.Value = EmployeeId;
                oCommand.Parameters.Add(oParam);
            }

            if (!String.IsNullOrEmpty(DelegateToId))
            {
                oParam = new SqlParameter("@p_DelegateTo", SqlDbType.VarChar);
                oParam.Value = DelegateToId;
                oCommand.Parameters.Add(oParam);
            }

            oParam = new SqlParameter("@p_BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate.Date;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate.Date;
            oCommand.Parameters.Add(oParam);


            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("DELEGATEDATA");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                DelegateData item = new DelegateData();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oTable.Dispose();
            return oReturn;
        }

        #endregion


        #region " MarkTimesheet "
        public override void MarkTimesheet(string EmployeeID, string ApplicationKey, string SubKey1, string SubKey2, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark, bool isCheck, string Detail)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlParameter oParam;
            SqlCommand oCommand = null;
            SqlDataAdapter oAdapter = null;
            DataTable oTable = null;
            SqlCommandBuilder oCB = null;
            if (isMark)
            {
                // Check collission
                DateTime date1, date2;
                MonthlyWS oCalendar = null;
                EmployeeData oEmp;
                DailyWS oDWS;
                DateTime bufferDate1;
                DateTime bufferDate2;

                if (isFullDay)
                {
                    oCalendar = MonthlyWS.GetCalendar(EmployeeID, BeginDate.Year, BeginDate.Month);
                    oEmp = new EmployeeData(EmployeeID, BeginDate.Date);
                    if (oEmp.OrgAssignment == null)
                    {
                        date1 = BeginDate;
                        date2 = EndDate;
                    }
                    else
                    {
                        oDWS = EmployeeManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetDailyWorkschedule(oEmp.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, oCalendar.GetWSCode(BeginDate.Day, true), BeginDate);
                        if (oDWS != null && oDWS.WorkBeginTime > TimeSpan.MinValue)
                        {
                            date1 = BeginDate.Date.Add(oDWS.WorkBeginTime);
                        }
                        else
                        {
                            date1 = BeginDate.Date;
                        }
                        if (BeginDate.Date == EndDate.Date)
                        {
                            if (oDWS.WorkEndTime > TimeSpan.MinValue)
                            {
                                date2 = BeginDate.Date.Add(oDWS.WorkEndTime);
                            }
                            else
                            {
                                date2 = BeginDate.Date.AddDays(1);
                            }
                        }
                        else
                        {
                            if (EndDate.Month != BeginDate.Month || EndDate.Year != BeginDate.Year)
                            {
                                oCalendar = MonthlyWS.GetCalendar(EmployeeID, EndDate.Year, EndDate.Month);
                            }
                            if (!(oEmp.OrgAssignment.BeginDate >= EndDate.Date && oEmp.OrgAssignment.EndDate <= EndDate.Date))
                            {
                                oEmp = new EmployeeData(EmployeeID, EndDate.Date);
                            }
                            oDWS = EmployeeManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetDailyWorkschedule(oEmp.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, oCalendar.GetWSCode(EndDate.Day, true), EndDate);
                            if (oDWS.WorkEndTime > TimeSpan.MinValue)
                            {
                                date2 = EndDate.Date.Add(oDWS.WorkEndTime);
                            }
                            else
                            {
                                date2 = EndDate.Date.AddDays(1);
                            }
                        }
                    }
                }
                else
                {
                    date1 = BeginDate;
                    date2 = EndDate;
                }
                List<TimesheetData> collisions, returnList;
                returnList = new List<TimesheetData>();
                foreach (DATACLASS.CollisionControl item in this.LoadApplicationCollision(ApplicationKey, SubKey1, SubKey2))
                {
                    #region " find Collision in self timesheet "
                    collisions = this.LoadTimesheetCollision(EmployeeID, date1, date2, item);
                    foreach (TimesheetData cl in collisions)
                    {
                        if (item.IgnoreSameDetail && cl.Detail.Trim() == Detail.Trim())
                        {
                            continue;
                        }
                        if (item.IsCheckOnlyPlanTime)
                        {
                            if (cl.FullDay)
                            {
                                for (DateTime rundate = cl.BeginDate.Date; rundate <= cl.EndDate.Date; rundate = rundate.AddDays(1))
                                {
                                    if (oCalendar == null || rundate.Month != oCalendar.WS_Month || rundate.Year != oCalendar.WS_Year)
                                    {
                                        oCalendar = MonthlyWS.GetCalendar(EmployeeID, rundate.Year, rundate.Month);
                                    }
                                    oEmp = new EmployeeData(EmployeeID, rundate);
                                    oDWS = EmployeeManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetDailyWorkschedule(oEmp.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, oCalendar.GetWSCode(rundate.Day, true), rundate);
                                    bufferDate1 = rundate.Add(oDWS.WorkBeginTime);
                                    bufferDate2 = rundate.Add(oDWS.WorkEndTime);
                                    if (bufferDate1 < date2 && bufferDate2 > date1)
                                    {
                                        cl.BeginDate = bufferDate1;
                                        cl.EndDate = bufferDate2;
                                        returnList.Add(cl);
                                        break;
                                    }
                                }
                            }
                        }
                        else
                        {
                            returnList.Add(cl);
                        }
                    }
                    #endregion

                    #region " find Collsion in Additional data "
                    ICollisionable oCollisionable = LoadAdditionalDataCollisionable(item.ApplicationKeyExists);
                    // check if implements
                    if (oCollisionable != null)
                    {
                        collisions = oCollisionable.LoadCollision(EmployeeID, date1, date2, item.SubKey1Exists, item.SubKey2Exists);
                        foreach (TimesheetData cl in collisions)
                        {
                            if (cl.Detail == Detail)
                            {
                                //case both of them is same requestNo ---> SKIP
                                continue;
                            }
                            if (item.IsCheckOnlyPlanTime)
                            {
                                if (cl.FullDay)
                                {
                                    for (DateTime rundate = cl.BeginDate.Date; rundate <= cl.EndDate.Date; rundate = rundate.AddDays(1))
                                    {
                                        if (oCalendar == null || rundate.Month != oCalendar.WS_Month || rundate.Year != oCalendar.WS_Year)
                                        {
                                            oCalendar = MonthlyWS.GetCalendar(EmployeeID, rundate.Year, rundate.Month);
                                        }
                                        oEmp = new EmployeeData(EmployeeID, rundate);
                                        oDWS = EmployeeManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetDailyWorkschedule(oEmp.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, oCalendar.GetWSCode(rundate.Day, false), rundate);
                                        bufferDate1 = rundate.Add(oDWS.WorkBeginTime);
                                        bufferDate2 = rundate.Add(oDWS.WorkEndTime);
                                        if (bufferDate1 < date2 && bufferDate2 > date1)
                                        {
                                            returnList.Add(cl);
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    if (cl.BeginDate < date2 && cl.EndDate > date1)
                                    {
                                        returnList.Add(cl);
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                if (cl.BeginDate < date2 && cl.EndDate > date1)
                                {
                                    returnList.Add(cl);
                                    break;
                                }
                            }
                        }
                    }
                    #endregion
                }

                if (returnList.Count > 0)
                {
                    throw new TimesheetCollisionException((returnList));
                }
            }
            try
            {
                if (!isCheck)
                {
                    oCommand = new SqlCommand("select * from Timesheet where EmployeeID = @employeeid and ApplicationKey = @ApplicationKey and Detail = @Detail", oConnection);
                    oParam = new SqlParameter("@employeeid", SqlDbType.VarChar);
                    oParam.Value = EmployeeID;
                    oCommand.Parameters.Add(oParam);
                    oParam = new SqlParameter("@ApplicationKey", SqlDbType.VarChar);
                    oParam.Value = ApplicationKey;
                    oCommand.Parameters.Add(oParam);
                    oParam = new SqlParameter("@Detail", SqlDbType.VarChar);
                    oParam.Value = Detail;
                    oCommand.Parameters.Add(oParam);
                    oAdapter = new SqlDataAdapter(oCommand);
                    oCB = new SqlCommandBuilder(oAdapter);
                    oTable = new DataTable("TIMESHEET");
                    oAdapter.FillSchema(oTable, SchemaType.Source);
                    oAdapter.Fill(oTable);

                    TimesheetData oTSD = new TimesheetData();
                    oTSD.EmployeeID = EmployeeID;
                    oTSD.ApplicationKey = ApplicationKey;
                    oTSD.BeginDate = BeginDate;
                    oTSD.EndDate = EndDate;
                    oTSD.FullDay = isFullDay;
                    oTSD.Detail = Detail;
                    oTSD.SubKey1 = SubKey1;
                    oTSD.SubKey2 = SubKey2;

                    DataTable dt = new DataTable("ListPeriod");
                    dt.Columns.Add("EmployeeID", typeof(System.String));
                    dt.Columns.Add("ApplicationKey", typeof(System.String));
                    dt.Columns.Add("SubKey1", typeof(System.String));
                    dt.Columns.Add("SubKey2", typeof(System.String));
                    dt.Columns.Add("BeginDate", typeof(System.DateTime));
                    dt.Columns.Add("EndDate", typeof(System.DateTime));
                    dt.Columns.Add("FullDay", typeof(System.Boolean));
                    dt.Columns.Add("Detail", typeof(System.String));


                    DataRow dRow = dt.NewRow();
                    dRow["EmployeeID"] = EmployeeID;
                    dRow["ApplicationKey"] = ApplicationKey;
                    dRow["SubKey1"] = SubKey1;
                    dRow["SubKey2"] = SubKey2;
                    dRow["BeginDate"] = BeginDate;
                    dRow["EndDate"] = EndDate;
                    dRow["FullDay"] = isFullDay;
                    dRow["Detail"] = Detail;

                    dt.Rows.Add(dRow);

                    //DataTable oTemp = oTSD.ToADODataTable();
                    DataRow oRow = oTable.LoadDataRow(dt.Rows[0].ItemArray, false);
                    if (!isMark)
                    {
                        oRow.Delete();
                    }
                    foreach (DataRow dr in oTable.Rows)
                    {
                        if (dr.RowState == DataRowState.Unchanged)
                        {
                            dr.Delete();
                        }
                    }

                    oAdapter.Update(oTable);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Dispose();
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                if (oCommand != null)
                {
                    oCommand.Dispose();
                }
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oTable != null)
                {
                    oTable.Dispose();
                }
            }
        }

        public override void MarkTimesheet(List<TimesheetData> oTimesheetDataList, bool isMark, bool isCheck)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlTransaction oTransaction;
            SqlParameter oParam;
            SqlCommand oCommand = null;
            SqlDataAdapter oAdapter = null;
            DataTable oTable = null;
            SqlCommandBuilder oCB = null;
            foreach (TimesheetData oTimesheetData in oTimesheetDataList)
            {
                string EmployeeID = oTimesheetData.EmployeeID;
                string ApplicationKey = oTimesheetData.ApplicationKey;
                string SubKey1 = oTimesheetData.SubKey1;
                string SubKey2 = oTimesheetData.SubKey2;
                bool isFullDay = oTimesheetData.FullDay;
                DateTime BeginDate = oTimesheetData.BeginDate;
                DateTime EndDate = oTimesheetData.EndDate;
                string Detail = oTimesheetData.Detail;

                if (isMark)
                {
                    // Check collission
                    DateTime date1, date2;
                    MonthlyWS oCalendar = null;
                    EmployeeData oEmp;
                    DailyWS oDWS;
                    DateTime bufferDate1;
                    DateTime bufferDate2;

                    if (isFullDay)
                    {
                        oCalendar = MonthlyWS.GetCalendar(EmployeeID, BeginDate.Year, BeginDate.Month);
                        oEmp = new EmployeeData(EmployeeID, BeginDate.Date);
                        if (oEmp.OrgAssignment == null)
                        {
                            date1 = BeginDate;
                            date2 = EndDate;
                        }
                        else
                        {
                            oDWS = DailyWS.GetDailyWorkschedule(oEmp.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, oCalendar.GetWSCode(BeginDate.Day, true), BeginDate);
                            //if (oDWS != null && oDWS.WorkBeginTime > TimeSpan.MinValue)
                            //{
                            //    date1 = BeginDate.Date.Add(oDWS.WorkBeginTime);
                            //}
                            //else
                            //{
                            //    date1 = BeginDate.Date;
                            //}
                            if (BeginDate.Date == EndDate.Date)
                            {
                                if (oTimesheetData.ApplicationKey.ToUpper() == "ATTENDANCE")//Add by Koissares 20160622
                                {
                                    date1 = BeginDate.Date;
                                    date2 = BeginDate.Date.AddDays(1).AddSeconds(-1);
                                }
                                else
                                {
                                    if (oDWS != null && oDWS.WorkBeginTime > TimeSpan.MinValue)
                                    {
                                        date1 = BeginDate.Date.Add(oDWS.WorkBeginTime);
                                    }
                                    else
                                    {
                                        date1 = BeginDate.Date;
                                    }
                                    if (oDWS.WorkEndTime > TimeSpan.MinValue)
                                    {
                                        date2 = EndDate.Date.Add(oDWS.WorkEndTime);
                                    }
                                    else
                                    {
                                        date2 = EndDate.Date.AddDays(1);
                                    }
                                }
                            }
                            else
                            {
                                if (EndDate.Month != BeginDate.Month || EndDate.Year != BeginDate.Year)
                                {
                                    oCalendar = MonthlyWS.GetCalendar(EmployeeID, EndDate.Year, EndDate.Month);
                                }
                                if (!(oEmp.OrgAssignment.BeginDate >= EndDate.Date && oEmp.OrgAssignment.EndDate <= EndDate.Date))
                                {
                                    oEmp = new EmployeeData(EmployeeID, EndDate.Date);
                                }
                                oDWS = DailyWS.GetDailyWorkschedule(oEmp.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, oCalendar.GetWSCode(EndDate.Day, true), EndDate);
                                if (oDWS != null && oDWS.WorkBeginTime > TimeSpan.MinValue)
                                {
                                    date1 = BeginDate.Date.Add(oDWS.WorkBeginTime);
                                }
                                else
                                {
                                    date1 = BeginDate.Date;
                                }
                                if (oDWS.WorkEndTime > TimeSpan.MinValue)
                                {
                                    date2 = EndDate.Date.Add(oDWS.WorkEndTime);
                                }
                                else
                                {
                                    date2 = EndDate.Date.AddDays(1);
                                }
                            }
                        }
                    }
                    else
                    {
                        date1 = BeginDate;
                        if (EndDate <= BeginDate)
                            date2 = EndDate.AddDays(1);
                        else
                            date2 = EndDate;
                    }
                    List<TimesheetData> collisions, returnList;
                    returnList = new List<TimesheetData>();
                    foreach (DATACLASS.CollisionControl item in this.LoadApplicationCollision(ApplicationKey, SubKey1, SubKey2))
                    {
                        #region " find Collision in self timesheet "
                        collisions = this.LoadTimesheetCollision(EmployeeID, date1.Date, date2.Date, item);
                        collisions.AddRange(this.LoadTimesheetCollision(EmployeeID, date1.Date.AddDays(-1), date2.Date.AddDays(-1), item));//Add by Koissares 20160622 for check previous day
                        if (EndDate < BeginDate)
                            collisions.AddRange(this.LoadTimesheetCollision(EmployeeID, BeginDate.Date.AddDays(1), EndDate.Date.AddDays(1), item));//Add by Koissares 20160622 for check next day
                        foreach (TimesheetData cl in collisions)
                        {
                            if (item.IgnoreSameDetail && cl.Detail.Trim() == Detail.Trim())
                            {
                                continue;
                            }
                            if (item.IsCheckOnlyPlanTime)
                            {
                                if (cl.FullDay)
                                {
                                    for (DateTime rundate = cl.BeginDate.Date; rundate <= cl.EndDate.Date; rundate = rundate.AddDays(1))
                                    {
                                        if (oCalendar == null || rundate.Month != oCalendar.WS_Month || rundate.Year != oCalendar.WS_Year)
                                        {
                                            oCalendar = MonthlyWS.GetCalendar(EmployeeID, rundate.Year, rundate.Month);
                                        }
                                        oEmp = new EmployeeData(EmployeeID, rundate);
                                        oDWS = DailyWS.GetDailyWorkschedule(oEmp.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, oCalendar.GetWSCode(rundate.Day, true), rundate);
                                        bufferDate1 = rundate.Add(oDWS.WorkBeginTime);
                                        bufferDate2 = rundate.Add(oDWS.WorkEndTime);
                                        if (bufferDate1 < date2 && bufferDate2 > date1)
                                        {
                                            cl.BeginDate = bufferDate1;
                                            cl.EndDate = bufferDate2;
                                            returnList.Add(cl);
                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                returnList.Add(cl);
                            }
                        }
                        #endregion

                        #region " find Collsion in Additional data "
                        ICollisionable oCollisionable = this.LoadAdditionalDataCollisionable(item.ApplicationKeyExists);
                        // check if implements
                        if (oCollisionable != null)
                        {
                            collisions = oCollisionable.LoadCollision(EmployeeID, date1.Date, date2.Date, item.SubKey1Exists, item.SubKey2Exists);
                            collisions.AddRange(oCollisionable.LoadCollision(EmployeeID, date1.Date.AddDays(-1), date2.Date.AddDays(-1), item.SubKey1Exists, item.SubKey2Exists));//Add by Koissares 20160622 for check previous day
                            if (EndDate < BeginDate)
                                collisions.AddRange(oCollisionable.LoadCollision(EmployeeID, BeginDate.Date.AddDays(1), EndDate.Date.AddDays(1), item.SubKey1Exists, item.SubKey2Exists));//Add by Koissares 20160622 for check next day
                            foreach (TimesheetData cl in collisions)
                            {
                                if (cl.Detail == Detail)
                                {
                                    //case both of them is same requestNo ---> SKIP
                                    continue;
                                }
                                if (item.IsCheckOnlyPlanTime)
                                {
                                    if (cl.FullDay)
                                    {
                                        for (DateTime rundate = cl.BeginDate.Date; rundate <= cl.EndDate.Date; rundate = rundate.AddDays(1))
                                        {
                                            if (oCalendar == null || rundate.Month != oCalendar.WS_Month || rundate.Year != oCalendar.WS_Year)
                                            {
                                                oCalendar = MonthlyWS.GetCalendar(EmployeeID, rundate.Year, rundate.Month);
                                            }
                                            oEmp = new EmployeeData(EmployeeID, rundate);
                                            oDWS = DailyWS.GetDailyWorkschedule(oEmp.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, oCalendar.GetWSCode(rundate.Day, true), rundate);
                                            if (!oDWS.IsDayOff)
                                            {
                                                bufferDate1 = rundate.Add(oDWS.WorkBeginTime);
                                                bufferDate2 = rundate.Add(oDWS.WorkEndTime);
                                                if (bufferDate1 < date2 && bufferDate2 > date1)
                                                {
                                                    returnList.Add(cl);
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (cl.EndDate < cl.BeginDate)
                                            cl.EndDate = cl.EndDate.AddDays(1);//Add by Koissares 20160621

                                        if (cl.BeginDate < date2 && cl.EndDate > date1)
                                        {
                                            returnList.Add(cl);
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    if (cl.EndDate < cl.BeginDate)
                                        cl.EndDate = cl.EndDate.AddDays(1);//Add by Koissares 20160621

                                    if (cl.BeginDate < date2 && cl.EndDate > date1)
                                    {
                                        returnList.Add(cl);
                                        break;
                                    }
                                }
                            }
                        }
                        #endregion
                    }

                    if (returnList.Count > 0)
                    {
                        throw new TimesheetCollisionException((returnList));
                    }
                }
            }
            if (!isCheck)
            {
                oConnection.Open();
                oTransaction = oConnection.BeginTransaction();
                try
                {

                    oCommand = new SqlCommand("select * from Timesheet where EmployeeID = @employeeid and ApplicationKey = @ApplicationKey and Detail = @Detail", oConnection, oTransaction);
                    string tempEmployeeID = string.Empty, tempApplicationKey = string.Empty, tempDetail = string.Empty;
                    foreach (TimesheetData oTimesheetData in oTimesheetDataList)
                    {
                        if (tempEmployeeID != oTimesheetData.EmployeeID || tempApplicationKey != oTimesheetData.ApplicationKey || tempDetail != oTimesheetData.Detail)
                        {
                            tempEmployeeID = oTimesheetData.EmployeeID;
                            tempApplicationKey = oTimesheetData.ApplicationKey;
                            tempDetail = oTimesheetData.Detail;

                            oCommand.Parameters.Clear();
                            oParam = new SqlParameter("@employeeid", SqlDbType.VarChar);
                            oParam.Value = tempEmployeeID;
                            oCommand.Parameters.Add(oParam);
                            oParam = new SqlParameter("@ApplicationKey", SqlDbType.VarChar);
                            oParam.Value = tempApplicationKey;
                            oCommand.Parameters.Add(oParam);
                            oParam = new SqlParameter("@Detail", SqlDbType.VarChar);
                            oParam.Value = tempDetail;
                            oCommand.Parameters.Add(oParam);
                            oAdapter = new SqlDataAdapter(oCommand);
                            oCB = new SqlCommandBuilder(oAdapter);
                            oTable = new DataTable("TIMESHEET");
                            oAdapter.FillSchema(oTable, SchemaType.Source);
                            oAdapter.Fill(oTable);
                        }
                        DataTable oTemp = oTimesheetData.ToADODataTable();
                        DataRow oRow = oTable.LoadDataRow(oTemp.Rows[0].ItemArray, false);
                        if (!isMark)
                        {
                            oRow.Delete();
                            foreach (DataRow dr in oTable.Rows)
                            {
                                if (dr.RowState == DataRowState.Unchanged)
                                {
                                    dr.Delete();
                                }
                            }
                        }
                        //foreach (DataRow dr in oTable.Rows)
                        //{
                        //    if (dr.RowState == DataRowState.Unchanged)
                        //    {
                        //        dr.Delete();
                        //    }
                        //}
                        oAdapter.Update(oTable);
                    }
                    oTransaction.Commit();
                }
                catch (Exception ex)
                {
                    oTransaction.Rollback();
                    throw new Exception("save data error", ex);
                }
                finally
                {
                    if (oConnection.State == ConnectionState.Open)
                        oConnection.Close();

                    oConnection.Dispose();
                    if (oCB != null)
                    {
                        oCB.Dispose();
                    }
                    if (oCommand != null)
                    {
                        oCommand.Dispose();
                    }
                    if (oAdapter != null)
                    {
                        oAdapter.Dispose();
                    }
                    if (oTable != null)
                    {
                        oTable.Dispose();
                    }
                }
            }
        }

        public override void MappingTimePairInLateOutEarlyID(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            SqlCommand oCommand;
            SqlParameter param;
            SqlTransaction oTransaction = null;
            DateTime CurrentDate = new DateTime();
            try
            {
                oConnection.Open();
                oTransaction = oConnection.BeginTransaction();
                oCommand = new SqlCommand("sp_MappingTimePairInLateOutEarly", oConnection, oTransaction);
                oCommand.CommandType = CommandType.StoredProcedure;
                param = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
                param.Value = EmployeeID;
                oCommand.Parameters.Add(param);
                param = new SqlParameter("@p_BeginDate", SqlDbType.DateTime);
                param.Value = BeginDate;
                oCommand.Parameters.Add(param);
                param = new SqlParameter("@p_EndDate", SqlDbType.DateTime);
                param.Value = EndDate;
                oCommand.Parameters.Add(param);
                oCommand.ExecuteNonQuery();

                oTransaction.Commit();
            }
            catch (Exception ex)
            {
                oTransaction.Rollback();
                throw new Exception("mapping TimePairInLateOutEarly error " + CurrentDate.ToShortDateString(), ex);
            }
            finally
            {
                if (oConnection.State == ConnectionState.Open)
                    oConnection.Close();
                oConnection.Dispose();
                oTransaction.Dispose();
            }
        }

        public override void SaveTimePair(string EmployeeID, DateTime BeginDate, DateTime EndDate, List<TimePair> list)
        {
            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select * from TM_TimePair where EmployeeID = @EmployeeID and [Date] between @BeginDate and @EndDate", oConnection, tx);
            oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);
            SqlDataAdapter oAdapter = null;
            DataTable oTable = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);
            oTable = new DataTable("TM_TimePair");
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            Dictionary<string, TimePair> dictTimePairInDB = new Dictionary<string, TimePair>();
            string strKey = "";
            int seqno = 0;
            try
            {
                foreach (DataRow dr in oTable.Rows)
                {
                    TimePair item = new TimePair();
                    item.EmployeeID = (string)dr["EmployeeID"];
                    item.Date = (DateTime)dr["Date"];
                    TimeElement buffer;
                    if (!dr.IsNull("ClockIN"))
                    {
                        buffer = new TimeElement();
                        buffer.DoorType = DoorType.IN;
                        buffer.EventTime = (DateTime)dr["ClockIN"];
                        buffer.EmployeeID = dr["EmployeeID"].ToString();
                        item.ClockIN = buffer;
                    }
                    if (!dr.IsNull("ClockOUT"))
                    {
                        buffer = new TimeElement();
                        buffer.DoorType = DoorType.OUT;
                        buffer.EventTime = (DateTime)dr["ClockOUT"];
                        buffer.EmployeeID = dr["EmployeeID"].ToString();
                        item.ClockOUT = buffer;
                    }
                    strKey = string.Format("{0}|{1}", item.EmployeeID, item.Date.ToShortDateString());
                    dictTimePairInDB.Add(strKey, item);
                }

                list.Sort(new TimePairSortingDate());
                foreach (TimePair item in list.FindAll(delegate (TimePair tp) { return (tp.Date >= BeginDate && tp.Date <= EndDate); }))
                {
                    strKey = string.Format("{0}|{1}", item.EmployeeID, item.Date.ToShortDateString());
                    if (dictTimePairInDB.ContainsKey(strKey))
                    {
                        if (dictTimePairInDB[strKey].ClockIN != item.ClockIN ||
                        dictTimePairInDB[strKey].ClockOUT != item.ClockOUT)
                        {
                            string strCondition = string.Format("EmployeeID='{0}' AND Date='#{1}#'", item.EmployeeID, item.Date.ToString());
                            DataRow[] Rows = oTable.Select(strCondition);
                            if (Rows.Length > 0)
                            {
                                if (item.ClockIN != null && item.ClockIN.EventTime != null)
                                    Rows[0]["ClockIN"] = item.ClockIN.EventTime;
                                else
                                    Rows[0]["ClockIN"] = DBNull.Value;

                                if (item.ClockOUT != null && item.ClockOUT.EventTime != null)
                                    Rows[0]["ClockOUT"] = item.ClockOUT.EventTime;
                                else
                                    Rows[0]["ClockOUT"] = DBNull.Value;
                                if (Rows.Length > 1)
                                {
                                    for (int row = Rows.Length; row > 0; row--)
                                    {
                                        Rows[row].Delete();
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        dictTimePairInDB.Add(strKey, item);
                        DataRow dr = oTable.NewRow();
                        dr["EmployeeID"] = item.EmployeeID;
                        dr["Date"] = item.Date;
                        dr["SeqNo"] = item.SeqNo;
                        if (item.ClockIN != null && item.ClockIN.EventTime != null)
                            dr["ClockIN"] = item.ClockIN.EventTime;
                        else
                            dr["ClockIN"] = DBNull.Value;

                        if (item.ClockOUT != null && item.ClockOUT.EventTime != null)
                            dr["ClockOUT"] = item.ClockOUT.EventTime;
                        else
                            dr["ClockOUT"] = DBNull.Value;
                        oTable.LoadDataRow(dr.ItemArray, false);
                    }
                    seqno++;
                }
                dictTimePairInDB.Clear();
                dictTimePairInDB = null;

                oAdapter.Update(oTable);
                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("Can't archive data", ex);
            }
            finally
            {
                if (oConnection.State == ConnectionState.Open)
                    oConnection.Close();

                oConnection.Dispose();
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                if (oCommand != null)
                {
                    oCommand.Dispose();
                }
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oTable != null)
                {
                    oTable.Dispose();
                }
            }
        }

        public override void SaveTimePairArchiveMappingByUser(List<TimePair> TimePairList)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();

            SqlTransaction oTransaction = oConnection.BeginTransaction();
            SqlCommand oCommand = new SqlCommand("sp_TimePairArchiveMappingByUserSave", oConnection, oTransaction);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;

            try
            {
                foreach (TimePair oTimePair in TimePairList)
                {
                    oCommand.Parameters.Clear();

                    oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
                    oParam.Value = oTimePair.EmployeeID;
                    oCommand.Parameters.Add(oParam);

                    oParam = new SqlParameter("@p_Date", SqlDbType.DateTime);
                    oParam.Value = oTimePair.Date;
                    oCommand.Parameters.Add(oParam);

                    if (oTimePair.ClockIN != null)
                    {
                        oParam = new SqlParameter("@p_ClockIN", SqlDbType.DateTime);
                        oParam.Value = oTimePair.ClockIN.EventTime;
                        oCommand.Parameters.Add(oParam);
                    }
                    if (oTimePair.ClockOUT != null)
                    {
                        oParam = new SqlParameter("@p_ClockOUT", SqlDbType.DateTime);
                        oParam.Value = oTimePair.ClockOUT.EventTime;
                        oCommand.Parameters.Add(oParam);
                    }

                    oCommand.ExecuteNonQuery();
                }
                oTransaction.Commit();
            }
            catch (Exception ex)
            {
                oTransaction.Rollback();
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
            }
        }

        public override void SaveTimePairArchiveMappingByUser(List<TimePair> TimePairList, string RequestNo)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();

            SqlTransaction oTransaction = oConnection.BeginTransaction();
            SqlCommand oCommand = new SqlCommand("sp_TimePairArchiveMappingByUserSave", oConnection, oTransaction);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;

            try
            {
                foreach (TimePair oTimePair in TimePairList)
                {
                    oCommand.Parameters.Clear();

                    oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
                    oParam.Value = oTimePair.EmployeeID;
                    oCommand.Parameters.Add(oParam);

                    oParam = new SqlParameter("@p_Date", SqlDbType.DateTime);
                    oParam.Value = oTimePair.Date;
                    oCommand.Parameters.Add(oParam);

                    if (oTimePair.ClockIN != null)
                    {
                        oParam = new SqlParameter("@p_ClockIN", SqlDbType.DateTime);
                        oParam.Value = oTimePair.ClockIN.EventTime;
                        oCommand.Parameters.Add(oParam);
                    }
                    if (oTimePair.ClockOUT != null)
                    {
                        oParam = new SqlParameter("@p_ClockOUT", SqlDbType.DateTime);
                        oParam.Value = oTimePair.ClockOUT.EventTime;
                        oCommand.Parameters.Add(oParam);
                    }

                    oParam = new SqlParameter("@p_RequestNo", SqlDbType.VarChar);
                    oParam.Value = RequestNo;
                    oCommand.Parameters.Add(oParam);

                    oCommand.ExecuteNonQuery();
                }
                oTransaction.Commit();
            }
            catch (Exception ex)
            {
                oTransaction.Rollback();
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
            }
        }

        public override void SaveArchiveEvent(string EmployeeID, DateTime BeginDate, DateTime EndDate, List<TimeElement> Data)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlParameter oParam;
            SqlCommand oCommand = null;
            SqlDataAdapter oAdapter = null;
            DataTable oTable = null;
            SqlCommandBuilder oCB = null;
            oCommand = new SqlCommand("select * from TimeEventArchive where EmployeeID = @employeeid and EventTime between @BeginDate and @EndDate", oConnection);
            oParam = new SqlParameter("@employeeid", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);
            oTable = new DataTable("TIMEEVENTARCHIVE");
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);

            DataRow dr;
            try
            {
                foreach (DataRow row in oTable.Rows)
                    row.Delete();
                oAdapter.Update(oTable);

                foreach (TimeElement item in Data.FindAll(delegate (TimeElement te)
                {
                    return (te.EventTime.Date >= BeginDate && te.EventTime <= EndDate);
                }))
                {
                    dr = oTable.NewRow();
                    item.LoadDataToTableRow(dr);
                    try
                    {
                        oTable.LoadDataRow(dr.ItemArray, false);
                    }
                    catch (ConstraintException ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                }

                oAdapter.Update(oTable);

            }
            catch (Exception ex)
            {
                throw new Exception("Can't archive data", ex);
            }
            finally
            {
                oConnection.Dispose();
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                if (oCommand != null)
                {
                    oCommand.Dispose();
                }
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oTable != null)
                {
                    oTable.Dispose();
                }
            }
        }
        #endregion

        #region " SaveArchiveTimePair "
        public override void SaveArchiveTimePair(string EmployeeID, DateTime BeginDate, DateTime EndDate, List<TimePair> list)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlParameter oParam;
            SqlCommand oCommand = null;
            SqlDataAdapter oAdapter = null;
            DataTable oTable = null;
            SqlCommandBuilder oCB = null;
            oCommand = new SqlCommand("select * from TimePairArchive where EmployeeID = @employeeid and [Date] between @BeginDate and @EndDate", oConnection);
            oParam = new SqlParameter("@employeeid", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);
            oTable = new DataTable("TIMEPAIRARCHIVE");
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);

            DataRow dr;
            int seqno = 0;
            DateTime lastDate = DateTime.MinValue;
            try
            {
                foreach (DataRow row in oTable.Rows)
                    row.Delete();
                oAdapter.Update(oTable);

                list.Sort(new TimePairSortingDate());
                foreach (TimePair item in list.FindAll(delegate (TimePair tp)
                {
                    return (tp.Date >= BeginDate && tp.Date <= EndDate);
                }))
                {
                    dr = oTable.NewRow();
                    item.LoadDataToTableRow(dr);
                    try
                    {
                        item.SeqNo = seqno;
                        item.LoadDataToTableRow(dr);
                        oTable.LoadDataRow(dr.ItemArray, false);
                        seqno++;
                    }
                    catch (ConstraintException ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                }

                oAdapter.Update(oTable);
            }
            catch (Exception ex)
            {
                throw new Exception("Can't archive data", ex);
            }
            finally
            {
                oConnection.Dispose();
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                if (oCommand != null)
                {
                    oCommand.Dispose();
                }
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oTable != null)
                {
                    oTable.Dispose();
                }
            }
        }
        #endregion

        #region " LoadApplicationCollision "
        public override List<DATACLASS.CollisionControl> LoadApplicationCollision(string ApplicationKey, string SubKey1, string SubKey2)
        {
            List<DATACLASS.CollisionControl> oReturn = new List<DATACLASS.CollisionControl>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("COLLISIONCONTROL");
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("select * from CollisionControl Where ApplicationKeyNew = @ApplicationKey and (SubKey1New = '*' or SubKey1New = @SubKey1) and (SubKey2New = '*' or SubKey2New = @SubKey2)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ApplicationKey", SqlDbType.VarChar);
            oParam.Value = ApplicationKey;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@SubKey1", SqlDbType.VarChar);
            oParam.Value = SubKey1;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@SubKey2", SqlDbType.VarChar);
            oParam.Value = SubKey2;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                DATACLASS.CollisionControl item = new DATACLASS.CollisionControl();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }
            oTable.Dispose();
            return oReturn;

        }
        #endregion

        #region " LoadTimesheetCollision "
        public override List<TimesheetData> LoadTimesheetCollision(string EmployeeID, DateTime BeginDate, DateTime EndDate, DATACLASS.CollisionControl Key)
        {
            List<TimesheetData> oReturn = new List<TimesheetData>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("TIMESHEETDATA");
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("select * from FindCollision(@EmployeeID, @BeginDate, @EndDate, @ApplicationKey, @SubKey1, @SubKey2)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@ApplicationKey", SqlDbType.VarChar);
            oParam.Value = Key.ApplicationKeyExists;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@SubKey1", SqlDbType.VarChar);
            oParam.Value = Key.SubKey1Exists;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@SubKey2", SqlDbType.VarChar);
            oParam.Value = Key.SubKey2Exists;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                TimesheetData item = new TimesheetData();
                item.EmployeeID = dr["EmployeeID"].ToString();
                item.ApplicationKey = dr["ApplicationKey"].ToString();
                item.SubKey1 = dr["SubKey1"].ToString();
                item.SubKey2 = dr["SubKey2"].ToString();
                item.BeginDate = Convert.ToDateTime(dr["BeginDate"]);
                item.EndDate = Convert.ToDateTime(dr["EndDate"]);
                item.FullDay = Convert.ToBoolean(dr["FullDay"]);
                item.Detail = dr["Detail"].ToString();
                //item.ParseToObject(dr);
                oReturn.Add(item);
            }
            oTable.Dispose();
            return oReturn;

        }
        #endregion

        #region " LoadAdditionalDataCollisionable "
        public override ICollisionable LoadAdditionalDataCollisionable(string ApplicationKey)
        {
            ICollisionable oReturn = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("ADDITIONALCOLLISION");
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("select * from AdditionalCollision Where ApplicationKey = @ApplicationKey", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ApplicationKey", SqlDbType.VarChar);
            oParam.Value = ApplicationKey;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                Assembly oAssembly;
                Type oType;
                string assemblyName = (string)dr["AssemblyName"];
                string typeName = (string)dr["ClassName"];
                oAssembly = Assembly.Load(assemblyName);
                if (oAssembly != null)
                {
                    oType = oAssembly.GetType(typeName);
                    if (oType != null)
                    {

                        if (oType.GetInterfaceMap(typeof(ICollisionable)).InterfaceType != null)
                        {
                            oReturn = (ICollisionable)Activator.CreateInstance(oType);
                        }
                    }
                }
                break;
            }
            oTable.Dispose();
            return oReturn;
        }

        public override List<TimeElement> LoadArchiveEvent(string EmployeeID, DateTime BeginDate, DateTime EndDate, int RecordCount)
        {
            List<TimeElement> oReturn = new List<TimeElement>();
            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            SqlParameter oParam;
            SqlCommand oCommand = null;
            SqlDataAdapter oAdapter = null;
            DataTable oTable = null;
            if (RecordCount > 0)
            {
                oCommand = new SqlCommand(string.Format("select top {0} * from TimeEventArchive where EmployeeID = @employeeid and EventTime between @BeginDate and @EndDate order by EventTime Desc", RecordCount), oConnection);
            }
            else
            {
                oCommand = new SqlCommand("select * from TimeEventArchive where EmployeeID = @employeeid and EventTime between @BeginDate and @EndDate order by EventTime Desc", oConnection);
            }
            oParam = new SqlParameter("@employeeid", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);
            oAdapter = new SqlDataAdapter(oCommand);
            oTable = new DataTable("TIMEEVENTARCHIVE");
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Dispose();
            oCommand.Dispose();
            oAdapter.Dispose();

            TimeElement item;
            foreach (DataRow dr in oTable.Rows)
            {
                item = new TimeElement();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oTable.Dispose();

            return oReturn;
        }

        public override List<CardSetting> LoadCardSetting(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            List<CardSetting> oReturn = new List<CardSetting>();
            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("CARDSETTING");
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("select * from CardSetting Where EmployeeID = @EmployeeID and BeginDate <= @EndDate and EndDate >= @BeginDate", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            if (oTable.Rows.Count > 0)
                foreach (DataRow dr in oTable.Rows)
                {
                    CardSetting item = new CardSetting();

                    item.EmployeeID = dr["EmployeeID"].ToString();
                    item.BeginDate = Convert.ToDateTime(dr["BeginDate"]);
                    item.EndDate = Convert.ToDateTime(dr["EndDate"]);
                    item.CardNo = dr["CardNo"].ToString();
                    item.Location = dr["Location"].ToString();
                    //item.ParseToObject(dr);
                    oReturn.Add(item);
                }
            else
            {
                CardSetting _CardSetting = new CardSetting();
                _CardSetting.CardNo = _CardSetting.EmployeeID = EmployeeID;
                _CardSetting.BeginDate = DateTime.MinValue;
                _CardSetting.EndDate = DateTime.MaxValue;
                _CardSetting.Location = "PTTCH";
                oReturn.Add(_CardSetting);
            }
            oTable.Dispose();
            return oReturn;
        }

        public override List<CardSetting> LoadCardSetting(string EmployeeID)
        {
            List<CardSetting> oReturn = new List<CardSetting>();
            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("CARDSETTING");
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("select * from CardSetting Where EmployeeID = @EmployeeID", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            if (oTable.Rows.Count > 0)
                foreach (DataRow dr in oTable.Rows)
                {
                    CardSetting item = new CardSetting();
                    item.ParseToObject(dr);
                    oReturn.Add(item);
                }
            else
            {
                CardSetting _CardSetting = new CardSetting();
                _CardSetting.CardNo = _CardSetting.EmployeeID = EmployeeID;
                _CardSetting.BeginDate = DateTime.MinValue;
                _CardSetting.EndDate = DateTime.MaxValue;
                _CardSetting.Location = "PTTCH";
                oReturn.Add(_CardSetting);
            }

            oTable.Dispose();
            return oReturn;
        }


        public override List<INFOTYPE2003_LOG> LoadINFOTYPE2003LOG(string RequestNo)
        {
            List<INFOTYPE2003_LOG> oReturn = new List<INFOTYPE2003_LOG>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_INFOTYPE2003LogGetByRequestNo", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter param;

            param = new SqlParameter("@p_RequestNo", SqlDbType.VarChar);
            param.Value = RequestNo;
            oCommand.Parameters.Add(param);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("INFOTYPE2003LOG");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE2003_LOG item = new INFOTYPE2003_LOG();

                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oConnection.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();

            return oReturn;
        }

        public override List<TimePair> LoadTimePairArchiveMappingByUser(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            List<TimePair> oReturn = new List<TimePair>();
            DataTable oTable = new DataTable();
            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            oConnection.Open();

            SqlCommand oCommand = new SqlCommand("sp_LoadTimePairArchiveMappingByUser", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);

            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);

            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            TimePair oTimePair;
            foreach (DataRow dr in oTable.Rows)
            {
                oTimePair = new TimePair();
                oTimePair.EmployeeID = dr["EmployeeID"].ToString();
                if (!DBNull.Value.Equals(dr["ClockIN"]))
                {
                    oTimePair.ClockIN = new TimeElement();
                    oTimePair.ClockIN.EventTime = Convert.ToDateTime(dr["ClockIN"], oCL);
                }
                if (!DBNull.Value.Equals(dr["ClockOUT"]))
                {
                    oTimePair.ClockOUT = new TimeElement();
                    oTimePair.ClockOUT.EventTime = Convert.ToDateTime(dr["ClockOUT"], oCL);
                }
                oTimePair.Date = Convert.ToDateTime(dr["Date"], oCL);
                oReturn.Add(oTimePair);
            }

            return oReturn;
        }

        public override List<TimePairArchive> LoadTimePairArchive(DateTime BeginDate, DateTime EndDate)
        {
            return LoadTimePairArchive(String.Empty, BeginDate, EndDate);
        }

        public override List<TimePairArchive> LoadTimePairArchive(string EmpID, DateTime BeginDate, DateTime EndDate)
        {
            List<TimePairArchive> Result = new List<TimePairArchive>();
            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            SqlParameter oParam;
            SqlCommand oCommand = null;
            SqlDataAdapter oAdapter = null;
            DataTable oTable = null;

            oConnection.Open();
            oCommand = new SqlCommand("sp_TimpairArchiveGet", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            //if (String.IsNullOrEmpty(EmpID))
            //    oCommand = new SqlCommand("Select * From TimePairArchive where (Date Between @BeginDate and @EndDate)", oConnection);
            //else
            //    oCommand = new SqlCommand("Select * From TimePairArchive where EmployeeID = @EmployeeID and (Date Between @BeginDate and @EndDate)", oConnection);

            //if (!String.IsNullOrEmpty(EmpID))
            //{
            //    oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            //    oParam.Value = EmpID;
            //    oCommand.Parameters.Add(oParam);
            //}

            oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmpID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);
            oAdapter = new SqlDataAdapter(oCommand);
            oTable = new DataTable("TIMEPAIRARCHIVE");

            try
            {
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);
            }
            catch (Exception ex)
            {
                throw new Exception("Can't load archive data", ex);
            }
            finally
            {
                oCommand.Dispose();
                oConnection.Close();
                oConnection.Dispose();
            }

            TimePairArchive item;
            foreach (DataRow row in oTable.Rows)
            {
                item = new TimePairArchive();
                item.ParseToObject(row);
                Result.Add(item);
            }
            return Result;
        }

        public override List<INFOTYPE2001> GetTM_AbsenceList(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            List<INFOTYPE2001> oReturn = new List<INFOTYPE2001>();
            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            SqlCommand oCommand;
            if (BeginDate.TimeOfDay <= new TimeSpan(0, 0, 0))
                oCommand = new SqlCommand("select * from TM_Absence Where EmployeeID = @EmpID and (BeginDate <= @EndDate and EndDate >= @BeginDate)", oConnection);
            else
                oCommand = new SqlCommand("select * from TM_Absence Where EmployeeID = @EmpID and (BeginDate <= @EndDate and EndDate >= @BeginDate) AND ((EndTime > @BeginTime AND BeginTime < @EndTime) OR (BeginTime = '00:00:00' AND EndTime = '00:00:00' AND AllDayFlag=1))", oConnection);

            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate.Date;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate.Date;
            oCommand.Parameters.Add(oParam);

            if (BeginDate.TimeOfDay > new TimeSpan(0, 0, 0))
            {
                oParam = new SqlParameter("@BeginTime", SqlDbType.VarChar);
                oParam.Value = BeginDate.TimeOfDay.ToString();
                oCommand.Parameters.Add(oParam);
            }
            if (EndDate.TimeOfDay > new TimeSpan(0, 0, 0))
            {
                oParam = new SqlParameter("@EndTime", SqlDbType.VarChar);
                oParam.Value = EndDate.TimeOfDay.ToString();
                oCommand.Parameters.Add(oParam);
            }

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("TM_Absence");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE2001 item = new INFOTYPE2001();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oTable.Dispose();
            return oReturn;
        }
        public override List<INFOTYPE2002> GetTM_AttendanceList(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            List<INFOTYPE2002> oReturn = new List<INFOTYPE2002>();
            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from TM_Attendance Where EmployeeID = @EmpID and (BeginDate <= @EndDate and EndDate >= @BeginDate) AND ((EndTime > @BeginTime AND BeginTime < @EndTime) OR (BeginTime = '00:00:00' AND EndTime = '00:00:00' AND AllDayFlag=1))", oConnection);
            if (BeginDate.TimeOfDay <= new TimeSpan(0, 0, 0))
                oCommand = new SqlCommand("select * from TM_Attendance Where EmployeeID = @EmpID and (BeginDate <= @EndDate and EndDate >= @BeginDate)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate.Date;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate.Date;
            oCommand.Parameters.Add(oParam);

            if (BeginDate.TimeOfDay > new TimeSpan(0, 0, 0))
            {
                oParam = new SqlParameter("@BeginTime", SqlDbType.VarChar);
                oParam.Value = BeginDate.TimeOfDay.ToString();
                oCommand.Parameters.Add(oParam);
            }
            if (EndDate.TimeOfDay > new TimeSpan(0, 0, 0))
            {
                oParam = new SqlParameter("@EndTime", SqlDbType.VarChar);
                oParam.Value = EndDate.TimeOfDay.ToString();
                oCommand.Parameters.Add(oParam);
            }

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("TM_Attendance");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                INFOTYPE2002 item = new INFOTYPE2002();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oTable.Dispose();
            return oReturn;
        }

        public override List<TimePair> GetTM_TimePairList(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            List<TimePair> oReturn = new List<TimePair>();
            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from TM_TimePair Where EmployeeID = @EmpID and Date BETWEEN @BeginDate AND @EndDate", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate.Date;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate.Date;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("TM_TimePair");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                TimePair item = new TimePair();
                item.EmployeeID = (string)dr["EmployeeID"];
                item.Date = (DateTime)dr["Date"];
                TimeElement buffer;
                if (!dr.IsNull("ClockIN"))
                {
                    buffer = new TimeElement();
                    buffer.DoorType = DoorType.IN;
                    buffer.EventTime = (DateTime)dr["ClockIN"];
                    buffer.EmployeeID = dr["EmployeeID"].ToString();
                    item.ClockIN = buffer;
                }
                if (!dr.IsNull("ClockOUT"))
                {
                    buffer = new TimeElement();
                    buffer.DoorType = DoorType.OUT;
                    buffer.EventTime = (DateTime)dr["ClockOUT"];
                    buffer.EmployeeID = dr["EmployeeID"].ToString();
                    item.ClockOUT = buffer;
                }
                oReturn.Add(item);
            }

            oTable.Dispose();
            return oReturn;
        }

        public override List<TM_Timesheet> GetTM_Timesheet(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            List<TM_Timesheet> oReturn = new List<TM_Timesheet>();
            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from v_TM_Timesheet Where EmployeeID = @EmpID and Date BETWEEN @BeginDate AND @EndDate ORDER BY EmployeeID,Date,BeginDate,BeginTime", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate.Date;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate.Date;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("TM_Timesheet");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                TM_Timesheet item = new TM_Timesheet();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oTable.Dispose();
            return oReturn;
        }
        #endregion


        public override void SaveLG_TIMEREQUEST(List<INFOTYPE2001> data, string type)
        {
            if (data.Count == 0)
            {
                return;
            }
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select * from LG_TIMEREQUEST where RequestNo = @RequestNo", oConnection, tx);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);

            DataTable oTable = new DataTable("LG_TIMEREQUEST");

            oParam = new SqlParameter("@RequestNo", SqlDbType.VarChar);
            oCommand.Parameters.Add(oParam);
            oAdapter.FillSchema(oTable, SchemaType.Source);

            List<string> requestList = new List<string>();

            try
            {
                foreach (INFOTYPE2001 item in data)
                {
                    DataRow dr = oTable.NewRow();
                    dr["EmployeeID"] = item.EmployeeID;
                    dr["AbsAttType"] = item.AbsenceType;
                    dr["BeginDate"] = item.BeginDate;
                    dr["EndDate"] = item.EndDate;

                    if (item.AllDayFlag)
                    {
                        dr["BeginTime"] = TimeSpan.Zero;
                        dr["EndTime"] = TimeSpan.Zero;
                    }
                    else
                    {
                        dr["BeginTime"] = item.BeginTime;
                        dr["EndTime"] = item.EndTime;
                    }

                    dr["AbsAttDays"] = item.AbsenceDays;
                    dr["AbsAttHours"] = item.AbsenceHours;
                    dr["PayrollDays"] = item.PayrollDays;
                    dr["AllDayFlag"] = item.AllDayFlag;
                    dr["Remark"] = item.Remark;
                    dr["RequestNo"] = item.RequestNo;
                    dr["CategoryCode"] = type;
                    dr["CreatedDate"] = DateTime.Now;
                    dr["UpdatedDate"] = DateTime.Now;
                    dr["UpdatedStatus"] = true;
                    dr["UpdatedDesc"] = string.Empty;
                    oTable.Rows.Add(dr);
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }

        public override void SaveLG_TIMEREQUEST(List<INFOTYPE2002> data, string type)
        {
            if (data.Count == 0)
            {
                return;
            }
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction tx = oConnection.BeginTransaction();
            SqlParameter oParam;
            SqlCommand oCommand = new SqlCommand("select * from LG_TIMEREQUEST where RequestNo = @RequestNo", oConnection, tx);
            SqlDataAdapter oAdapter = null;
            SqlCommandBuilder oCB = null;
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);

            DataTable oTable = new DataTable("LG_TIMEREQUEST");

            oParam = new SqlParameter("@RequestNo", SqlDbType.VarChar);
            oCommand.Parameters.Add(oParam);
            oAdapter.FillSchema(oTable, SchemaType.Source);

            List<string> requestList = new List<string>();

            try
            {
                foreach (INFOTYPE2002 item in data)
                {
                    DataRow dr = oTable.NewRow();
                    dr["EmployeeID"] = item.EmployeeID;
                    dr["AbsAttType"] = item.AttendanceType;
                    dr["BeginDate"] = item.BeginDate;
                    dr["EndDate"] = item.EndDate;
                    dr["BeginTime"] = item.BeginTime;
                    dr["EndTime"] = item.EndTime;
                    dr["AbsAttDays"] = item.AttendanceDays;
                    dr["AbsAttHours"] = item.AttendanceHours;
                    dr["PayrollDays"] = item.PayrollDays;
                    dr["AllDayFlag"] = item.AllDayFlag;
                    dr["Remark"] = item.Remark;
                    dr["RequestNo"] = item.RequestNo;
                    dr["CategoryCode"] = type;
                    dr["CreatedDate"] = DateTime.Now;
                    dr["UpdatedDate"] = DateTime.Now;
                    dr["UpdatedStatus"] = true;
                    dr["UpdatedDesc"] = string.Empty;
                    oTable.Rows.Add(dr);
                }

                oAdapter.Update(oTable);

                tx.Commit();
            }
            catch (Exception ex)
            {
                tx.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
                oCommand.Dispose();
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                oTable.Dispose();
            }
        }

        public override List<AreaWorkscheduleLog> GetAreaWorkscheduleLogByRequestNo(string RequestNo)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            List<AreaWorkscheduleLog> oReturn = new List<AreaWorkscheduleLog>();
            DataTable oTable = new DataTable();
            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            oConnection.Open();

            SqlCommand oCommand = new SqlCommand("sp_TM_AreaWorkscheduleLogGetByRequestNo", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);

            oParam = new SqlParameter("@p_RequestNo", SqlDbType.VarChar);
            oParam.Value = RequestNo;
            oCommand.Parameters.Add(oParam);

            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                AreaWorkscheduleLog oItem = new AreaWorkscheduleLog();
                oItem.ParseToObject(dr);
                oReturn.Add(oItem);
            }

            return oReturn;
        }


        public override List<AreaWorkscheduleLog> GetAreaWorkscheduleLogByPeriod(DateTime BeginDate, DateTime EndDate, string CreatorID)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            List<AreaWorkscheduleLog> oReturn = new List<AreaWorkscheduleLog>();
            DataTable oTable = new DataTable();
            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            oConnection.Open();

            SqlCommand oCommand = new SqlCommand("sp_TM_AreaWorkscheduleLogGetByPeriod", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);

            oParam = new SqlParameter("@p_BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate.Date;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate.Date;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_CreatorID", SqlDbType.VarChar);
            oParam.Value = CreatorID;
            oCommand.Parameters.Add(oParam);

            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                AreaWorkscheduleLog oItem = new AreaWorkscheduleLog();
                oItem.ParseToObject(dr);
                oReturn.Add(oItem);
            }

            return oReturn;
        }

        public override void SaveAreaWorkscheduleLog(List<AreaWorkscheduleLog> data, bool IsMark)
        {
            DeleteAreaWorkscheduleLog(data);
            if (!IsMark) { return; }

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlParameter param;
            SqlTransaction oTransaction = null;

            try
            {
                oConnection.Open();
                oTransaction = oConnection.BeginTransaction();

                oCommand = new SqlCommand("sp_TM_AreaWorkscheduleLogInsert", oConnection, oTransaction);
                oCommand.CommandType = CommandType.StoredProcedure;

                foreach (AreaWorkscheduleLog Area in data)
                {
                    oCommand.Parameters.Clear();
                    param = new SqlParameter("@p_RequestNo", SqlDbType.VarChar);
                    param.Value = Area.RequestNo;
                    oCommand.Parameters.Add(param);

                    param = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
                    param.Value = Area.EmployeeID;
                    oCommand.Parameters.Add(param);

                    param = new SqlParameter("@p_AreaWorkscheduleID", SqlDbType.Int);
                    param.Value = Area.AreaWorkscheduleID;
                    oCommand.Parameters.Add(param);

                    param = new SqlParameter("@p_EffectiveDate", SqlDbType.DateTime);
                    param.Value = Area.EffectiveDate;
                    oCommand.Parameters.Add(param);

                    param = new SqlParameter("@p_Status", SqlDbType.VarChar);
                    param.Value = Area.Status;
                    oCommand.Parameters.Add(param);

                    param = new SqlParameter("@p_CreatedDate", SqlDbType.DateTime);
                    param.Value = Area.CreatedDate;
                    oCommand.Parameters.Add(param);

                    param = new SqlParameter("@p_CreatorID", SqlDbType.VarChar);
                    param.Value = Area.CreatorID;
                    oCommand.Parameters.Add(param);

                    oCommand.ExecuteNonQuery();
                }

                oTransaction.Commit();
            }
            catch (Exception ex)
            {
                oTransaction.Rollback();
                throw new Exception("save data error", ex);
            }
            finally
            {
                if (oConnection.State == ConnectionState.Open)
                    oConnection.Close();
                oConnection.Dispose();
                oTransaction.Dispose();
            }
        }

        void DeleteAreaWorkscheduleLog(List<AreaWorkscheduleLog> data)
        {
            List<string> sqlcommand = new List<string>();

            foreach (AreaWorkscheduleLog Area in data)
            {
                sqlcommand.Add(String.Format("DELETE FROM TM_AreaWorkscheduleLog WHERE RequestNo = '{0}'", Area.RequestNo));
            }
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlTransaction trn = oConnection.BeginTransaction();
            SqlCommand oCommand = null;

            foreach (string str in sqlcommand)
            {
                oCommand = new SqlCommand(str, oConnection, trn);
                try
                {
                    oCommand.ExecuteNonQuery();
                }
                catch
                {
                    trn.Rollback();
                    oConnection.Close();
                    oConnection.Dispose();
                    oCommand.Dispose();
                    return;
                }
            }
            trn.Commit();
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
        }

        public override List<ReportGroupAdmin> GetGroupReportAdmin(int reportGroupId)
        {
            List<ReportGroupAdmin> list_group = new List<ReportGroupAdmin>();

            SqlConnection oConnection = new SqlConnection(BaseConnStr);

            //string sql_command = "Select * FROM AdminGroupMapping Where SubjectReportGroupID = @groupId  ORDER BY SubjectReportSubID ASC";
            string sql_command = "select a.SubjectReportSubID,a.SubjectReportGroupID,b.SubjectCode";
            sql_command += " from AdminGroupMapping a inner join ApplicationSubject b on a.SubjectReportSubID = b.SubjectID";
            sql_command += " where a.SubjectReportGroupID = @groupId";
            sql_command += " ORDER BY a.SeqNo ASC";

            SqlCommand oCommand = new SqlCommand(sql_command, oConnection);
            oCommand.CommandType = CommandType.Text;

            SqlParameter oParam;
            oParam = new SqlParameter("@groupId", SqlDbType.Int);
            oParam.Value = reportGroupId;
            oCommand.Parameters.Add(oParam);

            oConnection.Open();

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("GroupReport");

            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose(); // Clear memory
            oCommand.Dispose(); // Clear memory

            if (oTable.Rows.Count > 0)
            {
                foreach (DataRow row in oTable.Rows)
                {
                    ReportGroupAdmin item = new ReportGroupAdmin();
                    item.ParseToObject(row);
                    list_group.Add(item);
                }
            }

            return list_group;
        }

        //AddBy: Ratchatawan W. (2013-06-04)
        public override DataTable GetAbsenceReportByEmployeeXML(string EmployeeXML, string AType, DateTime BeginDate, DateTime EndDate, EmployeeData oEmp)
        {
            DataTable oReturn = new DataTable();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            SqlCommand oCommand = new SqlCommand("sp_GetAbsenceReportByEmployeeXML", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            oParam = new SqlParameter("@p_EmployeeList", SqlDbType.Xml);
            oParam.Value = EmployeeXML;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_AType", SqlDbType.Xml);
            oParam.Value = AType;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@Key1", SqlDbType.VarChar);
            oParam.Value = oEmp.OrgAssignment.SubAreaSetting.AbsAttGrouping;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            oAdapter.Fill(oReturn);
            oConnection.Close();
            return oReturn;
        }

        public override List<OTLogAll> GetOTLogAll(string year, string month)
        {
            List<OTLogAll> list_all_ot = new List<OTLogAll>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_GetOTLogAll", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;

            oParam = new SqlParameter("@p_Year", SqlDbType.VarChar);
            oParam.Value = year;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_Month", SqlDbType.VarChar);
            oParam.Value = month;
            oCommand.Parameters.Add(oParam);

            oConnection.Open();

            DataTable oTable = new DataTable("OTLogPostSap");
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);

            oAdapter.Fill(oTable);
            oConnection.Close(); // Close Connect
            oConnection.Dispose(); // Clear memory
            oCommand.Dispose(); // Clear memory

            if (oTable.Rows.Count > 0)
            {
                foreach (DataRow row in oTable.Rows)
                {
                    OTLogAll ot_log = new OTLogAll();
                    ot_log.ParseToObject(row);
                    list_all_ot.Add(ot_log);
                }
            }
            return list_all_ot;
        }

        public override List<PeriodOTSumary> GetPeiodReportOTSummary(string category, DateTime date_now)
        {
            List<PeriodOTSumary> list_period_ot_sumary = new List<PeriodOTSumary>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_TM_GetConfigReportSetting", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;

            oParam = new SqlParameter("@p_Category", SqlDbType.VarChar);
            oParam.Value = category;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_Datetime", SqlDbType.DateTime);
            oParam.Value = date_now.Date;
            oCommand.Parameters.Add(oParam);

            oConnection.Open();

            DataTable oTable = new DataTable("PeriodReportOTSetting");
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);

            oAdapter.Fill(oTable);
            oConnection.Close(); // Close Connect
            oConnection.Dispose(); // Clear memory
            oCommand.Dispose(); // Clear memory

            if (oTable.Rows.Count > 0)
            {
                foreach (DataRow row in oTable.Rows)
                {
                    PeriodOTSumary period_report = new PeriodOTSumary();
                    period_report.ParseToObject(row);
                    period_report.MinMinute = period_report.MinValue * 60; // �ӹǹҷյ���ش
                    period_report.MaxMinute = period_report.MaxValue * 60; // �ӹǹ�ҷ��٧�ش
                    list_period_ot_sumary.Add(period_report);
                }
            }

            return list_period_ot_sumary;
        }

        #region ��§ҹ��ػ�ѹ�һ�����Шӹǹ��ѡ�ҹ����һ���

        public override List<MasterWorkLocation> GetMasterWorklocation(int year)
        {
            List<MasterWorkLocation> list_master_worklocation = new List<MasterWorkLocation>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            string cmd = "SELECT * FROM WorkLocation wl " +
                "WHERE @p_year >= YEAR(wl.EffectiveDate)  AND @p_year <= YEAR(wl.[ExpireDate])";
            SqlCommand oCommand = new SqlCommand(cmd, oConnection);
            oCommand.CommandType = CommandType.Text;
            oCommand.Parameters.Add("@p_year", SqlDbType.Int).Value = year;

            oConnection.Open();

            DataTable oTable = new DataTable("MasterWorLocation");
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            oAdapter.Fill(oTable);

            if (oTable.Rows.Count > 0)
            {
                foreach (DataRow dr in oTable.Rows)
                {
                    MasterWorkLocation work_location = new MasterWorkLocation();
                    work_location.ParseToObject(dr);
                    list_master_worklocation.Add(work_location);
                }
            }


            oAdapter.Dispose();
            oCommand.Dispose();
            oTable.Dispose();
            if (oConnection.State == ConnectionState.Open)
                oConnection.Close();

            return list_master_worklocation;
        }

        public override List<DbAbsenceSickLeave> GetAbsenceSickLeaveSummary(int year, string abs_group)
        {
            List<DbAbsenceSickLeave> list_db_SickLeave = new List<DbAbsenceSickLeave>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_TM_ReportLeaveSummaryWorkLocation", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.Add("@p_year", SqlDbType.Int).Value = year;
            oCommand.Parameters.Add("@p_abs_group", SqlDbType.VarChar).Value = abs_group;

            oConnection.Open();

            DataTable oTable = new DataTable("SickLeaveEmployeeInWorkLocation");
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            oAdapter.Fill(oTable);

            if (oTable.Rows.Count > 0)
            {
                foreach (DataRow dr in oTable.Rows)
                {
                    DbAbsenceSickLeave sickLeave = new DbAbsenceSickLeave();
                    sickLeave.ParseToObject(dr);
                    list_db_SickLeave.Add(sickLeave);
                }
            }

            oAdapter.Dispose();
            oCommand.Dispose();
            oTable.Dispose();
            if (oConnection.State == ConnectionState.Open)
                oConnection.Close();

            return list_db_SickLeave;
        }

        #endregion


        #region  OT Dashabord

        public string DecryptAes256(string combinedString, string keyString)
        {
            string plainText;
            byte[] combinedData = Convert.FromBase64String(combinedString);
            Aes aes = Aes.Create();
            aes.KeySize = 256;
            aes.BlockSize = 128;
            aes.Key = Encoding.UTF8.GetBytes(keyString);
            byte[] iv = new byte[aes.BlockSize / 8];
            byte[] cipherText = new byte[combinedData.Length - iv.Length];
            Array.Copy(combinedData, iv, iv.Length);
            Array.Copy(combinedData, iv.Length, cipherText, 0, cipherText.Length);
            aes.IV = iv;
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;
            ICryptoTransform decipher = aes.CreateDecryptor(aes.Key, aes.IV);

            using (MemoryStream ms = new MemoryStream(cipherText))
            {
                using (CryptoStream cs = new CryptoStream(ms, decipher, CryptoStreamMode.Read))
                {
                    using (StreamReader sr = new StreamReader(cs))
                    {
                        plainText = sr.ReadToEnd();
                    }
                }

                return plainText;
            }
        }

        // �ͧ��� (�Ҩ���������)
        public override List<OrgUnitEmployeeByRole> GetListOrgUnitByUserRole(string employeeId, string userRole, string companyCode)
        {
            List<OrgUnitEmployeeByRole> list_org_unit = new List<OrgUnitEmployeeByRole>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_getUserRoleResponseSnapShot", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.Add("@p_EmployeeID", SqlDbType.VarChar).Value = employeeId;
            oCommand.Parameters.Add("@p_UserRole", SqlDbType.VarChar).Value = userRole;
            oCommand.Parameters.Add("@p_CheckDate", SqlDbType.DateTime).Value = DateTime.Now.Date;
            oCommand.Parameters.Add("@p_ResponseCompanyCode", SqlDbType.VarChar).Value = companyCode;
            oCommand.Parameters.Add("@p_ResponseCode", SqlDbType.VarChar).Value = "";

            oConnection.Open();

            DataTable oTable = new DataTable("OrgUnitByRole");
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            oAdapter.Fill(oTable);

            if (oTable.Rows.Count > 0)
            {
                foreach (DataRow dr in oTable.Rows)
                {
                    OrgUnitEmployeeByRole org_unit = new OrgUnitEmployeeByRole();
                    org_unit.ParseToObject(dr);
                    list_org_unit.Add(org_unit);
                }
            }

            oAdapter.Dispose();
            oCommand.Dispose();
            oTable.Dispose();
            if (oConnection.State == ConnectionState.Open)
                oConnection.Close();

            return list_org_unit;
        }
        // �ͧ��� (�Ҩ���������)
        public override List<OrgUnitEmployeeByRole> GetListOrgUnitNameByUserRole(List<OrgUnitEmployeeByRole> list_org_unit)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);

            oConnection.Open();

            foreach (var item in list_org_unit)
            {
                string cmd = "SELECT TOP(1) info1000.ShortText,info1000.ShortTextEn FROM INFOTYPE1000 info1000 " +
                    "WHERE info1000.ObjectID = @p_orgUnit " +
                    "AND info1000.ObjectType = 'O' " +
                    "AND (@p_begin_date1 >= info1000.BeginDate AND @p_begin_date2 <= info1000.EndDate)";

                SqlCommand oCommand = new SqlCommand(cmd, oConnection);
                oCommand.CommandType = CommandType.Text;

                oCommand.Parameters.Clear();
                oCommand.Parameters.Add("@p_orgUnit", SqlDbType.VarChar).Value = item.OrgUnit;
                oCommand.Parameters.Add("@p_begin_date1", SqlDbType.DateTime).Value = item.BeginDate.Date;
                oCommand.Parameters.Add("@p_begin_date2", SqlDbType.DateTime).Value = item.BeginDate.Date;

                DataTable oTable = new DataTable("OrgUnitName");
                SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                oAdapter.Fill(oTable);

                if (oTable.Rows.Count > 0)
                {
                    item.OrgNameTH = oTable.Rows[0]["ShortText"].ToString();
                    item.OrgNameEN = oTable.Rows[0]["ShortTextEn"].ToString();
                }

                oCommand.Dispose();
                oAdapter.Dispose();
                oTable.Dispose();
            }


            if (oConnection.State == ConnectionState.Open)
                oConnection.Close();

            return list_org_unit;
        }

        public override List<OrgUnitTree> GetOrgOTUnitTree(string employeeId, DateTime begin_date, DateTime end_date)
        {
            var list_org_tree = new List<OrgUnitTree>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_GetOTOrgUnitTree", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            oCommand.Parameters.Add("@p_EmployeeID", SqlDbType.VarChar).Value = employeeId;
            oCommand.Parameters.Add("@p_BeginDate", SqlDbType.DateTime).Value = begin_date.Date;
            oCommand.Parameters.Add("@p_EndDate", SqlDbType.DateTime).Value = end_date.Date;

            oConnection.Open();

            DataTable oTable = new DataTable("OrgUnitByRole");
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            oAdapter.Fill(oTable);

            if (oTable.Rows.Count > 0)
            {
                foreach (DataRow dr in oTable.Rows)
                {
                    var org_tree = new OrgUnitTree();
                    org_tree.ParseToObject(dr);
                    list_org_tree.Add(org_tree);
                }
            }

            oAdapter.Dispose();
            oCommand.Dispose();
            oTable.Dispose();
            if (oConnection.State == ConnectionState.Open)
                oConnection.Close();

            return list_org_tree;
        }

        public override List<string> GetEmployeeInOrganization(string[] list_org)
        {
            List<string> list_emp = new List<string>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();

            DateTime date_now = DateTime.Now;
            DateTime date_begin = new DateTime(date_now.Year, 1, 1);
            foreach (string org_id in list_org)
            {
                SqlCommand oCommand = new SqlCommand("sp_EmployeeGetByOrganizationID", oConnection);
                oCommand.CommandType = CommandType.StoredProcedure;
                oCommand.Parameters.Clear();
                oCommand.Parameters.Add("@p_OrganizationID", SqlDbType.VarChar).Value = org_id;
                oCommand.Parameters.Add("@p_BeginDate", SqlDbType.DateTime).Value = date_begin.Date;
                oCommand.Parameters.Add("@p_EndDate", SqlDbType.DateTime).Value = date_now.Date;

                DataTable oTable = new DataTable("EmployeeId");
                SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                oAdapter.Fill(oTable);
                if (oTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in oTable.Rows)
                    {
                        list_emp.Add(dr["EmployeeID"].ToString());
                    }
                }
                oCommand.Dispose();
                oAdapter.Dispose();
            }

            if (oConnection.State == ConnectionState.Open)
            {
                oConnection.Dispose();
                oConnection.Close();
            }

            return list_emp;
        }

        public override List<string> GetEmployeeInOrganization(string orgId)
        {
            List<string> list_emp = new List<string>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();

            DateTime date_now = DateTime.Now;
            DateTime date_begin = new DateTime(date_now.Year, 1, 1);
            SqlCommand oCommand = new SqlCommand("sp_EmployeeGetByOrganizationID", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            oCommand.Parameters.Clear();
            oCommand.Parameters.Add("@p_OrganizationID", SqlDbType.VarChar).Value = orgId;
            oCommand.Parameters.Add("@p_BeginDate", SqlDbType.DateTime).Value = date_begin.Date;
            oCommand.Parameters.Add("@p_EndDate", SqlDbType.DateTime).Value = date_now.Date;

            DataTable oTable = new DataTable("EmployeeId");
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            oAdapter.Fill(oTable);
            if (oTable.Rows.Count > 0)
            {
                foreach (DataRow dr in oTable.Rows)
                {
                    list_emp.Add(dr["EmployeeID"].ToString());
                }
            }

            oCommand.Dispose();
            oAdapter.Dispose();
            oTable.Dispose();
            if (oConnection.State == ConnectionState.Open)
            {
                oConnection.Dispose();
                oConnection.Close();
            }

            return list_emp;
        }

        public override List<PaySlipEmployeeInOrganization> GetPaySlipEmployeeInOrganization(List<string> list_employee)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();

            var list_slip_emp = new List<PaySlipEmployeeInOrganization>();

            DateTime date_now = DateTime.Now;
            SqlCommand oCommand = new SqlCommand("sp_TM_GetPayslipByEmployeeInYear_MultiEmp", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

                oCommand.Parameters.Clear();
                oCommand.Parameters.Add("@p_EmployeeIDs", SqlDbType.VarChar).Value = string.Join(",", list_employee);
                oCommand.Parameters.Add("@p_PeriodYear", SqlDbType.Int).Value = date_now.Date.Year;

                DataTable oTable = new DataTable("PaySlipEmployee");
                SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                oAdapter.Fill(oTable);
                if (oTable.Rows.Count > 0)
                {
                    foreach (DataRow dr in oTable.Rows)
                    {
                        var paySlip = new PaySlipEmployeeInOrganization();
                        paySlip.ParseToObject(dr);

                        string data_key = this.KeySecret + paySlip.EmployeeID.Trim();

                        decimal amt;
                        if (!string.IsNullOrEmpty(dr["CurrencyAmount"].ToString()))
                        {
                            string amt_decryp = DecryptAes256(dr["CurrencyAmount"].ToString(), data_key);
                            amt = Convert.ToDecimal(amt_decryp);
                        }
                        else
                            amt = 0;
                        paySlip.Amount = amt;
                        list_slip_emp.Add(paySlip);
                    }
                }
                oCommand.Dispose();
                oAdapter.Dispose();

            oConnection.Dispose();
            if (oConnection.State == ConnectionState.Open)
                oConnection.Close();

            return list_slip_emp;
        }

        public override List<DbOTReason> GetDashbaordOTReason(List<string> list_emp, int year, int month)
        {
            var result = new List<DbOTReason>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();

            if (list_emp.Count > 0)
            {
                string typeStore = month == 0 ? "ALL" : "MONTH";

                SqlCommand oCommand = new SqlCommand("sp_TM_LoadOTReasonsDashboard_MultiEmp", oConnection);
                oCommand.CommandType = CommandType.StoredProcedure;

                    oCommand.Parameters.Clear();
                    oCommand.Parameters.Add("@p_EmployeeIDs", SqlDbType.VarChar).Value = string.Join(",", list_emp);
                    oCommand.Parameters.Add("@p_Year", SqlDbType.Int).Value = year;
                    oCommand.Parameters.Add("@p_Type", SqlDbType.VarChar).Value = typeStore;
                    if (typeStore == "MONTH")
                        oCommand.Parameters.Add("@p_Month", SqlDbType.Int).Value = month;

                    DataTable oTable = new DataTable("OTReasonByEmployee");
                    SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                    oAdapter.Fill(oTable);
                    if (oTable.Rows.Count > 0)
                    {
                        foreach (DataRow dr in oTable.Rows)
                        {
                            DbOTReason data = new DbOTReason();
                            data.ParseToObject(dr);
                            result.Add(data);
                        }
                    }
                    oCommand.Dispose();
                    oAdapter.Dispose();
                    oTable.Dispose();
                }
            
            oConnection.Dispose();
            if (oConnection.State == ConnectionState.Open)
                oConnection.Close();

            return result;
        }

        public override List<DbOTWorkType> GetListOTWorkType()
        {

            List<DbOTWorkType> list_result = new List<DbOTWorkType>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();

            string cmd = "SELECT * FROM OTWorkType WHERE IsActive = 1";
            SqlCommand oCommand = new SqlCommand(cmd, oConnection);
            oCommand.CommandType = CommandType.Text;

            DataTable oTable = new DataTable("OtWorkType");
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            oAdapter.Fill(oTable);

            if (oTable.Rows.Count > 0)
            {
                foreach (DataRow dr in oTable.Rows)
                {
                    var data = new DbOTWorkType();
                    data.ParseToObject(dr);
                    list_result.Add(data);
                }
            }

            oAdapter.Dispose();
            oTable.Dispose();
            oCommand.Dispose();

            if (oConnection.State == ConnectionState.Open)
                oConnection.Close();

            return list_result;
        }

        public override OrganizationInfotype1000 GetNameOrganizationInfotype1000(string orgId)
        {
            var data = new OrganizationInfotype1000();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();

            string cmd = "SELECT [ObjectType],[ObjectID],[BeginDate],[EndDate],[ShortText],[Text] " +
                "FROM [ESS_GPSC].[dbo].[INFOTYPE1000] " +
                "WHERE [ObjectID] = @p_ObjectID " +
                "AND @p_datetime >= [BeginDate] AND @p_datetime <= [EndDate] " +
                "AND [ObjectType] = 'O'";

            SqlCommand oCommand = new SqlCommand(cmd, oConnection);
            oCommand.CommandType = CommandType.Text;
            oCommand.Parameters.Add("@p_ObjectID", SqlDbType.VarChar).Value = orgId;
            oCommand.Parameters.Add("@p_datetime", SqlDbType.DateTime).Value = DateTime.Now.Date;

            DataTable oTable = new DataTable("DataOrganization");
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            oAdapter.Fill(oTable);

            if (oTable.Rows.Count > 0)
            {
                data.ParseToObject(oTable);
            }


            oAdapter.Dispose();
            oTable.Dispose();
            oCommand.Dispose();

            if (oConnection.State == ConnectionState.Open)
                oConnection.Close();

            return data;
        }

        public override List<DbOvertimeInMonth> GetTransactionPayOvertimeByEmployeeInOrganization(List<string> list_emp, int year, int month)
        {
            List<DbOvertimeInMonth> lis_ot = new List<DbOvertimeInMonth>();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();

            if (list_emp.Count > 0)
            {
                    SqlCommand oCommand = new SqlCommand("sp_TM_LoadOvertimebyBusinessUnitInThisMonthDashbaord_MultiEmp", oConnection);
                    oCommand.CommandType = CommandType.StoredProcedure;

                    oCommand.Parameters.Clear();
                    oCommand.Parameters.Add("@p_EmployeeIDs", SqlDbType.VarChar).Value = string.Join(",", list_emp);
                    oCommand.Parameters.Add("@p_Year", SqlDbType.Int).Value = year;
                    oCommand.Parameters.Add("@p_Month", SqlDbType.Int).Value = month;

                    DataTable oTable = new DataTable("CountTransactionPayOvertime");
                    SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                    oAdapter.Fill(oTable);

                    if (oTable.Rows.Count > 0)
                    {
                        foreach (DataRow dr in oTable.Rows)
                        {
                            var payslip = new DbOvertimeInMonth();
                            payslip.ParseToObject(dr);

                            string data_key = this.KeySecret + payslip.EmployeeID.Trim();

                            decimal amt;
                            if (!string.IsNullOrEmpty(dr["CurrencyAmount"].ToString()))
                            {
                                string amt_decryp = DecryptAes256(dr["CurrencyAmount"].ToString(), data_key);
                                amt = Convert.ToDecimal(amt_decryp);
                            }
                            else
                                amt = 0;

                            payslip.Amount = amt;
                            lis_ot.Add(payslip);

                        }

                    }
                    oCommand.Dispose();
                    oTable.Dispose();
                    oAdapter.Dispose();
                }
            
            oConnection.Dispose();
            if (oConnection.State == ConnectionState.Open)
                oConnection.Close();

            return lis_ot;
        }

        public override DashbaordOvertimeAndSalaryByMonth GetSalaryOrOvertimeInYear(List<string> list_emp, int year)
        {
            DashbaordOvertimeAndSalaryByMonth result = new DashbaordOvertimeAndSalaryByMonth();
            List<decimal> list_salary = new List<decimal>();
            List<decimal> list_overtime = new List<decimal>();
            List<decimal> list_proportion_ot = new List<decimal>();

            if (list_emp.Count > 0)
            {
                decimal sJan = 0, oJan = 0;
                decimal sFeb = 0, oFeb = 0;
                decimal sMar = 0, oMar = 0;
                decimal sApr = 0, oApr = 0;
                decimal sMay = 0, oMay = 0;
                decimal sJun = 0, oJun = 0;
                decimal sJul = 0, oJul = 0;
                decimal sAug = 0, oAug = 0;
                decimal sSep = 0, oSep = 0;
                decimal sOct = 0, oOct = 0;
                decimal sNov = 0, oNov = 0;
                decimal sDec = 0, oDec = 0;

                SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
                oConnection.Open();

                SqlCommand oCommand = new SqlCommand("sp_TM_LoadSalaryOvertimeInYear_MultiEmp", oConnection);
                oCommand.CommandType = CommandType.StoredProcedure;

                    oCommand.Parameters.Clear();
                    oCommand.Parameters.Add("@p_EmployeeIDs", SqlDbType.VarChar).Value = string.Join(",", list_emp);
                    oCommand.Parameters.Add("@p_PeriodYear", SqlDbType.Int).Value = year;

                    DataTable oTable = new DataTable("PaySlipEmployee");
                    SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                    oAdapter.Fill(oTable);

                    if (oTable.Rows.Count > 0)
                    {
                        foreach (DataRow dr in oTable.Rows)
                        {
                            DataPaySlipInYear paySlip = new DataPaySlipInYear();
                            paySlip.ParseToObject(dr);

                            if (paySlip.WageTypeCode == "1000" || paySlip.WageTypeCode == "2010" || paySlip.WageTypeCode == "2015"
                                || paySlip.WageTypeCode == "2030")
                            {

                                string data_key = this.KeySecret + paySlip.EmployeeID.Trim();

                                decimal amt = 0;

                                if (!string.IsNullOrEmpty(paySlip.CurrencyAmount))
                                {
                                    string amt_decryp = DecryptAes256(paySlip.CurrencyAmount, data_key);
                                    amt = Convert.ToDecimal(amt_decryp);
                                }
                                
                                switch (paySlip.PeriodMonth)
                                {
                                    case 1: if (paySlip.WageTypeCode == "1000") sJan += amt; else oJan += amt; break;
                                    case 2: if (paySlip.WageTypeCode == "1000") sFeb += amt; else oFeb += amt; break;
                                    case 3: if (paySlip.WageTypeCode == "1000") sMar += amt; else oMar += amt; break;
                                    case 4: if (paySlip.WageTypeCode == "1000") sApr += amt; else oApr += amt; break;
                                    case 5: if (paySlip.WageTypeCode == "1000") sMay += amt; else oMay += amt; break;
                                    case 6: if (paySlip.WageTypeCode == "1000") sJun += amt; else oJun += amt; break;
                                    case 7: if (paySlip.WageTypeCode == "1000") sJul += amt; else oJul += amt; break;
                                    case 8: if (paySlip.WageTypeCode == "1000") sAug += amt; else oAug += amt; break;
                                    case 9: if (paySlip.WageTypeCode == "1000") sSep += amt; else oSep += amt; break;
                                    case 10: if (paySlip.WageTypeCode == "1000") sOct += amt; else oOct += amt; break;
                                    case 11: if (paySlip.WageTypeCode == "1000") sNov += amt; else oNov += amt; break;
                                    case 12: if (paySlip.WageTypeCode == "1000") sDec += amt; else oDec += amt; break;
                                }
                            }
                        }
                    }
                    oCommand.Dispose();
                    oAdapter.Dispose();
                    oTable.Dispose();
                
                oConnection.Dispose();
                if (oConnection.State == ConnectionState.Open)
                    oConnection.Close();

                // ���Ѵ��ǹ % OT
                for (int i = 1; i <= 12; i++)
                {
                    decimal proportion_ot = 0;
                    switch (i)
                    {
                        case 1:
                            list_salary.Add(sJan);
                            list_overtime.Add(oJan);
                            if (sJan > 0)
                                proportion_ot = Math.Round((oJan / sJan) * 100, 2);
                            list_proportion_ot.Add(proportion_ot);
                            break;
                        case 2:
                            list_salary.Add(sFeb);
                            list_overtime.Add(oFeb);
                            if (sFeb > 0)
                                proportion_ot = Math.Round((oFeb / sFeb) * 100, 2);
                            list_proportion_ot.Add(proportion_ot);
                            break;
                        case 3:
                            list_salary.Add(sMar);
                            list_overtime.Add(oMar);
                            if (sMar > 0)
                                proportion_ot = Math.Round((oMar / sMar) * 100, 2);
                            list_proportion_ot.Add(proportion_ot);
                            break;
                        case 4:
                            list_salary.Add(sApr);
                            list_overtime.Add(oApr);
                            if (sApr > 0)
                                proportion_ot = Math.Round((oApr / sApr) * 100, 2);
                            list_proportion_ot.Add(proportion_ot);
                            break;
                        case 5:
                            list_salary.Add(sMay);
                            list_overtime.Add(oMay);
                            if (sMay > 0)
                                proportion_ot = Math.Round((oMay / sMay) * 100, 2);
                            list_proportion_ot.Add(proportion_ot);
                            break;
                        case 6:
                            list_salary.Add(sJun);
                            list_overtime.Add(oJun);
                            if (sJun > 0)
                                proportion_ot = Math.Round((oJun / sJun) * 100, 2);
                            list_proportion_ot.Add(proportion_ot);
                            break;
                        case 7:
                            list_salary.Add(sJul);
                            list_overtime.Add(oJul);
                            if (sJul > 0)
                                proportion_ot = Math.Round((oJul / sJul) * 100, 2);
                            list_proportion_ot.Add(proportion_ot);
                            break;
                        case 8:
                            list_salary.Add(sAug);
                            list_overtime.Add(oAug);
                            if (sAug > 0)
                                proportion_ot = Math.Round((oAug / sAug) * 100, 2);
                            list_proportion_ot.Add(proportion_ot);
                            break;
                        case 9:
                            list_salary.Add(sSep);
                            list_overtime.Add(oSep);
                            if (sSep > 0)
                                proportion_ot = Math.Round((oSep / sSep) * 100, 2);
                            list_proportion_ot.Add(proportion_ot);
                            break;
                        case 10:
                            list_salary.Add(sOct);
                            list_overtime.Add(oOct);
                            if (sOct > 0)
                                proportion_ot = Math.Round((oOct / sOct) * 100, 2);
                            list_proportion_ot.Add(proportion_ot);
                            break;
                        case 11:
                            list_salary.Add(sNov);
                            list_overtime.Add(oNov);
                            if (sNov > 0)
                                proportion_ot = Math.Round((oNov / sNov) * 100, 2);
                            list_proportion_ot.Add(proportion_ot);
                            break;
                        case 12:
                            list_salary.Add(sDec);
                            list_overtime.Add(oDec);
                            if (sDec > 0)
                                proportion_ot = Math.Round((oDec / sDec) * 100, 2);
                            list_proportion_ot.Add(proportion_ot);
                            break;
                    }
                }
            }

            if (list_salary.Count > 0)
                result.Salary = list_salary;

            if (list_overtime.Count > 0)
                result.Overtime = list_overtime;

            if (list_proportion_ot.Count > 0)
                result.ProportionOvertime = list_proportion_ot;

            return result;
        }

        public override DashbaordOTSalary6Months GetDashbaordOTSalary6Months(List<DateTime> list_date, List<string> list_emp)
        {
            var result = new DashbaordOTSalary6Months();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            foreach (DateTime date_time in list_date)
            {
                string txt_date = date_time.Date.ToString("MMMM yy", CultureInfo.CreateSpecificCulture("en-US"));
                result.Month.Add(txt_date);
                if (list_emp.Count > 0)
                {
                    decimal total_salaryByMonth = 0;
                    decimal total_overtimeByMonth = 0;
                    SqlCommand oCommand = new SqlCommand("sp_TM_LoadSummaryOTAndSalaryByMonth_MultiEmp", oConnection);
                    oCommand.CommandType = CommandType.StoredProcedure;

                        oCommand.Parameters.Clear();
                        oCommand.Parameters.Add("@p_EmployeeIDs", SqlDbType.VarChar).Value = string.Join(",", list_emp);
                        oCommand.Parameters.Add("@p_PeriodYear", SqlDbType.Int).Value = date_time.Date.Year;
                        oCommand.Parameters.Add("@p_PeriodMonth", SqlDbType.Int).Value = date_time.Date.Month;


                        DataTable oTable = new DataTable("PaySlipEmployee");
                        SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                        oAdapter.Fill(oTable);

                        if (oTable.Rows.Count > 0)
                        {
                            foreach (DataRow dr in oTable.Rows)
                            {
                                DataPaySlipInYear paySlip = new DataPaySlipInYear();
                                paySlip.ParseToObject(dr);

                                if (paySlip.WageTypeCode == "1000" || paySlip.WageTypeCode == "2010" || paySlip.WageTypeCode == "2015"
                                    || paySlip.WageTypeCode == "2030")
                                {

                                    string data_key = this.KeySecret + paySlip.EmployeeID.Trim();

                                    decimal amt = 0;
                                    if (!string.IsNullOrEmpty(paySlip.CurrencyAmount))
                                    {
                                        string amt_decryp = DecryptAes256(paySlip.CurrencyAmount, data_key);
                                        amt = Convert.ToDecimal(amt_decryp);
                                    }
                                    
                                    if (paySlip.WageTypeCode == "1000")
                                        total_salaryByMonth += amt;
                                    else
                                        total_overtimeByMonth += amt;
                                }
                            }
                        }
                    
                    // ���Ѵ��ǹ % �ͧ OT
                    decimal percentageRatio = 0;
                    if (total_salaryByMonth > 0)
                    {
                        percentageRatio = Math.Round((total_overtimeByMonth / total_salaryByMonth) * 100, 2);
                    }

                    result.Salary.Add(total_salaryByMonth);
                    result.Overtime.Add(total_overtimeByMonth);
                    result.ProportionOvertime.Add(percentageRatio);
                }
                else
                {
                    result.Overtime.Add(0);
                    result.ProportionOvertime.Add(0);
                    result.Salary.Add(0);
                }

            }

            oConnection.Dispose();
            if (oConnection.State == ConnectionState.Open)
                oConnection.Close();

            return result;
        }

        public override DashbaordOTHours6Months GetDashbaordOTHours6Months(List<DateTime> list_date, List<string> list_emp)
        {
            var result = new DashbaordOTHours6Months();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            foreach (DateTime date_time in list_date)
            {
                string txt_date = date_time.Date.ToString("MMMM yy", CultureInfo.CreateSpecificCulture("en-US"));
                result.Month.Add(txt_date);

                if (list_emp.Count > 0)
                {
                    decimal total_salaryByMonth = 0;
                    decimal total_overtimehourByMonth = 0;
                    decimal total_overtimeAmountByMonth = 0;
                    SqlCommand oCommand = new SqlCommand("sp_TM_LoadSummaryOTAndSalaryByMonth_MultiEmp", oConnection);
                    oCommand.CommandType = CommandType.StoredProcedure;

                        oCommand.Parameters.Clear();
                        oCommand.Parameters.Add("@p_EmployeeIDs", SqlDbType.VarChar).Value = string.Join(",", list_emp);
                        oCommand.Parameters.Add("@p_PeriodYear", SqlDbType.Int).Value = date_time.Date.Year;
                        oCommand.Parameters.Add("@p_PeriodMonth", SqlDbType.Int).Value = date_time.Date.Month;

                        // 1. ���Թ��͹��ѡ�ҹ������� org �����͹
                        DataTable oTable = new DataTable("PaySlipEmployee");
                        SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                        oAdapter.Fill(oTable);
                        if (oTable.Rows.Count > 0)
                        {
                            foreach (DataRow dr in oTable.Rows)
                            {
                                DataPaySlipInYear paySlip = new DataPaySlipInYear();
                                paySlip.ParseToObject(dr);

                                if (paySlip.WageTypeCode == "1000" || paySlip.WageTypeCode == "2010" || paySlip.WageTypeCode == "2015"
                                    || paySlip.WageTypeCode == "2030")
                                {
                                    string data_key = this.KeySecret + paySlip.EmployeeID.Trim();

                                    decimal amt = 0;
                                    if (!string.IsNullOrEmpty(paySlip.CurrencyAmount))
                                    {
                                        string amt_decryp = DecryptAes256(paySlip.CurrencyAmount, data_key);
                                        amt = Convert.ToDecimal(amt_decryp);
                                    }

                                    
                                    if (paySlip.WageTypeCode == "1000")
                                        total_salaryByMonth += amt;
                                    else
                                        total_overtimeAmountByMonth += amt;
                                }
                            }
                        }

                    // 2. �Ҩӹǹ �.�  (�����ǧ����)
                        oCommand = new SqlCommand("sp_TM_LoadSummaryOTHourByMonth_MultiEmp", oConnection);
                        oCommand.CommandType = CommandType.StoredProcedure;
                        oCommand.Parameters.Clear();

                        oCommand.Parameters.Add("@p_EmployeeIDs", SqlDbType.VarChar).Value = string.Join(",", list_emp);
                        oCommand.Parameters.Add("@p_PeriodYear", SqlDbType.Int).Value = date_time.Date.Year;
                        oCommand.Parameters.Add("@p_PeriodMonth", SqlDbType.Int).Value = date_time.Date.Month;

                        oTable = new DataTable("OTHour");
                        oAdapter = new SqlDataAdapter(oCommand);
                        oAdapter.Fill(oTable);
                        if (oTable.Rows.Count > 0)
                        {
                            total_overtimehourByMonth += Convert.ToDecimal(oTable.Rows[0]["TotalFinalOTHour"]);
                        }

                        oCommand.Dispose();
                        oAdapter.Dispose();
                        oTable.Dispose();                   

                    result.Salary.Add(total_salaryByMonth); // �Թ��͹
                    result.OvertimePay.Add(total_overtimeAmountByMonth); // �ӹǹ�Թ�����ǧ����

                    decimal ot_hour = Math.Round(total_overtimehourByMonth / 60, 2);
                    result.OvertimeHour.Add(ot_hour); // �ӹǹ ������������ǧ����
                }
                else
                {
                    result.OvertimePay.Add(0);
                    result.OvertimeHour.Add(0);
                    result.Salary.Add(0);
                }

            }

            oConnection.Dispose();
            if (oConnection.State == ConnectionState.Open)
                oConnection.Close();


            return result;
        }

        public override DashboardSummarySalaryOvertimePayByYear GetDashboardSummarySalaryOvertimePayByYear(List<string> list_emp, int begin_year, int end_year)
        {
            var dashboard = new DashboardSummarySalaryOvertimePayByYear();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();

            for (int year = begin_year; year <= end_year; year++)
            {
                decimal sum_salary_year = 0;
                decimal sum_ot_year = 0;
                if (list_emp.Count > 0)
                {
                    SqlCommand oCommand = new SqlCommand("sp_TM_LoadSalaryOvertimeInYear_MultiEmp", oConnection);
                    oCommand.CommandType = CommandType.StoredProcedure;

                        oCommand.Parameters.Clear();
                        oCommand.Parameters.Add("@p_EmployeeIDs", SqlDbType.VarChar).Value = string.Join(",", list_emp);
                        oCommand.Parameters.Add("@p_PeriodYear", SqlDbType.Int).Value = year;

                        DataTable oTable = new DataTable("PaySlipEmployee");
                        SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                        oAdapter.Fill(oTable);

                        if (oTable.Rows.Count > 0)
                        {
                            foreach (DataRow dr in oTable.Rows)
                            {
                                DataPaySlipInYear paySlip = new DataPaySlipInYear();
                                paySlip.ParseToObject(dr);

                                if (paySlip.WageTypeCode == "1000" || paySlip.WageTypeCode == "2010" || paySlip.WageTypeCode == "2015"
                                    || paySlip.WageTypeCode == "2030")
                                {
                                    string data_key = this.KeySecret + paySlip.EmployeeID.Trim();

                                    decimal amt = 0;
                                    if (!string.IsNullOrEmpty(paySlip.CurrencyAmount))
                                    {
                                        string amt_decryp = DecryptAes256(paySlip.CurrencyAmount, data_key);
                                        amt = Convert.ToDecimal(amt_decryp);
                                    }
                                    

                                    if (paySlip.WageTypeCode == "1000")
                                        sum_salary_year += amt;
                                    else
                                        sum_ot_year += amt;
                                }
                            }
                        }
                        oCommand.Dispose();
                        oAdapter.Dispose();
                        oTable.Dispose();
                    
                    dashboard.TotalYear.Add(year.ToString());
                    dashboard.TotalSalaryYear.Add(sum_salary_year);
                    dashboard.TotalOvertimePayYear.Add(sum_ot_year);
                    decimal ratio_ot = 0;
                    if (sum_salary_year > 0)
                    {
                        ratio_ot = Math.Round((sum_ot_year / sum_salary_year) * 100, 2);
                    }

                    dashboard.TotalRatioOvertimePayYear.Add(ratio_ot);
                }
                else
                {
                    dashboard.TotalYear.Add(year.ToString());
                    dashboard.TotalSalaryYear.Add(0);
                    dashboard.TotalRatioOvertimePayYear.Add(0);
                    dashboard.TotalOvertimePayYear.Add(0);
                }
            }

            oConnection.Dispose();
            if (oConnection.State == ConnectionState.Open)
                oConnection.Close();

            return dashboard;
        }

        public override DashboardSummarySalaryOvertimeHourByYear GetDashboardSummarySalaryOvertimeHourByYear(List<string> list_emp, int begin_year, int end_year)
        {
            var dashboard = new DashboardSummarySalaryOvertimeHourByYear();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();
            for (int year = begin_year; year <= end_year; year++)
            {
                decimal sum_salary_year = 0;
                decimal sum_ot_year = 0;
                decimal sum_ot_minute = 0;
                if (list_emp.Count > 0)
                {
                    SqlCommand oCommand = new SqlCommand("sp_TM_LoadSalaryOvertimeInYear_MultiEmp", oConnection);
                    oCommand.CommandType = CommandType.StoredProcedure;

                        oCommand.Parameters.Clear();
                        oCommand.Parameters.Add("@p_EmployeeIDs", SqlDbType.VarChar).Value = string.Join(",", list_emp);
                        oCommand.Parameters.Add("@p_PeriodYear", SqlDbType.Int).Value = year;

                        DataTable oTable = new DataTable("PaySlipEmployee");
                        SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                        oAdapter.Fill(oTable);

                        // 1. �Թ��͹��� OT
                        if (oTable.Rows.Count > 0)
                        {
                            foreach (DataRow dr in oTable.Rows)
                            {
                                DataPaySlipInYear paySlip = new DataPaySlipInYear();
                                paySlip.ParseToObject(dr);

                                if (paySlip.WageTypeCode == "1000" || paySlip.WageTypeCode == "2010" || paySlip.WageTypeCode == "2015"
                                    || paySlip.WageTypeCode == "2030")
                                {
                                    string data_key = this.KeySecret + paySlip.EmployeeID.Trim();

                                    decimal amt = 0;
                                    if (!string.IsNullOrEmpty(paySlip.CurrencyAmount))
                                    {
                                        string amt_decryp = DecryptAes256(paySlip.CurrencyAmount, data_key);
                                        amt = Convert.ToDecimal(amt_decryp);
                                    }
                                    

                                    if (paySlip.WageTypeCode == "1000")
                                        sum_salary_year += amt;
                                    else
                                        sum_ot_year += amt;
                                }
                            }
                        }


                    // 2. �Ҩӹǹ�ҷ� (�����ǧ����)
                        oCommand = new SqlCommand("sp_TM_LoadSummaryOTHourByYear_MultiEmp", oConnection);
                        oCommand.CommandType = CommandType.StoredProcedure;

                        oCommand.Parameters.Clear();
                        oCommand.Parameters.Add("@p_EmployeeIDs", SqlDbType.VarChar).Value = string.Join(",", list_emp);
                        oCommand.Parameters.Add("@p_PeriodYear", SqlDbType.Int).Value = year;

                        oTable = new DataTable("OTMinute");
                        oAdapter = new SqlDataAdapter(oCommand);
                        oAdapter.Fill(oTable);
                        if (oTable.Rows.Count > 0)
                        {
                            sum_ot_minute += Convert.ToDecimal(oTable.Rows[0]["TotalFinalOTHour"]);
                        }

                        oCommand.Dispose();
                        oAdapter.Dispose();
                        oTable.Dispose();
                    
                    dashboard.TotalYear.Add(year.ToString());
                    dashboard.TotalSalaryYear.Add(sum_salary_year);
                    dashboard.TotalOvertimePayYear.Add(sum_ot_year);

                    //3. �ӹǹ �.� OT
                    decimal ot_hour = Math.Round(sum_ot_minute / 60, 2);
                    dashboard.TotalOvertimeHourYear.Add(ot_hour);
                }
                else
                {
                    dashboard.TotalYear.Add(year.ToString());
                    dashboard.TotalSalaryYear.Add(0);
                    dashboard.TotalOvertimeHourYear.Add(0);
                    dashboard.TotalOvertimePayYear.Add(0);
                }
            }

            oConnection.Dispose();
            if (oConnection.State == ConnectionState.Open)
                oConnection.Close();

            return dashboard;

        }

        public override OTEmployeeByLevel GetDashboardOTEmployeeByLevel(List<string> list_emp, int begin_year, int end_year)
        {
            var dashboard = new OTEmployeeByLevel();

            var level_january = new BaseOTEmployeeLevel_January();
            var level_february = new BaseOTEmployeeLevel_February();
            var level_march = new BaseOTEmployeeLevel_March();
            var level_april = new BaseOTEmployeeLevel_April();
            var level_may = new BaseOTEmployeeLevel_May();
            var level_june = new BaseOTEmployeeLevel_June();
            var level_july = new BaseOTEmployeeLevel_July();
            var level_august = new BaseOTEmployeeLevel_August();
            var level_september = new BaseOTEmployeeLevel_September();
            var level_october = new BaseOTEmployeeLevel_October();
            var level_november = new BaseOTEmployeeLevel_November();
            var level_december = new BaseOTEmployeeLevel_December();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();

            // 1. ��觵���ӹǹ��
            for (int year = begin_year; year <= end_year; year++)
            {
                if (list_emp.Count > 0)
                {
                    // ����ӹǹ��ѡ�ҹ����ѧ�Ѵ org

                        bool count_month_1 = false, count_month_2 = false, count_month_3 = false, count_month_4 = false
                            , count_month_5 = false, count_month_6 = false, count_month_7 = false, count_month_8 = false
                            , count_month_9 = false, count_month_10 = false, count_month_11 = false, count_month_12 = false;

                    SqlCommand oCommand = new SqlCommand("sp_TM_LoadSummaryOTLevelEmployeeByYear_MultiEmp", oConnection);
                    oCommand.CommandType = CommandType.StoredProcedure;

                        oCommand.Parameters.Clear();
                        oCommand.Parameters.Add("@p_EmployeeIDs", SqlDbType.VarChar).Value = string.Join(",", list_emp);
                        oCommand.Parameters.Add("@p_BeginDate", SqlDbType.Int).Value = year;

                        DataTable oTable = new DataTable("EmployeeOTLog");
                        SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                        oAdapter.Fill(oTable);

                        if (oTable.Rows.Count > 0)
                        {
                            foreach (DataRow dr in oTable.Rows)
                            {
                                var ot_level_emp = new dbOTEmployeeByLevel();
                                ot_level_emp.ParseToObject(dr);

                                int subgroup;
                                bool isParsable = Int32.TryParse(ot_level_emp.EmpSubGroup, out subgroup);
                                if (isParsable)
                                {
                                    switch (ot_level_emp.BeginDate.Month)
                                    {
                                        case 1:
                                            if (count_month_1 == false)
                                            {
                                                int level = SearchLevel(subgroup);
                                                if (level == 1)
                                                    level_january.Level1To2++;
                                                if (level == 2)
                                                    level_january.Level3To8++;
                                                //if (level == 3)
                                                //    level_january.Level5To8++;
                                                if (level == 3)
                                                    level_january.Level9To10++;

                                                count_month_1 = true;
                                            }
                                            break;
                                        case 2:
                                            if (count_month_2 == false)
                                            {
                                                int level = SearchLevel(subgroup);
                                                if (level == 1)
                                                    level_february.Level1To2++;
                                                if (level == 2)
                                                    level_february.Level3To8++;
                                                //if (level == 3)
                                                //    level_february.Level5To8++;
                                                if (level == 3)
                                                    level_february.Level9To10++;

                                                count_month_2 = true;
                                            }
                                            break;
                                        case 3:
                                            if (count_month_3 == false)
                                            {
                                                int level = SearchLevel(subgroup);
                                                if (level == 1)
                                                    level_march.Level1To2++;
                                                if (level == 2)
                                                    level_march.Level3To8++;
                                                //if (level == 3)
                                                //    level_march.Level5To8++;
                                                if (level == 3)
                                                    level_march.Level9To10++;

                                                count_month_3 = true;
                                            }
                                            break;
                                        case 4:
                                            if (count_month_4 == false)
                                            {
                                                int level = SearchLevel(subgroup);
                                                if (level == 1)
                                                    level_april.Level1To2++;
                                                if (level == 2)
                                                    level_april.Level3To8++;
                                                //if (level == 3)
                                                //    level_april.Level5To8++;
                                                if (level == 3)
                                                    level_april.Level9To10++;

                                                count_month_4 = true;
                                            }
                                            break;
                                        case 5:
                                            if (count_month_5 == false)
                                            {
                                                int level = SearchLevel(subgroup);
                                                if (level == 1)
                                                    level_may.Level1To2++;
                                                if (level == 2)
                                                    level_may.Level3To8++;
                                                //if (level == 3)
                                                //    level_may.Level5To8++;
                                                if (level == 3)
                                                    level_may.Level9To10++;

                                                count_month_5 = true;
                                            }
                                            break;
                                        case 6:
                                            if (count_month_6 == false)
                                            {
                                                int level = SearchLevel(subgroup);
                                                if (level == 1)
                                                    level_june.Level1To2++;
                                                if (level == 2)
                                                    level_june.Level3To8++;
                                                //if (level == 3)
                                                //    level_june.Level5To8++;
                                                if (level == 3)
                                                    level_june.Level9To10++;

                                                count_month_6 = true;
                                            }
                                            break;
                                        case 7:
                                            if (count_month_7 == false)
                                            {
                                                int level = SearchLevel(subgroup);
                                                if (level == 1)
                                                    level_july.Level1To2++;
                                                if (level == 2)
                                                    level_july.Level3To8++;
                                                //if (level == 3)
                                                //    level_july.Level5To8++;
                                                if (level == 3)
                                                    level_july.Level9To10++;

                                                count_month_7 = true;
                                            }
                                            break;
                                        case 8:
                                            if (count_month_8 == false)
                                            {
                                                int level = SearchLevel(subgroup);
                                                if (level == 1)
                                                    level_august.Level1To2++;
                                                if (level == 2)
                                                    level_august.Level3To8++;
                                                //if (level == 3)
                                                //    level_august.Level5To8++;
                                                if (level == 3)
                                                    level_august.Level9To10++;

                                                count_month_8 = true;
                                            }
                                            break;
                                        case 9:
                                            if (count_month_9 == false)
                                            {
                                                int level = SearchLevel(subgroup);
                                                if (level == 1)
                                                    level_september.Level1To2++;
                                                if (level == 2)
                                                    level_september.Level3To8++;
                                                //if (level == 3)
                                                //    level_september.Level5To8++;
                                                if (level == 3)
                                                    level_september.Level9To10++;

                                                count_month_9 = true;
                                            }
                                            break;
                                        case 10:
                                            if (count_month_10 == false)
                                            {
                                                int level = SearchLevel(subgroup);
                                                if (level == 1)
                                                    level_october.Level1To2++;
                                                if (level == 2)
                                                    level_october.Level3To8++;
                                                //if (level == 3)
                                                //    level_october.Level5To8++;
                                                if (level == 3)
                                                    level_october.Level9To10++;

                                                count_month_10 = true;
                                            }
                                            break;
                                        case 11:
                                            if (count_month_11 == false)
                                            {
                                                int level = SearchLevel(subgroup);
                                                if (level == 1)
                                                    level_november.Level1To2++;
                                                if (level == 2)
                                                    level_november.Level3To8++;
                                                //if (level == 3)
                                                //    level_november.Level5To8++;
                                                if (level == 3)
                                                    level_november.Level9To10++;

                                                count_month_11 = true;
                                            }
                                            break;
                                        case 12:
                                            if (count_month_12 == false)
                                            {
                                                int level = SearchLevel(subgroup);
                                                if (level == 1)
                                                    level_december.Level1To2++;
                                                if (level == 2)
                                                    level_december.Level3To8++;
                                                //if (level == 3)
                                                //    level_december.Level5To8++;
                                                if (level == 3)
                                                    level_december.Level9To10++;

                                                count_month_12 = true;
                                            }
                                            break;
                                    }
                                }
                            }
                        }

                        oCommand.Dispose();
                        oAdapter.Dispose();
                        oTable.Dispose();
                    }
                }
            
            oConnection.Dispose();
            if (oConnection.State == ConnectionState.Open)
                oConnection.Close();

            // 2. �Ѵ Model �ӹǹ��ѡ�ҹ ��� Percentage �ͧ������͹
            for (int month = 1; month <= 12; month++)
            {
                decimal total_emp = 0;
                decimal percentage_1to2 = 0;
                //decimal percentage_3to4 = 0;
                decimal percentage_3to8 = 0;
                decimal percentage_9to10 = 0;

                if (month == 1)
                {
                    total_emp = level_january.Level1To2 + level_january.Level3To8 + level_january.Level9To10;
                    if (total_emp > 0)
                    {
                        percentage_1to2 = Math.Round((level_january.Level1To2 * 100) / total_emp, 2);
                        percentage_3to8 = Math.Round((level_january.Level3To8 * 100) / total_emp, 2);
                        percentage_9to10 = Math.Round((level_january.Level9To10 * 100) / total_emp, 2);

                        // 3. % �Թ 100 ���͹��¡��� 100 ������� 100 �ʹ�
                        decimal total_percentage = percentage_1to2 + percentage_3to8 + percentage_9to10;
                        if (total_percentage > 0 && total_percentage != 100)
                        {
                            decimal max = 0;
                            if (percentage_1to2 > max)
                                max = percentage_1to2;

                            if (percentage_3to8 > max)
                                max = percentage_3to8;

                            if (percentage_9to10 > max)
                                max = percentage_9to10;

                            if (total_percentage > 100)
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                            else
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                        }
                    }

                    dashboard.EmployeeLevel1To2.Add((int)level_january.Level1To2);
                    dashboard.EmployeeLevel3To8.Add((int)level_january.Level3To8);
                    dashboard.EmployeeLevel9To10.Add((int)level_january.Level9To10);

                    dashboard.PercentageEmployeeLevel1To2.Add(percentage_1to2);
                    dashboard.PercentageEmployeeLevel3To8.Add(percentage_3to8);
                    dashboard.PercentageEmployeeLevel9To10.Add(percentage_9to10);
                }

                if (month == 2)
                {
                    total_emp = level_february.Level1To2 +
                        level_february.Level3To8 +
                        level_february.Level9To10;

                    if (total_emp > 0)
                    {
                        percentage_1to2 = Math.Round((level_february.Level1To2 * 100) / total_emp, 2);
                        percentage_3to8 = Math.Round((level_february.Level3To8 * 100) / total_emp, 2);
                        percentage_9to10 = Math.Round((level_february.Level9To10 * 100) / total_emp, 2);

                        // 3. % �Թ 100 ���͹��¡��� 100 ������� 100 �ʹ�
                        decimal total_percentage = percentage_1to2 + percentage_3to8 + percentage_9to10;
                        if (total_percentage > 0 && total_percentage != 100)
                        {
                            decimal max = 0;
                            if (percentage_1to2 > max)
                                max = percentage_1to2;
                            if (percentage_3to8 > max)
                                max = percentage_3to8;
                            if (percentage_9to10 > max)
                                max = percentage_9to10;

                            if (total_percentage > 100)
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                            else
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                        }
                    }

                    dashboard.EmployeeLevel1To2.Add((int)level_february.Level1To2);
                    dashboard.EmployeeLevel3To8.Add((int)level_february.Level3To8);
                    dashboard.EmployeeLevel9To10.Add((int)level_february.Level9To10);

                    dashboard.PercentageEmployeeLevel1To2.Add(percentage_1to2);
                    dashboard.PercentageEmployeeLevel3To8.Add(percentage_3to8);
                    dashboard.PercentageEmployeeLevel9To10.Add(percentage_9to10);

                }

                if (month == 3)
                {
                    total_emp = level_march.Level1To2 +
                        level_march.Level3To8 +
                        level_march.Level9To10;
                    if (total_emp > 0)
                    {
                        percentage_1to2 = Math.Round((level_march.Level1To2 * 100) / total_emp, 2);
                        percentage_3to8 = Math.Round((level_march.Level3To8 * 100) / total_emp, 2);
                        percentage_9to10 = Math.Round((level_march.Level9To10 * 100) / total_emp, 2);

                        // 3. % �Թ 100 ���͹��¡��� 100 ������� 100 �ʹ�
                        decimal total_percentage = percentage_1to2
                            + percentage_3to8
                            + percentage_9to10;
                        if (total_percentage > 0 && total_percentage != 100)
                        {
                            decimal max = 0;
                            if (percentage_1to2 > max)
                                max = percentage_1to2;
                            if (percentage_3to8 > max)
                                max = percentage_3to8;
                            if (percentage_9to10 > max)
                                max = percentage_9to10;

                            if (total_percentage > 100)
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                            else
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                        }
                    }

                    dashboard.EmployeeLevel1To2.Add((int)level_march.Level1To2);
                    dashboard.EmployeeLevel3To8.Add((int)level_march.Level3To8);
                    dashboard.EmployeeLevel9To10.Add((int)level_march.Level9To10);

                    dashboard.PercentageEmployeeLevel1To2.Add(percentage_1to2);
                    dashboard.PercentageEmployeeLevel3To8.Add(percentage_3to8);
                    dashboard.PercentageEmployeeLevel9To10.Add(percentage_9to10);
                }

                if (month == 4)
                {
                    total_emp = level_april.Level1To2 +
                        level_april.Level3To8 +
                        level_april.Level9To10;

                    if (total_emp > 0)
                    {
                        percentage_1to2 = Math.Round((level_april.Level1To2 * 100) / total_emp, 2);
                        percentage_3to8 = Math.Round((level_april.Level3To8 * 100) / total_emp, 2);
                        percentage_9to10 = Math.Round((level_april.Level9To10 * 100) / total_emp, 2);

                        // 3. % �Թ 100 ���͹��¡��� 100 ������� 100 �ʹ�
                        decimal total_percentage = percentage_1to2 + percentage_3to8 + percentage_9to10;
                        if (total_percentage > 0 && total_percentage != 100)
                        {
                            decimal max = 0;
                            if (percentage_1to2 > max)
                                max = percentage_1to2;
                            if (percentage_3to8 > max)
                                max = percentage_3to8;
                            if (percentage_9to10 > max)
                                max = percentage_9to10;

                            if (total_percentage > 100)
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                            else
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                        }
                    }

                    dashboard.EmployeeLevel1To2.Add((int)level_april.Level1To2);
                    dashboard.EmployeeLevel3To8.Add((int)level_april.Level3To8);
                    dashboard.EmployeeLevel9To10.Add((int)level_april.Level9To10);

                    dashboard.PercentageEmployeeLevel1To2.Add(percentage_1to2);
                    dashboard.PercentageEmployeeLevel3To8.Add(percentage_3to8);
                    dashboard.PercentageEmployeeLevel9To10.Add(percentage_9to10);
                }

                if (month == 5)
                {
                    total_emp = level_may.Level1To2 +
                        level_may.Level3To8 +
                        level_may.Level9To10;

                    if (total_emp > 0)
                    {
                        percentage_1to2 = Math.Round((level_may.Level1To2 * 100) / total_emp, 2);
                        percentage_3to8 = Math.Round((level_may.Level3To8 * 100) / total_emp, 2);
                        percentage_9to10 = Math.Round((level_may.Level9To10 * 100) / total_emp, 2);

                        // 3. % �Թ 100 ���͹��¡��� 100 ������� 100 �ʹ�
                        decimal total_percentage = percentage_1to2 + percentage_3to8 + percentage_9to10;
                        if (total_percentage > 0 && total_percentage != 100)
                        {
                            decimal max = 0;
                            if (percentage_1to2 > max)
                                max = percentage_1to2;
                            if (percentage_3to8 > max)
                                max = percentage_3to8;
                            if (percentage_9to10 > max)
                                max = percentage_9to10;

                            if (total_percentage > 100)
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                            else
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                        }
                    }

                    dashboard.EmployeeLevel1To2.Add((int)level_may.Level1To2);
                    dashboard.EmployeeLevel3To8.Add((int)level_may.Level3To8);
                    dashboard.EmployeeLevel9To10.Add((int)level_may.Level9To10);

                    dashboard.PercentageEmployeeLevel1To2.Add(percentage_1to2);
                    dashboard.PercentageEmployeeLevel3To8.Add(percentage_3to8);
                    dashboard.PercentageEmployeeLevel9To10.Add(percentage_9to10);
                }

                if (month == 6)
                {
                    total_emp = level_june.Level1To2 +
                        level_june.Level3To8 +
                        level_june.Level9To10;

                    if (total_emp > 0)
                    {
                        percentage_1to2 = Math.Round((level_june.Level1To2 * 100) / total_emp, 2);
                        percentage_3to8 = Math.Round((level_june.Level3To8 * 100) / total_emp, 2);
                        percentage_9to10 = Math.Round((level_june.Level9To10 * 100) / total_emp, 2);

                        // 3. % �Թ 100 ���͹��¡��� 100 ������� 100 �ʹ�
                        decimal total_percentage = percentage_1to2 + percentage_3to8 + percentage_9to10;
                        if (total_percentage > 0 && total_percentage != 100)
                        {
                            decimal max = 0;
                            if (percentage_1to2 > max)
                                max = percentage_1to2;
                            if (percentage_3to8 > max)
                                max = percentage_3to8;
                            if (percentage_9to10 > max)
                                max = percentage_9to10;

                            if (total_percentage > 100)
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                            else
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                        }
                    }

                    dashboard.EmployeeLevel1To2.Add((int)level_june.Level1To2);
                    dashboard.EmployeeLevel3To8.Add((int)level_june.Level3To8);
                    dashboard.EmployeeLevel9To10.Add((int)level_june.Level9To10);

                    dashboard.PercentageEmployeeLevel1To2.Add(percentage_1to2);
                    dashboard.PercentageEmployeeLevel3To8.Add(percentage_3to8);
                    dashboard.PercentageEmployeeLevel9To10.Add(percentage_9to10);
                }

                if (month == 7)
                {
                    total_emp = level_july.Level1To2 +
                        level_july.Level3To8 +
                        level_july.Level9To10;

                    if (total_emp > 0)
                    {
                        percentage_1to2 = Math.Round((level_july.Level1To2 * 100) / total_emp, 2);
                        percentage_3to8 = Math.Round((level_july.Level3To8 * 100) / total_emp, 2);
                        percentage_9to10 = Math.Round((level_july.Level9To10 * 100) / total_emp, 2);

                        // 3. % �Թ 100 ���͹��¡��� 100 ������� 100 �ʹ�
                        decimal total_percentage = percentage_1to2 + percentage_3to8 + percentage_9to10;
                        if (total_percentage > 0 && total_percentage != 100)
                        {
                            decimal max = 0;
                            if (percentage_1to2 > max)
                                max = percentage_1to2;
                            if (percentage_3to8 > max)
                                max = percentage_3to8;
                            if (percentage_9to10 > max)
                                max = percentage_9to10;

                            if (total_percentage > 100)
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                            else
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                        }
                    }

                    dashboard.EmployeeLevel1To2.Add((int)level_july.Level1To2);
                    dashboard.EmployeeLevel3To8.Add((int)level_july.Level3To8);
                    dashboard.EmployeeLevel9To10.Add((int)level_july.Level9To10);

                    dashboard.PercentageEmployeeLevel1To2.Add(percentage_1to2);
                    dashboard.PercentageEmployeeLevel3To8.Add(percentage_3to8);
                    dashboard.PercentageEmployeeLevel9To10.Add(percentage_9to10);
                }

                if (month == 8)
                {
                    total_emp = level_august.Level1To2 +
                        level_august.Level3To8 +
                        level_august.Level9To10;
                    if (total_emp > 0)
                    {
                        percentage_1to2 = Math.Round((level_august.Level1To2 * 100) / total_emp, 2);
                        percentage_3to8 = Math.Round((level_august.Level3To8 * 100) / total_emp, 2);
                        percentage_9to10 = Math.Round((level_august.Level9To10 * 100) / total_emp, 2);

                        // 3. % �Թ 100 ���͹��¡��� 100 ������� 100 �ʹ�
                        decimal total_percentage = percentage_1to2 + percentage_3to8 + percentage_9to10;
                        if (total_percentage > 0 && total_percentage != 100)
                        {
                            decimal max = 0;
                            if (percentage_1to2 > max)
                                max = percentage_1to2;
                            if (percentage_3to8 > max)
                                max = percentage_3to8;
                            if (percentage_9to10 > max)
                                max = percentage_9to10;

                            if (total_percentage > 100)
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                            else
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                        }
                    }

                    dashboard.EmployeeLevel1To2.Add((int)level_august.Level1To2);
                    dashboard.EmployeeLevel3To8.Add((int)level_august.Level3To8);
                    dashboard.EmployeeLevel9To10.Add((int)level_august.Level9To10);

                    dashboard.PercentageEmployeeLevel1To2.Add(percentage_1to2);
                    dashboard.PercentageEmployeeLevel3To8.Add(percentage_3to8);
                    dashboard.PercentageEmployeeLevel9To10.Add(percentage_9to10);
                }

                if (month == 9)
                {
                    total_emp = level_september.Level1To2 +
                        level_september.Level3To8 +
                        level_september.Level9To10;
                    if (total_emp > 0)
                    {
                        percentage_1to2 = Math.Round((level_september.Level1To2 * 100) / total_emp, 2);
                        percentage_3to8 = Math.Round((level_september.Level3To8 * 100) / total_emp, 2);
                        percentage_9to10 = Math.Round((level_september.Level9To10 * 100) / total_emp, 2);

                        // 3. % �Թ 100 ���͹��¡��� 100 ������� 100 �ʹ�
                        decimal total_percentage = percentage_1to2 + percentage_3to8 + percentage_9to10;
                        if (total_percentage > 0 && total_percentage != 100)
                        {
                            decimal max = 0;
                            if (percentage_1to2 > max)
                                max = percentage_1to2;
                            if (percentage_3to8 > max)
                                max = percentage_3to8;
                            if (percentage_9to10 > max)
                                max = percentage_9to10;

                            if (total_percentage > 100)
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                            else
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                        }
                    }

                    dashboard.EmployeeLevel1To2.Add((int)level_september.Level1To2);
                    dashboard.EmployeeLevel3To8.Add((int)level_september.Level3To8);
                    dashboard.EmployeeLevel9To10.Add((int)level_september.Level9To10);

                    dashboard.PercentageEmployeeLevel1To2.Add(percentage_1to2);
                    dashboard.PercentageEmployeeLevel3To8.Add(percentage_3to8);
                    dashboard.PercentageEmployeeLevel9To10.Add(percentage_9to10);
                }

                if (month == 10)
                {
                    total_emp = level_october.Level1To2 +
                        level_october.Level3To8 +
                        level_october.Level9To10;
                    if (total_emp > 0)
                    {
                        percentage_1to2 = Math.Round((level_october.Level1To2 * 100) / total_emp, 2);
                        percentage_3to8 = Math.Round((level_october.Level3To8 * 100) / total_emp, 2);
                        percentage_9to10 = Math.Round((level_october.Level9To10 * 100) / total_emp, 2);

                        // 3. % �Թ 100 ���͹��¡��� 100 ������� 100 �ʹ�
                        decimal total_percentage = percentage_1to2 + percentage_3to8 + percentage_9to10;
                        if (total_percentage > 0 && total_percentage != 100)
                        {
                            decimal max = 0;
                            if (percentage_1to2 > max)
                                max = percentage_1to2;
                            if (percentage_3to8 > max)
                                max = percentage_3to8;
                            if (percentage_9to10 > max)
                                max = percentage_9to10;

                            if (total_percentage > 100)
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                            else
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                        }
                    }

                    dashboard.EmployeeLevel1To2.Add((int)level_october.Level1To2);
                    dashboard.EmployeeLevel3To8.Add((int)level_october.Level3To8);
                    dashboard.EmployeeLevel9To10.Add((int)level_october.Level9To10);

                    dashboard.PercentageEmployeeLevel1To2.Add(percentage_1to2);
                    dashboard.PercentageEmployeeLevel3To8.Add(percentage_3to8);
                    dashboard.PercentageEmployeeLevel9To10.Add(percentage_9to10);
                }

                if (month == 11)
                {
                    total_emp = level_november.Level1To2 +
                        level_november.Level3To8 +
                        level_november.Level9To10;
                    if (total_emp > 0)
                    {
                        percentage_1to2 = Math.Round((level_november.Level1To2 * 100) / total_emp, 2);
                        percentage_3to8 = Math.Round((level_november.Level3To8 * 100) / total_emp, 2);
                        percentage_9to10 = Math.Round((level_november.Level9To10 * 100) / total_emp, 2);

                        // 3. % �Թ 100 ���͹��¡��� 100 ������� 100 �ʹ�
                        decimal total_percentage = percentage_1to2 + percentage_3to8 + percentage_9to10;
                        if (total_percentage > 0 && total_percentage != 100)
                        {
                            decimal max = 0;
                            if (percentage_1to2 > max)
                                max = percentage_1to2;
                            if (percentage_3to8 > max)
                                max = percentage_3to8;
                            if (percentage_9to10 > max)
                                max = percentage_9to10;

                            if (total_percentage > 100)
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                            else
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                        }
                    }

                    dashboard.EmployeeLevel1To2.Add((int)level_november.Level1To2);
                    dashboard.EmployeeLevel3To8.Add((int)level_november.Level3To8);
                    dashboard.EmployeeLevel9To10.Add((int)level_november.Level9To10);

                    dashboard.PercentageEmployeeLevel1To2.Add(percentage_1to2);
                    dashboard.PercentageEmployeeLevel3To8.Add(percentage_3to8);
                    dashboard.PercentageEmployeeLevel9To10.Add(percentage_9to10);
                }

                if (month == 12)
                {
                    total_emp = level_december.Level1To2 +
                        level_december.Level3To8 +
                        level_december.Level9To10;
                    if (total_emp > 0)
                    {
                        percentage_1to2 = Math.Round((level_december.Level1To2 * 100) / total_emp, 2);
                        percentage_3to8 = Math.Round((level_december.Level3To8 * 100) / total_emp, 2);
                        percentage_9to10 = Math.Round((level_december.Level9To10 * 100) / total_emp, 2);

                        // 3. % �Թ 100 ���͹��¡��� 100 ������� 100 �ʹ�
                        decimal total_percentage = percentage_1to2 + percentage_3to8 + percentage_9to10;
                        if (total_percentage > 0 && total_percentage != 100)
                        {
                            decimal max = 0;
                            if (percentage_1to2 > max)
                                max = percentage_1to2;
                            if (percentage_3to8 > max)
                                max = percentage_3to8;
                            if (percentage_9to10 > max)
                                max = percentage_9to10;

                            if (total_percentage > 100)
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                            else
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                        }
                    }

                    dashboard.EmployeeLevel1To2.Add((int)level_december.Level1To2);
                    dashboard.EmployeeLevel3To8.Add((int)level_december.Level3To8);
                    dashboard.EmployeeLevel9To10.Add((int)level_december.Level9To10);

                    dashboard.PercentageEmployeeLevel1To2.Add(percentage_1to2);
                    dashboard.PercentageEmployeeLevel3To8.Add(percentage_3to8);
                    dashboard.PercentageEmployeeLevel9To10.Add(percentage_9to10);
                }
            }

            return dashboard;
        }

        public override OTEmployeeByBath GetDashboardOTEmployeeByBath(List<string> list_emp, int begin_year, int end_year)
        {
            var dashboard = new OTEmployeeByBath();

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();

            var bath_january = new BaseOTEmployeeLevel_January();
            var bath_february = new BaseOTEmployeeLevel_February();
            var bath_march = new BaseOTEmployeeLevel_March();
            var bath_april = new BaseOTEmployeeLevel_April();
            var bath_may = new BaseOTEmployeeLevel_May();
            var bath_june = new BaseOTEmployeeLevel_June();
            var bath_july = new BaseOTEmployeeLevel_July();
            var bath_august = new BaseOTEmployeeLevel_August();
            var bath_september = new BaseOTEmployeeLevel_September();
            var bath_october = new BaseOTEmployeeLevel_October();
            var bath_november = new BaseOTEmployeeLevel_November();
            var bath_december = new BaseOTEmployeeLevel_December();

            for (int year = begin_year; year <= end_year; year++)
            {
                if (list_emp.Count > 0)
                {

                    SqlCommand oCommand = new SqlCommand("sp_TM_LoadSummaryOTBathEmployeeByYear_MultiEmp", oConnection);
                    oCommand.CommandType = CommandType.StoredProcedure;

                        oCommand.Parameters.Clear();
                        oCommand.Parameters.Add("@p_EmployeeIDs", SqlDbType.VarChar).Value = string.Join(",", list_emp);
                        oCommand.Parameters.Add("@p_PeriodYear", SqlDbType.Int).Value = year;

                        DataTable oTable = new DataTable("EmployeeOTBath");
                        SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                        oAdapter.Fill(oTable);

                        if (oTable.Rows.Count > 0)
                        {
                            foreach (DataRow item in oTable.Rows)
                            {
                                dbOTEmployeeByBath data = new dbOTEmployeeByBath();
                                data.ParseToObject(item);

                                int subgroup;
                                bool isParsable = Int32.TryParse(data.EmpSubGroup, out subgroup);
                                if (isParsable)
                                {
                                    string data_key = this.KeySecret + data.EmployeeID.Trim();

                                    decimal amt = 0;
                                    if (!string.IsNullOrEmpty(data.CurrencyAmount))
                                    {
                                        string amt_decryp = DecryptAes256(data.CurrencyAmount, data_key);
                                         amt = Convert.ToDecimal(amt_decryp);
                                    }

                                    switch (data.PeriodMonth)
                                    {
                                        case 1:
                                            int level = SearchLevel(subgroup);
                                            if (level == 1)
                                                bath_january.Bath1To2 += amt;
                                            if (level == 2)
                                                bath_january.Bath3To8 += amt;
                                            if (level == 3)
                                                bath_january.Bath9To10 += amt;
                                            break;
                                        case 2:
                                            level = SearchLevel(subgroup);
                                            if (level == 1)
                                                bath_february.Bath1To2 += amt;
                                            if (level == 2)
                                                bath_february.Bath3To8 += amt;
                                            if (level == 3)
                                                bath_february.Bath9To10 += amt;
                                            break;
                                        case 3:
                                            level = SearchLevel(subgroup);
                                            if (level == 1)
                                                bath_march.Bath1To2 += amt;
                                            if (level == 2)
                                                bath_march.Bath3To8 += amt;
                                            if (level == 3)
                                                bath_march.Bath9To10 += amt;
                                            break;
                                        case 4:
                                            level = SearchLevel(subgroup);
                                            if (level == 1)
                                                bath_april.Bath1To2 += amt;
                                            if (level == 2)
                                                bath_april.Bath3To8 += amt;
                                            if (level == 3)
                                                bath_april.Bath9To10 += amt;
                                            break;
                                        case 5:
                                            level = SearchLevel(subgroup);
                                            if (level == 1)
                                                bath_may.Bath1To2 += amt;
                                            if (level == 2)
                                                bath_may.Bath3To8 += amt;
                                            if (level == 3)
                                                bath_may.Bath9To10 += amt;
                                            break;
                                        case 6:
                                            level = SearchLevel(subgroup);
                                            if (level == 1)
                                                bath_june.Bath1To2 += amt;
                                            if (level == 2)
                                                bath_june.Bath3To8 += amt;
                                            if (level == 3)
                                                bath_june.Bath9To10 += amt;
                                            break;
                                        case 7:
                                            level = SearchLevel(subgroup);
                                            if (level == 1)
                                                bath_july.Bath1To2 += amt;
                                            if (level == 2)
                                                bath_july.Bath3To8 += amt;
                                            if (level == 3)
                                                bath_july.Bath9To10 += amt;
                                            break;
                                        case 8:
                                            level = SearchLevel(subgroup);
                                            if (level == 1)
                                                bath_august.Bath1To2 += amt;
                                            if (level == 2)
                                                bath_august.Bath3To8 += amt;
                                            if (level == 3)
                                                bath_august.Bath9To10 += amt;
                                            break;
                                        case 9:
                                            level = SearchLevel(subgroup);
                                            if (level == 1)
                                                bath_september.Bath1To2 += amt;
                                            if (level == 2)
                                                bath_september.Bath3To8 += amt;
                                            if (level == 3)
                                                bath_september.Bath9To10 += amt;
                                            break;
                                        case 10:
                                            level = SearchLevel(subgroup);
                                            if (level == 1)
                                                bath_october.Bath1To2 += amt;
                                            if (level == 2)
                                                bath_october.Bath3To8 += amt;
                                            if (level == 3)
                                                bath_october.Bath9To10 += amt;
                                            break;
                                        case 11:
                                            level = SearchLevel(subgroup);
                                            if (level == 1)
                                                bath_november.Bath1To2 += amt;
                                            if (level == 2)
                                                bath_november.Bath3To8 += amt;
                                            if (level == 3)
                                                bath_november.Bath9To10 += amt;
                                            break;
                                        case 12:
                                            level = SearchLevel(subgroup);
                                            if (level == 1)
                                                bath_december.Bath1To2 += amt;
                                            if (level == 2)
                                                bath_december.Bath3To8 += amt;
                                            if (level == 3)
                                                bath_december.Bath9To10 += amt;
                                            break;
                                    }
                                }
                            }
                        }

                        oCommand.Dispose();
                        oAdapter.Dispose();
                        oTable.Dispose();
                    }
                }

            oConnection.Dispose();
            if (oConnection.State == ConnectionState.Open)
                oConnection.Close();

            // 2. �Ѵ Model �ӹǹ��ѡ�ҹ ��� Percentage �ͧ������͹
            for (int month = 1; month <= 12; month++)
            {
                decimal total_amount = 0;
                decimal percentage_1to2 = 0;
                decimal percentage_3to8 = 0;
                decimal percentage_9to10 = 0;

                if (month == 1)
                {
                    total_amount = Math.Round(bath_january.Bath1To2 + bath_january.Bath3To8 + bath_january.Bath9To10, 2);
                    if (total_amount > 0)
                    {
                        percentage_1to2 = Math.Round((bath_january.Bath1To2 * 100) / total_amount, 2);
                        percentage_3to8 = Math.Round((bath_january.Bath3To8 * 100) / total_amount, 2);
                        percentage_9to10 = Math.Round((bath_january.Bath9To10 * 100) / total_amount, 2);

                        // 3. % �Թ 100 ���͹��¡��� 100 ������� 100 �ʹ�
                        decimal total_percentage = percentage_1to2 + percentage_3to8 + percentage_9to10;
                        if (total_percentage > 0 && total_percentage != 100)
                        {
                            decimal max = 0;
                            if (percentage_1to2 > max)
                                max = percentage_1to2;
                            if (percentage_3to8 > max)
                                max = percentage_3to8;
                            if (percentage_9to10 > max)
                                max = percentage_9to10;

                            if (total_percentage > 100)
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                            else
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                        }
                    }

                    dashboard.OTAmountLevel1To2.Add(bath_january.Bath1To2);
                    dashboard.OTAmountLevel3To8.Add(bath_january.Bath3To8);
                    dashboard.OTAmountLevel9To10.Add(bath_january.Bath9To10);

                    dashboard.PercentageOTAmountLevel1To2.Add(percentage_1to2);
                    dashboard.PercentageOTAmountLevel3To8.Add(percentage_3to8);
                    dashboard.PercentageOTAmountLevel9To10.Add(percentage_9to10);
                }

                if (month == 2)
                {
                    total_amount = Math.Round(bath_february.Bath1To2 + bath_february.Bath3To8 + bath_february.Bath9To10, 2);
                    if (total_amount > 0)
                    {
                        percentage_1to2 = Math.Round((bath_february.Bath1To2 * 100) / total_amount, 2);
                        percentage_3to8 = Math.Round((bath_february.Bath3To8 * 100) / total_amount, 2);
                        percentage_9to10 = Math.Round((bath_february.Bath9To10 * 100) / total_amount, 2);

                        // 3. % �Թ 100 ���͹��¡��� 100 ������� 100 �ʹ�
                        decimal total_percentage = percentage_1to2 + percentage_3to8 + percentage_9to10;
                        if (total_percentage > 0 && total_percentage != 100)
                        {
                            decimal max = 0;
                            if (percentage_1to2 > max)
                                max = percentage_1to2;
                            if (percentage_3to8 > max)
                                max = percentage_3to8;
                            if (percentage_9to10 > max)
                                max = percentage_9to10;

                            if (total_percentage > 100)
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                            else
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                        }
                    }

                    dashboard.OTAmountLevel1To2.Add(bath_february.Bath1To2);
                    dashboard.OTAmountLevel3To8.Add(bath_february.Bath3To8);
                    dashboard.OTAmountLevel9To10.Add(bath_february.Bath9To10);

                    dashboard.PercentageOTAmountLevel1To2.Add(percentage_1to2);
                    dashboard.PercentageOTAmountLevel3To8.Add(percentage_3to8);
                    dashboard.PercentageOTAmountLevel9To10.Add(percentage_9to10);

                }

                if (month == 3)
                {
                    total_amount = Math.Round(bath_march.Bath1To2 + bath_march.Bath3To8 + bath_march.Bath9To10, 2);
                    if (total_amount > 0)
                    {
                        percentage_1to2 = Math.Round((bath_march.Bath1To2 * 100) / total_amount, 2);
                        percentage_3to8 = Math.Round((bath_march.Bath3To8 * 100) / total_amount, 2);
                        percentage_9to10 = Math.Round((bath_march.Bath9To10 * 100) / total_amount, 2);

                        // 3. % �Թ 100 ���͹��¡��� 100 ������� 100 �ʹ�
                        decimal total_percentage = percentage_1to2 + percentage_3to8 + percentage_9to10;
                        if (total_percentage > 0 && total_percentage != 100)
                        {
                            decimal max = 0;
                            if (percentage_1to2 > max)
                                max = percentage_1to2;
                            if (percentage_3to8 > max)
                                max = percentage_3to8;
                            if (percentage_9to10 > max)
                                max = percentage_9to10;

                            if (total_percentage > 100)
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                            else
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                        }
                    }

                    dashboard.OTAmountLevel1To2.Add(bath_march.Bath1To2);
                    dashboard.OTAmountLevel3To8.Add(bath_march.Bath3To8);
                    dashboard.OTAmountLevel9To10.Add(bath_march.Bath9To10);

                    dashboard.PercentageOTAmountLevel1To2.Add(percentage_1to2);
                    dashboard.PercentageOTAmountLevel3To8.Add(percentage_3to8);
                    dashboard.PercentageOTAmountLevel9To10.Add(percentage_9to10);
                }

                if (month == 4)
                {
                    total_amount = Math.Round(bath_april.Bath1To2 + bath_april.Bath3To8 + bath_april.Bath9To10, 2);
                    if (total_amount > 0)
                    {
                        percentage_1to2 = Math.Round((bath_april.Bath1To2 * 100) / total_amount, 2);
                        percentage_3to8 = Math.Round((bath_april.Bath3To8 * 100) / total_amount, 2);
                        percentage_9to10 = Math.Round((bath_april.Bath9To10 * 100) / total_amount, 2);

                        // 3. % �Թ 100 ���͹��¡��� 100 ������� 100 �ʹ�
                        decimal total_percentage = percentage_1to2 + percentage_3to8 + percentage_9to10;
                        if (total_percentage > 0 && total_percentage != 100)
                        {
                            decimal max = 0;
                            if (percentage_1to2 > max)
                                max = percentage_1to2;
                            if (percentage_3to8 > max)
                                max = percentage_3to8;
                            if (percentage_9to10 > max)
                                max = percentage_9to10;

                            if (total_percentage > 100)
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                            else
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                        }
                    }

                    dashboard.OTAmountLevel1To2.Add(bath_april.Bath1To2);
                    dashboard.OTAmountLevel3To8.Add(bath_april.Bath3To8);
                    dashboard.OTAmountLevel9To10.Add(bath_april.Bath9To10);

                    dashboard.PercentageOTAmountLevel1To2.Add(percentage_1to2);
                    dashboard.PercentageOTAmountLevel3To8.Add(percentage_3to8);
                    dashboard.PercentageOTAmountLevel9To10.Add(percentage_9to10);
                }

                if (month == 5)
                {
                    total_amount = Math.Round(bath_may.Bath1To2 + bath_may.Bath3To8 + bath_may.Bath9To10, 2);
                    if (total_amount > 0)
                    {
                        percentage_1to2 = Math.Round((bath_may.Bath1To2 * 100) / total_amount, 2);
                        percentage_3to8 = Math.Round((bath_may.Bath3To8 * 100) / total_amount, 2);
                        percentage_9to10 = Math.Round((bath_may.Bath9To10 * 100) / total_amount, 2);

                        // 3. % �Թ 100 ���͹��¡��� 100 ������� 100 �ʹ�
                        decimal total_percentage = percentage_1to2 + percentage_3to8 + percentage_9to10;
                        if (total_percentage > 0 && total_percentage != 100)
                        {
                            decimal max = 0;
                            if (percentage_1to2 > max)
                                max = percentage_1to2;
                            if (percentage_3to8 > max)
                                max = percentage_3to8;
                            if (percentage_9to10 > max)
                                max = percentage_9to10;

                            if (total_percentage > 100)
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                            else
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                        }
                    }

                    dashboard.OTAmountLevel1To2.Add(bath_may.Bath1To2);
                    dashboard.OTAmountLevel3To8.Add(bath_may.Bath3To8);
                    dashboard.OTAmountLevel9To10.Add(bath_may.Bath9To10);

                    dashboard.PercentageOTAmountLevel1To2.Add(percentage_1to2);
                    dashboard.PercentageOTAmountLevel3To8.Add(percentage_3to8);
                    dashboard.PercentageOTAmountLevel9To10.Add(percentage_9to10);
                }

                if (month == 6)
                {
                    total_amount = Math.Round(bath_june.Bath1To2 + bath_june.Bath3To8 + bath_june.Bath9To10, 2);
                    if (total_amount > 0)
                    {
                        percentage_1to2 = Math.Round((bath_june.Bath1To2 * 100) / total_amount, 2);
                        percentage_3to8 = Math.Round((bath_june.Bath3To8 * 100) / total_amount, 2);
                        percentage_9to10 = Math.Round((bath_june.Bath9To10 * 100) / total_amount, 2);

                        // 3. % �Թ 100 ���͹��¡��� 100 ������� 100 �ʹ�
                        decimal total_percentage = percentage_1to2 + percentage_3to8 + percentage_9to10;
                        if (total_percentage > 0 && total_percentage != 100)
                        {
                            decimal max = 0;
                            if (percentage_1to2 > max)
                                max = percentage_1to2;
                            if (percentage_3to8 > max)
                                max = percentage_3to8;
                            if (percentage_9to10 > max)
                                max = percentage_9to10;

                            if (total_percentage > 100)
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                            else
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                        }
                    }

                    dashboard.OTAmountLevel1To2.Add(bath_june.Bath1To2);
                    dashboard.OTAmountLevel3To8.Add(bath_june.Bath3To8);
                    dashboard.OTAmountLevel9To10.Add(bath_june.Bath9To10);

                    dashboard.PercentageOTAmountLevel1To2.Add(percentage_1to2);
                    dashboard.PercentageOTAmountLevel3To8.Add(percentage_3to8);
                    dashboard.PercentageOTAmountLevel9To10.Add(percentage_9to10);
                }

                if (month == 7)
                {
                    total_amount = Math.Round(bath_july.Bath1To2 + bath_july.Bath3To8  + bath_july.Bath9To10, 2);
                    if (total_amount > 0)
                    {
                        percentage_1to2 = Math.Round((bath_july.Bath1To2 * 100) / total_amount, 2);
                        percentage_3to8 = Math.Round((bath_july.Bath3To8 * 100) / total_amount, 2);
                        percentage_9to10 = Math.Round((bath_july.Bath9To10 * 100) / total_amount, 2);

                        // 3. % �Թ 100 ���͹��¡��� 100 ������� 100 �ʹ�
                        decimal total_percentage = percentage_1to2 + percentage_3to8 + percentage_9to10;
                        if (total_percentage > 0 && total_percentage != 100)
                        {
                            decimal max = 0;
                            if (percentage_1to2 > max)
                                max = percentage_1to2;
                            if (percentage_3to8 > max)
                                max = percentage_3to8;
                            if (percentage_9to10 > max)
                                max = percentage_9to10;

                            if (total_percentage > 100)
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                            else
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                        }
                    }

                    dashboard.OTAmountLevel1To2.Add(bath_july.Bath1To2);
                    dashboard.OTAmountLevel3To8.Add(bath_july.Bath3To8);
                    dashboard.OTAmountLevel9To10.Add(bath_july.Bath9To10);

                    dashboard.PercentageOTAmountLevel1To2.Add(percentage_1to2);
                    dashboard.PercentageOTAmountLevel3To8.Add(percentage_3to8);
                    dashboard.PercentageOTAmountLevel9To10.Add(percentage_9to10);
                }

                if (month == 8)
                {
                    total_amount = Math.Round(bath_august.Bath1To2 + bath_august.Bath3To8 + bath_august.Bath9To10, 2);
                    if (total_amount > 0)
                    {
                        percentage_1to2 = Math.Round((bath_august.Bath1To2 * 100) / total_amount, 2);
                        percentage_3to8 = Math.Round((bath_august.Bath3To8 * 100) / total_amount, 2);

                        percentage_9to10 = Math.Round((bath_august.Bath9To10 * 100) / total_amount, 2);

                        // 3. % �Թ 100 ���͹��¡��� 100 ������� 100 �ʹ�
                        decimal total_percentage = percentage_1to2 + percentage_3to8  + percentage_9to10;
                        if (total_percentage > 0 && total_percentage != 100)
                        {
                            decimal max = 0;
                            if (percentage_1to2 > max)
                                max = percentage_1to2;
                            if (percentage_3to8 > max)
                                max = percentage_3to8;
                            if (percentage_9to10 > max)
                                max = percentage_9to10;

                            if (total_percentage > 100)
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                            else
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                        }
                    }

                    dashboard.OTAmountLevel1To2.Add(bath_august.Bath1To2);
                    dashboard.OTAmountLevel3To8.Add(bath_august.Bath3To8);
                    dashboard.OTAmountLevel9To10.Add(bath_august.Bath9To10);

                    dashboard.PercentageOTAmountLevel1To2.Add(percentage_1to2);
                    dashboard.PercentageOTAmountLevel3To8.Add(percentage_3to8);
                    dashboard.PercentageOTAmountLevel9To10.Add(percentage_9to10);
                }

                if (month == 9)
                {
                    total_amount = Math.Round(bath_september.Bath1To2 + bath_september.Bath3To8  + bath_september.Bath9To10, 2);
                    if (total_amount > 0)
                    {
                        percentage_1to2 = Math.Round((bath_september.Bath1To2 * 100) / total_amount, 2);
                        percentage_3to8 = Math.Round((bath_september.Bath3To8 * 100) / total_amount, 2);
                        percentage_9to10 = Math.Round((bath_september.Bath9To10 * 100) / total_amount, 2);

                        // 3. % �Թ 100 ���͹��¡��� 100 ������� 100 �ʹ�
                        decimal total_percentage = percentage_1to2 + percentage_3to8 + percentage_9to10;
                        if (total_percentage > 0 && total_percentage != 100)
                        {
                            decimal max = 0;
                            if (percentage_1to2 > max)
                                max = percentage_1to2;
                            if (percentage_3to8 > max)
                                max = percentage_3to8;
                            if (percentage_9to10 > max)
                                max = percentage_9to10;

                            if (total_percentage > 100)
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                            else
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                        }
                    }

                    dashboard.OTAmountLevel1To2.Add(bath_september.Bath1To2);
                    dashboard.OTAmountLevel3To8.Add(bath_september.Bath3To8);
                    dashboard.OTAmountLevel9To10.Add(bath_september.Bath9To10);

                    dashboard.PercentageOTAmountLevel1To2.Add(percentage_1to2);
                    dashboard.PercentageOTAmountLevel3To8.Add(percentage_3to8);
                    dashboard.PercentageOTAmountLevel9To10.Add(percentage_9to10);
                }

                if (month == 10)
                {
                    total_amount = Math.Round(bath_october.Bath1To2 + bath_october.Bath3To8  + bath_october.Bath9To10, 2);
                    if (total_amount > 0)
                    {
                        percentage_1to2 = Math.Round((bath_october.Bath1To2 * 100) / total_amount, 2);
                        percentage_3to8 = Math.Round((bath_october.Bath3To8 * 100) / total_amount, 2);
                        percentage_9to10 = Math.Round((bath_october.Bath9To10 * 100) / total_amount, 2);

                        // 3. % �Թ 100 ���͹��¡��� 100 ������� 100 �ʹ�
                        decimal total_percentage = percentage_1to2 + percentage_3to8 + percentage_9to10;
                        if (total_percentage > 0 && total_percentage != 100)
                        {
                            decimal max = 0;
                            if (percentage_1to2 > max)
                                max = percentage_1to2;
                            if (percentage_3to8 > max)
                                max = percentage_3to8;
                            if (percentage_9to10 > max)
                                max = percentage_9to10;

                            if (total_percentage > 100)
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                            else
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                        }
                    }

                    dashboard.OTAmountLevel1To2.Add(bath_october.Bath1To2);
                    dashboard.OTAmountLevel3To8.Add(bath_october.Bath3To8);
                    dashboard.OTAmountLevel9To10.Add(bath_october.Bath9To10);

                    dashboard.PercentageOTAmountLevel1To2.Add(percentage_1to2);
                    dashboard.PercentageOTAmountLevel3To8.Add(percentage_3to8);
                    dashboard.PercentageOTAmountLevel9To10.Add(percentage_9to10);
                }

                if (month == 11)
                {
                    total_amount = Math.Round(bath_november.Bath1To2 + bath_november.Bath3To8 + bath_november.Bath9To10, 2);
                    if (total_amount > 0)
                    {
                        percentage_1to2 = Math.Round((bath_november.Bath1To2 * 100) / total_amount, 2);
                        percentage_3to8 = Math.Round((bath_november.Bath3To8 * 100) / total_amount, 2);
                        percentage_9to10 = Math.Round((bath_november.Bath9To10 * 100) / total_amount, 2);

                        // 3. % �Թ 100 ���͹��¡��� 100 ������� 100 �ʹ�
                        decimal total_percentage = percentage_1to2 + percentage_3to8 + percentage_9to10;
                        if (total_percentage > 0 && total_percentage != 100)
                        {
                            decimal max = 0;
                            if (percentage_1to2 > max)
                                max = percentage_1to2;
                            if (percentage_3to8 > max)
                                max = percentage_3to8;
                            if (percentage_9to10 > max)
                                max = percentage_9to10;

                            if (total_percentage > 100)
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                            else
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                        }
                    }

                    dashboard.OTAmountLevel1To2.Add(bath_november.Bath1To2);
                    dashboard.OTAmountLevel3To8.Add(bath_november.Bath3To8);
                    dashboard.OTAmountLevel9To10.Add(bath_november.Bath9To10);

                    dashboard.PercentageOTAmountLevel1To2.Add(percentage_1to2);
                    dashboard.PercentageOTAmountLevel3To8.Add(percentage_3to8);
                    dashboard.PercentageOTAmountLevel9To10.Add(percentage_9to10);
                }

                if (month == 12)
                {
                    total_amount = Math.Round(bath_december.Bath1To2 + bath_december.Bath3To8  + bath_december.Bath9To10, 2);
                    if (total_amount > 0)
                    {
                        percentage_1to2 = Math.Round((bath_december.Bath1To2 * 100) / total_amount, 2);
                        percentage_3to8 = Math.Round((bath_december.Bath3To8 * 100) / total_amount, 2);
                        percentage_9to10 = Math.Round((bath_december.Bath9To10 * 100) / total_amount, 2);

                        // 3. % �Թ 100 ���͹��¡��� 100 ������� 100 �ʹ�
                        decimal total_percentage = percentage_1to2 + percentage_3to8 + percentage_9to10;
                        if (total_percentage > 0 && total_percentage != 100)
                        {
                            decimal max = 0;
                            if (percentage_1to2 > max)
                                max = percentage_1to2;
                            if (percentage_3to8 > max)
                                max = percentage_3to8;
                            if (percentage_9to10 > max)
                                max = percentage_9to10;

                            if (total_percentage > 100)
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                            else
                            {
                                decimal diff = total_percentage - 100;
                                decimal new_percentage = max - diff;

                                if (max == percentage_1to2)
                                    percentage_1to2 = new_percentage;
                                else if (max == percentage_3to8)
                                    percentage_3to8 = new_percentage;
                                else if (max == percentage_9to10)
                                    percentage_9to10 = new_percentage;
                            }
                        }
                    }

                    dashboard.OTAmountLevel1To2.Add(bath_december.Bath1To2);
                    dashboard.OTAmountLevel3To8.Add(bath_december.Bath3To8);
                    dashboard.OTAmountLevel9To10.Add(bath_december.Bath9To10);

                    dashboard.PercentageOTAmountLevel1To2.Add(percentage_1to2);
                    dashboard.PercentageOTAmountLevel3To8.Add(percentage_3to8);
                    dashboard.PercentageOTAmountLevel9To10.Add(percentage_9to10);
                }
            }

            return dashboard;
        }

        public int SearchLevel(int subgroup)
        {
            if (subgroup > 0 && subgroup < 3)
                return 1;
            else if (subgroup <= 8)
                return 2;
            else if (subgroup <= 10)
                return 3;
            else
                return 0;
        }

        public override decimal GetOTSummaryInMonth(string EmployeeID,DateTime BeginDate)
        {
            decimal sumOTHour = 0;
            CultureInfo oCL = new CultureInfo("en-US");
            List<AreaWorkscheduleLog> oReturn = new List<AreaWorkscheduleLog>();
            DataTable oTable = new DataTable();
            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            oConnection.Open();

            SqlCommand oCommand = new SqlCommand("sp_GetOTSummaryInMonth", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);

            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate.Date;
            oCommand.Parameters.Add(oParam);           

            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            if (oTable.Rows.Count > 0)
            {
                sumOTHour = Convert.ToDecimal(oTable.Rows[0]["SumOT"]);               
            }

            return sumOTHour;
        }
        #endregion

        public override DataTable GetOTTrackingStatusReport(DateTime BeginDate, DateTime EndDate, string EmpID, string OrgUnitList, string CompanyCode, string LanguageCode)
        {
            DataTable oReturn = new DataTable("OTTracking");

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_GetOTTrackingStatusReport", oConnection);
            oCommand.CommandTimeout = 500;
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;

            oParam = new SqlParameter("@p_BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_RequestorID", SqlDbType.VarChar);
            oParam.Value = EmpID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_OrgUnitList", SqlDbType.VarChar);
            oParam.Value = OrgUnitList;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_CompanyCode", SqlDbType.VarChar);
            oParam.Value = CompanyCode;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_LanguageCode", SqlDbType.VarChar);
            oParam.Value = LanguageCode;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            oAdapter.Fill(oReturn);
            oConnection.Close();
            oConnection.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();

            return oReturn;
        }

        public override DataTable GetDutyTrackingStatusReport(DateTime BeginDate, DateTime EndDate, string EmpID, string OrgUnitList, string CompanyCode, string LanguageCode)
        {
            DataTable oReturn = new DataTable("DutyTracking");

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_GetDutyTrackingStatusReport", oConnection);
            oCommand.CommandTimeout = 500;
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;

            oParam = new SqlParameter("@p_BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_RequestorID", SqlDbType.VarChar);
            oParam.Value = EmpID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_OrgUnitList", SqlDbType.VarChar);
            oParam.Value = OrgUnitList;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_CompanyCode", SqlDbType.VarChar);
            oParam.Value = CompanyCode;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_LanguageCode", SqlDbType.VarChar);
            oParam.Value = LanguageCode;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            oAdapter.Fill(oReturn);
            oConnection.Close();
            oConnection.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();

            return oReturn;
        }

        public override DataTable GetAttendanceMonthlyReport(DateTime BeginDate, DateTime EndDate, string EmpID, string OrgUnitList, string CompanyCode, string LanguageCode)
        {
            DataTable oReturn = new DataTable("AttendanceMonthly");

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_GetAttendanceReport", oConnection);
            oCommand.CommandTimeout = 500;
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;

            oParam = new SqlParameter("@p_BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_RequestorID", SqlDbType.VarChar);
            oParam.Value = EmpID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_OrgUnitList", SqlDbType.VarChar);
            oParam.Value = OrgUnitList;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_CompanyCode", SqlDbType.VarChar);
            oParam.Value = CompanyCode;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_LanguageCode", SqlDbType.VarChar);
            oParam.Value = LanguageCode;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            oAdapter.Fill(oReturn);
            oConnection.Close();
            oConnection.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();

            return oReturn;
        }

        public override DataTable GetTimeScanReport(DateTime BeginDate, DateTime EndDate, string EmpID, string OrgUnitList, string CompanyCode, string LanguageCode)
        {
            DataTable oReturn = new DataTable("TimeScan");

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_GetTimeScanReport", oConnection);
            oCommand.CommandTimeout = 500;
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;

            oParam = new SqlParameter("@p_BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_RequestorID", SqlDbType.VarChar);
            oParam.Value = EmpID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_OrgUnitList", SqlDbType.VarChar);
            oParam.Value = OrgUnitList;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_CompanyCode", SqlDbType.VarChar);
            oParam.Value = CompanyCode;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_LanguageCode", SqlDbType.VarChar);
            oParam.Value = LanguageCode;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            oAdapter.Fill(oReturn);
            oConnection.Close();
            oConnection.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();

            return oReturn;
        }

        public override DataTable GetWorkingSummaryReport(DateTime BeginDate, DateTime EndDate, string EmpID, string OrgUnitList, string CompanyCode, string LanguageCode)
        {
            DataTable oReturn = new DataTable("WorkingSummary");

            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_GetWorkingSummaryReport", oConnection);
            oCommand.CommandTimeout = 500;
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;

            oParam = new SqlParameter("@p_BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_RequestorID", SqlDbType.VarChar);
            oParam.Value = EmpID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_OrgUnitList", SqlDbType.VarChar);
            oParam.Value = OrgUnitList;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_CompanyCode", SqlDbType.VarChar);
            oParam.Value = CompanyCode;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_LanguageCode", SqlDbType.VarChar);
            oParam.Value = LanguageCode;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            oAdapter.Fill(oReturn);
            oConnection.Close();
            oConnection.Dispose();
            oAdapter.Dispose();
            oCommand.Dispose();

            return oReturn;
        }
    }
}
