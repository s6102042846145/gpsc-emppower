﻿using ESS.API.ABSTRACT;
using ESS.API.DATACLASS;
using ESS.SHAREDATASERVICE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using ESS.API.UTILITY;
using System.Json;
using System.Security.Cryptography;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.Configuration;
using System.Reflection;


namespace ESS.API.DB
{
    public class APIServiceImpl : AbstractAPIService
    {
        //   private static readonly ILog log = LogManager.GetLogger(typeof(APIDataService));
        #region Constructor
        public APIServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
        }

        #endregion Constructor

        #region Member
        public string CompanyCode { get; set; }

        private static string ModuleID = "ESS.API.DB";

        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
                //  return "server=10.224.76.11;uid=usrClient;pwd=2018@ESSClient;database=ESS_GPSC";
            }
        }

        #endregion Member
        private string publicKey =
         "<RSAKeyValue><Modulus>21wEnTU+mcD2w0Lfo1Gv4rtcSWsQJQTNa6gio05AOkV/Er9w3Y13Ddo5wGtjJ19402S71HUeN0vbKILLJdRSES5MHSdJPSVrOqdrll/vLXxDxWs/U0UT1c8u6k/Ogx9hTtZxYwoeYqdhDblof3E75d9n2F0Zvf6iTb4cI7j6fMs=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";

        private string privateKey =
            "<RSAKeyValue><Modulus>21wEnTU+mcD2w0Lfo1Gv4rtcSWsQJQTNa6gio05AOkV/Er9w3Y13Ddo5wGtjJ19402S71HUeN0vbKILLJdRSES5MHSdJPSVrOqdrll/vLXxDxWs/U0UT1c8u6k/Ogx9hTtZxYwoeYqdhDblof3E75d9n2F0Zvf6iTb4cI7j6fMs=</Modulus><Exponent>AQAB</Exponent><P>/aULPE6jd5IkwtWXmReyMUhmI/nfwfkQSyl7tsg2PKdpcxk4mpPZUdEQhHQLvE84w2DhTyYkPHCtq/mMKE3MHw==</P><Q>3WV46X9Arg2l9cxb67KVlNVXyCqc/w+LWt/tbhLJvV2xCF/0rWKPsBJ9MC6cquaqNPxWWEav8RAVbmmGrJt51Q==</Q><DP>8TuZFgBMpBoQcGUoS2goB4st6aVq1FcG0hVgHhUI0GMAfYFNPmbDV3cY2IBt8Oj/uYJYhyhlaj5YTqmGTYbATQ==</DP><DQ>FIoVbZQgrAUYIHWVEYi/187zFd7eMct/Yi7kGBImJStMATrluDAspGkStCWe4zwDDmdam1XzfKnBUzz3AYxrAQ==</DQ><InverseQ>QPU3Tmt8nznSgYZ+5jUo9E0SfjiTu435ihANiHqqjasaUNvOHKumqzuBZ8NRtkUhS6dsOEb8A2ODvy7KswUxyA==</InverseQ><D>cgoRoAUpSVfHMdYXW9nA3dfX75dIamZnwPtFHq80ttagbIe4ToYYCcyUz5NElhiNQSESgS5uCgNWqWXt5PnPu4XmCXx6utco1UVH8HGLahzbAnSy6Cj3iUIQ7Gj+9gQ7PkC434HTtHazmxVgIR5l56ZjoQ8yGNCPZnsdYEmhJWk=</D></RSAKeyValue>";
        public override ApiUser Authorize(string username, string password)
        {
            APIUtility util = new APIUtility();
            ApiUser user = new ApiUser();
            try
            {
                SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
                SqlCommand oCommand = new SqlCommand("sp_GetApiUserByNamePass", oConnection);
                oCommand.CommandType = System.Data.CommandType.StoredProcedure;
                oCommand.Parameters.Add("@p_UserName", SqlDbType.VarChar).Value = username;
                oCommand.Parameters.Add("@p_UserPassword", SqlDbType.VarChar).Value = password;
                oConnection.Open();

                DataTable dt = new DataTable();
                dt.Load(oCommand.ExecuteReader());
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {

                        user.UserID = (int)row["UserID"];
                        user.UserName = row["UserName"].ToString();
                        user.UserPassword = row["UserPassword"].ToString();
                        user.FirstName = row["FirstName"].ToString();
                        user.LastName = row["LastName"].ToString();
                        user.Token = row["Token"].ToString();
                        user.TimeLimit = (int)row["TimeLimit"];
                        user.RSAPublicKey = row["RSAPublicKey"].ToString();
                    }

                    string dbtoken = user.Token.Split(':')[0];
                    byte[] data = Convert.FromBase64String(dbtoken);
                    DateTime when = DateTime.FromBinary(BitConverter.ToInt64(data, 0));
                    if (when < DateTime.UtcNow.AddMinutes(-1 * user.TimeLimit))
                    {
                        ApiConfig con = GetAPIConfig("tokenTimeOut");
                        string tokenwithtime = util.SimpleToken();
                        string tokenjwt = util.JWTGenerator(user);
                        string token = tokenwithtime + ":" + tokenjwt;
                        user.Token = token;
                        user.TimeLimit = con != null ? Convert.ToInt32(con.Value) : 5;
                        UpdateAPIUser(user);
                    }
                }
                oConnection.Close();
                oConnection.Dispose();

            }
            catch (Exception ex)
            {
                //log.Error(ex);
                throw ex;
            }
            return user;
        }
        public override ApiUser AuthorizeCheckToken(string token)
        {
            ApiUser user = new ApiUser();
            try
            {
                SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
                SqlCommand oCommand = new SqlCommand("sp_GetApiUserByToken", oConnection);
                oCommand.CommandType = System.Data.CommandType.StoredProcedure;
                oCommand.Parameters.Add("@p_Token", SqlDbType.VarChar).Value = token;
                oConnection.Open();

                DataTable dt = new DataTable();
                dt.Load(oCommand.ExecuteReader());
                foreach (DataRow row in dt.Rows)
                {

                    user.UserID = (int)row["UserID"];
                    user.UserName = row["UserName"].ToString();
                    user.UserPassword = row["UserPassword"].ToString();
                    user.FirstName = row["FirstName"].ToString();
                    user.LastName = row["LastName"].ToString();
                    user.Token = row["Token"].ToString();
                    user.TimeLimit = (int)row["TimeLimit"];
                    user.RSAPublicKey = row["RSAPublicKey"].ToString();
                    user.Active = !string.IsNullOrEmpty(row["Active"].ToString()) ? Convert.ToBoolean(row["Active"]) : false;
                }
                oConnection.Close();
                oConnection.Dispose();


            }
            catch (Exception ex)
            {
                //log.Error(ex);
                throw ex;
            }
            return user;
        }
        public override List<ApiService> GetAPIServiceAll()
        {
            List<ApiService> apiServices = new List<ApiService>();
            try
            {
                SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
                SqlCommand oCommand = new SqlCommand("sp_GetApiServiceAll", oConnection);
                oCommand.CommandType = System.Data.CommandType.StoredProcedure;
                oConnection.Open();

                DataTable dt = new DataTable();
                dt.Load(oCommand.ExecuteReader());
                foreach (DataRow row in dt.Rows)
                {
                    ApiService api = new ApiService();
                    api.API_ID = (int)row["API_ID"];
                    api.APIName = row["APIName"].ToString();
                    api.Description = row["Description"].ToString();
                    api.Active = !string.IsNullOrEmpty(row["Active"].ToString()) ? Convert.ToBoolean(row["Active"]) : false;
                    apiServices.Add(api);
                }
                oConnection.Close();
                oConnection.Dispose();


            }
            catch (Exception ex)
            {
                //log.Error(ex);
                throw ex;
            }
            return apiServices;
        }
        public override ApiService GetAPIServiceByName(string apiName)
        {
            ApiService returnValue = new ApiService();
            try
            {
                SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
                SqlCommand oCommand = new SqlCommand("sp_GetApiServiceByName", oConnection);
                oCommand.CommandType = System.Data.CommandType.StoredProcedure;
                oCommand.Parameters.Add("@p_APIName", SqlDbType.VarChar).Value = apiName;
                oConnection.Open();
                DataTable dt = new DataTable();
                dt.Load(oCommand.ExecuteReader());
                foreach (DataRow row in dt.Rows)
                {
                    returnValue.API_ID = (int)row["API_ID"];
                    returnValue.APIName = row["APIName"].ToString();
                    returnValue.Description = row["Description"].ToString();
                    returnValue.Active = !string.IsNullOrEmpty(row["Active"].ToString()) ? Convert.ToBoolean(row["Active"]) : false;
                }
                oConnection.Close();
                oConnection.Dispose();


            }
            catch (Exception ex)
            {
                //log.Error(ex);
                throw ex;
            }
            return returnValue;
        }
        public override ApiAccess GetAPIAccess(int userID, int apiID)
        {
            ApiAccess data = new ApiAccess();
            try
            {
                SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
                SqlCommand oCommand = new SqlCommand("sp_GetApiAccess", oConnection);
                oCommand.CommandType = System.Data.CommandType.StoredProcedure;
                oCommand.Parameters.Add("@p_UserID", SqlDbType.VarChar).Value = userID;
                oCommand.Parameters.Add("@p_API_ID", SqlDbType.VarChar).Value = apiID;
                oConnection.Open();

                DataTable dt = new DataTable();
                dt.Load(oCommand.ExecuteReader());
                foreach (DataRow row in dt.Rows)
                {
                    data.APIAccessID = (int)row["APIAccessID"];
                    data.API_ID = (int)row["API_ID"];
                    data.UserID = (int)row["UserID"];
                    data.Active = !string.IsNullOrEmpty(row["Active"].ToString()) ? Convert.ToBoolean(row["Active"]) : false;
                }
                oConnection.Close();
                oConnection.Dispose();


            }
            catch (Exception ex)
            {
                //log.Error(ex);
                throw ex;
            }
            return data;
        }
        public override ApiUser GetAPIUser(string username)
        {
            ApiUser user = new ApiUser();
            try
            {
                SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
                SqlCommand oCommand = new SqlCommand("sp_GetApiUserByName", oConnection);
                oCommand.CommandType = System.Data.CommandType.StoredProcedure;
                oCommand.Parameters.Add("@p_UserName", SqlDbType.VarChar).Value = username;
                oConnection.Open();

                DataTable dt = new DataTable();
                dt.Load(oCommand.ExecuteReader());
                foreach (DataRow row in dt.Rows)
                {
                    user.UserID = (int)row["UserID"];
                    user.UserName = row["UserName"].ToString();
                    user.UserPassword = row["UserPassword"].ToString();
                    user.FirstName = row["FirstName"].ToString();
                    user.LastName = row["LastName"].ToString();
                    user.Token = row["Token"].ToString();
                    user.TimeLimit = (int)row["TimeLimit"];
                    user.RSAPublicKey = row["RSAPublicKey"].ToString();
                }
                oConnection.Close();
                oConnection.Dispose();


            }
            catch (Exception ex)
            {
                //log.Error(ex);
                throw ex;
            }
            return user;
        }
        public override ApiUser GetRSAPublicKey(string userName)
        {
            ApiUser user = new ApiUser();
            try
            {
                SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
                SqlCommand oCommand = new SqlCommand("sp_GetApiUserRSAKey", oConnection);
                oCommand.CommandType = System.Data.CommandType.StoredProcedure;
                oCommand.Parameters.Add("@p_UserName", SqlDbType.VarChar).Value = userName;
                oConnection.Open();

                DataTable dt = new DataTable();
                dt.Load(oCommand.ExecuteReader());
                foreach (DataRow row in dt.Rows)
                {

                    user.UserID = (int)row["UserID"];
                    user.UserName = row["UserName"].ToString();
                    user.UserPassword = row["UserPassword"].ToString();
                    user.FirstName = row["FirstName"].ToString();
                    user.LastName = row["LastName"].ToString();
                    user.Token = row["Token"].ToString();
                    user.TimeLimit = (int)row["TimeLimit"];
                    user.RSAPublicKey = row["RSAPublicKey"].ToString();
                }
                oConnection.Close();
                oConnection.Dispose();


            }
            catch (Exception ex)
            {
                //log.Error(ex);
                throw ex;
            }
            return user;
        }
        public override ApiConfig GetAPIConfig(string keyName)
        {
            ApiConfig config = new ApiConfig();
            try
            {
                SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
                SqlCommand oCommand = new SqlCommand("sp_GetApiConfigByKey", oConnection);
                oCommand.CommandType = System.Data.CommandType.StoredProcedure;
                oCommand.Parameters.Add("@p_Key", SqlDbType.VarChar).Value = keyName;
                oConnection.Open();

                DataTable dt = new DataTable();
                dt.Load(oCommand.ExecuteReader());
                foreach (DataRow row in dt.Rows)
                {

                    config.ConfigID = (int)row["ConfigID"];
                    config.Key = row["Key"].ToString();
                    config.Value = row["Value"].ToString();
                    config.Active = !string.IsNullOrEmpty(row["Active"].ToString()) ? Convert.ToBoolean(row["Active"]) : false;
                }
                oConnection.Close();
                oConnection.Dispose();


            }
            catch (Exception ex)
            {
                //log.Error(ex);
                throw ex;
            }
            return config;
        }
        public override void InsertAPILog(ApiLog model)
        {
            try
            {
                SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
                SqlCommand oCommand = new SqlCommand("sp_InsertApiLog", oConnection);
                oCommand.CommandType = System.Data.CommandType.StoredProcedure;
                oCommand.Parameters.Add("@p_EmployeeID", SqlDbType.NVarChar).Value = model.EmployeeID;
                oCommand.Parameters.Add("@p_RequestDateTime", SqlDbType.DateTime).Value = model.RequestDateTime;
                oCommand.Parameters.Add("@p_ResponseDateTime", SqlDbType.DateTime).Value = model.ResponseDateTime;
                oCommand.Parameters.Add("@p_GroupAPI", SqlDbType.NVarChar).Value = model.GroupAPI;
                oCommand.Parameters.Add("@p_APIName", SqlDbType.NVarChar).Value = model.APIName;
                oCommand.Parameters.Add("@p_MethodType", SqlDbType.NVarChar).Value = model.MethodType;
                oCommand.Parameters.Add("@p_RequestParameter", SqlDbType.NVarChar).Value = model.RequestParameter;
                oCommand.Parameters.Add("@p_RequestUrl", SqlDbType.NVarChar).Value = model.RequestUrl;
                oCommand.Parameters.Add("@p_ResponseText", SqlDbType.NVarChar).Value = model.ResponseText;
                oConnection.Open();
                oCommand.ExecuteNonQuery();
                oConnection.Close();
                oConnection.Dispose();
            }
            catch (Exception ex)
            {
                //log.Error(ex);
                throw ex;
            }
        }
        public override void UpdateAPIUser(ApiUser model)
        {
            try
            {
                SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
                SqlCommand oCommand = new SqlCommand("sp_UpdateApiUser", oConnection);
                oCommand.CommandType = System.Data.CommandType.StoredProcedure;
                oCommand.Parameters.Add("@p_UserID", SqlDbType.VarChar).Value = model.UserID;
                oCommand.Parameters.Add("@p_Token", SqlDbType.VarChar).Value = model.Token;
                oCommand.Parameters.Add("@p_TimeLimit", SqlDbType.Int).Value = model.TimeLimit;
                oConnection.Open();
                oCommand.ExecuteNonQuery();
                oConnection.Close();
                oConnection.Dispose();
            }
            catch (Exception ex)
            {
                //log.Error(ex);
                throw ex;
            }
        }
        public override void UpdateUserRsaPublicKey(ApiUser model)
        {
            try
            {
                SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
                SqlCommand oCommand = new SqlCommand("sp_UpdateApiUserRSAKey", oConnection);
                oCommand.CommandType = System.Data.CommandType.StoredProcedure;
                oCommand.Parameters.Add("@p_UserID", SqlDbType.VarChar).Value = model.UserID;
                oCommand.Parameters.Add("@p_RSAPublickey", SqlDbType.VarChar).Value = model.RSAPublicKey;
                oConnection.Open();
                oCommand.ExecuteNonQuery();
                oConnection.Close();
                oConnection.Dispose();
            }
            catch (Exception ex)
            {
                //log.Error(ex);
                throw ex;
            }

        }
        public string CreateKeyPair(string userName)
        {

            ApiUser user = GetRSAPublicKey(userName);
            if (user != null)
            {
                var csp = new RSACryptoServiceProvider(512);

                var priKey = csp.ExportParameters(true);

                var pubKey = csp.ExportParameters(false);

                var sw = new System.IO.StringWriter();
                var xs = new System.Xml.Serialization.XmlSerializer(typeof(RSAParameters));
                xs.Serialize(sw, pubKey);
                publicKey = sw.ToString();
                user.RSAPublicKey = publicKey;
                UpdateUserRsaPublicKey(user);

                var swd = new System.IO.StringWriter();
                var xsd = new System.Xml.Serialization.XmlSerializer(typeof(RSAParameters));
                xsd.Serialize(swd, priKey);
                privateKey = swd.ToString();
                return privateKey;
            }
            else
            {
                return "";
            }

        }
    }
}
