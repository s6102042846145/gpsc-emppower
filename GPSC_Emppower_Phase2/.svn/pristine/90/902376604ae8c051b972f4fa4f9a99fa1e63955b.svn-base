using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.TIMESHEET;
using ESS.HR.TM.CONFIG;

namespace ESS.HR.TM.INFOTYPE
{
    [Serializable]
    public class INFOTYPE2003 : Substitution, ICollisionable
    {
        private bool _IsOverride;
        private string __status = "";
        private string __substitute = "";
        private bool __isMark = true;
        private bool __isDraft = true;
        private string __requestNo = "";
        private string __remark = "";

        private string _CompanyCode = "";
        private string _SubDWSCodeOld = "";
        private string _SubDWSCodeNew = "";
        private string _EmpDWSCodeNew = "";
        private string _EmpDWSCodeOld = "";
        private string _EmployeeID = "";
        private DateTime _BeginDate;
        private DateTime _EndDate;
        private DateTime _SubstituteBeginDate;
        private DateTime _SubstituteEndDate;
        private TimeSpan _SubstituteBeginTime;
        private TimeSpan _SubstituteEndTime;
        private string _DWSGroup = "";
        private string _DWSCode = "";
        private string _EmpSubGroupSetting = "";
        private string _HolidayCalendar = "";
        private string _EmpSubAreaSetting = "";
        private string _WSRule = "";

        public bool IsOverride
        {
            get { return _IsOverride; }
            set
            {
                _IsOverride = value;
            }
        }
        public bool IsDraft
        {
            get
            {
                return __isDraft;
            }
            set
            {
                __isDraft = value;
            }
        }
        public bool IsMark
        {
            get
            {
                return __isMark;
            }
            set
            {
                __isMark = value;
            }
        }
        public string RequestNo
        {
            get
            {
                return __requestNo;
            }
            set
            {
                __requestNo = value;
            }
        }
        public string Status
        {
            get
            {
                return __status;
            }
            set
            {
                __status = value;
            }
        }
        public string Substitute
        {
            get
            {
                return __substitute;
            }
            set
            {
                __substitute = value;
            }
        }
        public string Remark
        {
            get
            {
                return __remark;
            }
            set
            {
                __remark = value;
            }
        }
        public string CompanyCode
        {
            get { return _CompanyCode; }
            set { _CompanyCode = value; }
        }
        public string SubDWSCodeOld
        {
            get { return _SubDWSCodeOld; }
            set { _SubDWSCodeOld = value; }
        }
        public string SubDWSCodeNew
        {
            get { return _SubDWSCodeNew; }
            set { _SubDWSCodeNew = value; }
        }
        public string EmpDWSCodeNew
        {
            get { return _EmpDWSCodeNew; }
            set { _EmpDWSCodeNew = value; }
        }
        public string EmpDWSCodeOld
        {
            get { return _EmpDWSCodeOld; }
            set { _EmpDWSCodeOld = value; }
        }
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        public DateTime BeginDate
        {
            get { return _BeginDate; }
            set { _BeginDate = value; }
        }
        public DateTime EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }
        public DateTime SubstituteBeginDate
        {
            get { return _SubstituteBeginDate; }
            set { _SubstituteBeginDate = value; }
        }
        public DateTime SubstituteEndDate
        {
            get { return _SubstituteEndDate; }
            set { _SubstituteEndDate = value; }
        }
        public TimeSpan SubstituteBeginTime
        {
            get { return _SubstituteBeginTime; }
            set { _SubstituteBeginTime = value; }
        }
        public TimeSpan SubstituteEndTime
        {
            get { return _SubstituteEndTime; }
            set { _SubstituteEndTime = value; }
        }
        public string DWSGroup
        {
            get { return _DWSGroup; }
            set { _DWSGroup = value; }
        }
        public string DWSCode
        {
            get { return _DWSCode; }
            set { _DWSCode = value; }
        }
        public string EmpSubGroupSetting
        {
            get { return _EmpSubGroupSetting; }
            set { _EmpSubGroupSetting = value; }
        }
        public string HolidayCalendar
        {
            get { return _HolidayCalendar; }
            set { _HolidayCalendar = value; }
        }
        public string EmpSubAreaSetting
        {
            get { return _EmpSubAreaSetting; }
            set { _EmpSubAreaSetting = value; }
        }
        public string WSRule
        {
            get { return _WSRule; }
            set { _WSRule = value; }
        }

        public void Validate()
        {
            //SubstituteCreatingRule oCreatingRule = SubstituteCreatingRule.GetCreatingRule();

            //SubstituteCreatingRule oCreatingRule = HRTMManagement.CreateInstance(CompanyCode).GetCreatingRule();

            SubstituteCreatingRule oCreatingRule = HRTMManagement.CreateInstance("0245").GetCreatingRule();
            DateTime oMonthNow = DateTime.Now.AddDays(1 - oCreatingRule.OffsetCutoff);
            oMonthNow = new DateTime(oMonthNow.Year, oMonthNow.Month, 1);
            bool lFound = false;
            if (WORKFLOW.WorkflowPrinciple.Current.IsInRole("TIMEADMIN") && this.EmployeeID != WORKFLOW.WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID)
            {
                switch (oCreatingRule.TimeAdminOffsetFlag)
                {
                    case "M":
                        if (this.BeginDate < oMonthNow.AddMonths(oCreatingRule.TimeAdminOffsetValue + 1))
                        {
                            throw new Exception("CAN_NOT_CREATE_BACKDATE");
                        }
                        lFound = true;
                        break;
                }
            }
            if (WORKFLOW.WorkflowPrinciple.Current.IsInRole("#MANAGER") && this.EmployeeID != WORKFLOW.WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID && !lFound)
            {
                switch (oCreatingRule.ManagerOffsetFlag)
                {
                    case "M":
                        if (this.BeginDate < oMonthNow.AddMonths(oCreatingRule.ManagerOffsetValue + 1))
                        {
                            throw new Exception("CAN_NOT_CREATE_BACKDATE");
                        }
                        lFound = true;
                        break;
                }
            }
            if (!lFound)
            {
                switch (oCreatingRule.OffsetFlag)
                {
                    case "M":
                        if (this.BeginDate < oMonthNow.AddMonths(oCreatingRule.OffsetValue + 1))
                        {
                            throw new Exception("CAN_NOT_CREATE_BACKDATE");
                        }
                        lFound = true;
                        break;
                    case "D":
                        if (this.BeginDate < DateTime.Now.AddDays(oCreatingRule.OffsetValue))
                        {
                            throw new Exception("CAN_NOT_CREATE_BACKDATE");
                        }
                        lFound = true;
                        break;
                }
            }
            EmployeeData oEmp;
            MonthlyWS oMWS_Emp, oMWS_Sub;
            DailyWS oDWS_Emp, oDWS_Sub;

            oMWS_Emp = MonthlyWS.GetCalendar(this.EmployeeID, this.BeginDate.Year, this.BeginDate.Month);
            oMWS_Sub = MonthlyWS.GetCalendar(this.Substitute, this.BeginDate.Year, this.BeginDate.Month);

            if (oMWS_Emp.ValuationClass != oMWS_Sub.ValuationClass)
            {
                throw new Exception("CAN_NOT_SUBSTITUTE_ACROSS_VALUATIONCLASS");
            }

            oEmp = new EmployeeData(this.EmployeeID, this.BeginDate);
            oDWS_Emp = oEmp.GetDailyWorkSchedule(this.BeginDate);
            oEmp = new EmployeeData(this.Substitute, this.BeginDate);
            oDWS_Sub = oEmp.GetDailyWorkSchedule(this.BeginDate);

            if (oDWS_Emp.DailyWorkscheduleGrouping != oDWS_Sub.DailyWorkscheduleGrouping)
            {
                throw new Exception("CAN_NOT_SUBSTITUTE_ACROSS_DWS_GROUPING");
            }
        }

        public override string ToString()
        {
            INFOTYPE2003_Convertor oConverter = new INFOTYPE2003_Convertor();
            return (string)oConverter.ConvertTo(this, typeof(string));
        }

        public List<INFOTYPE2003> GetData(EmployeeData employeeData, DateTime BeginDate, DateTime EndDate)
        {
            List<INFOTYPE2003> oReturn = new List<INFOTYPE2003>();
            List<Substitution> list;
            //list = HR.TM.ServiceManager.HRTMBuffer.GetAttendanceList(employeeData.EmployeeID, BeginDate, EndDate);
            //foreach (INFOTYPE2002 item in list)
            //{
            //    item.Status = "INPROCESS";
            //    oReturn.Add(item);
            //}
            list = HRTMManagement.CreateInstance(CompanyCode).GetSubstitutionList(employeeData.EmployeeID, BeginDate, EndDate);
            foreach (Substitution item in list)
            {
                DataTable oTable = item.ToADODataTable(false);
                INFOTYPE2003 newItem = new INFOTYPE2003();
                newItem.ParseToObject(oTable);
                newItem.Status = "RECORDED";
                oReturn.Add(newItem);
            }
            return oReturn;
        }

        #region ICollisionable Members

        public List<TimesheetData> LoadCollision(string EmployeeID, DateTime BeginDate, DateTime EndDate, string SubKey1, string SubKey2)
        {
            List<Substitution> list = Substitution.CreateInstance(CompanyCode).GetSubstitutionList(EmployeeID, BeginDate, EndDate, false);
            List<TimesheetData> oReturn = new List<TimesheetData>();
            foreach (Substitution item in list)
            {
                TimesheetData Data = new TimesheetData();
                Data.EmployeeID = item.EmployeeID;
                Data.ApplicationKey = "SUBSTITUTION";
                Data.BeginDate = item.BeginDate;
                Data.EndDate = item.EndDate;
                Data.FullDay = true;
                Data.SubKey1 = "";
                Data.SubKey2 = "";
                Data.Detail = item.RequestNo;
                oReturn.Add(Data);
            }
            return oReturn;
        }

        #endregion
    }
}
