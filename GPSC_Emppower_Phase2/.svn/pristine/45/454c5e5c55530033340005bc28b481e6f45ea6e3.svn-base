using System;
using System.Collections.Generic;
using System.Text;
using ESS.DATA;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.CONFIG;
using ESS.HR.TM.CONFIG;
using ESS.HR.TM.INFOTYPE;
using ESS.TIMESHEET;
using ESS.WORKFLOW;
using ESS.HR.TM;
using System.Data;
using ESS.DATA.ABSTRACT;
using ESS.DATA.EXCEPTION;
using Newtonsoft.Json;
using ESS.HR.TM.DATACLASS;

namespace ESS.HR.TM.DATASERVICE
{
    public class ChangeAttendance : AbstractDataService
    {
        public override object GenerateAdditionalData(EmployeeData Requestor, string ReferRequestNo, string CreateParam)
        {
            DataSet DS = new DataSet("ADDITIONAL");
            DataTable oTable;
            INFOTYPE2002 oItem = new INFOTYPE2002();
            oTable = oItem.ToADODataTable(true);
            oTable.TableName = "INFOTYPE2002";

            oTable.Clear();
            DS.Tables.Add(oTable);

            if(!string.IsNullOrEmpty(CreateParam))
            {
                INFOTYPE2002 attendance = new INFOTYPE2002();
                attendance = attendance.Parse(CreateParam);
                //attendance.ValidateData(documentDate);
                DataRow dr = DS.Tables["INFOTYPE2002"].NewRow();
                attendance.LoadDataToTableRow(dr);
                DS.Tables["INFOTYPE2002"].Rows.Add(dr);
            }
           
            return DS;     
        }

        public override void PrepareData(EmployeeData Requestor, object Data)
        {
            
        }

        public override void ValidateData(object ds, EmployeeData Requestor, string RequestNo, DateTime DocumentDate, int NoOfFileAttached)
        {
            DataSet newData = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            if (newData.Tables["INFOTYPE2002"].Rows.Count == 0)
            {
                throw new DataServiceException("HRTM_EXCEPTION", "NOT_FOUND_ORIGINAL_RECORD");
            }
            DataTable oAdditional = newData.Tables["INFOTYPE2002"];
            INFOTYPE2002 oItem = new INFOTYPE2002();
            oItem.ParseToObject(oAdditional.Rows[0]);
            try
            {
                #region " CheckCollision "
                if (newData.Tables.Contains("POSTDATA"))
                {
                    newData.Tables.Remove("POSTDATA");
                }
                if (oItem.AllDayFlag)
                {
                    HRTMManagement.CreateInstance(Requestor.CompanyCode).MarkTimesheet(Requestor.EmployeeID, "CANCELATTENDANCE", true, oItem.BeginDate, oItem.EndDate, true, true, RequestNo);
                }
                else
                {
                    HRTMManagement.CreateInstance(Requestor.CompanyCode).MarkTimesheet(Requestor.EmployeeID, "CANCELATTENDANCE", false, oItem.BeginDate.Add(oItem.BeginTime), oItem.EndDate.Add(oItem.EndTime), true, true, RequestNo);
                }
                #endregion

                // check existing
                if (!HRTMManagement.CreateInstance(Requestor.CompanyCode).CheckAttendanceExistingData(Requestor, oItem))
                {
                    throw new DataServiceException("HRTM_EXCEPTION", "NOT_FOUND_ORIGINAL_RECORD");
                }

            }
            catch (TimesheetCollisionException e)
            {
                throw new DataServiceException("HRTM_EXCEPTION", "COLLISION_OCCUR", "", e);
            }
            catch (Exception e)
            {
                string Err = HRTMManagement.CreateInstance(Requestor.CompanyCode).ShowError("HRTM_EXCEPTION", e.Message.ToString(), WorkflowPrinciple.Current.UserSetting.Language);
                throw new Exception(Err);
                //throw new DataServiceException("HRTM_EXCEPTION", e.Message, "");
            }
        }

        public override void SaveExternalData(EmployeeData Requestor, DataTable Info, object ds, string PreviousState, string State, string RequestNo, string Comment, string Comment2, string ActionCode)
        {
            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            DataTable oAdditional = Data.Tables["INFOTYPE2002"];
            INFOTYPE2002 oAttendance = new INFOTYPE2002();
            oAttendance.ParseToObject(oAdditional.Rows[0]);

            List<string> postList = new List<string>();
            postList.Add("COMPLETED");

            List<string> cancelList = new List<string>();
            cancelList.Add("CANCELLED");

            List<string> unmarkList = new List<string>();
            unmarkList.Add("CANCELLED");
            unmarkList.Add("COMPLETED");

            // check existing
            if (!cancelList.Contains(State.ToUpper()) && ((!HRTMManagement.CreateInstance(Requestor.CompanyCode).CheckAttendanceExistingData(Requestor, oAttendance)) && !oAttendance.IsPost)
                || (cancelList.Contains(State.ToUpper()) && (!HRTMManagement.CreateInstance(Requestor.CompanyCode).CheckAttendanceExistingData(Requestor, oAttendance))))
            {
                throw new Exception("Data for cancel can't been found");
            }

            List<INFOTYPE2002> postingList = new List<INFOTYPE2002>();
            if (postList.Contains(State))
            {
                #region " POST "
                // POST DATA
                try
                {
                    if (Data.Tables.Contains("POSTDATA"))
                    {
                        foreach (DataRow dr in Data.Tables["POSTDATA"].Rows)
                        {
                            INFOTYPE2002 item = new INFOTYPE2002();
                            item.ParseToObject(dr);
                            postingList.Add(item);
                        }
                        Data.Tables.Remove("POSTDATA");
                    }
                    else
                    {
                        postingList = new List<INFOTYPE2002>();
                        oAttendance.IsMark = false;
                        postingList.Add(oAttendance);
                    }
                    try
                    {
                        HRTMManagement.CreateInstance(Requestor.CompanyCode).SavePostAttendance(postingList);

                        try
                        {
                            foreach (INFOTYPE2002 data in postingList)
                            {
                                //HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveAttendanceFromSAP(data);   //Add by Koissares 20170822
                                //HRTMManagement.MappingTimePairAttendance(data.BeginDate, data.EndDate);

                                //List<ClockinLateClockoutEarly> ClockinLateClockoutEarlyList = HRTMManagement.CalculateTM_InLateOutEarly(data.EmployeeID, data.BeginDate, data.EndDate);
                                //if (ClockinLateClockoutEarlyList.Count > 0)
                                //{
                                //    HRTMManagement.SaveInLateOutEarly(ClockinLateClockoutEarlyList, data.EmployeeID, data.BeginDate, data.EndDate.AddDays(1).AddSeconds(-1));
                                //    HRTMManagement.MappingTimePairInLateOutEarlyID(data.EmployeeID, data.BeginDate, data.EndDate.AddDays(1).AddSeconds(-1));
                                //}
                            }
                        }
                        catch (Exception ex) { }
                    }
                    catch (Exception ex1)
                    {
                        throw ex1;
                    }
                    finally
                    {
                        INFOTYPE2002 item1 = new INFOTYPE2002();
                        DataTable oTempTable;
                        oTempTable = item1.ToADODataTable(true);
                        oTempTable.TableName = "POSTDATA";
                        Data.Tables.Add(oTempTable);
                        foreach (INFOTYPE2002 item in postingList)
                        {
                            DataRow oNewRow = oTempTable.NewRow();
                            item.LoadDataToTableRow(oNewRow);
                            oTempTable.LoadDataRow(oNewRow.ItemArray, false);
                        }
                    }

                    try
                    {
                        if (postList.Contains(State))
                        {
                            foreach (INFOTYPE2002 oAtt in postingList)
                            {
                                if(oAtt.BeginTime == TimeSpan.Zero && oAtt.EndTime == TimeSpan.Zero)
                                {
                                    oAtt.AllDayFlag = true;
                                }
                                var temp = HRTMManagement.CreateInstance(Requestor.CompanyCode).CalculateAttendanceEntry(Requestor, oAtt.SubType, oAtt.BeginDate, oAtt.EndDate, oAtt.BeginTime, oAtt.EndTime, oAtt.AllDayFlag); ;
                                oAtt.PayrollDays = temp.PayrollDays;
                                oAtt.AttendanceHours = temp.AbsenceHours;
                                oAtt.AttendanceDays = temp.AbsenceDays;
                                oAtt.RequestNo = RequestNo;
                            }

                            HRTMManagement.CreateInstance(Requestor.CompanyCode).SaveLG_TIMEREQUEST(postingList, "CHANGEATTENDANCE");
                        }
                    }
                    catch (Exception er)
                    {

                    }

                }
                catch (Exception ex)
                {
                    throw new SaveExternalDataException("SaveExternalData Error", true, ex);
                }
                #endregion
                
            }
            try
            {
                if (oAttendance.AllDayFlag)
                {
                    HRTMManagement.CreateInstance(Requestor.CompanyCode).MarkTimesheet(Requestor.EmployeeID, "CANCELATTENDANCE", oAttendance.AllDayFlag, oAttendance.BeginDate.Date, oAttendance.EndDate.Date, !unmarkList.Contains(State.ToUpper()), false, RequestNo);
                }
                else
                {
                    HRTMManagement.CreateInstance(Requestor.CompanyCode).MarkTimesheet(Requestor.EmployeeID, "CANCELATTENDANCE", oAttendance.AllDayFlag, oAttendance.BeginDate.Date.Add(oAttendance.BeginTime), oAttendance.EndDate.Add(oAttendance.EndTime), !unmarkList.Contains(State.ToUpper()), false, RequestNo);
                }
            }
            catch (Exception ex)
            {
                if (postList.Contains(State))
                    throw new SaveExternalDataException("SaveExternalData Error", true, ex);
                else
                    throw ex;
            }
        }

        public override void CalculateInfoData(EmployeeData Requestor, object ds, DataTable Info, string CurrentState, string ActionCode, DateTime SubmitDate)
        {
            DataSet Data = JsonConvert.DeserializeObject<DataSet>(ds.ToString());
            DataRow dr;
            DataRow dataRow = Data.Tables["INFOTYPE2002"].Rows[0];
            if (Info.Rows.Count == 0)
                dr = Info.NewRow();
            else
                dr = Info.Rows[0];

            INFOTYPE2002 item = new INFOTYPE2002();
            item.ParseToObject(dataRow);

            dr["ATTENDANCETYPECODE"] = string.Format("{0}#{1}", Requestor.OrgAssignment.SubAreaSetting.AbsAttGrouping, item.AttendanceType);
            dr["ATTENDANCEGROUP"] = Requestor.OrgAssignment.SubAreaSetting.AbsAttGrouping;
            dr["ATTENDANCETYPE"] = item.AttendanceType;
            dr["ATTENDANCEHOUR"] = item.AttendanceHours;
            dr["BEGINDATE"] = item.BeginDate;
            dr["ENDDATE"] = item.EndDate;
            dr["BEGINTIME"] = item.BeginTime.ToString();
            if (item.BeginTime > item.EndTime)
                dr["ENDTIME"] = item.EndTime.Add(new TimeSpan(1, 0, 0, 0)).ToString();
            else
                dr["ENDTIME"] = item.EndTime.ToString();

            if (dr.RowState == DataRowState.Detached)
                Info.Rows.Add(dr);
        }
    }
}
