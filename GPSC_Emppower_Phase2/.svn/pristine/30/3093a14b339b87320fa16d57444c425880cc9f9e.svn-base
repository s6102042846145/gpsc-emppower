using System;
using System.Collections.Generic;
using ESS.EMPLOYEE.CONFIG.OM;

namespace ESS.EMPLOYEE.INTERFACE
{
    public interface IOMService
    {
        List<ObjectTypeIDRange> GetObjectIDRange(ObjectType objectType);
        
        INFOTYPE1000 GetObjectData(ObjectType objectType, string objectID, DateTime CheckDate, string LanguageCode);

        List<INFOTYPE1000> GetAllObject(List<ObjectType> objectTypes);

        List<INFOTYPE1000> GetAllObject(List<ObjectType> objectTypes, DateTime dtCheckDate);

        List<INFOTYPE1000> GetAllObject(List<ObjectType> objectTypes, string Profile);

        List<INFOTYPE1000> GetAllObject(string objId1, string objId2, List<ObjectType> objectTypes, string Profile);

        List<INFOTYPE1513> GetAllJobIndex(string objId1, string objId2, string Profile);

        void SaveAllObject(List<ObjectType> objectTypes, List<INFOTYPE1000> data, string Profile);

        List<INFOTYPE1001> GetAllRelation(List<ObjectType> objectTypes, List<string> relationCodes);

        List<INFOTYPE1001> GetAllRelation(List<ObjectType> objectTypes, List<string> relationCodes, string Profile);

        List<INFOTYPE1001> GetAllRelation(string objId1, string objId2, List<ObjectType> objectTypes, List<string> relationCodes);

        List<INFOTYPE1001> GetAllRelation(string objId1, string objId2, List<ObjectType> objectTypes, List<string> relationCodes, string Profile);

        void SaveAllRelation(List<ObjectType> objectTypes, List<string> relationCodes, List<INFOTYPE1001> data, string Profile);

        List<INFOTYPE1000> GetOrgUnder(string OrgID, DateTime CheckDate, string LanguageCode);
        List<INFOTYPE1000> GetOrgUnit(string OrgID, bool IncludeSub);

        //List<INFOTYPE1000> GetRelationObjects(string objectID, ObjectType type, string relation, ObjectType nextObjectType, bool isReverse, string relation1, ObjectType nextObjectType1, bool isReverse1, DateTime CheckDate, string LanguageCode);
        INFOTYPE1000 GetOrgRoot(string OrgID, int Level, DateTime CheckDate, string LanguageCode);

        List<EmployeeData> GetEmployeeUnder(string OrgID, DateTime CheckDate);

        bool IsHaveSubOrdinate(string PositionID, DateTime CheckDate);

        List<INFOTYPE1000> GetAllPosition(string EmployeeID, DateTime CheckDate, string LanguageCode);

        List<INFOTYPE1000> GetAllPositionForHrMaster(DateTime CheckDate, string LanguageCode);

        List<INFOTYPE1000> GetAllPositionNoCheckDate(string LanguageCode);

        List<INFOTYPE1000> GetPositionByOrganize(string Organize, DateTime CheckDate, string LanguageCode);

        List<INFOTYPE1000> GetPositionByOrganizeForHRMaster(string Organize, DateTime CheckDate, string LanguageCode);

        INFOTYPE1000 GetOrgUnit(string PositionID, DateTime CheckDate, string Language);

        INFOTYPE1000 GetOrgParent(string OrgID, DateTime CheckDate);

        List<INFOTYPE1013> GetAllPositionBandLevel();

        List<INFOTYPE1013> GetAllPositionBandLevel(string Profile);

        List<INFOTYPE1013> GetAllPositionBandLevel(string objId1, string objId2, string Profile);

        void SaveAllPositionBandLevel(List<INFOTYPE1013> data, string Profile);
        void SaveAllJobIndex(List<INFOTYPE1513> data, string Profile);

        INFOTYPE1013 GetPositionBandLevel(string PositionID, DateTime CheckDate);

        EmployeeData GetEmployeeByPositionID(string PositionID, DateTime CheckDate);

        List<INFOTYPE1003> GetAllApprovalData();

        List<INFOTYPE1003> GetAllApprovalData(string Profile);

        List<INFOTYPE1003> GetAllApprovalData(string objId1, string objId2, string Profile);

        void SaveAllApprovalData(List<INFOTYPE1003> data, string Profile);

        INFOTYPE1003 GetApprovalData(string PositionID, DateTime CheckDate);

        INFOTYPE1000 FindNextPosition(string CurrentPositionID, DateTime CheckDate, string LanguageCode);

        List<EmployeeData> GetEmployeeByOrg(String OrgID, DateTime CheckDate);

        INFOTYPE1000 GetOrganizationByPositionID(string PositionID, string LanguageCode);

        //AddBy: Ratchatawan W. (2012-02-22)
        List<OrganizationWithLevel> GetAllOrganizationWithLevel(string Orgunit, string LanguageCode);

        //AddBy: Ratchatawan W. (2012-11-07)
        List<EmployeeData> GetEmployeeByOrg(String OrgID, DateTime BeginDate, DateTime EndDate);

        List<INFOTYPE1010> GetAllOrgUnitLevel(string ObjID1, string ObjID2, string Profile);

        void SaveOrgUnitLevel(List<INFOTYPE1010> data, string Profile);

        INFOTYPE1010 GetLevelByOrg(string ObjectID, DateTime CheckDate);

        INFOTYPE1000 GetJobByPositionID(string PositionID, DateTime CheckDate);

        List<string> GetAllRelationType();

        List<ObjectType> GetAllObjectType();
        List<ObjectTypeIDRange> GetAllObjectTypeIDRange();

        List<UserRoleResponse> GetUserRoleResponse();

        void SaveUserRoleResponseSnapshot(List<UserRoleResponseSnapShot> lstSnapshot);

        void CreateSnapshot();

        void CreateRequestFlowReceipientSnapshot();

        void MapEmpSubGroup();
        int GetBrandNoByEmpSubGroup(decimal EmployeeID);
    }
}