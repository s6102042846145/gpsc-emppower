using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Text;
using ESS.HR.TM;
using ESS.EMPLOYEE;
using ESS.WORKFLOW;

namespace ESS.DATA.DB
{
    public class HRTMREPORTSERVICE : AbstractHRTMReportService
    {
        #region " Private "
        private static Configuration __config;

        private static Configuration config
        {
            get
            {
                if (__config == null)
                {
                    __config = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().CodeBase.Replace("file:///", "").Replace("/", "\\"));
                }
                return __config;
            }
        }

        private string ConnectionString
        {
            get
            {
                if (config == null || config.AppSettings.Settings["BaseConnStr"] == null)
                {
                    return "";
                }
                else
                {
                    return config.AppSettings.Settings["BaseConnStr"].Value;
                }
            }
        }
        #endregion

        #region " SaveTimeResult "
        public override void SaveTimeResult(List<TimeResult> result)
        {
            List<string> deleteKey = new List<string>();
            string key = "";
            foreach (TimeResult item in result)
            {
                key = string.Format("{0}#{1}", item.EmployeeID, item.Period);
                if (deleteKey.Contains(key))
                {
                    continue;
                }
                deleteKey.Add(key);
            }

            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand;
            SqlCommand oCommand1;
            SqlParameter oParam;
            SqlParameter oParam1;
            DataTable oTable = null;
            oConnection.Open();

            try
            {
                oCommand1 = new SqlCommand("delete from TimeTypeTable where EmployeeID = @EmployeeID and Period = @Period", oConnection);
                oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
                oCommand1.Parameters.Add(oParam);

                oParam1 = new SqlParameter("@Period", SqlDbType.VarChar);
                oCommand1.Parameters.Add(oParam1);

                foreach (string item in deleteKey)
                {
                    string[] buffer = item.Split('#');
                    oParam.Value = buffer[0];
                    oParam1.Value = buffer[1];
                    oCommand1.ExecuteNonQuery();
                }


                oCommand = new SqlCommand("select * from TimeTypeTable", oConnection);

                SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
                SqlCommandBuilder oCB = new SqlCommandBuilder(oAdapter);
                oTable = new DataTable("TimeTypeTable");
                oAdapter.FillSchema(oTable, SchemaType.Source);

                foreach (TimeResult item in result)
                {
                    DataRow newRow = oTable.NewRow();
                    item.LoadDataToTableRow(newRow);
                    oTable.Rows.Add(newRow);
                }

                oAdapter.Update(oTable);
                oTable.Dispose();
                oCommand.Dispose();
                oCommand1.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oConnection.Close();
                oConnection.Dispose();
            }

        }
        #endregion

        #region " GetTimeResultFromPeriod "
        public override List<TimeResult> GetTimeResultFromPeriod(string employeeID, string period1, string period2)
        {
            List<TimeResult> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("select * from TimeTypeTable where Period between @period1 and @period2 and employeeID = @employeeID", oConnection);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            SqlParameter oParam;
            DataTable oTable = null;
            oParam = new SqlParameter("@period1", SqlDbType.VarChar);
            oParam.Value = period1;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@period2", SqlDbType.VarChar);
            oParam.Value = period2;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@employeeID", SqlDbType.VarChar);
            oParam.Value = employeeID;
            oCommand.Parameters.Add(oParam);

            oTable = new DataTable("TimeTypeTable");
            oAdapter.Fill(oTable);
            oAdapter.Dispose();
            oCommand.Dispose();
            oConnection.Dispose();
            if (oTable.Rows.Count > 0)
            {
                returnValue = new List<TimeResult>();
                List<UserRoleResponseSetting> roles = new List<UserRoleResponseSetting>();
                roles.Add(new UserRoleResponseSetting("#MANAGER", true));
                roles.Add(new UserRoleResponseSetting("EMPDATA_EXPORTER", true));
                roles.Add(new UserRoleResponseSetting("APPDEVELOPER", true));
                foreach (DataRow dr in oTable.Rows)
                {
                    TimeResult item = new TimeResult();
                    item.ParseToObject(dr);
                    if (WorkflowPrinciple.Current.UserSetting.Employee.IsUserInResponse(roles, item.EmployeeID))
                    {
                        returnValue.Add(item);
                    }
                }
            }
            return returnValue;
        }
        #endregion

        public override List<TimeResult> GetTimeResultFromPeriod1(List<string> employeeList, string period1, string period2)
        {
            List<TimeResult> returnValue = null;
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            StringBuilder oSB = new StringBuilder("select EmployeeID,TimeType, Sum(Amount) As Amount from TimeTypeTable where Period between @period1 and @period2");
            SqlCommand oCommand = new SqlCommand("", oConnection);
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            SqlParameter oParam;
            DataTable oTable = null;
            oParam = new SqlParameter("@period1", SqlDbType.VarChar);
            oParam.Value = period1;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@period2", SqlDbType.VarChar);
            oParam.Value = period2;
            oCommand.Parameters.Add(oParam);


            oTable = new DataTable("TimeTypeTable");

            oSB.AppendLine();
            oSB.AppendLine("group by EmployeeID, TimeType");
            oCommand.CommandText = oSB.ToString();
            oAdapter.Fill(oTable);
            oAdapter.Dispose();
            oCommand.Dispose();
            oConnection.Dispose();
            if (oTable.Rows.Count > 0)
            {
                returnValue = new List<TimeResult>();
                //List<UserRoleResponseSetting> roles = new List<UserRoleResponseSetting>();
                //roles.Add(new UserRoleResponseSetting("#MANAGER", true));
                foreach (DataRow dr in oTable.Rows)
                {
                    TimeResult item = new TimeResult();
                    item.ParseToObject(dr);
                    if (employeeList.Contains(item.EmployeeID))
                    { 
                        returnValue.Add(item);
                    }
                }
            }
            return returnValue;
        }

        public override DataTable GetOTLog(int Month, int Year, string EmployeeList)
        {
            DataTable oReturn = new DataTable("OTLOG");
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("sp_GetOTLog", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter oParam;
            oParam = new SqlParameter("@p_Month", SqlDbType.Int);
            oParam.Value = Month;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_Year", SqlDbType.Int);
            oParam.Value = Year;
            oCommand.Parameters.Add(oParam);

            DataTable dtData = new DataTable("OTLOG");
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.Fill(dtData);
            oConnection.Close();

            DataRow[] drs = dtData.Select(string.Format("EmployeeID in ({0})", EmployeeList));
            oReturn = dtData.Clone();
            foreach (DataRow dr in drs)
                oReturn.ImportRow(dr);

            return oReturn;
        }

    }
}
