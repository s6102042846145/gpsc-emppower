using System;
using System.Collections.Generic;
using System.Text;
using ESS.JOB;

namespace ESS.HR.TM.JOB
{
    public class SynchronizeTimeTypeJob : AbstractJobWorker
    {
        public override List<ITaskWorker> LoadTasks()
        {
            List<ITaskWorker> oReturn = new List<ITaskWorker>();

            Console.WriteLine("Try to synchronize TimeType data.");

            SynchronizeTimeTypeTask ta = new SynchronizeTimeTypeTask();
            oReturn.Add(ta);

            return oReturn;
        }

    }
}
