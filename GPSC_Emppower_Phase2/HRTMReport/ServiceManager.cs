using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Reflection;

namespace ESS.HR.TM
{
    public class ServiceManager
    {
        #region " privatedata "
        private static Configuration __config;
        private ServiceManager()
        {
        }
        private static Configuration config
        {
            get
            {
                if (__config == null)
                {
                    __config = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().CodeBase.Replace("file:///", "").Replace("/", "\\"));
                }
                return __config;
            }
        }

        private static string DATAMODE
        {
            get
            {
                if (config == null || config.AppSettings.Settings["DATAMODE"] == null)
                {
                    return "DB";
                }
                else
                {
                    return config.AppSettings.Settings["DATAMODE"].Value;
                }
            }
        }

        private static string SOURCEMODE
        {
            get
            {
                if (config == null || config.AppSettings.Settings["SOURCEMODE"] == null)
                {
                    return "SAP";
                }
                else
                {
                    return config.AppSettings.Settings["SOURCEMODE"].Value;
                }
            }
        }
        private static Type GetService(string Mode)
        {
            Type oReturn = null;
            Assembly oAssembly;
            string assemblyName = string.Format("ESS.HR.TM.REPORT.{0}", Mode.ToUpper());
            string typeName = string.Format("ESS.DATA.{0}.HRTMREPORTSERVICE", Mode.ToUpper());
            oAssembly = Assembly.Load(assemblyName);
            oReturn = oAssembly.GetType(typeName);
            return oReturn;
        }
        #endregion

        internal static IHRTMReportService HRTMReportService
        {
            get
            {
                Type oType = GetService(DATAMODE);
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IHRTMReportService)Activator.CreateInstance(oType);
                }
            }
        }

        internal static IHRTMReportService HRTMReportSource
        {
            get
            {
                Type oType = GetService(SOURCEMODE);
                if (oType == null)
                {
                    return null;
                }
                else
                {
                    return (IHRTMReportService)Activator.CreateInstance(oType);
                }
            }
        }
    }
}
