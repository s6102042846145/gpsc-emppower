using System;
using System.Collections.Generic;
using System.Text;
using ESS.DATA;

namespace ESS.HR.TM
{
    public class TimeResult : AbstractObject
    {
        private string __empID = "";
        private string __period = "";
        private string __timetype = "";
        private decimal __amount = 0.0M;
        public TimeResult()
        { 
        }

        public string EmployeeID
        {
            get
            {
                return __empID;
            }
            set
            {
                __empID = value; 
            }
        }

        public string Period
        {
            get
            {
                return __period;
            }
            set
            {
                __period = value;
            }
        }

        public string TimeType
        {
            get
            {
                return __timetype;
            }
            set
            {
                __timetype = value;
            }
        }

        public decimal Amount
        {
            get
            {
                return __amount;
            }
            set
            {
                __amount = value;
            }
        }
    }
}
