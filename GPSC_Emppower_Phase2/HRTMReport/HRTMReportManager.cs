using System;
using System.Collections.Generic;
using System.Text;
using ESS.EMPLOYEE;
using ESS.WORKFLOW;
using System.Data;

namespace ESS.HR.TM
{
    public class HRTMReportManager
    {
        public static List<TimeResult> GetTimeResultFromLastUpdate(DateTime LastUpdate, DateTime UpdateTo)
        {
            return ServiceManager.HRTMReportSource.GetTimeResultFromLastUpdate(LastUpdate, UpdateTo);
        }

        public static void CopyFromLastUpdate(DateTime LastUpdate, DateTime UpdateTo)
        {
            ServiceManager.HRTMReportService.SaveTimeResult(ServiceManager.HRTMReportSource.GetTimeResultFromLastUpdate(LastUpdate, UpdateTo));
        }

        public static List<TimeResult> GetTimeResultFromPeriod(string period1, string period2)
        {
            List<UserRoleResponseSetting> roles = new List<UserRoleResponseSetting>();
            roles.Add(new UserRoleResponseSetting("#MANAGER", true));
            roles.Add(new UserRoleResponseSetting("HROTVIEWER", true));
            string curEmpID = WorkflowPrinciple.Current.UserSetting.Employee.EmployeeID;
            List<EmployeeData> emps = WorkflowPrinciple.Current.UserSetting.Employee.LoadUserResponsible(roles);
            List<TimeResult> returnValue = new List<TimeResult>();
            List<TimeResult> buffer = new List<TimeResult>();
            List<string> empList = new List<string>();
            foreach (EmployeeData emp in emps)
            {
                empList.Add(emp.EmployeeID);
                //buffer = ServiceManager.HRTMReportService.GetTimeResultFromPeriod(emp.EmployeeID, period1, period2);
                //if (buffer != null)
                //{
                //    returnValue.AddRange(buffer);
                //}
            }
            if (empList.Count == 0 && !empList.Contains(curEmpID))
            {
                empList.Add(curEmpID);
            }
            returnValue = ServiceManager.HRTMReportService.GetTimeResultFromPeriod1(empList, period1, period2);
            return returnValue;
        }

        public static DataTable GetOTLog(int Month, int Year, string EmployeeList)
        {
            return ServiceManager.HRTMReportService.GetOTLog(Month, Year, EmployeeList);
        }
    }
}
