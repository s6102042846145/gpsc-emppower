using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace ESS.HR.TM
{
    public class AbstractHRTMReportService : IHRTMReportService
    {

        #region IHRTMReportService Members

        public virtual List<TimeResult> GetTimeResult(string EmployeeID)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<TimeResult> GetTimeResultFromLastUpdate(DateTime LastUpdate, DateTime UpdateTo)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual void SaveTimeResult(List<TimeResult> result)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<TimeResult> GetTimeResultFromPeriod(string employeeID ,string period1, string period2)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public virtual List<TimeResult> GetTimeResultFromPeriod1(List<string> employeeList, string period1, string period2)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public virtual DataTable GetOTLog(int Month, int Year, string EmployeeList)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion
    }
}
