using System;
using System.Collections.Generic;
using System.Text;
using ESS.JOB;

namespace ESS.HR.TM
{
    public class SynchronizeTimeTypeTask : AbstractTaskWorker
    {

        public override void Run()
        {
            DateTime n = DateTime.Now.Date.AddDays(2);
            // warranty n is date in current month ---> job run every of month
            DateTime d = new DateTime(n.Year, n.Month, 1);
            // get first date


            Console.WriteLine("Try to synchronize TimeType from SAP");
            HRTMReportManager.CopyFromLastUpdate(d.AddMonths(-1), d.AddMonths(1));
        }
    }
}
