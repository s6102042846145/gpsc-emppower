﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Globalization;
using ESS.HR.PY.ABSTRACT;
using ESS.HR.PY.DATACLASS;
using ESS.HR.PY.INFOTYPE;
using ESS.HR.PY.CONFIG;
using ESS.SHAREDATASERVICE;
using SAP.Connector;
using SAPInterface;
using ESS.DATA;
using ESS.HR.PA;
using ESS.HR.PA.INFOTYPE;
using AbsAttCalculateInterface;
using ESS.EMPLOYEE.DATACLASS;
using System.Security.Cryptography;
using System.Text;
using System.IO;

namespace ESS.HR.PY.SAP
{
    public class HRPYDataServiceImpl : AbstractHRPYDataService
    {
        CultureInfo oCL = new CultureInfo("en-US");
        #region Constructor
        public HRPYDataServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
            //Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID);
        }

        #endregion Constructor

        #region Member
        private CultureInfo DefaultCultureInfo = CultureInfo.GetCultureInfo("en-GB");
        //private Dictionary<string, string> Config { get; set; }

        private static string ModuleID = "ESS.HR.PY.SAP";
        public string CompanyCode { get; set; }
        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }
        private string Sap_Version
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "SAP_VERSION");


            }
        }

        #endregion Member


        public override PayslipResult GetPayslip(string EmployeeID, PayslipPeriodSetting Period)
        {
            PayslipResult oReturn = new PayslipResult();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            PYInterface oFunction = new PYInterface();
            oFunction.Connection = oConnection;

            ZHRPYS001Table oCurrent = new ZHRPYS001Table();
            ZHRPYS001Table oRetro = new ZHRPYS001Table();
            ZHRPYS002Table oTextNote = new ZHRPYS002Table();
            ZHRPYS003Table oWageType = new ZHRPYS003Table();
            ZHRPYS003Table oWageTypeSum = new ZHRPYS003Table();

            ZHRPYS003 item;
            item = new ZHRPYS003();
            item.Wagetype = "/101";     //เงินได้สะสม
            oWageTypeSum.Add(item);

            item = new ZHRPYS003();
            item.Wagetype = "/102";     //เงินได้สะสม regular income
            oWageTypeSum.Add(item);

            item = new ZHRPYS003();
            item.Wagetype = "/103";     //เงินได้สะสม irregular income
            oWageTypeSum.Add(item);

            item = new ZHRPYS003();
            item.Wagetype = "/401";     //ภาษีสะสม
            oWageTypeSum.Add(item);

            item = new ZHRPYS003();
            item.Wagetype = "/481";
            oWageTypeSum.Add(item);

            item = new ZHRPYS003();
            item.Wagetype = "/485";
            oWageTypeSum.Add(item);

            item = new ZHRPYS003();
            item.Wagetype = "/321";
            oWageTypeSum.Add(item);

            item = new ZHRPYS003();
            item.Wagetype = "/301";
            oWageTypeSum.Add(item);

            //item = new ZHRPYS003();
            //item.Wagetype = "/323";         // เงินกองทุนสำรองฯ สะสมในส่วนบริษัท
            //oWageType.Add(item);

            //item = new ZHRPYS003();
            //item.Wagetype = "4700";
            //oWageType.Add(item);

            //item = new ZHRPYS003();
            //item.Wagetype = "4701";
            //oWageType.Add(item);

            string periodCode;
            string offcycleFlag = "";
            if (Period.IsOffCycle)
            {
                offcycleFlag = "X";
                periodCode = Period.OffCycleDate.ToString("yyyyMMdd", oCL);
            }
            else
            {
                offcycleFlag = "";
                periodCode = string.Format("{0}{1}", Period.PeriodYear.ToString("0000"), Period.PeriodMonth.ToString("00"));
            }

            oFunction.Zhrpyi010(EmployeeID, offcycleFlag, periodCode,
                                        ref oCurrent, ref oTextNote, ref oRetro, ref oWageType, ref oWageTypeSum);

            #region " Parse Object "

            oReturn.SetCurrentRT(oCurrent.ToADODataTable());
            DataTable oTable = oRetro.ToADODataTable();
            oTable.Columns.Add("PERIOD", typeof(string));
            string cWageType;
            string cPeriod = "";
            List<DataRow> delList = new List<DataRow>();
            foreach (DataRow dr in oTable.Rows)
            {
                cWageType = (string)dr["WAGETYPE"];
                if (cWageType == "XXXX")
                {
                    cPeriod = (string)dr["WAGETYPETEXT"];
                    delList.Add(dr);
                }
                dr["PERIOD"] = cPeriod;
            }
            foreach (DataRow dr in delList)
            {
                dr.Delete();
            }
            oReturn.SetRetroRT(oTable);
            oReturn.SetTextNote(oTextNote.ToADODataTable());
            #endregion

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            return oReturn;
        }


        public override PayslipResult GetPayslipByMonthYear(string EmployeeID, string CompCode, PayslipPeriodSetting Period, string AccumType, string NotSummarizeRT)
        {
            PayslipResult oReturn = new PayslipResult();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);

            AbsAttCalculate oFunction = new AbsAttCalculate();
            oFunction.Connection = oConnection;

            ZHRPYS031_LTYPE oEt_Return = new ZHRPYS031_LTYPE();
            ZHRPYS005_INPUT_LTYPE oInput_LType = new ZHRPYS005_INPUT_LTYPE();

            ZHRPYS005_INPUT item = new ZHRPYS005_INPUT();
            item.Employeeid = EmployeeID;
            oInput_LType.Add(item);

            int iCompCode = int.Parse(CompCode);

            string periodCode;
            string offcycleFlag = "";
            if (Period.IsOffCycle)
            {
                offcycleFlag = "X";
                periodCode = Period.OffCycleDate.ToString("yyyyMMdd", oCL);
            }
            else
            {
                offcycleFlag = "";
                periodCode = string.Format("{0}{1}", Period.PeriodYear.ToString("0000"), Period.PeriodMonth.ToString("00"));
            }

            oFunction.Zhrpyi031(AccumType, iCompCode.ToString(), oInput_LType, NotSummarizeRT, offcycleFlag, periodCode, out oEt_Return);

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            if (oEt_Return.Count > 0)
            {
                for (int i = 0; i < oEt_Return.Count; i++)
                {
                    string Accumtype = oEt_Return[i].Accumtype;
                    decimal Currencyamt = oEt_Return[i].Currencyamt;
                    string Currencyunit = oEt_Return[i].Currencyunit;
                    string Employeeid = oEt_Return[i].Employeeid;
                    string Forperiod = oEt_Return[i].Forperiod;
                    string Inperiod = oEt_Return[i].Inperiod;
                    string Offcycle_Flag = oEt_Return[i].Offcycle_Flag;
                    string Pydate = oEt_Return[i].Pydate;
                    decimal Timeamt = oEt_Return[i].Timeamt;
                    string Timeunit = oEt_Return[i].Timeunit;
                    string Type = oEt_Return[i].Type;
                    string Wagetype = oEt_Return[i].Wagetype;
                    string Wagetypetext = oEt_Return[i].Wagetypetext;
                }
            }

            return oReturn;
        }


        #region "TaxAllowance"

        public override TaxAllowance GetTaxAllowanceData(string EmployeeID, DateTime CheckDate)
        {
            CultureInfo oCL_ENUS = new CultureInfo("en-US");
            TaxAllowance returnValue = new TaxAllowance();
            Connection oConnect = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnect;

            TAB512Table oTable = new TAB512Table();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();

            RFC_DB_OPT opt;
            RFC_DB_FLD fld;

            #region " Fields "
            //0
            fld = new RFC_DB_FLD();
            fld.Fieldname = "CHNO1";
            Fields.Add(fld);
            //1
            fld = new RFC_DB_FLD();
            fld.Fieldname = "CHNO3";
            Fields.Add(fld);
            //2
            fld = new RFC_DB_FLD();
            fld.Fieldname = "MGINT";
            Fields.Add(fld);
            //3
            fld = new RFC_DB_FLD();
            fld.Fieldname = "SPINS";
            Fields.Add(fld);
            //4
            fld = new RFC_DB_FLD();
            fld.Fieldname = "MFINV";
            Fields.Add(fld);
            //5
            fld = new RFC_DB_FLD();
            fld.Fieldname = "CHAMT";
            Fields.Add(fld);
            //6
            fld = new RFC_DB_FLD();
            fld.Fieldname = "CHEDU";
            Fields.Add(fld);
            //7
            fld = new RFC_DB_FLD();
            fld.Fieldname = "LPREM";
            Fields.Add(fld);
            //8
            fld = new RFC_DB_FLD();
            fld.Fieldname = "LTEQF";
            Fields.Add(fld);
            //9
            fld = new RFC_DB_FLD();
            fld.Fieldname = "FCNTR";
            Fields.Add(fld);
            //10
            fld = new RFC_DB_FLD();
            fld.Fieldname = "MCNTR";
            Fields.Add(fld);
            //11
            fld = new RFC_DB_FLD();
            fld.Fieldname = "SFCTR";
            Fields.Add(fld);
            //12
            fld = new RFC_DB_FLD();
            fld.Fieldname = "SMCTR";
            Fields.Add(fld);
            //13
            fld = new RFC_DB_FLD();
            fld.Fieldname = "SPRCTR";
            Fields.Add(fld);
            //14
            fld = new RFC_DB_FLD();
            fld.Fieldname = "FPRINS";
            Fields.Add(fld);
            //15
            fld = new RFC_DB_FLD();
            fld.Fieldname = "MPRINS";
            Fields.Add(fld);
            //16
            fld = new RFC_DB_FLD();
            fld.Fieldname = "SFPINS";
            Fields.Add(fld);
            //17
            fld = new RFC_DB_FLD();
            fld.Fieldname = "SMPINS";
            Fields.Add(fld);
            //18
            fld = new RFC_DB_FLD();
            fld.Fieldname = "TAXID";
            Fields.Add(fld);
            //19
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ALLTY";
            Fields.Add(fld);
            //20
            fld = new RFC_DB_FLD();
            fld.Fieldname = "SPALL";
            Fields.Add(fld);
            //21
            fld = new RFC_DB_FLD();
            fld.Fieldname = "DINO1";
            Fields.Add(fld);
            //22
            fld = new RFC_DB_FLD();
            fld.Fieldname = "HAHIR";
            Fields.Add(fld);
            //23
            fld = new RFC_DB_FLD();
            fld.Fieldname = "PENSN";
            Fields.Add(fld);
            //24
            fld = new RFC_DB_FLD();
            fld.Fieldname = "IMMEXP";
            Fields.Add(fld);
            //25
            fld = new RFC_DB_FLD();
            fld.Fieldname = "LPNSN";
            Fields.Add(fld);
            //2
            fld = new RFC_DB_FLD();
            fld.Fieldname = "TUREXM";
            Fields.Add(fld);
            #endregion

            #region " Options "
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR = '{0}'", EmployeeID);
            Options.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND BEGDA <= '{0}' and ENDDA >= '{0}'", CheckDate.ToString("yyyyMMdd", oCL_ENUS));
            Options.Add(opt);
            #endregion

            oFunction.Zrfc_Read_Table("^", "", "PA0364", 0, 0, ref oTable, ref Fields, ref Options);

            Connection.ReturnConnection(oConnect);
            oFunction.Dispose();

            foreach (TAB512 item in oTable)
            {
                string[] buffer = item.Wa.Split('^');
                int intVal = 0;
                int.TryParse(buffer[0].Trim(), out intVal);
                returnValue.ChdAllow = intVal;
                int.TryParse(buffer[1].Trim(), out intVal);
                returnValue.ChdEduAllow = intVal;
                returnValue.Mortgage = ConvertTextToDecimal(buffer[2].Trim());
                returnValue.SpouseIns = ConvertTextToDecimal(buffer[3].Trim());
                returnValue.MFund = ConvertTextToDecimal(buffer[4].Trim());
                returnValue.Charity = ConvertTextToDecimal(buffer[5].Trim());
                returnValue.CharityEdu = ConvertTextToDecimal(buffer[6].Trim());
                returnValue.LifeIns = ConvertTextToDecimal(buffer[7].Trim());
                returnValue.LongTerm = ConvertTextToDecimal(buffer[8].Trim());
                returnValue.AllowFather = ConvertTextToDecimal(buffer[9].Trim());
                returnValue.AllowMother = ConvertTextToDecimal(buffer[10].Trim());
                returnValue.SpAllowFather = ConvertTextToDecimal(buffer[11].Trim());
                returnValue.SpAllowMother = ConvertTextToDecimal(buffer[12].Trim());
                returnValue.SportContribution = ConvertTextToDecimal(buffer[13].Trim());
                returnValue.FatherIns = ConvertTextToDecimal(buffer[14].Trim());
                returnValue.MotherIns = ConvertTextToDecimal(buffer[15].Trim());
                returnValue.SpFatherIns = ConvertTextToDecimal(buffer[16].Trim());
                returnValue.SpMotherIns = ConvertTextToDecimal(buffer[17].Trim());
                returnValue.TaxID = buffer[18].Trim();
                returnValue.AllowanceType = buffer[19].Trim();
                returnValue.SpouseAllowance = buffer[20].Trim();

                int.TryParse(buffer[21].Trim(), out intVal);
                returnValue.ChdDisAllow = intVal;

                returnValue.HouseAllowanceHire = ConvertTextToDecimal(buffer[22].Trim());
                returnValue.PensionFundInvestment = ConvertTextToDecimal(buffer[23].Trim());
                returnValue.ExpenseimmovableProperty = ConvertTextToDecimal(buffer[24].Trim());
                returnValue.LifeInsurancePension = ConvertTextToDecimal(buffer[25].Trim());
                returnValue.TourismExemption = ConvertTextToDecimal(buffer[26].Trim());

                returnValue.EmployeeID = EmployeeID;
            }
            return returnValue;
        }

        public override void SaveTaxallowanceData(TaxAllowance saved)
        {

            Connection oConnect = Connection.GetConnectionFromPool(BaseConnStr);
            UPLOADINFOTYPE6 oFunction = new UPLOADINFOTYPE6();
            oFunction.Connection = oConnect;

            ZHRHRS001Table Updatetab = new ZHRHRS001Table();
            ZHRHRS001Table Updatetab_Ret = new ZHRHRS001Table();

            ZHRHRS001 item = new ZHRHRS001();
            //item.Begda = saved.BeginDate.ToString("yyyyMMdd", oCL);
            //item.Endda = saved.EndDate.ToString("yyyyMMdd", oCL);


            DateTime lastDayOfTheYear = new DateTime(9999, 12, 31);
            item.Begda = DateTime.Now.ToString("yyyyMMdd", oCL);
            item.Endda = lastDayOfTheYear.ToString("yyyyMMdd", oCL);


            item.Infty = "0364";
            item.Msg = "";
            item.Objps = saved.SpouseAllowance;
            item.Operation = "INS";
            item.Pernr = saved.EmployeeID;
            //เป็นการเอาเลขเครื่องมาคำนวณเพื่อใช้ป้องกันข้อมูลซ้ำกัน คือมันจะ gen key ที่ไม่มีทางซ้ำกันได้เลย
            item.Reqnr = Guid.NewGuid().ToString();
            item.Seqnr = "";
            item.Status = "N";
            item.Subty = "0001";
            //T ต่าง ๆ เอามาจาก function ที่อยู่ใน sap se37
            item.T01 = saved.Mortgage.ToString();
            item.T02 = saved.SpouseIns.ToString();
            item.T03 = saved.MFund.ToString();
            item.T04 = saved.Charity.ToString();
            item.T05 = saved.CharityEdu.ToString();
            item.T06 = saved.LifeIns.ToString();
            item.T07 = saved.LongTerm.ToString();
            item.T08 = saved.AllowFather.ToString();
            item.T09 = saved.AllowMother.ToString();
            item.T10 = saved.SpAllowFather.ToString();
            item.T11 = saved.SpAllowMother.ToString();
            item.T13 = saved.SportContribution.ToString();
            item.T14 = saved.FatherIns.ToString();
            item.T15 = saved.MotherIns.ToString();
            item.T16 = saved.SpFatherIns.ToString();
            item.T17 = saved.SpMotherIns.ToString();
            item.T18 = saved.ChdAllow.ToString();
            item.T19 = saved.ChdAllow.ToString();

            //item.T19 = saved.ChdEduAllow.ToString();
            item.T20 = saved.TaxID;

            //AddBy:Ratchatawan W. (2012-01-18)

            item.T21 = saved.ChdDisAllow.ToString();
            item.T22 = saved.HouseAllowanceHire.ToString();
            item.T23 = saved.PensionFundInvestment.ToString();
            item.T24 = saved.ExpenseimmovableProperty.ToString();
            item.T25 = saved.LifeInsurancePension.ToString();
            item.T26 = saved.TourismExemption.ToString();

            Updatetab.Add(item);
            oFunction.Zhrhri001(ref Updatetab, ref Updatetab_Ret);
            Connection.ReturnConnection(oConnect);
            oFunction.Dispose();
            if (Updatetab_Ret[0].Msgtype == "E")
            {
                throw new Exception(Updatetab_Ret[0].Msg);
            }
        }

        private decimal ConvertTextToDecimal(string text)
        {
            decimal decVal = 0;
            if (text.Contains("*"))
                return 1000000;
            else
            {
                decimal.TryParse(text, out decVal);
                return decVal;
            }
        }
        #endregion

        #region "ProvidentFund"
        public override INFOTYPE0366 GetProvidentFund(string EmployeeID, DateTime CheckDate)
        {
            CultureInfo oCL = new CultureInfo("en-US");
            INFOTYPE0366 returnValue = new INFOTYPE0366();
            Connection oConnect = Connection.GetConnectionFromPool(BaseConnStr);
            BNInterface oFunction = new BNInterface();
            oFunction.Connection = oConnect;

            string strEmployeeIndicator = string.Empty;
            decimal decEmployeePercentage = 0;
            string strEmployerIndicator = string.Empty;
            decimal decEmployerPercentage = 0;
            string strProvidentFundNumber = string.Empty;

            oFunction.Zhrbni012(CheckDate.ToString("yyyyMMdd", oCL), EmployeeID, out strEmployeeIndicator, out decEmployeePercentage, out strEmployerIndicator, out decEmployerPercentage, out strProvidentFundNumber);

            #region "Parse object"
            returnValue.EmployeeIndicator = strEmployeeIndicator;
            returnValue.EmployeePercentage = decEmployeePercentage;
            returnValue.EmployerIndicator = strEmployerIndicator;
            returnValue.EmployerPercentage = decEmployerPercentage;
            returnValue.ProvidentFundNumber = strProvidentFundNumber;
            #endregion

            Connection.ReturnConnection(oConnect);
            oFunction.Dispose();

            return returnValue;
        }

        public override void SaveProvidentFund(INFOTYPE0366 data)
        {
            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            UPLOADINFOTYPE6 oFunction = new UPLOADINFOTYPE6();

            oFunction.Connection = oConnection;

            ZHRHRS001Table Updatetab = new ZHRHRS001Table();

            ZHRHRS001Table Updatetab_Ret = new ZHRHRS001Table();

            #region " set item "
            ZHRHRS001 item = new ZHRHRS001();
            item.Pernr = data.EmployeeID;
            item.Begda = data.BeginDate.ToString("yyyyMMdd", oCL);
            item.Endda = "99991231";
            item.Infty = "0366";
            item.Operation = "INS";
            item.T01 = data.EmployeeID.Substring(2, 6) + data.ProvidentFundNumber;
            item.T02 = data.EmployeeIndicator.Equals("C") ? "X" : string.Empty;
            item.T03 = data.EmployeeIndicator.Equals("P") ? "X" : string.Empty;
            item.T04 = data.EmployeeIndicator.Equals("P") ? data.EmployeePercentage.ToString() : string.Empty;
            Updatetab.Add(item);
            #endregion

            oFunction.Zhrhri001(ref Updatetab, ref Updatetab_Ret);

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            //เพิ่ม Code เช็ค Error ตรงนี้
            bool lError = false;
            foreach (ZHRHRS001 result in Updatetab_Ret)
            {
                if (result.Msgtype == "E")
                {
                    lError = true;
                    data.ErrorMessage = result.Msg;
                }
            }

            if (lError)
            {
                throw new Exception("Post data error");
            }
        }

        public override List<INFOTYPE0021> GetProvidentFundMember(string EmployeeID)
        {

            List<INFOTYPE0021> returnValue = new List<INFOTYPE0021>();
            Connection oConnect = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnect;

            TAB512Table oTable = new TAB512Table();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();

            TAB512Table oTable2 = new TAB512Table();
            RFC_DB_OPTTable Options2 = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields2 = new RFC_DB_FLDTable();

            RFC_DB_OPT opt, opt2;
            RFC_DB_FLD fld, fld2;

            #region " Fields "

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FAMSA";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OBJPS";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FNMZU";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FAVOR";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FANAM";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FGBDT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FASEX";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FGBOT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FGBLD";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "FANAT";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SUBTY";
            Fields.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OBJPS";
            Fields.Add(fld);

            //Get Begda
            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            //Get Endda
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            #region INF0187

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "FTXID"; //Identity Number for Father
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "MTXID"; //Identity Number for Mother
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "STXID"; //Identity Number for Spouse
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "SFXID"; //Identity Number for Spouse's Father
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "SMXID"; //Identity Number for Spouse's Mother
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "ANRED"; //Form-of-Address Key
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "EFAML"; //Employer of Family Member (TH)
            Fields2.Add(fld2);

            fld2 = new RFC_DB_FLD();
            fld2.Fieldname = "JOBTL"; //Job Title
            Fields2.Add(fld2);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "SUBTY"; //Subtype
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "OBJPS"; //Object Identification
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ALDEC"; //Deceased Indicator
            Fields2.Add(fld);

            //Addby:Nuttapon.a 11.02.2013 adress
            fld = new RFC_DB_FLD();
            fld.Fieldname = "STRAS"; // Address Number, Soi Trok (TH)
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "LOCAT"; //Street/Road, Tambol
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ORT02"; // District (TH)
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "ORT01"; // Province
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "PSTLZ"; // Postal Code
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "LAND1"; // Country Key
            Fields2.Add(fld);

            fld = new RFC_DB_FLD();
            fld.Fieldname = "TELNR"; // Telephone Number
            Fields2.Add(fld);

            #endregion INF0187

            #endregion " Fields "

            #region " Options "

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR = '{0}' AND SUBTY = '6'", EmployeeID);
            Options.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND BEGDA <= '{0}' and ENDDA > '{0}'", DateTime.Now.ToString("yyyyMMdd", DefaultCultureInfo));
            Options.Add(opt);

            opt2 = new RFC_DB_OPT();
            opt2.Text = string.Format("PERNR = '{0}'", EmployeeID);
            Options2.Add(opt2);
            opt2 = new RFC_DB_OPT();
            opt2.Text = string.Format("AND BEGDA <= '{0}' and ENDDA > '{0}'", DateTime.Now.ToString("yyyyMMdd", DefaultCultureInfo));
            Options2.Add(opt2);

            #endregion " Options "

            oFunction.Zrfc_Read_Table("^", "", "PA0021", 0, 0, ref oTable, ref Fields, ref Options);
            oFunction.Zrfc_Read_Table("^", "", "PA0187", 0, 0, ref oTable2, ref Fields2, ref Options2);

            Connection.ReturnConnection(oConnect);
            oFunction.Dispose();
            foreach (TAB512 item in oTable)
            {
                string tempSub1, temp1;
                INFOTYPE0021 IF21 = new INFOTYPE0021();
                string[] buffer = item.Wa.Split('^');
                IF21.EmployeeID = EmployeeID;
                IF21.FamilyMember = buffer[0].Trim();
                IF21.ChildNo = buffer[1].Trim();
                IF21.TitleRank = buffer[2].Trim();
                IF21.Name = buffer[3].Trim();
                IF21.Surname = buffer[4].Trim();
                DateTime bufferDate;
                DateTime.TryParseExact(buffer[5].Trim(), "yyyyMMdd", DefaultCultureInfo, DateTimeStyles.None, out bufferDate);
                IF21.BirthDate = bufferDate;
                IF21.Sex = buffer[6].Trim();
                IF21.BirthPlace = buffer[7].Trim();
                IF21.CityOfBirth = buffer[8].Trim();
                IF21.Nationality = buffer[9].Trim();
                IF21.SubType = buffer[10].Trim();
                IF21.Age = 0;
                //Addby:Nuttapon.a 07.02.2013
                DateTime oTempDatetime;
                DateTime.TryParseExact(buffer[12].Trim(), "yyyyMMdd", DefaultCultureInfo, DateTimeStyles.None, out oTempDatetime);
                IF21.BeginDate = oTempDatetime;
                DateTime.TryParseExact(buffer[13].Trim(), "yyyyMMdd", DefaultCultureInfo, DateTimeStyles.None, out oTempDatetime);
                IF21.EndDate = oTempDatetime;

                tempSub1 = buffer[10].Trim();
                temp1 = buffer[11].Trim();
                string tempSub2, temp2;
                foreach (TAB512 item2 in oTable2)
                {
                    string[] buffer2 = item2.Wa.Split('^');
                    tempSub2 = buffer2[8].Trim();
                    temp2 = buffer2[9].Trim();
                    if (tempSub1 == tempSub2 && temp1 == temp2)
                    {
                        IF21.FatherID = buffer2[0].Trim();
                        IF21.MotherID = buffer2[1].Trim();
                        IF21.SpouseID = buffer2[2].Trim();
                        IF21.FatherSpouseID = buffer2[3].Trim();
                        IF21.MotherSpouseID = buffer2[4].Trim();
                        IF21.TitleName = buffer2[5].Trim();
                        IF21.Employer = buffer2[6].Trim();
                        IF21.JobTitle = buffer2[7].Trim();
                        //AddBY: Ratchatawan W. (2011-10-14)
                        IF21.Dead = buffer2[10].Trim();
                        //AddBy: Nuttapon.a 11.02.2013
                        IF21.Address = buffer2[11].Trim();
                        IF21.Street = buffer2[12].Trim();
                        IF21.District = buffer2[13].Trim();
                        IF21.City = buffer2[14].Trim();
                        IF21.Postcode = buffer2[15].Trim();
                        IF21.Country = buffer2[16].Trim();
                        IF21.TelephoneNo = buffer2[17].Trim();
                    }
                }

                returnValue.Add(IF21);
            }
            return returnValue;
        }

        public override void SaveProvidentFundMember(List<INFOTYPE0021> data)
        {
            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            UPLOADINFOTYPE6 oFunction = new UPLOADINFOTYPE6();
            oFunction.Connection = oConnection;

            ZHRHRS001Table Updatetab = new ZHRHRS001Table();
            ZHRHRS001Table Updatetab_Ret = new ZHRHRS001Table();

            foreach (INFOTYPE0021 info21 in data)
            {
                ZHRHRS001 item = new ZHRHRS001();
                item.Begda = info21.BeginDate.ToString("yyyyMMdd", DefaultCultureInfo);
                item.Endda = info21.EndDate.ToString("yyyyMMdd", DefaultCultureInfo);
                item.Infty = info21.InfoType;
                item.Msg = "";
                item.Msgtype = "";
                item.Objps = info21.ChildNo;
                item.Operation = "INS";
                item.Pernr = info21.EmployeeID;
                //เป็นการเอาเลขเครื่องมาคำนวณเพื่อใช้ป้องกันข้อมูลซ้ำกัน คือมันจะ gen key ที่ไม่มีทางซ้ำกันได้เลย
                item.Reqnr = Guid.NewGuid().ToString();
                item.Seqnr = "";
                item.Status = "N";
                item.Subty = info21.SubType;
                //T ต่าง ๆ เอามาจาก function ที่อยู่ใน sap se37
                item.T01 = info21.FamilyMember;
                item.T02 = info21.TitleRank;
                item.T03 = info21.Name;
                item.T04 = info21.Surname;
                if (info21.BirthDate == DateTime.MinValue)
                {
                    item.T05 = "00000000";
                }
                else
                {
                    item.T05 = info21.BirthDate.ToString("yyyyMMdd", DefaultCultureInfo);
                }
                item.T06 = info21.Sex;
                //Province province = Province.GetProvince(info21.BirthPlace);
                //item.T07 = province.Description;
                //ModifyBy: Ratchatawan W. (2011-10-14)
                item.T07 = info21.BirthPlace;

                item.T08 = info21.CityOfBirth;
                item.T09 = info21.Nationality;
                item.T10 = info21.FatherID;
                item.T11 = info21.MotherID;
                item.T12 = info21.SpouseID;
                item.T13 = info21.FatherSpouseID;
                item.T14 = info21.MotherSpouseID;
                item.T15 = info21.TitleName;
                item.T16 = info21.Employer;
                item.T17 = info21.JobTitle;
                //AddBy: Ratchatawan W. (2011-10-14)
                item.T18 = info21.Dead;
                //AddBy: Nuttapon.a 2013-02-19
                item.T19 = info21.Address;
                item.T20 = info21.Street;
                item.T21 = info21.District;
                item.T22 = info21.City;
                item.T23 = info21.Postcode;
                item.T24 = info21.Country;
                item.T25 = info21.TelephoneNo;
                Updatetab.Add(item);
            }
            oFunction.Zhrhri001(ref Updatetab, ref Updatetab_Ret);
            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            bool lError = false;
            string message = "";
            ZHRHRS001 result;
            for (int i = 0; i < Updatetab_Ret.Count; i++)
            {
                result = Updatetab_Ret[i];
                if (result.Msgtype == "E")
                {
                    message = result.Msg;
                    data[i].Remark = result.Msg;
                    lError = true;
                }
            }

            if (lError)
            {
                throw new Exception("Post data error: " + message);
            }
        }

        public override void DeleteProvidentFundMember(List<INFOTYPE0021> data)
        {
            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);
            UPLOADINFOTYPE6 oFunction = new UPLOADINFOTYPE6();
            oFunction.Connection = oConnection;

            ZHRHRS001Table Updatetab = new ZHRHRS001Table();
            ZHRHRS001Table Updatetab_Ret = new ZHRHRS001Table();
            foreach (INFOTYPE0021 info21 in data)
            {
                ZHRHRS001 item = new ZHRHRS001();
                item.Begda = info21.BeginDate.ToString("yyyyMMdd HHmmss", DefaultCultureInfo);
                item.Endda = info21.EndDate.ToString("yyyyMMdd HHmmss", DefaultCultureInfo);
                item.Infty = info21.InfoType;
                item.Msg = "";
                item.Msgtype = "";
                item.Objps = info21.ChildNo;
                item.Operation = "MOD"; // MOD
                item.Pernr = info21.EmployeeID;
                //เป็นการเอาเลขเครื่องมาคำนวณเพื่อใช้ป้องกันข้อมูลซ้ำกัน คือมันจะ gen key ที่ไม่มีทางซ้ำกันได้เลย
                item.Reqnr = Guid.NewGuid().ToString();
                item.Seqnr = "";
                item.Status = "N";
                item.Subty = info21.FamilyMember;
                //T ต่าง ๆ เอามาจาก function ที่อยู่ใน sap se37
                item.T01 = info21.FamilyMember;
                item.T02 = info21.TitleRank;
                item.T03 = info21.Name;
                item.T04 = info21.Surname;
                if (info21.BirthDate == DateTime.MinValue)
                {
                    item.T05 = "00000000";
                }
                else
                {
                    item.T05 = info21.BirthDate.ToString("yyyyMMdd", DefaultCultureInfo);
                }
                item.T06 = info21.Sex;
                item.T07 = info21.BirthPlace;
                item.T08 = info21.CityOfBirth;
                item.T09 = info21.Nationality;
                item.T10 = info21.FatherID;
                item.T11 = info21.MotherID;
                item.T12 = info21.SpouseID;
                item.T13 = info21.FatherSpouseID;
                item.T14 = info21.MotherSpouseID;
                item.T15 = info21.TitleName;
                item.T16 = info21.Employer;
                item.T17 = info21.JobTitle;
                item.T18 = info21.Dead;
                item.T19 = info21.Address;
                item.T20 = info21.Street;
                item.T21 = info21.District;
                item.T22 = info21.City;
                item.T23 = info21.Postcode;
                item.T24 = info21.Country;
                item.T25 = info21.TelephoneNo;
                Updatetab.Add(item);
            }
            oFunction.Zhrhri001(ref Updatetab, ref Updatetab_Ret);
            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            bool lError = false;
            ZHRHRS001 result;
            for (int i = 0; i < Updatetab_Ret.Count; i++)
            {
                result = Updatetab_Ret[i];
                if (result.Msgtype == "E")
                {
                    data[i].Remark = result.Msg;
                    lError = true;
                }
            }

            if (lError)
            {
                throw new Exception("Post data error");
            }
        }


        public override List<TaxData> GetTaxDataV2(string EmployeeID, string Year, string PINCODE)
        {
            //if (!ServiceManager.BYPASS_PINCODE && !EmployeeManagement.VerifyPinCode(EmployeeID, PINCODE))
            //{
            //    List<string> authRoles = new List<string>();
            //    authRoles.Add("PYADMIN");
            //    if (!(WorkflowPrinciple.Current.UserSetting.Employee.IsInRole(authRoles)))
            //    {
            //        throw new PincodeException();
            //    }
            //}
            List<TaxData> returnValue = new List<TaxData>();
            Connection oConnect = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            PYInterface oFunction1 = new PYInterface();
            ZHRPYS004Table taxAmount = new ZHRPYS004Table();
            oFunction.Connection = oConnect;
            oFunction1.Connection = oConnect;

            TAB512Table Data = new TAB512Table();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();

            TAB512Table Data2 = new TAB512Table();
            RFC_DB_OPTTable Options2 = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields2 = new RFC_DB_FLDTable();

            TAB512Table Data3 = new TAB512Table();
            RFC_DB_OPTTable Options3 = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields3 = new RFC_DB_FLDTable();

            RFC_DB_OPT opt;
            RFC_DB_FLD fld;

            #region " Fields "
            //ลำดับใบแนบ
            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ICOLD";
            Fields.Add(fld);

            //ที่อยู่
            fld = new RFC_DB_FLD();
            fld.Fieldname = "STRAS";
            Fields2.Add(fld);

            //ถนน/ตำบล
            fld = new RFC_DB_FLD();
            fld.Fieldname = "LOCAT";
            Fields2.Add(fld);

            //อำเภอ ( District ) 
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ORT02";
            Fields2.Add(fld);

            //จังหวัด
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ORT01";
            Fields2.Add(fld);

            //รหัสไปรษณีย์
            fld = new RFC_DB_FLD();
            fld.Fieldname = "PSTLZ";
            Fields2.Add(fld);

            //เลขประจำตัวภาษีพนักงาน
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ICNUM";
            Fields3.Add(fld);
            #endregion

            #region " Options "
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR = '{0}'", EmployeeID);
            Options.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = "AND SUBTY = '05'";
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR = '{0}'", EmployeeID);
            Options2.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = "AND SUBTY = '1'";
            Options2.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND BEGDA <= '{0}' and ENDDA >= '{0}'", DateTime.Now.ToString("yyyyMMdd", DefaultCultureInfo));
            Options2.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR = '{0}'", EmployeeID);
            Options3.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = "AND SUBTY = '01'";
            Options3.Add(opt);
            #endregion

            oFunction.Zrfc_Read_Table("^", "", "PA0185", 0, 0, ref Data, ref Fields, ref Options);
            oFunction.Zrfc_Read_Table("^", "", "PA0006", 0, 0, ref Data2, ref Fields2, ref Options2);
            oFunction.Zrfc_Read_Table("^", "", "PA0185", 0, 0, ref Data3, ref Fields3, ref Options3);
            oFunction1.Zhrpyi020(EmployeeID, Year, ref taxAmount);

            Connection.ReturnConnection(oConnect);
            oFunction.Dispose();

            TaxData taxitem;
            foreach (ZHRPYS004 line in taxAmount)
            {
                taxitem = new TaxData();
                taxitem.EmployeeID = EmployeeID;
                foreach (TAB512 item in Data)
                {
                    string[] buffer = item.Wa.Split('^');
                    if (line.Begda == buffer[0])
                    {
                        taxitem.Order = buffer[2].Trim();
                        break;
                    }
                }
                foreach (TAB512 item2 in Data2)
                {
                    string[] buffer2 = item2.Wa.Split('^');
                    taxitem.EmpAddress = buffer2[0].Trim();
                    taxitem.EmpStreet = buffer2[1].Trim();
                    taxitem.EmpDistrict = buffer2[2].Trim();
                    taxitem.EmpProvince = buffer2[3].Trim();
                    taxitem.EmpPostCode = buffer2[4].Trim();
                }
                foreach (TAB512 item3 in Data3)
                {
                    string[] buffer3 = item3.Wa.Split('^');
                    taxitem.EmployeeTaxNo = buffer3[0].Trim();
                }
                taxitem.Year = line.Year.Trim();
                taxitem.Area = line.Area.Trim();
                taxitem.SubArea = line.Subarea.Trim();
                taxitem.Condition = line.Condition.Trim();
                taxitem.Income = line.Income;
                taxitem.Tax = line.Tax;
                taxitem.Income2 = line.Income2;
                taxitem.Tax2 = line.Tax2;
                taxitem.IncomeOther = line.Income_Oth;
                taxitem.TaxOther = line.Tax_Oth;
                taxitem.PFAmount = line.Pfamount;
                taxitem.SSAmount = line.Ssamount;
                taxitem.CompanyCode = line.Compcode;
                returnValue.Add(taxitem);
            }
            return returnValue;
        }

        public override List<TaxData> GetTaxDataWithoutPinCode(string EmployeeID, string Year)
        {
            List<TaxData> returnValue = new List<TaxData>();
            Connection oConnect = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            PYInterface oFunction1 = new PYInterface();
            ZHRPYS004Table taxAmount = new ZHRPYS004Table();
            oFunction.Connection = oConnect;
            oFunction1.Connection = oConnect;

            TAB512Table Data = new TAB512Table();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();

            TAB512Table Data2 = new TAB512Table();
            RFC_DB_OPTTable Options2 = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields2 = new RFC_DB_FLDTable();

            TAB512Table Data3 = new TAB512Table();
            RFC_DB_OPTTable Options3 = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields3 = new RFC_DB_FLDTable();

            RFC_DB_OPT opt;
            RFC_DB_FLD fld;

            #region " Fields "
            //ลำดับใบแนบ
            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ICOLD";
            Fields.Add(fld);

            //ที่อยู่
            fld = new RFC_DB_FLD();
            fld.Fieldname = "STRAS";
            Fields2.Add(fld);

            //ถนน/ตำบล
            fld = new RFC_DB_FLD();
            fld.Fieldname = "LOCAT";
            Fields2.Add(fld);

            //อำเภอ ( District ) 
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ORT02";
            Fields2.Add(fld);

            //จังหวัด
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ORT01";
            Fields2.Add(fld);

            //รหัสไปรษณีย์
            fld = new RFC_DB_FLD();
            fld.Fieldname = "PSTLZ";
            Fields2.Add(fld);

            //เลขประจำตัวภาษีพนักงาน
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ICNUM";
            Fields3.Add(fld);
            #endregion

            #region " Options "
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR = '{0}'", EmployeeID);
            Options.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = "AND SUBTY = '05'";
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR = '{0}'", EmployeeID);
            Options2.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = "AND SUBTY = '1'";
            Options2.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("AND BEGDA <= '{0}' and ENDDA >= '{0}'", DateTime.Now.ToString("yyyyMMdd", DefaultCultureInfo));
            Options2.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR = '{0}'", EmployeeID);
            Options3.Add(opt);
            opt = new RFC_DB_OPT();
            opt.Text = "AND SUBTY = '01'";
            Options3.Add(opt);
            #endregion

            oFunction.Rfc_Read_Table("^", "", "PA0185", 0, 0, ref Data, ref Fields, ref Options);
            oFunction.Rfc_Read_Table("^", "", "PA0006", 0, 0, ref Data2, ref Fields2, ref Options2);
            oFunction.Rfc_Read_Table("^", "", "PA0185", 0, 0, ref Data3, ref Fields3, ref Options3);
            oFunction1.Zhrpyi011(EmployeeID, Year, ref taxAmount);

            Connection.ReturnConnection(oConnect);
            oFunction.Dispose();

            TaxData taxitem;
            foreach (ZHRPYS004 line in taxAmount)
            {
                taxitem = new TaxData();
                taxitem.EmployeeID = EmployeeID;
                foreach (TAB512 item in Data)
                {
                    string[] buffer = item.Wa.Split('^');
                    if (line.Begda == buffer[0])
                    {
                        taxitem.Order = buffer[2].Trim();
                        break;
                    }
                }
                foreach (TAB512 item2 in Data2)
                {
                    string[] buffer2 = item2.Wa.Split('^');
                    taxitem.EmpAddress = buffer2[0].Trim();
                    taxitem.EmpStreet = buffer2[1].Trim();
                    taxitem.EmpDistrict = buffer2[2].Trim();
                    taxitem.EmpProvince = buffer2[3].Trim();
                    taxitem.EmpPostCode = buffer2[4].Trim();
                }
                foreach (TAB512 item3 in Data3)
                {
                    string[] buffer3 = item3.Wa.Split('^');
                    taxitem.EmployeeTaxNo = buffer3[0].Trim();
                }
                taxitem.Year = line.Year.Trim();
                taxitem.Area = line.Area.Trim();
                taxitem.SubArea = line.Subarea.Trim();
                taxitem.Condition = line.Condition.Trim();
                taxitem.Income = line.Income;
                taxitem.Tax = line.Tax;
                taxitem.Income2 = line.Income2;
                taxitem.Tax2 = line.Tax2;
                taxitem.IncomeOther = line.Income_Oth;
                taxitem.TaxOther = line.Tax_Oth;
                taxitem.PFAmount = line.Pfamount;
                taxitem.SSAmount = line.Ssamount;
                taxitem.CompanyCode = line.Compcode;
                returnValue.Add(taxitem);
            }
            return returnValue;
        }
        #endregion "ProvidentFund"

        public override Dictionary<string, INFOTYPE0014> GetActiveWageRecuringData(string strEmployeeID, DateTime dtCheckDate, string strWageType)
        {
            Dictionary<string, INFOTYPE0014> oReturnValue = new Dictionary<string, INFOTYPE0014>();

            Connection oConnect = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnect;

            TAB512Table oTable = new TAB512Table();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();

            RFC_DB_OPT opt;
            RFC_DB_FLD fld;

            #region " Fields "
            //EmployeeID
            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);

            //BeginDate
            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            //EndDate
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            //WageType
            fld = new RFC_DB_FLD();
            fld.Fieldname = "LGART";
            Fields.Add(fld);

            //Amount
            fld = new RFC_DB_FLD();
            fld.Fieldname = "BETRG";
            Fields.Add(fld);

            //Currency
            fld = new RFC_DB_FLD();
            fld.Fieldname = "WAERS";
            Fields.Add(fld);

            //Number
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ANZHL";
            Fields.Add(fld);

            //Unit
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ZEINH";
            Fields.Add(fld);

            //Assignment Number
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ZUORD";
            Fields.Add(fld);

            //Reason for change
            fld = new RFC_DB_FLD();
            fld.Fieldname = "PREAS";
            Fields.Add(fld);
            #endregion

            #region " Options "
            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR = '{0}' AND BEGDA <= '{1}' AND ENDDA >= '{1}' AND ", strEmployeeID, dtCheckDate.ToString("yyyyMMdd", DefaultCultureInfo));
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            //Modify By Morakot.T 2018-11-15
            //opt.Text = string.Format("LGART IN ('{0}')", strWageType);
            opt.Text = string.Format("LGART IN ('{0}')", strWageType.Replace(",", "','"));
            Options.Add(opt);
            #endregion

            oFunction.Zrfc_Read_Table("^", "", "PA0014", 0, 0, ref oTable, ref Fields, ref Options);

            Connection.ReturnConnection(oConnect);
            oFunction.Dispose();

            INFOTYPE0014 data;
            foreach (TAB512 item in oTable)
            {
                data = new INFOTYPE0014();
                string[] buffer = item.Wa.Split('^');
                data.EmployeeID = buffer[0].Trim();
                data.BeginDate = DateTime.ParseExact(buffer[1], "yyyyMMdd", DefaultCultureInfo);
                data.EndDate = DateTime.ParseExact(buffer[2], "yyyyMMdd", DefaultCultureInfo);
                data.WageType = buffer[3].Trim();
                data.Amount = decimal.Parse(buffer[4].Trim());
                data.Currency = buffer[5].Trim();
                data.Number = decimal.Parse(buffer[6].Trim());
                data.Unit = buffer[7].Trim();
                data.AssignmentNumber = 0;// string.IsNullOrEmpty(buffer[8].Trim()) ? 0 : int.Parse(buffer[8].Trim());
                data.ReasonForChange = buffer[9].Trim();

                oReturnValue.Add(data.WageType, data);
            }

            return oReturnValue;
        }

        public override Dictionary<string, INFOTYPE0015> GetAssignmentNumberWageTypePA0015(string strEmployeeID, DateTime dtCheckDate, string strWageType)
        {
            Dictionary<string, INFOTYPE0015> oReturnValue = new Dictionary<string, INFOTYPE0015>();

            Connection oConnect = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnect;

            DateTime dBegin = new DateTime(dtCheckDate.Year, dtCheckDate.Month, 1);
            int LastDay = DateTime.DaysInMonth(dtCheckDate.Year, dtCheckDate.Month);
            DateTime dEnd = new DateTime(dtCheckDate.Year, dtCheckDate.Month, LastDay);


            TAB512Table oTable = new TAB512Table();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();

            RFC_DB_OPT opt;
            RFC_DB_FLD fld;

            #region " Fields "
            //EmployeeID
            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);

            //BeginDate
            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            //EndDate
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            //WageType
            fld = new RFC_DB_FLD();
            fld.Fieldname = "LGART";
            Fields.Add(fld);

            //Amount
            fld = new RFC_DB_FLD();
            fld.Fieldname = "BETRG";
            Fields.Add(fld);

            //Currency
            fld = new RFC_DB_FLD();
            fld.Fieldname = "WAERS";
            Fields.Add(fld);

            //Number
            //fld = new RFC_DB_FLD();
            //fld.Fieldname = "ANZHL";
            //Fields.Add(fld);

            //Unit
            //fld = new RFC_DB_FLD();
            //fld.Fieldname = "ZEINH";
            //Fields.Add(fld);

            //Assignment Number
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ZUORD";
            Fields.Add(fld);

            //Reason for change
            //fld = new RFC_DB_FLD();
            //fld.Fieldname = "PREAS";
            //Fields.Add(fld);
            #endregion

            #region " Options "
            //opt = new RFC_DB_OPT();
            //opt.Text = string.Format("PERNR = '{0}' AND BEGDA <= '{1}' AND ENDDA >= '{1}' AND ", strEmployeeID, dtCheckDate.ToString("yyyyMMdd", DefaultCultureInfo));
            //Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR = '{0}' AND BEGDA >= '{1}' AND BEGDA <= '{2}' AND ", strEmployeeID, dBegin.ToString("yyyyMMdd", DefaultCultureInfo), dEnd.ToString("yyyyMMdd", DefaultCultureInfo));
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            //Modify By Morakot.T 2018-11-15
            //opt.Text = string.Format("LGART IN ('{0}')", strWageType);
            opt.Text = string.Format("LGART IN ('{0}')", strWageType.Replace(",", "','"));
            Options.Add(opt);
            #endregion

            oFunction.Zrfc_Read_Table("^", "", "PA0015", 0, 0, ref oTable, ref Fields, ref Options);

            Connection.ReturnConnection(oConnect);
            oFunction.Dispose();

            INFOTYPE0015 data;
            int i = 0;
            foreach (TAB512 item in oTable)
            {
                data = new INFOTYPE0015();
                string[] buffer = item.Wa.Split('^');
                data.EmployeeID = buffer[0].Trim();
                data.BeginDate = DateTime.ParseExact(buffer[1], "yyyyMMdd", DefaultCultureInfo);
                data.EndDate = DateTime.ParseExact(buffer[2], "yyyyMMdd", DefaultCultureInfo);
                data.WageType = buffer[3].Trim();
                data.Amount = decimal.Parse(buffer[4].Trim());
                data.Currency = buffer[5].Trim();
                data.AssignmentNumber = buffer[6].Trim();

                if (oReturnValue.ContainsKey(data.WageType))
                {
                    data.WageType = data.WageType + " " + i.ToString();
                }

                oReturnValue.Add(data.WageType, data);
                i += 1;
            }

            return oReturnValue;
        }

        public override Dictionary<string, INFOTYPE0015> GetAssignmentNumberWageTypePA0267(string strEmployeeID, DateTime dtCheckDate, string strWageType)
        {
            Dictionary<string, INFOTYPE0015> oReturnValue = new Dictionary<string, INFOTYPE0015>();

            Connection oConnect = Connection.GetConnectionFromPool(BaseConnStr);
            RFCREADTABLE6 oFunction = new RFCREADTABLE6();
            oFunction.Connection = oConnect;

            DateTime dBegin = new DateTime(dtCheckDate.Year, dtCheckDate.Month, 1);
            int LastDay = DateTime.DaysInMonth(dtCheckDate.Year, dtCheckDate.Month);
            DateTime dEnd = new DateTime(dtCheckDate.Year, dtCheckDate.Month, LastDay);


            TAB512Table oTable = new TAB512Table();
            RFC_DB_OPTTable Options = new RFC_DB_OPTTable();
            RFC_DB_FLDTable Fields = new RFC_DB_FLDTable();

            RFC_DB_OPT opt;
            RFC_DB_FLD fld;

            #region " Fields "
            //EmployeeID
            fld = new RFC_DB_FLD();
            fld.Fieldname = "PERNR";
            Fields.Add(fld);

            //BeginDate
            fld = new RFC_DB_FLD();
            fld.Fieldname = "BEGDA";
            Fields.Add(fld);

            //EndDate
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ENDDA";
            Fields.Add(fld);

            //WageType
            fld = new RFC_DB_FLD();
            fld.Fieldname = "LGART";
            Fields.Add(fld);

            //Amount
            fld = new RFC_DB_FLD();
            fld.Fieldname = "BETRG";
            Fields.Add(fld);

            //Currency
            fld = new RFC_DB_FLD();
            fld.Fieldname = "WAERS";
            Fields.Add(fld);

            //Number
            //fld = new RFC_DB_FLD();
            //fld.Fieldname = "ANZHL";
            //Fields.Add(fld);

            //Unit
            //fld = new RFC_DB_FLD();
            //fld.Fieldname = "ZEINH";
            //Fields.Add(fld);

            //Assignment Number
            fld = new RFC_DB_FLD();
            fld.Fieldname = "ZUORD";
            Fields.Add(fld);

            //Reason for change
            //fld = new RFC_DB_FLD();
            //fld.Fieldname = "PREAS";
            //Fields.Add(fld);
            #endregion

            #region " Options "
            //opt = new RFC_DB_OPT();
            //opt.Text = string.Format("PERNR = '{0}' AND BEGDA <= '{1}' AND ENDDA >= '{1}' AND ", strEmployeeID, dtCheckDate.ToString("yyyyMMdd", DefaultCultureInfo));
            //Options.Add(opt);

            opt = new RFC_DB_OPT();
            opt.Text = string.Format("PERNR = '{0}' AND BEGDA >= '{1}' AND BEGDA <= '{2}' AND ", strEmployeeID, dBegin.ToString("yyyyMMdd", DefaultCultureInfo), dEnd.ToString("yyyyMMdd", DefaultCultureInfo));
            Options.Add(opt);

            opt = new RFC_DB_OPT();
            //Modify By Morakot.T 2018-11-15
            //opt.Text = string.Format("LGART IN ('{0}')", strWageType);
            opt.Text = string.Format("LGART IN ('{0}')", strWageType.Replace(",", "','"));
            Options.Add(opt);
            #endregion

            oFunction.Zrfc_Read_Table("^", "", "PA0267", 0, 0, ref oTable, ref Fields, ref Options);

            Connection.ReturnConnection(oConnect);
            oFunction.Dispose();

            INFOTYPE0015 data;
            int i = 0;
            foreach (TAB512 item in oTable)
            {
                data = new INFOTYPE0015();
                string[] buffer = item.Wa.Split('^');
                data.EmployeeID = buffer[0].Trim();
                data.BeginDate = DateTime.ParseExact(buffer[1], "yyyyMMdd", DefaultCultureInfo);
                data.EndDate = DateTime.ParseExact(buffer[2], "yyyyMMdd", DefaultCultureInfo);
                data.WageType = buffer[3].Trim();
                data.Amount = decimal.Parse(buffer[4].Trim());
                data.Currency = buffer[5].Trim();
                data.AssignmentNumber = buffer[6].Trim();

                if (oReturnValue.ContainsKey(data.WageType))
                {
                    data.WageType = data.WageType + i.ToString();
                }

                oReturnValue.Add(data.WageType, data);
                i += 1;
            }

            return oReturnValue;
        }

        #region Load Payroll Employee

        public override List<SapPayrollEmployee> LoadPayrollSapByEmployee(List<EmployeeAllActive> list_employee, string CompCode, PeriodSettingLatest Period, string AccumType, string NotSummarizeRT,string keySecret)
        {
            List<SapPayrollEmployee> list_sap_payroll_employee = new List<SapPayrollEmployee>();

            Connection oConnection = Connection.GetConnectionFromPool(BaseConnStr);

            AbsAttCalculate oFunction = new AbsAttCalculate();
            oFunction.Connection = oConnection;

            if (list_employee.Count > 0)
            {
                foreach (var employee in list_employee)
                {
                    ZHRPYS031_LTYPE oEt_Return = new ZHRPYS031_LTYPE();
                    ZHRPYS005_INPUT_LTYPE oInput_LType = new ZHRPYS005_INPUT_LTYPE();

                    ZHRPYS005_INPUT item = new ZHRPYS005_INPUT();

                    item.Employeeid = employee.EmployeeID;
                    oInput_LType.Add(item);

                    int iCompCode = int.Parse(CompCode);

                    string periodCode = "";
                    string offcycleFlag = "";
                    if (Period.IsOffCycle)
                    {
                        offcycleFlag = "X";
                        periodCode = Period.OffCycleDate.Value.ToString("yyyyMMdd", oCL);
                    }
                    else
                    {
                        offcycleFlag = "";
                        periodCode = string.Format("{0}{1}", Period.PeriodYear.ToString("0000"), Period.PeriodMonth.ToString("00"));
                    }

                    oFunction.Zhrpyi031(AccumType, iCompCode.ToString(), oInput_LType, NotSummarizeRT, offcycleFlag, periodCode, out oEt_Return);

                    if (oEt_Return.Count > 0)
                    {
                        SapPayrollEmployee payroll_employee = new SapPayrollEmployee();
                        string Accumtype = "";
                        string Offcycle_Flag = "";
                        string Pydate = "";
                        for (int i = 0; i < oEt_Return.Count; i++)
                        {
                            SapPayrollEmployeeDetail detail = new SapPayrollEmployeeDetail()
                            {
                                CurrencyUnit = oEt_Return[i].Currencyunit,
                                TimeType = oEt_Return[i].Type,
                                TimeUnit = oEt_Return[i].Timeunit,
                                TimeAmount = oEt_Return[i].Timeamt,
                                WageTypeCode = oEt_Return[i].Wagetype,
                                WageTypeName = oEt_Return[i].Wagetypetext,
                                ForPeriod = oEt_Return[i].Forperiod,
                                InPeriod = oEt_Return[i].Inperiod
                            };

                            // 1. keySecret + รหัสพนักงาน
                            string key_employee = keySecret + employee.EmployeeID.Trim();
                            detail.CurrencyAmount = EncryptAes256(oEt_Return[i].Currencyamt.ToString(), key_employee);

                            if (i == 0)
                            {
                                Accumtype = oEt_Return[i].Accumtype;
                                Offcycle_Flag = oEt_Return[i].Offcycle_Flag;
                                Pydate = oEt_Return[i].Pydate;
                            }

                            payroll_employee.ListPayrollDetail.Add(detail);
                        }

                        payroll_employee.EmployeeID = employee.EmployeeID;
                        payroll_employee.AccumType = Accumtype;
                        payroll_employee.OffCycleFlag = Offcycle_Flag;
                        payroll_employee.PeriodMonth = Period.PeriodMonth;
                        payroll_employee.PeriodYear = Period.PeriodYear;
                        payroll_employee.PYDate = Convert.ToDateTime(Pydate,oCL);

                        list_sap_payroll_employee.Add(payroll_employee);
                    }
                }
            }

            Connection.ReturnConnection(oConnection);
            oFunction.Dispose();

            return list_sap_payroll_employee;
        }

        public string EncryptAes256(string plainText, string keyString)
        {
            byte[] cipherData;
            Aes aes = Aes.Create();
            aes.Key = Encoding.UTF8.GetBytes(keyString);
            aes.GenerateIV();
            aes.Mode = CipherMode.CBC;
            ICryptoTransform cipher = aes.CreateEncryptor(aes.Key, aes.IV);

            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, cipher, CryptoStreamMode.Write))
                {
                    using (StreamWriter sw = new StreamWriter(cs))
                    {
                        sw.Write(plainText);
                    }
                }

                cipherData = ms.ToArray();
            }

            byte[] combinedData = new byte[aes.IV.Length + cipherData.Length];
            Array.Copy(aes.IV, 0, combinedData, 0, aes.IV.Length);
            Array.Copy(cipherData, 0, combinedData, aes.IV.Length, cipherData.Length);
            return Convert.ToBase64String(combinedData);
        }

        public string DecryptAes256(string combinedString, string keyString)
        {
            string plainText;
            byte[] combinedData = Convert.FromBase64String(combinedString);
            Aes aes = Aes.Create();
            aes.KeySize = 256;
            aes.BlockSize = 128;
            aes.Key = Encoding.UTF8.GetBytes(keyString);
            byte[] iv = new byte[aes.BlockSize / 8];
            byte[] cipherText = new byte[combinedData.Length - iv.Length];
            Array.Copy(combinedData, iv, iv.Length);
            Array.Copy(combinedData, iv.Length, cipherText, 0, cipherText.Length);
            aes.IV = iv;
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;
            ICryptoTransform decipher = aes.CreateDecryptor(aes.Key, aes.IV);

            using (MemoryStream ms = new MemoryStream(cipherText))
            {
                using (CryptoStream cs = new CryptoStream(ms, decipher, CryptoStreamMode.Read))
                {
                    using (StreamReader sr = new StreamReader(cs))
                    {
                        plainText = sr.ReadToEnd();
                    }
                }

                return plainText;
            }
        }

        #endregion
    }

}
