using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Web.Security;
using ESS.SECURITY;
using ESS.SHAREDATASERVICE;

namespace ESS.AUTHORIZE.DB
{
    public class AuthorizeServiceImpl : AbstractAuthorizeService
    {
        #region Constructor
        public AuthorizeServiceImpl(string oCompanyCode)
        {
            //Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID);
            CompanyCode = oCompanyCode;
        }

        #endregion Constructor

        #region Member
        //private Dictionary<string, string> Config { get; set; }
        public string CompanyCode { get; set; }

        private static string ModuleID = "ESS.AUTHORIZE.DB";

        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }

        #endregion Member

        #region IAuthorizeService Members

        public override bool CheckAuthroize(string UserName, string Password)
        {
            return true;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from BSA_UserPassword where UserName=@UserName and EncriptPassword = @EncriptPassword", oConnection);

            SqlParameter oParam;
            oParam = new SqlParameter("@UserName", SqlDbType.VarChar);
            oParam.Value = UserName;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EncriptPassword", SqlDbType.VarChar);
            oParam.Value = FormsAuthentication.HashPasswordForStoringInConfigFile(UserName + Password, "SHA1");
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("BSA_UserPassword");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            if (oTable.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override bool HavePassword(string UserName)
        {
            return true;
            bool returnValue = false;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select UserName from BSA_UserPassword Where UserName = @UserName ", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@UserName", SqlDbType.VarChar);
            oParam.Value = UserName;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("BSA_UserPassword");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            if (oTable.Rows.Count > 0)
            {
                returnValue = true;
            }
            oTable.Dispose();
            return returnValue;
        }

        public override void ChangePassword(string UserName, string NewPassword)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select * from BSA_UserPassword Where UserName = @UserName", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@UserName", SqlDbType.VarChar);
            oParam.Value = UserName;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            SqlCommandBuilder oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("BSA_UserPassword");
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();

            DataRow oNewRow = oTable.NewRow();
            oNewRow["UserName"] = UserName;
            oNewRow["EncriptPassword"] = FormsAuthentication.HashPasswordForStoringInConfigFile(UserName + NewPassword, "SHA1");
            oNewRow["LastLogonTime"] = System.DateTime.Now;

            oTable.LoadDataRow(oNewRow.ItemArray, false);

            oConnection.Open();
            oAdapter.Update(oTable);
            oConnection.Close();

            oTable.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
        }

        public override void ChangePassword(string UserName, string OldPassword, string NewPassword)
        {
            if (this.CheckAuthroize(UserName, OldPassword))
            {
                throw new Exception("Old password is incorrect");
            }
            else
            {
                this.ChangePassword(UserName, NewPassword);
            }
        }

        public override void ClearPassword(string UserName)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("Delete from BSA_UserPassword Where UserName = @UserName", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@UserName", SqlDbType.VarChar);
            oParam.Value = UserName;
            oCommand.Parameters.Add(oParam);
            oConnection.Open();
            oCommand.ExecuteNonQuery();
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
        }
        #endregion IAuthorizeService Members
    }
}