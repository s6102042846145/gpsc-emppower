using System;
using System.Collections.Generic;
using System.Text;
using PTTICT.AUTHEN;
using System.Data.SqlClient;
using System.Configuration;
using System.Reflection;
using System.Data;
using System.Web.Security;

namespace PTTICT.AUTHEN.DB
{
    public class AUTHENSERVICE : AbstractAuthenService
    {
        private static Configuration __config;
        private static Configuration config
        {
            get
            {
                if (__config == null)
                {
                    __config = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().CodeBase.Replace("file:///", "").Replace("/", "\\"));
                }
                return __config;
            }
        }

        private string ConnectionString
        {
            get
            {
                if (config == null || config.AppSettings.Settings["BaseConnStr"] == null)
                {
                    return "";
                }
                else
                {
                    return config.AppSettings.Settings["BaseConnStr"].Value;
                }
            }
        }

        public bool HavePassword(string UserName)
        {
            bool returnValue = false;
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("select UserName from BSA_UserPassword Where UserName = @UserName ", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@UserName", SqlDbType.VarChar);
            oParam.Value = UserName;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("BSA_UserPassword");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            if (oTable.Rows.Count > 0)
            {
                returnValue = true;
            }
            oTable.Dispose();
            return returnValue;
        }

        #region Change Password
        private void ChangePassword(string UserName, string NewPassword)
        {
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("select * from BSA_UserPassword Where UserName = @UserName", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@UserName", SqlDbType.VarChar);
            oParam.Value = UserName;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            SqlCommandBuilder oCB = new SqlCommandBuilder(oAdapter);
            DataTable oTable = new DataTable("BSA_UserPassword");
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();

            DataRow oNewRow = oTable.NewRow();
            oNewRow["UserName"] = UserName;
            oNewRow["EncriptPassword"] = FormsAuthentication.HashPasswordForStoringInConfigFile(UserName + NewPassword, "SHA1");
            oNewRow["LastLogonTime"] = System.DateTime.Now;

            oTable.LoadDataRow(oNewRow.ItemArray, false);

            oConnection.Open();
            oAdapter.Update(oTable);
            oConnection.Close();

            oTable.Dispose();
            oConnection.Dispose();
            oCommand.Dispose();
        }
        #endregion

        public void ChangePassword(string UserName, string OldPassword, string NewPassword)
        {
            if (ServiceManager.AuthorizeService.CheckAuthroize(UserName, OldPassword))
            {
                throw new Exception("Old password is incorrect");
            }
            else
            {
                this.ChangePassword(UserName, NewPassword);
            }
        }

        public void ClearPassword(string UserName)
        {
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand = new SqlCommand("Delete from BSA_UserPassword Where UserName = @UserName", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@UserName", SqlDbType.VarChar);
            oParam.Value = UserName;
            oCommand.Parameters.Add(oParam);
            oConnection.Open();
            oCommand.ExecuteNonQuery();
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
        }
    }
}
