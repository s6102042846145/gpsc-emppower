﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Data;
using System.Data.SqlClient;
using ESS.TIMESHEET.ABSTRACT;
using ESS.TIMESHEET.DATACLASS;
using ESS.TIMESHEET.EXCEPTION;
using ESS.TIMESHEET.INTERFACE;
using ESS.SHAREDATASERVICE;


namespace ESS.TIMESHEET.DB
{
    public class TimeSheetDataServiceImpl : AbstractTimesheetDataService
    {
        #region Constructor
        public TimeSheetDataServiceImpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
            //Config = ShareDataManager.GetModuleSettings(oCompanyCode, ModuleID);
        }

        #endregion Constructor

        #region Member
        //private Dictionary<string, string> Config { get; set; }

        private static string ModuleID = "ESS.TIMESHEET.DB";


        public string CompanyCode { get; set; }
        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }

        #endregion Member
        #region " MarkTimesheet "
        public override void MarkTimesheet(string EmployeeID, string ApplicationKey, string SubKey1, string SubKey2, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark, bool isCheck, string Detail)
        {
            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            SqlParameter oParam;
            SqlCommand oCommand = null;
            SqlDataAdapter oAdapter = null;
            DataTable oTable = null;
            SqlCommandBuilder oCB = null;
            DateTime date1, date2;
            if (isMark)
            {
                // Check collission
                //DateTime date1, date2;
                //MonthlyWS oCalendar = null;
                //EmployeeData oEmp;
                //DailyWS oDWS;
                DateTime bufferDate1;
                DateTime bufferDate2;

                if (isFullDay)
                {
                    date1 = BeginDate;
                    date2 = EndDate;
                    //oCalendar = MonthlyWS.GetCalendar(EmployeeID, BeginDate.Year, BeginDate.Month);
                    //oEmp = new EmployeeData(EmployeeID, BeginDate.Date);
                    //if (oEmp.OrgAssignment == null)
                    //{
                    //    date1 = BeginDate;
                    //    date2 = EndDate;
                    //}
                    //else
                    //{
                    //    oDWS = DailyWS.GetDailyWorkschedule(oEmp.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, oCalendar.GetWSCode(BeginDate.Day, true), BeginDate);
                    //    if (oDWS != null && oDWS.WorkBeginTime > TimeSpan.MinValue)
                    //    {
                    //        date1 = BeginDate.Date.Add(oDWS.WorkBeginTime);
                    //    }
                    //    else
                    //    {
                    //        date1 = BeginDate.Date;
                    //    }
                    //    if (BeginDate.Date == EndDate.Date)
                    //    {
                    //        if (oDWS.WorkEndTime > TimeSpan.MinValue)
                    //        {
                    //            if (oDWS.WorkEndTime > oDWS.WorkBeginTime)
                    //                date2 = BeginDate.Date.Add(oDWS.WorkEndTime);
                    //            else
                    //                date2 = BeginDate.Date.AddDays(1).Add(oDWS.WorkEndTime);
                    //        }
                    //        else
                    //        {
                    //            date2 = BeginDate.Date.AddDays(1);
                    //        }
                    //    }
                    //    else
                    //    {
                    //        if (EndDate.Month != BeginDate.Month || EndDate.Year != BeginDate.Year)
                    //        {
                    //            oCalendar = MonthlyWS.GetCalendar(EmployeeID, EndDate.Year, EndDate.Month);
                    //        }
                    //        if (!(oEmp.OrgAssignment.BeginDate >= EndDate.Date && oEmp.OrgAssignment.EndDate <= EndDate.Date))
                    //        {
                    //            oEmp = new EmployeeData(EmployeeID, EndDate.Date);
                    //        }
                    //        oDWS = DailyWS.GetDailyWorkschedule(oEmp.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, oCalendar.GetWSCode(EndDate.Day, true), EndDate);
                    //        if (oDWS.WorkEndTime > TimeSpan.MinValue)
                    //        {
                    //            if (oDWS.WorkEndTime > oDWS.WorkBeginTime)
                    //                date2 = EndDate.Date.Add(oDWS.WorkEndTime);
                    //            else
                    //                date2 = EndDate.Date.AddDays(1).Add(oDWS.WorkEndTime);
                    //        }
                    //        else
                    //        {
                    //            date2 = EndDate.Date.AddDays(1);
                    //        }
                    //    }
                    //}
                }
                else
                {
                    date1 = BeginDate;
                    date2 = EndDate;
                }
                List<TimesheetData> collisions, returnList;
                returnList = new List<TimesheetData>();
                foreach (CollisionControl item in this.LoadApplicationCollision(ApplicationKey, SubKey1, SubKey2))
                {
                    #region " find Collision in self timesheet "
                    collisions = this.LoadTimesheetCollision(EmployeeID, date1, date2, item);
                    foreach (TimesheetData cl in collisions)
                    {
                        if (item.IgnoreSameDetail && cl.Detail.Trim() == Detail.Trim())
                        {
                            continue;
                        }
                        if (item.IsCheckOnlyPlanTime)
                        {
                            if (cl.FullDay)
                            {
                                //for (DateTime rundate = cl.BeginDate.Date; rundate <= cl.EndDate.Date; rundate = rundate.AddDays(1))
                                //{
                                //    if (oCalendar == null || rundate.Month != oCalendar.WS_Month || rundate.Year != oCalendar.WS_Year)
                                //    {
                                //        oCalendar = MonthlyWS.GetCalendar(EmployeeID, rundate.Year, rundate.Month);
                                //    }
                                //    oEmp = new EmployeeData(EmployeeID, rundate);
                                //    oDWS = DailyWS.GetDailyWorkschedule(oEmp.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, oCalendar.GetWSCode(rundate.Day, true), rundate);
                                //    bufferDate1 = rundate.Add(oDWS.WorkBeginTime);
                                //    if (oDWS.WorkEndTime > oDWS.WorkBeginTime)
                                //        bufferDate2 = rundate.Add(oDWS.WorkEndTime);
                                //    else
                                //        bufferDate2 = rundate.AddDays(1).Add(oDWS.WorkEndTime);
                                //    if (bufferDate1 < date2 && bufferDate2 > date1)
                                //    {
                                //        cl.BeginDate = bufferDate1;
                                //        cl.EndDate = bufferDate2;
                                //        returnList.Add(cl);
                                //        break;
                                //    }
                                //}
                            }
                        }
                        else
                        {
                            returnList.Add(cl);
                        }
                    }
                    #endregion

                    #region " find Collsion in Additional data "
                    ICollisionable oCollisionable = this.LoadAdditionalDataCollisionable(item.ApplicationKeyExists);
                    // check if implements
                    if (oCollisionable != null)
                    {
                        collisions = oCollisionable.LoadCollision(EmployeeID, date1, date2, item.SubKey1Exists, item.SubKey2Exists);
                        foreach (TimesheetData cl in collisions)
                        {
                            if (cl.Detail == Detail)
                            {
                                //case both of them is same requestNo ---> SKIP
                                continue;
                            }
                            if (item.IsCheckOnlyPlanTime)
                            {
                                if (cl.FullDay)
                                {
                                    //for (DateTime rundate = cl.BeginDate.Date; rundate <= cl.EndDate.Date; rundate = rundate.AddDays(1))
                                    //{
                                    //    if (oCalendar == null || rundate.Month != oCalendar.WS_Month || rundate.Year != oCalendar.WS_Year)
                                    //    {
                                    //        oCalendar = MonthlyWS.GetCalendar(EmployeeID, rundate.Year, rundate.Month);
                                    //    }
                                    //    oEmp = new EmployeeData(EmployeeID, rundate);
                                    //    oDWS = DailyWS.GetDailyWorkschedule(oEmp.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, oCalendar.GetWSCode(rundate.Day, true), rundate);
                                    //    if (!oDWS.IsDayOff)
                                    //    {
                                    //        bufferDate1 = rundate.Add(oDWS.WorkBeginTime);
                                    //        if (oDWS.WorkEndTime > oDWS.WorkBeginTime)
                                    //            bufferDate2 = rundate.Add(oDWS.WorkEndTime);
                                    //        else
                                    //            bufferDate2 = rundate.AddDays(1).Add(oDWS.WorkEndTime);
                                    //        if (bufferDate1 < date2 && bufferDate2 > date1)
                                    //        {
                                    //            returnList.Add(cl);
                                    //            break;
                                    //        }
                                    //    }
                                    //}
                                }
                                else
                                {
                                    if (cl.BeginDate < date2 && cl.EndDate > date1)
                                    {
                                        returnList.Add(cl);
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                if (cl.BeginDate < date2 && cl.EndDate > date1)
                                {
                                    returnList.Add(cl);
                                    break;
                                }
                            }
                        }
                    }
                    #endregion
                }

                if (returnList.Count > 0)
                {
                    throw new TimesheetCollisionException(returnList);
                }
            }
            try
            {
                if (!isCheck)
                {
                    oCommand = new SqlCommand("select * from Timesheet where EmployeeID = @employeeid and ApplicationKey = @ApplicationKey and Detail = @Detail", oConnection);
                    oParam = new SqlParameter("@employeeid", SqlDbType.VarChar);
                    oParam.Value = EmployeeID;
                    oCommand.Parameters.Add(oParam);
                    oParam = new SqlParameter("@ApplicationKey", SqlDbType.VarChar);
                    oParam.Value = ApplicationKey;
                    oCommand.Parameters.Add(oParam);
                    oParam = new SqlParameter("@Detail", SqlDbType.VarChar);
                    oParam.Value = Detail;
                    oCommand.Parameters.Add(oParam);
                    oAdapter = new SqlDataAdapter(oCommand);
                    oCB = new SqlCommandBuilder(oAdapter);
                    oTable = new DataTable("TIMESHEET");
                    oAdapter.FillSchema(oTable, SchemaType.Source);
                    oAdapter.Fill(oTable);

                    TimesheetData oTSD = new TimesheetData();
                    oTSD.EmployeeID = EmployeeID;
                    oTSD.ApplicationKey = ApplicationKey;
                    oTSD.BeginDate = BeginDate;
                    oTSD.EndDate = EndDate;
                    oTSD.FullDay = isFullDay;
                    oTSD.Detail = Detail;
                    oTSD.SubKey1 = SubKey1;
                    oTSD.SubKey2 = SubKey2;
                    DataTable oTemp = oTSD.ToADODataTable();
                    DataRow oRow = oTable.LoadDataRow(oTemp.Rows[0].ItemArray, false);
                    if (!isMark)
                    {
                        oRow.Delete();
                    }
                    foreach (DataRow dr in oTable.Rows)
                    {
                        if (dr.RowState == DataRowState.Unchanged)
                        {
                            dr.Delete();
                        }
                    }
                    oAdapter.Update(oTable);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Dispose();
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                if (oCommand != null)
                {
                    oCommand.Dispose();
                }
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oTable != null)
                {
                    oTable.Dispose();
                }
            }
        }
        #endregion

        #region " LoadApplicationCollision "
        private List<CollisionControl> LoadApplicationCollision(string ApplicationKey, string SubKey1, string SubKey2)
        {
            List<CollisionControl> oReturn = new List<CollisionControl>();
            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("COLLISIONCONTROL");
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("select * from CollisionControl Where ApplicationKeyNew = @ApplicationKey and (SubKey1New = '*' or SubKey1New = @SubKey1) and (SubKey2New = '*' or SubKey2New = @SubKey2)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ApplicationKey", SqlDbType.VarChar);
            oParam.Value = ApplicationKey;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@SubKey1", SqlDbType.VarChar);
            oParam.Value = SubKey1;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@SubKey2", SqlDbType.VarChar);
            oParam.Value = SubKey2;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                CollisionControl item = new CollisionControl();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }
            oTable.Dispose();
            return oReturn;

        }
        #endregion

        #region " LoadTimesheetCollision "
        private List<TimesheetData> LoadTimesheetCollision(string EmployeeID, DateTime BeginDate, DateTime EndDate, CollisionControl Key)
        {
            List<TimesheetData> oReturn = new List<TimesheetData>();
            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("TIMESHEETDATA");
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("select * from FindCollision(@EmployeeID, @BeginDate, @EndDate, @ApplicationKey, @SubKey1, @SubKey2)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@ApplicationKey", SqlDbType.VarChar);
            oParam.Value = Key.ApplicationKeyExists;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@SubKey1", SqlDbType.VarChar);
            oParam.Value = Key.SubKey1Exists;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@SubKey2", SqlDbType.VarChar);
            oParam.Value = Key.SubKey2Exists;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                TimesheetData item = new TimesheetData();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }
            oTable.Dispose();
            return oReturn;

        }
        #endregion

        #region " LoadAdditionalDataCollisionable "
        private ICollisionable LoadAdditionalDataCollisionable(string ApplicationKey)
        {
            ICollisionable oReturn = null;
            SqlConnection oConnection = new SqlConnection(BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("ADDITIONALCOLLISION");
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("select * from AdditionalCollision Where ApplicationKey = @ApplicationKey", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ApplicationKey", SqlDbType.VarChar);
            oParam.Value = ApplicationKey;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                Assembly oAssembly;
                Type oType;
                string assemblyName = (string)dr["AssemblyName"];
                string typeName = (string)dr["ClassName"];
                oAssembly = Assembly.Load(assemblyName);
                if (oAssembly != null)
                {
                    oType = oAssembly.GetType(typeName);
                    if (oType != null)
                    {

                        if (oType.GetInterfaceMap(typeof(ICollisionable)).InterfaceType != null)
                        {
                            oReturn = (ICollisionable)Activator.CreateInstance(oType);
                        }
                    }
                }
                break;
            }
            oTable.Dispose();
            return oReturn;
        }
        #endregion
    }
}
