using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Text;
using ESS.EMPLOYEE;
using ESS.EMPLOYEE.CONFIG;
using ESS.EMPLOYEE.CONFIG.TM;
using ESS.TIMESHEET;
using ESS.WORKFLOW;
using ESS.UTILITY.EXTENSION;
using ESS.SHAREDATASERVICE;

namespace ESS.TIMESHEET.DB
{
    public class TimesheetServicelmpl : AbstractTimesheetService
    {
        public TimesheetServicelmpl(string oCompanyCode)
        {
            CompanyCode = oCompanyCode;
            oSqlManage["BaseConnStr"] = BaseConnStr;
        }


        #region Member
        private Dictionary<string, string> oSqlManage = new Dictionary<string, string>();
        private static string ModuleID = "ESS.HR.TM.DB";

        public string CompanyCode { get; set; }
        private string BaseConnStr
        {
            get
            {
                return ShareDataManagement.LookupCache(CompanyCode, ModuleID, "BASECONNSTR");
            }
        }

        #endregion Member

        //#region " private data "
        //private static Configuration __config;
        //private static Configuration config
        //{
        //    get
        //    {
        //        if (__config == null)
        //        {
        //            __config = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().CodeBase.Replace("file:///", "").Replace("/", "\\"));
        //        }
        //        return __config;
        //    }
        //}
        //private string ConnectionString
        //{
        //    get
        //    {
        //        if (config == null || config.AppSettings.Settings["BaseConnStr"] == null)
        //        {
        //            return "";
        //        }
        //        else
        //        {
        //            return config.AppSettings.Settings["BaseConnStr"].Value;
        //        }
        //    }
        //}
        //private string GetConnectionString(string profile)
        //{
        //    return config.AppSettings.Settings[profile].Value;
        //}
        //#endregion

        #region ITimesheetService Members

        #region " MarkTimesheet "
        public override void MarkTimesheet(string EmployeeID, string ApplicationKey, string SubKey1, string SubKey2, bool isFullDay, DateTime BeginDate, DateTime EndDate, bool isMark, bool isCheck, string Detail)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlParameter oParam;
            SqlCommand oCommand = null;
            SqlDataAdapter oAdapter = null;
            DataTable oTable = null;
            SqlCommandBuilder oCB = null;
            if (isMark)
            {
                // Check collission
                DateTime date1, date2;
                MonthlyWS oCalendar = null;
                EmployeeData oEmp;
                EMPLOYEE.CONFIG.TM.DailyWS oDWS;
                DateTime bufferDate1;
                DateTime bufferDate2;

                if (isFullDay)
                {
                    oCalendar = MonthlyWS.GetCalendar(EmployeeID, BeginDate.Year, BeginDate.Month);
                    oEmp = new EmployeeData(EmployeeID, BeginDate.Date);
                    if (oEmp.OrgAssignment == null)
                    {
                        date1 = BeginDate;
                        date2 = EndDate;
                    }
                    else
                    {
                        oDWS = EmployeeManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetDailyWorkschedule(oEmp.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, oCalendar.GetWSCode(BeginDate.Day, true), BeginDate);
                        if (oDWS != null && oDWS.WorkBeginTime > TimeSpan.MinValue)
                        {
                            date1 = BeginDate.Date.Add(oDWS.WorkBeginTime);
                        }
                        else
                        {
                            date1 = BeginDate.Date;
                        }
                        if (BeginDate.Date == EndDate.Date)
                        {
                            if (oDWS.WorkEndTime > TimeSpan.MinValue)
                            {
                                date2 = BeginDate.Date.Add(oDWS.WorkEndTime);
                            }
                            else
                            {
                                date2 = BeginDate.Date.AddDays(1);
                            }
                        }
                        else
                        {
                            if (EndDate.Month != BeginDate.Month || EndDate.Year != BeginDate.Year)
                            {
                                oCalendar = MonthlyWS.GetCalendar(EmployeeID, EndDate.Year, EndDate.Month);
                            }
                            if (!(oEmp.OrgAssignment.BeginDate >= EndDate.Date && oEmp.OrgAssignment.EndDate <= EndDate.Date))
                            {
                                oEmp = new EmployeeData(EmployeeID, EndDate.Date);
                            }
                            oDWS = EmployeeManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetDailyWorkschedule(oEmp.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, oCalendar.GetWSCode(EndDate.Day, true), EndDate);
                            if (oDWS.WorkEndTime > TimeSpan.MinValue)
                            {
                                date2 = EndDate.Date.Add(oDWS.WorkEndTime);
                            }
                            else
                            {
                                date2 = EndDate.Date.AddDays(1);
                            }
                        }
                    }
                }
                else
                {
                    date1 = BeginDate;
                    date2 = EndDate;
                }
                List<TimesheetData> collisions, returnList;
                returnList = new List<TimesheetData>();
                foreach (CollisionControl item in this.LoadApplicationCollision(ApplicationKey, SubKey1, SubKey2))
                {
                    #region " find Collision in self timesheet "
                    collisions = this.LoadTimesheetCollision(EmployeeID, date1, date2, item);
                    foreach (TimesheetData cl in collisions)
                    {
                        if (item.IgnoreSameDetail && cl.Detail.Trim() == Detail.Trim())
                        {
                            continue;
                        }
                        if (item.IsCheckOnlyPlanTime)
                        {
                            if (cl.FullDay)
                            {
                                for (DateTime rundate = cl.BeginDate.Date; rundate <= cl.EndDate.Date; rundate = rundate.AddDays(1))
                                {
                                    if (oCalendar == null || rundate.Month != oCalendar.WS_Month || rundate.Year != oCalendar.WS_Year)
                                    {
                                        oCalendar = MonthlyWS.GetCalendar(EmployeeID, rundate.Year, rundate.Month);
                                    }
                                    oEmp = new EmployeeData(EmployeeID, rundate);
                                    oDWS = EmployeeManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetDailyWorkschedule(oEmp.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, oCalendar.GetWSCode(rundate.Day, true), rundate);
                                    bufferDate1 = rundate.Add(oDWS.WorkBeginTime);
                                    bufferDate2 = rundate.Add(oDWS.WorkEndTime);
                                    if (bufferDate1 < date2 && bufferDate2 > date1)
                                    {
                                        cl.BeginDate = bufferDate1;
                                        cl.EndDate = bufferDate2;
                                        returnList.Add(cl);
                                        break;
                                    }
                                }
                            }
                        }
                        else
                        {
                            returnList.Add(cl);
                        }
                    }
                    #endregion

                    #region " find Collsion in Additional data "
                    ICollisionable oCollisionable = this.LoadAdditionalDataCollisionable(item.ApplicationKeyExists);
                    // check if implements
                    if (oCollisionable != null)
                    {
                        collisions = oCollisionable.LoadCollision(EmployeeID, date1, date2, item.SubKey1Exists, item.SubKey2Exists);
                        foreach (TimesheetData cl in collisions)
                        {
                            if (cl.Detail == Detail)
                            {
                                //case both of them is same requestNo ---> SKIP
                                continue;
                            }
                            if (item.IsCheckOnlyPlanTime)
                            {
                                if (cl.FullDay)
                                {
                                    for (DateTime rundate = cl.BeginDate.Date; rundate <= cl.EndDate.Date; rundate = rundate.AddDays(1))
                                    {
                                        if (oCalendar == null || rundate.Month != oCalendar.WS_Month || rundate.Year != oCalendar.WS_Year)
                                        {
                                            oCalendar = MonthlyWS.GetCalendar(EmployeeID, rundate.Year, rundate.Month);
                                        }
                                        oEmp = new EmployeeData(EmployeeID, rundate);
                                        oDWS = EmployeeManagement.CreateInstance(WorkflowPrinciple.CurrentIdentity.CompanyCode).GetDailyWorkschedule(oEmp.OrgAssignment.SubAreaSetting.DailyWorkScheduleGrouping, oCalendar.GetWSCode(rundate.Day, false), rundate);
                                        bufferDate1 = rundate.Add(oDWS.WorkBeginTime);
                                        bufferDate2 = rundate.Add(oDWS.WorkEndTime);
                                        if (bufferDate1 < date2 && bufferDate2 > date1)
                                        {
                                            returnList.Add(cl);
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    if (cl.BeginDate < date2 && cl.EndDate > date1)
                                    {
                                        returnList.Add(cl);
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                if (cl.BeginDate < date2 && cl.EndDate > date1)
                                {
                                    returnList.Add(cl);
                                    break;
                                }
                            }
                        }
                    }
                    #endregion
                }

                if (returnList.Count > 0)
                {
                    throw new TimesheetCollisionException((returnList));
                }
            }
            try
            {
                if (!isCheck)
                {
                    oCommand = new SqlCommand("select * from Timesheet where EmployeeID = @employeeid and ApplicationKey = @ApplicationKey and Detail = @Detail", oConnection);
                    oParam = new SqlParameter("@employeeid", SqlDbType.VarChar);
                    oParam.Value = EmployeeID;
                    oCommand.Parameters.Add(oParam);
                    oParam = new SqlParameter("@ApplicationKey", SqlDbType.VarChar);
                    oParam.Value = ApplicationKey;
                    oCommand.Parameters.Add(oParam);
                    oParam = new SqlParameter("@Detail", SqlDbType.VarChar);
                    oParam.Value = Detail;
                    oCommand.Parameters.Add(oParam);
                    oAdapter = new SqlDataAdapter(oCommand);
                    oCB = new SqlCommandBuilder(oAdapter);
                    oTable = new DataTable("TIMESHEET");
                    oAdapter.FillSchema(oTable, SchemaType.Source);
                    oAdapter.Fill(oTable);

                    TimesheetData oTSD = new TimesheetData();
                    oTSD.EmployeeID = EmployeeID;
                    oTSD.ApplicationKey = ApplicationKey;
                    oTSD.BeginDate = BeginDate;
                    oTSD.EndDate = EndDate;
                    oTSD.FullDay = isFullDay;
                    oTSD.Detail = Detail;
                    oTSD.SubKey1 = SubKey1;
                    oTSD.SubKey2 = SubKey2;
                    DataTable oTemp = oTSD.ToADODataTable();
                    DataRow oRow = oTable.LoadDataRow(oTemp.Rows[0].ItemArray, false);
                    if (!isMark)
                    {
                        oRow.Delete();
                    }
                    foreach (DataRow dr in oTable.Rows)
                    {
                        if (dr.RowState == DataRowState.Unchanged)
                        {
                            dr.Delete();
                        }
                    }

                    oAdapter.Update(oTable);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("save data error", ex);
            }
            finally
            {
                oConnection.Dispose();
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                if (oCommand != null)
                {
                    oCommand.Dispose();
                }
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oTable != null)
                {
                    oTable.Dispose();
                }
            }
        }
        #endregion

        #region " LoadArchiveDate "
        public override DateTime LoadArchiveDate(string EmployeeID)
        {
            DateTime oReturn = DateTime.MinValue;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("ARCHIVECONTROL");
            oCommand = new SqlCommand("select LastArchive from TimeEventArchiveControl Where EmployeeID = @EmployeeID", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            if (oTable.Rows.Count > 0)
            {
                oReturn = (DateTime)oTable.Rows[0]["LastArchive"];
            }
            else
            {
                //string cPeriod = LoadArchivePeriod();
                //oReturn = DateTime.ParseExact(cPeriod + "01", "yyyyMMdd", new System.Globalization.CultureInfo("en-US"));
                oReturn = DateTime.Now.Date.AddYears(-1);
            }
            oConnection.Dispose();
            oCommand.Dispose();
            oTable.Dispose();
            return oReturn;
        }
        #endregion

        #region " LoadArchivePeriod "
        public override string LoadArchivePeriod()
        {
            string oReturn = "";
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            oCommand = new SqlCommand("select top 1 TimeEventPeriod from Setting", oConnection);
            oConnection.Open();
            oReturn = (string)oCommand.ExecuteScalar();
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            return oReturn;
        }
        #endregion

        #region " LoadCardSetting "
        public override List<CardSetting> LoadCardSetting(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            List<CardSetting> oReturn = new List<CardSetting>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("CARDSETTING");
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("select * from CardSetting Where EmployeeID = @EmployeeID and BeginDate <= @EndDate and EndDate >= @BeginDate", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                CardSetting item = new CardSetting();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }
            oTable.Dispose();
            return oReturn;
        }

        public override List<CardSetting> LoadCardSetting(string EmployeeID, DateTime CheckDate)
        {
            List<CardSetting> oReturn = new List<CardSetting>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            oCommand = new SqlCommand("select * from CardSetting Where EmployeeID = @EmpID1 and @CheckDate Between BeginDate and EndDate", oConnection);

            SqlParameter oParam;
            oParam = new SqlParameter("@EmpID1", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@CheckDate", SqlDbType.DateTime);
            oParam.Value = CheckDate.Date;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("CARDSETTING");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                CardSetting item = new CardSetting();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oTable.Dispose();
            return oReturn;
        }

        public override List<CardSetting> LoadCardSetting(string EmployeeID)
        {
            List<CardSetting> oReturn = new List<CardSetting>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("CARDSETTING");
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("select * from CardSetting Where EmployeeID = @EmployeeID", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            foreach (DataRow dr in oTable.Rows)
            {
                CardSetting item = new CardSetting();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oTable.Dispose();
            return oReturn;
        }

        public override void SaveCardSetting(string EmployeeID, List<CardSetting> cardList)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            SqlCommandBuilder oCB;
            DataTable oTable = new DataTable("CARDSETTING");
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("select * from CardSetting Where EmployeeID = @EmployeeID", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);

            foreach (DataRow dr in oTable.Rows)
            {
                dr.Delete();
            }

            foreach (CardSetting item in cardList)
            {
                DataRow oNewRow = oTable.NewRow();
                item.LoadDataToTableRow(oNewRow);
                oTable.LoadDataRow(oNewRow.ItemArray, false);
            }

            oAdapter.Update(oTable);

            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            oTable.Dispose();
        }

        public override void SaveCardSetting(List<CardSetting> cardList)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            SqlCommandBuilder oCB;

            oConnection.Open();

            foreach (CardSetting item in cardList)
            {
                //Console.WriteLine("Employee=" + item.EmployeeID);

                DataTable oTable = new DataTable("CARDSETTING");
                oTable.CaseSensitive = false;

                oCommand = new SqlCommand("select * from CardSetting Where EmployeeID = @EmployeeID", oConnection);
                SqlParameter oParam;
                oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
                oParam.Value = item.EmployeeID;
                oCommand.Parameters.Add(oParam);

                oAdapter = new SqlDataAdapter(oCommand);
                oCB = new SqlCommandBuilder(oAdapter);
         
                oAdapter.FillSchema(oTable, SchemaType.Source);
                oAdapter.Fill(oTable);

                //Clear Existing Data
                foreach (DataRow dr in oTable.Rows)
                {
                    dr.Delete();
                }
               
                //Insert Current row
                DataRow oNewRow = oTable.NewRow();
                item.LoadDataToTableRow(oNewRow);
                oTable.LoadDataRow(oNewRow.ItemArray, false);

                oAdapter.Update(oTable);

                oCommand.Dispose();
                oTable.Dispose();
            }

            oConnection.Close();
            oConnection.Dispose();
        }

        #endregion

        #region " LoadArchiveEvent "
        public override List<TimeElement> LoadArchiveEvent(string EmployeeID, DateTime BeginDate, DateTime EndDate, int RecordCount)
        {
            List<TimeElement> oReturn = new List<TimeElement>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlParameter oParam;
            SqlCommand oCommand = null;
            SqlDataAdapter oAdapter = null;
            DataTable oTable = null;
            if (RecordCount > 0)
            {
                oCommand = new SqlCommand(string.Format("select distinct top {0} * from TimeEventArchive where EmployeeID = @employeeid and EventTime between @BeginDate and @EndDate order by EventTime Desc", RecordCount), oConnection);
            }
            else
            {
                oCommand = new SqlCommand("select distinct * from TimeEventArchive where EmployeeID = @employeeid and EventTime between @BeginDate and @EndDate order by EventTime Desc", oConnection);
            }
            oParam = new SqlParameter("@employeeid", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);
            oAdapter = new SqlDataAdapter(oCommand);
            oTable = new DataTable("TIMEEVENTARCHIVE");
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Dispose();
            oCommand.Dispose();
            oAdapter.Dispose();

            TimeElement item;
            foreach (DataRow dr in oTable.Rows)
            {
                item = new TimeElement();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }

            oTable.Dispose();

            return oReturn;
        }
        #endregion

        #region " LoadTextData "
        public override string LoadTextData(string Code, string Language)
        {
            string returnValue = "";
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("select dbo.GetCommonTextItem(@Code,@Language) As TextDescription", oConnection);
            SqlParameter oParam;

            oParam = new SqlParameter("@Code", SqlDbType.VarChar);
            oParam.Value = Code;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@Language", SqlDbType.VarChar);
            oParam.Value = Language;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable("TEXTTABLE");
            oConnection.Open();
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                returnValue = (string)dr["TextDescription"];
            }
            oTable.Dispose();
            return returnValue;
        }
        #endregion

        #region " SaveArchiveEvent "
        public override void SaveArchiveEvent(string EmployeeID, DateTime BeginDate, DateTime EndDate, List<TimeElement> Data)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlParameter oParam;
            SqlCommand oCommand = null;
            SqlDataAdapter oAdapter = null;
            DataTable oTable = null;
            SqlCommandBuilder oCB = null;
            oCommand = new SqlCommand("select * from TimeEventArchive where EmployeeID = @employeeid and EventTime between @BeginDate and @EndDate", oConnection);
            oParam = new SqlParameter("@employeeid", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);
            oTable = new DataTable("TIMEEVENTARCHIVE");
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);

            DataRow dr;
            try
            {
                foreach (DataRow row in oTable.Rows)
                    row.Delete();
                oAdapter.Update(oTable);

                foreach (TimeElement item in Data.FindAll(delegate(TimeElement te)
                                            {
                                                return (te.EventTime.Date >= BeginDate && te.EventTime <= EndDate);
                                            }))
                {
                    dr = oTable.NewRow();
                    item.LoadDataToTableRow(dr);
                    try
                    {
                        oTable.LoadDataRow(dr.ItemArray, false);
                    }
                    catch (ConstraintException ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                }

                oAdapter.Update(oTable);

            }
            catch (Exception ex)
            {
                throw new Exception("Can't archive data", ex);
            }
            finally
            {
                oConnection.Dispose();
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                if (oCommand != null)
                {
                    oCommand.Dispose();
                }
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oTable != null)
                {
                    oTable.Dispose();
                }
            }
        }
        #endregion

        #region " SaveArchiveTimePair "
        public override void SaveArchiveTimePair(string EmployeeID, DateTime BeginDate, DateTime EndDate, List<TimePair> list)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlParameter oParam;
            SqlCommand oCommand = null;
            SqlDataAdapter oAdapter = null;
            DataTable oTable = null;
            SqlCommandBuilder oCB = null;
            oCommand = new SqlCommand("select * from TimePairArchive where EmployeeID = @employeeid and [Date] between @BeginDate and @EndDate", oConnection);
            oParam = new SqlParameter("@employeeid", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);
            oAdapter = new SqlDataAdapter(oCommand);
            oCB = new SqlCommandBuilder(oAdapter);
            oTable = new DataTable("TIMEPAIRARCHIVE");
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);

            DataRow dr;
            int seqno = 0;
            DateTime lastDate = DateTime.MinValue;
            try
            {
                foreach (DataRow row in oTable.Rows)
                    row.Delete();
                oAdapter.Update(oTable);

                list.Sort(new TimePairSortingDate());
                foreach (TimePair item in list.FindAll(delegate(TimePair tp)
                                           {
                                               return (tp.Date >= BeginDate && tp.Date <= EndDate);
                                           }))
                {
                    dr = oTable.NewRow();
                    item.LoadDataToTableRow(dr);
                    try
                    {
                        item.SeqNo = seqno;
                        item.LoadDataToTableRow(dr);
                        oTable.LoadDataRow(dr.ItemArray, false);
                        seqno++;
                    }
                    catch (ConstraintException ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                }

                oAdapter.Update(oTable);
            }
            catch (Exception ex)
            {
                throw new Exception("Can't archive data", ex);
            }
            finally
            {
                oConnection.Dispose();
                if (oCB != null)
                {
                    oCB.Dispose();
                }
                if (oCommand != null)
                {
                    oCommand.Dispose();
                }
                if (oAdapter != null)
                {
                    oAdapter.Dispose();
                }
                if (oTable != null)
                {
                    oTable.Dispose();
                }
            }
        }
        #endregion

        #region " LoadArchiveTimePair "
        #region " Old "
        //public override List<TimePair> LoadArchiveTimePair(string EmployeeID, DateTime BeginDate, DateTime EndDate, bool OnlyFirstRecord)
        //{
        //    List<TimePair> pairs = new List<TimePair>();
        //    SqlConnection oConnection = new SqlConnection(this.ConnectionString);
        //    SqlParameter oParam;
        //    SqlCommand oCommand = null;
        //    SqlDataAdapter oAdapter = null;
        //    DataTable oTable = null;
        //    if (OnlyFirstRecord)
        //    {
        //        oCommand = new SqlCommand("select top 1 * from TimePairArchive where EmployeeID = @employeeid and [Date] between @BeginDate and @EndDate order by [Date], IsNull(ClockIN,ClockOUT)", oConnection);
        //    }
        //    else
        //    {
        //        oCommand = new SqlCommand("select * from TimePairArchive where EmployeeID = @employeeid and [Date] between @BeginDate and @EndDate", oConnection);
        //    }
        //    oParam = new SqlParameter("@employeeid", SqlDbType.VarChar);
        //    oParam.Value = EmployeeID;
        //    oCommand.Parameters.Add(oParam);
        //    oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
        //    oParam.Value = BeginDate;
        //    oCommand.Parameters.Add(oParam);
        //    oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
        //    oParam.Value = EndDate;
        //    oCommand.Parameters.Add(oParam);
        //    oAdapter = new SqlDataAdapter(oCommand);
        //    oTable = new DataTable("TIMEPAIRARCHIVE");
        //    oAdapter.FillSchema(oTable, SchemaType.Source);
        //    oAdapter.Fill(oTable);
        //    oCommand.Dispose();
        //    oAdapter.Dispose();

        //    TimePair item = null;
        //    foreach (DataRow dr in oTable.Rows)
        //    {
        //        item = new TimePair();
        //        item.ParseToObject(dr);
        //        pairs.Add(item);
        //    }
        //    if (!OnlyFirstRecord)
        //    {
        //        if (item != null && item.ClockOUT == null)
        //        {
        //            oCommand = new SqlCommand("select top 1 * from TimePairArchive where EmployeeID = @employeeid and [Date] > @EndDate order by [Date],SeqNo", oConnection);
        //            oParam = new SqlParameter("@employeeid", SqlDbType.VarChar);
        //            oParam.Value = EmployeeID;
        //            oCommand.Parameters.Add(oParam);
        //            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
        //            oParam.Value = EndDate;
        //            oCommand.Parameters.Add(oParam);
        //            oAdapter = new SqlDataAdapter(oCommand);
        //            oTable = new DataTable("TIMEPAIRARCHIVE");
        //            oAdapter.FillSchema(oTable, SchemaType.Source);
        //            oAdapter.Fill(oTable);
        //            foreach (DataRow dr in oTable.Rows)
        //            {
        //                item = new TimePair();
        //                item.ParseToObject(dr);
        //                if (item.ClockIN == null)
        //                {
        //                    pairs.Add(item);
        //                }
        //            }
        //        }
        //        else if (item != null && item.ClockIN == null)
        //        {
        //            oCommand = new SqlCommand("select top 1 * from TimePairArchive where EmployeeID = @employeeid and [Date] < @BeginDate order by [Date] Desc,SeqNo Desc", oConnection);
        //            oParam = new SqlParameter("@employeeid", SqlDbType.VarChar);
        //            oParam.Value = EmployeeID;
        //            oCommand.Parameters.Add(oParam);
        //            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
        //            oParam.Value = BeginDate;
        //            oCommand.Parameters.Add(oParam);
        //            oAdapter = new SqlDataAdapter(oCommand);
        //            oTable = new DataTable("TIMEPAIRARCHIVE");
        //            oAdapter.FillSchema(oTable, SchemaType.Source);
        //            oAdapter.Fill(oTable);
        //            foreach (DataRow dr in oTable.Rows)
        //            {
        //                item = new TimePair();
        //                item.ParseToObject(dr);
        //                if (item.ClockOUT == null)
        //                {
        //                    pairs.Add(item);
        //                }
        //            }
        //        }

        //    }
        //    oTable.Dispose();
        //    pairs.Sort(new TimePairSortingDate());
        //    return pairs;
        //}
        #endregion
        #endregion

        #region " LoadArchiveEventRelative "
        #region " Old "
        //public override List<TimeElement> LoadArchiveEventRelative(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        //{
        //    List<TimePair> relativePair = LoadArchiveTimePair(EmployeeID, BeginDate, EndDate);
        //    DateTime date1, date2;
        //    if (relativePair.Count > 0)
        //    {
        //        date1 = relativePair[0].Date;
        //        date2 = relativePair[relativePair.Count - 1].Date;
        //    }
        //    else
        //    {
        //        date1 = BeginDate;
        //        date2 = EndDate;
        //    }

        //    List<TimeElement> oReturn = new List<TimeElement>();
        //    SqlConnection oConnection = new SqlConnection(this.ConnectionString);
        //    SqlParameter oParam;
        //    SqlCommand oCommand = null;
        //    SqlDataAdapter oAdapter = null;
        //    DataTable oTable = null;
        //    oCommand = new SqlCommand("select * from GetRelativeTimeElement(@employeeid,@BeginDate,@EndDate)", oConnection);
        //    oParam = new SqlParameter("@employeeid", SqlDbType.VarChar);
        //    oParam.Value = EmployeeID;
        //    oCommand.Parameters.Add(oParam);
        //    oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
        //    oParam.Value = date1;
        //    oCommand.Parameters.Add(oParam);
        //    oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
        //    oParam.Value = date2;
        //    oCommand.Parameters.Add(oParam);

        //    oAdapter = new SqlDataAdapter(oCommand);
        //    oTable = new DataTable("TIMEEVENTARCHIVE");
        //    oAdapter.FillSchema(oTable, SchemaType.Source);
        //    oAdapter.Fill(oTable);
        //    oConnection.Dispose();
        //    oCommand.Dispose();
        //    oAdapter.Dispose();

        //    TimeElement item;
        //    foreach (DataRow dr in oTable.Rows)
        //    {
        //        item = new TimeElement();
        //        item.ParseToObject(dr);
        //        oReturn.Add(item);
        //    }

        //    oTable.Dispose();

        //    return oReturn;
        //}
        #endregion
        #endregion

        #region " LoadTimesheet "
        public override List<TimesheetData> LoadTimesheet(string EmployeeID, string ApplicationKey, string SubKey1, string SubKey2, DateTime BeginDate, DateTime EndDate)
        {
            List<TimesheetData> oReturn = new List<TimesheetData>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("TIMESHEET");
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("select * from Timesheet Where EmployeeID = @EmployeeID and BeginDate <= @EndDate and EndDate >= @BeginDate and (ApplicationKey = @ApplicationKey or @ApplicationKey = '*') and (SubKey1 = @SubKey1 or @SubKey1 = '*')  and (SubKey2 = @SubKey2 or @SubKey2 = '*') ", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@ApplicationKey", SqlDbType.VarChar);
            oParam.Value = ApplicationKey;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@SubKey1", SqlDbType.VarChar);
            oParam.Value = SubKey1;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@SubKey2", SqlDbType.VarChar);
            oParam.Value = SubKey2;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                TimesheetData item = new TimesheetData();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }
            oTable.Dispose();
            return oReturn;
        }
        #endregion

        #endregion

        #region " LoadApplicationCollision "
        private List<CollisionControl> LoadApplicationCollision(string ApplicationKey, string SubKey1, string SubKey2)
        {
            List<CollisionControl> oReturn = new List<CollisionControl>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("COLLISIONCONTROL");
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("select * from CollisionControl Where ApplicationKeyNew = @ApplicationKey and (SubKey1New = '*' or SubKey1New = @SubKey1) and (SubKey2New = '*' or SubKey2New = @SubKey2)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ApplicationKey", SqlDbType.VarChar);
            oParam.Value = ApplicationKey;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@SubKey1", SqlDbType.VarChar);
            oParam.Value = SubKey1;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@SubKey2", SqlDbType.VarChar);
            oParam.Value = SubKey2;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                CollisionControl item = new CollisionControl();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }
            oTable.Dispose();
            return oReturn;

        }
        #endregion

        #region " LoadTimesheetCollision "
        private List<TimesheetData> LoadTimesheetCollision(string EmployeeID, DateTime BeginDate, DateTime EndDate, CollisionControl Key)
        {
            List<TimesheetData> oReturn = new List<TimesheetData>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("TIMESHEETDATA");
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("select * from FindCollision(@EmployeeID, @BeginDate, @EndDate, @ApplicationKey, @SubKey1, @SubKey2)", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@ApplicationKey", SqlDbType.VarChar);
            oParam.Value = Key.ApplicationKeyExists;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@SubKey1", SqlDbType.VarChar);
            oParam.Value = Key.SubKey1Exists;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@SubKey2", SqlDbType.VarChar);
            oParam.Value = Key.SubKey2Exists;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                TimesheetData item = new TimesheetData();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }
            oTable.Dispose();
            return oReturn;

        }
        #endregion

        #region " LoadAdditionalDataCollisionable "
        private ICollisionable LoadAdditionalDataCollisionable(string ApplicationKey)
        {
            ICollisionable oReturn = null;
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("ADDITIONALCOLLISION");
            oTable.CaseSensitive = false;
            oCommand = new SqlCommand("select * from AdditionalCollision Where ApplicationKey = @ApplicationKey", oConnection);
            SqlParameter oParam;
            oParam = new SqlParameter("@ApplicationKey", SqlDbType.VarChar);
            oParam.Value = ApplicationKey;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                Assembly oAssembly;
                Type oType;
                string assemblyName = (string)dr["AssemblyName"];
                string typeName = (string)dr["ClassName"];
                oAssembly = Assembly.Load(assemblyName);
                if (oAssembly != null)
                {
                    oType = oAssembly.GetType(typeName);
                    if (oType != null)
                    {

                        if (oType.GetInterfaceMap(typeof(ICollisionable)).InterfaceType != null)
                        {
                            oReturn = (ICollisionable)Activator.CreateInstance(oType);
                        }
                    }
                }
                break;
            }
            oTable.Dispose();
            return oReturn;
        }
        #endregion

        public override List<TimePairArchive> LoadTimePairArchiveOverlap(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            List<TimePairArchive> Result = new List<TimePairArchive>();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            SqlCommand oCommand = new SqlCommand("sp_LoadTimePairArchiveOverlap", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter oParam;
            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);
            oParam = new SqlParameter("@p_EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);

            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);
            DataTable oTable = new DataTable();
            oAdapter.Fill(oTable);


            TimePairArchive item;
            foreach (DataRow row in oTable.Rows)
            {
                item = new TimePairArchive();
                item.ParseToObject(row);
                Result.Add(item);
            }

            return Result;
        }

        //AddBy: Ratchatawan W. (2012-11-20)
        public override void SaveTimePairArchiveMappingByUser(List<TimePair> TimePairList)
        {
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();

            SqlTransaction oTransaction = oConnection.BeginTransaction();
            SqlCommand oCommand = new SqlCommand("sp_TimePairArchiveMappingByUserSave", oConnection,oTransaction);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;

            try
            {
                foreach (TimePair oTimePair in TimePairList)
                {
                    oCommand.Parameters.Clear();

                    oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
                    oParam.Value = oTimePair.EmployeeID;
                    oCommand.Parameters.Add(oParam);

                    oParam = new SqlParameter("@p_Date", SqlDbType.DateTime);
                    oParam.Value = oTimePair.Date;
                    oCommand.Parameters.Add(oParam);

                    if (oTimePair.ClockIN != null)
                    {
                        oParam = new SqlParameter("@p_ClockIN", SqlDbType.DateTime);
                        oParam.Value = oTimePair.ClockIN.EventTime;
                        oCommand.Parameters.Add(oParam);
                    }
                    if (oTimePair.ClockOUT != null)
                    {
                        oParam = new SqlParameter("@p_ClockOUT", SqlDbType.DateTime);
                        oParam.Value = oTimePair.ClockOUT.EventTime;
                        oCommand.Parameters.Add(oParam);
                    }

                    oCommand.ExecuteNonQuery();
                }
                oTransaction.Commit();
            }
            catch (Exception ex)
            {
                oTransaction.Rollback();
            }
            finally
            {
                oConnection.Close();
                oCommand.Dispose();
            }
        }

        public override List<TimePair> LoadTimePairArchiveMappingByUser(string EmployeeID, DateTime BeginDate, DateTime EndDate)
        {
            List<TimePair> oReturn = new List<TimePair>();
            DataTable oTable = new DataTable();
            SqlConnection oConnection = new SqlConnection(this.BaseConnStr);
            oConnection.Open();

            SqlCommand oCommand = new SqlCommand("sp_LoadTimePairArchiveMappingByUser", oConnection);
            oCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter oParam;
            SqlDataAdapter oAdapter = new SqlDataAdapter(oCommand);

            oParam = new SqlParameter("@p_EmployeeID", SqlDbType.VarChar);
            oParam.Value = EmployeeID;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@p_EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);

            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();

            TimePair oTimePair;
            foreach (DataRow dr in oTable.Rows)
            {
                oTimePair = new TimePair();
                oTimePair.ParseToObject(dr);
                oReturn.Add(oTimePair);
            }

            return oReturn;
        }
    }
}