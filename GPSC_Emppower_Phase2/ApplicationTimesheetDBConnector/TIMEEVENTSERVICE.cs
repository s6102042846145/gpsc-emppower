using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Text;

namespace ESS.TIMESHEET.TIMEEVENT.DB
{
    public class TIMEEVENTSERVICE : ITimeEventService
    {
        #region " private data "
        private static Configuration __config;
        private static Configuration config
        {
            get
            {
                if (__config == null)
                {
                    __config = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().CodeBase.Replace("file:///", "").Replace("/", "\\"));
                }
                return __config;
            }
        }
        private string ConnectionString
        {
            get
            {
                if (config == null || config.AppSettings.Settings["BaseConnStr"] == null)
                {
                    return "";
                }
                else
                {
                    return config.AppSettings.Settings["BaseConnStr"].Value;
                }
            }
        }
        private string GetConnectionString(string profile)
        {
            return config.AppSettings.Settings[profile].Value;
        }
        #endregion

        #region ITimeEventService Members

        public List<TimeElement> GetTimeElement(string CardNo, DateTime BeginDate, DateTime EndDate, int RecordCount)
        {
            // recordCount = -1 mean get all record
            List<TimeElement> oReturn = new List<TimeElement>();
            SqlConnection oConnection = new SqlConnection(this.ConnectionString);
            SqlCommand oCommand;
            SqlDataAdapter oAdapter;
            DataTable oTable = new DataTable("TIMEEVENT");
            oTable.CaseSensitive = false;
            if (RecordCount == -1)
            {
                oCommand = new SqlCommand("select * from TimeEvents_BOSCH Where CardNo = @CardNo and EventTime <= @EndDate and EventTime > @BeginDate ", oConnection);
            }
            else
            {
                oCommand = new SqlCommand(string.Format("select Top {0} * from TimeEvents_BOSCH Where CardNo = @CardNo and EventTime <= @EndDate and EventTime > @BeginDate order by EventTime Desc", RecordCount), oConnection);
            }
            SqlParameter oParam;
            oParam = new SqlParameter("@CardNo", SqlDbType.VarChar);
            oParam.Value = CardNo;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@BeginDate", SqlDbType.DateTime);
            oParam.Value = BeginDate;
            oCommand.Parameters.Add(oParam);

            oParam = new SqlParameter("@EndDate", SqlDbType.DateTime);
            oParam.Value = EndDate;
            oCommand.Parameters.Add(oParam);

            oAdapter = new SqlDataAdapter(oCommand);
            oConnection.Open();
            oAdapter.FillSchema(oTable, SchemaType.Source);
            oAdapter.Fill(oTable);
            oConnection.Close();
            oConnection.Dispose();
            oCommand.Dispose();
            foreach (DataRow dr in oTable.Rows)
            {
                TimeElement item = new TimeElement();
                item.ParseToObject(dr);
                oReturn.Add(item);
            }
            oTable.Dispose();
            return oReturn;
        }

        #endregion
    }
}
